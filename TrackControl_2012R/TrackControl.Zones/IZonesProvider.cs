using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
  public interface IZonesProvider
  {
    void Save(IZone zone);
    void SaveGroupChanging(IEnumerable<IZone> zones);
    bool Delete(IZone zone);
    void Delete(IEnumerable<IZone> zones);
    IList<IZone> GetForGroup(ZonesGroup group);
    IZone GetZone(int zoneId);
    ZonesGroup GetZoneGroupe(int zoneGroupeId);
  }
}
