using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
  public class ZonesLocator : IZonesLocator
  {
    ITreeModel<IZone> _model;

    public ZonesLocator(ITreeModel<IZone> model)
    {
      _model = model;
    }

    public IEnumerable<IZone> GetZonesWithPoint(PointLatLng point)
    {
      foreach (IZone zone in _model.GetAll())
      {
        if(zone.Contains(point))
          yield return zone;
      }
    }

    public IEnumerable<IZone> GetCheckedZonesWithPoint(PointLatLng point)
    {
      foreach(IZone zone in _model.Checked)
      {
        if (zone.Contains(point))
          yield return zone;
      }
    }
  }
}
