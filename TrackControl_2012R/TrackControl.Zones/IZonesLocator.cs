using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
    public interface IZonesLocator
    {
        /// <summary>
        /// ���������� ��������� ����������� ���, ������� ��������
        /// ��������� �������������� �����
        /// </summary>
        /// <param name="point">����� � �������������� �����������</param>
        IEnumerable<IZone> GetZonesWithPoint(PointLatLng point);

        /// <summary>
        /// ���������� ��������� ���������� ����������� ���, �������
        /// �������� ��������� �������������� �����
        /// </summary>
        /// <param name="point">����� � �������������� �����������</param>
        IEnumerable<IZone> GetCheckedZonesWithPoint(PointLatLng point);
    }
}
