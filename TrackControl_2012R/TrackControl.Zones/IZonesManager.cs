using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
  public interface IZonesManager : IEntityFinder<IZone>
  {
    bool Enabled { get; }
    ZonesGroup Root { get; }
    List<ZonesGroup> Groups { get; }
    List<IZone> Zones { get; }

    //event LoadingComplete LoadingComplete;
    event Action<IEnumerable<IZone>> ZonesAdded;
    event Action<IEnumerable<IZone>> ZonesRemoved;
    event VoidHandler GroupsCountChanged;

    void StartLoading();
    
    void AddZones(IEnumerable<IZone> zones);
    void SaveZone(IZone zone);
    void RemoveZone(IZone zone);

    void AddGroup(ZonesGroup group);
    void SaveGroup(ZonesGroup group);
    void RemoveGroupOnly(ZonesGroup group);
    void RemoveGroupFully(ZonesGroup group);

    ZonesGroup GetGroupeByName(string groupeName);
    IZone GetZoneByName(string zoneName);
    IZone GetZoneByNameAndGroup(string zoneName, ZonesGroup zoneGroup);
    IZone GetZoneById(int zoneId);
  }
}
