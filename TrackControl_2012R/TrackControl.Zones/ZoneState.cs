namespace TrackControl.Zones
{
  public enum ZoneState
  {
    IsVisible = 0x1,
    IsStop = 0x2,
    IsRoute = 0x4
  }
}