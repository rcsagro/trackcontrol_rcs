using System;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.General.DAL;

namespace TrackControl.Zones
{
    public class ZonesManager : IZonesManager
    {
        private bool _enabled;
        private ZonesGroup _root;
        private IZonesProvider _zonesProvider;
        private IZoneGroupsProvider _groupsProvider;

        //public event LoadingComplete LoadingComplete;
        public event Action<IEnumerable<IZone>> ZonesAdded;
        public event Action<IEnumerable<IZone>> ZonesRemoved;
        public event VoidHandler GroupsCountChanged;

        public ZonesManager(IZonesProvider zonesProvider, IZoneGroupsProvider groupsProvider)
        {
            if (null == zonesProvider)
                throw new ArgumentNullException("zonesProvider");
            _zonesProvider = zonesProvider;

            if (null == groupsProvider)
                throw new ArgumentNullException("groupsProvider");
            _groupsProvider = groupsProvider;

            _root = ZonesGroup.CreateRoot();
        }

        public IZone GetById(int id)
        {
            foreach (IZone zone in _root.AllItems)
            {
                if (id == zone.Id)
                    return zone;
            }
            return null;
        }

        public ZonesGroup GetGroupeByName(string groupeName)
        {
            foreach (ZonesGroup zoneGroup in Groups)
            {
                if (groupeName == zoneGroup.Name)
                    return zoneGroup;
            }
            return null;
        }

        public bool Enabled
        {
            get { return _enabled; }
        }

        public ZonesGroup Root
        {
            get { return _root; }
        }

        public List<ZonesGroup> Groups
        {
            get
            {
                IList<Group<IZone>> allGroups = _root.AllGroups;
                List<ZonesGroup> groups = new List<ZonesGroup>(1 + allGroups.Count);
                groups.Add(_root);
                foreach (ZonesGroup group in allGroups)
                {
                    groups.Add(group);
                }
                return groups;
            }
        }

        public List<IZone> Zones
        {
            get { return new List<IZone>(_root.AllItems); }
        }

        public delegate void ReportPassData(List<IZone> zones);
        
        public void StartLoading()
        {
                foreach (ZonesGroup group in _groupsProvider.GetAll())
                {
                    if (group.Id > 1)
                        _root.AddGroup(group);
                    foreach (IZone zone in _zonesProvider.GetForGroup(group))
                    {
                        if (group.Id > 1)
                            group.AddZone(zone);
                        else
                            _root.AddZone(zone);
                    } // foreach
                } // foreach
            _enabled = true;
        } // StartLoading

        public void AddZones(IEnumerable<IZone> zones)
        {
            if (null == zones)
                throw new ArgumentNullException("zones");

            OnZonesAdded(zones);
        }

        public void SaveZone(IZone zone)
        {
            _zonesProvider.Save(zone);
        }

        public IZone GetZoneByName(string zoneName)
        {
            foreach (IZone zone in _root.AllItems)
            {
                if (zoneName == zone.Name)
                    return zone;
            }
            return null;
        }

        public IZone GetZoneByNameAndGroup(string zoneName, ZonesGroup zoneGroup)
        {
            foreach (IZone zone in _root.AllItems)
            {
                if (zoneName == zone.Name && zoneGroup == zone.Group)
                    return zone;
            }
            return null;
        }

        public IZone GetZoneById(int zoneId)
        {
            foreach (IZone zone in _root.AllItems)
            {
                if (zoneId == zone.Id)
                    return zone;
            }
            return null;
        }

        public void RemoveZone(IZone zone)
        {
            if (_zonesProvider.Delete(zone))
            {
                zone.Group.RemoveZone(zone);
                IZone[] zones = new IZone[] {zone};
                OnZonesRemoved(zones);
            }
        }

        public void AddGroup(ZonesGroup group)
        {
            if (null == group)
                throw new ArgumentNullException("group");

            _root.AddGroup(group);
        }

        public void SaveGroup(ZonesGroup group)
        {
            bool isNew = group.IsNew;
            _groupsProvider.Save(group);
            if (isNew) OnGroupsCountChanged();
        }

        public void RemoveGroupOnly(ZonesGroup group)
        {
            IList<IZone> zones = group.AllItems;
            foreach (IZone zone in zones)
            {
                zone.Group = _root;
            }
            _root.RemoveGroup(group);

            _zonesProvider.SaveGroupChanging(zones);
            _groupsProvider.Delete(group);
            OnGroupsCountChanged();
        }

        public void RemoveGroupFully(ZonesGroup group)
        {
            OnZonesRemoved(group.AllItems);
            _root.RemoveGroup(group);
            _groupsProvider.Delete(group);
            OnGroupsCountChanged();
        }

        private void OnZonesAdded(IEnumerable<IZone> zones)
        {
            Action<IEnumerable<IZone>> handler = ZonesAdded;
            if (null != handler)
                handler(zones);
        }

        private void OnZonesRemoved(IEnumerable<IZone> zones)
        {
            Action<IEnumerable<IZone>> handler = ZonesRemoved;
            if (null != handler)
                handler(zones);
        }

        private void OnGroupsCountChanged()
        {
            VoidHandler handler = GroupsCountChanged;
            if (null != handler)
                handler();
        }

        public List<IZone> ZonesInTrackArea()
        {
            List<IZone> zonesInTA = new List<IZone>();

            return zonesInTA;
        }
    }
}