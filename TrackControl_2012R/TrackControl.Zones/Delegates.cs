using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
  public delegate void LoadingComplete();
  public delegate void ZonesVisibilityChanged(IList<IZone> zones, bool visibility);
}
