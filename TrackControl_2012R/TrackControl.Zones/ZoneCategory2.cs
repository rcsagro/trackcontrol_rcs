﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;

namespace TrackControl.Zones
{
    public class ZoneCategory2 : Entity
    {
        public ZoneCategory2()
        {
            // to do this
        }

        public string Name { get; set; }
        
        public ZoneCategory2(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public ZoneCategory2(int id)
        {
            ZoneCategory2 zc = ZonesCategoryProvider.GetCategory2(id);

            if (zc != null)
            {
                Id = zc.Id;
                Name = zc.Name;
            }
        }

        public bool Save()
        {
            if( IsNew )
            {
                Id = ZonesCategoryProvider.Insert2( this );
                return ( Id != ConstsGen.RECORD_MISSING );
            }
            else
            {
                return ZonesCategoryProvider.Update2( this );
            }
        }

        public bool Delete()
        {
            return ZonesCategoryProvider.Delete2( this );
        }

        public override string ToString()
        {
            return String.Format( "{0}", Name );
        }
    }
}
