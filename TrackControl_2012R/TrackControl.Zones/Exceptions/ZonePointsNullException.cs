using System;

namespace TrackControl.Zones
{
  public class ZonePointsNullException : Exception
  {
    public ZonePointsNullException()
      : base("Zones points can not be NULL")
    {
    }

    public ZonePointsNullException(string message)
      : base(message)
    {
    }

    public override string ToString()
    {
      return Message;
    }
  }
}
