using System;

namespace TrackControl.Zones
{
  public class NotEnoughPointsException : Exception
  {
    public NotEnoughPointsException()
      : base("Check Zone must contain at least three points")
    {
    }

    public NotEnoughPointsException(string message)
      : base(message)
    {
    }

    public override string ToString()
    {
      return Message;
    }
  }
}
