using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones
{
  public interface IZoneGroupsProvider
  {
    IList<ZonesGroup> GetAll();
    bool Save(ZonesGroup group);
    bool Delete(ZonesGroup group);
  }
}
