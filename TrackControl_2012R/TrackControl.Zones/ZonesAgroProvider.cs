﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General.DatabaseDriver;
using TrackControl.General;
using TrackControl.General.DAL;

namespace TrackControl.Zones
{
    public static class ZonesAgroProvider
    {
        public static string GetCultureActive(int idZone)
        {
            string agroCulture = "";
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateToday", DateTime.Today);
                string sql = string.Format(TrackControlQuery.ZonesAgroProvider.GetCultureActive, idZone, driverDb.ParamPrefics);
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                if (driverDb.Read())
                {
                    agroCulture = driverDb.GetString("SeasonName");
                }
            }
            return agroCulture;
        }

        public static string GetCultureNameActive(int idZone)
        {
            string agroCulture = "";
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateToday", DateTime.Today);
                string sql = string.Format(TrackControlQuery.ZonesAgroProvider.GetCultureNameActive, idZone, driverDb.ParamPrefics);
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                if (driverDb.Read())
                {
                    agroCulture = driverDb.GetString("SeasonName");
                }
            }

            return GetName(agroCulture);
        }

        private static string GetName(string source)
        {
            string name = "";
            string[] dateStrings = source.Split(',');

            if(dateStrings.Length == 1)
            {
                name = string.Format("{0} {1}", dateStrings[0], "");
                return name;
            }

            if (dateStrings.Length == 2)
            {
                name = string.Format("{0} {1}", dateStrings[0], dateStrings[1]);
                return name;
            }

            if (dateStrings.Length == 3)
            {

                name = string.Format("{0} {1}", dateStrings[0], dateStrings[1]);
                if (Convert.ToInt32(dateStrings[1]) < Convert.ToInt32(dateStrings[2]))
                    name = string.Format("{0}/{1}", name, dateStrings[2]);
                return name;
            }

            return name;
        }
    }
}
