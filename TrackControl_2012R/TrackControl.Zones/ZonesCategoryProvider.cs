﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.Zones
{
    public static class ZonesCategoryProvider
    {
        #region CATEGORY
        public static BindingList<ZoneCategory> GetList()
        {
            BindingList<ZoneCategory> zCats = new BindingList<ZoneCategory>();
            ZoneCategory zCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();
                db.GetDataReader( TrackControlQuery.ZoneCategoryProvider.SelectAll );

                while( db.Read() )
                {
                    zCat = GetRecord( zCat, db );
                    zCats.Add( zCat );
                }
            }

            return zCats;
        }

        private static ZoneCategory GetRecord( ZoneCategory zCat, DriverDb db )
        {
            zCat = new ZoneCategory( db.GetInt32( "Id" ), db.GetString( "Name" ) );
            return zCat;
        }

        public static ZoneCategory GetCategory( int idCategory )
        {
            ZoneCategory zCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                db.GetDataReader( string.Format( TrackControlQuery.ZoneCategoryProvider.SelectOne, idCategory ) );

                if( db.Read() )
                {
                    zCat = GetRecord( zCat, db );
                }
            }
            return zCat;
        }

        public static bool Update( ZoneCategory vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 2 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    db.ExecuteNonQueryCommand( TrackControlQuery.ZoneCategoryProvider.UpdateOne, db.GetSqlParameterArray );

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static int Insert( ZoneCategory vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    return db.ExecuteReturnLastInsert( TrackControlQuery.ZoneCategoryProvider.InsertOne, db.GetSqlParameterArray, "zone_category" );
                }
            }
            catch
            {

                return ConstsGen.RECORD_MISSING;
            }

            return ConstsGen.RECORD_MISSING;
        }

        public static bool Delete( ZoneCategory zc )
        {

            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Id", zc.Id );
                    db.ExecuteNonQueryCommand( TrackControlQuery.ZoneCategoryProvider.DeleteOne, db.GetSqlParameterArray );
                    return true;
                }
            }
            catch
            {

                return false;
            }
        }

        public static int CountVehiclesWithCategory( ZoneCategory vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    return db.GetScalarValueNull<int>( TrackControlQuery.ZoneCategoryProvider.CountVehicleWithCategory, db.GetSqlParameterArray, 0 );
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message );
                return 0;
            }
        }
        #endregion

        #region CATEGORY2
        public static BindingList<ZoneCategory2> GetList2()
        {
            BindingList<ZoneCategory2> zCats = new BindingList<ZoneCategory2>();
            ZoneCategory2 zCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();
                db.GetDataReader( TrackControlQuery.ZoneCategoryProvider.SelectAll2 );

                while( db.Read() )
                {
                    zCat = GetRecord2( zCat, db );
                    zCats.Add( zCat );
                }
            }

            return zCats;
        }

        private static ZoneCategory2 GetRecord2( ZoneCategory2 zCat, DriverDb db )
        {
            zCat = new ZoneCategory2( db.GetInt32( "Id" ), db.GetString( "Name" ) );
            return zCat;
        }

        public static ZoneCategory2 GetCategory2( int idCategory )
        {
            ZoneCategory2 zCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                db.GetDataReader( string.Format( TrackControlQuery.ZoneCategoryProvider.SelectOne2, idCategory ) );

                if( db.Read() )
                {
                    zCat = GetRecord2( zCat, db );
                }
            }
            return zCat;
        }

        public static bool Update2( ZoneCategory2 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 2 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    db.ExecuteNonQueryCommand( TrackControlQuery.ZoneCategoryProvider.UpdateOne2, db.GetSqlParameterArray );

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static int Insert2( ZoneCategory2 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    return db.ExecuteReturnLastInsert( TrackControlQuery.ZoneCategoryProvider.InsertOne2, db.GetSqlParameterArray, "zone_category2" );
                }
            }
            catch
            {

                return ConstsGen.RECORD_MISSING;
            }

            return ConstsGen.RECORD_MISSING;
        }

        public static bool Delete2( ZoneCategory2 zc )
        {

            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Id", zc.Id );
                    db.ExecuteNonQueryCommand( TrackControlQuery.ZoneCategoryProvider.DeleteOne2, db.GetSqlParameterArray );
                    return true;
                }
            }
            catch
            {

                return false;
            }
        }

        public static int CountVehiclesWithCategory( ZoneCategory2 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    return db.GetScalarValueNull<int>( TrackControlQuery.ZoneCategoryProvider.CountVehicleWithCategory2, db.GetSqlParameterArray, 0 );
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message );
                return 0;
            }
        }
        #endregion
    }
}
