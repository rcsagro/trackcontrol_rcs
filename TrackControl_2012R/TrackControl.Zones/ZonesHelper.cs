using System;
using TrackControl.General;

namespace TrackControl.Zones
{
  public static class ZonesHelper
  {
    public static bool DoesZoneContainPoint(IZone zone, PointLatLng point)
    {
      if (!zone.Bounds.Contains(point))
        return false;

      bool parity = false;
      PointLatLng target = point;
      PointLatLng begin = zone.Points[zone.Points.Length - 1];
      PointLatLng end;

      for (int i = 0; i < zone.Points.Length; i++)
      {
        end = zone.Points[i];
        EdgeType t = GetEdgeType(target, new EdgeLatLng(begin, end));
        if (EdgeType.Touching == t)
          return true;
        if (EdgeType.Crossing == t)
          parity = !parity;

        begin = end;
      }

      return parity;
    }

    /// <summary>
    /// ����������� ���� ����� �� ��������� ������������ ����� �
    /// ���� ���������� �� ����� point.
    /// 
    /// <para>���� ����� point ����� ����� �� ����� edge, �� ����� �������� ������������, 
    /// ������ ���� ������� ������ ����� (edge.BeginPoint) ����� ���� ����, 
    /// � ������� ����� (edge.EndPoint) ����������� �� ���� ��� ����� ���� ���. 
    /// ����� ����� �� ����� ���� �������������� � ��� ������ ���������� ����� 
    /// � ��������� �����, ������������ �� ��� ������� �����. 
    /// 
    /// ���� ����� point ��������� ������ �� ����� edge, �� ���� �������� ����� 
    /// edge.BeginPoint � edge.EndPoint ������� ��������.</para>
    /// </summary>
    /// <param name="point">�����</param>
    /// <param name="edge">�����</param>
    /// <returns>EdgeType</returns>
    static EdgeType GetEdgeType(PointLatLng point, EdgeLatLng edge)
    {
      PointLinePosition position = ClassifyPosition(point, edge);
      switch (position)
      {
        case PointLinePosition.Left:
          return ((edge.Begin.Lng < point.Lng) && (point.Lng <= edge.End.Lng)) ?
            EdgeType.Crossing : EdgeType.Inessential;

        case PointLinePosition.Right:
          return ((edge.End.Lng < point.Lng) && (point.Lng <= edge.Begin.Lng)) ?
            EdgeType.Crossing : EdgeType.Inessential;

        case PointLinePosition.Between:
        case PointLinePosition.Begin:
        case PointLinePosition.End:
          return EdgeType.Touching;

        case PointLinePosition.Beyond:
        case PointLinePosition.Behind:
        default:
          return EdgeType.Inessential;
      }
    }

    static PointLinePosition ClassifyPosition(PointLatLng point, EdgeLatLng edge)
    {
      EdgeLatLng v = new EdgeLatLng(point, edge.Begin);

      double sa = edge.Diff.Lat * v.Diff.Lng - v.Diff.Lat * edge.Diff.Lng;

      if (sa > 0.0)
        return PointLinePosition.Left;

      if (sa < 0.0)
        return PointLinePosition.Right;

      // ���� ������� a � b ����� ��������������� �����������, �� ����� ����� 
      // ������ ������������� ������� p1p2
      if ((edge.Diff.Lat * v.Diff.Lat < 0.0) || (edge.Diff.Lng * v.Diff.Lng < 0.0))
        return PointLinePosition.Behind;

      // ���� ������ � ������ ������� b, �� ����� ����������� ������� ������� p1p2.
      if (edge.Length < v.Length)
        return PointLinePosition.Beyond;

      if (edge.Begin == point)
        return PointLinePosition.Begin;

      if (edge.End == point)
        return PointLinePosition.End;

      return PointLinePosition.Between;
    }

    #region --   Nested classes   --
    /// <summary>
    /// ��� ����� ������������ ����
    /// </summary>
    private enum EdgeType
    {
      /// <summary>
      /// ����� �������� �� ����������� � ���� 
      /// </summary>
      Touching,
      /// <summary>
      /// ����� ���������� ���
      /// </summary>
      Crossing,
      /// <summary>
      /// ����� �� ���������� ���
      /// </summary>
      Inessential
    }

    /// <summary>
    /// ������������� ��������� ����� � ������ �����
    /// </summary>
    private enum PointLinePosition
    {
      /// <summary>
      /// �����
      /// </summary>
      Left,
      /// <summary>
      /// ������
      /// </summary>
      Right,
      /// <summary>
      /// �������
      /// </summary>
      Beyond,
      /// <summary>
      /// ������
      /// </summary>
      Behind,
      /// <summary>
      /// �����
      /// </summary>
      Between,
      /// <summary>
      /// ������
      /// </summary>
      Begin,
      /// <summary>
      /// �����
      /// </summary>
      End
    }

    private struct EdgeLatLng
    {
      public readonly PointLatLng Begin;
      public readonly PointLatLng End;
      public readonly PointLatLng Diff;

      public EdgeLatLng(PointLatLng begin, PointLatLng end)
      {
        Begin = begin;
        End = end;
        Diff = End - Begin;
      }

      public double Length
      {
        get { return Diff.Magnitude; }
      }
    }
    #endregion
  }
}
