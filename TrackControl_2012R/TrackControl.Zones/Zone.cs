using System;
using System.Drawing; 
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Zones.Properties;


namespace TrackControl.Zones
{
    public class Zone : Entity, IZone
    {
        private string _name;
        private string _description;
        private ZonesGroup _group;
        private List<PointLatLng> _points;
        private RectLatLng _bounds;
        private double _areaKm;
        private bool _isGeometryChanged;
        private IZoneStyle _style;
        private bool _passagerYes = false;
        private ZoneCategory _category;
        private ZoneCategory2 _category2;
        
        /// <summary>
        /// ���� ��������� ������� ����
        /// </summary>
        private DateTime? _dateChange;

        private object _tag;

        /// <summary>
        /// ����������� ��� TrackControl.Zones.Zone
        /// </summary>
        /// <param name="name">�������� ����</param>
        /// <param name="description">�������� ����</param>
        /// <param name="group">������, � ������� ����������� ����</param>
        /// <param name="style">����� ��� ����������� ����</param>
        public Zone(string name, string description, ZonesGroup group, IZoneStyle style, DateTime? dateChange,
            string IdOutLink, ZoneCategory category, ZoneCategory2 category2)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("name can not be NULL or Empty");
            _name = name;

            if (null == description)
                throw new ArgumentNullException("description");
            _description = description;

            if (null == group)
                throw new ArgumentNullException("group");
            Group = group;

            if (null == style)
                throw new ArgumentNullException("style");
            _style = style;

            _points = new List<PointLatLng>();

            _dateChange = dateChange;
            _IdOutLink = IdOutLink;
            _category = category;
            _category2 = category2;
        }

        public object Category
        {
            get { return _category; }
            set { _category = (ZoneCategory) value; }
        }
        
        public object Category2
        {
            get { return _category2; }
            set { _category2 = (ZoneCategory2) value; }
        }

        public bool PassagerCalc
        {
            get
            {
                return _passagerYes;
            }

            set
            {
                _passagerYes = value;
            }
        }

        public bool PassengerCalc
        {
            get
            {
                return _passagerYes;
            }

            set
            {
                _passagerYes = value;
            }
        }

        public static Zone CreateNew(ZonesGroup group)
        {
            return new Zone(Resources.NewZone, "", group, new ZonesStyle(), DateTime.Now, "", null, null);
        }

        public string Name
        {
            get { return _name; }
            set { _name = String.IsNullOrEmpty(value) ? Resources.NoName : value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value ?? String.Empty; }
        }

        public IZoneStyle Style
        {
            get { return _style; }
            set { _style = value; }
        }

        public PointLatLng[] Points
        {
            get { return _points.ToArray(); }
            set
            {
                if (null == value)
                    throw new ZonePointsNullException();

                if (value.Length < 3)
                    throw new NotEnoughPointsException();

                _points = new List<PointLatLng>(value);
                _bounds = RectLatLng.Calculate(_points);
            }
        }

        public RectLatLng Bounds
        {
            get { return _bounds; }
        }

        public double AreaKm
        {
            get { return _areaKm; }
            set { _areaKm = value; }
        }

        public double AreaGa
        {
            get { return _areaKm * 100.0; }
            set { _areaKm = value / 100.0; }
        }

        public DateTime? DateChange
        {
            get { return _dateChange; }
            set { _dateChange = value; }
        }

        public ZonesGroup Group
        {
            get { return _group; }
            set
            {
                if (null == value)
                    throw new Exception("Zones Group can not be NULL");

                if (_group != value)
                {
                    if (null != _group)
                        _group.RemoveZone(this);

                    _group = value;
                    _group.AddZone(this);
                }
            }
        }

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public bool IsGeometryChanged
        {
            get { return _isGeometryChanged; }
            set
            {
                _isGeometryChanged = value;
                if (_isGeometryChanged)
                {
                    _areaKm = �GeoDistance.CalculateArea(_points);
                }
            }
        }

        public bool Contains(PointLatLng point)
        {
            return ZonesHelper.DoesZoneContainPoint(this, point);
        }

        public bool BoundContains(PointLatLng point)
        {
            return this.Bounds.Contains(point);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

        //public void RecalcArea()
        //{
        //    _areaKm = �GeoDistance.CalculateArea(_points);
        //}

        public string GetCultureActive()
        {
            return ZonesAgroProvider.GetCultureNameActive(Id);
        }
    }
}