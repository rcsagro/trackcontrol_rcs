﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;

namespace TrackControl.Zones
{
    public class ZoneCategory : Entity
    {
        public ZoneCategory()
        {
            // to do this
        }

        public string Name { get; set; }
        
        public ZoneCategory(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public ZoneCategory(int id)
        {
            ZoneCategory zc = ZonesCategoryProvider.GetCategory(id);

            if (zc != null)
            {
                Id = zc.Id;
                Name = zc.Name;
            }
        }

        public bool Save()
        {
            if( IsNew )
            {
                Id = ZonesCategoryProvider.Insert( this );
                return ( Id != ConstsGen.RECORD_MISSING );
            }
            else
            {
                return ZonesCategoryProvider.Update( this );
            }
        }

        public bool Delete()
        {
            return ZonesCategoryProvider.Delete( this );
        }

        public override string ToString()
        {
            return String.Format( "{0}", Name );
        }
    }
}
