using System;
using System.Drawing;
using TrackControl.General;

namespace TrackControl.Zones
{
  /// <summary>
  /// ����� ��� ��������� ����������� ���� �� �����
  /// </summary>
  public class ZonesStyle : IZoneStyle
  {
    static readonly Color DEFAULT_COLOR = Color.Red;

    public ZonesStyle()
    {

    }

    public ZonesStyle(Color color)
    {
        _color = color;
    }

    public Image Icon
    {
      get { return Shared.Zone; }
      set { }
    }

    /// <summary>
    /// Color for zone rendering
    /// </summary>
    public Color Color
    {
      get { return _color; }
      set
      {
        if (0 == value.R && 0 == value.G && 0 == value.B && 0 == value.A)
          _color = DEFAULT_COLOR;
        else
          _color = value;
      }
    }
    Color _color = DEFAULT_COLOR;
  }
}