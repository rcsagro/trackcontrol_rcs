using BaseReports;
using DevExpress.XtraEditors;
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.Vehicles.Tuning.Properties;
using System.Collections.Generic;
using TrackControl.Vehicles.Tuning.Vehicles;

namespace TrackControl.Vehicles.Tuning
{
    public class VehiclesTuningMode : ITuning
    {
        private bool _released;

        private VehiclesModel _model;
        private TuningVehiclesModel _tuningModel;
        private IVehicleProvider _vehicleProvider;
        private IVehiclesGroupProvider _groupProvider;

        private VehiclesView _view;
        private VehiclesTree _tree;
        private EditGroup _editGroup;
        private EditVehicle _editVehicle;

        private VehiclesGroup _newGroup;


        public VehiclesTuningMode(VehiclesModel model, IVehicleProvider vehicleProvider,
                                  IVehiclesGroupProvider groupProvider)
        {
            _released = true;
            if (null == model)
                throw new ArgumentNullException("model");
            if (null == vehicleProvider)
                throw new ArgumentNullException("vehicleProvider");
            if (null == groupProvider)
                throw new ArgumentNullException("groupProvider");

            _model = model;
            _model.DriverDeleted += model_DriverDeleted;
            _vehicleProvider = vehicleProvider;
            _groupProvider = groupProvider;

            init();
        }

        #region ITuning Members

        public string Caption
        {
            get { return Resources.Transport; }
        }

        public Image Icon
        {
            get { return Shared.CarPencil; }
        }

        public Control View
        {
            get
            {
                if (null == _view)
                {
                    throw new Exception("View can't be NULL. Probably, the method Advise() has not been called.");
                }
                return _view;
            }
        }

        public void Advise()
        {
            _tuningModel.Activate();

            _view = new VehiclesView();
            _view.NewGroupClicked += view_NewGroupClicked;
            _view.DeleteGroupClicked += view_DeleteGroupClicked;
            _view.SaveAllClicked += view_SaveAllClicked;
            _view.CancelAllClicked += view_CancelAllClicked;
            _view.CategoriesClicked += view_CategoriesClicked;
            _view.CategoriesClicked2 += view_CategoriesClicked2;
            _view.CategoriesClicked3 += view_CategoriesClicked3;
            _view.CategoriesClicked4 += view_CategoriesClicked4;

            _view.ShowTree(_tree);

            Entity entity = _tree.Current;
            showEditor(entity);
            updateDeleteGroupButtonVisibility(entity);
            updateSaveCancelButtonsVisibility();

            _released = false;
        }

        public void Release()
        {
            if (_released) return;

            _tree.Parent = null;
            _editVehicle.Parent = null;
            _editGroup.Parent = null;

            _view.NewGroupClicked -= view_NewGroupClicked;
            _view.DeleteGroupClicked -= view_DeleteGroupClicked;
            _view.SaveAllClicked -= view_SaveAllClicked;
            _view.CancelAllClicked -= view_CancelAllClicked;
            _view.CategoriesClicked -= view_CategoriesClicked;
            _view.CategoriesClicked2 -= view_CategoriesClicked2;
            _view.CategoriesClicked3 -= view_CategoriesClicked3;
            _view.CategoriesClicked4 -= view_CategoriesClicked4;

            _view.Parent = null;
            _view.Dispose();
            _view = null;

            _released = true;
        }

        #endregion

        private void init()
        {
            _tuningModel = new TuningVehiclesModel(_model);

            _tree = new VehiclesTree(_model);
            _tree.SelectionChanged += tree_SelectionChanged;
            _tree.RemoveGroupClicked += tree_RemoveGroupClicked;

            _editGroup = new EditGroup();
            _editGroup.Dock = DockStyle.Fill;
            _editGroup.StatusChanged += editor_StatusChanged;
            _editGroup.SaveButtonClicked += editGroup_SaveButtonClicked;
            _editGroup.CancelButtonClicked += editGroup_CancelButtonClicked;

            _editVehicle = new EditVehicle(_model);
            _editVehicle.Dock = DockStyle.Fill;
            _editVehicle.StatusChanged += editor_StatusChanged;
            _editVehicle.SaveButtonClicked += editVehicle_SaveButtonClicked;
            _editVehicle.CancelButtonClicked += editVehicle_CancelButtonClicked;
        }

        private void model_DriverDeleted(Driver driver)
        {
            foreach (Vehicle vehicle in _model.Vehicles)
            {
                if (driver == vehicle.Driver)
                {
                    vehicle.Driver = null;
                    _vehicleProvider.Save(vehicle);
                    TuningVehicle tuning = vehicle.Tag as TuningVehicle;
                    if (null != tuning)
                        tuning.Driver = Driver.Empty;
                }
            }
        }

        private void view_DeleteGroupClicked()
        {
            VehiclesGroup group = (VehiclesGroup) _tree.Current;
            deleteGroup(group);
            updateView();
        }

        private void view_SaveAllClicked()
        {
            _tuningModel.Groups.ForEach(delegate(TuningGroup group)
                {
                    saveGroup(group);
                });
            _tuningModel.Vehicles.ForEach(delegate(TuningVehicle vehicle)
                {
                    saveVehicle(vehicle);
                });
            updateView();
        }

        private void view_CancelAllClicked()
        {
            _tuningModel.Groups.ForEach(delegate(TuningGroup group)
                {
                    group.RefreshData();
                });
            _tuningModel.Vehicles.ForEach(delegate(TuningVehicle vehicle)
                {
                    vehicle.RefreshData();
                });
            updateView();
        }

        private void view_CategoriesClicked()
        {
            CategoriesEditor formEditor = new CategoriesEditor();
            formEditor.RefreshCategoriesList += RefreshCategoriesList;
            formEditor.ShowDialog();
        }

        private void view_CategoriesClicked2()
        {
            CategoriesEditor2 formEditor = new CategoriesEditor2();
            formEditor.RefreshCategoriesList += RefreshCategoriesList2;
            formEditor.ShowDialog();
        }

        private void view_CategoriesClicked3()
        {
            CategoriesEditor3 formEditor = new CategoriesEditor3();
            formEditor.RefreshCategoriesList += RefreshCategoriesList3;
            formEditor.ShowDialog();
        }

        private void view_CategoriesClicked4()
        {
            CategoriesEditor4 formEditor = new CategoriesEditor4();
            formEditor.RefreshCategoriesList += RefreshCategoriesList4;
            formEditor.ShowDialog();
        }
        
        private void RefreshCategoriesList()
        {
            _editVehicle.RefreshCategoryList();
        }

        private void RefreshCategoriesList2()
        {
            _editVehicle.RefreshCategory2List();
        }

        private void RefreshCategoriesList3()
        {
            _editVehicle.RefreshCategory3List();
        }

        private void RefreshCategoriesList4()
        {
            _editVehicle.RefreshCategory4List();
        }
        
        private void editor_StatusChanged()
        {
            //updateView();
        }

        private void updateView()
        {
            _tree.RefreshTree();
            updateSaveCancelButtonsVisibility();
        }

        private void editGroup_SaveButtonClicked(TuningGroup tuning)
        {
            saveGroup(tuning);
            updateView();
            _tree.SelectEntity(tuning.Group);
            showEditor(_tree.Current);

            atlantaDataSet _dataset = ReportTabControl.Dataset;
            DataSetManager.FillTeamTable( _dataset );
            DataSetManager.FillVehicleTable( _dataset );
            _tree.RefreshTree();
        }

        private void editGroup_CancelButtonClicked(TuningGroup tuning)
        {
            tuning.RefreshData();
            updateView();
        }

        private void editVehicle_SaveButtonClicked(TuningVehicle tuning)
        {
            saveVehicle(tuning);

            //updateView();
            _tree.SelectVehicle(tuning.Vehicle);
            showEditor(_tree.Current);

            atlantaDataSet _dataset = ReportTabControl.Dataset;
            DataSetManager.FillVehicleTable(_dataset);
        }

        private void editVehicle_CancelButtonClicked(TuningVehicle tuning)
        {
            tuning.CanselData();
            updateView();
        }

        private void tree_SelectionChanged(Entity entity)
        {
            updateDeleteGroupButtonVisibility(entity);
            showEditor(entity);
        }

        private void tree_RemoveGroupClicked(VehiclesGroup group)
        {
            deleteGroup(group);
            _tree.RefreshTree();
        }

        private void showEditor(Entity entity)
        {
            if (entity is VehiclesGroup)
                showEditor((VehiclesGroup) entity);
            else if (entity is Vehicle)
                showEditor((Vehicle) entity);
        }

        private void showEditor(VehiclesGroup group)
        {
            //group.GetDataTeam(group.Id);
            _editGroup.BindTo(group);
            _view.ShowEditView(_editGroup);
        }

        private void showEditor(Vehicle vehicle)
        {
            _editVehicle.BindTo(vehicle);
            _view.ShowEditView(_editVehicle);
        }

        private void view_NewGroupClicked()
        {
            if (null == _newGroup)
            {
                _newGroup = new VehiclesGroup("����� ������", "");
                TuningGroup tg = new TuningGroup(_newGroup);
                tg.Bind();
            }
            showEditor(_newGroup);
        }

        private void updateSaveCancelButtonsVisibility()
        {
            if (_tuningModel.IsChanged)
                _view.ShowSaveCancelButtons();
            else
                _view.HideSaveCancelButtons();
        }

        private void updateDeleteGroupButtonVisibility(Entity entity)
        {
            VehiclesGroup group = entity as VehiclesGroup;
            if (null != group && !group.IsRoot && group.Id > 1)
                _view.EnableDeleteGroupButton();
            else
                _view.DisableDeleteGroupButton();
        }

        private void saveVehicle(TuningVehicle tuning)
        {
            Vehicle vehicle = tuning.Vehicle;

            vehicle.RegNumber = tuning.RegNumber;
            vehicle.CarMaker = tuning.CarMaker;
            vehicle.CarModel = tuning.CarModel;
            vehicle.FuelWays = tuning.FuelWayLiter;
            vehicle.FuelMotor = tuning.FuelMotorLiter;
            vehicle.Identifier = tuning.Identifier;
            vehicle.Group = tuning.Group;
            vehicle.Driver = tuning.Driver.IsEmpty ? null : tuning.Driver;
            vehicle.PcbVersion = tuning.PcbVersus.IsEmpty ? null : tuning.PcbVersus;
            vehicle.Style.ColorTrack = tuning.ColorTrack;

            if( tuning.IdOutLink == null )
                tuning.IdOutLink = "";

            vehicle.IdOutLink = tuning.IdOutLink;

            vehicle.Category = tuning.Category;
            vehicle.Category2 = tuning.Category2;
            vehicle.Category3 = tuning.Category3;
            vehicle.Category4 = tuning.Category4;
            vehicle.VehicleComment = tuning.VehicleComment; 

            vehicle.Mobitel.Is64BitPackets = tuning.MobitelIs64Packets;
            vehicle.Mobitel.IsNotDrawDgps = tuning.MobitelIsNotDrawDgps;
			
            _vehicleProvider.Save(vehicle);
            tuning.RefreshData();
        }

        private void saveGroup(TuningGroup tuning)
        {
            if (tuning.IsRoot)
                return;

            VehiclesGroup group = tuning.Group;
            group.Name = tuning.NewName;
            group.Description = tuning.NewDescription;
            group.IdOutLink = tuning.IdOutLink;
            
            if (group.IsNew)
            {
                _model.Root.AddGroup(group);
                _tuningModel.AddGroup(tuning);
                _newGroup = null;
            }

            group.FuelWayKmGrp = tuning.FuelWayKmGrp;
            group.FuelMotoHrGrp = tuning.FuelMotoHrGrp;

            List<Vehicle> vhList = group.GetVehicleGroup();

            for (int i = 0; i < vhList.Count; i++)
            {
                Vehicle vhl = vhList[i];

                if(vhl.FuelMotor == "0")
                    vhl.FuelMotor = tuning.FuelMotoHrGrp;

                if(vhl.FuelWays == "0")
                    vhl.FuelWays = tuning.FuelWayKmGrp;
            }
           
            _groupProvider.Save(group);
            tuning.RefreshData();
        }

        private void deleteGroup(VehiclesGroup group)
        {
            if (group.IsRoot)
                throw new Exception("������ ������� �������� ���� ������");
            if (1 == group.Id)
                throw new Exception("������ ������� ������ � Id=1, �.�. ������ ���� 1.38.00 �� ����� ��������");

            StringBuilder message = new StringBuilder();
            message.AppendFormat("������ \"{0}\" ����� ������� ������������.{1}", group.Name, Environment.NewLine);
            if (group.AllItems.Count > 0)
                message.AppendFormat("{0} ������������ ������� ����� ����������.{1}", group.AllItems.Count,
                                     Environment.NewLine);
            message.Append("����������?");

            if (DialogResult.OK ==
                XtraMessageBox.Show(message.ToString(), "�������� ������", MessageBoxButtons.OKCancel))
            {
                foreach (Vehicle vehicle in group.AllItems)
                {
                    vehicle.Group = _model.Root;
                    _vehicleProvider.Save(vehicle);
                    TuningVehicle tuningVehicle = (TuningVehicle) vehicle.Tag;
                    tuningVehicle.GroupChanged();
                }

                TuningGroup tuning = (TuningGroup) group.Tag;
                _tuningModel.RemoveGroup(tuning);
                _model.Root.RemoveGroup(group);
                _groupProvider.Delete(group);
            }
        }
    }
}
