using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning
{
    public class DriversTuningMode : ITuning
    {
        private bool _released;

        private VehiclesModel _model;
        private IDriverProvider _driverProvider;
        private List<TuningDriver> _tuningDrivers;

        private TuningStatus _status;
        private Driver _newDriver;

        private DriversView _view;


        public DriversTuningMode(VehiclesModel model, IDriverProvider driverProvider)
        {
            _released = true;
            if (null == model)
                throw new ArgumentNullException("model");
            if (null == driverProvider)
                throw new ArgumentNullException("driverProvider");

            _model = model;
            _status = TuningStatus.Troubles;
            _driverProvider = driverProvider;
            init();
        }

        #region ITuning Members

        public string Caption
        {
            get { return Resources.Drivers; }
        }

        public Image Icon
        {
            get { return Shared.DriverPencil; }
        }

        public Control View
        {
            get
            {
                if (null == _view)
                {
                    throw new Exception("View can't be NULL. Probably, the method Advise() has not been called.");
                }
                return _view;
            }
        }

        public void Advise()
        {
            _tuningDrivers.ForEach(delegate(TuningDriver td)
                {
                    td.Bind();
                });
            _view = new DriversView();
            _view.AddNewClicked += view_AddNewClicked;
            _view.DriverDeleted += deleteDriver;
            _view.SaveAllClicked += view_SaveAllClicked;
            _view.CancelAllClicked += view_CancelAllClicked;
            _view.BindTo(_tuningDrivers);
            updateView();

            _released = false;
        }

        public void Release()
        {
            if (_released) return;

            _tuningDrivers.ForEach(delegate(TuningDriver td)
                {
                    td.Release();
                });

            _view.AddNewClicked -= view_AddNewClicked;
            _view.DriverDeleted -= deleteDriver;
            _view.SaveAllClicked -= view_SaveAllClicked;
            _view.CancelAllClicked -= view_CancelAllClicked;
            _view.Parent = null;
            _view.Dispose();
            _view = null;

            _released = true;
        }

        #endregion

        private void init()
        {
            _tuningDrivers = new List<TuningDriver>(_model.Drivers.Count);
            foreach (Driver driver in _model.Drivers)
            {
                TuningDriver tuning = new TuningDriver(driver);
                tuning.StatusChanged += updateView;
                _tuningDrivers.Add(tuning);
            }
        }

        private void view_AddNewClicked()
        {
            _newDriver = Driver.NewDriver;
            TuningDriver tuning = new TuningDriver(_newDriver);
            tuning.Bind();
            tuning.StatusChanged += updateView;
            _tuningDrivers.Add(tuning);
            _view.RefreshGrid();
            updateView();
        }

        private void view_SaveAllClicked()
        {
            if (_newDriver != null)
            {
                saveDriver(_newDriver);
                _model.AddDriver(_newDriver);
                _newDriver = null;
            }
            foreach (Driver driver in _model.Drivers)
            {
                saveDriver(driver);
            }
            _view.RefreshGrid();
            updateView();
        }

        private void view_CancelAllClicked()
        {
            if (null != _newDriver)
            {
                TuningDriver tuning = (TuningDriver) _newDriver.Tag;
                tuning.StatusChanged -= updateView;
                _tuningDrivers.Remove(tuning);
                _newDriver = null;
            }

            _tuningDrivers.ForEach(delegate(TuningDriver td)
                {
                    td.RefreshData();
                });

            _view.RefreshGrid();
            updateView();
        }

        private void updateView()
        {
            TuningStatus status = TuningStatus.Ok;
            foreach (TuningDriver td in _tuningDrivers)
            {
                if (td.Status == (int) TuningStatus.NotSaved)
                {
                    status = TuningStatus.NotSaved;
                    break;
                }
                else if (td.Status == (int) TuningStatus.Troubles)
                {
                    status = TuningStatus.Troubles;
                    break;
                }
                else if (td.Status == (int) TuningStatus.NewAdded)
                {
                    status = TuningStatus.NewAdded;
                    break;
                }
            }

            if (status != _status)
                _status = status;

            if (TuningStatus.Ok == _status)
                _view.HideSaveCancelButtons();
            else
                _view.ShowSaveCancelButtons();

            if (null == _newDriver)
                _view.EnableAddNewButton();
            else
                _view.DisableAddNewButton();

            if (_tuningDrivers.Count > 0)
                _view.EnableDeleteButton();
            else
                _view.DisableDeleteButton();
        }

        private void saveDriver(Driver driver)
        {
            TuningDriver td = (TuningDriver) driver.Tag;
            if (td.Status != (int) TuningStatus.Ok)
            {
                driver.FirstName = td.FirstName;
                driver.LastName = td.LastName;
                driver.DayOfBirth = td.DayOfBirth;
                driver.Categories = td.Categories;
                driver.License = td.License;
                driver.Identifier = td.Identifier;
                driver.Photo = td.Photo;
                driver.IdOutLink = td.IdOutLink;
                driver.TypeDriverId = td.TypeDriverId;
                driver.NumTelephone = td.NumTelephone;
                driver.Department = td.Department;
                _driverProvider.Save(driver);
                td.RefreshData();
            }
        }

        private void deleteDriver(Driver driver)
        {
            if (!_driverProvider.IsPermitToDelete(driver)) return;
            TuningDriver tuning = (TuningDriver) driver.Tag;
            tuning.StatusChanged -= updateView;
            _tuningDrivers.Remove(tuning);

            if (driver.IsNew)
                _newDriver = null;
            else
            {
                _driverProvider.Delete(driver);
                _model.DeleteDriver(driver);
            }

            _view.RefreshGrid();
            updateView();
        }
    }
}
