using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Vehicles.Tuning
{
    public class TuningVehiclesModel
    {
        private VehiclesModel _model;
        private List<TuningVehicle> _vehicles;
        private List<TuningGroup> _groups;

        public event ModelChanged ModelChanged;

        public TuningVehiclesModel(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model");

            _model = model;
            init();
        }

        public List<TuningGroup> Groups
        {
            get { return _groups; }
        }

        public List<TuningVehicle> Vehicles
        {
            get { return _vehicles; }
        }

        public bool IsChanged
        {
            get
            {
                foreach (TuningGroup group in _groups)
                {
                    if (TuningStatus.Ok != group.Status)
                        return true;
                }
                foreach (TuningVehicle vehicle in _vehicles)
                {
                    if (TuningStatus.Ok != vehicle.Status)
                        return true;
                }
                return false;
            }
        }

        public void Activate()
        {
            _vehicles.ForEach(delegate(TuningVehicle tv)
                {
                    tv.Bind();
                });
            _groups.ForEach(delegate(TuningGroup tg)
                {
                    tg.Bind();
                });
        }

        public void AddGroup(TuningGroup group)
        {
            _groups.Add(group);
            onModelChanged();
        }

        public void RemoveGroup(TuningGroup group)
        {
            _groups.Remove(group);
            onModelChanged();
        }

        private void init()
        {
            
            IList<Vehicle> vehicles = _model.Vehicles;
            _vehicles = new List<TuningVehicle>(vehicles.Count);
            foreach (Vehicle vehicle in vehicles)
            {
                _vehicles.Add(new TuningVehicle(vehicle));
            }

            IList<Group<Vehicle>> groups = _model.Root.AllGroups;
            _groups = new List<TuningGroup>(groups.Count + 1);
            _groups.Add(new TuningGroup(_model.Root));

            foreach (VehiclesGroup group in groups)
            {
                TuningGroup tuningGroup = new TuningGroup(group);
                _groups.Add(tuningGroup);
            }
        }

        private void onModelChanged()
        {
            ModelChanged handler = ModelChanged;
            if (null != handler)
                handler();
        }
    }
}
