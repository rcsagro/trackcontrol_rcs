using System;
using System.Drawing;
using TrackControl.General;

namespace TrackControl.Vehicles.Tuning
{
    public class TuningDriver
    {
        Driver _driver;

        public event StatusChanged StatusChanged;

        public TuningDriver(Driver driver)
        {
            _driver = driver;
            RefreshData();
        }

        public Driver Driver
        {
            get { return _driver; }
        }

        public int? Id
        {
            get
            {
                if (_driver.Id > 0)
                    return _driver.Id;
                else
                    return null;
            }
        }

        public int Status
        {
            get { return (int) _status; }
        }

        TuningStatus _status;

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value != _firstName)
                {
                    _firstName = value;
                    updateStatus();
                }

            }
        }

        string _firstName;

        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value != _lastName)
                {
                    _lastName = value;
                    updateStatus();
                }
            }
        }

        string _lastName;

        public Image Photo
        {
            get { return _photo; }
            set
            {
                if (value != _photo)
                {
                    _photo = value;
                    updateStatus();
                }
            }
        }

        Image _photo;
        
        public string NumTelephone
        {
            get { return _numtelephone; }
            set
            {
                if (value != _numtelephone)
                {
                    _numtelephone = value;
                    updateStatus();
                }
            }
        }

        string _numtelephone = "";

        public DateTime? DayOfBirth
        {
            get { return _dayOfBirth; }
            set
            {
                if (value != _dayOfBirth)
                {
                    _dayOfBirth = value;
                    updateStatus();
                }
            }
        }

        DateTime? _dayOfBirth;

        public string Categories
        {
            get { return _categories; }
            set
            {
                if (value != _categories)
                {
                    _categories = value;
                    updateStatus();
                }
            }
        }

        string _categories;

        public string License
        {
            get { return _license; }
            set
            {
                if (value != _license)
                {
                    _license = value;
                    updateStatus();
                }
            }
        }

        string _license;

        public UInt16? Identifier
        {
            get { return _identifier; }
            set
            {
                if (value != _identifier)
                {
                    _identifier = value;
                    updateStatus();
                }
            }
        }

        UInt16? _identifier;

        int _typeDriverId;

        public int TypeDriverId
        {
            get { return _typeDriverId; }
            set { _typeDriverId = value; }
        }

        public string Department { get; set; }

        public void RefreshData()
        {
            _status = _driver.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
            _firstName = _driver.FirstName;
            _lastName = _driver.LastName;
            _photo = _driver.Photo;
            _dayOfBirth = _driver.DayOfBirth;
            _categories = _driver.Categories;
            _license = _driver.License;
            _identifier = _driver.Identifier;
            _idOutLink = _driver.IdOutLink;
            _typeDriverId = _driver.TypeDriverId;
			_numtelephone = _driver.NumTelephone;
            Department = _driver.Department;
            updateStatus();
        }

        public void Bind()
        {
            _driver.Tag = this;
        }

        public void Release()
        {
            if (_driver.Tag == this)
                _driver.Tag = null;
        }

        void updateStatus()
        {
            if (_driver.IsNew)
                return;

            if (TuningStatus.Ok != _status && _firstName == _driver.FirstName &&
                _lastName == _driver.LastName && _photo == _driver.Photo &&
                _dayOfBirth == _driver.DayOfBirth && _categories == _driver.Categories &&
                _license == _driver.License && _identifier == _driver.Identifier && _numtelephone == _driver.NumTelephone
                && _idOutLink == _driver.IdOutLink && Department == _driver.Department
                && _typeDriverId == _driver.TypeDriverId)
            {
                _status = TuningStatus.Ok;
                onStatusChanged();
            }
            else if (TuningStatus.Ok == _status && (_firstName != _driver.FirstName ||
                                                    _lastName != _driver.LastName || _photo != _driver.Photo ||
                                                    _dayOfBirth != _driver.DayOfBirth ||
                                                    _categories != _driver.Categories ||
                                                    _license != _driver.License || _identifier != _driver.Identifier
                                                    || _idOutLink != _driver.IdOutLink || _numtelephone != _driver.NumTelephone
                                                    || Department != _driver.Department
                                                    || _typeDriverId == _driver.TypeDriverId))
            {
                _status = TuningStatus.NotSaved;
                onStatusChanged();
            }
        }

        void onStatusChanged()
        {
            StatusChanged handler = StatusChanged;
            if (null != handler)
                handler();
        }

        string _idOutLink;

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _idOutLink; }
            set
            {
                if (value != _idOutLink)
                {
                    _idOutLink = value;
                    updateStatus();
                }
            }
        }
    }
}
