using System;
using System.Drawing;
using DevExpress.XtraRichEdit.API.Word;
using TrackControl.General;

namespace TrackControl.Vehicles.Tuning
{
    public class TuningVehicle : Entity
    {
        private string _regNumber;
        private string _carMaker;
        private string _carModel;
        private VehiclesGroup _group;
        private Driver _driver;
        private PcbVersions _pcbVersions = null;
        private object _tag;
        private Vehicle _vehicle;
        private Color _colorTrack;
        private string fuelWay;
        private string fuelMotor;
        private int vehicleIdentif;
        private string previouseComment = "";

        public event StatusChanged StatusChanged;

        public TuningVehicle(Vehicle vehicle)
        {
            if (null == vehicle)
                throw new ArgumentNullException("vehicle");

            _vehicle = vehicle;
            Id = _vehicle.Id;
            RefreshData();
        }

        public string PreviouseComment
        {
            get { return previouseComment; }
            set 
            {
                if (previouseComment != value)
                {
                    previouseComment = value;
                }
            }
        }

        public Mobitel Mobitel
        {
            get { return _vehicle.Mobitel; }
        }

        public string CarMaker
        {
            get { return _carMaker; }
            set
            {
                if (value != _carMaker)
                {
                    _carMaker = value;
                    UpdateStatus();
                }
            }
        }

        public string CarModel
        {
            get { return _carModel; }
            set
            {
                if (value != _carModel)
                {
                    _carModel = value;
                    UpdateStatus();
                }
            }
        }

        public string RegNumber
        {
            get { return _regNumber; }
            set
            {
                if (value != _regNumber)
                {
                    _regNumber = value;
                    UpdateStatus();
                }
            }
        }

        public Driver Driver
        {
            get { return _driver; }
            set
            {
                if (value != _driver)
                {
                    _driver = value;
                    UpdateStatus();
                }
            }
        }

        public PcbVersions PcbVersus
        {
            get { return _pcbVersions; }
            set
            {
                if (value != _pcbVersions)
                {
                    _pcbVersions = value;
                    UpdateStatus();
                }
            }
        }

        public string FuelWayLiter
        {
            get { return this.fuelWay; }
            set
            {
                if (value != this.fuelWay)
                {
                    this.fuelWay = value;
                    UpdateStatus();
                }
            }
        }

        public string FuelMotorLiter
        {
            get { return this.fuelMotor; }
            set
            {
                if (value != this.fuelMotor)
                {
                    this.fuelMotor = value;
                    UpdateStatus();
                }
            }
        }

        public int Identifier
        {
            get { return this.vehicleIdentif; }
            set
            {
                if (value != this.vehicleIdentif)
                {
                    this.vehicleIdentif = value;
                    UpdateStatus();
                }
            }
        }

        public VehicleStyle Style
        {
            get { return _vehicle.Style; }
        }


        public VehiclesGroup Group
        {
            get { return _group; }
            set
            {
                if (value != _group)
                {
                    _group = value;
                    UpdateStatus();
                }
            }
        }

        public Color ColorTrack
        {
            get
            {
                return _colorTrack;
                //Console.WriteLine(_colorTrack.ToArgb());   
            }
            set
            {
                _colorTrack = value;
                UpdateStatus();
            }
        }

        public object Tag
        {
            get
            {
                return _tag;
                ;
            }
            set { _tag = value; }
        }

        public string MobitelInfo
        {
            get { return String.Format("{0}; SIM: {1}", _vehicle.Mobitel.Login, _vehicle.Mobitel.SimNumber); }
        }

        public int MobitelId
        {
            get { return _vehicle.Mobitel.Id; }
        }

        public string MobitelLogin
        {
            get { return _vehicle.Mobitel.Login; }
        }

        public string MobitelSim
        {
            get { return _vehicle.Mobitel.SimNumber; }
			set 
            {
                if (_vehicle.Mobitel.SimNumber != value)
                {
                    _vehicle.Mobitel.SimNumber = value;
                    UpdateStatus();
                }
            }
        }

        protected  bool Is64BitPacketsPreviose
        {
            get { return _is64BitPacketsPreviose; }
            set { _is64BitPacketsPreviose = value; }
        }

        public bool MobitelIs64Packets
        {
            get
            {
                Is64BitPacketsPreviose = _vehicle.Mobitel.Is64BitPackets;
                return _vehicle.Mobitel.Is64BitPackets;
            }
            set
            {
                if (value != _vehicle.Mobitel.Is64BitPackets)
                {
                    //Is64BitPacketsPreviose = _vehicle.Mobitel.Is64BitPackets;
                    _vehicle.Mobitel.Is64BitPackets = value;
                    UpdateStatus();
                }
            }
        }

        public bool MobitelIsNotDrawDgps
        { 
            get 
            {
                return _vehicle.Mobitel.IsNotDrawDgps;
            }

            set 
            {
                if (_vehicle.Mobitel.IsNotDrawDgps != value)
                {
                    _vehicle.Mobitel.IsNotDrawDgps = value;
                    UpdateStatus();
                }
            }
        }

    public int VehicleId
        {
            get { return _vehicle.Id; }
        }

        public VehicleCategory Category { get; set; }

        public VehicleCategory2 Category2 { get; set; }

        public VehicleCategory3 Category3 { get; set; }

        public VehicleCategory4 Category4 { get; set; }

        public string VehicleComment 
        {
            get;
            set; 
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
        }

        public TuningStatus Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                }
            }
        }

        public TuningStatus CommnetStatus
        {
            get { return commentStatus; }
            set
            {
                if (commentStatus != value)
                {
                    commentStatus = value;
                }
            }
        }
        
        private TuningStatus _status;
        private string vehicleComm = "";
        private TuningStatus commentStatus = TuningStatus.Ok;
        private bool _is64BitPacketsPreviose;

        public void Bind()
        {
            _vehicle.Tag = this;
        }

        public void RefreshData()
        {
            _status = _vehicle.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
            _regNumber = _vehicle.RegNumber;
            _carMaker = _vehicle.CarMaker;
            vehicleIdentif = _vehicle.Identifier;
            _carModel = _vehicle.CarModel;
            _group = _vehicle.Group;
            _driver = _vehicle.Driver ?? Driver.Empty;
            _pcbVersions = _vehicle.PcbVersion ?? PcbVersions.Empty;
            _colorTrack = _vehicle.Style.ColorTrack;
            _IdOutLink = _vehicle.IdOutLink;
            this.fuelWay = _vehicle.FuelWays;
            this.fuelMotor = _vehicle.FuelMotor;

            Category = _vehicle.Category;
            Category2 = _vehicle.Category2;
            Category3 = _vehicle.Category3;
            Category4 = _vehicle.Category4;
            VehicleComment = _vehicle.VehicleComment;

            MobitelIs64Packets = _vehicle.Mobitel.Is64BitPackets; 
            MobitelIsNotDrawDgps = _vehicle.Mobitel.IsNotDrawDgps;
        }

        public void CanselData()
        {
            _status = _vehicle.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
            _regNumber = _vehicle.RegNumber;
            _carMaker = _vehicle.CarMaker;
            vehicleIdentif = _vehicle.Identifier;
            _carModel = _vehicle.CarModel;
            _group = _vehicle.Group;
            _driver = _vehicle.Driver ?? Driver.Empty;
            _pcbVersions = _vehicle.PcbVersion ?? PcbVersions.Empty;
            _colorTrack = _vehicle.Style.ColorTrack;
            _IdOutLink = _vehicle.IdOutLink;
            this.fuelWay = _vehicle.FuelWays;
            this.fuelMotor = _vehicle.FuelMotor;

            Category = _vehicle.Category;
            Category2 = _vehicle.Category2;
            Category3 = _vehicle.Category3;
            Category4 = _vehicle.Category4;
            VehicleComment = _vehicle.VehicleComment;

            _vehicle.Mobitel.Is64BitPackets = Is64BitPacketsPreviose;
            MobitelIs64Packets = Is64BitPacketsPreviose;
            MobitelIsNotDrawDgps = _vehicle.Mobitel.IsNotDrawDgps;
        }

        public void GroupChanged()
        {
            _group = _vehicle.Group;
        }

        private void UpdateStatus()
        {
            if (TuningStatus.Ok != _status && _vehicle.RegNumber == _regNumber &&
                _vehicle.CarMaker == _carMaker && _vehicle.CarModel == _carModel &&
                _vehicle.Group == _group && _vehicle.Driver == _driver && _vehicle.PcbVersion == _pcbVersions && 
                _vehicle.IdOutLink == _IdOutLink &&
                _vehicle.FuelWays == this.fuelWay && _vehicle.FuelMotor == this.fuelMotor &&
                _vehicle.Identifier == this.vehicleIdentif && _vehicle.Mobitel.Is64BitPackets == Is64BitPacketsPreviose
                && _vehicle.Mobitel.IsNotDrawDgps == MobitelIsNotDrawDgps && _vehicle.VehicleComment == VehicleComment)
            {
                _status = TuningStatus.Ok;
                Is64BitPacketsPreviose = _vehicle.Mobitel.Is64BitPackets;
                onStatusChanged();
            }
            else if (TuningStatus.Ok == _status && (_vehicle.RegNumber != _regNumber ||
                                                    _vehicle.CarMaker != _carMaker || _vehicle.CarModel != _carModel ||
                                                    _vehicle.Group != _group || _vehicle.Driver != _driver || _vehicle.PcbVersion != _pcbVersions ||
                                                    _vehicle.Style.ColorTrack != _colorTrack
                                                    || _vehicle.IdOutLink != _IdOutLink ||
                                                    _vehicle.FuelWays != this.fuelWay ||
                                                    _vehicle.FuelMotor != this.fuelMotor ||
                                                    _vehicle.Identifier != this.vehicleIdentif ||
                _vehicle.Mobitel.Is64BitPackets != Is64BitPacketsPreviose || 
                _vehicle.Mobitel.IsNotDrawDgps != MobitelIsNotDrawDgps ||
            _vehicle.VehicleComment != VehicleComment))
            {
                Is64BitPacketsPreviose = _vehicle.Mobitel.Is64BitPackets;
                _status = TuningStatus.NotSaved;
                onStatusChanged();
            }
        }

        private void onStatusChanged()
        {
            StatusChanged handler = StatusChanged;
            if (null != handler)
                handler();
        }
    }
}
