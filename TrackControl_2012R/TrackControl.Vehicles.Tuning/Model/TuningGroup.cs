using System;
using TrackControl.General;

namespace TrackControl.Vehicles.Tuning
{
    public class TuningGroup
    {
        private VehiclesGroup _group;

        public event StatusChanged StatusChanged;

        public TuningGroup(VehiclesGroup group)
        {
            if (null == group)
                throw new ArgumentNullException("group");

            _group = group;
            RefreshData();
        }

        public bool IsRoot
        {
            get { return _group.IsRoot; }
        }

        public string NewName
        {
            get { return _newName; }
            set
            {
                if (value != _newName)
                {
                    _newName = value;
                    updateStatus();
                }
            }
        }

        private string _newName;

        public string FuelWayKmGrp
        {
            get { return _fuelWayKmGrp; }
            set
            {
                if (value != _fuelWayKmGrp)
                {
                    _fuelWayKmGrp = value;
                    updateStatus();
                }
            }
        }

        private string _fuelWayKmGrp = "";

        public string FuelMotoHrGrp
        {
            get { return _fuelMotoHrGrp; }
            set
            {
                if (value != _fuelMotoHrGrp)
                {
                    _fuelMotoHrGrp = value;
                    updateStatus();
                }
            }
        }

        private string _fuelMotoHrGrp = "";

        public string NewDescription
        {
            get { return _newDescription; }
            set
            {
                if (value != _newDescription)
                {
                    _newDescription = value;
                    updateStatus();
                }
            }
        }

        private string _newDescription;

        public VehiclesGroup Group
        {
            get { return _group; }
        }

        public int AllSubGroupsCount
        {
            get { return _group.AllGroups.Count; }
        }

        public int AllItemsCount
        {
            get { return _group.AllItems.Count; }
        }

        public TuningStatus Status
        {
            get { return _status; }
        }

        private TuningStatus _status;

        public void Bind()
        {
            _group.Tag = this;
        }

        public void RefreshData()
        {
            _newName = _group.Name;
            _newDescription = _group.Description;
            _IdOutLink = _group.IdOutLink;
            _status = (_group.IsNew && !_group.IsRoot) ? TuningStatus.NotSaved : TuningStatus.Ok;
            _fuelMotoHrGrp = _group.FuelMotoHrGrp;
            _fuelWayKmGrp = _group.FuelWayKmGrp;
            onStatusChanged();
        }

        private void updateStatus()
        {
            if (TuningStatus.Ok != _status && _group.Name == _newName && _group.Description == _newDescription &&
                _group.IdOutLink == _IdOutLink &&
                _group.FuelWayKmGrp == _fuelWayKmGrp && _group.FuelMotoHrGrp == _fuelMotoHrGrp)
            {
                _status = TuningStatus.Ok;
                onStatusChanged();
            }
            else if (TuningStatus.Ok == _status &&
                     (_group.Name != _newName || _group.Description != _newDescription || _group.IdOutLink != _IdOutLink ||
                      _group.FuelWayKmGrp != _fuelWayKmGrp || _group.FuelMotoHrGrp != _fuelMotoHrGrp))
            {
                _status = TuningStatus.NotSaved;
                onStatusChanged();
            }
        }

        private void onStatusChanged()
        {
            StatusChanged handler = StatusChanged;

            if (null != handler && !_group.IsNew)
                handler();
        }

        private string _IdOutLink;

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _IdOutLink; }
            set
            {
                if (value != _IdOutLink)
                {
                    _IdOutLink = value;
                    updateStatus();
                }
            }
        }
    }
}
