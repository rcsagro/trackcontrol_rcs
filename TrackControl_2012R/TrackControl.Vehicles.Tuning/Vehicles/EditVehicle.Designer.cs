namespace TrackControl.Vehicles.Tuning
{
  partial class EditVehicle
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
      if (!_driverGrid.IsDisposed)
        _driverGrid.Dispose();
      if (!_noDriver.IsDisposed)
        _noDriver.Dispose();
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditVehicle));
            this._mainTable = new System.Windows.Forms.TableLayoutPanel();
            this._propertyGrid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this._images = new DevExpress.Utils.ImageCollection();
            this._groupRepo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._driverRepo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._colorEdit = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.rteOutLink = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.teLimit60 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.teLimit120 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.editIdentifier = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cbeCategory = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbeCategory2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.pcbItemComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.rce64bitPackets = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cbeCategory3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbeCategory4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.vehMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.vehMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.vehRichTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this._mobitelCat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._loginRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._simNumberRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._pcbDataRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPacket64 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsNotDrawDgps = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._vehicleCat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._regNumberRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._carMakerRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._carModelRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._groupRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._driverRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._colorRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutLink = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCategory = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCategory2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorFuelWay = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorFuelMotor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorIdentif = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCategory4 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCategory3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.vehComment = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._summaryCat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._sensorsRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._logicSensorsRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._gageSensorsRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._driverGrid = new DevExpress.XtraGrid.GridControl();
            this._driverView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this._driverIdCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverFirstNameCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverLastNameCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverDayOfBirthCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item8 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverPhotoCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._photoRepo = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Item4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverLicenseCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverCategories = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._driverIdentifierCol = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.Item3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.Group3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Group4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this.row = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._mainTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._propertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLimit60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLimit120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editIdentifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbItemComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rce64bitPackets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehRichTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainTable
            // 
            this._mainTable.ColumnCount = 5;
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 232F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 283F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.Controls.Add(this._propertyGrid, 0, 0);
            this._mainTable.Controls.Add(this._driverGrid, 3, 0);
            this._mainTable.Controls.Add(this._cancelBtn, 3, 1);
            this._mainTable.Controls.Add(this._saveBtn, 1, 1);
            this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTable.Location = new System.Drawing.Point(10, 26);
            this._mainTable.Name = "_mainTable";
            this._mainTable.RowCount = 3;
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 351F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.Size = new System.Drawing.Size(706, 785);
            this._mainTable.TabIndex = 1;
            // 
            // _propertyGrid
            // 
            this._mainTable.SetColumnSpan(this._propertyGrid, 2);
            this._propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._propertyGrid.ImageList = this._images;
            this._propertyGrid.Location = new System.Drawing.Point(0, 0);
            this._propertyGrid.Margin = new System.Windows.Forms.Padding(0);
            this._propertyGrid.Name = "_propertyGrid";
            this._propertyGrid.OptionsBehavior.ShowEditorOnMouseUp = true;
            this._propertyGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._groupRepo,
            this._driverRepo,
            this._colorEdit,
            this.rteOutLink,
            this.teLimit60,
            this.teLimit120,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.editIdentifier,
            this.cbeCategory,
            this.cbeCategory2,
            this.pcbItemComboBox,
            this.rce64bitPackets,
            this.cbeCategory3,
            this.cbeCategory4,
            this.repositoryItemTextEdit3,
            this.vehMemoEdit,
            this.repositoryItemTextEdit4,
            this.vehMemoEdit1,
            this.vehRichTextEdit1});
            this._propertyGrid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._mobitelCat,
            this._vehicleCat,
            this._summaryCat});
            this._propertyGrid.ShowButtonMode = DevExpress.XtraVerticalGrid.ShowButtonModeEnum.ShowAlways;
            this._propertyGrid.Size = new System.Drawing.Size(353, 351);
            this._propertyGrid.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Driver");
            this._images.Images.SetKeyName(1, "Mobitel");
            this._images.Images.SetKeyName(2, "Vehicle");
            this._images.Images.SetKeyName(3, "Summary");
            // 
            // _groupRepo
            // 
            this._groupRepo.AutoHeight = false;
            this._groupRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._groupRepo.Name = "_groupRepo";
            this._groupRepo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _driverRepo
            // 
            this._driverRepo.AutoHeight = false;
            this._driverRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._driverRepo.Name = "_driverRepo";
            this._driverRepo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _colorEdit
            // 
            this._colorEdit.AllowFocused = false;
            this._colorEdit.AutoHeight = false;
            this._colorEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._colorEdit.Name = "_colorEdit";
            this._colorEdit.ShowSystemColors = false;
            // 
            // rteOutLink
            // 
            this.rteOutLink.AutoHeight = false;
            this.rteOutLink.MaxLength = 20;
            this.rteOutLink.Name = "rteOutLink";
            this.rteOutLink.EditValueChanged += new System.EventHandler(this.rteOutLink_EditValueChanged);
            // 
            // teLimit60
            // 
            this.teLimit60.AutoHeight = false;
            this.teLimit60.MaxLength = 60;
            this.teLimit60.Name = "teLimit60";
            // 
            // teLimit120
            // 
            this.teLimit120.AutoHeight = false;
            this.teLimit120.MaxLength = 120;
            this.teLimit120.Name = "teLimit120";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // editIdentifier
            // 
            this.editIdentifier.AutoHeight = false;
            this.editIdentifier.Name = "editIdentifier";
            // 
            // cbeCategory
            // 
            this.cbeCategory.AutoHeight = false;
            this.cbeCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeCategory.Name = "cbeCategory";
            this.cbeCategory.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbeCategory.EditValueChanged += new System.EventHandler(this.cbeCategory_EditValueChanged);
            // 
            // cbeCategory2
            // 
            this.cbeCategory2.AutoHeight = false;
            this.cbeCategory2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeCategory2.Name = "cbeCategory2";
            this.cbeCategory2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbeCategory2.EditValueChanged += new System.EventHandler(this.cbeCategory2_EditValueChanged);
            // 
            // pcbItemComboBox
            // 
            this.pcbItemComboBox.AutoHeight = false;
            this.pcbItemComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pcbItemComboBox.Name = "pcbItemComboBox";
            this.pcbItemComboBox.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // rce64bitPackets
            // 
            this.rce64bitPackets.AutoHeight = false;
            this.rce64bitPackets.Caption = "Check";
            this.rce64bitPackets.Name = "rce64bitPackets";
            // 
            // cbeCategory3
            // 
            this.cbeCategory3.AutoHeight = false;
            this.cbeCategory3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeCategory3.Name = "cbeCategory3";
            this.cbeCategory3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbeCategory4
            // 
            this.cbeCategory4.AutoHeight = false;
            this.cbeCategory4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeCategory4.Name = "cbeCategory4";
            this.cbeCategory4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemTextEdit3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.repositoryItemTextEdit3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit3.AppearanceDisabled.Options.UseTextOptions = true;
            this.repositoryItemTextEdit3.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemTextEdit3.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.repositoryItemTextEdit3.AppearanceFocused.Options.UseTextOptions = true;
            this.repositoryItemTextEdit3.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemTextEdit3.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.repositoryItemTextEdit3.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit3.AppearanceReadOnly.Options.UseTextOptions = true;
            this.repositoryItemTextEdit3.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemTextEdit3.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.repositoryItemTextEdit3.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.MaxLength = 255;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.ValidateOnEnterKey = true;
            // 
            // vehMemoEdit
            // 
            this.vehMemoEdit.AcceptsTab = true;
            this.vehMemoEdit.Appearance.Options.UseTextOptions = true;
            this.vehMemoEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.vehMemoEdit.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.vehMemoEdit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vehMemoEdit.AppearanceDisabled.Options.UseTextOptions = true;
            this.vehMemoEdit.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.vehMemoEdit.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.vehMemoEdit.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vehMemoEdit.AppearanceFocused.Options.UseTextOptions = true;
            this.vehMemoEdit.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.vehMemoEdit.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.vehMemoEdit.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vehMemoEdit.AppearanceReadOnly.Options.UseTextOptions = true;
            this.vehMemoEdit.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.vehMemoEdit.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.vehMemoEdit.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vehMemoEdit.MaxLength = 255;
            this.vehMemoEdit.Name = "vehMemoEdit";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // vehMemoEdit1
            // 
            this.vehMemoEdit1.AutoHeight = false;
            this.vehMemoEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vehMemoEdit1.Name = "vehMemoEdit1";
            // 
            // vehRichTextEdit1
            // 
            this.vehRichTextEdit1.Name = "vehRichTextEdit1";
            this.vehRichTextEdit1.ShowCaretInReadOnly = false;
            // 
            // _mobitelCat
            // 
            this._mobitelCat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._loginRow,
            this._simNumberRow,
            this._pcbDataRow,
            this.erPacket64,
            this.erIsNotDrawDgps});
            this._mobitelCat.Height = 15;
            this._mobitelCat.Name = "_mobitelCat";
            this._mobitelCat.Properties.Caption = "��������";
            this._mobitelCat.Properties.ImageIndex = 1;
            // 
            // _loginRow
            // 
            this._loginRow.Name = "_loginRow";
            this._loginRow.OptionsRow.AllowMove = false;
            this._loginRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._loginRow.OptionsRow.AllowSize = false;
            this._loginRow.OptionsRow.DblClickExpanding = false;
            this._loginRow.OptionsRow.ShowInCustomizationForm = false;
            this._loginRow.Properties.Caption = "�����";
            this._loginRow.Properties.FieldName = "MobitelLogin";
            this._loginRow.Properties.ReadOnly = true;
            // 
            // _simNumberRow
            // 
            this._simNumberRow.Name = "_simNumberRow";
            this._simNumberRow.OptionsRow.AllowMove = false;
            this._simNumberRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._simNumberRow.OptionsRow.AllowSize = false;
            this._simNumberRow.OptionsRow.DblClickExpanding = false;
            this._simNumberRow.OptionsRow.ShowInCustomizationForm = false;
            this._simNumberRow.Properties.Caption = "����� SIM";
            this._simNumberRow.Properties.FieldName = "MobitelSim";
            // 
            // _pcbDataRow
            // 
            this._pcbDataRow.Name = "_pcbDataRow";
            this._pcbDataRow.Properties.Caption = "������ ����� ��������";
            this._pcbDataRow.Properties.FieldName = "PcbVersus";
            this._pcbDataRow.Properties.RowEdit = this.pcbItemComboBox;
            this._pcbDataRow.Properties.ToolTip = "������ ����� ��������";
            // 
            // erPacket64
            // 
            this.erPacket64.Name = "erPacket64";
            this.erPacket64.Properties.Caption = "����� 64 ���";
            this.erPacket64.Properties.FieldName = "MobitelIs64Packets";
            this.erPacket64.Properties.RowEdit = this.rce64bitPackets;
            // 
            // erIsNotDrawDgps
            // 
            this.erIsNotDrawDgps.Name = "erIsNotDrawDgps";
            this.erIsNotDrawDgps.Properties.Caption = "�� ���������� DGPS";
            this.erIsNotDrawDgps.Properties.FieldName = "MobitelIsNotDrawDgps";
            this.erIsNotDrawDgps.Properties.RowEdit = this.rce64bitPackets;
            // 
            // _vehicleCat
            // 
            this._vehicleCat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._regNumberRow,
            this._carMakerRow,
            this._carModelRow,
            this._groupRow,
            this._driverRow,
            this._colorRow,
            this.erOutLink,
            this.erCategory,
            this.erCategory2,
            this.editorFuelWay,
            this.editorFuelMotor,
            this.editorIdentif,
            this.erCategory4,
            this.erCategory3,
            this.vehComment});
            this._vehicleCat.Name = "_vehicleCat";
            this._vehicleCat.OptionsRow.AllowFocus = false;
            this._vehicleCat.OptionsRow.AllowMove = false;
            this._vehicleCat.OptionsRow.AllowMoveToCustomizationForm = false;
            this._vehicleCat.OptionsRow.AllowSize = false;
            this._vehicleCat.OptionsRow.DblClickExpanding = false;
            this._vehicleCat.OptionsRow.ShowInCustomizationForm = false;
            this._vehicleCat.Properties.Caption = "���������";
            this._vehicleCat.Properties.ImageIndex = 2;
            // 
            // _regNumberRow
            // 
            this._regNumberRow.Name = "_regNumberRow";
            this._regNumberRow.OptionsRow.AllowMove = false;
            this._regNumberRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._regNumberRow.OptionsRow.AllowSize = false;
            this._regNumberRow.OptionsRow.DblClickExpanding = false;
            this._regNumberRow.OptionsRow.ShowInCustomizationForm = false;
            this._regNumberRow.Properties.Caption = "���. �����";
            this._regNumberRow.Properties.FieldName = "RegNumber";
            this._regNumberRow.Properties.RowEdit = this.teLimit60;
            // 
            // _carMakerRow
            // 
            this._carMakerRow.Name = "_carMakerRow";
            this._carMakerRow.OptionsRow.AllowMove = false;
            this._carMakerRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._carMakerRow.OptionsRow.AllowSize = false;
            this._carMakerRow.OptionsRow.DblClickExpanding = false;
            this._carMakerRow.OptionsRow.ShowInCustomizationForm = false;
            this._carMakerRow.Properties.Caption = "�����";
            this._carMakerRow.Properties.FieldName = "CarMaker";
            this._carMakerRow.Properties.RowEdit = this.teLimit120;
            // 
            // _carModelRow
            // 
            this._carModelRow.Name = "_carModelRow";
            this._carModelRow.OptionsRow.AllowMove = false;
            this._carModelRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._carModelRow.OptionsRow.AllowSize = false;
            this._carModelRow.OptionsRow.DblClickExpanding = false;
            this._carModelRow.OptionsRow.ShowInCustomizationForm = false;
            this._carModelRow.Properties.Caption = "������";
            this._carModelRow.Properties.FieldName = "CarModel";
            this._carModelRow.Properties.RowEdit = this.teLimit60;
            // 
            // _groupRow
            // 
            this._groupRow.Name = "_groupRow";
            this._groupRow.OptionsRow.AllowMove = false;
            this._groupRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._groupRow.OptionsRow.AllowSize = false;
            this._groupRow.OptionsRow.DblClickExpanding = false;
            this._groupRow.OptionsRow.ShowInCustomizationForm = false;
            this._groupRow.Properties.Caption = "������";
            this._groupRow.Properties.FieldName = "Group";
            this._groupRow.Properties.RowEdit = this._groupRepo;
            // 
            // _driverRow
            // 
            this._driverRow.Height = 18;
            this._driverRow.Name = "_driverRow";
            this._driverRow.Properties.Caption = "��������";
            this._driverRow.Properties.FieldName = "Driver";
            this._driverRow.Properties.ImageIndex = 0;
            this._driverRow.Properties.RowEdit = this._driverRepo;
            // 
            // _colorRow
            // 
            this._colorRow.Name = "_colorRow";
            this._colorRow.Properties.Caption = "���� �����";
            this._colorRow.Properties.FieldName = "ColorTrack";
            this._colorRow.Properties.RowEdit = this._colorEdit;
            // 
            // erOutLink
            // 
            this.erOutLink.Name = "erOutLink";
            this.erOutLink.Properties.Caption = "��� ������� ����";
            this.erOutLink.Properties.FieldName = "IdOutLink";
            this.erOutLink.Properties.RowEdit = this.rteOutLink;
            // 
            // erCategory
            // 
            this.erCategory.Name = "erCategory";
            this.erCategory.Properties.Caption = "���������";
            this.erCategory.Properties.FieldName = "Category";
            this.erCategory.Properties.RowEdit = this.cbeCategory;
            // 
            // erCategory2
            // 
            this.erCategory2.Name = "erCategory2";
            this.erCategory2.Properties.Caption = "���������2";
            this.erCategory2.Properties.FieldName = "Category2";
            this.erCategory2.Properties.RowEdit = this.cbeCategory2;
            // 
            // editorFuelWay
            // 
            this.editorFuelWay.Name = "editorFuelWay";
            this.editorFuelWay.Properties.Caption = "������ ������� �/100 ��";
            this.editorFuelWay.Properties.FieldName = "FuelWayLiter";
            this.editorFuelWay.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // editorFuelMotor
            // 
            this.editorFuelMotor.Name = "editorFuelMotor";
            this.editorFuelMotor.Properties.Caption = "������ ������� �/�������";
            this.editorFuelMotor.Properties.FieldName = "FuelMotorLiter";
            this.editorFuelMotor.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // editorIdentif
            // 
            this.editorIdentif.Appearance.Options.UseTextOptions = true;
            this.editorIdentif.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.editorIdentif.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.editorIdentif.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.editorIdentif.Name = "editorIdentif";
            this.editorIdentif.Properties.Caption = "�������������";
            this.editorIdentif.Properties.FieldName = "Identifier";
            this.editorIdentif.Properties.RowEdit = this.editIdentifier;
            // 
            // erCategory4
            // 
            this.erCategory4.Name = "erCategory4";
            this.erCategory4.Properties.Caption = "���������4";
            this.erCategory4.Properties.FieldName = "Category4";
            this.erCategory4.Properties.RowEdit = this.cbeCategory4;
            // 
            // erCategory3
            // 
            this.erCategory3.Name = "erCategory3";
            this.erCategory3.Properties.Caption = "���������3";
            this.erCategory3.Properties.FieldName = "Category3";
            this.erCategory3.Properties.RowEdit = this.cbeCategory3;
            // 
            // vehComment
            // 
            this.vehComment.Appearance.Options.UseTextOptions = true;
            this.vehComment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.vehComment.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.vehComment.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vehComment.Height = 40;
            this.vehComment.MaxCaptionLineCount = 5;
            this.vehComment.Name = "vehComment";
            this.vehComment.OptionsRow.AllowMove = false;
            this.vehComment.OptionsRow.AllowMoveToCustomizationForm = false;
            this.vehComment.OptionsRow.AllowSize = false;
            this.vehComment.OptionsRow.DblClickExpanding = false;
            this.vehComment.OptionsRow.ShowInCustomizationForm = false;
            this.vehComment.Properties.Caption = "�����������";
            this.vehComment.Properties.FieldName = "VehicleComment";
            this.vehComment.Properties.RowEdit = this.vehMemoEdit;
            this.vehComment.Properties.ToolTip = "����������� ������";
            this.vehComment.Tag = "<Null>";
            // 
            // _summaryCat
            // 
            this._summaryCat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._sensorsRow});
            this._summaryCat.Name = "_summaryCat";
            this._summaryCat.OptionsRow.AllowFocus = false;
            this._summaryCat.OptionsRow.AllowMove = false;
            this._summaryCat.OptionsRow.AllowMoveToCustomizationForm = false;
            this._summaryCat.OptionsRow.AllowSize = false;
            this._summaryCat.OptionsRow.DblClickExpanding = false;
            this._summaryCat.OptionsRow.ShowInCustomizationForm = false;
            this._summaryCat.Properties.Caption = "�����";
            this._summaryCat.Properties.ImageIndex = 3;
            this._summaryCat.Visible = false;
            // 
            // _sensorsRow
            // 
            this._sensorsRow.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._logicSensorsRow,
            this._gageSensorsRow});
            this._sensorsRow.Expanded = false;
            this._sensorsRow.Name = "_sensorsRow";
            this._sensorsRow.OptionsRow.AllowMove = false;
            this._sensorsRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._sensorsRow.OptionsRow.AllowSize = false;
            this._sensorsRow.OptionsRow.ShowInCustomizationForm = false;
            this._sensorsRow.Properties.Caption = "���������� ��������";
            this._sensorsRow.Visible = false;
            // 
            // _logicSensorsRow
            // 
            this._logicSensorsRow.Name = "_logicSensorsRow";
            this._logicSensorsRow.OptionsRow.AllowMove = false;
            this._logicSensorsRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._logicSensorsRow.OptionsRow.AllowSize = false;
            this._logicSensorsRow.OptionsRow.DblClickExpanding = false;
            this._logicSensorsRow.OptionsRow.ShowInCustomizationForm = false;
            this._logicSensorsRow.Properties.Caption = "����������";
            this._logicSensorsRow.Visible = false;
            // 
            // _gageSensorsRow
            // 
            this._gageSensorsRow.Name = "_gageSensorsRow";
            this._gageSensorsRow.OptionsRow.AllowMove = false;
            this._gageSensorsRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._gageSensorsRow.OptionsRow.AllowSize = false;
            this._gageSensorsRow.OptionsRow.DblClickExpanding = false;
            this._gageSensorsRow.OptionsRow.ShowInCustomizationForm = false;
            this._gageSensorsRow.Properties.Caption = "�������������";
            this._gageSensorsRow.Visible = false;
            // 
            // _driverGrid
            // 
            this._driverGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._driverGrid.Location = new System.Drawing.Point(373, 0);
            this._driverGrid.MainView = this._driverView;
            this._driverGrid.Margin = new System.Windows.Forms.Padding(0);
            this._driverGrid.Name = "_driverGrid";
            this._driverGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._photoRepo});
            this._driverGrid.Size = new System.Drawing.Size(283, 351);
            this._driverGrid.TabIndex = 3;
            this._driverGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._driverView});
            // 
            // _driverView
            // 
            this._driverView.CardMinSize = new System.Drawing.Size(298, 306);
            this._driverView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this._driverIdCol,
            this._driverFirstNameCol,
            this._driverLastNameCol,
            this._driverDayOfBirthCol,
            this._driverPhotoCol,
            this._driverLicenseCol,
            this._driverCategories,
            this._driverIdentifierCol});
            this._driverView.GridControl = this._driverGrid;
            this._driverView.Name = "_driverView";
            this._driverView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this._driverView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this._driverView.OptionsBehavior.AllowExpandCollapse = false;
            this._driverView.OptionsBehavior.AllowPanCards = false;
            this._driverView.OptionsBehavior.AllowRuntimeCustomization = false;
            this._driverView.OptionsBehavior.AllowSwitchViewModes = false;
            this._driverView.OptionsBehavior.AutoPopulateColumns = false;
            this._driverView.OptionsBehavior.AutoSelectAllInEditor = false;
            this._driverView.OptionsBehavior.ImmediateUpdateRowPosition = false;
            this._driverView.OptionsBehavior.KeepFocusedRowOnUpdate = false;
            this._driverView.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this._driverView.OptionsBehavior.UseTabKey = false;
            this._driverView.OptionsCustomization.AllowFilter = false;
            this._driverView.OptionsCustomization.AllowSort = false;
            this._driverView.OptionsFilter.AllowColumnMRUFilterList = false;
            this._driverView.OptionsFilter.AllowFilterEditor = false;
            this._driverView.OptionsFilter.AllowMRUFilterList = false;
            this._driverView.OptionsHeaderPanel.EnableCarouselModeButton = false;
            this._driverView.OptionsHeaderPanel.EnableColumnModeButton = false;
            this._driverView.OptionsHeaderPanel.EnableCustomizeButton = false;
            this._driverView.OptionsHeaderPanel.EnableMultiColumnModeButton = false;
            this._driverView.OptionsHeaderPanel.EnableMultiRowModeButton = false;
            this._driverView.OptionsHeaderPanel.EnablePanButton = false;
            this._driverView.OptionsHeaderPanel.EnableRowModeButton = false;
            this._driverView.OptionsHeaderPanel.EnableSingleModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowCarouselModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowColumnModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowCustomizeButton = false;
            this._driverView.OptionsHeaderPanel.ShowMultiColumnModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowMultiRowModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowPanButton = false;
            this._driverView.OptionsHeaderPanel.ShowRowModeButton = false;
            this._driverView.OptionsHeaderPanel.ShowSingleModeButton = false;
            this._driverView.OptionsView.AllowHotTrackFields = false;
            this._driverView.OptionsView.ShowCardCaption = false;
            this._driverView.OptionsView.ShowCardExpandButton = false;
            this._driverView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this._driverView.OptionsView.ShowHeaderPanel = false;
            this._driverView.TemplateCard = this.layoutViewCard1;
            // 
            // _driverIdCol
            // 
            this._driverIdCol.AppearanceCell.Options.UseTextOptions = true;
            this._driverIdCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._driverIdCol.Caption = "Id";
            this._driverIdCol.FieldName = "Id";
            this._driverIdCol.LayoutViewField = this.Item5;
            this._driverIdCol.Name = "_driverIdCol";
            this._driverIdCol.OptionsColumn.AllowEdit = false;
            this._driverIdCol.OptionsColumn.AllowFocus = false;
            this._driverIdCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverIdCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdCol.OptionsColumn.AllowMove = false;
            this._driverIdCol.OptionsColumn.AllowShowHide = false;
            this._driverIdCol.OptionsColumn.AllowSize = false;
            this._driverIdCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverIdCol.OptionsColumn.TabStop = false;
            // 
            // Item5
            // 
            this.Item5.EditorPreferredWidth = 155;
            this.Item5.Location = new System.Drawing.Point(110, 0);
            this.Item5.Name = "Item5";
            this.Item5.Size = new System.Drawing.Size(178, 24);
            this.Item5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.Item5.TextSize = new System.Drawing.Size(14, 13);
            this.Item5.TextToControlDistance = 5;
            // 
            // _driverFirstNameCol
            // 
            this._driverFirstNameCol.Caption = "���";
            this._driverFirstNameCol.FieldName = "FirstName";
            this._driverFirstNameCol.LayoutViewField = this.Item6;
            this._driverFirstNameCol.Name = "_driverFirstNameCol";
            this._driverFirstNameCol.OptionsColumn.AllowEdit = false;
            this._driverFirstNameCol.OptionsColumn.AllowFocus = false;
            this._driverFirstNameCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverFirstNameCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverFirstNameCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverFirstNameCol.OptionsColumn.AllowMove = false;
            this._driverFirstNameCol.OptionsColumn.AllowShowHide = false;
            this._driverFirstNameCol.OptionsColumn.AllowSize = false;
            this._driverFirstNameCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverFirstNameCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverFirstNameCol.OptionsColumn.TabStop = false;
            // 
            // Item6
            // 
            this.Item6.EditorPreferredWidth = 146;
            this.Item6.Location = new System.Drawing.Point(110, 24);
            this.Item6.Name = "Item6";
            this.Item6.Size = new System.Drawing.Size(178, 24);
            this.Item6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.Item6.TextSize = new System.Drawing.Size(23, 13);
            this.Item6.TextToControlDistance = 5;
            // 
            // _driverLastNameCol
            // 
            this._driverLastNameCol.Caption = "�������";
            this._driverLastNameCol.FieldName = "LastName";
            this._driverLastNameCol.LayoutViewField = this.Item7;
            this._driverLastNameCol.Name = "_driverLastNameCol";
            this._driverLastNameCol.OptionsColumn.AllowEdit = false;
            this._driverLastNameCol.OptionsColumn.AllowFocus = false;
            this._driverLastNameCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverLastNameCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverLastNameCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverLastNameCol.OptionsColumn.AllowMove = false;
            this._driverLastNameCol.OptionsColumn.AllowShowHide = false;
            this._driverLastNameCol.OptionsColumn.AllowSize = false;
            this._driverLastNameCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverLastNameCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverLastNameCol.OptionsColumn.TabStop = false;
            // 
            // Item7
            // 
            this.Item7.EditorPreferredWidth = 121;
            this.Item7.Location = new System.Drawing.Point(110, 48);
            this.Item7.Name = "Item7";
            this.Item7.Size = new System.Drawing.Size(178, 24);
            this.Item7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.Item7.TextSize = new System.Drawing.Size(48, 13);
            this.Item7.TextToControlDistance = 5;
            // 
            // _driverDayOfBirthCol
            // 
            this._driverDayOfBirthCol.Caption = "�. �.";
            this._driverDayOfBirthCol.FieldName = "DayOfBirth";
            this._driverDayOfBirthCol.LayoutViewField = this.Item8;
            this._driverDayOfBirthCol.Name = "_driverDayOfBirthCol";
            this._driverDayOfBirthCol.OptionsColumn.AllowEdit = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowFocus = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverDayOfBirthCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverDayOfBirthCol.OptionsColumn.AllowMove = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowShowHide = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowSize = false;
            this._driverDayOfBirthCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverDayOfBirthCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverDayOfBirthCol.OptionsColumn.TabStop = false;
            // 
            // Item8
            // 
            this.Item8.EditorPreferredWidth = 140;
            this.Item8.Location = new System.Drawing.Point(110, 72);
            this.Item8.Name = "Item8";
            this.Item8.Size = new System.Drawing.Size(178, 24);
            this.Item8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.Item8.TextSize = new System.Drawing.Size(29, 13);
            this.Item8.TextToControlDistance = 5;
            // 
            // _driverPhotoCol
            // 
            this._driverPhotoCol.Caption = "����";
            this._driverPhotoCol.ColumnEdit = this._photoRepo;
            this._driverPhotoCol.FieldName = "Photo";
            this._driverPhotoCol.LayoutViewField = this.Item4;
            this._driverPhotoCol.Name = "_driverPhotoCol";
            this._driverPhotoCol.OptionsColumn.AllowEdit = false;
            this._driverPhotoCol.OptionsColumn.AllowFocus = false;
            this._driverPhotoCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverPhotoCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverPhotoCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverPhotoCol.OptionsColumn.AllowMove = false;
            this._driverPhotoCol.OptionsColumn.AllowShowHide = false;
            this._driverPhotoCol.OptionsColumn.AllowSize = false;
            this._driverPhotoCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverPhotoCol.OptionsColumn.ShowCaption = false;
            this._driverPhotoCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverPhotoCol.OptionsColumn.TabStop = false;
            // 
            // _photoRepo
            // 
            this._photoRepo.Name = "_photoRepo";
            this._photoRepo.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // Item4
            // 
            this.Item4.EditorPreferredWidth = 96;
            this.Item4.Location = new System.Drawing.Point(0, 0);
            this.Item4.MaxSize = new System.Drawing.Size(110, 120);
            this.Item4.MinSize = new System.Drawing.Size(110, 120);
            this.Item4.Name = "Item4";
            this.Item4.Size = new System.Drawing.Size(110, 120);
            this.Item4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.Item4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 10, 0, 0);
            this.Item4.TextSize = new System.Drawing.Size(0, 0);
            this.Item4.TextToControlDistance = 0;
            this.Item4.TextVisible = false;
            // 
            // _driverLicenseCol
            // 
            this._driverLicenseCol.Caption = "�����";
            this._driverLicenseCol.FieldName = "License";
            this._driverLicenseCol.LayoutViewField = this.layoutViewField1;
            this._driverLicenseCol.Name = "_driverLicenseCol";
            this._driverLicenseCol.OptionsColumn.AllowEdit = false;
            this._driverLicenseCol.OptionsColumn.AllowFocus = false;
            this._driverLicenseCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverLicenseCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverLicenseCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverLicenseCol.OptionsColumn.AllowMove = false;
            this._driverLicenseCol.OptionsColumn.AllowShowHide = false;
            this._driverLicenseCol.OptionsColumn.AllowSize = false;
            this._driverLicenseCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverLicenseCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverLicenseCol.OptionsColumn.TabStop = false;
            // 
            // layoutViewField1
            // 
            this.layoutViewField1.EditorPreferredWidth = 234;
            this.layoutViewField1.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField1.Name = "layoutViewField1";
            this.layoutViewField1.Size = new System.Drawing.Size(278, 24);
            this.layoutViewField1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField1.TextSize = new System.Drawing.Size(35, 13);
            this.layoutViewField1.TextToControlDistance = 5;
            // 
            // _driverCategories
            // 
            this._driverCategories.Caption = "����. ���������";
            this._driverCategories.FieldName = "Categories";
            this._driverCategories.LayoutViewField = this.layoutViewField2;
            this._driverCategories.Name = "_driverCategories";
            this._driverCategories.OptionsColumn.AllowEdit = false;
            this._driverCategories.OptionsColumn.AllowFocus = false;
            this._driverCategories.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverCategories.OptionsColumn.AllowIncrementalSearch = false;
            this._driverCategories.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverCategories.OptionsColumn.AllowMove = false;
            this._driverCategories.OptionsColumn.AllowShowHide = false;
            this._driverCategories.OptionsColumn.AllowSize = false;
            this._driverCategories.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverCategories.OptionsColumn.ShowInCustomizationForm = false;
            this._driverCategories.OptionsColumn.TabStop = false;
            // 
            // layoutViewField2
            // 
            this.layoutViewField2.EditorPreferredWidth = 179;
            this.layoutViewField2.Location = new System.Drawing.Point(0, 24);
            this.layoutViewField2.Name = "layoutViewField2";
            this.layoutViewField2.Size = new System.Drawing.Size(278, 24);
            this.layoutViewField2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField2.TextSize = new System.Drawing.Size(90, 13);
            this.layoutViewField2.TextToControlDistance = 5;
            // 
            // _driverIdentifierCol
            // 
            this._driverIdentifierCol.AppearanceCell.Options.UseTextOptions = true;
            this._driverIdentifierCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._driverIdentifierCol.Caption = "�������������";
            this._driverIdentifierCol.FieldName = "Identifier";
            this._driverIdentifierCol.LayoutViewField = this.Item3;
            this._driverIdentifierCol.Name = "_driverIdentifierCol";
            this._driverIdentifierCol.OptionsColumn.AllowEdit = false;
            this._driverIdentifierCol.OptionsColumn.AllowFocus = false;
            this._driverIdentifierCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdentifierCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverIdentifierCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdentifierCol.OptionsColumn.AllowMove = false;
            this._driverIdentifierCol.OptionsColumn.AllowShowHide = false;
            this._driverIdentifierCol.OptionsColumn.AllowSize = false;
            this._driverIdentifierCol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._driverIdentifierCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverIdentifierCol.OptionsColumn.TabStop = false;
            // 
            // Item3
            // 
            this.Item3.EditorPreferredWidth = 183;
            this.Item3.Location = new System.Drawing.Point(0, 48);
            this.Item3.Name = "Item3";
            this.Item3.Size = new System.Drawing.Size(278, 24);
            this.Item3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.Item3.TextSize = new System.Drawing.Size(86, 13);
            this.Item3.TextToControlDistance = 5;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.GroupBordersVisible = false;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Group3});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // Group3
            // 
            this.Group3.CustomizationFormText = "��������";
            this.Group3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Group4,
            this.Item4,
            this.item1,
            this.Item5,
            this.Item6,
            this.Item7,
            this.Item8});
            this.Group3.Location = new System.Drawing.Point(0, 0);
            this.Group3.Name = "Group3";
            this.Group3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Group3.Size = new System.Drawing.Size(294, 249);
            this.Group3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.Group3.Text = "��������";
            // 
            // Group4
            // 
            this.Group4.CustomizationFormText = "����";
            this.Group4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField1,
            this.layoutViewField2,
            this.Item3});
            this.Group4.Location = new System.Drawing.Point(0, 120);
            this.Group4.Name = "Group4";
            this.Group4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Group4.Size = new System.Drawing.Size(288, 104);
            this.Group4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.Group4.Text = "����";
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(110, 96);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(178, 24);
            this.item1.Text = "item1";
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._cancelBtn.Location = new System.Drawing.Point(383, 363);
            this._cancelBtn.Margin = new System.Windows.Forms.Padding(10);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(64, 23);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.Text = "������";
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // _saveBtn
            // 
            this._saveBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._saveBtn.Location = new System.Drawing.Point(257, 363);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(10);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(86, 23);
            this._saveBtn.TabIndex = 2;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // row
            // 
            this.row.Name = "row";
            // 
            // EditVehicle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._mainTable);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EditVehicle";
            this.Padding = new System.Windows.Forms.Padding(10, 26, 10, 10);
            this.Size = new System.Drawing.Size(726, 821);
            this._mainTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._propertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLimit60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLimit120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editIdentifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbItemComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rce64bitPackets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeCategory4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehRichTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._driverView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Item3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private DevExpress.XtraVerticalGrid.PropertyGridControl _propertyGrid;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _vehicleCat;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _loginRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _simNumberRow;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _summaryCat;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _regNumberRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _carMakerRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _carModelRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _sensorsRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _logicSensorsRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _gageSensorsRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _groupRow;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _groupRepo;
    private DevExpress.XtraGrid.GridControl _driverGrid;
    private DevExpress.XtraGrid.Views.Layout.LayoutView _driverView;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverIdCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverFirstNameCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverLastNameCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverDayOfBirthCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverPhotoCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverLicenseCol;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverCategories;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _driverIdentifierCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _photoRepo;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _driverRepo;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _driverRow;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _mobitelCat;
    private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit _colorEdit;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _colorRow;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteOutLink;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teLimit60;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teLimit120;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erCategory;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow editorFuelWay;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow editorFuelMotor;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit editIdentifier;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow editorIdentif;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeCategory;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeCategory2;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erCategory2;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item5;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item6;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item7;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item8;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item4;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField1;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField2;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField Item3;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
    private DevExpress.XtraLayout.LayoutControlGroup Group3;
    private DevExpress.XtraLayout.LayoutControlGroup Group4;
    private DevExpress.XtraLayout.EmptySpaceItem item1;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox pcbItemComboBox;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _pcbDataRow;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rce64bitPackets;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erPacket64;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsNotDrawDgps;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeCategory3;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeCategory4;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erCategory4;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erCategory3;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit vehMemoEdit;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow vehComment;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit vehMemoEdit1;
    private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit vehRichTextEdit1;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow row;

  }
}
