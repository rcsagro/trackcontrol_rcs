namespace TrackControl.Vehicles.Tuning
{
  partial class EditGroup
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditGroup));
            this._mainTable = new System.Windows.Forms.TableLayoutPanel();
            this._propertyGrid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this._images = new DevExpress.Utils.ImageCollection();
            this._descriptionRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._nameRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rteOutLink = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.editIdentifier = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._attributesCat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._nameRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._descriptionRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutLink = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorFuelWayKmGrp = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorFuelMotoHrGrp = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._summaryCat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._ownGroupsRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._ownVehiclesRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._vehiclesGrid = new DevExpress.XtraGrid.GridControl();
            this._vehiclesView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._mobitelLoginCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._mobitelSimNumberCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._vehicleRegNumberCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleIdentifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this._vehicleModelCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._vehicleGroupCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._driverFullNameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFuelWay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFuelMotor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommentar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sbExcelVehicles = new DevExpress.XtraEditors.SimpleButton();
            this._mainTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._propertyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editIdentifier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._vehiclesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._vehiclesView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainTable
            // 
            this._mainTable.ColumnCount = 5;
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 274F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this._mainTable.Controls.Add(this._propertyGrid, 1, 1);
            this._mainTable.Controls.Add(this._cancelBtn, 2, 2);
            this._mainTable.Controls.Add(this._saveBtn, 1, 2);
            this._mainTable.Controls.Add(this._vehiclesGrid, 1, 3);
            this._mainTable.Controls.Add(this.sbExcelVehicles, 3, 2);
            this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTable.Location = new System.Drawing.Point(0, 0);
            this._mainTable.MinimumSize = new System.Drawing.Size(0, 210);
            this._mainTable.Name = "_mainTable";
            this._mainTable.RowCount = 4;
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.Size = new System.Drawing.Size(772, 595);
            this._mainTable.TabIndex = 0;
            // 
            // _propertyGrid
            // 
            this._mainTable.SetColumnSpan(this._propertyGrid, 2);
            this._propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._propertyGrid.ImageList = this._images;
            this._propertyGrid.Location = new System.Drawing.Point(13, 29);
            this._propertyGrid.Name = "_propertyGrid";
            this._propertyGrid.RecordWidth = 97;
            this._propertyGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._descriptionRepo,
            this._nameRepo,
            this.rteOutLink,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.editIdentifier});
            this._propertyGrid.RowHeaderWidth = 103;
            this._propertyGrid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._attributesCat,
            this._summaryCat});
            this._propertyGrid.Size = new System.Drawing.Size(352, 167);
            this._propertyGrid.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Summary");
            // 
            // _descriptionRepo
            // 
            this._descriptionRepo.MaxLength = 1000;
            this._descriptionRepo.Name = "_descriptionRepo";
            // 
            // _nameRepo
            // 
            this._nameRepo.AutoHeight = false;
            this._nameRepo.MaxLength = 50;
            this._nameRepo.Name = "_nameRepo";
            // 
            // rteOutLink
            // 
            this.rteOutLink.AutoHeight = false;
            this.rteOutLink.MaxLength = 20;
            this.rteOutLink.Name = "rteOutLink";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // editIdentifier
            // 
            this.editIdentifier.AutoHeight = false;
            this.editIdentifier.Name = "editIdentifier";
            // 
            // _attributesCat
            // 
            this._attributesCat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._nameRow,
            this._descriptionRow,
            this.erOutLink,
            this.editorFuelWayKmGrp,
            this.editorFuelMotoHrGrp});
            this._attributesCat.Name = "_attributesCat";
            this._attributesCat.OptionsRow.AllowFocus = false;
            this._attributesCat.OptionsRow.AllowMove = false;
            this._attributesCat.OptionsRow.AllowMoveToCustomizationForm = false;
            this._attributesCat.OptionsRow.AllowSize = false;
            this._attributesCat.OptionsRow.DblClickExpanding = false;
            this._attributesCat.OptionsRow.ShowInCustomizationForm = false;
            this._attributesCat.Properties.Caption = "��������";
            // 
            // _nameRow
            // 
            this._nameRow.Name = "_nameRow";
            this._nameRow.Properties.Caption = "�������� ������";
            this._nameRow.Properties.FieldName = "NewName";
            this._nameRow.Properties.RowEdit = this._nameRepo;
            // 
            // _descriptionRow
            // 
            this._descriptionRow.Height = 40;
            this._descriptionRow.Name = "_descriptionRow";
            this._descriptionRow.OptionsRow.AllowMove = false;
            this._descriptionRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._descriptionRow.OptionsRow.AllowSize = false;
            this._descriptionRow.OptionsRow.DblClickExpanding = false;
            this._descriptionRow.OptionsRow.ShowInCustomizationForm = false;
            this._descriptionRow.Properties.Caption = "�������� ������";
            this._descriptionRow.Properties.FieldName = "NewDescription";
            this._descriptionRow.Properties.RowEdit = this._descriptionRepo;
            // 
            // erOutLink
            // 
            this.erOutLink.Name = "erOutLink";
            this.erOutLink.Properties.Caption = "��� ������� ����";
            this.erOutLink.Properties.FieldName = "IdOutLink";
            this.erOutLink.Properties.RowEdit = this.rteOutLink;
            // 
            // editorFuelWayKmGrp
            // 
            this.editorFuelWayKmGrp.Name = "editorFuelWayKmGrp";
            this.editorFuelWayKmGrp.Properties.Caption = "������ ������� �/100 ��";
            this.editorFuelWayKmGrp.Properties.FieldName = "FuelWayKmGrp";
            this.editorFuelWayKmGrp.Properties.RowEdit = this.repositoryItemTextEdit1;
            // 
            // editorFuelMotoHrGrp
            // 
            this.editorFuelMotoHrGrp.Name = "editorFuelMotoHrGrp";
            this.editorFuelMotoHrGrp.Properties.Caption = "������ ������� �/�������";
            this.editorFuelMotoHrGrp.Properties.FieldName = "FuelMotoHrGrp";
            this.editorFuelMotoHrGrp.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // _summaryCat
            // 
            this._summaryCat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._ownGroupsRow,
            this._ownVehiclesRow});
            this._summaryCat.Name = "_summaryCat";
            this._summaryCat.OptionsRow.AllowFocus = false;
            this._summaryCat.OptionsRow.AllowMove = false;
            this._summaryCat.OptionsRow.AllowMoveToCustomizationForm = false;
            this._summaryCat.OptionsRow.AllowSize = false;
            this._summaryCat.OptionsRow.DblClickExpanding = false;
            this._summaryCat.OptionsRow.ShowInCustomizationForm = false;
            this._summaryCat.Properties.Caption = "�����";
            this._summaryCat.Properties.ImageIndex = 0;
            // 
            // _ownGroupsRow
            // 
            this._ownGroupsRow.Name = "_ownGroupsRow";
            this._ownGroupsRow.Properties.Caption = "����� ��������";
            this._ownGroupsRow.Properties.FieldName = "AllSubGroupsCount";
            this._ownGroupsRow.Properties.ReadOnly = true;
            // 
            // _ownVehiclesRow
            // 
            this._ownVehiclesRow.Name = "_ownVehiclesRow";
            this._ownVehiclesRow.Properties.Caption = "������ ����������";
            this._ownVehiclesRow.Properties.FieldName = "AllItemsCount";
            this._ownVehiclesRow.Properties.ReadOnly = true;
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._cancelBtn.Location = new System.Drawing.Point(298, 209);
            this._cancelBtn.Margin = new System.Windows.Forms.Padding(10);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(60, 23);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.Text = "������";
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // _saveBtn
            // 
            this._saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._saveBtn.Location = new System.Drawing.Point(182, 209);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(10);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(92, 23);
            this._saveBtn.TabIndex = 2;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // _vehiclesGrid
            // 
            this._mainTable.SetColumnSpan(this._vehiclesGrid, 3);
            this._vehiclesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._vehiclesGrid.Location = new System.Drawing.Point(13, 248);
            this._vehiclesGrid.MainView = this._vehiclesView;
            this._vehiclesGrid.Name = "_vehiclesGrid";
            this._vehiclesGrid.Size = new System.Drawing.Size(741, 344);
            this._vehiclesGrid.TabIndex = 3;
            this._vehiclesGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._vehiclesView,
            this.gridView2});
            // 
            // _vehiclesView
            // 
            this._vehiclesView.ColumnPanelRowHeight = 40;
            this._vehiclesView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._mobitelLoginCol,
            this._mobitelSimNumberCol,
            this._vehicleRegNumberCol,
            this.colVehicleIdentifier,
            this._vehicleModelCol,
            this._vehicleGroupCol,
            this._driverFullNameCol,
            this.columnFuelWay,
            this.columnFuelMotor,
            this.colCategory,
            this.colCategory2,
            this.colCategory3,
            this.colCategory4,
            this.colCommentar});
            this._vehiclesView.GridControl = this._vehiclesGrid;
            this._vehiclesView.Name = "_vehiclesView";
            // 
            // _mobitelLoginCol
            // 
            this._mobitelLoginCol.AppearanceCell.Options.UseTextOptions = true;
            this._mobitelLoginCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._mobitelLoginCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._mobitelLoginCol.AppearanceHeader.Options.UseTextOptions = true;
            this._mobitelLoginCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._mobitelLoginCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._mobitelLoginCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._mobitelLoginCol.Caption = "�����";
            this._mobitelLoginCol.FieldName = "Login";
            this._mobitelLoginCol.MinWidth = 10;
            this._mobitelLoginCol.Name = "_mobitelLoginCol";
            this._mobitelLoginCol.OptionsColumn.AllowEdit = false;
            this._mobitelLoginCol.OptionsColumn.AllowFocus = false;
            this._mobitelLoginCol.OptionsColumn.AllowShowHide = false;
            this._mobitelLoginCol.OptionsColumn.ShowInCustomizationForm = false;
            this._mobitelLoginCol.Visible = true;
            this._mobitelLoginCol.VisibleIndex = 0;
            this._mobitelLoginCol.Width = 30;
            // 
            // _mobitelSimNumberCol
            // 
            this._mobitelSimNumberCol.AppearanceCell.Options.UseTextOptions = true;
            this._mobitelSimNumberCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._mobitelSimNumberCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._mobitelSimNumberCol.AppearanceHeader.Options.UseTextOptions = true;
            this._mobitelSimNumberCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._mobitelSimNumberCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._mobitelSimNumberCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._mobitelSimNumberCol.Caption = "����� SIM";
            this._mobitelSimNumberCol.FieldName = "SimNumber";
            this._mobitelSimNumberCol.MinWidth = 10;
            this._mobitelSimNumberCol.Name = "_mobitelSimNumberCol";
            this._mobitelSimNumberCol.OptionsColumn.AllowEdit = false;
            this._mobitelSimNumberCol.OptionsColumn.AllowFocus = false;
            this._mobitelSimNumberCol.OptionsColumn.AllowShowHide = false;
            this._mobitelSimNumberCol.OptionsColumn.ShowInCustomizationForm = false;
            this._mobitelSimNumberCol.Visible = true;
            this._mobitelSimNumberCol.VisibleIndex = 1;
            this._mobitelSimNumberCol.Width = 50;
            // 
            // _vehicleRegNumberCol
            // 
            this._vehicleRegNumberCol.AppearanceCell.Options.UseTextOptions = true;
            this._vehicleRegNumberCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._vehicleRegNumberCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleRegNumberCol.AppearanceHeader.Options.UseTextOptions = true;
            this._vehicleRegNumberCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._vehicleRegNumberCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleRegNumberCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._vehicleRegNumberCol.Caption = "���. �����";
            this._vehicleRegNumberCol.FieldName = "RegNumber";
            this._vehicleRegNumberCol.MinWidth = 10;
            this._vehicleRegNumberCol.Name = "_vehicleRegNumberCol";
            this._vehicleRegNumberCol.OptionsColumn.AllowEdit = false;
            this._vehicleRegNumberCol.OptionsColumn.AllowFocus = false;
            this._vehicleRegNumberCol.OptionsColumn.AllowShowHide = false;
            this._vehicleRegNumberCol.OptionsColumn.ShowInCustomizationForm = false;
            this._vehicleRegNumberCol.Visible = true;
            this._vehicleRegNumberCol.VisibleIndex = 2;
            this._vehicleRegNumberCol.Width = 60;
            // 
            // colVehicleIdentifier
            // 
            this.colVehicleIdentifier.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleIdentifier.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleIdentifier.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVehicleIdentifier.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colVehicleIdentifier.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleIdentifier.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleIdentifier.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVehicleIdentifier.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleIdentifier.Caption = "�������������";
            this.colVehicleIdentifier.FieldName = "Identifier";
            this.colVehicleIdentifier.MinWidth = 10;
            this.colVehicleIdentifier.Name = "colVehicleIdentifier";
            this.colVehicleIdentifier.OptionsColumn.AllowEdit = false;
            this.colVehicleIdentifier.OptionsColumn.AllowFocus = false;
            this.colVehicleIdentifier.OptionsColumn.AllowShowHide = false;
            this.colVehicleIdentifier.OptionsColumn.ShowInCustomizationForm = false;
            this.colVehicleIdentifier.ToolTip = "������������� ������������� ��������";
            this.colVehicleIdentifier.Visible = true;
            this.colVehicleIdentifier.VisibleIndex = 3;
            this.colVehicleIdentifier.Width = 20;
            // 
            // _vehicleModelCol
            // 
            this._vehicleModelCol.AppearanceCell.Options.UseTextOptions = true;
            this._vehicleModelCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._vehicleModelCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleModelCol.AppearanceHeader.Options.UseTextOptions = true;
            this._vehicleModelCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._vehicleModelCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleModelCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._vehicleModelCol.Caption = "�����, ������";
            this._vehicleModelCol.FieldName = "VehicleModel";
            this._vehicleModelCol.MinWidth = 10;
            this._vehicleModelCol.Name = "_vehicleModelCol";
            this._vehicleModelCol.OptionsColumn.AllowEdit = false;
            this._vehicleModelCol.OptionsColumn.AllowFocus = false;
            this._vehicleModelCol.OptionsColumn.AllowShowHide = false;
            this._vehicleModelCol.OptionsColumn.ShowInCustomizationForm = false;
            this._vehicleModelCol.Visible = true;
            this._vehicleModelCol.VisibleIndex = 4;
            this._vehicleModelCol.Width = 60;
            // 
            // _vehicleGroupCol
            // 
            this._vehicleGroupCol.AppearanceCell.Options.UseTextOptions = true;
            this._vehicleGroupCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._vehicleGroupCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleGroupCol.AppearanceHeader.Options.UseTextOptions = true;
            this._vehicleGroupCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._vehicleGroupCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._vehicleGroupCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._vehicleGroupCol.Caption = "������";
            this._vehicleGroupCol.FieldName = "Group";
            this._vehicleGroupCol.MinWidth = 10;
            this._vehicleGroupCol.Name = "_vehicleGroupCol";
            this._vehicleGroupCol.OptionsColumn.AllowEdit = false;
            this._vehicleGroupCol.OptionsColumn.AllowFocus = false;
            this._vehicleGroupCol.OptionsColumn.AllowShowHide = false;
            this._vehicleGroupCol.OptionsColumn.ShowInCustomizationForm = false;
            this._vehicleGroupCol.Visible = true;
            this._vehicleGroupCol.VisibleIndex = 5;
            this._vehicleGroupCol.Width = 50;
            // 
            // _driverFullNameCol
            // 
            this._driverFullNameCol.AppearanceCell.Options.UseTextOptions = true;
            this._driverFullNameCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._driverFullNameCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverFullNameCol.AppearanceHeader.Options.UseTextOptions = true;
            this._driverFullNameCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._driverFullNameCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverFullNameCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._driverFullNameCol.Caption = "��������";
            this._driverFullNameCol.FieldName = "Driver";
            this._driverFullNameCol.MinWidth = 10;
            this._driverFullNameCol.Name = "_driverFullNameCol";
            this._driverFullNameCol.OptionsColumn.AllowEdit = false;
            this._driverFullNameCol.OptionsColumn.AllowFocus = false;
            this._driverFullNameCol.OptionsColumn.AllowIncrementalSearch = false;
            this._driverFullNameCol.OptionsColumn.AllowShowHide = false;
            this._driverFullNameCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverFullNameCol.Visible = true;
            this._driverFullNameCol.VisibleIndex = 6;
            this._driverFullNameCol.Width = 60;
            // 
            // columnFuelWay
            // 
            this.columnFuelWay.AppearanceCell.Options.UseTextOptions = true;
            this.columnFuelWay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.columnFuelWay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelWay.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFuelWay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFuelWay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelWay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnFuelWay.Caption = "������ ������� �/100 ��";
            this.columnFuelWay.FieldName = "FuelWayLiter";
            this.columnFuelWay.MinWidth = 10;
            this.columnFuelWay.Name = "columnFuelWay";
            this.columnFuelWay.OptionsColumn.AllowEdit = false;
            this.columnFuelWay.OptionsColumn.AllowFocus = false;
            this.columnFuelWay.OptionsColumn.AllowShowHide = false;
            this.columnFuelWay.OptionsColumn.ShowInCustomizationForm = false;
            this.columnFuelWay.ToolTip = "������� ������� �/100 ��";
            this.columnFuelWay.Visible = true;
            this.columnFuelWay.VisibleIndex = 7;
            this.columnFuelWay.Width = 50;
            // 
            // columnFuelMotor
            // 
            this.columnFuelMotor.AppearanceCell.Options.UseTextOptions = true;
            this.columnFuelMotor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.columnFuelMotor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelMotor.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFuelMotor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFuelMotor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelMotor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnFuelMotor.Caption = "������ ������� �/�������";
            this.columnFuelMotor.FieldName = "FuelMotorLiter";
            this.columnFuelMotor.MinWidth = 10;
            this.columnFuelMotor.Name = "columnFuelMotor";
            this.columnFuelMotor.OptionsColumn.AllowEdit = false;
            this.columnFuelMotor.OptionsColumn.AllowFocus = false;
            this.columnFuelMotor.OptionsColumn.AllowShowHide = false;
            this.columnFuelMotor.OptionsColumn.ShowInCustomizationForm = false;
            this.columnFuelMotor.ToolTip = "������ ������� �/�������";
            this.columnFuelMotor.Visible = true;
            this.columnFuelMotor.VisibleIndex = 8;
            this.columnFuelMotor.Width = 50;
            // 
            // colCategory
            // 
            this.colCategory.AppearanceCell.Options.UseTextOptions = true;
            this.colCategory.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategory.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colCategory.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategory.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategory.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategory.Caption = "���������";
            this.colCategory.FieldName = "VehicleCategory";
            this.colCategory.MinWidth = 10;
            this.colCategory.Name = "colCategory";
            this.colCategory.OptionsColumn.AllowEdit = false;
            this.colCategory.OptionsColumn.AllowFocus = false;
            this.colCategory.OptionsColumn.ReadOnly = true;
            this.colCategory.OptionsColumn.ShowInCustomizationForm = false;
            this.colCategory.ToolTip = "��������� ����������";
            this.colCategory.Visible = true;
            this.colCategory.VisibleIndex = 9;
            this.colCategory.Width = 50;
            // 
            // colCategory2
            // 
            this.colCategory2.AppearanceCell.Options.UseTextOptions = true;
            this.colCategory2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategory2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colCategory2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategory2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategory2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategory2.Caption = "���������2";
            this.colCategory2.FieldName = "VehicleCategory2";
            this.colCategory2.MinWidth = 10;
            this.colCategory2.Name = "colCategory2";
            this.colCategory2.OptionsColumn.AllowEdit = false;
            this.colCategory2.OptionsColumn.AllowFocus = false;
            this.colCategory2.OptionsColumn.ReadOnly = true;
            this.colCategory2.OptionsColumn.ShowInCustomizationForm = false;
            this.colCategory2.ToolTip = "��������� ���������� 2";
            this.colCategory2.Visible = true;
            this.colCategory2.VisibleIndex = 10;
            this.colCategory2.Width = 50;
            // 
            // colCategory3
            // 
            this.colCategory3.AppearanceCell.Options.UseTextOptions = true;
            this.colCategory3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategory3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategory3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategory3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategory3.Caption = "���������3";
            this.colCategory3.FieldName = "VehicleCategory3";
            this.colCategory3.MinWidth = 10;
            this.colCategory3.Name = "colCategory3";
            this.colCategory3.OptionsColumn.AllowEdit = false;
            this.colCategory3.OptionsColumn.AllowFocus = false;
            this.colCategory3.OptionsColumn.ReadOnly = true;
            this.colCategory3.OptionsColumn.ShowInCustomizationForm = false;
            this.colCategory3.ToolTip = "��������� ���������� 3";
            this.colCategory3.Visible = true;
            this.colCategory3.VisibleIndex = 11;
            this.colCategory3.Width = 60;
            // 
            // colCategory4
            // 
            this.colCategory4.AppearanceCell.Options.UseTextOptions = true;
            this.colCategory4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategory4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategory4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategory4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCategory4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategory4.Caption = "���������4";
            this.colCategory4.FieldName = "VehicleCategory4";
            this.colCategory4.MinWidth = 10;
            this.colCategory4.Name = "colCategory4";
            this.colCategory4.OptionsColumn.AllowEdit = false;
            this.colCategory4.OptionsColumn.AllowFocus = false;
            this.colCategory4.OptionsColumn.ReadOnly = true;
            this.colCategory4.OptionsColumn.ShowInCustomizationForm = false;
            this.colCategory4.ToolTip = "��������� ���������� 4";
            this.colCategory4.Visible = true;
            this.colCategory4.VisibleIndex = 12;
            this.colCategory4.Width = 50;
            // 
            // colCommentar
            // 
            this.colCommentar.Caption = "�����������";
            this.colCommentar.FieldName = "VehicleComment";
            this.colCommentar.Name = "colCommentar";
            this.colCommentar.OptionsColumn.AllowEdit = false;
            this.colCommentar.OptionsColumn.AllowFocus = false;
            this.colCommentar.OptionsColumn.ReadOnly = true;
            this.colCommentar.OptionsColumn.ShowInCustomizationForm = false;
            this.colCommentar.ToolTip = "����������� ������";
            this.colCommentar.Visible = true;
            this.colCommentar.VisibleIndex = 13;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this._vehiclesGrid;
            this.gridView2.Name = "gridView2";
            // 
            // sbExcelVehicles
            // 
            this.sbExcelVehicles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbExcelVehicles.Appearance.Options.UseTextOptions = true;
            this.sbExcelVehicles.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sbExcelVehicles.Location = new System.Drawing.Point(731, 219);
            this.sbExcelVehicles.Name = "sbExcelVehicles";
            this.sbExcelVehicles.Size = new System.Drawing.Size(23, 23);
            this.sbExcelVehicles.TabIndex = 4;
            this.sbExcelVehicles.Click += new System.EventHandler(this.sbExcelVehicles_Click);
            // 
            // EditGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._mainTable);
            this.Name = "EditGroup";
            this.Size = new System.Drawing.Size(772, 595);
            this._mainTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._propertyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editIdentifier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._vehiclesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._vehiclesView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private DevExpress.XtraVerticalGrid.PropertyGridControl _propertyGrid;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _attributesCat;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _nameRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _descriptionRow;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _summaryCat;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _ownGroupsRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _ownVehiclesRow;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _descriptionRepo;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraGrid.GridControl _vehiclesGrid;
    private DevExpress.XtraGrid.Views.Grid.GridView _vehiclesView;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    private DevExpress.XtraGrid.Columns.GridColumn _mobitelLoginCol;
    private DevExpress.XtraGrid.Columns.GridColumn _mobitelSimNumberCol;
    private DevExpress.XtraGrid.Columns.GridColumn _vehicleRegNumberCol;
    private DevExpress.XtraGrid.Columns.GridColumn _vehicleModelCol;
    private DevExpress.XtraGrid.Columns.GridColumn _vehicleGroupCol;
    private DevExpress.XtraGrid.Columns.GridColumn _driverFullNameCol;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _nameRepo;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteOutLink;
    private DevExpress.XtraGrid.Columns.GridColumn columnFuelWay;
    private DevExpress.XtraGrid.Columns.GridColumn columnFuelMotor;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow editorFuelWayKmGrp;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow editorFuelMotoHrGrp;
    private DevExpress.XtraGrid.Columns.GridColumn colVehicleIdentifier;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit editIdentifier;
    private DevExpress.XtraGrid.Columns.GridColumn colCategory;
    private DevExpress.XtraGrid.Columns.GridColumn colCategory2;
    private DevExpress.XtraGrid.Columns.GridColumn colCategory3;
    private DevExpress.XtraGrid.Columns.GridColumn colCategory4;
    private DevExpress.XtraGrid.Columns.GridColumn colCommentar;
    private DevExpress.XtraEditors.SimpleButton sbExcelVehicles;

  }
}
