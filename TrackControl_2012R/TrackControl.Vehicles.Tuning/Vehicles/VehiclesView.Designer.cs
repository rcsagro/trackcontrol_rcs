namespace TrackControl.Vehicles.Tuning
{
  partial class VehiclesView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this._addGroupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._deleteGroupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._saveAllBtn = new DevExpress.XtraBars.BarButtonItem();
            this._cancelAllBtn = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCategories = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCategories2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._treeSplitter = new DevExpress.XtraEditors.SplitContainerControl();
            this.bbiCategories3 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCategories4 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._treeSplitter)).BeginInit();
            this._treeSplitter.SuspendLayout();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._addGroupBtn,
            this._deleteGroupBtn,
            this._saveAllBtn,
            this._cancelAllBtn,
            this.bbiCategories,
            this.bbiCategories2,
            this.bbiCategories3,
            this.bbiCategories4});
            this._barManager.MaxItemId = 8;
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._addGroupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteGroupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._saveAllBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._cancelAllBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCategories, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCategories2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCategories3),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCategories4)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _addGroupBtn
            // 
            this._addGroupBtn.Caption = "��������";
            this._addGroupBtn.Hint = "�������� ����� ������";
            this._addGroupBtn.Id = 0;
            this._addGroupBtn.Name = "_addGroupBtn";
            this._addGroupBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._addGroupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addGroupBtn_ItemClick);
            // 
            // _deleteGroupBtn
            // 
            this._deleteGroupBtn.Caption = "�������";
            this._deleteGroupBtn.Hint = "������� ������� ������";
            this._deleteGroupBtn.Id = 1;
            this._deleteGroupBtn.Name = "_deleteGroupBtn";
            this._deleteGroupBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._deleteGroupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteGroupBtn_ItemClick);
            // 
            // _saveAllBtn
            // 
            this._saveAllBtn.Caption = "���������";
            this._saveAllBtn.Hint = "��������� ��� ���������";
            this._saveAllBtn.Id = 2;
            this._saveAllBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveAllBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._saveAllBtn.Name = "_saveAllBtn";
            this._saveAllBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._saveAllBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.saveAllBtn_ItemClick);
            // 
            // _cancelAllBtn
            // 
            this._cancelAllBtn.Caption = "������";
            this._cancelAllBtn.Hint = "�������� ��� ���������";
            this._cancelAllBtn.Id = 3;
            this._cancelAllBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._cancelAllBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._cancelAllBtn.Name = "_cancelAllBtn";
            this._cancelAllBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._cancelAllBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelAllBtn_ItemClick);
            // 
            // bbiCategories
            // 
            this.bbiCategories.Caption = "���������";
            this.bbiCategories.Hint = "��������� ������������ �������";
            this.bbiCategories.Id = 4;
            this.bbiCategories.Name = "bbiCategories";
            this.bbiCategories.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCategories_ItemClick);
            // 
            // bbiCategories2
            // 
            this.bbiCategories2.Caption = "���������2";
            this.bbiCategories2.Hint = "��������� ������������ �������";
            this.bbiCategories2.Id = 5;
            this.bbiCategories2.Name = "bbiCategories2";
            this.bbiCategories2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCategories2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(779, 29);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 544);
            this.barDockControlBottom.Size = new System.Drawing.Size(779, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 29);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(779, 29);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // _treeSplitter
            // 
            this._treeSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this._treeSplitter.Location = new System.Drawing.Point(0, 29);
            this._treeSplitter.Margin = new System.Windows.Forms.Padding(0);
            this._treeSplitter.Name = "_treeSplitter";
            this._treeSplitter.Panel1.Text = "VehiclesTree";
            this._treeSplitter.Panel2.Text = "Panel2";
            this._treeSplitter.Size = new System.Drawing.Size(779, 515);
            this._treeSplitter.SplitterPosition = 361;
            this._treeSplitter.TabIndex = 4;
            this._treeSplitter.Text = "_treeSplitter";
            // 
            // bbiCategories3
            // 
            this.bbiCategories3.Caption = "���������3";
            this.bbiCategories3.Hint = "��������� ������������ �������";
            this.bbiCategories3.Id = 6;
            this.bbiCategories3.Name = "bbiCategories3";
            this.bbiCategories3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCategories3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCategories3_ItemClick);
            // 
            // bbiCategories4
            // 
            this.bbiCategories4.Caption = "���������4";
            this.bbiCategories4.Hint = "��������� ������������ �������";
            this.bbiCategories4.Id = 7;
            this.bbiCategories4.Name = "bbiCategories4";
            this.bbiCategories4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCategories4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCategories4_ItemClick);
            // 
            // VehiclesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._treeSplitter);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "VehiclesView";
            this.Size = new System.Drawing.Size(779, 544);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._treeSplitter)).EndInit();
            this._treeSplitter.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraEditors.SplitContainerControl _treeSplitter;
    private DevExpress.XtraBars.BarButtonItem _addGroupBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteGroupBtn;
    private DevExpress.XtraBars.BarButtonItem _saveAllBtn;
    private DevExpress.XtraBars.BarButtonItem _cancelAllBtn;
    private DevExpress.XtraBars.BarButtonItem bbiCategories;
    private DevExpress.XtraBars.BarButtonItem bbiCategories2;
    private DevExpress.XtraBars.BarButtonItem bbiCategories3;
    private DevExpress.XtraBars.BarButtonItem bbiCategories4;
  }
}
