using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.ViewInfo;
using DevExpress.Utils;

using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning
{
    public partial class VehiclesTree : XtraUserControl
    {
        private bool _isLoaded;
        private VehiclesModel _model;
        private string _condition;
        private object _current;

        public event Action<Entity> SelectionChanged;
        public event Action<VehiclesGroup> RemoveGroupClicked;

        public VehiclesTree(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model");

            _model = model;
            InitializeComponent();
            Init();
            _eraseBtn.Glyph = Shared.FunnelClear;

            initPopupItems();
            _model.RefreshTreeView += RefreshLastTime;
        }

        private void Init()
        {
            _funnelBox.Caption = Resources.Filter;
            _eraseBtn.Caption = Resources.Clear;
            _regNumberCol.Caption = Resources.NumberPlate;
            _modelCol.Caption = Resources.BrandModel;
            _loginCol.Caption = Resources.Login;
            bbiExpand.Glyph = Shared.Expand;
            bbiCollapce.Glyph = Shared.Collapce;
        }

        public Entity Current
        {
            get
            {
                if (_tree.Selection.Count > 0)
                {
                    TreeListNode node = _tree.Selection[0];
                    return (Entity) _tree.GetDataRecordByNode(node);
                }
                else
                    return _model.Root;
            }
        }

        public void SelectEntity(Entity entity)
        {
            if (null == entity)
                throw new ArgumentNullException("entity");

            SetSelectionOperation operation = new SetSelectionOperation(entity);
            _tree.NodesIterator.DoOperation(operation);
        }

        public void SelectVehicle(Vehicle vehicle)
        {
            if (null == vehicle)
                throw new ArgumentNullException("vehicle");
            SetSelectionVehicleOperation operation = new SetSelectionVehicleOperation(vehicle, _regNumberCol, _modelCol,
                _loginCol);
            _tree.NodesIterator.DoOperation(operation);
            VehiclesGroup group;
            TreeListNode findedNode = operation.GetNode(out group);
            if (findedNode == null) return;
            if (group != null && vehicle.Group != group)
                findedNode.TreeList.MoveNode(findedNode,
                    findedNode.TreeList.FindNodeByFieldValue(_regNumberCol.FieldName, vehicle.Group.Name));
            findedNode.TreeList.SetFocusedNode(findedNode);
        }

        public void RefreshTree()
        {
            _isLoaded = false;
            _tree.RefreshDataSource();
        }

        private void RefreshLastTime()
        {
            MethodInvoker action = RefreshLastTimeInvoke;
            if (_tree.InvokeRequired)
            {
                _tree.Invoke(action);
            }
            else
            {
                action();
            }
        }

        private void RefreshLastTimeInvoke()
        {
            UpdateTimeStatus operation = new UpdateTimeStatus(timeCol);
            _tree.NodesIterator.DoLocalOperation(operation, _tree.Nodes);
        }

        private void this_Load(object sender, EventArgs e)
        {
            _tree.DataSource = new object();
            TreeViewService.TreeListInitialView(_tree);
            setCondition(String.Empty);
        }

        private void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new VehiclesGroup[] {_model.Root};
                _isLoaded = true;
            }
            else
            {
                VehiclesGroup group = e.Node as VehiclesGroup;
                if (null != group)
                {
                    IList children = new List<object>();
                    foreach (VehiclesGroup vg in group.OwnGroups)
                        if (true /*vg.AllItems.Count > 0*/)
                            children.Add(vg);
                    foreach (IVehicle vehicle in group.OwnItems)
                        children.Add(vehicle);

                    e.Children = children;
                }
                else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
                {
                    e.Children = new object[] {};
                }
            }
        }

        private void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) e.Node;
                if (e.Column == _regNumberCol)
                    e.CellData = group.Name;
            }
            else if (e.Node is IVehicle)
            {
                IVehicle vehicle = (IVehicle) e.Node;
                if (e.Column == _regNumberCol)
                    e.CellData = vehicle.RegNumber;
                else if (e.Column == _modelCol)
                    e.CellData = String.Format("{0} {1}", vehicle.CarMaker, vehicle.CarModel);
                else if (e.Column == timeCol)
                    e.CellData = vehicle.TimeStatus();
                else if (e.Column == _loginCol)
                    e.CellData = vehicle.Mobitel.Login;
            }
        }

        private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16)/2;
            rect.Y += (rect.Height - 16)/2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle) obj;
                e.Graphics.DrawImage(vehicle.Style.Icon, rect);
            }
            else if (obj is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        }

        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle) return;
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
        }

        private void tree_AfterFocusNode(object sender, NodeEventArgs e)
        {
            Entity entity = (Entity) _tree.GetDataRecordByNode(e.Node);
            onSelectionChanged(entity);
        }

        private void tree_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None
                && TreeListState.Regular == _tree.State)
            {
                _popup.ItemLinks.Clear();

                Point pt = _tree.PointToClient(MousePosition);
                TreeListHitInfo info = _tree.CalcHitInfo(pt);
                if (info.HitInfoType == HitInfoType.Cell)
                {
                    _tree.FocusedNode = info.Node;
                    _current = _tree.GetDataRecordByNode(info.Node);
                    if (_current is VehiclesGroup)
                    {
                        VehiclesGroup group = (VehiclesGroup) _current;
                        if (!group.IsRoot && group.Id > 1)
                            _popup.ItemLinks.Add(_removeGroupBtn);
                    }
                    _popup.ShowPopup(MousePosition);
                }
            }
        }

        private void tree_FilterNode(object sender, FilterNodeEventArgs e)
        {
            IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;
            if (null != vehicle)
            {
                if (_condition != null)
                {
                    bool visible1 = vehicle.RegNumber.ToLower().Contains(_condition.ToLower());
                    bool visible3 = vehicle.CarMaker.ToLower().Contains(_condition.ToLower());
                    bool visible4 = vehicle.CarModel.ToLower().Contains(_condition.ToLower());
                    e.Node.Visible = visible1 || visible3 || visible4;
                }
                else
                {
                    e.Node.Visible = vehicle.RegNumber.Contains("");
                }
                e.Handled = true;
            }
        }

        private void funnelRepo_KeyUp(object sender, KeyEventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            setCondition(edit.Text);
        }

        private void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _funnelBox.EditValue = String.Empty;
            setCondition(String.Empty);
            _tree.FilterNodes();
        }

        private void onSelectionChanged(Entity entity)
        {
            Action<Entity> handler = SelectionChanged;
            if (null != handler)
                handler(entity);
        }

        #region --   �������� ������������ ����   --

        private BarButtonItem _removeGroupBtn;

        private void initPopupItems()
        {
            _removeGroupBtn = new BarButtonItem();
            _removeGroupBtn.Caption = Resources.DeleteGroup;
            _removeGroupBtn.Glyph = Shared.Cross;
            _removeGroupBtn.ItemClick += removeGroupBtn_ItemClick;
        }

        // ���������� � Dispose().  ��. ���� "VehiclesTree.Designer.cs";
        private void disposePopupItems()
        {
            _removeGroupBtn.ItemClick -= removeGroupBtn_ItemClick;
        }

        private void removeGroupBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            VehiclesGroup group = (VehiclesGroup) _current;
            onRemoveGroupClicked(group);
        }

        #endregion

        private void onRemoveGroupClicked(VehiclesGroup group)
        {
            Action<VehiclesGroup> handler = RemoveGroupClicked;
            if (null != handler)
                handler(group);
        }

        private void hideEmpty()
        {
            TreeListNode root = _tree.Nodes[0];
            foreach (TreeListNode node in root.Nodes)
            {
                VehiclesGroup group = _tree.GetDataRecordByNode(node) as VehiclesGroup;
                if (null != group)
                {
                    if (getVisibleCount(node) > 0)
                        node.Visible = true;
                    else
                        node.Visible = false;
                }
            }
        }

        private static int getVisibleCount(TreeListNode node)
        {
            int result = 0;
            foreach (TreeListNode child in node.Nodes)
            {
                if (child.Visible)
                    result++;
            }
            return result;
        }

        private static void showAll(TreeListNode node)
        {
            foreach (TreeListNode child in node.Nodes)
            {
                child.Visible = true;
                showAll(child);
            }
        }

        private void setCondition(string condition)
        {
            _condition = condition;
            if (_condition.Length > 0)
            {
                _eraseBtn.Enabled = true;
                _tree.FilterNodes();
                hideEmpty();
            }
            else
            {
                _eraseBtn.Enabled = false;
                TreeListNode root = _tree.Nodes[0];
                showAll(root);
            }
        }

        #region --   Nested Classes  --

        private class SetSelectionOperation : TreeListOperation
        {
            private Entity _entity;
            private bool _canContinue;

            public SetSelectionOperation(Entity entity)
            {
                _entity = entity;
                _canContinue = true;
            }

            public override bool CanContinueIteration(TreeListNode node)
            {
                return _canContinue;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                Entity entity = (Entity) node.TreeList.GetDataRecordByNode(node);
                if (entity == _entity)
                {
                    node.TreeList.SetFocusedNode(node);
                }
            }
        }

        private class SetSelectionVehicleOperation : TreeListOperation
        {
            private Vehicle _vehicle;
            private bool _canContinue;
            private TreeListColumn _regNumberCol;
            private TreeListColumn _modelCol;
            private TreeListColumn _loginCol;
            private TreeListNode _findedNode;
            private VehiclesGroup _group;

            public SetSelectionVehicleOperation(Vehicle vehicle, TreeListColumn regNumberCol, TreeListColumn modelCol,
                TreeListColumn loginCol)
            {
                _vehicle = vehicle;
                _regNumberCol = regNumberCol;
                _modelCol = modelCol;
                _loginCol = loginCol;
                _canContinue = true;
            }

            public override bool CanContinueIteration(TreeListNode node)
            {
                return _canContinue;
            }

            public TreeListNode GetNode(out VehiclesGroup group)
            {
                group = _group;
                return _findedNode;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                Vehicle vehicle = node.TreeList.GetDataRecordByNode(node) as Vehicle;
                _group = node.TreeList.GetDataRecordByNode(node.ParentNode) as VehiclesGroup;
                if (vehicle == _vehicle)
                {
                    _findedNode = node;
                    node.SetValue(_regNumberCol.VisibleIndex, vehicle.RegNumber);
                    node.SetValue(_modelCol.VisibleIndex, vehicle.CarMaker);
                    node.SetValue(_loginCol.VisibleIndex, vehicle.Mobitel.Login);
                    _canContinue = false;
                }
            }
        }

        private class UpdateTimeStatus : TreeListOperation
        {
            private TreeListColumn _colTime;

            public UpdateTimeStatus(TreeListColumn colTime)
                : base()
            {
                _colTime = colTime;
            }

            public override void Execute(TreeListNode node)
            {
                object obj = node.TreeList.GetDataRecordByNode(node);

                if (obj is IVehicle)
                {
                    IVehicle vehicle = (IVehicle) obj;
                    node.SetValue(_colTime.VisibleIndex, vehicle.TimeStatus());
                }
            }
        }

        #endregion

        private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.CollapseAll();
        }

        private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.ExpandAll();
            TreeViewService.TreeListInitialView(_tree);
        }

        private void ttcTree_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl is DevExpress.XtraTreeList.TreeList)
            {
                TreeList tree = (TreeList) e.SelectedControl;
                TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);
                if (hit.Column == timeCol)
                {
                    if (hit.HitInfoType == HitInfoType.Cell)
                    {
                        object cellInfo = new TreeListCellToolTipInfo(hit.Node, hit.Column, null);
                        IVehicle vehicle = tree.GetDataRecordByNode(hit.Node) as IVehicle;
                        if (null != vehicle)
                        {
                            string toolTip = string.Format("{1}: {0}", vehicle.Mobitel.LastTimeGps.ToString(),
                                Resources.LastData);
                            e.Info = new DevExpress.Utils.ToolTipControlInfo(cellInfo, toolTip);
                        }

                    }
                    else if (hit.HitInfoType == HitInfoType.Column)
                    {
                        e.Info = new DevExpress.Utils.ToolTipControlInfo(hit.Column, TreeViewService.GetToolTip());
                    }
                }
                else if( hit.Column == _regNumberCol )
                {
                    object cellInfo = new TreeListCellToolTipInfo( hit.Node, hit.Column, null );
                    IVehicle vehicle = tree.GetDataRecordByNode( hit.Node ) as IVehicle;
                    if( null != vehicle )
                    {
                        string toolTip = string.Format( "{0}", vehicle.VehicleComment);
                        e.Info = new DevExpress.Utils.ToolTipControlInfo( cellInfo, toolTip );
                    }
                }
            }
        }

        bool isFlagVisible = false;

        private void _tree_VisibleChanged(object sender, EventArgs e)
        {
            if (isFlagVisible)
            {
                RefreshTree();
                RefreshLastTime();
                RefreshLastTimeInvoke();
                isFlagVisible = false;
            }
            else
            {
                isFlagVisible = true;
            }
        }
    }
}
