using System;
using System.Drawing; 
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning
{
    public partial class VehiclesView : XtraUserControl
    {
        public event ButtonClicked NewGroupClicked;
        public event ButtonClicked DeleteGroupClicked;
        public event ButtonClicked SaveAllClicked ;
        public event ButtonClicked CancelAllClicked ;
        public event ButtonClicked CategoriesClicked = delegate { };
        public event ButtonClicked CategoriesClicked2 = delegate { };
        public event ButtonClicked CategoriesClicked3 = delegate { };
        public event ButtonClicked CategoriesClicked4 = delegate { };

        public VehiclesView()
        {
            InitializeComponent();
            init();
        }

        public void ShowTree(VehiclesTree tree)
        {
            tree.Dock = DockStyle.Fill;
            tree.Parent = _treeSplitter.Panel1;
        }

        public void ShowSaveCancelButtons()
        {
            _saveAllBtn.Visibility = BarItemVisibility.Always;
            _cancelAllBtn.Visibility = BarItemVisibility.Always;
        }

        public void HideSaveCancelButtons()
        {
            _saveAllBtn.Visibility = BarItemVisibility.Never;
            _cancelAllBtn.Visibility = BarItemVisibility.Never;
        }

        public void EnableDeleteGroupButton()
        {
            _deleteGroupBtn.Enabled = true;
        }

        public void DisableDeleteGroupButton()
        {
            _deleteGroupBtn.Enabled = false;
        }

        public void ShowEditView(Control editor)
        {
            foreach (Control control in _treeSplitter.Panel2.Controls)
                control.Parent = null;
            editor.Parent = _treeSplitter.Panel2;
        }

        private void init()
        {
            _cancelAllBtn.Glyph = Shared.Cancel;
            _deleteGroupBtn.Glyph = Shared.FolderMinus;
            _addGroupBtn.Glyph = Shared.FolderPlus;
            _saveAllBtn.Glyph = Shared.Save;

            _addGroupBtn.Caption = Resources.Add;
            _addGroupBtn.Hint = Resources.AddNewGroup;

            _deleteGroupBtn.Caption = Resources.Delete;
            _deleteGroupBtn.Hint = Resources.DeleteCurrentGroup;

            _saveAllBtn.Caption = Resources.Save;
            _saveAllBtn.Hint = Resources.SaveAllChanges;

            _cancelAllBtn.Caption = Resources.Cancel;
            _cancelAllBtn.Hint = Resources.CancelAllChanges;

            bbiCategories.Caption = Resources.Categories;
            bbiCategories.Hint = Resources.CategoriesHint;
            bbiCategories.Glyph = Shared.FolderStack;

            bbiCategories2.Caption = string.Format("{0}2",  Resources.Categories);
            bbiCategories2.Hint = Resources.CategoriesHint;
            bbiCategories2.Glyph = Shared.FolderStack;

            bbiCategories3.Caption = string.Format( "{0}3", Resources.Categories );
            bbiCategories3.Hint = Resources.CategoriesHint;
            bbiCategories3.Glyph = Shared.FolderStack;

            bbiCategories4.Caption = string.Format( "{0}4", Resources.Categories );
            bbiCategories4.Hint = Resources.CategoriesHint;
            bbiCategories4.Glyph = Shared.FolderStack;
        }

        private void addGroupBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onNewGroupClicked();
        }

        private void deleteGroupBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onDeleteGroupClicked();
        }

        private void saveAllBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onSaveAllClicked();
        }

        private void cancelAllBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onCancelAllClicked();
        }

        private void onNewGroupClicked()
        {
            ButtonClicked handler = NewGroupClicked;
            if (null != handler)
                handler();
        }

        private void onDeleteGroupClicked()
        {
            ButtonClicked handler = DeleteGroupClicked;
            if (null != handler)
                handler();
        }

        private void onSaveAllClicked()
        {
            ButtonClicked handler = SaveAllClicked;
            if (null != handler)
                handler();
        }

        private void onCancelAllClicked()
        {
            ButtonClicked handler = CancelAllClicked;
            if (null != handler)
                handler();
        }

        private void bbiCategories_ItemClick(object sender, ItemClickEventArgs e)
        {
            CategoriesClicked();
        }

        private void bbiCategories2_ItemClick(object sender, ItemClickEventArgs e)
        {
            CategoriesClicked2();
        }

        private void bbiCategories3_ItemClick( object sender, ItemClickEventArgs e )
        {
            CategoriesClicked3();
        }

        private void bbiCategories4_ItemClick( object sender, ItemClickEventArgs e )
        {
            CategoriesClicked4();
        }
    }
}
