using System.Windows.Forms;
using DevExpress.XtraEditors;

using System;
using System.Collections.Generic;

using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning
{
    public partial class EditGroup : XtraUserControl
    {
        private TuningGroup _tuning;

        public event StatusChanged StatusChanged;
        public event Action<TuningGroup> SaveButtonClicked;
        public event Action<TuningGroup> CancelButtonClicked;

        public EditGroup()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _saveBtn.Image = Shared.Save;

            _attributesCat.Properties.Caption = Resources.Attributes;
            _nameRow.Properties.Caption = Resources.GroupName;
            editorFuelWayKmGrp.Properties.Caption = Resources.FuelWay;
            editorFuelMotoHrGrp.Properties.Caption = Resources.FuelMotor;
            _descriptionRow.Properties.Caption = Resources.GroupDescription;
            _summaryCat.Properties.Caption = Resources.Results;
            _ownGroupsRow.Properties.Caption = Resources.TotalSubgroups;
            _ownVehiclesRow.Properties.Caption = Resources.TotalVehicles;

            _mobitelLoginCol.Caption = Resources.Login;
            _mobitelSimNumberCol.Caption = Resources.SIM;
            _vehicleRegNumberCol.Caption = Resources.NumberPlate;
            _vehicleModelCol.Caption = Resources.BrandModel;
            _vehicleGroupCol.Caption = Resources.Group;
            _driverFullNameCol.Caption = Resources.Driver;

            _cancelBtn.Text = Resources.Cancel;
            _saveBtn.Text = Resources.Save;
            sbExcelVehicles.Image= Shared.ToXls;

            erOutLink.Properties.Caption = Resources.OuterBaseLink;

            colVehicleIdentifier.Caption = Resources.IdentifierVehicle;
            colCategory.Caption = Resources.VehCategory;
            colCategory.ToolTip = Resources.VehCategoryTip;
            colCategory2.Caption = Resources.VehCategory2;
            colCategory2.ToolTip = Resources.VehCategoryTip2;
            colCommentar.Caption = Resources.CommentVehicle;
            colCommentar.ToolTip = Resources.CommentVehicleTip;
        }

        public void BindTo(VehiclesGroup group)
        {
            if (null != _tuning)
                _tuning.StatusChanged -= tuning_StatusChanged;

            TuningGroup tuning = (TuningGroup) group.Tag;
            tuning.FuelWayKmGrp = group.FuelWayKmGrp;
            tuning.FuelMotoHrGrp = group.FuelMotoHrGrp;
            _tuning = tuning;
            _tuning.StatusChanged += tuning_StatusChanged;

            if (group.IsRoot)
                bindToRoot();
            else
                bindToGroup();

            _propertyGrid.FocusLast();
            _propertyGrid.SelectedObject = tuning;
            _propertyGrid.Refresh();

            List<VehicleRow> vehicles = new List<VehicleRow>();

            foreach (Vehicle vehicle in group.AllItems)
            {
                vehicles.Add(new VehicleRow(
                                 vehicle.Mobitel.Login,
                                 vehicle.Mobitel.SimNumber,
                                 vehicle.RegNumber,
                                 String.Format("{0} {1}", vehicle.CarMaker, vehicle.CarModel),
                                 vehicle.Group.IsRoot
                                     ? String.Format("--  {0}  --", Resources.OutsideGroups)
                                     : vehicle.Group.Name,
                                 vehicle.Driver != null ? vehicle.Driver.FullName : Resources.NotAssigned,
                                 vehicle.FuelWays, vehicle.FuelMotor, vehicle.Identifier,
                                 vehicle.Category != null ? vehicle.Category.Name : "",
                                 vehicle.Category2 != null ? vehicle.Category2.Name : "",
                                 vehicle.Category3 != null ? vehicle.Category3.Name : "",
                                 vehicle.Category4 != null ? vehicle.Category4.Name : "",
                                 vehicle.VehicleComment != null ? vehicle.VehicleComment : "") );
            }

            _vehiclesGrid.DataSource = vehicles;
            _vehiclesGrid.Refresh();
        }

        private void tuning_StatusChanged()
        {
            setButtonsVisibility();
            onStatusChanged();
        }

        private void bindToRoot()
        {
            _nameRow.OptionsRow.AllowFocus = false;
            _descriptionRow.OptionsRow.AllowFocus = false;
            hideButtons();
        }

        private void bindToGroup()
        {
            _nameRow.OptionsRow.AllowFocus = true;
            _descriptionRow.OptionsRow.AllowFocus = true;

            setButtonsVisibility();
        }

        private void setButtonsVisibility()
        {
            if (TuningStatus.Ok == _tuning.Status)
                hideButtons();
            else
                showButtons();
        }

        private void hideButtons()
        {
            _saveBtn.Visible = false;
            _cancelBtn.Visible = false;
        }

        private void showButtons()
        {
            _saveBtn.Visible = true;
            _cancelBtn.Visible = true;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onCancelButtonClicked();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            onSaveButtonClicked();
        }

        private void onStatusChanged()
        {
            StatusChanged handler = StatusChanged;
            if (null != handler)
                handler();
        }

        private void onSaveButtonClicked()
        {
            Action<TuningGroup> handler = SaveButtonClicked;
            if (null != handler)
                handler(_tuning);
        }

        private void onCancelButtonClicked()
        {
            Action<TuningGroup> handler = CancelButtonClicked;
            if (null != handler)
                handler(_tuning);
        }

        #region --   Nested Classes   --

        private class VehicleRow
        {
            private string _login;
            private string _simNumber;
            private string _regNumber;
            private string _vehicleModel;
            private string _group;
            private string _driver;
            private string fuelWay;
            private string fuelMotor;
            private int vehIdentifier;
            private string vehCategory;
            private string vehCategory2;
            private string vehCategory3;
            private string vehCategory4;
            private string vehComment;
            
            public VehicleRow(string login, string simNumber, string regNumber, string vehicleModel, string group,
                              string driver, string fuelWay, string fuelMotor, int _identif, string veh_category, string veh_category2, string veh_category3, string veh_category4, string vehcommnt )
            {
                _login = login;
                _simNumber = simNumber;
                _regNumber = regNumber;
                _vehicleModel = vehicleModel;
                _group = group;
                _driver = driver;
                this.fuelWay = fuelWay;
                this.fuelMotor = fuelMotor;
                this.vehIdentifier = _identif;
                this.vehCategory = veh_category;
                this.vehCategory2 = veh_category2;
                this.vehCategory3 = veh_category3;
                this.vehCategory4 = veh_category4;
                this.vehComment = vehcommnt;
            }

            public string Login
            {
                get { return _login; }
            }

            public string SimNumber
            {
                get { return _simNumber; }
            }

            public string RegNumber
            {
                get { return _regNumber; }
            }

            public string VehicleModel
            {
                get { return _vehicleModel; }
            }

            public string Group
            {
                get { return _group; }
            }

            public string Driver
            {
                get { return _driver; }
            }

            public string FuelWayLiter
            {
                get { return this.fuelWay; }
            }

            public string FuelMotorLiter
            {
                get { return this.fuelMotor; }
            }

            public int Identifier
            {
                get { return this.vehIdentifier; }
            }

            public string VehicleCategory
            {
                get { return this.vehCategory; }
            }

            public string VehicleCategory2
            {
                get { return this.vehCategory2; }
            }

            public string VehicleCategory3
            {
                get { return this.vehCategory3; }
            }

            public string VehicleCategory4
            {
                get { return this.vehCategory4; }
            }

            public string VehicleComment
            {
                get { return this.vehComment; }
            }
        }

        #endregion

        private void sbExcelVehicles_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ExcelOutputConfim, Resources.Transport, MessageBoxButtons.YesNo)) return;
            XtraGridService.ExportToExcel(_vehiclesView, Resources.Transport);
        }
    }
}
