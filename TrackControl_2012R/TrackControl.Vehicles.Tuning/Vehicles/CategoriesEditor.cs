using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General.Properties;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Vehicles.Tuning.Properties;


namespace TrackControl.Vehicles.Tuning
{
    public partial class CategoriesEditor : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshCategoriesList;
        bool _categoriesChanged;

        public CategoriesEditor()
        {
            InitializeComponent();
            Localization();
            gcCateg.DataSource = VehicleCategoryProvader.GetList();
        }



        private bool DeleteRowVehicleCategory()
        {
            VehicleCategory vc = (VehicleCategory) gvCateg.GetRow(gvCateg.FocusedRowHandle);

            if (vc != null)
            {
                int vehicles = VehicleCategoryProvader.CountVehiclesWithCategory(vc);

                if (vehicles > 0)
                {
                    XtraMessageBox.Show(Resources.DeleteBan, string.Format(Resources.VehiclesWithCategory, vehicles));
                    return false;
                }

                if (XtraMessageBox.Show(Resources.ConfirmDeleteQuestion,
                    Resources.ConfirmDeletion,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return vc.Delete();
                }
                else
                    return false;
            }
            else
                return false;
        }



        void Localization()
        {
            this.Text = Resources.EditorVehiclesCategory;
            colName.Caption = Resources.Category;
            colTonnage.Caption = Resources.CapacityTone;
            colFuelNormMoving.Caption = Resources.FuelNormMoving;
            colFuelNormParking.Caption = Resources.FuelNormParking; 
        }

        private void gvCateg_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            VehicleCategory vc = (VehicleCategory)gvCateg.GetRow(e.RowHandle);
            if (vc != null)
            {
                if (vc.Save()) _categoriesChanged = true;
            }

        }

        private void gcCateg_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowVehicleCategory();
                _categoriesChanged = !e.Handled;
            }
        }

        private void CategoriesEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_categoriesChanged && RefreshCategoriesList != null)
                RefreshCategoriesList();
        }

        private void gvCateg_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

    }
}