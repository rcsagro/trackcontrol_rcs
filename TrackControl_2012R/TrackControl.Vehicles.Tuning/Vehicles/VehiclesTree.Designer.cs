namespace TrackControl.Vehicles.Tuning
{
  partial class VehiclesTree
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }

      disposePopupItems();

      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehiclesTree));
            this._barManager = new DevExpress.XtraBars.BarManager();
            this._tools = new DevExpress.XtraBars.Bar();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this._funnelBox = new DevExpress.XtraBars.BarEditItem();
            this._funnelRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._regNumberCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._modelCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.timeCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._statusRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.timeImages = new DevExpress.Utils.ImageCollection();
            this._loginCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._images = new DevExpress.Utils.ImageCollection();
            this.ttcTree = new DevExpress.Utils.ToolTipController();
            this._popup = new DevExpress.XtraBars.PopupMenu();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._funnelBox,
            this._eraseBtn,
            this.bbiExpand,
            this.bbiCollapce});
            this._barManager.MaxItemId = 4;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._funnelRepo});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCollapce),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._funnelBox, "", false, true, true, 129),
            new DevExpress.XtraBars.LinkPersistInfo(this._eraseBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // bbiExpand
            // 
            this.bbiExpand.Caption = "Expand";
            this.bbiExpand.Id = 2;
            this.bbiExpand.Name = "bbiExpand";
            this.bbiExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExpand_ItemClick);
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Caption = "Collapce";
            this.bbiCollapce.Id = 3;
            this.bbiCollapce.Name = "bbiCollapce";
            this.bbiCollapce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCollapce_ItemClick);
            // 
            // _funnelBox
            // 
            this._funnelBox.Caption = "������";
            this._funnelBox.Edit = this._funnelRepo;
            this._funnelBox.Id = 0;
            this._funnelBox.Name = "_funnelBox";
            this._funnelBox.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _funnelRepo
            // 
            this._funnelRepo.AutoHeight = false;
            this._funnelRepo.MaxLength = 20;
            this._funnelRepo.Name = "_funnelRepo";
            this._funnelRepo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.funnelRepo_KeyUp);
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Caption = "��������";
            this._eraseBtn.Hint = "�������� ������";
            this._eraseBtn.Id = 1;
            this._eraseBtn.Name = "_eraseBtn";
            this._eraseBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eraseBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(442, 29);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 533);
            this.barDockControlBottom.Size = new System.Drawing.Size(442, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 29);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 504);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(442, 29);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 504);
            // 
            // _tree
            // 
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._regNumberCol,
            this._modelCol,
            this.timeCol,
            this._loginCol});
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.KeyFieldName = "Id";
            this._tree.Location = new System.Drawing.Point(0, 29);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.EnableFiltering = true;
            this._tree.OptionsMenu.EnableColumnMenu = false;
            this._tree.OptionsMenu.EnableFooterMenu = false;
            this._tree.OptionsView.ShowIndicator = false;
            this._tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._statusRepo});
            this._tree.SelectImageList = this._images;
            this._tree.Size = new System.Drawing.Size(442, 504);
            this._tree.TabIndex = 4;
            this._tree.ToolTipController = this.ttcTree;
            this._tree.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterFocusNode);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            this._tree.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.tree_FilterNode);
            this._tree.VisibleChanged += new System.EventHandler(this._tree_VisibleChanged);
            this._tree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tree_MouseUp);
            // 
            // _regNumberCol
            // 
            this._regNumberCol.Caption = "�����";
            this._regNumberCol.FieldName = "RegNumber";
            this._regNumberCol.MinWidth = 150;
            this._regNumberCol.Name = "_regNumberCol";
            this._regNumberCol.OptionsColumn.AllowEdit = false;
            this._regNumberCol.OptionsColumn.AllowFocus = false;
            this._regNumberCol.OptionsColumn.AllowMove = false;
            this._regNumberCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._regNumberCol.OptionsColumn.ShowInCustomizationForm = false;
            this._regNumberCol.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this._regNumberCol.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this._regNumberCol.Visible = true;
            this._regNumberCol.VisibleIndex = 0;
            this._regNumberCol.Width = 167;
            // 
            // _modelCol
            // 
            this._modelCol.Caption = "�����, ������";
            this._modelCol.FieldName = "Model";
            this._modelCol.MinWidth = 100;
            this._modelCol.Name = "_modelCol";
            this._modelCol.OptionsColumn.AllowEdit = false;
            this._modelCol.OptionsColumn.AllowFocus = false;
            this._modelCol.OptionsColumn.AllowMove = false;
            this._modelCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._modelCol.OptionsColumn.ShowInCustomizationForm = false;
            this._modelCol.Visible = true;
            this._modelCol.VisibleIndex = 1;
            this._modelCol.Width = 100;
            // 
            // timeCol
            // 
            this.timeCol.ColumnEdit = this._statusRepo;
            this.timeCol.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.timeCol.Name = "timeCol";
            this.timeCol.OptionsColumn.AllowEdit = false;
            this.timeCol.OptionsColumn.AllowFocus = false;
            this.timeCol.OptionsColumn.AllowMove = false;
            this.timeCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.timeCol.OptionsColumn.AllowSize = false;
            this.timeCol.OptionsColumn.FixedWidth = true;
            this.timeCol.OptionsColumn.ReadOnly = true;
            this.timeCol.OptionsColumn.ShowInCustomizationForm = false;
            this.timeCol.Visible = true;
            this.timeCol.VisibleIndex = 2;
            this.timeCol.Width = 20;
            // 
            // _statusRepo
            // 
            this._statusRepo.AutoHeight = false;
            this._statusRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._statusRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3)});
            this._statusRepo.Name = "_statusRepo";
            this._statusRepo.SmallImages = this.timeImages;
            // 
            // timeImages
            // 
            this.timeImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("timeImages.ImageStream")));
            this.timeImages.Images.SetKeyName(0, "status.png");
            this.timeImages.Images.SetKeyName(1, "status-away.png");
            this.timeImages.Images.SetKeyName(2, "status-busy.png");
            this.timeImages.Images.SetKeyName(3, "status-offline.png");
            // 
            // _loginCol
            // 
            this._loginCol.Caption = "�����";
            this._loginCol.FieldName = "Login";
            this._loginCol.MinWidth = 40;
            this._loginCol.Name = "_loginCol";
            this._loginCol.OptionsColumn.AllowEdit = false;
            this._loginCol.OptionsColumn.AllowFocus = false;
            this._loginCol.OptionsColumn.AllowMove = false;
            this._loginCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._loginCol.OptionsColumn.AllowSize = false;
            this._loginCol.OptionsColumn.FixedWidth = true;
            this._loginCol.OptionsColumn.ShowInCustomizationForm = false;
            this._loginCol.Visible = true;
            this._loginCol.VisibleIndex = 3;
            this._loginCol.Width = 40;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            // 
            // ttcTree
            // 
            this.ttcTree.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcTree_GetActiveObjectInfo);
            // 
            // _popup
            // 
            this._popup.Manager = this._barManager;
            this._popup.Name = "_popup";
            // 
            // VehiclesTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "VehiclesTree";
            this.Size = new System.Drawing.Size(442, 533);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraTreeList.TreeList _tree;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _regNumberCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _modelCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _loginCol;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraTreeList.Columns.TreeListColumn timeCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _statusRepo;
    private DevExpress.XtraBars.PopupMenu _popup;
    private DevExpress.XtraBars.BarEditItem _funnelBox;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _funnelRepo;
    private DevExpress.XtraBars.BarButtonItem _eraseBtn;
    private DevExpress.XtraBars.BarButtonItem bbiExpand;
    private DevExpress.XtraBars.BarButtonItem bbiCollapce;
    private DevExpress.Utils.ImageCollection timeImages;
    private DevExpress.Utils.ToolTipController ttcTree;
  }
}
