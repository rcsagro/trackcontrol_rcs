using DevExpress.XtraEditors;
using DevExpress.Utils;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using TrackControl.General;
using TrackControl.Vehicles.DAL;
using TrackControl.Vehicles.Tuning.Properties;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace TrackControl.Vehicles.Tuning
{
    public partial class EditVehicle : XtraUserControl
    {
        private VehiclesModel _model;
        private TuningVehicle _tuning;
        private LabelControl _noDriver;
        private MemoEdit editComment = null;
        
        public event StatusChanged StatusChanged;
        public event Action<TuningVehicle> SaveButtonClicked;
        public event Action<TuningVehicle> CancelButtonClicked;

        public EditVehicle(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model");

            _model = model;
            InitializeComponent();
            _saveBtn.Image = Shared.Save;
            init();
            this.vehMemoEdit.EditValueChanged += vehMemoEdit_EditValueChanged;
            this.cbeCategory3.EditValueChanged += cbeCategory3_EditValueChanged;
            this.cbeCategory4.EditValueChanged += cbeCategory4_EditValueChanged;
        }

        void cbeCategory4_EditValueChanged( object sender, EventArgs e )
        {
            _tuning.Category4 = ( VehicleCategory4 )_propertyGrid.EditingValue;
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        void cbeCategory3_EditValueChanged( object sender, EventArgs e )
        {
            _tuning.Category3 = ( VehicleCategory3 )_propertyGrid.EditingValue;
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        void vehMemoEdit_EditValueChanged( object sender, EventArgs e )
        {
            editComment = (MemoEdit) sender;

            _tuning.VehicleComment = ( string )_propertyGrid.EditingValue;

            if (_tuning.PreviouseComment == _tuning.VehicleComment)
            {
                _tuning.CommnetStatus = TuningStatus.Ok;
            }
            else
            {
                _tuning.CommnetStatus = TuningStatus.NotSaved;
            }

            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        public void BindTo(Vehicle vehicle)
        {
            if (null != _tuning)
            {
                _tuning.StatusChanged -= tuning_StatusChanged;
            }

            TuningVehicle tuning = (TuningVehicle) vehicle.Tag;
            _tuning = tuning;
            _tuning.Status = TuningStatus.Ok;
            
            _tuning.StatusChanged += tuning_StatusChanged;

            _tuning.FuelMotorLiter = vehicle.FuelMotor;
            _tuning.FuelWayLiter = vehicle.FuelWays;
            _tuning.MobitelIs64Packets = vehicle.Mobitel.Is64BitPackets;
            _tuning.MobitelIsNotDrawDgps = vehicle.Mobitel.IsNotDrawDgps;
            _tuning.VehicleComment = vehicle.VehicleComment;
            _tuning.PreviouseComment = vehicle.VehicleComment;

            _mobitelCat.Properties.Caption = String.Format("{0} (Id: {1})", Resources.Teletrack, vehicle.Mobitel.Id);

            _vehicleCat.Properties.Caption = vehicle.IsNew
                                                 ? Resources.CarNotSaved
                                                 : String.Format("{0} (Id: {1})", Resources.Car, vehicle.Id);

            _groupRepo.EditValueChanged -= groupRepo_EditValueChanged;
            _groupRepo.Items.Clear();
            _groupRepo.Items.Add(_model.Root);

            foreach (VehiclesGroup group in _model.Root.AllGroups)
                _groupRepo.Items.Add(group);

            _groupRepo.EditValueChanged += groupRepo_EditValueChanged;

            _driverRepo.EditValueChanged -= driverRepo_EditValueChanged;
            _driverRepo.Items.Clear();
            _driverRepo.Items.Add(Driver.Empty);

            foreach (Driver driver in _model.Drivers)
                _driverRepo.Items.Add(driver);

            _driverRepo.EditValueChanged += driverRepo_EditValueChanged;

            // ��������� ������ �� ������� ��������� ���� �������� (��� ������)
            pcbItemComboBox.EditValueChanged -= pcbItemComboBox_EditValueChanged;
            pcbItemComboBox.Items.Clear();
            pcbItemComboBox.Items.Add( PcbVersions.Empty );
            
            foreach (PcbVersions pcbver in _model.PcbVersion)
            {
                pcbItemComboBox.Items.Add(pcbver);
            }

            pcbItemComboBox.EditValueChanged += pcbItemComboBox_EditValueChanged;
            
            setButtonsVisibility();
            _propertyGrid.SelectedObject = _tuning;
            _propertyGrid.Refresh();

            bindDriver(_tuning.Driver);

            RefreshCategoryList();
            RefreshCategory2List();
            RefreshCategory3List();
            RefreshCategory4List();
            RefreshCommentary();
        }

        private void init()
        {
            _noDriver = new LabelControl();
            _noDriver.Dock = DockStyle.Fill;
            _noDriver.AllowHtmlString = true;
            _noDriver.Appearance.Image = Shared.NoUser;
            _noDriver.Appearance.Options.UseImage = true;
            _noDriver.Appearance.Options.UseTextOptions = true;
            _noDriver.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
            _noDriver.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            _noDriver.AutoSizeMode = LabelAutoSizeMode.None;
            _noDriver.ImageAlignToText = ImageAlignToText.TopCenter;
            _noDriver.Padding = new Padding(20);
            _noDriver.Text = Resources.DriverNotAssigned;

            _mobitelCat.Properties.Caption = Resources.Teletrack;
            _loginRow.Properties.Caption = Resources.Login;
            _simNumberRow.Properties.Caption = Resources.SIM;
            _vehicleCat.Properties.Caption = Resources.Transport;
            _regNumberRow.Properties.Caption = Resources.NumberPlate;
            _carMakerRow.Properties.Caption = Resources.Brand;
            _carModelRow.Properties.Caption = Resources.Model;
            _groupRow.Properties.Caption = Resources.Group;
            _driverRow.Properties.Caption = Resources.Driver;
            _summaryCat.Properties.Caption = Resources.Results;
            _sensorsRow.Properties.Caption = Resources.TotalSensors;
            _logicSensorsRow.Properties.Caption = Resources.Logical;
            _gageSensorsRow.Properties.Caption = Resources.Measuring;
            editorFuelWay.Properties.Caption = Resources.FuelWay;
            editorFuelMotor.Properties.Caption = Resources.FuelMotor;
            editorIdentif.Properties.Caption = Resources.IdentifierVehicle;

            _driverFirstNameCol.Caption = Resources.FirstName;
            _driverLastNameCol.Caption = Resources.LastName;
            _driverDayOfBirthCol.Caption = Resources.DateOfBirthShort;
            _driverPhotoCol.Caption = Resources.Photo;
            _driverLicenseCol.Caption = Resources.DrivingLicense;
            _driverCategories.Caption = Resources.Categories;
            _driverIdentifierCol.Caption = Resources.ID;

            Group3.CustomizationFormText = Resources.Driver;
            Group3.Text = Resources.Driver;
            Group4.CustomizationFormText = Resources.Info;
            Group4.Text = Resources.Info;

            _cancelBtn.Text = Resources.Cancel;
            _saveBtn.Text = Resources.Save;

            erOutLink.Properties.Caption = Resources.OuterBaseLink;
            erCategory.Properties.Caption = Resources.Category;
            erCategory2.Properties.Caption = string.Format("{0}2",Resources.Category);
            erPacket64.Properties.Caption = Resources.Packet64;
            erIsNotDrawDgps.Properties.Caption = Resources.IsNotDrawDgps;
            vehComment.Properties.Caption = Resources.CommentVehicle;
            vehComment.Properties.ToolTip = Resources.CommentVehicleTip;
        }
        
        private void tuning_StatusChanged()
        {
            setButtonsVisibility();
            onStatusChanged();
        }
        
        private void groupRepo_EditValueChanged(object sender, EventArgs e)
        {
            _tuning.Group = (VehiclesGroup) _propertyGrid.EditingValue;
        }

        private void driverRepo_EditValueChanged(object sender, EventArgs e)
        {
            Driver driver = (Driver) _propertyGrid.EditingValue;
            bindDriver(driver);
            _tuning.Driver = driver;
        }

        private void pcbItemComboBox_EditValueChanged(object sender, EventArgs e)
        {
            PcbVersions pcbvers = ( PcbVersions )_propertyGrid.EditingValue;
            _tuning.PcbVersus = pcbvers;
            bindPcbVers( pcbvers );
        }

        private void setButtonsVisibility()
        {
            if (TuningStatus.Ok == _tuning.Status)
                hideButtons();
            else
                showButtons();
        }

        private void hideButtons()
        {
            _saveBtn.Visible = false;
            _cancelBtn.Visible = false;
        }

        private void showButtons()
        {
            _saveBtn.Visible = true;
            _cancelBtn.Visible = true;
        }

        private void onStatusChanged()
        {
            StatusChanged handler = StatusChanged;
            if (null != handler)
                handler();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (_tuning.CommnetStatus == TuningStatus.NotSaved)
            {
                if (_tuning.PreviouseComment == _tuning.VehicleComment)
                {
                    _tuning.CommnetStatus = TuningStatus.Ok;
                }
                else
                {
                    if (VehicleCommentProvider.GetComment(_tuning.MobitelId) == "")
                    {
                        VehicleCommentProvider.InsertComment(_tuning.MobitelId, _tuning.VehicleComment);
                    }
                    else
                    {
                        VehicleCommentProvider.UpdateComment(_tuning.MobitelId, _tuning.VehicleComment);
                    }

                    _tuning.PreviouseComment = _tuning.VehicleComment;
                    _tuning.CommnetStatus = TuningStatus.Ok;
                }
            }

            Action<TuningVehicle> handler = SaveButtonClicked;

            if (null != handler)
                handler(_tuning);

            _tuning.Status = TuningStatus.Ok;
            tuning_StatusChanged();

            // ����� ��������� ������ ����� � ��������
            RefreshTree.refreshReport();
            // ����� ��������� ������ ����� � ��������
            if (RefreshTree.refreshOnline != null)
                RefreshTree.refreshOnline();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if(editComment != null)
                editComment.EditValue = _tuning.PreviouseComment;
            _tuning.VehicleComment = _tuning.PreviouseComment;
            _tuning.CommnetStatus = TuningStatus.Ok;

            Action<TuningVehicle> handler = CancelButtonClicked;
            if (null != handler)
                handler(_tuning);
        }

        private void bindPcbVers(PcbVersions pcbver)
        {
           // to do
        }

        private void bindDriver(Driver driver)
        {
            _driverGrid.Parent = null;
            _noDriver.Parent = null;

            if (Driver.Empty.Id != driver.Id)
            {
                _driverGrid.DataSource = new Driver[] {driver};
                _mainTable.Controls.Add(_driverGrid, 3, 0);
                _driverGrid.Refresh();
            }
            else
            {
                _driverGrid.DataSource = null;
                _mainTable.Controls.Add(_noDriver, 3, 0);
            }
        }

        private void rteOutLink_EditValueChanged(object sender, EventArgs e)
        {
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        private void rleCategory_EditValueChanged(object sender, EventArgs e)
        {
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        public void RefreshCategoryList()
        {
            try
            {
                cbeCategory.Items.Clear();
                foreach (VehicleCategory vCat in VehicleCategoryProvader.GetList())
                    cbeCategory.Items.Add(vCat);

                VehicleCategory emptyCategory = new VehicleCategory();
                emptyCategory.Name = " ";
                emptyCategory.Id = -1;
                cbeCategory.Items.Add(emptyCategory);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.Message, "Error edit vehicle", MessageBoxButtons.OK );
            }
        }

        public void RefreshCategory2List()
        {
            try
            {
                cbeCategory2.Items.Clear();
                foreach (VehicleCategory2 vCat in VehicleCategoryProvader.GetList2())
                    cbeCategory2.Items.Add(vCat);

                VehicleCategory2 emptyCategory2 = new VehicleCategory2();
                emptyCategory2.Name = " ";
                emptyCategory2.Id = -1;
                cbeCategory2.Items.Add(emptyCategory2);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.Message, "Error edit vehicle", MessageBoxButtons.OK );
            }
        }

        public void RefreshCategory3List()
        {
            try
            {
                cbeCategory3.Items.Clear();
                foreach( VehicleCategory3 vCat in VehicleCategoryProvader.GetList3() )
                    cbeCategory3.Items.Add( vCat );

                VehicleCategory3 emptyCategory3 = new VehicleCategory3();
                emptyCategory3.Name = " ";
                emptyCategory3.Id = -1;
                cbeCategory3.Items.Add( emptyCategory3 );
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message, "Error edit vehicle", MessageBoxButtons.OK );
            }
        }

        public void RefreshCategory4List()
        {
            try
            {
                cbeCategory4.Items.Clear();

                foreach( VehicleCategory4 vCat in VehicleCategoryProvader.GetList4() )
                    cbeCategory4.Items.Add( vCat );

                VehicleCategory4 emptyCategory4 = new VehicleCategory4();
                emptyCategory4.Name = " ";
                emptyCategory4.Id = -1;
                cbeCategory4.Items.Add(emptyCategory4);
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show(ex.Message, "Error edit vehicle", MessageBoxButtons.OK);
            }
        }

        public void RefreshCommentary()
        {
            try
            {
                if(editComment == null)
                    return;
                
                editComment.EditValue = VehicleCommentProvider.GetComment(_tuning.MobitelId);
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message, "Error edit vehicle", MessageBoxButtons.OK );
            }
        }

        private void cbeCategory_EditValueChanged(object sender, EventArgs e)
        {
            _tuning.Category = (VehicleCategory)_propertyGrid.EditingValue;
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }

        private void cbeCategory2_EditValueChanged(object sender, EventArgs e)
        {
            _tuning.Category2 = (VehicleCategory2)_propertyGrid.EditingValue;
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }
    }
}
