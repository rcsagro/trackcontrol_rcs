﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning.Vehicles
{
    public partial class CategoriesEditor3 : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshCategoriesList;
        bool _categoriesChanged;

        public CategoriesEditor3()
        {
            InitializeComponent();
            Localization();
            gcCategory3.DataSource = VehicleCategoryProvader.GetList3();
            this.gvCategory3.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( this.gvCategory3_CellValueChanged );
            this.gcCategory3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler( this.gcCategory3_EmbeddedNavigator_ButtonClick );
            this.gvCategory3.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler( this.gvCategory3_CustomDrawRowIndicator );
        }

        bool DeleteRowVehicleCategory()
        {
            VehicleCategory3 vc = (VehicleCategory3)gvCategory3.GetRow(gvCategory3.FocusedRowHandle);

            if (vc != null)
            {
                int vehicles = VehicleCategoryProvader.CountVehiclesWithCategory3(vc);

                if (vehicles > 0)
                {
                    XtraMessageBox.Show(Resources.DeleteBan, string.Format(Resources.VehiclesWithCategory, vehicles));
                    return false;
                }

                if (XtraMessageBox.Show(Resources.ConfirmDeleteQuestion, Resources.ConfirmDeletion,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return vc.Delete();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        void Localization()
        {
            this.Text =string.Format("{0}3", Resources.EditorVehiclesCategory);
            colName.Caption = string.Format("{0}3", Resources.Category); 
        }

        private void gvCategory3_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            VehicleCategory3 vc = (VehicleCategory3)gvCategory3.GetRow(e.RowHandle);

            if (vc != null)
            {
                if (vc.Save()) _categoriesChanged = true;
            }
        }

        private void gcCategory3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowVehicleCategory();
                _categoriesChanged = !e.Handled;
            }
        }

        private void CategoriesEditor3_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_categoriesChanged && RefreshCategoriesList != null)
                RefreshCategoriesList();
        }

        private void gvCategory3_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) 
                    e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
    }
}