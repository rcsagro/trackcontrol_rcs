﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning.Vehicles
{
    public partial class CategoriesEditor4 : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshCategoriesList;
        bool _categoriesChanged;

        public CategoriesEditor4()
        {
            InitializeComponent();
            Localization();
            gcCategory4.DataSource = VehicleCategoryProvader.GetList4();
            this.gvCategory4.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( this.gvCategory4_CellValueChanged );
            this.gcCategory4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler( this.gcCategory4_EmbeddedNavigator_ButtonClick );
            this.gvCategory4.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler( this.gvCategory4_CustomDrawRowIndicator );
        }

        private void gvCategory4_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            VehicleCategory4 vc = ( VehicleCategory4 )gvCategory4.GetRow( e.RowHandle );

            if( vc != null )
            {
                if( vc.Save() ) _categoriesChanged = true;
            }
        }

        private void gcCategory4_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            if( e.Button.ButtonType == NavigatorButtonType.Remove )
            {
                e.Handled = !DeleteRowVehicleCategory();
                _categoriesChanged = !e.Handled;
            }
        }

        private void gvCategory4_CustomDrawRowIndicator( object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e )
        {
            if( e.Info.IsRowIndicator )
            {
                if( e.RowHandle >= 0 )
                    e.Info.DisplayText = ( e.RowHandle + 1 ).ToString();
            }
        }

        private void CategoriesEditor4_FormClosed( object sender, FormClosedEventArgs e )
        {
            if( _categoriesChanged && RefreshCategoriesList != null )
                RefreshCategoriesList();
        }

        void Localization()
        {
            this.Text = string.Format( "{0}4", Resources.EditorVehiclesCategory );
            colName.Caption = string.Format( "{0}4", Resources.Category );
        }

        bool DeleteRowVehicleCategory()
        {
            VehicleCategory4 vc = ( VehicleCategory4 )gvCategory4.GetRow( gvCategory4.FocusedRowHandle );

            if( vc != null )
            {
                int vehicles = VehicleCategoryProvader.CountVehiclesWithCategory4( vc );

                if( vehicles > 0 )
                {
                    XtraMessageBox.Show( Resources.DeleteBan, string.Format( Resources.VehiclesWithCategory, vehicles ) );
                    return false;
                }

                if( XtraMessageBox.Show( Resources.ConfirmDeleteQuestion, Resources.ConfirmDeletion,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question ) == DialogResult.Yes )
                {

                    return vc.Delete();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}