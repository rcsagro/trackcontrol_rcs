namespace TrackControl.Vehicles.Tuning
{
    partial class CategoriesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcCateg = new DevExpress.XtraGrid.GridControl();
            this.gvCateg = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTonnage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelNormMoving = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelNormParking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcCateg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCateg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcCateg
            // 
            this.gcCateg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCateg.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcCateg_EmbeddedNavigator_ButtonClick);
            this.gcCateg.Location = new System.Drawing.Point(0, 0);
            this.gcCateg.MainView = this.gvCateg;
            this.gcCateg.Name = "gcCateg";
            this.gcCateg.Size = new System.Drawing.Size(616, 617);
            this.gcCateg.TabIndex = 0;
            this.gcCateg.UseEmbeddedNavigator = true;
            this.gcCateg.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCateg,
            this.gridView2});
            // 
            // gvCateg
            // 
            this.gvCateg.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCateg.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.Empty.Options.UseBackColor = true;
            this.gvCateg.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCateg.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCateg.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCateg.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCateg.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCateg.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCateg.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCateg.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCateg.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCateg.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCateg.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCateg.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCateg.Appearance.Preview.Options.UseBackColor = true;
            this.gvCateg.Appearance.Preview.Options.UseFont = true;
            this.gvCateg.Appearance.Preview.Options.UseForeColor = true;
            this.gvCateg.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.Row.Options.UseBackColor = true;
            this.gvCateg.Appearance.Row.Options.UseForeColor = true;
            this.gvCateg.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCateg.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCateg.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCateg.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCateg.ColumnPanelRowHeight = 40;
            this.gvCateg.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName,
            this.colTonnage,
            this.colFuelNormMoving,
            this.colFuelNormParking});
            this.gvCateg.GridControl = this.gcCateg;
            this.gvCateg.IndicatorWidth = 30;
            this.gvCateg.Name = "gvCateg";
            this.gvCateg.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCateg.OptionsView.EnableAppearanceOddRow = true;
            this.gvCateg.OptionsView.ShowGroupPanel = false;
            this.gvCateg.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvCateg_CustomDrawRowIndicator);
            this.gvCateg.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvCateg_CellValueChanged);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "���������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 340;
            // 
            // colTonnage
            // 
            this.colTonnage.AppearanceCell.Options.UseTextOptions = true;
            this.colTonnage.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTonnage.AppearanceHeader.Options.UseTextOptions = true;
            this.colTonnage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTonnage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTonnage.Caption = "����������������, ����";
            this.colTonnage.FieldName = "Tonnage";
            this.colTonnage.Name = "colTonnage";
            this.colTonnage.Visible = true;
            this.colTonnage.VisibleIndex = 1;
            this.colTonnage.Width = 256;
            // 
            // colFuelNormMoving
            // 
            this.colFuelNormMoving.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelNormMoving.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelNormMoving.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelNormMoving.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelNormMoving.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelNormMoving.Caption = "����� ������� ������� � �������� �/100 ��";
            this.colFuelNormMoving.FieldName = "FuelNormMoving";
            this.colFuelNormMoving.Name = "colFuelNormMoving";
            this.colFuelNormMoving.Visible = true;
            this.colFuelNormMoving.VisibleIndex = 2;
            this.colFuelNormMoving.Width = 311;
            // 
            // colFuelNormParking
            // 
            this.colFuelNormParking.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelNormParking.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelNormParking.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelNormParking.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelNormParking.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelNormParking.Caption = "����� ������� ������� �� �������� �������� �/���";
            this.colFuelNormParking.FieldName = "FuelNormParking";
            this.colFuelNormParking.Name = "colFuelNormParking";
            this.colFuelNormParking.Visible = true;
            this.colFuelNormParking.VisibleIndex = 3;
            this.colFuelNormParking.Width = 330;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcCateg;
            this.gridView2.Name = "gridView2";
            // 
            // CategoriesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 617);
            this.Controls.Add(this.gcCateg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CategoriesEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� ��������� ����������";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CategoriesEditor_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcCateg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCateg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcCateg;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCateg;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colTonnage;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelNormMoving;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelNormParking;
    }
}