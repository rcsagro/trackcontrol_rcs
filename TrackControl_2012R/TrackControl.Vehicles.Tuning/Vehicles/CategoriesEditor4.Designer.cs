﻿namespace TrackControl.Vehicles.Tuning.Vehicles
{
    partial class CategoriesEditor4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcCategory4 = new DevExpress.XtraGrid.GridControl();
            this.gvCategory4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcCategory4
            // 
            this.gcCategory4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCategory4.Location = new System.Drawing.Point(0, 0);
            this.gcCategory4.MainView = this.gvCategory4;
            this.gcCategory4.Name = "gcCategory4";
            this.gcCategory4.Size = new System.Drawing.Size(727, 403);
            this.gcCategory4.TabIndex = 1;
            this.gcCategory4.UseEmbeddedNavigator = true;
            this.gcCategory4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCategory4,
            this.gridView2});
            // 
            // gvCategory4
            // 
            this.gvCategory4.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCategory4.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCategory4.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory4.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory4.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCategory4.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCategory4.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCategory4.Appearance.Empty.Options.UseBackColor = true;
            this.gvCategory4.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory4.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory4.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory4.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory4.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCategory4.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCategory4.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCategory4.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCategory4.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCategory4.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCategory4.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCategory4.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCategory4.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCategory4.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCategory4.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory4.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory4.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCategory4.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCategory4.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCategory4.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCategory4.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCategory4.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCategory4.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCategory4.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCategory4.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCategory4.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory4.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory4.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCategory4.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory4.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory4.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCategory4.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCategory4.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCategory4.Appearance.Preview.Options.UseBackColor = true;
            this.gvCategory4.Appearance.Preview.Options.UseFont = true;
            this.gvCategory4.Appearance.Preview.Options.UseForeColor = true;
            this.gvCategory4.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory4.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.Row.Options.UseBackColor = true;
            this.gvCategory4.Appearance.Row.Options.UseForeColor = true;
            this.gvCategory4.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory4.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCategory4.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCategory4.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCategory4.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory4.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory4.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCategory4.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCategory4.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCategory4.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCategory4.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory4.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCategory4.ColumnPanelRowHeight = 40;
            this.gvCategory4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName});
            this.gvCategory4.GridControl = this.gcCategory4;
            this.gvCategory4.IndicatorWidth = 30;
            this.gvCategory4.Name = "gvCategory4";
            this.gvCategory4.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCategory4.OptionsView.EnableAppearanceOddRow = true;
            this.gvCategory4.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Категория4";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 340;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcCategory4;
            this.gridView2.Name = "gridView2";
            // 
            // CategoriesEditor4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 403);
            this.Controls.Add(this.gcCategory4);
            this.Name = "CategoriesEditor4";
            this.Text = "CategoriesEditor4";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CategoriesEditor4_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcCategory4;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCategory4;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    }
}