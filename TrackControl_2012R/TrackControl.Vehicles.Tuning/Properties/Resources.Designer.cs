﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.34209
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TrackControl.Vehicles.Tuning.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TrackControl.Vehicles.Tuning.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Add.
        /// </summary>
        internal static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Add new group.
        /// </summary>
        internal static string AddNewGroup {
            get {
                return ResourceManager.GetString("AddNewGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Attributes.
        /// </summary>
        internal static string Attributes {
            get {
                return ResourceManager.GetString("Attributes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Brand.
        /// </summary>
        internal static string Brand {
            get {
                return ResourceManager.GetString("Brand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Brand, model.
        /// </summary>
        internal static string BrandModel {
            get {
                return ResourceManager.GetString("BrandModel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Cancel.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Cancel all changes.
        /// </summary>
        internal static string CancelAllChanges {
            get {
                return ResourceManager.GetString("CancelAllChanges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Capacity, tons.
        /// </summary>
        internal static string CapacityTone {
            get {
                return ResourceManager.GetString("CapacityTone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicle.
        /// </summary>
        internal static string Car {
            get {
                return ResourceManager.GetString("Car", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Card view.
        /// </summary>
        internal static string CardView {
            get {
                return ResourceManager.GetString("CardView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicle not saved.
        /// </summary>
        internal static string CarNotSaved {
            get {
                return ResourceManager.GetString("CarNotSaved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Carousel view.
        /// </summary>
        internal static string Carousel {
            get {
                return ResourceManager.GetString("Carousel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Categories.
        /// </summary>
        internal static string Categories {
            get {
                return ResourceManager.GetString("Categories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicle Categories.
        /// </summary>
        internal static string CategoriesHint {
            get {
                return ResourceManager.GetString("CategoriesHint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Category.
        /// </summary>
        internal static string Category {
            get {
                return ResourceManager.GetString("Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Clear.
        /// </summary>
        internal static string Clear {
            get {
                return ResourceManager.GetString("Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Сomment.
        /// </summary>
        internal static string CommentVehicle {
            get {
                return ResourceManager.GetString("CommentVehicle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Comment vehicles.
        /// </summary>
        internal static string CommentVehicleTip {
            get {
                return ResourceManager.GetString("CommentVehicleTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Do you want to delete the current row?.
        /// </summary>
        internal static string ConfirmDeleteQuestion {
            get {
                return ResourceManager.GetString("ConfirmDeleteQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Confirm deletion.
        /// </summary>
        internal static string ConfirmDeletion {
            get {
                return ResourceManager.GetString("ConfirmDeletion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Date of birth.
        /// </summary>
        internal static string DateOfBirth {
            get {
                return ResourceManager.GetString("DateOfBirth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Date of birth.
        /// </summary>
        internal static string DateOfBirthShort {
            get {
                return ResourceManager.GetString("DateOfBirthShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Delete.
        /// </summary>
        internal static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Removal of prohibited!.
        /// </summary>
        internal static string DeleteBan {
            get {
                return ResourceManager.GetString("DeleteBan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Delete current group.
        /// </summary>
        internal static string DeleteCurrentGroup {
            get {
                return ResourceManager.GetString("DeleteCurrentGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Delete group.
        /// </summary>
        internal static string DeleteGroup {
            get {
                return ResourceManager.GetString("DeleteGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Department.
        /// </summary>
        internal static string Department {
            get {
                return ResourceManager.GetString("Department", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Documents.
        /// </summary>
        internal static string Documents {
            get {
                return ResourceManager.GetString("Documents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Driver.
        /// </summary>
        internal static string Driver {
            get {
                return ResourceManager.GetString("Driver", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Driver &lt;b&gt;not assigned&lt;/b&gt; for &lt;i&gt;selected&lt;/i&gt;vehicle.
        /// </summary>
        internal static string DriverNotAssigned {
            get {
                return ResourceManager.GetString("DriverNotAssigned", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Drivers.
        /// </summary>
        internal static string Drivers {
            get {
                return ResourceManager.GetString("Drivers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Drivers with type: {0}.
        /// </summary>
        internal static string DriversWithType {
            get {
                return ResourceManager.GetString("DriversWithType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Drivers type.
        /// </summary>
        internal static string DriverType {
            get {
                return ResourceManager.GetString("DriverType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Drivers types editor.
        /// </summary>
        internal static string DriverTypeEditor {
            get {
                return ResourceManager.GetString("DriverTypeEditor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Type.
        /// </summary>
        internal static string DriverTypeShort {
            get {
                return ResourceManager.GetString("DriverTypeShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Driving license.
        /// </summary>
        internal static string DrivingLicense {
            get {
                return ResourceManager.GetString("DrivingLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicles category editor.
        /// </summary>
        internal static string EditorVehiclesCategory {
            get {
                return ResourceManager.GetString("EditorVehiclesCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Error drivers handbook.
        /// </summary>
        internal static string ErrorNumer {
            get {
                return ResourceManager.GetString("ErrorNumer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Acknowledge output in Excel?.
        /// </summary>
        internal static string ExcelOutputConfim {
            get {
                return ResourceManager.GetString("ExcelOutputConfim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Export.
        /// </summary>
        internal static string Export {
            get {
                return ResourceManager.GetString("Export", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Filter.
        /// </summary>
        internal static string Filter {
            get {
                return ResourceManager.GetString("Filter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Name.
        /// </summary>
        internal static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Consumption fuel l/motohour.
        /// </summary>
        internal static string FuelMotor {
            get {
                return ResourceManager.GetString("FuelMotor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Fuel consumption rate on the move, l/100km.
        /// </summary>
        internal static string FuelNormMoving {
            get {
                return ResourceManager.GetString("FuelNormMoving", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Fuel consumption rate at idle, l/h.
        /// </summary>
        internal static string FuelNormParking {
            get {
                return ResourceManager.GetString("FuelNormParking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Consumption fuel l/100 km.
        /// </summary>
        internal static string FuelWay {
            get {
                return ResourceManager.GetString("FuelWay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Group.
        /// </summary>
        internal static string Group {
            get {
                return ResourceManager.GetString("Group", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Group description.
        /// </summary>
        internal static string GroupDescription {
            get {
                return ResourceManager.GetString("GroupDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Group name.
        /// </summary>
        internal static string GroupName {
            get {
                return ResourceManager.GetString("GroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Identificator.
        /// </summary>
        internal static string ID {
            get {
                return ResourceManager.GetString("ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Identifier.
        /// </summary>
        internal static string IdentifierVehicle {
            get {
                return ResourceManager.GetString("IdentifierVehicle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Info.
        /// </summary>
        internal static string Info {
            get {
                return ResourceManager.GetString("Info", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Do not show Dgps.
        /// </summary>
        internal static string IsNotDrawDgps {
            get {
                return ResourceManager.GetString("IsNotDrawDgps", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Last data.
        /// </summary>
        internal static string LastData {
            get {
                return ResourceManager.GetString("LastData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Surname.
        /// </summary>
        internal static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Logical.
        /// </summary>
        internal static string Logical {
            get {
                return ResourceManager.GetString("Logical", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Login.
        /// </summary>
        internal static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Measuring.
        /// </summary>
        internal static string Measuring {
            get {
                return ResourceManager.GetString("Measuring", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Model.
        /// </summary>
        internal static string Model {
            get {
                return ResourceManager.GetString("Model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Name.
        /// </summary>
        internal static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Not assigned.
        /// </summary>
        internal static string NotAssigned {
            get {
                return ResourceManager.GetString("NotAssigned", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Plate number.
        /// </summary>
        internal static string NumberPlate {
            get {
                return ResourceManager.GetString("NumberPlate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Open file.
        /// </summary>
        internal static string OpenFile {
            get {
                return ResourceManager.GetString("OpenFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Outer base link Id.
        /// </summary>
        internal static string OuterBaseLink {
            get {
                return ResourceManager.GetString("OuterBaseLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Not assigned group.
        /// </summary>
        internal static string OutsideGroups {
            get {
                return ResourceManager.GetString("OutsideGroups", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Packet 64.
        /// </summary>
        internal static string Packet64 {
            get {
                return ResourceManager.GetString("Packet64", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на PDF.
        /// </summary>
        internal static string PdfFormat {
            get {
                return ResourceManager.GetString("PdfFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Foto.
        /// </summary>
        internal static string Photo {
            get {
                return ResourceManager.GetString("Photo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Print.
        /// </summary>
        internal static string Print {
            get {
                return ResourceManager.GetString("Print", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Totals.
        /// </summary>
        internal static string Results {
            get {
                return ResourceManager.GetString("Results", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Save.
        /// </summary>
        internal static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Save all changes.
        /// </summary>
        internal static string SaveAllChanges {
            get {
                return ResourceManager.GetString("SaveAllChanges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SIM number.
        /// </summary>
        internal static string SIM {
            get {
                return ResourceManager.GetString("SIM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на The phone number should be only numbers!.
        /// </summary>
        internal static string SymbolNumer {
            get {
                return ResourceManager.GetString("SymbolNumer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Table view.
        /// </summary>
        internal static string TableView {
            get {
                return ResourceManager.GetString("TableView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Teletrack.
        /// </summary>
        internal static string Teletrack {
            get {
                return ResourceManager.GetString("Teletrack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на TOTAL:.
        /// </summary>
        internal static string TotalRecord {
            get {
                return ResourceManager.GetString("TotalRecord", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Total number of sensors.
        /// </summary>
        internal static string TotalSensors {
            get {
                return ResourceManager.GetString("TotalSensors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Total number of subgroups.
        /// </summary>
        internal static string TotalSubgroups {
            get {
                return ResourceManager.GetString("TotalSubgroups", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Total number of vehicles.
        /// </summary>
        internal static string TotalVehicles {
            get {
                return ResourceManager.GetString("TotalVehicles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicles.
        /// </summary>
        internal static string Transport {
            get {
                return ResourceManager.GetString("Transport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Category.
        /// </summary>
        internal static string VehCategory {
            get {
                return ResourceManager.GetString("VehCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Category2.
        /// </summary>
        internal static string VehCategory2 {
            get {
                return ResourceManager.GetString("VehCategory2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Category of vehicles.
        /// </summary>
        internal static string VehCategoryTip {
            get {
                return ResourceManager.GetString("VehCategoryTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Category of vehicles2.
        /// </summary>
        internal static string VehCategoryTip2 {
            get {
                return ResourceManager.GetString("VehCategoryTip2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Vehicles with category {0}.
        /// </summary>
        internal static string VehiclesWithCategory {
            get {
                return ResourceManager.GetString("VehiclesWithCategory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на View.
        /// </summary>
        internal static string View {
            get {
                return ResourceManager.GetString("View", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Web.
        /// </summary>
        internal static string WebPage {
            get {
                return ResourceManager.GetString("WebPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Excel.
        /// </summary>
        internal static string XlsFormat {
            get {
                return ResourceManager.GetString("XlsFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Saving data to an XML file.
        /// </summary>
        internal static string XMLSave {
            get {
                return ResourceManager.GetString("XMLSave", resourceCulture);
            }
        }
    }
}
