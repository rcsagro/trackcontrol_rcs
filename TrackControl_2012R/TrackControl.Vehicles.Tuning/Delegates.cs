using System;

namespace TrackControl.Vehicles.Tuning
{
  public delegate void StatusChanged();
  public delegate void ModelChanged();
  public delegate void ButtonClicked();
}
