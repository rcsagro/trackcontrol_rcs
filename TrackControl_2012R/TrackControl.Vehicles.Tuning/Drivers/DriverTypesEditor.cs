using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;

namespace TrackControl.Vehicles.Tuning
{
    public partial class DriverTypesEditor : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshDriverTypesList;

        bool _driverTypesChanged;

        public DriverTypesEditor()
        {
            InitializeComponent();
            Localization();
            gcSpec.DataSource = DriverVehicleProvider.GetListDriverTypes();

        }

        bool DeleteRowDriverType()
        {
            DriverType driverType = GetCurrentDriverType();
            if ( driverType != null)
            {
                int drivers = DriverVehicleProvider.CountDriversDriverType(driverType);
                if (drivers > 0)
                {
                    XtraMessageBox.Show(Resources.DeleteBan, string.Format(Resources.DriversWithType, drivers));
                    return false;
                }
                if (XtraMessageBox.Show(Resources.ConfirmDeleteQuestion,
               Resources.ConfirmDeletion,
               MessageBoxButtons.YesNoCancel,
               MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return driverType.Delete();
                }
                else
                    return false;
            }
            else
                return false;
        }

        private DriverType GetCurrentDriverType()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[gcSpec.DataSource];
            DriverType dt = (DriverType)cm.Current;
            return dt;
        }

        void Localization()
        {
            this.Text = Resources.DriverTypeEditor;
            colName.Caption = Resources.DriverType; 
        }

        private void gvSpec_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DriverType dt = GetCurrentDriverType();
            if (dt != null)
            {
                dt.Save();
                _driverTypesChanged = true;
            }
        }

        private void gcSpec_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowDriverType();
                _driverTypesChanged = !e.Handled;
            }
        }

        private void DriverTypesEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_driverTypesChanged && RefreshDriverTypesList != null)
                RefreshDriverTypesList();
        }

    }
}