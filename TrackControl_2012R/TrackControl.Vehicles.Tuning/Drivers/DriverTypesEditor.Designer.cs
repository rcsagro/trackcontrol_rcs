namespace TrackControl.Vehicles.Tuning
{
    partial class DriverTypesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcSpec = new DevExpress.XtraGrid.GridControl();
            this.gvSpec = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcSpec
            // 
            this.gcSpec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSpec.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcSpec_EmbeddedNavigator_ButtonClick);
            this.gcSpec.Location = new System.Drawing.Point(0, 0);
            this.gcSpec.MainView = this.gvSpec;
            this.gcSpec.Name = "gcSpec";
            this.gcSpec.Size = new System.Drawing.Size(381, 439);
            this.gcSpec.TabIndex = 0;
            this.gcSpec.UseEmbeddedNavigator = true;
            this.gcSpec.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSpec,
            this.gridView2});
            // 
            // gvSpec
            // 
            this.gvSpec.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvSpec.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvSpec.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvSpec.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSpec.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSpec.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvSpec.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvSpec.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvSpec.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvSpec.Appearance.Empty.Options.UseBackColor = true;
            this.gvSpec.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSpec.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSpec.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSpec.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSpec.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvSpec.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvSpec.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvSpec.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSpec.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvSpec.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvSpec.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvSpec.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvSpec.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvSpec.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvSpec.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvSpec.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvSpec.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvSpec.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvSpec.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvSpec.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvSpec.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSpec.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSpec.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvSpec.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvSpec.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvSpec.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvSpec.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvSpec.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvSpec.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSpec.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvSpec.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvSpec.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvSpec.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvSpec.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvSpec.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSpec.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSpec.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvSpec.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSpec.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSpec.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.OddRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.OddRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvSpec.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvSpec.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvSpec.Appearance.Preview.Options.UseBackColor = true;
            this.gvSpec.Appearance.Preview.Options.UseFont = true;
            this.gvSpec.Appearance.Preview.Options.UseForeColor = true;
            this.gvSpec.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSpec.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.Row.Options.UseBackColor = true;
            this.gvSpec.Appearance.Row.Options.UseForeColor = true;
            this.gvSpec.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSpec.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvSpec.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvSpec.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvSpec.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSpec.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSpec.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvSpec.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvSpec.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvSpec.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvSpec.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSpec.Appearance.VertLine.Options.UseBackColor = true;
            this.gvSpec.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName});
            this.gvSpec.GridControl = this.gcSpec;
            this.gvSpec.Name = "gvSpec";
            this.gvSpec.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSpec.OptionsView.EnableAppearanceOddRow = true;
            this.gvSpec.OptionsView.ShowGroupPanel = false;
            this.gvSpec.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvSpec_CellValueChanged);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "������������� ��������";
            this.colName.FieldName = "TypeName";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcSpec;
            this.gridView2.Name = "gridView2";
            // 
            // DriverTypesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 439);
            this.Controls.Add(this.gcSpec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DriverTypesEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�������� ������������� ���������";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DriverTypesEditor_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSpec;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSpec;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
    }
}