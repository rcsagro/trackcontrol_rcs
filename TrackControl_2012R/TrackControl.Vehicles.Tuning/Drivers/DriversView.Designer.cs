namespace TrackControl.Vehicles.Tuning
{
  partial class DriversView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DriversView));
            this._idCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._firstNameCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._lastNameCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_2 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._dayOfBirthCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._dateRepo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.layoutViewField_layoutViewColumn1_3 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._photoCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._photoRepo = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.layoutViewField_layoutViewColumn1_4 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._statusCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._statusRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._statusImages = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutViewField_layoutViewColumn1_5 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._licenseCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_6 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._categoriesCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this._categoriesRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutViewField_layoutViewColumn1_7 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._identifierCarouselColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_8 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._grid = new DevExpress.XtraGrid.GridControl();
            this._bandedView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this._personBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._firstNameGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._lastNameGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolDriverType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.leDriverTypes = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this._statusGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._idGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolOutLink = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rteOutLink = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._dayOfBirthGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._licenseBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._licenseGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._categoriesGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._identifier = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._numberTelephone = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._columnTelephone = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolDepartment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._photoBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this._photoGridColumn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this._layoutBtn = new DevExpress.XtraBars.BarButtonItem();
            this._layoutPopup = new DevExpress.XtraBars.PopupMenu(this.components);
            this._viewTableBtn = new DevExpress.XtraBars.BarButtonItem();
            this._viewCarouselBtn = new DevExpress.XtraBars.BarButtonItem();
            this._viewCardBtn = new DevExpress.XtraBars.BarButtonItem();
            this._exportBtn = new DevExpress.XtraBars.BarButtonItem();
            this._exportPopup = new DevExpress.XtraBars.PopupMenu(this.components);
            this._toPdfBtn = new DevExpress.XtraBars.BarButtonItem();
            this._toHtmlBtn = new DevExpress.XtraBars.BarButtonItem();
            this._toXlsBtn = new DevExpress.XtraBars.BarButtonItem();
            this._printBtn = new DevExpress.XtraBars.BarButtonItem();
            this._addNewBtn = new DevExpress.XtraBars.BarButtonItem();
            this._deleteBtn = new DevExpress.XtraBars.BarButtonItem();
            this._saveBtn = new DevExpress.XtraBars.BarButtonItem();
            this._cancelBtn = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSpecEdit = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._layoutView = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this._numberCarouselNumberTelefone = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_9 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this._departmentColumn = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_layoutViewColumn1_10 = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.Group1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this._mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.btXmlExport = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateRepo.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._categoriesRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bandedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriverTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._exportPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1)).BeginInit();
            this._mainTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // _idCarouselColumn
            // 
            this._idCarouselColumn.AppearanceCell.Options.UseTextOptions = true;
            this._idCarouselColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._idCarouselColumn.Caption = "Id";
            this._idCarouselColumn.FieldName = "Id";
            this._idCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1;
            this._idCarouselColumn.Name = "_idCarouselColumn";
            this._idCarouselColumn.OptionsColumn.AllowEdit = false;
            this._idCarouselColumn.OptionsColumn.AllowFocus = false;
            this._idCarouselColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._idCarouselColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._idCarouselColumn.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._idCarouselColumn.OptionsColumn.AllowMove = false;
            this._idCarouselColumn.OptionsColumn.AllowShowHide = false;
            this._idCarouselColumn.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // layoutViewField_layoutViewColumn1
            // 
            this.layoutViewField_layoutViewColumn1.EditorPreferredWidth = 224;
            this.layoutViewField_layoutViewColumn1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutViewField_layoutViewColumn1.Location = new System.Drawing.Point(100, 0);
            this.layoutViewField_layoutViewColumn1.Name = "layoutViewField_layoutViewColumn1";
            this.layoutViewField_layoutViewColumn1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1.Size = new System.Drawing.Size(247, 24);
            this.layoutViewField_layoutViewColumn1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1.TextSize = new System.Drawing.Size(14, 13);
            this.layoutViewField_layoutViewColumn1.TextToControlDistance = 5;
            // 
            // _firstNameCarouselColumn
            // 
            this._firstNameCarouselColumn.Caption = "���";
            this._firstNameCarouselColumn.FieldName = "FirstName";
            this._firstNameCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_1;
            this._firstNameCarouselColumn.Name = "_firstNameCarouselColumn";
            // 
            // layoutViewField_layoutViewColumn1_1
            // 
            this.layoutViewField_layoutViewColumn1_1.EditorPreferredWidth = 122;
            this.layoutViewField_layoutViewColumn1_1.Location = new System.Drawing.Point(100, 24);
            this.layoutViewField_layoutViewColumn1_1.Name = "layoutViewField_layoutViewColumn1_1";
            this.layoutViewField_layoutViewColumn1_1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_1.Size = new System.Drawing.Size(271, 24);
            this.layoutViewField_layoutViewColumn1_1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_1.TextSize = new System.Drawing.Size(140, 13);
            this.layoutViewField_layoutViewColumn1_1.TextToControlDistance = 5;
            // 
            // _lastNameCarouselColumn
            // 
            this._lastNameCarouselColumn.Caption = "�������";
            this._lastNameCarouselColumn.FieldName = "LastName";
            this._lastNameCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_2;
            this._lastNameCarouselColumn.Name = "_lastNameCarouselColumn";
            // 
            // layoutViewField_layoutViewColumn1_2
            // 
            this.layoutViewField_layoutViewColumn1_2.EditorPreferredWidth = 214;
            this.layoutViewField_layoutViewColumn1_2.Location = new System.Drawing.Point(100, 48);
            this.layoutViewField_layoutViewColumn1_2.Name = "layoutViewField_layoutViewColumn1_2";
            this.layoutViewField_layoutViewColumn1_2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_2.Size = new System.Drawing.Size(271, 24);
            this.layoutViewField_layoutViewColumn1_2.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1_2.TextSize = new System.Drawing.Size(48, 13);
            this.layoutViewField_layoutViewColumn1_2.TextToControlDistance = 5;
            // 
            // _dayOfBirthCarouselColumn
            // 
            this._dayOfBirthCarouselColumn.Caption = "���� ����.";
            this._dayOfBirthCarouselColumn.ColumnEdit = this._dateRepo;
            this._dayOfBirthCarouselColumn.FieldName = "DayOfBirth";
            this._dayOfBirthCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_3;
            this._dayOfBirthCarouselColumn.Name = "_dayOfBirthCarouselColumn";
            // 
            // _dateRepo
            // 
            this._dateRepo.AutoHeight = false;
            this._dateRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._dateRepo.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._dateRepo.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this._dateRepo.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this._dateRepo.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista;
            this._dateRepo.Name = "_dateRepo";
            this._dateRepo.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // layoutViewField_layoutViewColumn1_3
            // 
            this.layoutViewField_layoutViewColumn1_3.EditorPreferredWidth = 198;
            this.layoutViewField_layoutViewColumn1_3.Location = new System.Drawing.Point(100, 72);
            this.layoutViewField_layoutViewColumn1_3.Name = "layoutViewField_layoutViewColumn1_3";
            this.layoutViewField_layoutViewColumn1_3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_3.Size = new System.Drawing.Size(271, 24);
            this.layoutViewField_layoutViewColumn1_3.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1_3.TextSize = new System.Drawing.Size(64, 13);
            this.layoutViewField_layoutViewColumn1_3.TextToControlDistance = 5;
            // 
            // _photoCarouselColumn
            // 
            this._photoCarouselColumn.Caption = "����";
            this._photoCarouselColumn.ColumnEdit = this._photoRepo;
            this._photoCarouselColumn.FieldName = "Photo";
            this._photoCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_4;
            this._photoCarouselColumn.Name = "_photoCarouselColumn";
            // 
            // _photoRepo
            // 
            this._photoRepo.Name = "_photoRepo";
            this._photoRepo.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this._photoRepo.ImageChanged += new System.EventHandler(this.photoRepo_ImageChanged);
            // 
            // layoutViewField_layoutViewColumn1_4
            // 
            this.layoutViewField_layoutViewColumn1_4.EditorPreferredWidth = 96;
            this.layoutViewField_layoutViewColumn1_4.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1_4.MaxSize = new System.Drawing.Size(100, 120);
            this.layoutViewField_layoutViewColumn1_4.MinSize = new System.Drawing.Size(100, 120);
            this.layoutViewField_layoutViewColumn1_4.Name = "layoutViewField_layoutViewColumn1_4";
            this.layoutViewField_layoutViewColumn1_4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_4.Size = new System.Drawing.Size(100, 120);
            this.layoutViewField_layoutViewColumn1_4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn1_4.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1_4.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn1_4.TextVisible = false;
            // 
            // _statusCarouselColumn
            // 
            this._statusCarouselColumn.ColumnEdit = this._statusRepo;
            this._statusCarouselColumn.FieldName = "Status";
            this._statusCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_5;
            this._statusCarouselColumn.Name = "_statusCarouselColumn";
            this._statusCarouselColumn.OptionsColumn.AllowEdit = false;
            this._statusCarouselColumn.OptionsColumn.AllowFocus = false;
            this._statusCarouselColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._statusCarouselColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._statusCarouselColumn.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._statusCarouselColumn.OptionsColumn.AllowMove = false;
            this._statusCarouselColumn.OptionsColumn.AllowShowHide = false;
            this._statusCarouselColumn.OptionsColumn.AllowSize = false;
            this._statusCarouselColumn.OptionsColumn.FixedWidth = true;
            this._statusCarouselColumn.OptionsColumn.ShowCaption = false;
            this._statusCarouselColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._statusCarouselColumn.OptionsColumn.TabStop = false;
            // 
            // _statusRepo
            // 
            this._statusRepo.AutoHeight = false;
            this._statusRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._statusRepo.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._statusRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3)});
            this._statusRepo.Name = "_statusRepo";
            this._statusRepo.SmallImages = this._statusImages;
            // 
            // _statusImages
            // 
            this._statusImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_statusImages.ImageStream")));
            this._statusImages.Images.SetKeyName(0, "Ok");
            this._statusImages.Images.SetKeyName(1, "NotSaved");
            this._statusImages.Images.SetKeyName(2, "Error");
            this._statusImages.Images.SetKeyName(3, "New");
            // 
            // layoutViewField_layoutViewColumn1_5
            // 
            this.layoutViewField_layoutViewColumn1_5.EditorPreferredWidth = 20;
            this.layoutViewField_layoutViewColumn1_5.Location = new System.Drawing.Point(347, 0);
            this.layoutViewField_layoutViewColumn1_5.MaxSize = new System.Drawing.Size(24, 20);
            this.layoutViewField_layoutViewColumn1_5.MinSize = new System.Drawing.Size(24, 20);
            this.layoutViewField_layoutViewColumn1_5.Name = "layoutViewField_layoutViewColumn1_5";
            this.layoutViewField_layoutViewColumn1_5.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_5.Size = new System.Drawing.Size(24, 24);
            this.layoutViewField_layoutViewColumn1_5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutViewField_layoutViewColumn1_5.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewField_layoutViewColumn1_5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutViewField_layoutViewColumn1_5.TextToControlDistance = 0;
            this.layoutViewField_layoutViewColumn1_5.TextVisible = false;
            // 
            // _licenseCarouselColumn
            // 
            this._licenseCarouselColumn.Caption = "�����";
            this._licenseCarouselColumn.FieldName = "License";
            this._licenseCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_6;
            this._licenseCarouselColumn.Name = "_licenseCarouselColumn";
            // 
            // layoutViewField_layoutViewColumn1_6
            // 
            this.layoutViewField_layoutViewColumn1_6.EditorPreferredWidth = 297;
            this.layoutViewField_layoutViewColumn1_6.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_layoutViewColumn1_6.Name = "layoutViewField_layoutViewColumn1_6";
            this.layoutViewField_layoutViewColumn1_6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutViewField_layoutViewColumn1_6.Size = new System.Drawing.Size(347, 30);
            this.layoutViewField_layoutViewColumn1_6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1_6.TextSize = new System.Drawing.Size(35, 13);
            this.layoutViewField_layoutViewColumn1_6.TextToControlDistance = 5;
            // 
            // _categoriesCarouselColumn
            // 
            this._categoriesCarouselColumn.Caption = "���������";
            this._categoriesCarouselColumn.ColumnEdit = this._categoriesRepo;
            this._categoriesCarouselColumn.FieldName = "Categories";
            this._categoriesCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_7;
            this._categoriesCarouselColumn.Name = "_categoriesCarouselColumn";
            // 
            // _categoriesRepo
            // 
            this._categoriesRepo.AutoHeight = false;
            this._categoriesRepo.MaxLength = 5;
            this._categoriesRepo.Name = "_categoriesRepo";
            // 
            // layoutViewField_layoutViewColumn1_7
            // 
            this.layoutViewField_layoutViewColumn1_7.EditorPreferredWidth = 274;
            this.layoutViewField_layoutViewColumn1_7.Location = new System.Drawing.Point(0, 30);
            this.layoutViewField_layoutViewColumn1_7.Name = "layoutViewField_layoutViewColumn1_7";
            this.layoutViewField_layoutViewColumn1_7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutViewField_layoutViewColumn1_7.Size = new System.Drawing.Size(347, 30);
            this.layoutViewField_layoutViewColumn1_7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1_7.TextSize = new System.Drawing.Size(58, 13);
            this.layoutViewField_layoutViewColumn1_7.TextToControlDistance = 5;
            // 
            // _identifierCarouselColumn
            // 
            this._identifierCarouselColumn.Caption = "�������������";
            this._identifierCarouselColumn.FieldName = "Identifier";
            this._identifierCarouselColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_8;
            this._identifierCarouselColumn.Name = "_identifierCarouselColumn";
            // 
            // layoutViewField_layoutViewColumn1_8
            // 
            this.layoutViewField_layoutViewColumn1_8.EditorPreferredWidth = 246;
            this.layoutViewField_layoutViewColumn1_8.Location = new System.Drawing.Point(0, 60);
            this.layoutViewField_layoutViewColumn1_8.Name = "layoutViewField_layoutViewColumn1_8";
            this.layoutViewField_layoutViewColumn1_8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutViewField_layoutViewColumn1_8.Size = new System.Drawing.Size(347, 30);
            this.layoutViewField_layoutViewColumn1_8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutViewField_layoutViewColumn1_8.TextSize = new System.Drawing.Size(86, 13);
            this.layoutViewField_layoutViewColumn1_8.TextToControlDistance = 5;
            // 
            // _grid
            // 
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.MainView = this._bandedView;
            this._grid.Margin = new System.Windows.Forms.Padding(0);
            this._grid.MenuManager = this._barManager;
            this._grid.Name = "_grid";
            this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._dateRepo,
            this._statusRepo,
            this._photoRepo,
            this._categoriesRepo,
            this.rteOutLink,
            this.leDriverTypes});
            this._grid.Size = new System.Drawing.Size(684, 446);
            this._grid.TabIndex = 0;
            this._grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._bandedView,
            this._layoutView});
            // 
            // _bandedView
            // 
            this._bandedView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this._personBand,
            this._licenseBand,
            this._numberTelephone,
            this._photoBand});
            this._bandedView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this._firstNameGridColumn,
            this._lastNameGridColumn,
            this._statusGridColumn,
            this._idGridColumn,
            this._dayOfBirthGridColumn,
            this._licenseGridColumn,
            this._categoriesGridColumn,
            this._identifier,
            this._columnTelephone,
            this._photoGridColumn,
            this.bcolOutLink,
            this.bcolDriverType,
            this.bcolDepartment});
            this._bandedView.GridControl = this._grid;
            this._bandedView.Name = "_bandedView";
            this._bandedView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this._bandedView.OptionsCustomization.AllowBandMoving = false;
            this._bandedView.OptionsCustomization.AllowColumnMoving = false;
            this._bandedView.OptionsCustomization.AllowGroup = false;
            this._bandedView.OptionsCustomization.AllowQuickHideColumns = false;
            this._bandedView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this._bandedView.OptionsMenu.EnableFooterMenu = false;
            this._bandedView.OptionsMenu.EnableGroupPanelMenu = false;
            this._bandedView.OptionsPrint.PrintBandHeader = false;
            this._bandedView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._bandedView.OptionsView.ColumnAutoWidth = true;
            this._bandedView.OptionsView.ShowBands = false;
            this._bandedView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this._bandedView.OptionsView.ShowGroupPanel = false;
            // 
            // _personBand
            // 
            this._personBand.Caption = "�������������� ��������";
            this._personBand.Columns.Add(this._firstNameGridColumn);
            this._personBand.Columns.Add(this._lastNameGridColumn);
            this._personBand.Columns.Add(this.bcolDriverType);
            this._personBand.Columns.Add(this._statusGridColumn);
            this._personBand.Columns.Add(this._idGridColumn);
            this._personBand.Columns.Add(this.bcolOutLink);
            this._personBand.Columns.Add(this._dayOfBirthGridColumn);
            this._personBand.Name = "_personBand";
            this._personBand.OptionsBand.AllowMove = false;
            this._personBand.VisibleIndex = 0;
            this._personBand.Width = 388;
            // 
            // _firstNameGridColumn
            // 
            this._firstNameGridColumn.Caption = "���";
            this._firstNameGridColumn.FieldName = "FirstName";
            this._firstNameGridColumn.MinWidth = 175;
            this._firstNameGridColumn.Name = "_firstNameGridColumn";
            this._firstNameGridColumn.OptionsColumn.AllowMove = false;
            this._firstNameGridColumn.OptionsColumn.AllowShowHide = false;
            this._firstNameGridColumn.OptionsColumn.FixedWidth = true;
            this._firstNameGridColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._firstNameGridColumn.Visible = true;
            this._firstNameGridColumn.Width = 175;
            // 
            // _lastNameGridColumn
            // 
            this._lastNameGridColumn.Caption = "�������";
            this._lastNameGridColumn.FieldName = "LastName";
            this._lastNameGridColumn.Name = "_lastNameGridColumn";
            this._lastNameGridColumn.OptionsColumn.AllowMove = false;
            this._lastNameGridColumn.OptionsColumn.AllowShowHide = false;
            this._lastNameGridColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._lastNameGridColumn.Visible = true;
            this._lastNameGridColumn.Width = 93;
            // 
            // bcolDriverType
            // 
            this.bcolDriverType.Caption = "�������������";
            this.bcolDriverType.ColumnEdit = this.leDriverTypes;
            this.bcolDriverType.FieldName = "TypeDriverId";
            this.bcolDriverType.Name = "bcolDriverType";
            this.bcolDriverType.Visible = true;
            this.bcolDriverType.Width = 120;
            // 
            // leDriverTypes
            // 
            this.leDriverTypes.AutoHeight = false;
            this.leDriverTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leDriverTypes.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeName", "��������")});
            this.leDriverTypes.DisplayMember = "TypeName";
            this.leDriverTypes.Name = "leDriverTypes";
            this.leDriverTypes.NullText = "";
            this.leDriverTypes.ValueMember = "Id";
            // 
            // _statusGridColumn
            // 
            this._statusGridColumn.ColumnEdit = this._statusRepo;
            this._statusGridColumn.FieldName = "Status";
            this._statusGridColumn.MinWidth = 25;
            this._statusGridColumn.Name = "_statusGridColumn";
            this._statusGridColumn.OptionsColumn.AllowEdit = false;
            this._statusGridColumn.OptionsColumn.AllowFocus = false;
            this._statusGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._statusGridColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._statusGridColumn.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._statusGridColumn.OptionsColumn.AllowMove = false;
            this._statusGridColumn.OptionsColumn.AllowShowHide = false;
            this._statusGridColumn.OptionsColumn.AllowSize = false;
            this._statusGridColumn.OptionsColumn.FixedWidth = true;
            this._statusGridColumn.OptionsColumn.ShowCaption = false;
            this._statusGridColumn.OptionsColumn.TabStop = false;
            this._statusGridColumn.OptionsFilter.AllowAutoFilter = false;
            this._statusGridColumn.OptionsFilter.AllowFilter = false;
            this._statusGridColumn.RowIndex = 1;
            this._statusGridColumn.Visible = true;
            this._statusGridColumn.Width = 25;
            // 
            // _idGridColumn
            // 
            this._idGridColumn.AppearanceCell.Options.UseTextOptions = true;
            this._idGridColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._idGridColumn.Caption = "ID";
            this._idGridColumn.FieldName = "Id";
            this._idGridColumn.MinWidth = 150;
            this._idGridColumn.Name = "_idGridColumn";
            this._idGridColumn.OptionsColumn.AllowEdit = false;
            this._idGridColumn.OptionsColumn.AllowFocus = false;
            this._idGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._idGridColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._idGridColumn.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._idGridColumn.OptionsColumn.AllowMove = false;
            this._idGridColumn.OptionsColumn.AllowShowHide = false;
            this._idGridColumn.OptionsColumn.FixedWidth = true;
            this._idGridColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._idGridColumn.OptionsColumn.TabStop = false;
            this._idGridColumn.RowIndex = 1;
            this._idGridColumn.Visible = true;
            this._idGridColumn.Width = 150;
            // 
            // bcolOutLink
            // 
            this.bcolOutLink.Caption = "��� ������� ����";
            this.bcolOutLink.ColumnEdit = this.rteOutLink;
            this.bcolOutLink.FieldName = "IdOutLink";
            this.bcolOutLink.Name = "bcolOutLink";
            this.bcolOutLink.RowIndex = 1;
            this.bcolOutLink.Visible = true;
            this.bcolOutLink.Width = 93;
            // 
            // rteOutLink
            // 
            this.rteOutLink.AutoHeight = false;
            this.rteOutLink.MaxLength = 20;
            this.rteOutLink.Name = "rteOutLink";
            // 
            // _dayOfBirthGridColumn
            // 
            this._dayOfBirthGridColumn.Caption = "���� ��������";
            this._dayOfBirthGridColumn.ColumnEdit = this._dateRepo;
            this._dayOfBirthGridColumn.FieldName = "DayOfBirth";
            this._dayOfBirthGridColumn.Name = "_dayOfBirthGridColumn";
            this._dayOfBirthGridColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._dayOfBirthGridColumn.OptionsColumn.AllowMove = false;
            this._dayOfBirthGridColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._dayOfBirthGridColumn.RowIndex = 1;
            this._dayOfBirthGridColumn.Visible = true;
            this._dayOfBirthGridColumn.Width = 120;
            // 
            // _licenseBand
            // 
            this._licenseBand.Caption = "�����";
            this._licenseBand.Columns.Add(this._licenseGridColumn);
            this._licenseBand.Columns.Add(this._categoriesGridColumn);
            this._licenseBand.Columns.Add(this._identifier);
            this._licenseBand.Name = "_licenseBand";
            this._licenseBand.VisibleIndex = 1;
            this._licenseBand.Width = 188;
            // 
            // _licenseGridColumn
            // 
            this._licenseGridColumn.Caption = "�����";
            this._licenseGridColumn.FieldName = "License";
            this._licenseGridColumn.Name = "_licenseGridColumn";
            this._licenseGridColumn.OptionsColumn.AllowMove = false;
            this._licenseGridColumn.Visible = true;
            this._licenseGridColumn.Width = 188;
            // 
            // _categoriesGridColumn
            // 
            this._categoriesGridColumn.Caption = "�������� ���������";
            this._categoriesGridColumn.ColumnEdit = this._categoriesRepo;
            this._categoriesGridColumn.FieldName = "Categories";
            this._categoriesGridColumn.Name = "_categoriesGridColumn";
            this._categoriesGridColumn.OptionsColumn.AllowMove = false;
            this._categoriesGridColumn.RowIndex = 1;
            this._categoriesGridColumn.Visible = true;
            this._categoriesGridColumn.Width = 84;
            // 
            // _identifier
            // 
            this._identifier.Caption = "�������������";
            this._identifier.FieldName = "Identifier";
            this._identifier.Name = "_identifier";
            this._identifier.OptionsColumn.AllowMove = false;
            this._identifier.RowIndex = 1;
            this._identifier.Visible = true;
            this._identifier.Width = 104;
            // 
            // _numberTelephone
            // 
            this._numberTelephone.AppearanceHeader.Options.UseTextOptions = true;
            this._numberTelephone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._numberTelephone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._numberTelephone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._numberTelephone.Caption = "����� ��������";
            this._numberTelephone.Columns.Add(this._columnTelephone);
            this._numberTelephone.Columns.Add(this.bcolDepartment);
            this._numberTelephone.Name = "_numberTelephone";
            this._numberTelephone.ToolTip = "����� �������� ��������";
            this._numberTelephone.VisibleIndex = 2;
            this._numberTelephone.Width = 261;
            // 
            // _columnTelephone
            // 
            this._columnTelephone.AppearanceCell.Options.UseTextOptions = true;
            this._columnTelephone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._columnTelephone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._columnTelephone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._columnTelephone.AppearanceHeader.Options.UseTextOptions = true;
            this._columnTelephone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._columnTelephone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._columnTelephone.Caption = "����� �������� ��������";
            this._columnTelephone.FieldName = "NumTelephone";
            this._columnTelephone.Name = "_columnTelephone";
            this._columnTelephone.OptionsColumn.AllowIncrementalSearch = false;
            this._columnTelephone.OptionsColumn.AllowMove = false;
            this._columnTelephone.OptionsColumn.ShowInCustomizationForm = false;
            this._columnTelephone.ToolTip = "����� �������� �������� ��� �����";
            this._columnTelephone.Visible = true;
            this._columnTelephone.Width = 261;
            // 
            // bcolDepartment
            // 
            this.bcolDepartment.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolDepartment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolDepartment.Caption = "�����������";
            this.bcolDepartment.FieldName = "Department";
            this.bcolDepartment.Name = "bcolDepartment";
            this.bcolDepartment.RowIndex = 1;
            this.bcolDepartment.Visible = true;
            // 
            // _photoBand
            // 
            this._photoBand.Caption = "����";
            this._photoBand.Columns.Add(this._photoGridColumn);
            this._photoBand.Name = "_photoBand";
            this._photoBand.VisibleIndex = 3;
            this._photoBand.Width = 87;
            // 
            // _photoGridColumn
            // 
            this._photoGridColumn.AutoFillDown = true;
            this._photoGridColumn.Caption = "����";
            this._photoGridColumn.ColumnEdit = this._photoRepo;
            this._photoGridColumn.FieldName = "Photo";
            this._photoGridColumn.Name = "_photoGridColumn";
            this._photoGridColumn.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._photoGridColumn.OptionsColumn.AllowIncrementalSearch = false;
            this._photoGridColumn.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this._photoGridColumn.OptionsColumn.AllowMove = false;
            this._photoGridColumn.OptionsColumn.AllowShowHide = false;
            this._photoGridColumn.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this._photoGridColumn.OptionsColumn.ShowInCustomizationForm = false;
            this._photoGridColumn.OptionsFilter.AllowAutoFilter = false;
            this._photoGridColumn.OptionsFilter.AllowFilter = false;
            this._photoGridColumn.Visible = true;
            this._photoGridColumn.Width = 87;
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._layoutBtn,
            this._viewTableBtn,
            this._viewCarouselBtn,
            this._viewCardBtn,
            this._exportBtn,
            this._toPdfBtn,
            this._toHtmlBtn,
            this._toXlsBtn,
            this._printBtn,
            this._addNewBtn,
            this._deleteBtn,
            this._saveBtn,
            this._cancelBtn,
            this.bbiSpecEdit,
            this.btXmlExport});
            this._barManager.MaxItemId = 18;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._layoutBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._exportBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._printBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._addNewBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._saveBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._cancelBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSpecEdit, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _layoutBtn
            // 
            this._layoutBtn.ActAsDropDown = true;
            this._layoutBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._layoutBtn.Caption = "���...";
            this._layoutBtn.DropDownControl = this._layoutPopup;
            this._layoutBtn.Id = 2;
            this._layoutBtn.Name = "_layoutBtn";
            this._layoutBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _layoutPopup
            // 
            this._layoutPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._viewTableBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._viewCarouselBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._viewCardBtn)});
            this._layoutPopup.Manager = this._barManager;
            this._layoutPopup.Name = "_layoutPopup";
            // 
            // _viewTableBtn
            // 
            this._viewTableBtn.Caption = "� ���� �������";
            this._viewTableBtn.Id = 3;
            this._viewTableBtn.Name = "_viewTableBtn";
            this._viewTableBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.viewTableBtn_ItemClick);
            // 
            // _viewCarouselBtn
            // 
            this._viewCarouselBtn.Caption = "��������";
            this._viewCarouselBtn.Id = 4;
            this._viewCarouselBtn.Name = "_viewCarouselBtn";
            this._viewCarouselBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.viewCarouselBtn_ItemClick);
            // 
            // _viewCardBtn
            // 
            this._viewCardBtn.Caption = "� ���� ��������";
            this._viewCardBtn.Id = 5;
            this._viewCardBtn.Name = "_viewCardBtn";
            this._viewCardBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.viewCardBtn_ItemClick);
            // 
            // _exportBtn
            // 
            this._exportBtn.ActAsDropDown = true;
            this._exportBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._exportBtn.Caption = "�������...";
            this._exportBtn.DropDownControl = this._exportPopup;
            this._exportBtn.Id = 6;
            this._exportBtn.Name = "_exportBtn";
            this._exportBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _exportPopup
            // 
            this._exportPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._toPdfBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._toHtmlBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._toXlsBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this.btXmlExport)});
            this._exportPopup.Manager = this._barManager;
            this._exportPopup.Name = "_exportPopup";
            // 
            // _toPdfBtn
            // 
            this._toPdfBtn.Caption = "� ������� PDF";
            this._toPdfBtn.Id = 7;
            this._toPdfBtn.Name = "_toPdfBtn";
            this._toPdfBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toPdfBtn_ItemClick);
            // 
            // _toHtmlBtn
            // 
            this._toHtmlBtn.Caption = "���-�������� ���������";
            this._toHtmlBtn.Id = 8;
            this._toHtmlBtn.Name = "_toHtmlBtn";
            this._toHtmlBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toHtmlBtn_ItemClick);
            // 
            // _toXlsBtn
            // 
            this._toXlsBtn.Caption = "� �������� Excel";
            this._toXlsBtn.Id = 9;
            this._toXlsBtn.Name = "_toXlsBtn";
            this._toXlsBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toXlsBtn_ItemClick);
            // 
            // _printBtn
            // 
            this._printBtn.Caption = "������";
            this._printBtn.Id = 10;
            this._printBtn.Name = "_printBtn";
            this._printBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._printBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printBtn_ItemClick);
            // 
            // _addNewBtn
            // 
            this._addNewBtn.Caption = "��������";
            this._addNewBtn.Id = 11;
            this._addNewBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._addNewBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._addNewBtn.Name = "_addNewBtn";
            this._addNewBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._addNewBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addNewBtn_ItemClick);
            // 
            // _deleteBtn
            // 
            this._deleteBtn.Caption = "�������";
            this._deleteBtn.Id = 12;
            this._deleteBtn.Name = "_deleteBtn";
            this._deleteBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._deleteBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteBtn_ItemClick);
            // 
            // _saveBtn
            // 
            this._saveBtn.Caption = "���������";
            this._saveBtn.Id = 13;
            this._saveBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._saveBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._saveBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.saveBtn_ItemClick);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Caption = "��������";
            this._cancelBtn.Id = 14;
            this._cancelBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._cancelBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._cancelBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._cancelBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.cancelBtn_ItemClick);
            // 
            // bbiSpecEdit
            // 
            this.bbiSpecEdit.Caption = "�������������";
            this.bbiSpecEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSpecEdit.Glyph")));
            this.bbiSpecEdit.Id = 16;
            this.bbiSpecEdit.Name = "bbiSpecEdit";
            this.bbiSpecEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSpecEdit_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(684, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 477);
            this.barDockControlBottom.Size = new System.Drawing.Size(684, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 446);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(684, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 446);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // _layoutView
            // 
            this._layoutView.CardMinSize = new System.Drawing.Size(275, 260);
            this._layoutView.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this._idCarouselColumn,
            this._firstNameCarouselColumn,
            this._lastNameCarouselColumn,
            this._dayOfBirthCarouselColumn,
            this._numberCarouselNumberTelefone,
            this._photoCarouselColumn,
            this._statusCarouselColumn,
            this._licenseCarouselColumn,
            this._categoriesCarouselColumn,
            this._identifierCarouselColumn,
            this._departmentColumn});
            this._layoutView.GridControl = this._grid;
            this._layoutView.Name = "_layoutView";
            this._layoutView.OptionsCustomization.AllowFilter = false;
            this._layoutView.OptionsCustomization.AllowSort = false;
            this._layoutView.OptionsCustomization.ShowGroupCardCaptions = false;
            this._layoutView.OptionsCustomization.ShowResetShrinkButtons = false;
            this._layoutView.OptionsCustomization.ShowSaveLoadLayoutButtons = false;
            this._layoutView.OptionsHeaderPanel.EnableColumnModeButton = false;
            this._layoutView.OptionsHeaderPanel.EnableCustomizeButton = false;
            this._layoutView.OptionsHeaderPanel.EnableMultiColumnModeButton = false;
            this._layoutView.OptionsHeaderPanel.EnablePanButton = false;
            this._layoutView.OptionsHeaderPanel.EnableRowModeButton = false;
            this._layoutView.OptionsHeaderPanel.EnableSingleModeButton = false;
            this._layoutView.OptionsHeaderPanel.ShowColumnModeButton = false;
            this._layoutView.OptionsHeaderPanel.ShowCustomizeButton = false;
            this._layoutView.OptionsHeaderPanel.ShowMultiColumnModeButton = false;
            this._layoutView.OptionsHeaderPanel.ShowPanButton = false;
            this._layoutView.OptionsHeaderPanel.ShowRowModeButton = false;
            this._layoutView.OptionsHeaderPanel.ShowSingleModeButton = false;
            this._layoutView.OptionsView.ShowCardExpandButton = false;
            this._layoutView.OptionsView.ShowHeaderPanel = false;
            this._layoutView.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Carousel;
            this._layoutView.TemplateCard = this.layoutViewCard1;
            // 
            // _numberCarouselNumberTelefone
            // 
            this._numberCarouselNumberTelefone.Caption = "����� �������� ��������";
            this._numberCarouselNumberTelefone.FieldName = "NumTelephone";
            this._numberCarouselNumberTelefone.LayoutViewField = this.layoutViewField_layoutViewColumn1_9;
            this._numberCarouselNumberTelefone.Name = "_numberCarouselNumberTelefone";
            // 
            // layoutViewField_layoutViewColumn1_9
            // 
            this.layoutViewField_layoutViewColumn1_9.EditorPreferredWidth = 222;
            this.layoutViewField_layoutViewColumn1_9.Location = new System.Drawing.Point(0, 253);
            this.layoutViewField_layoutViewColumn1_9.Name = "layoutViewField_layoutViewColumn1_9";
            this.layoutViewField_layoutViewColumn1_9.Size = new System.Drawing.Size(371, 24);
            this.layoutViewField_layoutViewColumn1_9.TextSize = new System.Drawing.Size(140, 13);
            this.layoutViewField_layoutViewColumn1_9.TextToControlDistance = 5;
            // 
            // _departmentColumn
            // 
            this._departmentColumn.Caption = "�����������";
            this._departmentColumn.FieldName = "Department";
            this._departmentColumn.LayoutViewField = this.layoutViewField_layoutViewColumn1_10;
            this._departmentColumn.Name = "_departmentColumn";
            // 
            // layoutViewField_layoutViewColumn1_10
            // 
            this.layoutViewField_layoutViewColumn1_10.EditorPreferredWidth = 222;
            this.layoutViewField_layoutViewColumn1_10.Location = new System.Drawing.Point(0, 277);
            this.layoutViewField_layoutViewColumn1_10.Name = "layoutViewField_layoutViewColumn1_10";
            this.layoutViewField_layoutViewColumn1_10.Size = new System.Drawing.Size(371, 42);
            this.layoutViewField_layoutViewColumn1_10.TextSize = new System.Drawing.Size(140, 13);
            this.layoutViewField_layoutViewColumn1_10.TextToControlDistance = 5;
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.CustomizationFormText = "TemplateCard";
            this.layoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1_4,
            this.item1,
            this.layoutViewField_layoutViewColumn1_1,
            this.layoutViewField_layoutViewColumn1_2,
            this.layoutViewField_layoutViewColumn1_3,
            this.Group1,
            this.layoutViewField_layoutViewColumn1,
            this.layoutViewField_layoutViewColumn1_5,
            this.layoutViewField_layoutViewColumn1_9,
            this.layoutViewField_layoutViewColumn1_10});
            this.layoutViewCard1.Name = "layoutViewCard1";
            this.layoutViewCard1.OptionsItemText.TextToControlDistance = 5;
            this.layoutViewCard1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutViewCard1.Text = "TemplateCard";
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(100, 96);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(271, 24);
            this.item1.Text = "item1";
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Group1
            // 
            this.Group1.CustomizationFormText = "�����";
            this.Group1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_layoutViewColumn1_6,
            this.layoutViewField_layoutViewColumn1_7,
            this.layoutViewField_layoutViewColumn1_8});
            this.Group1.Location = new System.Drawing.Point(0, 120);
            this.Group1.Name = "Group1";
            this.Group1.Size = new System.Drawing.Size(371, 133);
            this.Group1.Text = "�����";
            // 
            // _mainTable
            // 
            this._mainTable.ColumnCount = 1;
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.Controls.Add(this._grid, 0, 0);
            this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTable.Location = new System.Drawing.Point(0, 31);
            this._mainTable.Name = "_mainTable";
            this._mainTable.RowCount = 1;
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.Size = new System.Drawing.Size(684, 446);
            this._mainTable.TabIndex = 4;
            // 
            // btXmlExport
            // 
            this.btXmlExport.Caption = "XML";
            this.btXmlExport.Glyph = ((System.Drawing.Image)(resources.GetObject("btXmlExport.Glyph")));
            this.btXmlExport.Id = 17;
            this.btXmlExport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btXmlExport.LargeGlyph")));
            this.btXmlExport.Name = "btXmlExport";
            this.btXmlExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btXmlExport_ItemClick);
            // 
            // DriversView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._mainTable);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "DriversView";
            this.Size = new System.Drawing.Size(684, 477);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateRepo.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._photoRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._statusImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._categoriesRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bandedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriverTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._exportPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._layoutView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_layoutViewColumn1_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1)).EndInit();
            this._mainTable.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private DevExpress.XtraGrid.GridControl _grid;
    DevExpress.XtraGrid.Views.Layout.LayoutView _layoutView;
    private DevExpress.Utils.ImageCollection _statusImages;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _idCarouselColumn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _firstNameCarouselColumn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _lastNameCarouselColumn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _dayOfBirthCarouselColumn;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _dateRepo;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _photoCarouselColumn;
    private DevExpress.XtraBars.BarButtonItem _layoutBtn;
    private DevExpress.XtraBars.PopupMenu _layoutPopup;
    private DevExpress.XtraBars.BarButtonItem _viewTableBtn;
    private DevExpress.XtraBars.BarButtonItem _viewCarouselBtn;
    private DevExpress.XtraBars.BarButtonItem _viewCardBtn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _statusCarouselColumn;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _statusRepo;
    private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _photoRepo;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _licenseCarouselColumn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _categoriesCarouselColumn;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _identifierCarouselColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView _bandedView;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _statusGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _idGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _firstNameGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _lastNameGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _photoGridColumn;
    private DevExpress.XtraBars.BarButtonItem _exportBtn;
    private DevExpress.XtraBars.BarButtonItem _toPdfBtn;
    private DevExpress.XtraBars.PopupMenu _exportPopup;
    private DevExpress.XtraBars.BarButtonItem _toHtmlBtn;
    private DevExpress.XtraBars.BarButtonItem _toXlsBtn;
    private DevExpress.XtraBars.BarButtonItem _printBtn;
    private DevExpress.XtraBars.BarButtonItem _addNewBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteBtn;
    private DevExpress.XtraBars.BarButtonItem _saveBtn;
    private DevExpress.XtraBars.BarButtonItem _cancelBtn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _dayOfBirthGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _licenseGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _categoriesGridColumn;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _identifier;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _categoriesRepo;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leDriverTypes;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolDriverType;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraBars.BarButtonItem bbiSpecEdit;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn _columnTelephone;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _numberCarouselNumberTelefone;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand _personBand;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand _licenseBand;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand _numberTelephone;
    private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolDepartment;
    private DevExpress.XtraGrid.Views.BandedGrid.GridBand _photoBand;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_1;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_2;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_3;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_4;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_5;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_6;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_7;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_8;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_9;
    private DevExpress.XtraGrid.Columns.LayoutViewColumn _departmentColumn;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_layoutViewColumn1_10;
    private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1;
    private DevExpress.XtraLayout.EmptySpaceItem item1;
    private DevExpress.XtraLayout.LayoutControlGroup Group1;
    private DevExpress.XtraBars.BarButtonItem btXmlExport;

  }
}
