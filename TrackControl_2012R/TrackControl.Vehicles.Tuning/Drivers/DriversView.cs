using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Layout;
using TrackControl.General;
using TrackControl.Vehicles.Tuning.Properties;
using TrackControl.MySqlDal;

namespace TrackControl.Vehicles.Tuning
{
    public partial class DriversView : XtraUserControl
    {
        public event ButtonClicked AddNewClicked;
        public event ButtonClicked SaveAllClicked;
        public event ButtonClicked CancelAllClicked;
        public event Action<Driver> DriverDeleted;
        private bool addNewRow = false;

        public DriversView()
        {
            InitializeComponent();
            Localization();

            _bandedView.RowCountChanged += BandedViewOnRowCountChanged;
            _bandedView.CellValueChanging += _bandedView_CellValueChanging;
        }

        void _bandedView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName != "NumTelephone")
                return;

            string cellValue = e.Value.ToString();
            if (cellValue.Length <= 0)
                return;

            for (int i = 0; i < cellValue.Length; i++)
            {
                if (!"+0123456789".Contains(cellValue[i].ToString()))
                {
                    XtraMessageBox.Show(Resources.SymbolNumer, Resources.ErrorNumer, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
            }
        }

        private void BandedViewOnRowCountChanged(object sender, EventArgs eventArgs)
        {
            if(!addNewRow)
                return;
            
            for (int i = 0; i < _bandedView.RowCount; i++)
            {
                object status = _bandedView.GetRowCellValue(i, "Status");
                int stat = Convert.ToInt32(status.ToString());

                if( stat == (int)TuningStatus.NewAdded )
                {
                    _bandedView.FocusedRowHandle = i;
                    addNewRow = false;
                    break;
                }
            }
        }

        public void BindTo(IList<TuningDriver> drivers)
        {
            _grid.DataSource = drivers;
            OnRefreshDriverTypesList();
        }

        public void RefreshGrid()
        {
            _grid.RefreshDataSource();
            _grid.MainView.RefreshData();
        }

        public void EnableAddNewButton()
        {
            _addNewBtn.Enabled = true;
        }

        public void DisableAddNewButton()
        {
            _addNewBtn.Enabled = false;
        }

        public void EnableDeleteButton()
        {
            _deleteBtn.Enabled = true;
        }

        public void DisableDeleteButton()
        {
            _deleteBtn.Enabled = false;
        }

        public void ShowSaveCancelButtons()
        {
            _saveBtn.Visibility = BarItemVisibility.Always;
            _cancelBtn.Visibility = BarItemVisibility.Always;
        }

        public void HideSaveCancelButtons()
        {
            _saveBtn.Visibility = BarItemVisibility.Never;
            _cancelBtn.Visibility = BarItemVisibility.Never;
        }

        private void Localization()
        {
            _cancelBtn.Glyph = Shared.Cancel;
            _viewCardBtn.Glyph = Shared.Cards;
            _viewCarouselBtn.Glyph = Shared.Carousel;
            _layoutBtn.Glyph = Shared.LayoutPencil;
            _printBtn.Glyph = Shared.Printer;
            _saveBtn.Glyph = Shared.Save;
            _viewTableBtn.Glyph = Shared.Table;
            _exportBtn.Glyph = Shared.TableExport;
            _toHtmlBtn.Glyph = Shared.ToHtml;
            _toPdfBtn.Glyph = Shared.ToPdf;
            _toXlsBtn.Glyph = Shared.ToXls;
            _deleteBtn.Glyph = Shared.UserMinus;
            _addNewBtn.Glyph = Shared.UserPlus;

            _firstNameCarouselColumn.Caption = Resources.FirstName;
            _lastNameCarouselColumn.Caption = Resources.LastName;
            _dayOfBirthCarouselColumn.Caption = Resources.DateOfBirth;
            _photoCarouselColumn.Caption = Resources.Photo;
            _licenseCarouselColumn.Caption = Resources.DrivingLicense;
            _categoriesCarouselColumn.Caption = Resources.Categories;
            _identifierCarouselColumn.Caption = Resources.ID;

            _firstNameGridColumn.Caption = Resources.FirstName;
            _lastNameGridColumn.Caption = Resources.LastName;
            _dayOfBirthGridColumn.Caption = Resources.DateOfBirth;
            _licenseBand.Caption = Resources.DrivingLicense;
            _licenseGridColumn.Caption = Resources.DrivingLicense;
            _categoriesGridColumn.Caption = Resources.Categories;
            _identifier.Caption = Resources.ID;
            _photoBand.Caption = Resources.Photo;
            _photoGridColumn.Caption = Resources.Photo;

            _layoutBtn.Caption = String.Format("{0}...", Resources.View);
            _viewTableBtn.Caption = Resources.TableView;
            _viewCarouselBtn.Caption = Resources.Carousel;
            _viewCardBtn.Caption = Resources.CardView;

            _exportBtn.Caption = String.Format("{0}...", Resources.Export);
            _toPdfBtn.Caption = Resources.PdfFormat;
            _toHtmlBtn.Caption = Resources.WebPage;
            _toXlsBtn.Caption = Resources.XlsFormat;

            _printBtn.Caption = Resources.Print;
            _addNewBtn.Caption = Resources.Add;
            _deleteBtn.Caption = Resources.Delete;
            _saveBtn.Caption = Resources.Save;
            _cancelBtn.Caption = Resources.Cancel;

            Group1.CustomizationFormText = Resources.DrivingLicense;
            Group1.Text = Resources.DrivingLicense;

            bcolOutLink.Caption = Resources.OuterBaseLink;
            bbiSpecEdit.Caption = Resources.DriverTypeShort;
            bcolDriverType.Caption = Resources.DriverTypeShort;
            leDriverTypes.Columns[0].Caption = Resources.Name;
            bcolDepartment.Caption = Resources.Department;
        }

        private void viewTableBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _grid.MainView = _bandedView;
        }

        private void viewCarouselBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _layoutView.OptionsView.ViewMode = LayoutViewMode.Carousel;
            _grid.MainView = _layoutView;
        }

        private void viewCardBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _layoutView.OptionsView.ViewMode = LayoutViewMode.MultiRow;
            _grid.MainView = _layoutView;
        }

        private void this_Load(object sender, EventArgs e)
        {
            _grid.Focus();
        }

        /// <summary>
        /// Export Error
        /// </summary>
        private const string ExportError = "Export Error";

        /// <summary>
        /// File not Saved{0}{1}
        /// </summary>
        private const string FileNotSaved = "File not Saved{0}{1}";

        private void toPdfBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = String.Format("{0} PDF (*.pdf)|*.pdf", Resources.Documents);
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToPdf(sfd.FileName);
                        if (DialogResult.OK == XtraMessageBox.Show(
                            String.Format("{0}?", Resources.OpenFile), "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format(FileNotSaved, Environment.NewLine, sfd.FileName);
                        XtraMessageBox.Show(message, ExportError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void toHtmlBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = String.Format("{0} (*.mht)|*.mht", Resources.WebPage);
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToMht(sfd.FileName);
                        if (DialogResult.OK == XtraMessageBox.Show(
                            String.Format("{0}?", Resources.OpenFile), "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format(FileNotSaved, Environment.NewLine, sfd.FileName);
                        XtraMessageBox.Show(message, ExportError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void toXlsBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = String.Format("{0} Excel (*.xls)|*.xls", Resources.Documents);
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToXls(sfd.FileName);
                        if (DialogResult.OK == XtraMessageBox.Show(
                            String.Format("{0}?", Resources.OpenFile), "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format(FileNotSaved, Environment.NewLine, sfd.FileName);
                        XtraMessageBox.Show(message, ExportError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void printBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _grid.Print();
        }

        private void addNewBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onAddNewClicked();
        }

        private void deleteBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            int rowHandle = getCurrentRowHandle();

            if (GridControl.InvalidRowHandle != rowHandle)
            {
                TuningDriver tuning = (TuningDriver) _grid.FocusedView.GetRow(rowHandle);
                onDriverDeleted(tuning.Driver);
            }
        }

        private void saveBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onSaveAllClicked();
        }

        private void cancelBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            onCancelAllClicked();
        }

        private void photoRepo_ImageChanged(object sender, EventArgs e)
        {
            PictureEdit editor = (PictureEdit) sender;
            Image photo = editor.Image;
            if (null != photo)
                photo = ImageService.Inscribe(photo, 100, 120);
            editor.Image = photo;

            int rowHandle = getCurrentRowHandle();
            if (GridControl.InvalidRowHandle != rowHandle)
            {
                TuningDriver tuning = (TuningDriver) _grid.FocusedView.GetRow(rowHandle);
                tuning.Photo = photo;
            }
        }

        private void onAddNewClicked()
        {
            addNewRow = true;

            ButtonClicked handler = AddNewClicked;
            if (null != handler)
                handler();
        }

        private void onSaveAllClicked()
        {
            ButtonClicked handler = SaveAllClicked;
            if (null != handler)
                handler();
        }

        private void onCancelAllClicked()
        {
            ButtonClicked handler = CancelAllClicked;
            if (null != handler)
                handler();
        }

        private void onDriverDeleted(Driver driver)
        {
            Action<Driver> handler = DriverDeleted;
            if (null != handler)
                handler(driver);
        }

        private int getCurrentRowHandle()
        {
            int rowHandle = GridControl.InvalidRowHandle;
            if (_grid.MainView == _bandedView)
                rowHandle = _bandedView.FocusedRowHandle;
            else if (_grid.MainView == _layoutView)
                rowHandle = _layoutView.FocusedRowHandle;

            return rowHandle;
        }

        private void bbiSpecEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            DriverTypesEditor formEditor = new DriverTypesEditor();
            formEditor.RefreshDriverTypesList += OnRefreshDriverTypesList;
            formEditor.ShowDialog();
        }

        private void OnRefreshDriverTypesList()
        {
            leDriverTypes.DataSource = DriverVehicleProvider.GetListDriverTypes();
        }

        private void btXmlExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            string fileName = string.Format("{0}_{1}.xml", Resources.Drivers, StaticMethods.SuffixDateToFileName());
            fileName = StaticMethods.ShowSaveFileDialog(fileName, Resources.XMLSave, "XML |*.xml");
            if (fileName.Length == 0) return;
            var writer = new XmlTextWriter(fileName, System.Text.Encoding.UTF8) { Formatting = Formatting.Indented };
            writer.WriteStartDocument(false);
            writer.WriteStartElement("Dictionary");
            var drivers = _grid.DataSource as IList<TuningDriver>;
            int countWrited = 0;
            foreach (var drv in drivers)
            {
                writer.WriteStartElement("driver");
                writer.WriteAttributeString("Id", drv.Id.ToString());
                writer.WriteAttributeString("Name", drv.Driver.FullName);
                writer.WriteAttributeString("Identifier", drv.Identifier.ToString());
                writer.WriteAttributeString("Phone", drv.NumTelephone);
                writer.WriteEndElement();
                writer.Flush();
                countWrited++;
            }
            writer.WriteEndDocument();
            writer.Close();
            XtraMessageBox.Show(string.Format("{0} {1}", Resources.TotalRecord, countWrited));
        }
    }
}
