using System;
using System.ComponentModel;
using Agro.Properties;
using DevExpress.XtraEditors; 
namespace Agro
{
        public class AgroSetItem
        {
            private double  dbNewValue;
            private int iNewValue;
            /// <summary>
            /// ����������� ������� ������������ ������� ��� �������� ������ ������
            /// </summary>
            public double MinSquarePersentForCreateRecord
            {
                get
                {
                    return Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.AgroMinSquarePersentForCreateRecord , "0"));
                }
                set
                {
                    if (Double.TryParse(value.ToString(), out dbNewValue))
                        Params.SetParTotal(Params.NUMPAR.AgroMinSquarePersentForCreateRecord, dbNewValue.ToString());
                }
            }
            /// <summary>
            ///����������� ������ �� ���� ��� �������� ������ ������, �����
            /// </summary>
            public  int MinPathInFieldForCreateRecord
            {
                get
                {
                    return Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.AgroMinPathInFieldForCreateRecord, "0"));
                }
                set
                {
                    if (Int32.TryParse(value.ToString(), out iNewValue))
                        Params.SetParTotal(Params.NUMPAR.AgroMinPathInFieldForCreateRecord, iNewValue.ToString());
                }
            }
            /// <summary>
            ///����������� ����� ������ � ���� ��� �������� ������ ������, ������
            /// </summary>
            public  int MinTimeInFieldForCreateRecord
            {
                get
                {
                    return Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.AgroMinTimeInFieldForCreateRecord, "1"));
                }
                set
                {
                    if (Int32.TryParse(value.ToString(), out iNewValue))
                    {
                        if (TestMinutesValue(iNewValue))
                            Params.SetParTotal(Params.NUMPAR.AgroMinTimeInFieldForCreateRecord, iNewValue.ToString());
                    }
                }
            }
            /// <summary>
            ///������� ��������, ������
            /// </summary>
            public decimal BounceSensor
            {
                get
                {
                    return Convert.ToDecimal(Params.ReadParTotal(Params.NUMPAR.AgroBounceSensor, "1"));
                }
                set
                {
                    decimal dcNewValue;
                    if (Decimal.TryParse(value.ToString(), out dcNewValue))
                    {
                        if (TestMinutesValue(iNewValue))
                            Params.SetParTotal(Params.NUMPAR.AgroBounceSensor, dcNewValue.ToString());
                    }
                }
            }
            /// <summary>
            ///�������� ����� �������� ������� (������������� �������� �������� - ��� ������ ���������� � 6 ���� � �� 6 ���� ���������� ���)
            /// </summary>
            public string TimeStartOrder
            {
                get
                {
                    return Params.ReadParTotal(Params.NUMPAR.AgroTimeStartOrder, "00:00");
                }
                set
                {
                    if (value.Length == "00:00".Length)
                    {
                        TimeSpanConverter tsc = new TimeSpanConverter();
                        try
                        {
                            TimeSpan tsWork = (TimeSpan)tsc.ConvertFromInvariantString(value);
                            Params.SetParTotal(Params.NUMPAR.AgroTimeStartOrder, value.ToString());
                        }
                        catch (FormatException ex)
                        {
                            XtraMessageBox.Show(ex.Message); 
                        }
                    }
                }
            }

            public TimeSpan TimeStartOrderTimeSpan
            {
                get
                {
                    TimeSpanConverter tsc = new TimeSpanConverter();
                    try
                    {
                        return (TimeSpan)tsc.ConvertFromInvariantString(Params.ReadParTotal(Params.NUMPAR.AgroTimeStartOrder, "00:00"));
                    }
                    catch 
                    {
                        return (TimeSpan)tsc.ConvertFromInvariantString("00:00");
                    }
                }
            }

            public double AgregatDefaultWidth
            {
                get
                {
                    const string WIDTH_AGREGAT_DEFAULT = "15";
                    return Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.AgroAgregatDefaultWidth, WIDTH_AGREGAT_DEFAULT));
                }
                set
                {
                    if (Double.TryParse(value.ToString(), out dbNewValue))
                        Params.SetParTotal(Params.NUMPAR.AgroAgregatDefaultWidth, dbNewValue.ToString());
                }
            }

            private bool TestMinutesValue(int MinuteValue)
            {
                if ((MinuteValue >= 0) && (MinuteValue < 60))
                    return true;
                else
                {
                    XtraMessageBox.Show(Resources.MinutesControl, Resources.DataControl);
                    return false;
                }
            }

        }
}
