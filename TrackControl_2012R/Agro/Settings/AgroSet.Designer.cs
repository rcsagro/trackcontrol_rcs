namespace Agro
{
    partial class AgroSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgroSet));
            this.prParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.teRoute = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.reTimeEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.teTimeOrder = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.crSattlement = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erMinSquare = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMinPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMinTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erBounceSensor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeOrderStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erAgregatWidht = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTimeEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // prParams
            // 
            this.prParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prParams.Location = new System.Drawing.Point(0, 0);
            this.prParams.Name = "prParams";
            this.prParams.RecordWidth = 53;
            this.prParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teRoute,
            this.reTimeEdit,
            this.teTimeOrder});
            this.prParams.RowHeaderWidth = 147;
            this.prParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crSattlement});
            this.prParams.Size = new System.Drawing.Size(551, 361);
            this.prParams.TabIndex = 1;
            // 
            // teRoute
            // 
            this.teRoute.AutoHeight = false;
            this.teRoute.Name = "teRoute";
            // 
            // reTimeEdit
            // 
            this.reTimeEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.reTimeEdit.AutoHeight = false;
            this.reTimeEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reTimeEdit.DisplayFormat.FormatString = "t";
            this.reTimeEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.reTimeEdit.EditFormat.FormatString = "t";
            this.reTimeEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.reTimeEdit.Mask.EditMask = "t";
            this.reTimeEdit.Name = "reTimeEdit";
            // 
            // teTimeOrder
            // 
            this.teTimeOrder.AutoHeight = false;
            this.teTimeOrder.Mask.EditMask = "00:00";
            this.teTimeOrder.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTimeOrder.Name = "teTimeOrder";
            // 
            // crSattlement
            // 
            this.crSattlement.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erMinSquare,
            this.erMinPath,
            this.erMinTime,
            this.erBounceSensor,
            this.erTimeOrderStart,
            this.erAgregatWidht});
            this.crSattlement.Height = 19;
            this.crSattlement.Name = "crSattlement";
            this.crSattlement.Properties.Caption = "��������� ������������ ������� � ������";
            // 
            // erMinSquare
            // 
            this.erMinSquare.Name = "erMinSquare";
            this.erMinSquare.Properties.Caption = "Min ������� ������������ ������� ��� �������� ������ ������,%";
            this.erMinSquare.Properties.FieldName = "MinSquarePersentForCreateRecord";
            this.erMinSquare.Properties.RowEdit = this.teRoute;
            // 
            // erMinPath
            // 
            this.erMinPath.Height = 16;
            this.erMinPath.Name = "erMinPath";
            this.erMinPath.Properties.Caption = "Min ������ �� ���� ��� �������� ������ ������, �����";
            this.erMinPath.Properties.FieldName = "MinPathInFieldForCreateRecord";
            // 
            // erMinTime
            // 
            this.erMinTime.Name = "erMinTime";
            this.erMinTime.Properties.Caption = "Min ����� ������ � ���� ��� �������� ������ ������, ������";
            this.erMinTime.Properties.FieldName = "MinTimeInFieldForCreateRecord";
            this.erMinTime.Properties.RowEdit = this.teRoute;
            // 
            // erBounceSensor
            // 
            this.erBounceSensor.Name = "erBounceSensor";
            this.erBounceSensor.Properties.Caption = "������� ��������, ������";
            this.erBounceSensor.Properties.FieldName = "BounceSensor";
            this.erBounceSensor.Properties.RowEdit = this.teRoute;
            // 
            // erTimeOrderStart
            // 
            this.erTimeOrderStart.Name = "erTimeOrderStart";
            this.erTimeOrderStart.Properties.Caption = "����� ������ �������";
            this.erTimeOrderStart.Properties.FieldName = "TimeStartOrder";
            this.erTimeOrderStart.Properties.RowEdit = this.teTimeOrder;
            // 
            // erAgregatWidht
            // 
            this.erAgregatWidht.Name = "erAgregatWidht";
            this.erAgregatWidht.Properties.Caption = "������ �������� �� ���������, �";
            this.erAgregatWidht.Properties.FieldName = "AgregatDefaultWidth";
            // 
            // AgroSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 361);
            this.Controls.Add(this.prParams);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AgroSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� ������ AGRO";
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTimeEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl prParams;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teRoute;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crSattlement;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMinSquare;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMinPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMinTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erBounceSensor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit reTimeEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeOrderStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTimeOrder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erAgregatWidht;
    }
}