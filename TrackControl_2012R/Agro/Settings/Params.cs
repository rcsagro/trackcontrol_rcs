using TrackControl.General.DatabaseDriver;

/// <summary>
/// ����� ��� ������ � �������� ���������� ����������
/// </summary>
namespace Agro
{
    public static class Params
    {

        #region ���������

        public enum NUMPAR
        {
            SpeedMin = 1,
            SpeedMax,
            SpeedColorMin,
            SpeedColorNorm,
            SpeedColorMax,
            RouteRadius,
            RouteDelayForIdent,
            AgroMinSquarePersentForCreateRecord,
            AgroMinPathInFieldForCreateRecord,
            AgroMinTimeInFieldForCreateRecord,
            AgroBounceSensor,
            AgroTimeStartOrder,
            AgroAgregatDefaultWidth
        }

        public static string ReadParTotal(NUMPAR Number)
        {
            string sReturn = "";
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string SQLselect = AgroQuery.Params.SelectAgroParams + (int)Number;
                object ob = driverDb.GetScalarValue(SQLselect);

                if (ob == null)
                {
                    SQLselect = string.Format(AgroQuery.Params.InsertIntoAgroParams, (int)Number);
                    driverDb.ExecuteNonQueryCommand(SQLselect);
                }
                else
                {
                    sReturn = ob.ToString();
                }
            }
            return sReturn;
        }

        public static string ReadParTotal(NUMPAR Number, string sDefault)
        {
            string sReturn = "";
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string SQLselect = AgroQuery.Params.SelectAgroParamsValue + (int)Number;
                object ob = driverDb.GetScalarValue(SQLselect);

                if (ob == null)
                {
                    SQLselect = string.Format(AgroQuery.Params.InsertIntoAgroParamsValue, (int)Number, sDefault);
                    driverDb.ExecuteNonQueryCommand(SQLselect);
                    driverDb.CloseDbConnection();
                    sReturn = sDefault;
                }
                else
                {
                    driverDb.CloseDbConnection();
                    sReturn = ob.ToString();
                }
                driverDb.CloseDbConnection();
            }
            return sReturn;
        }

        public static void SetParTotal(NUMPAR Number, string sValue)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string SQLselect = AgroQuery.Params.SelectFromAgroParams + (int)Number;
                int iCount = driverDb.GetDataReaderRecordsCount(SQLselect);

                if (iCount == 0)
                {
                    SQLselect = string.Format(AgroQuery.Params.InsertIntoAgroParamsNumber, (int)Number, sValue);
                }

                if (iCount == 1)
                {
                    SQLselect = string.Format(AgroQuery.Params.UpdateAgroParams, sValue, (int)Number);
                }

                driverDb.ExecuteNonQueryCommand(SQLselect);
                driverDb.CloseDbConnection();
            }
            return;
        }

        #endregion

    }
}
