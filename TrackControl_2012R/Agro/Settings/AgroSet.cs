using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Agro.Properties; 

namespace Agro
{
    public partial class AgroSet : DevExpress.XtraEditors.XtraForm
    {
        public AgroSet()
        {
            InitializeComponent();
            Localization();
            AgroSetItem asi = new AgroSetItem();
            prParams.SelectedObject = asi;
        }
        #region �����������
        private void Localization()
        {
            crSattlement.Properties.Caption = Resources.SettingsOrderRecords;
            erMinSquare.Properties.Caption = Resources.SettingsMinPercent;
            erMinPath.Properties.Caption = Resources.SettingsMinRun;
            erMinTime.Properties.Caption = Resources.SettingsMinTime;
            erBounceSensor.Properties.Caption = Resources.SettingsSensorBounceM;
            erTimeOrderStart.Properties.Caption = Resources.TimeStartOrder;
            erAgregatWidht.Properties.Caption = string.Format("{0}, {1}", Resources.WidthDefaultAgregat, Resources.MeterShort);
            Text = Resources.SettingsApplication;
        }
        #endregion
    }
}