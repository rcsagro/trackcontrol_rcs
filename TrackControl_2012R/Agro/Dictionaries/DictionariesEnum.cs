﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Dictionaries
{
    public class DictionariesEnum
    {
        public enum Dictionaries
        {
            ГруппыВидовРабот,
            ВидыРабот,
            ГруппыНавесногоОборудования,
            НавесноеОборудование,
            ГруппыПолей,
            Поля,
            ЕдиницыИзмеренияПлощади,
            Культуры,
            ПричиныПростояТехники,
            Сезоны,
            Приказы,
            Расценки
            
        }

    }
}
