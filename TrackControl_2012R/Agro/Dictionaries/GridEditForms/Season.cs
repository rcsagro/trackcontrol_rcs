﻿using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid;
using System; 

namespace Agro.GridEditForms
{
    public partial class Season : FormSample
    {
        public Season()
        {
            InitThis();
        }
        public Season(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroSeason(_Id);
            this.Text = this.Text + ": " + Resources.Season;
            GetFields();
            bFormLoading = false;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsTextDoublePositive(teSquare, dxErrP) 
                || !DicUtilites.ValidateLookUp(leCulture, dxErrP)
                || !DicUtilites.ValidateDateEditPair(deStart,deEnd, dxErrP) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["DateStart"], deStart.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["DateEnd"], deEnd.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_culture"], (int)(leCulture.EditValue ?? 0));
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["SquarePlan"], teSquare.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
        }
        public override void SetFields()
        {
            di.Comment = meComment.Text;
            (di as DictionaryAgroSeason).SquarePlan = Convert.ToDecimal(teSquare.Text);
            (di as DictionaryAgroSeason).IdCulture = (int)(leCulture.EditValue ?? 0);
            (di as DictionaryAgroSeason).DateStart = (DateTime)deStart.EditValue;
            (di as DictionaryAgroSeason).DateEnd = (DateTime)deEnd.EditValue;
            (di as DictionaryAgroSeason).IsClosed = Convert.ToBoolean(chIsClosed.EditValue);
        }
        public override sealed void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            leCulture.EditValue = (di as DictionaryAgroSeason).IdCulture;
            teSquare.Text = (di as DictionaryAgroSeason).SquarePlan.ToString();
            deStart.EditValue = (di as DictionaryAgroSeason).DateStart;
            deEnd.EditValue = (di as DictionaryAgroSeason).DateEnd;
            chIsClosed.EditValue = (di as DictionaryAgroSeason).IsClosed;
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            DicUtilites.LookUpLoad(leCulture, AgroQuery.DictionaryAgroCulture.SelectAgroCultureList);
            txName.Enabled = false;
        }

        #region Локализация
        private void Localization()
        {
            //labelControl8.Text = Resources.WorkDefault;
            //leOutLink.Text = Resources.OuterBaseLink;
        }
        #endregion

        private void deStart_EditValueChanged(object sender, System.EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void deEnd_EditValueChanged(object sender, System.EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void leCulture_EditValueChanged(object sender, System.EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void teSquare_EditValueChanged(object sender, System.EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void chIsClosed_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}
