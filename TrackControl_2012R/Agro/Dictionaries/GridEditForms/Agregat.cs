using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace Agro.GridEditForms
{
    public partial class Agregat : FormSample
    {
 

        /// <summary>
        /// ������������ ��� ���������� ���������� �� ������ � ������ � �����
        /// </summary>
        DataView _dvClone = null;

        readonly List<int> _rowUpdated = new List<int>();

        public Agregat()
        {
            InitThis();
        }

        public Agregat( GridView gvDict, bool bNew )
            : base( gvDict, bNew )
        {
            InitThis();
            bFormLoading = true;
            di = new DictionaryAgroAgregat( _Id );
            this.Text = string.Format("{0}: {1}" ,this.Text , Resources.Agregat);

            init_GridView();

            if( !bNew ) 
                GetFields();

            bFormLoading = false;
        }

        public Agregat(int idAgregat)
        {
            _Id = idAgregat;
            InitThis();
            bFormLoading = true;
            di = new DictionaryAgroAgregat(_Id);
            this.Text = string.Format("{0}: {1}", this.Text, Resources.Agregat);
            init_GridView();
            GetFields();
            btSave.Enabled = false;
            bFormLoading = false;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void InitThis()
        {
            InitializeComponent();
            Localizations();
            leGroupe.Properties.DataSource = DictionaryAgroAgregat.GetGroupsList();
            leWork.DataSource = DictionaryAgroWorkType.GetList();
            leSensorAlgorithms.DataSource = DictionaryAgroAgregat.GetAgroAlgorithmList();
            btOrders.Visible = true;
            txId.Text = _Id.ToString();
            tarButtonEdit.ButtonPressed += TarButtonEditOnButtonPressed;
        }

        private void TarButtonEditOnButtonPressed(object sender, ButtonPressedEventArgs buttonPressedEventArgs)
        {
            var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
            if (dictionaryAgroAgregat != null)
            {
                var form = new ExcelFormDvx();
                //var bs = new BindingSource {DataSource = dictionaryAgroAgregat.GetCalibrationRecords()};
                //form.DataSource = dictionaryAgroAgregat.GetCalibrationRecords();
                //form.BindingSource = bs;
                form.AgregatIdentificator = dictionaryAgroAgregat.Id;
                form.ShowDialog(this);
            }
        }

        public override sealed void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;

            var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
            pgProperties.SelectedObject = dictionaryAgroAgregat;
            if (dictionaryAgroAgregat != null) leGroupe.EditValue = dictionaryAgroAgregat.IdMain;

            if( _Id != 0 ) 
                LoadContent();
        }

        private void Localizations()
        {
            crValueSensor.Properties.Caption = Resources.LocalSensorsSettings;// Resources.ValueSensor;
            crValueSensor.Properties.ToolTip = Resources.ValueSensorTip;
            erCoefficient.Properties.Caption = Resources.CoefficientK;
            erCoefficient.Properties.ToolTip = Resources.CoefficientKTip;
            erShifter.Properties.Caption = Resources.Shifter;
            erShifter.Properties.ToolTip = Resources.ShifterTip;
            erTarirovka.Properties.Caption = Resources.Tarirovka;
            erTarirovka.Properties.Caption = Resources.TarirovkaTip;
            crProperties.Properties.Caption = Resources.Properties;
            erIdentifier.Properties.Caption = Resources.Identifier;
            erWidth.Properties.Caption = string.Format( "{0},{1}", Resources.Width, Resources.Meters );
            erUseDefault.Properties.Caption = Resources.OrdersDefaultUse;
            erInvNumber.Properties.Caption = Resources.NumberInv;
            erWorkDefault.Properties.Caption = Resources.WorkDefault;
            erSensorPresent.Properties.Caption = Resources.PositionSensorPresent;
            //erSensorAngleValue.Properties.Caption = Resources.ThresholdAngleSensorValue;
            erOutId.Properties.Caption = Resources.OuterBaseLink;

            crSensor.Properties.Caption = Resources.SensorWorkingPosition;
            erIsUseAgregatState.Properties.Caption = Resources.UseLocalSensorsSettings;// Resources.UseSettingOperatingStatus;
            erSensorAllgorithm.Properties.Caption = Resources.SensorsAlgorithm;
            crSensorState.Properties.Caption = Resources.AdjustingDepthGaugeGoniometerStates;// Resources.StatusSensor;
            erMinValue.Properties.Caption = Resources.LowerLimit;
            erMaxValue.Properties.Caption = Resources.UpperLimit;
            crLogicState.Properties.Caption = Resources.AdjustingStatesLogicalSensor;// Resources.StatusLogicSensor;
            erLogicState.Properties.Caption = Resources.ActiveState; 

            lbGroup.Text = Resources.Groupe;
            lbVehicles.Text = Resources.VehicleDefaultUse;

            VID.Caption = Resources.Vehicle;
            TID.Caption = Resources.Groupe;
            SID.Caption = Resources.Sensor;
            StID.Caption = Resources.OperatingRange;
        }

        private bool ValidateIdentifierUnique()
        {
            ushort ident;

            var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
            if ( dictionaryAgroAgregat != null && ushort.TryParse( dictionaryAgroAgregat.Identifier.ToString(), out ident ) )
            {
                if ( ident == 0 ) 
                    return true;

                if ( DicUtilites.Test_UNIQUE( "agro_agregat", "Identifier", ident, Convert.ToInt32( txId.Text ) ) )
                {
                    return true;
                }
                else
                {
                    pgProperties.SetRowError( erIdentifier.Properties, Resources.ValueMustUnicum );
                    return false;
                }
            }
            else
                return true;
        }

        public override void UpdateGRN()
        {
            if (_gvDict == null) 
                return;
            int iRow = _gvDict.FocusedRowHandle;

            if ( ( di as DictionaryAgroAgregat ).Default )
            {
                for ( int i = 0; i <= _gvDict.RowCount; i++ )
                {
                    _gvDict.SetRowCellValue( i, _gvDict.Columns["Def"], 0 );
                }
            } // if

            _gvDict.FocusedRowHandle = iRow;
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id"], txId.Text );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Name"], txName.Text );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Comment"], meComment.Text );
            var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
            if (dictionaryAgroAgregat != null)
                _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Width"], dictionaryAgroAgregat.Width );
            var agroAgregat = di as DictionaryAgroAgregat;
            if (agroAgregat != null)
                _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Identifier"], agroAgregat.Identifier );
            var agregat = di as DictionaryAgroAgregat;
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Def"], agregat != null && agregat.Default );
            var dictionaryAgroAgregat1 = di as DictionaryAgroAgregat;
            if (dictionaryAgroAgregat1 != null)
                _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id_work"], dictionaryAgroAgregat1.IdWork );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id_main"], (int)( leGroupe.EditValue ?? 0 ) );
            var agroAgregat1 = di as DictionaryAgroAgregat;
            if (agroAgregat1 != null)
                _gvDict.SetRowCellValue( iRow, _gvDict.Columns["InvNumber"], agroAgregat1.InvNumber );
        }

        public override void SetFields()
        {
            di.Name = txName.Text;

            if ( di.Name.Length == 0 ) 
                return;

            di.Comment = meComment.Text;
            var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
            if (dictionaryAgroAgregat != null)
                dictionaryAgroAgregat.IdMain = (int)( leGroupe.EditValue ?? 0 );
        }

        public override bool ValidateFields()
        {
            if ( bFormLoading ) return false;
            if ( !DicUtilites.ValidateFieldsTextWFocus( txName, dxErrP ) | !ValidateIdentifierUnique() )
            {
                return false;
            }
            else
            {
                dxErrP.ClearErrors();
                return true;
            }
        }

        private void init_GridView()
        {
            if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
            {
                var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
                if (dictionaryAgroAgregat != null)
                    gcVehicles.DataSource = dictionaryAgroAgregat.GetContent();
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                var tbs = new DataTable();
                tbs.Columns.Add( "Id", typeof( int ) );
                tbs.Columns.Add( "TID", typeof( string ) );
                tbs.Columns.Add( "VID", typeof( int ) );
                tbs.Columns.Add( "SID", typeof( string ) );
                tbs.Columns.Add( "StID", typeof(string));

                var dictionaryAgroAgregat = di as DictionaryAgroAgregat;
                if (dictionaryAgroAgregat != null)
                {
                    DataTable dtbs = dictionaryAgroAgregat.GetContent();

                    for (int i = 0; i < dtbs.Rows.Count; i++)
                    {
                        DataRow row = tbs.NewRow();
                        row["Id"] = dtbs.Rows[i][0];
                        row["TID"] = dtbs.Rows[i][1];
                        row["VID"] = dtbs.Rows[i][2];
                        row["SID"] = dtbs.Rows[i][3];
                        row["StID"] = dtbs.Rows[i][3];
                        tbs.Rows.Add(row);
                    }
                }

                gcVehicles.DataSource = tbs;
            }

            // leVehicleGrope
            rleTeam.ValueMember = "Id";
            rleTeam.DisplayMember = "Name";
            LookUpColumnInfoCollection coll1 = rleTeam.Columns;
            coll1.Add( new LookUpColumnInfo( "Name", "�������� ������" ) );
            rleTeam.BestFitMode = BestFitMode.BestFitResizePopup;
            rleTeam.SearchMode = SearchMode.AutoComplete;
            rleTeam.AutoSearchColumnIndex = 0;
            rleTeam.NullText = Resources.SelectGroup;

            // leVehicle
            rleVehicles.ValueMember = "Id";
            rleVehicles.DisplayMember = "Name";
            LookUpColumnInfoCollection coll2 = rleVehicles.Columns;
            coll2.Add( new LookUpColumnInfo( "Name", "��������" ) );
            rleVehicles.BestFitMode = BestFitMode.BestFitResizePopup;
            rleVehicles.SearchMode = SearchMode.AutoComplete;
            rleVehicles.AutoSearchColumnIndex = 0;
            rleVehicles.NullText = Resources.SelectVehicle;

            // leSensors
            rleSensors.ValueMember = "id";
            rleSensors.DisplayMember = "Name";
            LookUpColumnInfoCollection coll3 = rleSensors.Columns;
            coll3.Add( new LookUpColumnInfo( "Name", "�������" ) );
            rleSensors.BestFitMode = BestFitMode.BestFitResizePopup;
            rleSensors.SearchMode = SearchMode.AutoComplete;
            rleSensors.AutoSearchColumnIndex = 0;
            rleSensors.NullText = Resources.SelectSensor;
            rleSensors.ButtonClick += new ButtonPressedEventHandler( LookUpEdit3_ButtonClick );

            rleSensors.Columns[0].Caption = Resources.NameRes;
            rleVehicles.Columns[0].Caption = Resources.NameRes;
            rleTeam.Columns[0].Caption = Resources.NameRes;

            // rleStates
            rleStates.ValueMember = "Id";
            rleStates.DisplayMember = "Name";
            LookUpColumnInfoCollection coll4 = rleStates.Columns;
            coll4.Add(new LookUpColumnInfo("Name", "���������"));
            rleStates.BestFitMode = BestFitMode.BestFitResizePopup;
            rleStates.SearchMode = SearchMode.AutoComplete;
            rleStates.AutoSearchColumnIndex = 0;
            rleStates.NullText = Resources.SelectState;

            gvVehicles.ShownEditor += new EventHandler( gvVehicles_ShownEditor );
            gvVehicles.HiddenEditor += new EventHandler( gvVehicles_HiddenEditor );
            gvVehicles.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( gvVehicles_CellValueChanged );
            gcVehicles.EmbeddedNavigator.ButtonClick +=
                new NavigatorButtonClickEventHandler( EmbeddedNavigator_ButtonClick );
        }

        void gvVehicles_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            if ( e.Column.FieldName == "TID" )
            {
                int idRecord = 0;

                if ( !Int32.TryParse( gvVehicles.GetRowCellValue( e.RowHandle, gvVehicles.Columns["Id"] ).ToString(), out idRecord ) )
                {
                    gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["Id"], -1 );
                    gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["VID"], -1 );
                }
            }
            else if ( e.Column.FieldName == "VID" )
            {
                int idVehicle = 0;
                if ( Int32.TryParse( gvVehicles.GetRowCellValue( e.RowHandle, gvVehicles.Columns["VID"] ).ToString(), out idVehicle ) )
                {
                    if ( idVehicle > 0 )
                    {
                        gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["SID"], 0 );
                        ValidateFieldsAction();
                        bContentChange = true;
                        if ( e.RowHandle >= 0 && !_rowUpdated.Contains( e.RowHandle ) )
                            _rowUpdated.Add( e.RowHandle );
                    }
                }
            }
            else if ( e.Column.FieldName == "SID" )
            {
                ValidateFieldsAction();
                bContentChange = true;
                if ( e.RowHandle >= 0 && !_rowUpdated.Contains( e.RowHandle ) )
                    _rowUpdated.Add( e.RowHandle );
            }
            else if (e.Column.FieldName == "StID")
            {
                ValidateFieldsAction();
                bContentChange = true;
                if (e.RowHandle >= 0 && !_rowUpdated.Contains(e.RowHandle))
                    _rowUpdated.Add(e.RowHandle);
            }
        }

        private void EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if ( e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove )
            {
                int idRecord = 0;

                if ( Int32.TryParse( gvVehicles.GetRowCellValue( gvVehicles.FocusedRowHandle, gvVehicles.Columns["Id"] ).ToString(), out idRecord ) )
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName,
                                                                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                                                MessageBoxDefaultButton.Button2))
                    {
                        (di as DictionaryAgroAgregat).DeleteContent(idRecord);

                        e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void gvVehicles_HiddenEditor(object sender, EventArgs e)
        {
            if ( _dvClone != null )
            {
                _dvClone.Dispose();
                _dvClone = null;
            }
        }

        private void gvVehicles_ShownEditor(object sender, EventArgs e)
        {
            DataRow row = gvVehicles.GetDataRow(gvVehicles.FocusedRowHandle);
            int valueForFilter = 0;
            string filter = "";
            if( gvVehicles.FocusedColumn.FieldName == "VID" && gvVehicles.ActiveEditor is LookUpEdit )
            {
                if (!Int32.TryParse(row["TID"].ToString(), out valueForFilter))
                    return;
                filter = "[Team_id]=" + valueForFilter.ToString();
            }
            else if( gvVehicles.FocusedColumn.FieldName == "SID" && gvVehicles.ActiveEditor is LookUpEdit )
            {
                if (!Int32.TryParse(row["VID"].ToString(), out valueForFilter))
                    return;
                filter = "[VID]=" + valueForFilter.ToString();
            }
            else if (gvVehicles.FocusedColumn.FieldName == "StID" && gvVehicles.ActiveEditor is LookUpEdit)
            {
                if (!Int32.TryParse(row["SID"].ToString(), out valueForFilter))
                    return;
                filter = "[SID]=" + valueForFilter.ToString();
            }
            if (filter.Length > 0)
            {
                var edit = (LookUpEdit)gvVehicles.ActiveEditor;
                var table = edit.Properties.DataSource as DataTable;
                _dvClone = new DataView(table) { RowFilter = filter };
                edit.Properties.DataSource = _dvClone;
            }
        }

        private void LookUpEdit3_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            ( sender as LookUpEdit ).EditValue = null;
            ( sender as LookUpEdit ).Text = "";
        }

        public override bool LoadContent()
        {
            try
            {
                rleTeam.DataSource = GetVehicleGroup();
                rleVehicles.DataSource = GetListVehicle();
                rleSensors.DataSource = GetListAgregatSensors();
                rleStates.DataSource = GetListStates();

                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("{0}", ex.Message));
                return false;
            }
        }

        private object GetListAgregatSensors()
        {
            using( var db = new DriverDb() )
            {
                db.ConnectDb();

                string sSql = AgroQuery.Agregat.SelectSensors;
                return db.GetDataTable( sSql );
            }
        }

        private object GetListVehicle()
        {
            using( var db = new DriverDb() )
            {
                db.ConnectDb();

                string sSql = AgroQuery.Agregat.SelectVehicle;
                return db.GetDataTable( sSql );
            }
        }

        private object GetListStates()
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sSql = AgroQuery.Agregat.SelectStates;
                return db.GetDataTable(sSql);
            }
        }

        private object GetVehicleGroup()
        {
            IList<VehiclesGroup> listgroup = new List<VehiclesGroup>();

            listgroup = OrderItem.VehiclesModel.RootGroupsWithItems();

            return listgroup;
        }

        private void leGroupe_ButtonClick( object sender, ButtonPressedEventArgs e )
        {
            leGroupe.EditValue = null;
            leGroupe.Text = "";
        }

        private void leGroupe_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void pgProperties_CellValueChanged( object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e )
        {
            ValidateFieldsAction();
        }

        public override bool SaveContent()
        {
            try
            {
                SaveRows();
                return true;
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message );
                return false;
            }
        }

        private void SaveRows()
        {
            for( int i = 0; i < gvVehicles.RowCount; i++ )
            {
                int idRecord = 0;

                if( Int32.TryParse( gvVehicles.GetRowCellValue( i, gvVehicles.Columns["Id"] ).ToString(), out idRecord ) )
                {
                    if( idRecord < 0 )
                    {
                        SaveRow( i );
                    }
                    else if( _rowUpdated.Count > 0 && _rowUpdated.Contains( i ) )
                    {
                        SaveRow( i );
                        _rowUpdated.Remove( i );
                    }
                }
            }
        }

        private void SaveRow( int row )
        {
            int idRecord = 0;
            int idVehical = 0;
            int idSensor = 0;
            int idState = 0;

            if( !Int32.TryParse( gvVehicles.GetRowCellValue( row, gvVehicles.Columns["VID"] ).ToString(), out idVehical ) || idVehical == -1 ) 
                return;

            Int32.TryParse( gvVehicles.GetRowCellValue( row, gvVehicles.Columns["SID"] ).ToString(), out idSensor );
            Int32.TryParse(gvVehicles.GetRowCellValue(row, gvVehicles.Columns["StID"]).ToString(), out idState);

            if( Int32.TryParse( gvVehicles.GetRowCellValue( row, gvVehicles.Columns["Id"] ).ToString(), out idRecord ) )
            {
                string linkedAgregatName;

                if( !( di as DictionaryAgroAgregat ).GetLinkForVehicle( idVehical, out linkedAgregatName ) )
                {
                    if( idRecord > 0 )
                        (di as DictionaryAgroAgregat).EditContent(idRecord, idVehical, idSensor, idState);
                    else
                        gvVehicles.SetRowCellValue(row, gvVehicles.Columns["Id"], (di as DictionaryAgroAgregat).AddContent(idVehical, idSensor, idState));
                }
                else
                    XtraMessageBox.Show( string.Format( "{0}{1}{1}{2}", Resources.RecordDontSave, Environment.NewLine, linkedAgregatName ), Resources.ApplicationName );
            }
        }


    }
}