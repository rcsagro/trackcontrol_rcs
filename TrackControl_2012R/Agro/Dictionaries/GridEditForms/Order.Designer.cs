namespace Agro.GridEditForms
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            Free_dsAtlanta();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Columns.GridColumn FuelStartD;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.gvFuelDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_node = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStartD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEndD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeRateD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FactSquareD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelAddD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelSubD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelEndD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelExpensD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensMoveD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensStopD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensTotalD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFuel = new DevExpress.XtraGrid.GridControl();
            this.gbvFuel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.IdF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ZoneID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ZoneName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelAdd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelSub = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpens = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpensAvgRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Fuel_ExpensMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensAvgRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStartF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeEndF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeRateF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquareF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.gvContentPrice = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.FSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcContent = new DevExpress.XtraGrid.GridControl();
            this.gbvContent = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.���� = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Id = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FieldId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.GroupeFieldId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.GroupeField = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Field = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Square = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TimeStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeOut = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DistanceT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquareCalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpeedAvgField = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Zone_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PriceC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDateInit = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDriver = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMobitel = new System.Windows.Forms.ComboBox();
            this.lbGroupe = new System.Windows.Forms.Label();
            this.cbGroupe = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txComment = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txAgregatWidth = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbAgregat = new System.Windows.Forms.ComboBox();
            this.gbHandle = new System.Windows.Forms.GroupBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cbField = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbGroupeField = new System.Windows.Forms.ComboBox();
            this.btRecalcFuel = new System.Windows.Forms.Button();
            this.btRecalcContent = new System.Windows.Forms.Button();
            this.errP = new System.Windows.Forms.ErrorProvider(this.components);
            this.btCancel = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.gbSelectType = new System.Windows.Forms.GroupBox();
            this.rbClose = new System.Windows.Forms.RadioButton();
            this.rbAuto = new System.Windows.Forms.RadioButton();
            this.rbHandle = new System.Windows.Forms.RadioButton();
            this.gbPrice = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbWorkType = new System.Windows.Forms.ComboBox();
            this.gbAuto = new System.Windows.Forms.GroupBox();
            this.btSeek = new System.Windows.Forms.Button();
            this.bwCreateData = new System.ComponentModel.BackgroundWorker();
            this.strpOrder = new System.Windows.Forms.StatusStrip();
            this.tspbZone = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbOrder = new System.Windows.Forms.ToolStripStatusLabel();
            this.tspbOrder = new System.Windows.Forms.ToolStripProgressBar();
            this.taDataView = new LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter();
            this.gcMain = new DevExpress.XtraGrid.GridControl();
            this.gvMain = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.LocationStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStartM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEndM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeMoveM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Distance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btMapView = new System.Windows.Forms.Button();
            FuelStartD = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDetal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbHandle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errP)).BeginInit();
            this.gbSelectType.SuspendLayout();
            this.gbPrice.SuspendLayout();
            this.gbAuto.SuspendLayout();
            this.strpOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMain)).BeginInit();
            this.SuspendLayout();
            // 
            // FuelStartD
            // 
            FuelStartD.AppearanceHeader.Options.UseTextOptions = true;
            FuelStartD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            FuelStartD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            FuelStartD.Caption = "������� � ������, �";
            FuelStartD.FieldName = "FuelStart";
            FuelStartD.Name = "FuelStartD";
            FuelStartD.Visible = true;
            FuelStartD.VisibleIndex = 4;
            FuelStartD.Width = 115;
            // 
            // gvFuelDetal
            // 
            this.gvFuelDetal.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvFuelDetal.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gvFuelDetal.ColumnPanelRowHeight = 40;
            this.gvFuelDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdD,
            this.Id_node,
            this.TimeStartD,
            this.TimeEndD,
            this.TimeRateD,
            this.FactSquareD,
            FuelStartD,
            this.FuelAddD,
            this.FuelSubD,
            this.FuelEndD,
            this.FuelExpensD,
            this.Fuel_ExpensMoveD,
            this.Fuel_ExpensStopD,
            this.Fuel_ExpensTotalD});
            this.gvFuelDetal.GridControl = this.gcFuel;
            this.gvFuelDetal.Name = "gvFuelDetal";
            this.gvFuelDetal.OptionsBehavior.Editable = false;
            this.gvFuelDetal.OptionsView.ShowGroupPanel = false;
            // 
            // IdD
            // 
            this.IdD.Caption = "IdD";
            this.IdD.FieldName = "Id";
            this.IdD.Name = "IdD";
            // 
            // Id_node
            // 
            this.Id_node.Caption = "Id_node";
            this.Id_node.FieldName = "Id_node";
            this.Id_node.Name = "Id_node";
            // 
            // TimeStartD
            // 
            this.TimeStartD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStartD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStartD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStartD.Caption = "�����";
            this.TimeStartD.DisplayFormat.FormatString = "t";
            this.TimeStartD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStartD.FieldName = "TimeStart";
            this.TimeStartD.Name = "TimeStartD";
            this.TimeStartD.Visible = true;
            this.TimeStartD.VisibleIndex = 0;
            this.TimeStartD.Width = 61;
            // 
            // TimeEndD
            // 
            this.TimeEndD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEndD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEndD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEndD.Caption = "�����";
            this.TimeEndD.DisplayFormat.FormatString = "t";
            this.TimeEndD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndD.FieldName = "TimeEnd";
            this.TimeEndD.Name = "TimeEndD";
            this.TimeEndD.Visible = true;
            this.TimeEndD.VisibleIndex = 1;
            this.TimeEndD.Width = 70;
            // 
            // TimeRateD
            // 
            this.TimeRateD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRateD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRateD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRateD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRateD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRateD.Caption = "��������";
            this.TimeRateD.FieldName = "TimeRate";
            this.TimeRateD.Name = "TimeRateD";
            this.TimeRateD.Visible = true;
            this.TimeRateD.VisibleIndex = 2;
            this.TimeRateD.Width = 78;
            // 
            // FactSquareD
            // 
            this.FactSquareD.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareD.Caption = "���������, ��";
            this.FactSquareD.FieldName = "FactSquare";
            this.FactSquareD.Name = "FactSquareD";
            this.FactSquareD.Visible = true;
            this.FactSquareD.VisibleIndex = 3;
            this.FactSquareD.Width = 106;
            // 
            // FuelAddD
            // 
            this.FuelAddD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAddD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAddD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAddD.Caption = "����������, �";
            this.FuelAddD.FieldName = "FuelAdd";
            this.FuelAddD.Name = "FuelAddD";
            this.FuelAddD.Visible = true;
            this.FuelAddD.VisibleIndex = 5;
            this.FuelAddD.Width = 104;
            // 
            // FuelSubD
            // 
            this.FuelSubD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSubD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSubD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSubD.Caption = "�����, �";
            this.FuelSubD.FieldName = "FuelSub";
            this.FuelSubD.Name = "FuelSubD";
            this.FuelSubD.Visible = true;
            this.FuelSubD.VisibleIndex = 6;
            this.FuelSubD.Width = 76;
            // 
            // FuelEndD
            // 
            this.FuelEndD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEndD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEndD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEndD.Caption = "������� � �����, �";
            this.FuelEndD.FieldName = "FuelEnd";
            this.FuelEndD.Name = "FuelEndD";
            this.FuelEndD.Visible = true;
            this.FuelEndD.VisibleIndex = 7;
            this.FuelEndD.Width = 110;
            // 
            // FuelExpensD
            // 
            this.FuelExpensD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensD.Caption = "����� ������, �";
            this.FuelExpensD.FieldName = "FuelExpens";
            this.FuelExpensD.Name = "FuelExpensD";
            this.FuelExpensD.Visible = true;
            this.FuelExpensD.VisibleIndex = 8;
            this.FuelExpensD.Width = 108;
            // 
            // Fuel_ExpensMoveD
            // 
            this.Fuel_ExpensMoveD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMoveD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMoveD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensMoveD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMoveD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMoveD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMoveD.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMoveD.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMoveD.Name = "Fuel_ExpensMoveD";
            this.Fuel_ExpensMoveD.Visible = true;
            this.Fuel_ExpensMoveD.VisibleIndex = 9;
            this.Fuel_ExpensMoveD.Width = 113;
            // 
            // Fuel_ExpensStopD
            // 
            this.Fuel_ExpensStopD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStopD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStopD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensStopD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStopD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStopD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStopD.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStopD.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStopD.Name = "Fuel_ExpensStopD";
            this.Fuel_ExpensStopD.Visible = true;
            this.Fuel_ExpensStopD.VisibleIndex = 10;
            this.Fuel_ExpensStopD.Width = 118;
            // 
            // Fuel_ExpensTotalD
            // 
            this.Fuel_ExpensTotalD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotalD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotalD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensTotalD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotalD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotalD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotalD.Caption = "����� ������, �";
            this.Fuel_ExpensTotalD.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotalD.Name = "Fuel_ExpensTotalD";
            this.Fuel_ExpensTotalD.Visible = true;
            this.Fuel_ExpensTotalD.VisibleIndex = 11;
            this.Fuel_ExpensTotalD.Width = 119;
            // 
            // gcFuel
            // 
            this.gcFuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.LevelTemplate = this.gvFuelDetal;
            gridLevelNode1.RelationName = "DetalTime";
            this.gcFuel.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcFuel.Location = new System.Drawing.Point(11, 507);
            this.gcFuel.MainView = this.gbvFuel;
            this.gcFuel.Name = "gcFuel";
            this.gcFuel.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1});
            this.gcFuel.Size = new System.Drawing.Size(1199, 223);
            this.gcFuel.TabIndex = 30;
            this.gcFuel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gbvFuel,
            this.gvFuelDetal});
            // 
            // gbvFuel
            // 
            this.gbvFuel.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gbvFuel.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand5});
            this.gbvFuel.ColumnPanelRowHeight = 40;
            this.gbvFuel.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.IdF,
            this.ZoneID,
            this.ZoneName,
            this.TimeStartF,
            this.TimeEndF,
            this.TimeRateF,
            this.FactSquareF,
            this.FuelStart,
            this.FuelAdd,
            this.FuelSub,
            this.FuelEnd,
            this.FuelExpens,
            this.FuelExpensAvg,
            this.FuelExpensAvgRate,
            this.Fuel_ExpensMove,
            this.Fuel_ExpensStop,
            this.Fuel_ExpensTotal,
            this.Fuel_ExpensAvg,
            this.Fuel_ExpensAvgRate});
            this.gbvFuel.GridControl = this.gcFuel;
            this.gbvFuel.Name = "gbvFuel";
            this.gbvFuel.OptionsBehavior.Editable = false;
            this.gbvFuel.OptionsView.ShowGroupPanel = false;
            this.gbvFuel.ShowGridMenu += new DevExpress.XtraGrid.Views.Grid.GridMenuEventHandler(this.gbvFuel_ShowGridMenu);
            this.gbvFuel.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gbvFuel_RowStyle);
            // 
            // gridBand4
            // 
            this.gridBand4.Columns.Add(this.IdF);
            this.gridBand4.Columns.Add(this.ZoneID);
            this.gridBand4.Columns.Add(this.ZoneName);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 89;
            // 
            // IdF
            // 
            this.IdF.AppearanceHeader.Options.UseTextOptions = true;
            this.IdF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IdF.AutoFillDown = true;
            this.IdF.Caption = "IdF";
            this.IdF.FieldName = "IdF";
            this.IdF.Name = "IdF";
            this.IdF.OptionsColumn.ReadOnly = true;
            // 
            // ZoneID
            // 
            this.ZoneID.AppearanceHeader.Options.UseTextOptions = true;
            this.ZoneID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ZoneID.AutoFillDown = true;
            this.ZoneID.Caption = "ZoneID";
            this.ZoneID.FieldName = "ZoneID";
            this.ZoneID.Name = "ZoneID";
            this.ZoneID.OptionsColumn.ReadOnly = true;
            this.ZoneID.Width = 80;
            // 
            // ZoneName
            // 
            this.ZoneName.AppearanceHeader.Options.UseTextOptions = true;
            this.ZoneName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ZoneName.AutoFillDown = true;
            this.ZoneName.Caption = "����������� ����";
            this.ZoneName.FieldName = "ZoneName";
            this.ZoneName.Name = "ZoneName";
            this.ZoneName.Visible = true;
            this.ZoneName.Width = 89;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "������ ������ ������� (���)";
            this.gridBand1.Columns.Add(this.FuelStart);
            this.gridBand1.Columns.Add(this.FuelAdd);
            this.gridBand1.Columns.Add(this.FuelSub);
            this.gridBand1.Columns.Add(this.FuelEnd);
            this.gridBand1.Columns.Add(this.FuelExpens);
            this.gridBand1.Columns.Add(this.FuelExpensAvg);
            this.gridBand1.Columns.Add(this.FuelExpensAvgRate);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 570;
            // 
            // FuelStart
            // 
            this.FuelStart.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelStart.AutoFillDown = true;
            this.FuelStart.Caption = "������� � ������, �";
            this.FuelStart.FieldName = "FuelStart";
            this.FuelStart.Name = "FuelStart";
            this.FuelStart.Visible = true;
            this.FuelStart.Width = 73;
            // 
            // FuelAdd
            // 
            this.FuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAdd.AutoFillDown = true;
            this.FuelAdd.Caption = "����������, �";
            this.FuelAdd.FieldName = "FuelAdd";
            this.FuelAdd.Name = "FuelAdd";
            this.FuelAdd.Visible = true;
            this.FuelAdd.Width = 81;
            // 
            // FuelSub
            // 
            this.FuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSub.AutoFillDown = true;
            this.FuelSub.Caption = "�����, �";
            this.FuelSub.FieldName = "FuelSub";
            this.FuelSub.Name = "FuelSub";
            this.FuelSub.Visible = true;
            this.FuelSub.Width = 62;
            // 
            // FuelEnd
            // 
            this.FuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEnd.AutoFillDown = true;
            this.FuelEnd.Caption = "������� � �����, �";
            this.FuelEnd.FieldName = "FuelEnd";
            this.FuelEnd.Name = "FuelEnd";
            this.FuelEnd.Visible = true;
            this.FuelEnd.Width = 68;
            // 
            // FuelExpens
            // 
            this.FuelExpens.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpens.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpens.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpens.AutoFillDown = true;
            this.FuelExpens.Caption = "����� ������, �";
            this.FuelExpens.FieldName = "FuelExpens";
            this.FuelExpens.Name = "FuelExpens";
            this.FuelExpens.Visible = true;
            this.FuelExpens.Width = 89;
            // 
            // FuelExpensAvg
            // 
            this.FuelExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvg.AutoFillDown = true;
            this.FuelExpensAvg.Caption = "�������  ������, �/��";
            this.FuelExpensAvg.FieldName = "FuelExpensAvg";
            this.FuelExpensAvg.Name = "FuelExpensAvg";
            this.FuelExpensAvg.Visible = true;
            this.FuelExpensAvg.Width = 71;
            // 
            // FuelExpensAvgRate
            // 
            this.FuelExpensAvgRate.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvgRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvgRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvgRate.AutoFillDown = true;
            this.FuelExpensAvgRate.Caption = "������� ������ �/�������";
            this.FuelExpensAvgRate.FieldName = "FuelExpensAvgRate";
            this.FuelExpensAvgRate.Name = "FuelExpensAvgRate";
            this.FuelExpensAvgRate.Visible = true;
            this.FuelExpensAvgRate.Width = 126;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "������ ������� ������� (���/CAN)";
            this.gridBand5.Columns.Add(this.Fuel_ExpensMove);
            this.gridBand5.Columns.Add(this.Fuel_ExpensStop);
            this.gridBand5.Columns.Add(this.Fuel_ExpensTotal);
            this.gridBand5.Columns.Add(this.Fuel_ExpensAvg);
            this.gridBand5.Columns.Add(this.Fuel_ExpensAvgRate);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 519;
            // 
            // Fuel_ExpensMove
            // 
            this.Fuel_ExpensMove.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMove.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMove.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensMove.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMove.AutoFillDown = true;
            this.Fuel_ExpensMove.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMove.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Name = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Visible = true;
            this.Fuel_ExpensMove.Width = 106;
            // 
            // Fuel_ExpensStop
            // 
            this.Fuel_ExpensStop.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStop.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStop.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensStop.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStop.AutoFillDown = true;
            this.Fuel_ExpensStop.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStop.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Name = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Visible = true;
            this.Fuel_ExpensStop.Width = 103;
            // 
            // Fuel_ExpensTotal
            // 
            this.Fuel_ExpensTotal.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotal.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotal.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotal.AutoFillDown = true;
            this.Fuel_ExpensTotal.Caption = "����� ������, �";
            this.Fuel_ExpensTotal.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Name = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Visible = true;
            this.Fuel_ExpensTotal.Width = 92;
            // 
            // Fuel_ExpensAvg
            // 
            this.Fuel_ExpensAvg.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensAvg.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensAvg.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvg.AutoFillDown = true;
            this.Fuel_ExpensAvg.Caption = "������� ������, �/��";
            this.Fuel_ExpensAvg.FieldName = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Name = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Visible = true;
            this.Fuel_ExpensAvg.Width = 89;
            // 
            // Fuel_ExpensAvgRate
            // 
            this.Fuel_ExpensAvgRate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensAvgRate.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensAvgRate.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensAvgRate.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvgRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvgRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvgRate.AutoFillDown = true;
            this.Fuel_ExpensAvgRate.Caption = "������� ������, �/�������";
            this.Fuel_ExpensAvgRate.FieldName = "Fuel_ExpensAvgRate";
            this.Fuel_ExpensAvgRate.Name = "Fuel_ExpensAvgRate";
            this.Fuel_ExpensAvgRate.Visible = true;
            this.Fuel_ExpensAvgRate.Width = 129;
            // 
            // TimeStartF
            // 
            this.TimeStartF.Caption = "TimeStartF";
            this.TimeStartF.FieldName = "TimeStart";
            this.TimeStartF.Name = "TimeStartF";
            // 
            // TimeEndF
            // 
            this.TimeEndF.Caption = "TimeEndF";
            this.TimeEndF.FieldName = "TimeEnd";
            this.TimeEndF.Name = "TimeEndF";
            // 
            // TimeRateF
            // 
            this.TimeRateF.Caption = "TimeRateF";
            this.TimeRateF.FieldName = "TimeRate";
            this.TimeRateF.Name = "TimeRateF";
            // 
            // FactSquareF
            // 
            this.FactSquareF.Caption = "FactSquareF";
            this.FactSquareF.FieldName = "FactSquare";
            this.FactSquareF.Name = "FactSquareF";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Appearance.BackColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ShowTitle = true;
            this.repositoryItemProgressBar1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            // 
            // gvContentPrice
            // 
            this.gvContentPrice.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdP,
            this.Work,
            this.FSP,
            this.Price,
            this.Sum,
            this.Comment});
            this.gvContentPrice.GridControl = this.gcContent;
            this.gvContentPrice.Name = "gvContentPrice";
            this.gvContentPrice.OptionsView.ShowGroupPanel = false;
            this.gvContentPrice.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvContentPrice_CellValueChanged);
            // 
            // IdP
            // 
            this.IdP.Caption = "IdP";
            this.IdP.FieldName = "IdP";
            this.IdP.Name = "IdP";
            this.IdP.OptionsColumn.AllowEdit = false;
            // 
            // Work
            // 
            this.Work.AppearanceHeader.Options.UseTextOptions = true;
            this.Work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Work.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Work.Caption = "��� �����";
            this.Work.ColumnEdit = this.WorkLookUp;
            this.Work.FieldName = "Work";
            this.Work.Name = "Work";
            this.Work.Visible = true;
            this.Work.VisibleIndex = 0;
            // 
            // WorkLookUp
            // 
            this.WorkLookUp.AutoHeight = false;
            this.WorkLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��� �����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.WorkLookUp.DisplayMember = "Name";
            this.WorkLookUp.DropDownRows = 10;
            this.WorkLookUp.Name = "WorkLookUp";
            this.WorkLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.WorkLookUp.ValueMember = "Id";
            // 
            // FSP
            // 
            this.FSP.AppearanceHeader.Options.UseTextOptions = true;
            this.FSP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FSP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FSP.Caption = "���������� (�� �������), ��";
            this.FSP.FieldName = "FSP";
            this.FSP.Name = "FSP";
            this.FSP.OptionsColumn.AllowEdit = false;
            this.FSP.Visible = true;
            this.FSP.VisibleIndex = 1;
            // 
            // Price
            // 
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Price.Caption = "��������, ���";
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 2;
            // 
            // Sum
            // 
            this.Sum.AppearanceHeader.Options.UseTextOptions = true;
            this.Sum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sum.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Sum.Caption = "�����, ���";
            this.Sum.FieldName = "Sum";
            this.Sum.Name = "Sum";
            this.Sum.OptionsColumn.AllowEdit = false;
            this.Sum.Visible = true;
            this.Sum.VisibleIndex = 3;
            // 
            // Comment
            // 
            this.Comment.Caption = "����������";
            this.Comment.FieldName = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Visible = true;
            this.Comment.VisibleIndex = 4;
            // 
            // gcContent
            // 
            this.gcContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode2.LevelTemplate = this.gvContentPrice;
            gridLevelNode2.RelationName = "Price";
            this.gcContent.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gcContent.Location = new System.Drawing.Point(12, 328);
            this.gcContent.MainView = this.gbvContent;
            this.gcContent.Name = "gcContent";
            this.gcContent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemGridLookUpEdit1,
            this.WorkLookUp});
            this.gcContent.Size = new System.Drawing.Size(1199, 173);
            this.gcContent.TabIndex = 24;
            this.gcContent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gbvContent,
            this.gvContentPrice});
            // 
            // gbvContent
            // 
            this.gbvContent.ActiveFilterEnabled = false;
            this.gbvContent.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.����,
            this.gridBand2,
            this.gridBand3});
            this.gbvContent.ColumnPanelRowHeight = 40;
            this.gbvContent.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Id,
            this.GroupeFieldId,
            this.GroupeField,
            this.FieldId,
            this.Field,
            this.Zone_ID,
            this.Square,
            this.TimeStart,
            this.TimeEnd,
            this.FactTime,
            this.TimeMove,
            this.TimeStop,
            this.TimeOut,
            this.TimeRate,
            this.DistanceT,
            this.FactSquare,
            this.FactSquareCalc,
            this.PriceC,
            this.SpeedAvgField});
            this.gbvContent.GridControl = this.gcContent;
            this.gbvContent.Name = "gbvContent";
            this.gbvContent.OptionsView.ShowGroupPanel = false;
            this.gbvContent.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gbvContent_CellValueChanged);
            this.gbvContent.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gbvContent_MouseDown);
            // 
            // ����
            // 
            this.����.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.����.AppearanceHeader.Options.UseFont = true;
            this.����.AppearanceHeader.Options.UseTextOptions = true;
            this.����.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.����.AutoFillDown = false;
            this.����.Caption = "����";
            this.����.Columns.Add(this.Id);
            this.����.Columns.Add(this.FieldId);
            this.����.Columns.Add(this.GroupeFieldId);
            this.����.Columns.Add(this.GroupeField);
            this.����.Columns.Add(this.Field);
            this.����.Columns.Add(this.Square);
            this.����.Name = "����";
            this.����.Width = 205;
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id.AutoFillDown = true;
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.ReadOnly = true;
            this.Id.Width = 20;
            // 
            // FieldId
            // 
            this.FieldId.AppearanceHeader.Options.UseTextOptions = true;
            this.FieldId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FieldId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FieldId.AutoFillDown = true;
            this.FieldId.Caption = "FieldId";
            this.FieldId.FieldName = "FieldId";
            this.FieldId.Name = "FieldId";
            this.FieldId.OptionsColumn.AllowEdit = false;
            this.FieldId.OptionsColumn.ReadOnly = true;
            this.FieldId.Width = 62;
            // 
            // GroupeFieldId
            // 
            this.GroupeFieldId.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeFieldId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeFieldId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GroupeFieldId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GroupeFieldId.AutoFillDown = true;
            this.GroupeFieldId.Caption = "GroupeFieldId";
            this.GroupeFieldId.FieldName = "GroupeFieldId";
            this.GroupeFieldId.Name = "GroupeFieldId";
            this.GroupeFieldId.OptionsColumn.AllowEdit = false;
            this.GroupeFieldId.OptionsColumn.ReadOnly = true;
            this.GroupeFieldId.Width = 56;
            // 
            // GroupeField
            // 
            this.GroupeField.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeField.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GroupeField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GroupeField.AutoFillDown = true;
            this.GroupeField.Caption = "������ �����";
            this.GroupeField.FieldName = "GroupeField";
            this.GroupeField.Name = "GroupeField";
            this.GroupeField.OptionsColumn.AllowEdit = false;
            this.GroupeField.OptionsColumn.ReadOnly = true;
            this.GroupeField.Visible = true;
            this.GroupeField.Width = 74;
            // 
            // Field
            // 
            this.Field.AppearanceHeader.Options.UseTextOptions = true;
            this.Field.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Field.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Field.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Field.AutoFillDown = true;
            this.Field.Caption = "����";
            this.Field.FieldName = "FName";
            this.Field.Name = "Field";
            this.Field.OptionsColumn.AllowEdit = false;
            this.Field.OptionsColumn.ReadOnly = true;
            this.Field.Visible = true;
            this.Field.Width = 66;
            // 
            // Square
            // 
            this.Square.AppearanceHeader.Options.UseTextOptions = true;
            this.Square.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Square.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Square.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Square.AutoFillDown = true;
            this.Square.Caption = "�������, ��";
            this.Square.FieldName = "Square";
            this.Square.Name = "Square";
            this.Square.OptionsColumn.AllowEdit = false;
            this.Square.OptionsColumn.ReadOnly = true;
            this.Square.Visible = true;
            this.Square.Width = 65;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AutoFillDown = false;
            this.gridBand2.Caption = "�����";
            this.gridBand2.Columns.Add(this.TimeStart);
            this.gridBand2.Columns.Add(this.TimeEnd);
            this.gridBand2.Columns.Add(this.FactTime);
            this.gridBand2.Columns.Add(this.TimeMove);
            this.gridBand2.Columns.Add(this.TimeStop);
            this.gridBand2.Columns.Add(this.TimeOut);
            this.gridBand2.Columns.Add(this.TimeRate);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 523;
            // 
            // TimeStart
            // 
            this.TimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStart.AutoFillDown = true;
            this.TimeStart.Caption = "�����";
            this.TimeStart.DisplayFormat.FormatString = "t";
            this.TimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStart.FieldName = "TimeStart";
            this.TimeStart.Name = "TimeStart";
            this.TimeStart.OptionsColumn.AllowEdit = false;
            this.TimeStart.OptionsColumn.ReadOnly = true;
            this.TimeStart.Visible = true;
            this.TimeStart.Width = 64;
            // 
            // TimeEnd
            // 
            this.TimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEnd.AutoFillDown = true;
            this.TimeEnd.Caption = "�����";
            this.TimeEnd.DisplayFormat.FormatString = "t";
            this.TimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEnd.FieldName = "TimeEnd";
            this.TimeEnd.Name = "TimeEnd";
            this.TimeEnd.OptionsColumn.AllowEdit = false;
            this.TimeEnd.OptionsColumn.ReadOnly = true;
            this.TimeEnd.Visible = true;
            this.TimeEnd.Width = 62;
            // 
            // FactTime
            // 
            this.FactTime.AppearanceCell.Options.UseTextOptions = true;
            this.FactTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.Options.UseTextOptions = true;
            this.FactTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FactTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactTime.AutoFillDown = true;
            this.FactTime.Caption = "����� �����, �";
            this.FactTime.FieldName = "FactTime";
            this.FactTime.Name = "FactTime";
            this.FactTime.OptionsColumn.AllowEdit = false;
            this.FactTime.OptionsColumn.ReadOnly = true;
            this.FactTime.Visible = true;
            this.FactTime.Width = 69;
            // 
            // TimeMove
            // 
            this.TimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMove.Caption = "����� ��������, �";
            this.TimeMove.DisplayFormat.FormatString = "DateTime \"t\"";
            this.TimeMove.FieldName = "TimeMove";
            this.TimeMove.Name = "TimeMove";
            this.TimeMove.OptionsColumn.AllowEdit = false;
            this.TimeMove.OptionsColumn.ReadOnly = true;
            this.TimeMove.Visible = true;
            this.TimeMove.Width = 76;
            // 
            // TimeStop
            // 
            this.TimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStop.Caption = "����� �������, �";
            this.TimeStop.DisplayFormat.FormatString = "DateTime \"t\"";
            this.TimeStop.FieldName = "TimeStop";
            this.TimeStop.Name = "TimeStop";
            this.TimeStop.OptionsColumn.AllowEdit = false;
            this.TimeStop.OptionsColumn.ReadOnly = true;
            this.TimeStop.Visible = true;
            // 
            // TimeOut
            // 
            this.TimeOut.AppearanceCell.Options.UseTextOptions = true;
            this.TimeOut.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeOut.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeOut.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeOut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeOut.Caption = "����� ��� ����, �";
            this.TimeOut.DisplayFormat.FormatString = "t";
            this.TimeOut.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeOut.FieldName = "TimeOut";
            this.TimeOut.Name = "TimeOut";
            this.TimeOut.OptionsColumn.AllowEdit = false;
            this.TimeOut.OptionsColumn.ReadOnly = true;
            this.TimeOut.Visible = true;
            this.TimeOut.Width = 79;
            // 
            // TimeRate
            // 
            this.TimeRate.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRate.Caption = "��������, �";
            this.TimeRate.DisplayFormat.FormatString = "t";
            this.TimeRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeRate.FieldName = "TimeRate";
            this.TimeRate.Name = "TimeRate";
            this.TimeRate.OptionsColumn.AllowEdit = false;
            this.TimeRate.OptionsColumn.ReadOnly = true;
            this.TimeRate.Visible = true;
            this.TimeRate.Width = 98;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.AutoFillDown = false;
            this.gridBand3.Caption = "������";
            this.gridBand3.Columns.Add(this.DistanceT);
            this.gridBand3.Columns.Add(this.FactSquare);
            this.gridBand3.Columns.Add(this.FactSquareCalc);
            this.gridBand3.Columns.Add(this.SpeedAvgField);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 450;
            // 
            // DistanceT
            // 
            this.DistanceT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DistanceT.Caption = "���������� ���� , ��";
            this.DistanceT.FieldName = "Distance";
            this.DistanceT.Name = "DistanceT";
            this.DistanceT.OptionsColumn.AllowEdit = false;
            this.DistanceT.Visible = true;
            this.DistanceT.Width = 97;
            // 
            // FactSquare
            // 
            this.FactSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquare.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FactSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquare.AutoFillDown = true;
            this.FactSquare.Caption = "���������� (�� �������), ��";
            this.FactSquare.FieldName = "FactSquare";
            this.FactSquare.Name = "FactSquare";
            this.FactSquare.OptionsColumn.AllowEdit = false;
            this.FactSquare.OptionsColumn.ReadOnly = true;
            this.FactSquare.Visible = true;
            this.FactSquare.Width = 107;
            // 
            // FactSquareCalc
            // 
            this.FactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareCalc.Caption = "���������� (�� �������), ��";
            this.FactSquareCalc.FieldName = "FactSquareCalc";
            this.FactSquareCalc.Name = "FactSquareCalc";
            this.FactSquareCalc.OptionsColumn.AllowEdit = false;
            this.FactSquareCalc.Visible = true;
            this.FactSquareCalc.Width = 114;
            // 
            // SpeedAvgField
            // 
            this.SpeedAvgField.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvgField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvgField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvgField.Caption = "������� ��������";
            this.SpeedAvgField.FieldName = "SpeedAvg";
            this.SpeedAvgField.Name = "SpeedAvgField";
            this.SpeedAvgField.OptionsColumn.AllowEdit = false;
            this.SpeedAvgField.OptionsColumn.ReadOnly = true;
            this.SpeedAvgField.Visible = true;
            this.SpeedAvgField.Width = 132;
            // 
            // Zone_ID
            // 
            this.Zone_ID.Caption = "Zone_ID";
            this.Zone_ID.FieldName = "Zone_ID";
            this.Zone_ID.Name = "Zone_ID";
            this.Zone_ID.OptionsColumn.AllowEdit = false;
            this.Zone_ID.OptionsColumn.ReadOnly = true;
            // 
            // PriceC
            // 
            this.PriceC.Caption = "Price";
            this.PriceC.FieldName = "Price";
            this.PriceC.Name = "PriceC";
            this.PriceC.OptionsColumn.AllowEdit = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "�����������",
            "³�����������"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // txId
            // 
            this.txId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txId.Location = new System.Drawing.Point(64, 12);
            this.txId.Name = "txId";
            this.txId.Size = new System.Drawing.Size(82, 21);
            this.txId.TabIndex = 9;
            this.txId.Text = "0";
            this.txId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "�����";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(16, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "����";
            // 
            // dtDateInit
            // 
            this.dtDateInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtDateInit.Location = new System.Drawing.Point(64, 39);
            this.dtDateInit.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dtDateInit.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtDateInit.Name = "dtDateInit";
            this.dtDateInit.Size = new System.Drawing.Size(139, 20);
            this.dtDateInit.TabIndex = 11;
            this.dtDateInit.ValueChanged += new System.EventHandler(this.dtDateInit_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbDriver);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbMobitel);
            this.groupBox1.Controls.Add(this.lbGroupe);
            this.groupBox1.Controls.Add(this.cbGroupe);
            this.groupBox1.Location = new System.Drawing.Point(218, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 110);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(24, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 15);
            this.label4.TabIndex = 24;
            this.label4.Text = "��������";
            // 
            // cbDriver
            // 
            this.cbDriver.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbDriver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDriver.FormattingEnabled = true;
            this.cbDriver.Location = new System.Drawing.Point(107, 77);
            this.cbDriver.Name = "cbDriver";
            this.cbDriver.Size = new System.Drawing.Size(289, 23);
            this.cbDriver.TabIndex = 23;
            this.cbDriver.SelectedIndexChanged += new System.EventHandler(this.cbDriver_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(33, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 22;
            this.label3.Text = "������";
            // 
            // cbMobitel
            // 
            this.cbMobitel.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbMobitel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMobitel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbMobitel.FormattingEnabled = true;
            this.cbMobitel.Location = new System.Drawing.Point(107, 47);
            this.cbMobitel.Name = "cbMobitel";
            this.cbMobitel.Size = new System.Drawing.Size(289, 23);
            this.cbMobitel.TabIndex = 21;
            this.cbMobitel.SelectedIndexChanged += new System.EventHandler(this.cbMobitel_SelectedIndexChanged);
            // 
            // lbGroupe
            // 
            this.lbGroupe.AutoSize = true;
            this.lbGroupe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbGroupe.Location = new System.Drawing.Point(6, 19);
            this.lbGroupe.Name = "lbGroupe";
            this.lbGroupe.Size = new System.Drawing.Size(89, 15);
            this.lbGroupe.TabIndex = 20;
            this.lbGroupe.Text = "������ �����";
            // 
            // cbGroupe
            // 
            this.cbGroupe.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbGroupe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbGroupe.FormattingEnabled = true;
            this.cbGroupe.Location = new System.Drawing.Point(107, 16);
            this.cbGroupe.Name = "cbGroupe";
            this.cbGroupe.Size = new System.Drawing.Size(289, 23);
            this.cbGroupe.TabIndex = 19;
            this.cbGroupe.SelectedIndexChanged += new System.EventHandler(this.cbGroupe_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txComment);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txAgregatWidth);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbAgregat);
            this.groupBox2.Location = new System.Drawing.Point(637, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(523, 110);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(89, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "����������";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txComment
            // 
            this.txComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txComment.Location = new System.Drawing.Point(165, 80);
            this.txComment.Name = "txComment";
            this.txComment.Size = new System.Drawing.Size(337, 21);
            this.txComment.TabIndex = 10;
            this.txComment.TextChanged += new System.EventHandler(this.txComment_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "������ ������������, �";
            // 
            // txAgregatWidth
            // 
            this.txAgregatWidth.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txAgregatWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txAgregatWidth.Location = new System.Drawing.Point(165, 50);
            this.txAgregatWidth.Name = "txAgregatWidth";
            this.txAgregatWidth.Size = new System.Drawing.Size(71, 20);
            this.txAgregatWidth.TabIndex = 2;
            this.txAgregatWidth.Text = "0";
            this.txAgregatWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txAgregatWidth.TextChanged += new System.EventHandler(this.txAgregatWidth_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(6, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "�������� ������������";
            // 
            // cbAgregat
            // 
            this.cbAgregat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAgregat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbAgregat.FormattingEnabled = true;
            this.cbAgregat.Location = new System.Drawing.Point(165, 19);
            this.cbAgregat.Name = "cbAgregat";
            this.cbAgregat.Size = new System.Drawing.Size(337, 23);
            this.cbAgregat.TabIndex = 0;
            this.cbAgregat.SelectedIndexChanged += new System.EventHandler(this.cbAgregat_SelectedIndexChanged);
            // 
            // gbHandle
            // 
            this.gbHandle.Controls.Add(this.btAdd);
            this.gbHandle.Controls.Add(this.label8);
            this.gbHandle.Controls.Add(this.cbField);
            this.gbHandle.Controls.Add(this.label9);
            this.gbHandle.Controls.Add(this.cbGroupeField);
            this.gbHandle.Location = new System.Drawing.Point(19, 272);
            this.gbHandle.Name = "gbHandle";
            this.gbHandle.Size = new System.Drawing.Size(679, 50);
            this.gbHandle.TabIndex = 22;
            this.gbHandle.TabStop = false;
            // 
            // btAdd
            // 
            this.btAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btAdd.Location = new System.Drawing.Point(604, 15);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(69, 27);
            this.btAdd.TabIndex = 23;
            this.btAdd.Text = "��������";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(301, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 15);
            this.label8.TabIndex = 22;
            this.label8.Text = "����";
            // 
            // cbField
            // 
            this.cbField.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbField.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbField.FormattingEnabled = true;
            this.cbField.Location = new System.Drawing.Point(344, 19);
            this.cbField.Name = "cbField";
            this.cbField.Size = new System.Drawing.Size(254, 23);
            this.cbField.TabIndex = 21;
            this.cbField.SelectedIndexChanged += new System.EventHandler(this.cbField_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(12, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "������ �����";
            // 
            // cbGroupeField
            // 
            this.cbGroupeField.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbGroupeField.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbGroupeField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbGroupeField.FormattingEnabled = true;
            this.cbGroupeField.Location = new System.Drawing.Point(103, 19);
            this.cbGroupeField.Name = "cbGroupeField";
            this.cbGroupeField.Size = new System.Drawing.Size(182, 23);
            this.cbGroupeField.TabIndex = 19;
            this.cbGroupeField.SelectedIndexChanged += new System.EventHandler(this.cbGroupeField_SelectedIndexChanged);
            this.cbGroupeField.Enter += new System.EventHandler(this.cbGroupeField_Enter);
            // 
            // btRecalcFuel
            // 
            this.btRecalcFuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecalcFuel.Location = new System.Drawing.Point(9, 736);
            this.btRecalcFuel.Name = "btRecalcFuel";
            this.btRecalcFuel.Size = new System.Drawing.Size(175, 27);
            this.btRecalcFuel.TabIndex = 12;
            this.btRecalcFuel.Text = "����������� ������ �������";
            this.btRecalcFuel.UseVisualStyleBackColor = true;
            this.btRecalcFuel.Click += new System.EventHandler(this.btRecalcFuel_Click);
            // 
            // btRecalcContent
            // 
            this.btRecalcContent.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btRecalcContent.Location = new System.Drawing.Point(899, 290);
            this.btRecalcContent.Name = "btRecalcContent";
            this.btRecalcContent.Size = new System.Drawing.Size(171, 27);
            this.btRecalcContent.TabIndex = 26;
            this.btRecalcContent.Text = "����������� ����� �����, ��";
            this.btRecalcContent.UseVisualStyleBackColor = true;
            this.btRecalcContent.Click += new System.EventHandler(this.btRecalcContent_Click);
            // 
            // errP
            // 
            this.errP.ContainerControl = this;
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(1125, 736);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(84, 27);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "�����";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btSave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btSave.Location = new System.Drawing.Point(1035, 736);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(84, 27);
            this.btSave.TabIndex = 28;
            this.btSave.Text = "���������";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btOk_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(1076, 290);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(84, 27);
            this.btDelete.TabIndex = 29;
            this.btDelete.Text = "��������";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // gbSelectType
            // 
            this.gbSelectType.Controls.Add(this.rbClose);
            this.gbSelectType.Controls.Add(this.rbAuto);
            this.gbSelectType.Controls.Add(this.rbHandle);
            this.gbSelectType.Location = new System.Drawing.Point(19, 204);
            this.gbSelectType.Name = "gbSelectType";
            this.gbSelectType.Size = new System.Drawing.Size(612, 62);
            this.gbSelectType.TabIndex = 33;
            this.gbSelectType.TabStop = false;
            this.gbSelectType.Text = "����� ������ ������������ ������";
            // 
            // rbClose
            // 
            this.rbClose.AutoSize = true;
            this.rbClose.Location = new System.Drawing.Point(472, 28);
            this.rbClose.Name = "rbClose";
            this.rbClose.Size = new System.Drawing.Size(97, 17);
            this.rbClose.TabIndex = 35;
            this.rbClose.TabStop = true;
            this.rbClose.Text = "����� ������";
            this.rbClose.UseVisualStyleBackColor = true;
            this.rbClose.Click += new System.EventHandler(this.rbClose_Click);
            // 
            // rbAuto
            // 
            this.rbAuto.AutoSize = true;
            this.rbAuto.Location = new System.Drawing.Point(199, 28);
            this.rbAuto.Name = "rbAuto";
            this.rbAuto.Size = new System.Drawing.Size(229, 17);
            this.rbAuto.TabIndex = 34;
            this.rbAuto.Text = "���������������� �� ����������� ���";
            this.rbAuto.UseVisualStyleBackColor = true;
            this.rbAuto.Click += new System.EventHandler(this.rbAuto_Click);
            // 
            // rbHandle
            // 
            this.rbHandle.AutoSize = true;
            this.rbHandle.Checked = true;
            this.rbHandle.Location = new System.Drawing.Point(15, 28);
            this.rbHandle.Name = "rbHandle";
            this.rbHandle.Size = new System.Drawing.Size(128, 17);
            this.rbHandle.TabIndex = 33;
            this.rbHandle.TabStop = true;
            this.rbHandle.Text = "������ �����������";
            this.rbHandle.UseVisualStyleBackColor = true;
            this.rbHandle.Click += new System.EventHandler(this.rbHandle_Click);
            // 
            // gbPrice
            // 
            this.gbPrice.Controls.Add(this.label16);
            this.gbPrice.Controls.Add(this.txPrice);
            this.gbPrice.Controls.Add(this.label7);
            this.gbPrice.Controls.Add(this.cbWorkType);
            this.gbPrice.Location = new System.Drawing.Point(637, 204);
            this.gbPrice.Name = "gbPrice";
            this.gbPrice.Size = new System.Drawing.Size(523, 62);
            this.gbPrice.TabIndex = 34;
            this.gbPrice.TabStop = false;
            this.gbPrice.Text = "�������� �� ������";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(313, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 15);
            this.label16.TabIndex = 32;
            this.label16.Text = "��������, ���/��";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txPrice
            // 
            this.txPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txPrice.Location = new System.Drawing.Point(430, 22);
            this.txPrice.Name = "txPrice";
            this.txPrice.Size = new System.Drawing.Size(87, 21);
            this.txPrice.TabIndex = 31;
            this.txPrice.Text = "0";
            this.txPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "��� �����";
            // 
            // cbWorkType
            // 
            this.cbWorkType.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.cbWorkType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbWorkType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbWorkType.FormattingEnabled = true;
            this.cbWorkType.Location = new System.Drawing.Point(91, 22);
            this.cbWorkType.Name = "cbWorkType";
            this.cbWorkType.Size = new System.Drawing.Size(205, 23);
            this.cbWorkType.TabIndex = 25;
            this.cbWorkType.SelectedIndexChanged += new System.EventHandler(this.cbWorkType_SelectedIndexChanged);
            this.cbWorkType.Enter += new System.EventHandler(this.cbWorkType_Enter);
            // 
            // gbAuto
            // 
            this.gbAuto.Controls.Add(this.btSeek);
            this.gbAuto.Location = new System.Drawing.Point(704, 273);
            this.gbAuto.Name = "gbAuto";
            this.gbAuto.Size = new System.Drawing.Size(170, 49);
            this.gbAuto.TabIndex = 35;
            this.gbAuto.TabStop = false;
            // 
            // btSeek
            // 
            this.btSeek.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btSeek.Location = new System.Drawing.Point(6, 15);
            this.btSeek.Name = "btSeek";
            this.btSeek.Size = new System.Drawing.Size(154, 27);
            this.btSeek.TabIndex = 32;
            this.btSeek.Text = "��������� �����";
            this.btSeek.UseVisualStyleBackColor = true;
            this.btSeek.Click += new System.EventHandler(this.btSeek_Click);
            // 
            // bwCreateData
            // 
            this.bwCreateData.WorkerReportsProgress = true;
            this.bwCreateData.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCreateData_DoWork);
            this.bwCreateData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwCreateData_RunWorkerCompleted);
            this.bwCreateData.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwCreateData_ProgressChanged);
            // 
            // strpOrder
            // 
            this.strpOrder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspbZone,
            this.tslbOrder,
            this.tspbOrder});
            this.strpOrder.Location = new System.Drawing.Point(0, 777);
            this.strpOrder.Name = "strpOrder";
            this.strpOrder.Size = new System.Drawing.Size(1223, 22);
            this.strpOrder.TabIndex = 36;
            this.strpOrder.Text = "statusStrip1";
            // 
            // tspbZone
            // 
            this.tspbZone.Name = "tspbZone";
            this.tspbZone.Size = new System.Drawing.Size(0, 17);
            this.tspbZone.Visible = false;
            // 
            // tslbOrder
            // 
            this.tslbOrder.Name = "tslbOrder";
            this.tslbOrder.Size = new System.Drawing.Size(0, 17);
            // 
            // tspbOrder
            // 
            this.tspbOrder.Name = "tspbOrder";
            this.tspbOrder.Size = new System.Drawing.Size(100, 16);
            this.tspbOrder.Visible = false;
            // 
            // taDataView
            // 
            this.taDataView.ClearBeforeFill = true;
            // 
            // gcMain
            // 
            this.gcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.gcMain.Location = new System.Drawing.Point(15, 128);
            this.gcMain.MainView = this.gvMain;
            this.gcMain.Name = "gcMain";
            this.gcMain.Size = new System.Drawing.Size(1199, 64);
            this.gcMain.TabIndex = 37;
            this.gcMain.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMain});
            // 
            // gvMain
            // 
            this.gvMain.ColumnPanelRowHeight = 40;
            this.gvMain.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.LocationStart,
            this.LocationEnd,
            this.TimeStartM,
            this.TimeEndM,
            this.TimeWork,
            this.TimeMoveM,
            this.Distance,
            this.SpeedAvg});
            this.gvMain.GridControl = this.gcMain;
            this.gvMain.Name = "gvMain";
            this.gvMain.OptionsView.ShowGroupPanel = false;
            this.gvMain.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            // 
            // LocationStart
            // 
            this.LocationStart.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LocationStart.Caption = "����� ������ ��������";
            this.LocationStart.FieldName = "LocationStart";
            this.LocationStart.Name = "LocationStart";
            this.LocationStart.Visible = true;
            this.LocationStart.VisibleIndex = 0;
            // 
            // LocationEnd
            // 
            this.LocationEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LocationEnd.Caption = "����� ��������� ��������";
            this.LocationEnd.FieldName = "LocationEnd";
            this.LocationEnd.Name = "LocationEnd";
            this.LocationEnd.Visible = true;
            this.LocationEnd.VisibleIndex = 1;
            // 
            // TimeStartM
            // 
            this.TimeStartM.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStartM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartM.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStartM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStartM.Caption = "����� ������ ��������";
            this.TimeStartM.DisplayFormat.FormatString = "t";
            this.TimeStartM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStartM.FieldName = "TimeStart";
            this.TimeStartM.Name = "TimeStartM";
            this.TimeStartM.Visible = true;
            this.TimeStartM.VisibleIndex = 2;
            // 
            // TimeEndM
            // 
            this.TimeEndM.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEndM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndM.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEndM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEndM.Caption = "����� ��������� ��������";
            this.TimeEndM.DisplayFormat.FormatString = "t";
            this.TimeEndM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndM.FieldName = "TimeEnd";
            this.TimeEndM.Name = "TimeEndM";
            this.TimeEndM.Visible = true;
            this.TimeEndM.VisibleIndex = 3;
            // 
            // TimeWork
            // 
            this.TimeWork.AppearanceCell.Options.UseTextOptions = true;
            this.TimeWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeWork.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeWork.Caption = "����������������� �����, �";
            this.TimeWork.FieldName = "TimeWork";
            this.TimeWork.Name = "TimeWork";
            this.TimeWork.Visible = true;
            this.TimeWork.VisibleIndex = 4;
            // 
            // TimeMoveM
            // 
            this.TimeMoveM.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMoveM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMoveM.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMoveM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMoveM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMoveM.Caption = "����� ����� ��������, �";
            this.TimeMoveM.FieldName = "TimeMove";
            this.TimeMoveM.Name = "TimeMoveM";
            this.TimeMoveM.Visible = true;
            this.TimeMoveM.VisibleIndex = 5;
            // 
            // Distance
            // 
            this.Distance.AppearanceHeader.Options.UseTextOptions = true;
            this.Distance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Distance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Distance.Caption = "���������� ����, ��";
            this.Distance.FieldName = "Distance";
            this.Distance.Name = "Distance";
            this.Distance.Visible = true;
            this.Distance.VisibleIndex = 6;
            // 
            // SpeedAvg
            // 
            this.SpeedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvg.Caption = "������� ��������, ��/�";
            this.SpeedAvg.FieldName = "SpeedAvg";
            this.SpeedAvg.Name = "SpeedAvg";
            this.SpeedAvg.Visible = true;
            this.SpeedAvg.VisibleIndex = 7;
            // 
            // btMapView
            // 
            this.btMapView.Location = new System.Drawing.Point(61, 66);
            this.btMapView.Name = "btMapView";
            this.btMapView.Size = new System.Drawing.Size(55, 23);
            this.btMapView.TabIndex = 40;
            this.btMapView.Text = "�����";
            this.btMapView.UseVisualStyleBackColor = true;
            this.btMapView.Click += new System.EventHandler(this.btMapView_Click);
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(1223, 799);
            this.Controls.Add(this.btMapView);
            this.Controls.Add(this.gcMain);
            this.Controls.Add(this.strpOrder);
            this.Controls.Add(this.gbAuto);
            this.Controls.Add(this.gbPrice);
            this.Controls.Add(this.gbSelectType);
            this.Controls.Add(this.gcFuel);
            this.Controls.Add(this.btRecalcFuel);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btRecalcContent);
            this.Controls.Add(this.gcContent);
            this.Controls.Add(this.gbHandle);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtDateInit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txId);
            this.Name = "Order";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� �� ������� ������";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Order_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDetal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbHandle.ResumeLayout(false);
            this.gbHandle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errP)).EndInit();
            this.gbSelectType.ResumeLayout(false);
            this.gbSelectType.PerformLayout();
            this.gbPrice.ResumeLayout(false);
            this.gbPrice.PerformLayout();
            this.gbAuto.ResumeLayout(false);
            this.strpOrder.ResumeLayout(false);
            this.strpOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDateInit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbDriver;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMobitel;
        private System.Windows.Forms.Label lbGroupe;
        private System.Windows.Forms.ComboBox cbGroupe;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txAgregatWidth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbAgregat;
        private System.Windows.Forms.GroupBox gbHandle;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbField;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbGroupeField;
        private System.Windows.Forms.Button btAdd;
        private DevExpress.XtraGrid.GridControl gcContent;
        private System.Windows.Forms.Button btRecalcFuel;
        private System.Windows.Forms.Button btRecalcContent;
        private System.Windows.Forms.ErrorProvider errP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txComment;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btDelete;
        private DevExpress.XtraGrid.GridControl gcFuel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gbvContent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn GroupeFieldId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn GroupeField;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FieldId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Field;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Square;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gbvFuel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ZoneID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ZoneName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelAdd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelSub;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpens;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn IdF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpensAvgRate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensAvgRate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeRate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Zone_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeOut;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStartF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeEndF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquareF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeRateF;
        private System.Windows.Forms.GroupBox gbSelectType;
        private System.Windows.Forms.RadioButton rbAuto;
        private System.Windows.Forms.RadioButton rbHandle;
        private System.Windows.Forms.GroupBox gbPrice;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbWorkType;
        private System.Windows.Forms.GroupBox gbAuto;
        private System.Windows.Forms.RadioButton rbClose;
        private System.Windows.Forms.Button btSeek;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelDetal;
        private DevExpress.XtraGrid.Columns.GridColumn IdD;
        private DevExpress.XtraGrid.Columns.GridColumn Id_node;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStartD;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEndD;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRateD;
        private DevExpress.XtraGrid.Columns.GridColumn FactSquareD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelAddD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelSubD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelEndD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelExpensD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensMoveD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensStopD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensTotalD;
        private System.ComponentModel.BackgroundWorker bwCreateData;
        private System.Windows.Forms.StatusStrip strpOrder;
        private System.Windows.Forms.ToolStripProgressBar tspbOrder;
        private System.Windows.Forms.ToolStripStatusLabel tslbOrder;
        private LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter taDataView;
        private DevExpress.XtraGrid.GridControl gcMain;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMain;
        private DevExpress.XtraGrid.Columns.GridColumn LocationStart;
        private DevExpress.XtraGrid.Columns.GridColumn LocationEnd;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStartM;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEndM;
        private DevExpress.XtraGrid.Columns.GridColumn TimeWork;
        private DevExpress.XtraGrid.Columns.GridColumn TimeMoveM;
        private DevExpress.XtraGrid.Columns.GridColumn Distance;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedAvg;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit WorkLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SpeedAvgField;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DistanceT;
        private System.Windows.Forms.Button btMapView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquareCalc;
        private System.Windows.Forms.ToolStripStatusLabel tspbZone;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand ����;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContentPrice;
        private DevExpress.XtraGrid.Columns.GridColumn IdP;
        private DevExpress.XtraGrid.Columns.GridColumn Work;
        private DevExpress.XtraGrid.Columns.GridColumn FSP;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn Sum;
        private DevExpress.XtraGrid.Columns.GridColumn Comment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PriceC;
    }
}