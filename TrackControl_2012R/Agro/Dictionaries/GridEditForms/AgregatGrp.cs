using System;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro.GridEditForms
{
    public partial class AgregatGrp : FormSample
    {
        public AgregatGrp()
        {
            InitThis();
        }
        public AgregatGrp(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroAgregatGrp(_Id);
            this.Text = this.Text + ": " + Resources.AgregatGroups;
            GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_work"], (int)(leWork.EditValue ?? 0));
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroAgregatGrp).Id_work = (int)(leWork.EditValue ?? 0);
            (di as DictionaryAgroAgregatGrp).IdOutLink = (teOutLink.EditValue ?? "") .ToString() ;
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            leWork.EditValue = (di as DictionaryAgroAgregatGrp).Id_work;
            teOutLink.EditValue = (di as DictionaryAgroAgregatGrp).IdOutLink;
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            DicUtilites.LookUpLoad(leWork, AgroQuery.DictionaryAgroWorkType.SelectAgroWorkList);
        }

        private void leWork_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void teOutLink_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void leWork_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
                leWork.EditValue = null;
                leWork.Text = "";
        }
        #region �����������
        private void Localization()
        {
            labelControl8.Text = Resources.WorkDefault;
            leOutLink.Text = Resources.OuterBaseLink;
        }
        #endregion


    }
}

