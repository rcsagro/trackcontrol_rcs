using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using CheckZones;
using LocalCache;
using DevExpress.XtraGrid.Views.Grid;

namespace Agro.GridEditForms
{
    public static class DicUtilites
    {
        #region Combo
            public static bool ComboSet(DataGridView dgvGrn, ComboBox cb, string sId, string sName)
            {
                if (dgvGrn.SelectedRows[0].Cells[sId].Value != DBNull.Value)
                {
                    cb.SelectedValue = (int)dgvGrn.SelectedRows[0].Cells[sId].Value;
                    cb.SelectedText = Convert.ToString(dgvGrn.SelectedRows[0].Cells[sName].Value);
                    return true;
                }
                else
                    return false;

            }
            public static bool ComboSet(GridView gvGrn, ComboBox cb, string sId, string sName)
        {
            if (gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sId) != DBNull.Value)
            {
                cb.SelectedValue = (int)gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sId);
                cb.SelectedText = Convert.ToString(gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sName));
                return true;
            }
            else
                return false;

        }
            public static bool ComboSet(MySqlDataReader drWork , ComboBox cb, string sId, string sName)
                {
                    if (!drWork.IsDBNull(drWork.GetOrdinal(sId)))
                    {
                        if (drWork.GetInt32(drWork.GetOrdinal(sId)) > 0)
                        {
                            cb.SelectedValue = drWork.GetInt32(drWork.GetOrdinal(sId));
                            cb.SelectedText = drWork.GetString(drWork.GetOrdinal(sName));
                            return true;
                        }
                        else
                        return false;
                    }
                    else
                        cb.SelectedValue = null;
                        cb.SelectedText = ""; 
                        return false;
                }
        public static void ComboLoad(ComboBox cbWork, string sSQL)
        {
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            DataTable dtWork = _cnMySQL.GetDataTable(sSQL);
            cbWork.DataSource = dtWork;
            cbWork.DisplayMember = dtWork.Columns[1].ColumnName;
            cbWork.ValueMember = dtWork.Columns[0].ColumnName;
            cbWork.SelectedIndex = -1;
            cbWork.Text = "";
            _cnMySQL.CloseMySQLConnection();
        }
        #endregion
        #region Validation
        public static bool ValidateCombo(ComboBox  cbTest, ErrorProvider errP)
        {
                if (cbTest.Items.Count !=0 )
                {
                    if (cbTest.SelectedValue == null || cbTest.SelectedValue.GetType().Name !="Int32" )
                    {
                        errP.SetError(cbTest, "�������� ������������ �������� �� ������!");
                        cbTest.Focus();
                        return false;
                    }
                    else
                    {
                        errP.SetError(cbTest, "");
                        return true;
                    }
                }
                else
                {
                    errP.SetError(cbTest, "�������� ������������ �������� �� ������!");
                    cbTest.Focus();
                    return false;
                }

        }
        public static bool ValidateFieldsText(TextBox txTest, ErrorProvider errP)
        {
            if (txTest.Text.Trim().Length == 0)
            {
                errP.SetError(txTest, "���� �� ����� ���� ������");
                txTest.Focus();
                return false;
            }
            else
            {
                errP.SetError(txTest, "");
                return true;
            }
        }
        public static bool ValidateFieldsTextDoublePositive(TextBox txTest, ErrorProvider errP)
        {
            double dbOut;
            if (!Double.TryParse(txTest.Text, out dbOut))
            {
                errP.SetError(txTest, "�������� ���� ������ ���� ��������!");
                txTest.Focus();
                return false;
            }
            else
            {
                if (dbOut == 0)
                {
                    errP.SetError(txTest, "�������� ���� ������ ���� ������ ����!");
                    txTest.Focus();
                    return false;
                }
                else
                {
                    errP.SetError(txTest, "");
                    return true;
                }
            }
        }
        #endregion
    }
}
