namespace Agro.GridEditForms
{
    partial class Culture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.peIcon = new DevExpress.XtraEditors.PictureEdit();
            this.btIcon = new DevExpress.XtraEditors.SimpleButton();
            this.ofdIcon = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // peIcon
            // 
            this.peIcon.EditValue = global::Agro.Properties.ResourcesImages.image_edit;
            this.peIcon.Location = new System.Drawing.Point(13, 171);
            this.peIcon.Name = "peIcon";
            this.peIcon.Size = new System.Drawing.Size(67, 47);
            this.peIcon.TabIndex = 9;
            // 
            // btIcon
            // 
            this.btIcon.Image = global::Agro.Properties.ResourcesImages.image_edit;
            this.btIcon.Location = new System.Drawing.Point(92, 174);
            this.btIcon.Name = "btIcon";
            this.btIcon.Size = new System.Drawing.Size(88, 26);
            this.btIcon.TabIndex = 10;
            this.btIcon.Text = "������";
            this.btIcon.Click += new System.EventHandler(this.btIcon_Click);
            // 
            // ofdIcon
            // 
            this.ofdIcon.Filter = "(*.png)|*.png| (*.ico)|*.ico\"";
            // 
            // Culture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.btIcon);
            this.Controls.Add(this.peIcon);
            this.Name = "Culture";
            this.Controls.SetChildIndex(this.peIcon, 0);
            this.Controls.SetChildIndex(this.btIcon, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit peIcon;
        private DevExpress.XtraEditors.SimpleButton btIcon;
        private System.Windows.Forms.OpenFileDialog ofdIcon;
    }
}
