namespace Agro.GridEditForms
{
    partial class Field
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leZone = new DevExpress.XtraEditors.LookUpEdit();
            this.lbZone = new DevExpress.XtraEditors.LabelControl();
            this.zoneProperty = new TrackControl.Zones.Tuning.ZoneProperty();
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGroup = new DevExpress.XtraEditors.LabelControl();
            this.chIsOutOfDate = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIsOutOfDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // leZone
            // 
            this.leZone.Location = new System.Drawing.Point(126, 181);
            this.leZone.Name = "leZone";
            this.leZone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leZone.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leZone.Properties.DisplayMember = "Name";
            this.leZone.Properties.NullText = "";
            this.leZone.Properties.ValueMember = "id";
            this.leZone.Size = new System.Drawing.Size(322, 20);
            this.leZone.TabIndex = 17;
            this.leZone.EditValueChanged += new System.EventHandler(this.leZone_EditValueChanged);
            // 
            // lbZone
            // 
            this.lbZone.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbZone.Location = new System.Drawing.Point(15, 184);
            this.lbZone.Name = "lbZone";
            this.lbZone.Size = new System.Drawing.Size(103, 14);
            this.lbZone.TabIndex = 16;
            this.lbZone.Text = "����������� ����";
            // 
            // zoneProperty
            // 
            this.zoneProperty.Location = new System.Drawing.Point(5, 218);
            this.zoneProperty.Name = "zoneProperty";
            this.zoneProperty.Size = new System.Drawing.Size(626, 179);
            this.zoneProperty.TabIndex = 24;
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(72, 12);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leGroupe.Properties.Appearance.Options.UseFont = true;
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "Id";
            this.leGroupe.Size = new System.Drawing.Size(542, 20);
            this.leGroupe.TabIndex = 26;
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbGroup
            // 
            this.lbGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbGroup.Location = new System.Drawing.Point(19, 15);
            this.lbGroup.Name = "lbGroup";
            this.lbGroup.Size = new System.Drawing.Size(39, 14);
            this.lbGroup.TabIndex = 25;
            this.lbGroup.Text = "������";
            // 
            // chIsOutOfDate
            // 
            this.chIsOutOfDate.Location = new System.Drawing.Point(5, 413);
            this.chIsOutOfDate.Name = "chIsOutOfDate";
            this.chIsOutOfDate.Properties.Caption = "���� �������� � � �������� ������� �� ���������";
            this.chIsOutOfDate.Size = new System.Drawing.Size(284, 19);
            this.chIsOutOfDate.TabIndex = 27;
            this.chIsOutOfDate.EditValueChanged += new System.EventHandler(this.chIsOutOfDate_EditValueChanged);
            // 
            // Field
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 510);
            this.Controls.Add(this.chIsOutOfDate);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.lbGroup);
            this.Controls.Add(this.zoneProperty);
            this.Controls.Add(this.leZone);
            this.Controls.Add(this.lbZone);
            this.Name = "Field";
            this.Controls.SetChildIndex(this.lbZone, 0);
            this.Controls.SetChildIndex(this.leZone, 0);
            this.Controls.SetChildIndex(this.zoneProperty, 0);
            this.Controls.SetChildIndex(this.lbGroup, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            this.Controls.SetChildIndex(this.chIsOutOfDate, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIsOutOfDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit leZone;
        private DevExpress.XtraEditors.LabelControl lbZone;
        private TrackControl.Zones.Tuning.ZoneProperty zoneProperty;
        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbGroup;
        private DevExpress.XtraEditors.CheckEdit chIsOutOfDate;

    }
}
