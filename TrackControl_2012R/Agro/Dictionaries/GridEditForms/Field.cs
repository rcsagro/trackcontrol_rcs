using System;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General; 

namespace Agro.GridEditForms
{
    public partial class Field : FormSample
    {
        public Field()
        {
            InitThis();
        }
        public Field(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroField(_Id);
            this.Text = string.Format("{0}: {1}",this.Text, Resources.Fields); 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            //if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP) || !DicUtilites.ValidateLookUp(leGroupe, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["FGROUPE"], leGroupe.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["FZONE"], (leZone.EditValue ?? 0));
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Square"],zoneProperty.AreaGa);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["IsOutOfDate"], (bool)(chIsOutOfDate.EditValue ?? 0));
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroField).IdMain = (int)(leGroupe.EditValue ?? 0);
            (di as DictionaryAgroField).IdZone = (int)(leZone.EditValue ?? 0);
            (di as DictionaryAgroField).IsOutOfDate = (bool)(chIsOutOfDate.EditValue ?? 0);
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            leGroupe.EditValue = (di as DictionaryAgroField).IdMain;
            leZone.EditValue = (di as DictionaryAgroField).IdZone;
            chIsOutOfDate.EditValue = (di as DictionaryAgroField).IsOutOfDate;
            DrawZone((di as DictionaryAgroField).IdZone); 
        }
        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }
        private void leZone_EditValueChanged(object sender, EventArgs e)
        {
            int id_zone;
            if (Int32.TryParse(Convert.ToString(leZone.EditValue), out id_zone))
            {
                DrawZone(id_zone); 
                ValidateFieldsAction();
            }
        }

        private void chIsOutOfDate_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void DrawZone(int id_zone)
        {
            IZone zone = DictionaryAgroField.ZonesModel.GetById(id_zone);
            if (zone != null) zoneProperty.SetZone(DictionaryAgroField.ZonesModel.GetById(id_zone));
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            leGroupe.Properties.DataSource = DictionaryAgroFieldGrp.GetList(); 
            DicUtilites.LookUpLoad(leZone, "SELECT   zones.Zone_ID, zones.Name FROM zones ORDER BY zones.Name");
            btOrders.Visible = true;
        }
            
        #region �����������
        private void Localization()
        {
            leGroupe.Properties.NullText = Resources.GroupeSelect;
            lbGroup.Text = Resources.Groupe;
            lbZone.Text = Resources.CheckZone;
        }
        #endregion


    }
}

