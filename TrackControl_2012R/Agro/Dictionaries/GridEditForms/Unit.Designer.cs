namespace Agro.GridEditForms
{
    partial class Unit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txNameShort = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txFactor = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txNameShort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txFactor.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // txNameShort
            // 
            this.txNameShort.Location = new System.Drawing.Point(157, 181);
            this.txNameShort.Name = "txNameShort";
            this.txNameShort.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txNameShort.Properties.Appearance.Options.UseFont = true;
            this.txNameShort.Properties.Appearance.Options.UseTextOptions = true;
            this.txNameShort.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txNameShort.Size = new System.Drawing.Size(69, 21);
            this.txNameShort.TabIndex = 9;
            this.txNameShort.EditValueChanged += new System.EventHandler(this.txNameShort_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(17, 184);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(101, 14);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "�������� �������";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(287, 184);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(194, 14);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "���������� ��������� � �������";
            // 
            // txFactor
            // 
            this.txFactor.EditValue = "0";
            this.txFactor.Location = new System.Drawing.Point(498, 181);
            this.txFactor.Name = "txFactor";
            this.txFactor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txFactor.Properties.Appearance.Options.UseFont = true;
            this.txFactor.Properties.Appearance.Options.UseTextOptions = true;
            this.txFactor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txFactor.Size = new System.Drawing.Size(100, 21);
            this.txFactor.TabIndex = 11;
            this.txFactor.EditValueChanged += new System.EventHandler(this.txFactor_EditValueChanged);
            // 
            // Unit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.txFactor);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.txNameShort);
            this.Name = "Unit";
            this.Controls.SetChildIndex(this.txNameShort, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.txFactor, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txNameShort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txFactor.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txNameShort;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txFactor;
    }
}
