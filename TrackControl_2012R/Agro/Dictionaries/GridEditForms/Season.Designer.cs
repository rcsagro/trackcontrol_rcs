﻿namespace Agro.GridEditForms
{
    partial class Season
    {
        /// <summary>
        ///Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.deStart = new DevExpress.XtraEditors.DateEdit();
            this.lbDateStart = new DevExpress.XtraEditors.LabelControl();
            this.lbDateEnd = new DevExpress.XtraEditors.LabelControl();
            this.deEnd = new DevExpress.XtraEditors.DateEdit();
            this.leCulture = new DevExpress.XtraEditors.LookUpEdit();
            this.lbCulture = new DevExpress.XtraEditors.LabelControl();
            this.teSquare = new DevExpress.XtraEditors.TextEdit();
            this.lbSquare = new DevExpress.XtraEditors.LabelControl();
            this.chIsClosed = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSquare.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIsClosed.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // deStart
            // 
            this.deStart.EditValue = null;
            this.deStart.Location = new System.Drawing.Point(103, 12);
            this.deStart.Name = "deStart";
            this.deStart.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.deStart.Properties.Appearance.Options.UseFont = true;
            this.deStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "d";
            this.deStart.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.CalendarTimeProperties.EditFormat.FormatString = "d";
            this.deStart.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.Size = new System.Drawing.Size(85, 22);
            this.deStart.TabIndex = 9;
            this.deStart.EditValueChanged += new System.EventHandler(this.deStart_EditValueChanged);
            // 
            // lbDateStart
            // 
            this.lbDateStart.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbDateStart.Location = new System.Drawing.Point(21, 15);
            this.lbDateStart.Name = "lbDateStart";
            this.lbDateStart.Size = new System.Drawing.Size(69, 14);
            this.lbDateStart.TabIndex = 10;
            this.lbDateStart.Text = "Дата начала";
            // 
            // lbDateEnd
            // 
            this.lbDateEnd.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbDateEnd.Location = new System.Drawing.Point(197, 15);
            this.lbDateEnd.Name = "lbDateEnd";
            this.lbDateEnd.Size = new System.Drawing.Size(91, 14);
            this.lbDateEnd.TabIndex = 12;
            this.lbDateEnd.Text = "Дата окончания";
            // 
            // deEnd
            // 
            this.deEnd.EditValue = null;
            this.deEnd.Location = new System.Drawing.Point(294, 12);
            this.deEnd.Name = "deEnd";
            this.deEnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.deEnd.Properties.Appearance.Options.UseFont = true;
            this.deEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.Properties.CalendarTimeProperties.DisplayFormat.FormatString = "d";
            this.deEnd.Properties.CalendarTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.CalendarTimeProperties.EditFormat.FormatString = "d";
            this.deEnd.Properties.CalendarTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.Size = new System.Drawing.Size(89, 22);
            this.deEnd.TabIndex = 11;
            this.deEnd.EditValueChanged += new System.EventHandler(this.deEnd_EditValueChanged);
            // 
            // leCulture
            // 
            this.leCulture.Location = new System.Drawing.Point(451, 12);
            this.leCulture.Name = "leCulture";
            this.leCulture.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.leCulture.Properties.Appearance.Options.UseFont = true;
            this.leCulture.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCulture.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leCulture.Properties.DisplayMember = "Name";
            this.leCulture.Properties.NullText = "";
            this.leCulture.Properties.ValueMember = "Id";
            this.leCulture.Size = new System.Drawing.Size(163, 22);
            this.leCulture.TabIndex = 13;
            this.leCulture.EditValueChanged += new System.EventHandler(this.leCulture_EditValueChanged);
            // 
            // lbCulture
            // 
            this.lbCulture.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbCulture.Location = new System.Drawing.Point(392, 15);
            this.lbCulture.Name = "lbCulture";
            this.lbCulture.Size = new System.Drawing.Size(51, 14);
            this.lbCulture.TabIndex = 14;
            this.lbCulture.Text = "Культура";
            // 
            // teSquare
            // 
            this.teSquare.EditValue = "0";
            this.teSquare.Location = new System.Drawing.Point(537, 183);
            this.teSquare.Name = "teSquare";
            this.teSquare.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.teSquare.Properties.Appearance.Options.UseFont = true;
            this.teSquare.Properties.Appearance.Options.UseTextOptions = true;
            this.teSquare.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teSquare.Size = new System.Drawing.Size(77, 22);
            this.teSquare.TabIndex = 15;
            this.teSquare.EditValueChanged += new System.EventHandler(this.teSquare_EditValueChanged);
            // 
            // lbSquare
            // 
            this.lbSquare.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbSquare.Location = new System.Drawing.Point(303, 186);
            this.lbSquare.Name = "lbSquare";
            this.lbSquare.Size = new System.Drawing.Size(228, 14);
            this.lbSquare.TabIndex = 16;
            this.lbSquare.Text = "Запланированная посевная площадь, га";
            // 
            // chIsClosed
            // 
            this.chIsClosed.Location = new System.Drawing.Point(11, 183);
            this.chIsClosed.Name = "chIsClosed";
            this.chIsClosed.Properties.Caption = "Закрыт";
            this.chIsClosed.Size = new System.Drawing.Size(124, 19);
            this.chIsClosed.TabIndex = 28;
            this.chIsClosed.EditValueChanged += new System.EventHandler(this.chIsClosed_EditValueChanged);
            // 
            // Season
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 336);
            this.Controls.Add(this.chIsClosed);
            this.Controls.Add(this.lbSquare);
            this.Controls.Add(this.teSquare);
            this.Controls.Add(this.lbCulture);
            this.Controls.Add(this.leCulture);
            this.Controls.Add(this.lbDateEnd);
            this.Controls.Add(this.deEnd);
            this.Controls.Add(this.lbDateStart);
            this.Controls.Add(this.deStart);
            this.Name = "Season";
            this.Controls.SetChildIndex(this.deStart, 0);
            this.Controls.SetChildIndex(this.lbDateStart, 0);
            this.Controls.SetChildIndex(this.deEnd, 0);
            this.Controls.SetChildIndex(this.lbDateEnd, 0);
            this.Controls.SetChildIndex(this.leCulture, 0);
            this.Controls.SetChildIndex(this.lbCulture, 0);
            this.Controls.SetChildIndex(this.teSquare, 0);
            this.Controls.SetChildIndex(this.lbSquare, 0);
            this.Controls.SetChildIndex(this.chIsClosed, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSquare.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chIsClosed.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit deStart;
        private DevExpress.XtraEditors.LabelControl lbDateStart;
        private DevExpress.XtraEditors.LabelControl lbDateEnd;
        private DevExpress.XtraEditors.DateEdit deEnd;
        private DevExpress.XtraEditors.LookUpEdit leCulture;
        private DevExpress.XtraEditors.LabelControl lbCulture;
        private DevExpress.XtraEditors.TextEdit teSquare;
        private DevExpress.XtraEditors.LabelControl lbSquare;
        private DevExpress.XtraEditors.CheckEdit chIsClosed;
    }
}
