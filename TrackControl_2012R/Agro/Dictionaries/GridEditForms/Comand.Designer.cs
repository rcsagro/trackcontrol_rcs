namespace Agro.GridEditForms
{
    partial class Comand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbName = new System.Windows.Forms.Label();
            this.txName = new System.Windows.Forms.TextBox();
            this.txComment = new System.Windows.Forms.TextBox();
            this.lbComment = new System.Windows.Forms.Label();
            this.btSave = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.txId = new System.Windows.Forms.TextBox();
            this.errP = new System.Windows.Forms.ErrorProvider(this.components);
            this.dtDateInit = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDateComand = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.errP)).BeginInit();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbName.Location = new System.Drawing.Point(12, 12);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(83, 20);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "��������";
            // 
            // txName
            // 
            this.txName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txName.Location = new System.Drawing.Point(101, 12);
            this.txName.Name = "txName";
            this.txName.Size = new System.Drawing.Size(400, 26);
            this.txName.TabIndex = 2;
            this.txName.TextChanged += new System.EventHandler(this.txName_TextChanged);
            // 
            // txComment
            // 
            this.txComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txComment.Location = new System.Drawing.Point(2, 96);
            this.txComment.Multiline = true;
            this.txComment.Name = "txComment";
            this.txComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txComment.Size = new System.Drawing.Size(583, 93);
            this.txComment.TabIndex = 4;
            this.txComment.TextChanged += new System.EventHandler(this.txComment_TextChanged);
            // 
            // lbComment
            // 
            this.lbComment.AutoSize = true;
            this.lbComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbComment.Location = new System.Drawing.Point(-1, 73);
            this.lbComment.Name = "lbComment";
            this.lbComment.Size = new System.Drawing.Size(98, 17);
            this.lbComment.TabIndex = 3;
            this.lbComment.Text = "�����������";
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSave.Enabled = false;
            this.btSave.Location = new System.Drawing.Point(429, 197);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 5;
            this.btSave.Text = "���������";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(510, 197);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "������";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // btDelete
            // 
            this.btDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btDelete.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btDelete.Enabled = false;
            this.btDelete.Location = new System.Drawing.Point(348, 197);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 7;
            this.btDelete.Text = "�������";
            this.btDelete.UseVisualStyleBackColor = true;
            // 
            // txId
            // 
            this.txId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txId.Location = new System.Drawing.Point(522, 12);
            this.txId.Name = "txId";
            this.txId.Size = new System.Drawing.Size(63, 26);
            this.txId.TabIndex = 8;
            this.txId.Text = "0";
            this.txId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // errP
            // 
            this.errP.ContainerControl = this;
            // 
            // dtDateInit
            // 
            this.dtDateInit.Location = new System.Drawing.Point(301, 44);
            this.dtDateInit.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dtDateInit.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtDateInit.Name = "dtDateInit";
            this.dtDateInit.Size = new System.Drawing.Size(200, 20);
            this.dtDateInit.TabIndex = 9;
            this.dtDateInit.ValueChanged += new System.EventHandler(this.dtDateInit_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(196, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "���� �������";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(196, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "���� ��������";
            // 
            // dtDateComand
            // 
            this.dtDateComand.Location = new System.Drawing.Point(301, 70);
            this.dtDateComand.Name = "dtDateComand";
            this.dtDateComand.Size = new System.Drawing.Size(200, 20);
            this.dtDateComand.TabIndex = 11;
            this.dtDateComand.ValueChanged += new System.EventHandler(this.dtDateComand_ValueChanged);
            // 
            // Comand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 232);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtDateComand);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDateInit);
            this.Controls.Add(this.txId);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.txComment);
            this.Controls.Add(this.lbComment);
            this.Controls.Add(this.txName);
            this.Controls.Add(this.lbName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Comand";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������/��������������/�������� � ���������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Comand_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.errP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox txName;
        private System.Windows.Forms.TextBox txComment;
        private System.Windows.Forms.Label lbComment;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.TextBox txId;
        private System.Windows.Forms.ErrorProvider errP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDateComand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDateInit;
    }
}