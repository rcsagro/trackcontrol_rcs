namespace Agro.GridEditForms
{
    partial class Agregat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGroup = new DevExpress.XtraEditors.LabelControl();
            this.lbVehicles = new DevExpress.XtraEditors.LabelControl();
            this.pgProperties = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.teNumeric = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ceBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.teMax50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.teMax20 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.leWork = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.leSensorAlgorithms = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.kTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.tarButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.crProperties = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIdentifier = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erWidth = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erInvNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erWorkDefault = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erUseDefault = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crSensor = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erSensorPresent = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsUseAgregatState = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crValueSensor = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erSensorAllgorithm = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCoefficient = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erShifter = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSigned = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTarirovka = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crLogicState = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erLogicState = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erBounceSecondsSensorNoActive = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crSensorState = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erMinValue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMaxValue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcVehicles = new DevExpress.XtraGrid.GridControl();
            this.gvVehicles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleTeam = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.VID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleVehicles = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleSensors = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.StID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleStates = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leSensorAlgorithms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(72, 12);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leGroupe.Properties.Appearance.Options.UseFont = true;
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "Id";
            this.leGroupe.Size = new System.Drawing.Size(526, 20);
            this.leGroupe.TabIndex = 17;
            this.leGroupe.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.leGroupe_ButtonClick);
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbGroup
            // 
            this.lbGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbGroup.Location = new System.Drawing.Point(19, 15);
            this.lbGroup.Name = "lbGroup";
            this.lbGroup.Size = new System.Drawing.Size(39, 14);
            this.lbGroup.TabIndex = 16;
            this.lbGroup.Text = "������";
            // 
            // lbVehicles
            // 
            this.lbVehicles.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbVehicles.Location = new System.Drawing.Point(12, 343);
            this.lbVehicles.Name = "lbVehicles";
            this.lbVehicles.Size = new System.Drawing.Size(321, 14);
            this.lbVehicles.TabIndex = 44;
            this.lbVehicles.Text = "������������ �� ��������� ��� ������������ �������";
            // 
            // pgProperties
            // 
            this.pgProperties.Location = new System.Drawing.Point(12, 184);
            this.pgProperties.Name = "pgProperties";
            this.pgProperties.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgProperties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teNumeric,
            this.ceBox,
            this.teMax50,
            this.teMax20,
            this.leWork,
            this.leSensorAlgorithms,
            this.kTextEdit,
            this.bTextEdit,
            this.tarButtonEdit,
            this.repCheckEdit1});
            this.pgProperties.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crProperties,
            this.erIdentifier,
            this.erWidth,
            this.erInvNumber,
            this.erWorkDefault,
            this.erUseDefault,
            this.erOutId,
            this.crSensor,
            this.erSensorPresent,
            this.erIsUseAgregatState,
            this.crValueSensor,
            this.erSensorAllgorithm,
            this.erCoefficient,
            this.erShifter,
            this.erSigned,
            this.erTarirovka,
            this.crLogicState,
            this.erLogicState,
            this.erBounceSecondsSensorNoActive,
            this.crSensorState,
            this.erMinValue,
            this.erMaxValue});
            this.pgProperties.ShowButtonMode = DevExpress.XtraVerticalGrid.ShowButtonModeEnum.ShowAlways;
            this.pgProperties.Size = new System.Drawing.Size(601, 153);
            this.pgProperties.TabIndex = 47;
            this.pgProperties.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgProperties_CellValueChanged);
            // 
            // teNumeric
            // 
            this.teNumeric.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.teNumeric.AutoHeight = false;
            this.teNumeric.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.teNumeric.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.teNumeric.Mask.BeepOnError = true;
            this.teNumeric.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.teNumeric.Name = "teNumeric";
            this.teNumeric.ValidateOnEnterKey = true;
            // 
            // ceBox
            // 
            this.ceBox.AutoHeight = false;
            this.ceBox.Caption = "";
            this.ceBox.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ceBox.Name = "ceBox";
            // 
            // teMax50
            // 
            this.teMax50.AutoHeight = false;
            this.teMax50.MaxLength = 50;
            this.teMax50.Name = "teMax50";
            // 
            // teMax20
            // 
            this.teMax20.AutoHeight = false;
            this.teMax20.MaxLength = 20;
            this.teMax20.Name = "teMax20";
            // 
            // leWork
            // 
            this.leWork.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.leWork.AutoHeight = false;
            this.leWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leWork.DisplayMember = "Name";
            this.leWork.DropDownRows = 10;
            this.leWork.Name = "leWork";
            this.leWork.NullText = "";
            this.leWork.ValueMember = "Id";
            // 
            // leSensorAlgorithms
            // 
            this.leSensorAlgorithms.AutoHeight = false;
            this.leSensorAlgorithms.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leSensorAlgorithms.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leSensorAlgorithms.DisplayMember = "Name";
            this.leSensorAlgorithms.Name = "leSensorAlgorithms";
            this.leSensorAlgorithms.ValueMember = "Id";
            // 
            // kTextEdit
            // 
            this.kTextEdit.AutoHeight = false;
            this.kTextEdit.Name = "kTextEdit";
            // 
            // bTextEdit
            // 
            this.bTextEdit.AutoHeight = false;
            this.bTextEdit.Name = "bTextEdit";
            // 
            // tarButtonEdit
            // 
            this.tarButtonEdit.AutoHeight = false;
            this.tarButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tarButtonEdit.Name = "tarButtonEdit";
            this.tarButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repCheckEdit1
            // 
            this.repCheckEdit1.AutoHeight = false;
            this.repCheckEdit1.Caption = "";
            this.repCheckEdit1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repCheckEdit1.Name = "repCheckEdit1";
            // 
            // crProperties
            // 
            this.crProperties.Appearance.Options.UseTextOptions = true;
            this.crProperties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.crProperties.Name = "crProperties";
            this.crProperties.Properties.Caption = "��������";
            // 
            // erIdentifier
            // 
            this.erIdentifier.Name = "erIdentifier";
            this.erIdentifier.Properties.Caption = "�������������";
            this.erIdentifier.Properties.FieldName = "Identifier";
            // 
            // erWidth
            // 
            this.erWidth.Name = "erWidth";
            this.erWidth.Properties.Caption = "������, ����";
            this.erWidth.Properties.FieldName = "Width";
            this.erWidth.Properties.RowEdit = this.teNumeric;
            // 
            // erInvNumber
            // 
            this.erInvNumber.Name = "erInvNumber";
            this.erInvNumber.Properties.Caption = "����������� �����";
            this.erInvNumber.Properties.FieldName = "InvNumber";
            this.erInvNumber.Properties.RowEdit = this.teMax50;
            // 
            // erWorkDefault
            // 
            this.erWorkDefault.Height = 16;
            this.erWorkDefault.Name = "erWorkDefault";
            this.erWorkDefault.Properties.Caption = "������ �� ���������";
            this.erWorkDefault.Properties.FieldName = "IdWork";
            this.erWorkDefault.Properties.RowEdit = this.leWork;
            // 
            // erUseDefault
            // 
            this.erUseDefault.Name = "erUseDefault";
            this.erUseDefault.Properties.Caption = "������������ �� ��������� ��� �������";
            this.erUseDefault.Properties.FieldName = "Default";
            this.erUseDefault.Properties.RowEdit = this.ceBox;
            // 
            // erOutId
            // 
            this.erOutId.Name = "erOutId";
            this.erOutId.Properties.Caption = "��� ������� ����";
            this.erOutId.Properties.FieldName = "IdOutLink";
            this.erOutId.Properties.RowEdit = this.teMax20;
            // 
            // crSensor
            // 
            this.crSensor.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.crSensor.Appearance.Options.UseFont = true;
            this.crSensor.Appearance.Options.UseTextOptions = true;
            this.crSensor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.crSensor.Name = "crSensor";
            this.crSensor.Properties.Caption = "������ �������� ���������";
            // 
            // erSensorPresent
            // 
            this.erSensorPresent.Name = "erSensorPresent";
            this.erSensorPresent.Properties.Caption = "������������ ������ ���������";
            this.erSensorPresent.Properties.FieldName = "HasSensor";
            this.erSensorPresent.Properties.RowEdit = this.ceBox;
            // 
            // erIsUseAgregatState
            // 
            this.erIsUseAgregatState.Name = "erIsUseAgregatState";
            this.erIsUseAgregatState.Properties.Caption = " ������������ ��������� ��������� �������";
            this.erIsUseAgregatState.Properties.FieldName = "IsUseAgregatState";
            this.erIsUseAgregatState.Properties.RowEdit = this.ceBox;
            // 
            // crValueSensor
            // 
            this.crValueSensor.Appearance.Options.UseTextOptions = true;
            this.crValueSensor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.crValueSensor.Name = "crValueSensor";
            this.crValueSensor.Properties.Caption = "��������� ��������� �������";
            // 
            // erSensorAllgorithm
            // 
            this.erSensorAllgorithm.Name = "erSensorAllgorithm";
            this.erSensorAllgorithm.Properties.Caption = "�������� �������";
            this.erSensorAllgorithm.Properties.FieldName = "SensorAlgorithm";
            this.erSensorAllgorithm.Properties.RowEdit = this.leSensorAlgorithms;
            // 
            // erCoefficient
            // 
            this.erCoefficient.Appearance.Options.UseTextOptions = true;
            this.erCoefficient.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.erCoefficient.Name = "erCoefficient";
            this.erCoefficient.Properties.Caption = "�������� ����������� �";
            this.erCoefficient.Properties.FieldName = "CoefficientK";
            this.erCoefficient.Properties.RowEdit = this.kTextEdit;
            this.erCoefficient.Properties.ToolTip = "�������� ����������� K";
            // 
            // erShifter
            // 
            this.erShifter.Appearance.Options.UseTextOptions = true;
            this.erShifter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.erShifter.Name = "erShifter";
            this.erShifter.Properties.Caption = "�������� ������ B";
            this.erShifter.Properties.FieldName = "ShiftB";
            this.erShifter.Properties.RowEdit = this.bTextEdit;
            this.erShifter.Properties.ToolTip = "�������� ������ B";
            // 
            // erSigned
            // 
            this.erSigned.Appearance.Options.UseTextOptions = true;
            this.erSigned.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.erSigned.Name = "erSigned";
            this.erSigned.Properties.Caption = "������� �����";
            this.erSigned.Properties.FieldName = "Signed";
            this.erSigned.Properties.RowEdit = this.repCheckEdit1;
            this.erSigned.Properties.ToolTip = "������� ��������������� �����";
            // 
            // erTarirovka
            // 
            this.erTarirovka.Appearance.Options.UseTextOptions = true;
            this.erTarirovka.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.erTarirovka.Name = "erTarirovka";
            this.erTarirovka.Properties.Caption = "������������ �������";
            this.erTarirovka.Properties.FieldName = "Id";
            this.erTarirovka.Properties.ReadOnly = true;
            this.erTarirovka.Properties.RowEdit = this.tarButtonEdit;
            this.erTarirovka.Properties.ToolTip = "������������ �������";
            // 
            // crLogicState
            // 
            this.crLogicState.Appearance.Options.UseTextOptions = true;
            this.crLogicState.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.crLogicState.Name = "crLogicState";
            this.crLogicState.Properties.Caption = "��������� ��������� ����������� ������� �������� ���������";
            // 
            // erLogicState
            // 
            this.erLogicState.Name = "erLogicState";
            this.erLogicState.Properties.Caption = "�������� ���������";
            this.erLogicState.Properties.FieldName = "LogicState";
            this.erLogicState.Properties.RowEdit = this.ceBox;
            // 
            // erBounceSecondsSensorNoActive
            // 
            this.erBounceSecondsSensorNoActive.Name = "erBounceSecondsSensorNoActive";
            this.erBounceSecondsSensorNoActive.Properties.Caption = "����� �������� � ���������� ��������� (���)";
            this.erBounceSecondsSensorNoActive.Properties.FieldName = "BounceSecondsSensorNoActive";
            // 
            // crSensorState
            // 
            this.crSensorState.Appearance.Options.UseTextOptions = true;
            this.crSensorState.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.crSensorState.Name = "crSensorState";
            this.crSensorState.Properties.Caption = "��������� ��������� �����������/��������";
            // 
            // erMinValue
            // 
            this.erMinValue.Name = "erMinValue";
            this.erMinValue.Properties.Caption = "������ ������";
            this.erMinValue.Properties.FieldName = "MinStateValue";
            this.erMinValue.Properties.RowEdit = this.teNumeric;
            // 
            // erMaxValue
            // 
            this.erMaxValue.Name = "erMaxValue";
            this.erMaxValue.Properties.Caption = "������� ������";
            this.erMaxValue.Properties.FieldName = "MaxStateValue";
            this.erMaxValue.Properties.RowEdit = this.teNumeric;
            // 
            // gcVehicles
            // 
            this.gcVehicles.Location = new System.Drawing.Point(13, 363);
            this.gcVehicles.MainView = this.gvVehicles;
            this.gcVehicles.Name = "gcVehicles";
            this.gcVehicles.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleTeam,
            this.rleVehicles,
            this.repositoryItemLookUpEdit1,
            this.rleSensors,
            this.rleStates});
            this.gcVehicles.Size = new System.Drawing.Size(601, 175);
            this.gcVehicles.TabIndex = 48;
            this.gcVehicles.UseEmbeddedNavigator = true;
            this.gcVehicles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicles});
            // 
            // gvVehicles
            // 
            this.gvVehicles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.TID,
            this.VID,
            this.SID,
            this.StID});
            this.gvVehicles.GridControl = this.gcVehicles;
            this.gvVehicles.Name = "gvVehicles";
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            // 
            // TID
            // 
            this.TID.AppearanceHeader.Options.UseTextOptions = true;
            this.TID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TID.Caption = "������";
            this.TID.ColumnEdit = this.rleTeam;
            this.TID.FieldName = "TID";
            this.TID.Name = "TID";
            this.TID.Visible = true;
            this.TID.VisibleIndex = 0;
            // 
            // rleTeam
            // 
            this.rleTeam.AutoHeight = false;
            this.rleTeam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTeam.Name = "rleTeam";
            // 
            // VID
            // 
            this.VID.AppearanceHeader.Options.UseTextOptions = true;
            this.VID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.VID.Caption = "������������ ��������";
            this.VID.ColumnEdit = this.rleVehicles;
            this.VID.FieldName = "VID";
            this.VID.Name = "VID";
            this.VID.Visible = true;
            this.VID.VisibleIndex = 1;
            // 
            // rleVehicles
            // 
            this.rleVehicles.AutoHeight = false;
            this.rleVehicles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehicles.Name = "rleVehicles";
            // 
            // SID
            // 
            this.SID.AppearanceHeader.Options.UseTextOptions = true;
            this.SID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SID.Caption = "���������� ������";
            this.SID.ColumnEdit = this.rleSensors;
            this.SID.FieldName = "SID";
            this.SID.Name = "SID";
            this.SID.Visible = true;
            this.SID.VisibleIndex = 2;
            // 
            // rleSensors
            // 
            this.rleSensors.AutoHeight = false;
            this.rleSensors.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleSensors.Name = "rleSensors";
            // 
            // StID
            // 
            this.StID.AppearanceCell.Options.UseTextOptions = true;
            this.StID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.StID.AppearanceHeader.Options.UseTextOptions = true;
            this.StID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StID.Caption = "������� ��������";
            this.StID.ColumnEdit = this.rleStates;
            this.StID.FieldName = "StID";
            this.StID.Name = "StID";
            this.StID.Visible = true;
            this.StID.VisibleIndex = 3;
            // 
            // rleStates
            // 
            this.rleStates.AutoHeight = false;
            this.rleStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleStates.Name = "rleStates";
            this.rleStates.NullText = "";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // Agregat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 591);
            this.Controls.Add(this.gcVehicles);
            this.Controls.Add(this.pgProperties);
            this.Controls.Add(this.lbVehicles);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.lbGroup);
            this.Name = "Agregat";
            this.Controls.SetChildIndex(this.lbGroup, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            this.Controls.SetChildIndex(this.lbVehicles, 0);
            this.Controls.SetChildIndex(this.pgProperties, 0);
            this.Controls.SetChildIndex(this.gcVehicles, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leSensorAlgorithms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleSensors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbGroup;
        private DevExpress.XtraEditors.LabelControl lbVehicles;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgProperties;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crProperties;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIdentifier;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erWidth;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erInvNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erWorkDefault;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erUseDefault;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSensorPresent;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutId;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teNumeric;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teMax50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teMax20;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leWork;
        private DevExpress.XtraGrid.GridControl gcVehicles;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn TID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTeam;
        private DevExpress.XtraGrid.Columns.GridColumn VID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn SID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleSensors;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn StID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleStates;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crSensor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsUseAgregatState;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crSensorState;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMinValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMaxValue;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crLogicState;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erLogicState;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leSensorAlgorithms;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSensorAllgorithm;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erBounceSecondsSensorNoActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit kTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit bTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit tarButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crValueSensor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erCoefficient;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erShifter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTarirovka;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSigned;
    }
}
