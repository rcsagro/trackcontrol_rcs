namespace Agro.GridEditForms
{
    partial class AgregatGrp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leWork = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.teOutLink = new DevExpress.XtraEditors.TextEdit();
            this.leOutLink = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLink.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // leWork
            // 
            this.leWork.Location = new System.Drawing.Point(156, 180);
            this.leWork.Name = "leWork";
            this.leWork.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leWork.Properties.Appearance.Options.UseFont = true;
            this.leWork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leWork.Properties.DisplayMember = "Name";
            this.leWork.Properties.NullText = "";
            this.leWork.Properties.ValueMember = "id";
            this.leWork.Size = new System.Drawing.Size(458, 21);
            this.leWork.TabIndex = 23;
            this.leWork.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.leWork_ButtonClick);
            this.leWork.EditValueChanged += new System.EventHandler(this.leWork_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(25, 184);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(125, 14);
            this.labelControl8.TabIndex = 22;
            this.labelControl8.Text = "������ �� ���������";
            // 
            // teOutLink
            // 
            this.teOutLink.EditValue = "";
            this.teOutLink.Location = new System.Drawing.Point(156, 219);
            this.teOutLink.Name = "teOutLink";
            this.teOutLink.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teOutLink.Properties.Appearance.Options.UseFont = true;
            this.teOutLink.Properties.Appearance.Options.UseTextOptions = true;
            this.teOutLink.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teOutLink.Properties.MaxLength = 20;
            this.teOutLink.Size = new System.Drawing.Size(215, 21);
            this.teOutLink.TabIndex = 48;
            this.teOutLink.TabStop = false;
            this.teOutLink.EditValueChanged += new System.EventHandler(this.teOutLink_EditValueChanged);
            // 
            // leOutLink
            // 
            this.leOutLink.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leOutLink.Appearance.Options.UseFont = true;
            this.leOutLink.Location = new System.Drawing.Point(46, 222);
            this.leOutLink.Name = "leOutLink";
            this.leOutLink.Size = new System.Drawing.Size(104, 14);
            this.leOutLink.TabIndex = 47;
            this.leOutLink.Text = "��� ������� ����";
            // 
            // AgregatGrp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.teOutLink);
            this.Controls.Add(this.leOutLink);
            this.Controls.Add(this.leWork);
            this.Controls.Add(this.labelControl8);
            this.Name = "AgregatGrp";
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.leWork, 0);
            this.Controls.SetChildIndex(this.leOutLink, 0);
            this.Controls.SetChildIndex(this.teOutLink, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLink.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leWork;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit teOutLink;
        private DevExpress.XtraEditors.LabelControl leOutLink;
    }
}
