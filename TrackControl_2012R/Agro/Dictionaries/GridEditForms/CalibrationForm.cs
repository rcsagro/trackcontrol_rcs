﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using System.Diagnostics;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using LocalCache;
using TrackControl.General.Core;
using TrackControl.General.DAL;
using TrackControl.General;
using Report;
using System.IO;
using System.Data.OleDb;
using Agro.Properties;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace Agro.GridEditForms
{
    public partial class ExcelFormDvx : DevExpress.XtraEditors.XtraForm
    {
        public class reportExls
        {
            private double uservalue;
            private double sensorvalue;

            public reportExls( double uservalue, double sensorvalue )
            {
                this.uservalue = uservalue;
                this.sensorvalue = sensorvalue;
            } // reportExls

            public double UserValue
            {
                get { return uservalue; }
            }

            public double SensorValue
            {
                get { return sensorvalue; }
            }
        } // reportExls

        public class StartIE
        {
            public StartIE( string msg )
            {
                XtraMessageBox.Show( msg + "\n" + Resources.DriverOfficeFileError, Resources.ErrorCalibration, MessageBoxButtons.OK );
                ProcessStartInfo startInfo = new ProcessStartInfo( "IExplore.exe" );
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.Arguments = "http://www.microsoft.com/en-us/download/details.aspx?id=23734";
                Process.Start( startInfo );
            }
        }

        private ReportBase<reportExls, int> ReportingExcel;
        private List<string> listNumCol;
        private BindingSource bs;
        private int agregat_id = -5;
        private int isTypeExcel = -1;
        private const int MICROSOFT_EXCEL_PRESENT = 0; // Microsoft Office Excel program
        private const int DRIVER_EXCEL_PRESENT = -1; // 2007 Office System Driver: Data Connectivity Components
        private string strExcelProvider = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"{1};HDR=YES\";";
        private const string COLUMN_USER_VALUE = "UserValue";
        private const string COLUMN_SENSOR_VALUE = "SensorValue";
        
        public ExcelFormDvx()
        {
            InitializeComponent();

            init();
            listNumCol = new List<string>();
            ReportingExcel =
                new ReportBase<reportExls, int>( Controls, compositeLink1, printingSystem1, gridViewExcel, listNumCol );

            isExcelExist();

            //gridCntrExcel.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_BttnClick;
            //NavigatorCustomButton bttn = gridCntrExcel.EmbeddedNavigator.Buttons.CustomButtons.Add();
            //bttn.ImageIndex = 0;
            //bttn.Tag = "Adding";
            //bttn.Hint = Resources.AddNewData;
            //gridCntrExcel.EmbeddedNavigator.Buttons.ImageList = imageList1;

            //gridCntrExcel.EmbeddedNavigator.Buttons.Append.Visible = false;
            //gridCntrExcel.EmbeddedNavigator.Buttons.EndEdit.Visible = true;
            //gridCntrExcel.EmbeddedNavigator.Buttons.Remove.Visible = true;
            //gridCntrExcel.EmbeddedNavigator.Buttons.CancelEdit.Visible = true;
            //gridCntrExcel.EmbeddedNavigator.Buttons.Edit.Visible = true;
            //gridCntrExcel.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
            FormClosing += ExcelFormDvx_FormClosing;
        }

        private void bindingNavigatorAddNewItem_Click()
        {
            //atlantaDataSet.sensorcoefficientRow newSensCoef_row =
            //    ( atlantaDataSet.sensorcoefficientRow )atlDataSet.sensorcoefficient.NewRow();
            //newSensCoef_row.b = 0;
            ////newSensCoef_row.id;
            //newSensCoef_row.K = 0;
            //newSensCoef_row.Sensor_id = agregat_id;
            //newSensCoef_row.SensorValue = 0;
            //newSensCoef_row.UserValue = 0;
            //atlDataSet.sensorcoefficient.AddsensorcoefficientRow( newSensCoef_row );
            //sensorcoefficientBindingSource.EndEdit();
        }

        void ExcelFormDvx_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
        {
            try
            {
                var appr = new LinearApproximation();

                foreach (DataRowView rowV in bs.List)
                {
                    double x = Convert.ToDouble(rowV["SensorValue"]);
                    double y = Convert.ToDouble(rowV["UserValue"]);

                    appr.AddPoint(x, y);
                }

                var dat = new List<double>(appr.Keys);
                PrepareContext(bs, "UserValue", dat);
                var cr = new CalibrationRecord();
                foreach (DataRowView rowV in bs.List)
                {
                    double x = Convert.ToDouble(rowV["SensorValue"]);

                    if (appr.Keys.Contains(x))
                    {
                        LinearApproximation.ValuePoint v = appr[x];

                        if (!rowV["UserValue"].Equals(v.Y))
                            rowV["UserValue"] = v.Y;

                        if (!rowV["K"].Equals(v.K))
                            rowV["K"] = v.K;

                        if (!rowV["b"].Equals(v.b))
                            rowV["b"] = v.b;
                    }
                    cr.Id = Convert.ToInt64(rowV["Id"]);
                    cr.UserValue = Convert.ToDouble(rowV["UserValue"]);
                    cr.SensorValue= Convert.ToDouble(rowV["SensorValue"]);
                    cr.K = Convert.ToDouble(rowV["K"]);
                    cr.b = Convert.ToDouble(rowV["b"]);
                    cr.Save();
                    new CalibrationRecord(Convert.ToInt64(rowV["Id"]), Convert.ToDouble(rowV["UserValue"]),
                                                   Convert.ToDouble(rowV["SensorValue"]), Convert.ToDouble(rowV["K"]),
                                                   Convert.ToDouble(rowV["b"])).Save(agregat_id);

                }
            }
            catch (LinearApproximationException ex)
            {
                XtraMessageBox.Show(ex.GetText, "Exception");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, "Updating error.");
            }

            //bs.EndEdit();
            //bs.Filter = "";
        }

        public int AgregatIdentificator
        {
            set { 
                agregat_id = value;
                SetDataSource();
            }
        }

        public BindingSource BindingSource
        {
            set
            {
                if( value == null ) 
                    return;

                bs = value;
                gridCntrExcel.DataSource = bs;
                UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, Resources.Entrence, agregat_id);
            }
        }

        public BindingList<CalibrationRecord> DataSource
        {
            set
            {
                if (value == null)
                    return;

                //bs = value;
                gridCntrExcel.DataSource = value;
                UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, Resources.Entrence, agregat_id);
            }
        }


        private void isExcelExist()
        {
            isTypeExcel = -1;

            try
            {
                Excel ex = null;
                ex = new Excel();

                if( ex != null )
                {
                    isTypeExcel = 0; // Microsoft Office Excel program присутствует на локальном компьютере
                    ex.Dispose();
                    return;
                }
            }
            catch( Exception e )
            {
                // to do
            }
        } // isExcelExist

        public void DraftWithMicrosoftExsel()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.OverwritePrompt = true;
            dlg.CreatePrompt = true;
            dlg.RestoreDirectory = false;
            dlg.CheckFileExists = false;
            dlg.CheckPathExists = false;
            dlg.Filter = "Microsoft Excel document (*.xls)|*.xls";
            dlg.Title = Resources.SaveCoeffTable;
            dlg.FileName = "Calibration.xls";

            if( dlg.ShowDialog() == DialogResult.OK )
            {
                listNumCol.Add( COLUMN_USER_VALUE );
                listNumCol.Add( COLUMN_SENSOR_VALUE );
                ReportingExcel.CreateBindDataList();
                ReportingExcel.AddDataToBindList( new reportExls( 0, 0 ) );
                ReportingExcel.CreateElementOtherReport();
                ReportingExcel.ExportDataToExcel( dlg.FileName );
                ReportingExcel.DeleteData();
            } // if
        } // DraftWithMicrosoftExsel

        public void DraftWithDriverExcel()
        {
            try
            {
                bool b2007 = false; // Microsoft Office 2007 или 2003
                DataTable dtData = null;
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel file(*.xls)|*.xls";
                dlg.Title = Resources.SaveCoeffTable;
                dlg.DefaultExt = "xls";
                dlg.FileName = "Calibration0";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    if( File.Exists( dlg.FileName ) )
                        File.Delete( dlg.FileName );

                    dtData = new DataTable();
                    Random rndm = new Random();
                    int num = rndm.Next( 50 );
                    dtData.TableName = "Calibration" + Convert.ToString( num );
                    DataColumn dCol0 = new DataColumn( Resources.UsersColumn );
                    dtData.Columns.Add( dCol0 );
                    DataColumn dCol1 = new DataColumn( Resources.MeasuredColumn );
                    dtData.Columns.Add( dCol1 );

                    listNumCol.Add( COLUMN_USER_VALUE );
                    listNumCol.Add( COLUMN_SENSOR_VALUE );

                    for( int i = 1; i < 11; i++ )
                    {
                        double uservalue = i;
                        double sensorvalue = i;

                        DataRow rw = dtData.NewRow();
                        rw[Resources.UsersColumn] = uservalue;
                        rw[Resources.MeasuredColumn] = sensorvalue;
                        dtData.Rows.Add( rw );
                    } // for

                    string sConnStr = String.Format( strExcelProvider, dlg.FileName, b2007 ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStr ) )
                    {
                        odcConn.Open();
                        using( OleDbCommand odcComm = new OleDbCommand() { Connection = odcConn } )
                        {
                            // Создание таблицы
                            odcComm.CommandText = GenerateSqlStatementCreateTable( dtData );
                            odcComm.ExecuteNonQuery();

                            DataRow dr;
                            OleDbParameter odpParam;

                            // Генерируем скрипт создания строк со значениями (в качестве параметров)
                            string sColumns, sParameters;
                            GenerateColumnsString( dtData, out sColumns, out sParameters );

                            for( int i = 0; i < dtData.Rows.Count; i++ )
                            {
                                dr = dtData.Rows[i];
                                // Устанавливаем параметр для INSERT
                                odcComm.Parameters.Clear();

                                for( int j = 0; j < dtData.Columns.Count; j++ )
                                {
                                    odpParam = new OleDbParameter();
                                    odpParam.ParameterName = "@p" + j;

                                    odpParam.Value = dr.IsNull( j ) ? DBNull.Value : dr[j];

                                    odcComm.Parameters.Add( odpParam );
                                } // for

                                odcComm.CommandText = string.Format( TrackControlQuery.ExcelFormDvx.InsertInto, dtData.TableName, sColumns, sParameters );
                                odcComm.ExecuteNonQuery();
                            } // for

                            odcConn.Close();
                        } // using 
                    } // using
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorCalibration, MessageBoxButtons.OK );
            }
        } // DraftWithDriverExcel

        // Создает SQL-скрипт для создания таблицы, в соответствии с DataTable
        // Возвращает запрос 'CREATE TABLE...'
        private static string GenerateSqlStatementCreateTable( DataTable dtData )
        {
            StringBuilder sbCreateTable = new StringBuilder();

            DataColumn dc;

            sbCreateTable.AppendFormat( "CREATE TABLE {0} (", dtData.TableName );

            for( int i = 0; i < dtData.Columns.Count; i++ )
            {
                dc = dtData.Columns[i];

                if( i != 0 )
                    sbCreateTable.Append( "," );

                string dataType = dc.DataType.Equals( typeof( double ) ) ? "DOUBLE" : "NVARCHAR";

                sbCreateTable.AppendFormat( "[{0}] {1}", dc.ColumnName, dataType );
            }

            sbCreateTable.Append( ")" );

            return sbCreateTable.ToString();
        } // GenerateSqlStatementCreateTable

        // Создает список столбцов ([columnname0],[columnname1],[columnname2])
        // и соответствующих им параметров (@p0,@p1,@p2)
        // В качестве разделителя используется запятая
        private static void GenerateColumnsString( DataTable dtData, out string sColumns, out string sParams )
        {
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbParams = new StringBuilder();

            for( int i = 0; i < dtData.Columns.Count; i++ )
            {
                if( i != 0 )
                {
                    sbColumns.Append( ',' );
                    sbParams.Append( ',' );
                }

                sbColumns.AppendFormat( "[{0}]", dtData.Columns[i].ColumnName );
                sbParams.AppendFormat( "@p{0}", i );
            }

            sColumns = sbColumns.ToString();
            sParams = sbParams.ToString();
        } // GenerateColumnsString

        private void btnClose_Click_1( object sender, EventArgs e )
        {
            Close();
        }

        private void btnSetup_Click_1( object sender, EventArgs e )
        {
            UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, btnSetup.Text, agregat_id);
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                DraftWithMicrosoftExsel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                DraftWithDriverExcel();
            }
        }

        private struct sValues
        {
            public string uval;
            public string sval;
        };

        List<sValues> lstValues = null;

        public void OpenWithMisrosoftExcel()
        {
            bs.EndEdit();

            int iCount = 0;
            int cols = 0;
            int rows = 0;
            lstValues = new List<sValues>();

            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel document(*.xls;*.xlsx)|*.xls;*.xlsx|All files (*.*)|*.*";
                dlg.Title = Resources.OpenCoeffTable;

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    string[,] ceValues = null;
                    bool flagWorDocument = false;

                    using( Excel ex = new Excel() )
                    {
                        if( ex.OpenDocument( dlg.FileName ) )
                        {
                            cols = Math.Min( ex.ColumnMax(), ExcelColumnsDictionary );
                            rows = ( int )ex.RowMax() - 1;
                            ceValues = new string[rows, cols];
                            int k = 0;
                            int m = 0;
                            flagWorDocument = true;

                            for( int i = 2; i <= ( rows + 1 ); i++ )
                            {
                                for( int j = 0; j < cols; j++ )
                                {
                                    string range = ex.ParseColNum( j ) + i;
                                    ceValues[k, m] = ex.GetValue( range );
                                    m++;
                                } // for
                                k++;
                                m = 0;
                            } // for

                            ex.CloseDocument();
                        } // if
                    } // using

                    double temp = 0;

                    // проверяем значения первого и второго столбца
                    for( int h = 0; h < rows; h++ )
                    {
                        try
                        {
                            temp = Convert.ToDouble( ceValues[h, 0] );
                            temp = Convert.ToDouble( ceValues[h, 1] );
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }

                        sValues vls = new sValues();
                        vls.uval = ceValues[h, 0];
                        vls.sval = ceValues[h, 1];
                        lstValues.Add( vls );
                    } // for

                    if( !flagWorDocument )
                    {
                        XtraMessageBox.Show( Resources.ErrorDocCalibration, Resources.ErrorCalibration, MessageBoxButtons.OK );
                        return;
                    }

                    Dictionary<double, double> excel = new Dictionary<double, double>( rows );

                    double uval_prev = 0, sval_prev = 0;
                    double uval = 0.0;
                    double sval = 0.0;

                    for( iCount = 0; iCount < lstValues.Count; iCount++ )
                    {
                        sValues sV = lstValues[iCount];

                        uval = Convert.ToDouble( sV.uval );
                        sval = Convert.ToDouble( sV.sval );
                        // закоментировано -- aketner 05.11.2015
                        //if( uval_prev > uval )
                        //    throw new Exception( Resources.NotCorrectValue + " [" + uval + "] " + Resources.NumStr + " " + Convert.ToString( iCount + 2 ) );
                        //else
                        //    uval_prev = uval;

                        //if( sval_prev > sval )
                        //    throw new Exception( Resources.NotCorrectValue + " [" + sval + "] " + Resources.NumStr + " " + Convert.ToString( iCount + 2 ) );
                        //else
                        //    sval_prev = sval;
                    } // for

                    uval = 0.0;
                    sval = 0.0;

                    for( iCount = 0; iCount < lstValues.Count; iCount++ )
                    {
                        sValues sV = lstValues[iCount];

                        uval = Convert.ToDouble( sV.uval );
                        sval = Convert.ToDouble( sV.sval );
                        
                        if( !excel.ContainsKey( uval ) )
                        {
                            excel.Add( uval, sval );
                        }
                    }

                    PrepareContext( bs, "UserValue", excel.Keys );
                    SetDataSource();
                    gridViewExcel.RefreshData();
                    foreach( DataRowView rowV in bs.List)
                    {
                        double uvall = Convert.ToDouble( rowV["UserValue"] );
                        object svall = excel[uvall];

                        if( !rowV["SensorValue"].Equals( svall ) )
                        {
                            rowV["SensorValue"] = svall;
                            var cr = new CalibrationRecord
                                {
                                    Id = Convert.ToInt64(rowV["Id"]),
                                    UserValue = Convert.ToDouble(rowV["UserValue"]),
                                    SensorValue = Convert.ToDouble(rowV["SensorValue"])
                                };
                            cr.Save(agregat_id);
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message + " Строка: " + (iCount + 2) + ".\n" + "В документе всего " + (rows + 1) + " строк." , Resources.ErrorExcelTable, MessageBoxButtons.OK );
            }
        } // OpenWithMisrosoftExcel

        /// <summary>
        /// ограничение на количество столбцов при импорте справочника
        /// </summary>
        public virtual int ExcelColumnsDictionary
        {
            get { return 2; }
        }

        /// <summary>
        ///  Преобразует Excel-файл в DataTable
        ///  Таблица для загрузки данных
        ///  Полный путь к Excel-файлу
        ///  SQL - запрос. Используем $SHEETS$ для выборки по всем листам, если нужно
        /// </summary>
        public void OpenFileWithExcelDriver()
        {
            bs.EndEdit();

            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel document(*.xls;*.xlsx)|*.xls;*.xlsx|All files (*.*)|*.*";
                dlg.Title = Resources.OpenCoeffTable;

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    DataSet dataSet = new DataSet();
                    DataTable tableData = new DataTable();

                    // Для Excel 2007 или для Excel 2003
                    string sConnStrExcls = String.Format( strExcelProvider, dlg.FileName, dlg.FileName.EndsWith( ".xlsx" ) ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStrExcls ) )
                    {
                        odcConn.Open();
                        DataTable schemaTable = odcConn.GetOleDbSchemaTable( OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" } );
                        string sheet1 = ( string )schemaTable.Rows[0].ItemArray[2];

                        string sRequest = String.Format( TrackControlQuery.ExcelFormDvx.SelectFrom, sheet1 );
                        OleDbCommand odcComm = new OleDbCommand( sRequest, odcConn );
                        using( OleDbDataAdapter oddaAdapter = new OleDbDataAdapter( odcComm ) )
                            oddaAdapter.Fill( dataSet );

                        odcConn.Close();
                    } // using

                    tableData = dataSet.Tables[0];
                    lstValues = new List<sValues>();

                    if( tableData.Rows.Count <= 0 )
                    {
                        XtraMessageBox.Show( Resources.ErrorDocCalibration, Resources.ErrorCalibration, MessageBoxButtons.OK );
                        return;
                    }

                    Dictionary<double, double> excel = new Dictionary<double, double>( tableData.Rows.Count );

                    double uval_prev = 0, sval_prev = 0, temp = 0;

                    // проверяем значения первого и второго столбца
                    for( int h = 0; h < tableData.Rows.Count; h++ )
                    {
                        DataRow row = tableData.Rows[h];

                        try
                        {
                            temp = Convert.ToDouble( row[0] );
                            temp = Convert.ToDouble( row[1] );
                        }
                        catch( Exception ex )
                        {
                            continue;
                        }

                        sValues vls = new sValues();
                        vls.uval = row[0].ToString();
                        vls.sval = row[1].ToString();
                        lstValues.Add( vls );
                    } // for

                    for( int i = 0; i < lstValues.Count; i++ )
                    {
                        sValues vls = lstValues[i];
                        double uval = Convert.ToDouble( vls.uval );
                        double sval = Convert.ToDouble( vls.sval );

                        if( uval_prev > uval )
                            throw new Exception( Resources.NotCorrectValue + " [" + uval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            uval_prev = uval;

                        if( sval_prev > sval )
                            throw new Exception( Resources.NotCorrectValue + " [" + sval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            sval_prev = sval;
                    } // for

                    for( int i = 0; i < lstValues.Count; i++ )
                    {
                        sValues vls = lstValues[i];
                        double uval = Convert.ToDouble( vls.uval );
                        double sval = Convert.ToDouble( vls.sval );

                        if( !excel.ContainsKey( uval ) )
                        {
                            excel.Add( uval, sval );
                        }
                    } // for

                    PrepareContext( bs, "UserValue", excel.Keys );

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uval = Convert.ToDouble( rowV["UserValue"] );
                        object sval = excel[uval];

                        if( !rowV["SensorValue"].Equals( sval ) )
                        {
                            rowV["SensorValue"] = sval;
                        }
                    } // foreach
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorCalibration, MessageBoxButtons.OK );
            }
        } // OpenFileWithExcelDriver

        private void PrepareContext( BindingSource bs, string parameter_name, ICollection parameter_collection )
        {
            // коллекция, к которой нужно привести BindingSource 
            ArrayList collection = new ArrayList( parameter_collection );
            // совпадающие записи
            ArrayList coincides = new ArrayList();

            foreach( DataRowView rowV in bs.List )
            {
                object pval = rowV[parameter_name];

                if( collection.Contains( pval ) )
                {
                    collection.Remove( pval );

                    if( !coincides.Contains( pval ) )
                        coincides.Add( pval );
                }
            }

            List<DataRowView> Del = new List<DataRowView>();

            foreach( DataRowView rowV in bs.List )
            {
                object pval = rowV[parameter_name];

                if( coincides.Contains( pval ) )
                {
                    coincides.Remove( pval );
                }
                else
                {
                    if( collection.Count > 0 )
                    {
                        IEnumerator ie = collection.GetEnumerator( 0, 1 );
                        ie.MoveNext();
                        pval = ie.Current;
                        collection.Remove( pval );
                        rowV[parameter_name] = pval;
                    }
                    else
                    {
                        Del.Add( rowV );
                    }
                }
            }

            for( int i = Del.Count - 1; i >= 0; i-- )
            {
                Del[i].Delete();
            }

            foreach( object pval in collection )
            {
                var cr = new CalibrationRecord();
                cr.UserValue = (double) pval; 
                cr.Save(agregat_id);
                //atlantaDataSet.sensorcoefficientRow newSensCoef_row =
                //    ( atlantaDataSet.sensorcoefficientRow )atlDataSet.sensorcoefficient.NewRow();
                //newSensCoef_row.b = 0;
                //newSensCoef_row.K = 0;
                //newSensCoef_row.Sensor_id = agregat_id;
                //newSensCoef_row.SensorValue = 0;
                //newSensCoef_row.UserValue = ( double )pval; // 0;
                //atlDataSet.sensorcoefficient.AddsensorcoefficientRow( newSensCoef_row );
            }
            sensorcoefficientBindingSource.EndEdit();
        }

        private void btnLoad_Click_1( object sender, EventArgs e )
        {
            UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, btnLoad.Text, agregat_id);
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                OpenWithMisrosoftExcel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                OpenFileWithExcelDriver();
            }
        }

        /// Преобразует dataTable в Excel-файл
        /// Excel 2003 или новее
        public void SaveAsWithDriverExcel()
        {
            bs.EndEdit();

            try
            {
                bool b2007 = false; // Microsoft Office 2007 или 2003
                DataTable dtData = null;
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.OverwritePrompt = false;
                dlg.CreatePrompt = false;
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.DefaultExt = "xls";
                dlg.Filter = @"Office Excel file(*.xls)|*.xls";
                dlg.Title = Resources.SaveAsTableToExcel;
                dlg.FileName = "Calibration";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    if( File.Exists( dlg.FileName ) )
                        File.Delete( dlg.FileName );

                    dtData = new DataTable();
                    dtData.TableName = "Calibration";
                    DataColumn dCol0 = new DataColumn( Resources.UsersColumn );
                    dtData.Columns.Add( dCol0 );
                    DataColumn dCol1 = new DataColumn( Resources.MeasuredColumn );
                    dtData.Columns.Add( dCol1 );

                    listNumCol.Add( COLUMN_USER_VALUE );
                    listNumCol.Add( COLUMN_SENSOR_VALUE );

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uservalue = Convert.ToDouble( rowV["UserValue"] );
                        double sensorvalue = Convert.ToDouble( rowV["Sensorvalue"] );

                        DataRow rw = dtData.NewRow();
                        rw[Resources.UsersColumn] = uservalue;
                        rw[Resources.MeasuredColumn] = sensorvalue;
                        dtData.Rows.Add( rw );
                    } // foreach

                    string sConnStr = String.Format( strExcelProvider, dlg.FileName, b2007 ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStr ) )
                    {
                        odcConn.Open();
                        using( OleDbCommand odcComm = new OleDbCommand() { Connection = odcConn } )
                        {
                            // Создание таблицы
                            odcComm.CommandText = GenerateSqlStatementCreateTable( dtData );
                            odcComm.ExecuteNonQuery();

                            DataRow dr;
                            OleDbParameter odpParam;

                            // Генерируем скрипт создания строк со значениями (в качестве параметров)
                            string sColumns, sParameters;
                            GenerateColumnsString( dtData, out sColumns, out sParameters );

                            for( int i = 0; i < dtData.Rows.Count; i++ )
                            {
                                dr = dtData.Rows[i];
                                // Устанавливаем параметр для INSERT
                                odcComm.Parameters.Clear();

                                for( int j = 0; j < dtData.Columns.Count; j++ )
                                {
                                    odpParam = new OleDbParameter();
                                    odpParam.ParameterName = "@p" + j;

                                    odpParam.Value = dr.IsNull( j ) ? DBNull.Value : dr[j];

                                    odcComm.Parameters.Add( odpParam );
                                } // for

                                odcComm.CommandText = string.Format( TrackControlQuery.ExcelFormDvx.InsertInto, dtData.TableName, sColumns, sParameters );
                                odcComm.ExecuteNonQuery();
                            } // for

                            odcConn.Close();
                        } // using 
                    } // using
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorSaveExcel, MessageBoxButtons.OK );
            }
        } // SaveAsWithDriverExcel

        public void SaveAsWithExcel()
        {
            bs.EndEdit();
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel Book(*.xls)|*.xls";
                dlg.Title = Resources.SaveAsTableToExcel;
                dlg.FileName = "Calibration.xls";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    listNumCol.Add( COLUMN_USER_VALUE );
                    listNumCol.Add( COLUMN_SENSOR_VALUE );
                    ReportingExcel.CreateBindDataList();

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uservalue = Convert.ToDouble( rowV["UserValue"] );
                        double sensorvalue = Convert.ToDouble( rowV["Sensorvalue"] );

                        ReportingExcel.AddDataToBindList( new reportExls( uservalue, sensorvalue ) );
                    } // foreach

                    ReportingExcel.CreateElementOtherReport();
                    ReportingExcel.ExportDataToExcel( dlg.FileName );
                    ReportingExcel.DeleteData();
                } // if
            } // try
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message, Resources.ErrorSaveExcel );
            }
        } // SaveAsWithExcel

        private void btnSaveAs_Click_1( object sender, EventArgs e )
        {
            UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, btnSaveAs.Text, agregat_id);
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                SaveAsWithExcel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                SaveAsWithDriverExcel();
            }
        }

        private void init()
        {
            btnClose.Text = Resources.Close;
            btnClose.ToolTip = Resources.CloseTip;
            btnSetup.ToolTip = Resources.SetupTip;
            btnLoad.ToolTip = Resources.LoadTip;
            btnSaveAs.ToolTip = Resources.SaveAsTip;
            btnSetup.Text = Resources.ExcelForm_Default;
            btnLoad.Text = Resources.Load;
            btnSaveAs.Text = Resources.SaveAs;
            btnLog.Text = Resources.EventLog; 
            colKoeff.Caption = Resources.ExcelForm_Measured;
            colKoeff.ToolTip = Resources.ExcelForm_Measured;
            colValue.Caption = Resources.ExcelForm_Custom;
            colValue.ToolTip = Resources.ExcelForm_Custom;
            Text = Resources.TextTip;
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            UserLog.ViewLog(UserLogTypes.AgroAgregatCalibration, agregat_id);
        }

        private void gridViewExcel_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            string mes = string.Format(Resources.EditRecord, e.RowHandle > 0 ? (e.RowHandle + 1).ToString() : "", e.Column, e.Value);
            DataRow rowV = gridViewExcel.GetDataRow(e.RowHandle);
            SaveRow(rowV);
            UserLog.InsertLog(UserLogTypes.AgroAgregatCalibration, mes, agregat_id);
        }

        private void SaveRow(DataRow rowV)
        {
            var cr = new CalibrationRecord();
            long crId = 0;
            double parDouble = 0;
            Int64.TryParse(rowV["Id"].ToString(), out crId);
            cr.Id = crId;
            parDouble = 0;
            Double.TryParse(rowV["SensorValue"].ToString(), out parDouble);
            cr.SensorValue = parDouble;
            parDouble = 0;
            Double.TryParse(rowV["UserValue"].ToString(), out parDouble);
            cr.UserValue = parDouble;
            parDouble = 0;
            Double.TryParse(rowV["K"].ToString(), out parDouble);
            cr.K = parDouble;
            parDouble = 0;
            Double.TryParse(rowV["b"].ToString(), out parDouble);
            cr.b = parDouble;
            cr.Save(agregat_id);
        }

        private void EmbeddedNavigator_BttnClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowCalibrationRecod();
            }

        }

        private bool DeleteRowCalibrationRecod()
        {
            //var cr = (CalibrationRecord)gridViewExcel.GetRow(gridViewExcel.FocusedRowHandle);
            DataRow rowV = gridViewExcel.GetDataRow(gridViewExcel.FocusedRowHandle);
            long idRecord = 0;
            if (Int64.TryParse(rowV["Id"].ToString(),out idRecord))
            {

                if (DialogResult.No ==
                    XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                    return false;
                var cr = new CalibrationRecord();
                cr.Id = idRecord;
                return cr.Delete();
            }
            else
                return false;
        }

        private void gridViewExcel_InitNewRow(object sender, InitNewRowEventArgs e)
        {
             var cr = new CalibrationRecord();
             cr.Save(agregat_id);
            DataRow rowV = gridViewExcel.GetDataRow(e.RowHandle);
            rowV["Id"] = cr.Id;
            rowV["SensorValue"] = 0;
            rowV["UserValue"] = 0;
            rowV["K"] = 0;
            rowV["b"] = 0;
        }

        private void SetDataSource()
        {
            using (var daa = new DictionaryAgroAgregat(agregat_id))
            {
                this.BindingSource = new BindingSource { DataSource = daa.GetCalibrationDataTable() };
            }
        }
    }
}