﻿using System;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro.GridEditForms
{
    public partial class WorkTypeGroup : FormSample
    {
        public WorkTypeGroup()
        {
            InitThis();
        }

        public WorkTypeGroup(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroWorkTypeGroup(_Id);
            this.Text = this.Text + ": " + Resources.WorkTypesGroups;
            GetFields();
            bFormLoading = false;
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
        }

        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["IsSpeedControl"], chSpeedControl.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["IsDepthControl"], chDepthControl.EditValue);
        }

        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            var dictionaryAgroWorkTypeGroup = di as DictionaryAgroWorkTypeGroup;
            if (dictionaryAgroWorkTypeGroup != null)
            {
                dictionaryAgroWorkTypeGroup.IsSpeedControl = (bool)(chSpeedControl.EditValue ?? 0);
                dictionaryAgroWorkTypeGroup.IsDepthControl = (bool)(chDepthControl.EditValue ?? 0);
            }

        }

        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            var dictionaryAgroWorkTypeGroup = di as DictionaryAgroWorkTypeGroup;
            chSpeedControl.EditValue = dictionaryAgroWorkTypeGroup != null && dictionaryAgroWorkTypeGroup.IsSpeedControl;
            chDepthControl.EditValue = dictionaryAgroWorkTypeGroup != null && dictionaryAgroWorkTypeGroup.IsDepthControl;
        }

        private void WorkTypeGroup_Load(object sender, EventArgs e)
        {

        }

        private void chSpeedControl_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void chDepthControl_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void Localization()
        {
            chSpeedControl.Text = Resources.SpeedControl;
            chDepthControl.Text = Resources.DepthControl;
        }
    }
}
