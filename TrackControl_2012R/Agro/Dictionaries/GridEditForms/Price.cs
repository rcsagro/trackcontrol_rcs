using System;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General.DatabaseDriver;

namespace Agro.GridEditForms
{
    public partial class Price : FormSample
    {
        public Price()
        {
            InitThis();
        }
        public Price( GridView gvDict, bool bNew )
            : base( gvDict, bNew )
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroPrice( _Id );
            this.Text = this.Text + ": " + Resources.Prices;
            if( !bNew ) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if( bFormLoading ) return false;
            if( !DicUtilites.ValidateLookUp( leOrder, dxErrP ) |
                !DicUtilites.ValidateFieldsTextDoublePositive( tePrice, dxErrP ) |
                !DicUtilites.ValidateLookUp( leWork, dxErrP ) |
                !DicUtilites.ValidateLookUp( leMobitel, dxErrP ) |
                !DicUtilites.ValidateLookUp( leAgregat, dxErrP ) |
                !DicUtilites.ValidateLookUp( leUnit, dxErrP ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id_main"], leOrder.EditValue );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id_work"], leWork.EditValue );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id_mobitel"], leMobitel.EditValue );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id_agregat"], leAgregat.EditValue );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Id_unit"], leUnit.EditValue );
            _gvDict.SetRowCellValue( _gvDict.FocusedRowHandle, _gvDict.Columns["Price"], tePrice.Text );
        }
        public override void SetFields()
        {
            di.Comment = meComment.Text;
            ( di as DictionaryAgroPrice ).Id_main = ( int )( leOrder.EditValue ?? 0 );
            ( di as DictionaryAgroPrice ).Id_work = ( int )( leWork.EditValue ?? 0 );
            ( di as DictionaryAgroPrice ).Id_mobitel = ( int )( leMobitel.EditValue ?? 0 );
            ( di as DictionaryAgroPrice ).Id_agregat = ( int )( leAgregat.EditValue ?? 0 );
            ( di as DictionaryAgroPrice ).Id_unit = ( int )( leUnit.EditValue ?? 0 );
            ( di as DictionaryAgroPrice ).Price = Convert.ToDouble( tePrice.Text );
        }
        public override void GetFields()
        {
            meComment.Text = di.Comment;
            leOrder.EditValue = ( di as DictionaryAgroPrice ).Id_main;
            leWork.EditValue = ( di as DictionaryAgroPrice ).Id_work;
            leMobitel.EditValue = ( di as DictionaryAgroPrice ).Id_mobitel;
            leAgregat.EditValue = ( di as DictionaryAgroPrice ).Id_agregat;
            leUnit.EditValue = ( di as DictionaryAgroPrice ).Id_unit;
            tePrice.Text = ( di as DictionaryAgroPrice ).Price.ToString();
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            DicUtilites.LookUpLoad( leOrder, AgroQuery.Price.SelectAgroPrice );
            DicUtilites.LookUpLoad( leWork, AgroQuery.Price.SelectAgroWork );
            DicUtilites.LookUpLoad( leMobitel, AgroQuery.Price.SelectVehicleId );

            DicUtilites.LookUpLoad( leAgregat, AgroQuery.Price.SelectAgroAgregat );
            DicUtilites.LookUpLoad( leUnit, AgroQuery.Price.SelectAgroUnit );
        }

        private void leOrder_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void leWork_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void leMobitel_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void leAgregat_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void tePrice_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void leUnit_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        #region �����������
        private void Localization()
        {
            //leOrder.Properties.Columns[0].Caption = Resources.CommandsDateInit;
            labelControl6.Text = Resources.Command;
            labelControl4.Text = Resources.Work;
            labelControl5.Text = Resources.Car;
            labelControl7.Text = Resources.Agregat;
            labelControl8.Text = Resources.Price;
            labelControl9.Text = Resources.Unit;
        }
        #endregion
    }
}

