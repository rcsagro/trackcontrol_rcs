﻿namespace Agro.GridEditForms
{
    partial class WorkTypeGroup
    {
        /// <summary>
        ///Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.chSpeedControl = new DevExpress.XtraEditors.CheckEdit();
            this.chDepthControl = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSpeedControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDepthControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // chSpeedControl
            // 
            this.chSpeedControl.Location = new System.Drawing.Point(12, 171);
            this.chSpeedControl.Name = "chSpeedControl";
            this.chSpeedControl.Properties.Caption = " Контролировать скорость";
            this.chSpeedControl.Size = new System.Drawing.Size(284, 19);
            this.chSpeedControl.TabIndex = 28;
            this.chSpeedControl.EditValueChanged += new System.EventHandler(this.chSpeedControl_EditValueChanged);
            // 
            // chDepthControl
            // 
            this.chDepthControl.Location = new System.Drawing.Point(12, 196);
            this.chDepthControl.Name = "chDepthControl";
            this.chDepthControl.Properties.Caption = "Контролировать глубину";
            this.chDepthControl.Size = new System.Drawing.Size(284, 19);
            this.chDepthControl.TabIndex = 29;
            this.chDepthControl.EditValueChanged += new System.EventHandler(this.chDepthControl_EditValueChanged);
            // 
            // WorkTypeGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.chDepthControl);
            this.Controls.Add(this.chSpeedControl);
            this.Name = "WorkTypeGroup";
            this.Load += new System.EventHandler(this.WorkTypeGroup_Load);
            this.Controls.SetChildIndex(this.chSpeedControl, 0);
            this.Controls.SetChildIndex(this.chDepthControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSpeedControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDepthControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit chSpeedControl;
        private DevExpress.XtraEditors.CheckEdit chDepthControl;

    }
}
