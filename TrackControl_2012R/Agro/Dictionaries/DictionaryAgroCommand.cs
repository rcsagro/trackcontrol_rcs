using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    public class DictionaryAgroCommand:DictionaryItem
    {
        /// <summary>
        /// ���� ������� � ���������
        /// </summary>
        DateTime? _DateInit = null;

        public DateTime? DateInit
        {
            get { return _DateInit; }
            set { _DateInit = value; }
        }

        /// <summary>
        /// ���� ,� ������ ��������� ���� 
        /// </summary>
        DateTime? _DateComand = null;

        public DateTime? DateComand
        {
            get { return _DateComand; }
            set { _DateComand = value; }
        }

        public DictionaryAgroCommand()
        {
        }

        public DictionaryAgroCommand(int ID)
            : base(ID)
        {
            Sql = AgroQuery.DictionaryAgroCommand.SelectAgroPrice + ID;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    if (driverDb.GetValue(driverDb.GetOrdinal("DateInit")) != DBNull.Value)
                        _DateInit = driverDb.GetDateTime(driverDb.GetOrdinal("DateInit"));
                    if (driverDb.GetValue(driverDb.GetOrdinal("DateComand")) != DBNull.Value)
                        _DateComand = driverDb.GetDateTime("DateComand");
                }
                driverDb.CloseDataReader();
            }
        }

        public override int AddItem()
        {
            Sql = AgroQuery.DictionaryAgroCommand.InsertIntoAgroPrice;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_price");
            }
            return Id;
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroCommand.UpdateAgroPrice, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateInit", _DateInit);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateComand", _DateComand);
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            return false;
                    }
                    Sql = string.Format(AgroQuery.DictionaryAgroCommand.DeleteAgroPrice, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroCommand.SelectAgroPricet, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroCommand.SelectAgroPriceJornal;
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
