using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General.DatabaseDriver;
using TrackControl.Zones; 

namespace Agro.Dictionaries
{
    public class DictionaryAgroField : DictionaryItem
    {
        private const string TableName = "agro_field";
        public const int Pereezd = 0;
        /// <summary>
        /// ��� ����,������� ������������� ����
        /// </summary>
        private int _idZone;

        public static IZonesManager ZonesModel;

        public int IdZone
        {
            get { return _idZone; }
            set { 
                _idZone = value;
                if (value > 0)
                {
                    //Square = driverDb.GetScalarValueNull<Double>("SELECT  zones.Square FROM    zones WHERE   zones.Zone_ID = " + _Id_zone,0);
                    AreaGa = Math.Round(ZonesModel.GetById(_idZone).AreaGa,2); 
                }
                else
                AreaGa = 0;
            }
        }

        public DictionaryAgroFieldGrp Group  { get; set; }

        public int IdMain { get; set; }

        public double AreaGa { get; set; }

        /// <summary>
        /// ���� �������� (���������� ������������ � ������� ��-�� ������ �������������) � � ��������� ������� ������� �� ���������
        /// </summary>
        public bool IsOutOfDate { get; set; }

        public DictionaryAgroField()
        {
        }

        public DictionaryAgroField(int id):base(id)
        {
            GetProperties(id);
        }

        public override sealed void GetProperties(int id)
        {
            Sql = string.Format( @"SELECT   zones.Square, agro_field.* FROM agro_field 
            LEFT OUTER JOIN  zones ON agro_field.Id_zone = zones.Zone_ID
                    WHERE   agro_field.Id = {0}", id);

            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Id = id;
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    IdMain = driverDb.GetInt32("Id_main");
                    Group = new DictionaryAgroFieldGrp(IdMain);
                    _idZone = driverDb.GetInt32("Id_zone");
                    AreaGa = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "Square", 0));
                    AreaGa = Math.Round(AreaGa*100, 2);
                    IsOutOfDate = driverDb.GetBit("IsOutOfDate");

                }
                driverDb.CloseDataReader();
            }
        }

        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            Sql = AgroQuery.DictionaryAgroField.InsertIntoAgroField;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_field");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(5);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", Comment );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_main", IdMain );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_zone", _idZone );
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsOutOfDate", IsOutOfDate);
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroField.UpdateAgroField, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroField.DeleteAgroField, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroField.SelectAgroOrderT, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);

                Sql = string.Format(AgroQuery.DictionaryAgroField.SelectAgroOrdertControl, Id,
                                    (int) Consts.TypeControlObject.Field);
                links = links + driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            //��� ������� �� MIF ����� �� ����� ����� ���������� ��������.
            //������� �������� �� �� � ���� ����������� ���� ��� �����������.������ ���������
            return false;
            //_sSql = "SELECT   agro_field.Id FROM agro_field WHERE   agro_field.Name = ?Name"
            //    + " AND agro_field.Id <> " + Id;
            //MySqlParameter[] parDate = new MySqlParameter[1];
            //parDate[0] = new MySqlParameter("?Name", MySqlDbType.String);
            //parDate[0].Value = sNameForTest;
            //int Id_name = driverDb.GetScalarValueIntNull(_sSql, parDate);
            //if (Id_name > 0)
            //{
            //    XtraMessageBox.Show(Resources.FieldExist + " " + sNameForTest + "!", Resources.ApplicationName);
            //    return true;
            //}
            //else
            //    return false;

        }

        public static DataTable GetList(int idGroup = 0)
        {
            using (var driverDb = new DriverDb())
            {
                DataTable dt;
                driverDb.ConnectDb();
                if (idGroup == 0)
                {
                    string sql = AgroQuery.DictionaryAgroField.SelectAgroField;
                    dt = driverDb.GetDataTable(sql);
                    dt.Rows.Add(Pereezd, "  " + Resources.Move1);
                    dt.Rows.Add(Consts.ZONE_OUTSIDE_THE_LIST, "  " + Resources.FieldUnknown);
                    dt.DefaultView.Sort = "Name ASC";
                }
                else
                {
                    string sql = string.Format(AgroQuery.DictionaryAgroField.SelectAgroFieldGroup, idGroup);
                    dt = driverDb.GetDataTable(sql);
                }
                return dt;
            }
        }

        public int GetIdFromZoneId(int idZone)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroField.SelectAgroFieldZone, idZone);
            int fieldId = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                fieldId = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            return fieldId;
        }

        public override DataTable GetDocumentsList()
        {
            string sql = string.Format( GetDocumentListSql(), AgroQuery.SqlVehicleIdent, Id,
                                           (int)Consts.TypeControlObject.Field, "Id_field" );

            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroField.SelectAgroFieldJornal;
                return driverDb.GetDataTable(sql);
            }
        }


        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

        public static List<DictionaryAgroField> GetEntityList(int idMain =0)
        {
            return GetEntityList<DictionaryAgroField>(TableName, idMain);
        }

        public static void SetComboBoxDataSource(RepositoryItemComboBox  combo,int idMain = 0)
        {
            SetComboBoxDataSource<DictionaryAgroField>(TableName, combo, idMain);
        }
    }

}
