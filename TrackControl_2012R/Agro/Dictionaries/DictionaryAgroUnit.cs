using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    /// <summary>
    /// ������� ��������� �������
    /// </summary>
    public sealed class DictionaryAgroUnit : DictionaryItem 
    {
        /// <summary>
        /// ������� ��� ������� ���������
        /// </summary>
        private string _NameShort;
        public string NameShort
        {
            get { return _NameShort; }
            set { _NameShort = value; }
        }
        private double _Factor;
        /// <summary>
        /// ���������� ��������� � �������
        /// </summary>
        public double Factor
        {
            get { return _Factor; }
            set { _Factor = value; }
        }
        public DictionaryAgroUnit()
        {
        }
        public DictionaryAgroUnit(int ID)
            : base(ID)
        {
                Sql = "SELECT   agro_unit.* FROM agro_unit WHERE   agro_unit.Id = " + ID;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    _NameShort = TotUtilites.NdbNullReader(driverDb, "NameShort", "").ToString();
                    _Factor = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "Factor", 0));
                }
                driverDb.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            Sql = AgroQuery.DictionaryAgroUnit.InsertIntoAgroUnit;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_unit");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", Comment );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "NameShort", _NameShort );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Factor", _Factor );
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format( AgroQuery.DictionaryAgroUnit.UpdateAgroUnit, Id );
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroUnit.DeleteAgroUnit, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroUnit.SelectAgroUnitCount, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }
        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            Sql = string.Format( AgroQuery.DictionaryAgroUnit.SelectFromAgroUnit, Id );
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0 )
            {
                if (bViewInfo) XtraMessageBox.Show(Resources.UnitExist + " " + sNameForTest + "!", Resources.ApplicationName);
                return true;
            }
            else
                return false;

        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = AgroQuery.DictionaryAgroUnit.SelectAgroUnitList;
                DataTable dt = driverDb.GetDataTable(sql);
                dt.Rows.Add(0, "  " + Resources.Move1);
                return dt;
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroUnit.SelectAgroUnitJornal;
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
