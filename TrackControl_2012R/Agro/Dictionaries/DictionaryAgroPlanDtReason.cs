﻿using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    public class DictionaryAgroPlanDtReason : DictionaryItem, IDisposable  
    {
        public DictionaryAgroPlanDtReason()
        {

        }
        public DictionaryAgroPlanDtReason(int id)
            : base(id)
        {
            Id = id;
            Init (id);
        }
        #region Члены IDictionary


        public override  int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql =
                    string.Format(
                        "INSERT INTO agro_plan_downtimereason(ReasonName,ReasonComment) VALUES ({0}Name,{0}Comment)",
                        driverDb.ParamPrefics);
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_plan_downtimereason");
                return Id;
            }
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                    Sql = string.Format("DELETE FROM agro_plan_downtimereason WHERE ID = {0}", Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool EditItem()
        {
            try
            {
                using (var driverDb = new DriverDb())
                {
                    Sql = string.Format("UPDATE agro_plan_downtimereason SET ReasonName = {1}Name ,ReasonComment = {1}Comment WHERE (ID = {0})", Id, driverDb.ParamPrefics);
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format("SELECT   COUNT(agro_plan.Id) FROM   agro_plan WHERE   agro_plan.IdDowntimeReason  = {0} ",Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest,bool bViewInfo = true)
        { 
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql =
                    string.Format(@"SELECT   agro_plan_downtimereason.Id FROM agro_plan_downtimereason WHERE   agro_plan_downtimereason.ReasonName = {1}Name
              AND agro_plan_downtimereason.Id <> {0}", Id, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(string.Format("{0} {1}!", Resources.WorkTypeExist, sNameForTest), Resources.ApplicationName);
                return true;
            }
            else
                return false;
        }

        #endregion

        private void Init(int id)
        {
            Sql = string.Format("SELECT   agro_plan_downtimereason.* FROM agro_plan_downtimereason WHERE   agro_plan_downtimereason.Id = {0}", id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "ReasonName", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "ReasonComment", "").ToString();
                }
                driverDb.CloseDataReader();
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = "SELECT agro_plan_downtimereason.Id, agro_plan_downtimereason.ReasonName , agro_plan_downtimereason.ReasonComment FROM agro_plan_downtimereason  ORDER BY agro_plan_downtimereason.ReasonName";
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = " SELECT   agro_plan_downtimereason.Id, agro_plan_downtimereason.ReasonName FROM agro_plan_downtimereason ORDER BY agro_plan_downtimereason.ReasonName";
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
