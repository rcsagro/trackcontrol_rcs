using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    public sealed class DictionaryAgroPrice:DictionaryItem
    {
        /// <summary>
        /// ������ �� ������
        /// </summary>
        private int _Id_main;
        public int Id_main
        {
            get { return _Id_main; }
            set { _Id_main = value; }
        }
        /// <summary>
        /// ������ �� ������
        /// </summary>
        private int _Id_work;
        public int Id_work
        {
            get { return _Id_work; }
            set { _Id_work = value; }
        }
        /// <summary>
        /// ������ �� ������� ���������
        /// </summary>
        private int _Id_unit;
        public int Id_unit
        {
            get { return _Id_unit; }
            set { _Id_unit = value; }
        }
        /// <summary>
        /// ������������ ��������
        /// </summary>
        private int _Id_mobitel;
        public int Id_mobitel
        {
            get { return _Id_mobitel; }
            set { _Id_mobitel = value; }
        }
        /// <summary>
        /// �������� ������������
        /// </summary>
        private int _Id_agregat;
        public int Id_agregat
        {
            get { return _Id_agregat; }
            set { _Id_agregat = value; }
        }
        private double _Price;
        /// <summary>
        /// ��������
        /// </summary>
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public DictionaryAgroPrice()
        {
        }
        public DictionaryAgroPrice(int ID)
            : base(ID)
        {
            Sql = "SELECT   agro_pricet.* FROM agro_pricet WHERE   agro_pricet.Id = " + ID;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    //Name = TotUtilites.NDBNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    _Id_main = driverDb.GetInt32("Id_main");
                    _Id_work = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_work", 0));
                    _Id_mobitel = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_mobitel", 0));
                    _Id_agregat = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_agregat", 0));
                    _Id_unit = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_unit", 0));
                    _Price = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "Price", 0));
                }
                driverDb.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            Sql = AgroQuery.DictionaryAgroPrice.InsertIntoAgroPricet;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_pricet");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(7);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", _Id_main);
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_work", _Id_work );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_mobitel", _Id_mobitel );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_agregat", _Id_agregat );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_unit", _Id_unit );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Price", _Price );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", Comment );
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroPrice.UpdateAgroPricet, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroPrice.DeleteFromAgroPricet, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool TestLinks()
        {
                return true;

        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroPrice.SelectFromAgroPricet;
              
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
