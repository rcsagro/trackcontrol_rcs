using System.ComponentModel;
using System.Xml;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace Agro.Dictionaries
{
    //public class CalibrationTable
    //{
    //    public double UserValue { get; set; }
    //    public double SensorValue { get; set; }
    //}

    /// <summary>
    /// �������� ������������
    /// </summary>
    public class DictionaryAgroAgregat : DictionaryItem
    {
        //protected List<CalibrationTable> calibrTable = new List<CalibrationTable>();
        //protected BindingSource calibr = new BindingSource();

        //public List<CalibrationTable> CalibrTable
        //{
        //    get { return calibrTable; }
        //    set { calibrTable = value; }
        //}

        //public BindingSource Calibr
        //{
        //    get { return calibr; }
        //    set { calibr = value; }
        //}

        #region ����

        public double Width { get; set; }

        public ushort Identifier { get; set; }

        public bool Default { get; set; }

        public string InvNumber { get; set; }

        public int IdMain { get; set; }

        public int IdWork { get; set; }

        public bool HasSensor { get; set; }

        public bool IsUseAgregatState { get; set; }

        public double MinStateValue { get; set; }

        public double MaxStateValue { get; set; }

        public bool LogicState { get; set; }

        public int SensorAlgorithm { get; set; }

        public double CoefficientK { get; set; }

        public double ShiftB { get; set; }

        public bool Signed { get; set; }

        //public BindingSource Tarirovka { get; set; }
        //{
        //    set
        //    {
        //        if (value == null)
        //            return;

        //        calibr = value;
        //    }

        //    get { return calibr; }
        //}

        /// <summary>
        /// ����� �������� � ���������� ��������� (���)
        /// ��������:����. ����� �������� ������� ��� ���������� �������� �� ���� ��� �������� � ��������� ���������
        /// </summary>
        public int BounceSecondsSensorNoActive { get; set; }


        #endregion

        private const int ExcelFields = 5;

        public DictionaryAgroAgregat()
        {
            InvNumber = "";
        }

        public DictionaryAgroAgregat(int ID)
            : base(ID)
        {
            InvNumber = "";
            GetProperties(ID);
        }

        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0))
                return 0;

            if (Identifier > 0)
            {
                Sql = AgroQuery.DictionaryAgroAgregat.InsertIntoAgroAgregatIdent;
            }
            else
            {

                Sql = AgroQuery.DictionaryAgroAgregat.InsertIntoAgroAgregat;
            }
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_agregat");
                if (Default)
                    SetUniqueDefault();
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            //if (Identifier >= 0)
            //{
            driverDb.NewSqlParameterArray(19);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Identifier", Identifier);
            }
            else
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Identifier", (int) Identifier);
            }
            //}
            //else
            //    driverDb.NewSqlParameterArray(9);

            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name",
                (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Width", Width);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Def", Default);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Def", Default ? 1 : 0);
            }


            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", IdMain);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "InvNumber",
                (InvNumber.Length > 50) ? InvNumber.Substring(0, 50) : InvNumber);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", IdWork);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "OutLinkId", _idOutLink ?? "");
            //driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SensorAngleValue", SensorAngleValue);


            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "HasSensor", HasSensor);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsUseAgregatState", IsUseAgregatState);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LogicState", LogicState);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "HasSensor", HasSensor ? 1 : 0);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsUseAgregatState", IsUseAgregatState ? 1 : 0);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LogicState", LogicState ? 1 : 0);
            }
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "MinStateValue", MinStateValue);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "MaxStateValue", MaxStateValue);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SensorAlgorithm", SensorAlgorithm);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LogicStateBounce", BounceSecondsSensorNoActive);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "K", CoefficientK);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "B", ShiftB);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "S", Signed);
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroAgregat.UpdateAgroAgregatIdent, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                    if (Default)
                        SetUniqueDefault();
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No ==
                            XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            return false;
                    }
                    Sql = AgroQuery.DictionaryAgroAgregat.DeleteAgroAgregat + Id;
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            //_sSql = "SELECT   COUNT(agro_PriceT.Id) FROM   agro_PriceT WHERE   Id_agregat  = " + Id;
            //int iLinks = driverDb.GetScalarValueNull<Int32>(_sSql,0);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = AgroQuery.DictionaryAgroAgregat.SelectCountAgroOrderT + Id;
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
                Sql = "SELECT   COUNT(agro_ordert_control.Id) FROM   agro_ordert_control WHERE   Id_object  = " + Id
                      + " AND Type_object = " + (int) Consts.TypeControlObject.Agregat;
                links = links + driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public double GetWidthDefault()
        {
            Sql = AgroQuery.DictionaryAgroAgregat.SelectAgro_Agregat;
            double width = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                width = driverDb.GetScalarValueNull<double>(Sql, 0);
            }
            if (width == 0)
            {
                var aSet = new AgroSetItem();
                width = aSet.AgregatDefaultWidth;
            }
            return width;
        }

        public double GetWidthInOrderRecord(int idOrderT)
        {
            Sql = AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatWidth + idOrderT;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Width = driverDb.GetScalarValueNull<Double>(Sql, 0);
            }
            if (Width == 0)
            {
                var aSet = new AgroSetItem();
                Width = aSet.AgregatDefaultWidth;
            }
            return Width;
        }

        /// <summary>
        /// ���� ������� ����� ������ �� ���������
        /// </summary>
        private void SetUniqueDefault()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.UpdateAgroAgregatSet, Id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.ExecuteNonQueryCommand(Sql);
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            // ����� ��������� ����� �����������.������������� ������������ ��������      

            //_sSql = "SELECT   agro_agregat.Id FROM agro_agregat WHERE   agro_agregat.Name = ?Name"
            //    + " AND agro_agregat.Id <> " + Id;
            //MySqlParameter[] parDate = new MySqlParameter[1];
            //parDate[0] = new MySqlParameter("?Name", MySqlDbType.String);
            //parDate[0].Value = sNameForTest;
            //int Id_name = driverDb.GetScalarValueIntNull(_sSql, parDate);
            //if (Id_name > 0)
            //{
            //    XtraMessageBox.Show("�������� ������������ � ������ " + sNameForTest + " ��� ����������!", Resources.ApplicationName);
            //    return true;
            //}
            //else
            return false;

        }

        public override sealed void GetProperties(int id)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatFrom, id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    Width = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "Width", 0));
                    Identifier = Convert.ToUInt16(TotUtilites.NdbNullReader(driverDb, "Identifier", 0));
                    Default = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "Def", 0));
                    InvNumber = TotUtilites.NdbNullReader(driverDb, "InvNumber", "").ToString();
                    IdWork = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_work", 0));
                    IdMain = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_main", 0));
                    _idOutLink = TotUtilites.NdbNullReader(driverDb, "OutLinkId", "").ToString();
                    HasSensor = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "HasSensor", 0));
                    IsUseAgregatState = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "IsUseAgregatState", 0));
                    LogicState = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "LogicState", 0));
                    MinStateValue = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "MinStateValue", 0));
                    MaxStateValue = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "MaxStateValue", 0));
                    SensorAlgorithm = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "SensorAlgorithm", 0));
                    BounceSecondsSensorNoActive =
                        Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "LogicStateBounce", 0));
                    //����  ������ �� ��������� �� �������, �������� ������ �� ��������� ������ ��������� 
                    if ((IdMain > 0) && (IdWork == 0))
                    {
                        using (var daag = new DictionaryAgroAgregatGrp(IdMain))
                        {
                            IdWork = daag.Id_work;
                        }
                    }
                    CoefficientK = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "K", 0));
                    ShiftB = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "B", 0));
                    Signed = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "S", 0));
                }
                driverDb.CloseDataReader();
            }
        }

        public bool InitFromIdentifier(ushort identifier)
        {

            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatFrm, identifier);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Id = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (Id > 0)
            {
                GetProperties(Id);
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override string DictionaryName
        {
            get { return Resources.DictionaryAgregat; }
        }

        public override string ExcelFormatMessage
        {
            get
            {
                return "1 " + Resources.Column + " - " + Resources.NameRes.ToLower() + Environment.NewLine
                       + "2 " + Resources.Column + " - " + Resources.NumberInv.ToLower() + "(" + Resources.Optional +
                       ")" + Environment.NewLine
                       + "3 " + Resources.Column + " - " + Resources.Identifier.ToLower() + "(" + Resources.Optional +
                       ")" + Environment.NewLine
                       + "4 " + Resources.Column + " - " + Resources.WidthM.ToLower() + "(" + Resources.Optional + ")" +
                       Environment.NewLine
                       + "5 " + Resources.Column + " - " + Resources.Remark.ToLower() + "(" + Resources.Optional + ")";
            }
        }

        public override int ExcelColumnsDictionary
        {
            get { return ExcelFields; }
        }

        public override void AddFromExcel(params string[] cellValues)
        {
            Name = cellValues[0];
            if (Name == "")
                return;
            Identifier = 0;
            Width = 0;
            InvNumber = "";
            IdMain = 0;
            Comment = "";
            Default = false;
            if (cellValues.GetUpperBound(0) >= (ExcelFields - 4))
            {
                InvNumber = cellValues[1];
                if (cellValues.GetUpperBound(0) >= (ExcelFields - 3))
                {
                    int ident;
                    if (int.TryParse(cellValues[2], out ident))
                        Identifier = (ushort) ident;
                    if (cellValues.GetUpperBound(0) >= (ExcelFields - 2))
                    {
                        double w;
                        if (double.TryParse(cellValues[3], out w))
                            Width = w;
                        if (cellValues.GetUpperBound(0) >= (ExcelFields - 1))
                            Comment = cellValues[4];

                    }
                }
            }

            AddItem();
        }

        public int GetDefault()
        {
            Sql = AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatId;
            int def = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                def = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            return def;
        }

        public int GetDefaultForVehicle(int vehicleId)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatVehicleId, vehicleId);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Id = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            return Id;
        }

        public int GetDefaultSensorForVehicle(int vehicleId)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatVehicleIdSensor, vehicleId, Id);
            int idSensor = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                idSensor = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            return idSensor;
        }

        public bool GetLinkForVehicle(int vehicleId, out string agregatName)
        {
            agregatName = "";
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatVehicleIdAgro, vehicleId, Id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    using (var daa = new DictionaryAgroAgregat(driverDb.GetInt32("Id_agregat")))
                    {
                        agregatName = string.Format("{0}{1}{2}", daa.Name, Environment.NewLine, daa.InvNumber);
                    }
                    driverDb.CloseDataReader();
                    driverDb.CloseDbConnection();
                    return true;
                }
                driverDb.CloseDataReader();
            }
            return false;
        }

        public DataTable GetContent()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatVehicle, Id);
            DataTable dt = null;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                dt = driverDb.GetDataTable(Sql);
            }
            return dt;
        }

        public bool EditContent(int idRecord, int idVehicle, int idSensor, int idState)
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroAgregat.UpdateAgroAgregatVehicle, idRecord, idVehicle,
                    idSensor, idState);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.ExecuteNonQueryCommand(Sql);
                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return false;
            }

        }

        public int AddContent(int idVehicle, int idSensor, int idState)
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroAgregat.InsertIntoAgroAgregatVehicle, Id, idVehicle,
                    idSensor, idState);
                int lastId = 0;
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    lastId = driverDb.ExecuteReturnLastInsert(Sql);
                }
                return lastId;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return -1;
            }
        }

        public bool DeleteContent(int idRecord)
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroAgregat.DeleteAgroAgregatVehicle, idRecord);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.ExecuteNonQueryCommand(Sql);
                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return false;
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregat, 0);
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetGroupsList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatGroup, 1);
                return driverDb.GetDataTable(sql);
            }
        }

        public override DataTable GetDocumentsList()
        {
            string sql = string.Format(GetDocumentListSql(), AgroQuery.SqlVehicleIdent, Id,
                (int) Consts.TypeControlObject.Agregat, "Id_agregat");

            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable table = driverDb.GetDataTable(sql);
                driverDb.CloseDbConnection();
                return table;
            }
        }

        public static DataTable GetListForJournal()
        {

            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatJornal, 0);
                DataTable table = driverDb.GetDataTable(sql);
                table.Columns[0].ReadOnly = false;
                driverDb.CloseDbConnection();
                return table;
            }
        }

        public static DataTable GetAgroAlgorithmList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroWorkType.SelectAgroAlgorithmsList;
                return driverDb.GetDataTable(sql);
            }
        }

        public ExternalDevice GetAgroSensor(int idVehicle, int idMobitel, ref int idSensor)
        {
            ExternalDevice agroSensor = null;
            idSensor = GetDefaultSensorForVehicle(idVehicle);
            var calibrRecords = GetCalibrationRecords();
            bool isAgregatCalibrated = HasSensor && IsUseAgregatState && calibrRecords.Count > 0; 
            bool isUseAgregatKbs = HasSensor && IsUseAgregatState && calibrRecords.Count == 0; 
            if (idSensor == 0)
            {
                switch (SensorAlgorithm)
                {
                    case (int) (int) AlgorithmType.AGREGAT_LOGIC:
                        agroSensor = new LogicSensor((int) AlgorithmType.AGREGAT_LOGIC, idMobitel);
                        idSensor = agroSensor.Id;
                        if (idSensor > 0 && IsUseAgregatState)
                        {
                            ((LogicSensor) agroSensor).SetK(LogicState);
                            ((LogicSensor) agroSensor).BounceTimeSensorNoActive =
                                TimeSpan.FromSeconds(BounceSecondsSensorNoActive);
                        }
                        break;
                    case (int) (int) AlgorithmType.ANGLE:
                        agroSensor = isAgregatCalibrated ? new SensorCalibrated((int)AlgorithmType.ANGLE, idMobitel, calibrRecords) : new SensorCalibrated((int)AlgorithmType.ANGLE, idMobitel);
                        idSensor = agroSensor.Id;
                        if (idSensor > 0) SetSensorStateAgregat(agroSensor);
                        break;
                    case (int) (int) AlgorithmType.RANGEFINDER:
                        agroSensor = isAgregatCalibrated ? new SensorCalibrated((int)AlgorithmType.RANGEFINDER, idMobitel, calibrRecords) : new SensorCalibrated((int)AlgorithmType.RANGEFINDER, idMobitel);
                        idSensor = agroSensor.Id;
                        if (idSensor > 0) SetSensorStateAgregat(agroSensor);
                        break;
                    default:
                        agroSensor = GetAgroSensorByPriority(idMobitel, ref idSensor);
                        break;
                }
            }
            else
            {
                agroSensor = new Sensor(idSensor);
                agroSensor.SetAlgoritm();
                if (agroSensor.Algoritm == (int) AlgorithmType.AGREGAT_LOGIC)
                {
                    agroSensor = new LogicSensor((Sensor) agroSensor);
                    if (IsUseAgregatState)
                    {
                        ((LogicSensor) agroSensor).SetK(LogicState);
                        ((LogicSensor) agroSensor).BounceTimeSensorNoActive =
                            TimeSpan.FromSeconds(BounceSecondsSensorNoActive);
                    }
                }
                else if (agroSensor.Algoritm == (int) AlgorithmType.RANGEFINDER)
                {
                    agroSensor = isAgregatCalibrated ? new SensorCalibrated((Sensor)agroSensor, calibrRecords) : new SensorCalibrated((Sensor)agroSensor);
                    SetSensorStateAgregat(agroSensor);
                }
                else
                {
                    //((Sensor) agroSensor).BoundAgregatValue = daa.SensorAngleValue;
                    ((Sensor) agroSensor).SetSensorStateAgregat(Id);
                    SetSensorStateAgregat(agroSensor);
                }
            }
            if (isUseAgregatKbs && agroSensor is Sensor)
            {
                ((Sensor)agroSensor).K = CoefficientK;
                ((Sensor) agroSensor).B = ShiftB;
                ((Sensor)agroSensor).S = Signed;
            }
            return agroSensor;
        }

        private ExternalDevice GetAgroSensorByPriority(int idMobitel, ref int idSensor)
        {
            ExternalDevice agroSensor;
            agroSensor = new LogicSensor((int) AlgorithmType.AGREGAT_LOGIC, idMobitel);
            idSensor = agroSensor.Id;
            if (idSensor > 0)
            {
                if (IsUseAgregatState) ((LogicSensor) agroSensor).SetK(LogicState);
            }
            else
            {
                agroSensor = new SensorCalibrated((int) AlgorithmType.ANGLE, idMobitel);
                idSensor = agroSensor.Id;
                if (idSensor > 0)
                    SetSensorStateAgregat(agroSensor);
                else
                {
                    agroSensor = new SensorCalibrated((int) AlgorithmType.RANGEFINDER, idMobitel);
                    idSensor = agroSensor.Id;
                    if (idSensor > 0)
                        SetSensorStateAgregat(agroSensor);
                }
            }
            return agroSensor;
        }

        public bool SetSensorStateAgregat(ExternalDevice agroSensor)
        {
            if (IsUseAgregatState)
            {
                return ((Sensor) agroSensor).SetSensorStateAgregat(MinStateValue, MaxStateValue);
            }
            else
            {
                return ((Sensor) agroSensor).SetSensorStateAgregat(Id);
            }
        }

        protected override int WriteToXmlDictionary(ref XmlTextWriter writer)
        {
            int countWrited = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregatGroupXml);
                while (driverDb.Read())
                {
                    writer.WriteStartElement("group");
                    writer.WriteAttributeString("Id", driverDb.GetInt32("Id").ToString());
                    writer.WriteAttributeString("Id_work", TotUtilites.NdbNullReader(driverDb, "Id_work", 0).ToString());
                    writer.WriteAttributeString("Name", driverDb.GetString("GroupName"));
                    writer.WriteAttributeString("WorkName",
                        TotUtilites.NdbNullReader(driverDb, "WorkName", "").ToString());
                    writer.WriteEndElement();
                    writer.Flush();
                }
                driverDb.CloseDataReader();
                driverDb.GetDataReader(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatXml);
                while (driverDb.Read())
                {
                    writer.WriteStartElement("tool");
                    writer.WriteAttributeString("Id", driverDb.GetInt32("Id").ToString());
                    writer.WriteAttributeString("Name", driverDb.GetString("GroupName"));
                    writer.WriteAttributeString("Id_main", TotUtilites.NdbNullReader(driverDb, "Id_main", 0).ToString());
                    writer.WriteAttributeString("Id_work", TotUtilites.NdbNullReader(driverDb, "Id_work", 0).ToString());
                    writer.WriteAttributeString("WorkName",
                        TotUtilites.NdbNullReader(driverDb, "WorkName", "").ToString());
                    writer.WriteAttributeString("Width", TotUtilites.NdbNullReader(driverDb, "Width", 0).ToString());
                    writer.WriteAttributeString("Identifier",
                        TotUtilites.NdbNullReader(driverDb, "Identifier", 0).ToString());
                    writer.WriteEndElement();
                    countWrited += 1;
                    writer.Flush();
                }
                driverDb.CloseDataReader();
            }
            return countWrited;
        }


        public DataTable GetCalibrationDataTable()
        {
            string sql = string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatCalibration, Id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable table = driverDb.GetDataTable(sql);
                driverDb.CloseDbConnection();
                return table;
            }
        }

        public List<CalibrationRecord> GetCalibrationRecords()
            {
            var records = new List<CalibrationRecord>();
            using (var db = new DriverDb())
                {
                    db.ConnectDb();
                    db.GetDataReader(string.Format(AgroQuery.DictionaryAgroAgregat.SelectAgroAgregatCalibration, Id));
                    while (db.Read())
                    {
                        var record = new CalibrationRecord(db.GetInt64("Id"), db.GetDouble("UserValue"),
                                                                         db.GetDouble("SensorValue"), db.GetDouble("K"), db.GetDouble("b"));
                        records.Add(record);
                    }
                }
                return records;
            }
    }
}
