using System.Xml;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
   

    public abstract class DictionaryItem : IDisposable, IDictionary
    {
        #region �������
        public event StatusMessage ChangeStatusEvent;
        public event ProgressBarValue ChangeProgressBar;
        #endregion

        protected string Sql = "";

        private const short AlowAdd = -1;


        protected DictionaryItem()
        {

        }


        protected DictionaryItem( int id )
            : base()
        {
            Id = id;
        }


        protected virtual string DictionaryName
        {
            get { return Resources.Dictionary; }
        }

        public Boolean IsNew
        {
            get { return -1 == _id; }
        }

        #region ����
        private int _id = -1;
        /// <summary>
        /// �������������
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name = "";
        /// <summary>
        /// ��������
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if( Id == 0 )
                {
                    if(Id == 0 && !TestNameForExist( value ) )
                        _name = value;
                }
                else
                    _name = value;
            }
        }
        private string _comment = "";
        /// <summary>
        /// ����������
        /// </summary>
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        protected string _idOutLink;
        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _idOutLink; }
            set { _idOutLink = value; }
        }

        #endregion

        public virtual int AddItem()
        {
            if( Name.Length == 0 )
                return 0;
            else
                return AlowAdd;
        }

        public virtual bool DeleteItem( bool bQuestionNeed )
        {
            return false;
        }

        public virtual bool EditItem()
        {
            return false;
        }

        /// <summary>
        /// ������� ������ �������� ����������� � ������ ��������
        /// </summary>
        /// <returns></returns>
        public virtual bool TestLinks()
        {
            return true;

        }
        /// <summary>
        /// �������� �� �������� ����� �����������
        /// </summary>
        /// <returns></returns>
        public virtual bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            return false;

        }
        /// <summary>
        /// �������� ���������� ������� �� ������� �����������
        /// </summary>
        public virtual bool DeleteSelectedFromGrid( GridView gv )
        {
            if( gv == null || gv.SelectedRowsCount == 0 ) return false;
            if( DialogResult.No == XtraMessageBox.Show( Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 ) ) return false;
            var rows = new DataRow[gv.SelectedRowsCount];
            for( int i = 0; i < gv.SelectedRowsCount; i++ )
                rows[i] = gv.GetDataRow( gv.GetSelectedRows()[i] );
            foreach( DataRow row in rows )
            {
                Id = ( int )row["Id"];
                if( DeleteItem( false ) ) row.Delete();
            }
            return true;
        }

        /// <summary>
        /// ������ ���������� � ������� ����������� ������� �������
        /// </summary>
        /// <returns></returns>
        public virtual DataTable GetDocumentsList()
        {
            return null;
        }

        protected string GetDocumentListSql()
        {
            return AgroQuery.DictionaryItem.SelectFromAgroOrder;
        }

        public virtual void GetProperties(int id)
        {
 
        }


        #region IDisposable Members

        public void Dispose()
        {

        }
        #endregion

        #region Excel
        //----- import from excel ----------------------------------
        public void ExcelRead()
        {
            if( DialogResult.No == XtraMessageBox.Show( Resources.DataImportConfirm + " " + Environment.NewLine + DictionaryName.ToUpper() + Environment.NewLine + Resources.DataFormat + Environment.NewLine
                + ExcelFormatMessage + "?", Resources.ExcelImport, MessageBoxButtons.YesNo ) )
                return;
            string File = StaticMethods.ShowOpenFileDialog(Resources.ExcelImport + " " + DictionaryName.ToUpper(), "Excel|*.xlsx|Excel2003|*.xls");
            ChangeStatusEvent( Resources.ExcelConnect );
            using( Excel ex = new Excel() )
            {
                ChangeStatusEvent( Resources.DocumentOpen );
                if( ex.OpenDocument( File ) )
                {
                    int Cols = Math.Min( ex.ColumnMax(), ExcelColumnsDictionary );
                    int Rows = ( int )ex.RowMax();
                    string[] CellValues = new string[Cols];
                    ChangeProgressBar( Rows );
                    ChangeStatusEvent( Resources.ExcelImport );
                    for( int i = 1; i <= Rows; i++ )
                    {
                        for( int j = 0; j < Cols; j++ )
                        {
                            string range = ex.ParseColNum( j ) + i.ToString();
                            CellValues[j] = ex.GetValue( range );
                        }
                        Id = 0;
                        Name = "";
                        AddFromExcel( CellValues );
                        ChangeProgressBar( i );

                    }
                    ChangeProgressBar( Consts.PROGRESS_BAR_STOP );
                    ChangeStatusEvent( Resources.DocumentClose );
                    ex.CloseDocument();
                    ChangeStatusEvent( Resources.Ready );

                }
                else
                {
                    ChangeStatusEvent( Resources.DocumentOpenError + " " + File );
                }

            }
        }
        /// <summary>
        /// ������ �� Excel � ���������� ����� ������
        /// </summary>
        /// <param name="cellValues"></param>
        public virtual void AddFromExcel( params string[] cellValues )
        {
            Name = cellValues[0];
            Comment = cellValues[1];
            AddItem();
        }

        public virtual string ExcelFormatMessage
        {
            get
            {
                return "1 " + Resources.Column + " - " + Resources.NameRes.ToLower() + Environment.NewLine
                    + "2 " + Resources.Column + " - " + Resources.Remark.ToLower() + " ( " + Resources.Optional + ")";
            }
        }
        /// <summary>
        /// ����������� �� ���������� �������� ��� ������� �����������
        /// </summary>
        public virtual int ExcelColumnsDictionary
        {
            get { return 2; }
        }
        //---------------------- import from excel
        #endregion

        protected static List<T> GetEntityList<T>(string TableName, int idMain = 0, string sortOrder = "ORDER BY Name") where T : DictionaryItem, new()
        {
            var result = new List<T>();
            string sql = GetEntityListSql(TableName, idMain, sortOrder);
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(sql);
                while (db.Read())
                {
                    using (T agroEntity = new T())
                    {
                        agroEntity.GetProperties(db.GetInt32("Id"));
                        result.Add(agroEntity);
                    }
                }
                db.CloseDataReader();
            }
            return result;
        }

        protected static void SetComboBoxDataSource<T>(string tableName, RepositoryItemComboBox combo, int idMain = 0, string sortOrder = "ORDER BY Name", string andWhere = "") where T : DictionaryItem, new()
        {
            string sql = GetEntityListSql(tableName, idMain, sortOrder, andWhere);
            combo.Items.Clear();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(sql);
                while (db.Read())
                {
                    using (T agroEntity = new T())
                    {
                        agroEntity.GetProperties(db.GetInt32("Id"));
                        combo.Items.Add(agroEntity);
                    }
                }
                db.CloseDataReader();
            }
         }

        private static string GetEntityListSql(string tableName, int idMain = 0, string sortOrder = "ORDER BY Name",string andWhere = "")
        {
            if (idMain > 0)
                if (andWhere.Length==0)
                    return string.Format(@"SELECT   {0}.Id FROM {0} Where {0}.Id_main = {1} {2}", tableName, idMain, sortOrder);
                else
                    return string.Format(@"SELECT   {0}.Id FROM {0} Where {0}.Id_main = {1} AND {2} {3}", tableName, idMain, andWhere, sortOrder);
            if (andWhere.Length == 0)
                return string.Format(@"SELECT   {0}.Id FROM {0} {1} ", tableName, sortOrder);
            return string.Format(@"SELECT   {0}.Id FROM {0} WHERE {1} {2} ", tableName,andWhere, sortOrder);
        }

        public virtual int GetIdFromName(string tableName, string fieldName, string nameValue)
        {
            Sql = string.Format(@"SELECT   {0}.Id FROM {0} Where {0}.{1} = '{2}'", tableName, fieldName, nameValue);
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(Sql);
                if (db.Read())
                {
                    Id = db.GetInt32("Id");
                }
                db.CloseDataReader();
            }
            return Id;
        }

        public void WriteToXml()
        {
            string fileName = string.Format("{0}_{1}.xml",DictionaryName, StaticMethods.SuffixDateToFileName());
            fileName = StaticMethods.ShowSaveFileDialog(fileName, Resources.XMLSave, "XML |*.xml");
            if (fileName.Length == 0) return ;
            var writer = new XmlTextWriter(fileName, System.Text.Encoding.UTF8) {Formatting = Formatting.Indented};
            writer.WriteStartDocument(false);
            writer.WriteStartElement("Dictionary");
            int writed = WriteToXmlDictionary(ref writer);
            writer.WriteEndDocument();
            writer.Close();
            XtraMessageBox.Show(string.Format("{0} {1}",Resources.TotalRecord,writed));
        }

        protected virtual int WriteToXmlDictionary(ref XmlTextWriter writer)
        {
            return 0;
        }

    }
}
