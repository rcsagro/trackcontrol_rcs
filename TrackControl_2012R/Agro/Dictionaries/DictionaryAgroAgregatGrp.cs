using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    /// <summary>
    /// ������ ��������� ������������
    /// </summary>
    public class DictionaryAgroAgregatGrp : DictionaryItem
    {

        #region ����

        /// <summary>
        /// ��� ����� �� ���������
        /// </summary>
        private int _Id_work;

        public int Id_work
        {
            get { return _Id_work; }
            set { _Id_work = value; }
        }

        #endregion

        public DictionaryAgroAgregatGrp()
        {
        }

        public DictionaryAgroAgregatGrp(int id):base(id)
        {
            GetProperties(id);
        }

        public override sealed void GetProperties(int id)
        {
            if (id > 0)
            {
                Sql = AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregat + id;
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(Sql);
                    if (driverDb.Read())
                    {
                        Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                        Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                        _Id_work = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_work", 0));
                        _idOutLink = TotUtilites.NdbNullReader(driverDb, "OutLinkId", "").ToString();
                    }
                    driverDb.CloseDataReader();
                }
            }

        }

        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0))
                return 0;

            Sql = AgroQuery.DictionaryAgroAgregatGrp.InsertIntoAgroAgregat;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_agregat");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", _Id_work);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "OutLinkId", _idOutLink ?? "");
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroAgregatGrp.UpdateAgroAgregat, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            return false;
                    }
                    Sql = string.Format(AgroQuery.DictionaryAgroAgregatGrp.DeleteAgroAgregatItem, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregatCount, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);

                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregatId, Id);
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(Resources.AgregatGroupeExist + " " + sNameForTest + " !", Resources.ApplicationName); 
                return true;
            }
            else
                return false;

        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregatList;
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetListForJournal()
        {
            DataTable table = null;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroAgregatGrp.SelectAgroAgregatJornal;
                table = driverDb.GetDataTable(sql);
                table.Columns[0].ReadOnly = false;
                return table;
            }
        }
    }
}
