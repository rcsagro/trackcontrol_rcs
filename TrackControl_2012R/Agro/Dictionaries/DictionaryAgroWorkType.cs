using System.Xml;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;  
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
namespace Agro.Dictionaries
{
    /// <summary>
    /// ���� �����
    /// </summary>
    public class DictionaryAgroWorkType:DictionaryItem 
    {
        readonly static string _tableName = "agro_work";
        /// <summary>
        /// ������ ������ ��������
        /// </summary>
        double _speedBottom;
        public double SpeedBottom
        {
            get { return _speedBottom; }
            set { _speedBottom = value; }
        }
        /// <summary>
        /// ������� ������ ��������
        /// </summary>
        double _speedTop;
        public double SpeedTop
        {
            get { return _speedTop; }
            set { _speedTop = value; }
        }

        /// <summary>
        /// ���� ����� - ������ � ����, ������� ��� ����,������� �� ����
        /// </summary>
        private int _typeWork;
        public int TypeWork
        {
            get { return _typeWork; }
            set { _typeWork = value; }
        }

        /// <summary>
        /// ��� ������ �����, ���� ������ ������
        /// </summary>
        private int? _Id_main;
        public int? Id_main
        {
            get { return _Id_main; }
            set { _Id_main = value; }
        }

        public Color TrackColor { get; set; }

        protected override string DictionaryName
        {
            get { return Resources.WorkTypes; }
        }

        public DictionaryAgroWorkType()
        {
        }
        public DictionaryAgroWorkType(int ID)
            : base(ID)
        {
            GetProperties(ID);
        }

        public override sealed void GetProperties(int ID)
        {
            Sql = string.Format("SELECT   agro_work.* FROM agro_work WHERE   agro_work.Id = {0}", ID);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Id = ID;
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    _speedBottom = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "SpeedBottom", 0));
                    _speedTop = Convert.ToDouble(TotUtilites.NdbNullReader(driverDb, "SpeedTop", 0));
                    _typeWork = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "TypeWork", 0));
                    _Id_main = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_main", 0));
                    _idOutLink = TotUtilites.NdbNullReader(driverDb, "OutLinkId", "").ToString();
                    TrackColor =
                        Color.FromArgb(driverDb.GetInt32("TrackColor") == 0
                                           ? Color.MidnightBlue.ToArgb()
                                           : driverDb.GetInt32("TrackColor"));
                }
                driverDb.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.DictionaryAgroWorkType.InsertIntoAgroWork, driverDb.ParamPrefics);
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_work");
            }
            return Id;
        }


        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(8);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", Comment );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "SpeedBottom", _speedBottom );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "SpeedTop", _speedTop );
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TypeWork", _typeWork );
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "OutLinkId", _idOutLink);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TrackColor", TrackColor.ToArgb());

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", _Id_main == 0 ? null : _Id_main);
            }
            else if(DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if(_Id_main == 0)
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_main", DBNull.Value );
                else
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Id_main", _Id_main );
            }
        }

        public override bool EditItem()
        {
            try
            {
                using (var driverDb = new DriverDb())
                {
                    Sql = string.Format(AgroQuery.DictionaryAgroWorkType.UpdateAgroWork, Id, driverDb.ParamPrefics);
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroWorkType.DeleteAgroWork, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroWorkType.SelectAgroPriceT, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);

                Sql = string.Format(AgroQuery.DictionaryAgroWorkType.SelectAgroOrderT, Id);
                links = links + driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }

            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroWorkType.SelectFromAgroWork ,Id);
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(string.Format("{0} {1}!", Resources.WorkTypeExist, sNameForTest), Resources.ApplicationName);
                return true;
            }
            else
                return false;

        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = AgroQuery.DictionaryAgroWorkType.SelectAgroWorkList;
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroWorkType.SelectAgroWorkJornal;
               
                return driverDb.GetDataTable(sql);
            }
        }

        public override DataTable GetDocumentsList()
        {
            string sql = string.Format( GetDocumentListSql(), AgroQuery.SqlVehicleIdent, Id, ConstsGen.RECORD_MISSING, "Id_work" );
            
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetWorkTypesList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroWorkType.SelectAgroWorkTypes;
                return driverDb.GetDataTable(sql);
            }
        }

        public static int GetFirstWorkWithType(WorkTypes workType)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.DictionaryAgroWorkType.SelectAgroWorkTypeWork, ( int )workType );
                return driverDb.GetScalarValueIntNull(sql);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

        public static List<DictionaryAgroWorkType> GetEntityList()
        {
            return GetEntityList<DictionaryAgroWorkType>(_tableName);
        }

        public static void SetComboBoxDataSource(RepositoryItemComboBox combo, int idMain = 0)
        {
            SetComboBoxDataSource<DictionaryAgroWorkType>(_tableName, combo, idMain);
        }

        public enum WorkTypes
        {
            WorkInField = 1,
            MoveOutField,
            MoveInField
        }
        protected override int WriteToXmlDictionary(ref XmlTextWriter writer)
        {
            int countWrited = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(AgroQuery.DictionaryAgroWorkType.SelectAgroWorkXml);
                int idGroup = 0;
                while (driverDb.Read())
                {
                    int idGroupReader = driverDb.GetInt32("GroupId");
                    if (idGroupReader != idGroup)
                    {
                        writer.WriteStartElement("group");
                        writer.WriteAttributeString("Id", idGroupReader.ToString());
                        writer.WriteAttributeString("Name", driverDb.GetString("GroupWork"));
                        writer.WriteEndElement();
                        idGroup = idGroupReader;
                    }
                    writer.WriteStartElement("work");
                    writer.WriteAttributeString("Id", driverDb.GetInt32("Id").ToString());
                    writer.WriteAttributeString("GroupId", idGroup.ToString());
                    writer.WriteAttributeString("Name", driverDb.GetString("Name"));
                    writer.WriteAttributeString("SpeedBottom",TotUtilites.NdbNullReader(driverDb, "SpeedBottom", 0).ToString().Replace(",","."));
                    writer.WriteAttributeString("SpeedTop", TotUtilites.NdbNullReader(driverDb, "SpeedTop", 0).ToString().Replace(",", "."));
                    writer.WriteEndElement();
                    countWrited += 1;
                    writer.Flush();
                }
                driverDb.CloseDataReader();
            }
            return countWrited;
        }
    }
}
