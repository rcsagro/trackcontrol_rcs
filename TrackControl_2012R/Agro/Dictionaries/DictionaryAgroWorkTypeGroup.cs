﻿using System;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    public class DictionaryAgroWorkTypeGroup: DictionaryItem 
    {
        public DictionaryAgroWorkTypeGroup()
        {
        }

        public bool IsSpeedControl { get; set; }
        public bool IsDepthControl { get; set; }
        public DictionaryAgroWorkTypeGroup(int id):base(id)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroWorkTypeGroup.SelectAgroWorkGroup, id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    IsSpeedControl = driverDb.GetBit("IsSpeedControl");
                    IsDepthControl = driverDb.GetBit("IsDepthControl");
                }
                driverDb.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            if (base.AddItem()==0 || (Id != 0)) return 0;
            Sql = AgroQuery.DictionaryAgroWorkTypeGroup.InsertIntoAgroWorkGroup;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_workgroup");
                return Id;
            }
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsSpeedControl", IsSpeedControl);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsDepthControl", IsDepthControl);
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format(AgroQuery.DictionaryAgroWorkTypeGroup.UpdateAgroWorkGroup, Id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroWorkTypeGroup.DeleteAgroWorkGroup, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                        Id = 0;
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format( AgroQuery.DictionaryAgroWorkTypeGroup.SelectFromAgroWorkGroup, Id );
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            Sql = string.Format(AgroQuery.DictionaryAgroWorkTypeGroup.SelectAgroWorkGroupId, Id );
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }

            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(Resources.AgregatGroupeExist + " " + sNameForTest + " !", Resources.ApplicationName);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroWorkTypeGroup.SelectAgroWorkGroupList;
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroWorkTypeGroup.SelectAgroWorkGroupJornal;
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
