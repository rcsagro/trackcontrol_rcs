using System;
using System.Data; 
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Agro.Utilites;
using DevExpress.XtraEditors;
using Agro.Properties;
using DevExpress.XtraReports.Design;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    /// <summary>
    /// ������������ ��������
    /// </summary>
    public class DictionaryAgroCulture:DictionaryItem 
    {
        /// <summary>
        /// ������ ��������
        /// </summary>
        private byte[] _Icon;
        public byte[] Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        public DictionaryAgroCulture()
        {

        }
        public DictionaryAgroCulture(int ID):base(ID)
        {
                Sql = "SELECT   agro_culture.* FROM agro_culture WHERE   agro_culture.Id = " + ID;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    //_Icon = (byte[])dr.GetValue("Icon");
                    if (driverDb["Icon"] != DBNull.Value)
                        _Icon = (byte[])driverDb["Icon"];
                    //dr.GetBytes(1, 0, _Icon, 0, bufferSize);
                }
                driverDb.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id > 0)) return 0;
            Sql = AgroQuery.DictionaryAgroCulture.InsertIntoAgroCulture;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_culture");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(3);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", Comment );

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Icon", _Icon );
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if( _Icon == null )
                {
                    byte[] byteArray = new byte[0];
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Icon", byteArray );
                }
                else
                {
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Icon", _Icon );
                }
            }
        }
        public override bool EditItem()
        {
            try
            {
                Sql = string.Format( AgroQuery.DictionaryAgroCulture.UpdateAgroCulture, Id );
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                        Sql = string.Format(AgroQuery.DictionaryAgroCulture.DeleteFromAgroCulture, Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }
        public override bool TestLinks()
        {
            Sql = string.Format( AgroQuery.DictionaryAgroCulture.SelectFromAgroField, Id );
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
                Sql = string.Format(AgroQuery.DictionaryAgroCulture.SelectFromAgroSeason, Id);
                links += driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }
        public override bool TestNameForExist(string sNameForTest,bool bViewInfo = true)
        {
            Sql =  string.Format( AgroQuery.DictionaryAgroCulture.SelectAgroCulture, Id);
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);

                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(Resources.CultureExist + " " + sNameForTest + "!", Resources.ApplicationName);
                return true;
            }
            else
                return false;

        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroCulture.SelectAgroCultureJornal;
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroCulture.SelectAgroCultureList;
                return driverDb.GetDataTable(sql);
            }
        }
    }
}
