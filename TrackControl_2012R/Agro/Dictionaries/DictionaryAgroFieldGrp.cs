using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    /// <summary>
    /// ������ �����
    /// </summary>
    public class DictionaryAgroFieldGrp:DictionaryItem 
    {
        readonly static string _tableName = "agro_fieldgroupe";
        private int _idZonesgroup;

        public int IdZonesgroup
        {
            get { return _idZonesgroup; }
            set { _idZonesgroup = value; }
        }

        public DictionaryAgroFieldGrp()
        {
        }

        public DictionaryAgroFieldGrp(int id)
            : base(id)
        {
            GetProperties(id);
        }

        public override sealed void GetProperties(int id)
        {
            if (id > 0)
            {
            Sql = string.Format(
                "SELECT   agro_fieldgroupe.* FROM agro_fieldgroupe WHERE   agro_fieldgroupe.Id = {0}", id);
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(Sql);
                    if (driverDb.Read())
                    {
                        Id = id;
                        Name = TotUtilites.NdbNullReader(driverDb, "Name", "").ToString();
                        Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                        _idZonesgroup = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_Zonesgroup", 0));

                        if (_idZonesgroup == 0)
                        {
                            LinkToZoneGroupe();
                        }
                        driverDb.CloseDataReader();
                    }
                }
            }

    }

        private void LinkToZoneGroupe()
        {
            _idZonesgroup = GetGroupeFromFieldsZones();
            if (_idZonesgroup > 0)
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    Sql = string.Format("UPDATE agro_fieldgroupe SET Id_Zonesgroup = {1} WHERE   agro_fieldgroupe.Id = {0}", Id, _idZonesgroup);
                    driverDb.ExecuteNonQueryCommand(Sql);
                }
            }
        }
        /// <summary>
        /// ��� ������ ��� ����������� �� ����� ��� ����� (� ��������� ��������� ������ ����� �� ����� ���� ������ ���)  
        /// </summary>
        /// <returns></returns>
        private int GetGroupeFromFieldsZones()
        {
            int zonesGroupId = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.DictionaryAgroFieldGrp.SelectZones, Id);

                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    zonesGroupId = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "ZonesGroupId", 0));
                }
                driverDb.CloseDataReader();
            }
            return zonesGroupId;
        }

        public override int AddItem()
        {
            if (base.AddItem()==0 || (Id != 0)) return 0;

            Sql = AgroQuery.DictionaryAgroFieldGrp.SelectAgroFieldGroup;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_fieldgroupe");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(3);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_Zonesgroup", _idZonesgroup);
        }

        public override bool EditItem()
        {
            try
            {
                Sql = string.Format( AgroQuery.DictionaryAgroFieldGrp.UpdateAgroFieldGroup, Id );
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                    Sql = string.Format( AgroQuery.DictionaryAgroFieldGrp.DeleteAgroFieldGroup, Id );
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format( AgroQuery.DictionaryAgroFieldGrp.SelectCountAgroField, Id );
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            bool isTested = false;
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                Sql = string.Format(AgroQuery.DictionaryAgroFieldGrp.SelectAgroFromFieldGroup, Id);
                db.NewSqlParameterArray(1);
                db.SetNewSqlParameter(db.ParamPrefics + "Name", sNameForTest);
                int idName = db.GetScalarValueIntNull(Sql, db.GetSqlParameterArray);
                if (idName > 0)
                {
                    if (bViewInfo)
                        XtraMessageBox.Show(Resources.FieldGroupeExist + " " + sNameForTest + "!", Resources.ApplicationName);
                    isTested = true;
                }
            }
            return isTested;

        }

        public int CreateFromCheckZoneGrp(string sZoneGrp, int iZoneGrpId)
        {
            if (Id != 0) return 0;
                Name = sZoneGrp;
                Comment = Resources.AutocreationFromZone;
                _idZonesgroup = iZoneGrpId;
                Id = AddItem();
                if (Id > 0)
                {
                    Sql = string.Format(AgroQuery.DictionaryAgroFieldGrp.SelectFromZones, iZoneGrpId);
                    AddFieldsBasedOnZones();
                    return Id;
                }
                else
                    return 0;
        }

        private int AddFieldsBasedOnZones()
        {
            int cnt = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                while (driverDb.Read())
                {
                    using (var daf = new DictionaryAgroField(0))
                    {
                        daf.Name = driverDb.GetString("Name");
                        daf.IdMain = Id;
                        daf.IdZone = driverDb.GetInt32("Zone_ID");
                        daf.Comment = Resources.AutocreationFromZone;
                        daf.AddItem();
                    }
                    cnt++;
                }
                driverDb.CloseDataReader();
            }
            return cnt;
        }

        public void SynhroWithCheckZoneGrp(int iZoneGrpId,out int countAdd,out int countDelete)
        {
            countAdd = 0;
            countDelete = 0;
            if (Id == 0) return ;
            Sql = string.Format(AgroQuery.DictionaryAgroFieldGrp.SelectFromZonesJoin, iZoneGrpId);
            countAdd =  AddFieldsBasedOnZones();
            countDelete = DeleteFieldsWithoutZones();
        }

        public int DeleteFieldsWithoutZones()
        {
            if (Id == 0) return 0;
            int cnt = 0;
            Sql = string.Format(AgroQuery.DictionaryAgroFieldGrp.SelectFromAgroFieldJoin, Id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                while (driverDb.Read())
                {
                    using (var daf = new DictionaryAgroField(driverDb.GetInt32("Id")))
                    {
                        if (daf.DeleteItem(false)) cnt++;
                    }
                }
                driverDb.CloseDataReader();
            }
            return cnt;
        }

        public string CommentForCreateFromCheckZoneGrp()
        {
            return Resources.AutocreationFromZone;
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroFieldGrp.SelectFromAgroFieldGroupe;
                return driverDb.GetDataTable(sql);
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = AgroQuery.DictionaryAgroFieldGrp.SelectAgroFieldGroupeList;
                return driverDb.GetDataTable(sql);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

        public static List<DictionaryAgroFieldGrp> GetEntityList()
        {
            return GetEntityList<DictionaryAgroFieldGrp>(_tableName);
        }

        public static void SetComboBoxDataSource(RepositoryItemComboBox combo, int idMain = 0)
        {
            SetComboBoxDataSource<DictionaryAgroFieldGrp>(_tableName, combo, idMain);
        }
    }
}
