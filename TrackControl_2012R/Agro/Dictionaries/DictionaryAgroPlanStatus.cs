﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro.Dictionaries
{
    public class DictionaryAgroPlanStatus : DictionaryItem, IDisposable  
    {

        public DictionaryAgroPlanStatus()
        {

        }
        public DictionaryAgroPlanStatus(int id)
            : base(id)
        {
            Id = id;
            Init (id);
        }


        #region Члены IDictionary


        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(
                    "INSERT INTO agro_plan_state(StatusName,StatusComment) VALUES ({0}Name,{0}Comment)",
                    driverDb.ParamPrefics);
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "agro_plan_state");
            }
            return Id;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", (Name.Length > 50) ? Name.Substring(0, 50) : Name);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                    Sql = string.Format("DELETE FROM agro_plan_state WHERE ID = {0}", Id);
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(Sql);
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool EditItem()
        {
            try
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    Sql =
                        string.Format(
                            "UPDATE agro_plan_state SET StatusName = {1}Name ,StatusComment = {1}Comment WHERE (ID = {0})",
                            Id, driverDb.ParamPrefics);
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(Sql, driverDb.GetSqlParameterArray);
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format("SELECT   COUNT(agro_plan.Id) FROM   agro_plan WHERE   agro_plan.IdStatus  = {0} ",Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            int idName = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql =
                    string.Format(@"SELECT   agro_plan_state.Id FROM agro_plan_state WHERE   agro_plan_state.StatusName = {1}Name
              AND agro_plan_state.Id <> {0}", Id, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Name", sNameForTest);
                idName = driverDb.GetScalarValueIntNull(Sql, driverDb.GetSqlParameterArray);
            }
            if (idName > 0)
            {
                if (bViewInfo) XtraMessageBox.Show(string.Format("{0} {1}!", Resources.WorkTypeExist, sNameForTest), Resources.ApplicationName);
                return true;
            }
            else
                return false;
        }

        #endregion

        private void Init(int id)
        {
            Sql = string.Format("SELECT   agro_plan_state.* FROM agro_plan_state WHERE   agro_plan_state.Id = {0}", id);
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    Name = TotUtilites.NdbNullReader(driverDb, "StatusName", "").ToString();
                    Comment = TotUtilites.NdbNullReader(driverDb, "StatusComment", "").ToString();
                }
                driverDb.CloseDataReader();
            }
        }

        public static DataTable GetList()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();    
                const string sql = " SELECT   agro_plan_state.Id, agro_plan_state.StatusName FROM   agro_plan_state ORDER BY   agro_plan_state.StatusName";
                return driverDb.GetDataTable(sql);
            }
        }


    }
}
