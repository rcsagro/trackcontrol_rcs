﻿using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;

namespace Agro.Dictionaries
{
    public class DictionaryAgroSeason : DictionaryItem 
    {

        
        #region Поля
        readonly static string _tableName = "agro_season";
        public DateTime DateStart { get; set;}
        public DateTime DateEnd { get; set; }
        public int IdCulture { get; set; }
        public bool IsClosed { get; set; }
        DictionaryAgroCulture Culture;
        //public DateTime DateInsert { get; set; }
        //public string UserInsert { get; set; }
        public decimal SquarePlan { get; set; }
        #endregion

        private const int ExcelFields = 8;

        public DictionaryAgroSeason()
        {

        }

        public DictionaryAgroSeason(int id)
            : base(id)
        {
            GetProperties(id);
        }

        public override sealed void GetProperties(int id)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(string.Format(AgroQuery.DictionaryAgroSeason.SelectAgroSeason, id));
                if (driverDb.Read())
                {
                    Id = id;
                    DateStart = Convert.ToDateTime(TotUtilites.NdbNullReader(driverDb, "DateStart", 0));
                    DateEnd = Convert.ToDateTime(TotUtilites.NdbNullReader(driverDb, "DateEnd", 0));
                    IdCulture = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Id_culture", 0));
                    SquarePlan = Convert.ToDecimal(TotUtilites.NdbNullReader(driverDb, "SquarePlan", 0));
                    Comment = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                    IsClosed = Convert.ToBoolean(TotUtilites.NdbNullReader(driverDb, "IsClosed", 0));
                    Culture = new DictionaryAgroCulture(IdCulture);
                    Culture.Dispose();
                    Name = GetName();
                }
                else
                {
                    DateStart = DateTime.Today;
                    DateEnd = DateTime.Today;
                }
                driverDb.CloseDataReader();
            }
        }

        string GetName()
        {
            string name = string.Format("{0} {1}", Culture.Name.TrimEnd(), DateStart.Year);
            if (DateStart.Year < DateEnd.Year) name = string.Format("{0}/{1}", name, DateEnd.Year);
            return name;
        }

        public override int AddItem()
        {
            if (Id != 0)  return Id;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                CreateParameters(driverDb);
                Id = driverDb.ExecuteReturnLastInsert(AgroQuery.DictionaryAgroSeason.InsertIntoAgroSeason,
                                                      driverDb.GetSqlParameterArray, "agro_season");
                return Id;
            }
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(6);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", DateStart);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", DateEnd);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_culture", IdCulture);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquarePlan", SquarePlan);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", Comment);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsClosed", IsClosed);
        }

        public override bool EditItem()
        {
            try
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    CreateParameters(driverDb);
                    driverDb.ExecuteNonQueryCommand(string.Format(AgroQuery.DictionaryAgroSeason.UpdateAgroSeason, Id),
                                                    driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        driverDb.ExecuteNonQueryCommand(string.Format(AgroQuery.DictionaryAgroSeason.DeleteAgroSeason,
                                                                      Id));
                    }
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        public static DataTable GetListForJournal()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                return driverDb.GetDataTable(AgroQuery.DictionaryAgroSeason.SelectAgroSeasonJornal);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }


        public static List<DictionaryAgroSeason> GetEntityList()
        {
            return GetEntityList<DictionaryAgroSeason>(_tableName);
        }

        public static void SetComboBoxDataSource(RepositoryItemComboBox combo, int idMain = 0)
        {
            SetComboBoxDataSource<DictionaryAgroSeason>(_tableName, combo, idMain, "ORDER BY Id","IsClosed = 0");
        }

        protected override string DictionaryName
        {
            get { return Resources.Seasons; }
        }

        public override string ExcelFormatMessage
        {
            get
            {
                return (ExcelCols.FieldId + 1) + " " + Resources.Column + " - " + Resources.Code.ToLower() + Environment.NewLine
                + (ExcelCols.FieldName + 1) + " " + Resources.Column + " - " + Resources.fieldName.ToLower() + Environment.NewLine
                + (ExcelCols.CulturaName + 1) + " " + Resources.Column + " - " + Resources.Cultura.ToLower() + Environment.NewLine
                + (ExcelCols.DateStart + 1) + " " + Resources.Column + " - " + Resources.DateStart.ToLower() + Environment.NewLine
                + (ExcelCols.DateEnd + 1) + " " + Resources.Column + " - " + Resources.DateEnd.ToLower();
            }
        }

        public override int ExcelColumnsDictionary
        {
            get { return ExcelFields; }
        }

        public override void AddFromExcel(params string[] cellValues)
        {
            try
            {
                if (cellValues.GetUpperBound(0) == (ExcelFields - 1))
                {
                    DateTime dateStart;
                    DateTime dateEnd;
                    int idField;
                    Comment = "";
                    SquarePlan = 0;
                    if (DateTime.TryParse(cellValues[ExcelCols.DateStart], out dateStart) 
                        && DateTime.TryParse(cellValues[ExcelCols.DateEnd], out dateEnd)
                        && Int32.TryParse(cellValues[ExcelCols.FieldId], out idField)
                        && cellValues[ExcelCols.CulturaName].ToString().Length >0
                        && cellValues[ExcelCols.FieldName].ToString().Length > 0)
                    {
                        string cultureName = cellValues[ExcelCols.CulturaName];
                        DateStart = dateStart;
                        DateEnd = dateEnd;
                        if (idField > 0)
                        {
                            var daf = new DictionaryAgroField(idField);
                            if (daf.Id == idField)
                            {
                                var dac = new DictionaryAgroCulture();
                                IdCulture = dac.GetIdFromName("agro_culture", "Name", cultureName);
                                if (IdCulture <= 0)
                                {
                                    dac.Name = cultureName;
                                    IdCulture = dac.AddItem();
                                }
                                Id = GetIdFromCultureDates(DateStart, DateEnd, IdCulture);
                                if (Id <= 0) AddItem();
                                var entity = new FieldsTcEntity
                                {
                                    Date = DateTime.Now,
                                    DateStart = DateStart,
                                    DateEnd = DateEnd,
                                    Season = this,
                                    Field = daf,
                                    UserCreated = UserBaseCurrent.Instance.Name
                                };

                                using (var fc = new FieldsTcController(entity))
                                {
                                    if (!fc.IsFieldsTcExistForSeasonField(false)) fc.AddDoc();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        public int GetIdFromCultureDates(DateTime dateStart,DateTime dateEnd,int idCulture)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter("DateStart", DateStart);
                driverDb.SetNewSqlParameter("DateEnd", DateEnd);
                driverDb.SetNewSqlParameter("Id_culture", IdCulture);
                Sql = string.Format(AgroQuery.DictionaryAgroSeason.SelectIdFromCultureDates, driverDb.ParamPrefics);
                return driverDb.GetScalarValueNull<Int32>(Sql,driverDb.GetSqlParameterArray, 0);
            }
        }

        public override bool TestLinks()
        {
            Sql = string.Format(AgroQuery.DictionaryAgroSeason.SelectSeasonsTK, Id);
            int links = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                links = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (links == 0)
                return true;
            else
            {
                XtraMessageBox.Show(Resources.DeleteBun, Resources.ApplicationName);
                return false;
            }
        }
    }

    static class ExcelCols
    {
        public const int FieldId = 1;
        public const int FieldName = 2;
        public const int CulturaName = 3;
        public const int DateStart = 6;
        public const int DateEnd = 7;
    }
}
