﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using LocalCache;
using BaseReports.Procedure;
using TrackControl.Vehicles;  
namespace Agro
{
    public partial class OrderItem 
    {
        DateTime _dateDoc;
        int _Team_id;
        /// <summary>
        /// группа транспортных средств
        /// </summary>
        public int Team_id
        {
            get { return _Team_id; }
            set { _Team_id = value; }
        }
        /// <summary>
        /// телетрек
        /// </summary>
        int _Mobitel_Id;
        public int Mobitel_Id
        {
            get { return _Mobitel_Id; }
            set { _Mobitel_Id = value; }
        }
        /// <summary>
        /// транспортное средство
        /// </summary>
        int _Vehicle_Id;
        public int Vehicle_Id
        {
            get { return _Vehicle_Id; }
            set { _Vehicle_Id = value; }
        }
        /// <summary>
        /// название транспортного средства
        /// </summary>
        string _MobitelName;
        public string MobitelName
        {
            get { return _MobitelName; }
            set { _MobitelName = value; }
        }
        /// <summary>
        /// Место начала движения
        /// </summary>
        string _LocationStart;
        public string LocationStart
        {
            get { return _LocationStart; }
            set { _LocationStart = value; }
        }
        /// <summary>
        /// Место окончания движения
        /// </summary>
        string _LocationEnd;
        public string LocationEnd
        {
            get { return _LocationEnd; }
            set { _LocationEnd = value; }
        }
        /// <summary>
        /// Средняя скорость, км/ч
        /// </summary>
        double _SpeedAvg;
        public double SpeedAvg
        {
            get { return _SpeedAvg; }
            set { _SpeedAvg = value; }
        }
        /// <summary>
        /// Пройденный путь, км
        /// </summary>
        double _Distance;
        public double Distance
        {
            get { return _Distance; }
            set { _Distance = value; }
        }
        /// <summary>
        /// Время начала движения
        /// </summary>
        DateTime _TimeStart;
        public DateTime TimeStart
        {
            get { return _TimeStart; }
            set { _TimeStart = value; }
        }
        /// <summary>
        /// Время окончания  движения
        /// </summary>
        DateTime _TimeEnd;
        public DateTime TimeEnd
        {
            get { return _TimeEnd; }
            set { _TimeEnd = value; }
        }
        /// <summary>
        /// Продолжительность смены, ч
        /// </summary>
        string _TimeWork;
        public string TimeWork
        {
            get { return _TimeWork; }
            set { _TimeWork = value; }
        }
        /// <summary>
        /// Общее время движения, ч
        /// </summary>
        string _TimeMove;
        public string TimeMove
        {
            get { return _TimeMove; }
            set { _TimeMove = value; }
        }
        /// <summary>
        /// Общее время стоянок, ч
        /// </summary>
        string _TimeStop;
        public string TimeStop
        {
            get
            {
                return _TimeStop;
            }
            set { _TimeStop = value; }
        }
        /// <summary>
        /// информационная строка об обработанных площадях
        /// </summary>
        /// 
        string _SquareWorkDescript;
        public string SquareWorkDescript
        {
            get
            {
                return _SquareWorkDescript;
            }
            set
            {
                _SquareWorkDescript = value;
            }
        }
        /// <summary>
        /// информационная строка об обработанных площадях с учетом перекрытий всех записей наряда
        /// </summary>
        /// 
        string _SquareWorkDescriptOverlap;
        public string SquareWorkDescriptOverlap
        {
            get
            {
                return _SquareWorkDescriptOverlap;
            }
            set
            {
                _SquareWorkDescriptOverlap = value;
            }
        }
        /// <summary>
        /// сумарная обработанная вычесленная площадь с учетом перекрытий всех записей наряда
        /// </summary>
        double _factSquareCalcOverlap;
        public double FactSquareCalcOverlap
        {
            get { return _factSquareCalcOverlap; }
            set { _factSquareCalcOverlap = value; }
        }
        /// <summary>
        /// пробег вне полей
        /// </summary>
        private double _PathWithoutWork;
        public double PathWithoutWork
        {
            get
            {
                return _PathWithoutWork;
            }
            set
            {
                _PathWithoutWork = value;
            }
        }
        /// <summary>
        /// дата последнего пересчета наряда
        /// </summary>
        DateTime _dateLastRecalc;

        public DateTime DateLastRecalc
        {
            get { return _dateLastRecalc; }
            set { _dateLastRecalc = value; }
        }

        /// <summary>
        /// создатель наряда
        /// </summary>
        string _userCreated;

        public string UserCreated
        {
            get { return _userCreated; }
            set { _userCreated = value; }
        }

        /// <summary>
        /// последний пересчитавший наряд
        /// </summary>
        string _userRecalced;

        public string UserRecalced
        {
            get { return _userRecalced; }
            set { _userRecalced = value; }
        }


        public string InforCreate
        {
            get { return string.Format("{0} {1}", _dateDoc, _userCreated); }
        }

        public string InforRecalc
        {
            get { return string.Format("{0} {1}", _dateLastRecalc, _userRecalced); }
        }

        int _stateOrder;

        public int StateOrder
        {
            get { return _stateOrder; }
            set { _stateOrder = value; }
        }

        /// <summary>
        /// время последней считанной записи GPS данных. Используетсмя для опредления статуса наряда
        /// </summary>
        DateTime _lastGpsTime;

        #region топливо DUT
        /// <summary>
        /// общий расход топлива на перездах
        /// </summary>
        double _FuelDUTExpensWithoutWork;
        public double FuelDUTExpensWithoutWork
        {
            get { return _FuelDUTExpensWithoutWork; }
            set { _FuelDUTExpensWithoutWork = value; }
        }
        /// <summary>
        /// средний расход топлива на перездах л/100 км
        /// </summary>
        double _FuelDUTExpensAvgWithoutWork;
        public double FuelDUTExpensAvgWithoutWork
        {
            get { return _FuelDUTExpensAvgWithoutWork; }
            set { _FuelDUTExpensAvgWithoutWork = value; }
        }
        /// <summary>
        /// общий расход топлива в полях
        /// </summary>
        double _FuelDUTExpensSquare;
        public double FuelDUTExpensSquare
        {
            get { return _FuelDUTExpensSquare; }
            set { _FuelDUTExpensSquare = value; }
        }
        /// <summary>
        /// средний расход топлива в полях л/га
        /// </summary>
        double _FuelDUTExpensAvgSquare;
        public double FuelDUTExpensAvgSquare
        {
            get { return _FuelDUTExpensAvgSquare; }
            set { _FuelDUTExpensAvgSquare = value; }
        }

        /// <summary>
        /// топлива на начало наряда
        /// </summary>
        public double FuelDUTStart { get; set;}

        /// <summary>
        /// топлива на конец наряда
        /// </summary>
        public double FuelDUTEnd { get; set; }

        /// <summary>
        /// заправлено
        /// </summary>
        public double FuelDUTAdd { get; set; }

        /// <summary>
        /// слито
        /// </summary>
        public double FuelDUTSub { get; set; }

        /// <summary>
        /// расход
        /// </summary>
        public double FuelDUTExpense { get; set; }

        #endregion

        #region топливо DRT
        /// <summary>
        /// общий расход топлива на перездах
        /// </summary>
        double _FuelDRTExpensWithoutWork;
        public double FuelDRTExpensWithoutWork
        {
            get { return _FuelDRTExpensWithoutWork; }
            set { _FuelDRTExpensWithoutWork = value; }
        }
        /// <summary>
        /// средний расход топлива на перездах л/100 км
        /// </summary>
        double _FuelDRTExpensAvgWithoutWork;
        public double FuelDRTExpensAvgWithoutWork
        {
            get { return _FuelDRTExpensAvgWithoutWork; }
            set { _FuelDRTExpensAvgWithoutWork = value; }
        }
        /// <summary>
        /// общий расход топлива в полях
        /// </summary>
        double _FuelDRTExpensSquare;
        public double FuelDRTExpensSquare
        {
            get { return _FuelDRTExpensSquare; }
            set { _FuelDRTExpensSquare = value; }
        }
        /// <summary>
        /// средний расход топлива в полях л/га
        /// </summary>
        double _FuelDRTExpensAvgSquare;
        public double FuelDRTExpensAvgSquare
        {
            get { return _FuelDRTExpensAvgSquare; }
            set { _FuelDRTExpensAvgSquare = value; }
        }
        #endregion
        FuelDictionarys fuelDict;
        /// <summary>
        /// управляющие данные для создания записей наряда
        /// </summary>
        DataTable _dtControl;
        /// <summary>
        /// заблокированные записи наряда
        /// </summary>
        DataTable _dtLock;
        /// <summary>
        /// режим пересчета с управляющими данными
        /// </summary>
        bool _Control;
        atlantaDataSet _dsFromAlgorithm;
        /// <summary>
        /// счетчик сформированных записей
        /// </summary>
        int _recordNumber;
        /// <summary>
        /// дребезг пересечения границ поля
        /// </summary>
        TimeSpan tsDeltaField;
        /// <summary>
        /// дребезг датчиков
        /// </summary>
        TimeSpan tsDeltaSensor;

        Dictionary<int, OrderSquareBitmap> dcZonesBitmap;

        /// <summary>
        /// записи наряда
        /// </summary>
        List<OrderItemRecord> _oirs;

        List<OrderFueling> _ofs;

        /// <summary>
        /// привязка полей к зонам
        /// </summary>
        Dictionary<int, int> _zones_fields;
    }
}
