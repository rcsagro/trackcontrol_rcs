﻿using System.Collections.Generic;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    public class TrackZonesFinder
    {
        LON_LAT_MIN_MAX _trackBounds;
        atlantaDataSet.dataviewRow[] _d_rows;
        List<IZone> _zonesId;

        public TrackZonesFinder(LON_LAT_MIN_MAX trackBounds, atlantaDataSet.dataviewRow[] d_rows)
        {
            _zonesId = new List<IZone>(); 
            _trackBounds = trackBounds;
            _d_rows = d_rows;
        }

        public List<IZone> GetZones()
        {
           string _sSQL = "SELECT  DISTINCT zones.Zone_ID, agro_field.Id,zones.Name FROM agro_field "
            + " INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID ";
           using (DriverDb driverDb = new DriverDb())
           {
               driverDb.ConnectDb();
               driverDb.GetDataReader(_sSQL);
               while (driverDb.Read())
                {
                    int Zone_id = driverDb.GetInt32("Zone_ID");
                    IZone zone = OrderItem.ZonesModel.GetById(Zone_id);
                    //if (zone != null && (TrackInZone(zone) || BoundsTrackInBoundsZone(zone)))
                    if (zone != null && TrackInZone(zone))
                    {
                        if (!_zonesId.Contains(zone)) _zonesId.Add(zone); 
                    }
                }
               driverDb.CloseDataReader();
            }
            return _zonesId;
        }

        bool TrackInZone(IZone zone)
        {
            if (!IsTrackWithZoneIntersect(zone)) return false;

            foreach (atlantaDataSet.dataviewRow d_row in _d_rows)
            {
                if (zone.BoundContains(new PointLatLng(d_row.Lat, d_row.Lon))) return true;
            }
            return false;
        }

        bool IsTrackWithZoneIntersect(IZone zone)
        {
            return (_trackBounds.LON_min<=zone.Bounds.Right 
            && zone.Bounds.Left <= _trackBounds.LON_max
            && _trackBounds.LAT_max >= zone.Bounds.Bottom
            && zone.Bounds.Top >= _trackBounds.LAT_min);
        }
    }
}
