using System;
using System.Collections.Generic;
using Agro.Dictionaries;
using Agro.Utilites;
using GeoData;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    /// <summary>
    /// ������ ������
    /// </summary>
    public class OrderItemRecord:IDisposable 
    {
        public List<atlantaDataSet.dataviewRow> LsDrows;
        public int ZoneId;
        public int DriverId;
        public int AgregatId;
        public int FieldId;
        public int IdOrderT;
        public int IdMain;
        public Double Distance;
        public Double Square;
        public Double SquareCalcCont;
        public TimeSpan TsMove;
        public TimeSpan TsStop;
        public TimeSpan TsEngineOn;
        public int RowHandle;
        public bool AddNewRow;
        public int Confirm = 0;
        public double WidthAgr;
        public int WorkId;
        public double Price;
        public double FuelStart;
        public double FuelEnd;
        public double FuelAdd;
        public double FuelSub;
        public double FuelExpens;
        public double FuelExpensAvg;
        public double FuelExpensAvgRate;
        public double FuelExpensMove;
        public double FuelExpensStop;
        public double FuelExpensTotal;
        public double Fuel_ExpensAvg;
        public double Fuel_ExpensAvgRate;
        public List<TRealPoint> Rpoints;
        public int PointsValidity;
        public DateTime TimeStart;
        public DateTime TimeEnd;
        public AgrDrvInputSources AgrInputSource;
        public AgrDrvInputSources DrvInputSource;

        public OrderItemRecord(int idRecord)
        {
            IdOrderT = idRecord;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sSql = "SELECT   agro_ordert.*"
                + " FROM   agro_ordert  "
                + " WHERE   agro_ordert.Id = " + IdOrderT;
                driverDb.GetDataReader(sSql);
                if (driverDb.Read())
                {
                    IdMain = (int)TotUtilites.NDBNullReader(driverDb, "Id_main", 0);
                    ZoneId = (int)TotUtilites.NDBNullReader(driverDb, "Id_zone", 0);
                    DriverId = (int)TotUtilites.NDBNullReader(driverDb, "Id_driver", 0);
                    AgregatId = (int)TotUtilites.NDBNullReader(driverDb, "Id_agregat", 0);
                    GetAgregatWidthWork();
                    FieldId = (int)TotUtilites.NDBNullReader(driverDb, "Id_field", 0);
                    Square = (double)TotUtilites.NDBNullReader(driverDb, "FactSquare", 0);
                    PointsValidity = (int)TotUtilites.NDBNullReader(driverDb, "PointsValidity", 0);
                    WorkId = (int)TotUtilites.NDBNullReader(driverDb, "Id_work", 0);
                    Confirm = Convert.ToInt32(TotUtilites.NDBNullReader(driverDb, "Confirm", 0));
                    TsEngineOn = (TimeSpan)TotUtilites.NDBNullReader(driverDb, "TimeRate", 0);
                    FuelExpens = (double)TotUtilites.NDBNullReader(driverDb, "FuelExpens", 0);
                    SquareCalcCont = (double)TotUtilites.NDBNullReader(driverDb, "FactSquareCalc", 0);
                    FuelExpensAvg = (double)TotUtilites.NDBNullReader(driverDb, "FuelExpensAvg", 0);
                    FuelExpensAvgRate = (double)TotUtilites.NDBNullReader(driverDb, "FuelExpensAvgRate", 0);
                    TimeStart = (DateTime)TotUtilites.NDBNullReader(driverDb, "TimeStart", 0);
                    TimeEnd = (DateTime)TotUtilites.NDBNullReader(driverDb, "TimeEnd", 0);
                    FuelStart = (double)TotUtilites.NDBNullReader(driverDb, "FuelStart", 0);
                    FuelEnd = (double)TotUtilites.NDBNullReader(driverDb, "FuelEnd", 0);
                    FuelAdd = (double)TotUtilites.NDBNullReader(driverDb, "FuelAdd", 0);
                    FuelSub = (double)TotUtilites.NDBNullReader(driverDb, "FuelSub", 0);
                    TsEngineOn = (TimeSpan)TotUtilites.NDBNullReader(driverDb, "TimeRate", 0);
                    int source = Convert.ToInt16(TotUtilites.NDBNullReader(driverDb, "AgrInputSource", 0));
                    AgrInputSource = (AgrDrvInputSources)source;
                    source = Convert.ToInt16(TotUtilites.NDBNullReader(driverDb, "DrvInputSource", 0));
                    DrvInputSource = (AgrDrvInputSources)source;
                    CompressData cd = new CompressData("agro_datagps", IdOrderT);
                    Rpoints = cd.DeCompressReal();
                    if (WidthAgr == 0) SetAgregatWidth();
                }
                driverDb.CloseDataReader();
            }

        }

        public OrderItemRecord(int iZoneId, int iDriverId, int iAgregatId, AgrDrvInputSources agrInputSource, AgrDrvInputSources drvInputSource, List<atlantaDataSet.dataviewRow> lsDvrows)
        {
            ZoneId = iZoneId;
            DriverId = iDriverId;
            AgregatId = iAgregatId;
            AgrInputSource = agrInputSource;
            DrvInputSource = drvInputSource;
            if (ZoneId == Consts.ZONE_OUTSIDE_THE_LIST)
                FieldId = Consts.ZONE_OUTSIDE_THE_LIST;
            else
                FieldId = GetFieldId(ZoneId);
            LsDrows = lsDvrows;
            SetAgregatWidth();
            ConvertDRowsRPoints();
        }

        public OrderItemRecord(int mainID, int fieldID, int driverID, int agregatId, int workId, DateTime start, DateTime end, AgrDrvInputSources agrInputSource, AgrDrvInputSources drvInputSource)
        {
            
            IdMain = mainID;
            FieldId = fieldID;
            using (DictionaryAgroField daf = new DictionaryAgroField(FieldId))
            {
                ZoneId = daf.Id_zone;
            }
            DriverId = driverID;
            AgregatId = agregatId;
            AgrInputSource = agrInputSource;
            DrvInputSource = drvInputSource;
            WorkId = workId;
            TimeStart = start;
            TimeEnd = end;
        }

        public void ConvertDRowsRPoints()
        {
            Rpoints = new List<TRealPoint>();
            for (int i = 0; i < LsDrows.Count; i++)
            {
                TRealPoint rpoint = new TRealPoint();
                rpoint.X = LsDrows[i].Lon;
                rpoint.Y = LsDrows[i].Lat;
                Rpoints.Add(rpoint);
            }
        }

        public void SetAgregatWidth()
        {
            if (AgregatId == 0)
            {
                WidthAgr = AgroController.Instance.WidthAgregatDefault; 
            }
            if (AgregatId != 0)
            {
                GetAgregatWidthWork();
            }

        }

        /// <summary>
        /// ��� �������� �� ��������������
        /// </summary>
        /// <param name="Identifier">����� - �������� ���, ���� - ��� ��������������</param>
        /// <returns></returns>
        private int GetAgregatId(int Identifier)
        {
            if (Identifier < 0)
            {
                return -Identifier;
            }
            else
            {
                using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat())
                {
                    daa.InitFromIdentifier((ushort)Identifier);
                    return daa.Id;
                }
            }
        }
        /// <summary>
        /// ������ ��������
        /// </summary>
        /// <param name="iAgregatId"></param>
        /// <returns></returns>
        private void GetAgregatWidthWork()
        {
            using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat(AgregatId))
            {
                WidthAgr = daa.Width;
                WorkId = daa.Id_work;
            }
        }

        public void SetFuelDUT()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sSQLupdate =string.Format(@"UPDATE agro_ordert SET FuelAdd = {1}FuelAdd
                , FuelSub = {1}FuelSub
                , agro_ordert.FuelExpens = {1}FuelExpens
                , agro_ordert.FuelExpensAvg = {1}FuelExpensAvg
                , agro_ordert.FuelExpensAvgRate = {1}FuelExpensAvgRate
                WHERE ID = {0}", IdOrderT, driverDb.ParamPrefics );
                driverDb.NewSqlParameterArray(5);
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FuelAdd", FuelAdd );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FuelAdd", FuelSub );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FuelAdd", FuelExpens );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FuelAdd", FuelExpensAvg );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FuelAdd", FuelExpensAvgRate );
                driverDb.ExecuteNonQueryCommand(sSQLupdate, driverDb.GetSqlParameterArray);
            }
        }

        public void RecalcFuelExpensDUT()
        {
            //����� ������, �
            FuelExpens = Math.Round((FuelStart + FuelAdd - FuelEnd), 2);
            ////����������� �������, (��)
            //oir.Square = oir.Distance * dbWidth * 0.001 * 100;
            // ������ ������� �� ������, �
            if (SquareCalcCont > 0)
            {
                FuelExpensAvg = Math.Round(FuelExpens / SquareCalcCont, 2);
            }
            // ������ ������� � �������, �
            Double dbHMoto = TsEngineOn.TotalHours;
            if (dbHMoto > 0)
            {
                FuelExpensAvgRate = Math.Round(FuelExpens / dbHMoto, 2);
            }
        }

        public void SetPointsValidity()
        {
            PointsValidity = 0;
            if (LsDrows.Count > 0)
            {
                int iLogID_F = LsDrows[0].LogID;
                int iLogID_L = LsDrows[LsDrows.Count - 1].LogID;
                if ((iLogID_L - iLogID_F) > 0)
                {
                    double dbCalc = (double)(LsDrows.Count - 1) / (iLogID_L - iLogID_F);
                    PointsValidity = (int)(Math.Round(dbCalc * 100, 0));
                }

            }
            else
            {
                PointsValidity = 0;
            }
        }

        public bool LockRecord(bool lockRecord)
        {
            try
            {
                if ((lockRecord && DocItem.GetPointsValidityGauge(PointsValidity) == (int)ValidityColors.Green) || !lockRecord)
                {
                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb(); 
                        string sql = string.Format(@"UPDATE agro_ordert SET LockRecord = {0}
                        WHERE (ID = {1})", lockRecord ? 1 : 0, IdOrderT);
                        driverDb.ExecuteNonQueryCommand(sql);
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// ������������� ������ ������ �� ����
        /// </summary>
        /// <param name="id_main"></param>
        /// <param name="dateCompare"></param>
        /// <returns></returns>
        public static int GetIdByDate(int id_main, DateTime dateCompare)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(@"SELECT Id From agro_ordert  WHERE Id_main = {0} 
                    AND TimeStart <= {1}DateCompare AND TimeEnd >= {1}DateCompare", id_main, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "DateCompare", dateCompare );
                    return driverDb.GetScalarValueIntNull(sql, driverDb.GetSqlParameterArray);
                }
            }
            catch
            {
                return ConstsGen.RECORD_MISSING;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
           if (Rpoints !=null) Rpoints.Clear();
           //if (lsDrows != null) lsDrows.Clear(); 
        }

        #endregion

        public int AddRecord()
        {
            int idT = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                int confirm = FieldId == 0 ? 0 : Confirm;
                SetDefaultWork();
                string sqlInsert = string.Format("INSERT INTO agro_ordert ( Id_main,Id_field,Id_Zone,Id_driver, Id_agregat ,FactTime, TimeStart, TimeEnd,Confirm,Id_work,AgrInputSource,DrvInputSource)"
                  + " VALUES ({0},{1},{2},{3},{4},{7}FactTime,{7}TimeStart,{7}TimeEnd,{5},{6},{8},{9})", IdMain, FieldId, ZoneId,
                  DriverId, AgregatId, confirm, WorkId, driverDb.ParamPrefics, (int)AgrInputSource, (int)DrvInputSource);
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "FactTime", TimeEnd.Subtract( TimeStart ) );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeStart", TimeStart );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", TimeEnd );
                idT = driverDb.ExecuteReturnLastInsert( sqlInsert, driverDb.GetSqlParameterArray, "agro_ordert" );
            }
            return idT;
        }

        private void SetDefaultWork()
        {
            if (FieldId == 0) 
                WorkId = DictionaryAgroWorkType.GetFirstWorkWithType(DictionaryAgroWorkType.WorkTypes.MoveOutField);
            else if (Confirm == 0)
            {
                WorkId = DictionaryAgroWorkType.GetFirstWorkWithType(DictionaryAgroWorkType.WorkTypes.MoveInField );
            }
        }

        public bool UpdateRecord()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(@"UPDATE agro_orderT SET TimeRate = {1}TimeRate
                    , TimeStop = {1}TimeStop
                     , TimeMove = {1}TimeMove
                     , Distance = {1}Distance
                     , FactSquare = {1}FactSquare
                     , FactSquareCalc = {1}FactSquareCalc
                     , Sum = {1}Sum
                     , SpeedAvg = {1}SpeedAvg
                     , FuelStart = {1}FuelStart
                     , FuelAdd = {1}FuelAdd
                     , FuelSub = {1}FuelSub
                     , FuelEnd = {1}FuelEnd
                     , FuelExpens = {1}FuelExpens
                     , FuelExpensAvg = {1}FuelExpensAvg
                     , FuelExpensAvgRate = {1}FuelExpensAvgRate
                     , Fuel_ExpensMove = {1}Fuel_ExpensMove
                     , Fuel_ExpensStop = {1}Fuel_ExpensStop
                     , Fuel_ExpensTotal = {1}Fuel_ExpensTotal
                     , Fuel_ExpensAvg = {1}Fuel_ExpensAvg
                     , Fuel_ExpensAvgRate = {1}Fuel_ExpensAvgRate
                     , PointsValidity = {1}PointsValidity
                      WHERE (ID = {0})",  IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(21);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"TimeRate",  TsEngineOn);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"TimeStop", TsStop);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"TimeMove", TsMove);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Distance", Math.Round(Distance, 3));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FactSquare", Math.Round(Square, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FactSquareCalc", Math.Round(SquareCalcCont, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Sum", Math.Round(Square * Price, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"SpeedAvg", TsMove.TotalSeconds == 0 ? 0 : Math.Round(Distance / (TsMove.TotalSeconds / 3600), 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelStart", FuelStart);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelAdd", FuelAdd);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelSub", FuelSub);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelEnd", FuelEnd);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelExpens", FuelExpens);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelExpensAvg", FuelExpensAvg);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"FuelExpensAvgRate", FuelExpensAvgRate);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Fuel_ExpensMove", FuelExpensMove);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Fuel_ExpensStop", FuelExpensStop);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Fuel_ExpensTotal", FuelExpensTotal);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Fuel_ExpensAvg", Fuel_ExpensAvg);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"Fuel_ExpensAvgRate", Fuel_ExpensAvgRate);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics +"PointsValidity", PointsValidity);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteRecord()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format("DELETE FROM   agro_ordert WHERE agro_ordert.Id = {0} ", IdOrderT);
                    IdOrderT = 0;
                    driverDb.ExecuteNonQueryCommand(sql);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateRecordTimeStart()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string _sSQL = string.Format(@"UPDATE agro_orderT SET TimeStart = {1}TimeStart
                      WHERE (ID = {0})", IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeStart", TimeStart );
                    driverDb.ExecuteNonQueryCommand(_sSQL,driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateRecordTimeEnd()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string _sSQL = string.Format(@"UPDATE agro_orderT SET TimeEnd = {1}TimeEnd
                      WHERE (ID = {0})", IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", TimeEnd );
                    driverDb.ExecuteNonQueryCommand(_sSQL, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateControlParameters()
        {
            try
            {
                if (FieldId == 0)
                    ZoneId = 0;
                else
                {
                    using (DictionaryAgroField daf = new DictionaryAgroField(FieldId))
                    {
                        ZoneId = daf.Id_zone;
                    }
                }
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format(@"UPDATE agro_orderT SET Id_field = {0},Id_zone = {1},Id_driver = {2}, Id_agregat = {3} ,Id_work = {4}
                      WHERE (ID = {5})", FieldId, ZoneId, DriverId, AgregatId, WorkId, IdOrderT);
                    driverDb.ExecuteNonQueryCommand(sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        private int GetFieldId(int ZoneId)
        {
            if (ZoneId < 0)
            {
                return -ZoneId;
            }
            else if (ZoneId == 0)
            {
                return 0;
            }
            else
            {
                using (DictionaryAgroField daf = new DictionaryAgroField())
                {
                    return daf.GetIdFromZoneId(ZoneId);
                }
            }
        }

        /// <summary>
        /// ���� ������� ��������� ����
        /// </summary>
        /// <param name="ID_Field"></param>
        /// <returns></returns>
        private int GetZone(int ID_Field)
        {
            using (DictionaryAgroField daf = new DictionaryAgroField(ID_Field))
            {
            return daf.Id_zone;
            }
        }

        /// <summary>
        /// �������� ������,����������� � ������ ������
        /// </summary>
        public string GetWorkName()
        {
            using (DictionaryAgroWorkType dawt = new DictionaryAgroWorkType(WorkId))
            {
                return dawt.Name;
            }
        }

    }
}
