namespace Agro
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
            DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState5 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState6 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState7 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState8 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Order));
            this.gvContentPrice = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.FSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcContent = new DevExpress.XtraGrid.GridControl();
            this.gbvContent = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.���� = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Id = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.GFName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Square = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TimeStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Family = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.AName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Width_c = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DistanceT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquareCalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpeedAvgField = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Zone_ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDrvInputSource = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PriceC = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Id_agregat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAgrInputSource = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.FieldGroupeLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FieldLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.ttcGridView = new DevExpress.Utils.ToolTipController(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btSave = new DevExpress.XtraBars.BarButtonItem();
            this.btRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFact = new DevExpress.XtraBars.BarSubItem();
            this.btFactControl = new DevExpress.XtraBars.BarButtonItem();
            this.btFact = new DevExpress.XtraBars.BarButtonItem();
            this.btLog = new DevExpress.XtraBars.BarButtonItem();
            this.btMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bsiReports = new DevExpress.XtraBars.BarSubItem();
            this.btReport = new DevExpress.XtraBars.BarButtonItem();
            this.btReportContentGrouped = new DevExpress.XtraBars.BarButtonItem();
            this.btReportContent = new DevExpress.XtraBars.BarButtonItem();
            this.btReportFuelDUT = new DevExpress.XtraBars.BarButtonItem();
            this.btReportFuelDRT = new DevExpress.XtraBars.BarButtonItem();
            this.bsiExcel = new DevExpress.XtraBars.BarSubItem();
            this.btExcelTotal = new DevExpress.XtraBars.BarButtonItem();
            this.btExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bsiStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.leMobitel = new DevExpress.XtraEditors.LookUpEdit();
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.deDateInit = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txId = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.meRemark = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.xtbCont = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gcContentGrp = new DevExpress.XtraGrid.GridControl();
            this.gvContentGrp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FNameGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SquareGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_driverGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leDriver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDrvInputSourceGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_agregatGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAgrInputSourceGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FactSquareGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FactSquareCalcGr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.le = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit5View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gcFuelDRT = new DevExpress.XtraGrid.GridControl();
            this.gvFuelDRT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IDDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FieldDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FamilyDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStartDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEndDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensMoveDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensStopDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensTotalDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensAvgDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensAvgRateDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBar3 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabMap = new DevExpress.XtraTab.XtraTabPage();
            this.pnMap = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.scControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcControlDrv = new DevExpress.XtraGrid.GridControl();
            this.gvControlDrv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIddrv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_object1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DriverLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colEnterDrv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExitDrv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeDrv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemGridLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTimeEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.scControlInner = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcControlAgr = new DevExpress.XtraGrid.GridControl();
            this.gvControlAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AgrLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colEnterAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txEditAgr = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExitAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcControlField = new DevExpress.XtraGrid.GridControl();
            this.gvControlField = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FldLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colEnter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txEditFld = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tmeTime = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DrvLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTimeEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.pbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemProgressBar4 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pgOrder = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.crTotalc = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMobitel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateLastRecalc = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquare = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareOverlap = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.errRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erParth = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crTotalPath = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erPlaceStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPlaceEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSpeed = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crTotalTime = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erDateStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeWork = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeMove = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeStop = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crDUT = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erExpensSquareDUT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensSquareAvgDUT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensWithoutWorkDUT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensAvgWithoutWorkDUT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crDRT = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erExpensSquareDRT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensSquareAvgDRT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensWithoutWorkDRT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erExpensAvgWithoutWorkDRT = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.ValidityData = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.PointsValidity = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsCalc = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsFact = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsIntervalMax = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.repositoryItemProgressBar5 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemProgressBar6 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.erRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcQuality = new DevExpress.XtraEditors.GroupControl();
            this.elLightsIndicator = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateGauge = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldGroupeLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateInit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateInit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbCont)).BeginInit();
            this.xtbCont.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcContentGrp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentGrp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.le)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5View)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelDRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.xtraTabMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scControl)).BeginInit();
            this.scControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcControlDrv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlDrv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DriverLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scControlInner)).BeginInit();
            this.scControlInner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcControlAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgrLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEditAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcControlField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FldLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEditFld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmeTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrvLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuality)).BeginInit();
            this.gcQuality.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateGauge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            this.SuspendLayout();
            // 
            // gvContentPrice
            // 
            this.gvContentPrice.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.Empty.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentPrice.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentPrice.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentPrice.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentPrice.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvContentPrice.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvContentPrice.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvContentPrice.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentPrice.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentPrice.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvContentPrice.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentPrice.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentPrice.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentPrice.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentPrice.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.OddRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.OddRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvContentPrice.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvContentPrice.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvContentPrice.Appearance.Preview.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.Preview.Options.UseFont = true;
            this.gvContentPrice.Appearance.Preview.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentPrice.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.Row.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.Row.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentPrice.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvContentPrice.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentPrice.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentPrice.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvContentPrice.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvContentPrice.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvContentPrice.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvContentPrice.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentPrice.Appearance.VertLine.Options.UseBackColor = true;
            this.gvContentPrice.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdP,
            this.Work,
            this.FSP,
            this.Price,
            this.Sum,
            this.Comment});
            this.gvContentPrice.GridControl = this.gcContent;
            this.gvContentPrice.Name = "gvContentPrice";
            this.gvContentPrice.OptionsView.EnableAppearanceEvenRow = true;
            this.gvContentPrice.OptionsView.EnableAppearanceOddRow = true;
            this.gvContentPrice.OptionsView.ShowGroupPanel = false;
            this.gvContentPrice.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvContentPrice_CellValueChanged);
            // 
            // IdP
            // 
            this.IdP.Caption = "IdP";
            this.IdP.FieldName = "IdP";
            this.IdP.Name = "IdP";
            this.IdP.OptionsColumn.AllowEdit = false;
            // 
            // Work
            // 
            this.Work.AppearanceHeader.Options.UseTextOptions = true;
            this.Work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Work.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Work.Caption = "��� �����";
            this.Work.ColumnEdit = this.WorkLookUp;
            this.Work.FieldName = "Work";
            this.Work.Name = "Work";
            this.Work.Visible = true;
            this.Work.VisibleIndex = 0;
            // 
            // WorkLookUp
            // 
            this.WorkLookUp.AutoHeight = false;
            this.WorkLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��� �����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.WorkLookUp.DisplayMember = "Name";
            this.WorkLookUp.DropDownRows = 10;
            this.WorkLookUp.Name = "WorkLookUp";
            this.WorkLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.WorkLookUp.ValueMember = "Id";
            // 
            // FSP
            // 
            this.FSP.AppearanceHeader.Options.UseTextOptions = true;
            this.FSP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FSP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FSP.Caption = "���������� (�� �������), ��";
            this.FSP.FieldName = "FSP";
            this.FSP.Name = "FSP";
            this.FSP.OptionsColumn.AllowEdit = false;
            this.FSP.Visible = true;
            this.FSP.VisibleIndex = 1;
            // 
            // Price
            // 
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Price.Caption = "��������, ���";
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 2;
            // 
            // Sum
            // 
            this.Sum.AppearanceHeader.Options.UseTextOptions = true;
            this.Sum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sum.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Sum.Caption = "�����, ���";
            this.Sum.FieldName = "Sum";
            this.Sum.Name = "Sum";
            this.Sum.OptionsColumn.AllowEdit = false;
            this.Sum.Visible = true;
            this.Sum.VisibleIndex = 3;
            // 
            // Comment
            // 
            this.Comment.Caption = "����������";
            this.Comment.FieldName = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.Visible = true;
            this.Comment.VisibleIndex = 4;
            // 
            // gcContent
            // 
            this.gcContent.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.LevelTemplate = this.gvContentPrice;
            gridLevelNode3.RelationName = "Price";
            this.gcContent.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gcContent.Location = new System.Drawing.Point(0, 0);
            this.gcContent.MainView = this.gbvContent;
            this.gcContent.Name = "gcContent";
            this.gcContent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemGridLookUpEdit1,
            this.WorkLookUp,
            this.repositoryItemTimeEdit1,
            this.FieldGroupeLookUp,
            this.FieldLookUp,
            this.repositoryItemLookUpEdit1});
            this.gcContent.Size = new System.Drawing.Size(801, 306);
            this.gcContent.TabIndex = 25;
            this.gcContent.ToolTipController = this.ttcGridView;
            this.gcContent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gbvContent,
            this.gvContentPrice});
            // 
            // gbvContent
            // 
            this.gbvContent.ActiveFilterEnabled = false;
            this.gbvContent.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gbvContent.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.BandPanel.Options.UseBackColor = true;
            this.gbvContent.Appearance.BandPanel.Options.UseBorderColor = true;
            this.gbvContent.Appearance.BandPanel.Options.UseFont = true;
            this.gbvContent.Appearance.BandPanel.Options.UseForeColor = true;
            this.gbvContent.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.gbvContent.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gbvContent.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gbvContent.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gbvContent.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gbvContent.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gbvContent.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gbvContent.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gbvContent.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gbvContent.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.Empty.Options.UseBackColor = true;
            this.gbvContent.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gbvContent.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gbvContent.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.EvenRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.EvenRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gbvContent.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gbvContent.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gbvContent.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gbvContent.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gbvContent.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gbvContent.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gbvContent.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gbvContent.Appearance.FixedLine.Options.UseBackColor = true;
            this.gbvContent.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gbvContent.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gbvContent.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gbvContent.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gbvContent.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gbvContent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gbvContent.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gbvContent.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gbvContent.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gbvContent.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gbvContent.Appearance.GroupButton.Options.UseBackColor = true;
            this.gbvContent.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gbvContent.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gbvContent.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gbvContent.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gbvContent.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gbvContent.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gbvContent.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gbvContent.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.GroupRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.GroupRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gbvContent.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gbvContent.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gbvContent.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.gbvContent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gbvContent.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gbvContent.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.HorzLine.Options.UseBackColor = true;
            this.gbvContent.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gbvContent.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gbvContent.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.OddRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.OddRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.OddRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gbvContent.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gbvContent.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gbvContent.Appearance.Preview.Options.UseBackColor = true;
            this.gbvContent.Appearance.Preview.Options.UseFont = true;
            this.gbvContent.Appearance.Preview.Options.UseForeColor = true;
            this.gbvContent.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gbvContent.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.Row.Options.UseBackColor = true;
            this.gbvContent.Appearance.Row.Options.UseForeColor = true;
            this.gbvContent.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gbvContent.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gbvContent.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gbvContent.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gbvContent.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gbvContent.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gbvContent.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gbvContent.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gbvContent.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gbvContent.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gbvContent.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gbvContent.Appearance.VertLine.Options.UseBackColor = true;
            this.gbvContent.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.����,
            this.gridBand2,
            this.gridBand3});
            this.gbvContent.ColumnPanelRowHeight = 40;
            this.gbvContent.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Id,
            this.GFName,
            this.FName,
            this.Zone_ID,
            this.Square,
            this.Family,
            this.colDrvInputSource,
            this.TimeStart,
            this.TimeEnd,
            this.FactTime,
            this.TimeMove,
            this.TimeStop,
            this.TimeRate,
            this.DistanceT,
            this.FactSquare,
            this.FactSquareCalc,
            this.PriceC,
            this.SpeedAvgField,
            this.Id_agregat,
            this.AName,
            this.colAgrInputSource,
            this.Width_c});
            this.gbvContent.GridControl = this.gcContent;
            this.gbvContent.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquare", null, "(���������� �� �������:  {0:N2} ��)"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquareCalc", null, "(���������� �� �������:  {0:N2}  ��)")});
            this.gbvContent.Name = "gbvContent";
            this.gbvContent.OptionsView.EnableAppearanceEvenRow = true;
            this.gbvContent.OptionsView.EnableAppearanceOddRow = true;
            this.gbvContent.OptionsView.ShowFooter = true;
            this.gbvContent.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gbvContent_RowCellStyle);
            // 
            // ����
            // 
            this.����.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.����.AppearanceHeader.Options.UseFont = true;
            this.����.AppearanceHeader.Options.UseTextOptions = true;
            this.����.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.����.AutoFillDown = false;
            this.����.Caption = "����";
            this.����.Columns.Add(this.Id);
            this.����.Columns.Add(this.GFName);
            this.����.Columns.Add(this.FName);
            this.����.Columns.Add(this.Square);
            this.����.Name = "����";
            this.����.VisibleIndex = 0;
            this.����.Width = 180;
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id.AutoFillDown = true;
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.ReadOnly = true;
            this.Id.Width = 20;
            // 
            // GFName
            // 
            this.GFName.AppearanceHeader.Options.UseTextOptions = true;
            this.GFName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GFName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.GFName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GFName.AutoFillDown = true;
            this.GFName.Caption = "������ �����";
            this.GFName.FieldName = "GFName";
            this.GFName.Name = "GFName";
            this.GFName.OptionsColumn.AllowEdit = false;
            this.GFName.OptionsColumn.ReadOnly = true;
            this.GFName.Visible = true;
            this.GFName.Width = 46;
            // 
            // FName
            // 
            this.FName.AppearanceHeader.Options.UseTextOptions = true;
            this.FName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FName.AutoFillDown = true;
            this.FName.Caption = "����";
            this.FName.FieldName = "FName";
            this.FName.Name = "FName";
            this.FName.OptionsColumn.AllowEdit = false;
            this.FName.OptionsColumn.ReadOnly = true;
            this.FName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "FName", "�������: {0}")});
            this.FName.Visible = true;
            this.FName.Width = 68;
            // 
            // Square
            // 
            this.Square.AppearanceHeader.Options.UseTextOptions = true;
            this.Square.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Square.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Square.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Square.AutoFillDown = true;
            this.Square.Caption = "�������, ��";
            this.Square.FieldName = "Square";
            this.Square.Name = "Square";
            this.Square.OptionsColumn.AllowEdit = false;
            this.Square.OptionsColumn.ReadOnly = true;
            this.Square.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Square", "�����")});
            this.Square.Visible = true;
            this.Square.Width = 66;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AutoFillDown = false;
            this.gridBand2.Caption = "�����";
            this.gridBand2.Columns.Add(this.TimeStart);
            this.gridBand2.Columns.Add(this.TimeEnd);
            this.gridBand2.Columns.Add(this.FactTime);
            this.gridBand2.Columns.Add(this.TimeMove);
            this.gridBand2.Columns.Add(this.TimeStop);
            this.gridBand2.Columns.Add(this.TimeRate);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 360;
            // 
            // TimeStart
            // 
            this.TimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStart.AutoFillDown = true;
            this.TimeStart.Caption = "�����";
            this.TimeStart.DisplayFormat.FormatString = "t";
            this.TimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStart.FieldName = "TimeStart";
            this.TimeStart.Name = "TimeStart";
            this.TimeStart.OptionsColumn.AllowEdit = false;
            this.TimeStart.OptionsColumn.ReadOnly = true;
            this.TimeStart.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Min, "TimeStart", "{0:t}")});
            this.TimeStart.Visible = true;
            this.TimeStart.Width = 45;
            // 
            // TimeEnd
            // 
            this.TimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEnd.AutoFillDown = true;
            this.TimeEnd.Caption = "�����";
            this.TimeEnd.DisplayFormat.FormatString = "t";
            this.TimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEnd.FieldName = "TimeEnd";
            this.TimeEnd.Name = "TimeEnd";
            this.TimeEnd.OptionsColumn.AllowEdit = false;
            this.TimeEnd.OptionsColumn.ReadOnly = true;
            this.TimeEnd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Max, "TimeEnd", "{0:t}")});
            this.TimeEnd.Visible = true;
            this.TimeEnd.Width = 45;
            // 
            // FactTime
            // 
            this.FactTime.AppearanceCell.Options.UseTextOptions = true;
            this.FactTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.Options.UseTextOptions = true;
            this.FactTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FactTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactTime.AutoFillDown = true;
            this.FactTime.Caption = "����� �����, �";
            this.FactTime.DisplayFormat.FormatString = "t";
            this.FactTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FactTime.FieldName = "FactTime";
            this.FactTime.Name = "FactTime";
            this.FactTime.OptionsColumn.AllowEdit = false;
            this.FactTime.OptionsColumn.ReadOnly = true;
            this.FactTime.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactTime", "{0}")});
            this.FactTime.Visible = true;
            this.FactTime.Width = 55;
            // 
            // TimeMove
            // 
            this.TimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMove.Caption = "����� ��������, �";
            this.TimeMove.FieldName = "TimeMove";
            this.TimeMove.Name = "TimeMove";
            this.TimeMove.OptionsColumn.AllowEdit = false;
            this.TimeMove.OptionsColumn.ReadOnly = true;
            this.TimeMove.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeMove", "{0}")});
            this.TimeMove.Visible = true;
            this.TimeMove.Width = 67;
            // 
            // TimeStop
            // 
            this.TimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStop.Caption = "����� �������, �";
            this.TimeStop.FieldName = "TimeStop";
            this.TimeStop.Name = "TimeStop";
            this.TimeStop.OptionsColumn.AllowEdit = false;
            this.TimeStop.OptionsColumn.ReadOnly = true;
            this.TimeStop.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeStop", "{0}")});
            this.TimeStop.Visible = true;
            this.TimeStop.Width = 55;
            // 
            // TimeRate
            // 
            this.TimeRate.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRate.Caption = "��������, �";
            this.TimeRate.DisplayFormat.FormatString = "t";
            this.TimeRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeRate.FieldName = "TimeRate";
            this.TimeRate.Name = "TimeRate";
            this.TimeRate.OptionsColumn.AllowEdit = false;
            this.TimeRate.OptionsColumn.ReadOnly = true;
            this.TimeRate.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeRate", "{0}")});
            this.TimeRate.Visible = true;
            this.TimeRate.Width = 93;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.AutoFillDown = false;
            this.gridBand3.Caption = "������";
            this.gridBand3.Columns.Add(this.Family);
            this.gridBand3.Columns.Add(this.AName);
            this.gridBand3.Columns.Add(this.Width_c);
            this.gridBand3.Columns.Add(this.DistanceT);
            this.gridBand3.Columns.Add(this.FactSquare);
            this.gridBand3.Columns.Add(this.FactSquareCalc);
            this.gridBand3.Columns.Add(this.SpeedAvgField);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 549;
            // 
            // Family
            // 
            this.Family.AppearanceHeader.Options.UseTextOptions = true;
            this.Family.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Family.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Family.Caption = "��������";
            this.Family.FieldName = "Family";
            this.Family.Name = "Family";
            this.Family.OptionsColumn.AllowEdit = false;
            this.Family.Visible = true;
            this.Family.Width = 71;
            // 
            // AName
            // 
            this.AName.AppearanceHeader.Options.UseTextOptions = true;
            this.AName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AName.Caption = "�������";
            this.AName.FieldName = "AName";
            this.AName.Name = "AName";
            this.AName.OptionsColumn.AllowEdit = false;
            this.AName.Visible = true;
            this.AName.Width = 69;
            // 
            // Width_c
            // 
            this.Width_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Width_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_c.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Width_c.Caption = "������, �";
            this.Width_c.FieldName = "Width";
            this.Width_c.Name = "Width_c";
            this.Width_c.OptionsColumn.AllowEdit = false;
            this.Width_c.Visible = true;
            this.Width_c.Width = 61;
            // 
            // DistanceT
            // 
            this.DistanceT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DistanceT.Caption = "���������� ���� , ��";
            this.DistanceT.FieldName = "Distance";
            this.DistanceT.Name = "DistanceT";
            this.DistanceT.OptionsColumn.AllowEdit = false;
            this.DistanceT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:#.##}")});
            this.DistanceT.Visible = true;
            this.DistanceT.Width = 80;
            // 
            // FactSquare
            // 
            this.FactSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquare.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FactSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquare.AutoFillDown = true;
            this.FactSquare.Caption = "���������� (�� �������), ��";
            this.FactSquare.FieldName = "FactSquare";
            this.FactSquare.Name = "FactSquare";
            this.FactSquare.OptionsColumn.AllowEdit = false;
            this.FactSquare.OptionsColumn.ReadOnly = true;
            this.FactSquare.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquare", "{0:#.##}")});
            this.FactSquare.Visible = true;
            this.FactSquare.Width = 87;
            // 
            // FactSquareCalc
            // 
            this.FactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareCalc.Caption = "���������� (�� �������), ��";
            this.FactSquareCalc.FieldName = "FactSquareCalc";
            this.FactSquareCalc.Name = "FactSquareCalc";
            this.FactSquareCalc.OptionsColumn.AllowEdit = false;
            this.FactSquareCalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquareCalc", "{0:#.##}")});
            this.FactSquareCalc.Visible = true;
            this.FactSquareCalc.Width = 98;
            // 
            // SpeedAvgField
            // 
            this.SpeedAvgField.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvgField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvgField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvgField.Caption = "������� ��������";
            this.SpeedAvgField.FieldName = "SpeedAvg";
            this.SpeedAvgField.Name = "SpeedAvgField";
            this.SpeedAvgField.OptionsColumn.AllowEdit = false;
            this.SpeedAvgField.OptionsColumn.ReadOnly = true;
            this.SpeedAvgField.Visible = true;
            this.SpeedAvgField.Width = 83;
            // 
            // Zone_ID
            // 
            this.Zone_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.Zone_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Zone_ID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Zone_ID.Caption = "Zone_ID";
            this.Zone_ID.FieldName = "Zone_ID";
            this.Zone_ID.Name = "Zone_ID";
            this.Zone_ID.OptionsColumn.AllowEdit = false;
            this.Zone_ID.OptionsColumn.ReadOnly = true;
            // 
            // colDrvInputSource
            // 
            this.colDrvInputSource.Caption = "DrvInputSource";
            this.colDrvInputSource.FieldName = "DrvInputSource";
            this.colDrvInputSource.Name = "colDrvInputSource";
            this.colDrvInputSource.OptionsColumn.AllowEdit = false;
            this.colDrvInputSource.OptionsColumn.ReadOnly = true;
            // 
            // PriceC
            // 
            this.PriceC.AppearanceHeader.Options.UseTextOptions = true;
            this.PriceC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PriceC.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PriceC.Caption = "Price";
            this.PriceC.FieldName = "Price";
            this.PriceC.Name = "PriceC";
            this.PriceC.OptionsColumn.AllowEdit = false;
            this.PriceC.Width = 20;
            // 
            // Id_agregat
            // 
            this.Id_agregat.Caption = "Id_agregat";
            this.Id_agregat.FieldName = "Id_agregat";
            this.Id_agregat.Name = "Id_agregat";
            // 
            // colAgrInputSource
            // 
            this.colAgrInputSource.Caption = "AgrInputSource";
            this.colAgrInputSource.FieldName = "AgrInputSource";
            this.colAgrInputSource.Name = "colAgrInputSource";
            this.colAgrInputSource.OptionsColumn.AllowEdit = false;
            this.colAgrInputSource.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            this.repositoryItemTimeEdit1.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // FieldGroupeLookUp
            // 
            this.FieldGroupeLookUp.AutoHeight = false;
            this.FieldGroupeLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FieldGroupeLookUp.Name = "FieldGroupeLookUp";
            this.FieldGroupeLookUp.View = this.repositoryItemGridLookUpEdit2View;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // FieldLookUp
            // 
            this.FieldLookUp.AutoHeight = false;
            this.FieldLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FieldLookUp.Name = "FieldLookUp";
            this.FieldLookUp.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // ttcGridView
            // 
            this.ttcGridView.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcGridView_GetActiveObjectInfo);
            // 
            // gridColumn7
            // 
            gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn21
            // 
            gridColumn21.Name = "gridColumn21";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btRefresh,
            this.btMapView,
            this.bsiExcel,
            this.btExcelTotal,
            this.btExcel,
            this.bsiStatus,
            this.btSave,
            this.btDelete,
            this.bsiFact,
            this.btFactControl,
            this.btFact,
            this.bsiReports,
            this.btLog,
            this.btReport,
            this.btReportContentGrouped,
            this.btReportContent,
            this.btReportFuelDUT,
            this.btReportFuelDRT});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 21;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btLog, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btMapView, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiReports, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btSave
            // 
            this.btSave.Caption = "���������";
            this.btSave.Glyph = global::Agro.Properties.ResourcesImages.Save;
            this.btSave.Id = 7;
            this.btSave.Name = "btSave";
            this.btSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btSave_ItemClick);
            // 
            // btRefresh
            // 
            this.btRefresh.Caption = "��������";
            this.btRefresh.Glyph = global::Agro.Properties.ResourcesImages.ref_16;
            this.btRefresh.Id = 0;
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btRefresh_ItemClick);
            // 
            // bsiFact
            // 
            this.bsiFact.Caption = "����";
            this.bsiFact.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFact.Glyph")));
            this.bsiFact.Id = 10;
            this.bsiFact.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btFactControl),
            new DevExpress.XtraBars.LinkPersistInfo(this.btFact)});
            this.bsiFact.Name = "bsiFact";
            // 
            // btFactControl
            // 
            this.btFactControl.Caption = "� ������ ����������� ������";
            this.btFactControl.Id = 11;
            this.btFactControl.Name = "btFactControl";
            this.btFactControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFactControl_ItemClick);
            // 
            // btFact
            // 
            this.btFact.Caption = "��� ����� ����������� ������";
            this.btFact.Id = 12;
            this.btFact.Name = "btFact";
            this.btFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFact_ItemClick);
            // 
            // btLog
            // 
            this.btLog.Caption = "������ �������";
            this.btLog.Glyph = ((System.Drawing.Image)(resources.GetObject("btLog.Glyph")));
            this.btLog.Id = 20;
            this.btLog.Name = "btLog";
            this.btLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btLog_ItemClick);
            // 
            // btMapView
            // 
            this.btMapView.Caption = "�����";
            this.btMapView.Glyph = ((System.Drawing.Image)(resources.GetObject("btMapView.Glyph")));
            this.btMapView.Id = 2;
            this.btMapView.Name = "btMapView";
            this.btMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btMapView_ItemClick);
            // 
            // bsiReports
            // 
            this.bsiReports.Caption = "������";
            this.bsiReports.Glyph = global::Agro.Properties.ResourcesImages.reports_stack;
            this.bsiReports.Id = 13;
            this.bsiReports.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportContentGrouped),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportContent),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportFuelDUT),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportFuelDRT)});
            this.bsiReports.Name = "bsiReports";
            // 
            // btReport
            // 
            this.btReport.Caption = "�����";
            this.btReport.Glyph = global::Agro.Properties.ResourcesImages.report;
            this.btReport.Id = 14;
            this.btReport.Name = "btReport";
            this.btReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReport_ItemClick);
            // 
            // btReportContentGrouped
            // 
            this.btReportContentGrouped.Caption = "����������";
            this.btReportContentGrouped.Glyph = global::Agro.Properties.ResourcesImages.report;
            this.btReportContentGrouped.Id = 15;
            this.btReportContentGrouped.Name = "btReportContentGrouped";
            this.btReportContentGrouped.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportContentGrouped_ItemClick);
            // 
            // btReportContent
            // 
            this.btReportContent.Caption = "������";
            this.btReportContent.Glyph = global::Agro.Properties.ResourcesImages.report;
            this.btReportContent.Id = 16;
            this.btReportContent.Name = "btReportContent";
            this.btReportContent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportContent_ItemClick);
            // 
            // btReportFuelDUT
            // 
            this.btReportFuelDUT.Caption = "������� (���)";
            this.btReportFuelDUT.Glyph = global::Agro.Properties.ResourcesImages.report;
            this.btReportFuelDUT.Id = 17;
            this.btReportFuelDUT.Name = "btReportFuelDUT";
            this.btReportFuelDUT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportFuelDUT_ItemClick);
            // 
            // btReportFuelDRT
            // 
            this.btReportFuelDRT.Caption = "������� (���/CAN)";
            this.btReportFuelDRT.Glyph = global::Agro.Properties.ResourcesImages.report;
            this.btReportFuelDRT.Id = 18;
            this.btReportFuelDRT.Name = "btReportFuelDRT";
            this.btReportFuelDRT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportFuelDRT_ItemClick);
            // 
            // bsiExcel
            // 
            this.bsiExcel.Caption = "������� � Excel";
            this.bsiExcel.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiExcel.Glyph")));
            this.bsiExcel.Id = 3;
            this.bsiExcel.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcelTotal),
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcel)});
            this.bsiExcel.Name = "bsiExcel";
            // 
            // btExcelTotal
            // 
            this.btExcelTotal.Caption = "������� ����������";
            this.btExcelTotal.Id = 4;
            this.btExcelTotal.Name = "btExcelTotal";
            this.btExcelTotal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcelTotal_ItemClick);
            // 
            // btExcel
            // 
            this.btExcel.Caption = "������� ������";
            this.btExcel.Id = 5;
            this.btExcel.Name = "btExcel";
            this.btExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcel_ItemClick);
            // 
            // btDelete
            // 
            this.btDelete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btDelete.Caption = "�������";
            this.btDelete.Glyph = global::Agro.Properties.ResourcesImages.Remove_16;
            this.btDelete.Id = 8;
            this.btDelete.Name = "btDelete";
            this.btDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btDelete_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiStatus)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bsiStatus
            // 
            this.bsiStatus.Caption = "������";
            this.bsiStatus.Id = 6;
            this.bsiStatus.Name = "bsiStatus";
            this.bsiStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(831, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 533);
            this.barDockControlBottom.Size = new System.Drawing.Size(831, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 509);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(831, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 509);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.leMobitel);
            this.panelControl1.Controls.Add(this.leGroupe);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.deDateInit);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txId);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(14, 34);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(370, 120);
            this.panelControl1.TabIndex = 4;
            // 
            // leMobitel
            // 
            this.leMobitel.Location = new System.Drawing.Point(107, 81);
            this.leMobitel.MenuManager = this.barManager1;
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leMobitel.Properties.Appearance.Options.UseFont = true;
            this.leMobitel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Properties.NullText = "";
            this.leMobitel.Size = new System.Drawing.Size(258, 20);
            this.leMobitel.TabIndex = 7;
            this.leMobitel.EditValueChanged += new System.EventHandler(this.leMobitel_EditValueChanged);
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(107, 50);
            this.leGroupe.MenuManager = this.barManager1;
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leGroupe.Properties.Appearance.Options.UseFont = true;
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Size = new System.Drawing.Size(258, 20);
            this.leGroupe.TabIndex = 6;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Location = new System.Drawing.Point(9, 83);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 14);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "������";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl3.Location = new System.Drawing.Point(9, 52);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(79, 14);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "������ �����";
            // 
            // deDateInit
            // 
            this.deDateInit.EditValue = null;
            this.deDateInit.Location = new System.Drawing.Point(189, 18);
            this.deDateInit.MenuManager = this.barManager1;
            this.deDateInit.Name = "deDateInit";
            this.deDateInit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.deDateInit.Properties.Appearance.Options.UseFont = true;
            this.deDateInit.Properties.Appearance.Options.UseTextOptions = true;
            this.deDateInit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deDateInit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateInit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deDateInit.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deDateInit.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deDateInit.Properties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F5);
            this.deDateInit.Properties.DisplayFormat.FormatString = "g";
            this.deDateInit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deDateInit.Properties.EditFormat.FormatString = "g";
            this.deDateInit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deDateInit.Properties.Mask.EditMask = "g";
            this.deDateInit.Size = new System.Drawing.Size(176, 20);
            this.deDateInit.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl2.Location = new System.Drawing.Point(147, 21);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(26, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "����";
            // 
            // txId
            // 
            this.txId.EditValue = "0";
            this.txId.Location = new System.Drawing.Point(62, 17);
            this.txId.MenuManager = this.barManager1;
            this.txId.Name = "txId";
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txId.Properties.ReadOnly = true;
            this.txId.Size = new System.Drawing.Size(68, 20);
            this.txId.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl1.Location = new System.Drawing.Point(9, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(37, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "�����";
            // 
            // meRemark
            // 
            this.meRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.meRemark.Location = new System.Drawing.Point(106, 158);
            this.meRemark.Name = "meRemark";
            this.meRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.meRemark.Properties.Appearance.Options.UseFont = true;
            this.meRemark.Size = new System.Drawing.Size(591, 32);
            this.meRemark.TabIndex = 70;
            this.meRemark.UseOptimizedRendering = true;
            this.meRemark.EditValueChanged += new System.EventHandler(this.meRemark_EditValueChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl9.Location = new System.Drawing.Point(27, 163);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(71, 14);
            this.labelControl9.TabIndex = 69;
            this.labelControl9.Text = "����������";
            // 
            // xtbCont
            // 
            this.xtbCont.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtbCont.Location = new System.Drawing.Point(12, 196);
            this.xtbCont.Name = "xtbCont";
            this.xtbCont.SelectedTabPage = this.xtraTabPage6;
            this.xtbCont.Size = new System.Drawing.Size(807, 334);
            this.xtbCont.TabIndex = 71;
            this.xtbCont.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage6,
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabMap,
            this.xtraTabPage5});
            this.xtbCont.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtbCont_SelectedPageChanged);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gcContentGrp);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(801, 306);
            this.xtraTabPage6.Text = "����������";
            // 
            // gcContentGrp
            // 
            this.gcContentGrp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcContentGrp.Location = new System.Drawing.Point(0, 0);
            this.gcContentGrp.MainView = this.gvContentGrp;
            this.gcContentGrp.MenuManager = this.barManager1;
            this.gcContentGrp.Name = "gcContentGrp";
            this.gcContentGrp.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.le,
            this.leDriver,
            this.leAgregat});
            this.gcContentGrp.Size = new System.Drawing.Size(801, 306);
            this.gcContentGrp.TabIndex = 0;
            this.gcContentGrp.ToolTipController = this.ttcGridView;
            this.gcContentGrp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContentGrp});
            // 
            // gvContentGrp
            // 
            this.gvContentGrp.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.Empty.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentGrp.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentGrp.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentGrp.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentGrp.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvContentGrp.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvContentGrp.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvContentGrp.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentGrp.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentGrp.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvContentGrp.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentGrp.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentGrp.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentGrp.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentGrp.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.OddRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.OddRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvContentGrp.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvContentGrp.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvContentGrp.Appearance.Preview.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.Preview.Options.UseFont = true;
            this.gvContentGrp.Appearance.Preview.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentGrp.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.Row.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.Row.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentGrp.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvContentGrp.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentGrp.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentGrp.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvContentGrp.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvContentGrp.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvContentGrp.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvContentGrp.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentGrp.Appearance.VertLine.Options.UseBackColor = true;
            this.gvContentGrp.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FNameGr,
            this.SquareGr,
            this.Id_driverGr,
            this.colDrvInputSourceGr,
            this.Id_agregatGr,
            this.colAgrInputSourceGr,
            this.DistanceGr,
            this.FactSquareGr,
            this.FactSquareCalcGr});
            this.gvContentGrp.GridControl = this.gcContentGrp;
            this.gvContentGrp.Name = "gvContentGrp";
            this.gvContentGrp.OptionsView.EnableAppearanceEvenRow = true;
            this.gvContentGrp.OptionsView.EnableAppearanceOddRow = true;
            this.gvContentGrp.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvContentGrp_RowCellStyle);
            // 
            // FNameGr
            // 
            this.FNameGr.AppearanceHeader.Options.UseTextOptions = true;
            this.FNameGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FNameGr.Caption = "����";
            this.FNameGr.FieldName = "FName";
            this.FNameGr.Name = "FNameGr";
            this.FNameGr.OptionsColumn.AllowEdit = false;
            this.FNameGr.Visible = true;
            this.FNameGr.VisibleIndex = 0;
            this.FNameGr.Width = 112;
            // 
            // SquareGr
            // 
            this.SquareGr.AppearanceHeader.Options.UseTextOptions = true;
            this.SquareGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SquareGr.Caption = "�������, ��";
            this.SquareGr.FieldName = "FSquare";
            this.SquareGr.Name = "SquareGr";
            this.SquareGr.OptionsColumn.AllowEdit = false;
            this.SquareGr.Visible = true;
            this.SquareGr.VisibleIndex = 1;
            this.SquareGr.Width = 139;
            // 
            // Id_driverGr
            // 
            this.Id_driverGr.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_driverGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_driverGr.Caption = "��������";
            this.Id_driverGr.ColumnEdit = this.leDriver;
            this.Id_driverGr.FieldName = "Id_driver";
            this.Id_driverGr.Name = "Id_driverGr";
            this.Id_driverGr.OptionsColumn.AllowEdit = false;
            this.Id_driverGr.Visible = true;
            this.Id_driverGr.VisibleIndex = 2;
            this.Id_driverGr.Width = 195;
            // 
            // leDriver
            // 
            this.leDriver.AutoHeight = false;
            this.leDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leDriver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "��������")});
            this.leDriver.DisplayMember = "Family";
            this.leDriver.Name = "leDriver";
            this.leDriver.ValueMember = "id";
            // 
            // colDrvInputSourceGr
            // 
            this.colDrvInputSourceGr.Caption = "DrvInputSource";
            this.colDrvInputSourceGr.FieldName = "DrvInputSource";
            this.colDrvInputSourceGr.Name = "colDrvInputSourceGr";
            this.colDrvInputSourceGr.OptionsColumn.AllowEdit = false;
            this.colDrvInputSourceGr.OptionsColumn.ReadOnly = true;
            // 
            // Id_agregatGr
            // 
            this.Id_agregatGr.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_agregatGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_agregatGr.Caption = "�������";
            this.Id_agregatGr.ColumnEdit = this.leAgregat;
            this.Id_agregatGr.FieldName = "Id_agregat";
            this.Id_agregatGr.Name = "Id_agregatGr";
            this.Id_agregatGr.OptionsColumn.AllowEdit = false;
            this.Id_agregatGr.Visible = true;
            this.Id_agregatGr.VisibleIndex = 3;
            this.Id_agregatGr.Width = 165;
            // 
            // leAgregat
            // 
            this.leAgregat.AutoHeight = false;
            this.leAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leAgregat.DisplayMember = "Name";
            this.leAgregat.Name = "leAgregat";
            this.leAgregat.ValueMember = "Id";
            // 
            // colAgrInputSourceGr
            // 
            this.colAgrInputSourceGr.Caption = "AgrInputSource";
            this.colAgrInputSourceGr.FieldName = "AgrInputSource";
            this.colAgrInputSourceGr.Name = "colAgrInputSourceGr";
            this.colAgrInputSourceGr.OptionsColumn.AllowEdit = false;
            this.colAgrInputSourceGr.OptionsColumn.ReadOnly = true;
            // 
            // DistanceGr
            // 
            this.DistanceGr.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceGr.Caption = "���������� ���� , ��";
            this.DistanceGr.FieldName = "D";
            this.DistanceGr.Name = "DistanceGr";
            this.DistanceGr.OptionsColumn.AllowEdit = false;
            this.DistanceGr.Visible = true;
            this.DistanceGr.VisibleIndex = 4;
            this.DistanceGr.Width = 211;
            // 
            // FactSquareGr
            // 
            this.FactSquareGr.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareGr.Caption = "���������� (�� �������), �� ";
            this.FactSquareGr.FieldName = "FS";
            this.FactSquareGr.Name = "FactSquareGr";
            this.FactSquareGr.OptionsColumn.AllowEdit = false;
            this.FactSquareGr.Visible = true;
            this.FactSquareGr.VisibleIndex = 5;
            this.FactSquareGr.Width = 236;
            // 
            // FactSquareCalcGr
            // 
            this.FactSquareCalcGr.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCalcGr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCalcGr.Caption = "���������� (�� �������), ��";
            this.FactSquareCalcGr.FieldName = "FSC";
            this.FactSquareCalcGr.Name = "FactSquareCalcGr";
            this.FactSquareCalcGr.OptionsColumn.AllowEdit = false;
            this.FactSquareCalcGr.Visible = true;
            this.FactSquareCalcGr.VisibleIndex = 6;
            this.FactSquareCalcGr.Width = 192;
            // 
            // le
            // 
            this.le.AutoHeight = false;
            this.le.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.le.Name = "le";
            this.le.View = this.repositoryItemGridLookUpEdit5View;
            // 
            // repositoryItemGridLookUpEdit5View
            // 
            this.repositoryItemGridLookUpEdit5View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit5View.Name = "repositoryItemGridLookUpEdit5View";
            this.repositoryItemGridLookUpEdit5View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit5View.OptionsView.ShowGroupPanel = false;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gcContent);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(801, 306);
            this.xtraTabPage1.Text = "������";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(801, 306);
            this.xtraTabPage2.Text = "������� (���)";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gcFuelDRT);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(801, 306);
            this.xtraTabPage3.Text = "������� (���/CAN)";
            // 
            // gcFuelDRT
            // 
            this.gcFuelDRT.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "DetalTime";
            this.gcFuelDRT.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcFuelDRT.Location = new System.Drawing.Point(0, 0);
            this.gcFuelDRT.MainView = this.gvFuelDRT;
            this.gcFuelDRT.Name = "gcFuelDRT";
            this.gcFuelDRT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar3});
            this.gcFuelDRT.Size = new System.Drawing.Size(801, 306);
            this.gcFuelDRT.TabIndex = 32;
            this.gcFuelDRT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuelDRT,
            this.gridView4});
            // 
            // gvFuelDRT
            // 
            this.gvFuelDRT.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuelDRT.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuelDRT.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuelDRT.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuelDRT.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvFuelDRT.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvFuelDRT.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvFuelDRT.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuelDRT.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuelDRT.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvFuelDRT.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuelDRT.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuelDRT.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuelDRT.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuelDRT.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.OddRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvFuelDRT.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvFuelDRT.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvFuelDRT.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.Preview.Options.UseFont = true;
            this.gvFuelDRT.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuelDRT.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.Row.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.Row.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuelDRT.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvFuelDRT.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuelDRT.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuelDRT.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvFuelDRT.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvFuelDRT.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvFuelDRT.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvFuelDRT.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuelDRT.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuelDRT.ColumnPanelRowHeight = 40;
            this.gvFuelDRT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IDDRT,
            this.FieldDRT,
            this.FamilyDRT,
            this.TimeStartDRT,
            this.TimeEndDRT,
            this.DistanceDRT,
            this.Fuel_ExpensMoveDRT,
            this.Fuel_ExpensStopDRT,
            this.Fuel_ExpensTotalDRT,
            this.Fuel_ExpensAvgDRT,
            this.Fuel_ExpensAvgRateDRT});
            this.gvFuelDRT.GridControl = this.gcFuelDRT;
            this.gvFuelDRT.Name = "gvFuelDRT";
            this.gvFuelDRT.OptionsBehavior.Editable = false;
            this.gvFuelDRT.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuelDRT.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuelDRT.OptionsView.ShowFooter = true;
            this.gvFuelDRT.OptionsView.ShowGroupPanel = false;
            // 
            // IDDRT
            // 
            this.IDDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.IDDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IDDRT.Caption = "IdF";
            this.IDDRT.FieldName = "Id";
            this.IDDRT.Name = "IDDRT";
            this.IDDRT.OptionsColumn.AllowEdit = false;
            this.IDDRT.OptionsColumn.ReadOnly = true;
            // 
            // FieldDRT
            // 
            this.FieldDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.FieldDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FieldDRT.Caption = "����";
            this.FieldDRT.FieldName = "FName";
            this.FieldDRT.Name = "FieldDRT";
            this.FieldDRT.OptionsColumn.AllowEdit = false;
            this.FieldDRT.OptionsColumn.ReadOnly = true;
            this.FieldDRT.Visible = true;
            this.FieldDRT.VisibleIndex = 0;
            // 
            // FamilyDRT
            // 
            this.FamilyDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.FamilyDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FamilyDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FamilyDRT.Caption = "��������";
            this.FamilyDRT.FieldName = "Family";
            this.FamilyDRT.Name = "FamilyDRT";
            this.FamilyDRT.OptionsColumn.AllowEdit = false;
            this.FamilyDRT.OptionsColumn.ReadOnly = true;
            this.FamilyDRT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Family", "�����")});
            this.FamilyDRT.Visible = true;
            this.FamilyDRT.VisibleIndex = 1;
            // 
            // TimeStartDRT
            // 
            this.TimeStartDRT.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStartDRT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStartDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStartDRT.Caption = "�����";
            this.TimeStartDRT.DisplayFormat.FormatString = "t";
            this.TimeStartDRT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStartDRT.FieldName = "TimeStart";
            this.TimeStartDRT.Name = "TimeStartDRT";
            this.TimeStartDRT.OptionsColumn.AllowEdit = false;
            this.TimeStartDRT.OptionsColumn.ReadOnly = true;
            this.TimeStartDRT.Visible = true;
            this.TimeStartDRT.VisibleIndex = 2;
            // 
            // TimeEndDRT
            // 
            this.TimeEndDRT.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEndDRT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEndDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEndDRT.Caption = "�����";
            this.TimeEndDRT.DisplayFormat.FormatString = "t";
            this.TimeEndDRT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndDRT.FieldName = "TimeEnd";
            this.TimeEndDRT.Name = "TimeEndDRT";
            this.TimeEndDRT.OptionsColumn.AllowEdit = false;
            this.TimeEndDRT.OptionsColumn.ReadOnly = true;
            this.TimeEndDRT.Visible = true;
            this.TimeEndDRT.VisibleIndex = 3;
            // 
            // DistanceDRT
            // 
            this.DistanceDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DistanceDRT.Caption = "���������� ����, ��";
            this.DistanceDRT.FieldName = "Distance";
            this.DistanceDRT.Name = "DistanceDRT";
            this.DistanceDRT.OptionsColumn.AllowEdit = false;
            this.DistanceDRT.OptionsColumn.ReadOnly = true;
            this.DistanceDRT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:#.##}")});
            this.DistanceDRT.Visible = true;
            this.DistanceDRT.VisibleIndex = 4;
            // 
            // Fuel_ExpensMoveDRT
            // 
            this.Fuel_ExpensMoveDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMoveDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMoveDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMoveDRT.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMoveDRT.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMoveDRT.Name = "Fuel_ExpensMoveDRT";
            this.Fuel_ExpensMoveDRT.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensMoveDRT.OptionsColumn.ReadOnly = true;
            this.Fuel_ExpensMoveDRT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Fuel_ExpensMove", "{0:#.##}")});
            this.Fuel_ExpensMoveDRT.Visible = true;
            this.Fuel_ExpensMoveDRT.VisibleIndex = 5;
            this.Fuel_ExpensMoveDRT.Width = 106;
            // 
            // Fuel_ExpensStopDRT
            // 
            this.Fuel_ExpensStopDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStopDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStopDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStopDRT.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStopDRT.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStopDRT.Name = "Fuel_ExpensStopDRT";
            this.Fuel_ExpensStopDRT.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensStopDRT.OptionsColumn.ReadOnly = true;
            this.Fuel_ExpensStopDRT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Fuel_ExpensStop", "{0:#.##}")});
            this.Fuel_ExpensStopDRT.Visible = true;
            this.Fuel_ExpensStopDRT.VisibleIndex = 6;
            this.Fuel_ExpensStopDRT.Width = 103;
            // 
            // Fuel_ExpensTotalDRT
            // 
            this.Fuel_ExpensTotalDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotalDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotalDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotalDRT.Caption = "����� ������, �";
            this.Fuel_ExpensTotalDRT.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotalDRT.Name = "Fuel_ExpensTotalDRT";
            this.Fuel_ExpensTotalDRT.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensTotalDRT.OptionsColumn.ReadOnly = true;
            this.Fuel_ExpensTotalDRT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Fuel_ExpensTotal", "{0:#.##}")});
            this.Fuel_ExpensTotalDRT.Visible = true;
            this.Fuel_ExpensTotalDRT.VisibleIndex = 7;
            this.Fuel_ExpensTotalDRT.Width = 92;
            // 
            // Fuel_ExpensAvgDRT
            // 
            this.Fuel_ExpensAvgDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvgDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvgDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvgDRT.Caption = "������� ������, �/��";
            this.Fuel_ExpensAvgDRT.FieldName = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvgDRT.Name = "Fuel_ExpensAvgDRT";
            this.Fuel_ExpensAvgDRT.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensAvgDRT.OptionsColumn.ReadOnly = true;
            this.Fuel_ExpensAvgDRT.Visible = true;
            this.Fuel_ExpensAvgDRT.VisibleIndex = 8;
            this.Fuel_ExpensAvgDRT.Width = 89;
            // 
            // Fuel_ExpensAvgRateDRT
            // 
            this.Fuel_ExpensAvgRateDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvgRateDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvgRateDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvgRateDRT.Caption = "������� ������, �/�������";
            this.Fuel_ExpensAvgRateDRT.FieldName = "Fuel_ExpensAvgRate";
            this.Fuel_ExpensAvgRateDRT.Name = "Fuel_ExpensAvgRateDRT";
            this.Fuel_ExpensAvgRateDRT.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensAvgRateDRT.OptionsColumn.ReadOnly = true;
            this.Fuel_ExpensAvgRateDRT.Visible = true;
            this.Fuel_ExpensAvgRateDRT.VisibleIndex = 9;
            this.Fuel_ExpensAvgRateDRT.Width = 129;
            // 
            // repositoryItemProgressBar3
            // 
            this.repositoryItemProgressBar3.Appearance.BackColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar3.Name = "repositoryItemProgressBar3";
            this.repositoryItemProgressBar3.ShowTitle = true;
            this.repositoryItemProgressBar3.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            // 
            // gridView4
            // 
            this.gridView4.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView4.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView4.ColumnPanelRowHeight = 40;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28});
            this.gridView4.GridControl = this.gcFuelDRT;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.Editable = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "IdD";
            this.gridColumn15.FieldName = "Id";
            this.gridColumn15.Name = "gridColumn15";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Id_node";
            this.gridColumn16.FieldName = "Id_node";
            this.gridColumn16.Name = "gridColumn16";
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "�����";
            this.gridColumn17.DisplayFormat.FormatString = "t";
            this.gridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn17.FieldName = "TimeStart";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 61;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "�����";
            this.gridColumn18.DisplayFormat.FormatString = "t";
            this.gridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn18.FieldName = "TimeEnd";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 70;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn19.Caption = "��������";
            this.gridColumn19.FieldName = "TimeRate";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            this.gridColumn19.Width = 78;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn20.Caption = "���������, ��";
            this.gridColumn20.FieldName = "FactSquare";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            this.gridColumn20.Width = 106;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "����������, �";
            this.gridColumn22.FieldName = "FuelAdd";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 4;
            this.gridColumn22.Width = 104;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn23.Caption = "�����, �";
            this.gridColumn23.FieldName = "FuelSub";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 5;
            this.gridColumn23.Width = 76;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn24.Caption = "������� � �����, �";
            this.gridColumn24.FieldName = "FuelEnd";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 6;
            this.gridColumn24.Width = 110;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn25.Caption = "����� ������, �";
            this.gridColumn25.FieldName = "FuelExpens";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 7;
            this.gridColumn25.Width = 108;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn26.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn26.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn26.Caption = "������ ������� � ��������, �";
            this.gridColumn26.FieldName = "Fuel_ExpensMove";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 8;
            this.gridColumn26.Width = 113;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn27.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn27.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn27.Caption = "������ ������� �� ��������, �";
            this.gridColumn27.FieldName = "Fuel_ExpensStop";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 9;
            this.gridColumn27.Width = 118;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn28.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn28.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn28.Caption = "����� ������, �";
            this.gridColumn28.FieldName = "Fuel_ExpensTotal";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 10;
            this.gridColumn28.Width = 119;
            // 
            // xtraTabMap
            // 
            this.xtraTabMap.Controls.Add(this.pnMap);
            this.xtraTabMap.Name = "xtraTabMap";
            this.xtraTabMap.Size = new System.Drawing.Size(801, 306);
            this.xtraTabMap.Text = "����� ������";
            // 
            // pnMap
            // 
            this.pnMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMap.Location = new System.Drawing.Point(0, 0);
            this.pnMap.Name = "pnMap";
            this.pnMap.Size = new System.Drawing.Size(801, 306);
            this.pnMap.TabIndex = 1;
            this.pnMap.TabStop = true;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.scControl);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(801, 306);
            this.xtraTabPage5.Text = "����������";
            // 
            // scControl
            // 
            this.scControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scControl.Location = new System.Drawing.Point(0, 0);
            this.scControl.Name = "scControl";
            this.scControl.Panel1.Controls.Add(this.gcControlDrv);
            this.scControl.Panel1.Text = "Panel1";
            this.scControl.Panel2.Controls.Add(this.scControlInner);
            this.scControl.Panel2.Text = "Panel2";
            this.scControl.Size = new System.Drawing.Size(801, 306);
            this.scControl.SplitterPosition = 310;
            this.scControl.TabIndex = 47;
            this.scControl.Text = "splitContainerControl1";
            // 
            // gcControlDrv
            // 
            this.gcControlDrv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcControlDrv.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcControlDrv_EmbeddedNavigator_ButtonClick);
            this.gcControlDrv.Location = new System.Drawing.Point(0, 0);
            this.gcControlDrv.MainView = this.gvControlDrv;
            this.gcControlDrv.Name = "gcControlDrv";
            this.gcControlDrv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.DriverLookUp,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemGridLookUpEdit4,
            this.repositoryItemLookUpEdit6,
            this.repositoryItemTimeEdit3,
            this.txEdit});
            this.gcControlDrv.Size = new System.Drawing.Size(310, 306);
            this.gcControlDrv.TabIndex = 44;
            this.gcControlDrv.UseEmbeddedNavigator = true;
            this.gcControlDrv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvControlDrv});
            // 
            // gvControlDrv
            // 
            this.gvControlDrv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.Empty.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlDrv.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlDrv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlDrv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlDrv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvControlDrv.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvControlDrv.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvControlDrv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlDrv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlDrv.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvControlDrv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlDrv.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlDrv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlDrv.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlDrv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.OddRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.OddRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvControlDrv.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvControlDrv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvControlDrv.Appearance.Preview.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.Preview.Options.UseFont = true;
            this.gvControlDrv.Appearance.Preview.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlDrv.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.Row.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.Row.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlDrv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvControlDrv.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlDrv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlDrv.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvControlDrv.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvControlDrv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvControlDrv.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvControlDrv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlDrv.Appearance.VertLine.Options.UseBackColor = true;
            this.gvControlDrv.ColumnPanelRowHeight = 40;
            this.gvControlDrv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIddrv,
            this.Id_object1,
            this.colEnterDrv,
            this.colExitDrv,
            this.colTimeDrv});
            this.gvControlDrv.GridControl = this.gcControlDrv;
            this.gvControlDrv.Name = "gvControlDrv";
            this.gvControlDrv.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvControlDrv.OptionsView.EnableAppearanceEvenRow = true;
            this.gvControlDrv.OptionsView.EnableAppearanceOddRow = true;
            this.gvControlDrv.OptionsView.ShowGroupPanel = false;
            this.gvControlDrv.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gvControlDrv.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvControlDrv_InitNewRow);
            this.gvControlDrv.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvControlDrv_CellValueChanged);
            this.gvControlDrv.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvControlDrv_InvalidRowException);
            this.gvControlDrv.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvControlDrv_ValidateRow);
            // 
            // colIddrv
            // 
            this.colIddrv.Caption = "Id";
            this.colIddrv.CustomizationCaption = "Id";
            this.colIddrv.FieldName = "Id";
            this.colIddrv.Name = "colIddrv";
            // 
            // Id_object1
            // 
            this.Id_object1.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_object1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_object1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_object1.Caption = "��������";
            this.Id_object1.ColumnEdit = this.DriverLookUp;
            this.Id_object1.FieldName = "Id_object";
            this.Id_object1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.Id_object1.Name = "Id_object1";
            this.Id_object1.Visible = true;
            this.Id_object1.VisibleIndex = 0;
            // 
            // DriverLookUp
            // 
            this.DriverLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DriverLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.DriverLookUp.DisplayMember = "Family";
            this.DriverLookUp.DropDownRows = 10;
            this.DriverLookUp.Name = "DriverLookUp";
            this.DriverLookUp.NullText = "������� ��������";
            this.DriverLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.DriverLookUp.ValueMember = "id";
            // 
            // colEnterDrv
            // 
            this.colEnterDrv.AppearanceCell.Options.UseTextOptions = true;
            this.colEnterDrv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnterDrv.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnterDrv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnterDrv.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEnterDrv.Caption = "�����";
            this.colEnterDrv.ColumnEdit = this.txEdit;
            this.colEnterDrv.DisplayFormat.FormatString = "g";
            this.colEnterDrv.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEnterDrv.FieldName = "TimeStart";
            this.colEnterDrv.Name = "colEnterDrv";
            this.colEnterDrv.Visible = true;
            this.colEnterDrv.VisibleIndex = 1;
            // 
            // txEdit
            // 
            this.txEdit.AutoHeight = false;
            this.txEdit.DisplayFormat.FormatString = "g";
            this.txEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEdit.EditFormat.FormatString = "g";
            this.txEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEdit.Mask.EditMask = "dd.MM.yyyy HH:mm";
            this.txEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txEdit.Name = "txEdit";
            // 
            // colExitDrv
            // 
            this.colExitDrv.AppearanceCell.Options.UseTextOptions = true;
            this.colExitDrv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExitDrv.AppearanceHeader.Options.UseTextOptions = true;
            this.colExitDrv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExitDrv.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExitDrv.Caption = "�����";
            this.colExitDrv.ColumnEdit = this.txEdit;
            this.colExitDrv.DisplayFormat.FormatString = "g";
            this.colExitDrv.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExitDrv.FieldName = "TimeEnd";
            this.colExitDrv.Name = "colExitDrv";
            this.colExitDrv.Visible = true;
            this.colExitDrv.VisibleIndex = 2;
            // 
            // colTimeDrv
            // 
            this.colTimeDrv.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeDrv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDrv.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeDrv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDrv.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeDrv.Caption = "�����";
            this.colTimeDrv.DisplayFormat.FormatString = "t";
            this.colTimeDrv.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeDrv.FieldName = "Time";
            this.colTimeDrv.Name = "colTimeDrv";
            this.colTimeDrv.OptionsColumn.AllowEdit = false;
            this.colTimeDrv.Visible = true;
            this.colTimeDrv.VisibleIndex = 3;
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.DisplayMember = "Family";
            this.repositoryItemGridLookUpEdit3.ImmediatePopup = true;
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.ValueMember = "id";
            this.repositoryItemGridLookUpEdit3.View = this.gridView5;
            // 
            // gridView5
            // 
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemGridLookUpEdit4
            // 
            this.repositoryItemGridLookUpEdit4.AutoHeight = false;
            this.repositoryItemGridLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit4.Name = "repositoryItemGridLookUpEdit4";
            this.repositoryItemGridLookUpEdit4.View = this.gridView10;
            // 
            // gridView10
            // 
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            // 
            // repositoryItemTimeEdit3
            // 
            this.repositoryItemTimeEdit3.AutoHeight = false;
            this.repositoryItemTimeEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit3.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemTimeEdit3.Name = "repositoryItemTimeEdit3";
            this.repositoryItemTimeEdit3.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // scControlInner
            // 
            this.scControlInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scControlInner.Location = new System.Drawing.Point(0, 0);
            this.scControlInner.Name = "scControlInner";
            this.scControlInner.Panel1.Controls.Add(this.gcControlAgr);
            this.scControlInner.Panel1.Text = "Panel1";
            this.scControlInner.Panel2.Controls.Add(this.gcControlField);
            this.scControlInner.Panel2.Text = "Panel2";
            this.scControlInner.Size = new System.Drawing.Size(486, 306);
            this.scControlInner.SplitterPosition = 317;
            this.scControlInner.TabIndex = 0;
            this.scControlInner.Text = "splitContainerControl2";
            // 
            // gcControlAgr
            // 
            this.gcControlAgr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcControlAgr.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcControlAgr_EmbeddedNavigator_ButtonClick);
            this.gcControlAgr.Location = new System.Drawing.Point(0, 0);
            this.gcControlAgr.MainView = this.gvControlAgr;
            this.gcControlAgr.Name = "gcControlAgr";
            this.gcControlAgr.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.AgrLookUp,
            this.txEditAgr});
            this.gcControlAgr.Size = new System.Drawing.Size(317, 306);
            this.gcControlAgr.TabIndex = 45;
            this.gcControlAgr.UseEmbeddedNavigator = true;
            this.gcControlAgr.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvControlAgr});
            // 
            // gvControlAgr
            // 
            this.gvControlAgr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.Empty.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlAgr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlAgr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlAgr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlAgr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvControlAgr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvControlAgr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvControlAgr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlAgr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlAgr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvControlAgr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlAgr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlAgr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlAgr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlAgr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvControlAgr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvControlAgr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvControlAgr.Appearance.Preview.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.Preview.Options.UseFont = true;
            this.gvControlAgr.Appearance.Preview.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlAgr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.Row.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.Row.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlAgr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvControlAgr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlAgr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlAgr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvControlAgr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvControlAgr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvControlAgr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvControlAgr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlAgr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvControlAgr.ColumnPanelRowHeight = 40;
            this.gvControlAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdAgr,
            this.colAgregat,
            this.colEnterAgr,
            this.colExitAgr,
            this.colTimeAgr});
            this.gvControlAgr.GridControl = this.gcControlAgr;
            this.gvControlAgr.Name = "gvControlAgr";
            this.gvControlAgr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvControlAgr.OptionsView.EnableAppearanceOddRow = true;
            this.gvControlAgr.OptionsView.ShowGroupPanel = false;
            this.gvControlAgr.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gvControlAgr.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvControlAgr_InitNewRow);
            this.gvControlAgr.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvControlAgr_CellValueChanged);
            this.gvControlAgr.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvControlAgr_InvalidRowException);
            this.gvControlAgr.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvControlAgr_ValidateRow);
            // 
            // colIdAgr
            // 
            this.colIdAgr.Caption = "Id";
            this.colIdAgr.CustomizationCaption = "Id";
            this.colIdAgr.FieldName = "Id";
            this.colIdAgr.Name = "colIdAgr";
            // 
            // colAgregat
            // 
            this.colAgregat.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregat.Caption = "�������";
            this.colAgregat.ColumnEdit = this.AgrLookUp;
            this.colAgregat.FieldName = "Id_object";
            this.colAgregat.Name = "colAgregat";
            this.colAgregat.Visible = true;
            this.colAgregat.VisibleIndex = 0;
            // 
            // AgrLookUp
            // 
            this.AgrLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AgrLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "�������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.AgrLookUp.DisplayMember = "Name";
            this.AgrLookUp.Name = "AgrLookUp";
            this.AgrLookUp.NullText = "������� �������";
            this.AgrLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.AgrLookUp.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.AgrLookUp.ValueMember = "Id";
            // 
            // colEnterAgr
            // 
            this.colEnterAgr.AppearanceCell.Options.UseTextOptions = true;
            this.colEnterAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnterAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnterAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnterAgr.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEnterAgr.Caption = "�����";
            this.colEnterAgr.ColumnEdit = this.txEditAgr;
            this.colEnterAgr.DisplayFormat.FormatString = "g";
            this.colEnterAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEnterAgr.FieldName = "TimeStart";
            this.colEnterAgr.Name = "colEnterAgr";
            this.colEnterAgr.Visible = true;
            this.colEnterAgr.VisibleIndex = 1;
            // 
            // txEditAgr
            // 
            this.txEditAgr.AutoHeight = false;
            this.txEditAgr.DisplayFormat.FormatString = "g";
            this.txEditAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEditAgr.EditFormat.FormatString = "g";
            this.txEditAgr.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEditAgr.Mask.EditMask = "dd.MM.yyyy HH:mm";
            this.txEditAgr.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txEditAgr.Name = "txEditAgr";
            // 
            // colExitAgr
            // 
            this.colExitAgr.AppearanceCell.Options.UseTextOptions = true;
            this.colExitAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExitAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.colExitAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExitAgr.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExitAgr.Caption = "�����";
            this.colExitAgr.ColumnEdit = this.txEditAgr;
            this.colExitAgr.DisplayFormat.FormatString = "g";
            this.colExitAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExitAgr.FieldName = "TimeEnd";
            this.colExitAgr.Name = "colExitAgr";
            this.colExitAgr.Visible = true;
            this.colExitAgr.VisibleIndex = 2;
            // 
            // colTimeAgr
            // 
            this.colTimeAgr.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeAgr.Caption = "�����";
            this.colTimeAgr.DisplayFormat.FormatString = "t";
            this.colTimeAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeAgr.FieldName = "Time";
            this.colTimeAgr.Name = "colTimeAgr";
            this.colTimeAgr.OptionsColumn.AllowEdit = false;
            this.colTimeAgr.Visible = true;
            this.colTimeAgr.VisibleIndex = 3;
            // 
            // gcControlField
            // 
            this.gcControlField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcControlField.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcControlField_EmbeddedNavigator_ButtonClick);
            this.gcControlField.Location = new System.Drawing.Point(0, 0);
            this.gcControlField.MainView = this.gvControlField;
            this.gcControlField.Name = "gcControlField";
            this.gcControlField.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.FldLookUp,
            this.txEditFld,
            this.tmeTime,
            this.repositoryItemDateEdit1});
            this.gcControlField.Size = new System.Drawing.Size(164, 306);
            this.gcControlField.TabIndex = 46;
            this.gcControlField.UseEmbeddedNavigator = true;
            this.gcControlField.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvControlField});
            // 
            // gvControlField
            // 
            this.gvControlField.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvControlField.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvControlField.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvControlField.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlField.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlField.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvControlField.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvControlField.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvControlField.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvControlField.Appearance.Empty.Options.UseBackColor = true;
            this.gvControlField.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlField.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvControlField.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlField.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlField.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvControlField.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvControlField.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvControlField.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlField.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvControlField.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvControlField.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvControlField.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvControlField.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvControlField.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvControlField.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvControlField.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvControlField.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvControlField.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvControlField.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvControlField.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvControlField.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlField.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvControlField.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvControlField.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvControlField.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvControlField.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvControlField.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvControlField.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvControlField.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvControlField.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvControlField.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvControlField.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvControlField.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvControlField.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvControlField.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlField.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlField.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvControlField.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlField.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlField.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.OddRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.OddRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvControlField.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvControlField.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvControlField.Appearance.Preview.Options.UseBackColor = true;
            this.gvControlField.Appearance.Preview.Options.UseFont = true;
            this.gvControlField.Appearance.Preview.Options.UseForeColor = true;
            this.gvControlField.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvControlField.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.Row.Options.UseBackColor = true;
            this.gvControlField.Appearance.Row.Options.UseForeColor = true;
            this.gvControlField.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvControlField.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvControlField.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvControlField.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvControlField.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvControlField.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvControlField.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvControlField.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvControlField.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvControlField.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvControlField.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvControlField.Appearance.VertLine.Options.UseBackColor = true;
            this.gvControlField.ColumnPanelRowHeight = 40;
            this.gvControlField.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colField,
            this.colEnter,
            this.colExit,
            this.colTime});
            this.gvControlField.GridControl = this.gcControlField;
            this.gvControlField.Name = "gvControlField";
            this.gvControlField.OptionsView.EnableAppearanceEvenRow = true;
            this.gvControlField.OptionsView.EnableAppearanceOddRow = true;
            this.gvControlField.OptionsView.ShowGroupPanel = false;
            this.gvControlField.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gvControlField.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvControlField_InitNewRow);
            this.gvControlField.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvControlField_CellValueChanged);
            this.gvControlField.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvControlField_InvalidRowException);
            this.gvControlField.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvControlField_ValidateRow);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.CustomizationCaption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colField
            // 
            this.colField.AppearanceHeader.Options.UseTextOptions = true;
            this.colField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colField.Caption = "����";
            this.colField.ColumnEdit = this.FldLookUp;
            this.colField.FieldName = "Id_object";
            this.colField.Name = "colField";
            this.colField.Visible = true;
            this.colField.VisibleIndex = 0;
            // 
            // FldLookUp
            // 
            this.FldLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FldLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.FldLookUp.DisplayMember = "Name";
            this.FldLookUp.DropDownRows = 10;
            this.FldLookUp.Name = "FldLookUp";
            this.FldLookUp.NullText = "������� ����";
            this.FldLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.FldLookUp.ValueMember = "Id";
            // 
            // colEnter
            // 
            this.colEnter.AppearanceCell.Options.UseTextOptions = true;
            this.colEnter.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnter.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEnter.Caption = "�����";
            this.colEnter.ColumnEdit = this.txEditFld;
            this.colEnter.DisplayFormat.FormatString = "g";
            this.colEnter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEnter.FieldName = "TimeStart";
            this.colEnter.Name = "colEnter";
            this.colEnter.Visible = true;
            this.colEnter.VisibleIndex = 1;
            // 
            // txEditFld
            // 
            this.txEditFld.AutoHeight = false;
            this.txEditFld.DisplayFormat.FormatString = "g";
            this.txEditFld.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEditFld.EditFormat.FormatString = "g";
            this.txEditFld.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txEditFld.Mask.EditMask = "dd.MM.yyyy HH:mm";
            this.txEditFld.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txEditFld.Name = "txEditFld";
            this.txEditFld.ValidateOnEnterKey = true;
            // 
            // colExit
            // 
            this.colExit.AppearanceCell.Options.UseTextOptions = true;
            this.colExit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExit.AppearanceHeader.Options.UseTextOptions = true;
            this.colExit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExit.Caption = "�����";
            this.colExit.ColumnEdit = this.txEditFld;
            this.colExit.DisplayFormat.FormatString = "g";
            this.colExit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExit.FieldName = "TimeEnd";
            this.colExit.Name = "colExit";
            this.colExit.Visible = true;
            this.colExit.VisibleIndex = 2;
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.Caption = "�����";
            this.colTime.DisplayFormat.FormatString = "t";
            this.colTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTime.FieldName = "Time";
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.AllowEdit = false;
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 3;
            // 
            // tmeTime
            // 
            this.tmeTime.AutoHeight = false;
            this.tmeTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tmeTime.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.tmeTime.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.tmeTime.Mask.EditMask = "D";
            this.tmeTime.Name = "tmeTime";
            this.tmeTime.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit1.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Appearance.BackColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            this.repositoryItemProgressBar2.ShowTitle = true;
            this.repositoryItemProgressBar2.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            // 
            // gridView3
            // 
            this.gridView3.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.ColumnPanelRowHeight = 40;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.Editable = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "IdD";
            this.gridColumn1.FieldName = "Id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Id_node";
            this.gridColumn2.FieldName = "Id_node";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "�����";
            this.gridColumn3.DisplayFormat.FormatString = "t";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn3.FieldName = "TimeStart";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 61;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "�����";
            this.gridColumn4.DisplayFormat.FormatString = "t";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn4.FieldName = "TimeEnd";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 70;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "��������";
            this.gridColumn5.FieldName = "TimeRate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 78;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "���������, ��";
            this.gridColumn6.FieldName = "FactSquare";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 106;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "����������, �";
            this.gridColumn8.FieldName = "FuelAdd";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 104;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "�����, �";
            this.gridColumn9.FieldName = "FuelSub";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 76;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "������� � �����, �";
            this.gridColumn10.FieldName = "FuelEnd";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 7;
            this.gridColumn10.Width = 110;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "����� ������, �";
            this.gridColumn11.FieldName = "FuelExpens";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 8;
            this.gridColumn11.Width = 108;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn12.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn12.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "������ ������� � ��������, �";
            this.gridColumn12.FieldName = "Fuel_ExpensMove";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 9;
            this.gridColumn12.Width = 113;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn13.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn13.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "������ ������� �� ��������, �";
            this.gridColumn13.FieldName = "Fuel_ExpensStop";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 10;
            this.gridColumn13.Width = 118;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn14.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gridColumn14.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn14.Caption = "����� ������, �";
            this.gridColumn14.FieldName = "Fuel_ExpensTotal";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 11;
            this.gridColumn14.Width = 119;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Name = "gridColumn40";
            // 
            // gridColumn41
            // 
            this.gridColumn41.Name = "gridColumn41";
            // 
            // gridColumn42
            // 
            this.gridColumn42.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn42.Caption = "�����";
            this.gridColumn42.DisplayFormat.FormatString = "t";
            this.gridColumn42.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn42.FieldName = "TimeStart";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 1;
            // 
            // gridColumn43
            // 
            this.gridColumn43.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn43.Caption = "�����";
            this.gridColumn43.DisplayFormat.FormatString = "t";
            this.gridColumn43.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn43.FieldName = "TimeEnd";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 2;
            // 
            // gridColumn44
            // 
            this.gridColumn44.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.Caption = "�����";
            this.gridColumn44.DisplayFormat.FormatString = "t";
            this.gridColumn44.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn44.FieldName = "Time";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 3;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Name = "gridColumn45";
            // 
            // gridColumn46
            // 
            this.gridColumn46.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn46.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn46.Caption = "�����";
            this.gridColumn46.DisplayFormat.FormatString = "t";
            this.gridColumn46.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn46.FieldName = "TimeStart";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 1;
            // 
            // gridColumn47
            // 
            this.gridColumn47.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn47.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn47.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn47.Caption = "�����";
            this.gridColumn47.DisplayFormat.FormatString = "t";
            this.gridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn47.FieldName = "TimeEnd";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 2;
            // 
            // gridColumn48
            // 
            this.gridColumn48.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn48.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn48.Caption = "�����";
            this.gridColumn48.DisplayFormat.FormatString = "t";
            this.gridColumn48.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn48.FieldName = "Time";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 3;
            // 
            // gridColumn49
            // 
            this.gridColumn49.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn49.Caption = "�����";
            this.gridColumn49.DisplayFormat.FormatString = "t";
            this.gridColumn49.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn49.FieldName = "TimeStart";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 1;
            // 
            // gridColumn50
            // 
            this.gridColumn50.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn50.Caption = "�����";
            this.gridColumn50.DisplayFormat.FormatString = "t";
            this.gridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn50.FieldName = "TimeEnd";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 2;
            // 
            // gridColumn51
            // 
            this.gridColumn51.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn51.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn51.Caption = "�����";
            this.gridColumn51.DisplayFormat.FormatString = "t";
            this.gridColumn51.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn51.FieldName = "Time";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 3;
            // 
            // DrvLookUp
            // 
            this.DrvLookUp.AutoHeight = false;
            this.DrvLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DrvLookUp.DisplayMember = "Family";
            this.DrvLookUp.ImmediatePopup = true;
            this.DrvLookUp.Name = "DrvLookUp";
            this.DrvLookUp.ValueMember = "id";
            this.DrvLookUp.View = this.gridView6;
            // 
            // gridView6
            // 
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.View = this.gridView8;
            // 
            // gridView8
            // 
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // repositoryItemTimeEdit2
            // 
            this.repositoryItemTimeEdit2.AutoHeight = false;
            this.repositoryItemTimeEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit2.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemTimeEdit2.Name = "repositoryItemTimeEdit2";
            this.repositoryItemTimeEdit2.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // pbValidity
            // 
            this.pbValidity.Name = "pbValidity";
            this.pbValidity.ShowTitle = true;
            // 
            // repositoryItemProgressBar4
            // 
            this.repositoryItemProgressBar4.Name = "repositoryItemProgressBar4";
            this.repositoryItemProgressBar4.ShowTitle = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pgOrder);
            this.panelControl2.Location = new System.Drawing.Point(388, 32);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(431, 120);
            this.panelControl2.TabIndex = 72;
            // 
            // pgOrder
            // 
            this.pgOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgOrder.Location = new System.Drawing.Point(2, 2);
            this.pgOrder.Name = "pgOrder";
            this.pgOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.pgOrder.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crTotalc,
            this.crTotalPath,
            this.crTotalTime,
            this.crDUT,
            this.erExpensSquareDUT,
            this.erExpensSquareAvgDUT,
            this.erExpensWithoutWorkDUT,
            this.erExpensAvgWithoutWorkDUT,
            this.crDRT,
            this.erExpensSquareDRT,
            this.erExpensSquareAvgDRT,
            this.erExpensWithoutWorkDRT,
            this.erExpensAvgWithoutWorkDRT,
            this.ValidityData});
            this.pgOrder.Size = new System.Drawing.Size(427, 116);
            this.pgOrder.TabIndex = 0;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // crTotalc
            // 
            this.crTotalc.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erNumber,
            this.erMobitel,
            this.erDate,
            this.erDateLastRecalc,
            this.erSquare,
            this.erSquareOverlap,
            this.errRemark,
            this.erParth});
            this.crTotalc.Height = 19;
            this.crTotalc.Name = "crTotalc";
            this.crTotalc.Properties.Caption = "����� ����������";
            // 
            // erNumber
            // 
            this.erNumber.Height = 16;
            this.erNumber.Name = "erNumber";
            this.erNumber.Properties.Caption = "�����";
            this.erNumber.Properties.FieldName = "ID";
            this.erNumber.Properties.ReadOnly = true;
            // 
            // erMobitel
            // 
            this.erMobitel.Height = 16;
            this.erMobitel.Name = "erMobitel";
            this.erMobitel.Properties.Caption = "������";
            this.erMobitel.Properties.FieldName = "MobitelName";
            this.erMobitel.Properties.ReadOnly = true;
            // 
            // erDate
            // 
            this.erDate.Height = 16;
            this.erDate.Name = "erDate";
            this.erDate.Properties.Caption = "����";
            this.erDate.Properties.FieldName = "InforCreate";
            this.erDate.Properties.ReadOnly = true;
            // 
            // erDateLastRecalc
            // 
            this.erDateLastRecalc.Height = 16;
            this.erDateLastRecalc.Name = "erDateLastRecalc";
            this.erDateLastRecalc.Properties.Caption = "���� ���������� ��������� ������";
            this.erDateLastRecalc.Properties.FieldName = "InforRecalc";
            this.erDateLastRecalc.Properties.ReadOnly = true;
            // 
            // erSquare
            // 
            this.erSquare.Appearance.ForeColor = System.Drawing.Color.Green;
            this.erSquare.Appearance.Options.UseForeColor = true;
            this.erSquare.Appearance.Options.UseTextOptions = true;
            this.erSquare.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.erSquare.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.erSquare.Name = "erSquare";
            this.erSquare.Properties.Caption = "���������� (�� �������) ��������, ��";
            this.erSquare.Properties.FieldName = "SquareWorkDescript";
            // 
            // erSquareOverlap
            // 
            this.erSquareOverlap.Appearance.ForeColor = System.Drawing.Color.Green;
            this.erSquareOverlap.Appearance.Options.UseForeColor = true;
            this.erSquareOverlap.Appearance.Options.UseTextOptions = true;
            this.erSquareOverlap.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.erSquareOverlap.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.erSquareOverlap.Name = "erSquareOverlap";
            this.erSquareOverlap.Properties.Caption = "���������� (�� �������)  �����, ��";
            this.erSquareOverlap.Properties.FieldName = "SquareWorkDescriptOverlap";
            // 
            // errRemark
            // 
            this.errRemark.Name = "errRemark";
            this.errRemark.Properties.Caption = "����������";
            this.errRemark.Properties.FieldName = "Remark";
            this.errRemark.Properties.ReadOnly = true;
            // 
            // erParth
            // 
            this.erParth.Name = "erParth";
            this.erParth.Properties.Caption = "��������, ��";
            this.erParth.Properties.FieldName = "PathWithoutWork";
            this.erParth.Properties.ReadOnly = true;
            // 
            // crTotalPath
            // 
            this.crTotalPath.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erPlaceStart,
            this.erPlaceEnd,
            this.erSpeed,
            this.erPath});
            this.crTotalPath.Name = "crTotalPath";
            this.crTotalPath.Properties.Caption = "��������� ��������";
            // 
            // erPlaceStart
            // 
            this.erPlaceStart.Height = 16;
            this.erPlaceStart.Name = "erPlaceStart";
            this.erPlaceStart.Properties.Caption = "����� ������ ��������";
            this.erPlaceStart.Properties.FieldName = "LocationStart";
            this.erPlaceStart.Properties.ReadOnly = true;
            // 
            // erPlaceEnd
            // 
            this.erPlaceEnd.Name = "erPlaceEnd";
            this.erPlaceEnd.Properties.Caption = "����� ��������� ��������";
            this.erPlaceEnd.Properties.FieldName = "LocationEnd";
            this.erPlaceEnd.Properties.ReadOnly = true;
            // 
            // erSpeed
            // 
            this.erSpeed.Name = "erSpeed";
            this.erSpeed.Properties.Caption = "������� �������� ��/�";
            this.erSpeed.Properties.FieldName = "SpeedAvg";
            this.erSpeed.Properties.ReadOnly = true;
            // 
            // erPath
            // 
            this.erPath.Name = "erPath";
            this.erPath.Properties.Caption = "���������� ����, ��";
            this.erPath.Properties.FieldName = "Distance";
            this.erPath.Properties.ReadOnly = true;
            // 
            // crTotalTime
            // 
            this.crTotalTime.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erDateStart,
            this.erDateEnd,
            this.erTimeWork,
            this.erTimeMove,
            this.erTimeStop});
            this.crTotalTime.Expanded = false;
            this.crTotalTime.Height = 19;
            this.crTotalTime.Name = "crTotalTime";
            this.crTotalTime.Properties.Caption = "��������� �������";
            // 
            // erDateStart
            // 
            this.erDateStart.Name = "erDateStart";
            this.erDateStart.Properties.Caption = "����� ������ ��������";
            this.erDateStart.Properties.FieldName = "TimeStart";
            this.erDateStart.Properties.ReadOnly = true;
            // 
            // erDateEnd
            // 
            this.erDateEnd.Name = "erDateEnd";
            this.erDateEnd.Properties.Caption = "����� ��������� ��������";
            this.erDateEnd.Properties.FieldName = "TimeEnd";
            this.erDateEnd.Properties.ReadOnly = true;
            // 
            // erTimeWork
            // 
            this.erTimeWork.Name = "erTimeWork";
            this.erTimeWork.Properties.Caption = "����������������� �����,�";
            this.erTimeWork.Properties.FieldName = "TimeWork";
            this.erTimeWork.Properties.ReadOnly = true;
            // 
            // erTimeMove
            // 
            this.erTimeMove.Name = "erTimeMove";
            this.erTimeMove.Properties.Caption = "����� ����� ��������, �";
            this.erTimeMove.Properties.FieldName = "TimeMove";
            this.erTimeMove.Properties.ReadOnly = true;
            // 
            // erTimeStop
            // 
            this.erTimeStop.Name = "erTimeStop";
            this.erTimeStop.Properties.Caption = "����� ����� �������, �";
            this.erTimeStop.Properties.FieldName = "TimeStop";
            this.erTimeStop.Properties.ReadOnly = true;
            // 
            // crDUT
            // 
            this.crDUT.Name = "crDUT";
            this.crDUT.Properties.Caption = "�������, ���";
            // 
            // erExpensSquareDUT
            // 
            this.erExpensSquareDUT.Name = "erExpensSquareDUT";
            this.erExpensSquareDUT.Properties.Caption = "������ � �����,  �����";
            this.erExpensSquareDUT.Properties.FieldName = "FuelDUTExpensSquare";
            this.erExpensSquareDUT.Properties.ReadOnly = true;
            // 
            // erExpensSquareAvgDUT
            // 
            this.erExpensSquareAvgDUT.Name = "erExpensSquareAvgDUT";
            this.erExpensSquareAvgDUT.Properties.Caption = "������ � �����, �/��";
            this.erExpensSquareAvgDUT.Properties.FieldName = "FuelDUTExpensAvgSquare";
            this.erExpensSquareAvgDUT.Properties.ReadOnly = true;
            // 
            // erExpensWithoutWorkDUT
            // 
            this.erExpensWithoutWorkDUT.Name = "erExpensWithoutWorkDUT";
            this.erExpensWithoutWorkDUT.Properties.Caption = "������ �� ���������, �����";
            this.erExpensWithoutWorkDUT.Properties.FieldName = "FuelDUTExpensWithoutWork";
            this.erExpensWithoutWorkDUT.Properties.ReadOnly = true;
            // 
            // erExpensAvgWithoutWorkDUT
            // 
            this.erExpensAvgWithoutWorkDUT.Name = "erExpensAvgWithoutWorkDUT";
            this.erExpensAvgWithoutWorkDUT.Properties.Caption = "������ �� ���������, �/100��";
            this.erExpensAvgWithoutWorkDUT.Properties.FieldName = "FuelDUTExpensAvgWithoutWork";
            this.erExpensAvgWithoutWorkDUT.Properties.ReadOnly = true;
            // 
            // crDRT
            // 
            this.crDRT.Name = "crDRT";
            this.crDRT.Properties.Caption = "�������, ���";
            // 
            // erExpensSquareDRT
            // 
            this.erExpensSquareDRT.Name = "erExpensSquareDRT";
            this.erExpensSquareDRT.Properties.Caption = "������ � �����,  �����";
            this.erExpensSquareDRT.Properties.FieldName = "FuelDRTExpensSquare";
            this.erExpensSquareDRT.Properties.ReadOnly = true;
            // 
            // erExpensSquareAvgDRT
            // 
            this.erExpensSquareAvgDRT.Name = "erExpensSquareAvgDRT";
            this.erExpensSquareAvgDRT.Properties.Caption = "������ � �����, �/��";
            this.erExpensSquareAvgDRT.Properties.FieldName = "FuelDRTExpensAvgSquare";
            this.erExpensSquareAvgDRT.Properties.ReadOnly = true;
            // 
            // erExpensWithoutWorkDRT
            // 
            this.erExpensWithoutWorkDRT.Name = "erExpensWithoutWorkDRT";
            this.erExpensWithoutWorkDRT.Properties.Caption = "������ �� ���������, �����";
            this.erExpensWithoutWorkDRT.Properties.FieldName = "FuelDRTExpensWithoutWork";
            this.erExpensWithoutWorkDRT.Properties.ReadOnly = true;
            // 
            // erExpensAvgWithoutWorkDRT
            // 
            this.erExpensAvgWithoutWorkDRT.Name = "erExpensAvgWithoutWorkDRT";
            this.erExpensAvgWithoutWorkDRT.Properties.Caption = "������ �� ���������, �/100��";
            this.erExpensAvgWithoutWorkDRT.Properties.FieldName = "FuelDRTExpensAvgWithoutWork";
            this.erExpensAvgWithoutWorkDRT.Properties.ReadOnly = true;
            // 
            // ValidityData
            // 
            this.ValidityData.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.PointsValidity,
            this.PointsCalc,
            this.PointsFact,
            this.PointsIntervalMax});
            this.ValidityData.Name = "ValidityData";
            this.ValidityData.Properties.Caption = "�������� ������";
            // 
            // PointsValidity
            // 
            this.PointsValidity.Appearance.Options.UseTextOptions = true;
            this.PointsValidity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PointsValidity.Name = "PointsValidity";
            this.PointsValidity.Properties.Caption = "������� ������";
            this.PointsValidity.Properties.FieldName = "PointsValidity";
            this.PointsValidity.Properties.ReadOnly = true;
            this.PointsValidity.Properties.RowEdit = this.pbValidity;
            // 
            // PointsCalc
            // 
            this.PointsCalc.Appearance.Options.UseTextOptions = true;
            this.PointsCalc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsCalc.Name = "PointsCalc";
            this.PointsCalc.Properties.Caption = "���������� ����� (�� Log_ID) ���������";
            this.PointsCalc.Properties.FieldName = "PointsCalc";
            this.PointsCalc.Properties.ReadOnly = true;
            // 
            // PointsFact
            // 
            this.PointsFact.Appearance.Options.UseTextOptions = true;
            this.PointsFact.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsFact.Name = "PointsFact";
            this.PointsFact.Properties.Caption = "���������� ����� (�� Log_ID) �����������";
            this.PointsFact.Properties.FieldName = "PointsFact";
            this.PointsFact.Properties.ReadOnly = true;
            // 
            // PointsIntervalMax
            // 
            this.PointsIntervalMax.Appearance.Options.UseTextOptions = true;
            this.PointsIntervalMax.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsIntervalMax.Name = "PointsIntervalMax";
            this.PointsIntervalMax.Properties.Caption = "������������ �������� ����� �������";
            this.PointsIntervalMax.Properties.FieldName = "PointsIntervalMax";
            this.PointsIntervalMax.Properties.ReadOnly = true;
            // 
            // repositoryItemProgressBar5
            // 
            this.repositoryItemProgressBar5.Name = "repositoryItemProgressBar5";
            this.repositoryItemProgressBar5.ShowTitle = true;
            // 
            // repositoryItemProgressBar6
            // 
            this.repositoryItemProgressBar6.Name = "repositoryItemProgressBar6";
            this.repositoryItemProgressBar6.ShowTitle = true;
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // erRemark
            // 
            this.erRemark.Name = "erRemark";
            // 
            // gcQuality
            // 
            this.gcQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gcQuality.Controls.Add(this.elLightsIndicator);
            this.gcQuality.Location = new System.Drawing.Point(703, 156);
            this.gcQuality.Name = "gcQuality";
            this.gcQuality.Size = new System.Drawing.Size(116, 58);
            this.gcQuality.TabIndex = 75;
            this.gcQuality.Text = "������� ������";
            // 
            // elLightsIndicator
            // 
            this.elLightsIndicator.AutoLayout = false;
            this.elLightsIndicator.BackColor = System.Drawing.Color.Transparent;
            this.elLightsIndicator.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.elLightsIndicator.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateGauge});
            this.elLightsIndicator.Location = new System.Drawing.Point(26, 20);
            this.elLightsIndicator.Name = "elLightsIndicator";
            this.elLightsIndicator.Size = new System.Drawing.Size(60, 38);
            this.elLightsIndicator.TabIndex = 75;
            // 
            // stateGauge
            // 
            this.stateGauge.Bounds = new System.Drawing.Rectangle(10, 2, 38, 33);
            this.stateGauge.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateGauge.Name = "stateGauge";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 1;
            indicatorState5.Name = "State1";
            indicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState6.Name = "State2";
            indicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState7.Name = "State3";
            indicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState8.Name = "State4";
            indicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState5,
            indicatorState6,
            indicatorState7,
            indicatorState8});
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 558);
            this.Controls.Add(this.gcQuality);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.xtbCont);
            this.Controls.Add(this.meRemark);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(798, 568);
            this.Name = "Order";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� �� ������� ������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Order_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvContentPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbvContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldGroupeLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateInit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateInit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbCont)).EndInit();
            this.xtbCont.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcContentGrp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentGrp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.le)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5View)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelDRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.xtraTabMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scControl)).EndInit();
            this.scControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcControlDrv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlDrv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DriverLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scControlInner)).EndInit();
            this.scControlInner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcControlAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgrLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEditAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcControlField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvControlField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FldLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txEditFld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmeTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrvLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuality)).EndInit();
            this.gcQuality.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateGauge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btRefresh;
        private DevExpress.XtraBars.BarButtonItem btMapView;
        private DevExpress.XtraBars.BarSubItem bsiExcel;
        private DevExpress.XtraBars.BarButtonItem btExcelTotal;
        private DevExpress.XtraBars.BarButtonItem btExcel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txId;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit deDateInit;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LookUpEdit leMobitel;
        private DevExpress.XtraEditors.MemoEdit meRemark;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraTab.XtraTabControl xtbCont;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabMap;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gcContent;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContentPrice;
        private DevExpress.XtraGrid.Columns.GridColumn IdP;
        private DevExpress.XtraGrid.Columns.GridColumn Work;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit WorkLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn FSP;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn Sum;
        private DevExpress.XtraGrid.Columns.GridColumn Comment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gbvContent;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand ����;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn GFName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Square;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeRate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Family;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Width_c;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DistanceT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquareCalc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SpeedAvgField;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Zone_ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PriceC;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id_agregat;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit FieldGroupeLookUp;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit FieldLookUp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        //private DevExpress.XtraGrid.Views.Grid.GridView gvFuelDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn IDDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn FieldDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn FamilyDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn TimeStartDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn TimeEndDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn DistanceDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensMoveDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensStopDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensTotalDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensAvgDRT;
        //private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensAvgRateDRT;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        //private DevExpress.XtraGrid.Views.Grid.GridView gvControlField;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        //private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit FldLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        //private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEditFld;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        //private DevExpress.XtraGrid.Views.Grid.GridView gvControlAgr;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        //private DevExpress.XtraGrid.Columns.GridColumn Agregat;
        //private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit AgrLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        //private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEditAgr;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        //private DevExpress.XtraGrid.Views.Grid.GridView gvControlDrv;
        //private DevExpress.XtraGrid.Columns.GridColumn Iddrv;
        //private DevExpress.XtraGrid.Columns.GridColumn Id_object1;
        //private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit DriverLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        //private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit DrvLookUp;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar pbValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar4;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar5;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar6;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgOrder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotalPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPlaceStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPlaceEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSpeed;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotalc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMobitel;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotalTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeWork;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeMove;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeStop;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow ValidityData;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsValidity;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsCalc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsFact;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsIntervalMax;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        private DevExpress.XtraEditors.PanelControl pnMap;
        private DevExpress.XtraBars.BarStaticItem bsiStatus;
        private DevExpress.XtraBars.BarButtonItem btSave;
        private DevExpress.XtraGrid.GridControl gcFuelDRT;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelDRT;
        private DevExpress.XtraGrid.Columns.GridColumn IDDRT;
        private DevExpress.XtraGrid.Columns.GridColumn FieldDRT;
        private DevExpress.XtraGrid.Columns.GridColumn FamilyDRT;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStartDRT;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEndDRT;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceDRT;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensMoveDRT;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensStopDRT;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensTotalDRT;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensAvgDRT;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensAvgRateDRT;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.GridControl gcControlDrv;
        private DevExpress.XtraGrid.Views.Grid.GridView gvControlDrv;
        private DevExpress.XtraGrid.Columns.GridColumn colIddrv;
        private DevExpress.XtraGrid.Columns.GridColumn Id_object1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit DriverLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn colEnterDrv;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colExitDrv;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeDrv;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit3;
        private DevExpress.XtraGrid.GridControl gcControlAgr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvControlAgr;
        private DevExpress.XtraGrid.Columns.GridColumn colIdAgr;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit AgrLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn colEnterAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEditAgr;
        private DevExpress.XtraGrid.Columns.GridColumn colExitAgr;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeAgr;
        private DevExpress.XtraGrid.GridControl gcControlField;
        private DevExpress.XtraGrid.Views.Grid.GridView gvControlField;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colField;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit FldLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn colEnter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txEditFld;
        private DevExpress.XtraGrid.Columns.GridColumn colExit;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraBars.BarButtonItem btDelete;
        private DevExpress.XtraBars.BarSubItem bsiFact;
        private DevExpress.XtraBars.BarButtonItem btFactControl;
        private DevExpress.XtraBars.BarButtonItem btFact;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRemark;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquare;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow errRemark;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraGrid.GridControl gcContentGrp;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContentGrp;
        private DevExpress.XtraGrid.Columns.GridColumn FNameGr;
        private DevExpress.XtraGrid.Columns.GridColumn SquareGr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_agregatGr;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceGr;
        private DevExpress.XtraGrid.Columns.GridColumn FactSquareGr;
        private DevExpress.XtraGrid.Columns.GridColumn FactSquareCalcGr;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit le;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit5View;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leDriver;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leAgregat;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crDUT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensSquareDUT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensSquareAvgDUT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensWithoutWorkDUT;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crDRT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensSquareDRT;
        public DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensAvgWithoutWorkDUT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensSquareAvgDRT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensWithoutWorkDRT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erExpensAvgWithoutWorkDRT;
        private DevExpress.XtraBars.BarSubItem bsiReports;
        private DevExpress.XtraBars.BarButtonItem btReport;
        private DevExpress.XtraBars.BarButtonItem btReportContentGrouped;
        private DevExpress.XtraBars.BarButtonItem btReportContent;
        private DevExpress.XtraBars.BarButtonItem btReportFuelDUT;
        private DevExpress.XtraBars.BarButtonItem btReportFuelDRT;
        private DevExpress.XtraEditors.SplitContainerControl scControl;
        private DevExpress.XtraEditors.SplitContainerControl scControlInner;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erParth;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareOverlap;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateLastRecalc;
        private DevExpress.XtraBars.BarButtonItem btLog;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit tmeTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.GroupControl gcQuality;
        private DevExpress.XtraGauges.Win.GaugeControl elLightsIndicator;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateGauge;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraGrid.Columns.GridColumn colAgrInputSourceGr;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDrvInputSource;
        private DevExpress.Utils.ToolTipController ttcGridView;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn AName;
        internal DevExpress.XtraGrid.Columns.GridColumn colDrvInputSourceGr;
        public DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgrInputSource;
        public DevExpress.XtraGrid.Columns.GridColumn Id_driverGr;
    }
}