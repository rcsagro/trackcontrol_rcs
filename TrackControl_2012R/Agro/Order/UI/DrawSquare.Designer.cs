namespace Agro
{
    partial class DrawSquare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawSquare));
            this.peSq = new DevExpress.XtraEditors.PictureEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pgParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.erSquareZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareProcess = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareProcessPersent = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareSecondProcess = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareSecondProcessPersent = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareSecondProcessFactor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.peSq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).BeginInit();
            this.SuspendLayout();
            // 
            // peSq
            // 
            this.peSq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peSq.Location = new System.Drawing.Point(0, 0);
            this.peSq.Name = "peSq";
            this.peSq.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peSq.Size = new System.Drawing.Size(650, 659);
            this.peSq.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pgParams);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.peSq);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(935, 659);
            this.splitContainerControl1.SplitterPosition = 279;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // pgParams
            // 
            this.pgParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgParams.Location = new System.Drawing.Point(0, 0);
            this.pgParams.Name = "pgParams";
            this.pgParams.RecordWidth = 63;
            this.pgParams.RowHeaderWidth = 137;
            this.pgParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erSquareZone,
            this.erSquareProcess,
            this.erSquareProcessPersent,
            this.erSquareSecondProcess,
            this.erSquareSecondProcessPersent,
            this.erSquareSecondProcessFactor});
            this.pgParams.Size = new System.Drawing.Size(279, 659);
            this.pgParams.TabIndex = 0;
            // 
            // erSquareZone
            // 
            this.erSquareZone.Name = "erSquareZone";
            this.erSquareZone.Properties.Caption = "����� �������, ��";
            this.erSquareZone.Properties.FieldName = "SquareZone";
            this.erSquareZone.Properties.ReadOnly = true;
            // 
            // erSquareProcess
            // 
            this.erSquareProcess.Name = "erSquareProcess";
            this.erSquareProcess.Properties.Caption = "����������, ��";
            this.erSquareProcess.Properties.FieldName = "SquareProcess";
            this.erSquareProcess.Properties.ReadOnly = true;
            // 
            // erSquareProcessPersent
            // 
            this.erSquareProcessPersent.Name = "erSquareProcessPersent";
            this.erSquareProcessPersent.Properties.Caption = "����������, %";
            this.erSquareProcessPersent.Properties.FieldName = "SquareProcessPersent";
            this.erSquareProcessPersent.Properties.ReadOnly = true;
            // 
            // erSquareSecondProcess
            // 
            this.erSquareSecondProcess.Name = "erSquareSecondProcess";
            this.erSquareSecondProcess.Properties.Caption = "�������� ����������, ��";
            this.erSquareSecondProcess.Properties.FieldName = "SquareSecondProcess";
            this.erSquareSecondProcess.Properties.ReadOnly = true;
            // 
            // erSquareSecondProcessPersent
            // 
            this.erSquareSecondProcessPersent.Name = "erSquareSecondProcessPersent";
            this.erSquareSecondProcessPersent.Properties.Caption = "�������� ����������, %";
            this.erSquareSecondProcessPersent.Properties.FieldName = "SquareSecondProcessPersent";
            this.erSquareSecondProcessPersent.Properties.ReadOnly = true;
            // 
            // erSquareSecondProcessFactor
            // 
            this.erSquareSecondProcessFactor.Name = "erSquareSecondProcessFactor";
            this.erSquareSecondProcessFactor.Properties.Caption = "���� ��������� ���������, %";
            this.erSquareSecondProcessFactor.Properties.FieldName = "SquareSecondProcessFactor";
            this.erSquareSecondProcessFactor.Properties.ReadOnly = true;
            // 
            // DrawSquare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 659);
            this.Controls.Add(this.splitContainerControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DrawSquare";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� �������";
            ((System.ComponentModel.ISupportInitialize)(this.peSq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit peSq;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgParams;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareProcessPersent;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareSecondProcessPersent;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareZone;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareProcess;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareSecondProcess;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareSecondProcessFactor;

    }
}