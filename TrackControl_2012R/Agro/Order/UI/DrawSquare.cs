using Agro.Properties;
using TrackControl.General; 
namespace Agro
{
    public partial class DrawSquare : DevExpress.XtraEditors.XtraForm
    {
        OrderSquareBitmap osb;
        public DrawSquare(int Id_orderT)
        {
            InitializeComponent();
            Localization();
            DrawWithClass(Id_orderT);
        }
        private void DrawWithClass(int ID_orderT)
        {
            OrderItemRecord oir = new OrderItemRecord(ID_orderT);
            if (oir.ZoneId > 0)
            {
                IZone zone = OrderItem.ZonesModel.GetById(oir.ZoneId);
                osb = new OrderSquareBitmap(zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK, zone.Points);
                {
                    CompressData cd = new CompressData("agro_datagps", ID_orderT);
                    oir.Rpoints = cd.DeCompressReal();
                    osb.SquareCalcBitmap(oir,true);
                    pgParams.SelectedObject = osb;
                    peSq.Image = osb.BtmField;
                }
            }
            oir.Dispose();
         }

        #region �����������
        private void Localization()
        {
            erSquareZone.Properties.Caption = Resources.SquareTotalGA;
            erSquareProcess.Properties.Caption = Resources.ProcessGA;
            erSquareProcessPersent.Properties.Caption = Resources.ProcessPercent;
            erSquareSecondProcess.Properties.Caption = Resources.ProcessRepeatGA;
            erSquareSecondProcessPersent.Properties.Caption = Resources.ProcessRepeatPercent;
            erSquareSecondProcessFactor.Properties.Caption = Resources.ProcessRepeatCoeff;
            Text = Resources.SquareProcess;
        }
        #endregion
    }
}