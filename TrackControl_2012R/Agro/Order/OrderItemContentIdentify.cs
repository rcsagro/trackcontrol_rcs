﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles; 

namespace Agro
{
    partial class OrderItem 
    {
        /// <summary>
        /// разбивка данных GPS на записи наряда по границам полей
        /// </summary>
        private void CI_ContentIdentify()
        {
            //нарезка дня телетрека по полям и водителям с момента начала движения до момента окончания движения
            _oirs = new List<OrderItemRecord>();
            _cid = new CI_Data();
            _agregat_ident = new Dictionary<ulong, int>();
            _driver_ident = new Dictionary<ulong, int>();
            _oir_sensors = new Dictionary<int, LogicSensor>();
            _zones_fields = new Dictionary<int, int>();
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + _Mobitel_Id, "time");
            CI_PrepareControlData(dataRows);
            SetPointsIntervalMax(dataRows);
            SetPointsValidity(dataRows);
            atlantaDataSet.dataviewRow dRowStart = dataRows[0];
            atlantaDataSet.dataviewRow dRowPrev = dataRows[0];
            atlantaDataSet.dataviewRow dRowLast = dataRows[dataRows.Length - 1];
            _lastGpsTime = dRowLast.time;
            _cid.dtObjChange = dataRows[0].time;
            //отрезаем стоянку в конце суток
            dRowLast = CI_CutStopsInTrackEnd(dataRows, dRowLast);
            // индикатор начала движения
            _cid.startMove = false;
            SetStatusEvent(Resources.OrderRecordsDefine);
            //список записей DataGps, принадлежащих зоне
            List<atlantaDataSet.dataviewRow> lsDrows = new List<atlantaDataSet.dataviewRow>();
            //буферный список DataGps при дребезге
            List<atlantaDataSet.dataviewRow> lsDrowsBuf = new List<atlantaDataSet.dataviewRow>();
            foreach (atlantaDataSet.dataviewRow d_row in dataRows)
            {
                if (!_cid.startMove)
                {
                    //в наряд попадают данные с момента старта движения
                    if (d_row.speed > 0)
                    {
                        CI_SetStartPoint(ref dRowStart, dRowPrev, lsDrows, d_row);
                    }
                }
                if (_cid.startMove)
                {
                    CI_SetDriverAgregZoneLockingForPointGps(false, d_row);
                    if (CI_IsPointLocked())
                    {
                        //для случая если залоченная запись последняя
                        if (d_row.DataGps_ID == dRowLast.DataGps_ID)
                        {
                            lsDrows.Clear();
                            break;
                        }
                        else
                        {
                            // для связи между последней точкой лоченой записи и первой нелоченной (CI_SetLinkedPoint)
                            lsDrows.Add(d_row);
                            continue;
                        }
                    }
                    if (_cid.prevZone > 0)
                    {
                        if (dcZonesBitmap[_cid.prevZone].PointInZone(d_row.Lon, d_row.Lat))
                        {
                            _cid.curZone = _cid.prevZone;
                        }
                    }
                    // для режима с учетом управляющих данных если зоны нет - новую не ищем (так можно управляющими данными регулировать пробеги) 
                    if (_cid.curZone == 0 && !_Control)
                    {
                        _cid.curZone = CI_DefineZone(_cid.curZone, d_row);
                    }
                    //управление нарезкой записей с помощью внешнего датчика
                    CI_SetSensorZoneAgregat(d_row);
                    // накапливаем время движения 
                    if (d_row.speed > 0)
                        _cid.tsTimeMoveAfterChangeRecord = _cid.tsTimeMoveAfterChangeRecord.Add(d_row.time.Subtract(dRowPrev.time));
                    //определение смены поля , водителя или агрегата
                    CI_SetObjChange(lsDrows, lsDrowsBuf, d_row);
                    // дребезг работает по накопительному принципу.Если измененеие зоны происходит с Т < tsDeltaField,
                    //то новая запись не образуется.После повторной смены зоны c Т1 < tsDeltaField дальше анализируется уже (Т + Т1) < tsDeltaField 
                    if ((_cid.objChange) || (d_row.DataGps_ID == dRowLast.DataGps_ID))
                    {
                        if (d_row.DataGps_ID == dRowLast.DataGps_ID)
                        {
                            _cid.stopMove = true;
                            dRowPrev = d_row;
                        }
                        if ((dRowStart.DataGps_ID != dRowPrev.DataGps_ID) && ((_cid.objChange && CI_TestBounce(d_row.time))))
                        {
                            if (ContentCreateRecord(lsDrows))
                            {
                                CI_SetLinkedPoint(lsDrows);
                                CI_CopyFromBuferList(lsDrows, lsDrowsBuf);
                                CI_SetCurrentToPrev(ref dRowStart, lsDrowsBuf, d_row);
                                if (_cid.curLocked) lsDrows.Clear();
                            }
                        }
                        if (_cid.stopMove) break;
                    }
                    dRowPrev = d_row;
                    _cid.prevLocked = _cid.curLocked;
                    Application.DoEvents();
                }
            }
            //запись последнего трека
            if (lsDrows.Count > 0)
            {
                CI_CopyFromBuferList(lsDrows, lsDrowsBuf);
                ContentCreateRecord( lsDrows);
            }
            //обновление полного списка записей наряда - из-за наличия залоченных записей
            if ((_dtLock.Rows.Count != 0)) _oirs = GetOrderItemRecords();
            SetStatusEvent(Resources.DataCleaning);
            if (dcZonesBitmap.Count > 0)
            {
                foreach (KeyValuePair<int, OrderSquareBitmap> kvp in dcZonesBitmap)
                {
                    kvp.Value.Dispose();
                }
                GC.Collect();
            }
        }

        private bool CI_IsPointLocked()
        {
            return (_cid.prevLocked & _cid.curLocked);
        }

        /// <summary>
        /// связь между записями должна быть непрерывной.Последующая запись должна начинаться с предыдущей  с нулевым пробегом 
        /// </summary>
        /// <param name="lsDrows"></param>
        private static void CI_SetLinkedPoint(List<atlantaDataSet.dataviewRow> lsDrows)
        {
            if (lsDrows.Count > 1)
            {
                lsDrows.RemoveRange(0, lsDrows.Count - 1);
                lsDrows[0].dist = 0;
            }
        }

        private void CI_SetCurrentToPrev(ref atlantaDataSet.dataviewRow d_row_start, List<atlantaDataSet.dataviewRow> lsDrowsBuf, atlantaDataSet.dataviewRow d_row)
        {
            lsDrowsBuf.Clear();
            CI_SetCurrentToPrevZDA();
            _cid.prevLocked = _cid.curLocked;
            d_row_start = d_row;
            SetStatusEvent(Resources.OrderRecordsDefine);
            _cid.objChange = false;
            _cid.zoneChange = false;
            _cid.tsTimeMoveAfterChangeRecord = TimeSpan.Zero;
        }

        private void CI_SetObjChange(List<atlantaDataSet.dataviewRow> lsDrows, List<atlantaDataSet.dataviewRow> lsDrowsBuf, atlantaDataSet.dataviewRow d_row)
        {
            if (_cid.firstRecord)
            {
                _cid.firstRecord = false;
                CI_SetCurrentToPrevZDA();
                if (!lsDrows.Contains(d_row))
                {
                    lsDrows.Add(d_row);
                }
            }
            else
            {
                if (CI_IsPointLockedLast())
                {
                    //cid.curZone = CI_DefineZone(cid.curZone, d_row);
                    CI_SetCurrentToPrevZDA();
                    _cid.tsTimeMoveAfterChangeRecord = TimeSpan.Zero;
                    CI_SetLinkedPoint(lsDrows);
                }
                if ((_cid.curZone != _cid.prevZone) || (_cid.curDriver != _cid.prevDriver) || (_cid.curAgreg != _cid.prevAgreg) || CI_IsPointLockedFirst())
                {
                    if (!_cid.objChange)
                    {
                        _cid.objChange = true;
                        if (_cid.curZone != _cid.prevZone)
                            _cid.zoneChange = true;
                        else
                            _cid.zoneChange = false;
                        _cid.dtObjChange = d_row.time;
                        _cid.tsTimeMoveAfterChangeRecord = TimeSpan.Zero;
                        _cid.changeZone = _cid.curZone;
                        if (!lsDrows.Contains(d_row))
                        {
                            lsDrows.Add(d_row);
                        }
                    }
                    else
                    {
                        lsDrowsBuf.Add(d_row);
                    }
                }
                else
                {
                    _cid.objChange = false;
                    _cid.zoneChange = false;
                    if (lsDrowsBuf.Count > 0)
                    {
                        CI_CopyFromBuferList(lsDrows, lsDrowsBuf);
                        lsDrowsBuf.Clear();
                    }
                    if (!lsDrows.Contains(d_row))
                    {
                        lsDrows.Add(d_row);
                    }
                }
            }
        }

        private void CI_SetCurrentToPrevZDA()
        {
            _cid.prevZone = _cid.curZone;
            _cid.prevDriver = _cid.curDriver;
            _cid.prevAgreg = _cid.curAgreg;
            _cid.prevAgrInputSource = _cid.curAgrInputSource;
            _cid.prevDrvInputSource = _cid.curDrvInputSource;
        }

        private bool CI_IsPointLockedLast()
        {
            return (!_cid.curLocked & _cid.prevLocked);
        }

        private bool CI_IsPointLockedFirst()
        {
            return (_cid.curLocked & !_cid.prevLocked);
        }

        private void CI_SetStartPoint(ref atlantaDataSet.dataviewRow d_row_start, atlantaDataSet.dataviewRow d_row_prev, List<atlantaDataSet.dataviewRow> lsDrows, atlantaDataSet.dataviewRow d_row)
        {
            _cid.startMove = true;
            d_row_start = d_row;
            //зона , водитель, агрегат первой точки
            CI_SetDriverAgregZoneLockingForPointGps(true, d_row);
            if (_cid.prevZone == 0)
            {
                _cid.prevZone = CI_DefineZone(_cid.prevZone, d_row);
                if (_cid.prevZone > 0 && !lsDrows.Contains(d_row_prev))
                {
                    if (d_row_prev.speed > 0) lsDrows.Add(d_row_prev);
                }
            }
            else
            {
                // добавление точки перед зоной для режима с учетом управляющих данных
                if (d_row_prev.speed > 0) lsDrows.Add(d_row_prev);
            }
        }

        private int CI_DefineZone(int curZone, atlantaDataSet.dataviewRow d_row)
        {
            foreach (KeyValuePair<int, OrderSquareBitmap> kvp in dcZonesBitmap)
            {
                if (kvp.Value.PointInZone(d_row.Lon, d_row.Lat))
                {
                    curZone = kvp.Key;
                    break;
                }
            }
            return curZone;
        }

        private void CI_PrepareControlData(atlantaDataSet.dataviewRow[] dataRows)
        {
            // дребезг карточки водителя  и агрегата + выезд за пределы зоны
            AgroSetItem asi = new AgroSetItem();
            tsDeltaField = new TimeSpan(0, asi.MinTimeInFieldForCreateRecord, 0);
            tsDeltaSensor = new TimeSpan(0, asi.BounceSensor, 0);
            SetStatusEvent(Resources.DataSetCreate);
            _dtControl = GetControlDataAll();
            _dtLock = GetLockedData();
            CI_GetSensorsParametersDriverAgregat();
            //перечень зон с полями
            SetStatusEvent(Resources.FieldsSelection);
            dcZonesBitmap = new Dictionary<int, OrderSquareBitmap>();
            ContentCreateZonesList(dataRows);
            //агрегат по умолчанию
            using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat())
            {
                _id_agregat_default = daa.GetDefaultForVehicle(_Vehicle_Id);
                if (_id_agregat_default == 0) _id_agregat_default = daa.GetDefault();
            }
            _cid.firstRecord = true;
            _id_driver_default = DicUtilites.GetDriverId_Mobitel(_Mobitel_Id);
        }

        private static atlantaDataSet.dataviewRow CI_CutStopsInTrackEnd(LocalCache.atlantaDataSet.dataviewRow[] dataRows, atlantaDataSet.dataviewRow d_row_Last)
        {
            for (int i = dataRows.Length - 1; i > 0; i--)
            {
                if (dataRows[i].speed > 0)
                {
                    if (i == dataRows.Length - 1)
                        d_row_Last = dataRows[i];
                    else
                        d_row_Last = dataRows[i + 1];
                    break;
                }
            }
            return d_row_Last;
        }

        private void CI_GetSensorsParametersDriverAgregat()
        {
            //параметры индикатора водителя 
            _cid.senDrvLength = 0;
            _cid.senDrvStartBit = 0;
            GetSensor(AlgorithmType.DRIVER, out _cid.senDrvLength, out _cid.senDrvStartBit);
            //параметры индикатора агрегата
            _cid.senAgrLength = 0;
            _cid.senAgrStartBit = 0;
            GetSensor(AlgorithmType.AGREGAT, out _cid.senAgrLength, out _cid.senAgrStartBit);
        }

        private void CI_SetDriverAgregZoneLockingForPointGps(bool setPrevData, atlantaDataSet.dataviewRow d_row)
        {
            AgrDrvInputSources AgrInputSource = AgrDrvInputSources.Missing;
            AgrDrvInputSources DrvInputSource = AgrDrvInputSources.Missing;
            int id_agregat = ContentGetAgregat(d_row, _cid.senAgrLength, _cid.senAgrStartBit, out AgrInputSource);
            if (setPrevData)
            {
                _cid.prevDriver = ContentGetDriver(d_row, _cid.senDrvLength, _cid.senDrvStartBit,out DrvInputSource);
                _cid.prevAgreg = id_agregat;
                _cid.prevZone = ContentGetZone(d_row);
                _cid.prevLocked = ContentPointLocked(d_row);
                _cid.prevAgrInputSource=AgrInputSource;
                _cid.prevDrvInputSource = DrvInputSource;
            }
            else
            {
                _cid.curDriver = ContentGetDriver(d_row, _cid.senDrvLength, _cid.senDrvStartBit,out DrvInputSource);
                _cid.curAgreg = id_agregat;
                _cid.curZone = ContentGetZone(d_row);
                _cid.curLocked = ContentPointLocked(d_row);
                _cid.curAgrInputSource = AgrInputSource;
                _cid.curDrvInputSource = DrvInputSource;
            }
        }

        private void CI_SetSensorValue(atlantaDataSet.dataviewRow d_row, int id_agregat)
        {
            if (_oir_sensors.ContainsKey(id_agregat))
            {
                if (_oir_sensors[id_agregat].Id > 0)
                {
                    _cid.IsSensorExist = true;
                    _cid.IsSensorActive = _oir_sensors[id_agregat].IsActive(d_row.sensor);
                }
            }
            else
            {
                using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat(id_agregat))
                {
                    int id_sensor = 0;
                    LogicSensor ls;
                    if (daa.HasSensor)
                    {
                        id_sensor = daa.GetDefaultSensorForVehicle(_Vehicle_Id);

                        if (id_sensor == 0)
                        {
                            ls = new LogicSensor((int)AlgorithmType.AGREGAT_LOGIC, _Mobitel_Id);
                            id_sensor = ls.Id;
                        }
                        else
                        {
                            ls = new LogicSensor(id_sensor);
                        }
                        _oir_sensors.Add(id_agregat, ls);
                        if (id_sensor > 0)
                        {
                            _cid.IsSensorExist = true;
                            _cid.IsSensorActive = _oir_sensors[id_agregat].IsActive(d_row.sensor);
                        }
                    }
                    else
                    {
                        _cid.IsSensorExist = false;
                        ls = new LogicSensor(id_sensor);
                        _oir_sensors.Add(id_agregat, ls);
                    }
                }
            }
        }

        /// <summary>
        /// определение рабочего трека с помощью внешнего датчика
        /// </summary>
        /// <param name="d_row"></param>
        void CI_SetSensorZoneAgregat(atlantaDataSet.dataviewRow d_row)
        {
            if (_cid.curAgreg > 0)
            {
                 CI_SetSensorValue(d_row, _cid.curAgreg);
            }
            if (_cid.IsSensorExist)
            {
                if (_cid.IsSensorActive)
                {
                    if (_cid.curZone == 0) _cid.curZone = Consts.ZONE_OUTSIDE_THE_LIST;
                }
                //При наличии логического датчика писать Переезд в поле при неактивном состоянии
                else
                {
                    _cid.curAgreg = 0;
                }

                //else
                //{
                //    _cid.curZone = 0;
                //}
            }
        }

        private void CI_CopyFromBuferList(List<atlantaDataSet.dataviewRow> lsDrows, List<atlantaDataSet.dataviewRow> lsDrowsBuf)
        {
            if (lsDrowsBuf.Count > 0)
            {
                for (int i = 0; i < lsDrowsBuf.Count; i++)
                {
                    if (!lsDrows.Contains(lsDrowsBuf[i]))
                    {
                        lsDrows.Add(lsDrowsBuf[i]);
                    }
                }
            }
        }
        /// <summary>
        /// подавление дребезга
        /// </summary>
        /// <param name="TimeDataRow">текущее время анализа</param>
        /// <param name="ZoneChange">признак изменения поля</param>
        /// <param name="DateObjChange">время изменения параметра</param>
        /// <returns></returns>
        private bool CI_TestBounce(DateTime TimeDataRow)
        {
            //if (cid.curLocked & !cid.prevLocked) return true;
            //return true;
            if (CI_IsPointLockedFirst()) return true;
            if (_cid.zoneChange)
            {
                //TimeDataRow.Subtract(DateObjChange)tsTimeMoveAfterChangeRecord 
                if (_cid.tsTimeMoveAfterChangeRecord > tsDeltaField)
                    return true;
                else
                    return false;
            }
            else
            {
                if (TimeDataRow.Subtract(_cid.dtObjChange) > tsDeltaSensor)
                    return true;
                else
                    return false;
            }
        }
    }
}
