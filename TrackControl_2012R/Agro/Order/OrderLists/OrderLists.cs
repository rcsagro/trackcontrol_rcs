﻿using System;
using System.Data;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    public static class OrderLists
    {
        public static DataTable GetListMain(int idOrder, DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable dtOrder;

                string sSQL = AgroQuery.OrderList.SelectAgroOrder;

                if (idOrder > 0)
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrderId + idOrder;
                    dtOrder = driverDb.GetDataTable(sSQL);
                }
                else
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrder;
                   
                    if (idTeam > 0) 
                        sSQL += AgroQuery.OrderList.EndVehicleTeamId + idTeam;

                    if (idMobitel > 0) 
                        sSQL += AgroQuery.OrderList.AndMobitelsId + idMobitel;

                    sSQL = sSQL + AgroQuery.OrderList.OrderByAgroOrder;

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                    dtOrder = driverDb.GetDataTable(sSQL, driverDb.GetSqlParameterArray);
                }
                return dtOrder;
            }
        }

        public static DataTable GetListContent(int idOrder, DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable dtContent;
                string sSQL = AgroQuery.OrderList.SelectAgroOrderT;

                if (idOrder > 0)
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrderId + idOrder;
                    dtContent = driverDb.GetDataTable(sSQL);
                }
                else
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrder;
                    
                    if (idTeam > 0) 
                        sSQL += AgroQuery.OrderList.EndVehicleTeamId + idTeam;

                    if (idMobitel > 0) 
                        sSQL += AgroQuery.OrderList.AndAgroOrderIdMobitel + idMobitel;

                    sSQL = sSQL + AgroQuery.OrderList.OrderByAgroOrder;

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                    dtContent = driverDb.GetDataTable(sSQL, driverDb.GetSqlParameterArray);
                }
                return dtContent;
            }
        }

        public static DataTable GetListFields(DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string subString = "";

                subString = AgroQuery.OrderList.SelectCountCnt;
            
                if (idTeam > 0) 
                    subString = string.Format(AgroQuery.OrderList.AndVehicleTeam, subString, idTeam);

                if (idMobitel > 0) 
                    subString = string.Format(AgroQuery.OrderList.AndAgroOrder, subString, idMobitel);

                subString += AgroQuery.OrderList.WhereIdField;

                string sSQLselect = string.Format(AgroQuery.OrderList.SelectZones,subString);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", (DateTime)start);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", (DateTime)end);
                return driverDb.GetDataTable(sSQLselect, driverDb.GetSqlParameterArray);
            }
        }
    }
}
