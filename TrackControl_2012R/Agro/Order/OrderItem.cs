using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace Agro
{
    /// <summary>
    /// ����� �� ������
    /// </summary>
    public partial class OrderItem : DocItem
    {

    #region �������
    public event StatusMessage ChangeStatusEvent;
    //������� ���������
    private void SetStatusEvent(string sMessage)
    {
        if (ChangeStatusEvent != null)
        {
            if (sMessage == Resources.Ready)
            {
                ChangeStatusEvent(sMessage);
            }
            else
            {
                ChangeStatusEvent(_ID > 0 ? (Resources.Order +  " " + _ID
                    + ((_recordNumber > 0) ? "| " + Resources.Record + " " + _recordNumber : "")
                    + ":" + sMessage) : sMessage);
            }
            Application.DoEvents();
        }
    }
    #endregion

    #region ������������
        public OrderItem()
        {
        }    
        public OrderItem(DateTime dtDoc, int Mobitel_Id)
            {
                _Mobitel_Id = Mobitel_Id;
                _dateDoc = dtDoc;
            }
        public OrderItem(int ID_Order)
            {
                _ID = ID_Order;
                if (_ID == 0) return;
                GetDocById(_ID);
            }
    #endregion        

    #region ��������� ������ � ���������
     /// <summary>
            /// ������� �� ������� ����������� ����
     /// </summary>
     private class ItemSqClass
            {
                private double _Lon;
                private double _Lat;
                private bool _Selected;
                private bool _Border;
                private bool _BorderNode;
                // ����������� ���������� ��������� - ������� �������
                private static int _SelectedCount;
                // ����������� ��� ����� ������� - ��� �������������� ����������
                
                private static int _NumberCnt = 0;
                private  int _NumberBorder = 0;
                // ������ ������� ����� �������
                private static string _sSQLinsert;//  "INSERT INTO `agro_calct`(Id_main,Lon,Lat,Selected,Border,BorderNode,Number)  VALUES ";
                private static List<string> _lsSQLinsert = new List<string>();
                //������� ������ INSERT ���������
                private static int _iListCount = 0;
                public double Lon
                {
                    get { return _Lon; }
                    set { _Lon = value; }
                }
                public double Lat
                {
                    get { return _Lat; }
                    set { _Lat = value; }
                }
                public bool Selected
                {
                    get { return _Selected; }
                    set
                    {
                        if ((!_Selected) && value)
                            _SelectedCount++;
                        else
                            if ((_Selected) && !value)
                                _SelectedCount--;

                        _Selected = value;

                    }
                }
                public bool Border
                {
                    get { return _Border; }
                    set { _Border = value; }
                }
                public bool BorderNode
                {
                    get { return _BorderNode; }
                    set
                    {
                        _BorderNode = value;
                        if (_BorderNode)
                        {
                            if( _iListCount == 0 ) 
                                _sSQLinsert = AgroQuery.OrderItem.sqlInsertHead;
                          
                            _NumberCnt++;
                            _iListCount++;
                            _NumberBorder = _NumberCnt;
                            //���������������� ������  INSERT ��� ������ ����� ������
                            //��������� ����� �� 500 ������� - ��������� ��������
                            // ������ Id_calc ### - ���� ������ �� ������� ������� �� ���������� ��� �������
                            _sSQLinsert = _sSQLinsert + (_iListCount == 1 ? "" : ",") + "("
                           + "###,"
                           + Lon.ToString().Replace(",", ".") + ","
                           + Lat.ToString().Replace(",", ".") + ","
                           + Selected + "," + Border + "," + BorderNode + "," + _NumberCnt + ")";
                            if (_iListCount == 500)
                            {
                                _lsSQLinsert.Add(_sSQLinsert);
                                _iListCount = 0;
                                _sSQLinsert = "";
                            }


                        }
                    }
                }

                public int Number
                {
                    get { return _NumberCnt; }
                    set { _NumberCnt = value;}
                }
                public int NumberBorder
                {
                    get { return _NumberBorder; }
                    set { _NumberBorder = value; }
                }
                public int SelectedCount
                {
                    get { return _SelectedCount; }
                    set { _SelectedCount = value; }
                }
                public List<string> lsSQLinsert
                {
                    get {
                        if (_sSQLinsert.Length > 0)
                        {
                            _lsSQLinsert.Add(_sSQLinsert);
                            _sSQLinsert = "";
                            _iListCount = 0;
                            _NumberCnt = 0;

                        }
                        return _lsSQLinsert; 
                    }
                    set { _lsSQLinsert = value; }
                }
                public ItemSqClass(double iLon, double iLat)
                {
                    _Selected = false;
                    _Border = false;
                    _BorderNode = false;
                    _Lon = iLon;
                    _Lat = iLat;
                }
            }

     private struct CI_Data
     {
         public int senDrvLength;
         public int senDrvStartBit;
         public int senAgrLength;
         public int senAgrStartBit;
         public int curDriver;// ������������� �������� �� Dat�GPS , ������������ ��������� ����� � ������� 
         public int prevDriver;
         public int curZone;
         public int prevZone;
         public int changeZone;
         public int curAgreg;
         public int prevAgreg;
         public TimeSpan tsTimeMoveAfterChangeRecord;
         public bool startMove;
         public bool stopMove ;
         public bool objChange;
         public bool zoneChange;
         public bool prevLocked;
         public bool curLocked;
         public AgrDrvInputSources prevAgrInputSource;
         public AgrDrvInputSources curAgrInputSource;
         public AgrDrvInputSources prevDrvInputSource;
         public AgrDrvInputSources curDrvInputSource;
         public bool IsSensorExist;
         public bool IsSensorActive;
         /// <summary>
         /// ��� ������ � ���� � ����������� ������������ �������� �������� ��������� ������ ������ ������
         /// </summary>
         public bool firstRecord;
         public DateTime dtObjChange;
     }
	#endregion

    #region ��������
        public override DateTime Date
            {
                get
                {
                    return _dateDoc;
                }
                set
                {
                    _dateDoc = value;
                }
            }
        public override int AddDoc()
            {
                if (_Mobitel_Id == 0) return 0;    
                if (TestDemo())
                {
                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        string sSQL = AgroQuery.OrderItem.InsertIntoAgroOrder;

                        driverDb.NewSqlParameterArray(5);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Date", _dateDoc);
                        driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Mobitel_Id", _Mobitel_Id );

                        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                        {
                            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", _Remark);
                        }
                        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                        {
                            if(_Remark == null)
                                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", "" );
                            else
                                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", _Remark );
                        }

                        driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "StateOrder", (int)StatesOrder.New );
                        driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "UserCreated", UserBaseCurrent.Instance.Name );
                        _ID = driverDb.ExecuteReturnLastInsert( sSQL, driverDb.GetSqlParameterArray, "agro_order" );
                        UserLog.InsertLog(UserLogTypes.AGRO, Resources.OrderCreating, _ID);
                    }
                }
                return _ID;
            }
        public override bool DeleteDoc(bool bQuestionNeed)
            {
                if (DeleteDocTest() && DeleteDocContent())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.OrderDeleteConfirm + " " + _ID + "?", Resources.ApplicationName, MessageBoxButtons.YesNo))
                            return false;
                    }

                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb(); 
                            string sSQL = string.Format(AgroQuery.OrderItem.DeleteAgroOrder, _ID);
                            driverDb.ExecuteNonQueryCommand(sSQL);
                    }
                    UserLog.InsertLog(UserLogTypes.AGRO, Resources.OrderDelete, _ID);
                        return true;
                }
                else
                    return false;
            }
        public override bool DeleteDocContent()
                    {
                        return ContentDeleteControlObjects() && ContentDelete();
                    }
        public override bool UpdateDoc()
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.OrderItem.UpdateAgroOrder, _ID);
                  
                    driverDb.NewSqlParameterArray(3);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Date", _dateDoc);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Mobitel_Id", _Mobitel_Id );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Comment", _Remark );
                    driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
        public override bool DeleteDocTest()
            {
                return true;
            }
        public override bool TestDemo()
        {
            if (GlobalVars.g_AGRO == (int)Consts.RegimeType.Demo)
            {
                int cnt = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    _sSQL = AgroQuery.OrderItem.SelectAgroOrderCount;

                    cnt = driverDb.GetScalarValueNull<Int32>(_sSQL, 0);
                }
                if (cnt >= Consts.AGRO_LIMIT + 1000)
                {
                    XtraMessageBox.Show(Resources.AgroLimit + " " + Consts.AGRO_LIMIT.ToString() + "!", Resources.ApplicationName);
                    _ID = 0;
                    return false;
                }
            }
            return true;
        }
        public override DataTable GetContent()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                _sSQL = string.Format( AgroQuery.OrderItem.SelectAgroOrdert, Resources.Move1, _ID, Consts.ZONE_OUTSIDE_THE_LIST,
                                          Resources.FieldUnknown );
               

                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ������������ ���� + ������ ��� ����� ��� ������������ "�������"
        /// </summary>
        /// <returns></returns>
        public DataTable GetContentGrouped()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
             _sSQL = string.Format(AgroQuery.OrderItem.SelectIfAgroOrdert, Resources.Move1, _ID, Consts.ZONE_OUTSIDE_THE_LIST, Resources.FieldUnknown );
             
                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ������ ������� �� ������� ������
        /// </summary>
        public DataTable GetFuelDUT()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                _sSQL = string.Format( AgroQuery.OrderItem.SelectAgroOrdertId, Resources.Move1, _ID, Consts.ZONE_OUTSIDE_THE_LIST,
                                      Resources.FieldUnknown );
                
                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ������� ���� ������ � ��������
        /// </summary>
        /// <returns></returns>
        public DataTable GetFuelDUTFueling()
        {
            OrderFueling of = new OrderFueling(_ID);
            return of.GetRecords(); 
        }

        /// <summary>
        /// ������ ������� �� ������� ������� �������
        /// </summary>
        public DataTable GetFuelDRT()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
               _sSQL = string.Format( AgroQuery.OrderItem.SelectAgroOrdertIf, Resources.Move1, _ID, Consts.ZONE_OUTSIDE_THE_LIST,
                                     Resources.FieldUnknown );
                
                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ������,����������� ��������� ������
        /// </summary>
        public DataTable GetControlData(int DataType)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
               _sSQL = string.Format( AgroQuery.OrderItem.SelectAgroOrdertControl, DataType, _ID );
                
                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ��� ������,����������� ��������� ������
        /// </summary>
        public DataTable GetControlDataAll()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
               _sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrdertControlFrom, _ID);
               return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ��� ��������������� ������ ������
        /// </summary>
        public DataTable GetLockedData()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                _sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrdertFrom, _ID);
                return driverDb.GetDataTable(_sSQL);
            }
        }
        /// <summary>
        /// ���������� ����� �� ������������ �������� �� ����
        /// </summary>
        /// <returns></returns>
        public bool Exist(ref string MessageAboutExist)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                _sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrderId, Consts.msSqlvehicleIdent, _Mobitel_Id);
               
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Y", _dateDoc.Year);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "M", _dateDoc.Month);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "D", _dateDoc.Day);
                driverDb.GetDataReader(_sSQL, driverDb.GetSqlParameterArray);
                if (driverDb.Read())
                {
                    _ID = driverDb.GetInt32("Id");
                    MessageAboutExist += string.Format("{0}\t{1}\t{2}\n", driverDb.GetInt32("Id").ToString(), driverDb.GetDateTime("Date").ToString(), driverDb.GetString("Veh"));
                    driverDb.CloseDataReader(); 
                    return true;
                }
                else
                    return false;
             }
        }
        //��������� �������� ����� ���������� �� ����
        public override void UpdateDocTotals()
        {
            SetStatusEvent(Resources.DateDayCreate);
            _stateOrder = GetStateOrder();
            BaseReports.Procedure.IAlgorithm brKmDay = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.KilometrageDays();
            brKmDay.SelectItem(m_row);
            brKmDay.Run();
            atlantaDataSet.KilometrageReportDayRow[] km_Day_rows = (atlantaDataSet.KilometrageReportDayRow[])_dsAtlanta.KilometrageReportDay.Select("MobitelId = " + _Mobitel_Id);
            if (km_Day_rows.Length > 0)
            {
                DateTime InitialTime = km_Day_rows[0].InitialTime;
                DateTime FinalTime = (km_Day_rows.Length == 1) ? km_Day_rows[0].FinalTime : km_Day_rows[km_Day_rows.Length - 1].FinalTime;
                string LocationStart = (km_Day_rows[0].LocationStart.Length > 250) ? km_Day_rows[0].LocationStart.Substring(0, 250) : km_Day_rows[0].LocationStart;
                string LocationEnd = (km_Day_rows[km_Day_rows.Length - 1].LocationEnd.Length > 250) ? km_Day_rows[km_Day_rows.Length - 1].LocationEnd.Substring(0, 250) : km_Day_rows[km_Day_rows.Length - 1].LocationEnd;
                TimeSpan tsWork;
                TimeSpan IntervalWork = TimeSpan.Zero;
                TimeSpan IntervalMove = TimeSpan.Zero;
                double Distance = 0;
                double AverageSpeed =0 ;
                for (int i = 0; i < km_Day_rows.Length; i++)
			    {
                    if (TimeSpan.TryParse(km_Day_rows[i].IntervalWork, out tsWork))
                    {
                        IntervalWork = IntervalWork.Add(tsWork); 
                    }
                    if (TimeSpan.TryParse(km_Day_rows[i].IntervalMove, out tsWork))
                    {
                       IntervalMove = IntervalMove.Add(tsWork);
                    }
                    Distance += km_Day_rows[i].Distance;
                    AverageSpeed += km_Day_rows[i].AverageSpeed;
			    }
                AverageSpeed = AverageSpeed / km_Day_rows.Length;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                        GetSquareWork();
                        GetPathWithoutWork();
                    string sSQLupdate = "";
                    sSQLupdate = string.Format( AgroQuery.OrderItem.UpdateAgroOrderExist, _ID );
                    
                    driverDb.NewSqlParameterArray(27);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", InitialTime);
                        driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", FinalTime );
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeWork", IntervalWork.ToString().Substring(0, 5));
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeMove", IntervalMove.ToString().Substring(0, 5));
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LocationStart", LocationStart);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LocationEnd", LocationEnd);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Distance", Math.Round(Distance, 2));
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SpeedAvg", Math.Round(AverageSpeed, 2));
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareWorkDescript", SquareWorkDescript);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PathWithoutWork", PathWithoutWork);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensSquare", FuelDUTExpensSquare);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensAvgSquare", FuelDUTExpensAvgSquare);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensWithoutWork", FuelDUTExpensWithoutWork);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensAvgWithoutWork", FuelDUTExpensAvgWithoutWork);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensSquare", FuelDRTExpensSquare);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensAvgSquare", FuelDRTExpensAvgSquare);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensWithoutWork", FuelDRTExpensWithoutWork);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensAvgWithoutWork", FuelDRTExpensAvgWithoutWork);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareWorkDescripOverlap", SquareWorkDescriptOverlap);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactSquareCalcOverlap", FactSquareCalcOverlap);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateLastRecalc", DateTime.Now);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserLastRecalc", UserBaseCurrent.Instance.Name);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsValidity", _PointsValidity);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsCalc", _PointsCalc);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsFact", _PointsFact);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsIntervalMax", _PointsIntervalMax);
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "StateOrder", _stateOrder);

                        driverDb.ExecuteNonQueryCommand(sSQLupdate, driverDb.GetSqlParameterArray);
                        _dateLastRecalc = DateTime.Now;
                        _userRecalced = UserBaseCurrent.Instance.Name;
                    }
                    return;
            }
        }

        public override bool GetDocById(int id)
        {
            try
            {
               _sSQL = string.Format( AgroQuery.OrderItem.SelectAgroOrderVehicle, AgroQuery.SqlVehicleIdent, id );
               
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(_sSQL);
                    if (driverDb.Read())
                    {
                        _Mobitel_Id = driverDb.GetInt32("Id_mobitel");
                        _Team_id = (int)TotUtilites.NDBNullReader(driverDb, "Team_id", 0);
                        _Remark = TotUtilites.NDBNullReader(driverDb, "Comment", "").ToString();
                        _dateDoc = driverDb.GetDateTime("Date");
                        _dateLastRecalc = (DateTime)TotUtilites.NDBNullReader(driverDb, "DateLastRecalc", _dateDoc);
                        _MobitelName = TotUtilites.NDBNullReader(driverDb, "MobitelName", "").ToString();
                        _LocationStart = TotUtilites.NDBNullReader(driverDb, "LocationStart", "").ToString();
                        _LocationEnd = TotUtilites.NDBNullReader(driverDb, "LocationEnd", "").ToString();
                        _SpeedAvg = (double)TotUtilites.NDBNullReader(driverDb, "SpeedAvg", 0.0);
                        _Distance = (double)TotUtilites.NDBNullReader(driverDb, "Distance", 0.0);
                        _TimeStart = (DateTime)TotUtilites.NDBNullReader(driverDb, "TimeStart", DateTime.MinValue);
                        _TimeEnd = (DateTime)TotUtilites.NDBNullReader(driverDb, "TimeEnd", DateTime.MinValue);
                        _TimeWork = TotUtilites.NDBNullReader(driverDb, "TimeWork", "").ToString();
                        _TimeMove = TotUtilites.NDBNullReader(driverDb, "TimeMove", "").ToString();
                        //TimeSpanConverter tsc = new TimeSpanConverter();
                        TimeSpan tsWork;
                        if (TimeSpan.TryParse(_TimeWork, out tsWork))
                        {
                            TimeSpan tsMove;
                            if (TimeSpan.TryParse(_TimeMove, out tsMove))
                            {
                                _TimeStop = tsWork.Subtract(tsMove).ToString().Substring(0, 5);
                            }
                        }
                        _PathWithoutWork = (double)TotUtilites.NDBNullReader(driverDb, "PathWithoutWork", 0);
                        _factSquareCalcOverlap = (double)TotUtilites.NDBNullReader(driverDb, "FactSquareCalcOverlap", 0);
                        _SquareWorkDescript = TotUtilites.NDBNullReader(driverDb, "SquareWorkDescript", "").ToString();
                        _SquareWorkDescriptOverlap = TotUtilites.NDBNullReader(driverDb, "SquareWorkDescripOverlap", "").ToString();
                        _FuelDUTExpensSquare = (double)TotUtilites.NDBNullReader(driverDb, "FuelDUTExpensSquare", 0.0);
                        _FuelDUTExpensAvgSquare = (double)TotUtilites.NDBNullReader(driverDb, "FuelDUTExpensAvgSquare", 0.0);
                        _FuelDUTExpensWithoutWork = (double)TotUtilites.NDBNullReader(driverDb, "FuelDUTExpensWithoutWork", 0.0);
                        _FuelDUTExpensAvgWithoutWork = (double)TotUtilites.NDBNullReader(driverDb, "FuelDUTExpensAvgWithoutWork", 0.0);
                        _FuelDRTExpensSquare = (double)TotUtilites.NDBNullReader(driverDb, "FuelDRTExpensSquare", 0.0);
                        _FuelDRTExpensAvgSquare = (double)TotUtilites.NDBNullReader(driverDb, "FuelDRTExpensAvgSquare", 0.0);
                        _FuelDRTExpensWithoutWork = (double)TotUtilites.NDBNullReader(driverDb, "FuelDRTExpensWithoutWork", 0.0);
                        _FuelDRTExpensAvgWithoutWork = (double)TotUtilites.NDBNullReader(driverDb, "FuelDRTExpensAvgWithoutWork", 0.0);

                        _userCreated = TotUtilites.NDBNullReader(driverDb, "UserCreated", "").ToString();
                        _userRecalced = TotUtilites.NDBNullReader(driverDb, "UserLastRecalc", "").ToString();

                        _PointsValidity = (int)TotUtilites.NDBNullReader(driverDb, "PointsValidity", 0);
                        _PointsCalc = (int)TotUtilites.NDBNullReader(driverDb, "PointsCalc", 0);
                        _PointsFact = (int)TotUtilites.NDBNullReader(driverDb, "PointsFact", 0);
                        _PointsIntervalMax = TotUtilites.NDBNullReader(driverDb, "PointsIntervalMax", 0).ToString();

                        _stateOrder = (int)TotUtilites.NDBNullReader(driverDb, "StateOrder", 0);
                        _blockUser = (int)TotUtilites.NDBNullReader(driverDb, "BlockUserId", 0);
                        _blockStartDate = (DateTime)(TotUtilites.NDBNullReader(driverDb, "BlockDate", DateTime.MinValue));
                        Remark = TotUtilites.NDBNullReader(driverDb, "Comment", "").ToString(); ;
                    }
                    driverDb.CloseDataReader();
                }
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show( ex.Message + "\n" + ex.StackTrace, "OrderItem.GetDocById(int id):612" );  
                return false;
            }
        }

        public void UpdateStateOrder()
        {
            _stateOrder = GetStateOrder();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format(AgroQuery.OrderItem.UpdateAgroOrderState, _stateOrder , _ID);
                driverDb.ExecuteNonQueryCommand(sql);
            }
        }

        public override void UpdateBlockState(int id_user, DateTime? blockDate)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = "";
                sql = string.Format( Agro.AgroQuery.OrderItem.UpdateAgroOrderBlock, _ID );
                
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "BlockUserId", id_user);

                if(blockDate == null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "BlockDate", DBNull.Value);
                else
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "BlockDate", blockDate );

                driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                _blockUser = id_user;
            }
        }

        public void UpdateDocAfterSplitRecords()
        {
            if (_dsAtlanta == null)
            {
                if (Create_dsAtlanta(_dateDoc, _dateDoc.AddDays(1).AddMinutes(-1)) == 0)
                {
                    SetStatusEvent(Resources.Ready);
                    return;
                }
                _oirs = new List<OrderItemRecord>();
            }
            SetStatusEvent(Resources.DateDayCreate);
            _oirs.Clear();
            _oirs = GetOrderItemRecords(); 
            GetSquareWork();
            UpdateDocTotals();
            ContentDataSetReturnClear();
            UserLog.InsertLog(UserLogTypes.AGRO, Resources.OrderRecordsCorrection, ID);
        }
        #endregion

    #region ���������� �������
       /// <summary>
       /// ��������� ������� �� ��������� � ��������� ��� �����.
       /// ��������� ������ ��� ��������� ���������� �������
       /// ������� ������� ������ ����������� ��� ������� �� �����, ���������, ���������
       /// ��������� 05.10.2011 �� ��������� �������: ���������� ����������� ������ � ���� ��� - ����� ������� ��������� ��� �� ����
       /// </summary>
       private void ContentCalcControlObject(Consts.TypeControlObject tco, string sFieldObj)
       {
           using (DriverDb driverDb = new DriverDb())
           {
               driverDb.ConnectDb(); 
               DateTime LastControlTime = ContentGetLastControlTime(tco);
               string sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrderObj, _ID);
           
               driverDb.NewSqlParameterArray(1);
               if(LastControlTime.Year <= 1900)
                   LastControlTime = new DateTime(1900, 1, 1);
               driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LastControlTime", LastControlTime);
               driverDb.GetDataReader(sSQL, driverDb.GetSqlParameterArray);

                   int Id_obj_cur = 0;
                   int Id_obj_prev = 0;
                   DateTime dtStart = new DateTime();
                   DateTime dtEnd = new DateTime();
                   bool bFirst = true;
                   while (driverDb.Read())
                   {
                       if (bFirst)
                       {
                           dtStart = driverDb.GetDateTime(driverDb.GetOrdinal("TimeStart"));

                           if (LastControlTime.Subtract(dtStart).TotalSeconds > 0) 
                               dtStart = LastControlTime;

                           Id_obj_prev = driverDb.GetInt32(driverDb.GetOrdinal(sFieldObj));
                           bFirst = false;
                       } // if

                       Id_obj_cur = driverDb.GetInt32(driverDb.GetOrdinal(sFieldObj));

                       if (Id_obj_cur != Id_obj_prev)
                       {
                           //������ ���������� ��� ����������� ������
                           InsertControlObject(tco, driverDb, ref sSQL, Id_obj_prev, dtStart, dtEnd);
                           dtStart = dtEnd; //driverDb.GetDateTime(driverDb.GetOrdinal("TimeEnd"));
                           Id_obj_prev = Id_obj_cur;
                       } // if
                       dtEnd = driverDb.GetDateTime("TimeEnd");
                    } // while
                   if (!bFirst)
                   {
                       InsertControlObject(tco, driverDb, ref sSQL, Id_obj_prev, dtStart, dtEnd);
                   } // if
                   driverDb.CloseDataReader();
           } // using
       } // ContentCalcControlObject

       private void InsertControlObject(Consts.TypeControlObject tco, DriverDb driverDb, ref string sSQL, int Id_obj_prev, DateTime dtStart, DateTime dtEnd)
       {
           sSQL = String.Format( AgroQuery.OrderItem.InsertIntoAgroOrdertControl, _ID, Id_obj_prev, (int)tco, dtStart, dtEnd );
          
           using (DriverDb nestedDriverDb = new DriverDb())
           {
               nestedDriverDb.ConnectDb();
               nestedDriverDb.NewSqlParameterArray(2);
               nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", dtStart);
               nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", dtEnd);
               nestedDriverDb.ExecuteNonQueryCommand(sSQL, nestedDriverDb.GetSqlParameterArray);
           }
       }
       /// <summary>
       /// �������� ������������ ��������� �� ����
       /// </summary>
       /// <param name="dt"> ����� ����������� ������ </param>
       /// <param name="tco"> ��� ��������� </param>
       /// <param name="dtFind"> ���� ��� ������ </param>
       /// <returns></returns>
       private int ContentGetControlObject(DataTable dt, Consts.TypeControlObject tco, DateTime dtFind)
       {
           foreach (DataRow dr in dt.Rows)
           {
               if ((Convert.ToInt32(dr["Type_object"]) == (int)tco) && (dtFind.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalMinutes >= 0)
                   && (dtFind.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalMinutes < 0))
                   return Convert.ToInt32(dr["Id_object"]);
           }
           return 0;
       }
       /// <summary>
       /// �������� ������������ ��������� �� ���� �����
       /// </summary>
       /// <param name="dt"> ����� ����������� ������ </param>
       /// <param name="tco"> ��� ��������� </param>
       /// <param name="dtFind"> ���� ������ ������ </param>
       /// <param name="dtEnd">���� ���������</param>
       /// <returns></returns>
       private int ContentGetControlObjectExactly( Consts.TypeControlObject tco, DateTime dtStart, OrderItemRecord oir)
       {
           if (_dtControl.Rows.Count == 0)
               if (TestPathSquareZone(oir))
                   return 1;
               else
                   return 0;
           DateTime dtEnd = oir.LsDrows[oir.LsDrows.Count - 1].time;
           foreach (DataRow dr in _dtControl.Rows)
           {
               if ((Convert.ToInt32(dr["Type_object"]) == (int)tco) && ((int)dtStart.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalSeconds == 0)
                   && ((int)(dtEnd.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalSeconds) == 0))
                   return Convert.ToInt32(dr["Id_object"]);
           }
           return 0;
       }

       private bool ContentPointLocked(atlantaDataSet.dataviewRow d_row)
       {
           if (_dtLock.Rows.Count == 0) return false;
           DateTime dtTest = d_row.time;
           foreach (DataRow dr in _dtLock.Rows)
           {
               if (((int)d_row.time.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalSeconds > 0)
                   && ((int)(d_row.time.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalSeconds) <= 0))
                   return true;
           }
           return false;
       }
       /// <summary>
       /// �������� ���� ����������� ����������
       /// </summary>
       private bool ContentDeleteControlObjects()
       {
           try
           {
               using (DriverDb driverDb = new DriverDb())
               {
                   driverDb.ConnectDb(); 
                   _sSQL = string.Format(AgroQuery.OrderItem.DeleteAgroOrdertControl, _ID);
                   driverDb.ExecuteNonQueryCommand(_sSQL);
               }
               return true;
           }
           catch
           {
               return false;
           }

       }
        /// <summary>
       /// ���������  ���� � ���������� �������
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="tco"></param>
        /// <returns></returns>
       private DateTime ContentGetLastControlTime(Consts.TypeControlObject tco)
       {
           using (DriverDb driverDb = new DriverDb())
           {
               driverDb.ConnectDb();
               string sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrdertControlTime, _ID, (int)tco );
               
               driverDb.GetDataReader(sSQL);
               DateTime timeForReturn = _TimeStart;
               if (driverDb.Read())
                {
                    timeForReturn =  driverDb.GetDateTime("TimeEnd");
                }
               driverDb.CloseDataReader();
               return timeForReturn;
               
           }
       }

       private int GetStateOrder()
       {
           _dtLock = GetLockedData(); 
           int lockedRecords = _dtLock.Rows.Count ;
           int totalRecords = GetRecordsCount();
           if (totalRecords == 0 || lockedRecords == 0)
               return (int)StatesOrder.New;
           else if (totalRecords > lockedRecords)
               return (int)StatesOrder.Updated;
           else
           {
               if (_lastGpsTime == DateTime.MinValue) _lastGpsTime = GetLastDataGps();
               if (_lastGpsTime == DateTime.MinValue) return (int)StatesOrder.Tested;
               if (_dateDoc.AddDays(1).Subtract(_lastGpsTime).TotalMinutes < 5)
                   return (int)StatesOrder.Closed;
               else
                   return (int)StatesOrder.Tested;
           }
       }

       public int GetRecordsCount()
       {
           using (DriverDb driverDb = new DriverDb())
           {
               driverDb.ConnectDb(); 
               _sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrdertCount, _ID);
               return driverDb.GetScalarValueNull<int>(_sSQL, 0);
           }
       }

       public DateTime GetLastDataGps()
       {
           return MobitelProvider.GetLastTimeGps(_Mobitel_Id);  
       }

       public bool LockRecords(bool lockRecord)
       {
           try
           {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format(AgroQuery.OrderItem.UpdateAgroOrdertRecord, lockRecord ? 1 : 0, _ID);
                    driverDb.ExecuteNonQueryCommand(sql);
                    return true;
                }
           }
           catch
           {
               return false;
           }
       }
    #endregion

    #region Atlanta
            atlantaDataSet.mobitelsRow m_row;
            atlantaDataSet.settingRow set_row;
            TimeSpan _legalTimeBreak = new TimeSpan(0, 5, 0); 
            public int Create_dsAtlanta(DateTime dtStart,DateTime dtEnd)
            {
                if (_dsAtlanta==null) _dsAtlanta = new atlantaDataSet();
                SetStatusEvent(Resources.DataInitSelect);
                LocalCacheItem lci = new LocalCacheItem(dtStart, dtEnd, _Mobitel_Id);
                int iRecords = lci.CreateDSAtlanta(ref _dsAtlanta);
                if (iRecords == 0) return 0;
                m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
                if (m_row == null) return 0;
                m_row.Check = true;
                VehicleInfo vehicle = new VehicleInfo(m_row);
                atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();

                set_row = (vehicles == null || vehicles.Length == 0) ?
                  null : _dsAtlanta.setting.FindByid(vehicles[0].setting_id);

                _Vehicle_Id = (vehicles == null || vehicles.Length == 0) ? 0 : vehicles[0].id;

                if (set_row != null)
                {
                    _legalTimeBreak = set_row.TimeBreak;
                }
                //����������� ���������� �������� ������ ������ ������������.� ����� ��������� �������� ��������� 
                _dsFromAlgorithm = Algorithm.AtlantaDataSet;
                Algorithm.RunFromModule = true;
                Algorithm.AtlantaDataSet = _dsAtlanta;
                SetMinMaxGPS(_Mobitel_Id);
                return iRecords;
               
            }
            #endregion

    #region Get
            private void GetSensor(AlgorithmType alg,  out int senLength, out int senStartBit)
            {
                Algorithm Alg = new Algorithm();
                Alg.SelectItem(m_row);
                atlantaDataSet.sensorsRow sensor = Alg.FindSensor(alg);
                if (sensor != null)
                {
                    senLength = sensor.Length;
                    senStartBit = sensor.StartBit;
                }
                else
                {
                    senLength = 0;
                    senStartBit = 0;
                }
            }

            /// <summary>
                /// ����������� ��������� ��������� ��� ����� GPS
            /// </summary>
            /// <param name="DataGps_ID"></param>
            /// <returns></returns>
            private bool GetFactZoneRotation(System.Int64 DataGps_ID)
            {
                atlantaDataSet.RotateReportRow[] rt_rows = (atlantaDataSet.RotateReportRow[])_dsAtlanta.RotateReport.Select("InitialPointId <= " + DataGps_ID + " AND FinalPointId > =" + DataGps_ID);
                if (rt_rows.Length == 0) return false;
                if (rt_rows[0].State == Resources.Worked) return true;
                return false;
            }
            /// <summary>
                /// ����������� ���� �� ������
            /// </summary>
            /// <param name="iWorkType"></param>
            /// <returns></returns>
            private double GetPrice(int iWorkType)
            {
                if ((iWorkType == 0) || (_Mobitel_Id ==0)) return 0;
                double dbPrice = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string SQLselect = string.Format(AgroQuery.OrderItem.SelectAgroPricetPrice, iWorkType, _Mobitel_Id);
                    
                    //Console.WriteLine(SQLselect);  
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateComand", _dateDoc);
                    driverDb.GetDataReader(SQLselect, driverDb.GetSqlParameterArray);
                    if (driverDb.Read())
                    {
                        double dbFactor = driverDb.GetDouble(driverDb.GetOrdinal("Factor"));
                        dbPrice = Math.Round(driverDb.GetDouble(driverDb.GetOrdinal("Price")) * dbFactor / 100, 2);
                    }
                    driverDb.CloseDataReader();
                }
                return dbPrice;
            }

            private int GetZone(int ID_Field)
            {
                using (DictionaryAgroField daf = new DictionaryAgroField(ID_Field))
                {
                    return daf.Id_zone;
                }
            }

            private int GetFieldId(int ZoneId)
            {
                if (ZoneId < 0)
                {
                    return -ZoneId;
                }
                else if (ZoneId == 0)
                {
                    return 0;
                }
                else
                {
                    using (DictionaryAgroField daf = new DictionaryAgroField())
                    {
                        return daf.GetIdFromZoneId(ZoneId);
                    }
                }
            }

            #endregion

    #region IDisposable Members
    public override  void Dispose()
    {
        if (_oirs != null) _oirs.Clear() ;
        if (_dsAtlanta!=null) _dsAtlanta.Clear();
    }
    #endregion


    }
}
