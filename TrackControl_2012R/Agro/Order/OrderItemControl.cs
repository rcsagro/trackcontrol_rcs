﻿using System;
using System.Collections.Generic;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    /// <summary>
    /// управляющие данные наряда
    /// </summary>
    public class OrderItemControl
    {
        int _id_main;

        public OrderItemControl(int id_main)
        {
            _id_main = id_main;
        }

        public int AddRecord(int id_object,Consts.TypeControlObject tco ,DateTime start,DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format("INSERT INTO agro_ordert_control (Id_main ,Id_object, Type_object,TimeStart,TimeEnd )"
                + " VALUES ({0},{1},{2},{3}TimeStart,{3}TimeEnd)", _id_main, id_object, (int)tco, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", end );
                return driverDb.ExecuteReturnLastInsert( sql, driverDb.GetSqlParameterArray, "agro_ordert_control" );
            }
        }

        public bool UpdateRecord(int id_object, int id_record, DateTime start, DateTime end)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format("UPDATE agro_ordert_control SET Id_object = {0}, "
                    + "TimeStart = {2}TimeStart , TimeEnd = {2}TimeEnd WHERE ID = {1}", id_object, id_record, driverDb.ParamPrefics);
                     driverDb.NewSqlParameterArray(2);
                     driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeStart", start );
                     driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", end );
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<OrderItemControlRecord> GetListTypeControlBetweenDates(Consts.TypeControlObject tco, DateTime start, DateTime end)
        {
            List<OrderItemControlRecord> oicrs = new List<OrderItemControlRecord>() ;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format(@"SELECT *
                FROM   agro_ordert_control
                WHERE
                agro_ordert_control.Id_main = {0}
                AND agro_ordert_control.Type_object = {1}
                AND (agro_ordert_control.TimeStart <= {2}TimeStart and agro_ordert_control.TimeEnd >= {2}TimeEnd)", _id_main, (int)tco, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeStart", start );
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", end );
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                while (driverDb.Read())
                {
                    OrderItemControlRecord oicr = new OrderItemControlRecord();
                    oicr.Id_record = driverDb.GetInt32("Id");
                    oicr.Id_object = driverDb.GetInt32("Id_object");
                    oicr.TypeObject  = (Consts.TypeControlObject)driverDb.GetInt32("Type_object");
                    oicr.TimeStart = driverDb.GetDateTime("TimeStart");
                    oicr.TimeEnd = driverDb.GetDateTime("TimeEnd");
                    oicrs.Add(oicr); 
                }
                driverDb.CloseDataReader(); 
            }
            return oicrs;
        }


    }
}
