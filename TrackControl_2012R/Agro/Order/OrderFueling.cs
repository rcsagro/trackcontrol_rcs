﻿using System;
using System.Collections.Generic;
using System.Data;
using Agro.Properties;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    /// <summary>
    /// заправки и сливы , обнаруженные во время наряда 
    /// </summary>
    public class OrderFueling
    {
        PointLatLng _point;
        double _valueSystem;
        double _valueStart;
        int _id_main;
        string _location;
        string _remark;
        /// <summary>
        /// значения , проставленные диспетчером
        /// </summary>
        double _valueDisp;
        public double ValueDisp
        {
            get { return _valueDisp; }
            set { _valueDisp = value; }
        }
        DateTime _dateFueling;
        /// <summary>
        /// дата заправки / слива
        /// </summary>
        public DateTime DateFueling
        {
            get { return _dateFueling; }
            set { _dateFueling = value; }
        }
        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public OrderFueling(int id_main)
        {
            _id_main = id_main;
        }

        public OrderFueling(int Id, DateTime dateFueling, double valueDisp)
        {
            _id = Id;
            _dateFueling = dateFueling;
            _valueDisp = valueDisp;
        }

        public int AddRecord(PointLatLng point, DateTime dateFueling, double valueStart, double valueSystem, double valueDisp, string location)
        {
            _point = point;
            _dateFueling = dateFueling;
            _valueStart = valueStart;
            _valueSystem = valueSystem;
            _valueDisp = valueDisp;
            _location = location;
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();

                    string sql = AgroQuery.OrderFueling.InsertIntoAgroFueling;

                    driverDb.NewSqlParameterArray(8);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", _id_main);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Lat", _point.Lat );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Lng", _point.Lng );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "DateFueling", _dateFueling );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "ValueStart", Math.Round( _valueStart, 2 ) );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "ValueSystem", Math.Round( _valueSystem, 2 ) );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "ValueDisp", Math.Round( _valueDisp, 2 ) );
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Location", _location );
                    _id = driverDb.ExecuteReturnLastInsert( sql, driverDb.GetSqlParameterArray, "agro_fueling" );
                    if (valueSystem == 0 && _valueDisp !=0)
                    {
                        string toLog = string.Format("{0}.{1}->{2}: {3}", Resources.Fuel, Resources.Adding, Math.Round(_valueDisp, 2), _dateFueling);
                        UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);
                    }
                    return _id;
                }
            }
            catch
            {
                return ConstsGen.RECORD_MISSING;
            }
        }

        public bool ClearRecords()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(AgroQuery.OrderFueling.DeleteFromAgroFueling,_id_main);
                    driverDb.ExecuteNonQueryCommand(sql);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataTable GetRecords()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format( AgroQuery.OrderFueling.SelectFromAgroFueling, _id_main );
                return driverDb.GetDataTable(sql);
            }
        }

        public bool UpdateLock(int id ,bool locked,DateTime dateRecord)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format( AgroQuery.OrderFueling.UpdateFromAgroFueling, id );
                    
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LockRecord", locked ? 0 : 1);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    string toLog = string.Format("{0}.{1}->{2}: {3}", Resources.Fuel, Resources.SetAPPROVED, locked ? 0 : 1, dateRecord);
                    UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);     
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateValueDisp(int id, double  valueDisp, DateTime dateRecord)
        {
            _valueDisp = valueDisp;
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format( AgroQuery.OrderFueling.UpdateAgroFueling, id );
                    
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "ValueDisp", valueDisp);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    string toLog = string.Format("{0}.{1}->{2}: {3}", Resources.Fuel, Resources.SetDispValue, valueDisp, dateRecord);
                    UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateRemark(int id, string remark, DateTime dateRecord)
        {
            _remark = remark;
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format( AgroQuery.OrderFueling.UpdateAgroFuelingRem, id );
                    
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Remark", remark);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    string toLog = string.Format("{0}.{1}->{2}: {3}", Resources.Fuel, Resources.SetRemark, remark, dateRecord);
                    UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDateFueling(int id, DateTime dateRecord)
        {
            _dateFueling = dateRecord;
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format( AgroQuery.OrderFueling.UpdateAgroFuelingSet, id );

                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateFueling", dateRecord);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    string toLog = string.Format("{0}.{1}->{2}", Resources.Fuel, Resources.Date, _dateFueling);
                    UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateOrderRecord(DateTime dateRecord)
        {
            try
            {
                    int id_ordert = OrderItemRecord.GetIdByDate(_id_main, dateRecord);
                    if (id_ordert == ConstsGen.RECORD_MISSING) return false;
                    using (OrderItemRecord oir = new OrderItemRecord(id_ordert))
                    {
                        oir.FuelAdd = GetTotalAddSub(oir.TimeStart, oir.TimeEnd,">");
                        oir.FuelSub = -GetTotalAddSub(oir.TimeStart, oir.TimeEnd, "<");
                        oir.RecalcFuelExpensDUT();
                        oir.SetFuelDUT(); 
                    }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public double GetTotalAddSub(DateTime timeStart, DateTime timeEnd,string sign)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sql = string.Format( AgroQuery.OrderFueling.SelectAgroFuelingSum, _id_main, sign );
                
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", timeStart);
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "TimeEnd", timeEnd );
                return driverDb.GetScalarValueDblNull(sql, driverDb.GetSqlParameterArray);
            }
        }

        public bool IsExistLockedValueToTime(List<OrderFueling> ofs,DateTime dateValue, out double valueFueling, out int recordId)
        {
            valueFueling = 0;
            recordId = 0;
            foreach (OrderFueling of in ofs)
            {
                if (of.DateFueling == dateValue)
                {
                    valueFueling = of.ValueDisp;
                    recordId = of.Id ;
                    return true;
                }
            }
            return false;
        }

        public List<OrderFueling> GetRecordsLocked()
        {
            List<OrderFueling> records = new List<OrderFueling>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format(AgroQuery.OrderFueling.SelectAgroFuelingId, _id_main);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    OrderFueling of = new OrderFueling((int)driverDb.GetInt32("Id"), driverDb.GetDateTime("DateFueling"), driverDb.GetDouble("ValueDisp"));
                    records.Add(of); 
                }
                driverDb.CloseDataReader();
                return records;
            }
        }

        public bool DeleteRecord(int id,double dispValue, DateTime dateRecord)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 

                    string sql = string.Format(AgroQuery.OrderFueling.DeleteAgroFuelingId, id);

                    driverDb.ExecuteNonQueryCommand(sql);
                    string toLog = string.Format("{0}.{1}->{2}: {3}", Resources.Fuel, Resources.RecordDelete,dispValue, dateRecord);
                    UserLog.InsertLog(UserLogTypes.AGRO, toLog, _id_main);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
