﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using TrackControl.General.DatabaseDriver;
using Agro.Utilites;
using TUtil = Agro.Utilites.TotUtilites;
using TrackControl.General;
using Agro.Properties;

namespace Agro
{
    public static class PlanTController
    {
        public static void Save(PlanTRecord record)
        {
            using (DriverDb driverDb = new DriverDb())
            {
               driverDb.ConnectDb();
               if (record.Id == 0)
               {
                   InsertRecord(record, driverDb);
               }
               else
               {
                   UpdateRecord(record, driverDb);
               }
            }
        }

        private static void InsertRecord(PlanTRecord record, DriverDb driverDb)
        {
            string sSQL = string.Format(@"INSERT INTO agro_plant(
                    Id_main
                    ,Id_field
                    ,Id_driver
                    ,Id_work
                    ,Id_agregat
                    ,Remark
                    ,TimeStart)
                    VALUES ( 
                    {0}Id_main 
                    ,{0}Id_field 
                    ,{0}Id_driver
                    ,{0}Id_work 
                    ,{0}Id_agregat
                    ,{0}Remark
                    ,{0}TimeStart)", driverDb.ParamPrefics);
            CreateParameters(driverDb, record);
            record.Id = driverDb.ExecuteReturnLastInsert( sSQL, driverDb.GetSqlParameterArray, "agro_plant" );
            UserLog.InsertLog(UserLogTypes.AGRO_PLAN, string.Format(Resources.LogAddContent,record.Id) , record.IdPlan);
        }

        private static void CreateParameters(DriverDb driverDb, PlanTRecord record)
        {
            driverDb.NewSqlParameterArray(7);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", record.IdPlan);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", record.IdField); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (record.IdField != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", record.IdField);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", DBNull.Value);
                }
            }

            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_driver", record.IdDriver);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", record.IdWork);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_agregat", record.IdAgregat);

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Remark", record.Remark ); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (record.Remark != null)
                {
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Remark", record.Remark );
                }
                else
                {
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "Remark", "" );
                }
            }
            if (record.TimeStartPlan == null && record.Id ==0) record.TimeStartPlan = TUtil.SetOrderDate(DateTime.Today);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", record.TimeStartPlan);
        }

        private static void UpdateRecord(PlanTRecord record, DriverDb driverDb)
        {
            string sSQL = string.Format(@"UPDATE agro_plant
                    Set Id_main = {0}Id_main 
                    ,Id_field = {0}Id_field 
                    ,Id_driver = {0}Id_driver
                    ,Id_work = {0}Id_work 
                    ,Id_agregat = {0}Id_agregat
                    ,Remark = {0}Remark
                    ,TimeStart = {0}TimeStart WHERE Id = {1}", driverDb.ParamPrefics, record.Id);
            CreateParameters(driverDb, record);
            driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_plant");
            UserLog.InsertLog(UserLogTypes.AGRO_PLAN, string.Format(Resources.LogUpdateContent,record.Id), record.IdPlan);
        }

        public static bool Validate(PlanTRecord record)
        {
            if (record.IdDriver > 0 || record.IdField > 0)
                return true;
            else
                return false; 
        }

        public static BindingList<PlanTRecord> GetList(int idPlan)
        {
            BindingList<PlanTRecord> records = new BindingList<PlanTRecord>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format("SELECT * FROM agro_plant WHERE agro_plant.Id_main = {0}", idPlan);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    PlanTRecord record = new PlanTRecord();
                    record.Id = driverDb.GetInt32("Id"); 
                    record.IdPlan  = driverDb.GetInt32("Id_main");
                    record.IdDriver = (int)TotUtilites.NDBNullReader(driverDb, "Id_driver", 0);
                    record.IdField = (int)TotUtilites.NDBNullReader(driverDb, "Id_field", 0);
                    record.IdAgregat = (int)TotUtilites.NDBNullReader(driverDb, "Id_agregat", 0);
                    record.IdWork = (int)TotUtilites.NDBNullReader(driverDb, "Id_work", 0);
                    record.Remark = (string)TotUtilites.NDBNullReader(driverDb, "Remark", "");
                    record.TimeStartPlan = (DateTime?)TotUtilites.NDBNullReader(driverDb, "TimeStart", null);
                    records.Add(record); 
                }
                driverDb.CloseDataReader(); 
            }
            return records;
        }

        public static bool Delete(PlanTRecord record)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format("DELETE FROM agro_plant WHERE agro_plant.Id = {0}", record.Id);
                driverDb.ExecuteNonQueryCommand(sql);
                return true;
            }
        }
    }
}
