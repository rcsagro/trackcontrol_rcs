﻿using System;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Vehicles;

namespace Agro
{
    public class PlanEntity : IDocumentEntity
    {
        public PlanEntity()
        {

        }

        public Vehicle  VehiclePlan
        {
            get { return _vehicle; }
            set 
            { 
                _vehicle=value;
                _vehicleGroup = _vehicle.Group;
            }
        }

        VehiclesGroup _vehicleGroup;
        Vehicle _vehicle;
        public VehiclesGroup  VehicleGroup
        {
            get { return _vehicleGroup; }
            set { _vehicleGroup = value; }
        }
        //public string VehiclePlan { get; set; }
        public Double SquarePlanned { get; set; }
        public Double FuelAtStart { get; set; }
        public Double FuelForWork { get; set; }
        public int  PlanStatus { get; set; }
        public string UserCreated { get; set; }
        public bool IsDownTime { get; set; }
        public int? PlanDowntimeReason { get; set; }
        public DateTime? DowntimeStart { get; set; }
        public DateTime? DowntimeEnd { get; set; }

        OrderItem _order;
        IList<PlanTRecord> _planRecords;

        public IList<PlanTRecord> PlanRecords
        {
            get { return _planRecords; }
            set { _planRecords = value; }
        }
        IList<OrderItemRecord> _orderRecords;

        public IList<OrderItemRecord> OrderRecords
        {
            get { return _orderRecords; }
            set { _orderRecords = value; }
        }

        public OrderItem Order
        {
            get { return _order; }
            set { 
                _order = value;
                ConnectToOrderRecords();
                }
        }

        public void ConnectToOrderRecords()
        {
            if (_planRecords == null) _planRecords = PlanTController.GetList(ID);  
            if (_order == null) return;
            _orderRecords = _order.GetOrderItemRecords();
            if (_planRecords == null || _orderRecords == null) return;
            PlanOrderConnector connector = new PlanOrderConnector();
            connector.FillPlanRecordsOrderValues(_planRecords, _orderRecords);
        }


        #region Члены IDocumentEntity

        public int ID  { get; set; }

        public DateTime Date { get; set; }

        public string Remark { get; set; }

        #endregion

    }
}
