﻿namespace Agro
{
    partial class PlanSetView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanSetView));
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.bdeStart = new DevExpress.XtraBars.BarEditItem();
            this.deStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEnd = new DevExpress.XtraBars.BarEditItem();
            this.deEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcPlan = new DevExpress.XtraGrid.GridControl();
            this.gvPlan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFamily = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgrName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelForWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId_order = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.grDownTime = new DevExpress.XtraEditors.GroupControl();
            this.gcDownTime = new DevExpress.XtraGrid.GridControl();
            this.gvDownTime = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDtId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtPlanDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtVehicleIdent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtReasonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtDowntimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDtDowntimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grWithoutTasks = new DevExpress.XtraEditors.GroupControl();
            this.gcWithoutTasks = new DevExpress.XtraGrid.GridControl();
            this.gvWithoutTasks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWtLastDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWtLastId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWtVehicleId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWtVehicleGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWtVehicleIdent = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grDownTime)).BeginInit();
            this.grDownTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDownTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDownTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grWithoutTasks)).BeginInit();
            this.grWithoutTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcWithoutTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWithoutTasks)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiStart,
            this.bdeStart,
            this.bdeEnd});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.deStart,
            this.deEnd});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.bdeStart, "", false, true, true, 140, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEnd, "", false, true, true, 123)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "ПУСК";
            this.bbiStart.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStart.Glyph")));
            this.bbiStart.Id = 0;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // bdeStart
            // 
            this.bdeStart.Caption = "Дата с";
            this.bdeStart.Edit = this.deStart;
            this.bdeStart.Id = 1;
            this.bdeStart.Name = "bdeStart";
            this.bdeStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deStart
            // 
            this.deStart.AutoHeight = false;
            this.deStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F6);
            this.deStart.DisplayFormat.FormatString = "g";
            this.deStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.EditFormat.FormatString = "g";
            this.deStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Mask.EditMask = "g";
            this.deStart.Name = "deStart";
            this.deStart.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // bdeEnd
            // 
            this.bdeEnd.Caption = "по";
            this.bdeEnd.Edit = this.deEnd;
            this.bdeEnd.Id = 2;
            this.bdeEnd.Name = "bdeEnd";
            this.bdeEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deEnd
            // 
            this.deEnd.AutoHeight = false;
            this.deEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.DisplayFormat.FormatString = "g";
            this.deEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.EditFormat.FormatString = "g";
            this.deEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Mask.EditMask = "g";
            this.deEnd.Name = "deEnd";
            this.deEnd.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.splitContainerControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 26);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1088, 515);
            this.panelControl1.TabIndex = 4;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(2, 2);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gcPlan);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1084, 511);
            this.splitContainerControl1.SplitterPosition = 359;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gcPlan
            // 
            this.gcPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPlan.Location = new System.Drawing.Point(0, 0);
            this.gcPlan.MainView = this.gvPlan;
            this.gcPlan.MenuManager = this.barManager1;
            this.gcPlan.Name = "gcPlan";
            this.gcPlan.Size = new System.Drawing.Size(1084, 359);
            this.gcPlan.TabIndex = 0;
            this.gcPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPlan});
            // 
            // gvPlan
            // 
            this.gvPlan.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvPlan.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvPlan.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvPlan.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPlan.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPlan.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvPlan.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvPlan.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvPlan.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvPlan.Appearance.Empty.Options.UseBackColor = true;
            this.gvPlan.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPlan.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPlan.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPlan.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPlan.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvPlan.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvPlan.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvPlan.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPlan.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvPlan.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvPlan.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvPlan.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvPlan.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvPlan.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvPlan.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvPlan.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvPlan.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvPlan.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvPlan.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvPlan.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvPlan.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPlan.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPlan.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvPlan.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvPlan.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvPlan.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvPlan.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvPlan.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvPlan.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPlan.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvPlan.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvPlan.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvPlan.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvPlan.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvPlan.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPlan.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPlan.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvPlan.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPlan.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPlan.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.OddRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvPlan.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvPlan.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvPlan.Appearance.Preview.Options.UseBackColor = true;
            this.gvPlan.Appearance.Preview.Options.UseFont = true;
            this.gvPlan.Appearance.Preview.Options.UseForeColor = true;
            this.gvPlan.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPlan.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.Row.Options.UseBackColor = true;
            this.gvPlan.Appearance.Row.Options.UseForeColor = true;
            this.gvPlan.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPlan.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvPlan.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvPlan.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvPlan.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPlan.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPlan.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvPlan.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvPlan.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvPlan.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvPlan.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPlan.Appearance.VertLine.Options.UseBackColor = true;
            this.gvPlan.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPlanDate,
            this.colNumberPlate,
            this.colFamily,
            this.colAgrName,
            this.colWorkName,
            this.colFieldName,
            this.colFuelForWork,
            this.colId_order,
            this.colPlanComment});
            this.gvPlan.GridControl = this.gcPlan;
            this.gvPlan.Name = "gvPlan";
            this.gvPlan.OptionsSelection.MultiSelect = true;
            this.gvPlan.OptionsView.EnableAppearanceEvenRow = true;
            this.gvPlan.OptionsView.EnableAppearanceOddRow = true;
            this.gvPlan.DoubleClick += new System.EventHandler(this.gvPlan_DoubleClick);
            // 
            // colId
            // 
            this.colId.AppearanceCell.Options.UseTextOptions = true;
            this.colId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Номер";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 59;
            // 
            // colPlanDate
            // 
            this.colPlanDate.AppearanceCell.Options.UseTextOptions = true;
            this.colPlanDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlanDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlanDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlanDate.Caption = "Дата";
            this.colPlanDate.DisplayFormat.FormatString = "g";
            this.colPlanDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPlanDate.FieldName = "PlanDate";
            this.colPlanDate.Name = "colPlanDate";
            this.colPlanDate.OptionsColumn.AllowEdit = false;
            this.colPlanDate.OptionsColumn.ReadOnly = true;
            this.colPlanDate.Visible = true;
            this.colPlanDate.VisibleIndex = 1;
            this.colPlanDate.Width = 141;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.Caption = "Техника";
            this.colNumberPlate.FieldName = "VehicleIdent";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.Visible = true;
            this.colNumberPlate.VisibleIndex = 2;
            this.colNumberPlate.Width = 129;
            // 
            // colFamily
            // 
            this.colFamily.AppearanceHeader.Options.UseTextOptions = true;
            this.colFamily.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFamily.Caption = "Водитель";
            this.colFamily.FieldName = "Family";
            this.colFamily.Name = "colFamily";
            this.colFamily.OptionsColumn.AllowEdit = false;
            this.colFamily.OptionsColumn.ReadOnly = true;
            this.colFamily.Visible = true;
            this.colFamily.VisibleIndex = 3;
            this.colFamily.Width = 129;
            // 
            // colAgrName
            // 
            this.colAgrName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgrName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgrName.Caption = "Навесное оборудование";
            this.colAgrName.FieldName = "AgrName";
            this.colAgrName.Name = "colAgrName";
            this.colAgrName.OptionsColumn.AllowEdit = false;
            this.colAgrName.OptionsColumn.ReadOnly = true;
            this.colAgrName.Visible = true;
            this.colAgrName.VisibleIndex = 4;
            this.colAgrName.Width = 176;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.Caption = "Вид работ";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.VisibleIndex = 5;
            this.colWorkName.Width = 120;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "Поле";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 6;
            this.colFieldName.Width = 156;
            // 
            // colFuelForWork
            // 
            this.colFuelForWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelForWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelForWork.Caption = "Отпустить топлива , л";
            this.colFuelForWork.FieldName = "FuelForWork";
            this.colFuelForWork.Name = "colFuelForWork";
            this.colFuelForWork.OptionsColumn.AllowEdit = false;
            this.colFuelForWork.OptionsColumn.ReadOnly = true;
            this.colFuelForWork.Visible = true;
            this.colFuelForWork.VisibleIndex = 7;
            this.colFuelForWork.Width = 132;
            // 
            // colId_order
            // 
            this.colId_order.AppearanceHeader.Options.UseTextOptions = true;
            this.colId_order.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId_order.Caption = "Наряд";
            this.colId_order.FieldName = "Id_order";
            this.colId_order.Name = "colId_order";
            this.colId_order.OptionsColumn.AllowEdit = false;
            this.colId_order.OptionsColumn.ReadOnly = true;
            this.colId_order.Visible = true;
            this.colId_order.VisibleIndex = 8;
            this.colId_order.Width = 80;
            // 
            // colPlanComment
            // 
            this.colPlanComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlanComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlanComment.Caption = "Примечание";
            this.colPlanComment.FieldName = "PlanComment";
            this.colPlanComment.Name = "colPlanComment";
            this.colPlanComment.OptionsColumn.AllowEdit = false;
            this.colPlanComment.OptionsColumn.ReadOnly = true;
            this.colPlanComment.Visible = true;
            this.colPlanComment.VisibleIndex = 9;
            this.colPlanComment.Width = 155;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.grDownTime);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.grWithoutTasks);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1084, 146);
            this.splitContainerControl2.SplitterPosition = 669;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // grDownTime
            // 
            this.grDownTime.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grDownTime.AppearanceCaption.Options.UseFont = true;
            this.grDownTime.AppearanceCaption.Options.UseTextOptions = true;
            this.grDownTime.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grDownTime.Controls.Add(this.gcDownTime);
            this.grDownTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grDownTime.Location = new System.Drawing.Point(0, 0);
            this.grDownTime.Name = "grDownTime";
            this.grDownTime.Size = new System.Drawing.Size(669, 146);
            this.grDownTime.TabIndex = 0;
            this.grDownTime.Text = "Простой техники";
            // 
            // gcDownTime
            // 
            this.gcDownTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDownTime.Location = new System.Drawing.Point(2, 23);
            this.gcDownTime.MainView = this.gvDownTime;
            this.gcDownTime.MenuManager = this.barManager1;
            this.gcDownTime.Name = "gcDownTime";
            this.gcDownTime.Size = new System.Drawing.Size(665, 121);
            this.gcDownTime.TabIndex = 0;
            this.gcDownTime.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDownTime});
            // 
            // gvDownTime
            // 
            this.gvDownTime.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvDownTime.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvDownTime.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDownTime.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDownTime.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvDownTime.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvDownTime.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvDownTime.Appearance.Empty.Options.UseBackColor = true;
            this.gvDownTime.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDownTime.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDownTime.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDownTime.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDownTime.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvDownTime.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvDownTime.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvDownTime.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvDownTime.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvDownTime.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvDownTime.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvDownTime.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvDownTime.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvDownTime.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvDownTime.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDownTime.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDownTime.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvDownTime.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvDownTime.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvDownTime.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvDownTime.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvDownTime.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvDownTime.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvDownTime.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvDownTime.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvDownTime.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDownTime.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDownTime.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvDownTime.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDownTime.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDownTime.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.OddRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.OddRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvDownTime.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvDownTime.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvDownTime.Appearance.Preview.Options.UseBackColor = true;
            this.gvDownTime.Appearance.Preview.Options.UseFont = true;
            this.gvDownTime.Appearance.Preview.Options.UseForeColor = true;
            this.gvDownTime.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDownTime.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.Row.Options.UseBackColor = true;
            this.gvDownTime.Appearance.Row.Options.UseForeColor = true;
            this.gvDownTime.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDownTime.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvDownTime.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvDownTime.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvDownTime.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDownTime.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvDownTime.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvDownTime.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvDownTime.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvDownTime.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvDownTime.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDownTime.Appearance.VertLine.Options.UseBackColor = true;
            this.gvDownTime.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDtId,
            this.colDtPlanDate,
            this.colDtVehicleIdent,
            this.colDtReasonName,
            this.colDtDowntimeStart,
            this.colDtDowntimeEnd});
            this.gvDownTime.GridControl = this.gcDownTime;
            this.gvDownTime.Name = "gvDownTime";
            this.gvDownTime.OptionsView.EnableAppearanceEvenRow = true;
            this.gvDownTime.OptionsView.EnableAppearanceOddRow = true;
            this.gvDownTime.DoubleClick += new System.EventHandler(this.gvDownTime_DoubleClick);
            // 
            // colDtId
            // 
            this.colDtId.AppearanceCell.Options.UseTextOptions = true;
            this.colDtId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtId.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtId.Caption = "Номер";
            this.colDtId.FieldName = "Id";
            this.colDtId.Name = "colDtId";
            this.colDtId.OptionsColumn.AllowEdit = false;
            this.colDtId.OptionsColumn.ReadOnly = true;
            this.colDtId.Visible = true;
            this.colDtId.VisibleIndex = 0;
            this.colDtId.Width = 110;
            // 
            // colDtPlanDate
            // 
            this.colDtPlanDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDtPlanDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtPlanDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtPlanDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtPlanDate.Caption = "Дата";
            this.colDtPlanDate.DisplayFormat.FormatString = "d";
            this.colDtPlanDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDtPlanDate.FieldName = "PlanDate";
            this.colDtPlanDate.Name = "colDtPlanDate";
            this.colDtPlanDate.OptionsColumn.AllowEdit = false;
            this.colDtPlanDate.OptionsColumn.ReadOnly = true;
            this.colDtPlanDate.Visible = true;
            this.colDtPlanDate.VisibleIndex = 1;
            this.colDtPlanDate.Width = 137;
            // 
            // colDtVehicleIdent
            // 
            this.colDtVehicleIdent.AppearanceCell.Options.UseTextOptions = true;
            this.colDtVehicleIdent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDtVehicleIdent.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtVehicleIdent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtVehicleIdent.Caption = "Техника";
            this.colDtVehicleIdent.FieldName = "VehicleIdent";
            this.colDtVehicleIdent.Name = "colDtVehicleIdent";
            this.colDtVehicleIdent.OptionsColumn.AllowEdit = false;
            this.colDtVehicleIdent.OptionsColumn.ReadOnly = true;
            this.colDtVehicleIdent.Visible = true;
            this.colDtVehicleIdent.VisibleIndex = 2;
            this.colDtVehicleIdent.Width = 339;
            // 
            // colDtReasonName
            // 
            this.colDtReasonName.AppearanceCell.Options.UseTextOptions = true;
            this.colDtReasonName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDtReasonName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtReasonName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtReasonName.Caption = "Причина простоя";
            this.colDtReasonName.FieldName = "ReasonName";
            this.colDtReasonName.Name = "colDtReasonName";
            this.colDtReasonName.OptionsColumn.AllowEdit = false;
            this.colDtReasonName.OptionsColumn.ReadOnly = true;
            this.colDtReasonName.Visible = true;
            this.colDtReasonName.VisibleIndex = 3;
            this.colDtReasonName.Width = 335;
            // 
            // colDtDowntimeStart
            // 
            this.colDtDowntimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.colDtDowntimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtDowntimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtDowntimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtDowntimeStart.Caption = "Начало";
            this.colDtDowntimeStart.DisplayFormat.FormatString = "d";
            this.colDtDowntimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDtDowntimeStart.FieldName = "DowntimeStart";
            this.colDtDowntimeStart.Name = "colDtDowntimeStart";
            this.colDtDowntimeStart.OptionsColumn.AllowEdit = false;
            this.colDtDowntimeStart.OptionsColumn.ReadOnly = true;
            this.colDtDowntimeStart.Visible = true;
            this.colDtDowntimeStart.VisibleIndex = 4;
            this.colDtDowntimeStart.Width = 158;
            // 
            // colDtDowntimeEnd
            // 
            this.colDtDowntimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colDtDowntimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtDowntimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colDtDowntimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDtDowntimeEnd.Caption = "Окончание";
            this.colDtDowntimeEnd.DisplayFormat.FormatString = "d";
            this.colDtDowntimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDtDowntimeEnd.FieldName = "DowntimeEnd";
            this.colDtDowntimeEnd.Name = "colDtDowntimeEnd";
            this.colDtDowntimeEnd.OptionsColumn.AllowEdit = false;
            this.colDtDowntimeEnd.OptionsColumn.ReadOnly = true;
            this.colDtDowntimeEnd.Visible = true;
            this.colDtDowntimeEnd.VisibleIndex = 5;
            this.colDtDowntimeEnd.Width = 171;
            // 
            // grWithoutTasks
            // 
            this.grWithoutTasks.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grWithoutTasks.AppearanceCaption.Options.UseFont = true;
            this.grWithoutTasks.AppearanceCaption.Options.UseTextOptions = true;
            this.grWithoutTasks.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grWithoutTasks.Controls.Add(this.gcWithoutTasks);
            this.grWithoutTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grWithoutTasks.Location = new System.Drawing.Point(0, 0);
            this.grWithoutTasks.Name = "grWithoutTasks";
            this.grWithoutTasks.Size = new System.Drawing.Size(409, 146);
            this.grWithoutTasks.TabIndex = 0;
            this.grWithoutTasks.Text = "Техника без заданий";
            // 
            // gcWithoutTasks
            // 
            this.gcWithoutTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcWithoutTasks.Location = new System.Drawing.Point(2, 23);
            this.gcWithoutTasks.MainView = this.gvWithoutTasks;
            this.gcWithoutTasks.MenuManager = this.barManager1;
            this.gcWithoutTasks.Name = "gcWithoutTasks";
            this.gcWithoutTasks.Size = new System.Drawing.Size(405, 121);
            this.gcWithoutTasks.TabIndex = 0;
            this.gcWithoutTasks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvWithoutTasks});
            // 
            // gvWithoutTasks
            // 
            this.gvWithoutTasks.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.Empty.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWithoutTasks.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWithoutTasks.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWithoutTasks.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWithoutTasks.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvWithoutTasks.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvWithoutTasks.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvWithoutTasks.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWithoutTasks.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWithoutTasks.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvWithoutTasks.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWithoutTasks.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWithoutTasks.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWithoutTasks.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWithoutTasks.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.OddRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.OddRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvWithoutTasks.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvWithoutTasks.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvWithoutTasks.Appearance.Preview.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.Preview.Options.UseFont = true;
            this.gvWithoutTasks.Appearance.Preview.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWithoutTasks.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.Row.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.Row.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWithoutTasks.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvWithoutTasks.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWithoutTasks.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvWithoutTasks.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvWithoutTasks.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvWithoutTasks.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvWithoutTasks.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvWithoutTasks.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWithoutTasks.Appearance.VertLine.Options.UseBackColor = true;
            this.gvWithoutTasks.ColumnPanelRowHeight = 40;
            this.gvWithoutTasks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWtLastDate,
            this.colWtLastId,
            this.colWtVehicleId,
            this.colWtVehicleGroup,
            this.colWtVehicleIdent});
            this.gvWithoutTasks.GridControl = this.gcWithoutTasks;
            this.gvWithoutTasks.Name = "gvWithoutTasks";
            this.gvWithoutTasks.OptionsView.EnableAppearanceEvenRow = true;
            this.gvWithoutTasks.OptionsView.EnableAppearanceOddRow = true;
            this.gvWithoutTasks.ShowGridMenu += new DevExpress.XtraGrid.Views.Grid.GridMenuEventHandler(this.gvWithoutTasks_ShowGridMenu);
            this.gvWithoutTasks.DoubleClick += new System.EventHandler(this.gvWithoutTasks_DoubleClick);
            // 
            // colWtLastDate
            // 
            this.colWtLastDate.AppearanceCell.Options.UseTextOptions = true;
            this.colWtLastDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtLastDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colWtLastDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtLastDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWtLastDate.Caption = "Дата последнего планового задания";
            this.colWtLastDate.DisplayFormat.FormatString = "d";
            this.colWtLastDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colWtLastDate.FieldName = "LastDate";
            this.colWtLastDate.Name = "colWtLastDate";
            this.colWtLastDate.OptionsColumn.AllowEdit = false;
            this.colWtLastDate.OptionsColumn.ReadOnly = true;
            this.colWtLastDate.Visible = true;
            this.colWtLastDate.VisibleIndex = 0;
            this.colWtLastDate.Width = 293;
            // 
            // colWtLastId
            // 
            this.colWtLastId.AppearanceCell.Options.UseTextOptions = true;
            this.colWtLastId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtLastId.AppearanceHeader.Options.UseTextOptions = true;
            this.colWtLastId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtLastId.Caption = "Номер";
            this.colWtLastId.FieldName = "Id";
            this.colWtLastId.Name = "colWtLastId";
            this.colWtLastId.OptionsColumn.AllowEdit = false;
            this.colWtLastId.OptionsColumn.ReadOnly = true;
            this.colWtLastId.Visible = true;
            this.colWtLastId.VisibleIndex = 1;
            this.colWtLastId.Width = 158;
            // 
            // colWtVehicleId
            // 
            this.colWtVehicleId.Caption = "VehicleId";
            this.colWtVehicleId.FieldName = "VehicleId";
            this.colWtVehicleId.Name = "colWtVehicleId";
            this.colWtVehicleId.OptionsColumn.AllowEdit = false;
            this.colWtVehicleId.OptionsColumn.ReadOnly = true;
            // 
            // colWtVehicleGroup
            // 
            this.colWtVehicleGroup.AppearanceCell.Options.UseTextOptions = true;
            this.colWtVehicleGroup.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWtVehicleGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colWtVehicleGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtVehicleGroup.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWtVehicleGroup.Caption = "Группа";
            this.colWtVehicleGroup.FieldName = "VehicleGroup";
            this.colWtVehicleGroup.Name = "colWtVehicleGroup";
            this.colWtVehicleGroup.OptionsColumn.AllowEdit = false;
            this.colWtVehicleGroup.OptionsColumn.ReadOnly = true;
            this.colWtVehicleGroup.Visible = true;
            this.colWtVehicleGroup.VisibleIndex = 2;
            this.colWtVehicleGroup.Width = 246;
            // 
            // colWtVehicleIdent
            // 
            this.colWtVehicleIdent.AppearanceCell.Options.UseTextOptions = true;
            this.colWtVehicleIdent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWtVehicleIdent.AppearanceHeader.Options.UseTextOptions = true;
            this.colWtVehicleIdent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWtVehicleIdent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWtVehicleIdent.Caption = "Техника";
            this.colWtVehicleIdent.FieldName = "VehicleIdent";
            this.colWtVehicleIdent.Name = "colWtVehicleIdent";
            this.colWtVehicleIdent.OptionsColumn.AllowEdit = false;
            this.colWtVehicleIdent.OptionsColumn.ReadOnly = true;
            this.colWtVehicleIdent.Visible = true;
            this.colWtVehicleIdent.VisibleIndex = 3;
            this.colWtVehicleIdent.Width = 553;
            // 
            // PlanSetView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PlanSetView";
            this.Size = new System.Drawing.Size(1088, 541);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grDownTime)).EndInit();
            this.grDownTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDownTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDownTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grWithoutTasks)).EndInit();
            this.grWithoutTasks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcWithoutTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWithoutTasks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gcPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colFamily;
        private DevExpress.XtraGrid.Columns.GridColumn colAgrName;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanComment;
        private DevExpress.XtraBars.BarEditItem bdeStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStart;
        private DevExpress.XtraBars.BarEditItem bdeEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelForWork;
        private DevExpress.XtraGrid.Columns.GridColumn colId_order;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.GroupControl grDownTime;
        private DevExpress.XtraEditors.GroupControl grWithoutTasks;
        private DevExpress.XtraGrid.GridControl gcDownTime;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDownTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDtId;
        private DevExpress.XtraGrid.Columns.GridColumn colDtPlanDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDtVehicleIdent;
        private DevExpress.XtraGrid.Columns.GridColumn colDtReasonName;
        private DevExpress.XtraGrid.Columns.GridColumn colDtDowntimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn colDtDowntimeEnd;
        private DevExpress.XtraGrid.GridControl gcWithoutTasks;
        private DevExpress.XtraGrid.Views.Grid.GridView gvWithoutTasks;
        private DevExpress.XtraGrid.Columns.GridColumn colWtLastDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWtVehicleGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colWtVehicleIdent;
        private DevExpress.XtraGrid.Columns.GridColumn colWtLastId;
        private DevExpress.XtraGrid.Columns.GridColumn colWtVehicleId;
    }
}
