﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Agro.Utilites;
using Agro.Properties;
using TrackControl.General;
using TrackControl.Vehicles; 
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro
{
    public partial class PlanSetView : UserControl
    {

        ControlDataInterval cdePlanSet;

        public PlanSetView()
        {
            InitializeComponent();
            SetControlDates();
            RefreshData();
        }

        private void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        public void RefreshData()
        {
            gcPlan.DataSource = PlanSetController.GetSet((DateTime)bdeStart.EditValue, (DateTime)bdeEnd.EditValue);
            gcDownTime.DataSource = PlanSetController.GetSetDownTime((DateTime)bdeStart.EditValue, (DateTime)bdeEnd.EditValue);
            gcWithoutTasks.DataSource = PlanSetController.GetSetWithoutTask(); 
        }

        private void SetControlDates()
        {
            DateTime _stardData = TotUtilites.SetOrderDate(DateTime.Today);
            cdePlanSet = new ControlDataInterval(_stardData, _stardData.AddDays(1).AddMinutes(-1));
            bdeStart.EditValue = cdePlanSet.Begin;
            bdeEnd.EditValue = cdePlanSet.End;
        }

        public void ExportExcel()
        {
            XtraGridService.ExportToExcel(gvPlan, "Плановые задания"); 
        }

        private void gvPlan_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvPlan);
        }

        private void gvWithoutTasks_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvWithoutTasks);
        }

        private void gvDownTime_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvDownTime);
        }

        public void ViewPlanFromMainGrid()
        {
            ViewPlan(gvPlan);
        }

        private void ViewPlan(GridView gv)
        {
            if (!gv.IsValidRowHandle(gv.FocusedRowHandle))
            {
                XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                return;
            }

            int idPlan;
            if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, "Id").ToString(), out idPlan))
            {
                using (PlanController pc = new PlanController())
                {
                    pc.ViewPlan(idPlan);
                }
            }
        }

        public void DeleteSelectedFromGrid()
        {
            using (PlanController pc = new PlanController())
            {
                pc.DeleteSelectedFromGrid(gvPlan);
            }
        }

        private void gvWithoutTasks_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu)e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.CreatePlanOrder, new EventHandler(CreateOrderFromWithoutTaskRecord));
            gvMenu.Items.Add(menuItem);
        }

        void CreateOrderFromWithoutTaskRecord(object sender, System.EventArgs e)
        {
            int idVihicle = 0;
            if (Int32.TryParse (gvWithoutTasks.GetRowCellValue(gvWithoutTasks.FocusedRowHandle, gvWithoutTasks.Columns["VehicleId"]).ToString(),out idVihicle))
            {
                Vehicle vh = DocItem.VehiclesModel.GetVehicleById(idVihicle);
                if (vh != null)
                {
                    
                    using (PlanController pc = new PlanController())
                    {
                        pc.AddNewPlanFromView(vh);
                        RefreshData(); 
                    }
                }
            }
            
        }

    }
}
