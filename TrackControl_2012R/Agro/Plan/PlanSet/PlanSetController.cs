﻿using System;
using System.Data; 
using System.Collections.Generic;
using System.Text;
using TrackControl.General.DatabaseDriver;
using Agro.Utilites;
using TUtil = Agro.Utilites.TotUtilites;

namespace Agro
{
    public static class PlanSetController
    {
        public static DataTable GetSet(DateTime start,DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sql = string.Format(AgroQuery.PlanSetController.SelectAgroPlanId, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent);

                SetTimeParams(start, end, driverDb);
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }

        public static DataTable GetSetDownTime(DateTime start, DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.PlanSetController.SelectFromAgroPlanId, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent);
                SetTimeParams(start, end, driverDb);
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }

        public static DataTable GetSetWithoutTask()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
              
                string sql = string.Format(AgroQuery.PlanSetController.SelectDistinctFromVehicle, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent
                                                   , AgroQuery.PlanSetController.SelectAgroPlanIdMobitel
                                                   , AgroQuery.PlanSetController.SqlLastDate);
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeToday", TUtil.SetOrderDate(DateTime.Today));
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }

        private static void SetTimeParams(DateTime start, DateTime end, DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
        }
    }
}
