﻿using System;
using System.Data;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;
using TUtil = Agro.Utilites.TotUtilites;

namespace Agro
{
    public class PlanController : DocItem, IDocument, IDisposable
    {
        private PlanEntity _planEntity;

        public PlanEntity PlanEntity
        {
            get { return _planEntity; }
            set { _planEntity = value; }
        }

        private PlanForm _viewPlan;

        public PlanController()
        {

        }

        public PlanController(PlanEntity planEntity)
        {
            _planEntity = planEntity;
        }

        #region Члены IDocument

        public override int AddDoc()
        {
            if (TestDemo() && ValidateHeader() && !IsPlanExistForVehicleDay())
            {
                _planEntity.FuelAtStart = GetStartFuelLevel();
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.PlanController.InsertIntoAgroPlan, driverDb.ParamPrefics);
                    CreateParameters(driverDb);
                    _planEntity.ID = driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_plan");
                    if (_planEntity.ID > 0)
                        UserLog.InsertLog(UserLogTypes.AGRO_PLAN, Resources.LogCreate, _planEntity.ID);
                }
            }
            return _ID;
        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(13);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PlanDate", _planEntity.Date);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_mobitel", _planEntity.VehiclePlan.MobitelId);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquarePlanned", _planEntity.SquarePlanned);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelAtStart", _planEntity.FuelAtStart);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelForWork", _planEntity.FuelForWork);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PlanComment", _planEntity.Remark);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (_planEntity == null || _planEntity.Remark == null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PlanComment", "");
                else
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PlanComment", _planEntity.Remark);
            }

            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdStatus", _planEntity.PlanStatus);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_order",
                _planEntity.Order == null ? 0 : _planEntity.Order.ID);

            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserCreated", _planEntity.UserCreated);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsDowntime", _planEntity.IsDownTime);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdDowntimeReason",
                    _planEntity.PlanDowntimeReason == 0 ? null : _planEntity.PlanDowntimeReason);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (_planEntity.PlanDowntimeReason != null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdDowntimeReason",
                        _planEntity.PlanDowntimeReason == 0 ? null : _planEntity.PlanDowntimeReason);
                else
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdDowntimeReason", DBNull.Value);
            }

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeStart", _planEntity.DowntimeStart);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (_planEntity.DowntimeStart == null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeStart", new DateTime(1900, 1, 1));
                else
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeStart", _planEntity.DowntimeStart);
            }

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeEnd", _planEntity.DowntimeEnd);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (_planEntity.DowntimeEnd == null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeEnd", new DateTime(1900, 1, 1));
                else
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DowntimeEnd", _planEntity.DowntimeEnd);
            }
        } // CreateParameters

        public override bool UpdateDoc()
        {
            if (IsPlanExistForVehicleDay()) return false;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sSQL = string.Format(AgroQuery.PlanController.UpdateAgroPlan, driverDb.ParamPrefics,
                    _planEntity.ID);

                CreateParameters(driverDb);
                driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                UserLog.InsertLog(UserLogTypes.AGRO_PLAN, Resources.LogUpdateHeader, _planEntity.ID);
                return true;
            }
        }

        public override bool DeleteDoc(bool bQuestionNeed)
        {
            if (DeleteDocTest() && DeleteDocContent())
            {
                if (bQuestionNeed)
                {
                    if (DialogResult.No ==
                        XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo))
                        return false;
                }


                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.PlanController.DeleteFromAgroPlan, _ID);
                    driverDb.ExecuteNonQueryCommand(sSQL);
                }
                return true;
            }
            else
                return false;
        }

        public override bool DeleteDocContent()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    _sSQL = string.Format(AgroQuery.PlanController.DeleteFromAgroPlanT, _ID);
                    driverDb.ExecuteNonQueryCommand(_sSQL);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool DeleteDocTest()
        {
            return true;
        }

        public override bool TestDemo()
        {
            if (GlobalVars.g_AGRO == (int) Consts.RegimeType.Demo)
            {
                int cnt = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();

                    _sSQL = AgroQuery.PlanController.SelectCountAgroPlan;

                    cnt = driverDb.GetScalarValueNull<Int32>(_sSQL, 0);
                }
                if (cnt >= Consts.AGRO_LIMIT)
                {
                    XtraMessageBox.Show(Resources.AgroLimit_1 + " " + Consts.AGRO_LIMIT.ToString() + "!",
                        Resources.ApplicationName);
                    _ID = 0;
                    return false;
                }
            }
            return true;
        }

        public override DataTable GetContent()
        {
            throw new NotImplementedException();
        }

        public override void UpdateDocTotals()
        {
            throw new NotImplementedException();
        }

        public override bool GetDocById(int id)
        {
            try
            {
                _sSQL = string.Format(AgroQuery.PlanController.SelectAgroPlan, id);

                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(_sSQL);
                    if (driverDb.Read())
                    {
                        _planEntity = new PlanEntity();
                        _planEntity.ID = driverDb.GetInt32("Id");
                        _planEntity.Date = driverDb.GetDateTime("PlanDate");
                        _planEntity.Remark = TotUtilites.NDBNullReader(driverDb, "PlanComment", "").ToString();
                        int idMobitel = driverDb.GetInt32("Id_mobitel");
                        foreach (Vehicle vh in DocItem.VehiclesModel.Vehicles)
                        {
                            if (vh.MobitelId == idMobitel)
                            {
                                _planEntity.VehiclePlan = vh;
                                break;
                            }
                        }
                        _planEntity.SquarePlanned = (double) TotUtilites.NDBNullReader(driverDb, "SquarePlanned", 0.0);
                        _planEntity.FuelAtStart = (double) TotUtilites.NDBNullReader(driverDb, "FuelAtStart", 0.0);
                        _planEntity.FuelForWork = (double) TotUtilites.NDBNullReader(driverDb, "FuelForWork", 0.0);
                        _planEntity.PlanStatus = driverDb.GetInt32("IdStatus");
                        _planEntity.PlanRecords = PlanTController.GetList(_planEntity.ID);
                        _planEntity.Order = new OrderItem(driverDb.GetInt32("Id_order"));
                        _planEntity.UserCreated = TotUtilites.NDBNullReader(driverDb, "UserCreated", "").ToString();
                        _planEntity.IsDownTime = driverDb.GetBoolean("IsDownTime");
                        _planEntity.PlanDowntimeReason =
                            (int) TotUtilites.NDBNullReader(driverDb, "IdDowntimeReason", 0);
                        if (driverDb.GetValue(driverDb.GetOrdinal("DowntimeStart")) == DBNull.Value)
                            _planEntity.DowntimeStart = null;
                        else
                            _planEntity.DowntimeStart = driverDb.GetDateTime("DowntimeStart");
                        if (driverDb.GetValue(driverDb.GetOrdinal("DowntimeEnd")) == DBNull.Value)
                            _planEntity.DowntimeEnd = null;
                        else
                            _planEntity.DowntimeEnd = driverDb.GetDateTime("DowntimeEnd");
                    }
                    driverDb.CloseDataReader();

                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "PlanController.GetDocById(int id):274");
                return false;
            }
        }

        #endregion

        internal void AddNewPlanFromView()
        {
            PlanEntity newPlanEntity = new PlanEntity();
            SetHeaderDefaultsValues(newPlanEntity);
            ViewPlanForm(newPlanEntity);

        }

        private void ViewPlanForm(PlanEntity planEntity)
        {
            _viewPlan = new PlanForm();
            LoadPlanFormControls(_viewPlan);
            _viewPlan.pgPlan.SelectedObject = planEntity;
            _viewPlan.Show();
        }

        internal void AddNewPlanFromView(Vehicle vh)
        {
            _planEntity = new PlanEntity();
            SetHeaderDefaultsValues(_planEntity);
            _planEntity.VehiclePlan = vh;
            AddDoc();
            if (_planEntity.ID > 0) ViewPlanForm(_planEntity);
        }


        internal void ViewPlan(int idPlan)
        {
            if (GetDocById(idPlan))
            {
                _viewPlan = new PlanForm();
                LoadPlanFormControls(_viewPlan);
                _viewPlan.pgPlan.SelectedObject = _planEntity;
                _viewPlan.pgOrder.SelectedObject = _planEntity.Order;
                _viewPlan.gcPlan.DataSource = _planEntity.PlanRecords;
                _viewPlan.Show();
            }

        }

        public static void SetHeaderDefaultsValues(PlanEntity newPlanEntity)
        {
            newPlanEntity.Date = TUtil.SetOrderDate(DateTime.Today);
            newPlanEntity.PlanStatus = (int) PlanStates.Active;
            newPlanEntity.UserCreated = UserBaseCurrent.Instance.Name;
        }

        internal void LoadPlanFormControls(PlanForm view)
        {
            view.rleTeam.DataSource = DocItem.VehiclesModel.RootGroupsWithItems();
            view.rleStates.DataSource = DictionaryAgroPlanStatus.GetList();
            if (_planEntity != null) view.rleVehicle.DataSource = _planEntity.VehicleGroup.OwnRealItems();
            view.rleDriver.DataSource = DriverVehicleProvider.GetList();
            view.rleField.DataSource = DictionaryAgroField.GetList();
            view.rleAgregat.DataSource = DictionaryAgroAgregat.GetList();
            view.rleWorkType.DataSource = DictionaryAgroWorkType.GetList();
            view.rleDtReasons.DataSource = DictionaryAgroPlanDtReason.GetList();
        }

        internal bool ValidateHeader()
        {
            if (_planEntity.PlanStatus == 0 || _planEntity.VehiclePlan == null)
                return false;
            else
                return true;
        }

        private bool IsPlanExistForVehicleDay()
        {
            int idPlan = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                _sSQL = string.Format(AgroQuery.PlanController.SelectAgroPlanId, _planEntity.VehiclePlan.MobitelId,
                    _planEntity.Date.Day, _planEntity.Date.Month, _planEntity.Date.Year, _planEntity.ID);
                idPlan = driverDb.GetScalarValueNull<Int32>(_sSQL, 0);
            }
            if (idPlan > 0)
            {
                XtraMessageBox.Show(string.Format(Resources.PlanExistForVehicleDate, _planEntity.VehiclePlan.Info),
                    Resources.ApplicationName);
                return true;
            }
            return false;
        }

        internal bool SetFact()
        {
            PlanOrderConnector connector = new PlanOrderConnector();
            connector.Plan = _planEntity;
            if (connector.FindOrder())
            {
                _planEntity.Order = connector.Order;
                UserLog.InsertLog(UserLogTypes.AGRO_PLAN,
                    string.Format(Resources.LogConnectToOrder, _planEntity.Order.ID), _planEntity.ID);
                UpdateDoc();
                return true;
            }
            else
                return false;
        }

        public double GetStartFuelLevel()
        {
            GpsData gpsData = GpsDataProvider.GetValidDataGpsOnTime(_planEntity.VehiclePlan.MobitelId, _planEntity.Date);
            return FuelLevel.GetFuelValueOnPoint(_planEntity.VehiclePlan, gpsData);
        }

        #region Члены IDisposable

        void IDisposable.Dispose()
        {

        }

        #endregion
    }
}
