﻿using System;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;

namespace Agro
{
    public class AgroQuery
    {
        private static readonly int TypeDataBaseForUsing = DriverDb.TypeDataBaseUsing;
        private const int MySqlUse = 0;
        private const int MssqlUse = 1;
        // глобальные запросы
        private const string MySqlvehicleIdent =
            "CONCAT(IFNULL(vehicle.NumberPlate,''),' | ' , IFNULL(vehicle.CarModel,''),' | ' , IFNULL(vehicle.MakeCar,''))";
        private const string MySqlvehicleNumberPlateMakeCar =
            "CONCAT(IFNULL(vehicle.NumberPlate,''),'  ' , IFNULL(vehicle.MakeCar,''))";
        private const string MsSqlvehicleIdent =
            "convert(VARCHAR, ISNULL(vehicle.NumberPlate,'')) + ' | ' + convert(VARCHAR, ISNULL(vehicle.CarModel,'')) + ' | ' + convert(varchar, ISNULL(vehicle.MakeCar,''))";
        private const string MsSqlvehicleNumberPlateMakeCar =
            "convert(VARCHAR, ISNULL(vehicle.NumberPlate,'')) + '  ' + convert(VARCHAR, ISNULL(vehicle.MakeCar,''))";
        private const string SSqlinsertHead =
            "INSERT INTO `agro_calct`(Id_main, Lon, Lat, Selected, Border, BorderNode, Number) VALUES ";
        private const string MSsSqLinsertHead =
            "INSERT INTO agro_calct(Id_main, Lon, Lat, Selected, Border, BorderNode, Number) VALUES ";
        private const string MySqlFieldIdent =
            "CONCAT(IFNULL(agro_field.Name,''), ' (', ROUND(IFNULL(zones.Square*100,0),3),')')";
        private const string MsSqlFieldIdent =
            "(ISNULL(agro_field.Name,'') + ' (' + convert(VARCHAR, ROUND(ISNULL(zones.Square*100,0),3)) + ')')";
        private const string MySqlStateIdent =
            "CAST(CONCAT(IFNULL(state.Title,''),' | ' , IFNULL(state.MinValue,''),' | ' , IFNULL(state.MaxValue,'')) as CHAR)";
        private const string MsSqlStateIdent =
            "convert(VARCHAR, ISNULL(state.Title,'')) + ' | ' + convert(VARCHAR, ISNULL(state.MinValue,'')) + ' | ' + convert(varchar, ISNULL(state.MaxValue,''))";
        private const string MySqlDriverIdent = "CONCAT(IFNULL(driver.Family,''), ' ', IFNULL(driver.Name,''))";
        private const string MsSqlDriverIdent = "(ISNULL(driver.Family,'') + ' ' + ISNULL(driver.Name,''))";



        public static string SqlFieldIdent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return MySqlFieldIdent;

                    case MssqlUse:
                        return MsSqlFieldIdent;
                }

                return "";
            }
        }

        public static string SqlVehicleIdent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return MySqlvehicleIdent;

                    case MssqlUse:
                        return MsSqlvehicleIdent;
                }

                return "";
            }
        }

        public static string SqlVehicle_NumberPlateCarModel
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return MySqlvehicleNumberPlateMakeCar;

                    case MssqlUse:
                        return MsSqlvehicleNumberPlateMakeCar;
                }

                return "";
            }
        }

        public static string SqlDriverIdent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return MySqlDriverIdent;

                    case MssqlUse:
                        return MsSqlDriverIdent;
                }

                return "";
            }
        }


        public class MainAgro
        {
            public static string SelectFromAgroField
            {
                get
                {
                    return
                        "SELECT * FROM agro_field";
                }
            }

            public static string SelectZonesId
            {
                get
                {
                    return
                        "SELECT zones.Zone_ID as Id, zones.Name FROM zones ORDER BY zones.Name";
                }
            }

            public static string SelectAgroPrice
            {
                get
                {
                    return
                        "SELECT agro_price.Id, agro_price.DateComand as DateOrder FROM agro_price ORDER BY  agro_price.DateComand DESC";
                }
            }

            public static string SelectVehicleMobitel
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + MySqlvehicleIdent + " AS Name FROM vehicle";

                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + MsSqlvehicleIdent + " AS Name FROM vehicle";
                    }

                    return "";
                }
            }
        }
        // Agro_MainAgro
        public class AgroMap
        {
            public static string SelectAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + MySqlvehicleIdent +
                            @" as Name,driver.Family,
                                    agro_order.`Date`, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND 
                                    (agro_order.`Date` >= ?start and agro_order.`Date` < ?end)
                                    ORDER BY agro_order.`Date`, " + MySqlvehicleIdent;

                        case MssqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + MsSqlvehicleIdent +
                            @" as Name,driver.Family,
                                    agro_order.Date, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND
                                    (agro_order.Date >= @start and agro_order.Date < @end)
                                    ORDER BY agro_order.Date, " + MsSqlvehicleIdent;
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderById
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + MySqlvehicleIdent +
                            @" as Name,driver.Family,
                                    agro_order.`Date`, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND 
                                    (agro_order.`Id` >= {1})
                                    ORDER BY agro_order.`Date`, " + MySqlvehicleIdent;

                        case MssqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + MsSqlvehicleIdent +
                            @" as Name,driver.Family,
                                    agro_order.Date, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND
                                    (agro_order.Id >= {1})
                                    ORDER BY agro_order.Date, " + MsSqlvehicleIdent;
                    }

                    return "";
                }
            }
        }

        // AgroMap
        public class AgroMapOrder
        {
            public static string SelectAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT   agro_ordert.Id,agro_ordert.Id_zone,IF(agro_ordert.Id_zone= {2},'{3}', IFNULL(agro_field.Name, '{0}')) AS FName, {4} as Driver, agro_ordert.TimeStart, agro_ordert.TimeEnd,
                                    TIME_Format(TIMEDIFF(agro_ordert.TimeEnd, agro_ordert.TimeStart), '%H:%i') as Time,
                                    agro_ordert.Id_work ,agro_ordert.Distance,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.FactSquareCalc) as FactSquareCalc,
                                    agro_ordert.Confirm,agro_agregat.Width,LockRecord,PointsValidity
                                    FROM agro_ordert 
                                    LEFT OUTER JOIN  agro_field ON agro_ordert.Id_field = agro_field.Id 
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id 
                                    LEFT OUTER JOIN  agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id 
                                    WHERE agro_ordert.Id_main = {1} ORDER BY TimeStart,TimeEnd";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert.Id,agro_ordert.Id_zone, (CASE WHEN agro_ordert.Id_zone = {2} then '{3}' else ISNULL(agro_field.Name, '{0}')END) AS FName, 
                                    {4} as Driver, agro_ordert.TimeStart, agro_ordert.TimeEnd,
                                    dbo.get_hh_min(DATEDIFF(mi, agro_ordert.TimeStart, agro_ordert.TimeEnd)) as Time,
                                    agro_ordert.Id_work ,agro_ordert.Distance,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FactSquareCalc end) as FactSquareCalc,
                                    agro_ordert.Confirm,agro_agregat.Width,LockRecord,PointsValidity
                                    FROM agro_ordert 
                                    LEFT OUTER JOIN  agro_field ON agro_ordert.Id_field = agro_field.Id 
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id 
                                    LEFT OUTER JOIN  agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id 
                                    WHERE agro_ordert.Id_main = {1} ORDER BY TimeStart,TimeEnd";
                    }

                    return "";
                }
            }
        }
        // AgroMapOrder
        public class DictionaryAgroAgregat
        {
            public static string InsertIntoAgroAgregatIdent
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_agregat`(Name,Width,Identifier,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId
                                 ,HasSensor,IsUseAgregatState,MinStateValue,MaxStateValue,LogicState,SensorAlgorithm,LogicStateBounce) 
                                    VALUES (?Name,?Width,?Identifier,?Def,?Comment,?Id_main,?InvNumber,?Id_work,0,?OutLinkId
                                ,?HasSensor,?IsUseAgregatState,?MinStateValue,?MaxStateValue,?LogicState,?SensorAlgorithm,?LogicStateBounce)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_agregat(Name,Width,Identifier,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId
                                ,HasSensor,IsUseAgregatState,MinStateValue,MaxStateValue,LogicState,SensorAlgorithm,LogicStateBounce) 
                                    VALUES (@Name,@Width,@Identifier,@Def,@Comment,@Id_main,@InvNumber,@Id_work,0,@OutLinkId
                                ,@HasSensor,@IsUseAgregatState,@MinStateValue,@MaxStateValue,@LogicState,@SensorAlgorithm,@LogicStateBounce)";
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_agregat`(Name,Width,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId
                                    ,HasSensor,IsUseAgregatState,MinStateValue,MaxStateValue,LogicState,SensorAlgorithm,LogicStateBounce,K,B,S) 
                                    VALUES (?Name,?Width,?Def,?Comment,?Id_main,?InvNumber,?Id_work,0,?OutLinkId
                                    ,?HasSensor,?IsUseAgregatState,?MinStateValue,?MaxStateValue,?LogicState,?SensorAlgorithm,?LogicStateBounce,?K,?B,?S)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_agregat(Name,Width,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId
                                ,HasSensor,IsUseAgregatState,MinStateValue,MaxStateValue,LogicState,SensorAlgorithm,LogicStateBounce,K,B,S) 
                                    VALUES (@Name,@Width,@Def,@Comment,@Id_main,@InvNumber,@Id_work,0,@OutLinkId
                                ,@HasSensor,@IsUseAgregatState,@MinStateValue,@MaxStateValue,@LogicState,@SensorAlgorithm,@LogicStateBounce,@K,@B,@S)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregatIdent
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"UPDATE agro_agregat SET Name = ?Name ,Width = ?Width ,Identifier = ?Identifier ,Def = ?Def,
                                Comment = ?Comment,Id_main = ?Id_main,InvNumber = ?InvNumber,Id_work = ?Id_work ,OutLinkId=?OutLinkId 
                                , HasSensor=?HasSensor,IsUseAgregatState=?IsUseAgregatState
                                ,MinStateValue=?MinStateValue,MaxStateValue=?MaxStateValue
                                ,LogicState=?LogicState,SensorAlgorithm=?SensorAlgorithm,LogicStateBounce=?LogicStateBounce
                                ,K=?K,B=?B ,S=?S  WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                @"UPDATE agro_agregat SET Name = @Name ,Width = @Width ,Identifier = @Identifier ,Def = @Def,
                            Comment = @Comment,Id_main = @Id_main,InvNumber = @InvNumber,Id_work = @Id_work ,OutLinkId=@OutLinkId 
                            , HasSensor=@HasSensor ,IsUseAgregatState=@IsUseAgregatState
                            , MinStateValue=@MinStateValue,MaxStateValue=@MaxStateValue
                            ,LogicState=@LogicState ,SensorAlgorithm=@SensorAlgorithm ,LogicStateBounce=@LogicStateBounce
                            ,K=@K,B=@B ,S=@S WHERE (ID ={0})";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_agregat SET Name = ?Name ,Width = ?Width ,Def = ?Def," +
                            "Comment = ?Comment,Id_main = ?Id_main,InvNumber = ?InvNumber,Id_work = ?Id_work ,OutLinkId=?OutLinkId , HasSensor=?HasSensor WHERE (ID = " +
                            "{0})";

                        case MssqlUse:
                            return "UPDATE agro_agregat SET Name = @Name ,Width = @Width ,Def = @Def," +
                            "Comment = @Comment,Id_main = @Id_main,InvNumber = @InvNumber,Id_work = @Id_work ,OutLinkId=@OutLinkId , HasSensor=@HasSensor WHERE (ID = " +
                            "{0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroAgregatVehicle
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_agregat_vehicle.Id , team.id as TID, vehicle.id as VID, sensors.id AS SID,state.Id AS StID
                                    FROM agro_agregat_vehicle
                                    INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                                    LEFT OUTER JOIN team ON team.id = vehicle.Team_id
                                    LEFT OUTER JOIN sensors ON agro_agregat_vehicle.Id_sensor = sensors.id 
                                    LEFT OUTER JOIN state ON agro_agregat_vehicle.Id_state = state.Id AND agro_agregat_vehicle.Id_sensor = state.SensorId
                                    WHERE   agro_agregat_vehicle.Id_agregat = {0}
                                    ORDER BY team.Name,vehicle.MakeCar";

                        case MssqlUse:
                            return
                                @"SELECT agro_agregat_vehicle.Id, team.id as [TID], vehicle.id as [VID], sensors.id AS [SID],state.Id AS [StID]
                                    FROM agro_agregat_vehicle
                                    INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                                    LEFT OUTER JOIN team ON team.id = vehicle.Team_id
                                    LEFT OUTER JOIN sensors ON agro_agregat_vehicle.Id_sensor = sensors.id 
                                    LEFT OUTER JOIN state ON agro_agregat_vehicle.Id_state = state.Id AND agro_agregat_vehicle.Id_sensor = state.SensorId
                                    WHERE   agro_agregat_vehicle.Id_agregat = {0}
                                    ORDER BY team.Name,vehicle.MakeCar";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregatVehicle
            {
                get
                {
                    return "UPDATE agro_agregat_vehicle SET Id_vehicle = {1}, Id_sensor = {2}, Id_state = {3} WHERE (ID = {0})";
                }
            }

            public static string InsertIntoAgroAgregatVehicle
            {
                get
                {
                    return "INSERT INTO agro_agregat_vehicle (Id_agregat,Id_vehicle,Id_sensor,Id_state) VALUES ({0},{1},{2},{3})";
                }
            }

            public static string DeleteAgroAgregatVehicle
            {
                get
                {
                    return "DELETE FROM agro_agregat_vehicle WHERE (ID = {0})";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_agregat.Id, CONCAT(IFNULL(agro_agregat.InvNumber,0),' ',  agro_agregat.Name) as Name FROM agro_agregat WHERE agro_agregat.IsGroupe = {0} ORDER BY IFNULL(agro_agregat.InvNumber,0),agro_agregat.Name";
                                //"SELECT agro_agregat.Id, LTRIM(CONCAT(IFNULL(agro_agregat.Identifier,''),' ',  IFNULL(agro_agregat.Name,''))) as Name FROM agro_agregat WHERE agro_agregat.IsGroupe = {0} ORDER BY IFNULL(agro_agregat.Identifier,''),agro_agregat.Name";

                        case MssqlUse:
                            return
                                "SELECT agro_agregat.Id, convert(VARCHAR, ISNULL(agro_agregat.Identifier,0)) + ' ' + agro_agregat.Name as Name FROM agro_agregat WHERE agro_agregat.IsGroupe = {0} ORDER BY ISNULL(agro_agregat.Identifier,0),agro_agregat.Name";
                    }
                    return "";
                }
            }

            public static string SelectAgroAgregatGroup
            {
                get
                {
                    return "SELECT agro_agregat.Id,  agro_agregat.Name as Name FROM agro_agregat WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";

                }
            }

            public static string SelectAgroAgregatXml
            {
                get
                {
                    return @"SELECT agro_agregat.Id, agro_agregat.Name  as GroupName ,agro_agregat.Id_work,agro_work.Name as WorkName
                    ,agro_agregat.Width, agro_agregat.Identifier,agro_agregat.Id_main
                    FROM agro_agregat LEFT OUTER JOIN agro_work ON agro_agregat.Id_work = agro_work.Id WHERE agro_agregat.IsGroupe = 0";
                }
            }
            public static string SelectAgroAgregatJornal
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_agregat.Id
                            , agro_agregat.Id_main
                            , agro_agregat.Name 
                            , agro_agregat.InvNumber 
                            , agro_agregat.Width 
                            , agro_agregat.Identifier
                            , agro_agregat.Def 
                            , (SELECT " + MySqlvehicleIdent + @" FROM agro_agregat_vehicle   
                            INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                            INNER JOIN team ON vehicle.Team_id = team.id
                            WHERE agro_agregat_vehicle.Id_agregat = agro_agregat.Id ORDER BY team.Name,vehicle.MakeCar LIMIT 1) as VehicleAgr
                            , agro_agregat.Comment
                            ,agro_agregat.Id_work
                            FROM   agro_agregat 
                            WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";

                        case MssqlUse:
                            return @"SELECT agro_agregat.Id
                            , agro_agregat.Id_main
                            , agro_agregat.Name 
                            , agro_agregat.InvNumber 
                            , agro_agregat.Width 
                            , agro_agregat.Identifier
                            , agro_agregat.Def 
                            , (SELECT TOP 1 " + MsSqlvehicleIdent + @" FROM agro_agregat_vehicle   
                            INNER JOIN vehicle     ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                            INNER JOIN team ON vehicle.Team_id = team.id
                            WHERE agro_agregat_vehicle.Id_agregat = agro_agregat.Id ORDER BY team.Name,vehicle.MakeCar) as VehicleAgr
                            , agro_agregat.Comment
                            , agro_agregat.Id_work
                            FROM   agro_agregat 
                            WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";
                    }

                    return "";
                }
            }

            public static string DeleteAgroAgregat
            {
                get
                {
                    return
                        "DELETE FROM agro_agregat WHERE ID = ";
                }
            }

            public static string SelectCountAgroOrderT
            {
                get
                {
                    return
                        "SELECT COUNT(agro_ordert.Id) FROM agro_ordert WHERE   Id_agregat  = ";
                }
            }

            public static string SelectAgro_Agregat
            {
                get
                {
                    return
                        "SELECT agro_agregat.Width FROM agro_agregat WHERE agro_agregat.Def = 1";
                }
            }

            public static string SelectAgroAgregatWidth
            {
                get
                {
                    return
                        "SELECT agro_agregat.Width FROM agro_agregat INNER JOIN  agro_ordert ON agro_agregat.Id = agro_ordert.Id_agregat WHERE agro_ordert.Id = ";
                }
            }

            public static string UpdateAgroAgregatSet
            {
                get
                {
                    return
                        "UPDATE agro_agregat Set  Def = 0 WHERE Id <> {0}";
                }
            }

            public static string SelectAgroAgregatFrom
            {
                get
                {
                    return
                        "SELECT agro_agregat.* FROM agro_agregat WHERE   agro_agregat.Id = {0}";
                }
            }

            public static string SelectAgroAgregatFrm
            {
                get
                {
                    return
                        "SELECT agro_agregat.* FROM agro_agregat WHERE agro_agregat.Identifier = {0}";
                }
            }

            public static string SelectAgroAgregatId
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id FROM agro_agregat WHERE agro_agregat.Def = 1";
                }
            }

            public static string SelectAgroAgregatVehicleId
            {
                get
                {
                    return
                        "SELECT   agro_agregat_vehicle.Id_agregat  FROM  agro_agregat_vehicle  WHERE "
                    + " agro_agregat_vehicle.Id_vehicle = {0}";
                }
            }

            public static string SelectAgroAgregatVehicleIdSensor
            {
                get
                {
                    return
                        "SELECT   agro_agregat_vehicle.Id_sensor  FROM  agro_agregat_vehicle  WHERE "
                    + " agro_agregat_vehicle.Id_vehicle = {0} AND agro_agregat_vehicle.Id_agregat = {1}";
                }
            }

            public static string SelectAgroAgregatVehicleIdAgro
            {
                get
                {
                    return
                        "SELECT agro_agregat_vehicle.Id,agro_agregat_vehicle.Id_agregat  FROM  agro_agregat_vehicle  WHERE "
                    + " agro_agregat_vehicle.Id_vehicle = {0} AND agro_agregat_vehicle.Id_agregat <> {1}";
                }
            }

            public static string SelectAgroAgregatCalibration
            {
                get
                {
                    return
                        @"SELECT ID,UserValue,SensorValue,K,b FROM agro_sensorkoefficients
                        WHERE agro_sensorkoefficients.AgregatID = {0} ORDER BY ID";
                }
            }
        }
        // DictionaryAgroAgregat
        public class DictionaryAgroAgregatGrp
        {
            public static string InsertIntoAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_agregat`(Name, Comment, Id_work, IsGroupe, OutLinkId) VALUES (?Name, ?Comment, ?Id_work, 1, ?OutLinkId)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_agregat(Name, Comment, Id_work, IsGroupe, OutLinkId) VALUES (@Name, @Comment, @Id_work, 1, @OutLinkId)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_agregat SET Name = ?Name ,Comment = ?Comment,Id_work = ?Id_work,OutLinkId=?OutLinkId WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_agregat SET Name = @Name ,Comment = @Comment,Id_work = @Id_work,OutLinkId=@OutLinkId WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroAgregatItem
            {
                get
                {
                    return "DELETE FROM agro_agregat WHERE ID = {0}";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    return "SELECT agro_agregat.* FROM agro_agregat WHERE agro_agregat.Id = ";
                }
            }

            public static string SelectAgroAgregatCount
            {
                get
                {
                    return "SELECT COUNT(agro_agregat.Id) FROM agro_agregat WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroAgregatId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_agregat.Id FROM agro_agregat WHERE agro_agregat.Name = ?Name AND agro_agregat.Id <> {0} AND IsGroupe = 1";

                        case MssqlUse:
                            return
                                @"SELECT agro_agregat.Id FROM agro_agregat WHERE agro_agregat.Name = @Name AND agro_agregat.Id <> {0} AND IsGroupe = 1";
                    }

                    return "";
                }
            }

            public static string SelectAgroAgregatList
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id, agro_agregat.Name FROM agro_agregat WHERE agro_agregat.IsGroupe = 1 ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroAgregatJornal
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id, agro_agregat.Name ,agro_agregat.Id_work,agro_agregat.Comment FROM agro_agregat WHERE agro_agregat.IsGroupe = 1 ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroAgregatGroupXml
            {
                get
                {
                    return @"SELECT agro_agregat.Id, agro_agregat.Name  as GroupName ,agro_agregat.Id_work,agro_work.Name as WorkName 
                    FROM agro_agregat LEFT OUTER JOIN agro_work ON agro_agregat.Id_work = agro_work.Id WHERE agro_agregat.IsGroupe = 1";
                }
            }

        }
        // DictionaryAgroAgregatGrp
        public class DictionaryAgroCommand
        {
            public static string InsertIntoAgroPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_price`(Name, DateComand, DateInit, Comment) VALUES (?Name, ?DateComand, ?DateInit, ?Comment)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_price(Name, DateComand, DateInit, Comment) VALUES (@Name, @DateComand, @DateInit, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_price SET Name = ?Name, DateInit = ?DateInit, DateComand = ?DateComand, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_price SET Name = @Name, DateInit = @DateInit, DateComand = @DateComand, Comment = @Comment WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroPrice
            {
                get
                {
                    return "DELETE FROM agro_price WHERE ID = {0}";
                }
            }

            public static string SelectAgroPrice
            {
                get
                {
                    return "SELECT agro_price.* FROM agro_price WHERE agro_price.Id = ";
                }
            }

            public static string SelectAgroPricet
            {
                get
                {
                    return "SELECT COUNT(agro_pricet.Id) FROM agro_pricet WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroPriceJornal
            {
                get
                {
                    return "SELECT Id, DateInit, DateComand, Name, Comment FROM agro_price";
                }
            }
        }
        // DictionaryAgroCommand
        public class DictionaryAgroCulture
        {
            public static string InsertIntoAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_culture`(Name, Comment, Icon) VALUES (?Name, ?Comment, ?Icon)";

                        case MssqlUse:
                            return "INSERT INTO agro_culture(Name, Comment, Icon) VALUES (@Name, @Comment, @Icon)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_culture SET Name = ?Name, Icon = ?Icon, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_culture SET Name = @Name, Icon = @Icon, Comment = @Comment WHERE (ID = {0})";
                            ;
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroCulture
            {
                get
                {
                    return "DELETE FROM agro_culture WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroField
            {
                get
                {
                    return "SELECT COUNT(agro_field.Id) FROM agro_field WHERE Id_main  = {0}";
                }
            }

            public static string SelectFromAgroSeason
            {
                get
                {
                    return "SELECT COUNT(agro_season.Id) FROM agro_season WHERE Id_culture  = {0}";
                }
            }

            public static string SelectAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_culture.Id FROM agro_culture WHERE agro_culture.Name = ?Name AND agro_culture.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_culture.Id FROM agro_culture WHERE agro_culture.Name = @Name AND agro_culture.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroCultureJornal
            {
                get
                {
                    return "SELECT Id, Name, Icon, Comment FROM agro_culture ORDER BY Name";
                }
            }

            public static string SelectAgroCultureList
            {
                get
                {
                    return "SELECT Id, Name FROM agro_culture ORDER BY Name";
                }
            }
        }
        // DictionaryAgroCulture
        public class DictionaryAgroField
        {
            public static string InsertIntoAgroField
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_field`(Id_main, Id_zone, Name, Comment,IsOutOfDate) VALUES (?Id_main, ?Id_zone, ?Name, ?Comment,?IsOutOfDate)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_field(Id_main, Id_zone, Name, Comment,IsOutOfDate) VALUES (@Id_main, @Id_zone, @Name, @Comment,@IsOutOfDate)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroField
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_field SET Id_main = ?Id_main, Id_zone = ?Id_zone, Name = ?Name, Comment = ?Comment,IsOutOfDate=?IsOutOfDate WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_field SET Id_main = @Id_main, Id_zone = @Id_zone, Name = @Name, Comment = @Comment,IsOutOfDate=@IsOutOfDate WHERE (ID ={0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroField
            {
                get
                {
                    return "DELETE FROM agro_field WHERE ID = {0}";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return "SELECT COUNT(agro_OrderT.Id) FROM agro_OrderT WHERE Id_field = {0}";
                }
            }

            public static string SelectAgroOrdertControl
            {
                get
                {
                    return
                        "SELECT COUNT(agro_ordert_control.Id) FROM agro_ordert_control WHERE Id_object  = {0} AND Type_object = {1}";
                }
            }

            public static string SelectAgroField
            {
                get
                {
                    return "SELECT agro_field.Id, agro_field.Name FROM agro_field ORDER BY agro_field.Name";
                }
            }

            public static string SelectAgroFieldGroup
            {
                get
                {
                    return "SELECT agro_field.Id, agro_field.Name FROM agro_field  WHERE agro_field.Id_main = {0} ORDER BY agro_field.Name";
                }
            }

            public static string SelectAgroFieldZone
            {
                get
                {
                    return "SELECT agro_field.Id FROM agro_field WHERE agro_field.Id_zone = {0}";
                }
            }

            public static string SelectAgroFieldJornal
            {
                get
                {
                    return
                        @"SELECT agro_field.Id, agro_field.Id_main as FGROUPE, agro_field.Id_zone as FZONE, agro_field.Name, 100 * zones.Square as Square,agro_field.IsOutOfDate, agro_field.Comment FROM agro_field LEFT OUTER JOIN zones ON agro_field.Id_zone = zones.Zone_ID ";
                }
            }


        }
        // DictionaryAgroField
        public class DictionaryAgroFieldGrp
        {
            public static string SelectAgroFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_fieldgroupe`(Name, Comment, Id_Zonesgroup) VALUES (?Name, ?Comment, ?Id_Zonesgroup)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_fieldgroupe(Name, Comment, Id_Zonesgroup) VALUES (@Name, @Comment, @Id_Zonesgroup)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_fieldgroupe SET Name = ?Name, Comment = ?Comment, Id_Zonesgroup = ?Id_Zonesgroup WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_fieldgroupe SET Name = @Name, Comment = @Comment, Id_Zonesgroup = @Id_Zonesgroup WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroFieldGroup
            {
                get
                {
                    return "DELETE FROM agro_fieldgroupe WHERE ID = {0}";
                }
            }

            public static string SelectCountAgroField
            {
                get
                {
                    return "SELECT COUNT(agro_field.Id) FROM agro_field WHERE Id_main  = {0}";
                }
            }

            public static string SelectAgroFromFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_fieldgroupe.Id FROM agro_fieldgroupe WHERE agro_fieldgroupe.Name = ?Name AND agro_fieldgroupe.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_fieldgroupe.Id FROM agro_fieldgroupe WHERE agro_fieldgroupe.Name = @Name AND agro_fieldgroupe.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectFromZones
            {
                get
                {
                    return "SELECT zones.Zone_ID, zones.Name FROM zones WHERE ZonesGroupId = {0}";
                }
            }

            public static string SelectFromZonesJoin
            {
                get
                {
                    return @"SELECT  zones.Zone_ID, zones.Name
                            FROM agro_field
                            RIGHT OUTER JOIN zones
                            ON agro_field.Id_zone = zones.Zone_ID
                            WHERE zones.ZonesGroupId = {0} AND agro_field.Id IS NULL";
                }
            }

            public static string SelectFromAgroFieldJoin
            {
                get
                {
                    return @"SELECT agro_field.Id
                            FROM agro_field LEFT OUTER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                            WHERE agro_field.Id_main = {0} AND zones.Zone_ID IS NULL";
                }
            }

            public static string SelectFromAgroFieldGroupe
            {
                get
                {
                    return "SELECT Id, Name, Comment FROM  agro_fieldgroupe ORDER BY Name";
                }
            }

            public static string SelectAgroFieldGroupeList
            {
                get
                {
                    return
                        "SELECT agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM agro_fieldgroupe ORDER BY  agro_fieldgroupe.Name";
                    ;
                }
            }

            public static string SelectZones
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT zones.ZonesGroupId, count(zones.Zone_ID) AS Cnt
                                    FROM agro_field INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                                    WHERE agro_field.Id_main = {0} GROUP BY zones.ZonesGroupId
                                    ORDER BY Cnt DESC LIMIT 1";

                        case MssqlUse:
                            return @"SELECT TOP 1 zones.ZonesGroupId, count(zones.Zone_ID) AS Cnt
                                    FROM agro_field INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                                    WHERE agro_field.Id_main = {0} GROUP BY zones.ZonesGroupId
                                    ORDER BY Cnt DESC";
                    }

                    return "";
                }
            }
        }
        // DictionaryAgroFieldGrp
        public class DictionaryAgroPrice
        {
            public static string InsertIntoAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_pricet`(Id_main, Id_work, Id_mobitel, Id_agregat, Id_unit, Price,Comment) "
                            +
                            " VALUES (?Id_main, ?Id_work, ?Id_mobitel, ?Id_agregat, ?Id_unit, ?Price, ?Comment)";

                        case MssqlUse:
                            return "INSERT INTO agro_pricet(Id_main, Id_work, Id_mobitel, Id_agregat, Id_unit, Price, Comment) "
                            +
                            " VALUES (@Id_main, @Id_work, @Id_mobitel, @Id_agregat, @Id_unit, @Price, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_pricet SET Id_main = ?Id_main, Id_work = ?Id_work, Id_mobitel = ?Id_mobitel"
                            +
                            ", Id_agregat = ?Id_agregat, Id_unit = ?Id_unit, Price = ?Price, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_pricet SET Id_main = @Id_main, Id_work = @Id_work, Id_mobitel = @Id_mobitel" +
                            ", Id_agregat = @Id_agregat, Id_unit = @Id_unit, Price = @Price, Comment = @Comment WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroPricet
            {
                get
                {
                    return "DELETE FROM agro_pricet WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.`Comment`"
                            + " FROM agro_pricet";

                        case MssqlUse:
                            return "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.Comment"
                            + " FROM agro_pricet";
                    }

                    return "";
                }
            }
        }
        // DictionaryAgroPrice
        public class DictionaryAgroUnit
        {
            public static string InsertIntoAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_unit`(Name, NameShort, Factor, Comment) VALUES (?Name, ?NameShort, ?Factor, ?Comment)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_unit(Name, NameShort, Factor, Comment) VALUES (@Name, @NameShort, @Factor, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_unit SET Name = ?Name, NameShort = ?NameShort, Comment = ?Comment, Factor = ?Factor WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_unit SET Name = @Name, NameShort = @NameShort, Comment = @Comment, Factor = @Factor WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroUnit
            {
                get
                {
                    return "DELETE FROM agro_unit WHERE ID = {0}";
                }
            }

            public static string SelectAgroUnitCount
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE `Id_unit` = {0}";

                        case MssqlUse:
                            return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE Id_unit = {0}";
                    }

                    return "";
                }
            }

            public static string SelectFromAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_unit.Id FROM agro_unit WHERE agro_unit.Name = ?Name AND agro_unit.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_unit.Id FROM agro_unit WHERE agro_unit.Name = @Name AND agro_unit.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroUnitList
            {
                get
                {
                    return
                        "SELECT agro_unit.Id, agro_unit.NameShort as Name FROM agro_unit ORDER BY agro_unit.NameShort";
                }
            }

            public static string SelectAgroUnitJornal
            {
                get
                {
                    return "SELECT Id, Name ,NameShort , Factor, Comment  FROM  agro_unit ORDER BY Name";
                }
            }
        }// DictionaryAgroUnit

        public class DictionaryAgroWorkType
        {
            public static string InsertIntoAgroWork
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_work`(Name,SpeedBottom,SpeedTop,Comment,TypeWork,Id_main,OutLinkId) VALUES ({0}Name,{0}SpeedBottom,{0}SpeedTop,{0}Comment,{0}TypeWork,{0}Id_main,{0}OutLinkId)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_work(Name,SpeedBottom,SpeedTop,Comment,TypeWork,Id_main,OutLinkId) VALUES ({0}Name,{0}SpeedBottom,{0}SpeedTop,{0}Comment,{0}TypeWork,{0}Id_main,{0}OutLinkId)";
                    }

                    return "";
                }    
            }

            public static string UpdateAgroWork
            {
                get
                {
                    return
                        "UPDATE agro_work SET Name = {1}Name, SpeedBottom = {1}SpeedBottom, Comment = {1}Comment, SpeedTop = {1}SpeedTop, TypeWork = {1}TypeWork, Id_main = {1}Id_main, OutLinkId = {1}OutLinkId,TrackColor = {1}TrackColor   WHERE (ID = {0})";
                }
            }

            public static string DeleteAgroWork
            {
                get
                {
                    return "DELETE FROM agro_work WHERE ID = {0}";
                }
            }

            public static string SelectAgroPriceT
            {
                get
                {
                    return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE Id_work = {0}";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return "SELECT COUNT(agro_OrderT.Id) FROM agro_OrderT WHERE Id_work = {0}";
                }
            }

            public static string SelectFromAgroWork
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_work.Id FROM agro_work WHERE agro_work.Name = ?Name AND agro_work.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_work.Id FROM agro_work WHERE agro_work.Name = @Name AND agro_work.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkList
            {
                get
                {
                    return "SELECT agro_work.Id, agro_work.Name FROM agro_work ORDER BY agro_work.Name";
                }
            }

            public static string SelectAgroAlgorithmsList
            {
                get
                {
                    return string.Format(@"SELECT sensoralgorithms.AlgorithmID as Id,sensoralgorithms.Name
                    FROM sensoralgorithms
                    WHERE sensoralgorithms.AlgorithmID = {0} OR sensoralgorithms.AlgorithmID = {1} OR sensoralgorithms.AlgorithmID = {2} 
                    ORDER BY sensoralgorithms.Name", (int)AlgorithmType.AGREGAT_LOGIC, (int)AlgorithmType.ANGLE, (int)AlgorithmType.RANGEFINDER );
                }
            }

            public static string SelectAgroWorkJornal
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work_types.Name as TypeWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.`Comment`
                                    FROM agro_work
                                    INNER JOIN agro_work_types
                                    ON agro_work.TypeWork = agro_work_types.Id
                                    LEFT OUTER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY Name";

                        case MssqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work_types.Name as TypeWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.Comment
                                    FROM agro_work
                                    INNER JOIN agro_work_types
                                    ON agro_work.TypeWork = agro_work_types.Id
                                    LEFT OUTER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY Name";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkXml
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Id as GroupId, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.`Comment`
                                    FROM agro_work
                                    INNER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY agro_workgroup.Id,Name";

                        case MssqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Id as GroupId, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.Comment
                                    FROM agro_work
                                    INNER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY agro_workgroup.Id,Name";
                    }
                    return "";
                }
            }

            public static string SelectAgroWorkTypes
            {
                get
                {
                    return
                        " SELECT agro_work_types.Id, agro_work_types.Name FROM agro_work_types ORDER BY agro_work_types.Name";
                }
            }

            public static string SelectAgroWorkTypeWork
            {
                get
                {
                    return "SELECT agro_work.Id FROM agro_work WHERE agro_work.TypeWork = {0}";
                }
            }
        }
        // DictionaryAgroWorkType
        public class DictionaryAgroWorkTypeGroup
        {
            public static string SelectAgroWorkGroup
            {
                get
                {
                    return "SELECT agro_workgroup.* FROM agro_workgroup WHERE agro_workgroup.Id = {0}";
                }
            }

            public static string InsertIntoAgroWorkGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_workgroup`(Name, Comment,IsSpeedControl,IsDepthControl) VALUES (?Name, ?Comment,?IsSpeedControl,?IsDepthControl)";

                        case MssqlUse:
                            return "INSERT INTO agro_workgroup(Name, Comment,IsSpeedControl,IsDepthControl) VALUES (@Name, @Comment,@IsSpeedControl,@IsDepthControl)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroWorkGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_workgroup SET Name = ?Name, Comment = ?Comment, IsSpeedControl = ?IsSpeedControl, IsDepthControl = ?IsDepthControl WHERE (ID = {0})";

                        case MssqlUse:
                            return "UPDATE agro_workgroup SET Name = @Name, Comment = @Comment, IsSpeedControl = @IsSpeedControl, IsDepthControl = @IsDepthControl WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroWorkGroup
            {
                get
                {
                    return "DELETE FROM agro_workgroup WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroWorkGroup
            {
                get
                {
                    return "SELECT COUNT(agro_work.Id) FROM agro_work WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroWorkGroupId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_workgroup.Id FROM agro_workgroup WHERE agro_workgroup.Name = ?Name AND agro_workgroup.Id <> {0}";

                        case MssqlUse:
                            return
                                @"SELECT agro_workgroup.Id FROM agro_workgroup WHERE agro_workgroup.Name = @Name AND agro_workgroup.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkGroupList
            {
                get
                {
                    return
                        "SELECT agro_workgroup.Id, agro_workgroup.Name, agro_workgroup.IsSpeedControl, agro_workgroup.IsDepthControl,agro_workgroup.Comment FROM agro_workgroup ORDER BY agro_workgroup.Name";
                }
            }

            public static string SelectAgroWorkGroupJornal
            {
                get
                {
                    return
                        "SELECT agro_workgroup.Id, agro_workgroup.Name , agro_workgroup.IsSpeedControl, agro_workgroup.IsDepthControl,agro_workgroup.Comment FROM agro_workgroup ORDER BY agro_workgroup.Name";
                }
            }
        }
        // DictionaryAgroWorkTypeGroup
        public class DictionaryAgroSeason
        {
            public static string SelectAgroSeason
            {
                get
                {
                    return "SELECT   agro_season.* FROM agro_season WHERE   agro_season.Id = {0}";
                }
            }

            public static string SelectSeasonsTK
            {
                get
                {
                    return "SELECT   agro_fieldseason_tc.* FROM agro_fieldseason_tc WHERE   agro_fieldseason_tc.IdSeason = {0}";
                }
            }

            public static string InsertIntoAgroSeason
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_season`(DateStart, DateEnd,Id_culture,SquarePlan,`Comment`,`IsClosed`) VALUES (?DateStart, ?DateEnd,?Id_culture,?SquarePlan,?Comment,?IsClosed)";

                        case MssqlUse:
                            return "INSERT INTO agro_season(DateStart, DateEnd,Id_culture,SquarePlan,Comment,IsClosed) VALUES (@DateStart, @DateEnd,@Id_culture,@SquarePlan,@Comment,@IsClosed)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroSeason
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"UPDATE `agro_season`  
                            SET DateStart = ?DateStart
                            , DateEnd = ?DateEnd
                            , Id_culture =?Id_culture
                            , SquarePlan = ?SquarePlan
                            , Comment = ?Comment
                            , IsClosed = ?IsClosed
                            WHERE (ID = {0})";
                        case MssqlUse:
                            return @"UPDATE agro_season  
                            SET DateStart = @DateStart
                            , DateEnd = @DateEnd
                            , Id_culture = @Id_culture
                            , SquarePlan = @SquarePlan
                            , Comment = @Comment
                            , IsClosed = @IsClosed
                            WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroSeason
            {
                get
                {
                    return "DELETE FROM agro_season WHERE ID = {0}";
                }
            }

            public static string SelectAgroSeasonJornal
            {
                get
                {
                    return
                        @"SELECT
                        agro_season.Id,
                        agro_season.DateStart,
                        agro_season.DateEnd,
                        agro_season.Id_culture,
                        agro_season.SquarePlan,
                        agro_season.Comment
                        FROM agro_season
                        ORDER BY agro_season.DateStart DESC";
                }
            }
            public static string SelectIdFromCultureDates
            {
                get
                {
                    return @"SELECT agro_season.Id  FROM agro_season
                    WHERE DateStart = {0}DateStart AND DateEnd = {0}DateEnd AND Id_culture ={0}Id_culture";
                }
            }
        }
        // DictionaryAgroSeason
        public class DictionaryItem
        {
            public static string SelectFromAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DISTINCT agro_order.Id, 
                                    agro_order.`Date`, 
                                    {0} as Vehicle, 
                                    agro_order.SquareWorkDescript
                                    FROM agro_ordert
                                    INNER JOIN agro_order
                                    ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle
                                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert.{3} = {1}
                                    UNION SELECT DISTINCT agro_order.Id, agro_order.`Date`, {0} as Vehicle, 
                                    agro_order.SquareWorkDescript FROM agro_ordert_control
                                    INNER JOIN agro_order ON agro_ordert_control.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert_control.Type_object = {2}
                                    AND agro_ordert_control.Id_object = {1} ORDER BY ID";

                        case MssqlUse:
                            return @"SELECT DISTINCT agro_order.Id, 
                                    agro_order.Date, {0} as Vehicle, 
                                    agro_order.SquareWorkDescript
                                    FROM agro_ordert
                                    INNER JOIN agro_order
                                    ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle
                                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert.{3} = {1} UNION
                                    SELECT DISTINCT agro_order.Id, agro_order.Date, 
                                    {0} as Vehicle, agro_order.SquareWorkDescript
                                    FROM agro_ordert_control INNER JOIN agro_order
                                    ON agro_ordert_control.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert_control.Type_object = {2}
                                    AND agro_ordert_control.Id_object = {1}
                                    ORDER BY ID";
                    }

                    return "";
                }
            }
        }
        // DictionaryItem
        public class Agregat
        {
            public static string SelectSensors
            {
                get
                {
//                    return @"SELECT sensors.id , sensors.Name , vehicle.id as VID
//                            FROM  sensors
//                            INNER JOIN vehicle ON sensors.mobitel_id = vehicle.Mobitel_id 
//                            WHERE   sensors.Length = 1 
//                            ORDER BY sensors.Name";
                    return string.Format(@"SELECT
                            sensors.id,
                            sensors.Name,
                            vehicle.id AS VID,
                            relationalgorithms.AlgorithmID
                            FROM sensors
                            INNER JOIN vehicle
                            ON sensors.mobitel_id = vehicle.Mobitel_id
                            INNER JOIN relationalgorithms
                            ON sensors.id = relationalgorithms.SensorID
                            WHERE relationalgorithms.AlgorithmID = {0} OR relationalgorithms.AlgorithmID = {1}  OR relationalgorithms.AlgorithmID = {2}
                            ORDER BY sensors.Name", (int)AlgorithmType.AGREGAT_LOGIC, (int)AlgorithmType.ANGLE, (int)AlgorithmType.RANGEFINDER);
                }
            }

            public static string SelectVehicle
            {
                get
                {
                    return
                        String.Format("SELECT vehicle.id as Id, {0} as Name, vehicle.Team_id FROM vehicle ORDER BY {0}",
                                      DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse ? MsSqlvehicleIdent : MySqlvehicleIdent);
                }
            }
            public static string SelectStates
            {
                get
                {
                    return
                     String.Format("SELECT state.Id, {0} as Name,state.SensorId as SID FROM state ORDER BY {0}",
                                  DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse ? MsSqlStateIdent : MySqlStateIdent);

                }
            }

        }
        // Agregat
        public class Price
        {
            public static string SelectAgroPrice
            {
                get
                {
                    return
                        "SELECT agro_price.Id, DATE_FORMAT(agro_price.DateComand, '%d.%m.%Y') as DateComand FROM agro_price ORDER BY agro_price.DateComand DESC";
                }
            }

            public static string SelectAgroWork
            {
                get
                {
                    return "SELECT agro_work.Id, agro_work.Name FROM agro_work ORDER BY agro_work.Name";
                }
            }

            public static string SelectVehicleId
            {
                get
                {
                    return "SELECT vehicle.Mobitel_id as Id, " + AgroQuery.SqlVehicleIdent +
                    " AS Namr, vehicle.Team_id FROM vehicle";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    return "SELECT agro_agregat.Id, agro_agregat.Name FROM agro_agregat ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroUnit
            {
                get
                {
                    return
                        "SELECT agro_unit.Id, agro_unit.NameShort as Name FROM agro_unit ORDER BY agro_unit.NameShort";
                }
            }
        }
        // Price
        public class CompressData
        {
            public static string DeleteTable
            {
                get
                {
                    return "DELETE FROM {0} WHERE Id_main = {1}";
                }
            }

            public static string InsertIntoTable
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO {0} (Id_main, Lon_base, Lat_base, Speed_base, Lon_dev, Lat_dev, Speed_dev)"
                            + " VALUES ({1}, ?Lon_base, ?Lat_base, ?Speed_base, ?Lon_dev, ?Lat_dev, ?Speed_dev)";

                        case MssqlUse:
                            return "INSERT INTO {0} (Id_main, Lon_base, Lat_base, Speed_base, Lon_dev, Lat_dev, Speed_dev)"
                            + " VALUES ({1},@Lon_base,@Lat_base,@Speed_base,@Lon_dev,@Lat_dev,@Speed_dev)";
                    }

                    return "";
                }
            }

            public static string SelectFromTable
            {
                get
                {
                    return "SELECT {0}.* FROM {0} WHERE {0}.Id_main = {1} ORDER BY {0}.Id";
                }
            }
        }
        // CompressData
        public class OrderFueling
        {
            public static string InsertIntoAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO agro_fueling (Id_main, Lat, Lng, DateFueling, ValueStart, ValueSystem, ValueDisp, Location) 
                                    values (?Id_main, ?Lat, ?Lng, ?DateFueling, ?ValueStart, ?ValueSystem, ?ValueDisp, ?Location)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_fueling (Id_main, Lat, Lng, DateFueling, ValueStart, ValueSystem, ValueDisp, Location) 
                                    values (@Id_main, @Lat, @Lng, @DateFueling, @ValueStart, @ValueSystem, @ValueDisp, @Location)";
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroFueling
            {
                get
                {
                    return "DELETE FROM agro_fueling WHERE Id_main = {0} AND LockRecord = 0";
                }
            }

            public static string SelectFromAgroFueling
            {
                get
                {
                    return "SELECT * FROM  agro_fueling WHERE Id_main = {0} ORDER BY DateFueling";
                }
            }

            public static string UpdateFromAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set LockRecord = ?LockRecord WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set LockRecord = @LockRecord WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set ValueDisp = ?ValueDisp WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set ValueDisp = @ValueDisp WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFuelingRem
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set Remark = ?Remark WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set Remark = @Remark WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFuelingSet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set DateFueling = ?DateFueling WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set DateFueling = @DateFueling WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroFuelingSum
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT sum(agro_fueling.ValueDisp) AS sumDisp
                                    FROM agro_fueling WHERE agro_fueling.Id_main = {0}
                                    AND agro_fueling.ValueDisp {1} 0
                                    AND agro_fueling.DateFueling >= ?TimeStart 
                                    AND agro_fueling.DateFueling <= ?TimeEnd
                                    GROUP BY agro_fueling.Id_main, agro_fueling.DateFueling";

                        case MssqlUse:
                            return @"SELECT sum(agro_fueling.ValueDisp) AS sumDisp
                                    FROM agro_fueling WHERE agro_fueling.Id_main = {0}
                                    AND agro_fueling.ValueDisp {1} 0
                                    AND agro_fueling.DateFueling >= @TimeStart 
                                    AND agro_fueling.DateFueling <= @TimeEnd
                                    GROUP BY agro_fueling.Id_main, agro_fueling.DateFueling";
                    }

                    return "";
                }
            }

            public static string SelectAgroFuelingId
            {
                get
                {
                    return @"SELECT agro_fueling.* FROM agro_fueling WHERE Id_main = {0} AND LockRecord = 1";
                }
            }

            public static string DeleteAgroFuelingId
            {
                get
                {
                    return "DELETE FROM agro_fueling WHERE Id = {0}";
                }
            }
        }
        // OrderFueling
        public class OrderItem
        {
            public static string sqlInsertHead
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return SSqlinsertHead;

                        case MssqlUse:
                            return MSsSqLinsertHead;
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_order`(`Date`, Id_mobitel, Comment, StateOrder, UserCreated) VALUES
                                    (?Date, ?Mobitel_Id, ?Comment, ?StateOrder, ?UserCreated)";

                        case MssqlUse:
                            return @"INSERT INTO agro_order(Date, Id_mobitel, Comment, StateOrder, UserCreated) VALUES 
                                    (@Date, @Mobitel_Id, @Comment, @StateOrder, @UserCreated)";
                    }

                    return "";
                }
            }

            public static string DeleteAgroOrder
            {
                get
                {
                    return "DELETE FROM agro_order WHERE ID = {0}";
                }
            }

            public static string UpdateAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE `agro_order` Set `Date` = ?Date, Id_mobitel = ?Mobitel_Id, Comment = ?Comment "
                            + " WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_order Set Date = @Date, Id_mobitel = @Mobitel_Id, Comment = @Comment "
                            + " WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderCount
            {
                get
                {
                    return "SELECT COUNT(agro_order.Id) AS CNT FROM agro_order";
                }
            }

            public static string SelectAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT   agro_ordert.Id, agro_fieldgroupe.Name AS GFName, 
                                    IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', 
                                    IFNULL(agro_field.Name, '{0}'))) AS FName, zones.Zone_ID, zones.Square * 100 AS Square, {4} as Driver, 
                                    agro_ordert.DrvInputSource, agro_ordert.Id_agregat, agro_agregat.Name AS AName, agro_ordert.AgrInputSource, 
                                    agro_agregat.Width,agro_ordert.TimeStart, agro_ordert.TimeEnd, agro_ordert.TimeMove, agro_ordert.TimeStop, 
                                    agro_ordert.TimeRate, agro_ordert.FactTime, agro_ordert.Distance, agro_ordert.FactSquare, 
                                    agro_ordert.FactSquareCalc,  agro_ordert.SpeedAvg 
                                    ,CONCAT(IFNULL(driver.Family,''),' ' , IFNULL(driver.Name,'')) AS Family
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id
                                    LEFT OUTER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID
                                    WHERE   agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT   agro_ordert.Id, agro_fieldgroupe.Name AS GFName, (CASE WHEN agro_ordert.Id_zone = {2} THEN '{3}' ELSE
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN '{0}' ELSE ISNULL(agro_field.Name, '{0}')END)END) AS 
                                    FName, zones.Zone_ID, zones.Square * 100 AS Square,  {4} as Driver, agro_ordert.Id_agregat, 
                                    agro_agregat.Name AS AName, agro_ordert.AgrInputSource, agro_agregat.Width,agro_ordert.TimeStart,
                                    agro_ordert.TimeEnd, agro_ordert.TimeMove, agro_ordert.TimeStop, agro_ordert.TimeRate, 
                                    agro_ordert.FactTime, agro_ordert.Distance, agro_ordert.FactSquare, agro_ordert.FactSquareCalc,  
                                    agro_ordert.SpeedAvg 
                                    ,cast(ISNULL(driver.Family,'') AS VARCHAR) + '  ' + cast(ISNULL(driver.Name,'') AS VARCHAR)   AS Family
                                    FROM agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id
                                    LEFT OUTER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID
                                    WHERE   agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectIfAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                     @"SELECT IF(agro_ordert.Id_zone= {2},'{3}',IFNULL(agro_field.Name, '{0}')) AS FName
                                    ,IFNULL(zones.Square, 0)*100 AS FSquare
                                    ,agro_ordert.Id_driver
                                    ,agro_ordert.Id_agregat
                                    ,SUM(agro_ordert.Distance) AS D 
                                    ,SUM(agro_ordert.FactSquare) AS FS
                                    ,SUM(agro_ordert.FactSquareCalc) AS FSC
                                    ,SUM(agro_ordert.FactSquareJointProcessing) AS FSJP
                                    ,0.0 as FSCOverLap
                                    ,CONCAT(IFNULL(driver.Family,''),' ' , IFNULL(driver.Name,'')) AS Driver
                                    ,agro_agregat.Name AS Agregat
                                    ,agro_ordert.Id_zone
                                    FROM  agro_ordert 
                                    Left Outer Join  agro_field ON agro_ordert.Id_field = agro_field.Id 
                                    Left Outer Join  zones   ON zones.Zone_ID = agro_ordert.Id_zone 
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id 
                                    LEFT OUTER JOIN  driver ON driver.id = agro_ordert.Id_driver
                                    WHERE agro_ordert.Id_main = {1} AND agro_ordert.Confirm <> 0
                                    GROUP BY FName,FSquare, agro_ordert.Id_driver, agro_ordert.Id_agregat,agro_ordert.Id_zone
                                    ,CONCAT(IFNULL(driver.Family,''),' ' , IFNULL(driver.Name,''))
                                UNION
                                    SELECT '{0}' AS FName 
                                    ,0 AS FSquare 
                                    ,agro_ordert.Id_driver
                                    ,agro_ordert.Id_agregat
                                    ,sum(agro_ordert.Distance) AS D 
                                    ,0 AS FS
                                    ,0 AS FSC 
                                    ,0 AS FSJP 
                                    ,0.0 as FSCOverLap
                                    , CONCAT(IFNULL(driver.Family,''),' ' , IFNULL(driver.Name,''))  AS Driver
                                    ,agro_agregat.Name AS Agregat
                                    ,agro_ordert.Id_zone
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                    LEFT OUTER JOIN driver ON driver.id = agro_ordert.Id_driver
                                    WHERE agro_ordert.Id_main = {1} AND agro_ordert.Confirm = 0
                                    GROUP BY  agro_ordert.Id_driver, 
                                    agro_ordert.Id_agregat, driver.Family, agro_agregat.Name,agro_ordert.Id_zone
                                    ,CONCAT(IFNULL(driver.Family,''),' ' , IFNULL(driver.Name,''))
                                    ORDER BY FName";
                        case MssqlUse:
                            return
                                    @"SELECT (CASE WHEN (agro_ordert.Id_zone = {2}) THEN '{3}' ELSE (isnull(agro_field.Name, '{0}')) END) AS FName 
                                        ,isnull(zones.Square, 0) * 100 AS FSquare 
                                        ,agro_ordert.Id_driver
                                        ,agro_ordert.Id_agregat
                                        ,sum(agro_ordert.Distance) AS D,sum(agro_ordert.FactSquare) AS FS 
                                        ,sum(agro_ordert.FactSquareCalc) AS FSC 
                                        ,0.0 as FSCOverLap
                                        ,cast(ISNULL(driver.Family,'') AS VARCHAR) + '  ' + cast(ISNULL(driver.Name,'') AS VARCHAR)  AS Driver
                                        ,agro_agregat.Name AS Agregat
                                        ,agro_ordert.Id_zone
                                        FROM agro_ordert
                                        LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                        LEFT OUTER JOIN zones ON zones.Zone_ID = agro_ordert.Id_zone
                                        LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                        LEFT OUTER JOIN driver ON driver.id = agro_ordert.Id_driver
                                        WHERE agro_ordert.Id_main = {1}  AND agro_ordert.Confirm <> 0
                                        GROUP BY agro_ordert.Id_zone, agro_ordert.Confirm, agro_ordert.Id_driver, 
                                        agro_ordert.Id_agregat, agro_field.Name, zones.Square, agro_agregat.Name
                                        , cast(ISNULL(driver.Family,'') AS VARCHAR) + '  ' + cast(ISNULL(driver.Name,'') AS VARCHAR)
                                    UNION
                                        SELECT '{0}' AS FName 
                                        ,0 AS FSquare 
                                        ,agro_ordert.Id_driver
                                        ,agro_ordert.Id_agregat
                                        ,sum(agro_ordert.Distance) AS D,0 AS FS 
                                        ,0 AS FSC
                                        ,0.0 as FSCOverLap
                                        ,cast(ISNULL(driver.Family,'') AS VARCHAR) + '  ' + cast(ISNULL(driver.Name,'') AS VARCHAR)  AS Driver
                                        ,agro_agregat.Name AS Agregat
                                        ,agro_ordert.Id_zone
                                        FROM agro_ordert
                                        LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                        LEFT OUTER JOIN driver ON driver.id = agro_ordert.Id_driver
                                        WHERE agro_ordert.Id_main = {1} AND agro_ordert.Confirm = 0
                                        GROUP BY  agro_ordert.Id_zone,agro_ordert.Confirm, agro_ordert.Id_driver, 
                                        agro_ordert.Id_agregat, agro_agregat.Name
                                        , cast(ISNULL(driver.Family,'') AS VARCHAR) + '  ' + cast(ISNULL(driver.Name,'') AS VARCHAR)
                                        ORDER BY FName";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert.Id, IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', IFNULL(agro_field.Name, '{0}'))) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.FuelAdd,
                                    agro_ordert.FuelStart, agro_ordert.FuelSub, agro_ordert.FuelEnd, agro_ordert.FuelExpens,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.FuelExpensAvg) as FuelExpensAvg, agro_ordert.FuelExpensAvgRate
                                    FROM    agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE   agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT   agro_ordert.Id, (CASE WHEN agro_ordert.Id_zone= {2} THEN '{3}' ELSE (CASE WHEN agro_ordert.Confirm = 0 then '{0}' else ISNULL(agro_field.Name, '{0}')end)end) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.FuelAdd,
                                    agro_ordert.FuelStart, agro_ordert.FuelSub, agro_ordert.FuelEnd, agro_ordert.FuelExpens,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FuelExpensAvg end) as FuelExpensAvg, agro_ordert.FuelExpensAvgRate
                                    FROM    agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE   agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertIf
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert.Id, IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', IFNULL(agro_field.Name, '{0}'))) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.Fuel_ExpensMove,
                                    agro_ordert.Fuel_ExpensStop, agro_ordert.Fuel_ExpensTotal, agro_ordert.Fuel_ExpensAvg,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.Fuel_ExpensAvgRate) as Fuel_ExpensAvgRate
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert.Id, (CASE WHEN agro_ordert.Id_zone = {2} THEN '{3}' ELSE (CASE WHEN agro_ordert.Confirm = 0 then '{0}' else ISNULL(agro_field.Name, '{0}')end)end) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.Fuel_ExpensMove,
                                    agro_ordert.Fuel_ExpensStop, agro_ordert.Fuel_ExpensTotal, agro_ordert.Fuel_ExpensAvg,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.Fuel_ExpensAvgRate end) as Fuel_ExpensAvgRate
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertControl
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return

                                @"SELECT agro_ordert_control.Id, agro_ordert_control.Id_object, agro_ordert_control.TimeStart,agro_ordert_control.TimeEnd , 
                                    TIME_Format(TIMEDIFF(agro_ordert_control.TimeEnd, agro_ordert_control.TimeStart), '%H:%i') as Time
                                    FROM  agro_ordert_control WHERE agro_ordert_control.Type_object = {0}
                                    AND agro_ordert_control.Id_main = {1} ORDER BY agro_ordert_control.TimeStart";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert_control.Id, agro_ordert_control.Id_object, agro_ordert_control.TimeStart, agro_ordert_control.TimeEnd,
                                    dbo.get_hh_min(DATEDIFF(mi, agro_ordert_control.TimeStart, agro_ordert_control.TimeEnd)) as Time
                                    FROM  agro_ordert_control WHERE   agro_ordert_control.Type_object = {0}
                                    AND agro_ordert_control.Id_main = {1} ORDER BY agro_ordert_control.TimeStart";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertControlFrom
            {
                get
                {
                    return
                        "SELECT agro_ordert_control.* FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}";
                }
            }


            public static string SelectAgroEventsManual
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_event_manually.Id,agro_event_manually.TimeStart,agro_event_manually.TimeEnd,agro_event_manually.Id_work
                                FROM agro_event_manually
                                WHERE agro_event_manually.Id_mobitel = {0} 
                                AND ({1}DateStart  between agro_event_manually.TimeStart and agro_event_manually.TimeEnd
                                OR {1}DateEnd  between agro_event_manually.TimeStart and agro_event_manually.TimeEnd
                                OR agro_event_manually.TimeStart between {1}DateStart and {1}DateEnd 
                                OR {1}DateEnd > agro_event_manually.TimeStart and IFNULL(agro_event_manually.TimeEnd,0) = 0)
                                AND IFNULL( (SELECT   agro_ordert.Id FROM agro_ordert WHERE agro_ordert.Id_event_manually = agro_event_manually.Id LIMIT 1),0) =0
                                ORDER BY agro_event_manually.TimeStart";
                        case MssqlUse:
                            return @"SELECT agro_event_manually.Id,agro_event_manually.TimeStart,agro_event_manually.TimeEnd,agro_event_manually.Id_work
                                FROM agro_event_manually
                                WHERE agro_event_manually.Id_mobitel = {0} 
                                AND ({1}DateStart  between agro_event_manually.TimeStart and agro_event_manually.TimeEnd
                                OR {1}DateEnd  between agro_event_manually.TimeStart and agro_event_manually.TimeEnd
                                OR agro_event_manually.TimeStart between {1}DateStart and {1}DateEnd 
                                OR {1}DateEnd > agro_event_manually.TimeStart and ISNULL(agro_event_manually.TimeEnd,0) = 0)
                                AND ISNULL( (SELECT TOP 1  agro_ordert.Id FROM agro_ordert WHERE agro_ordert.Id_event_manually = agro_event_manually.Id),0) =0
                                ORDER BY agro_event_manually.TimeStart";
                    }
                    return "";
                }
            }


            public static string SelectAgroOrdertFrom
            {
                get
                {
                    return
                        "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.Id_main = {0} AND agro_ordert.LockRecord =1 ORDER BY TimeStart";
                }
            }

            public static string SelectAgroOrderId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_order.Id,agro_order.`Date`,{0} as Veh FROM   agro_order LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_order.Id_mobitel = {1} AND Year(agro_order.`Date`) = ?Y AND Month(agro_order.`Date`) = ?M AND Day(agro_order.`Date`) = ?D";

                        case MssqlUse:
                            return
                                @"SELECT agro_order.Id,agro_order.Date,{0} as Veh FROM   agro_order LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_order.Id_mobitel = {1} AND Year(agro_order.Date) = @Y AND Month(agro_order.Date) = @M AND Day(agro_order.Date) = @D";
                    }

                    return "";
                }
            }

            public static string UpdateAgroOrderExist
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"UPDATE agro_order SET 
                                  TimeStart = ?TimeStart
                                , TimeEnd = ?TimeEnd
                                , TimeWork = ?TimeWork
                                , TimeMove = ?TimeMove
                                , LocationStart = ?LocationStart
                                , LocationEnd = ?LocationEnd
                                , Distance = ?Distance
                                , SpeedAvg = ?SpeedAvg
                                , SquareWorkDescript = ?SquareWorkDescript
                                , SquareWorkDescripOverlap = ?SquareWorkDescripOverlap
                                , FactSquareCalcOverlap = ?FactSquareCalcOverlap
                                , PathWithoutWork = ?PathWithoutWork
                                , FuelDUTExpensSquare = ?FuelDUTExpensSquare
                                , FuelDUTExpensAvgSquare = ?FuelDUTExpensAvgSquare
                                , FuelDUTExpensWithoutWork = ?FuelDUTExpensWithoutWork
                                , FuelDUTExpensAvgWithoutWork = ?FuelDUTExpensAvgWithoutWork
                                , FuelDRTExpensSquare = ?FuelDRTExpensSquare
                                , FuelDRTExpensAvgSquare = ?FuelDRTExpensAvgSquare
                                , FuelDRTExpensWithoutWork = ?FuelDRTExpensWithoutWork
                                , FuelDRTExpensAvgWithoutWork = ?FuelDRTExpensAvgWithoutWork
                                , DateLastRecalc = ?DateLastRecalc
                                , PointsValidity = ?PointsValidity
                                , PointsCalc = ?PointsCalc
                                , PointsFact = ?PointsFact
                                , PointsIntervalMax=?PointsIntervalMax
                                , StateOrder=?StateOrder
                                WHERE (ID = {0})";

                        case MssqlUse:
                            return @"UPDATE agro_order SET 
                                  TimeStart = @TimeStart
                                , TimeEnd = @TimeEnd
                                , TimeWork = @TimeWork
                                , TimeMove = @TimeMove
                                , LocationStart = @LocationStart
                                , LocationEnd = @LocationEnd
                                , Distance = @Distance
                                , SpeedAvg = @SpeedAvg
                                , SquareWorkDescript = @SquareWorkDescript
                                , SquareWorkDescripOverlap = @SquareWorkDescripOverlap
                                , FactSquareCalcOverlap = @FactSquareCalcOverlap
                                , PathWithoutWork = @PathWithoutWork
                                , FuelDUTExpensSquare = @FuelDUTExpensSquare
                                , FuelDUTExpensAvgSquare = @FuelDUTExpensAvgSquare
                                , FuelDUTExpensWithoutWork = @FuelDUTExpensWithoutWork
                                , FuelDUTExpensAvgWithoutWork = @FuelDUTExpensAvgWithoutWork
                                , FuelDRTExpensSquare = @FuelDRTExpensSquare
                                , FuelDRTExpensAvgSquare = @FuelDRTExpensAvgSquare
                                , FuelDRTExpensWithoutWork = @FuelDRTExpensWithoutWork
                                , FuelDRTExpensAvgWithoutWork = @FuelDRTExpensAvgWithoutWork
                                , DateLastRecalc = @DateLastRecalc
                                , PointsValidity = @PointsValidity
                                , PointsCalc = @PointsCalc
                                , PointsFact = @PointsFact
                                , PointsIntervalMax=@PointsIntervalMax
                                , StateOrder=@StateOrder
                                 WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderVehicle
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_order.*, vehicle.Team_id,{0} AS MobitelName"
                            +
                            " FROM   agro_order   LEFT OUTER JOIN  vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_Id WHERE agro_order.Id = {1}";

                        case MssqlUse:
                            return @"SELECT agro_order.*, vehicle.Team_id,{0} AS MobitelName"
                            + " FROM   agro_order   LEFT OUTER JOIN  vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_Id WHERE agro_order.Id = {1}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroOrderState
            {
                get
                {
                    return @"UPDATE agro_order SET StateOrder = {0} WHERE (ID = {1})";
                }
            }

            public static string UpdateAgroOrderBlock
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"UPDATE agro_order SET BlockUserId = ?BlockUserId,BlockDate = ?BlockDate WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                @"UPDATE agro_order SET BlockUserId = @BlockUserId,BlockDate = @BlockDate WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderObj
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.TimeEnd > ?LastControlTime AND agro_ordert.Id_main = {0} ORDER BY Id";

                        case MssqlUse:
                            return
                                "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.TimeEnd > @LastControlTime AND agro_ordert.Id_main = {0} ORDER BY Id";
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroOrdertControl
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main,Id_object,Type_object,TimeStart,TimeEnd)"
                            + " VALUES ({0},{1},{2},?TimeStart,?TimeEnd)";

                        case MssqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main,Id_object,Type_object,TimeStart,TimeEnd)"
                            + " VALUES ({0},{1},{2},@TimeStart,@TimeEnd)";
                    }

                    return "";
                }
            }

            public static string DeleteAgroOrdertControl
            {
                get
                {
                    return "DELETE FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}";
                }
            }

            public static string SelectAgroOrdertControlTime
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_ordert_control.TimeEnd FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}"
                            +
                            " AND agro_ordert_control.Type_object = {1} ORDER BY agro_ordert_control.TimeEnd DESC Limit 1";

                        case MssqlUse:
                            return "SELECT TOP 1 agro_ordert_control.TimeEnd FROM  agro_ordert_control WHERE agro_ordert_control.Id_main = {0}"
                            +
                            " AND agro_ordert_control.Type_object = {1} ORDER BY agro_ordert_control.TimeEnd DESC";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertCount
            {
                get
                {
                    return "SELECT count(agro_ordert.Id) AS CNT FROM agro_ordert WHERE agro_ordert.Id_main = {0}";
                }
            }

            public static string UpdateAgroOrdertRecord
            {
                get
                {
                    return @"UPDATE agro_ordert SET LockRecord = {0} WHERE (Id_main = {1})";
                }
            }

            public static string SelectAgroPricetPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                            + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                            + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                            + " WHERE   agro_price.DateComand < ?DateComand AND agro_pricet.Id_work = " +
                            "{0}" + " AND agro_pricet.Id_mobitel = " + "{1}"
                            + " ORDER BY   agro_price.DateComand DESC";

                        case MssqlUse:
                            return "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                            + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                            + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                            + " WHERE   agro_price.DateComand < @DateComand AND agro_pricet.Id_work = " +
                            "{0}" + " AND agro_pricet.Id_mobitel = " + "{0}"
                            + " ORDER BY   agro_price.DateComand DESC";
                    }

                    return "";
                }
            }

            public static string SelectDriversCount
            {
                get
                {
                    return @"SELECT agro_ordert.Id_driver,COUNT(agro_ordert.Id) AS Cnt
                    FROM agro_ordert
                    WHERE agro_ordert.Id_main = {0} AND agro_ordert.Id_driver > 0 AND agro_ordert.Id_zone > 0
                    GROUP BY agro_ordert.Id_driver,
                    agro_ordert.Id_main";
                }
            }
        }
        // OrderItem
        public class OrderList
        {
            public static string SelectAgroOrder
            {
                get
                {
                    return
                        @"SELECT agro_order.*, vehicle_category.Name AS Category, (SELECT SUM(agro_ordert.FactSquare) FROM agro_ordert WHERE agro_ordert.Id_main = agro_order.Id AND agro_ordert.Confirm <> 0) as FactSquare,
                            (SELECT SUM(agro_ordert.FactSquareCalc) FROM agro_ordert WHERE agro_ordert.Id_main = agro_order.Id AND agro_ordert.Confirm=1) as FactSquareCalc
                            FROM agro_order LEFT OUTER JOIN mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                            LEFT OUTER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                            LEFT OUTER JOIN vehicle_category ON vehicle.Category_id = vehicle_category.Id";
                }
            }

            public static string WhereAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return " WHERE (agro_order.`Date` >= ?TimeStart and agro_order.`Date` < ?TimeEnd)";

                        case MssqlUse:
                            return " WHERE (agro_order.Date >= @TimeStart and agro_order.Date < @TimeEnd)";
                    }

                    return "";
                }
            }

            public static string WhereAgroOrderId
            {
                get
                {
                    return " WHERE agro_order.Id = ";
                }
            }

            public static string AndAgroOrderIdMobitel
            {
                get
                {
                    return " AND agro_order.Id_mobitel = ";
                }
            }

            public static string EndVehicleTeamId
            {
                get
                {
                    return " AND vehicle.Team_id = ";
                }
            }

            public static string AndMobitelsId
            {
                get
                {
                    return " AND mobitels.Mobitel_ID = ";
                }
            }

            public static string OrderByAgroOrder
            {
                get
                {
                    return " ORDER BY agro_order.Id";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert.Id as Idt,vehicle.MakeCar, vehicle.CarModel, vehicle.NumberPlate, 
                                    agro_agregat.InvNumber, agro_agregat.Name as AgrName, Concat(driver.Family, ' ', driver.Name) as Family,agro_fieldgroupe.Name as FieldGroupName, agro_field.Name as FieldName, 
                                    agro_work.Name as WorkName, agro_ordert.FactSquareCalc, IF (zones.Square = 0, 0, agro_ordert.FactSquareCalc/zones.Square) as PersSquare, 
                                    agro_order.Id, agro_ordert.`Comment`, agro_ordert.TimeStart, agro_ordert.TimeEnd, 
                                    DATE_FORMAT(agro_ordert.FactTime,'%H:%i') as FactTime, DATE_FORMAT(agro_ordert.TimeMove,'%H:%i') as TimeMove, 
                                    DATE_FORMAT(agro_ordert.TimeRate,'%H:%i') as TimeRate, agro_ordert.Distance, agro_ordert.SpeedAvg, 
                                    agro_ordert.Fuel_ExpensTotal as Fuel_ExpensTotal,
                                    agro_ordert.Fuel_ExpensAvg as Fuel_ExpensAvg,
                                    agro_ordert.Fuel_ExpensAvgRate as Fuel_ExpensAvgRate,
                                    agro_ordert.Fuel_ExpensMove as Fuel_ExpensMove,
                                    agro_ordert.Fuel_ExpensStop as Fuel_ExpensStop,
                                    IF (agro_ordert.Confirm = 0, 0, agro_ordert.FuelExpens) as FuelExpens, 
                                    IF (agro_ordert.Confirm = 0, 0, agro_ordert.FuelExpensAvg) as FuelExpensAvg, 
                                    IF (agro_ordert.Confirm = 0, agro_ordert.FuelExpens, 0) as FuelExpensRun, 
                                    IF (agro_ordert.Confirm = 0, IF (agro_ordert.Distance = 0, 0, agro_ordert.FuelExpens/(100 * agro_ordert.Distance)), 0) as FuelExpensRunKm, 
                                    agro_order.Id_mobitel,zones.OutLinkId FROM agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id 
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id ";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert.Id AS Idt, vehicle.MakeCar, vehicle.CarModel, vehicle.NumberPlate, 
                                    agro_agregat.InvNumber, agro_agregat.Name AS AgrName, (driver.Family + ' ' + driver.Name) as Family,agro_fieldgroupe.Name as FieldGroupName, agro_field.Name AS FieldName, 
                                    agro_work.Name AS WorkName, agro_ordert.FactSquareCalc, (CASE WHEN zones.Square = 0 THEN 0 ELSE agro_ordert.FactSquareCalc / zones.Square END) AS PersSquare,
                                    agro_order.Id, agro_ordert.Comment, agro_ordert.TimeStart, agro_ordert.TimeEnd, 
                                    dbo.get_hh_min(datediff(mi, convert(TIME, '00:00:00', 108), agro_ordert.FactTime)) AS FactTime, 
                                    dbo.get_hh_min(DATEDIFF(mi, convert(TIME, '00:00:00', 108), agro_ordert.TimeMove)) AS TimeMove, 
                                    dbo.get_hh_min(DATEDIFF(mi, convert(TIME, '00:00:00', 108), agro_ordert.TimeRate)) AS TimeRate, 
                                    agro_ordert.Distance, agro_ordert.SpeedAvg, (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FuelExpens END) AS FuelExpens, 
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FuelExpensAvg END) AS FuelExpensAvg, 
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN agro_ordert.FuelExpens ELSE 0 END) AS FuelExpensRun, 
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN (CASE WHEN agro_ordert.Distance = 0 THEN 0 ELSE agro_ordert.FuelExpens / (100 * agro_ordert.Distance) END) ELSE 0 END) AS FuelExpensRunKm, 
                                    agro_order.Id_mobitel,zones.OutLinkId FROM agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id INNER JOIN vehicle
                                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    LEFT OUTER JOIN agro_agregat
                                    ON agro_ordert.Id_agregat = agro_agregat.Id
                                    LEFT OUTER JOIN agro_field
                                    ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN driver
                                    ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN zones
                                    ON agro_ordert.Id_zone = zones.Zone_ID
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id 
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id ";
                    }

                    return "";
                }
            }

            public static string SelectCountCnt
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT COUNT(*) as CNT from (SELECT DISTINCT agro_ordert.Id_main,agro_ordert.Id_field FROM agro_ordert
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE (agro_ordert.Confirm = 1) AND (agro_order.`Date` >= ?TimeStart and agro_order.`Date` < ?TimeEnd)";

                        case MssqlUse:
                            return
                                @"SELECT COUNT(*) as CNT from (SELECT DISTINCT agro_ordert.Id_main,agro_ordert.Id_field  FROM  agro_ordert
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id LEFT OUTER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE (agro_ordert.Confirm = 1) AND (agro_order.Date >= @TimeStart and agro_order.Date < @TimeEnd)";
                    }

                    return "";
                }
            }

            public static string SelectCountCntOrderId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT COUNT(*) as CNT from (SELECT DISTINCT agro_ordert.Id_main,agro_ordert.Id_field FROM agro_ordert
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE (agro_ordert.Confirm = 1) AND (agro_order.`Id` = {0})";

                        case MssqlUse:
                            return
                                @"SELECT COUNT(*) as CNT from (SELECT DISTINCT agro_ordert.Id_main,agro_ordert.Id_field  FROM  agro_ordert
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id LEFT OUTER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE (agro_ordert.Confirm = 1) AND (agro_order.Id = {0})";
                    }

                    return "";
                }
            }

            public static string SelectZones
            {
                get
                {
                    return
                        @"SELECT zones.Zone_ID, agro_field.Id, agro_fieldgroupe.Name as GrpName, agro_field.Name as FieldName, 
                            Round(zones.Square * 100,3) as Sq, ({0}) as ORDERS FROM agro_fieldgroupe
                            RIGHT OUTER JOIN agro_field ON agro_fieldgroupe.Id = agro_field.Id_main
                            INNER JOIN zones ON zones.Zone_ID = agro_field.Id_zone
                            WHERE ({0}) > 0 ORDER BY GrpName,FieldName";
                }
            }

            public static string AndVehicleTeam
            {
                get
                {
                    return "{0} AND vehicle.Team_id = {1} ";
                }
            }

            public static string AndAgroOrder
            {
                get
                {
                    return " {0} AND agro_order.Id_mobitel = {1} ";
                }
            }

            public static string WhereIdField
            {
                get
                {
                    return ") as CC WHERE CC.Id_field = agro_field.Id";
                }
            }
        }
        // OrderList
        public class OrderItemContent
        {
            public static string DeleteFromAgroOrderT
            {
                get
                {
                    return "DELETE FROM agro_ordert WHERE agro_ordert.Id_main = {0} AND agro_ordert.LockRecord = 0";
                }
            }

            public static string SelectCountAgroOrderT
            {
                get
                {
                    return "SELECT Count(agro_ordert.Id) as CNT, SUM(agro_ordert.FactSquareCalc) AS SS, SUM(agro_ordert.FuelExpens) AS FEDUT, SUM(agro_ordert.Fuel_ExpensTotal) AS FEDRT, agro_field.Name, zones.Square,zones.Zone_ID "
                    + " FROM agro_ordert INNER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id "
                    + " INNER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID "
                    + " WHERE agro_ordert.Confirm = 1"
                    + " AND agro_ordert.Id_main = {0}"
                    +
                    " GROUP BY agro_ordert.Id_main, agro_ordert.Confirm, agro_field.Name, zones.Square, zones.Zone_ID";
                }
            }

            public static string SelectSumAgroOrderT
            {
                get
                {
                    return "SELECT SUM(agro_ordert.Distance) AS DS, SUM(agro_ordert.FuelExpens) AS FEDUT,SUM(agro_ordert.Fuel_ExpensTotal) AS FEDRT"
                    + " FROM agro_ordert "
                    + " WHERE (agro_ordert.Confirm = 0 OR agro_ordert.Id_field = 0 )"
                    + " AND agro_ordert.Id_main = {0}"
                    + " GROUP BY agro_ordert.Id_main";
                }
            }

            public static string DeleteFromAgroOrdertControl
            {
                get
                {
                    return "DELETE FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}"
                    + " AND agro_ordert_control.Type_object = ";
                }
            }

            public static string SelectAgroOrdertTimeStart
            {
                get
                {
                    return
                        "SELECT agro_ordert.TimeStart, agro_ordert.TimeEnd, agro_ordert.Confirm FROM agro_ordert WHERE agro_ordert.Id_main = {0}";
                }
            }

            public static string InsertIntoAgroOrderControl
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main, Id_object, Type_object, TimeStart, TimeEnd)"
                            + " VALUES ({0}, {1}, {2}, ?TimeStart, ?TimeEnd)";

                        case MssqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main, Id_object, Type_object, TimeStart, TimeEnd)"
                            + " VALUES ({0}, {1}, {2}, @TimeStart, @TimeEnd)";
                    }

                    return "";
                }
            }
        }
        // OrderItemContent
        public class Order
        {
            public static string SelectAgroOrdertTimeStart
            {
                get
                {
                    return "SELECT team.id, team.Name FROM team ORDER BY team.Name";
                }
            }

            public static string SelectVehicleMobitelId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id, " + MySqlvehicleIdent +
                            " AS F, vehicle.Team_id FROM vehicle"
                            + " WHERE (vehicle.Team_id IS NULL) ORDER BY " + MySqlvehicleIdent;

                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id, " + MsSqlvehicleIdent +
                            " AS F, vehicle.Team_id FROM vehicle"
                            + " WHERE (vehicle.Team_id IS NULL) ORDER BY " + MsSqlvehicleIdent;
                    }

                    return "";
                }
            }

            public static string SelectVehicleMobitel
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id, " + MySqlvehicleIdent +
                            " AS F, vehicle.Team_id FROM vehicle"
                            + " WHERE (vehicle.Team_id = {0}" +
                            " OR vehicle.Team_id IS NULL) ORDER BY " + MySqlvehicleIdent;

                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id, " + MsSqlvehicleIdent +
                            " AS F, vehicle.Team_id FROM vehicle"
                            + " WHERE (vehicle.Team_id = {0}" +
                            " OR vehicle.Team_id IS NULL) ORDER BY " + MsSqlvehicleIdent;
                    }

                    return "";
                }
            }

            public static string SelectAgroPriceT
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_pricet.Price, agro_unit.Factor FROM agro_pricet"
                            + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                            + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                            + " WHERE   agro_price.DateComand < ?DateComand AND agro_pricet.Id_work = {0}"
                            + " AND agro_pricet.Id_mobitel = {1}"
                            + " ORDER BY   agro_price.DateComand DESC";

                        case MssqlUse:
                            return "SELECT agro_pricet.Price, agro_unit.Factor FROM agro_pricet"
                            + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                            + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                            + " WHERE   agro_price.DateComand < @DateComand AND agro_pricet.Id_work = {0}"
                            + " AND agro_pricet.Id_mobitel = {1}"
                            + " ORDER BY   agro_price.DateComand DESC";
                    }

                    return "";
                }
            }

            public static string UpdateAgroOrdert
            {
                get
                {
                    return "UPDATE agro_ordert SET Price = {0}"
                    + " , agro_ordert.Id_work = {1}"
                    + " , agro_ordert.Sum = {2}"
                    + " WHERE (ID = {3})";
                }
            }

            public static string UpdateAgroOrdertSet
            {
                get
                {
                    return "UPDATE agro_ordert SET Comment = ' {0}" + "' WHERE (ID = {1})";
                }
            }

            public static string SelectAgroOrdertId
            {
                get
                {
                    return "SELECT agro_ordert.Id as IdP, agro_ordert.Id_work AS Work, agro_ordert.FactSquare as FSP, agro_ordert.Price, agro_ordert.Sum, agro_ordert.Comment"
                    + " FROM agro_ordert " + " WHERE agro_ordert.Id_main = {0}";
                }
            }

            public static string DeleteFromAgroOrderT
            {
                get
                {
                    return "DELETE FROM agro_ordert WHERE ID = ";
                }
            }

            public static string DeleteFromAgroOrdertControl
            {
                get
                {
                    return "DELETE FROM agro_ordert_control WHERE agro_ordert_control.Id = ";
                }
            }
        }
        // Order
        public class PlanController
        {
            public static string InsertIntoAgroPlan
            {
                get
                {
                    return
                        @"INSERT INTO agro_plan(PlanDate, Id_mobitel, SquarePlanned, FuelAtStart, FuelForWork, IdStatus, Id_order,
                            PlanComment, UserCreated, IsDowntime, IdDowntimeReason, DowntimeStart, DowntimeEnd) VALUES 
                            ({0}PlanDate, {0}Id_mobitel, {0}SquarePlanned, {0}FuelAtStart, {0}FuelForWork, {0}IdStatus, {0}Id_order,
                            {0}PlanComment, {0}UserCreated, {0}IsDowntime, {0}IdDowntimeReason, {0}DowntimeStart, {0}DowntimeEnd)";
                }
            }

            public static string UpdateAgroPlan
            {
                get
                {
                    return @"UPDATE agro_plan 
                    Set PlanDate = {0}PlanDate 
                    ,Id_mobitel = {0}Id_mobitel
                    ,SquarePlanned = {0}SquarePlanned
                    ,FuelAtStart = {0}FuelAtStart
                    ,FuelForWork = {0}FuelForWork
                    ,IdStatus = {0}IdStatus
                    ,Id_order = {0}Id_order
                    ,PlanComment = {0}PlanComment
                    ,UserCreated = {0}UserCreated
                    ,IsDowntime = {0}IsDowntime
                    ,IdDowntimeReason = {0}IdDowntimeReason
                    ,DowntimeStart = {0}DowntimeStart
                    ,DowntimeEnd = {0}DowntimeEnd WHERE Id = {1}";
                }
            }

            public static string DeleteFromAgroPlan
            {
                get
                {
                    return "DELETE FROM agro_plan WHERE ID = {0}";
                }
            }

            public static string DeleteFromAgroPlanT
            {
                get
                {
                    return "DELETE FROM agro_plant WHERE agro_plant.Id_main = {0} ";
                }
            }

            public static string SelectCountAgroPlan
            {
                get
                {
                    return "SELECT COUNT(agro_plan.Id) AS CNT FROM agro_plan";
                }
            }

            public static string SelectAgroPlan
            {
                get
                {
                    return @"SELECT agro_plan.* FROM agro_plan WHERE agro_plan.Id = {0}";
                }
            }

            public static string SelectAgroPlanId
            {
                get
                {
                    return @"SELECT
                            agro_plan.Id FROM agro_plan
                            WHERE agro_plan.Id_mobitel = {0} 
                            AND DAY(agro_plan.PlanDate) = {1} 
                            AND MONTH(agro_plan.PlanDate) = {2} 
                            AND YEAR(agro_plan.PlanDate) = {3} 
                            AND agro_plan.Id <> {4}";
                }
            }
        }
        // PlanController
        public class PlanSetController
        {
            public static string SelectAgroPlanId
            {
                get
                {
                    return @"SELECT
                    agro_plan.Id
                    ,agro_plan.PlanDate
                    ,{1} as VehicleIdent
                    ,driver.Family
                    ,agro_agregat.Name as AgrName
                    ,agro_work.Name as WorkName
                    ,agro_field.Name as FieldName
                    ,agro_plan.FuelForWork
                    ,agro_plan.Id_order
                    ,agro_plan.PlanComment
                    FROM agro_plant
                    RIGHT OUTER JOIN agro_plan ON agro_plant.Id_main = agro_plan.Id
                    INNER JOIN vehicle ON agro_plan.Id_mobitel = vehicle.Mobitel_id
                    LEFT OUTER JOIN driver ON agro_plant.Id_driver = driver.id
                    LEFT OUTER JOIN agro_agregat  ON agro_plant.Id_agregat = agro_agregat.Id
                    LEFT OUTER JOIN agro_work ON agro_plant.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_field ON agro_plant.Id_field = agro_field.Id
                    WHERE (agro_plan.PlanDate >= {0}TimeStart and agro_plan.PlanDate < {0}TimeEnd and agro_plan.IsDownTime = 0)
                    ORDER BY agro_plan.PlanDate";
                }
            }

            public static string SelectFromAgroPlanId
            {
                get
                {
                    return @"SELECT
                    agro_plan.Id
                    ,agro_plan.PlanDate
                    ,{1} as VehicleIdent
                    ,agro_plan_downtimereason.ReasonName
                    ,agro_plan.DowntimeStart
                    ,agro_plan.DowntimeEnd
                    FROM agro_plan
                    LEFT OUTER JOIN agro_plan_downtimereason ON agro_plan.IdDowntimeReason = agro_plan_downtimereason.Id
                    INNER JOIN vehicle ON agro_plan.Id_mobitel = vehicle.Mobitel_id
                    WHERE agro_plan.DowntimeStart <= {0}TimeEnd AND agro_plan.DowntimeEnd >= {0}TimeStart  and agro_plan.IsDownTime <> 0
                    ORDER BY agro_plan.PlanDate";
                }
            }

            public static string SqlLastDate
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_plan.PlanDate FROM agro_plan WHERE agro_plan.Id_mobitel = vehicle.Mobitel_id ORDER BY agro_plan.PlanDate DESC LIMIT 1";

                        case MssqlUse:
                            return
                                "SELECT TOP 1 agro_plan.PlanDate FROM agro_plan WHERE agro_plan.Id_mobitel = vehicle.Mobitel_id ORDER BY agro_plan.PlanDate DESC";
                    }

                    return "";
                }
            }

            public static string SelectAgroPlanIdMobitel
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_plan.Id FROM agro_plan WHERE agro_plan.Id_mobitel = vehicle.Mobitel_id ORDER BY agro_plan.PlanDate DESC LIMIT 1";

                        case MssqlUse:
                            return
                                "SELECT TOP 1 agro_plan.Id FROM agro_plan WHERE agro_plan.Id_mobitel = vehicle.Mobitel_id ORDER BY agro_plan.PlanDate DESC";
                    }

                    return "";
                }
            }

            public static string SelectDistinctFromVehicle
            {
                get
                {
                    return @"SELECT DISTINCT
                            ({2}) as Id
                            ,({3}) as LastDate
                            ,vehicle.id AS VehicleId
                            ,team.Name as VehicleGroup                 
                            ,{1} as VehicleIdent
                            FROM vehicle LEFT OUTER JOIN agro_plan ON agro_plan.Id_mobitel = vehicle.Mobitel_id
                            INNER JOIN team  ON vehicle.Team_id = team.id
                            WHERE (agro_plan.Id IS NULL) OR (agro_plan.DowntimeStart >= {0}TimeToday AND agro_plan.DowntimeEnd <= {0}TimeToday  and agro_plan.IsDownTime <> 0) OR (agro_plan.PlanDate <> {0}TimeToday  and agro_plan.IsDownTime = 0)
                            ORDER BY VehicleGroup, VehicleIdent";
                }
            }
        }
        // PlanSetController
        public class FieldsTcController
        {
            public static string SelectCountAgroFieldsTc
            {
                get
                {
                    return "SELECT COUNT(agro_fieldseason_tc.Id) AS CNT FROM agro_fieldseason_tc";
                }
            }

            public static string InsertIntoAgroFieldsTc
            {
                get
                {
                    return @"INSERT INTO agro_fieldseason_tc(DateInit,DateStart,DateEnd, IdSeason, IdField, Comment, UserCreated) VALUES 
                            ({0}DateInit,{0}DateStart,{0}DateEnd, {0}IdSeason, {0}IdField, {0}Comment, {0}UserCreated)";
                    ;
                }
            }

            public static string UpdateAgroFieldsTc
            {
                get
                {
                    return @"UPDATE agro_fieldseason_tc 
                    Set DateInit = {0}DateInit 
                    ,IdSeason = {0}IdSeason
                    ,IdField = {0}IdField
                    ,Comment = {0}Comment
                    ,UserCreated = {0}UserCreated
                    ,DateStart = {0}DateStart
                    ,DateEnd = {0}DateEnd
                    WHERE Id = {1}";
                }
            }

            public static string DeleteFromAgroFieldsTc
            {
                get
                {
                    return "DELETE FROM agro_fieldseason_tc WHERE ID = {0}";
                }
            }

            public static string DeleteFromAgroFieldsTcT
            {
                get
                {
                    return "DELETE FROM agro_fieldseason_tct  WHERE agro_fieldseason_tct.Id_main = {0} ";
                }
            }

            public static string SelectAgroFieldsTcId
            {
                get
                {
                    return @"SELECT
                            agro_fieldseason_tc.Id FROM agro_fieldseason_tc
                            WHERE agro_fieldseason_tc.IdSeason = {0} 
                            AND agro_fieldseason_tc.IdField = {1} 
                            AND agro_fieldseason_tc.Id <> {2}";
                }
            }

            public static string SelectAgroFieldsTc
            {
                get
                {
                    return @"SELECT agro_fieldseason_tc.* FROM agro_fieldseason_tc WHERE agro_fieldseason_tc.Id = {0}";
                }
            }
        }
        // FieldsTcController
        public class FieldsTcSetController
        {
            public static string SelectFieldsTcSet(string prefix, int idCulture = 0, int idFieldGroup = 0, int idField = 0)
            {
                string subQuery = @"SELECT
                    COUNT(agro_fieldseason_tct.Id) AS expr1
                    FROM agro_fieldseason_tct
                    WHERE agro_fieldseason_tct.Id_main = agro_fieldseason_tc.Id
                    GROUP BY agro_fieldseason_tct.Id_main";
                string sql = 
                    string.Format(@"SELECT
                      agro_fieldseason_tc.Id,
                      agro_fieldseason_tc.DateInit,
                      agro_culture.Name AS CultureName,
                      agro_fieldseason_tc.DateStart,
                      agro_fieldseason_tc.DateEnd,
                      agro_fieldgroupe.Name AS FieldGroupName,
                      agro_field.Name AS FieldName,
                      ({0}) as OpersQty,  
                      agro_fieldseason_tc.Comment
                    FROM agro_fieldseason_tc
                      INNER JOIN agro_season
                        ON agro_fieldseason_tc.IdSeason = agro_season.Id
                      INNER JOIN agro_culture
                        ON agro_season.Id_culture = agro_culture.Id
                      INNER JOIN agro_field
                        ON agro_fieldseason_tc.IdField = agro_field.Id
                      LEFT OUTER JOIN agro_fieldgroupe
                        ON agro_field.Id_main = agro_fieldgroupe.Id
                       WHERE (agro_fieldseason_tc.DateStart >= {1}TimeStart and agro_fieldseason_tc.DateStart < {1}TimeEnd)", subQuery, prefix);
                if (idCulture > 0)
                    sql = string.Format("{0} AND agro_culture.Id = {1}", sql, idCulture);
                if (idFieldGroup > 0)
                    sql = string.Format("{0} AND agro_fieldgroupe.Id = {1}", sql, idFieldGroup);
                if (idField > 0)
                    sql = string.Format("{0} AND agro_field.Id = {1}", sql, idField);
                return sql;
            }
        }
        // FieldsTcController
        public class GraficSensor
        {
            public static string SelectVehicleMobitel
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + MySqlvehicleIdent +
                            " AS Name FROM vehicle";

                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + MsSqlvehicleIdent +
                            " AS Name FROM vehicle";
                    }

                    return "";
                }
            }

            public static string SelectSensor
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT sensors.id,sensors.mobitel_id,team.Name as Groupe, " +
                            MySqlvehicleIdent +
                            " as CarModel,  sensors.Name,  sensors.StartBit,  sensors.Length"
                            + " FROM sensors "
                            + " INNER JOIN vehicle ON sensors.mobitel_id = vehicle.Mobitel_id"
                            + " INNER JOIN relationalgorithms ON sensors.id = relationalgorithms.SensorID "
                            + " LEFT OUTER JOIN team ON vehicle.Team_id = team.id"
                            + " WHERE relationalgorithms.AlgorithmID = {0}";

                        case MssqlUse:
                            return "SELECT sensors.id,sensors.mobitel_id,team.Name as Groupe, " +
                            MsSqlvehicleIdent +
                            " as CarModel,  sensors.Name,  sensors.StartBit,  sensors.Length"
                            + " FROM sensors "
                            + " INNER JOIN vehicle ON sensors.mobitel_id = vehicle.Mobitel_id"
                            + " INNER JOIN relationalgorithms ON sensors.id = relationalgorithms.SensorID "
                            + " LEFT OUTER JOIN team ON vehicle.Team_id = team.id"
                            + " WHERE relationalgorithms.AlgorithmID = {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_agregat.Id,  agro_agregat.Name,  agro_agregat.Identifier,  agro_agregat.Width"
                            + " FROM agro_agregat"
                            + " WHERE LENGTH(COALESCE(agro_agregat.Identifier,'')) > 0"
                            + " ORDER BY agro_agregat.Name";

                        case MssqlUse:
                            return @"SELECT agro_agregat.Id, agro_agregat.Name, agro_agregat.Identifier, 
                                    agro_agregat.Width FROM agro_agregat WHERE
                                    LEN(coalesce(agro_agregat.Identifier, '')) > 0
                                    ORDER BY agro_agregat.Name";
                    }

                    return "";
                }
            }

            public static string SelectDriverId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT driver.id as Id, driver.Family, driver.Name, driver.Identifier"
                            + " FROM driver "
                            + " WHERE LENGTH(COALESCE(driver.Identifier,'')) > 0"
                            + " ORDER BY driver.Family";

                        case MssqlUse:
                            return @"SELECT driver.id AS Id, driver.Family, driver.Name, 
                                    driver.Identifier FROM driver WHERE LEN(coalesce(driver.Identifier, '')) > 0
                                    ORDER BY driver.Family";
                    }

                    return "";
                }
            }
        }
        // GraficSensor

        public class RepDriversData
        {
            public static string SelectDId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT d.id
                                    , Concat(d.Family, ' ', d.Name) as Family
                                    , agro_order.`Date`
                                    , agro_order.FactSquareCalcOverlap
                                    , agro_order.Id as OR_ID
                                    , agro_ordert.*
                                    FROM agro_ordert 
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN driver d ON agro_ordert.Id_driver = d.id 
                                    WHERE agro_order.`Date` >= ?TimeStart
                                    AND agro_order.`Date`<= ?TimeEnd
                                    ORDER BY d.Family,d.id, agro_order.`Date`";
                        case MssqlUse:
                            return @"SELECT d.id
                                    , (d.Family + ' ' + d.Name) as Family
                                    , agro_order.Date
                                    , agro_order.FactSquareCalcOverlap
                                    , agro_order.Id as OR_ID
                                    , agro_ordert.*
                                    FROM agro_ordert 
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN driver d ON agro_ordert.Id_driver = d.id 
                                    WHERE agro_order.Date >= @TimeStart
                                    AND agro_order.Date<= @TimeEnd 
                                    ORDER BY d.Family,d.id, agro_order.Date";
                    }

                    return "";
                }
            }

            public static string SelectAgro_OrderT
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_ordert.Id_driver
                                    , agro_order.`Date`
                                    , agro_order.FactSquareCalcOverlap
                                    , agro_order.Id as OR_ID
                                    , Concat(driver.Family, ' ', driver.Name) as Family
                                    , agro_ordert.*
                                    FROM agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN driver ON agro_ordert.Id_driver = driver.id 
                                    WHERE (agro_order.`Date` >= ?TimeStart
                                    AND agro_order.`Date`<= ?TimeEnd )
                                    AND agro_ordert.Id_driver >0
                                    ORDER BY agro_ordert.Id_driver,agro_order.Id ,  agro_order.`Date`";

                        case MssqlUse:
                            return @"SELECT agro_ordert.Id_driver
                                    , agro_order.Date
                                    , agro_order.FactSquareCalcOverlap
                                    , agro_order.Id as OR_ID
                                    , (driver.Family + ' ' + driver.Name) as Family
                                    , agro_ordert.*
                                    FROM agro_ordert 
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN driver ON agro_ordert.Id_driver = driver.id 
                                    WHERE (agro_order.Date >= @TimeStart
                                    AND agro_order.Date<= @TimeEnd )
                                    AND agro_ordert.Id_driver >0
                                    ORDER BY  agro_ordert.Id_driver,agro_order.Id , agro_order.Date";
                    }

                    return "";
                }
            }

            public static string SelectDatagps
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT datagps.DataGps_ID FROM  datagps "
                            + " WHERE  Date_Format(FROM_UNIXTIME(datagps.UnixTime), '%d-%m-%Y') = '"
                            + "{0}" + "-" + "{1}" + "-"
                            + "{2}" + "'  Limit 1";

                        case MssqlUse:
                            return "SELECT TOP 1 datagps.DataGps_ID FROM datagps WHERE CONVERT(VARCHAR(9), dbo.FROM_UNIXTIME(datagps.UnixTime), 5) = '"
                            + "{0}" + "-" + "{1}" + "-" + "{2}" + "'";
                    }

                    return "";
                }
            }
        }
        // RepDriversData
        public class RepDriversAgregatsWorksData
        {
            public static string SelectDriverRecords
            {
                get
                {
                    return @"SELECT  {0} as VehicleName
                    , vehicle.OutLinkId as VehicleOutLinkId
                    , agro_work.Name as WorkName
                    , agro_work.OutLinkId as WorkOutLinkId
                    , agro_work.TypeWork
                    , agro_agregat.Name as AgregatName
                    , agro_agregat.OutLinkId as AgregatOutLinkId
                    ,{2} as FieldName
                    , agro_ordert.Id_field as FieldId
                    , zones.OutLinkId as ZoneOutLinkId
                    , agro_ordert.TimeStart
                    , agro_ordert.TimeEnd
                    , agro_ordert.Distance
                    , agro_ordert.FactSquare
                    , agro_ordert.FactSquareCalc
                    , agro_ordert.FactSquareCalc as FactSquareClarified
                    , agro_ordert.FuelExpens
                    , agro_ordert.Fuel_ExpensTotal
                    , agro_ordert.FuelExpensAvg
                    , agro_ordert.Fuel_ExpensAvg
                    , agro_ordert.SpeedAvg
                    ,0 as Pricing
                    ,0 as Salary
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    LEFT OUTER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    INNER JOIN vehicle
                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                    LEFT OUTER JOIN zones
                    ON agro_ordert.Id_zone = zones.Zone_ID
                    WHERE
                    agro_order.TimeStart between {1}DateStart and {1}DateEnd AND agro_ordert.Id_driver = {3}
                    ORDER BY agro_order.Date, agro_ordert.TimeStart";
                }
            }
        }

        public class RepDriverVehicleCategoryData
        {
            public static string SelectDriverRecords
            {
                get
                {
                    return @"SELECT vehicle_category.Name as CategoryName
                    , vehicle_category2.Name as CategoryName2
                    , vehicle_category3.Name as CategoryName3
                    , vehicle_category4.Name as CategoryName4
                    , {3} as DriverName
                    , driver.OutLinkId as DriverOutLinkId
                    , {0} as VehicleName
                    , vehicle.OutLinkId as VehicleOutLinkId
                    , agro_work.Name as WorkName
                    , agro_work.OutLinkId as WorkOutLinkId
                    , agro_work.TypeWork
                    , agro_agregat.Name as AgregatName
                    , agro_agregat.OutLinkId as AgregatOutLinkId
                    ,{2} as FieldName
                    , agro_ordert.Id_field as FieldId
                    , zones.OutLinkId as ZoneOutLinkId
                    , agro_ordert.TimeStart
                    , agro_ordert.TimeEnd
                    , agro_ordert.Distance
                    , agro_ordert.FactSquare
                    , agro_ordert.FactSquareCalc
                    , agro_ordert.FactSquareCalc as FactSquareClarified
                    , agro_ordert.FuelExpens
                    , agro_ordert.Fuel_ExpensTotal
                    , agro_ordert.FuelExpensAvg
                    , agro_ordert.Fuel_ExpensAvg
                    , agro_ordert.SpeedAvg
                    ,0 as Pricing
                    ,0 as Salary
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    LEFT OUTER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    INNER JOIN vehicle
                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                    LEFT OUTER JOIN zones
                    ON agro_ordert.Id_zone = zones.Zone_ID
                    LEFT OUTER JOIN driver
                    ON agro_ordert.Id_driver = driver.Id
                    LEFT OUTER JOIN vehicle_category
                    ON vehicle.Category_id = vehicle_category.Id
                    LEFT OUTER JOIN vehicle_category2
                    ON vehicle.Category_id2 = vehicle_category2.Id
                    LEFT OUTER JOIN vehicle_category3
                    ON vehicle.Category_id3 = vehicle_category3.Id
                    LEFT OUTER JOIN vehicle_category4
                    ON vehicle.Category_id4 = vehicle_category4.Id
                    WHERE agro_order.TimeStart between {1}DateStart and {1}DateEnd 
                    AND (vehicle.Team_id = {4} OR {4} = 0)
                    AND (vehicle.Id = {5} OR {5} = 0)
                    AND (vehicle.Category_id = {6} OR {6} = 0)
                    AND (vehicle.Category_id2 = {7} OR {7} = 0)
                    AND (vehicle.Category_id3 = {8} OR {8} = 0)
                    AND (vehicle.Category_id4 = {9} OR {9} = 0)
                    ORDER BY agro_order.Date, agro_ordert.TimeStart";

                    //AND agro_ordert.Id_driver = {3}
                }
            }
        }
        // RepFieldsProcessingData
        public class RepFieldsProcessingData
        {
            public static string SelectDriverName
            {
                get
                {
                    return @"SELECT {2} as DriverName
                    , driver.Department
                    , {0} as VehicleName
                    , agro_agregat.Name as AgregatName
                    , agro_fieldgroupe.Name as FieldGroupName    
                    ,{3} as FieldName
                    , agro_work.Name as WorkName
                    , sum(agro_ordert.FactSquareCalc) AS TotalSquare
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    INNER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN driver
                    ON agro_ordert.Id_driver = driver.id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    INNER JOIN mobitels
                    ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                    INNER JOIN vehicle
                    ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                    INNER JOIN zones
                    ON agro_field.Id_zone = zones.Zone_ID
                    INNER JOIN agro_fieldgroupe
                    ON agro_field.Id_main = agro_fieldgroupe.Id
                    WHERE
                    agro_order.TimeStart between {1}DateStart and {1}DateEnd AND agro_ordert.Confirm = 1
                    GROUP BY
                    driver.Name
                    , driver.Department
                    , agro_field.Id
                    , driver.Family
                    , agro_fieldgroupe.Name
                    , agro_field.Name
                    , agro_agregat.Name
                    , vehicle.MakeCar
                    , vehicle.NumberPlate
                    , vehicle.CarModel
                    , agro_work.Name
                    , zones.Square
                    ORDER BY driver.Family, vehicle.NumberPlate";
                }
            }
        }
        // RepFieldsProcessingExtData
        public class RepFieldsProcessingExtData
        {
            public static string SelectDriverName
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT {2} as DriverName
                    , {0} as VehicleName
                    , agro_fieldgroupe.Name AS FieldGroupName
                    , agro_agregat.Name as AgregatName
                    , agro_field.Name as FieldName
                    , (SELECT agro_culture.Name FROM agro_season
                          INNER JOIN agro_culture
                          ON agro_season.Id_culture = agro_culture.Id
                          INNER JOIN agro_fieldseason_tc
                          ON agro_fieldseason_tc.IdSeason = agro_season.Id
                          WHERE agro_fieldseason_tc.IdField = agro_field.Id 
                         AND agro_order.TimeStart between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd
                         LIMIT 1) as CultureName
                    , ROUND(IFNULL(zones.Square, 0) * 100, 2) AS FieldSquare
                    , agro_work.Name as WorkName
                    , ROUND(SUM(HOUR(agro_ordert.FactTime) + MINUTE(agro_ordert.FactTime) / 60),2) AS WorkTime
                    , SUM(agro_ordert.FactSquareCalc) as  FactSquareCalc
                    , CASE IFNULL(zones.Square, 0) 
                    WHEN 0 THEN 0
                    ELSE 100 * ROUND(SUM(agro_ordert.FactSquareCalc) / (IFNULL(zones.Square, 0) * 100), 4) END AS PersentSquare
                    , CASE SUM(HOUR(agro_ordert.TimeMove) + MINUTE(agro_ordert.TimeMove) / 60)
                    WHEN 0 THEN 0
                    ELSE ROUND(SUM(agro_ordert.Distance) / SUM(HOUR(agro_ordert.TimeMove) + MINUTE(agro_ordert.TimeMove) / 60),2) END AS SpeedAvg
                    , CASE SUM(agro_ordert.FactSquareCalc)
                    WHEN 0 THEN 0
                    ELSE ROUND(SUM(agro_ordert.Fuel_ExpensTotal)/SUM(agro_ordert.FactSquareCalc),2) END  AS FuelExpGa
                    , CASE SUM(agro_ordert.FactSquareCalc)
                    WHEN 0 THEN 0
                    ELSE ROUND(SUM(agro_ordert.FuelExpens)/SUM(agro_ordert.FactSquareCalc),2) END  AS FuelExpGaDUT
                    , MAX(agro_ordert.Comment) as Remark
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    INNER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN driver
                    ON agro_ordert.Id_driver = driver.id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    INNER JOIN mobitels
                    ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                    INNER JOIN vehicle
                    ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                    INNER JOIN zones
                    ON agro_field.Id_zone = zones.Zone_ID
                    LEFT OUTER JOIN agro_fieldgroupe
                    ON agro_field.Id_main = agro_fieldgroupe.Id
                    WHERE
                    agro_order.TimeStart between {1}DateStart and {1}DateEnd AND agro_ordert.Confirm = 1
                    GROUP BY
                     agro_field.Id
                    , driver.Family
                    , agro_agregat.Name
                    , vehicle.MakeCar
                    , vehicle.NumberPlate
                    , vehicle.CarModel
                    , agro_work.Name
                    , agro_fieldgroupe.Name
                    , agro_field.Name
                    , zones.Square
                    ORDER BY driver.Family, vehicle.NumberPlate";

                        case MssqlUse:
                            return @"SELECT {2} as DriverName
                        , {0} as VehicleName
                        , agro_fieldgroupe.Name AS FieldGroupName
                        , agro_agregat.Name AS AgregatName
                        , agro_field.Name AS FieldName
                    , (SELECT TOP 1 RTRIM(agro_culture.Name) FROM agro_season
                          INNER JOIN agro_culture
                          ON agro_season.Id_culture = agro_culture.Id
                          INNER JOIN agro_fieldseason_tc
                          ON agro_fieldseason_tc.IdSeason = agro_season.Id
                          WHERE agro_fieldseason_tc.IdField = agro_field.Id 
                         AND agro_order.TimeStart between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd) as CultureName
                        , round(isnull(zones.Square, 0) * 100, 2) AS FieldSquare
                        , agro_work.Name AS WorkName
                        , round(sum(datepart (hh, agro_ordert.FactTime) + datepart (mi, agro_ordert.FactTime) / 60), 2) AS WorkTime
                        , sum(agro_ordert.FactSquareCalc) AS FactSquareCalc
                        , CASE isnull(zones.Square, 0)
                        WHEN 0 THEN
                        0
                        ELSE
                        100 * round(sum(agro_ordert.FactSquareCalc) / (isnull(zones.Square, 0) * 100), 4)
                        END AS PersentSquare
                        , CASE sum(datepart (hh, agro_ordert.TimeMove) + datepart (mi, agro_ordert.TimeMove) / 60)
                        WHEN 0 THEN
                        0
                        ELSE
                        round(sum(agro_ordert.Distance) / sum(datepart (hh, agro_ordert.TimeMove) + datepart (mi, agro_ordert.TimeMove) / 60), 2)
                        END AS SpeedAvg
                        , CASE sum(agro_ordert.FactSquareCalc)
                        WHEN 0 THEN
                        0
                        ELSE
                        round(sum(agro_ordert.Fuel_ExpensTotal) / sum(agro_ordert.FactSquareCalc), 2)
                        END AS FuelExpGa
                        , CASE sum(agro_ordert.FactSquareCalc)
                        WHEN 0 THEN
                        0
                        ELSE
                        round(sum(agro_ordert.FuelExpens) / sum(agro_ordert.FactSquareCalc), 2)
                        END AS FuelExpGaDUT
                        , MAX(agro_ordert.Comment) AS Remark
                        FROM
                        agro_ordert
                        INNER JOIN agro_order
                        ON agro_ordert.Id_main = agro_order.Id
                        INNER JOIN agro_field
                        ON agro_ordert.Id_field = agro_field.Id
                        LEFT OUTER JOIN driver
                        ON agro_ordert.Id_driver = driver.id
                        LEFT OUTER JOIN agro_work
                        ON agro_ordert.Id_work = agro_work.Id
                        LEFT OUTER JOIN agro_agregat
                        ON agro_ordert.Id_agregat = agro_agregat.Id
                        INNER JOIN mobitels
                        ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                        INNER JOIN vehicle
                        ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                        INNER JOIN zones
                        ON agro_field.Id_zone = zones.Zone_ID
                        LEFT OUTER JOIN agro_fieldgroupe
                        ON agro_field.Id_main = agro_fieldgroupe.Id
                        WHERE
                        agro_order.TimeStart BETWEEN {1}DateStart AND {1}DateEnd
                        AND agro_ordert.Confirm = 1
                        GROUP BY
                        agro_field.Id
                        , driver.Family
                        , driver.Name
                        , agro_agregat.Name
                        , vehicle.MakeCar
                        , vehicle.NumberPlate
                        , vehicle.CarModel
                        , agro_work.Name
                        , agro_fieldgroupe.Name
                        , agro_field.Name
                        , zones.Square
                        ,agro_order.TimeStart
                        ORDER BY
                        driver.Family
                        , vehicle.NumberPlate";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return
                        @"SELECT agro_ordert.Id FROM agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                            WHERE agro_order.TimeStart BETWEEN {0}DateStart AND {0}DateEnd AND agro_ordert.Id_zone = {1} AND agro_order.Id_mobitel = {2} ";
                }
            }
        }

        // RepFieldsProcessing4Data
        public class RepFieldsProcessing4Data
        {
            public static string SelectReportsData
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_order.Id,agro_order.Date,{2} as DriverName,driver.OutLinkId as DrvOutLinkId
                    , {0} as VehicleName
                    , (SELECT internalmobitelconfig.devIdShort
                        FROM  mobitels
                        INNER JOIN internalmobitelconfig
                        ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                        WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                        FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                        AND  mobitels.Mobitel_ID = vehicle.Mobitel_id) as DevIdShort
                    , agro_fieldgroupe.Name AS FieldGroupName
                    , agro_field.Name as FieldName
                    , zones.OutLinkId as ZonesOutLinkId
                    , agro_agregat.Name as AgregatName
                    , agro_agregat.OutLinkId as AgrOutLinkId
                    , (SELECT agro_culture.Name FROM agro_season
                          INNER JOIN agro_culture
                          ON agro_season.Id_culture = agro_culture.Id
                          INNER JOIN agro_fieldseason_tc
                          ON agro_fieldseason_tc.IdSeason = agro_season.Id
                          WHERE agro_fieldseason_tc.IdField = agro_field.Id 
                         AND agro_order.TimeStart between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd
                         LIMIT 1) as CultureName
                    , ROUND(IFNULL(zones.Square, 0) * 100, 2) AS FieldSquare
                    , agro_work.Name as WorkName
                    , agro_ordert.*
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    INNER JOIN mobitels
                    ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                    INNER JOIN vehicle
                    ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                    LEFT OUTER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN driver
                    ON agro_ordert.Id_driver = driver.id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    LEFT OUTER JOIN zones
                    ON agro_field.Id_zone = zones.Zone_ID
                    LEFT OUTER JOIN agro_fieldgroupe
                    ON agro_field.Id_main = agro_fieldgroupe.Id
                    WHERE agro_order.TimeStart between {1}DateStart and {1}DateEnd
                    ORDER BY agro_order.Date, agro_order.Id";

                        // agro_order.Id = 15530

                        case MssqlUse:
                            return @"SELECT agro_order.Id,agro_order.Date,{2} as DriverName,driver.OutLinkId as DrvOutLinkId
                    , {0} as VehicleName
                    , (SELECT internalmobitelconfig.devIdShort
                        FROM  mobitels
                        INNER JOIN internalmobitelconfig
                        ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                        WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                        FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                        AND  mobitels.Mobitel_ID = vehicle.Mobitel_id) as DevIdShort
                    , agro_fieldgroupe.Name AS FieldGroupName
                    , agro_field.Name as FieldName
                    , zones.OutLinkId as ZonesOutLinkId
                    , agro_agregat.Name as AgregatName
                    , agro_agregat.OutLinkId as AgrOutLinkId
                    , (SELECT TOP 1 agro_culture.Name FROM agro_season
                          INNER JOIN agro_culture
                          ON agro_season.Id_culture = agro_culture.Id
                          INNER JOIN agro_fieldseason_tc
                          ON agro_fieldseason_tc.IdSeason = agro_season.Id
                          WHERE agro_fieldseason_tc.IdField = agro_field.Id 
                         AND agro_order.TimeStart between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd) as CultureName
                    , ROUND(ISNULL(zones.Square, 0) * 100, 2) AS FieldSquare
                    , agro_work.Name as WorkName
                    , agro_ordert.*
                    FROM
                    agro_ordert
                    INNER JOIN agro_order
                    ON agro_ordert.Id_main = agro_order.Id
                    INNER JOIN mobitels
                    ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                    INNER JOIN vehicle
                    ON mobitels.Mobitel_ID = vehicle.Mobitel_id
                    LEFT OUTER JOIN agro_field
                    ON agro_ordert.Id_field = agro_field.Id
                    LEFT OUTER JOIN driver
                    ON agro_ordert.Id_driver = driver.id
                    LEFT OUTER JOIN agro_work
                    ON agro_ordert.Id_work = agro_work.Id
                    LEFT OUTER JOIN agro_agregat
                    ON agro_ordert.Id_agregat = agro_agregat.Id
                    LEFT OUTER JOIN zones
                    ON agro_field.Id_zone = zones.Zone_ID
                    LEFT OUTER JOIN agro_fieldgroupe
                    ON agro_field.Id_main = agro_fieldgroupe.Id
                    WHERE agro_order.TimeStart between {1}DateStart and {1}DateEnd
                    ORDER BY agro_order.Date, agro_order.Id";;
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return
                        @"SELECT agro_ordert.Id FROM agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                            WHERE agro_order.TimeStart BETWEEN {0}DateStart AND {0}DateEnd AND agro_ordert.Id_zone = {1} AND agro_order.Id_mobitel = {2} ";
                }
            }
        }

        // RepFieldsProcessing3Data
        public class RepFieldsProcessing3Data
        {
            public static string Select
            {
                get
                {
                    return @"SELECT agro_order.Date,
                                {0} AS VehicleName,
                                agro_agregat.Name AS AgregatName,
                                agro_work.Name AS WorkName,
                                {1} AS FieldName,
                                agro_field.Name AS FieldNameOrig,
                                agro_fieldgroupe.Name AS FieldGroupName,
                                agro_ordert.FactSquareCalc,
                                agro_ordert.FuelStart,  
                                agro_ordert.FuelEnd, 
                                agro_ordert.FuelExpens,
                                agro_ordert.Fuel_ExpensTotal,
                                agro_ordert.FuelAdd,
                                agro_ordert.FuelSub,
                                agro_ordert.Distance,
                                agro_order.Id_mobitel,
                                agro_ordert.Id_agregat,
                                agro_ordert.Id_work,
                                agro_ordert.Id_field,
                                agro_order.Id,
                                agro_order.SquareWorkDescripOverlap,
                                agro_work.TypeWork
                            FROM agro_ordert
                                INNER JOIN agro_order
                                ON agro_ordert.Id_main = agro_order.Id
                                INNER JOIN vehicle
                                ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                LEFT OUTER JOIN agro_agregat
                                ON agro_ordert.Id_agregat = agro_agregat.Id
                                LEFT OUTER JOIN agro_work
                                ON agro_ordert.Id_work = agro_work.Id 
                                LEFT OUTER JOIN agro_field
                                ON agro_ordert.Id_field = agro_field.Id
                                LEFT OUTER JOIN agro_fieldgroupe
                                ON agro_field.Id_main = agro_fieldgroupe.Id
                                LEFT OUTER JOIN zones
                                ON agro_field.Id_zone = zones.Zone_ID
                            WHERE agro_order.Date between {2}TimeStart and {2}TimeEnd 
                            ORDER BY agro_order.Date,agro_order.Id,{0},agro_ordert.Id_field,agro_ordert.TimeStart";
                    //and (agro_order.Id = 18780 OR agro_order.Id = 20092)
                }
            }
        }

        // RepFieldsProcessingExtData
        public class RepVehiclesAgregatsData
        {
            public static string SelectAgro_Ordert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_ordert.*
                                    , agro_agregat.Id as AgregatId
                                    , agro_agregat.Name as AgregatName
                                    , {0} as VehicleName
                                    , vehicle.id  as vehicleId
                                    FROM agro_ordert
                                    INNER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE agro_order.`Date` between ?TimeStart and ?TimeEnd
                                    ORDER BY {0}, vehicle.id, agro_agregat.Name, agro_ordert.TimeStart";

                        case MssqlUse:
                            return @"SELECT agro_ordert.*
                                    , agro_agregat.Id as AgregatId
                                    , agro_agregat.Name as AgregatName
                                    , {0} as VehicleName
                                    , vehicle.id  as vehicleId
                                    FROM agro_ordert
                                    INNER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                    INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                                    INNER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                    WHERE agro_order.Date between @TimeStart and @TimeEnd
                                    ORDER BY {0}, vehicle.id, agro_agregat.Name, agro_ordert.TimeStart";
                    }

                    return "";
                }
            }
        }

        // RepFieldsProcessingExtData
        public class RepVehiclesAgregatWorkssData
        {
            public static string SelectAgroWork_Ordert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT {0} AS VehicleName,
                                      agro_agregat.Name AS AgregatName,
                                      agro_work.Name AS WorkName,
                                      agro_ordert.FactSquareCalc,
                                      agro_ordert.FuelExpens,
                                      agro_ordert.FuelExpensAvg,
                                      agro_ordert.Fuel_ExpensAvg,
                                      agro_ordert.Fuel_ExpensTotal,
                                      agro_ordert.FuelAdd,
                                      agro_ordert.FuelSub,
                                      agro_ordert.Distance,
                                      agro_ordert.TimeStart,
                                      agro_ordert.TimeEnd,
                                      agro_order.Id_mobitel,
                                      agro_ordert.Id_agregat,
                                      agro_ordert.Id_work,
                                      agro_order.Id
                                    FROM agro_ordert
                                      INNER JOIN agro_agregat
                                        ON agro_ordert.Id_agregat = agro_agregat.Id
                                      INNER JOIN agro_order
                                        ON agro_ordert.Id_main = agro_order.Id
                                      INNER JOIN vehicle
                                        ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                      INNER JOIN agro_work
                                        ON agro_work.Id = agro_ordert.Id_work
                                    WHERE agro_order.`Date` between ?TimeStart and ?TimeEnd  AND agro_work.TypeWork = 1
                                    ORDER BY {0},agro_order.Id_mobitel,  agro_agregat.Name
                                    ,agro_ordert.Id_agregat,WorkName,agro_ordert.Id_work, agro_ordert.TimeStart";

                        case MssqlUse:
                            return @"SELECT {0} AS VehicleName,
                                      agro_agregat.Name AS AgregatName,
                                      agro_work.Name AS WorkName,
                                      agro_ordert.FactSquareCalc,
                                      agro_ordert.FuelExpens,
                                      agro_ordert.FuelExpensAvg,
                                      agro_ordert.Fuel_ExpensAvg,
                                      agro_ordert.Fuel_ExpensTotal,
                                      agro_ordert.FuelAdd,
                                      agro_ordert.FuelSub,
                                      agro_ordert.Distance,
                                      agro_ordert.TimeStart,
                                      agro_ordert.TimeEnd,
                                      agro_order.Id_mobitel,
                                      agro_ordert.Id_agregat,
                                      agro_ordert.Id_work,
                                      agro_order.Id  
                                    FROM agro_ordert
                                      INNER JOIN agro_agregat
                                        ON agro_ordert.Id_agregat = agro_agregat.Id
                                      INNER JOIN agro_order
                                        ON agro_ordert.Id_main = agro_order.Id
                                      INNER JOIN vehicle
                                        ON vehicle.Mobitel_id = agro_order.Id_mobitel
                                      INNER JOIN agro_work
                                        ON agro_work.Id = agro_ordert.Id_work
                                    WHERE agro_order.Date between @TimeStart and @TimeEnd  AND agro_work.TypeWork = 1
                                    ORDER BY {0},agro_order.Id_mobitel,  agro_agregat.Name 
                                    ,agro_ordert.Id_agregat,WorkName,agro_ordert.Id_work, agro_ordert.TimeStart";
                    }

                    return "";
                }
            }
        }

        public class RepSpeedControlData
        {
            public static string SelectOrdersInterval(int groupId,int vehicleId)
            {
 {

                    string sql = @"SELECT agro_ordert.Id
                            ,agro_ordert.Distance
                            , agro_ordert.TimeMove
                            , agro_ordert.TimeStop
                            , agro_field.Id as FieldId
                            , agro_field.Name as FieldName
                            , {0} as VehicleName
                            , vehicle.id  as VehicleId
                            FROM agro_ordert
                            INNER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                            INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                            INNER JOIN vehicle ON vehicle.Mobitel_id = agro_order.Id_mobitel
                            WHERE agro_order.Date between {1}TimeStart and {1}TimeEnd";
                    if (groupId > 0) sql += string.Format(" AND vehicle.Team_id = {0}", groupId);
                    if (vehicleId > 0) sql += string.Format(" AND vehicle.id = {0}", vehicleId);
                    sql += " ORDER BY {0}, agro_field.Name";
                    return sql;
                }
            }
        }
        // RepFieldsJointProcessingData
        public class RepFieldsJointProcessingData
        {
            public static string SelectFromOrders(int group, int field)
            {

                string sql = @"SELECT
                        agro_ordert.Id 
                        ,agro_ordert.Id_main
                        ,agro_order.Date
                        ,agro_ordert.Id_field as FieldId
                        ,{3} as FieldName
                        ,agro_ordert.FactSquareCalc
                        ,{0} as VehicleName
                        ,{2} as DriverName
                        ,agro_work.Id as WorkId
                        ,agro_work.Name as WorkName
                        ,agro_agregat.Name as AgrName
                        ,agro_ordert.Id_zone
                        ,agro_fieldgroupe.Name as FieldGroupName
                        ,agro_ordert.Id_driver as DriverId
                    FROM agro_ordert
                        INNER JOIN agro_work
                        ON agro_ordert.Id_work = agro_work.Id
                        INNER JOIN agro_order
                        ON agro_ordert.Id_main = agro_order.Id
                        INNER JOIN vehicle
                        ON agro_order.Id_mobitel = vehicle.Mobitel_id
                        INNER JOIN agro_field
                        ON agro_ordert.Id_field = agro_field.Id
                        LEFT OUTER JOIN agro_agregat
                        ON agro_ordert.Id_agregat = agro_agregat.Id
                        LEFT OUTER JOIN driver
                        ON agro_ordert.Id_driver = driver.id
                        INNER JOIN zones
                        ON agro_ordert.Id_zone = zones.Zone_ID
                        LEFT OUTER JOIN agro_fieldgroupe
                        ON agro_field.Id_main = agro_fieldgroupe.Id
                    WHERE agro_ordert.PointsValidity = 100 AND agro_ordert.Confirm = 1 AND agro_ordert.TimeStart BETWEEN {1}DS AND {1}DE";
                    if (group > 0) sql += string.Format(" AND agro_fieldgroupe.Id = {0}", group);
                    if (field > 0) sql += string.Format(" AND agro_field.Id = {0}", field);
                    sql += " ORDER BY agro_field.Name,agro_ordert.Id_field,agro_work.Name, agro_work.Id,{2},agro_ordert.Id_driver,agro_ordert.Id_main";
                return sql;
            }
            public static string GetFieldsCount(int group, int field)
            {

                string sql = @"SELECT        COUNT(DISTINCT Id_field) AS CNT
                            FROM            agro_ordert INNER JOIN
                            agro_field ON agro_ordert.Id_field = agro_field.Id
                            WHERE        TimeStart BETWEEN {0}DS AND {0}DE";
                if (group > 0) sql += string.Format(" AND agro_field.Id_main = {0}", group);
                            if (field > 0) sql += string.Format(" AND agro_field.Id = {0}", field);
                return sql;
            }

        }
        // RepVehiclesAgregatsData
        public class MapUtilites
        {
            public static string SelectPoints
            {
                get
                {
                    return "SELECT points.Latitude as Lat, points.Longitude as Lon"
                    + " FROM points WHERE points.Zone_ID = {0}" + " ORDER BY points.Point_ID";
                }
            }
        }
        // MapUtilites
        public class RepSensor
        {
            public static string SelectDatagps
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT datagps.DataGps_ID, datagps.Mobitel_ID, datagps.Speed, datagps.Sensor1, datagps.Sensor2, datagps.Sensor3, datagps.Sensor4, datagps.Sensor5, datagps.Sensor6,"
                            + " datagps.Sensor7, datagps.Sensor8, from_unixtime(datagps.UnixTime) as FromUT"
                            + " FROM datagps WHERE datagps.UnixTime BETWEEN ?TimeStart AND ?TimeEnd";

                        case MssqlUse:
                            return "SELECT datagps.DataGps_ID,datagps.Mobitel_ID,datagps.Speed,datagps.Sensor1, datagps.Sensor2, datagps.Sensor3, datagps.Sensor4, datagps.Sensor5, datagps.Sensor6,"
                            + " datagps.Sensor7, datagps.Sensor8,dbo.FROM_UNIXTIME(datagps.UnixTime) as FromUT"
                            + " FROM  datagps WHERE datagps.UnixTime BETWEEN @TimeStart AND @TimeEnd";
                    }

                    return "";
                }
            }

            public static string AndDatagpsValid
            {
                get
                {
                    return " AND datagps.Valid = 1 ORDER BY datagps.Mobitel_ID, datagps.UnixTime";
                }
            }
        }
        // RepSensor
        public class RepSensorAgr
        {
            public static string SelectSensors
            {
                get
                {
                    return @"SELECT sensors.mobitel_id, sensors.StartBit, sensors.Length 
                            FROM relationalgorithms 
                            INNER JOIN sensors ON relationalgorithms.SensorID = sensors.id 
                            WHERE relationalgorithms.AlgorithmID = {0}";
                }
            }
        }
        // RepSensorAgr
        public class DicUtilites
        {
            public static string SelectSensors
            {
                get
                {
                    return "SELECT COUNT(Id) FROM  " + "{0}"
                    + " WHERE " + "{1}" + " = " + "{2}" + " AND Id <> " + "{3}";
                }
            }

            public static string SelectDriverId
            {
                get
                {
                    return "SELECT driver.id"
                    + " FROM vehicle INNER JOIN driver ON vehicle.driver_id = driver.id "
                    + " WHERE vehicle.Mobitel_id = ";
                }
            }

            public static string SelectFromDriver
            {
                get
                {
                    return "SELECT driver.id, driver.Family FROM driver WHERE driver.Identifier = ";
                }
            }

            public static string SelectDriverIdentifier
            {
                get
                {
                    return "SELECT driver.Identifier FROM driver WHERE driver.Id = ";
                }
            }

            public static string SelectPointsLatLon
            {
                get
                {
                    return "SELECT points.Latitude as Lat, points.Longitude as Lon"
                    + " FROM points WHERE points.Zone_ID = {0}"
                    + " ORDER BY points.Point_ID";
                }
            }

            public static string SelectZones
            {
                get
                {
                    return "SELECT zones.Square FROM  zones "
                    + " WHERE zones.Zone_ID = ";
                }
            }

            public static string SelectFromVehicle
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT " + MySqlvehicleIdent + " AS Name"
                            + " FROM vehicle "
                            + " WHERE vehicle.Mobitel_id = ";

                        case MssqlUse:
                            return "SELECT " + MsSqlvehicleIdent + " AS Name"
                            + " FROM vehicle "
                            + " WHERE vehicle.Mobitel_id = ";
                    }

                    return "";
                }
            }
        }
        // DicUtilites
        public class Params
        {
            public static string SelectAgroParams
            {
                get
                {
                    return "SELECT  agro_params.Value FROM  agro_params "
                    + " WHERE agro_params.Number = ";
                }
            }

            public static string InsertIntoAgroParams
            {
                get
                {
                    return "INSERT INTO agro_params (Number,Value) VALUES  (" + "{0}" + ",'" + "" + "')";
                }
            }

            public static string SelectAgroParamsValue
            {
                get
                {
                    return "SELECT  agro_params.Value FROM  agro_params "
                    + " WHERE agro_params.Number = ";
                }
            }

            public static string InsertIntoAgroParamsValue
            {
                get
                {
                    return "INSERT INTO agro_params (Number,Value) VALUES  (" + "{0}" + ",'" + "{1}" + "')";
                }
            }

            public static string SelectFromAgroParams
            {
                get
                {
                    return "SELECT  agro_params.Value FROM  agro_params "
                    + " WHERE agro_params.Number = ";
                }
            }

            public static string InsertIntoAgroParamsNumber
            {
                get
                {
                    return "INSERT INTO agro_params (Number,Value) VALUES  ('" + "{0}" + "','" + "{1}" + "')";
                }
            }

            public static string UpdateAgroParams
            {
                get
                {
                    return "UPDATE agro_params Set Value = '" + "{0}" + "' WHERE Number = " + "{1}";
                }
            }
        }
        // Params
        public class RepSensorDrv
        {
            public static string SelectSensorMobId
            {
                get
                {
                    return "SELECT sensors.mobitel_id, sensors.StartBit, sensors.Length "
                    + " FROM relationalgorithms "
                    + " INNER JOIN  sensors ON relationalgorithms.SensorID = sensors.id "
                    + " WHERE relationalgorithms.AlgorithmID = ";
                }
            }
        }
        // RepSensorDrv
        public class PlanTController
        {
            public static string SelectAgroPlant
            {
                get
                {
                    return "SELECT * FROM agro_plant WHERE agro_plant.Id_main = {0}";
                }
            }

            public static string DeleteFromAgroPlant
            {
                get
                {
                    return "DELETE FROM agro_plant WHERE agro_plant.Id = {0}";
                }
            }

            public static string UpdateAgroPlant
            {
                get
                {
                    return @"UPDATE agro_plant
                    Set Id_main = {0}Id_main 
                    ,Id_field = {0}Id_field 
                    ,Id_driver = {0}Id_driver
                    ,Id_work = {0}Id_work 
                    ,Id_agregat = {0}Id_agregat
                    ,Remark = {0}Remark
                    ,TimeStart = {0}TimeStart WHERE Id = {1}";
                }
            }

            public static string InsertIntoAgroPlant
            {
                get
                {
                    return @"INSERT INTO agro_plant(
                    Id_main
                    ,Id_field
                    ,Id_driver
                    ,Id_work
                    ,Id_agregat
                    ,Remark
                    ,TimeStart)
                    VALUES ( 
                    {0}Id_main 
                    ,{0}Id_field 
                    ,{0}Id_driver
                    ,{0}Id_work 
                    ,{0}Id_agregat
                    ,{0}Remark
                    ,{0}TimeStart)";
                }
            }
        }
        // PlanTController
        public class OrderItemControl
        {
            public static string SelectAgroOrdertControl
            {
                get
                {
                    return @"SELECT * FROM agro_ordert_control
                            WHERE agro_ordert_control.Id_main = {0}
                            AND agro_ordert_control.Type_object = {1}
                            AND (agro_ordert_control.TimeStart <= {2}TimeStart and agro_ordert_control.TimeEnd >= {2}TimeEnd)";
                }
            }

            public static string InsertIntoAgroOrdertControl
            {
                get
                {
                    return "INSERT INTO agro_ordert_control (Id_main ,Id_object, Type_object,TimeStart,TimeEnd )"
                    + " VALUES ({0},{1},{2},{3}TimeStart,{3}TimeEnd)";
                }
            }

            public static string UpdateAgroOrdertControl
            {
                get
                {
                    return "UPDATE agro_ordert_control SET Id_object = {0}, "
                    + "TimeStart = {2}TimeStart , TimeEnd = {2}TimeEnd WHERE ID = {1}";
                }
            }
        }
        // OrderItemControl
        public class OrderItemRecord
        {
            public static string SelectAgroOrdert
            {
                get
                {
                    return "SELECT agro_ordert.*"
                    + " FROM agro_ordert  "
                    + " WHERE agro_ordert.Id = {0} ";
                }
            }

            public static string UpdateAgroOrdert
            {
                get
                {
                    return @"UPDATE agro_ordert SET FuelAdd = {1}FuelAdd
                            , FuelSub = {1}FuelSub
                            , agro_ordert.FuelExpens = {1}FuelExpens
                            , agro_ordert.FuelExpensAvg = {1}FuelExpensAvg
                            , agro_ordert.FuelExpensAvgRate = {1}FuelExpensAvgRate
                            WHERE ID = {0}";
                }
            }

            public static string UpdateAgroOrdertLockRecord
            {
                get
                {
                    return @"UPDATE agro_ordert SET LockRecord = {0}
                        WHERE (ID = {1})";
                }
            }

            public static string SelectIdAgroOrdert
            {
                get
                {
                    return @"SELECT Id From agro_ordert  WHERE Id_main = {0} 
                    AND TimeStart <= {1}DateCompare AND TimeEnd >= {1}DateCompare";
                }
            }

            public static string InsertIntoAgroOrdert{
                get
                {
                    return "INSERT INTO agro_ordert ( Id_main,Id_field,Id_Zone,Id_driver, Id_agregat ,FactTime, TimeStart, TimeEnd, Confirm, Id_work, AgrInputSource, DrvInputSource,Id_event_manually)"
                    + " VALUES ({0},{1},{2},{3},{4},{7}FactTime,{7}TimeStart,{7}TimeEnd,{5},{6},{8},{9},{10})";
                }
            }

            public static string UpdateAgroOrderT
            {
                get
                {
                    return @"UPDATE agro_orderT SET TimeRate = {1}TimeRate
                    , TimeStop = {1}TimeStop
                     , TimeMove = {1}TimeMove
                     , Distance = {1}Distance
                     , FactSquare = {1}FactSquare
                     , FactSquareCalc = {1}FactSquareCalc
                     , Sum = {1}Sum
                     , SpeedAvg = {1}SpeedAvg
                     , FuelStart = {1}FuelStart
                     , FuelAdd = {1}FuelAdd
                     , FuelSub = {1}FuelSub
                     , FuelEnd = {1}FuelEnd
                     , FuelExpens = {1}FuelExpens
                     , FuelExpensAvg = {1}FuelExpensAvg
                     , FuelExpensAvgRate = {1}FuelExpensAvgRate
                     , Fuel_ExpensMove = {1}Fuel_ExpensMove
                     , Fuel_ExpensStop = {1}Fuel_ExpensStop
                     , Fuel_ExpensTotal = {1}Fuel_ExpensTotal
                     , Fuel_ExpensAvg = {1}Fuel_ExpensAvg
                     , Fuel_ExpensAvgRate = {1}Fuel_ExpensAvgRate
                     , PointsValidity = {1}PointsValidity
                     , Id_main = {1}Id_main
                      WHERE (ID = {0})";
                }
            }

            public static string DeleteFromAgroOrdert
            {
                get
                {
                    return "DELETE FROM agro_ordert WHERE agro_ordert.Id = {0} ";
                }
            }

            public static string UpdateAgroOrderTTimeStart
            {
                get
                {
                    return @"UPDATE agro_orderT SET TimeStart = {1}TimeStart WHERE (ID = {0})";
                }
            }

            public static string UpdateFactSquareJointProcessing
            {
                get
                {
                    return @"UPDATE agro_orderT SET FactSquareJointProcessing = {1}FactSquareJointProcessing WHERE (ID = {0})";
                }
            }

            public static string UpdateAgroOrderTTimeEnd
            {
                get
                {
                    return @"UPDATE agro_orderT SET TimeEnd = {1}TimeEnd WHERE (ID = {0})";
                }
            }

            public static string UpdateAgroOrderTIdField
            {
                get
                {
                    return @"UPDATE agro_orderT SET Id_field = {0},Id_zone = {1},Id_driver = {2}, Id_agregat = {3} ,Id_work = {4}
                      WHERE (ID = {5})";
                }
            }
        }
        // OrderItemRecord
        public class TrackZonesFinder
        {
            public static string SelectDistinctZones
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT  DISTINCT zones.Zone_ID, agro_field.Id,zones.Name FROM agro_field 
                            INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID WHERE IFNULL(agro_field.IsOutOfDate,0) = 0";

                        case MssqlUse:
                            return @"SELECT  DISTINCT zones.Zone_ID, agro_field.Id,zones.Name FROM agro_field 
                            INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID WHERE ISNULL(agro_field.IsOutOfDate,0) = 0";
                    }
                    return "";
                }
            }
        }

        public class FieldTechOpers
        {
            public static string SelectContentTechCart
            {
                get
                {
                    return "SELECT * FROM agro_fieldseason_tct WHERE agro_fieldseason_tct.Id_main = {0}";
                }
            }

            public static string Delete
            {
                get
                {
                    return "DELETE FROM agro_fieldseason_tct WHERE agro_fieldseason_tct.Id = {0}";
                    ;
                }
            }

            public static string Insert
            {
                get
                {
                    return @"INSERT INTO agro_fieldseason_tct(
                    Id_main
                    ,IdWork
                    ,DateStartPlan
                    ,DateEndPlan
                    ,WorkQtyPlan
                    ,DateStartFact
                    ,DateEndFact
                    ,SquareGa
                    ,SquarePercent
                    ,IsClosed
                    ,Comment
                    )
                    VALUES ( 
                    {0}Id_main 
                    ,{0}IdWork 
                    ,{0}DateStartPlan
                    ,{0}DateEndPlan 
                    ,{0}WorkQtyPlan
                    ,{0}DateStartFact
                    ,{0}DateEndFact
                    ,{0}SquareGa
                    ,{0}SquarePercent
                    ,{0}IsClosed
                    ,{0}Comment
                      )";

                }
            }

            public static string Update
            {
                get
                {
                    return @"UPDATE agro_fieldseason_tct
                    Set Id_main = {0}Id_main 
                    ,IdWork = {0}IdWork 
                    ,DateStartPlan = {0}DateStartPlan
                    ,DateEndPlan = {0}DateEndPlan 
                    ,WorkQtyPlan = {0}WorkQtyPlan
                    ,DateStartFact = {0}DateStartFact
                    ,DateEndFact = {0}DateEndFact 
                    ,SquareGa = {0}SquareGa
                    ,SquarePercent = {0}SquarePercent
                    ,Comment = {0}Comment 
                    ,IsClosed = {0}IsClosed 
                    WHERE Id = {1}";
                }
            }

            public static string SelectLinkedGroupedOrderTRecords
            {
                get
                {
                    return @"SELECT
                            agro_order.Id,
                            agro_ordert.Id_agregat,
                            agro_ordert.Id_driver,
                            SUM(agro_ordert.FactSquareCalc) AS SQ,
                            MIN(agro_ordert.TimeStart) AS DateStart,
                            MAX(agro_ordert.TimeEnd) AS DateEnd
                            FROM agro_ordert
                            INNER JOIN agro_order
                            ON agro_ordert.Id_main = agro_order.Id
                            WHERE agro_ordert.Id_field = {0}Id_field AND agro_ordert.Confirm = 1
                            AND agro_order.Date between {0}DateStart AND {0}DateEnd AND agro_ordert.Id_work = {0}Id_work 
                            GROUP BY agro_order.Id,
                            agro_ordert.Id_driver,
                            agro_ordert.Id_agregat";
                }
            }

            public static string SelectLinkedOrderTRecords
            {
                get
                {
                    return @"SELECT
                            agro_ordert.Id
                            FROM agro_ordert
                            WHERE agro_ordert.Id_main = {0}Id_main AND agro_ordert.Confirm = 1
                            AND agro_ordert.Id_field = {0}Id_field AND agro_ordert.Id_work = {0}Id_work";
                }
            }
        }
        //FieldTechOpers
        public class FieldTechOpersFact
        {
            public static string InsertOrderTGroupLink
            {
                get
                {
                    return @"INSERT INTO agro_fieldseason_tct_fact(Id_main,IdOrder,IdDriver,IdAgreg,SquareFactGa) 
                    VALUES ( {0}Id_main,{0}IdOrder,{0}IdDriver,{0}IdAgreg,{0}SquareFactGa)";

                }
            }

            public static string SelectFactRecords
            {
                get
                {
                    return @"SELECT
                      agro_fieldseason_tct_fact.Id as RecordId,
                      agro_order.Id as OrderId,
                      agro_order.Date,
                      vehicle.NumberPlate,
                      agro_agregat.Name as AgrName,
                      driver.Family,
                      agro_fieldseason_tct_fact.SquareFactGa,
                      agro_fieldseason_tct_fact.JointProcess,
                      agro_fieldseason_tct_fact.SquareAfterRecalc,
                      agro_fieldseason_tct_fact.DateRecalc,
                      agro_fieldseason_tct_fact.UserRecalc
                    FROM agro_fieldseason_tct_fact
                      INNER JOIN agro_order
                        ON agro_fieldseason_tct_fact.IdOrder = agro_order.Id
                      INNER JOIN vehicle
                        ON vehicle.Mobitel_id = agro_order.Id_mobitel
                      LEFT OUTER JOIN agro_agregat
                        ON agro_fieldseason_tct_fact.IdAgreg = agro_agregat.Id
                      LEFT OUTER JOIN driver
                        ON agro_fieldseason_tct_fact.IdDriver = driver.id
                    WHERE agro_fieldseason_tct_fact.Id_main = {0}";
                }
            }

            public static string DeleteLinkedOrderTRecords
            {
                get
                {
                    return @"DELETE FROM agro_fieldseason_tct_fact WHERE agro_fieldseason_tct_fact.Id_main ={0}";
                }
            }

            public static string Update
            {
                get
                {
                    return @"UPDATE agro_fieldseason_tct_fact
                    Set JointProcess = {0}JointProcess 
                    ,SquareAfterRecalc = {0}SquareAfterRecalc 
                    ,DateRecalc = {0}DateRecalc
                    ,UserRecalc = {0}UserRecalc 
                    WHERE Id = {1}";
                }
            }
        }
        //FieldTechOpersFact

        public class OrderReports
        {
            public static string GetFieldProcAreaGaDate
            {
                get
                {
                    return @"SELECT  MAX(agro_ordert.Id) AS MaxOrderTid, SUM(agro_ordert.FactSquareCalc) AS AreaSum
                FROM  agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                WHERE   (agro_ordert.TimeStart >= {0}DB) AND (agro_ordert.TimeStart <= {0}DE) AND (Id_field = {1}) AND (agro_order.Id_mobitel = {2})";
                }
            }

            public static string GetFieldProcAreaGaId
            {
                get
                {
                    return @"SELECT  MAX(agro_ordert.Id) AS MaxOrderTid, SUM(FactSquareCalc) AS AreaSum
                FROM  agro_ordert INNER JOIN agro_order ON agro_ordert.Id_main = agro_order.Id
                WHERE   (agro_ordert.Id > {1}) AND (agro_ordert.TimeEnd <= {0}DE) AND (Id_field = {2}) AND (agro_order.Id_mobitel = {3})";
                }
            }
        }//OrderReports
    }
    // AgroQuery
}
// Agro
