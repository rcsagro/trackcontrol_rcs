namespace Agro
{
    partial class PlanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanForm));
            this.pgPlan = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.rleTeam = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleVehicle = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleStates = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleDtReasons = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rdeDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rceDt = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.crTotal = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erData = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erStatus = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTeam = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erVehicle = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erUser = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crTask = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erSquare = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crFuel = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erFuelAtStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erFuelForWork = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crDownTime = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erDt = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDtReason = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDtStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDtEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcPlan = new DevExpress.XtraGrid.GridControl();
            this.bgvPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gbPlan = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDriver = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleDriver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colField = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleField = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAgregat = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colWorkType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleWorkType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colRemark = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStartPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rteTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gbFact = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimeStartFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeEndFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactSquareCalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bsPlan = new System.Windows.Forms.BindingSource(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFact = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLog = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pgOrder = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.crOrdTotal = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erOrdId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdTimeStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdTimeEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdDistance = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdSpeedAvg = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdCreator = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crOrdExecute = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erOrdSquare = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crOrdFuel = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erOrdFuelDUTStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdFuelDUTAdd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdFuelDUTSub = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdFuelDUTEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOrdFuelDUTExpense = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.pgPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDtReasons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rceDt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWorkType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // pgPlan
            // 
            this.pgPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgPlan.Location = new System.Drawing.Point(0, 0);
            this.pgPlan.Name = "pgPlan";
            this.pgPlan.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgPlan.RecordWidth = 96;
            this.pgPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleTeam,
            this.rleVehicle,
            this.rleStates,
            this.rleDtReasons,
            this.rdeDate,
            this.rceDt});
            this.pgPlan.RowHeaderWidth = 104;
            this.pgPlan.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crTotal,
            this.crTask,
            this.crFuel,
            this.crDownTime});
            this.pgPlan.Size = new System.Drawing.Size(507, 330);
            this.pgPlan.TabIndex = 6;
            this.pgPlan.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgPlan_CellValueChanged);
            // 
            // rleTeam
            // 
            this.rleTeam.AutoHeight = false;
            this.rleTeam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTeam.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleTeam.DisplayMember = "Name";
            this.rleTeam.Name = "rleTeam";
            this.rleTeam.NullText = "";
            // 
            // rleVehicle
            // 
            this.rleVehicle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehicle.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "��������")});
            this.rleVehicle.DisplayMember = "Info";
            this.rleVehicle.DropDownRows = 20;
            this.rleVehicle.Name = "rleVehicle";
            this.rleVehicle.NullText = "";
            // 
            // rleStates
            // 
            this.rleStates.AutoHeight = false;
            this.rleStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleStates.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StatusName", "��������")});
            this.rleStates.DisplayMember = "StatusName";
            this.rleStates.Name = "rleStates";
            this.rleStates.NullText = "";
            this.rleStates.ValueMember = "Id";
            // 
            // rleDtReasons
            // 
            this.rleDtReasons.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleDtReasons.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ReasonName", "��������")});
            this.rleDtReasons.DisplayMember = "ReasonName";
            this.rleDtReasons.Name = "rleDtReasons";
            this.rleDtReasons.NullText = "";
            this.rleDtReasons.ValueMember = "Id";
            // 
            // rdeDate
            // 
            this.rdeDate.AutoHeight = false;
            this.rdeDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rdeDate.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rdeDate.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.rdeDate.Name = "rdeDate";
            // 
            // rceDt
            // 
            this.rceDt.AutoHeight = false;
            this.rceDt.Caption = "Check";
            this.rceDt.Name = "rceDt";
            // 
            // crTotal
            // 
            this.crTotal.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erNumber,
            this.erData,
            this.erStatus,
            this.erTeam,
            this.erVehicle,
            this.erRemark,
            this.erUser});
            this.crTotal.Height = 19;
            this.crTotal.Name = "crTotal";
            this.crTotal.Properties.Caption = "�������� �������";
            // 
            // erNumber
            // 
            this.erNumber.Height = 18;
            this.erNumber.Name = "erNumber";
            this.erNumber.Properties.Caption = "�����";
            this.erNumber.Properties.FieldName = "ID";
            this.erNumber.Properties.ReadOnly = true;
            // 
            // erData
            // 
            this.erData.Height = 18;
            this.erData.Name = "erData";
            this.erData.Properties.Caption = "����";
            this.erData.Properties.FieldName = "Date";
            this.erData.Properties.RowEdit = this.rdeDate;
            // 
            // erStatus
            // 
            this.erStatus.Height = 18;
            this.erStatus.Name = "erStatus";
            this.erStatus.Properties.Caption = "������";
            this.erStatus.Properties.FieldName = "PlanStatus";
            this.erStatus.Properties.RowEdit = this.rleStates;
            // 
            // erTeam
            // 
            this.erTeam.Height = 18;
            this.erTeam.Name = "erTeam";
            this.erTeam.Properties.Caption = "������ �����";
            this.erTeam.Properties.FieldName = "VehicleGroup";
            this.erTeam.Properties.RowEdit = this.rleTeam;
            // 
            // erVehicle
            // 
            this.erVehicle.Appearance.ForeColor = System.Drawing.Color.Green;
            this.erVehicle.Appearance.Options.UseForeColor = true;
            this.erVehicle.Appearance.Options.UseTextOptions = true;
            this.erVehicle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.erVehicle.Height = 18;
            this.erVehicle.Name = "erVehicle";
            this.erVehicle.Properties.Caption = "������";
            this.erVehicle.Properties.FieldName = "VehiclePlan";
            this.erVehicle.Properties.RowEdit = this.rleVehicle;
            // 
            // erRemark
            // 
            this.erRemark.Height = 17;
            this.erRemark.Name = "erRemark";
            this.erRemark.Properties.Caption = "����������";
            this.erRemark.Properties.FieldName = "Remark";
            // 
            // erUser
            // 
            this.erUser.Height = 18;
            this.erUser.Name = "erUser";
            this.erUser.Properties.Caption = "���������";
            this.erUser.Properties.FieldName = "UserCreated";
            this.erUser.Properties.ReadOnly = true;
            // 
            // crTask
            // 
            this.crTask.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erSquare});
            this.crTask.Name = "crTask";
            this.crTask.Properties.Caption = "�������";
            // 
            // erSquare
            // 
            this.erSquare.Height = 18;
            this.erSquare.Name = "erSquare";
            this.erSquare.Properties.Caption = "����������� ��� ��������� �������, ��";
            this.erSquare.Properties.FieldName = "SquarePlanned";
            // 
            // crFuel
            // 
            this.crFuel.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erFuelAtStart,
            this.erFuelForWork});
            this.crFuel.Name = "crFuel";
            this.crFuel.Properties.Caption = "�������";
            // 
            // erFuelAtStart
            // 
            this.erFuelAtStart.Height = 18;
            this.erFuelAtStart.Name = "erFuelAtStart";
            this.erFuelAtStart.Properties.Caption = "������� � ������, �";
            this.erFuelAtStart.Properties.FieldName = "FuelAtStart";
            this.erFuelAtStart.Properties.ReadOnly = true;
            // 
            // erFuelForWork
            // 
            this.erFuelForWork.Height = 18;
            this.erFuelForWork.Name = "erFuelForWork";
            this.erFuelForWork.Properties.Caption = "��������� ������� , �";
            this.erFuelForWork.Properties.FieldName = "FuelForWork";
            // 
            // crDownTime
            // 
            this.crDownTime.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erDt,
            this.erDtReason,
            this.erDtStart,
            this.erDtEnd});
            this.crDownTime.Name = "crDownTime";
            this.crDownTime.Properties.Caption = "������� �������";
            // 
            // erDt
            // 
            this.erDt.Name = "erDt";
            this.erDt.Properties.Caption = "������� �� ��������� � ������� �������";
            this.erDt.Properties.FieldName = "IsDownTime";
            this.erDt.Properties.RowEdit = this.rceDt;
            // 
            // erDtReason
            // 
            this.erDtReason.Height = 18;
            this.erDtReason.Name = "erDtReason";
            this.erDtReason.Properties.Caption = "������� �������";
            this.erDtReason.Properties.FieldName = "PlanDowntimeReason";
            this.erDtReason.Properties.RowEdit = this.rleDtReasons;
            // 
            // erDtStart
            // 
            this.erDtStart.Height = 18;
            this.erDtStart.Name = "erDtStart";
            this.erDtStart.Properties.Caption = "���� ������";
            this.erDtStart.Properties.FieldName = "DowntimeStart";
            this.erDtStart.Properties.RowEdit = this.rdeDate;
            // 
            // erDtEnd
            // 
            this.erDtEnd.Height = 18;
            this.erDtEnd.Name = "erDtEnd";
            this.erDtEnd.Properties.Caption = "���� ���������";
            this.erDtEnd.Properties.FieldName = "DowntimeEnd";
            this.erDtEnd.Properties.RowEdit = this.rdeDate;
            // 
            // gcPlan
            // 
            this.gcPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPlan.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcPlan_EmbeddedNavigator_ButtonClick);
            this.gcPlan.Location = new System.Drawing.Point(0, 0);
            this.gcPlan.MainView = this.bgvPlan;
            this.gcPlan.Name = "gcPlan";
            this.gcPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleDriver,
            this.rleField,
            this.rleAgregat,
            this.rleWorkType,
            this.rteTime});
            this.gcPlan.Size = new System.Drawing.Size(1005, 300);
            this.gcPlan.TabIndex = 7;
            this.gcPlan.UseEmbeddedNavigator = true;
            this.gcPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvPlan});
            // 
            // bgvPlan
            // 
            this.bgvPlan.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bgvPlan.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bgvPlan.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvPlan.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvPlan.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bgvPlan.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bgvPlan.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.bgvPlan.Appearance.Empty.Options.UseBackColor = true;
            this.bgvPlan.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvPlan.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvPlan.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.EvenRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.EvenRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.EvenRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvPlan.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvPlan.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bgvPlan.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvPlan.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bgvPlan.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.bgvPlan.Appearance.FixedLine.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bgvPlan.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bgvPlan.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.bgvPlan.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.bgvPlan.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bgvPlan.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bgvPlan.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvPlan.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvPlan.Appearance.GroupButton.Options.UseBackColor = true;
            this.bgvPlan.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bgvPlan.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bgvPlan.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.bgvPlan.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvPlan.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bgvPlan.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bgvPlan.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.GroupRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.GroupRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bgvPlan.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bgvPlan.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvPlan.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvPlan.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.HorzLine.Options.UseBackColor = true;
            this.bgvPlan.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvPlan.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvPlan.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.OddRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.OddRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.OddRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.bgvPlan.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.bgvPlan.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.bgvPlan.Appearance.Preview.Options.UseBackColor = true;
            this.bgvPlan.Appearance.Preview.Options.UseFont = true;
            this.bgvPlan.Appearance.Preview.Options.UseForeColor = true;
            this.bgvPlan.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvPlan.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.Row.Options.UseBackColor = true;
            this.bgvPlan.Appearance.Row.Options.UseForeColor = true;
            this.bgvPlan.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvPlan.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.bgvPlan.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bgvPlan.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.bgvPlan.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvPlan.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvPlan.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.bgvPlan.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bgvPlan.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bgvPlan.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bgvPlan.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvPlan.Appearance.VertLine.Options.UseBackColor = true;
            this.bgvPlan.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbPlan,
            this.gbFact});
            this.bgvPlan.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colId,
            this.colDriver,
            this.colField,
            this.colAgregat,
            this.colWorkType,
            this.colTimeStartPlan,
            this.colRemark,
            this.colTimeStartFact,
            this.colTimeEndFact,
            this.colFactSquareCalc});
            this.bgvPlan.GridControl = this.gcPlan;
            this.bgvPlan.Name = "bgvPlan";
            this.bgvPlan.OptionsView.EnableAppearanceEvenRow = true;
            this.bgvPlan.OptionsView.EnableAppearanceOddRow = true;
            this.bgvPlan.OptionsView.ShowFooter = true;
            this.bgvPlan.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bgvPlan_CellValueChanged);
            this.bgvPlan.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.bgvPlan_ValidateRow);
            // 
            // gbPlan
            // 
            this.gbPlan.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gbPlan.AppearanceHeader.Options.UseFont = true;
            this.gbPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.gbPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbPlan.Caption = "����";
            this.gbPlan.Columns.Add(this.colId);
            this.gbPlan.Columns.Add(this.colDriver);
            this.gbPlan.Columns.Add(this.colField);
            this.gbPlan.Columns.Add(this.colAgregat);
            this.gbPlan.Columns.Add(this.colWorkType);
            this.gbPlan.Columns.Add(this.colRemark);
            this.gbPlan.Columns.Add(this.colTimeStartPlan);
            this.gbPlan.MinWidth = 20;
            this.gbPlan.Name = "gbPlan";
            this.gbPlan.VisibleIndex = 0;
            this.gbPlan.Width = 665;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colDriver
            // 
            this.colDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.Caption = "��������";
            this.colDriver.ColumnEdit = this.rleDriver;
            this.colDriver.FieldName = "IdDriver";
            this.colDriver.Name = "colDriver";
            this.colDriver.Visible = true;
            this.colDriver.Width = 115;
            // 
            // rleDriver
            // 
            this.rleDriver.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rleDriver.AutoHeight = false;
            this.rleDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleDriver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "��������")});
            this.rleDriver.DisplayMember = "Family";
            this.rleDriver.Name = "rleDriver";
            this.rleDriver.NullText = "";
            this.rleDriver.ValueMember = "id";
            this.rleDriver.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rleDriver_KeyDown);
            // 
            // colField
            // 
            this.colField.AppearanceHeader.Options.UseTextOptions = true;
            this.colField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colField.Caption = "����";
            this.colField.ColumnEdit = this.rleField;
            this.colField.FieldName = "IdField";
            this.colField.Name = "colField";
            this.colField.Visible = true;
            this.colField.Width = 85;
            // 
            // rleField
            // 
            this.rleField.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rleField.AutoHeight = false;
            this.rleField.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleField.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleField.DisplayMember = "Name";
            this.rleField.Name = "rleField";
            this.rleField.NullText = "";
            this.rleField.ValueMember = "Id";
            this.rleField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rleField_KeyDown);
            // 
            // colAgregat
            // 
            this.colAgregat.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregat.Caption = "�������� ������������";
            this.colAgregat.ColumnEdit = this.rleAgregat;
            this.colAgregat.FieldName = "IdAgregat";
            this.colAgregat.Name = "colAgregat";
            this.colAgregat.Visible = true;
            this.colAgregat.Width = 147;
            // 
            // rleAgregat
            // 
            this.rleAgregat.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rleAgregat.AutoHeight = false;
            this.rleAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleAgregat.DisplayMember = "Name";
            this.rleAgregat.Name = "rleAgregat";
            this.rleAgregat.NullText = "";
            this.rleAgregat.ValueMember = "Id";
            this.rleAgregat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rleAgregat_KeyDown);
            // 
            // colWorkType
            // 
            this.colWorkType.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkType.Caption = "��� �����";
            this.colWorkType.ColumnEdit = this.rleWorkType;
            this.colWorkType.FieldName = "IdWork";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.Visible = true;
            this.colWorkType.Width = 113;
            // 
            // rleWorkType
            // 
            this.rleWorkType.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rleWorkType.AutoHeight = false;
            this.rleWorkType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleWorkType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleWorkType.DisplayMember = "Name";
            this.rleWorkType.Name = "rleWorkType";
            this.rleWorkType.NullText = "";
            this.rleWorkType.ValueMember = "Id";
            this.rleWorkType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rleWorkType_KeyDown);
            // 
            // colRemark
            // 
            this.colRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRemark.Caption = "����������";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.Visible = true;
            this.colRemark.Width = 90;
            // 
            // colTimeStartPlan
            // 
            this.colTimeStartPlan.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStartPlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStartPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartPlan.Caption = "����� ������";
            this.colTimeStartPlan.ColumnEdit = this.rteTime;
            this.colTimeStartPlan.DisplayFormat.FormatString = "g";
            this.colTimeStartPlan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStartPlan.FieldName = "TimeStartPlan";
            this.colTimeStartPlan.Name = "colTimeStartPlan";
            this.colTimeStartPlan.Visible = true;
            this.colTimeStartPlan.Width = 115;
            // 
            // rteTime
            // 
            this.rteTime.AutoHeight = false;
            this.rteTime.DisplayFormat.FormatString = "g";
            this.rteTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rteTime.EditFormat.FormatString = "g";
            this.rteTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rteTime.Mask.BeepOnError = true;
            this.rteTime.Mask.EditMask = "dd.MM.yyyy HH:mm";
            this.rteTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.rteTime.Name = "rteTime";
            // 
            // gbFact
            // 
            this.gbFact.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gbFact.AppearanceHeader.Options.UseFont = true;
            this.gbFact.AppearanceHeader.Options.UseTextOptions = true;
            this.gbFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbFact.Caption = "����";
            this.gbFact.Columns.Add(this.colTimeStartFact);
            this.gbFact.Columns.Add(this.colTimeEndFact);
            this.gbFact.Columns.Add(this.colFactSquareCalc);
            this.gbFact.MinWidth = 20;
            this.gbFact.Name = "gbFact";
            this.gbFact.VisibleIndex = 1;
            this.gbFact.Width = 371;
            // 
            // colTimeStartFact
            // 
            this.colTimeStartFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStartFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStartFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartFact.Caption = "����� ������";
            this.colTimeStartFact.DisplayFormat.FormatString = "t";
            this.colTimeStartFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStartFact.FieldName = "TimeStartFact";
            this.colTimeStartFact.Name = "colTimeStartFact";
            this.colTimeStartFact.OptionsColumn.AllowEdit = false;
            this.colTimeStartFact.OptionsColumn.ReadOnly = true;
            this.colTimeStartFact.Visible = true;
            this.colTimeStartFact.Width = 125;
            // 
            // colTimeEndFact
            // 
            this.colTimeEndFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEndFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEndFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEndFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEndFact.Caption = "����� �����";
            this.colTimeEndFact.DisplayFormat.FormatString = "t";
            this.colTimeEndFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEndFact.FieldName = "TimeEndFact";
            this.colTimeEndFact.Name = "colTimeEndFact";
            this.colTimeEndFact.OptionsColumn.AllowEdit = false;
            this.colTimeEndFact.OptionsColumn.ReadOnly = true;
            this.colTimeEndFact.Visible = true;
            this.colTimeEndFact.Width = 111;
            // 
            // colFactSquareCalc
            // 
            this.colFactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalc.Caption = "�����. ������� , ��";
            this.colFactSquareCalc.FieldName = "FactSquareCalc";
            this.colFactSquareCalc.Name = "colFactSquareCalc";
            this.colFactSquareCalc.OptionsColumn.AllowEdit = false;
            this.colFactSquareCalc.OptionsColumn.ReadOnly = true;
            this.colFactSquareCalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFactSquareCalc.Visible = true;
            this.colFactSquareCalc.Width = 135;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiRefresh,
            this.bbiPrint,
            this.bbiFact,
            this.bbiLog});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiLog, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiPrint, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "���������";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "��������";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiFact
            // 
            this.bbiFact.Caption = "����";
            this.bbiFact.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFact.Glyph")));
            this.bbiFact.Id = 3;
            this.bbiFact.Name = "bbiFact";
            this.bbiFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFact_ItemClick);
            // 
            // bbiLog
            // 
            this.bbiLog.Caption = "������ �������";
            this.bbiLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLog.Glyph")));
            this.bbiLog.Id = 4;
            this.bbiLog.Name = "bbiLog";
            this.bbiLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLog_ItemClick);
            // 
            // bbiPrint
            // 
            this.bbiPrint.Caption = "������";
            this.bbiPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrint.Glyph")));
            this.bbiPrint.Id = 2;
            this.bbiPrint.Name = "bbiPrint";
            this.bbiPrint.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1005, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 659);
            this.barDockControlBottom.Size = new System.Drawing.Size(1005, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 635);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1005, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 635);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 24);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcPlan);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1005, 635);
            this.splitContainerControl1.SplitterPosition = 330;
            this.splitContainerControl1.TabIndex = 12;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pgPlan);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.pgOrder);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1005, 330);
            this.splitContainerControl2.SplitterPosition = 507;
            this.splitContainerControl2.TabIndex = 7;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // pgOrder
            // 
            this.pgOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgOrder.Location = new System.Drawing.Point(0, 0);
            this.pgOrder.Name = "pgOrder";
            this.pgOrder.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgOrder.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crOrdTotal,
            this.crOrdExecute,
            this.crOrdFuel});
            this.pgOrder.Size = new System.Drawing.Size(493, 330);
            this.pgOrder.TabIndex = 0;
            // 
            // crOrdTotal
            // 
            this.crOrdTotal.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erOrdId,
            this.erOrdTimeStart,
            this.erOrdTimeEnd,
            this.erOrdDistance,
            this.erOrdSpeedAvg,
            this.erOrdRemark,
            this.erOrdCreator});
            this.crOrdTotal.Name = "crOrdTotal";
            this.crOrdTotal.Properties.Caption = "�����";
            // 
            // erOrdId
            // 
            this.erOrdId.Height = 18;
            this.erOrdId.Name = "erOrdId";
            this.erOrdId.Properties.Caption = "�����";
            this.erOrdId.Properties.FieldName = "ID";
            this.erOrdId.Properties.ReadOnly = true;
            // 
            // erOrdTimeStart
            // 
            this.erOrdTimeStart.Height = 18;
            this.erOrdTimeStart.Name = "erOrdTimeStart";
            this.erOrdTimeStart.Properties.Caption = "����� ������ ��������";
            this.erOrdTimeStart.Properties.FieldName = "TimeStart";
            this.erOrdTimeStart.Properties.ReadOnly = true;
            // 
            // erOrdTimeEnd
            // 
            this.erOrdTimeEnd.Height = 18;
            this.erOrdTimeEnd.Name = "erOrdTimeEnd";
            this.erOrdTimeEnd.Properties.Caption = "����� ��������� ��������";
            this.erOrdTimeEnd.Properties.FieldName = "TimeEnd";
            this.erOrdTimeEnd.Properties.ReadOnly = true;
            // 
            // erOrdDistance
            // 
            this.erOrdDistance.Height = 18;
            this.erOrdDistance.Name = "erOrdDistance";
            this.erOrdDistance.Properties.Caption = "���������� ����, ��";
            this.erOrdDistance.Properties.FieldName = "Distance";
            this.erOrdDistance.Properties.ReadOnly = true;
            // 
            // erOrdSpeedAvg
            // 
            this.erOrdSpeedAvg.Height = 18;
            this.erOrdSpeedAvg.Name = "erOrdSpeedAvg";
            this.erOrdSpeedAvg.Properties.Caption = "������� ��������, ��/�";
            this.erOrdSpeedAvg.Properties.FieldName = "SpeedAvg";
            this.erOrdSpeedAvg.Properties.ReadOnly = true;
            // 
            // erOrdRemark
            // 
            this.erOrdRemark.Height = 18;
            this.erOrdRemark.Name = "erOrdRemark";
            this.erOrdRemark.Properties.Caption = "����������";
            this.erOrdRemark.Properties.FieldName = "Remark";
            this.erOrdRemark.Properties.ReadOnly = true;
            // 
            // erOrdCreator
            // 
            this.erOrdCreator.Height = 18;
            this.erOrdCreator.Name = "erOrdCreator";
            this.erOrdCreator.Properties.Caption = "���������";
            this.erOrdCreator.Properties.FieldName = "UserCreated";
            this.erOrdCreator.Properties.ReadOnly = true;
            // 
            // crOrdExecute
            // 
            this.crOrdExecute.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erOrdSquare});
            this.crOrdExecute.Name = "crOrdExecute";
            this.crOrdExecute.Properties.Caption = "����������";
            // 
            // erOrdSquare
            // 
            this.erOrdSquare.Name = "erOrdSquare";
            this.erOrdSquare.Properties.Caption = "������������ �������, ��";
            this.erOrdSquare.Properties.FieldName = "FactSquareCalcOverlap";
            this.erOrdSquare.Properties.ReadOnly = true;
            // 
            // crOrdFuel
            // 
            this.crOrdFuel.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erOrdFuelDUTStart,
            this.erOrdFuelDUTAdd,
            this.erOrdFuelDUTSub,
            this.erOrdFuelDUTEnd,
            this.erOrdFuelDUTExpense});
            this.crOrdFuel.Name = "crOrdFuel";
            this.crOrdFuel.Properties.Caption = "�������";
            // 
            // erOrdFuelDUTStart
            // 
            this.erOrdFuelDUTStart.Height = 18;
            this.erOrdFuelDUTStart.Name = "erOrdFuelDUTStart";
            this.erOrdFuelDUTStart.Properties.Caption = "������� � ������ , �";
            this.erOrdFuelDUTStart.Properties.FieldName = "FuelDUTStart";
            this.erOrdFuelDUTStart.Properties.ReadOnly = true;
            // 
            // erOrdFuelDUTAdd
            // 
            this.erOrdFuelDUTAdd.Height = 18;
            this.erOrdFuelDUTAdd.Name = "erOrdFuelDUTAdd";
            this.erOrdFuelDUTAdd.Properties.Caption = "������� ���������� , �";
            this.erOrdFuelDUTAdd.Properties.FieldName = "FuelDUTAdd";
            this.erOrdFuelDUTAdd.Properties.ReadOnly = true;
            // 
            // erOrdFuelDUTSub
            // 
            this.erOrdFuelDUTSub.Height = 18;
            this.erOrdFuelDUTSub.Name = "erOrdFuelDUTSub";
            this.erOrdFuelDUTSub.Properties.Caption = "������� ����� , �";
            this.erOrdFuelDUTSub.Properties.FieldName = "FuelDUTSub";
            this.erOrdFuelDUTSub.Properties.ReadOnly = true;
            // 
            // erOrdFuelDUTEnd
            // 
            this.erOrdFuelDUTEnd.Height = 18;
            this.erOrdFuelDUTEnd.Name = "erOrdFuelDUTEnd";
            this.erOrdFuelDUTEnd.Properties.Caption = "������� � ����� , �";
            this.erOrdFuelDUTEnd.Properties.FieldName = "FuelDUTEnd";
            this.erOrdFuelDUTEnd.Properties.ReadOnly = true;
            // 
            // erOrdFuelDUTExpense
            // 
            this.erOrdFuelDUTExpense.Name = "erOrdFuelDUTExpense";
            this.erOrdFuelDUTExpense.Properties.Caption = "������ ������� , �";
            this.erOrdFuelDUTExpense.Properties.FieldName = "FuelDUTExpense";
            this.erOrdFuelDUTExpense.Properties.ReadOnly = true;
            // 
            // PlanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 659);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PlanForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�������� �������";
            ((System.ComponentModel.ISupportInitialize)(this.pgPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDtReasons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rceDt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWorkType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erData;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTeam;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erVehicle;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquare;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crFuel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erFuelForWork;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRemark;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erStatus;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crDownTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDtReason;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDtStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDtEnd;
        internal DevExpress.XtraVerticalGrid.PropertyGridControl pgPlan;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTeam;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiPrint;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleStates;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erUser;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehicle;
        private DevExpress.XtraBars.BarButtonItem bbiFact;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crOrdTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdTimeStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdTimeEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdDistance;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdSpeedAvg;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crOrdFuel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdFuelDUTStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdFuelDUTAdd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdFuelDUTSub;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdFuelDUTEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdRemark;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdCreator;
        internal DevExpress.XtraVerticalGrid.PropertyGridControl pgOrder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTask;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crOrdExecute;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdSquare;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOrdFuelDUTExpense;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleDriver;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleField;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleAgregat;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleWorkType;
        internal DevExpress.XtraGrid.GridControl gcPlan;
        internal System.Windows.Forms.BindingSource bsPlan;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleDtReasons;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rceDt;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDt;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erFuelAtStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDriver;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colField;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgregat;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStartPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRemark;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStartFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeEndFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactSquareCalc;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteTime;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbFact;
        private DevExpress.XtraBars.BarButtonItem bbiLog;


    }
}