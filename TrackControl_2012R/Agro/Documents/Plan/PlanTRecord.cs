﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro
{
    public class PlanTRecord
    {

        public PlanTRecord()
        {
        }

        public int Id {get;set;}

        public int IdPlan { get; set; }

        public int? IdField { get; set; }

        public int IdDriver { get; set; }

        public int IdAgregat { get; set; }

        public int IdWork { get; set; }

        public string Remark { get; set; }

        public DateTime? TimeStartPlan { get; set; }

        public DateTime? TimeStartFact { get; set; }

        public DateTime? TimeEndFact { get; set; }

        public double FactSquareCalc { get; set; }
        
    }
}
