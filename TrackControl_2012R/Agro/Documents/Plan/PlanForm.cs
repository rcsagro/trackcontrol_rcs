using Agro.Properties;
using TrackControl.General;
using TrackControl.Vehicles;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Collections.Generic;
using System;
namespace Agro
{
    public partial class PlanForm : DevExpress.XtraEditors.XtraForm
    {
        public PlanForm()
        {
            InitializeComponent();
            Localization();
        }

        #region �����������
        private void Localization()
        {
            crOrdTotal.Properties.Caption = Resources.Detaill;
            erData.Properties.Caption = Resources.Date;
            bbiLog.Caption = Resources.LogEvents;
            gbFact.Caption = Resources.Fact;
            gbPlan.Caption = Resources.Plan;
            colDriver.Caption = Resources.Driver;
            colField.Caption = Resources.Field;
            colAgregat.Caption = Resources.Agregat;
            colWorkType.Caption = Resources.WorkType;
            colTimeStartPlan.Caption = Resources.TimeStartShort;
            colRemark.Caption = Resources.Remark;
            colFactSquareCalc.Caption = Resources.SquareProcessGa;
            colTimeStartFact.Caption = Resources.TimeStartShort;
            colTimeStartFact.ToolTip = Resources.TimeStartShort;
            colTimeEndFact.Caption = Resources.TimeEndShort;
            colTimeEndFact.ToolTip = Resources.TimeEndShort;
        }
        #endregion

        private void pgPlan_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            var peHeader = (PlanEntity)pgPlan.SelectedObject;
            switch (e.Row.Name)
            {
                case "erTeam":
                    {
                        var groupe = (VehiclesGroup)rleTeam.GetDataSourceRowByKeyValue(e.Value );
                        rleVehicle.DataSource = groupe == null ? null : groupe.OwnRealItems();
                        break;
                    }
            }

            if (peHeader != null)
            {
                using (var pc = new PlanController(peHeader))
                {
                   if (pc.ValidateHeader())  bbiSave.Enabled = true;
                }
            }

           
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SavePlan();
        }

        private void SavePlan()
        {
            var peHeader = (PlanEntity)pgPlan.SelectedObject;
            using (var pc = new PlanController(peHeader))
            {
                if (peHeader.ID == 0)
                {
                    pc.AddDoc();
                    if (peHeader.ID == 0) return;
                    gcPlan.DataSource = PlanTController.GetList(peHeader.ID);
                }
                else
                    if (!pc.UpdateDoc()) return; 
            }
            RefreshPlan();
            bbiSave.Enabled = false;
        }

        private void SetFact()
        {
            PlanEntity peHeader = (PlanEntity)pgPlan.SelectedObject;
            using (PlanController pc = new PlanController(peHeader))
            {

                if (pc.SetFact())
                {
                    RefreshPlan();
                }
            }
        }

        void RefreshPlan()
        {
            pgPlan.Refresh();
            PlanEntity peHeader = (PlanEntity)pgPlan.SelectedObject;
            if (peHeader != null)
            {
                pgOrder.SelectedObject = peHeader.Order;
                pgOrder.Refresh();
                peHeader.ConnectToOrderRecords();
                gcPlan.DataSource = peHeader.PlanRecords;
                gcPlan.RefreshDataSource();
            }
        }

        private void bbiFact_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SetFact();
        }

        private void bgvPlan_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            SaveRecord(e.RowHandle);
        }

        private void SaveRecord(int rowHandle)
        {
            PlanTRecord record = bgvPlan.GetRow(rowHandle) as PlanTRecord;
            if (record.Id == 0)
            {
                PlanEntity peHeader = (PlanEntity)pgPlan.SelectedObject;
                record.IdPlan = peHeader.ID; 
            }
            PlanTController.Save(record);  
        }

        private void bgvPlan_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            PlanTRecord record = bgvPlan.GetRow(e.RowHandle) as PlanTRecord;
            if (record == null)
            {
                e.Valid = false;
                return;
            }
            if (PlanTController.Validate(record))
            {
               e.Valid =true ;
            }
            else
            {
                e.Valid = false;
            }
        }

        private void gcPlan_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:
                    {
                        if (XtraMessageBox.Show(Resources.DeleteQuestion,
                        Resources.ApplicationName,
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            PlanTRecord record = bgvPlan.GetRow(bgvPlan.FocusedRowHandle) as PlanTRecord;
                            if (record == null)
                                e.Handled = true;
                            else
                                e.Handled = !PlanTController.Delete(record);
                        }
                        else
                        {
                            e.Handled = true;
                        }
                        break;
                    }
            }
        }

        private void rleField_KeyDown(object sender, KeyEventArgs e)
        {
            PlanTRecord record = DeleteLookUpEditValue(sender, e);
            if (record != null) record.IdField = 0;
        }

        private void rleDriver_KeyDown(object sender, KeyEventArgs e)
        {
            PlanTRecord record = DeleteLookUpEditValue(sender, e);
            if (record != null) record.IdDriver = 0;
        }

        private void rleAgregat_KeyDown(object sender, KeyEventArgs e)
        {
            PlanTRecord record = DeleteLookUpEditValue(sender, e);
            if (record != null) record.IdAgregat = 0;
        }

        private void rleWorkType_KeyDown(object sender, KeyEventArgs e)
        {
            PlanTRecord record = DeleteLookUpEditValue(sender, e);
            if (record != null) record.IdWork = 0;
        }

        PlanTRecord DeleteLookUpEditValue(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                (sender as LookUpEdit).EditValue = 0;
                return bgvPlan.GetRow(bgvPlan.FocusedRowHandle) as PlanTRecord;
            }
            else
                return null;
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshPlan();
        }

        private void bbiLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PlanEntity peHeader = (PlanEntity)pgPlan.SelectedObject;
            if (peHeader != null && peHeader.ID >0) UserLog.ViewLog(UserLogTypes.AGRO_PLAN, peHeader.ID);
        }


    }
}