﻿using Agro.Properties;
using Agro.Utilites;
using System;
using System.ComponentModel;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites;

namespace Agro
{
    public static class PlanTController
    {
        public static void Save(PlanTRecord record)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                if (record.Id == 0)
                {
                    InsertRecord(record, driverDb);
                }
                else
                {
                    UpdateRecord(record, driverDb);
                }
            }
        }

        private static void InsertRecord(PlanTRecord record, DriverDb driverDb)
        {
            string sSQL = string.Format(AgroQuery.PlanTController.InsertIntoAgroPlant, driverDb.ParamPrefics);
            CreateParameters(driverDb, record);
            record.Id = driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_plant");
            UserLog.InsertLog(UserLogTypes.AGRO_PLAN, string.Format(Resources.LogAddContent, record.Id), record.IdPlan);
        }

        private static void CreateParameters(DriverDb driverDb, PlanTRecord record)
        {
            driverDb.NewSqlParameterArray(7);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", record.IdPlan);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", record.IdField); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (record.IdField != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", record.IdField);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", DBNull.Value);
                }
            }

            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_driver", record.IdDriver);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", record.IdWork);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_agregat", record.IdAgregat);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Remark", record.Remark); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (record.Remark != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Remark", record.Remark);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Remark", "");
                }
            }
            if (record.TimeStartPlan == null && record.Id == 0)
                record.TimeStartPlan = TUtil.GetOrderDate(DateTime.Today);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", record.TimeStartPlan);
        }

        private static void UpdateRecord(PlanTRecord record, DriverDb driverDb)
        {
            string sSQL = string.Format(AgroQuery.PlanTController.UpdateAgroPlant, driverDb.ParamPrefics, record.Id);
            CreateParameters(driverDb, record);
            driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_plant");
            UserLog.InsertLog(UserLogTypes.AGRO_PLAN, string.Format(Resources.LogUpdateContent, record.Id), record.IdPlan);
        }

        public static bool Validate(PlanTRecord record)
        {
            if (record.IdDriver > 0 || record.IdField > 0)
                return true;
            else
                return false; 
        }

        public static BindingList<PlanTRecord> GetList(int idPlan)
        {
            BindingList<PlanTRecord> records = new BindingList<PlanTRecord>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.PlanTController.SelectAgroPlant, idPlan);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    PlanTRecord record = new PlanTRecord();
                    record.Id = driverDb.GetInt32("Id"); 
                    record.IdPlan = driverDb.GetInt32("Id_main");
                    record.IdDriver = (int)TotUtilites.NdbNullReader(driverDb, "Id_driver", 0);
                    record.IdField = (int)TotUtilites.NdbNullReader(driverDb, "Id_field", 0);
                    record.IdAgregat = (int)TotUtilites.NdbNullReader(driverDb, "Id_agregat", 0);
                    record.IdWork = (int)TotUtilites.NdbNullReader(driverDb, "Id_work", 0);
                    record.Remark = (string)TotUtilites.NdbNullReader(driverDb, "Remark", "");
                    record.TimeStartPlan = (DateTime?)TotUtilites.NdbNullReader(driverDb, "TimeStart", null);
                    records.Add(record); 
                }
                driverDb.CloseDataReader(); 
            }
            return records;
        }

        public static bool Delete(PlanTRecord record)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.PlanTController.DeleteFromAgroPlant, record.Id);
                driverDb.ExecuteNonQueryCommand(sql);
                return true;
            }
        }
    }
}
