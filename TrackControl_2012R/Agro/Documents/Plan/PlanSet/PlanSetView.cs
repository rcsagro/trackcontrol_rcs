﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Agro.Utilites;
using Agro.Properties;
using DevExpress.XtraReports.Serialization;
using TrackControl.General;
using TrackControl.Vehicles; 
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace Agro
{
    public partial class PlanSetView : UserControl
    {

        private ControlDataInterval cdePlanSet;

        public PlanSetView()
        {
            InitializeComponent();
            Init();
            SetControlDates();
            RefreshData();
        }

        private void Init()
        {
            colId.Caption = Resources.NumberId;
            colId.ToolTip = Resources.NumberId;
            colPlanDate.Caption = Resources.planDate;
            colPlanDate.ToolTip = Resources.planDate;
            colNumberPlate.Caption = Resources.numberPlate;
            colNumberPlate.ToolTip = Resources.numberPlate;
            colFamily.Caption = Resources.familyDrive;
            colFamily.ToolTip = Resources.familyDrive;
            colAgrName.Caption = Resources.agrName;
            colAgrName.ToolTip = Resources.agrName;
            colWorkName.Caption = Resources.workName;
            colWorkName.ToolTip = Resources.workName;
            colFieldName.Caption = Resources.fieldName;
            colFieldName.ToolTip = Resources.fieldName;
            colId_order.Caption = Resources.OrderId;
            colId_order.ToolTip = Resources.OrderId;
            colPlanComment.Caption = Resources.planComment;
            colPlanComment.ToolTip = Resources.planComment;
            colFuelForWork.Caption = Resources.FuelWork;
            colFuelForWork.ToolTip = Resources.FuelWork;
            grDownTime.Text = Resources.DownTime;
            grWithoutTasks.Text = Resources.WithoutTasks;
            colDtId.Caption = Resources.NumberId;
            colDtId.ToolTip = Resources.NumberId;
            colDtPlanDate.Caption = Resources.planDate;
            colDtPlanDate.ToolTip = Resources.planDate;
            colDtVehicleIdent.Caption = Resources.numberPlate;
            colDtVehicleIdent.ToolTip = Resources.numberPlate;
            colDtReasonName.Caption = Resources.ReasonName;
            colDtReasonName.ToolTip = Resources.ReasonName;
            colDtDowntimeStart.Caption = Resources.Begin;
            colDtDowntimeStart.ToolTip = Resources.Begin;
            colDtDowntimeEnd.Caption = Resources.End;
            colDtDowntimeEnd.ToolTip = Resources.End;
            colWtLastDate.Caption = Resources.WtLastDate;
            colWtLastDate.ToolTip = Resources.WtLastDate;
            colWtLastId.Caption = Resources.NumberId;
            colWtLastId.ToolTip = Resources.NumberId;
            colWtVehicleId.Caption = "";
            colWtVehicleId.ToolTip = "";
            colWtVehicleGroup.Caption = Resources.VehicleGroupe;
            colWtVehicleGroup.ToolTip = Resources.VehicleGroupe;
            colWtVehicleIdent.Caption = Resources.numberPlate;
            colWtVehicleIdent.ToolTip = Resources.numberPlate;
            bbiStart.Caption = Resources.PUSK;
            bdeStart.Caption = Resources.bdeStart;
            bdeEnd.Caption = Resources.bdeEnd;
        }

        private void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        public void RefreshData()
        {
            gcPlan.DataSource = PlanSetController.GetSet((DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue);
            gcDownTime.DataSource = PlanSetController.GetSetDownTime((DateTime) bdeStart.EditValue,
                (DateTime) bdeEnd.EditValue);
            gcWithoutTasks.DataSource = PlanSetController.GetSetWithoutTask();
        }

        private void SetControlDates()
        {
            DateTime _stardData = TotUtilites.GetOrderDate(DateTime.Today);
            cdePlanSet = new ControlDataInterval(_stardData, _stardData.AddDays(1).AddMinutes(-1));
            bdeStart.EditValue = cdePlanSet.Begin;
            bdeEnd.EditValue = cdePlanSet.End;
        }

        public void ExportExcel()
        {
            XtraGridService.ExportToExcel(gvPlan, "Плановые задания");
        }

        private void gvPlan_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvPlan);
        }

        private void gvWithoutTasks_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvWithoutTasks);
        }

        private void gvDownTime_DoubleClick(object sender, EventArgs e)
        {
            ViewPlan(gvDownTime);
        }

        public void ViewPlanFromMainGrid()
        {
            ViewPlan(gvPlan);
        }

        private void ViewPlan(GridView gv)
        {
            if (!gv.IsValidRowHandle(gv.FocusedRowHandle))
            {
                XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                return;
            }

            int idPlan;
            if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, "Id").ToString(), out idPlan))
            {
                using (PlanController pc = new PlanController())
                {
                    pc.ViewPlan(idPlan);
                }
            }
        }

        public void DeleteSelectedFromGrid()
        {
            using (PlanController pc = new PlanController())
            {
                pc.DeleteSelectedFromGrid(gvPlan);
            }
        }

        private void gvWithoutTasks_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu) e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.CreatePlanOrder,
                new EventHandler(CreateOrderFromWithoutTaskRecord));
            gvMenu.Items.Add(menuItem);
        }

        private void CreateOrderFromWithoutTaskRecord(object sender, System.EventArgs e)
        {
            int idVihicle = 0;

            if (
                Int32.TryParse(
                    gvWithoutTasks.GetRowCellValue(gvWithoutTasks.FocusedRowHandle, gvWithoutTasks.Columns["VehicleId"])
                        .ToString(), out idVihicle))
            {
                Vehicle vh = DocItem.VehiclesModel.GetVehicleById(idVihicle);
                if (vh != null)
                {

                    using (PlanController pc = new PlanController())
                    {
                        pc.AddNewPlanFromView(vh);
                        RefreshData();
                    }
                }
            }
        }
    }
}
