﻿using System;
using System.Data;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites;

namespace Agro
{
    public static class PlanSetController
    {
        public static DataTable GetSet(DateTime start,DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sql = string.Format(AgroQuery.PlanSetController.SelectAgroPlanId, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent);

                TUtil.SetTimeParams(start, end, driverDb);
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }

        public static DataTable GetSetDownTime(DateTime start, DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.PlanSetController.SelectFromAgroPlanId, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent);
                TUtil.SetTimeParams(start, end, driverDb);
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }

        public static DataTable GetSetWithoutTask()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
              
                string sql = string.Format(AgroQuery.PlanSetController.SelectDistinctFromVehicle, driverDb.ParamPrefics, AgroQuery.SqlVehicleIdent
                                                   , AgroQuery.PlanSetController.SelectAgroPlanIdMobitel
                                                   , AgroQuery.PlanSetController.SqlLastDate);
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeToday", TUtil.GetOrderDate(DateTime.Today));
                return driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
        }
    }
}
