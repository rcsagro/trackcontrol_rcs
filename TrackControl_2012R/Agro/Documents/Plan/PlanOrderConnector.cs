﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    public class PlanOrderConnector
    {
        PlanEntity _plan;

        public PlanEntity Plan
        {
            get { return _plan; }
            set { _plan = value; }
        }

        OrderItem _order;

        public OrderItem Order
        {
            get { return _order; }
            set { _order = value; }
        }


        public PlanOrderConnector()
        {

        }

        public bool  FindOrder()
        {
            try
            {
                if (_plan == null || _plan.VehiclePlan == null) 
                    return false;

                if (_order != null && _order.ID > 0 && _order.Mobitel_Id == _plan.VehiclePlan.MobitelId) 
                    return true;

                _order = new OrderItem(new DateTime(_plan.Date.Year, _plan.Date.Month, _plan.Date.Day),
                                       _plan.VehiclePlan.MobitelId);

                string messageAboutExist = "";
                if (_order.IsExist(ref messageAboutExist))
                {
                    _order.GetDocById(_order.ID);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "FindOrder: Error");
                return false;
            }
        }

        public void FillPlanRecordsOrderValues(IList<PlanTRecord> planRecords, IList<OrderItemRecord> orderRecords)
        {
            if (planRecords == null || orderRecords == null) return;
            foreach (PlanTRecord planRecord in planRecords)
            {
                planRecord.TimeStartFact = null;
                planRecord.TimeEndFact = null;
                planRecord.FactSquareCalc = 0;
                foreach (OrderItemRecord orderRecord in orderRecords)
                {
                    if (MatchRecords(planRecord, orderRecord))
                    {
                        if (planRecord.TimeStartFact == null)
                        {
                            planRecord.TimeStartFact = orderRecord.TimeStart;
                            planRecord.FactSquareCalc = orderRecord.SquareCalcCont;
                        }
                        else
                        {
                            planRecord.FactSquareCalc += orderRecord.SquareCalcCont;
                        }
                        planRecord.TimeEndFact = orderRecord.TimeEnd;
                        
                    }
                }
            }
        }

        bool MatchRecords(PlanTRecord planRecord,OrderItemRecord orderRecord)
        {
            if (planRecord.IdDriver == orderRecord.DriverId
               && (planRecord.IdField ?? 0) == orderRecord.FieldId
                && planRecord.IdAgregat == orderRecord.AgregatId
                && planRecord.IdWork == orderRecord.WorkId)
                return true;
            else
                return false;
        }
    }
}
