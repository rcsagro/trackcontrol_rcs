﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agro
{
    /// <summary>
    /// запись наряда о фактическом выполнении работ по технологической карте
    /// (сгруппированные по полю и виду работ данные одного наряда)
    /// </summary>
    public class TechOperationFactRecord
    {
        public int Id { get; set; }
        public FieldTechOperation Operation { get; set; }
        public DateTime DateOrder { get; set; }
        public int NumberOrder { get; set; }
        public string VehicleName { get; set; }
        public string DriverName { get; set; }
        public string AgregateName { get; set; }
        public Decimal SquareFactGa { get; set; }
        public bool  JointProcess  { get; set; }
        public Decimal SquareAfterRecalc { get; set; }
        public Decimal SquareForOperation
        {
            get
            {
                if (SquareAfterRecalc > 0)
                    return SquareAfterRecalc;
                else
                    return SquareFactGa;
            }
        }
        public DateTime? DateRecalc { get; set; }
        public string UserRecalc { get; set; }
        public List<int> IdOrderTs { get; set; }
        public void Save()
        {
            TechOpersFactController.Save(this);
        }
    }
}
