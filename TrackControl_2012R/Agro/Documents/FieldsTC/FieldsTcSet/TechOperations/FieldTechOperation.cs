﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Agro.Dictionaries;

namespace Agro
{
    /// <summary>
    /// операция в технологической карте поля 
    /// </summary>
    public class FieldTechOperation
    {
        public int Id { get; set; }

        /// <summary>
        /// операция технологической карты на поле
        /// </summary>
        public FieldsTcEntity FieldsTc { get; set; }
        public DictionaryAgroWorkType WorkType { get; set; }
        public DateTime? DateStartPlan { get; set; }
        public DateTime? DateEndPlan { get; set; }
        public Decimal WorkQtyPlan { get; set; }
        public DateTime? DateStartFact { get; set; }
        public DateTime? DateEndFact { get; set; }
        public Decimal SquareGa { get; set; }
        public Decimal SquarePercent { get; set; }
        public string Comment { get; set; }
        public IList<TechOperationFactRecord> FactRecords { get; set; } 
        public void DeleteOrderTLinks()
        {
            FieldTechOpersController.DeleteOrderTLinks(this);  
        }

        public void SaveOrderTLinks()
        {
            FieldTcOrderConnector connector = new FieldTcOrderConnector(this);
            connector.SaveOrderItemRecordsList(); 
        }

        public void Save()
        {
            FieldTechOpersController.Save(this); 
        }

        public void RecalcSquare()
        {
            this.SquareGa = 0;
            this.SquarePercent = 0;
            foreach (TechOperationFactRecord record in this.FactRecords)
            {
                this.SquareGa += record.SquareForOperation; 
            }
            if (this.FieldsTc.Field.Square > 0)
            {
                this.SquarePercent = Math.Round(100 * this.SquareGa / (decimal)this.FieldsTc.Field.Square, 2);
            }
            FieldTechOpersController.Save(this);
        }

        
    }
}
