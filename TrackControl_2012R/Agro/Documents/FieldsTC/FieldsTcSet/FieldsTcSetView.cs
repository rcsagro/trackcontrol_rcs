﻿using System;
using DevExpress.XtraEditors;
using Agro.Dictionaries;
using Agro.Utilites;
using Agro.Properties;
using TrackControl.General;

namespace Agro
{
    public partial class FieldsTcSetView : DevExpress.XtraEditors.XtraUserControl
    {
        public FieldsTcSetView()
        {
            InitializeComponent();
            Init();
            InitControls();
        }

        void Init()
        {
            bbiStart.Caption = Resources.PUSK;
            bdeStart.Caption = Resources.bdeStart;
            bdeEnd.Caption = Resources.bdeEnd;
            bleCulture.Caption = Resources.bleCulture;
            bleFieldGroups.Caption = Resources.bleFieldGroups;
            bleField.Caption = Resources.bleField;

            colId.Caption = Resources.NumberId;
            colId.ToolTip = Resources.NumberId;
            colDate.Caption = Resources.Date;
            colDate.ToolTip = Resources.Date;
            colCultureName.Caption = Resources.Cultura;
            colCultureName.ToolTip = Resources.Cultura;
            colDateStart.Caption = Resources.DateStarte;
            colDateStart.ToolTip = Resources.DateStarte;
            colDateEnd.Caption = Resources.theDateEnd;
            colDateEnd.ToolTip = Resources.theDateEnd;
            colFieldGroupName.Caption = Resources.fieldGroupName;
            colFieldGroupName.ToolTip = Resources.fieldGroupName;
            colFieldName.Caption = Resources.NameField;
            colFieldName.ToolTip = Resources.NameField;
            colOpersQty.Caption = Resources.QtyOpers;
            colOpersQty.ToolTip = Resources.QtyOpers;
            colComment.Caption = Resources.planComment;
            colComment.ToolTip = Resources.planComment;
        }

        void InitControls()
        {
            leCulture.DataSource = DictionaryAgroCulture.GetList();
            leFieldGroups.DataSource = DictionaryAgroFieldGrp.GetList();
            SetControlDates();
        }

        private void bleFieldGroups_EditValueChanged(object sender, EventArgs e)
        {
            if (bleFieldGroups.EditValue != null)
            leField.DataSource = DictionaryAgroField.GetList((int)bleFieldGroups.EditValue);
        }

        public void RefreshData()
        {
            int idCulture = 0;
            if (bleCulture.EditValue != null) idCulture = (int)bleCulture.EditValue;
            int idFieldGroup = 0;
            if (bleFieldGroups.EditValue != null) idFieldGroup = (int)bleFieldGroups.EditValue;
            int idField = 0;
            if (bleField.EditValue != null) idField = (int)bleField.EditValue;
            gcSet.DataSource = FieldsTcSetController.GetSet((DateTime)bdeStart.EditValue, (DateTime)bdeEnd.EditValue, idCulture, idFieldGroup, idField);
        }

        private void SetControlDates()
        {
            DateTime stardData = DateTime.Today;
            var cdePlanSet = new ControlDataInterval(stardData, stardData.AddDays(1).AddMinutes(-1));
            bdeStart.EditValue = cdePlanSet.Begin;
            bdeEnd.EditValue = cdePlanSet.End;
        }

        private void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void bbiFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleCulture.EditValue  = null;
            bleFieldGroups.EditValue = null;
            bleField.EditValue = null; 
        }

        public void ViewDocumentFromGrid()
        {
            if (!gvSet.IsValidRowHandle(gvSet.FocusedRowHandle))
            {
                XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                return;
            }

            int idDoc;
            if (Int32.TryParse(gvSet.GetRowCellValue(gvSet.FocusedRowHandle, "Id").ToString(), out idDoc))
            {
                using (FieldsTcController fc = new FieldsTcController())
                {
                    fc.ViewForm(idDoc);
                }
            }
        }

        private void gvSet_DoubleClick(object sender, EventArgs e)
        {
            ViewDocumentFromGrid();
        }

        public void DeleteSelectedFromGrid()
        {
            using (FieldsTcController pc = new FieldsTcController())
            {
                pc.DeleteSelectedFromGrid(gvSet);
            }
        }

        public void ExportExcel()
        {
            XtraGridService.ExportToExcel(gvSet, "Технологические карты полей");
        }
    }
}
