﻿namespace Agro
{
    partial class FieldsTcSetView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FieldsTcSetView));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.bdeStart = new DevExpress.XtraBars.BarEditItem();
            this.deStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEnd = new DevExpress.XtraBars.BarEditItem();
            this.deEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bleCulture = new DevExpress.XtraBars.BarEditItem();
            this.leCulture = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleFieldGroups = new DevExpress.XtraBars.BarEditItem();
            this.leFieldGroups = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleField = new DevExpress.XtraBars.BarEditItem();
            this.leField = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bbiFilterClear = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gcSet = new DevExpress.XtraGrid.GridControl();
            this.gvSet = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCultureName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOpersQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSet)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiStart,
            this.bdeStart,
            this.bdeEnd,
            this.bleCulture,
            this.bleFieldGroups,
            this.bleField,
            this.bbiFilterClear});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 7;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.deStart,
            this.deEnd,
            this.leCulture,
            this.leFieldGroups,
            this.leField});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.bdeStart, "", false, true, true, 140, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEnd, "", false, true, true, 123),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleCulture, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleFieldGroups, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleField, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFilterClear)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "ПУСК";
            this.bbiStart.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStart.Glyph")));
            this.bbiStart.Id = 0;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // bdeStart
            // 
            this.bdeStart.Caption = "Дата с";
            this.bdeStart.Edit = this.deStart;
            this.bdeStart.Id = 1;
            this.bdeStart.Name = "bdeStart";
            this.bdeStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bdeStart.Width = 60;
            // 
            // deStart
            // 
            this.deStart.AutoHeight = false;
            this.deStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStart.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F6);
            this.deStart.DisplayFormat.FormatString = "g";
            this.deStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.EditFormat.FormatString = "g";
            this.deStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Mask.EditMask = "g";
            this.deStart.Name = "deStart";
            // 
            // bdeEnd
            // 
            this.bdeEnd.Caption = "по";
            this.bdeEnd.Edit = this.deEnd;
            this.bdeEnd.Id = 2;
            this.bdeEnd.Name = "bdeEnd";
            this.bdeEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bdeEnd.Width = 69;
            // 
            // deEnd
            // 
            this.deEnd.AutoHeight = false;
            this.deEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEnd.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.DisplayFormat.FormatString = "g";
            this.deEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.EditFormat.FormatString = "g";
            this.deEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Mask.EditMask = "g";
            this.deEnd.Name = "deEnd";
            // 
            // bleCulture
            // 
            this.bleCulture.Caption = "Культура";
            this.bleCulture.Edit = this.leCulture;
            this.bleCulture.Id = 3;
            this.bleCulture.Name = "bleCulture";
            this.bleCulture.Width = 150;
            // 
            // leCulture
            // 
            this.leCulture.AutoHeight = false;
            this.leCulture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCulture.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leCulture.DisplayMember = "Name";
            this.leCulture.Name = "leCulture";
            this.leCulture.NullText = "";
            this.leCulture.ValueMember = "Id";
            // 
            // bleFieldGroups
            // 
            this.bleFieldGroups.Caption = "Группа полей";
            this.bleFieldGroups.Edit = this.leFieldGroups;
            this.bleFieldGroups.Id = 4;
            this.bleFieldGroups.Name = "bleFieldGroups";
            this.bleFieldGroups.Width = 150;
            this.bleFieldGroups.EditValueChanged += new System.EventHandler(this.bleFieldGroups_EditValueChanged);
            // 
            // leFieldGroups
            // 
            this.leFieldGroups.AutoHeight = false;
            this.leFieldGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leFieldGroups.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leFieldGroups.DisplayMember = "Name";
            this.leFieldGroups.Name = "leFieldGroups";
            this.leFieldGroups.NullText = "";
            this.leFieldGroups.ValueMember = "Id";
            // 
            // bleField
            // 
            this.bleField.Caption = "Поле";
            this.bleField.Edit = this.leField;
            this.bleField.Id = 5;
            this.bleField.Name = "bleField";
            this.bleField.Width = 150;
            // 
            // leField
            // 
            this.leField.AutoHeight = false;
            this.leField.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leField.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leField.DisplayMember = "Name";
            this.leField.Name = "leField";
            this.leField.NullText = "";
            this.leField.ValueMember = "Id";
            // 
            // bbiFilterClear
            // 
            this.bbiFilterClear.Caption = "Очистить фильтр";
            this.bbiFilterClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFilterClear.Glyph")));
            this.bbiFilterClear.Id = 6;
            this.bbiFilterClear.Name = "bbiFilterClear";
            this.bbiFilterClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFilterClear_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1037, 46);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 514);
            this.barDockControlBottom.Size = new System.Drawing.Size(1037, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 46);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1037, 46);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // gcSet
            // 
            this.gcSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSet.Location = new System.Drawing.Point(0, 46);
            this.gcSet.MainView = this.gvSet;
            this.gcSet.MenuManager = this.barManager1;
            this.gcSet.Name = "gcSet";
            this.gcSet.Size = new System.Drawing.Size(1037, 468);
            this.gcSet.TabIndex = 4;
            this.gcSet.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSet});
            // 
            // gvSet
            // 
            this.gvSet.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvSet.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvSet.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvSet.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSet.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSet.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvSet.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvSet.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvSet.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvSet.Appearance.Empty.Options.UseBackColor = true;
            this.gvSet.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSet.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSet.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvSet.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvSet.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSet.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSet.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvSet.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvSet.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvSet.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSet.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvSet.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvSet.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvSet.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvSet.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvSet.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvSet.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvSet.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvSet.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvSet.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvSet.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvSet.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvSet.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvSet.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvSet.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSet.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSet.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvSet.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvSet.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvSet.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvSet.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvSet.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvSet.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSet.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvSet.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvSet.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvSet.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvSet.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvSet.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvSet.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvSet.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSet.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSet.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvSet.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvSet.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvSet.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSet.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSet.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.OddRow.Options.UseBackColor = true;
            this.gvSet.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.OddRow.Options.UseForeColor = true;
            this.gvSet.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvSet.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvSet.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvSet.Appearance.Preview.Options.UseBackColor = true;
            this.gvSet.Appearance.Preview.Options.UseFont = true;
            this.gvSet.Appearance.Preview.Options.UseForeColor = true;
            this.gvSet.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSet.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.Row.Options.UseBackColor = true;
            this.gvSet.Appearance.Row.Options.UseForeColor = true;
            this.gvSet.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSet.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvSet.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvSet.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvSet.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSet.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSet.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvSet.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvSet.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvSet.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvSet.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvSet.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSet.Appearance.VertLine.Options.UseBackColor = true;
            this.gvSet.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colDate,
            this.colCultureName,
            this.colDateStart,
            this.colDateEnd,
            this.colFieldGroupName,
            this.colFieldName,
            this.colOpersQty,
            this.colComment});
            this.gvSet.GridControl = this.gcSet;
            this.gvSet.Name = "gvSet";
            this.gvSet.OptionsSelection.MultiSelect = true;
            this.gvSet.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSet.OptionsView.EnableAppearanceOddRow = true;
            this.gvSet.DoubleClick += new System.EventHandler(this.gvSet_DoubleClick);
            // 
            // colId
            // 
            this.colId.AppearanceCell.Options.UseTextOptions = true;
            this.colId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Номер";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 76;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.Caption = "Дата";
            this.colDate.DisplayFormat.FormatString = "g";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "DateInit";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            this.colDate.Width = 85;
            // 
            // colCultureName
            // 
            this.colCultureName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCultureName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCultureName.Caption = "Культура";
            this.colCultureName.FieldName = "CultureName";
            this.colCultureName.Name = "colCultureName";
            this.colCultureName.OptionsColumn.AllowEdit = false;
            this.colCultureName.OptionsColumn.ReadOnly = true;
            this.colCultureName.Visible = true;
            this.colCultureName.VisibleIndex = 2;
            this.colCultureName.Width = 116;
            // 
            // colDateStart
            // 
            this.colDateStart.AppearanceCell.Options.UseTextOptions = true;
            this.colDateStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStart.Caption = "Старт сезона";
            this.colDateStart.FieldName = "DateStart";
            this.colDateStart.Name = "colDateStart";
            this.colDateStart.OptionsColumn.AllowEdit = false;
            this.colDateStart.OptionsColumn.ReadOnly = true;
            this.colDateStart.Visible = true;
            this.colDateStart.VisibleIndex = 3;
            this.colDateStart.Width = 107;
            // 
            // colDateEnd
            // 
            this.colDateEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colDateEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEnd.Caption = "Окончание сезона";
            this.colDateEnd.FieldName = "DateEnd";
            this.colDateEnd.Name = "colDateEnd";
            this.colDateEnd.OptionsColumn.AllowEdit = false;
            this.colDateEnd.OptionsColumn.ReadOnly = true;
            this.colDateEnd.Visible = true;
            this.colDateEnd.VisibleIndex = 4;
            this.colDateEnd.Width = 107;
            // 
            // colFieldGroupName
            // 
            this.colFieldGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldGroupName.Caption = "Группа полей";
            this.colFieldGroupName.FieldName = "FieldGroupName";
            this.colFieldGroupName.Name = "colFieldGroupName";
            this.colFieldGroupName.OptionsColumn.AllowEdit = false;
            this.colFieldGroupName.OptionsColumn.ReadOnly = true;
            this.colFieldGroupName.Visible = true;
            this.colFieldGroupName.VisibleIndex = 5;
            this.colFieldGroupName.Width = 107;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "Поле";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 6;
            this.colFieldName.Width = 224;
            // 
            // colOpersQty
            // 
            this.colOpersQty.AppearanceCell.Options.UseTextOptions = true;
            this.colOpersQty.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOpersQty.AppearanceHeader.Options.UseTextOptions = true;
            this.colOpersQty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOpersQty.Caption = "Операций";
            this.colOpersQty.FieldName = "OpersQty";
            this.colOpersQty.Name = "colOpersQty";
            this.colOpersQty.OptionsColumn.AllowEdit = false;
            this.colOpersQty.OptionsColumn.ReadOnly = true;
            this.colOpersQty.Visible = true;
            this.colOpersQty.VisibleIndex = 7;
            this.colOpersQty.Width = 72;
            // 
            // colComment
            // 
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.Caption = "Примечание";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 8;
            this.colComment.Width = 258;
            // 
            // FieldsTcSetView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcSet);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FieldsTcSetView";
            this.Size = new System.Drawing.Size(1037, 514);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraBars.BarEditItem bdeStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStart;
        private DevExpress.XtraBars.BarEditItem bdeEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEnd;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.GridControl gcSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSet;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCultureName;
        private DevExpress.XtraBars.BarEditItem bleMobitelGrn;
        private DevExpress.XtraBars.BarEditItem bleGroupe;
        private DevExpress.XtraBars.BarEditItem bleCulture;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCulture;
        private DevExpress.XtraBars.BarEditItem bleFieldGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leFieldGroups;
        private DevExpress.XtraBars.BarEditItem bleField;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leField;
        private DevExpress.XtraGrid.Columns.GridColumn colDateStart;
        private DevExpress.XtraGrid.Columns.GridColumn colDateEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colOpersQty;
        private DevExpress.XtraBars.BarButtonItem bbiFilterClear;
    }
}
