﻿using System;
using System.Data;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites;

namespace Agro
{
    public static class FieldsTcSetController
    {
        public static DataTable GetSet(DateTime start, DateTime end, int idCulture =0 , int idFieldGroup =0 , int idField =0)
        {
            DataTable dt = new DataTable(); 
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sql = AgroQuery.FieldsTcSetController.SelectFieldsTcSet(driverDb.ParamPrefics, idCulture, idFieldGroup, idField);

                TUtil.SetTimeParams(start, end, driverDb);
                dt = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
            }
            return dt;
        }
    }
}
