﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms; 
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using DevExpress.XtraEditors;
using System.ComponentModel;

namespace Agro
{
    public class FieldsTcController : DocItem, IDocument, IDisposable
    {

        private FieldsTcEntity _entity;

        public FieldsTcController()
        {

        }

        public FieldsTcController(FieldsTcEntity entity)
        {
            _entity = entity;
        }

        public override int AddDoc()
        {
            if (TestDemo() && !IsFieldsTcExistForSeasonField())
            {
                    using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.FieldsTcController.InsertIntoAgroFieldsTc, driverDb.ParamPrefics);
                    CreateParameters(driverDb);
                    _entity.ID = driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_fieldseason_tc");
                    if (_entity!= null && _entity.ID > 0)
                        UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, Resources.LogCreate, _entity.ID);
                    _entity.Operations = FieldTechOpersController.GetList(_entity);
                }
            }
            return _ID;
        }

        public override bool  UpdateDoc()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sSQL = string.Format(AgroQuery.FieldsTcController.UpdateAgroFieldsTc, driverDb.ParamPrefics,
                    _entity.ID);
                CreateParameters(driverDb);
                driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, Resources.LogUpdateHeader, _entity.ID);
                return true;
            }
        }

        public override bool DeleteDoc(bool bQuestionNeed)
        {
            if (DeleteDocTest() && DeleteDocContent())
            {
                if (bQuestionNeed)
                {
                    if (DialogResult.No ==
                        XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo))
                        return false;
                }


                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.FieldsTcController.DeleteFromAgroFieldsTc, _ID);
                    driverDb.ExecuteNonQueryCommand(sSQL);
                    UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, Resources.LogDelete, _ID);
                }
                return true;
            }
            else
                return false;
        }

        public override bool DeleteDocContent()
        {
            try
            {
                GetDocById(_ID);
                if (_entity != null && _entity.Operations != null)
                foreach (FieldTechOperation operation in _entity.Operations  )
                    {
                        operation.DeleteOrderTLinks();
                        operation.Delete(); 
                    }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool DeleteDocTest()
        {
            return true;
        }

        public override bool TestDemo()
        {
            if (GlobalVars.g_AGRO == (int)Consts.RegimeType.Demo)
            {
                int cnt = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();

                    Sql = AgroQuery.FieldsTcController.SelectCountAgroFieldsTc;

                    cnt = driverDb.GetScalarValueNull<Int32>(Sql, 0);
                }
                if (cnt >= Consts.AGRO_LIMIT)
                {
                    XtraMessageBox.Show(Resources.AgroLimit_1 + " " + Consts.AGRO_LIMIT.ToString() + "!",
                        Resources.ApplicationName);
                    _ID = 0;
                    return false;
                }
            }
            return true;
        }

        System.Data.DataTable IDocument.GetContent()
        {
            throw new NotImplementedException();
        }

        bool IDocument.DeleteSelectedFromGrid(DevExpress.XtraGrid.Views.Grid.GridView gv)
        {
            throw new NotImplementedException();
        }

        void IDocument.UpdateDocTotals()
        {
            throw new NotImplementedException();
        }

        public override bool GetDocById(int id)
        {
            try
            {
                Sql = string.Format(AgroQuery.FieldsTcController.SelectAgroFieldsTc, id);

                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(Sql);
                    if (driverDb.Read())
                    {
                        _entity = new FieldsTcEntity();
                        _entity.ID = driverDb.GetInt32("Id");

                        _entity.Date = driverDb.GetDateTime("DateInit");
                        _entity.DateStart = driverDb.GetDateTime("DateStart");
                        _entity.DateEnd = driverDb.GetDateTime("DateEnd"); 

                        _entity.Remark = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                        _entity.Season = new DictionaryAgroSeason(driverDb.GetInt32("IdSeason"));
                        _entity.Season.Dispose(); 
                        _entity.Field = new DictionaryAgroField(driverDb.GetInt32("IdField"));
                        _entity.Field.Dispose(); 
                        _entity.FieldGroup = new DictionaryAgroFieldGrp(_entity.Field.IdMain);
                        _entity.FieldGroup.Dispose(); 
                        _entity.UserCreated = driverDb.GetString("UserCreated");
                        _entity.Operations = FieldTechOpersController.GetList(_entity);
                    }
                    driverDb.CloseDataReader();
                    driverDb.CloseDbConnection(); 
                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.Message + "\n" + ex.StackTrace, "FieldsTcController.GetDocById(int id):188" );
                return false;
            }
        }

        void IDisposable.Dispose()
        {

        }

        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(7);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateInit", _entity.Date);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", _entity.DateStart);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", _entity.DateEnd);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdField", _entity.Field.Id);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdSeason", _entity.Season.Id);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", _entity.Remark ?? "");
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserCreated", _entity.UserCreated);
        }

        public bool IsFieldsTcExistForSeasonField(bool bViewInfo = true)
        {
            int idFieldsTc = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.FieldsTcController.SelectAgroFieldsTcId, _entity.Season.Id  ,
                    _entity.Field.Id , _entity.ID);
                idFieldsTc = driverDb.GetScalarValueNull<Int32>(Sql, 0);
            }
            if (idFieldsTc > 0)
            {
                if (bViewInfo)
                {
                    XtraMessageBox.Show(string.Format(Resources.FieldsTcExistForSeason, _entity.Season.Name),
                        Resources.ApplicationName);
                }
                return true;
            }
            return false;
        }

        public static void SetHeaderDefaultsValues(FieldsTcEntity newEntity)
        {
            newEntity.Date = DateTime.Now;
            newEntity.UserCreated = UserBaseCurrent.Instance.Name;
        }

        internal void AddNewFromView()
        {
            FieldsTcEntity newEntity = new FieldsTcEntity();
            SetHeaderDefaultsValues(newEntity);
            ViewForm(newEntity);

        }

        private void ViewForm(FieldsTcEntity entity)
        {
            using (FieldsTcForm view = new FieldsTcForm())
            {
                LoadFormControls(view);
                view.pgProperties.SelectedObject = entity;
                if (entity.Field != null) DrawZone(view, entity);
                view.gcTechOpers.DataSource = entity.Operations;
                view.ShowDialog();
            }
        }

        internal void ViewForm(int idDoc)
        {
            if (GetDocById(idDoc))
            {
                ViewForm (_entity);
            }

        }

        void LoadFormControls(FieldsTcForm view)
        {
            DictionaryAgroFieldGrp.SetComboBoxDataSource(view.cbeFeildGroups);
            DictionaryAgroSeason.SetComboBoxDataSource(view.cbeSeason);
            DictionaryAgroWorkType.SetComboBoxDataSource(view.cbeWorkType);
        }

        internal bool ValidateHeader()
        {
            if (_entity.Season == null || _entity.Field == null)
                return false;
            else
                return true;
        }

        internal void DrawZone(FieldsTcForm view, FieldsTcEntity entity)
        {
            int idZone = entity.Field.IdZone;
            if (idZone > 0)
            {
                IZone zone = DictionaryAgroField.ZonesModel.GetById(idZone);
                if (zone != null) view.zoneProperty.SetZone(DictionaryAgroField.ZonesModel.GetById(idZone));
            }
        }

        internal bool SetFact()
        {
            if (_entity == null || _entity.Operations == null) return false;
           foreach (FieldTechOperation record in _entity.Operations)
            {
                record.DeleteOrderTLinks();
                record.SaveOrderTLinks();
                record.FactRecords = TechOpersFactController.GetList(record);
                record.RecalcSquare(); 
            }
            return true;
        }



    }
}
