﻿using Agro.Dictionaries;
using System;
using TrackControl.General;
using System.Collections.Generic; 

namespace Agro
{
    /// <summary>
    /// технологическая карта на поле (поле + сезон)
    /// </summary>
    public class FieldsTcEntity:IDocumentEntity
    {

        public int ID { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Remark { get; set; }
        public string UserCreated { get; set; }
        public DictionaryAgroSeason Season { get; set; }
        public DictionaryAgroField Field { get; set; }
        public DictionaryAgroFieldGrp FieldGroup { get; set; }
        public IList<FieldTechOperation> Operations { get; set; } 
        public FieldsTcEntity()
        {
           
        }

    }
}
