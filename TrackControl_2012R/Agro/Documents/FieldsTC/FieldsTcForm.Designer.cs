﻿namespace Agro
{
    partial class FieldsTcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FieldsTcForm));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFact = new DevExpress.XtraBars.BarButtonItem();
            this.bbiJointProcess = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLog = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.pgProperties = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.cbeFeildGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbeField = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbeSeason = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.category = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateInit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erFieldGroup = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erField = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSeason = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erComment = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erUserCreated = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.zoneProperty = new TrackControl.Zones.Tuning.ZoneProperty();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tabCtrl = new DevExpress.XtraTab.XtraTabControl();
            this.tpTechOpers = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcTechOpers = new DevExpress.XtraGrid.GridControl();
            this.gvTechOpers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTOid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOwork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbeWorkType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colTODateStartPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTODateEndPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOWorkQtyPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTODateStartFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTODateEndFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOSquareGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOSquarePercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOClosed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcOrder = new DevExpress.XtraGrid.GridControl();
            this.gvOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOdId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregateName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSquareFactGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJointProcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSquareAfterRecalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserRecalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpMap = new DevExpress.XtraTab.XtraTabPage();
            this.pnMap = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeFeildGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeSeason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabCtrl)).BeginInit();
            this.tabCtrl.SuspendLayout();
            this.tpTechOpers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTechOpers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTechOpers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeWorkType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).BeginInit();
            this.tpMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiRefresh,
            this.bbiLog,
            this.bbiFact,
            this.bbiJointProcess});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 5;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Главное меню";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiJointProcess, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiLog, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Главное меню";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Сохранить";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Обновить";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiFact
            // 
            this.bbiFact.Caption = "Факт";
            this.bbiFact.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFact.Glyph")));
            this.bbiFact.Id = 3;
            this.bbiFact.Name = "bbiFact";
            this.bbiFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFact_ItemClick);
            // 
            // bbiJointProcess
            // 
            this.bbiJointProcess.Caption = "Совместная обработка";
            this.bbiJointProcess.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiJointProcess.Glyph")));
            this.bbiJointProcess.Id = 4;
            this.bbiJointProcess.Name = "bbiJointProcess";
            this.bbiJointProcess.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiJointProcess_ItemClick);
            // 
            // bbiLog
            // 
            this.bbiLog.Caption = "Журнал событий";
            this.bbiLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLog.Glyph")));
            this.bbiLog.Id = 2;
            this.bbiLog.Name = "bbiLog";
            this.bbiLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLog_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Строка состояния";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Строка состояния";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1189, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 722);
            this.barDockControlBottom.Size = new System.Drawing.Size(1189, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 698);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1189, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 698);
            // 
            // pgProperties
            // 
            this.pgProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgProperties.Location = new System.Drawing.Point(0, 0);
            this.pgProperties.Name = "pgProperties";
            this.pgProperties.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgProperties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cbeFeildGroups,
            this.cbeField,
            this.cbeSeason});
            this.pgProperties.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.category});
            this.pgProperties.Size = new System.Drawing.Size(572, 202);
            this.pgProperties.TabIndex = 4;
            this.pgProperties.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgProperties_CellValueChanged);
            // 
            // cbeFeildGroups
            // 
            this.cbeFeildGroups.AutoHeight = false;
            this.cbeFeildGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeFeildGroups.DropDownRows = 20;
            this.cbeFeildGroups.Name = "cbeFeildGroups";
            this.cbeFeildGroups.Sorted = true;
            // 
            // cbeField
            // 
            this.cbeField.AutoHeight = false;
            this.cbeField.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeField.DropDownRows = 20;
            this.cbeField.Name = "cbeField";
            this.cbeField.Sorted = true;
            // 
            // cbeSeason
            // 
            this.cbeSeason.AutoHeight = false;
            this.cbeSeason.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeSeason.Name = "cbeSeason";
            // 
            // category
            // 
            this.category.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erId,
            this.erDateInit,
            this.erFieldGroup,
            this.erField,
            this.erSeason,
            this.erComment,
            this.erUserCreated,
            this.erDateStart,
            this.erDateEnd});
            this.category.Name = "category";
            this.category.Properties.Caption = "Свойства";
            // 
            // erId
            // 
            this.erId.Name = "erId";
            this.erId.Properties.Caption = "Номер";
            this.erId.Properties.FieldName = "ID";
            this.erId.Properties.ReadOnly = true;
            // 
            // erDateInit
            // 
            this.erDateInit.Name = "erDateInit";
            this.erDateInit.Properties.Caption = "Дата создания";
            this.erDateInit.Properties.FieldName = "Date";
            this.erDateInit.Properties.ReadOnly = true;
            // 
            // erFieldGroup
            // 
            this.erFieldGroup.Name = "erFieldGroup";
            this.erFieldGroup.Properties.Caption = "Группа полей";
            this.erFieldGroup.Properties.FieldName = "FieldGroup";
            this.erFieldGroup.Properties.RowEdit = this.cbeFeildGroups;
            // 
            // erField
            // 
            this.erField.Name = "erField";
            this.erField.Properties.Caption = "Поле";
            this.erField.Properties.FieldName = "Field";
            this.erField.Properties.RowEdit = this.cbeField;
            // 
            // erSeason
            // 
            this.erSeason.Height = 18;
            this.erSeason.Name = "erSeason";
            this.erSeason.Properties.Caption = "Сезон";
            this.erSeason.Properties.FieldName = "Season";
            this.erSeason.Properties.RowEdit = this.cbeSeason;
            // 
            // erComment
            // 
            this.erComment.Name = "erComment";
            this.erComment.Properties.Caption = "Примечание";
            this.erComment.Properties.FieldName = "Remark";
            // 
            // erUserCreated
            // 
            this.erUserCreated.Name = "erUserCreated";
            this.erUserCreated.Properties.Caption = "Создатель";
            this.erUserCreated.Properties.FieldName = "UserCreated";
            this.erUserCreated.Properties.ReadOnly = true;
            // 
            // erDateStart
            // 
            this.erDateStart.Name = "erDateStart";
            this.erDateStart.Properties.Caption = "Дата начала";
            this.erDateStart.Properties.FieldName = "DateStart";
            // 
            // erDateEnd
            // 
            this.erDateEnd.Name = "erDateEnd";
            this.erDateEnd.Properties.Caption = "Дата окончания";
            this.erDateEnd.Properties.FieldName = "DateEnd";
            // 
            // zoneProperty
            // 
            this.zoneProperty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoneProperty.Location = new System.Drawing.Point(0, 0);
            this.zoneProperty.Name = "zoneProperty";
            this.zoneProperty.Size = new System.Drawing.Size(612, 202);
            this.zoneProperty.TabIndex = 25;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 24);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.tabCtrl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1189, 698);
            this.splitContainerControl1.SplitterPosition = 202;
            this.splitContainerControl1.TabIndex = 30;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pgProperties);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.zoneProperty);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1189, 202);
            this.splitContainerControl2.SplitterPosition = 572;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // tabCtrl
            // 
            this.tabCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrl.Location = new System.Drawing.Point(0, 0);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedTabPage = this.tpTechOpers;
            this.tabCtrl.Size = new System.Drawing.Size(1189, 491);
            this.tabCtrl.TabIndex = 0;
            this.tabCtrl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpTechOpers,
            this.tpMap});
            this.tabCtrl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabCtrl_SelectedPageChanged);
            // 
            // tpTechOpers
            // 
            this.tpTechOpers.Controls.Add(this.splitContainerControl3);
            this.tpTechOpers.Name = "tpTechOpers";
            this.tpTechOpers.Size = new System.Drawing.Size(1183, 463);
            this.tpTechOpers.Text = "Технологические операции";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gcTechOpers);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.gcOrder);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1183, 463);
            this.splitContainerControl3.SplitterPosition = 233;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gcTechOpers
            // 
            this.gcTechOpers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTechOpers.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcTechOpers_EmbeddedNavigator_ButtonClick);
            this.gcTechOpers.Location = new System.Drawing.Point(0, 0);
            this.gcTechOpers.MainView = this.gvTechOpers;
            this.gcTechOpers.MenuManager = this.barManager1;
            this.gcTechOpers.Name = "gcTechOpers";
            this.gcTechOpers.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cbeWorkType});
            this.gcTechOpers.Size = new System.Drawing.Size(1183, 233);
            this.gcTechOpers.TabIndex = 0;
            this.gcTechOpers.UseEmbeddedNavigator = true;
            this.gcTechOpers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTechOpers});
            // 
            // gvTechOpers
            // 
            this.gvTechOpers.ColumnPanelRowHeight = 40;
            this.gvTechOpers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTOid,
            this.colTOwork,
            this.colTODateStartPlan,
            this.colTODateEndPlan,
            this.colTOWorkQtyPlan,
            this.colTODateStartFact,
            this.colTODateEndFact,
            this.colTOSquareGa,
            this.colTOSquarePercent,
            this.colTOClosed,
            this.colTOComment});
            this.gvTechOpers.GridControl = this.gcTechOpers;
            this.gvTechOpers.IndicatorWidth = 20;
            this.gvTechOpers.Name = "gvTechOpers";
            this.gvTechOpers.OptionsView.ShowGroupPanel = false;
            this.gvTechOpers.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvTechOpers_CustomDrawRowIndicator);
            this.gvTechOpers.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTechOpers_FocusedRowChanged);
            this.gvTechOpers.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvTechOpers_CellValueChanged);
            this.gvTechOpers.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvTechOpers_ValidateRow);
            // 
            // colTOid
            // 
            this.colTOid.AppearanceCell.Options.UseTextOptions = true;
            this.colTOid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOid.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOid.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOid.Caption = "Номер";
            this.colTOid.FieldName = "Id";
            this.colTOid.Name = "colTOid";
            this.colTOid.OptionsColumn.AllowEdit = false;
            this.colTOid.OptionsColumn.ReadOnly = true;
            this.colTOid.Visible = true;
            this.colTOid.VisibleIndex = 0;
            this.colTOid.Width = 62;
            // 
            // colTOwork
            // 
            this.colTOwork.AppearanceCell.Options.UseTextOptions = true;
            this.colTOwork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTOwork.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOwork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOwork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOwork.Caption = "Вид работ";
            this.colTOwork.ColumnEdit = this.cbeWorkType;
            this.colTOwork.FieldName = "WorkType";
            this.colTOwork.Name = "colTOwork";
            this.colTOwork.Visible = true;
            this.colTOwork.VisibleIndex = 1;
            this.colTOwork.Width = 184;
            // 
            // cbeWorkType
            // 
            this.cbeWorkType.AutoHeight = false;
            this.cbeWorkType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeWorkType.DropDownRows = 20;
            this.cbeWorkType.Name = "cbeWorkType";
            // 
            // colTODateStartPlan
            // 
            this.colTODateStartPlan.AppearanceCell.Options.UseTextOptions = true;
            this.colTODateStartPlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateStartPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colTODateStartPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateStartPlan.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTODateStartPlan.Caption = "Дата начала (план)";
            this.colTODateStartPlan.FieldName = "DateStartPlan";
            this.colTODateStartPlan.Name = "colTODateStartPlan";
            this.colTODateStartPlan.Visible = true;
            this.colTODateStartPlan.VisibleIndex = 2;
            this.colTODateStartPlan.Width = 94;
            // 
            // colTODateEndPlan
            // 
            this.colTODateEndPlan.AppearanceCell.Options.UseTextOptions = true;
            this.colTODateEndPlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateEndPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colTODateEndPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateEndPlan.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTODateEndPlan.Caption = "Дата окончания (план)";
            this.colTODateEndPlan.FieldName = "DateEndPlan";
            this.colTODateEndPlan.Name = "colTODateEndPlan";
            this.colTODateEndPlan.Visible = true;
            this.colTODateEndPlan.VisibleIndex = 3;
            this.colTODateEndPlan.Width = 110;
            // 
            // colTOWorkQtyPlan
            // 
            this.colTOWorkQtyPlan.AppearanceCell.Options.UseTextOptions = true;
            this.colTOWorkQtyPlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOWorkQtyPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOWorkQtyPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOWorkQtyPlan.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOWorkQtyPlan.Caption = "Кол-во часов (план)";
            this.colTOWorkQtyPlan.FieldName = "WorkQtyPlan";
            this.colTOWorkQtyPlan.Name = "colTOWorkQtyPlan";
            this.colTOWorkQtyPlan.Visible = true;
            this.colTOWorkQtyPlan.VisibleIndex = 4;
            this.colTOWorkQtyPlan.Width = 93;
            // 
            // colTODateStartFact
            // 
            this.colTODateStartFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTODateStartFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateStartFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTODateStartFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateStartFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTODateStartFact.Caption = "Дата начала (факт)";
            this.colTODateStartFact.FieldName = "DateStartFact";
            this.colTODateStartFact.Name = "colTODateStartFact";
            this.colTODateStartFact.OptionsColumn.AllowEdit = false;
            this.colTODateStartFact.OptionsColumn.ReadOnly = true;
            this.colTODateStartFact.Visible = true;
            this.colTODateStartFact.VisibleIndex = 5;
            this.colTODateStartFact.Width = 94;
            // 
            // colTODateEndFact
            // 
            this.colTODateEndFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTODateEndFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateEndFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTODateEndFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTODateEndFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTODateEndFact.Caption = "Дата окончания (факт)";
            this.colTODateEndFact.FieldName = "DateEndFact";
            this.colTODateEndFact.Name = "colTODateEndFact";
            this.colTODateEndFact.OptionsColumn.AllowEdit = false;
            this.colTODateEndFact.OptionsColumn.ReadOnly = true;
            this.colTODateEndFact.Visible = true;
            this.colTODateEndFact.VisibleIndex = 6;
            this.colTODateEndFact.Width = 103;
            // 
            // colTOSquareGa
            // 
            this.colTOSquareGa.AppearanceCell.Options.UseTextOptions = true;
            this.colTOSquareGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquareGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOSquareGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquareGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOSquareGa.Caption = "Обраб. площадь, га (факт)";
            this.colTOSquareGa.FieldName = "SquareGa";
            this.colTOSquareGa.Name = "colTOSquareGa";
            this.colTOSquareGa.OptionsColumn.AllowEdit = false;
            this.colTOSquareGa.OptionsColumn.ReadOnly = true;
            this.colTOSquareGa.Visible = true;
            this.colTOSquareGa.VisibleIndex = 7;
            this.colTOSquareGa.Width = 109;
            // 
            // colTOSquarePercent
            // 
            this.colTOSquarePercent.AppearanceCell.Options.UseTextOptions = true;
            this.colTOSquarePercent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquarePercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOSquarePercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquarePercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOSquarePercent.Caption = "Обраб. площадь, % (факт)";
            this.colTOSquarePercent.FieldName = "SquarePercent";
            this.colTOSquarePercent.Name = "colTOSquarePercent";
            this.colTOSquarePercent.OptionsColumn.AllowEdit = false;
            this.colTOSquarePercent.OptionsColumn.ReadOnly = true;
            this.colTOSquarePercent.Visible = true;
            this.colTOSquarePercent.VisibleIndex = 8;
            this.colTOSquarePercent.Width = 118;
            // 
            // colTOClosed
            // 
            this.colTOClosed.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOClosed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOClosed.Caption = "Закрыта";
            this.colTOClosed.FieldName = "IsClosed";
            this.colTOClosed.Name = "colTOClosed";
            this.colTOClosed.Visible = true;
            this.colTOClosed.VisibleIndex = 9;
            // 
            // colTOComment
            // 
            this.colTOComment.AppearanceCell.Options.UseTextOptions = true;
            this.colTOComment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTOComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOComment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOComment.Caption = "Примечание";
            this.colTOComment.FieldName = "Comment";
            this.colTOComment.Name = "colTOComment";
            this.colTOComment.Visible = true;
            this.colTOComment.VisibleIndex = 10;
            this.colTOComment.Width = 185;
            // 
            // gcOrder
            // 
            this.gcOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrder.Location = new System.Drawing.Point(0, 0);
            this.gcOrder.MainView = this.gvOrder;
            this.gcOrder.MenuManager = this.barManager1;
            this.gcOrder.Name = "gcOrder";
            this.gcOrder.Size = new System.Drawing.Size(1183, 225);
            this.gcOrder.TabIndex = 0;
            this.gcOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOrder});
            // 
            // gvOrder
            // 
            this.gvOrder.ColumnPanelRowHeight = 40;
            this.gvOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOdId,
            this.colNumberOrder,
            this.colDateOrder,
            this.colVehicleName,
            this.colDriverName,
            this.colAgregateName,
            this.colSquareFactGa,
            this.colJointProcess,
            this.colSquareAfterRecalc,
            this.colDateRecalc,
            this.colUserRecalc});
            this.gvOrder.GridControl = this.gcOrder;
            this.gvOrder.IndicatorWidth = 20;
            this.gvOrder.Name = "gvOrder";
            this.gvOrder.OptionsDetail.EnableMasterViewMode = false;
            this.gvOrder.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvOrder_CustomDrawRowIndicator);
            // 
            // colOdId
            // 
            this.colOdId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOdId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOdId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOdId.Caption = "Id";
            this.colOdId.FieldName = "Id";
            this.colOdId.Name = "colOdId";
            this.colOdId.OptionsColumn.AllowEdit = false;
            this.colOdId.OptionsColumn.ReadOnly = true;
            // 
            // colNumberOrder
            // 
            this.colNumberOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberOrder.Caption = "Номер";
            this.colNumberOrder.FieldName = "NumberOrder";
            this.colNumberOrder.Name = "colNumberOrder";
            this.colNumberOrder.OptionsColumn.AllowEdit = false;
            this.colNumberOrder.OptionsColumn.ReadOnly = true;
            this.colNumberOrder.Visible = true;
            this.colNumberOrder.VisibleIndex = 0;
            this.colNumberOrder.Width = 115;
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOrder.Caption = "Дата";
            this.colDateOrder.FieldName = "DateOrder";
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 1;
            this.colDateOrder.Width = 115;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "Транспорт";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 2;
            this.colVehicleName.Width = 115;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.Caption = "Водитель";
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 3;
            this.colDriverName.Width = 115;
            // 
            // colAgregateName
            // 
            this.colAgregateName.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregateName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregateName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregateName.Caption = "Агрегат";
            this.colAgregateName.FieldName = "AgregateName";
            this.colAgregateName.Name = "colAgregateName";
            this.colAgregateName.OptionsColumn.AllowEdit = false;
            this.colAgregateName.OptionsColumn.ReadOnly = true;
            this.colAgregateName.Visible = true;
            this.colAgregateName.VisibleIndex = 4;
            this.colAgregateName.Width = 115;
            // 
            // colSquareFactGa
            // 
            this.colSquareFactGa.AppearanceCell.Options.UseTextOptions = true;
            this.colSquareFactGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareFactGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colSquareFactGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareFactGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSquareFactGa.Caption = "Факт обработанная площадь, га";
            this.colSquareFactGa.FieldName = "SquareFactGa";
            this.colSquareFactGa.Name = "colSquareFactGa";
            this.colSquareFactGa.OptionsColumn.AllowEdit = false;
            this.colSquareFactGa.OptionsColumn.ReadOnly = true;
            this.colSquareFactGa.Visible = true;
            this.colSquareFactGa.VisibleIndex = 5;
            this.colSquareFactGa.Width = 115;
            // 
            // colJointProcess
            // 
            this.colJointProcess.AppearanceCell.Options.UseTextOptions = true;
            this.colJointProcess.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJointProcess.AppearanceHeader.Options.UseTextOptions = true;
            this.colJointProcess.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJointProcess.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colJointProcess.Caption = "Совместная обработка";
            this.colJointProcess.FieldName = "JointProcess";
            this.colJointProcess.Name = "colJointProcess";
            this.colJointProcess.Visible = true;
            this.colJointProcess.VisibleIndex = 6;
            this.colJointProcess.Width = 115;
            // 
            // colSquareAfterRecalc
            // 
            this.colSquareAfterRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.colSquareAfterRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareAfterRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colSquareAfterRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareAfterRecalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSquareAfterRecalc.Caption = "Факт обработанная площадь, га (совместно)";
            this.colSquareAfterRecalc.FieldName = "SquareAfterRecalc";
            this.colSquareAfterRecalc.Name = "colSquareAfterRecalc";
            this.colSquareAfterRecalc.OptionsColumn.AllowEdit = false;
            this.colSquareAfterRecalc.OptionsColumn.ReadOnly = true;
            this.colSquareAfterRecalc.Visible = true;
            this.colSquareAfterRecalc.VisibleIndex = 7;
            this.colSquareAfterRecalc.Width = 151;
            // 
            // colDateRecalc
            // 
            this.colDateRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.colDateRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateRecalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateRecalc.Caption = "Дата пересчета";
            this.colDateRecalc.FieldName = "DateRecalc";
            this.colDateRecalc.Name = "colDateRecalc";
            this.colDateRecalc.OptionsColumn.AllowEdit = false;
            this.colDateRecalc.OptionsColumn.ReadOnly = true;
            this.colDateRecalc.Visible = true;
            this.colDateRecalc.VisibleIndex = 8;
            this.colDateRecalc.Width = 97;
            // 
            // colUserRecalc
            // 
            this.colUserRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.colUserRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colUserRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserRecalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserRecalc.Caption = "Диспетчер";
            this.colUserRecalc.FieldName = "UserRecalc";
            this.colUserRecalc.Name = "colUserRecalc";
            this.colUserRecalc.OptionsColumn.AllowEdit = false;
            this.colUserRecalc.OptionsColumn.ReadOnly = true;
            this.colUserRecalc.Visible = true;
            this.colUserRecalc.VisibleIndex = 9;
            this.colUserRecalc.Width = 99;
            // 
            // tpMap
            // 
            this.tpMap.Controls.Add(this.pnMap);
            this.tpMap.Name = "tpMap";
            this.tpMap.Size = new System.Drawing.Size(1183, 463);
            this.tpMap.Text = "Карта";
            // 
            // pnMap
            // 
            this.pnMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMap.Location = new System.Drawing.Point(0, 0);
            this.pnMap.Name = "pnMap";
            this.pnMap.Size = new System.Drawing.Size(1183, 463);
            this.pnMap.TabIndex = 8;
            // 
            // FieldsTcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 745);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FieldsTcForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Технологическая карта поля";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeFeildGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeSeason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabCtrl)).EndInit();
            this.tabCtrl.ResumeLayout(false);
            this.tpTechOpers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTechOpers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTechOpers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeWorkType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).EndInit();
            this.tpMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiLog;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateInit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erFieldGroup;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erField;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSeason;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erComment;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erUserCreated;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow category;
        internal DevExpress.XtraVerticalGrid.PropertyGridControl pgProperties;
        internal DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeFeildGroups;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        internal DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeSeason;
        internal DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeField;
        internal TrackControl.Zones.Tuning.ZoneProperty zoneProperty;
        private DevExpress.XtraTab.XtraTabControl tabCtrl;
        private DevExpress.XtraTab.XtraTabPage tpTechOpers;
        private DevExpress.XtraTab.XtraTabPage tpMap;
        private DevExpress.XtraGrid.Columns.GridColumn colTOid;
        private DevExpress.XtraGrid.Columns.GridColumn colTOwork;
        private DevExpress.XtraGrid.Columns.GridColumn colTODateStartPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colTODateEndPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colTOWorkQtyPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colTODateStartFact;
        private DevExpress.XtraGrid.Columns.GridColumn colTODateEndFact;
        private DevExpress.XtraGrid.Columns.GridColumn colTOSquareGa;
        private DevExpress.XtraGrid.Columns.GridColumn colTOSquarePercent;
        private DevExpress.XtraGrid.Columns.GridColumn colTOComment;
        internal DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeWorkType;
        internal DevExpress.XtraGrid.GridControl gcTechOpers;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvTechOpers;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gcOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colOdId;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregateName;
        private DevExpress.XtraGrid.Columns.GridColumn colSquareFactGa;
        private DevExpress.XtraGrid.Columns.GridColumn colJointProcess;
        private DevExpress.XtraGrid.Columns.GridColumn colSquareAfterRecalc;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecalc;
        private DevExpress.XtraGrid.Columns.GridColumn colUserRecalc;
        private DevExpress.XtraBars.BarButtonItem bbiFact;
        private DevExpress.XtraBars.BarButtonItem bbiJointProcess;
        private DevExpress.XtraEditors.PanelControl pnMap;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTOClosed;
    }
}