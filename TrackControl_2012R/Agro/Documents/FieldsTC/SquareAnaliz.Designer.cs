namespace Agro
{
    partial class SquareAnaliz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SquareAnaliz));
            this.peSq = new DevExpress.XtraEditors.PictureEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.teSquare = new DevExpress.XtraEditors.TextEdit();
            this.gcOrder = new DevExpress.XtraGrid.GridControl();
            this.gvOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOdId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregateName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSquareFactGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSquareAfterRecalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bsiRules = new DevExpress.XtraBars.BarSubItem();
            this.bchRuleProport = new DevExpress.XtraBars.BarCheckItem();
            this.bchRuleEqually = new DevExpress.XtraBars.BarCheckItem();
            this.bbiRecalc = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup2 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.richRuleProport = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.richRuleEqually = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.peSq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teSquare.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richRuleProport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richRuleEqually)).BeginInit();
            this.SuspendLayout();
            // 
            // peSq
            // 
            this.peSq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peSq.Location = new System.Drawing.Point(0, 0);
            this.peSq.Name = "peSq";
            this.peSq.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peSq.Size = new System.Drawing.Size(944, 389);
            this.peSq.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 31);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.teSquare);
            this.splitContainerControl1.Panel1.Controls.Add(this.peSq);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcOrder);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(944, 580);
            this.splitContainerControl1.SplitterPosition = 389;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // teSquare
            // 
            this.teSquare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.teSquare.EditValue = "0";
            this.teSquare.Location = new System.Drawing.Point(832, 362);
            this.teSquare.Name = "teSquare";
            this.teSquare.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.teSquare.Properties.Appearance.Options.UseFont = true;
            this.teSquare.Properties.Appearance.Options.UseTextOptions = true;
            this.teSquare.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teSquare.Size = new System.Drawing.Size(109, 26);
            this.teSquare.TabIndex = 1;
            // 
            // gcOrder
            // 
            this.gcOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrder.Location = new System.Drawing.Point(0, 0);
            this.gcOrder.MainView = this.gvOrder;
            this.gcOrder.Name = "gcOrder";
            this.gcOrder.Size = new System.Drawing.Size(944, 186);
            this.gcOrder.TabIndex = 1;
            this.gcOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOrder});
            // 
            // gvOrder
            // 
            this.gvOrder.ColumnPanelRowHeight = 40;
            this.gvOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOdId,
            this.colNumberOrder,
            this.colDateOrder,
            this.colVehicleName,
            this.colDriverName,
            this.colAgregateName,
            this.colSquareFactGa,
            this.colSquareAfterRecalc});
            this.gvOrder.GridControl = this.gcOrder;
            this.gvOrder.Name = "gvOrder";
            this.gvOrder.OptionsDetail.EnableMasterViewMode = false;
            this.gvOrder.OptionsDetail.ShowDetailTabs = false;
            this.gvOrder.OptionsDetail.SmartDetailExpand = false;
            this.gvOrder.OptionsView.ShowFooter = true;
            this.gvOrder.OptionsView.ShowGroupPanel = false;
            // 
            // colOdId
            // 
            this.colOdId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOdId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOdId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOdId.Caption = "Id";
            this.colOdId.FieldName = "Id";
            this.colOdId.Name = "colOdId";
            this.colOdId.OptionsColumn.AllowEdit = false;
            this.colOdId.OptionsColumn.ReadOnly = true;
            // 
            // colNumberOrder
            // 
            this.colNumberOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberOrder.Caption = "�����";
            this.colNumberOrder.FieldName = "NumberOrder";
            this.colNumberOrder.Name = "colNumberOrder";
            this.colNumberOrder.OptionsColumn.AllowEdit = false;
            this.colNumberOrder.OptionsColumn.ReadOnly = true;
            this.colNumberOrder.Visible = true;
            this.colNumberOrder.VisibleIndex = 0;
            this.colNumberOrder.Width = 115;
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOrder.Caption = "����";
            this.colDateOrder.FieldName = "DateOrder";
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 1;
            this.colDateOrder.Width = 115;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "���������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 2;
            this.colVehicleName.Width = 115;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.Caption = "��������";
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 3;
            this.colDriverName.Width = 115;
            // 
            // colAgregateName
            // 
            this.colAgregateName.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregateName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregateName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregateName.Caption = "�������";
            this.colAgregateName.FieldName = "AgregateName";
            this.colAgregateName.Name = "colAgregateName";
            this.colAgregateName.OptionsColumn.AllowEdit = false;
            this.colAgregateName.OptionsColumn.ReadOnly = true;
            this.colAgregateName.Visible = true;
            this.colAgregateName.VisibleIndex = 4;
            this.colAgregateName.Width = 115;
            // 
            // colSquareFactGa
            // 
            this.colSquareFactGa.AppearanceCell.Options.UseTextOptions = true;
            this.colSquareFactGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareFactGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colSquareFactGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareFactGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSquareFactGa.Caption = "���� ������������ �������, ��";
            this.colSquareFactGa.FieldName = "SquareFactGa";
            this.colSquareFactGa.Name = "colSquareFactGa";
            this.colSquareFactGa.OptionsColumn.AllowEdit = false;
            this.colSquareFactGa.OptionsColumn.ReadOnly = true;
            this.colSquareFactGa.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colSquareFactGa.Visible = true;
            this.colSquareFactGa.VisibleIndex = 5;
            this.colSquareFactGa.Width = 115;
            // 
            // colSquareAfterRecalc
            // 
            this.colSquareAfterRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.colSquareAfterRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareAfterRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colSquareAfterRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareAfterRecalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSquareAfterRecalc.Caption = "���� ������������ �������, �� (���������)";
            this.colSquareAfterRecalc.FieldName = "SquareAfterRecalc";
            this.colSquareAfterRecalc.Name = "colSquareAfterRecalc";
            this.colSquareAfterRecalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colSquareAfterRecalc.Visible = true;
            this.colSquareAfterRecalc.VisibleIndex = 6;
            this.colSquareAfterRecalc.Width = 151;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bsiRules,
            this.barEditItem1,
            this.barEditItem2,
            this.bchRuleProport,
            this.bchRuleEqually,
            this.bbiRecalc});
            this.barManager1.MaxItemId = 16;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemTextEdit1,
            this.repositoryItemRadioGroup2,
            this.repositoryItemCheckEdit1,
            this.richRuleProport,
            this.richRuleEqually});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "������";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiRules, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRecalc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.Text = "������";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "���������";
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bsiRules
            // 
            this.bsiRules.Caption = "������� ������������� ����������";
            this.bsiRules.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiRules.Glyph")));
            this.bsiRules.Id = 1;
            this.bsiRules.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bchRuleProport),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchRuleEqually)});
            this.bsiRules.Name = "bsiRules";
            this.bsiRules.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bchRuleProport
            // 
            this.bchRuleProport.Caption = "��������������� ������������ �������";
            this.bchRuleProport.Checked = true;
            this.bchRuleProport.Id = 13;
            this.bchRuleProport.Name = "bchRuleProport";
            this.bchRuleProport.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchRuleProport_CheckedChanged);
            // 
            // bchRuleEqually
            // 
            this.bchRuleEqually.Caption = "�������";
            this.bchRuleEqually.Id = 14;
            this.bchRuleEqually.Name = "bchRuleEqually";
            this.bchRuleEqually.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchRuleEqually_CheckedChanged);
            // 
            // bbiRecalc
            // 
            this.bbiRecalc.Caption = "�����������";
            this.bbiRecalc.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRecalc.Glyph")));
            this.bbiRecalc.Id = 15;
            this.bbiRecalc.Name = "bbiRecalc";
            this.bbiRecalc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalc_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "������ ���������";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "������ ���������";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(944, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 611);
            this.barDockControlBottom.Size = new System.Drawing.Size(944, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 580);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(944, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 580);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit1;
            this.barEditItem1.Id = 6;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemRadioGroup2;
            this.barEditItem2.Id = 7;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemRadioGroup2
            // 
            this.repositoryItemRadioGroup2.Columns = 2;
            this.repositoryItemRadioGroup2.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "���"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(2)), "����")});
            this.repositoryItemRadioGroup2.Name = "repositoryItemRadioGroup2";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "���"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(2)), "���"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(3)), "���")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // richRuleProport
            // 
            this.richRuleProport.AutoHeight = false;
            this.richRuleProport.Caption = "��������������� ������������ �������";
            this.richRuleProport.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.richRuleProport.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.richRuleProport.Name = "richRuleProport";
            // 
            // richRuleEqually
            // 
            this.richRuleEqually.AutoHeight = false;
            this.richRuleEqually.Caption = "�������";
            this.richRuleEqually.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.richRuleEqually.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.richRuleEqually.Name = "richRuleEqually";
            this.richRuleEqually.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // SquareAnaliz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 634);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SquareAnaliz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� �������";
            ((System.ComponentModel.ISupportInitialize)(this.peSq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teSquare.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richRuleProport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richRuleEqually)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        internal DevExpress.XtraEditors.PictureEdit peSq;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colOdId;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregateName;
        private DevExpress.XtraGrid.Columns.GridColumn colSquareFactGa;
        private DevExpress.XtraGrid.Columns.GridColumn colSquareAfterRecalc;
        internal DevExpress.XtraGrid.GridControl gcOrder;
        internal DevExpress.XtraEditors.TextEdit teSquare;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarSubItem bsiRules;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup2;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit richRuleProport;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit richRuleEqually;
        private DevExpress.XtraBars.BarCheckItem bchRuleProport;
        private DevExpress.XtraBars.BarCheckItem bchRuleEqually;
        private DevExpress.XtraBars.BarButtonItem bbiRecalc;

    }
}