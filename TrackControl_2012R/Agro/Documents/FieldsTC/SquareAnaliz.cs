using Agro.Properties;
using TrackControl.General; 
using System.Collections.Generic ;
using System;
using System.Windows.Forms;
using DevExpress.XtraBars; 

namespace Agro
{
    public partial class SquareAnaliz : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler SaveAnaliz = delegate { };
        public event Action<RulesForRecalcOverlap> RecalcByRule = delegate { };

        public SquareAnaliz()
        {
            InitializeComponent();
            Localization();
        }

        #region �����������
        private void Localization()
        {
            //erSquareZone.Properties.Caption = Resources.SquareTotalGA;
            //erSquareProcess.Properties.Caption = Resources.ProcessGA;
            //erSquareProcessPersent.Properties.Caption = Resources.ProcessPercent;
            //erSquareSecondProcess.Properties.Caption = Resources.ProcessRepeatGA;
            //erSquareSecondProcessPersent.Properties.Caption = Resources.ProcessRepeatPercent;
            //erSquareSecondProcessFactor.Properties.Caption = Resources.ProcessRepeatCoeff;
            //Text = Resources.SquareProcess;
        }
        #endregion

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveAnaliz();
            bbiSave.Enabled = false;
        }

        private void bchRuleProport_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bchRuleProport.Checked) bchRuleEqually.Checked = false;
        }

        private void bchRuleEqually_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bchRuleEqually.Checked) bchRuleProport.Checked = false;
        }



        private void bbiRecalc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RecalcByRule(GetActiveRule());
            gcOrder.RefreshDataSource(); 
        }

        RulesForRecalcOverlap GetActiveRule()
        {
              return RulesForRecalcOverlap.ProportionalyArea;
        }



    }
}