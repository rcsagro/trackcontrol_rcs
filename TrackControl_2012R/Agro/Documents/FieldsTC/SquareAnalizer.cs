﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraBars;
using DevExpress.XtraPrinting.Native;
using TrackControl.Zones;
using TrackControl.General;
using Agro.Properties;
using System.Windows.Forms;
using DevExpress.XtraEditors; 

namespace Agro
{
    public class SquareAnalizer
    {
        public event VoidHandler DataChanged = delegate { };
        readonly IList<TechOperationFactRecord> _factRecords;
        OrderSquareBitmap _osb;
        readonly IZone _zone;
        double _squareOverlapRecords;
        decimal _squareTotal;

        public SquareAnalizer(IList<TechOperationFactRecord> factRecords,int idZone)
        {
            _factRecords = factRecords;
            _zone = DocItem.ZonesModel.GetById(idZone);
        }

        public void StartDraw(bool viewSaveButton = true)
        {
            CalcSquares();
            ViewForm(viewSaveButton);
        }


        public void CalcSquares()
        {
            _osb =
                new OrderSquareBitmap(
                    _zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK,
                    _zone.Points);
            _factRecords.ForEach(DrawOneRecord);
            _factRecords.ForEach(r => SetCalcSquare(r, RulesForRecalcOverlap.ProportionalyArea));
        }

        private void ViewForm(bool viewSaveButton = true)
        {
            var formAnaliz = new SquareAnaliz();
            if (viewSaveButton)
            {
                formAnaliz.bbiSave.Visibility = BarItemVisibility.Always;
                formAnaliz.SaveAnaliz += OnSaveAnaliz;
            }
            else
            {
                formAnaliz.bbiSave.Visibility = BarItemVisibility.Never;
            }
            formAnaliz.RecalcByRule += OnRecalcByRule;
            formAnaliz.peSq.Image = _osb.BtmField;
            formAnaliz.teSquare.Text = _squareOverlapRecords.ToString();
            formAnaliz.gcOrder.DataSource = _factRecords;
            formAnaliz.Show();
        }

        void DrawOneRecord(TechOperationFactRecord factRecord)
        {
            if (factRecord.IdOrderTs==null) factRecord.IdOrderTs = TechOpersFactController.GetListIdOrderT(factRecord);
            _squareTotal += factRecord.SquareFactGa;
            foreach (int idOrderT in factRecord.IdOrderTs)
            {
                var oir = new OrderItemRecord(idOrderT);
                var cd = new CompressData("agro_datagps", idOrderT);
                oir.Rpoints = cd.DeCompressReal();
                if (_squareOverlapRecords == 0)
                    _squareOverlapRecords = _osb.SquareCalcBitmap(oir, false);
                else
                    _squareOverlapRecords = _osb.SquareCalcAddTrack(oir);
            }

        }

        void SetCalcSquare(TechOperationFactRecord factRecord, RulesForRecalcOverlap rule)
        {
            switch (rule)
            {
                //case RulesForRecalcOverlap.Equaly:
                //    {
                //        factRecord.SquareAfterRecalc = Math.Round(factRecord.SquareFactGa - (_squareTotal - (decimal)_squareOverlapRecords) / _factRecords.Count, 3);
                //        break;
                //    }
                case RulesForRecalcOverlap.ProportionalyArea:
                    {
                        decimal squareRecalc = _squareTotal;
                        if (_squareTotal > 0)
                        {
                            squareRecalc = (_squareTotal - (decimal) _squareOverlapRecords)*
                                           (factRecord.SquareFactGa/_squareTotal);
                        }

                        factRecord.SquareAfterRecalc = Math.Round(factRecord.SquareFactGa - squareRecalc, 3);
                        break;
                    }
            }
            
        }

        void OnSaveAnaliz()
        {
            if (DialogResult.Yes == XtraMessageBox.Show(Resources.ConfirmSave, Resources.ApplicationName, MessageBoxButtons.YesNo))
            {
                _factRecords.ForEach(SaveRecord);
                FieldTechOperation operation = _factRecords[0].Operation;
                operation.RecalcSquare();
                DataChanged();

            }

        }

        void SaveRecord(TechOperationFactRecord factRecord)
        {
            factRecord.UserRecalc = UserBaseCurrent.Instance.Name;
            factRecord.DateRecalc = DateTime.Now;
            factRecord.Save();
        }

        void OnRecalcByRule(RulesForRecalcOverlap rule)
        {
            _factRecords.ForEach(r => SetCalcSquare(r, rule));
        }


    }
}
