﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Agro.Dictionaries;
using TrackControl.General.DatabaseDriver;
using System.Data;

namespace Agro
{
    public class FieldTcOrderConnector
    {
        FieldTechOperation Oper { get; set; }

        public FieldTcOrderConnector(FieldTechOperation oper)
        {
            Oper = oper;
        }


        private void CreateParameters(DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", Oper.FieldsTc.Field.Id);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", Oper.FieldsTc.DateStart);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", Oper.FieldsTc.DateEnd);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", Oper.WorkType.Id);
        }

        public void SaveOrderItemRecordsList()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sqlSelect = string.Format(AgroQuery.FieldTechOpers.SelectLinkedGroupedOrderTRecords,
                    driverDb.ParamPrefics);

                string sqlInsert = string.Format(AgroQuery.FieldTechOpersFact.InsertOrderTGroupLink, driverDb.ParamPrefics);

                CreateParameters(driverDb);
                driverDb.GetDataReader(sqlSelect, driverDb.GetSqlParameterArray);
                DriverDb driverDbIns = new DriverDb();
                driverDbIns.ConnectDb();

                while (driverDb.Read())
                {
                    driverDbIns.NewSqlParameterArray(5);
                    driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "Id_main", Oper.Id);
                    driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "IdOrder", driverDb.GetInt32("Id"));
                    if (driverDb.GetInt32("Id_driver") == 0)
                    {
                        driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "IdDriver", null);
                    }
                    else
                    {
                        driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "IdDriver",
                            driverDb.GetInt32("Id_driver"));
                    }
                    if (driverDb.GetInt32("Id_agregat") == 0)
                    {
                        driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "IdAgreg", null);
                    }
                    else
                    {
                        driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "IdAgreg",
                            driverDb.GetInt32("Id_agregat"));
                    }

                    driverDbIns.SetNewSqlParameter(driverDbIns.ParamPrefics + "SquareFactGa", (decimal)driverDb.GetDouble("SQ"));
                    driverDbIns.ExecuteNonQueryCommand(sqlInsert, driverDbIns.GetSqlParameterArray);

                    if (Oper.DateStartFact == null)
                    {
                        Oper.DateStartFact = driverDb.GetDateTime("DateStart");
                    }
                    else if (Oper.DateStartFact > driverDb.GetDateTime("DateStart"))
                        Oper.DateStartFact = driverDb.GetDateTime("DateStart");

                    if (Oper.DateEndFact == null)
                    {
                        Oper.DateEndFact = driverDb.GetDateTime("DateEnd");
                    }
                    else if (Oper.DateEndFact < driverDb.GetDateTime("DateEnd"))
                        Oper.DateEndFact = driverDb.GetDateTime("DateEnd");
                }

                driverDb.CloseDataReader();
                driverDbIns.CloseDbConnection();
                Oper.RecalcSquare();
                driverDb.CloseDbConnection();
            }
        }
    }
}
