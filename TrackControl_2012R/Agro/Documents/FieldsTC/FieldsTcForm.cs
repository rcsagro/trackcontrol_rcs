﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Controls;
using TrackControl.General;

namespace Agro
{
    public partial class FieldsTcForm : DevExpress.XtraEditors.XtraForm
    {
        private AgroFieldTc _mapControl;

        public FieldsTcForm()
        {
            InitializeComponent();
            bbiLog.Caption = Resources.LogEvents;
            colDateOrder.Caption = Resources.Date;
        }

        private void pgProperties_CellValueChanged(object sender,
            DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            var entity = (FieldsTcEntity) pgProperties.SelectedObject;
            switch (e.Row.Name)
            {
                case "erFieldGroup":
                {

                    FillFieldsList(entity);
                    break;
                }
                case "erField":
                {
                    using (FieldsTcController fc = new FieldsTcController())
                    {
                        fc.DrawZone(this, entity);
                    }
                    break;
                }
                case "erSeason":
                {
                    SetSeasonDates(entity);
                    break;
                }
            }

            if (entity != null)
            {
                using (FieldsTcController fc = new FieldsTcController(entity))
                {
                    if (fc.ValidateHeader()) bbiSave.Enabled = true;
                }
            }
        }

        private void FillFieldsList(FieldsTcEntity entity)
        {
            DictionaryAgroField.SetComboBoxDataSource(cbeField, entity.FieldGroup.Id);
        }

        private void DrawZone(FieldsTcEntity entity)
        {
            int idZone = entity.Field.IdZone;
            if (idZone > 0)
            {
                IZone zone = DictionaryAgroField.ZonesModel.GetById(idZone);
                if (zone != null) zoneProperty.SetZone(DictionaryAgroField.ZonesModel.GetById(idZone));
            }
        }

        #region ButtonsEvents

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }

        private void bbiLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FieldsTcEntity entity = (FieldsTcEntity) pgProperties.SelectedObject;
            if (entity != null && entity.ID > 0) UserLog.ViewLog(UserLogTypes.AGRO_FieldTc, entity.ID);
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void bbiFact_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SetFact();
        }

        private void bbiJointProcess_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AnalizRecordsForOverlap();
        }

        #endregion

        #region XtraGridsEvents

        private void gvTechOpers_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            FieldTechOperation record = gvTechOpers.GetRow(e.RowHandle) as FieldTechOperation;
            if (record == null)
            {
                e.Valid = false;
                return;
            }
            if (FieldTechOpersController.Validate(record))
            {
                e.Valid = true;
            }
            else
            {
                e.Valid = false;
            }
        }

        private void gcTechOpers_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:
                {
                    if (XtraMessageBox.Show(Resources.DeleteQuestion,
                        Resources.ApplicationName,
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FieldTechOperation record =
                            gvTechOpers.GetRow(gvTechOpers.FocusedRowHandle) as FieldTechOperation;
                        if (record == null)
                            e.Handled = true;
                        else
                            e.Handled = !FieldTechOpersController.Delete(record);
                    }
                    else
                    {
                        e.Handled = true;
                    }
                    break;
                }
            }
        }

        private void gvTechOpers_CellValueChanged(object sender,
            DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            SaveRecord(e.RowHandle);
        }

        private void gvTechOpers_FocusedRowChanged(object sender,
            DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SetFactRecordsDataSource();

        }

        private void SetFactRecordsDataSource()
        {
            var operation = gvTechOpers.GetRow(gvTechOpers.FocusedRowHandle) as FieldTechOperation;
            if (operation != null)
            {
                gcOrder.DataSource = operation.FactRecords;
            }
        }

        private void gvTechOpers_CustomDrawRowIndicator(object sender,
            DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gvOrder_CustomDrawRowIndicator(object sender,
            DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        #endregion

        private void Save()
        {
            var entity = (FieldsTcEntity) pgProperties.SelectedObject;
            using (var fc = new FieldsTcController(entity))
            {
                if (entity.ID == 0)
                {
                    fc.AddDoc();
                    if (entity.ID == 0) return;
                    gcTechOpers.DataSource = entity.Operations;
                }
                else if (!fc.UpdateDoc()) return;
            }
            RefreshData();
            bbiSave.Enabled = false;
        }

        private void RefreshData()
        {
            pgProperties.Refresh();
            gcTechOpers.RefreshDataSource();
            gcOrder.RefreshDataSource();
        }

        private void SetFact()
        {
            FieldsTcEntity entity = (FieldsTcEntity) pgProperties.SelectedObject;
            if (entity == null) return;
            FieldsTcController fc = new FieldsTcController(entity);
            {
                if (fc.SetFact())
                {
                    SetFactRecordsDataSource();
                    RefreshData();
                }
            }
        }

        private void SetSeasonDates(FieldsTcEntity entity)
        {
            if (entity == null) return;
            using (DictionaryAgroSeason season = new DictionaryAgroSeason(entity.Season.Id))
            {
                if (entity.DateStart == DateTime.MinValue) entity.DateStart = season.DateStart;
                if (entity.DateEnd == DateTime.MinValue) entity.DateEnd = season.DateEnd;
            }
        }

        private void SaveRecord(int rowHandle)
        {
            FieldTechOperation record = gvTechOpers.GetRow(rowHandle) as FieldTechOperation;
            if (!FieldTechOpersController.Validate(record)) return;
            if (record.Id == 0)
            {
                FieldsTcEntity entity = (FieldsTcEntity) pgProperties.SelectedObject;
                record.FieldsTc = entity;
            }
            FieldTechOpersController.Save(record);
        }

        private void AnalizRecordsForOverlap()
        {
            var operation = gvTechOpers.GetRow(gvTechOpers.FocusedRowHandle) as FieldTechOperation;
            if (operation != null)
            {
                var records = operation.FactRecords.Where(fr => fr.JointProcess == true).ToList();
                if (records.Count > 0)
                {
                    var sa = new SquareAnalizer(records, records[0].Operation.FieldsTc.Field.IdZone);
                    sa.StartDraw();
                    sa.DataChanged += RefreshData;
                }
            }
        }

        private void tabCtrl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (tabCtrl.SelectedTabPageIndex)
            {
                case 1:
                {
                    if (_mapControl == null)
                    {
                        FieldsTcEntity entity = (FieldsTcEntity) pgProperties.SelectedObject;
                        if (entity != null)
                        {
                            _mapControl = new AgroFieldTc(entity);
                            pnMap.Controls.Add(_mapControl);
                        }
                    }

                    break;
                }
            }
        }
    }
}