﻿using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using System;
using System.Collections.Generic;  
using System.ComponentModel;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    public static class TechOpersFactController
    {
        public static BindingList<TechOperationFactRecord> GetList(FieldTechOperation operation)
        {
            var records = new BindingList<TechOperationFactRecord>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.FieldTechOpersFact.SelectFactRecords, operation.Id);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    var record = new TechOperationFactRecord
                        {
                            Id = driverDb.GetInt32("RecordId"),
                            Operation = operation,
                            DateOrder = driverDb.GetDateTime("Date"),
                            NumberOrder = driverDb.GetInt32("OrderId"),
                            VehicleName = driverDb.GetString("NumberPlate"),
                            DriverName = (string) TotUtilites.NdbNullReader(driverDb, "Family", ""),
                            AgregateName = (string) TotUtilites.NdbNullReader(driverDb, "AgrName", ""),
                            SquareFactGa = (Decimal) TotUtilites.NdbNullReader(driverDb, "SquareFactGa", 0),
                            JointProcess = driverDb.GetBoolean("JointProcess"),
                            SquareAfterRecalc = (Decimal) TotUtilites.NdbNullReader(driverDb, "SquareAfterRecalc", 0),
                            DateRecalc = (DateTime?) TotUtilites.NdbNullReader(driverDb, "DateRecalc", null),
                            UserRecalc = (string) TotUtilites.NdbNullReader(driverDb, "UserRecalc", "")
                        };
                    records.Add(record);
                }
                driverDb.CloseDataReader();
                driverDb.CloseDbConnection(); 
            }
            return records;
        }

        public static List<int> GetListIdOrderT(TechOperationFactRecord factRecord)
        {
            var idOrderTs = new List<int>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.FieldTechOpers.SelectLinkedOrderTRecords, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", factRecord.NumberOrder);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_field", factRecord.Operation.FieldsTc.Field.Id); 
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_work", factRecord.Operation.WorkType.Id);
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                while (driverDb.Read())
                {
                    idOrderTs.Add(driverDb.GetInt32("Id")); 
                }
                driverDb.CloseDataReader();
            }
            return idOrderTs;
        }

        public static void Save(TechOperationFactRecord record)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();     
                UpdateRecord(record, driverDb);
            }
        }

         static void UpdateRecord(TechOperationFactRecord record, DriverDb driverDb)
        {
            string sSql = string.Format(AgroQuery.FieldTechOpersFact.Update, driverDb.ParamPrefics, record.Id);
            CreateParameters(driverDb, record);
            driverDb.ExecuteNonQueryCommand(sSql, driverDb.GetSqlParameterArray);
            UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, string.Format(Resources.SaveRecalcFactRecord, record.SquareAfterRecalc ,record.NumberOrder ,record.Operation.WorkType.Name ), record.Operation.FieldsTc.ID);
        }

         private static void CreateParameters(DriverDb driverDb, TechOperationFactRecord record)
        {
            driverDb.NewSqlParameterArray(4);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "JointProcess", record.JointProcess);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareAfterRecalc", record.SquareAfterRecalc); 
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserRecalc", record.UserRecalc);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateRecalc", record.DateRecalc); 
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (record.DateRecalc  != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateRecalc", record.DateRecalc);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateRecalc", DBNull.Value);
                }
            }
        }
    }
}
