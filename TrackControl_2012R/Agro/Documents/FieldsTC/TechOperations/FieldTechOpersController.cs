﻿using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using System;
using System.Linq;
using System.Collections.Generic; 
using System.ComponentModel;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    class FieldTechOpersController
    {
        public static void Save(FieldTechOperation operation)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                if (operation.Id == 0)
                {
                    InsertRecord(operation, driverDb);
                }
                else
                {
                    UpdateRecord(operation, driverDb);
                }
                driverDb.CloseDbConnection(); 
            }
        }

        private static void InsertRecord(FieldTechOperation operation, DriverDb driverDb)
        {
            string sSQL = string.Format(AgroQuery.FieldTechOpers.Insert, driverDb.ParamPrefics);
            CreateParameters(driverDb, operation);
            operation.Id = driverDb.ExecuteReturnLastInsert(sSQL, driverDb.GetSqlParameterArray, "agro_fieldseason_tct");
            UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, string.Format(Resources.LogAddContent, operation.Id), operation.FieldsTc.ID);
        }

        private static void CreateParameters(DriverDb driverDb, FieldTechOperation operation)
        {
            driverDb.NewSqlParameterArray(11);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", operation.FieldsTc.ID);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IdWork", operation.WorkType.Id); // !
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStartPlan", operation.DateStartPlan);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEndPlan", operation.DateEndPlan);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "WorkQtyPlan", operation.WorkQtyPlan);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStartFact", operation.DateStartFact); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (operation.DateStartFact != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStartFact", operation.DateStartFact);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStartFact", DBNull.Value);
                }
            }

             if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEndFact", operation.DateEndFact); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (operation.DateStartFact != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEndFact", operation.DateEndFact);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEndFact", DBNull.Value);
                }
            }
             driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareGa", operation.SquareGa);
             driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquarePercent", operation.SquarePercent);
             driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "IsClosed", operation.IsClosed);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", operation.Comment); // !
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (operation.Comment != null)
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", operation.Comment);
                }
                else
                {
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", "");
                }
            }
        }

        private static void UpdateRecord(FieldTechOperation record, DriverDb driverDb)
        {
            string sSQL = string.Format(AgroQuery.FieldTechOpers.Update, driverDb.ParamPrefics, record.Id);
            CreateParameters(driverDb, record);
            driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
            UserLog.InsertLog(UserLogTypes.AGRO_FieldTc, string.Format(Resources.LogUpdateContent, record.Id), record.FieldsTc.ID);
        }

        public static bool Validate(FieldTechOperation operation)
        {
            if (operation.WorkType != null && operation.DateStartPlan != null
                && operation.DateEndPlan != null && operation.WorkQtyPlan>0
                && operation.DateEndPlan >= operation.DateStartPlan)
                return true;
            else
                return false;
        }

        public static BindingList<FieldTechOperation> GetList(FieldsTcEntity fieldsTc)
        {
            BindingList<FieldTechOperation> records = new BindingList<FieldTechOperation>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.FieldTechOpers.SelectContentTechCart, fieldsTc.ID);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    FieldTechOperation record = new FieldTechOperation();
                    record.Id = driverDb.GetInt32("Id");
                    record.FieldsTc = fieldsTc;
                    int idWork = (int)TotUtilites.NdbNullReader(driverDb, "IdWork", 0);
                    if (idWork > 0)
                    {
                        record.WorkType = new DictionaryAgroWorkType(idWork);
                        record.WorkType.Dispose(); 
                    }
                    record.DateStartPlan = (DateTime?)TotUtilites.NdbNullReader(driverDb, "DateStartPlan", null);
                    record.DateEndPlan = (DateTime?)TotUtilites.NdbNullReader(driverDb, "DateEndPlan", null);
                    record.WorkQtyPlan = (Decimal)TotUtilites.NdbNullReader(driverDb, "WorkQtyPlan", 0);
                    record.DateStartFact = (DateTime?)TotUtilites.NdbNullReader(driverDb, "DateStartFact", null);
                    record.DateEndFact = (DateTime?)TotUtilites.NdbNullReader(driverDb, "DateEndFact", null);
                    record.SquareGa = (Decimal)TotUtilites.NdbNullReader(driverDb, "SquareGa", 0);
                    record.SquarePercent = (Decimal)TotUtilites.NdbNullReader(driverDb, "SquarePercent", 0);
                    record.Comment = (string)TotUtilites.NdbNullReader(driverDb, "Comment", "");
                    record.FactRecords = TechOpersFactController.GetList(record);
                    record.IsClosed = driverDb.GetBoolean("IsClosed");
                    record.DrawTrack = true;
                    records.Add(record);
                }
                driverDb.CloseDataReader();
                driverDb.CloseDbConnection(); 
            }
            return records;
        }

        public static bool Delete(FieldTechOperation operation)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.FieldTechOpers.Delete, operation.Id);
                driverDb.ExecuteNonQueryCommand(sql);
                return true;
            }
        }

        public static bool DeleteOrderTLinks(FieldTechOperation operation)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.FieldTechOpersFact.DeleteLinkedOrderTRecords, operation.Id);
                driverDb.ExecuteNonQueryCommand(sql);
                driverDb.CloseDbConnection(); 
                return true;
            }
        }

        public static void RefreshDataOnView()
        {

        }

    }
}
