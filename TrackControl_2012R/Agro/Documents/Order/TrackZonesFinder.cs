﻿using System.Collections.Generic;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;


namespace Agro
{
    public class TrackZonesFinder
    {
        LON_LAT_MIN_MAX _trackBounds;
        GpsData[] _gpsDatas;
        List<IZone> _zonesId;

        public TrackZonesFinder(LON_LAT_MIN_MAX trackBounds, GpsData[] gpsDatas)
        {
            _zonesId = new List<IZone>(); 
            _trackBounds = trackBounds;
            _gpsDatas = gpsDatas;
        }

        public List<IZone> GetZones()
        {
            string _sSql = AgroQuery.TrackZonesFinder.SelectDistinctZones;

            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(_sSql);
                while (driverDb.Read())
                {
                    int Zone_id = driverDb.GetInt32("Zone_ID");
                    IZone zone = OrderItem.ZonesModel.GetById(Zone_id);
                    //if (zone != null && (TrackInZone(zone) || BoundsTrackInBoundsZone(zone)))
                    if (zone != null && TrackInZone(zone))
                    {
                        if (!_zonesId.Contains(zone))
                            _zonesId.Add(zone); 
                    }
                }
                driverDb.CloseDataReader();
            }
            return _zonesId;
        }

        bool TrackInZone(IZone zone)
        {
            if (!IsTrackWithZoneIntersect(zone))
                return false;

            foreach (GpsData  gpsData in _gpsDatas)
            {
                if (zone.BoundContains(gpsData.LatLng))
                    return true;
            }
            return false;
        }

        bool IsTrackWithZoneIntersect(IZone zone)
        {
            return (_trackBounds.LON_min <= zone.Bounds.Right
            && zone.Bounds.Left <= _trackBounds.LON_max
            && _trackBounds.LAT_max >= zone.Bounds.Bottom
            && zone.Bounds.Top >= _trackBounds.LAT_min);
        }
    }
}
