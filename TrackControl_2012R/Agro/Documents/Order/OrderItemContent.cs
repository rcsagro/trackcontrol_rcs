﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;


namespace Agro
{
    public partial class OrderItem
    {
        /// <summary>
        /// время выхода из предыдущей записи наряда 
        /// </summary>
        //DateTime dtPrevEnd;
        /// <summary>
        /// время остановки с предыдущей записи наряда, возникшее в конце
        /// </summary>
        TimeSpan _tsStopFromPrev = TimeSpan.MinValue;
        /// <summary>
        /// структура сбора данных
        /// </summary>
        CiData _cid;

        /// <summary>
        /// нарезка движенеим за сутки на записи.Новая запись - смена поля (или выезд за поле с учетом дребезга),водителя или агрегата
        /// </summary>
        /// <param name="bControl">пересчет с учетом управляющих данных</param>
        /// <returns></returns>
        public bool ContentAutoCreate(bool bControl)
        {
            _Control = bControl;

            if (Create_dsAtlanta(_dateDoc, _dateDoc.AddDays(1).AddMinutes(-1)) == 0) return false;
            if (!bControl)
            {
                if (!ContentDeleteControlObjects()) return false;
            }
            else
            {
                ContentCreateConfimedData();
            }
            if (_ID == 0)
            {
                //создание наряда 
                AddDoc();
            }
            if (_ID == 0)
            {
                SetStatusEvent(Resources.AgroLimit_1);
                ContentDataSetReturnClear();
                return false;
            }
            ContentDelete();
            ContentPrepareAlgoritmsData();

            CI_ContentIdentify();
            UpdateDocTotals();
            // подсчет управляющих нарезкой нарядов объектов
            ContentCalcControlObject(Consts.TypeControlObject.Agregat, "Id_agregat");
            ContentCalcControlObject(Consts.TypeControlObject.Driver, "Id_driver");
            ContentCalcControlObject(Consts.TypeControlObject.Field, "Id_field");
            ContentDataSetReturnClear();
            if (bControl)
                UserLog.InsertLog(UserLogTypes.AGRO, "Пересчет наряда с учетом управляющих данных", _ID);
            else
                UserLog.InsertLog(UserLogTypes.AGRO, "Пересчет наряда без учета управляющих данных", _ID);
            return true;
        }

        /// <summary>
        /// борьба с переездами по полям , не связанными с работой
        /// </summary>
        /// <returns></returns>
        private bool TestPathSquareZone(OrderItemRecord oir)
        {
            AgroSetItem asi = new AgroSetItem();
            if (oir.Distance >= (double)asi.MinPathInFieldForCreateRecord / 1000)
            {
                double ZoneSquare = 0;
                if (oir.ZoneId > 0)
                {
                    ZoneSquare = ZonesModel.GetById(oir.ZoneId).AreaGa;
                }
                if (ZoneSquare > 0)
                {
                    double PersentWork = 100 * oir.Square / ZoneSquare;
                    if (PersentWork >= asi.MinSquarePersentForCreateRecord)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return true;
            }
            else
                return false;
        }


        /// <summary>
        /// идентификатор поля (не зоны) из массива управления созданием наряда
        /// </summary>
        private int ContentGetZone(GpsData gpsData)
        {
            int iField = 0;
            try
            {
                // если поле определено в гриде управления - подставляем его идентификтор 
                if (_dtControl.Rows.Count > 0)
                {
                    iField = ContentGetControlObject(_dtControl, Consts.TypeControlObject.Field, gpsData.Time);
                }
                if (iField > 0)
                {
                    // если в режиме управления полю назначено время, при котором работы в поле не велись - отсекаем 
                    int iZone = 0;
                    if (_zones_fields.ContainsKey(iField))
                    {
                        iZone = _zones_fields[iField];
                    }
                    else
                    {
                        iZone = GetZone(iField);
                        _zones_fields.Add(iField, iZone);
                    }
                    //29.04.2014 Агродом - нельзя было управляющими данными переезд преобразовать в поле 
                    //(возникла дырка в записи обработки поля из-за узкости поля и постоянным выездом трактора за предел  - срабатывало ограничение по времени для создания записи наряда)
                    //if (dcZonesBitmap.ContainsKey(iZone) && dcZonesBitmap[iZone].PointInZone(gpsData.LatLng.Lng, gpsData.LatLng.Lat))
                        return iZone;
                    //else
                    //    return 0;
                }
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// добавление записи в наряд с подсчетом параметров
        /// </summary>
        /// <param name="gpsDataZone"> набор DataGPS соответствующий записи наряда </param>
        /// <returns></returns>
        private bool ContentCreateRecord(List<GpsData> gpsDataZone)
        {
            if (gpsDataZone.Count == 0 || m_row == null) return false;
            if (_ID == 0) AddDoc();
            if (_ID == 0) return false;
            if (_recordNumber == 0) _recordNumber = 1;
            var oir = new OrderItemRecord(_cid.PrevZone, _cid.PrevDriver, _cid.PrevAgreg, _cid.PrevAgrInputSource, _cid.PrevDrvInputSource, gpsDataZone);
            if (_cid.BreakingPointId > 0)
            {
                oir.BreakingPointId = _cid.BreakingPointId;
                oir.BreakingPointWorkId = _cid.BreakingPointWorkId;
            }
            else
            {
                oir.BreakingPointId = 0;
            }
            ContentCalcParams(ref oir);
            if (oir.IdOrderT == 0)
            {
                oir.Confirm = ContentGetControlObjectExactly(Consts.TypeControlObject.Confirm, oir.GpsDataZone[0].Time, oir);
                //При наличии логического датчика работы при работе в поле и неактивном датчике работу обнуляем
                if (_cid.IsSensorExist)
                {
                    if (oir.Confirm == 1 && _cid.PrevAgreg == 0) oir.Confirm = 0;
                    if (oir.Confirm == 0 && _cid.PrevAgreg != 0) oir.Confirm = 1;
                }
                oir.IdOrderT = ContentAdd(ref oir);
            }

            if (oir.ZoneId > 0)
            {
                SetStatusEvent(Resources.SquareCalcRun + " " + oir.Distance.ToString());
                IZone zone = ZonesModel.GetById(oir.ZoneId);
                if (zone != null)
                {
                    using (var osb = new OrderSquareBitmap((zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK), zone.Points))
                    {
                        oir.SquareCalcCont = osb.SquareCalcBitmap(oir, false);
                    }
                }
            }
            WriteCompressDataGPS(ref oir);
            var pa = new PartialAlgorithms(m_row);
            ContentCalcParamsDUT(ref oir, pa);
            ContentCalcParamsDRT(ref oir, pa);
            oir.SetPointsValidity();
            ContentUpdate(ref oir);
            _oirs.Add(oir);
            return true;
        }

        public bool ContentCreateRecord(int idZone, int idDriver, int idAgregat, DateTime dtStart, DateTime dtEnd)
        {
            if (_dsAtlanta == null)
            {
                if (Create_dsAtlanta(_dateDoc, _dateDoc.AddDays(1).AddMinutes(-1)) == 0)
                {
                    SetStatusEvent(Resources.Ready);
                    return false;
                }
                _oirs = new List<OrderItemRecord>();
            }
            //atlantaDataSet.dataviewRow[] drows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id = " + _Mobitel_Id + " AND time >= #" + dtStart.ToString("MM.dd.yyyy HH:mm:ss") + "# and time  <= #" + dtEnd.ToString("MM.dd.yyyy HH:mm:ss") + "#", "time");
            if (GpsDatasDocItem.Length == 0) return false;
            List<GpsData> gpsDataZone = GpsDatasDocItem.Where(gps => gps.Time > dtStart & gps.Time < dtEnd & gps.Mobitel == _Mobitel_Id).OrderBy(gps => gps.Time).ToList(); 
            //foreach (atlantaDataSet.dataviewRow drow in drows)
            //{
            //    lsDrows.Add(drow);
            //}
            //стыковочные точки - для устранения дублирования подсчета расстояния
            if (GpsDatasDocItem.Length > 0) GpsDatasDocItem[0].Dist = 0;
            _cid = new CiData {PrevZone = idZone, PrevDriver = idDriver, PrevAgreg = idAgregat};
            ContentPrepareAlgoritmsData();
            CI_PrepareControlData();
            _dtControl.Clear();  // для пересчета без управляющих даных подтвержденных позиций
            if (ContentCreateRecord(gpsDataZone))
            {
                SetStatusEvent(Resources.Ready);
            }
            return true;
        }
        /// <summary>
        /// запись сжатых данных для быстрой прорисовки наряда
        /// </summary>
        private void WriteCompressDataGPS(ref OrderItemRecord oir)
        {
            List<CompressData.TRealPointSpeed> rspoints = new List<CompressData.TRealPointSpeed>();
            for (int i = 0; i < oir.GpsDataZone.Count; i++)
            {
                //if (oir.GpsDataZone[i].speed > 0)
                //{
                CompressData.TRealPointSpeed rspoint;
                rspoint.X = oir.GpsDataZone[i].LatLng.Lng;
                rspoint.Y = oir.GpsDataZone[i].LatLng.Lat;
                rspoint.Speed = oir.GpsDataZone[i].Speed;
                rspoints.Add(rspoint);
                Application.DoEvents();
                //}
            }
            CompressData cs = new CompressData("agro_datagps", oir.IdOrderT);
            cs.Compress(rspoints);
        }
        /// <summary>
        /// создание записи наряда
        /// </summary>
        /// <param name="oir"></param>
        /// <returns></returns>
        private int ContentAdd(ref OrderItemRecord oir)
        {
            oir.TimeStart = oir.GpsDataZone[0].Time;
            oir.TimeEnd = oir.GpsDataZone[oir.GpsDataZone.Count - 1].Time;
            // запись наряда не должна превышать суток
            if (oir.TimeEnd > oir.TimeStart.AddDays(1).AddMinutes(-1))
                oir.TimeEnd = oir.TimeStart.AddDays(1).AddMinutes(-1);
            oir.IdMain = _ID;
            int idT = oir.AddRecord();
            if (idT>0) _recordNumber++;
            return idT;
        }

        private bool ContentUpdate(ref OrderItemRecord oir)
        {
            return oir.UpdateRecord();
        }
        ///// <summary>
        ///// вычисление параметров одной записи наряда
        ///// </summary> 
        ///// <param name="oir"></param>
        private void ContentCalcParams(ref OrderItemRecord oir)
        {
            // наличие подсчета времени остановки
            bool bStartStop = false;
            GpsData GpsDataStartStop = null;
            GpsData GpsDataPrev = oir.GpsDataZone[0];
            //d_row_prev = dtPrevEnd;// oir.lsgpsDatas[0];
            int cnt = 0;
            foreach (GpsData gpsData in oir.GpsDataZone)
            {
                if (gpsData.Speed == 0)
                {
                    if (!bStartStop)
                    {
                        bStartStop = true;
                        GpsDataStartStop = gpsData;
                    }
                }
                else
                {
                    if (bStartStop)
                    {
                        TimeSpan timeBreak = gpsData.Time - GpsDataStartStop.Time;
                        if (timeBreak >= _legalTimeBreak)
                        {
                            oir.TsStop = oir.TsStop.Add(timeBreak);
                            bStartStop = false;
                            cnt++;
                        }
                    }
                    bStartStop = false;
                }
                oir.Distance += gpsData.Dist;
                if (GetFactZoneRotation(gpsData.Id)) oir.TsEngineOn = oir.TsEngineOn.Add(gpsData.Time.Subtract(GpsDataPrev.Time));

                GpsDataPrev = gpsData;
            }
            if (bStartStop)
            {
                TimeSpan timeBreak = GpsDataPrev.Time - GpsDataStartStop.Time;
                if (timeBreak >= _legalTimeBreak)
                {
                    oir.TsStop = oir.TsStop.Add(timeBreak);
                    bStartStop = false;
                }
            }
            oir.Distance = Math.Round(oir.Distance, 2);
            if ((oir.ZoneId > 0) || (oir.FieldId > 0) || (oir.ZoneId == Consts.ZONE_OUTSIDE_THE_LIST)) oir.Square = 100 * oir.Distance * Convert.ToDouble(oir.WidthAgr) * 0.001;
            oir.TsMove = (oir.GpsDataZone[oir.GpsDataZone.Count - 1].Time - oir.GpsDataZone[0].Time - oir.TsStop);
        }
        /// <summary>
        /// вычисление расхода топлива по датчику уровня топлива одной записи наряда
        /// </summary>
        /// <param name="oir"></param>
        /// <param name="PA"></param>
        private void ContentCalcParamsDUT(ref OrderItemRecord oir, PartialAlgorithms PA)
        {
            if ((oir.GpsDataZone.Count == 0) || (fuelDict.ValueFuelSum.Count == 0)) return;
            //Топлива в начале, л
            oir.FuelStart = Math.Round(fuelDict.ValueFuelSum[oir.GpsDataZone[0].Id].value, 2);
            //Топлива в конце, л
            oir.FuelEnd = Math.Round(fuelDict.ValueFuelSum[oir.GpsDataZone[oir.GpsDataZone.Count - 1].Id].value, 2);
            //GpsData[] gpsData = new GpsData[oir.GpsDataZone.Count];
            //oir.GpsDataZone.CopyTo(gpsData);
            LocalCache.atlantaDataSet.FuelReportRow[] FRRows = PA.GetFuelReportRow(_Mobitel_Id, oir.GpsDataZone[0].Time, oir.GpsDataZone[oir.GpsDataZone.Count-1].Time);
            oir.FuelAdd = 0;
            oir.FuelSub = 0;
            ContentCalcParamsDUTFuelAddSub(oir, FRRows);
            oir.RecalcFuelExpensDut();
        }
        /// <summary>
        /// определение заправок / сливов топлива по датчику ДУТ
        /// </summary>
        /// <param name="oir"></param>
        /// <param name="FRRows"></param>
        private void ContentCalcParamsDUTFuelAddSub(OrderItemRecord oir, LocalCache.atlantaDataSet.FuelReportRow[] FRRows)
        {
            List<int> recordsUsedToReplaceData = new List<int>();
            OrderFueling of = new OrderFueling(_ID);
            double valueFuel = 0;
            if (FRRows.Length > 0)
            {
                foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                {
                    if (tmp_FR_row.dValue != 0)
                    {
                        valueFuel = 0;
                        int recordId = 0;
                        if (of.IsExistLockedValueToTime(_ofs, tmp_FR_row.time_, out valueFuel, out recordId))
                        {
                            if (recordId > 0) recordsUsedToReplaceData.Add(recordId);
                            ContentCalcParamsDUTFuelAddSubValueAdd(oir, valueFuel);
                        }
                        else
                        {
                            ContentCalcParamsDUTFuelAddSubValueAdd(oir, tmp_FR_row.dValue);
                            of.AddRecord(new PointLatLng(tmp_FR_row.Lat, tmp_FR_row.Lon),
                                tmp_FR_row.time_, tmp_FR_row.beginValue, tmp_FR_row.dValue, tmp_FR_row.dValue, tmp_FR_row.Location);
                        }
                    }
                }
            }
            // добавленные диспетчером записи о заправках / сливах
            foreach (OrderFueling ofl in _ofs)
            {
                if (!recordsUsedToReplaceData.Contains(ofl.Id))
                {
                    if (ofl.DateFueling >= oir.TimeStart & ofl.DateFueling <= oir.TimeEnd)
                    {
                        ContentCalcParamsDUTFuelAddSubValueAdd(oir, ofl.ValueDisp);
                    }
                }
            }
        }

        private static void ContentCalcParamsDUTFuelAddSubValueAdd(OrderItemRecord oir, double valueFuel)
        {
            if (valueFuel > 0)
                oir.FuelAdd += valueFuel;
            else
                oir.FuelSub += -valueFuel;
        }

        /// <summary>
        /// вычисление расхода топлива по датчику расхода топлива одной записи наряда
        /// </summary>
        /// <param name="oir"></param>
        /// <param name="PA"></param> 
        private void ContentCalcParamsDRT(ref OrderItemRecord oir, PartialAlgorithms PA)
        {
            //GpsData[] tmp = new GpsData[oir.GpsDataZone.Count];
            //oir.GpsDataZone.CopyTo(tmp);
            if (oir.GpsDataZone.Count > 0)
            {
                //GpsData[] dataRows = DataSetManager.ConvertDataviewRowsToDataGpsArray(tmp);
                // Расход топлива в движении, л
                oir.FuelExpensMove = Math.Round(PA.GetFuelUseInMotionDrt(oir.GpsDataZone.ToArray(), 0), 2);
                // Расход топлива на стоянках, л
                oir.FuelExpensStop = Math.Round(PA.GetFuelUseInStopDrt(oir.GpsDataZone.ToArray(), 0, 0), 2);
                // Общий расход топлива в пути, л
                oir.FuelExpensTotal = Math.Round(oir.FuelExpensMove + oir.FuelExpensStop, 2);
                // Расход топлива на гектар, л
                if (oir.SquareCalcCont > 0)
                {
                    oir.Fuel_ExpensAvg = Math.Round(oir.FuelExpensTotal / oir.SquareCalcCont, 2);
                }
                // Расход топлива в моточас, л
                Double dbHMoto = oir.TsEngineOn.TotalHours;
                if (dbHMoto > 0)
                {
                    oir.Fuel_ExpensAvgRate = Math.Round(oir.FuelExpensTotal / dbHMoto, 2);
                }

            }
        }
        /// <summary>
        /// удаление содержимого наряда
        /// </summary>
        /// <returns></returns>
        private bool ContentDelete()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    Sql = string.Format(AgroQuery.OrderItemContent.DeleteFromAgroOrderT, _ID);
                    driverDb.ExecuteNonQueryCommand(Sql);
                }
                OrderFueling of = new OrderFueling(_ID);
                of.ClearRecords();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// итоговая строка об обработанных площадях для PropertyGrid формы наряда
        /// </summary>
        /// <returns></returns>
        public void GetSquareWork()
        {
            SquareWorkDescript = "";
            SquareWorkDescriptOverlap = "";
            _factSquareCalcOverlap = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.OrderItemContent.SelectCountAgroOrderT, _ID);

                driverDb.GetDataReader(Sql);
                double dbSquare = 0;
                double squareOverlap = 0;
                double dbDut = 0;
                double dbDrt = 0;
                while (driverDb.Read())
                {
                    dbDut = dbDut + (double)TotUtilites.NdbNullReader(driverDb, "FEDUT", 0);
                    dbDrt = dbDrt + (double)TotUtilites.NdbNullReader(driverDb, "FEDRT", 0);

                    if (driverDb.GetDouble("Square") > 0)
                    {
                        dbSquare += driverDb.GetDouble("SS");
                        squareOverlap = Math.Min(CalcSquareOverlap(driverDb), driverDb.GetDouble("SS"));
                        _factSquareCalcOverlap += squareOverlap;
                        string workNamesList = GetWorkNamesListForField(driverDb.GetInt32("Zone_ID"));
                        if (workNamesList.Length > 0) workNamesList = string.Format("({0})", workNamesList);
                        SquareWorkDescript = string.Format("{0} {1} {2} : {3}({4}%) {5}", SquareWorkDescript, SquareWorkDescript.Length > 0 ? "   " : "", driverDb.GetString("Name"),
                            driverDb.GetDouble("SS"), Math.Round(100 * (driverDb.GetDouble("SS") / (100 * driverDb.GetDouble("Square"))), 2), workNamesList);
                        SquareWorkDescriptOverlap = string.Format("{0} {1} {2} : {3}({4}%) {5}", SquareWorkDescriptOverlap, SquareWorkDescriptOverlap.Length > 0 ? "   " : "", driverDb.GetString("Name"),
                        squareOverlap, Math.Round(100 * (squareOverlap / (100 * driverDb.GetDouble("Square"))), 2), workNamesList);
                    }
                }
                driverDb.CloseDataReader();
                _FuelDUTExpensSquare = Math.Round(dbDut, 2);
                _FuelDRTExpensSquare = Math.Round(dbDrt, 2);
                if (squareOverlap > 0)
                {
                    _FuelDUTExpensAvgSquare = Math.Round(dbDut / squareOverlap, 2);
                    _FuelDRTExpensAvgSquare = Math.Round(dbDrt / squareOverlap, 2);
                }
                else if (dbSquare > 0)
                {
                    _FuelDUTExpensAvgSquare = Math.Round(dbDut / dbSquare, 2);
                    _FuelDRTExpensAvgSquare = Math.Round(dbDrt / dbSquare, 2);
                }
                else
                {
                    _FuelDUTExpensAvgSquare = 0;
                    _FuelDRTExpensAvgSquare = 0;
                }

            }
        }
        /// <summary>
        /// сложение накладывающихся площадей треков
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private double CalcSquareOverlap(DriverDb driverDb)
        {
            //Проблема: переважно правильною є площа По контуру всего, але коли трактор(оприскувач) протягом наряду двічі обробляв поле ця площа не буде правильною, а заниженою на величину 2 сліду.
            //При чому ця проблема стосується не тільки 2 сліду, але й іншої роботи, яку виконував той самий ТЗ у даній КЗ за період наряду.
            //Думаю варто до критеріїв обчислення площі По контуру всего додати вид робіт
            double squareOverlap = 0;
            if (driverDb.GetInt32("CNT") == 1)
            {
                squareOverlap = driverDb.GetDouble("SS");
            }
            else
            {
                // список всех работ наряда
                List<int> worksOrder = new List<int>();
                foreach (OrderItemRecord oir in _oirs)
                {
                    if (!worksOrder.Contains(oir.WorkId)) worksOrder.Add(oir.WorkId);
                }
                IZone zone = ZonesModel.GetById(driverDb.GetInt32("Zone_ID"));
                foreach (int workId in worksOrder)
                {
                    squareOverlap += CalcSquareOverlapToWork(zone, workId);
                }
            }
            return squareOverlap;
        }

        /// <summary>
        /// сложение накладывающихся площадей треков для одной работы
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private double CalcSquareOverlapToWork(IZone zone, int workId)
        {
            double squareOverlapWork = 0;
            using (var osb = new OrderSquareBitmap((zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK), zone.Points))
            {
                foreach (OrderItemRecord oir in _oirs)
                {
                    if (oir.ZoneId == zone.Id && oir.Confirm == 1 && oir.WorkId == workId)
                    {
                        if (squareOverlapWork == 0)
                            squareOverlapWork = osb.SquareCalcBitmap(oir, false);
                        else
                            squareOverlapWork = osb.SquareCalcAddTrack(oir);
                    }
                }
            }
            return squareOverlapWork;
        }


        private double CalcSquareOverlapDriverAgregat(int zoneId, int driverId, int agregatId)
        {
            double squareOverlap = 0;
            // список всех работ наряда
            var worksOrder = new List<int>();
            foreach (OrderItemRecord oir in _oirs)
            {
                if (oir.DriverId == driverId && oir.AgregatId == agregatId)
                {
                    if (!worksOrder.Contains(oir.WorkId)) worksOrder.Add(oir.WorkId);
                }
            }
            IZone zone = ZonesModel.GetById(zoneId);
            foreach (int workId in worksOrder)
            {
                squareOverlap += CalcSquareOverlapToWorkDriverAgregat(zone, workId, driverId, agregatId);
            }
            return squareOverlap;
        }

        /// <summary>
        /// сложение накладывающихся площадей треков для одной работы,водителя агрегата
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private double CalcSquareOverlapToWorkDriverAgregat(IZone zone, int workId, int driverId, int agregatId)
        {
            double squareOverlapWork = 0;
            if (zone != null)
            {
                using (var osb = new OrderSquareBitmap((zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK), zone.Points))
                {
                    foreach (OrderItemRecord oir in _oirs)
                    {
                        if (oir.ZoneId == zone.Id && oir.Confirm == 1 && oir.WorkId == workId && oir.DriverId == driverId && oir.AgregatId == agregatId)
                        {
                            if (squareOverlapWork == 0)
                                squareOverlapWork = osb.SquareCalcBitmap(oir, false);
                            else
                                squareOverlapWork = osb.SquareCalcAddTrack(oir);
                        }
                    }
                }
            }
            return squareOverlapWork;
        }

        public double CalcSquareOverlapDriver(int orderId, int driverId)
        {
            double squareOverlap = 0;
            _ID = orderId;
            if (_oirs == null) _oirs = GetOrderItemRecords();
            var worksOrder = new List<string>();
            foreach (OrderItemRecord oir in _oirs)
            {
                if (oir.DriverId == driverId && oir.ZoneId > 0 && oir.Confirm == 1)
                {
                    string value = string.Format("{0} {1}", oir.WorkId, oir.ZoneId);
                    if (!worksOrder.Contains(value))
                    {
                        worksOrder.Add(value);
                    }
                       
                }
            }
            foreach (var item in worksOrder)
            {
                string[] value = item.Split();
                int workId = Convert.ToInt32(value[0]);
                int zoneId = Convert.ToInt32(value[1]);
                IZone zone = ZonesModel.GetById(zoneId);
                squareOverlap += CalcSquareOverlapToWorkDriver(zone, workId, driverId);
            }
            return squareOverlap;
        }

        private double CalcSquareOverlapToWorkDriver(IZone zone, int workId, int driverId)
        {
            double squareOverlapWork = 0;
            using (var osb = new OrderSquareBitmap((zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK), zone.Points))
            {
                foreach (OrderItemRecord oir in _oirs)
                {
                    if (oir.ZoneId == zone.Id && oir.Confirm == 1 && oir.WorkId == workId && oir.DriverId == driverId)
                    {
                        if (squareOverlapWork == 0)
                            squareOverlapWork = Math.Min(osb.SquareCalcBitmap(oir, false),oir.SquareCalcCont) ;
                        else
                            squareOverlapWork = osb.SquareCalcAddTrack(oir);
                    }
                }
            }
            return squareOverlapWork;
        }

        private string GetWorkNamesListForField(int idZone)
        {
            string worksList = "";
            List<string> worksNames = new List<string>();
            foreach (OrderItemRecord oir in _oirs)
            {
                if (oir.ZoneId == idZone && oir.Confirm == 1)
                {
                    string workName = oir.GetWorkName();
                    if (!worksNames.Contains(workName)) worksNames.Add(workName);
                }
            }
            if (worksNames.Count > 0)
            {
                foreach (string workName in worksNames)
                {
                    if (worksList.Length == 0)
                    {
                        worksList = workName;
                    }
                    else
                        worksList = string.Format("{0},{1}", worksList, workName);
                }
            }
            return worksList;
        }

        /// <summary>
        /// пробег между полями
        /// </summary>
        private void GetPathWithoutWork()
        {
            _PathWithoutWork = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.OrderItemContent.SelectSumAgroOrderT, _ID);

                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    double dbPath = driverDb.GetDouble("DS");
                    _PathWithoutWork = dbPath;
                    _FuelDUTExpensWithoutWork = Math.Round(GetTotalFuelDUTExpens() - _FuelDUTExpensSquare, 2);
                    _FuelDRTExpensWithoutWork = (double)TotUtilites.NdbNullReader(driverDb, "FEDRT", 0);
                    if (dbPath > 0)
                    {
                        _FuelDUTExpensAvgWithoutWork = Math.Round(100 * _FuelDUTExpensWithoutWork / dbPath, 2);
                        _FuelDRTExpensAvgWithoutWork = Math.Round(100 * _FuelDRTExpensWithoutWork / dbPath, 2);
                    }
                    else
                    {
                        _FuelDUTExpensAvgWithoutWork = 0;
                        _FuelDRTExpensAvgWithoutWork = 0;
                    }
                }
                driverDb.CloseDataReader();
            }
        }
        /// <summary>
        /// общий расход топлива наряда
        /// </summary>
        /// <returns></returns>
        private double GetTotalFuelDUTExpens()
        {
            DataTable Content = GetFuelDUT();
            if (Content.Rows.Count > 0)
            {
                Double Expense = (double)Content.Rows[0]["FuelStart"] - (double)Content.Rows[Content.Rows.Count - 1]["FuelEnd"];
                Double Add = 0;
                for (int i = 0; i < Content.Rows.Count; i++)
                {
                    if ((double)Content.Rows[i]["FuelAdd"] > 0)
                    {
                        Add += (double)Content.Rows[i]["FuelAdd"];
                    }
                }
                return Math.Round(Expense + Add, 2);
            }
            return 0;
        }

        private void ContentPrepareAlgoritmsData()
        {
            if (m_row == null) return;
            List<GpsData> gpsList = GpsDatasDocItem.ToList();  
            SetStatusEvent(Resources.DataPrepareAlgoritms);
            var brKlm = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Kilometrage();
            //brKlm.SelectItem(m_row);
            brKlm.SelectItem(_Mobitel_Id , gpsList);
            brKlm.Run();
            var brFuel1 = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Fuel(AlgorithmType.FUEL1);
            //brFuel1.SelectItem(m_row);
            brFuel1.SelectItem(_Mobitel_Id, gpsList);
            brFuel1.Run();
            var fuelAlg = new Fuel();
            fuelAlg.SelectItem(_Mobitel_Id, gpsList);
            //fuelAlg.SelectItem(m_row);
            fuelDict = new FuelDictionarys();
            fuelAlg.GettingValuesDUTAlgorithm(fuelDict, GpsDatasDocItem, AlgorithmType.FUEL1);
            //алгоритм подсчета моточасов
            var brRot = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Rotation();
            brRot.SelectItem(_Mobitel_Id, gpsList);
            brRot.SelectItem(m_row);
            brRot.Run();
            var of = new OrderFueling(_ID);
            _ofs = of.GetRecordsLocked();
        }

        private void ContentCreateZonesList()
        {
            dcZonesBitmap.Clear();
            TrackZonesFinder tzFinder = new TrackZonesFinder(MinMax, GpsDatasDocItem);
            foreach (IZone zone in tzFinder.GetZones())
            {
                OrderSquareBitmap osb = new OrderSquareBitmap((zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK), zone.Points);
                if (osb.CreateZoneBitmap()) dcZonesBitmap.Add(zone.Id, osb);
            }
        }


        /// <summary>
        /// запоминание идентификаторов подтверждения выполненных работ
        /// </summary>
        private void ContentCreateConfimedData()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.OrderItemContent.DeleteFromAgroOrdertControl, _ID) + (int)Consts.TypeControlObject.Confirm;

                driverDb.ExecuteNonQueryCommand(Sql);

                Sql = string.Format(AgroQuery.OrderItemContent.SelectAgroOrdertTimeStart, _ID);

                driverDb.GetDataReader(Sql);
                while (driverDb.Read())
                {
                    int Confirm = driverDb.GetInt16("Confirm");
                    if (Confirm == 1)
                    {
                        using (DriverDb nestedDriverDb = new DriverDb())
                        {
                            nestedDriverDb.ConnectDb();
                            Sql = string.Format(AgroQuery.OrderItemContent.InsertIntoAgroOrderControl, _ID, 1,
                                        (int)Consts.TypeControlObject.Confirm);
                           
                            nestedDriverDb.NewSqlParameterArray(2);
                            nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", driverDb.GetDateTime("TimeStart"));
                            nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", driverDb.GetDateTime("TimeEnd"));
                            nestedDriverDb.ExecuteNonQueryCommand(Sql, nestedDriverDb.GetSqlParameterArray);
                        }
                    }
                }
                driverDb.CloseDataReader();
            }
        }
        /// <summary>
        /// возврат общего  AtlantaDataSet в алгоритм и очистка DataSet класса 
        /// </summary>
        public void ContentDataSetReturnClear()
        {
            Algorithm.AtlantaDataSet = _dsFromAlgorithm;
            Algorithm.RunFromModule = false;
            _dsAtlanta.Clear();
            _recordNumber = 0;
            SetStatusEvent(Resources.Ready);
        }
        /// <summary>
        /// список записей наряда, упакованных в объекты
        /// </summary>
        /// <returns></returns>
        public List<OrderItemRecord> GetOrderItemRecords()
        {
            List<OrderItemRecord> oirs = new List<OrderItemRecord>();
            DataTable dtOirs = GetContent();
            FuelDUTAdd = 0;
            FuelDUTSub = 0;
            foreach (DataRow drow in dtOirs.Rows)
            {
                OrderItemRecord oir = new OrderItemRecord((int)drow["Id"]);
                FuelDUTAdd += oir.FuelAdd;
                FuelDUTSub += oir.FuelSub;
                oirs.Add(oir);
            }
            if (oirs.Count > 0)
            {
                FuelDUTAdd = Math.Round(FuelDUTAdd, 1);
                FuelDUTSub = Math.Round(FuelDUTSub, 1); 
                FuelDUTStart = Math.Round(oirs[0].FuelStart,1);
                FuelDUTEnd = Math.Round(oirs[oirs.Count -1].FuelEnd,1);
                FuelDUTExpense = Math.Round(FuelDUTStart + FuelDUTAdd - FuelDUTEnd,1);  
            }
            return oirs;
        }

    }
}
