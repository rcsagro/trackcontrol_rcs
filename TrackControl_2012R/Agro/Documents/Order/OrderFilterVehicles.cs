﻿using System.Collections.Generic;
using System.Data;
using TrackControl.General;
using TrackControl.Vehicles; 

namespace Agro
{
    public static class OrderFilterVehicles
    {

        public static void SetFilter(ref DataTable dtOrder)
        {
            if (!UserBaseCurrent.Instance.Admin && dtOrder.Rows.Count > 0)
            {
                IList<Vehicle> vehicles = OrderItem.VehiclesModel.Vehicles;
                List<DataRow> deleteRows = new List<DataRow>();
                foreach (DataRow drow in dtOrder.Rows)
                {
                    if (!IsVehiclePermit(vehicles, (int)drow["Id_mobitel"]))
                        deleteRows.Add(drow);
                }
                foreach (DataRow drow in deleteRows)
                {
                    dtOrder.Rows.Remove(drow);
                }

            }
        }


        private static bool IsVehiclePermit(IList<Vehicle> vehicles, int Id_mobitel)
        {
            foreach (Vehicle veh in vehicles)
            {
                if (veh.MobitelId == Id_mobitel)
                    return true;
            }
            return false;
        }
    }
}
