using System.Windows.Forms.VisualStyles;
using Agro.Dictionaries;
using Agro.Utilites;
using GeoData;
using System;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;


namespace Agro
{
    /// <summary>
    /// ������ ������
    /// </summary>
    public class OrderItemRecord:IDisposable
    {
        public List<GpsData> GpsDataZone;
        public int ZoneId;
        public int DriverId;
        public int AgregatId;
        public int FieldId;
        public int IdOrderT;
        public int IdMain;
        public Double Distance;
        public Double Square;
        public Double SquareCalcCont;
        public TimeSpan TsMove;
        public TimeSpan TsStop;
        public TimeSpan TsEngineOn;
        public int RowHandle;
        public bool AddNewRow;
        public int Confirm = 0;
        public double WidthAgr;
        public int WorkId;
        public double Price;
        public double FuelStart;
        public double FuelEnd;
        public double FuelAdd;
        public double FuelSub;
        public double FuelExpens;
        public double FuelExpensAvg;
        public double FuelExpensAvgRate;
        public double FuelExpensMove;
        public double FuelExpensStop;
        public double FuelExpensTotal;
        public double Fuel_ExpensAvg;
        public double Fuel_ExpensAvgRate;
        public List<TRealPoint> Rpoints;
        public int PointsValidity;
        public DateTime TimeStart;
        public DateTime TimeEnd;
        public AgrDrvInputSources AgrInputSource;
        public AgrDrvInputSources DrvInputSource;
        public int BreakingPointId;
        public int BreakingPointWorkId;

        public OrderItemRecord()
        {

        }

        public OrderItemRecord(int idRecord)
        {
                IdOrderT = idRecord;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();

                    string sSql = string.Format(AgroQuery.OrderItemRecord.SelectAgroOrdert ,IdOrderT);

                    driverDb.GetDataReader(sSql);

                    if (driverDb.Read())
                    {
                        IdMain = (int) TotUtilites.NdbNullReader(driverDb, "Id_main", 0);
                        ZoneId = (int) TotUtilites.NdbNullReader(driverDb, "Id_zone", 0);
                        DriverId = (int) TotUtilites.NdbNullReader(driverDb, "Id_driver", 0);
                        AgregatId = (int) TotUtilites.NdbNullReader(driverDb, "Id_agregat", 0);
                        GetAgregatWidthWork();
                        FieldId = (int)TotUtilites.NdbNullReader(driverDb, "Id_field", 0);
                        Square = (double) TotUtilites.NdbNullReader(driverDb, "FactSquare", 0);
                        PointsValidity = (int) TotUtilites.NdbNullReader(driverDb, "PointsValidity", 0);
                        WorkId = (int) TotUtilites.NdbNullReader(driverDb, "Id_work", 0);
                        Confirm = Convert.ToInt32(TotUtilites.NdbNullReader(driverDb, "Confirm", 0));
                        TsEngineOn = (TimeSpan)TotUtilites.NdbNullReader(driverDb, "TimeRate", TimeSpan.Zero);
                        FuelExpens = (double) TotUtilites.NdbNullReader(driverDb, "FuelExpens", 0);
                        SquareCalcCont = (double) TotUtilites.NdbNullReader(driverDb, "FactSquareCalc", 0);
                        FuelExpensAvg = (double) TotUtilites.NdbNullReader(driverDb, "FuelExpensAvg", 0);
                        FuelExpensAvgRate = (double) TotUtilites.NdbNullReader(driverDb, "FuelExpensAvgRate", 0);
                        TimeStart = (DateTime)TotUtilites.NdbNullReader(driverDb, "TimeStart", DateTime.MinValue);
                        TimeEnd = (DateTime)TotUtilites.NdbNullReader(driverDb, "TimeEnd", DateTime.MinValue);
                        FuelStart = (double) TotUtilites.NdbNullReader(driverDb, "FuelStart", 0);
                        FuelEnd = (double) TotUtilites.NdbNullReader(driverDb, "FuelEnd", 0);
                        FuelAdd = (double) TotUtilites.NdbNullReader(driverDb, "FuelAdd", 0);
                        FuelSub = (double) TotUtilites.NdbNullReader(driverDb, "FuelSub", 0);
                        
                        int source = Convert.ToInt16(TotUtilites.NdbNullReader(driverDb, "AgrInputSource", 0));
                        AgrInputSource = (AgrDrvInputSources) source;
                        source = Convert.ToInt16(TotUtilites.NdbNullReader(driverDb, "DrvInputSource", 0));
                        DrvInputSource = (AgrDrvInputSources) source;
                        CompressData cd = new CompressData("agro_datagps", IdOrderT);
                        Rpoints = cd.DeCompressReal();

                        if (WidthAgr == 0)
                            SetAgregatWidth();
                    }
                    driverDb.CloseDataReader();
                }

        }

        public OrderItemRecord(int iZoneId, int iDriverId, int iAgregatId, AgrDrvInputSources agrInputSource, AgrDrvInputSources drvInputSource, List<GpsData> gpsDataZone)
        {
            ZoneId = iZoneId;
            DriverId = iDriverId;
            AgregatId = iAgregatId;
            AgrInputSource = agrInputSource;
            DrvInputSource = drvInputSource;
            if (ZoneId == Consts.ZONE_OUTSIDE_THE_LIST)
                FieldId = Consts.ZONE_OUTSIDE_THE_LIST;
            else
                FieldId = GetFieldId(ZoneId);
            GpsDataZone = gpsDataZone;
            SetAgregatWidth();
            ConvertDRowsRPoints();
        }

        public OrderItemRecord(int mainID, int fieldID, int driverID, int agregatId, int workId, DateTime start, DateTime end, AgrDrvInputSources agrInputSource, AgrDrvInputSources drvInputSource)
        {
            
            IdMain = mainID;
            FieldId = fieldID;
            using (DictionaryAgroField daf = new DictionaryAgroField(FieldId))
            {
                ZoneId = daf.IdZone;
            }
            DriverId = driverID;
            AgregatId = agregatId;
            AgrInputSource = agrInputSource;
            DrvInputSource = drvInputSource;
            WorkId = workId;
            TimeStart = start;
            TimeEnd = end;
        }

        public void ConvertDRowsRPoints()
        {
            Rpoints = new List<TRealPoint>();
            for (int i = 0; i < GpsDataZone.Count; i++)
            {
                TRealPoint rpoint = new TRealPoint();
                rpoint.X = GpsDataZone[i].LatLng.Lng;
                rpoint.Y = GpsDataZone[i].LatLng.Lat;
                Rpoints.Add(rpoint);
            }
        }

        public void SetAgregatWidth()
        {
            if (AgregatId == 0)
            {
                WidthAgr = AgroController.Instance.WidthAgregatDefault; 
            }
            if (AgregatId != 0)
            {
                GetAgregatWidthWork();
            }

        }

        /// <summary>
        /// ��� �������� �� ��������������
        /// </summary>
        /// <param name="Identifier">����� - �������� ���, ���� - ��� ��������������</param>
        /// <returns></returns>
        private int GetAgregatId(int Identifier)
        {
            if (Identifier < 0)
            {
                return -Identifier;
            }
            else
            {
                using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat())
                {
                    daa.InitFromIdentifier((ushort)Identifier);
                    return daa.Id;
                }
            }
        }

        /// <summary>
        /// ������ ��������
        /// </summary>
        /// <param name="iAgregatId"></param>
        /// <returns></returns>
        private void GetAgregatWidthWork()
        {
            using (var daa = new DictionaryAgroAgregat(AgregatId))
            {
                WidthAgr = daa.Width;
                WorkId = daa.IdWork;
            }
        }

        public void SetFuelDut()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 

                string sSqlUpdate = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrdert, IdOrderT, driverDb.ParamPrefics);

                driverDb.NewSqlParameterArray(5);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelAdd", FuelAdd);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelSub", FuelSub);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpens", FuelExpens);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpensAvg", FuelExpensAvg);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpensAvgRate", FuelExpensAvgRate);
                driverDb.ExecuteNonQueryCommand(sSqlUpdate, driverDb.GetSqlParameterArray);
            }
        }

        public void RecalcFuelExpensDut()
        {
            //����� ������, �
            FuelExpens = Math.Round((FuelStart + FuelAdd - FuelEnd), 2);
            ////����������� �������, (��)
            //oir.Square = oir.Distance * dbWidth * 0.001 * 100;
            // ������ ������� �� ������, �
            if (SquareCalcCont > 0)
            {
                FuelExpensAvg = Math.Round(FuelExpens / SquareCalcCont, 2);
            }
            // ������ ������� � �������, �
            Double dbHMoto = TsEngineOn.TotalHours;
            if (dbHMoto > 0)
            {
                FuelExpensAvgRate = Math.Round(FuelExpens / dbHMoto, 2);
            }
        }

        public void SetPointsValidity()
        {
            PointsValidity = 0;
            if (GpsDataZone.Count > 0)
            {
                int iLogID_F = GpsDataZone[0].LogId;
                int iLogID_L = GpsDataZone[GpsDataZone.Count - 1].LogId;
                if ((iLogID_L - iLogID_F) > 0)
                {
                    double dbCalc = (double)(GpsDataZone.Count - 1) / (iLogID_L - iLogID_F);
                    PointsValidity = (int)(Math.Round(dbCalc * 100, 0));
                }

            }
        }

        public bool LockRecord(bool lockRecord)
        {
            try
            {
                if ((lockRecord && DocItem.GetPointsValidityGauge(PointsValidity) == (int)ValidityColors.Green) || !lockRecord)
                {
                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb(); 
                        string sql = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrdertLockRecord, lockRecord ? 1 : 0, IdOrderT);
                        driverDb.ExecuteNonQueryCommand(sql);
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// ������������� ������ ������ �� ����
        /// </summary>
        /// <param name="id_main"></param>
        /// <param name="dateCompare"></param>
        /// <returns></returns>
        public static int GetIdByDate(int id_main, DateTime dateCompare)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(AgroQuery.OrderItemRecord.SelectIdAgroOrdert, id_main, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateCompare", dateCompare);
                    return driverDb.GetScalarValueIntNull(sql, driverDb.GetSqlParameterArray);
                }
            }catch
            {
                return ConstsGen.RECORD_MISSING;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Rpoints != null)
                Rpoints.Clear();
            //if (GpsDataZone != null) GpsDataZone.Clear(); 
        }

        #endregion

        public int AddRecord()
        {
            int idT ;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                int confirm = FieldId == 0 ? 0 : Confirm;
                SetDefaultWork();
                string sqlInsert = string.Format(AgroQuery.OrderItemRecord.InsertIntoAgroOrdert, IdMain, FieldId, ZoneId,
                                                 DriverId, AgregatId, confirm, WorkId, driverDb.ParamPrefics, (int)AgrInputSource, (int)DrvInputSource,BreakingPointId);
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactTime", (TimeEnd > TimeStart) ? TimeEnd.Subtract(TimeStart) : TimeSpan.Zero);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", TimeStart);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", TimeEnd);
                idT = driverDb.ExecuteReturnLastInsert(sqlInsert, driverDb.GetSqlParameterArray, "agro_ordert");
            }
            return idT;
        }

        private void SetDefaultWork()
        {
            if (BreakingPointWorkId > 0)
                WorkId = BreakingPointWorkId;
            else if (FieldId == 0)
                WorkId = DictionaryAgroWorkType.GetFirstWorkWithType(DictionaryAgroWorkType.WorkTypes.MoveOutField);
            else if (Confirm == 0)
            {
                WorkId = DictionaryAgroWorkType.GetFirstWorkWithType(DictionaryAgroWorkType.WorkTypes.MoveInField);
            }
        }

        public bool UpdateRecord()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrderT, IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(22);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeRate", TsEngineOn);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStop", TsStop);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeMove", TsMove);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Distance", Math.Round(Distance, 3));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactSquare", Math.Round(Square, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactSquareCalc", Math.Round(SquareCalcCont, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Sum", Math.Round(Square * Price, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SpeedAvg", TsMove.TotalSeconds == 0 ? 0 : Math.Round(Distance / (TsMove.TotalSeconds / 3600), 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelStart", FuelStart);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelAdd", Math.Round(FuelAdd,2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelSub", Math.Round(FuelSub,2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelEnd", FuelEnd);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpens", FuelExpens);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpensAvg", FuelExpensAvg);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelExpensAvgRate", FuelExpensAvgRate);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Fuel_ExpensMove", FuelExpensMove);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Fuel_ExpensStop", FuelExpensStop);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Fuel_ExpensTotal", FuelExpensTotal);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Fuel_ExpensAvg", Fuel_ExpensAvg);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Fuel_ExpensAvgRate", Fuel_ExpensAvgRate);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsValidity", PointsValidity);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Id_main", IdMain);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteRecord()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format(AgroQuery.OrderItemRecord.DeleteFromAgroOrdert, IdOrderT);
                    IdOrderT = 0;
                    driverDb.ExecuteNonQueryCommand(sql);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateRecordTimeStart()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string _sSql = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrderTTimeStart, IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", TimeStart);
                    driverDb.ExecuteNonQueryCommand(_sSql, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateRecordTimeEnd()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string _sSql = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrderTTimeEnd, IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", TimeEnd);
                    driverDb.ExecuteNonQueryCommand(_sSql, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateFactSquareJointProcessing(decimal factSquareJointProcessing)
        {
            try
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSql = string.Format(AgroQuery.OrderItemRecord.UpdateFactSquareJointProcessing, IdOrderT, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactSquareJointProcessing", factSquareJointProcessing);
                    driverDb.ExecuteNonQueryCommand(sSql, driverDb.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateControlParameters()
        {
            try
            {
                if (FieldId == 0)
                    ZoneId = 0;
                else
                {
                    using (DictionaryAgroField daf = new DictionaryAgroField(FieldId))
                    {
                        ZoneId = daf.IdZone;
                    }
                }
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format(AgroQuery.OrderItemRecord.UpdateAgroOrderTIdField, FieldId, ZoneId, DriverId, AgregatId, WorkId, IdOrderT);
                    driverDb.ExecuteNonQueryCommand(sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private int GetFieldId(int ZoneId)
        {
            if (ZoneId < 0)
            {
                return -ZoneId;
            }
            else if (ZoneId == 0)
            {
                return 0;
            }
            else
            {
                using (DictionaryAgroField daf = new DictionaryAgroField())
                {
                    return daf.GetIdFromZoneId(ZoneId);
                }
            }
        }

        /// <summary>
        /// ���� ������� ��������� ����
        /// </summary>
        /// <param name="ID_Field"></param>
        /// <returns></returns>
        private int GetZone(int ID_Field)
        {
            using (DictionaryAgroField daf = new DictionaryAgroField(ID_Field))
            {
                return daf.IdZone;
            }
        }

        /// <summary>
        /// �������� ������,����������� � ������ ������
        /// </summary>
        public string GetWorkName()
        {
            using (DictionaryAgroWorkType dawt = new DictionaryAgroWorkType(WorkId))
            {
                return dawt.Name;
            }
        }
    }
}
