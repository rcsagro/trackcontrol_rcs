﻿using Agro.Dictionaries;
using Agro.Documents.Order;
using Agro.Properties;
using Agro.Utilites;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.Reports;
using TrackControl.Vehicles; 

namespace Agro
{
    partial class OrderItem 
    {
        /// <summary>
        /// разбивка данных GPS на записи наряда по границам полей
        /// </summary>
        private void CI_ContentIdentify()
        {
            //нарезка дня телетрека по полям и водителям с момента начала движения до момента окончания движения
            _oirs = new List<OrderItemRecord>();
            _cid = new CiData();
            _agregatIdent = new Dictionary<ulong, int>();
            _driverIdent = new Dictionary<ulong, int>();
            _oirSensors = new Dictionary<int, ExternalDevice>();
            _zones_fields = new Dictionary<int, int>();
            //LocalCache.atlantaDataSet.dataviewRow[] GpsDatas = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + _Mobitel_Id, "time");
            CI_PrepareControlData();
            SetPointsIntervalMax(GpsDatasDocItem);
            SetPointsValidity(GpsDatasDocItem);
            GpsData gpsDataStart = GpsDatasDocItem[0];
            GpsData gpsDataPrev = GpsDatasDocItem[0];
            GpsData gpsDataLast = GpsDatasDocItem[GpsDatasDocItem.Length - 1];
            _lastGpsTime = gpsDataLast.Time;
            _cid.dtObjChange = GpsDatasDocItem[0].Time;
            //10.09.2016 Алтухов
            //Добрый день! Вопрос следующий в версии 1.48.10.0 было заявлено - АГРО: в наряд теперь попадают точки не с начала первого движения, а с начала времени наряда;
            //Но вот конец попрежнему  обрезает по остановке
            //тоесть встал трактор в 5 утра иобрезался наряд по 5 утра
            //отрезаем стоянку в конце суток
            //gpsDataLast = CI_CutStopsInTrackEnd(gpsDataLast);
            // индикатор начала движения
            _cid.StartMove = false;
            SetStatusEvent(Resources.OrderRecordsDefine);
            //список записей DataGps, принадлежащих зоне
            var gpsDataZone = new List<GpsData>();
            //буферный список DataGps при дребезге
            var gpsDataBuf = new List<GpsData>();
            foreach (GpsData gpsData in GpsDatasDocItem)
            {
                if (!_cid.StartMove)
                {
                    //в наряд попадают данные с момента старта движения
                    // 08.07.2016 решили включать все точки с момента начала наряда
                    //if (gpsData.Speed > 0)
                    //{
                        CI_SetStartPoint(ref gpsDataStart, gpsDataPrev, gpsDataZone, gpsData);
                    //}
                }
                if (_cid.StartMove)
                {
                    CI_SetDriverAgregZoneLockingForPointGps(false, gpsData);
                    if (CI_IsPointLocked())
                    {
                        //для случая если залоченная запись последняя
                        if (gpsData.Id == gpsDataLast.Id)
                        {
                            gpsDataZone.Clear();
                            break;
                        }
                        else
                        {
                            // для связи между последней точкой лоченой записи и первой нелоченной (CI_SetLinkedPoint)
                            gpsDataZone.Add(gpsData);
                            continue;
                        }
                    }
                    if (_cid.PrevZone > 0)
                    {
                        if (dcZonesBitmap.ContainsKey(_cid.PrevZone ) &&  dcZonesBitmap[_cid.PrevZone].PointInZone(gpsData.LatLng.Lng, gpsData.LatLng.Lat))
                        {
                            _cid.CurZone = _cid.PrevZone;
                        }
                    }
                    // для режима с учетом управляющих данных если зоны нет - новую не ищем (так можно управляющими данными регулировать пробеги) 
                    if (_cid.CurZone == 0 && !_Control)
                    {
                        _cid.CurZone = CI_DefineZone(_cid.CurZone, gpsData);
                    }
                    //управление нарезкой записей с помощью внешнего датчика
                    CI_SetSensorZoneAgregat(gpsData);
                    // накапливаем время движения 
                    if (gpsData.Speed > 0)
                        _cid.TsTimeMoveAfterChangeRecord = _cid.TsTimeMoveAfterChangeRecord.Add(gpsData.Time.Subtract(gpsDataPrev.Time));
                    //определение смены поля , водителя или агрегата
                    CI_SetObjChange(gpsDataZone, gpsDataBuf, gpsData);
                    // дребезг работает по накопительному принципу.Если измененеие зоны происходит с Т < tsDeltaField,
                    //то новая запись не образуется.После повторной смены зоны c Т1 < tsDeltaField дальше анализируется уже (Т + Т1) < tsDeltaField 
                    if ((_cid.ObjChange) || (gpsData.Id == gpsDataLast.Id) || _bps.IsBreakingPoint(gpsData.Time))
                    {
                        if (gpsData.Id == gpsDataLast.Id)
                        {
                            _cid.StopMove = true;
                            gpsDataPrev = gpsData;
                        }
                        if ((gpsDataStart.Id != gpsDataPrev.Id) && ((_cid.ObjChange && CI_TestBounce(gpsData.Time, gpsDataPrev.Time)) || _bps.IsBreakingPointWork(gpsData.Time)))
                        {

                            if (ContentCreateRecord(gpsDataZone))
                            {
                                CI_SetLinkedPoint(gpsDataZone);
                                CI_CopyFromBuferList(gpsDataZone, gpsDataBuf);
                                CI_SetCurrentToPrev(ref gpsDataStart, gpsDataBuf, gpsData);
                                if (_cid.CurLocked) gpsDataZone.Clear();
                                CI_SetBreakPointWork(gpsData);
                            }
                        }
                        if (_cid.StopMove) break;
                    }
                    gpsDataPrev = gpsData;
                    _cid.PrevLocked = _cid.CurLocked;
                    Application.DoEvents();
                }
            } // foreach
            //запись последнего трека
            if (gpsDataZone.Count > 0)
            {
                CI_CopyFromBuferList(gpsDataZone, gpsDataBuf);
                ContentCreateRecord( gpsDataZone);
            }
            //обновление полного списка записей наряда - из-за наличия залоченных записей
            if ((_dtLock.Rows.Count != 0)) _oirs = GetOrderItemRecords();
            SetStatusEvent(Resources.DataCleaning);
            if (dcZonesBitmap.Count > 0)
            {
                foreach (KeyValuePair<int, OrderSquareBitmap> kvp in dcZonesBitmap)
                {
                    kvp.Value.Dispose();
                }
                GC.Collect();
            }
        }

        private void CI_SetBreakPointWork(GpsData gpsData)
        {
            if (_bps.IsBreakingPointWork(gpsData.Time))
            {
                var bp = _bps.GetBreakingPoint();
                if (_cid.BreakingPointId == 0)
                {
                    _cid.BreakingPointId = bp.Id;
                    _cid.BreakingPointWorkId = bp.IdWork;
                }
                else
                {
                    _cid.BreakingPointId = 0;
                    _cid.BreakingPointWorkId = 0;
                }
            }
        }

        private bool CI_IsPointLocked()
        {
            return (_cid.PrevLocked & _cid.CurLocked);
        }

        /// <summary>
        /// связь между записями должна быть непрерывной.Последующая запись должна начинаться с предыдущей  с нулевым пробегом 
        /// </summary>
        /// <param name="lsDrows"></param>
        /// <param name="gpsDataZone"></param>
        private static void CI_SetLinkedPoint(List<GpsData> gpsDataZone)
        {
            if (gpsDataZone.Count > 1)
            {
                gpsDataZone.RemoveRange(0, gpsDataZone.Count - 1);
                gpsDataZone[0].Dist  = 0;
            }
        }

        private void CI_SetCurrentToPrev(ref GpsData gpsDataStart, List<GpsData> gpsDataBuf, GpsData gpsData)
        {
            if (gpsDataStart == null) throw new ArgumentNullException("gpsDataStart");
            gpsDataBuf.Clear();
            CI_SetCurrentToPrevZDA();
            _cid.PrevLocked = _cid.CurLocked;
            gpsDataStart = gpsData;
            SetStatusEvent(Resources.OrderRecordsDefine);
            _cid.ObjChange = false;
            _cid.ZoneChange = false;
            _cid.TsTimeMoveAfterChangeRecord = TimeSpan.Zero;
            _cid.TsTimeMoveAfterSensorNoActive = TimeSpan.Zero;
            _cid.IsSensorSetNoActive = false;
        }

        private void CI_SetObjChange(List<GpsData> gpsDataZone, List<GpsData> gpsDataBuf, GpsData gpsData) 
        {
            if (_cid.firstRecord)
            {
                _cid.firstRecord = false;
                CI_SetCurrentToPrevZDA();
                if (!gpsDataZone.Contains(gpsData))
                {
                    gpsDataZone.Add(gpsData);
                }
            }
            else
            {
                if (CI_IsPointLockedLast())
                {
                    //cid.curZone = CI_DefineZone(cid.curZone, gpsData);
                    CI_SetCurrentToPrevZDA();
                    _cid.TsTimeMoveAfterChangeRecord = TimeSpan.Zero;
                    _cid.TsTimeMoveAfterSensorNoActive = TimeSpan.Zero;
                    CI_SetLinkedPoint(gpsDataZone);
                }
                if ((_cid.CurZone != _cid.PrevZone) || (_cid.CurDriver != _cid.PrevDriver) || (_cid.CurAgreg != _cid.PrevAgreg) || CI_IsPointLockedFirst())
                {
                    if (!_cid.ObjChange)
                    {
                        _cid.ObjChange = true;
                        _cid.ZoneChange = _cid.CurZone != _cid.PrevZone;
                        _cid.dtObjChange = gpsData.Time;
                        _cid.TsTimeMoveAfterChangeRecord = TimeSpan.Zero;
                        _cid.TsTimeMoveAfterSensorNoActive = TimeSpan.Zero;
                        _cid.ChangeZone = _cid.CurZone;
                        if (!gpsDataZone.Contains(gpsData))
                        {
                            gpsDataZone.Add(gpsData);
                        }
                    }
                    else
                    {
                        gpsDataBuf.Add(gpsData);
                    }
                }
                else
                {
                    _cid.ObjChange = false;
                    _cid.ZoneChange = false;
                    if (gpsDataBuf.Count > 0)
                    {
                        CI_CopyFromBuferList(gpsDataZone, gpsDataBuf);
                        gpsDataBuf.Clear();
                    }
                    if (!gpsDataZone.Contains(gpsData))
                    {
                        gpsDataZone.Add(gpsData);
                    }
                }
            }
        }

        private void CI_SetCurrentToPrevZDA()
        {
            _cid.PrevZone = _cid.CurZone;
            _cid.PrevDriver = _cid.CurDriver;
            _cid.PrevAgreg = _cid.CurAgreg;
            _cid.PrevAgrInputSource = _cid.CurAgrInputSource;
            _cid.PrevDrvInputSource = _cid.CurDrvInputSource;
        }

        private bool CI_IsPointLockedLast()
        {
            return (!_cid.CurLocked & _cid.PrevLocked);
        }

        private bool CI_IsPointLockedFirst()
        {
            return (_cid.CurLocked & !_cid.PrevLocked);
        }

        private void CI_SetStartPoint(ref GpsData gpsDataStart, GpsData gpsDataPrev,List<GpsData>gpsDataZone, GpsData gpsData)
        {
            _cid.StartMove = true;
            gpsDataStart = gpsData;
            //зона , водитель, агрегат первой точки
            CI_SetDriverAgregZoneLockingForPointGps(true, gpsData);
            if (_cid.PrevZone == 0)
            {
                _cid.PrevZone = CI_DefineZone(_cid.PrevZone, gpsData);
                if (_cid.PrevZone > 0 && !gpsDataZone.Contains(gpsDataPrev))
                {
                    if (gpsDataPrev.Speed > 0) gpsDataZone.Add(gpsDataPrev);
                }
            }
            else
            {
                // добавление точки перед зоной для режима с учетом управляющих данных
                if (gpsDataPrev.Speed > 0) gpsDataZone.Add(gpsDataPrev);
            }
            CI_SetBreakPointWork(gpsDataStart);
        }

        private int CI_DefineZone(int curZone, GpsData gpsData)
        {
            foreach (KeyValuePair<int, OrderSquareBitmap> kvp in dcZonesBitmap)
            {
                if (kvp.Value.PointInZone(gpsData.LatLng.Lng, gpsData.LatLng.Lat))
                {
                    curZone = kvp.Key;
                    break;
                }
            }
            return curZone;
        }

        private void CI_PrepareControlData()
        {
            // дребезг карточки водителя  и агрегата + выезд за пределы зоны
            var asi = new AgroSetItem();
            _tsDeltaField = new TimeSpan(0, asi.MinTimeInFieldForCreateRecord, 0);
            decimal bc = asi.BounceSensor;
            _tsDeltaSensor = new TimeSpan(0, (int)bc, (int)((bc - (int)bc)*60));
            SetStatusEvent(Resources.DataSetCreate);
            _dtControl = GetControlDataAll();
            _bps = new BreakingPointsManually();
            _bps.SetPoints(_Mobitel_Id, _dateDoc);
            _dtLock = GetLockedData();
            CI_GetSensorsParametersDriverAgregat();
            //перечень зон с полями
            SetStatusEvent(Resources.FieldsSelection);
            dcZonesBitmap = new Dictionary<int, OrderSquareBitmap>();
            ContentCreateZonesList();
            //агрегат по умолчанию
            using (var daa = new DictionaryAgroAgregat())
            {
                _idAgregatDefault = daa.GetDefaultForVehicle(_Vehicle_Id);
                if (_idAgregatDefault == 0) _idAgregatDefault = daa.GetDefault();
            }
            _cid.firstRecord = true;
            _idDriverDefault = DicUtilites.GetDriverId_Mobitel(_Mobitel_Id);
        }

        private  GpsData CI_CutStopsInTrackEnd(GpsData gpsData_Last)
        {
            for (int i = GpsDatasDocItem.Length - 1; i > 0; i--)
            {
                if (GpsDatasDocItem[i].Speed > 0)
                {
                    if (i == GpsDatasDocItem.Length - 1)
                        gpsData_Last = GpsDatasDocItem[i];
                    else
                        gpsData_Last = GpsDatasDocItem[i + 1];
                    break;
                }
            }
            return gpsData_Last;
        }

        private void CI_GetSensorsParametersDriverAgregat()
        {
            //параметры индикатора водителя 
            _cid.SenDrvLength = 0;
            _cid.SenDrvStartBit = 0;
            GetSensor(AlgorithmType.DRIVER, out _cid.SenDrvLength, out _cid.SenDrvStartBit);
            //параметры индикатора агрегата
            _cid.SenAgrLength = 0;
            _cid.SenAgrStartBit = 0;
            GetSensor(AlgorithmType.AGREGAT, out _cid.SenAgrLength, out _cid.SenAgrStartBit);
        }

        private void CI_SetDriverAgregZoneLockingForPointGps(bool setPrevData, GpsData gpsData)
        {
            var agrInputSource = AgrDrvInputSources.Missing;
            var drvInputSource = AgrDrvInputSources.Missing;
            int idAgregat = ContentGetAgregat(gpsData, _cid.SenAgrLength, _cid.SenAgrStartBit, out agrInputSource);
            if (setPrevData)
            {
                _cid.PrevDriver = ContentGetDriver(gpsData, _cid.SenDrvLength, _cid.SenDrvStartBit,out drvInputSource);
                _cid.PrevAgreg = idAgregat;
                _cid.PrevZone = ContentGetZone(gpsData);
                _cid.PrevLocked = ContentPointLocked(gpsData);
                _cid.PrevAgrInputSource=agrInputSource;
                _cid.PrevDrvInputSource = drvInputSource;
            }
            else
            {
                _cid.CurDriver = ContentGetDriver(gpsData, _cid.SenDrvLength, _cid.SenDrvStartBit,out drvInputSource);
                _cid.CurAgreg = idAgregat;
                _cid.CurZone = ContentGetZone(gpsData);
                _cid.CurLocked = ContentPointLocked(gpsData);
                _cid.CurAgrInputSource = agrInputSource;
                _cid.CurDrvInputSource = drvInputSource;
            }
        }

        private void CI_SetSensorValue(GpsData gpsData, int idAgregat)
        {
            if (_oirSensors.ContainsKey(idAgregat))
            {
                if (_oirSensors[idAgregat].Id > 0)
                {
                    _cid.IsSensorExist = true;
                    _cid.IsSensorActive = _oirSensors[idAgregat].IsActive(gpsData.Sensors, _oirSensors[idAgregat].Algoritm == (int)AlgorithmType.ANGLE);
                }
            }
            else
            {
                using (var daa = new DictionaryAgroAgregat(idAgregat))
                {
                    int idSensor = 0;
                    ExternalDevice agroSensor;
                    if (daa.HasSensor)
                    {
                        agroSensor = daa.GetAgroSensor(_Vehicle_Id,_Mobitel_Id, ref idSensor);
                        _oirSensors.Add(idAgregat, agroSensor);
                        if (idSensor > 0)
                        {
                            _cid.IsSensorExist = true;
                            _cid.IsSensorActive = _oirSensors[idAgregat].IsActive(gpsData.Sensors, _oirSensors[idAgregat].Algoritm == (int)AlgorithmType.ANGLE);
                        }
                    }
                    else
                    {
                        _cid.IsSensorExist = false;
                        agroSensor = new LogicSensor(idSensor);
                        _oirSensors.Add(idAgregat, agroSensor);
                    }
                }
            }
        }

        //private ExternalDevice GetAgroSensor(int idAgregat, DictionaryAgroAgregat daa, ref int idSensor)
        //{
        //    ExternalDevice agroSensor;
        //    idSensor = daa.GetDefaultSensorForVehicle(_Vehicle_Id);


        //    if (idSensor == 0)
        //    {
        //        agroSensor = new LogicSensor((int) AlgorithmType.AGREGAT_LOGIC, _Mobitel_Id);
        //        idSensor = agroSensor.Id;
        //    }
        //    else
        //    {
        //        agroSensor = new Sensor(idSensor);
        //        agroSensor.SetAlgoritm();
        //        if (agroSensor.Algoritm == (int) AlgorithmType.AGREGAT_LOGIC)
        //        {
        //            agroSensor = new LogicSensor((Sensor) agroSensor);
        //        }
        //        else if (agroSensor.Algoritm == (int) AlgorithmType.RANGEFINDER)
        //        {
        //            agroSensor = new SensorCalibrated((Sensor) agroSensor);
        //            ((Sensor) agroSensor).SetSensorStateAgregat(idAgregat);
        //        }
        //        else
        //        {
        //            //((Sensor) agroSensor).BoundAgregatValue = daa.SensorAngleValue;
        //            ((Sensor) agroSensor).SetSensorStateAgregat(idAgregat);
        //        }
        //    }
        //    return agroSensor;
        //}

        /// <summary>
        /// определение рабочего трека с помощью внешнего датчика
        /// </summary>
        /// <param name="d_row"></param>
        void CI_SetSensorZoneAgregat(GpsData gpsData)
        {
            if (_cid.CurAgreg > 0)
            {
                CI_SetSensorValue(gpsData, _cid.CurAgreg);
            }
            if (_cid.IsSensorExist)
            {
                if (_cid.IsSensorActive)
                {
                    if (_cid.CurZone == 0) _cid.CurZone = Consts.ZONE_OUTSIDE_THE_LIST;
                    _cid.TsTimeMoveAfterSensorNoActive = TimeSpan.Zero;
                    _cid.IsSensorSetNoActive = false;
                }
                //При наличии логического датчика писать Переезд в поле при неактивном состоянии

                //19.09.2016 Вернул - нарезка по датчику пропала 07.09.2016 Убрано, потому что не иденитифицируется агрегат.Забыли в карте агрегата указать алгоритм и он пропал из наряда, хотя его RFID поступал в датчик
                else
                {
                    _cid.IsSensorSetNoActive = true;
                    _cid.CurAgreg = 0;
                }
                //07.09.2016______________________________________________________________________________________________

                //else
                //{
                //    _cid.curZone = 0;
                //}
            }
        }

        private void CI_CopyFromBuferList(List<GpsData> gpsDataZone, List<GpsData> gpsDataBuf)
        {
            if (gpsDataBuf.Count > 0)
            {
                for (int i = 0; i < gpsDataBuf.Count; i++)
                {
                    if (!gpsDataZone.Contains(gpsDataBuf[i]))
                    {
                        gpsDataZone.Add(gpsDataBuf[i]);
                    }
                }
            }
        }
        /// <summary>
        /// подавление дребезга
        /// </summary>
        /// <param name="timeDataRow">текущее время анализа</param>
        /// <param name="gpsDataPrevTime">время пердыдущей точки</param>
        /// <returns></returns>
        private bool CI_TestBounce(DateTime timeDataRow,DateTime gpsDataPrevTime)
        {
            //if (cid.curLocked & !cid.prevLocked) return true;
            //return true;
            if (CI_IsPointLockedFirst()) return true;
            if (_cid.ZoneChange)
            {
                //TimeDataRow.Subtract(DateObjChange)tsTimeMoveAfterChangeRecord 
                if (_cid.TsTimeMoveAfterChangeRecord > _tsDeltaField)
                {
                    return true;
                }
                return false;
            }
                //16.05.2017 - Мрия - возможность отключенния датчика во время разворорота в поле (переход 1 -> 0 садится на свой антидребезг )
            if (_cid.IsSensorSetNoActive && _oirSensors.ContainsKey(_cid.PrevAgreg) && (_oirSensors[_cid.PrevAgreg] is LogicSensor)
                && ((LogicSensor)_oirSensors[_cid.PrevAgreg]).BounceTimeSensorNoActive.TotalSeconds> 0)
            {
                _cid.TsTimeMoveAfterSensorNoActive = _cid.TsTimeMoveAfterSensorNoActive.Add(timeDataRow.Subtract(gpsDataPrevTime));
                if (((LogicSensor)_oirSensors[_cid.PrevAgreg]).BounceTimeSensorNoActive >
                    _cid.TsTimeMoveAfterSensorNoActive) 
                    return false;
                return true;
            }
            if (timeDataRow.Subtract(_cid.dtObjChange) > _tsDeltaSensor)
                return true;
            return false;
        }
    }
}
