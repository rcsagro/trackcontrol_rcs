﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TrackControl.General.DatabaseDriver;

namespace Agro.Documents.Order
{

    /// <summary>
    /// внешние данные для нарезки наряда, вносятся пользователем вне TrackControl
    /// </summary>
    public class BreakingPointsManually
    {
        List<BreakingPoint> _bps = new List<BreakingPoint>();
        private int _indexCurrent;

        public bool SetPoints(int idMobitel, DateTime dateOrder)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.OrderItem.SelectAgroEventsManual, idMobitel, driverDb.ParamPrefics);
                DateTime dateOrderEnd = dateOrder.AddDays(1).AddMinutes(-1); 
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", dateOrder);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", dateOrderEnd);
                DataTable dtWork = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
                if (dtWork.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtWork.Rows)
                    {
                        DateTime dtStartAgroEvents = Convert.ToDateTime(dr["TimeStart"]);
                        bool startBeforOrder = false;
                        if (dateOrder.Subtract(dtStartAgroEvents).TotalSeconds > 0)
                        {
                            startBeforOrder = true;
                            dtStartAgroEvents = dateOrder;
                        }
                        var bpStart = new BreakingPoint { Id = Convert.ToInt32(dr["Id"]), DatePoint = dtStartAgroEvents, StartBeforOrder = startBeforOrder };
                        int idWork;
                        Int32.TryParse(dr["Id_work"].ToString(), out idWork);
                        bpStart.IdWork = idWork;
                        _bps.Add(bpStart);
                        DateTime dtEnd;
                        if (DateTime.TryParse(dr["TimeEnd"].ToString(), out dtEnd))
                        {

                            if (dtEnd.Subtract(dateOrderEnd).TotalSeconds > 0) dtEnd = dateOrderEnd;
                            var bpEnd = new BreakingPoint { Id = Convert.ToInt32(dr["Id"]), DatePoint = dtEnd };
                            _bps.Add(bpEnd);
                        }
                    }
                    _bps = _bps.OrderBy(bp => bp.DatePoint).ToList();
                }


            }
            if (_bps.Count > 0)
            {
                return true;
            }
            return false;
        }

        public bool IsBreakingPointWork(DateTime dateDataGps)
        {
            if (_bps.Count==0 || _indexCurrent == _bps.Count) return false;
            if (dateDataGps.Subtract(_bps[_indexCurrent].DatePoint).TotalSeconds >= 0) return true;
            return false;
        }

        public bool IsBreakingPoint(DateTime dateDataGps)
        {
            if (_bps.Count == 0 || _indexCurrent == _bps.Count) return false;
            if (dateDataGps.Subtract(_bps[_indexCurrent].DatePoint).TotalSeconds > 0 & !_bps[_indexCurrent].StartBeforOrder) return true;
            return false;
        }

        public BreakingPoint GetBreakingPoint()
        {
            var bpOut = _bps[_indexCurrent];
            _indexCurrent++;
            return bpOut;
        }

    }

    public class BreakingPoint
    {
        public int Id { get; set; }
        public DateTime DatePoint { get; set; }
        public int IdWork { get; set; }
        public bool StartBeforOrder { get; set; }
    }

}
