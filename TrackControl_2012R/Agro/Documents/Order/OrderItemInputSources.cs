﻿using System.Collections.Generic;
using System.Drawing;
using Agro.Dictionaries;
using Agro.Utilites;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using LocalCache;
using TrackControl.Vehicles;  


namespace Agro
{
    public enum AgrDrvInputSources
    {
        Missing,
        ControlData,
        Rfid,
        Settings
    }
    
    public partial class OrderItem
    {

        /// <summary>
        /// агрегат по умолчанию транспортного средства
        /// </summary>
        int _idAgregatDefault;

        /// <summary>
        /// водитель из справочника для транспортного средства
        /// </summary>
        int _idDriverDefault;

        /// <summary>
        /// датчики агрегатов
        /// </summary>
        Dictionary<int, ExternalDevice> _oirSensors;

        /// <summary>
        /// идентификаторы агрегатов
        /// </summary>
        Dictionary<ulong, int> _agregatIdent;

        /// <summary>
        /// идентификаторы водителей
        /// </summary>
        Dictionary<ulong, int> _driverIdent;

        public string GetToolTipTextForDriverInputSource(AgrDrvInputSources inputSource)
        {
            const string text = "Источник: ";
            switch ((AgrDrvInputSources)inputSource)
            {
                case AgrDrvInputSources.Rfid:
                    {
                        return text + "RFID";
                    }
                case AgrDrvInputSources.ControlData:
                    {
                        return text + "управляющие данные";
                    }
                case AgrDrvInputSources.Settings:
                    {
                        return text + "установки транспорта -> водитель";
                    }
                default:
                    {
                        return "";
                    }
            }
        }

        public string GetToolTipTextForAgrInputSource(AgrDrvInputSources inputSource)
        {
            const string text = "Источник: ";
            switch ((AgrDrvInputSources)inputSource)
            {
                case AgrDrvInputSources.Rfid:
                    {
                        return text + "RFID";
                    }
                case AgrDrvInputSources.ControlData:
                    {
                        return text + "управляющие данные";
                    }
                case AgrDrvInputSources.Settings:
                    {
                        return text + "установки навесного оборудования -> транспортное средство по умолчанию";
                    }
                default:
                    {
                        return "";
                    }
            }
        }

        public Color GetInputSourseColor(AgrDrvInputSources inputSource)
        {
            switch (inputSource)
            {
                case AgrDrvInputSources.Rfid:
                    {
                        return Color.LightGreen;
                    }
                case AgrDrvInputSources.ControlData:
                    {
                        return Color.LightYellow;
                    }
                case AgrDrvInputSources.Settings:
                    {
                        return Color.LightSteelBlue;
                    }
                default:
                    return Color.White;
            }
        }

        /// <summary>
        /// идентификатор агрегата - три источника:
        /// 1 - управляющие данные
        /// 2 - поиск в DatаGPS
        /// 3 - наличие связи с транспортом в установках агрегата
        /// </summary>
        private int ContentGetAgregat(GpsData gpsData, int senAgrLength, int senAgrStartBit, out AgrDrvInputSources AgrInputSource)
        {
            //Агродом еще просил такую вещь. 
            //Если в справочнике прицепного оборудования прописано транспортное средство 
            //по умолчанию, но при этом от этого транспортного средства приходит 
            //идентификатор другого оборудования, 
            //то идентификатор должен иметь приоритет
            int idAgregat = 0;
            // если агрегат определен в гриде управления - возвращаем его идентификтор 
            if (_dtControl.Rows.Count > 0)
                idAgregat = ContentGetControlObject(_dtControl, Consts.TypeControlObject.Agregat, gpsData.Time);
            if (idAgregat > 0)
            {
                AgrInputSource = AgrDrvInputSources.ControlData;
                return idAgregat;
            }
            // идентификтор агрегата из DatаGPS 
            if (senAgrLength > 0)
            {
                ulong identAgregat = 0;
                identAgregat = Calibrate.ulongSector(gpsData.Sensors, senAgrLength, senAgrStartBit);
                //Поставить в главный приоритет датчик идентификации нас попросил один клиент. Так что, для того чтобы согласовать все интересы предлагаю сделать как я говорил выше. 1. При наличии прописанного датчика идентификации, слушаем его. 2. Если от него нет значащих данных, смотрим на связку ТС +агрегат в справочнике агрегатов 3. Если связка для данного ТС не указана, смотрим на агрегат по умолчанию в справочнике для всех ТС 4. Если он не указан, используем ширину по умолчанию в установках 5. Если в п.1. не прописан датчик идентификации, сразу переходим к пп. 2-4 
                if (identAgregat != Algorithm.Rfid_New_Missing && identAgregat != Algorithm.RFID_NOT_WORKING)
                {
                    if (identAgregat == (ulong)((1 << senAgrLength) - 1)) identAgregat = 0;
                    AgrInputSource = AgrDrvInputSources.Rfid;
                    if (_agregatIdent.ContainsKey(identAgregat))
                    {

                        return _agregatIdent[identAgregat];
                    }
                    else
                    {
                        if (identAgregat > 0)
                        {
                            
                            using (var daa = new DictionaryAgroAgregat())
                            {
                                daa.InitFromIdentifier((ushort)identAgregat);
                                idAgregat = daa.Id;
                            }
                            _agregatIdent.Add(identAgregat, idAgregat);
                            return idAgregat;
                        }

                    }
                }

            }
            // если назначен идентификатор по умолчанию для транспортного средства - возвращаем его идентификтор 
            if (_idAgregatDefault > 0)
            {
                AgrInputSource = AgrDrvInputSources.Settings;
                return _idAgregatDefault;
            }
            AgrInputSource = AgrDrvInputSources.Missing;
            return 0;

        }

        /// <summary>
        /// идентификатор водителя 
        /// </summary>
        private int ContentGetDriver(GpsData gpsData, int senDrvLength, int senDrvStartBit, out AgrDrvInputSources DrvInputSource)
        {
            int id_driver = 0;
            // если водитель определен в гриде управления - подставляем его идентификтор 
            if (_dtControl.Rows.Count > 0)
                id_driver = ContentGetControlObject(_dtControl, Consts.TypeControlObject.Driver, gpsData.Time);
            // с минусом, чтобы отличить от идентификтора водителя из DatаGPS - разные алгоритмы получения реального ИД
            if (id_driver > 0)
            {
                DrvInputSource = AgrDrvInputSources.ControlData;
                return id_driver;
            }
            // идентификтор водителя из DatаGPS 
            if (senDrvLength > 0)
            {
                ulong ident_driver = Calibrate.ulongSector(gpsData.Sensors, senDrvLength, senDrvStartBit);
                if (ident_driver == (ulong)((1 << senDrvLength) - 1)) ident_driver = 0;

                if (_driverIdent.ContainsKey(ident_driver))
                {
                    DrvInputSource = AgrDrvInputSources.Rfid;
                    return _driverIdent[ident_driver];
                }
                else
                {
                    if (ident_driver > 0)
                    {
                        SensorAtributs sa = DicUtilites.GetDriverAtributes((ushort)ident_driver);
                        id_driver = sa.Id;
                        _driverIdent.Add(ident_driver, id_driver);
                        DrvInputSource = AgrDrvInputSources.Rfid;
                        return id_driver;
                    }

                }
            }
            if (_idDriverDefault > 0)
            {
                DrvInputSource = AgrDrvInputSources.Settings;
                return _idDriverDefault;
            }
            DrvInputSource = AgrDrvInputSources.Missing;
            return id_driver;
        }
    
    }


}
