
namespace Agro
{
    public struct LON_LAT_MIN_MAX
    {
        public double LON_max;
        public double LAT_max;
        public double LON_min;
        public double LAT_min;
        public double LON_first;
        public double LAT_first;
    }
}
