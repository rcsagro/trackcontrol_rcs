using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Agro.Utilites;
using GeoData;
using TrackControl.General;
namespace Agro
{
    /// <summary>
    /// ������� ������� ������������ ������� � ������� �������� �� BitMap
    /// ������������ ����� ��� ���������� ����� 
    /// </summary>
    public class OrderSquareBitmap:IDisposable 
    {
        Graphics gr;
        private Bitmap _btmField;
        public Bitmap BtmField
        {
            get { return _btmField; }
            set { _btmField = value; }
        }
        private int Y_min;
        private int X_min;
        private int Y_max;
        private int X_max;
        private int _iScale;
        private GraphicsPath pathTrack;
        private Pen pLine;
        private Pen pCenter;
        private Pen pEndLine;
        private int curIndex;
        private List<TRealPoint> rpoints ;
        /// <summary>
        /// ��� ���������� ����������� ����� ���� � ����� � ������ ����� �� ���� �������� � ������� ������ G B
        /// </summary>
        private Color _colorZone = Color.FromArgb(70, 0, 128, 0);
        private Color _ColorAgregat = Color.FromArgb(70, 0, 0, 128);
        private Pen pWidth;
        // �������� ����
        private double  _squareZone;
        public double SquareZone
        {
            get { return _squareZone; }
        }
        /// <summary>
        /// ������������ �������
        /// </summary>
        private double _squareProcess;
        public double SquareProcess
        {
            get { return _squareProcess; }
        }
        /// <summary>
        /// ������� ������������ �������
        /// </summary>
        private double _squareProcessPersent;
        public double SquareProcessPersent
        {
            get { return _squareProcessPersent; }
        }
        /// <summary>
        /// �������� ������������ �������
        /// </summary>
        private double _squareSecondProcess;
        public double SquareSecondProcess
        {
            get { return _squareSecondProcess; }
        }
        /// <summary>
        /// ������� ��������-������������ ������� 
        /// </summary>
        private double _squareSecondProcessPersent;
        public double SquareSecondProcessPersent
        {
            get { return _squareSecondProcessPersent; }
        }
        /// <summary>
        /// ����-� ��������� ���������
        /// </summary>
        private double _squareSecondProcessFactor;
        public double SquareSecondProcessFactor
        {
            get { return _squareSecondProcessFactor; }
        }
        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="iScale"> ������� ����������� </param>
        public OrderSquareBitmap(int iScale)
        {
            _iScale = iScale;
            pWidth = new Pen(_ColorAgregat) {Alignment = PenAlignment.Center, LineJoin = LineJoin.Round};
        }
        public OrderSquareBitmap(int iScale, PointLatLng[] rpointsNew)
            : this(iScale)
        {
            rpoints = new List<TRealPoint>();
            ConvertPoints(ref rpoints, rpointsNew); 
        }
        #region Public

        public int Scale
        {
            get { return _iScale; }
            set { _iScale = value; }
        }

        /// <summary>
        /// ���������� ���� �� BitMap
        /// </summary>
        /// <param name="gr"></param>
        public void DrawZone(ref Point[] spoints)
        {
            if (gr == null) SetGraphic();
            GraphicsPath path = new GraphicsPath();
            path.AddLines(spoints);
            path.CloseFigure();
            gr.DrawPath(new Pen(_colorZone, 1), path);
            gr.FillPath(new SolidBrush(_colorZone), path);
        }
        public void DrawZone()
        {
            if (rpoints == null) return;
            Point[] spoints = new System.Drawing.Point[rpoints.Count];
            SetScreenPoints(true, ref rpoints, ref spoints);
            DrawZone(ref spoints);
        }
        /// <summary>
        /// ���������� ����� �� BitMap
        /// </summary>
        /// <param name="gr"></param>
        public void DrawTrackPath(ref Point[] spoints, double dbWidth)
        {
            if (gr == null) SetGraphic();
            //--------------------------------------------------------------------
            GraphicsPath path = new GraphicsPath();
            pWidth.Width = (float)dbWidth;
            //--------------------------------------------------------------------
            for (int j = 1; j < spoints.Length; j++)
            {
                path.AddLine(spoints[j - 1], spoints[j]);
            }
            gr.DrawPath(pWidth, path);
            
        }
        public void DrawTrackLine(ref Point[] spoints, double dbWidth)
        {
            if (gr == null) SetGraphic();
            pWidth.Width = (float)dbWidth;
            for (int j = 1; j < spoints.Length; j++)
            {
                gr.DrawLine(pWidth, spoints[j - 1], spoints[j]);
            }

        }
        public void DrawTrackSegment(ref Point[] spoints, int j)
        {
            if (gr == null) SetGraphic();
            //pEndLine = null;
            if ((pEndLine == null) || (pLine == null)) InitTrack(0);
            gr.DrawLine(pWidth, spoints[j - 1], spoints[j]);
            //------------------------------------------
            // ���������� ������� 
            if (MapUtilites.CheckLength(spoints[curIndex],
               spoints[j], 150))
            {
                pCenter = pEndLine;
                curIndex = j;
            }
            else
            {
                pCenter = pLine;
            }
            if (pCenter == null) return;
            gr.DrawLine(pCenter, spoints[j - 1], spoints[j]);
        }
        public void InitTrack(double dbWidth)
        {
            Color colWork = Color.Brown;
            _ColorAgregat = Color.FromArgb(70, colWork.R, colWork.G, colWork.B);
            pWidth.Color = _ColorAgregat;
            pWidth.Width = (float)dbWidth;
            //------------------------------------------
            pLine = new Pen(Color.DarkBlue, 1);
            pCenter = new Pen(Color.DarkBlue, 1);
            pEndLine = MapUtilites.GetLineCap();
            pEndLine.Color = pLine.Color;
            pCenter.Alignment = PenAlignment.Center;
            pEndLine.Width = pLine.Width;

        }
    /// <summary>
            /// ������ ������� �� �������� �� BitMap
    /// </summary>
    /// <param name="oir">������ ������ ������</param>
    /// <param name="DrawSecondTrack">���������� ��������� ��������� ��� ����������� ���������</param>
    /// <returns></returns>
        public double SquareCalcBitmap(OrderItemRecord oir,Boolean DrawSecondTrack)
        {
            if (oir.Rpoints.Count  == 0) return 0;
            ResetMinMax();
            // �������� BitMap ��� ����������� ����
            if (rpoints.Count > 0)
            {
                DrawZone();
                DrawTrack(oir, DrawSecondTrack);
                _squareZone = DicUtilites.GetZoneSquare(oir.ZoneId);
                _squareProcessPersent = AnalizBitmap();
                _squareProcess = Math.Round(_squareZone * _squareProcessPersent, 2);
                CalculateAddParameters(oir);
            }
            return _squareProcess;
        }

        public double SquareCalcAddTrack(OrderItemRecord oir)
        {
            DrawTrack(oir, false);
            _squareZone = DicUtilites.GetZoneSquare(oir.ZoneId);
            _squareProcessPersent = AnalizBitmap();
            _squareProcess = Math.Round(_squareZone * _squareProcessPersent, 2);
            return _squareProcess;
        }

        private void CalculateAddParameters(OrderItemRecord oir)
        {
            if ((_squareProcess > oir.Square) && (_squareZone > 0))
            {
                _squareProcess = oir.Square;
                _squareProcessPersent = Math.Round(_squareProcess * 100 / _squareZone, 2);
                _squareSecondProcess = 0;
                _squareSecondProcessPersent = 0;
                _squareSecondProcessFactor = 0;
            }
            else
            {
                _squareProcessPersent = Math.Round(_squareProcessPersent * 100, 2);
                _squareSecondProcess = Math.Round(oir.Square - _squareProcess, 2);
                _squareSecondProcessPersent = Math.Round(_squareSecondProcess * 100 / _squareZone, 2);
                if (_squareSecondProcess > 0)
                    _squareSecondProcessFactor = Math.Round(_squareSecondProcess * 100 / _squareProcess, 2);
                else
                    _squareSecondProcessFactor = 0;
            }
        }

        private void DrawTrack(OrderItemRecord oir, Boolean DrawSecondTrack)
        {
            List<TRealPoint>  rpoints_track = new List<TRealPoint>();
            for (int i = 0; i < oir.Rpoints.Count; i++)
            {
                TRealPoint rpoint = new TRealPoint();
                rpoint.X = oir.Rpoints[i].X;
                rpoint.Y = oir.Rpoints[i].Y;
                rpoints_track.Add(rpoint);
            }
            Point[] spoints = new System.Drawing.Point[rpoints_track.Count];
            SetScreenPoints(false, ref rpoints_track, ref spoints);
            double dbWidthPixel = GetAgregatWidthInPixels(oir, spoints, rpoints_track);
            DrawTrackPath(ref spoints, dbWidthPixel);

            if (DrawSecondTrack) DrawTrackLine(ref spoints, dbWidthPixel);
            rpoints_track.Clear(); 
        }

        private void ResetMinMax()
        {
            Y_min = 0;
            X_min = 0;
            Y_max = 0;
            X_max = 0;
        }

        private static double GetAgregatWidthInPixels(OrderItemRecord oir, Point[] spoints, List<TRealPoint> rpoints_track)
        {
            double dbWidthPixel = 0;
            int iCntWidth = 0;
            double dbMetrPixel = 0;
            //--------------------------------------------------------------------
            for (int j = 1; j < rpoints_track.Count; j++)
            {
                //������ � �������� ��� ���������� �������� �����
                double dist = 1000 * �GeoDistance.CalculateFromGrad(rpoints_track[j - 1].Y, rpoints_track[j - 1].X, rpoints_track[j].Y, rpoints_track[j].X);
                if (dist > 0)
                {
                    double dbMetrPixelLoc = (Math.Pow(Math.Pow((spoints[j].X - spoints[j - 1].X), 2) + Math.Pow((spoints[j].Y - spoints[j - 1].Y), 2), 0.5)) / dist;
                    if (dbMetrPixelLoc > 0)
                    {
                        dbMetrPixel += dbMetrPixelLoc;
                        iCntWidth++;
                    }
                }
            }
            dbWidthPixel =  dbMetrPixel * oir.WidthAgr / iCntWidth;
            return dbWidthPixel;
        }
        /// <summary>
        /// �������������� ��� ��������� � �������� 
        /// </summary>
        /// <param name="bSetMinMax">��������� ������ BitMap</param>
        /// <param name="rpoints"></param>
        /// <param name="spoints"></param>
        public void SetScreenPoints(bool bSetMinMax, ref List<TRealPoint> rpoints, ref Point[] spoints)
        {
            //Y_min = 0;
            //X_min = 0;
            //Y_max = 0;
            //X_max = 0;
            
            for (int i = 0; i < rpoints.Count; i++)
            {
                spoints[i] = MapUtilites.Geo2BmpPix(rpoints[i], _iScale);
                if (bSetMinMax)
                {
                    if ((X_min > spoints[i].X) | X_min == 0) X_min = spoints[i].X;
                    if ((Y_min > spoints[i].Y) | Y_min == 0) Y_min = spoints[i].Y;
                }
            }
            for (int i = 0; i < rpoints.Count; i++)
            {
                //spoints[i] = GeoToSurface(rpoints[i]);
                //if (i == 73)
                //    Debug.Print("STOP");
                spoints[i].X = spoints[i].X - X_min;
                spoints[i].Y = spoints[i].Y - Y_min;
                if (bSetMinMax)
                {
                    if ((X_max < spoints[i].X) | X_max == 0) X_max = spoints[i].X;
                    if ((Y_max < spoints[i].Y) | Y_max == 0) Y_max = spoints[i].Y;
                }
            }

        }
        /// <summary>
        /// �������� bitmap ���� �� ��� ����
        /// </summary>
        /// <param name="ZoneCode">��� ����</param>
        /// <returns></returns>
        public bool CreateZoneBitmap()
        {
            if (rpoints.Count == 0) return false;
            try
            {
                Point[] spoints = new System.Drawing.Point[rpoints.Count];
                SetScreenPoints(true, ref rpoints, ref spoints);
                DrawZone(ref spoints);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// �������������� ����� ����
        /// </summary>
        /// <param name="Lon"></param>
        /// <param name="Lat"></param>
        /// <returns></returns>
        public bool PointInZone(double Lon, double Lat)
        {
            TRealPoint rpoint = new TRealPoint();
            rpoint.X = Lon;
            rpoint.Y = Lat;
            Point spoint = MapUtilites.Geo2BmpPix(rpoint, _iScale);
            spoint.X = spoint.X - X_min;
            spoint.Y = spoint.Y - Y_min;
            if ((spoint.X < 0) | (spoint.Y < 0))
                return false;
            if ((spoint.X >= _btmField.Width) || (spoint.Y >= _btmField.Height))
            {
                return false;
            }
            else
            {
                Color clWork = _btmField.GetPixel(spoint.X, spoint.Y);
                if ((clWork.R == 0) && (clWork.B == 0) && (clWork.G > 0))
                    return true;
                else
                    return false;
            }

        }
        public bool TrackInZone(LON_LAT_MIN_MAX min_max)
        {
            if (rpoints.Count == 0) return false;
            for (int i = 0; i < rpoints.Count ; i++)
            {
                if ((rpoints[i].X >= min_max.LON_min) && (rpoints[i].X <= min_max.LON_max)
                    && (rpoints[i].Y >= min_max.LAT_min)  && (rpoints[i].Y <= min_max.LAT_max))
                    return true;
            }
            return false;
        }

        public double CalcSquareOverlapToWork(List<int> orderRecords)
        {
            double squareOverlapWork = 0;
            foreach (int orderRecord in orderRecords)
            {
                using (OrderItemRecord oir = new OrderItemRecord(orderRecord))
                {
                    if (squareOverlapWork == 0)
                        squareOverlapWork = SquareCalcBitmap(oir, false);
                    else
                        squareOverlapWork = SquareCalcAddTrack(oir);
                }
            }
            return squareOverlapWork;
        }

        public void DrawText(string textDraw)
        {
           
        }
        #endregion

        #region Private
        /// <summary>
        /// ���������� ����� ���� � �����
        /// </summary>
        private double AnalizBitmap()
        {
            int iCntColorField = 0;
            int iCntColorAgregat = 0;
            using (BitmapDecorator bd = new BitmapDecorator(_btmField))
                for (int X = 0; X < _btmField.Width; X++)
                {
                    for (int Y = 0; Y < _btmField.Height; Y++)
                    {
                        Color clWork = bd.GetPixel(X, Y);
                        if ((clWork.G > 0) && (clWork.B > 0))
                        {
                            iCntColorAgregat++;
                        }
                        if ((clWork.G > 0) && (clWork.B >= 0))
                        {
                            iCntColorField++;
                        }
                    }
                }
            double dbTrack = (double)iCntColorAgregat;
            if (iCntColorField > 0)
                return dbTrack / iCntColorField;
            else
                return 0;
        }
        /// <summary>
        /// ���������� ������ ������� ������ ��� ���������� ��������� ������������ �������
        /// </summary>
        private void CreateCompressInsert(ref OrderItemRecord zw)
        {
            List<CompressData.TRealPointSpeed> rspoints = new List<CompressData.TRealPointSpeed>();
            for (int i = 0; i < zw.GpsDataZone.Count; i++)
            {
                if (zw.GpsDataZone[i].Speed > 0)
                {
                    CompressData.TRealPointSpeed rspoint;
                    rspoint.X = zw.GpsDataZone[i].LatLng.Lng ;
                    rspoint.Y = zw.GpsDataZone[i].LatLng.Lat;
                    rspoint.Speed = zw.GpsDataZone[i].Speed;
                    rspoints.Add(rspoint);
                    Application.DoEvents();
                }
            }
            CompressData cs = new CompressData("agro_datagps", zw.IdOrderT);
            cs.Compress(rspoints);
            //--------------------------------------
            //CompressData
            //--------------------------------------
        }

        private void SetGraphic()
        {
                _btmField = new Bitmap(X_max, Y_max, PixelFormat.Format32bppArgb);
                gr = Graphics.FromImage(_btmField);
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                pathTrack = new GraphicsPath();
        }

        private void ConvertPoints(ref List<TRealPoint> rpoints, PointLatLng[] rpoints_new)
        {
            for (int i = 0; i < rpoints_new.Length; i++)
            {
                TRealPoint rpoint = new TRealPoint();
                rpoint.X = rpoints_new[i].Lng;
                rpoint.Y = rpoints_new[i].Lat;
                rpoints.Add(rpoint);
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_btmField != null)
                _btmField.Dispose();
            if (gr != null)
                gr.Dispose();
        }

        #endregion
    }
}
