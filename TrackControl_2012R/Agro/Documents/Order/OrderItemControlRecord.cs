﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;

namespace Agro
{
    public class OrderItemControlRecord
    {
        DateTime _timeStart;

        public DateTime TimeStart
        {
            get { return _timeStart; }
            set { _timeStart = value; }
        }

        DateTime _timeEnd;

        public DateTime TimeEnd
        {
            get { return _timeEnd; }
            set { _timeEnd = value; }
        }
        int _id_object;

        public int Id_object
        {
            get { return _id_object; }
            set { _id_object = value; }
        }
        Consts.TypeControlObject _typeObject;

        public Consts.TypeControlObject TypeObject
        {
            get { return _typeObject; }
            set { _typeObject = value; }
        }
        int _id_record;

        public int Id_record
        {
            get { return _id_record; }
            set { _id_record = value; }
        }

        public OrderItemControlRecord()
        {

        }


    }
}
