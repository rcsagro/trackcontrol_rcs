using System.Linq;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;

namespace Agro
{
    /// <summary>
    /// ����� �� ������
    /// </summary>
    public partial class OrderItem : DocItem
    {

        #region �������

        public event StatusMessage ChangeStatusEvent;
        //������� ���������
        private void SetStatusEvent(string sMessage)
        {
            if (ChangeStatusEvent != null)
            {
                if (sMessage == Resources.Ready)
                {
                    ChangeStatusEvent(sMessage);
                }
                else
                {
                    ChangeStatusEvent(_ID > 0
                        ? (Resources.Order + " " + _ID
                           + ((_recordNumber > 0) ? "| " + Resources.Record + " " + _recordNumber : "")
                           + ":" + sMessage)
                        : sMessage);
                }
                Application.DoEvents();
            }
        }

        #endregion

        #region ������������

        public OrderItem()
        {
        }

        public OrderItem(DateTime dtDoc, int Mobitel_Id)
        {
            _Mobitel_Id = Mobitel_Id;
            _dateDoc = dtDoc;
        }

        public OrderItem(int ID_Order)
        {
            _ID = ID_Order;
            if (_ID == 0) return;
            GetDocById(_ID);
        }

        #endregion

        #region ��������� ������ � ���������

        /// <summary>
        /// ������� �� ������� ����������� ����
        /// </summary>
        private class ItemSqClass
        {
            private double _Lon;
            private double _Lat;
            private bool _Selected;
            private bool _Border;
            private bool _BorderNode;
            // ����������� ���������� ��������� - ������� �������
            private static int _SelectedCount;
            // ����������� ��� ����� ������� - ��� �������������� ����������

            private static int _NumberCnt = 0;
            private int _NumberBorder = 0;
            // ������ ������� ����� �������
            private static string _sSqlinsert;
                //  "INSERT INTO `agro_calct`(Id_main,Lon,Lat,Selected,Border,BorderNode,Number)  VALUES ";

            private static List<string> _lsSQLinsert = new List<string>();
            //������� ������ INSERT ���������
            private static int _iListCount = 0;

            public double Lon
            {
                get { return _Lon; }
                set { _Lon = value; }
            }

            public double Lat
            {
                get { return _Lat; }
                set { _Lat = value; }
            }

            public bool Selected
            {
                get { return _Selected; }
                set
                {
                    if ((!_Selected) && value)
                        _SelectedCount++;
                    else if ((_Selected) && !value)
                        _SelectedCount--;

                    _Selected = value;

                }
            }

            public bool Border
            {
                get { return _Border; }
                set { _Border = value; }
            }

            public bool BorderNode
            {
                get { return _BorderNode; }
                set
                {
                    _BorderNode = value;
                    if (_BorderNode)
                    {
                        if (_iListCount == 0)
                            _sSqlinsert = AgroQuery.OrderItem.sqlInsertHead;

                        _NumberCnt++;
                        _iListCount++;
                        _NumberBorder = _NumberCnt;
                        //���������������� ������  INSERT ��� ������ ����� ������
                        //��������� ����� �� 500 ������� - ��������� ��������
                        // ������ Id_calc ### - ���� ������ �� ������� ������� �� ���������� ��� �������
                        _sSqlinsert = _sSqlinsert + (_iListCount == 1 ? "" : ",") + "("
                                      + "###,"
                                      + Lon.ToString().Replace(",", ".") + ","
                                      + Lat.ToString().Replace(",", ".") + ","
                                      + Selected + "," + Border + "," + BorderNode + "," + _NumberCnt + ")";
                        if (_iListCount == 500)
                        {
                            _lsSQLinsert.Add(_sSqlinsert);
                            _iListCount = 0;
                            _sSqlinsert = "";
                        }


                    }
                }
            }

            public int Number
            {
                get { return _NumberCnt; }
                set { _NumberCnt = value; }
            }

            public int NumberBorder
            {
                get { return _NumberBorder; }
                set { _NumberBorder = value; }
            }

            public int SelectedCount
            {
                get { return _SelectedCount; }
                set { _SelectedCount = value; }
            }

            public List<string> lsSQLinsert
            {
                get
                {
                    if (_sSqlinsert.Length > 0)
                    {
                        _lsSQLinsert.Add(_sSqlinsert);
                        _sSqlinsert = "";
                        _iListCount = 0;
                        _NumberCnt = 0;

                    }
                    return _lsSQLinsert;
                }
                set { _lsSQLinsert = value; }
            }

            public ItemSqClass(double iLon, double iLat)
            {
                _Selected = false;
                _Border = false;
                _BorderNode = false;
                _Lon = iLon;
                _Lat = iLat;
            }
        }

        private struct CiData
        {
            public int SenDrvLength;
            public int SenDrvStartBit;
            public int SenAgrLength;
            public int SenAgrStartBit;
            public int CurDriver; // ������������� �������� �� Dat�GPS , ������������ ��������� ����� � ������� 
            public int PrevDriver;
            public int CurZone;
            public int PrevZone;
            public int ChangeZone;
            public int CurAgreg;
            public int PrevAgreg;
            public TimeSpan TsTimeMoveAfterChangeRecord;
            public TimeSpan TsTimeMoveAfterSensorNoActive;
            public bool StartMove;
            public bool StopMove;
            public bool ObjChange;
            public bool ZoneChange;
            public bool PrevLocked;
            public bool CurLocked;
            public AgrDrvInputSources PrevAgrInputSource;
            public AgrDrvInputSources CurAgrInputSource;
            public AgrDrvInputSources PrevDrvInputSource;
            public AgrDrvInputSources CurDrvInputSource;
            public bool IsSensorExist;
            public bool IsSensorActive;
            public bool IsSensorSetNoActive;
            public int BreakingPointId;
            public int BreakingPointWorkId;

            /// <summary>
            /// ��� ������ � ���� � ����������� ������������ �������� �������� ��������� ������ ������ ������
            /// </summary>
            public bool firstRecord;

            public DateTime dtObjChange;
        }

        #endregion

        #region ��������

        public override DateTime Date
        {
            get
            {
                return _dateDoc;
            }
            set
            {
                _dateDoc = value;
            }
        }

        public override int AddDoc()
        {
            if (_Mobitel_Id == 0) return 0;

            if (_Mobitel_Id == 121)
            {
                int k = _Mobitel_Id;
            }

            if (TestDemo())
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSql = AgroQuery.OrderItem.InsertIntoAgroOrder;

                    driverDb.NewSqlParameterArray(5);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Date", _dateDoc);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Mobitel_Id", _Mobitel_Id);

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", _Remark);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", _Remark ?? "");
                    }

                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "StateOrder", (int) StatesOrder.New);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserCreated", UserBaseCurrent.Instance.Name);
                    _ID = driverDb.ExecuteReturnLastInsert(sSql, driverDb.GetSqlParameterArray, "agro_order");
                    UserLog.InsertLog(UserLogTypes.AGRO, Resources.OrderCreating, _ID);
                }
            }
            return _ID;
        }

        public override bool DeleteDoc(bool bQuestionNeed)
        {
            if (DeleteDocTest() && DeleteDocContent())
            {
                if (bQuestionNeed)
                {
                    if (DialogResult.No ==
                        XtraMessageBox.Show(Resources.OrderDeleteConfirm + " " + _ID + "?", Resources.ApplicationName,
                            MessageBoxButtons.YesNo))
                        return false;
                }

                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sSQL = string.Format(AgroQuery.OrderItem.DeleteAgroOrder, _ID);
                    driverDb.ExecuteNonQueryCommand(sSQL);
                }
                UserLog.InsertLog(UserLogTypes.AGRO, Resources.LogDelete, _ID);
                return true;
            }
            else
                return false;
        }

        public override bool DeleteDocContent()
        {
            return ContentDeleteControlObjects() && ContentDelete();
        }

        public override bool UpdateDoc()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSQL = string.Format(AgroQuery.OrderItem.UpdateAgroOrder, _ID);

                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Date", _dateDoc);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Mobitel_Id", _Mobitel_Id);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Comment", _Remark);
                driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                return true;
            }
        }

        public override bool DeleteDocTest()
        {
            return true;
        }

        public override bool TestDemo()
        {
            if (GlobalVars.g_AGRO == (int) Consts.RegimeType.Demo)
            {
                int cnt = 0;
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    Sql = AgroQuery.OrderItem.SelectAgroOrderCount;

                    cnt = driverDb.GetScalarValueNull<Int32>(Sql, 0);
                    driverDb.CloseDbConnection();
                }
                if (cnt >= Consts.AGRO_LIMIT + 1000)
                {
                    XtraMessageBox.Show(Resources.AgroLimit + " " + Consts.AGRO_LIMIT.ToString() + "!",
                        Resources.ApplicationName);
                    _ID = 0;
                    return false;
                }
            }
            return true;
        }

        public override DataTable GetContent()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdert, Resources.Move1, _ID,
                    Consts.ZONE_OUTSIDE_THE_LIST,
                    Resources.FieldUnknown, AgroQuery.SqlDriverIdent);
                return driverDb.GetDataTable(Sql);
            }
        }

        /// <summary>
        /// ������������ ���� + ������ ��� ����� ��� ������������ "�������"
        /// </summary>
        /// <returns></returns>
        public DataTable GetContentGrouped()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectIfAgroOrdert, Resources.Move1, _ID, Consts.ZONE_OUTSIDE_THE_LIST, Resources.FieldUnknown);

                DataTable contentGrps = driverDb.GetDataTable(Sql);

                contentGrps.Columns["FSCOverLap"].ReadOnly = false;
                foreach (DataRow contentGrp in contentGrps.Rows)
                {
                    int zoneId = Convert.ToInt32(contentGrp["Id_zone"].ToString());
                    if (zoneId > 0)
                    {
                        double sqOverlap = GetFieldSquareFromFieldsString(SquareWorkDescriptOverlap, contentGrp["FName"].ToString());
                        float sq =(float)GetFieldSquareFromFieldsString(SquareWorkDescript, contentGrp["FName"].ToString());
                        float sqCalc = (float)Convert.ToDouble(contentGrp["FSC"].ToString());
                        if (sq == sqCalc)
                        {
                            contentGrp["FSCOverLap"] = sqOverlap;
                        }
                        else
                        {
                            if (_oirs == null) _oirs = GetOrderItemRecords();
                            int driverd = Convert.ToInt32(contentGrp["Id_driver"].ToString());
                            int agregatId = Convert.ToInt32(contentGrp["Id_agregat"].ToString());
                            double sqCalcOverlap = CalcSquareOverlapDriverAgregat(zoneId, driverd, agregatId);
                            contentGrp["FSCOverLap"] = Math.Round(Math.Min(sqCalc, sqCalcOverlap),3);
                        }
                    }
                }
                return contentGrps; 
            }
        }

        /// <summary>
        /// ������� ���� �� ������ � ������ ���������
        /// </summary>
        /// <param name="fields">������ � ������ ���������</param>
        /// <param name="field">��� ����</param>
        /// <returns></returns>
        public double GetFieldSquareFromFieldsString(string fields, string field)
        {
            double dSquare = 0;
            if (SquareWorkDescriptOverlap.Contains(field))
            {
                int indPos = fields.IndexOf(field) + field.Length + 3;
                string sSquare = fields.Substring(indPos);
                indPos = sSquare.IndexOf("(");
                sSquare = sSquare.Substring(0, indPos);
                Double.TryParse(sSquare, out dSquare);
            }
            return dSquare;
        }




        /// <summary>
        /// ������ ������� �� ������� ������
        /// </summary>
        public DataTable GetFuelDUT()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertId, Resources.Move1, _ID,
                    Consts.ZONE_OUTSIDE_THE_LIST,
                    Resources.FieldUnknown);

                return driverDb.GetDataTable(Sql);
            }
        }

        /// <summary>
        /// ������� ���� ������ � ��������
        /// </summary>
        /// <returns></returns>
        public DataTable GetFuelDUTFueling()
        {
            OrderFueling of = new OrderFueling(_ID);
            return of.GetRecords();
        }

        /// <summary>
        /// ������ ������� �� ������� ������� �������
        /// </summary>
        public DataTable GetFuelDRT()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertIf, Resources.Move1, _ID,
                    Consts.ZONE_OUTSIDE_THE_LIST,
                    Resources.FieldUnknown);

                return driverDb.GetDataTable(Sql);
            }
        }

        /// <summary>
        /// ������,����������� ��������� ������
        /// </summary>
        public DataTable GetControlData(int DataType)
        {
            DataTable table = null;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertControl, DataType, _ID);
                table = driverDb.GetDataTable(Sql);
                table.Columns["Id"].ReadOnly = false;
                return table;
            }
        }

        /// <summary>
        /// ��� ������,����������� ��������� ������
        /// </summary>
        private DataTable GetControlDataAll()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertControlFrom, _ID);
                return driverDb.GetDataTable(Sql);
            }
        }


        /// <summary>
        /// ��� ��������������� ������ ������
        /// </summary>
        private DataTable GetLockedData()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertFrom, _ID);
                return driverDb.GetDataTable(Sql);
            }
        }

        /// <summary>
        /// ���������� ����� �� ������������ �������� �� ����
        /// </summary>
        /// <returns></returns>
        public bool IsExist(ref string messageAboutExist)
        {
            bool isExist = false;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrderId, AgroQuery.SqlVehicleIdent,
    _Mobitel_Id);
                driverDb.NewSqlParameterArray(3);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Y", _dateDoc.Year);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "M", _dateDoc.Month);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "D", _dateDoc.Day);
                driverDb.GetDataReader(Sql, driverDb.GetSqlParameterArray);

                if (driverDb.Read())
                {
                    _ID = driverDb.GetInt32("Id");
                    messageAboutExist += string.Format("{0}\t{1}\t{2}\n", driverDb.GetInt32("Id").ToString(),
                                                       driverDb.GetDateTime("Date").ToString(),
                                                       driverDb.GetString("Veh"));
                    driverDb.CloseDataReader();
                    isExist = true;
                }

                driverDb.CloseDbConnection();    
            }
            return isExist;
        }

        //��������� �������� ����� ���������� �� ����
        public override void UpdateDocTotals()
        {
            SetStatusEvent(Resources.DateDayCreate);
            _stateOrder = GetStateOrder();
            BaseReports.Procedure.IAlgorithm brKmDay =
                (BaseReports.Procedure.IAlgorithm) new BaseReports.Procedure.KilometrageDays();
            brKmDay.SelectItem(m_row);
            brKmDay.Run();
            atlantaDataSet.KilometrageReportDayRow[] km_Day_rows =
                (atlantaDataSet.KilometrageReportDayRow[])
                    _dsAtlanta.KilometrageReportDay.Select("MobitelId = " + _Mobitel_Id);
            if (km_Day_rows.Length > 0)
            {
                DateTime InitialTime = km_Day_rows[0].InitialTime;
                DateTime FinalTime = (km_Day_rows.Length == 1)
                    ? km_Day_rows[0].FinalTime
                    : (km_Day_rows[km_Day_rows.Length - 1].Distance == 0 ? km_Day_rows[km_Day_rows.Length - 2].FinalTime : km_Day_rows[km_Day_rows.Length - 1].FinalTime);
                string LocationStart = (km_Day_rows[0].LocationStart.Length > 250)
                    ? km_Day_rows[0].LocationStart.Substring(0, 250)
                    : km_Day_rows[0].LocationStart;
                string LocationEnd = (km_Day_rows[km_Day_rows.Length - 1].LocationEnd.Length > 250)
                    ? km_Day_rows[km_Day_rows.Length - 1].LocationEnd.Substring(0, 250)
                    : km_Day_rows[km_Day_rows.Length - 1].LocationEnd;
                TimeSpan tsWork;
                TimeSpan IntervalWork = TimeSpan.Zero;
                TimeSpan IntervalMove = TimeSpan.Zero;
                double Distance = 0;
                double AverageSpeed = 0;
                for (int i = 0; i < km_Day_rows.Length; i++)
                {
                    if (TimeSpan.TryParse(km_Day_rows[i].IntervalWork, out tsWork))
                    {
                        IntervalWork = IntervalWork.Add(tsWork);
                    }
                    if (TimeSpan.TryParse(km_Day_rows[i].IntervalMove, out tsWork))
                    {
                        IntervalMove = IntervalMove.Add(tsWork);
                    }
                    if (!double.IsInfinity(km_Day_rows[i].Distance)) Distance += km_Day_rows[i].Distance;
                    if (!double.IsInfinity(km_Day_rows[i].AverageSpeed)) AverageSpeed += km_Day_rows[i].AverageSpeed;
                    
                }
                if (km_Day_rows.Length > 0) AverageSpeed = AverageSpeed/km_Day_rows.Length;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    GetSquareWork();
                    GetPathWithoutWork();
                    string sSQLupdate = "";
                    sSQLupdate = string.Format(AgroQuery.OrderItem.UpdateAgroOrderExist, _ID);

                    driverDb.NewSqlParameterArray(27);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", InitialTime);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", FinalTime);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeWork",
                        IntervalWork.ToString().Substring(0, 5));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeMove",
                        IntervalMove.ToString().Substring(0, 5));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LocationStart", LocationStart);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LocationEnd", LocationEnd);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Distance", Math.Round(Distance, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SpeedAvg", Math.Round(AverageSpeed, 2));
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareWorkDescript", SquareWorkDescript);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PathWithoutWork", PathWithoutWork);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensSquare", FuelDUTExpensSquare);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensAvgSquare", FuelDUTExpensAvgSquare);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensWithoutWork",
                        FuelDUTExpensWithoutWork);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDUTExpensAvgWithoutWork",
                        FuelDUTExpensAvgWithoutWork);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensSquare", FuelDRTExpensSquare);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensAvgSquare", FuelDRTExpensAvgSquare);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensWithoutWork",
                        FuelDRTExpensWithoutWork);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FuelDRTExpensAvgWithoutWork",
                        FuelDRTExpensAvgWithoutWork);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SquareWorkDescripOverlap",
                        SquareWorkDescriptOverlap);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "FactSquareCalcOverlap", FactSquareCalcOverlap);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateLastRecalc", DateTime.Now);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserLastRecalc", UserBaseCurrent.Instance.Name);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsValidity", _PointsValidity);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsCalc", _PointsCalc);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsFact", _PointsFact);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "PointsIntervalMax", _PointsIntervalMax);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "StateOrder", _stateOrder);

                    driverDb.ExecuteNonQueryCommand(sSQLupdate, driverDb.GetSqlParameterArray);
                    _dateLastRecalc = DateTime.Now;
                    _userRecalced = UserBaseCurrent.Instance.Name;
                }
                return;
            }
        }

        public override sealed bool GetDocById(int id)
        {
            try
            {
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrderVehicle, AgroQuery.SqlVehicleIdent, id);

                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    driverDb.GetDataReader(Sql);
                    if (driverDb.Read())
                    {
                        _Mobitel_Id = driverDb.GetInt32("Id_mobitel");
                        _Team_id = (int) TotUtilites.NdbNullReader(driverDb, "Team_id", 0);
                        _Remark = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                        _dateDoc = driverDb.GetDateTime("Date");
                        _dateLastRecalc = (DateTime) TotUtilites.NdbNullReader(driverDb, "DateLastRecalc", _dateDoc);
                        _MobitelName = TotUtilites.NdbNullReader(driverDb, "MobitelName", "").ToString();
                        _LocationStart = TotUtilites.NdbNullReader(driverDb, "LocationStart", "").ToString();
                        _LocationEnd = TotUtilites.NdbNullReader(driverDb, "LocationEnd", "").ToString();
                        _SpeedAvg = (double) TotUtilites.NdbNullReader(driverDb, "SpeedAvg", 0.0);
                        _Distance = (double) TotUtilites.NdbNullReader(driverDb, "Distance", 0.0);
                        _TimeStart = (DateTime) TotUtilites.NdbNullReader(driverDb, "TimeStart", DateTime.MinValue);
                        _TimeEnd = (DateTime) TotUtilites.NdbNullReader(driverDb, "TimeEnd", DateTime.MinValue);
                        _TimeWork = TotUtilites.NdbNullReader(driverDb, "TimeWork", "").ToString().TrimEnd();
                        _TimeMove = TotUtilites.NdbNullReader(driverDb, "TimeMove", "").ToString().TrimEnd();
                        //TimeSpanConverter tsc = new TimeSpanConverter();
                        TimeSpan tsWork;
                        if (TimeSpan.TryParse(_TimeWork, out tsWork))
                        {
                            TimeSpan tsMove;
                            if (TimeSpan.TryParse(_TimeMove, out tsMove))
                            {
                                _TimeStop = tsWork.Subtract(tsMove).ToString().Substring(0, 5);
                            }
                        }
                        _PathWithoutWork = (double) TotUtilites.NdbNullReader(driverDb, "PathWithoutWork", 0);
                        _factSquareCalcOverlap =
                            (double) TotUtilites.NdbNullReader(driverDb, "FactSquareCalcOverlap", 0);
                        SquareWorkDescript = TotUtilites.NdbNullReader(driverDb, "SquareWorkDescript", "").ToString();
                        SquareWorkDescriptOverlap =
                            TotUtilites.NdbNullReader(driverDb, "SquareWorkDescripOverlap", "").ToString();
                        _FuelDUTExpensSquare = (double) TotUtilites.NdbNullReader(driverDb, "FuelDUTExpensSquare", 0.0);
                        _FuelDUTExpensAvgSquare =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDUTExpensAvgSquare", 0.0);
                        _FuelDUTExpensWithoutWork =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDUTExpensWithoutWork", 0.0);
                        _FuelDUTExpensAvgWithoutWork =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDUTExpensAvgWithoutWork", 0.0);
                        _FuelDRTExpensSquare = (double) TotUtilites.NdbNullReader(driverDb, "FuelDRTExpensSquare", 0.0);
                        _FuelDRTExpensAvgSquare =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDRTExpensAvgSquare", 0.0);
                        _FuelDRTExpensWithoutWork =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDRTExpensWithoutWork", 0.0);
                        _FuelDRTExpensAvgWithoutWork =
                            (double) TotUtilites.NdbNullReader(driverDb, "FuelDRTExpensAvgWithoutWork", 0.0);

                        _userCreated = TotUtilites.NdbNullReader(driverDb, "UserCreated", "").ToString();
                        _userRecalced = TotUtilites.NdbNullReader(driverDb, "UserLastRecalc", "").ToString();

                        _PointsValidity = (int) TotUtilites.NdbNullReader(driverDb, "PointsValidity", 0);
                        _PointsCalc = (int) TotUtilites.NdbNullReader(driverDb, "PointsCalc", 0);
                        _PointsFact = (int) TotUtilites.NdbNullReader(driverDb, "PointsFact", 0);
                        _PointsIntervalMax = TotUtilites.NdbNullReader(driverDb, "PointsIntervalMax", 0).ToString();

                        _stateOrder = (int) TotUtilites.NdbNullReader(driverDb, "StateOrder", 0);
                        _blockUser = (int) TotUtilites.NdbNullReader(driverDb, "BlockUserId", 0);
                        _blockStartDate =
                            (DateTime) (TotUtilites.NdbNullReader(driverDb, "BlockDate", DateTime.MinValue));
                        Remark = TotUtilites.NdbNullReader(driverDb, "Comment", "").ToString();
                        ;
                    }
                    driverDb.CloseDataReader();
                }
                return true;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "OrderItem.GetDocById(int id):701",
                    MessageBoxButtons.OK);
                return false;
            }
        }

        public void UpdateStateOrder()
        {
            _stateOrder = GetStateOrder();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.OrderItem.UpdateAgroOrderState, _stateOrder, _ID);
                driverDb.ExecuteNonQueryCommand(sql);
            }
        }

        public override void UpdateBlockState(int id_user, DateTime? blockDate)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = "";
                sql = string.Format(AgroQuery.OrderItem.UpdateAgroOrderBlock, _ID);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "BlockUserId", id_user);

                if (blockDate == null)
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "BlockDate", DBNull.Value);
                else
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "BlockDate", blockDate);

                driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                _blockUser = id_user;
            }
        }

        public void UpdateDocAfterSplitRecords()
        {
            if (_dsAtlanta == null)
            {
                if (Create_dsAtlanta(_dateDoc, _dateDoc.AddDays(1).AddMinutes(-1)) == 0)
                {
                    SetStatusEvent(Resources.Ready);
                    return;
                }
                _oirs = new List<OrderItemRecord>();
            }
            SetStatusEvent(Resources.DateDayCreate);
            _oirs.Clear();
            _oirs = GetOrderItemRecords();
            GetSquareWork();
            UpdateDocTotals();
            ContentDataSetReturnClear();
            UserLog.InsertLog(UserLogTypes.AGRO, Resources.OrderRecordsCorrection, ID);
        }

        #endregion

        #region ���������� �������

        /// <summary>
        /// ��������� ������� �� ��������� � ��������� ��� �����.
        /// ��������� ������ ��� ��������� ���������� �������
        /// ������� ������� ������ ����������� ��� ������� �� �����, ���������, ���������
        /// ��������� 05.10.2011 �� ��������� �������: ���������� ����������� ������ � ���� ��� - ����� ������� ��������� ��� �� ����
        /// </summary>
        private void ContentCalcControlObject(Consts.TypeControlObject tco, string sFieldObj)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DateTime LastControlTime = ContentGetLastControlTime(tco);
                string sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrderObj, _ID);

                driverDb.NewSqlParameterArray(1);
                if (LastControlTime.Year <= 1900)
                    LastControlTime = new DateTime(1900, 1, 1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "LastControlTime", LastControlTime);
                driverDb.GetDataReader(sSQL, driverDb.GetSqlParameterArray);

                int Id_obj_cur = 0;
                int Id_obj_prev = 0;
                DateTime dtStart = new DateTime();
                DateTime dtEnd = new DateTime();
                bool bFirst = true;
                while (driverDb.Read())
                {
                    if (bFirst)
                    {
                        dtStart = driverDb.GetDateTime(driverDb.GetOrdinal("TimeStart"));

                        if (LastControlTime.Subtract(dtStart).TotalSeconds > 0)
                            dtStart = LastControlTime;

                        Id_obj_prev = driverDb.GetInt32(driverDb.GetOrdinal(sFieldObj));
                        bFirst = false;
                    } // if

                    Id_obj_cur = driverDb.GetInt32(driverDb.GetOrdinal(sFieldObj));

                    if (Id_obj_cur != Id_obj_prev)
                    {
                        //������ ���������� ��� ����������� ������
                        InsertControlObject(tco, driverDb, ref sSQL, Id_obj_prev, dtStart, dtEnd);
                        dtStart = dtEnd; //driverDb.GetDateTime(driverDb.GetOrdinal("TimeEnd"));
                        Id_obj_prev = Id_obj_cur;
                    } // if
                    dtEnd = driverDb.GetDateTime("TimeEnd");
                } // while
                if (!bFirst)
                {
                    InsertControlObject(tco, driverDb, ref sSQL, Id_obj_prev, dtStart, dtEnd);
                } // if
                driverDb.CloseDataReader();
            } // using
        } // ContentCalcControlObject

        private void InsertControlObject(Consts.TypeControlObject tco, DriverDb driverDb, ref string sSQL,
            int Id_obj_prev, DateTime dtStart, DateTime dtEnd)
        {
            sSQL = String.Format(AgroQuery.OrderItem.InsertIntoAgroOrdertControl, _ID, Id_obj_prev, (int) tco, dtStart,
                dtEnd);

            using (DriverDb nestedDriverDb = new DriverDb())
            {
                nestedDriverDb.ConnectDb();
                nestedDriverDb.NewSqlParameterArray(2);
                nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", dtStart);
                nestedDriverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", dtEnd);
                nestedDriverDb.ExecuteNonQueryCommand(sSQL, nestedDriverDb.GetSqlParameterArray);
            }
        }

        /// <summary>
        /// �������� ������������ ��������� �� ����
        /// </summary>
        /// <param name="dt"> ����� ����������� ������ </param>
        /// <param name="tco"> ��� ��������� </param>
        /// <param name="dtFind"> ���� ��� ������ </param>
        /// <returns></returns>
        private int ContentGetControlObject(DataTable dt, Consts.TypeControlObject tco, DateTime dtFind)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if ((Convert.ToInt32(dr["Type_object"]) == (int) tco) &&
                    (dtFind.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalMinutes >= 0)
                    && (dtFind.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalMinutes < 0))
                    return Convert.ToInt32(dr["Id_object"]);
            }
            return 0;
        }

        /// <summary>
        /// �������� ������������ ��������� �� ���� �����
        /// </summary>
        /// <param name="dt"> ����� ����������� ������ </param>
        /// <param name="tco"> ��� ��������� </param>
        /// <param name="dtFind"> ���� ������ ������ </param>
        /// <param name="dtEnd">���� ���������</param>
        /// <returns></returns>
        private int ContentGetControlObjectExactly(Consts.TypeControlObject tco, DateTime dtStart, OrderItemRecord oir)
        {
            if (_dtControl.Rows.Count == 0)
            {
            return TestPathSquareZone(oir) ? 1 : 0;
            }

            DateTime dtEnd = oir.GpsDataZone[oir.GpsDataZone.Count - 1].Time;
            return (from DataRow dr in _dtControl.Rows where (Convert.ToInt32(dr["Type_object"]) == (int) tco) && ((int) dtStart.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalSeconds == 0) && ((int) (dtEnd.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalSeconds) == 0) select Convert.ToInt32(dr["Id_object"])).FirstOrDefault();
        }

        private bool ContentPointLocked(GpsData gpsData)
        {
            if (_dtLock.Rows.Count == 0) return false;
            DateTime dtTest = gpsData.Time;
            foreach (DataRow dr in _dtLock.Rows)
            {
                if (((int) gpsData.Time.Subtract(Convert.ToDateTime(dr["TimeStart"])).TotalSeconds > 0)
                    && ((int) (gpsData.Time.Subtract(Convert.ToDateTime(dr["TimeEnd"])).TotalSeconds) <= 0))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// �������� ���� ����������� ����������
        /// </summary>
        private bool ContentDeleteControlObjects()
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    Sql = string.Format(AgroQuery.OrderItem.DeleteAgroOrdertControl, _ID);
                    driverDb.ExecuteNonQueryCommand(Sql);
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// ���������  ���� � ���������� �������
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="tco"></param>
        /// <returns></returns>
        private DateTime ContentGetLastControlTime(Consts.TypeControlObject tco)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSQL = string.Format(AgroQuery.OrderItem.SelectAgroOrdertControlTime, _ID, (int) tco);

                driverDb.GetDataReader(sSQL);
                DateTime timeForReturn = _TimeStart;
                if (driverDb.Read())
                {
                    timeForReturn = driverDb.GetDateTime("TimeEnd");
                }
                driverDb.CloseDataReader();
                return timeForReturn;

            }
        }

        private int GetStateOrder()
        {
            _dtLock = GetLockedData();
            int lockedRecords = _dtLock.Rows.Count;
            int totalRecords = GetRecordsCount();
            if (totalRecords == 0 || lockedRecords == 0)
                return (int) StatesOrder.New;
            else if (totalRecords > lockedRecords)
                return (int) StatesOrder.Updated;
            else
            {
                if (_lastGpsTime == DateTime.MinValue) _lastGpsTime = GetLastDataGps();
                if (_lastGpsTime == DateTime.MinValue) return (int) StatesOrder.Tested;
                if (_dateDoc.AddDays(1).Subtract(_lastGpsTime).TotalMinutes < 5)
                    return (int) StatesOrder.Closed;
                else
                    return (int) StatesOrder.Tested;
            }
        }

        public int GetRecordsCount()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql = string.Format(AgroQuery.OrderItem.SelectAgroOrdertCount, _ID);
                return driverDb.GetScalarValueNull<int>(Sql, 0);
            }
        }

        public DateTime GetLastDataGps()
        {
            return MobitelProvider.GetLastTimeGps(_Mobitel_Id);
        }

        public bool LockRecords(bool lockRecord)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = string.Format(AgroQuery.OrderItem.UpdateAgroOrdertRecord, lockRecord ? 1 : 0, _ID);
                    driverDb.ExecuteNonQueryCommand(sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Atlanta

        private atlantaDataSet.mobitelsRow m_row;
        private atlantaDataSet.settingRow set_row;
        private TimeSpan _legalTimeBreak = new TimeSpan(0, 5, 0);
        public int Create_dsAtlanta(DateTime dtStart, DateTime dtEnd)
        {
            if (_dsAtlanta == null) _dsAtlanta = new atlantaDataSet();
            SetStatusEvent(Resources.DataInitSelect);
            var lci = new LocalCacheItem(dtStart, dtEnd, _Mobitel_Id);
            var gpsProvider = new GetGpsData(GpsDataProvider.GetGpsDataAllProtocols);
            int iRecords = lci.CreateDsAtlantaWGpsDataProvider(ref _dsAtlanta, gpsProvider);
            if (iRecords == 0) return 0;
            GpsDatasDocItem = lci.GpsDatas;
            m_row = (LocalCache.atlantaDataSet.mobitelsRow) _dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
            if (m_row == null) return 0;
            m_row.Check = true;
            var vehicle = new VehicleInfo(m_row);
            atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();

            set_row = (vehicles == null || vehicles.Length == 0)
                ? null
                : _dsAtlanta.setting.FindByid(vehicles[0].setting_id);

            _Vehicle_Id = (vehicles == null || vehicles.Length == 0) ? 0 : vehicles[0].id;

            if (set_row != null)
            {
                _legalTimeBreak = set_row.TimeBreak;
            }
            //����������� ���������� �������� ������ ������ ������������.� ����� ��������� �������� ��������� 
            _dsFromAlgorithm = Algorithm.AtlantaDataSet;
            Algorithm.RunFromModule = true;
            Algorithm.AtlantaDataSet = _dsAtlanta;
            Algorithm.GpsDatas = GpsDatasDocItem;
            SetMinMaxGPS(_Mobitel_Id);
            return iRecords;
        }

        #endregion

        #region Get

        private void GetSensor(AlgorithmType alg, out int senLength, out int senStartBit)
        {

            var sensor = new Sensor((int) alg, Mobitel_Id);
            //Algorithm Alg = new Algorithm();
            ////Alg.SelectItem(m_row);
            //atlantaDataSet.sensorsRow sensor = Alg.FindSensor(alg);
            if (sensor.Id >0)
            {
                senLength = (int)sensor.BitLength;
                senStartBit = (int)sensor.StartBit;
            }
            else
            {
                senLength = 0;
                senStartBit = 0;
            }
        }

        /// <summary>
        /// ����������� ��������� ��������� ��� ����� GPS
        /// </summary>
        /// <param name="DataGps_ID"></param>
        /// <returns></returns>
        private bool GetFactZoneRotation(System.Int64 DataGps_ID)
        {
            var rtRows = (atlantaDataSet.RotateReportRow[])
                    _dsAtlanta.RotateReport.Select("InitialPointId <= " + DataGps_ID + " AND FinalPointId > =" +
                                                   DataGps_ID);
            if (rtRows.Length == 0) return false;
            if (rtRows[0].State == Resources.Worked) return true;
            return false;
        }

        /// <summary>
        /// ����������� ���� �� ������
        /// </summary>
        /// <param name="iWorkType"></param>
        /// <returns></returns>
        private double GetPrice(int iWorkType)
        {
            if ((iWorkType == 0) || (_Mobitel_Id == 0)) return 0;
            double dbPrice = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sqLselect = string.Format(AgroQuery.OrderItem.SelectAgroPricetPrice, iWorkType, _Mobitel_Id);

                //Console.WriteLine(SQLselect);  
                driverDb.NewSqlParameterArray(1);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateComand", _dateDoc);
                driverDb.GetDataReader(sqLselect, driverDb.GetSqlParameterArray);
                if (driverDb.Read())
                {
                    double dbFactor = driverDb.GetDouble(driverDb.GetOrdinal("Factor"));
                    dbPrice = Math.Round(driverDb.GetDouble(driverDb.GetOrdinal("Price"))*dbFactor/100, 2);
                }
                driverDb.CloseDataReader();
            }
            return dbPrice;
        }

        private int GetZone(int idField)
        {
            using (var daf = new DictionaryAgroField(idField))
            {
                return daf.IdZone;
            }
        }

        private int GetFieldId(int zoneId)
        {
            if (zoneId < 0)
            {
                return -zoneId;
            }
            else if (zoneId == 0)
            {
                return 0;
            }
            else
            {
                using (var daf = new DictionaryAgroField())
                {
                    return daf.GetIdFromZoneId(zoneId);
                }
            }
        }

        public int GetDriversCount(int idOrder)
        {
            int countDrivers = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sqLselect = string.Format(AgroQuery.OrderItem.SelectDriversCount, idOrder);
                driverDb.GetDataReader(sqLselect);
                while (driverDb.Read())
                {
                    countDrivers ++;
                }
                driverDb.CloseDataReader();
            }
            return countDrivers;
        }

        public double[] GetSensorsCalibratedData(int algorithm, SensorCalibrated sensorMobitel, GpsData[] gpsDatas)
        {
            var sensorValues = new List<double>();
            var agregatSensors = new Dictionary<int, SensorCalibrated>();
            foreach (var oir in GetOrderItemRecords())
            {
                var gpsOir = gpsDatas.Where(gps => gps.Time >= oir.TimeStart && gps.Time < oir.TimeEnd).ToList();
                if (oir.AgregatId > 0)
                {
                    if (agregatSensors.ContainsKey(oir.AgregatId))
                    {
                        sensorValues.AddRange(gpsOir.Select(gps => agregatSensors[oir.AgregatId].GetValue(gps.Sensors)).ToList());
                    }
                    else
                    {
                        var daa = new DictionaryAgroAgregat(oir.AgregatId);
                        if (daa.HasSensor && daa.IsUseAgregatState)
                        {
                            var calibrRecords = daa.GetCalibrationRecords();
                            if (calibrRecords.Count > 0)
                            {
                                var sensor = new SensorCalibrated(algorithm, _Mobitel_Id, calibrRecords);
                                sensorValues.AddRange(gpsOir.Select(gps => sensor.GetValue(gps.Sensors)).ToList());
                                agregatSensors.Add(oir.AgregatId, sensor);
                            }
                            else
                            {
                                var sensor = new SensorCalibrated(algorithm, _Mobitel_Id)
                                    {
                                        K = daa.CoefficientK,
                                        B = daa.ShiftB,
                                        S = daa.Signed
                                    };
                                sensorValues.AddRange(gpsOir.Select(gps => sensor.GetValue(gps.Sensors)).ToList());
                                agregatSensors.Add(oir.AgregatId, sensor);
                            }
                        }
                        else
                        {
                            sensorValues.AddRange(gpsOir.Select(gps => sensorMobitel.GetValue(gps.Sensors)).ToList());
                            agregatSensors.Add(oir.AgregatId, sensorMobitel);
                        }
                    }

                }
                else
                {
                    sensorValues.AddRange(gpsOir.Select(gps => sensorMobitel.GetValue(gps.Sensors)).ToList());
                }
            }
            return sensorValues.ToArray();
        }
        #endregion

        #region IDisposable Members

        public override void Dispose()
        {
            Algorithm.RunFromModule = false;
            if (_oirs != null) _oirs.Clear();
            if (_dsAtlanta != null) _dsAtlanta.Clear();
            base.Dispose();
            //if (GpsDatasDocItem != null) GpsDatasDocItem.;

        }

        #endregion
    }
}
