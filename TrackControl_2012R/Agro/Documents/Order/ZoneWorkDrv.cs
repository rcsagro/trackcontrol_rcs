using System;
using System.Collections.Generic;
using System.Text;
using LocalCache;

namespace Agro
{
    /// <summary>
    /// ������� ��������� ��� �������� ���������� ����� ������ ������
    /// </summary>
    public struct ZoneWorkDrv
    {
        public List<atlantaDataSet.dataviewRow> lsDrows;
        public int ZoneID;
        public int DriverID;
        public int AgregatID;
        public int FieldID;
        public int ID_orderT;
        public Double Distance;
        public Double Square;
        public Double SquareCalcCont;
        public TimeSpan tsMove;
        public TimeSpan tsStop;
        public TimeSpan tsEngineOn;
        public int RowHandle;
        public bool AddNewRow;
        public double WidthAgr;
        public int WorkId;
        public double Price;
        public double FuelStart;
        public double FuelEnd;
        public double FuelAdd;
        public double FuelSub;
        public double FuelExpens;
        public double FuelExpensAvg;
        public double FuelExpensAvgRate;
        public double Fuel_ExpensMove;
        public double Fuel_ExpensStop;
        public double Fuel_ExpensTotal;
        public double Fuel_ExpensAvg;
        public double Fuel_ExpensAvgRate;
        public ZoneWorkDrv(int iZoneID, int iDriverID, List<atlantaDataSet.dataviewRow> lsDvrows)
        {
            ZoneID = iZoneID;
            DriverID = iDriverID;
            AgregatID = 0;
            FieldID = 0;
            Distance = 0;
            tsMove = new TimeSpan();
            tsStop = tsMove;
            tsEngineOn = tsMove;
            FuelStart = 0;
            FuelEnd = 0;
            FuelAdd = 0;
            FuelSub = 0;
            FuelExpens = 0;
            FuelExpensAvg = 0;
            FuelExpensAvgRate = 0;
            Fuel_ExpensMove = 0;
            Fuel_ExpensStop = 0;
            Fuel_ExpensTotal = 0;
            Fuel_ExpensAvg = 0;
            Fuel_ExpensAvgRate = 0;
            Square = 0;
            SquareCalcCont = 0;
            lsDrows = lsDvrows;
            ID_orderT = 0;
            RowHandle = 0;
            AddNewRow = false;
            Price = 0;
            WidthAgr = 0;
            WorkId = 0;
        }
    }
}
