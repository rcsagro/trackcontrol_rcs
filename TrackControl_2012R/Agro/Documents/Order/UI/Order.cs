using Agro.Controls;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using Agro.XtraReports;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraReports.UI;
using DevExpress.XtraVerticalGrid.Rows;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace Agro
{
    public partial class Order : DevExpress.XtraEditors.XtraForm,IFormDictionary
    {

        #region ����
        //private AgroMapOrder _map;
        private AgroGraphFuel _gpaphFuel;
        /// <summary>
        /// ��������� ��������� ��������
        /// </summary>
        private bool bLoad = true;
        /// <summary>
        /// ���������� ��������
        /// </summary>
        private bool _bDeleteEnable = false;
        private bool _bSaveEnable = false;

        private OrderItem _oi;
        private bool _newDoc = false;
        private bool _readOnly;
        private List<int> _listActiveZones = new List<int>();


        #endregion

        #region ������������

        internal Order()
        {
            InitializeComponent();
            Localization();
            LoadLookUpControls();
            SetControls();
        }

        private void LoadLookUpControls()
        {
            DicUtilites.LookUpLoad(leGroupe, AgroQuery.Order.SelectAgroOrdertTimeStart);
            DicUtilites.LookUpLoad(leMobitel, AgroQuery.Order.SelectVehicleMobitelId);
            leWorkType.DataSource = DictionaryAgroWorkType.GetList();
            leAgregat.DataSource = DictionaryAgroAgregat.GetList();
            leDriver.DataSource = DriverVehicleProvider.GetList();
        }

        public Order(int idOrder, bool bNew)
            : this()
        {
            if (!bNew)
            {
                _oi = new OrderItem {ID = idOrder};
                GetFields();
                //LoadContent();
                xtbCont.SelectedTabPage = xtraTabMap;
                bLoad = false;
                deDateInit.Properties.ReadOnly = true;
                leGroupe.Properties.ReadOnly = true;
                leMobitel.Properties.ReadOnly = true;
                btSave.Caption = Resources.Save;
                _newDoc = false;
                UserLog.InsertLog(UserLogTypes.AGRO, Resources.Opening, idOrder);
                SetControls(); 
            }
            else
            {
                _newDoc = true;
                deDateInit.DateTime = DateTime.Today;
                btSave.Caption = Resources.Create;
                bLoad = false;
            }
            TotUtilites.FormSize800_600(this);
            scControl.SplitterPosition = this.Width / 3;
            scControlInner.SplitterPosition = scControlInner.Location.X + this.Width / 3;
            SetStatus(Resources.Ready);
            btSave.Enabled = false;
        }
        #endregion

        #region IFormDictionary Members

        public bool ValidateFields()
        {
            if (!DicUtilites.ValidateLookUp(leMobitel, dxErrP) ||!DicUtilites.ValidateDateEdit(deDateInit ,dxErrP))
                return false;
            else
                return true;
        }

        public bool ValidateFieldsAction()
        {
            if (!ValidateFields())
            {
                btSave.Enabled = false;
                return false;
            }
            else
            {
                btSave.Enabled = true;
                return true;
            }
        }

        public void SaveChanges(bool question)
        {
            if (!ValidateFields()) return;
            if (txId.Text == "0")
            {
                SetStatus(Resources.OrderCreating);
                _oi = new OrderItem((DateTime)deDateInit.DateTime, (int)leMobitel.EditValue) {Remark = meRemark.Text};
                txId.Text = _oi.AddDoc().ToString();
                if (txId.Text != "0")
                {
                    _oi.ChangeStatusEvent += SetStatus;
                    _oi.ContentAutoCreate(false);
                    _oi.ChangeStatusEvent -= SetStatus;
                    LoadContent();
                }
            }
            else
            {
                SetFields();
                _oi.UpdateDoc();
            }
            btSave.Enabled = false;
            GetFields();
            SetStatus(Resources.Ready);
        }

        public bool TestLinks()
        {
            return true;
        }

        public bool DeleteRecord(bool question)
        {
            if (_oi.DeleteDoc(true))
            {
                if (!_newDoc)
                {
                    //DataRow row = _gvGrn.GetDataRow(_gvGrn.FocusedRowHandle);
                    //row.Delete();
                }
                return true;
            }
            else
                return false;
        }

        public void SetControls()
        {
            if (_readOnly)
            {
                meRemark.Properties.ReadOnly = true;
                LockMenuBar(true);
                gcContent.Enabled = false; 
                gcContentGrp.Enabled = false;
                gcFuelDRT.Enabled = false;
                gcContentGrp.Enabled = false;
                gcControlAgr.Enabled = false;
                gcControlDrv.Enabled = false;
                gcControlField.Enabled = false;
            }
            else
            {
                meRemark.Properties.ReadOnly = false;
                LockMenuBar(false);
            }
            switch (xtbCont.SelectedTabPageIndex)
            {
                case 0:
                    {
                        btExcel.Enabled = true;
                        break;
                    }
                case 1:
                    {
                        btExcel.Enabled = true;
                        break;
                    }
                case 2:
                    {
                        btExcel.Enabled = true;
                        break;
                    }
                case 3:
                    {
                        btExcel.Enabled = true;
                        break;
                    }
                case 4:
                    {
                        btExcel.Enabled = false;
                        break;
                    }
                case 5:
                    {
                        btExcel.Enabled = false;
                        break;
                    }
            }
        }

        public void GetFields()
        {

            _oi.GetDocById(_oi.ID);
            if (!leMobitel.Properties.ReadOnly)
            {
                leGroupe.EditValue = _oi.Team_id;
                SetLeMobitel();
                leMobitel.EditValue = _oi.Mobitel_Id;
                deDateInit.DateTime = _oi.Date;
            }
                txId.Text = _oi.ID.ToString();   
                meRemark.Text = _oi.Remark;
                _oi.UpdateStateOrder(); 
                pgOrder.SelectedObject = _oi;
                stateIndicatorComponent1.StateIndex = _oi.PointsValidityGauge;
                pgOrder.Refresh();
                this.Text = string.Format("{0} # {1}", Resources.Order, _oi.ID);
                SetIconToFormCaption();
                if (_oi.IsDocBlocked)
                    _readOnly = true;
                else
                {
                    _readOnly = false;
                    _oi.UpdateBlockState(UserBaseCurrent.Instance.Id, DateTime.Now);
                }
        }

        private void SetIconToFormCaption()
        {
            switch (_oi.StateOrder)
            {
                case (int)StatesOrder.New:
                    {
                        this.Icon = Shared.DocNew;
                        break;
                    }
                case (int)StatesOrder.Updated:
                    {
                        this.Icon = Shared.DocUpdate;
                        break;
                    }
                case (int)StatesOrder.Tested:
                    {
                        this.Icon = Shared.DocTest;
                        break;
                    }
                case (int)StatesOrder.Closed:
                    {
                        this.Icon = Shared.DocClose;
                        break;
                    }
                default:
                    {
                        this.Icon = Shared.DocNew;
                        break;
                    }
            }
        }

        public void SetFields()
        {
            _oi.Date = deDateInit.DateTime;
            _oi.Mobitel_Id = (int)leMobitel.EditValue;
            _oi.Remark = meRemark.Text; 
        }

        public void UpdateGRN()
        {
            //throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region ������
        private void btSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveChanges(true);
            btSave.Enabled = false;
        }
        /// <summary>
        /// ������������ ����������� ������
        /// </summary>
        private void btFact_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FactCals(false);
        }
        private void btFactControl_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FactCals(true);
        }
        private void FactCals(bool bControl)
        {
            int ID = 0;
            if (Int32.TryParse(txId.Text, out ID))
            {
                if (_readOnly) return;
                if (DialogResult.No == XtraMessageBox.Show(Resources.OrderRecordsRefreshConfirm, Resources.ApplicationName, MessageBoxButtons.YesNo)) return;
                LockMenuBar(true);
                AgroController.Instance.MapOrderControl.ReadOnly = true;
                SetStatus(Resources.OrderRecordsRefresh);
                _oi.ChangeStatusEvent += SetStatus;
                _oi.ContentAutoCreate(bControl);
                _oi.ChangeStatusEvent -= SetStatus;
                GetFields();
                LoadContent();
                LockMenuBar(false);
                AgroController.Instance.MapOrderControl.ReadOnly = false;
                SetStatus(Resources.Ready);
            }
        }
        private void btMapView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (leMobitel.EditValue != null)
            {
                if (DialogResult.No == XtraMessageBox.Show(Resources.OrderMapViewConfirm, Resources.ApplicationName, MessageBoxButtons.YesNo)) return;
                //this.WindowState = FormWindowState.Minimized;
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.GetType().ToString() == "Agro.MainAgro")
                    {
                        frm.WindowState = FormWindowState.Minimized;
                        break;
                    }
                }
                //var ea = new Form_Utils.DrawEventArgs(new DateTime(deDateInit.DateTime.Year, deDateInit.DateTime.Month, deDateInit.DateTime.Day, 0, 0, 0), deDateInit.DateTime, (int)leMobitel.EditValue, _listActiveZones);
                var ea = new Form_Utils.DrawEventArgs(deDateInit.DateTime, deDateInit.DateTime.AddDays(1).AddMinutes(-1), (int)leMobitel.EditValue, _listActiveZones);
                Form_Utils.DrawMapEventHandler(this, ea);
            }
        }
        private void btRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveChanges(true);
            SetStatus(Resources.OrderRefresh);
            GetFields();
            LoadContent();
            btSave.Enabled = false;
            SetStatus(Resources.Ready);

        }
        private void btExcelTotal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ExcelOutputConfim, Resources.ApplicationName, MessageBoxButtons.YesNo)) return;
            string sFileName = Resources.Order  + "_" + txId.Text + StaticMethods.SuffixDateToFileName()   ; 

            string xslFilePath = StaticMethods.ShowSaveFileDialog(sFileName, "Excel", "Excel|*.xls");
            if (xslFilePath.Length > 0)
            {
                try
                {
                    foreach (BaseRow br in pgOrder.Rows)
                    {
                        br.Expanded = true;
                    }
                    pgOrder.ExportToXls(xslFilePath);
                    System.Diagnostics.Process.Start(xslFilePath);
                }
                catch (Exception)
                {
                    XtraMessageBox.Show(Resources.RewriteBun, Resources.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ExcelOutputConfim, Resources.ApplicationName, MessageBoxButtons.YesNo)) return;
            GridView gvPrint = gbvContent;
            string sFileName = xtbCont.SelectedTabPage.Text   + "_" + txId.Text + StaticMethods.SuffixDateToFileName();
            switch (xtbCont.SelectedTabPageIndex)
            {
                case 0:
                    {
                        gvPrint = gvContentGrp;
                        break;
                    }
                case 1:
                    {
                        gvPrint = gbvContent;
                        break;
                    }
                case 2:
                    {
                        gvPrint = _gpaphFuel.GridViewFuel;
                        break;
                    }
                case 3:
                    {
                        gvPrint = gvFuelDRT;
                        break;
                    }
                default:
                    {
                        return;
                    }
            }

            string xslFilePath = StaticMethods.ShowSaveFileDialog(sFileName, "Excel", "Excel|*.xls");
            if (xslFilePath.Length > 0)
            {
                try
                {
                    gvPrint.OptionsPrint.ExpandAllDetails = true;
                    gvPrint.OptionsPrint.PrintDetails = true;
                    gvPrint.OptionsPrint.AutoWidth = false;
                    gvPrint.BestFitColumns();
                    gvPrint.OptionsPrint.AutoWidth = true;
                    gvPrint.ExportToXls(xslFilePath);
                    System.Diagnostics.Process.Start(xslFilePath);
                }
                catch (Exception)
                {
                    XtraMessageBox.Show(Resources.RewriteBun, Resources.ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DeleteRecord(true)) this.Close();
        }
        private void btLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UserLog.ViewLog(UserLogTypes.AGRO, _oi.ID);
        }
        #region ������
        private void btReportContentGrouped_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderContentGrouped report = new OrderContentGrouped(Convert.ToInt32(txId.Text));
            report.ShowPreview(); 
          
        }

        private void btReportContent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderContent report = new OrderContent(Convert.ToInt32(txId.Text));
             report.ShowPreview(); 
        }

        private void btReportFuelDUT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderFuelDUT report = new OrderFuelDUT(Convert.ToInt32(txId.Text));
             report.ShowPreview();
        }

        private void btReportFuelDRT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderFuelDRT report = new OrderFuelDRT(Convert.ToInt32(txId.Text));
             report.ShowPreview(); 
        }

        private void btReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderTotal report = new OrderTotal(Convert.ToInt32(txId.Text));
             report.ShowPreview(); 
        }
        #endregion


#endregion    

        #region ����� ������
        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            SetLeMobitel();
        }
        private void cbMobitel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLoad)
                if (ValidateFields()) SaveChanges(true);
        }
        private void dtDateInit_ValueChanged(object sender, EventArgs e)
        {
            if (!bLoad)
                if (ValidateFields()) SaveChanges(true);
        }
        private void meRemark_EditValueChanged(object sender, EventArgs e)
        {
            if (!bLoad) ValidateFieldsAction();
        }
        private void Order_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (GetStatus() != Resources.Ready)
            {
                e.Cancel = true;
                return;
            }
            if (_oi != null)
            {
                if (_oi.BlockUser == UserBaseCurrent.Instance.Id) _oi.UpdateBlockState(0, null);
                _oi.Dispose();
                AgroController.Instance.MapOrderControl.RefreshFromControl -= GetFields;
                AgroController.Instance.MapOrderControl.SetLockButtons -= LockMenuBar;
                pnMap.Controls.Remove(AgroController.Instance.MapOrderControl);
            }
        }

        private void Order_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                Close();
            }
            if (e.KeyCode == Keys.Enter && e.Control)
            {
                e.Handled = true;
                SaveChanges(false);
            }
        }

        private void xtbCont_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtbCont.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (gcContentGrp.DataSource == null) LoadContentGrouped();  
                        break;
                    }
                case 1:
                    {
                        if (gcContent.DataSource == null) LoadSubForm();
                        break;
                    }

                case 2:
                    {
                        if ((_gpaphFuel == null) && (txId.Text != "0"))
                        {
                            _gpaphFuel = new AgroGraphFuel(_oi, _readOnly) {Dock = DockStyle.Fill};
                            xtraTabPage2.Controls.Add(_gpaphFuel);
                            _gpaphFuel.LoadSubFormFuel_DUT();
                        }
                        break;
                    }
                case 3:
                    {
                        if (gcFuelDRT.DataSource == null) LoadSubFormFuel_DRT();
                        break;
                    }
                case 4:
                    {
                        SetLinkToMapOrderControl();
                        break;
                    }
                case 5:
                    {
                        if (gcControlDrv.DataSource == null) LoadSubFormControlDriver();
                        if (gcControlAgr.DataSource == null) LoadSubFormControlAgregat();
                        if (gcControlField.DataSource == null) LoadSubFormControlField(); ;
                        break;
                    }
            }
            SetControls(); 
        }

        private void SetLinkToMapOrderControl()
        {
            if (pnMap.Controls.Count == 0 & txId.Text != "0")
            {
                AgroController.Instance.MapOrderControl.ReadOnly = _readOnly;
                pnMap.Controls.Add(AgroController.Instance.MapOrderControl);
                AgroController.Instance.MapOrderControl.Order = _oi;
                AgroController.Instance.MapOrderControl.RefreshFromControl += GetFields;
                AgroController.Instance.MapOrderControl.SetLockButtons += LockMenuBar;
                _listActiveZones = AgroController.Instance.MapOrderControl.GetActiveZones;
            }
        }

        private void SetLeMobitel()
        {
            leMobitel.EditValue = null;
            if (DicUtilites.ValidateLookUp(leGroupe, dxErrP))
            {
                DicUtilites.LookUpLoad(leMobitel,
                    string.Format(AgroQuery.Order.SelectVehicleMobitel, leGroupe.EditValue.ToString()));
            }
        }
        private void deDateInit_EditValueChanged(object sender, EventArgs e)
        {
            if (!bLoad)  ValidateFieldsAction();
        }
        private void leMobitel_EditValueChanged(object sender, EventArgs e)
        {
            if (!bLoad) ValidateFieldsAction();
        }

        public void LockMenuBar(bool bLock)
        {
            btMapView.Enabled = !bLock;
            btDelete.Enabled = !bLock;
            bsiExcel.Enabled = !bLock;
            bsiFact.Enabled = !bLock;
            btRefresh.Enabled = !bLock;
            bsiReports.Enabled = !bLock;
            btLog.Enabled = !bLock;
            btDelete.Enabled = !bLock;
            if (bLock)
            {
                //_bDeleteEnable = btDelete.Enabled;
                _bSaveEnable = btSave.Enabled;
            }
            else
            {
                //btDelete.Enabled = _bDeleteEnable;
                btSave.Enabled = _bSaveEnable;
            }
        }
        #endregion

        #region ����� �� ����

        private void LoadContent()
        {
            LoadContentGrouped();
            LoadSubForm();
            LoadSubFormFuel_DUT();
            LoadSubFormFuel_DRT();
            LoadSubFormControlDriver();
            LoadSubFormControlAgregat();
            LoadSubFormControlField();
            if (pnMap.Controls.Count > 0) 
                AgroController.Instance.MapOrderControl.DrawOrder();
        }

        /// <summary>
        /// ����������� ���� �� ������
        /// </summary>
        private double GetPrice(int iWorkType)
        {
            double dbPrice = 0;
            if (DicUtilites.ValidateLookUp(leMobitel, dxErrP))
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string sql = "";

                    sql = string.Format(AgroQuery.Order.SelectAgroPriceT, iWorkType, leMobitel.EditValue);
                    
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateComand", deDateInit.DateTime);
                    driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);

                    if (driverDb.Read())
                    {
                        double dbFactor = driverDb.GetDouble(driverDb.GetOrdinal("Factor"));
                        dbPrice = Math.Round(driverDb.GetDouble(driverDb.GetOrdinal("Price")) * dbFactor / 100, 2);
                        //���������� � �������
                        //txPrice.Text = Convert.ToString(dbPrice);
                    }
                    driverDb.CloseDataReader(); 
                };

            }
            //txSum.Text = Convert.ToString(Math.Round(dbPrice * Convert.ToDouble(txSquare.Text),2)); 
            return dbPrice;
        }
        /// <summary>
        /// �������������� ���� ����� � ����������
        /// </summary>
        private void gvContentPrice_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int iID = 0;
            //Console.WriteLine(e.Value.ToString()); 
            var gv = (GridView)gcContent.FocusedView;
            //Console.WriteLine(gcContent.FocusedView.Name);
            if (Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["IdP"]).ToString(), out iID))
            {
                //e.RowHandle
                if (iID == 0) return;
                if (e.Column.FieldName.ToString() == "Work")
                {
                    int iWork = 0;
                    double dbPrice = 0;
                    double dbSum = 0;
                    double dbSquare = 0;
                    Int32.TryParse(e.Value.ToString(), out iWork);
                    Double.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("FSP")).ToString(), out dbSquare);
                    if (iWork > 0) dbPrice = GetPrice((int)e.Value);
                    dbSum = Math.Round(dbPrice * dbSquare, 2);
                    gv.SetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("Price"), dbPrice);
                    gv.SetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("Sum"), dbSum);

                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();

                        string sql = string.Format(AgroQuery.Order.UpdateAgroOrdert, dbPrice.ToString().Replace(",", "."),
                            iWork, dbSum.ToString().Replace(",", "."), iID);

                        driverDb.ExecuteNonQueryCommand(sql);
                    }
                    //
                }
                else
                    if (e.Column.FieldName.ToString() == "Comment")
                    {
                        using (var driverDb = new DriverDb())
                        {
                            driverDb.ConnectDb();

                            string sql = string.Format(AgroQuery.Order.UpdateAgroOrdertSet, e.Value, iID);

                            driverDb.ExecuteNonQueryCommand(sql);
                        }
                    }
            }
        }

        /// <summary>
        /// �������� ���������� ������
        /// </summary>
        private void LoadSubForm()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                DataTable dt = _oi.GetContent().Copy() ;
                dt.TableName = "agro_ordert";
                var dsOrder = new DataSet() ;
                dsOrder.Tables.Add(dt);

                string sSqLselect = string.Format(AgroQuery.Order.SelectAgroOrdertId, txId.Text);

                driverDb.FillDataSet(sSqLselect, dsOrder, "agro_ordert_Price");
                DataColumn keyColumn = dsOrder.Tables["agro_ordert"].Columns["Id"];
                DataColumn foreignKeyColumn = dsOrder.Tables["agro_ordert_Price"].Columns["IdP"];
                dsOrder.Relations.Add(Resources.WorksExecuted, keyColumn, foreignKeyColumn);
                gcContent.DataSource = dsOrder.Tables["agro_ordert"];
                gcContent.ForceInitialize();
                gcContent.LevelTree.Nodes.Add(Resources.WorksExecuted, gvContentPrice);
            }
        }

        private void LoadContentGrouped()
        {
                gcContentGrp.DataSource = _oi.GetContentGrouped();
        }
        /// <summary>
        /// �������� ������� ������� �� ������� ������
        /// </summary>
        private void LoadSubFormFuel_DUT()
        {
            if (_gpaphFuel != null) _gpaphFuel.LoadSubFormFuel_DUT(); 
        }
        /// <summary>
        /// �������� ������� ������� �� ������� �������
        /// </summary>
        private void LoadSubFormFuel_DRT()
        {
                gcFuelDRT.DataSource = _oi.GetFuelDRT();
        }
        /// <summary>
        /// �������� �������
        /// </summary>
        private bool DeleteRecordContent(int iID)
        {
            try
            {
                if (TestLinks())
                {
                    using (var driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();

                        string sSQLdel = AgroQuery.Order.DeleteFromAgroOrderT + iID.ToString();

                        driverDb.ExecuteNonQueryCommand(sSQLdel);
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ApplicationName);
                return false;
            }
        }

        private void DeleteItem(object sender, System.EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                if (DeleteRecordContent((int)gbvContent.GetRowCellValue(gbvContent.FocusedRowHandle, "Id")))
                {
                    gbvContent.DeleteRow(gbvContent.FocusedRowHandle);
                    LoadSubFormFuel_DRT();
                    LoadSubFormFuel_DUT();
                }
            }
        }
        /// <summary>
        /// ���� �� ������ ������
        /// </summary>
        private void gbvContent_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu)e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem("�������", new EventHandler(DeleteItem));
            menuItem.Tag = e.Menu;
            gvMenu.Items.Add(menuItem);
            gvMenu.Show(e.Point);
        }

        private void gbvContent_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                GridView view = sender as GridView;
                if (e.Column.FieldName == "AName")
                {
                    int AgrInputSource = Convert.ToInt16(view.GetRowCellValue(e.RowHandle, view.Columns["AgrInputSource"]));
                    if (AgrInputSource > 0) e.Appearance.BackColor = _oi.GetInputSourseColor((AgrDrvInputSources)AgrInputSource);
                }
                if (e.Column.FieldName == "Family")
                {
                    int DrvInputSource = Convert.ToInt16(view.GetRowCellValue(e.RowHandle, view.Columns["DrvInputSource"]));
                    if (DrvInputSource > 0) e.Appearance.BackColor = _oi.GetInputSourseColor((AgrDrvInputSources)DrvInputSource);
                }
            }
        }

        private void gvContentGrp_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                GridView view = sender as GridView;
                if (e.Column.FieldName == "Id_agregat")
                {
                    int AgrInputSource = Convert.ToInt16(view.GetRowCellValue(e.RowHandle, view.Columns["AgrInputSource"]));
                    if (AgrInputSource > 0) e.Appearance.BackColor = _oi.GetInputSourseColor((AgrDrvInputSources)AgrInputSource);
                }
                if (e.Column.FieldName == "Id_driver")
                {
                    int DrvInputSource = Convert.ToInt16(view.GetRowCellValue(e.RowHandle, view.Columns["DrvInputSource"]));
                    if (DrvInputSource > 0) e.Appearance.BackColor = _oi.GetInputSourseColor((AgrDrvInputSources)DrvInputSource);
                }
            }
        }

        private void ttcGridView_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {

            GridView view = null;
            if (e.SelectedControl == gcContentGrp)
            {
                view = gcContentGrp.GetViewAt(e.ControlMousePosition) as GridView;
            }
            else if (e.SelectedControl == gcContent)
            {
                view = gcContent.GetViewAt(e.ControlMousePosition) as GridView;
            }
            else
                return;

            ToolTipControlInfo info = null;
            //Get the view at the current mouse position

            if (view == null) return;
            //Get the view's element information that resides at the current position
            GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
            //Display a hint for row indicator cells
            if (hi.HitTest == GridHitTest.RowCell)
            {
                string toolTip = "";
                if (hi.Column == gvContentGrp.Columns["Id_driver"] || hi.Column == gbvContent.Columns["Family"])
                {
                    int drvInputSource = Convert.ToInt16(view.GetRowCellValue(hi.RowHandle, view.Columns["DrvInputSource"]));
                    toolTip = _oi.GetToolTipTextForDriverInputSource((AgrDrvInputSources)drvInputSource);
                }
                else if (hi.Column == gvContentGrp.Columns["Id_agregat"] || hi.Column == gbvContent.Columns["AName"])
                {
                    int drvInputSource = Convert.ToInt16(view.GetRowCellValue(hi.RowHandle, view.Columns["AgrInputSource"]));
                    toolTip = _oi.GetToolTipTextForAgrInputSource((AgrDrvInputSources)drvInputSource);
                }
                //An object that uniquely identifies a row indicator cell
                if (toolTip.Length > 0)
                {
                    object o = hi.HitTest.ToString() + hi.RowHandle.ToString();
                    info = new ToolTipControlInfo(o, toolTip);
                }
            }
            //Supply tooltip information if applicable, otherwise preserve default tooltip (if any)
            if (info != null)
                e.Info = info;
        }

        private void gbvContent_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
        #endregion

        #region ���������� �������
        /// <summary>
        /// �������� ���������� �������
        /// </summary>
        private void LoadSubFormControlField()
        {
            gcControlField.DataSource = _oi.GetControlData((int)Consts.TypeControlObject.Field);
            FldLookUp.DataSource = DictionaryAgroField.GetList(); 
        }

        /// <summary>
        /// �������� ���������� ����������
        /// </summary>
        private void LoadSubFormControlDriver()
        {
            gcControlDrv.DataSource = _oi.GetControlData((int)Consts.TypeControlObject.Driver);
            DriverLookUp.DataSource = DriverVehicleProvider.GetList();
        }

        /// <summary>
        /// �������� ���������� �������� �������������
        /// </summary>
        private void LoadSubFormControlAgregat()
        {
            gcControlAgr.DataSource = _oi.GetControlData((int)Consts.TypeControlObject.Agregat);
            AgrLookUp.DataSource = DictionaryAgroAgregat.GetList(); 
        }

        //--------------------------------------------------
        private void ControlInitNewRow(GridView gv, InitNewRowEventArgs e)
        {
            DataRow row = gv.GetDataRow(e.RowHandle);

            if (gv.RowCount > 1)
            {
                row["TimeStart"] = gv.GetRowCellValue(gv.RowCount - 2, "TimeEnd");
            }
            else
                row["TimeStart"] = deDateInit.DateTime;
            row["TimeEnd"] = deDateInit.DateTime.AddDays(1).AddMinutes(-1)  ;
            row["Time"] = deDateInit.DateTime.AddDays(1).AddMinutes(-1).Subtract((DateTime) row["TimeStart"]);
            row["Id"] = 0;
        }

        private bool ControlDelete(int iIdRecord)
        {
            if (DialogResult.Yes == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();

                    string sSQLdelete = AgroQuery.Order.DeleteFromAgroOrdertControl + iIdRecord;

                    driverDb.ExecuteNonQueryCommand(sSQLdelete);
                }
                return true;
            }
            return false;
        }

        private void ControlEdit(GridView gv, Consts.TypeControlObject tco, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int iIdRecord = 0;
            int iObject = 0;
            DateTime dtStart;
            DateTime dtEnd;
            if (e.Column.FieldName== "Time") return;
            if (e.Column.FieldName == "Id") return;
            if (e.Column.FieldName == "Id_object")
            {
                if (e.Value == null) return;
                iObject = (int)e.Value;
            }
            else
                if (!Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["Id_object"]).ToString(), out iObject))
                    return;
            if (DateTime.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["TimeStart"]).ToString(), out dtStart)
                && DateTime.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["TimeEnd"]).ToString(), out dtEnd))
            {
                if (!TestTimeChain(gv, e.RowHandle)) return;
                if (Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["Id"]).ToString(), out iIdRecord) && iIdRecord>0)
                {
                    UpdateControlRow(gv, e.RowHandle, iIdRecord, iObject, dtStart, dtEnd);
                }
                else
                {
                    InsertControlRow(gv, tco, e, iObject, ref dtStart, ref dtEnd);
                }
            }
        }

        private void InsertControlRow(GridView gv, Consts.TypeControlObject tco, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e, int iObject, ref DateTime dtStart, ref DateTime dtEnd)
        {
            OrderItemControl oic = new OrderItemControl( _oi.ID);
            gv.SetRowCellValue(e.RowHandle, gv.Columns["Id"], oic.AddRecord(iObject, tco, dtStart, dtEnd));
        }

        private void UpdateControlRow(GridView gv, int RowHandle, int iIdRecord, int iObject,  DateTime dtStart, DateTime dtEnd)
        {
            OrderItemControl oic = new OrderItemControl(_oi.ID);
            if (oic.UpdateRecord( iObject,iIdRecord,dtStart, dtEnd))
            {
                gv.SetRowCellValue(RowHandle, gv.Columns["Time"], dtEnd.Subtract(dtStart).ToString().Substring(0, 5));
            }
        }
        /// <summary>
        /// ����� � ����������� ������ ������ ���� �� ������������ � �� �������� �� ������� ������
        /// </summary>
        /// <returns></returns>
        private bool TestTimeChain(GridView gv, int RowHandle)
        {
            DataRow row = gv.GetDataRow(RowHandle);
            if (row == null) return true;
            DateTime orderStart = (DateTime)deDateInit.EditValue;
            DateTime orderEnd = orderStart.AddDays(1).AddSeconds(-1);
            DateTime drowStart = (DateTime)row["TimeStart"];
            DateTime drowEnd = (DateTime)row["TimeEnd"];
            if ((drowEnd.Subtract(drowStart).TotalMinutes < 0))
            {
                gv.SetColumnError(gv.Columns["TimeEnd"], Resources.TimeMustRise);
                return false;
            }
            if (drowEnd.Subtract(orderStart).TotalMinutes < 0 || drowEnd.Subtract(orderEnd).TotalMinutes > 0)
            {
                gv.SetColumnError(gv.Columns["TimeEnd"], Resources.TimeOver);
                return false;
            }
            if (drowStart.Subtract(orderStart).TotalMinutes < 0 || drowStart.Subtract(orderEnd).TotalMinutes > 0)
            {
                gv.SetColumnError(gv.Columns["TimeStart"], Resources.TimeOver);
                return false;
            }
            gv.ClearColumnErrors(); 
            return true;
        }
        //--------------------------------------------------    
        private void gvControlDrv_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            ControlInitNewRow(gvControlDrv, e);
        }

        private void gvControlAgr_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            ControlInitNewRow(gvControlAgr, e);
        }

        private void gvControlField_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            ControlInitNewRow(gvControlField, e);
        }

        //-------------------------------------------------
        private void gvControlDrv_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            ControlEdit(gvControlDrv, Consts.TypeControlObject.Driver, e);
        }

        private void gvControlAgr_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            ControlEdit(gvControlAgr, Consts.TypeControlObject.Agregat, e);
        }

        private void gvControlField_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            ControlEdit(gvControlField, Consts.TypeControlObject.Field, e);
        }
        //---------------------------------------------
        private void gcControlDrv_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            GridView gv = (GridView)gcControlDrv.Views[0];
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                int iIdRecord;
                if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Id"]).ToString(), out iIdRecord))
                {
                    if (!ControlDelete(iIdRecord))
                        e.Handled = true;
                }
            }
        }

        private void gcControlAgr_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            GridView gv = (GridView)gcControlAgr.Views[0];
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                int idRecord;
                if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Id"]).ToString(), out idRecord) && idRecord>0)
                {
                    if (!ControlDelete(idRecord))
                        e.Handled = true;
                }
            }
        }

        private void gcControlField_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            GridView gv = (GridView)gcControlField.Views[0];
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                int iIdRecord;
                if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Id"]).ToString(), out iIdRecord))
                {
                    if (!ControlDelete(iIdRecord))
                        e.Handled = true;
                }
            }
        }
        //---------------------------------------------
        private void gvControlDrv_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!TestTimeChain(gvControlDrv, e.RowHandle))
            {
                e.Valid = false;
            }
        }

        private void gvControlAgr_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!TestTimeChain(gvControlAgr, e.RowHandle))
                e.Valid = false;
        }

        private void gvControlField_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!TestTimeChain(gvControlField, e.RowHandle))
                e.Valid = false;
        }

        private void gvControlDrv_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void gvControlAgr_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void gvControlField_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        public void SetStatus(string sMessage)
        {
            bsiStatus.Caption = sMessage;
        }

        private string GetStatus()
        {
            return (bsiStatus.Caption);
        }
        #endregion

        #region �����������
        private void Localization()
        {
            erNumber.Properties.Caption = Resources.Detaill;
            erDate.Properties.Caption = Resources.Date;
            labelControl2.Text = Resources.Date;
            btLog.Caption = Resources.LogEvents;
            Work.Caption = Resources.WorkType;
            leWorkType.Columns[0].Caption = Resources.WorkType;
            FSP.Caption = Resources.SquareRunGa;
            Price.Caption  = Resources.Price;
            Sum.Caption  = Resources.Sum;
            Comment.Caption  = Resources.Remark;
            gbvContent.GroupSummary[0].DisplayFormat = Resources.DisplayFormatRun;
            gbvContent.GroupSummary[1].DisplayFormat = Resources.DisplayFormatContur;
            ����.Caption  = Resources.Field;
            ����.Name  = Resources.Field;
            GFName.Caption  = Resources.FieldsGroup;
            FName.Caption  = Resources.Field;
            Square.Caption  = Resources.SquareGA ;
            gridBand2.Caption  = Resources.Time;
            TimeStart.Caption  = Resources.Entry ;
            TimeEnd.Caption  = Resources.Exit;
            FactTime.Caption  = Resources.TimeWorksH;
            TimeMove.Caption  = Resources.TimeMoveH;
            TimeStop.Caption  = Resources.TimeStopH;
            TimeRate.Caption  = Resources.MotoHourH;
            gridBand3.Caption  = Resources.Work;
            Driver.Caption  = Resources.Driver;
            AName.Caption  = Resources.Agregat;
            Width_c.Caption  = Resources.WidthM;
            DistanceT.Caption  = Resources.PathKm;
            FactSquare.Caption  = Resources.SquareRunGa;
            FactSquareCalc.Caption  = Resources.SquareContourGa;
            SpeedAvgField.Caption  = Resources.SpeedAvg;
            //FuelStartD.Caption  = Resources.FuelStartL;
            //gridColumn7.Caption  = Resources.FuelStartL;
            //gridColumn21.Caption  = Resources.FuelStartL;
            btSave.Caption  = Resources.Save;
            btRefresh.Caption  = Resources.Refresh;
            bsiFact.Caption  = Resources.Fact;
            btFactControl.Caption  = Resources.DataControlTaking;
            btFact.Caption  = Resources.DataControlNTaking;
            btMapView.Caption  = Resources.Map;
            bsiReports.Caption  = Resources.Reports;
            btReport.Caption  = Resources.Total;
            btReportContentGrouped.Caption  = Resources.Content;
            btReportContent.Caption  = Resources.Run;
            btReportFuelDUT.Caption  = Resources.FuelDUT;
            btReportFuelDRT.Caption  = Resources.FuelDRT;
            bsiExcel.Caption  = Resources.ExcelExport ;
            btSave.Caption = Resources.Save;
            btRefresh.Caption = Resources.Refresh;
            bsiFact.Caption = Resources.Fact;
            btFactControl.Caption = Resources.DataControlTaking;
            btFact.Caption = Resources.DataControlNTaking;
            btMapView.Caption = Resources.Map;
            bsiReports.Caption = Resources.Reports;
            btReport.Caption = Resources.Total;
            btReportContentGrouped.Caption = Resources.Content;
            btReportContent.Caption = Resources.Run;
            btReportFuelDUT.Caption = Resources.FuelDUT ;
            btReportFuelDRT.Caption = Resources.FuelDRT;
            bsiExcel.Caption = Resources.ExcelExport;
            btExcelTotal.Caption = Resources.InformationAssembled;
            btExcel.Caption = Resources.DataTab;
            btDelete.Caption = Resources.Delete;
            bsiStatus.Caption = Resources.Ready;
            labelControl4.Text = Resources.Car;
            labelControl3.Text = Resources.CarGroupe;
            labelControl2.Text = Resources.Date;
            labelControl1.Text = Resources.Number;
            labelControl9.Text = Resources.Remark;
            xtraTabPage6.Text = Resources.Content;
            FNameGr.Caption = Resources.Field;
            SquareGr.Caption = Resources.SquareGA;
            Id_driverGr.Caption = Resources.Driver;
            leDriver.Columns[0].Caption = Resources.NameRes;
            Id_agregatGr.Caption = Resources.Agregat;
            leAgregat.Columns[0].Caption = Resources.NameRes;
            DistanceGr.Caption = Resources.PathKm;
            FactSquareGr.Caption = Resources.SquareRunGa;
            FactSquareCalcGr.Caption = Resources.SquareContourGa;
            FactSquareCalcGrOverlap.Caption = Resources.SquareContourGaOverlap;
            FactSquareJointProcessing.Caption = Resources.FieldsJointProcessing;
            xtraTabPage1.Text = Resources.Run;
            xtraTabPage2.Text = Resources.FuelDUT;

            xtraTabPage3.Text  = Resources.FuelDRT;
            FieldDRT.Caption  = Resources.Field;
            FamilyDRT.Caption  = Resources.Driver;
            TimeStartDRT.Caption  = Resources.Entry;
            TimeEndDRT.Caption  = Resources.Exit;
            DistanceDRT.Caption  = Resources.PathKm;
            Fuel_ExpensMoveDRT.Caption  = Resources.FuelExpenseMovingL;
            Fuel_ExpensStopDRT.Caption  = Resources.FuelExpenseStopL;
            Fuel_ExpensTotalDRT.Caption  = Resources.FuelExpenseTotalL;
            Fuel_ExpensAvgDRT.Caption  = Resources.FuelExpenseAvgLGa;
            Fuel_ExpensAvgRateDRT.Caption  = Resources.FuelExpenseAvgLMHour;
            gridColumn17.Caption  = Resources.Entry;
            gridColumn18.Caption  = Resources.Exit;
            gridColumn19.Caption  = Resources.MotoHour;
            gridColumn20.Caption  = Resources.ExecuteGa;
            gridColumn22.Caption  = Resources.FuelAddL;
            gridColumn23.Caption  = Resources.FuelSubL;
            gridColumn24.Caption  = Resources.FuelEndL;
            gridColumn25.Caption  = Resources.FuelExpenseTotalL;
            gridColumn26.Caption  = Resources.FuelExpenseMovingL;
            gridColumn27.Caption  = Resources.FuelExpenseStopL;
            gridColumn28.Caption  = Resources.FuelExpenseTotalL;
            xtraTabMap.Text  = Resources.OrderMap;
            xtraTabPage5.Text  = Resources.Control;
            Id_object1.Caption  = Resources.Driver;
            DriverLookUp.NullText  = Resources.DriverShow;
            colEnterDrv.Caption  = Resources.Entry;
            colExitDrv.Caption  = Resources.Exit;
            colTimeDrv.Caption  = Resources.Time;
            colAgregat.Caption  = Resources.Agregat;
            AgrLookUp.Columns[0].Caption  = Resources.Agregat;
            AgrLookUp.NullText  = Resources.AgregatShow;
            colEnterAgr.Caption  = Resources.Entry;
            colExitAgr.Caption  = Resources.Exit;
            colTimeAgr.Caption  = Resources.Time;
            colField.Caption  = Resources.Field;
            FldLookUp.Columns[0].Caption  = Resources.Field;
            FldLookUp.NullText  = Resources.FieldShow ;
            colEnter.Caption  = Resources.Entry;
            colExit.Caption  = Resources.Exit;
            colTime.Caption  = Resources.Time;
            gridColumn3.Caption  = Resources.Entry;
            gridColumn4.Caption  = Resources.Exit;
            gridColumn5.Caption  = Resources.MotoHour;
            gridColumn6.Caption  = Resources.ExecuteGa;
            gridColumn8.Caption  = Resources.FuelAddL;
            gridColumn9.Caption  = Resources.FuelSubL;
            gridColumn10.Caption  = Resources.FuelEndL;
            gridColumn11.Caption  = Resources.FuelExpenseTotalL;
            gridColumn12.Caption  = Resources.FuelExpenseMovingL;
            gridColumn13.Caption  = Resources.FuelExpenseStopL;
            gridColumn14.Caption  = Resources.FuelExpenseTotalL;
            gridColumn42.Caption  = Resources.Entry;
            gridColumn43.Caption  = Resources.Exit;
            gridColumn44.Caption  = Resources.Time;
            gridColumn46.Caption  = Resources.Entry;
            gridColumn47.Caption  = Resources.Exit;
            gridColumn48.Caption  = Resources.Time;
            gridColumn49.Caption  = Resources.Entry;
            gridColumn50.Caption  = Resources.Exit;
            gridColumn51.Caption  = Resources.Time;
            crTotalc.Properties.Caption  = Resources.InformationTotal;
            erMobitel.Properties.Caption  = Resources.Car;
            erSquare.Properties.Caption = Resources.SquareContourGa;
            erSquareOverlap.Properties.Caption = Resources.SquareContourGaOverlap;
            erParth.Properties.Caption  = Resources.MoveKm;
            errRemark.Properties.Caption  = Resources.Remark;
            crTotalPath.Properties.Caption  = Resources.MovingParameters;
            erPlaceStart.Properties.Caption  = Resources.PlaceStart;
            erPlaceEnd.Properties.Caption  = Resources.PlaceEnd;
            erSpeed.Properties.Caption  = Resources.SpeedAvgKmH;
            erPath.Properties.Caption  = Resources.PathKm;
            crTotalTime.Properties.Caption  = Resources.TimeParameters;
            erDateStart.Properties.Caption  = Resources.TimeStart;
            erDateEnd.Properties.Caption  = Resources.TimeEnd;
            erTimeWork.Properties.Caption  = Resources.DurationRotation;
            erTimeMove.Properties.Caption  = Resources.TimeMoveTotalH ;
            erTimeStop.Properties.Caption  = Resources.TimeStopTotalH;
            crDUT.Properties.Caption  = Resources.FuelDUT ;
            erExpensSquareDUT.Properties.Caption  = Resources.FuelExpenseFieldTotal;
            erExpensSquareAvgDUT.Properties.Caption  = Resources.FuelExpenseFieldLGa;
            erExpensWithoutWorkDUT.Properties.Caption  = Resources.FuelExpenseRunTotal;
            erExpensAvgWithoutWorkDUT.Properties.Caption  = Resources.FuelExpenseRunL100Km;
            crDRT.Properties.Caption = Resources.FuelDRT;
            erExpensSquareDRT.Properties.Caption  = Resources.FuelExpenseFieldTotal;
            erExpensSquareAvgDRT.Properties.Caption  = Resources.FuelExpenseFieldLGa;
            erExpensWithoutWorkDRT.Properties.Caption  = Resources.FuelExpenseRunTotal;
            erExpensAvgWithoutWorkDRT.Properties.Caption  = Resources.FuelExpenseRunL100Km;
            erDateLastRecalc.Properties.Caption = Resources.DateLastOrderRecalc;
            Text = Resources.OrderFullName;
            ValidityData.Properties.Caption = Resources.DataQuality;
            PointsValidity.Properties.Caption = Resources.DataValidity;
            PointsCalc.Properties.Caption = Resources.PointsCalc;
            PointsFact.Properties.Caption = Resources.PointsFact;
            PointsIntervalMax.Properties.Caption = Resources.Max_Interval;
            gcQuality.Text = Resources.DataValidity;
        }
        #endregion



    }
}