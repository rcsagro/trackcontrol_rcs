﻿using System;
using System.Data;
//using System.Data.DataSetExtensions;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using System.Linq;

namespace Agro
{
    public static class OrderLists
    {
        public static DataTable GetListMain(int idOrder, DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable dtOrder;

                string sSql = AgroQuery.OrderList.SelectAgroOrder;

                if (idOrder > 0)
                {
                    sSql += AgroQuery.OrderList.WhereAgroOrderId + idOrder;
                    dtOrder = driverDb.GetDataTable(sSql);
                }
                else
                {
                    sSql += AgroQuery.OrderList.WhereAgroOrder;
                   
                    if (idTeam > 0) 
                        sSql += AgroQuery.OrderList.EndVehicleTeamId + idTeam;

                    if (idMobitel > 0) 
                        sSql += AgroQuery.OrderList.AndMobitelsId + idMobitel;


                    sSql = sSql + AgroQuery.OrderList.OrderByAgroOrder;

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                    dtOrder = driverDb.GetDataTable(sSql, driverDb.GetSqlParameterArray);
                    if (dtOrder.Rows.Count != 0)
                    {
                        //var temp = dtOrder.Rows[10]["DateLastRecalc"];
                    }

                    OrderFilterVehicles.SetFilter(ref dtOrder); 
                }
                return dtOrder;
            }
        }

        public static DataTable GetListContent(int idOrder, DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                DataTable dtContent;
                string sSQL = AgroQuery.OrderList.SelectAgroOrderT;

                if (idOrder > 0)
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrderId + idOrder;
                    dtContent = driverDb.GetDataTable(sSQL);
                }
                else
                {
                    sSQL += AgroQuery.OrderList.WhereAgroOrder;
                    
                    if (idTeam > 0) 
                        sSQL += AgroQuery.OrderList.EndVehicleTeamId + idTeam;

                    if (idMobitel > 0) 
                        sSQL += AgroQuery.OrderList.AndAgroOrderIdMobitel + idMobitel;

                    sSQL = sSQL + AgroQuery.OrderList.OrderByAgroOrder;

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                    dtContent = driverDb.GetDataTable(sSQL, driverDb.GetSqlParameterArray);
                }
                return dtContent;
            }
        }

        public static DataTable GetListFields(int idOrder,DateTime start, DateTime end, int idTeam, int idMobitel)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string subString = "";
                string sSqLselect = "";

                if (idOrder > 0)
                {
                    subString = string.Format(AgroQuery.OrderList.SelectCountCntOrderId,idOrder);
                    subString += AgroQuery.OrderList.WhereIdField;
                    sSqLselect = string.Format(AgroQuery.OrderList.SelectZones, subString);
                    return driverDb.GetDataTable(sSqLselect);
                }
                else
                {
                    subString = AgroQuery.OrderList.SelectCountCnt;

                    if (idTeam > 0)
                        subString = string.Format(AgroQuery.OrderList.AndVehicleTeam, subString, idTeam);

                    if (idMobitel > 0)
                        subString = string.Format(AgroQuery.OrderList.AndAgroOrder, subString, idMobitel);

                    subString += AgroQuery.OrderList.WhereIdField;

                    sSqLselect = string.Format(AgroQuery.OrderList.SelectZones, subString);

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", (DateTime)start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", (DateTime)end);
                    return driverDb.GetDataTable(sSqLselect, driverDb.GetSqlParameterArray);
                }

            }
        }
    }
}
