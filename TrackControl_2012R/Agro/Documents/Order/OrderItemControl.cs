﻿using System;
using System.Collections.Generic;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    /// <summary>
    /// управляющие данные наряда
    /// </summary>
    public class OrderItemControl
    {
        int _id_main;

        public OrderItemControl(int id_main)
        {
            _id_main = id_main;
        }

        public int AddRecord(int id_object, Consts.TypeControlObject tco, DateTime start, DateTime end)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format(AgroQuery.OrderItemControl.InsertIntoAgroOrdertControl, _id_main, id_object, (int)tco, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                return driverDb.ExecuteReturnLastInsert(sql, driverDb.GetSqlParameterArray, "agro_ordert_control");
            }
        }

        public bool UpdateRecord(int id_object, int id_record, DateTime start, DateTime end)
        {
            try
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string sql = string.Format(AgroQuery.OrderItemControl.UpdateAgroOrdertControl, id_object, id_record, driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                    driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<OrderItemControlRecord> GetListTypeControlBetweenDates(Consts.TypeControlObject tco, DateTime start, DateTime end)
        {
            List<OrderItemControlRecord> oicrs = new List<OrderItemControlRecord>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                string sql = string.Format(AgroQuery.OrderItemControl.SelectAgroOrdertControl, _id_main, (int)tco, driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                while (driverDb.Read())
                {
                    OrderItemControlRecord oicr = new OrderItemControlRecord();
                    oicr.Id_record = driverDb.GetInt32("Id");
                    oicr.Id_object = driverDb.GetInt32("Id_object");
                    oicr.TypeObject = (Consts.TypeControlObject)driverDb.GetInt16("Type_object");
                    oicr.TimeStart = driverDb.GetDateTime("TimeStart");
                    oicr.TimeEnd = driverDb.GetDateTime("TimeEnd");
                    oicrs.Add(oicr); 
                }
                driverDb.CloseDataReader(); 
            }
            return oicrs;
        }
    }
}
