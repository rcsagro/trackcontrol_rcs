using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace Agro
{
    public abstract class DocItem: IDocument, IDisposable 
    {
        private const int BlockHouers = 8;
        /// <summary>
        /// atlantaDataSet - ��� ������������� � ������������ ����������
        /// </summary>
        protected atlantaDataSet _dsAtlanta ;
        protected GpsData[] GpsDatasDocItem;
        protected DocItem()
        {

        }
        /// <summary>
        /// ������� ������ ���������
        /// </summary>
        protected bool IsDocNew = false;

        public static IZonesManager ZonesModel;
        public static VehiclesModel VehiclesModel;
        //public static IZonesManager ZonesModel; VehiclesModel

        /// <summary>
        /// ����������� � ���� ������
        /// </summary>
        protected string Sql = "";

        /// <summary>
        /// ��������� ����� �����
        /// </summary>
        protected LON_LAT_MIN_MAX MinMax;

        /// <summary>
        /// ������������, ���������� � ����������
        /// </summary>
        protected int _blockUser;

        public int BlockUser
        {
            get { return _blockUser; }
            set { _blockUser = value; }
        }

        protected DateTime? _blockStartDate;

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink { get; set; }


        #region Points
        /// <summary>
        /// ���������� ������
        /// </summary>
        protected int _PointsValidity;
        public int PointsValidity
        {
            get { return _PointsValidity; }
            set { _PointsValidity = value; }
        }
        public int PointsValidityGauge
        {
            get
            {
                return GetPointsValidityGauge(_PointsValidity); 
            }
        }
        public static int GetPointsValidityGauge(int inputValue)
        {
                if (inputValue >= 90)
                    return (int)ValidityColors.Green;
                else
                {
                    if ((inputValue < 90) && (inputValue >= 70))
                        return (int)ValidityColors.Yellow;
                    else return (int)ValidityColors.Red;
                }
        }
        /// <summary>
        /// ���������� ����� (�� Log_ID) ���������
        /// </summary>
        protected int _PointsCalc;
        public int PointsCalc
        {
            get { return _PointsCalc; }
            set { _PointsCalc = value; }
        }
        /// <summary>
        /// ���������� ����� (�� Log_ID) �����������
        /// </summary>
        protected int _PointsFact;
        public int PointsFact
        {
            get { return _PointsFact; }
            set { _PointsFact = value; }
        }
        /// <summary>
        /// PointsIntervalMax
        /// </summary>
        protected string _PointsIntervalMax = "";
        public string PointsIntervalMax
        {
            get { return _PointsIntervalMax; }
            set { _PointsIntervalMax = value; }
        }
        #endregion


        /// <summary>
        /// ����������� min max ��� ��������� �� ��� ������� ���������
        /// </summary>
        protected void SetMinMaxGPS(int Mobitel_Id)
        {
            bool First = true;
            foreach (GpsData gpsData in GpsDatasDocItem)
            {
                First = SetMinMaxGpsOneRecord(First, gpsData);
            }
        }

        private bool SetMinMaxGpsOneRecord(bool First, GpsData gpsData)
        {
            if (First)
            {
                MinMax.LAT_max = gpsData.LatLng.Lat;
                MinMax.LAT_min = MinMax.LAT_max;

                MinMax.LON_max =gpsData.LatLng.Lng;
                MinMax.LON_min = MinMax.LON_max;

                MinMax.LON_first = gpsData.LatLng.Lng;
                MinMax.LAT_first = gpsData.LatLng.Lat;
                First = false;
            }
            else
            {
                if (MinMax.LAT_max < gpsData.LatLng.Lat) MinMax.LAT_max = gpsData.LatLng.Lat;
                if (MinMax.LAT_min > gpsData.LatLng.Lat) MinMax.LAT_min = gpsData.LatLng.Lat;
                if (MinMax.LON_max < gpsData.LatLng.Lng) MinMax.LON_max = gpsData.LatLng.Lng;
                if (MinMax.LON_min > gpsData.LatLng.Lng) MinMax.LON_min = gpsData.LatLng.Lng;
            }
            return First;
        }

        protected void SetMinMaxGPS(GpsData[] gpsDatas)
        {
            bool First = true;
            for (int i = 0; i < gpsDatas.Length ; i++)
            {
                First = SetMinMaxGpsOneRecord(First, gpsDatas[i]);
            }

        }

        protected bool AngleZoneInTrackArea(PointLatLng point)
        {
            if (point.Lat >= MinMax.LAT_min && point.Lat <= MinMax.LAT_max && point.Lng >= MinMax.LON_min && point.Lng <= MinMax.LON_max )
                return true;
            else
                return false;
        }

        protected bool AngleTrackInZoneArea(IZone zone,PointLatLng point)
        {
            PointLatLng topLeft = new PointLatLng(zone.Bounds.Lat, zone.Bounds.Lng);
            PointLatLng topRigth = new PointLatLng(zone.Bounds.Lat, zone.Bounds.Lng + zone.Bounds.WidthLng);
            PointLatLng bottomLeft = new PointLatLng(zone.Bounds.Lat + zone.Bounds.HeightLat, zone.Bounds.Lng);
            if (point.Lat >= topLeft.Lat && point.Lat <= bottomLeft.Lat && point.Lng >= topLeft.Lng && point.Lng <= topRigth.Lng )
                return true;
            else
                return false;
        }

        #region IDisposable Members
        public virtual void Dispose()
        {
            if (_dsAtlanta != null) _dsAtlanta.Clear();
           // if (GpsDatasDocItem != null && GpsDatasDocItem.Length > 0) Array.Clear(GpsDatasDocItem, 0, GpsDatasDocItem.Length); 
        }
        #endregion

        #region IDocument Members
        #region ����
        /// <summary>
        /// ������������� ���������
        /// </summary>
        protected int _ID = 0;
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
        /// <summary>
        /// ���� ���������
        /// </summary>
        protected DateTime _Date;
        public virtual  DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        /// <summary>
        /// ����������
        /// </summary>
        protected string _Remark;
        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }
        #endregion
        #region ������
        public virtual int AddDoc()
        {
            return 0;
        }

        public virtual bool UpdateDoc()
        {
            return true;
        }

        public virtual bool DeleteDoc(bool bQuestionNeed)
        {
            return true;
        }

        public virtual bool DeleteDocContent()
        {
            return true;
        }

        public virtual bool DeleteDocTest()
        {
            return true;
        }
        
        #endregion
        public virtual bool TestDemo()
        {
            return true;
        }
        public virtual DataTable GetContent()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /// <summary>
        /// �������� ���������� ������� �� ������� �����������
        /// </summary>
        public virtual bool DeleteSelectedFromGrid(GridView gv)
        {
            if (gv == null || gv.SelectedRowsCount == 0) return false;
            if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
            DataRow[] rows = new DataRow[gv.SelectedRowsCount];
            for (int i = 0; i < gv.SelectedRowsCount; i++)
                rows[i] = gv.GetDataRow(gv.GetSelectedRows()[i]);
            foreach (DataRow row in rows)
            {
                _ID = (int)row["Id"];
                if (DeleteDoc(false)) row.Delete();
            }
            return true;
        }

        public virtual void UpdateDocTotals()
        {
            throw new NotImplementedException();
        }

        public virtual bool GetDocById(int id)
        {
            return false;
        }
        #endregion

        #region ������������ ��������� ��� �������������������� ������
        public  bool IsDocBlocked
        {
            get
            {
                if (_blockUser == 0 || _blockUser == UserBaseCurrent.Instance.Id)
                    return false;
                else
                    // ��������� ����������, ���� ������ BLOCK_HOUERS � ������� ����������
                    if (DateTime.Now.AddHours(-BlockHouers).Subtract(_blockStartDate ?? DateTime.MinValue).TotalMinutes > 0)
                    {
                        UpdateBlockState(0, null); 
                        return false;
                    }
                    else
                    {
                        string userName;
                        if (_blockUser == ConstsGen.USER_ADMIN_CODE)
                            userName = ConstsGen.USER_ADMIN_NAME;
                        else
                        {
                        UserBase ub = UserBaseProvider.GetUserBase(_blockUser);
                        userName = ub.Name;
                        }
                        XtraMessageBox.Show(string.Format("{0} {1}", Resources.WorkWithDocument, userName), string.Format("{0} # {1} : {2}", Resources.Order, _ID, _blockStartDate.ToString()));  
                        return true;
                    }
            }
        }

        public virtual void UpdateBlockState(int id_user, DateTime ?blockDate)
        {
        }
        #endregion

        protected void SetPointsValidity(GpsData[] gpsDatas)
        {
            _PointsValidity = 0;
            if (gpsDatas.Length > 0)
            {
                int iLogID_F = gpsDatas[0].LogId;
                int iLogID_L = gpsDatas[gpsDatas.Length - 1].LogId;
                _PointsCalc = (iLogID_L - iLogID_F) +1;
                _PointsFact = gpsDatas.Length;
                if (_PointsCalc > 0)
                {
                    double dbCalc = (double)_PointsFact / _PointsCalc;
                    _PointsValidity = (int)(Math.Round(dbCalc * 100, 0));
                }
            }
            else
            {
                _PointsValidity = 0;
                _PointsCalc = 0;
                _PointsFact = 0;
            }
        }

        protected void SetPointsIntervalMax(GpsData [] GpsDatas)
        {
            TimeSpan tsMax = TimeSpan.Zero;
            for (int i = 1; i < GpsDatas.Length; i++)
            {
                TimeSpan tsWork = GpsDatas[i].Time.Subtract(GpsDatas[i - 1].Time);
                if (tsMax.TotalSeconds < tsWork.TotalSeconds)
                    tsMax = tsWork;
            }
            _PointsIntervalMax = tsMax.ToString();
        }

        public atlantaDataSet GetDataset()
        {
            return _dsAtlanta; 
        }
    }
}
