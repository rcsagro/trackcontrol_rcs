namespace Agro
{
    partial class MainAgro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAgro));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode12 = new DevExpress.XtraGrid.GridLevelNode();
            this.gvGFields = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDict = new DevExpress.XtraGrid.GridControl();
            this.gvFields = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FGROUPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leFieldGrope = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.FZONE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leZone = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Name_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Square = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsOutOfDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvCalt = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IconC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.peIcon = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Name_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.gvAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupeAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leGroupeAgr = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Name_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Width_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Identifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Def = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_work_agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leWork = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gvOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_o = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateInit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateComand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_o = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvPrice = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateComand_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.WName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leMobitel = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.AName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leUnit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Comment_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvUnit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameShort_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Factor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvGAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_gagr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_gagr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Remark_gagr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Work_gagr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvGWorks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_gw = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_gw = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DepthControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_gw = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvDtReasons = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_dtr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_dtr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_dtr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvSeasons = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colsId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsDateStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsDateEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsCulture = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleCulture = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colsSquarePlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvWorks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedBottom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedTop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ieCulture = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gvbFields = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Id1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Field = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Family = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TimeStart1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeEnd1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeMove1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DistanceM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquareCalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpeedAvgField = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gcOrder = new DevExpress.XtraGrid.GridControl();
            this.bgvOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colState = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.icbStatuses = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.DateLastRecalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DateGrn = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Mobitel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.leMobitelOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bcolCategory = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SquareWorkDescript = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FactSquareCol = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactSquareCalc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactSquareCalcOverlap = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PathWithoutWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CommentOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LocationStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.LocationEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Distance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpeedAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TimeWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelDUTExpensSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDUTExpensAvgSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDUTExpensWithoutWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDUTExpensAvgWithoutWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelDRTExpensSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensAvgSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensWithoutWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensAvgWithoutWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gbQuontity = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolValidity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.pbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.bcolPointsIntervalMax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gvbFuel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FamilyF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.IdF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelAdd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelSub = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpens = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Fuel_ExpensMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensAvgRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.sbtAdd = new DevExpress.XtraBars.BarButtonItem();
            this.sbtDelete = new DevExpress.XtraBars.BarButtonItem();
            this.sbtEdit = new DevExpress.XtraBars.BarButtonItem();
            this.sbtRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFact = new DevExpress.XtraBars.BarSubItem();
            this.btFactControl = new DevExpress.XtraBars.BarButtonItem();
            this.btFact = new DevExpress.XtraBars.BarButtonItem();
            this.sbtExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btExcelImport = new DevExpress.XtraBars.BarButtonItem();
            this.btXmlExport = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSettings = new DevExpress.XtraBars.BarSubItem();
            this.btColumnsSet = new DevExpress.XtraBars.BarButtonItem();
            this.btColumnsSave = new DevExpress.XtraBars.BarButtonItem();
            this.btColumnRestore = new DevExpress.XtraBars.BarButtonItem();
            this.btSettings = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.pbStatus = new DevExpress.XtraBars.BarEditItem();
            this.rpbStatus = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.lbStatus = new DevExpress.XtraBars.BarStaticItem();
            this.btnStopExecutingData = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btStart = new DevExpress.XtraBars.BarButtonItem();
            this.bsiNumber = new DevExpress.XtraBars.BarStaticItem();
            this.beiNumber = new DevExpress.XtraBars.BarEditItem();
            this.riteNumber = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bdeStart = new DevExpress.XtraBars.BarEditItem();
            this.deStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEnd = new DevExpress.XtraBars.BarEditItem();
            this.deEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bleGroupe = new DevExpress.XtraBars.BarEditItem();
            this.leGroupe = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleCategory = new DevExpress.XtraBars.BarEditItem();
            this.leCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleMobitelGrn = new DevExpress.XtraBars.BarEditItem();
            this.leMobitelGrn = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.btFilterClear = new DevExpress.XtraBars.BarButtonItem();
            this.btOrdersCreate = new DevExpress.XtraBars.BarButtonItem();
            this.chTimeCreate = new DevExpress.XtraBars.BarEditItem();
            this.rchTimeCreate = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.teTimeCreate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.btStartReport = new DevExpress.XtraBars.BarButtonItem();
            this.bdeStartReport = new DevExpress.XtraBars.BarEditItem();
            this.deStartReport = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEndReport = new DevExpress.XtraBars.BarEditItem();
            this.deEndReport = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bdeStartReportAgr = new DevExpress.XtraBars.BarEditItem();
            this.bdeEndReportAgr = new DevExpress.XtraBars.BarEditItem();
            this.btStartReportAgr = new DevExpress.XtraBars.BarButtonItem();
            this.bsiActiveReport = new DevExpress.XtraBars.BarStaticItem();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.deStartReportAgr = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.deEndReportAgr = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rbAnaliz = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.leAgregats = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.leFieldGroups = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.leField = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtbView = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tlDictionary = new DevExpress.XtraTreeList.TreeList();
            this.DicCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.DicName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtbOrder = new DevExpress.XtraTab.XtraTabControl();
            this.xtbpOrderMain = new DevExpress.XtraTab.XtraTabPage();
            this.xtbpOrderDetal = new DevExpress.XtraTab.XtraTabPage();
            this.gcOrderContent = new DevExpress.XtraGrid.GridControl();
            this.bgvOrderContent = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colMakeCar = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCarModel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colInvNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colAgrName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFamily = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colFieldGroupName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOutLinkId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactSquareCalcContent = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPersSquare = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rtePersent = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIdOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colComment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimeStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDistance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSpeedAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colFuelExpens = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFuelExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFuelExpensRun = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFuelExpensRunKm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelDRTExpensSquare_ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensAvgSquare_ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensWithoutWork_ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensAvgWithoutWork_ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelDRTExpensAvgRate_ = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.rteTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bandedGridView3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridView4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridView5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn49 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn50 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn51 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn52 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn53 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn54 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn55 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcFieldsList = new DevExpress.XtraEditors.GroupControl();
            this.gcFields = new DevExpress.XtraGrid.GridControl();
            this.gvFieldsOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Zone_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FieldId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GrpName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SquareF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ORDERS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnMap = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.nbReports = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgReports = new DevExpress.XtraNavBar.NavBarGroup();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.pnGraficAgr = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.pnGraficDrv = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.pnPlan = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.pnFieldsTc = new DevExpress.XtraEditors.PanelControl();
            this.tmCreateOrders = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gvGFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGrope)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCalt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupeAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGWorks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDtReasons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeasons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ieCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvbFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatuses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitelOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvbFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitelGrn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchTimeCreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReport.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReport.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbAnaliz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregats)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).BeginInit();
            this.xtbView.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtbOrder)).BeginInit();
            this.xtbOrder.SuspendLayout();
            this.xtbpOrderMain.SuspendLayout();
            this.xtbpOrderDetal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrderContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvOrderContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtePersent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFieldsList)).BeginInit();
            this.gcFieldsList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFieldsOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbReports)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnGraficAgr)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnGraficDrv)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnPlan)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnFieldsTc)).BeginInit();
            this.SuspendLayout();
            // 
            // gvGFields
            // 
            this.gvGFields.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvGFields.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvGFields.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvGFields.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGFields.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGFields.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvGFields.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvGFields.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvGFields.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvGFields.Appearance.Empty.Options.UseBackColor = true;
            this.gvGFields.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGFields.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGFields.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGFields.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGFields.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvGFields.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvGFields.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvGFields.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGFields.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvGFields.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvGFields.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvGFields.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvGFields.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvGFields.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvGFields.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvGFields.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvGFields.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvGFields.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvGFields.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvGFields.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvGFields.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGFields.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGFields.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvGFields.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvGFields.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvGFields.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvGFields.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvGFields.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvGFields.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGFields.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvGFields.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvGFields.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvGFields.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvGFields.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvGFields.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGFields.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGFields.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvGFields.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGFields.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGFields.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.OddRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.OddRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvGFields.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvGFields.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvGFields.Appearance.Preview.Options.UseBackColor = true;
            this.gvGFields.Appearance.Preview.Options.UseFont = true;
            this.gvGFields.Appearance.Preview.Options.UseForeColor = true;
            this.gvGFields.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGFields.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.Row.Options.UseBackColor = true;
            this.gvGFields.Appearance.Row.Options.UseForeColor = true;
            this.gvGFields.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGFields.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvGFields.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvGFields.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvGFields.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGFields.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGFields.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvGFields.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvGFields.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvGFields.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvGFields.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGFields.Appearance.VertLine.Options.UseBackColor = true;
            this.gvGFields.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_gf,
            this.Name_gf,
            this.Comment_gf});
            this.gvGFields.GridControl = this.gcDict;
            this.gvGFields.Name = "gvGFields";
            this.gvGFields.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gvGFields.OptionsBehavior.Editable = false;
            this.gvGFields.OptionsSelection.MultiSelect = true;
            this.gvGFields.OptionsView.EnableAppearanceEvenRow = true;
            this.gvGFields.OptionsView.EnableAppearanceOddRow = true;
            this.gvGFields.DoubleClick += new System.EventHandler(this.gvGFields_DoubleClick);
            // 
            // Id_gf
            // 
            this.Id_gf.Caption = "Id";
            this.Id_gf.FieldName = "Id";
            this.Id_gf.Name = "Id_gf";
            this.Id_gf.OptionsColumn.AllowEdit = false;
            // 
            // Name_gf
            // 
            this.Name_gf.AppearanceCell.Options.UseTextOptions = true;
            this.Name_gf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_gf.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_gf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_gf.Caption = "��������";
            this.Name_gf.FieldName = "Name";
            this.Name_gf.Name = "Name_gf";
            this.Name_gf.OptionsColumn.AllowEdit = false;
            this.Name_gf.Visible = true;
            this.Name_gf.VisibleIndex = 0;
            // 
            // Comment_gf
            // 
            this.Comment_gf.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_gf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_gf.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_gf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_gf.Caption = "����������";
            this.Comment_gf.FieldName = "Comment";
            this.Comment_gf.Name = "Comment_gf";
            this.Comment_gf.OptionsColumn.AllowEdit = false;
            this.Comment_gf.Visible = true;
            this.Comment_gf.VisibleIndex = 1;
            // 
            // gcDict
            // 
            this.gcDict.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvGFields;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.gvFields;
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode3.LevelTemplate = this.gvCalt;
            gridLevelNode3.RelationName = "Level3";
            gridLevelNode4.LevelTemplate = this.gvAgr;
            gridLevelNode4.RelationName = "Level4";
            gridLevelNode5.LevelTemplate = this.gvOrder;
            gridLevelNode5.RelationName = "Level5";
            gridLevelNode6.LevelTemplate = this.gvPrice;
            gridLevelNode6.RelationName = "Level6";
            gridLevelNode7.LevelTemplate = this.gvUnit;
            gridLevelNode7.RelationName = "Level7";
            gridLevelNode8.LevelTemplate = this.gvGAgr;
            gridLevelNode8.RelationName = "Level8";
            gridLevelNode9.LevelTemplate = this.gvGWorks;
            gridLevelNode9.RelationName = "Level9";
            gridLevelNode10.LevelTemplate = this.gvDtReasons;
            gridLevelNode10.RelationName = "Level10";
            gridLevelNode11.LevelTemplate = this.gvSeasons;
            gridLevelNode11.RelationName = "Level11";
            this.gcDict.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11});
            this.gcDict.Location = new System.Drawing.Point(0, 0);
            this.gcDict.MainView = this.gvWorks;
            this.gcDict.Name = "gcDict";
            this.gcDict.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leFieldGrope,
            this.leZone,
            this.ieCulture,
            this.peIcon,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2,
            this.repositoryItemTextEdit1,
            this.leOrder,
            this.leWork,
            this.leMobitel,
            this.leAgregat,
            this.leUnit,
            this.leGroupeAgr,
            this.rleCulture});
            this.gcDict.Size = new System.Drawing.Size(898, 652);
            this.gcDict.TabIndex = 1;
            this.gcDict.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFields,
            this.gvCalt,
            this.gvAgr,
            this.gvOrder,
            this.gvPrice,
            this.gvUnit,
            this.gvGAgr,
            this.gvGWorks,
            this.gvDtReasons,
            this.gvSeasons,
            this.gvWorks,
            this.gvGFields});
            // 
            // gvFields
            // 
            this.gvFields.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFields.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFields.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFields.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFields.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFields.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFields.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFields.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFields.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvFields.Appearance.Empty.Options.UseBackColor = true;
            this.gvFields.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFields.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFields.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFields.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvFields.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFields.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFields.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFields.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFields.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvFields.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFields.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFields.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFields.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvFields.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvFields.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvFields.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvFields.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvFields.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvFields.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvFields.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFields.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFields.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFields.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFields.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvFields.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFields.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFields.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFields.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFields.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFields.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFields.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvFields.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvFields.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFields.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFields.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFields.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFields.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvFields.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFields.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFields.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvFields.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFields.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFields.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFields.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvFields.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFields.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFields.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFields.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFields.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.OddRow.Options.UseForeColor = true;
            this.gvFields.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvFields.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvFields.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvFields.Appearance.Preview.Options.UseBackColor = true;
            this.gvFields.Appearance.Preview.Options.UseFont = true;
            this.gvFields.Appearance.Preview.Options.UseForeColor = true;
            this.gvFields.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFields.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.Row.Options.UseBackColor = true;
            this.gvFields.Appearance.Row.Options.UseForeColor = true;
            this.gvFields.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFields.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvFields.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFields.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvFields.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFields.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFields.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFields.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvFields.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvFields.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvFields.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvFields.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFields.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFields.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_f,
            this.FGROUPE,
            this.FZONE,
            this.Name_f,
            this.Square,
            this.IsOutOfDate,
            this.Comment_f});
            this.gvFields.GridControl = this.gcDict;
            this.gvFields.Name = "gvFields";
            this.gvFields.OptionsSelection.MultiSelect = true;
            this.gvFields.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFields.OptionsView.EnableAppearanceOddRow = true;
            this.gvFields.DoubleClick += new System.EventHandler(this.gvFields_DoubleClick);
            // 
            // Id_f
            // 
            this.Id_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_f.Caption = "Id";
            this.Id_f.FieldName = "Id";
            this.Id_f.Name = "Id_f";
            this.Id_f.OptionsColumn.AllowEdit = false;
            // 
            // FGROUPE
            // 
            this.FGROUPE.AppearanceCell.Options.UseTextOptions = true;
            this.FGROUPE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.FGROUPE.AppearanceHeader.Options.UseTextOptions = true;
            this.FGROUPE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FGROUPE.Caption = "������ �����";
            this.FGROUPE.ColumnEdit = this.leFieldGrope;
            this.FGROUPE.FieldName = "FGROUPE";
            this.FGROUPE.Name = "FGROUPE";
            this.FGROUPE.OptionsColumn.AllowEdit = false;
            this.FGROUPE.Visible = true;
            this.FGROUPE.VisibleIndex = 0;
            this.FGROUPE.Width = 158;
            // 
            // leFieldGrope
            // 
            this.leFieldGrope.AutoHeight = false;
            this.leFieldGrope.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leFieldGrope.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leFieldGrope.DisplayMember = "Name";
            this.leFieldGrope.Name = "leFieldGrope";
            this.leFieldGrope.ValueMember = "Id";
            // 
            // FZONE
            // 
            this.FZONE.AppearanceCell.Options.UseTextOptions = true;
            this.FZONE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.FZONE.AppearanceHeader.Options.UseTextOptions = true;
            this.FZONE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FZONE.Caption = "�������� ����";
            this.FZONE.ColumnEdit = this.leZone;
            this.FZONE.FieldName = "FZONE";
            this.FZONE.Name = "FZONE";
            this.FZONE.OptionsColumn.AllowEdit = false;
            this.FZONE.Visible = true;
            this.FZONE.VisibleIndex = 1;
            this.FZONE.Width = 177;
            // 
            // leZone
            // 
            this.leZone.AutoHeight = false;
            this.leZone.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leZone.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leZone.DisplayMember = "Name";
            this.leZone.Name = "leZone";
            this.leZone.ValueMember = "Id";
            // 
            // Name_f
            // 
            this.Name_f.AppearanceCell.Options.UseTextOptions = true;
            this.Name_f.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_f.Caption = "��������";
            this.Name_f.FieldName = "Name";
            this.Name_f.Name = "Name_f";
            this.Name_f.OptionsColumn.AllowEdit = false;
            this.Name_f.Visible = true;
            this.Name_f.VisibleIndex = 2;
            this.Name_f.Width = 443;
            // 
            // Square
            // 
            this.Square.AppearanceCell.Options.UseTextOptions = true;
            this.Square.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Square.AppearanceHeader.Options.UseTextOptions = true;
            this.Square.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Square.Caption = "�������, ��";
            this.Square.FieldName = "Square";
            this.Square.Name = "Square";
            this.Square.OptionsColumn.AllowEdit = false;
            this.Square.Visible = true;
            this.Square.VisibleIndex = 3;
            this.Square.Width = 126;
            // 
            // IsOutOfDate
            // 
            this.IsOutOfDate.Caption = "��������";
            this.IsOutOfDate.FieldName = "IsOutOfDate";
            this.IsOutOfDate.Name = "IsOutOfDate";
            this.IsOutOfDate.OptionsColumn.AllowEdit = false;
            this.IsOutOfDate.Visible = true;
            this.IsOutOfDate.VisibleIndex = 5;
            // 
            // Comment_f
            // 
            this.Comment_f.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_f.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_f.Caption = "����������";
            this.Comment_f.FieldName = "Comment";
            this.Comment_f.Name = "Comment_f";
            this.Comment_f.OptionsColumn.AllowEdit = false;
            this.Comment_f.Visible = true;
            this.Comment_f.VisibleIndex = 4;
            this.Comment_f.Width = 186;
            // 
            // gvCalt
            // 
            this.gvCalt.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCalt.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCalt.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCalt.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCalt.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCalt.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCalt.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCalt.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCalt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCalt.Appearance.Empty.Options.UseBackColor = true;
            this.gvCalt.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCalt.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCalt.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCalt.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCalt.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCalt.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCalt.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCalt.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCalt.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCalt.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCalt.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCalt.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCalt.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCalt.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCalt.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCalt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCalt.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCalt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCalt.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCalt.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCalt.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCalt.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCalt.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCalt.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCalt.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCalt.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCalt.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCalt.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCalt.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCalt.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCalt.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCalt.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCalt.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCalt.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCalt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCalt.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCalt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCalt.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCalt.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCalt.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCalt.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCalt.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCalt.Appearance.Preview.Options.UseBackColor = true;
            this.gvCalt.Appearance.Preview.Options.UseFont = true;
            this.gvCalt.Appearance.Preview.Options.UseForeColor = true;
            this.gvCalt.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCalt.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.Row.Options.UseBackColor = true;
            this.gvCalt.Appearance.Row.Options.UseForeColor = true;
            this.gvCalt.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCalt.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCalt.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCalt.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCalt.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCalt.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCalt.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCalt.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCalt.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCalt.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCalt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCalt.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCalt.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_c,
            this.IconC,
            this.Name_c,
            this.Comment_c});
            this.gvCalt.GridControl = this.gcDict;
            this.gvCalt.Images = this.imCollection;
            this.gvCalt.Name = "gvCalt";
            this.gvCalt.OptionsSelection.MultiSelect = true;
            this.gvCalt.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCalt.OptionsView.EnableAppearanceOddRow = true;
            this.gvCalt.DoubleClick += new System.EventHandler(this.gvCalt_DoubleClick);
            // 
            // Id_c
            // 
            this.Id_c.AppearanceCell.Options.UseTextOptions = true;
            this.Id_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_c.Caption = "Id";
            this.Id_c.FieldName = "Id";
            this.Id_c.Name = "Id_c";
            this.Id_c.OptionsColumn.AllowEdit = false;
            // 
            // IconC
            // 
            this.IconC.AppearanceCell.Options.UseTextOptions = true;
            this.IconC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IconC.AppearanceHeader.Options.UseTextOptions = true;
            this.IconC.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IconC.Caption = "Image";
            this.IconC.ColumnEdit = this.peIcon;
            this.IconC.FieldName = "Icon";
            this.IconC.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.IconC.ImageIndex = 2;
            this.IconC.Name = "IconC";
            this.IconC.OptionsColumn.AllowEdit = false;
            this.IconC.Visible = true;
            this.IconC.VisibleIndex = 0;
            this.IconC.Width = 52;
            // 
            // peIcon
            // 
            this.peIcon.AppearanceDisabled.Image = global::Agro.Properties.ResourcesImages.image_edit;
            this.peIcon.AppearanceDisabled.Options.UseImage = true;
            this.peIcon.Name = "peIcon";
            this.peIcon.NullText = "  ";
            this.peIcon.ReadOnly = true;
            // 
            // Name_c
            // 
            this.Name_c.AppearanceCell.Options.UseTextOptions = true;
            this.Name_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_c.Caption = "��������";
            this.Name_c.FieldName = "Name";
            this.Name_c.Name = "Name_c";
            this.Name_c.OptionsColumn.AllowEdit = false;
            this.Name_c.Visible = true;
            this.Name_c.VisibleIndex = 1;
            this.Name_c.Width = 516;
            // 
            // Comment_c
            // 
            this.Comment_c.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_c.Caption = "����������";
            this.Comment_c.FieldName = "Comment";
            this.Comment_c.Name = "Comment_c";
            this.Comment_c.OptionsColumn.AllowEdit = false;
            this.Comment_c.Visible = true;
            this.Comment_c.VisibleIndex = 2;
            this.Comment_c.Width = 522;
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "bullet_green.png");
            this.imCollection.Images.SetKeyName(1, "bullet_red.png");
            this.imCollection.Images.SetKeyName(2, "image_edit.png");
            this.imCollection.Images.SetKeyName(3, "checkbox_no.png");
            this.imCollection.Images.SetKeyName(4, "document.png");
            this.imCollection.Images.SetKeyName(5, "document_locked.png");
            this.imCollection.Images.SetKeyName(6, "document-hf-insert-footer.png");
            this.imCollection.Images.SetKeyName(7, "document-task.png");
            // 
            // gvAgr
            // 
            this.gvAgr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvAgr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvAgr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvAgr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAgr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAgr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvAgr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvAgr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvAgr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvAgr.Appearance.Empty.Options.UseBackColor = true;
            this.gvAgr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAgr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAgr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAgr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAgr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvAgr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvAgr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvAgr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvAgr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvAgr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvAgr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvAgr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvAgr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvAgr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvAgr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvAgr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvAgr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvAgr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvAgr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvAgr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvAgr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAgr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAgr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvAgr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvAgr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvAgr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvAgr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvAgr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvAgr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvAgr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvAgr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvAgr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvAgr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvAgr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvAgr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAgr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAgr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvAgr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAgr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAgr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvAgr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvAgr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvAgr.Appearance.Preview.Options.UseBackColor = true;
            this.gvAgr.Appearance.Preview.Options.UseFont = true;
            this.gvAgr.Appearance.Preview.Options.UseForeColor = true;
            this.gvAgr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAgr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.Row.Options.UseBackColor = true;
            this.gvAgr.Appearance.Row.Options.UseForeColor = true;
            this.gvAgr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAgr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvAgr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvAgr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvAgr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAgr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvAgr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvAgr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvAgr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvAgr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvAgr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAgr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_a,
            this.GroupeAgr,
            this.Name_a,
            this.InvNumber,
            this.Width_a,
            this.Identifier,
            this.Def,
            this.colVehicle,
            this.Comment_a,
            this.Id_work_agr});
            this.gvAgr.GridControl = this.gcDict;
            this.gvAgr.Name = "gvAgr";
            this.gvAgr.OptionsSelection.MultiSelect = true;
            this.gvAgr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvAgr.OptionsView.EnableAppearanceOddRow = true;
            this.gvAgr.DoubleClick += new System.EventHandler(this.gvAgr_DoubleClick);
            // 
            // Id_a
            // 
            this.Id_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_a.Caption = "Id";
            this.Id_a.FieldName = "Id";
            this.Id_a.Name = "Id_a";
            this.Id_a.OptionsColumn.AllowEdit = false;
            // 
            // GroupeAgr
            // 
            this.GroupeAgr.AppearanceCell.Options.UseTextOptions = true;
            this.GroupeAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GroupeAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeAgr.Caption = "������";
            this.GroupeAgr.ColumnEdit = this.leGroupeAgr;
            this.GroupeAgr.FieldName = "Id_main";
            this.GroupeAgr.Name = "GroupeAgr";
            this.GroupeAgr.OptionsColumn.AllowEdit = false;
            this.GroupeAgr.Visible = true;
            this.GroupeAgr.VisibleIndex = 0;
            this.GroupeAgr.Width = 157;
            // 
            // leGroupeAgr
            // 
            this.leGroupeAgr.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.leGroupeAgr.AutoHeight = false;
            this.leGroupeAgr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupeAgr.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leGroupeAgr.DisplayMember = "Name";
            this.leGroupeAgr.Name = "leGroupeAgr";
            this.leGroupeAgr.ValueMember = "Id";
            // 
            // Name_a
            // 
            this.Name_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_a.Caption = "��������";
            this.Name_a.FieldName = "Name";
            this.Name_a.Name = "Name_a";
            this.Name_a.OptionsColumn.AllowEdit = false;
            this.Name_a.Visible = true;
            this.Name_a.VisibleIndex = 1;
            this.Name_a.Width = 284;
            // 
            // InvNumber
            // 
            this.InvNumber.AppearanceCell.Options.UseTextOptions = true;
            this.InvNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.InvNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.InvNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InvNumber.Caption = "���. �����";
            this.InvNumber.FieldName = "InvNumber";
            this.InvNumber.Name = "InvNumber";
            this.InvNumber.OptionsColumn.AllowEdit = false;
            this.InvNumber.Visible = true;
            this.InvNumber.VisibleIndex = 2;
            this.InvNumber.Width = 112;
            // 
            // Width_a
            // 
            this.Width_a.AppearanceCell.Options.UseTextOptions = true;
            this.Width_a.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Width_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_a.Caption = "������, �";
            this.Width_a.FieldName = "Width";
            this.Width_a.Name = "Width_a";
            this.Width_a.OptionsColumn.AllowEdit = false;
            this.Width_a.Visible = true;
            this.Width_a.VisibleIndex = 4;
            this.Width_a.Width = 99;
            // 
            // Identifier
            // 
            this.Identifier.AppearanceCell.Options.UseTextOptions = true;
            this.Identifier.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier.AppearanceHeader.Options.UseTextOptions = true;
            this.Identifier.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier.Caption = "�������������";
            this.Identifier.FieldName = "Identifier";
            this.Identifier.Name = "Identifier";
            this.Identifier.OptionsColumn.AllowEdit = false;
            this.Identifier.Visible = true;
            this.Identifier.VisibleIndex = 3;
            this.Identifier.Width = 131;
            // 
            // Def
            // 
            this.Def.AppearanceCell.Options.UseTextOptions = true;
            this.Def.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Def.AppearanceHeader.Options.UseTextOptions = true;
            this.Def.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Def.Caption = "�� ���������";
            this.Def.FieldName = "Def";
            this.Def.Name = "Def";
            this.Def.OptionsColumn.AllowEdit = false;
            this.Def.Visible = true;
            this.Def.VisibleIndex = 5;
            this.Def.Width = 119;
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "���������";
            this.colVehicle.FieldName = "VehicleAgr";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.OptionsColumn.ReadOnly = true;
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 7;
            this.colVehicle.Width = 115;
            // 
            // Comment_a
            // 
            this.Comment_a.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_a.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_a.Caption = "����������";
            this.Comment_a.FieldName = "Comment";
            this.Comment_a.Name = "Comment_a";
            this.Comment_a.OptionsColumn.AllowEdit = false;
            this.Comment_a.Visible = true;
            this.Comment_a.VisibleIndex = 8;
            this.Comment_a.Width = 117;
            // 
            // Id_work_agr
            // 
            this.Id_work_agr.AppearanceCell.Options.UseTextOptions = true;
            this.Id_work_agr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Id_work_agr.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_work_agr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_work_agr.Caption = "��� �����";
            this.Id_work_agr.ColumnEdit = this.leWork;
            this.Id_work_agr.FieldName = "Id_work";
            this.Id_work_agr.Name = "Id_work_agr";
            this.Id_work_agr.OptionsColumn.AllowEdit = false;
            this.Id_work_agr.Visible = true;
            this.Id_work_agr.VisibleIndex = 6;
            this.Id_work_agr.Width = 116;
            // 
            // leWork
            // 
            this.leWork.AutoHeight = false;
            this.leWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leWork.DisplayMember = "Name";
            this.leWork.Name = "leWork";
            this.leWork.ValueMember = "Id";
            // 
            // gvOrder
            // 
            this.gvOrder.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvOrder.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.Empty.Options.UseBackColor = true;
            this.gvOrder.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvOrder.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvOrder.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvOrder.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvOrder.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvOrder.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvOrder.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvOrder.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.OddRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.OddRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvOrder.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvOrder.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvOrder.Appearance.Preview.Options.UseBackColor = true;
            this.gvOrder.Appearance.Preview.Options.UseFont = true;
            this.gvOrder.Appearance.Preview.Options.UseForeColor = true;
            this.gvOrder.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.Row.Options.UseBackColor = true;
            this.gvOrder.Appearance.Row.Options.UseForeColor = true;
            this.gvOrder.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvOrder.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvOrder.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvOrder.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.VertLine.Options.UseBackColor = true;
            this.gvOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_o,
            this.DateInit,
            this.DateComand,
            this.Name_o,
            this.Comment});
            this.gvOrder.GridControl = this.gcDict;
            this.gvOrder.IndicatorWidth = 30;
            this.gvOrder.Name = "gvOrder";
            this.gvOrder.OptionsSelection.MultiSelect = true;
            this.gvOrder.OptionsView.EnableAppearanceEvenRow = true;
            this.gvOrder.OptionsView.EnableAppearanceOddRow = true;
            this.gvOrder.DoubleClick += new System.EventHandler(this.gvOrder_DoubleClick);
            // 
            // Id_o
            // 
            this.Id_o.Caption = "Id";
            this.Id_o.FieldName = "Id";
            this.Id_o.Name = "Id_o";
            this.Id_o.OptionsColumn.AllowEdit = false;
            // 
            // DateInit
            // 
            this.DateInit.AppearanceCell.Options.UseTextOptions = true;
            this.DateInit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateInit.AppearanceHeader.Options.UseTextOptions = true;
            this.DateInit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateInit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateInit.Caption = "����";
            this.DateInit.FieldName = "DateInit";
            this.DateInit.Name = "DateInit";
            this.DateInit.OptionsColumn.AllowEdit = false;
            this.DateInit.Visible = true;
            this.DateInit.VisibleIndex = 0;
            this.DateInit.Width = 175;
            // 
            // DateComand
            // 
            this.DateComand.AppearanceCell.Options.UseTextOptions = true;
            this.DateComand.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand.AppearanceHeader.Options.UseTextOptions = true;
            this.DateComand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateComand.Caption = "���� �����";
            this.DateComand.FieldName = "DateComand";
            this.DateComand.Name = "DateComand";
            this.DateComand.OptionsColumn.AllowEdit = false;
            this.DateComand.Visible = true;
            this.DateComand.VisibleIndex = 1;
            this.DateComand.Width = 183;
            // 
            // Name_o
            // 
            this.Name_o.AppearanceCell.Options.UseTextOptions = true;
            this.Name_o.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_o.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_o.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_o.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Name_o.Caption = "��������";
            this.Name_o.FieldName = "Name";
            this.Name_o.Name = "Name_o";
            this.Name_o.OptionsColumn.AllowEdit = false;
            this.Name_o.Visible = true;
            this.Name_o.VisibleIndex = 2;
            this.Name_o.Width = 415;
            // 
            // Comment
            // 
            this.Comment.AppearanceCell.Options.UseTextOptions = true;
            this.Comment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment.Caption = "����������";
            this.Comment.FieldName = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.OptionsColumn.AllowEdit = false;
            this.Comment.Visible = true;
            this.Comment.VisibleIndex = 3;
            this.Comment.Width = 317;
            // 
            // gvPrice
            // 
            this.gvPrice.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvPrice.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvPrice.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvPrice.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPrice.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPrice.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvPrice.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvPrice.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvPrice.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvPrice.Appearance.Empty.Options.UseBackColor = true;
            this.gvPrice.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPrice.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPrice.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPrice.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPrice.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvPrice.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvPrice.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvPrice.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPrice.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvPrice.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvPrice.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvPrice.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvPrice.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvPrice.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvPrice.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvPrice.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvPrice.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvPrice.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvPrice.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvPrice.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvPrice.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPrice.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPrice.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvPrice.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvPrice.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvPrice.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvPrice.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvPrice.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvPrice.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPrice.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvPrice.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvPrice.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvPrice.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvPrice.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvPrice.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPrice.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPrice.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvPrice.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPrice.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPrice.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.OddRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvPrice.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvPrice.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvPrice.Appearance.Preview.Options.UseBackColor = true;
            this.gvPrice.Appearance.Preview.Options.UseFont = true;
            this.gvPrice.Appearance.Preview.Options.UseForeColor = true;
            this.gvPrice.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPrice.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.Row.Options.UseBackColor = true;
            this.gvPrice.Appearance.Row.Options.UseForeColor = true;
            this.gvPrice.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPrice.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvPrice.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvPrice.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvPrice.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPrice.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPrice.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvPrice.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvPrice.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvPrice.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvPrice.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPrice.Appearance.VertLine.Options.UseBackColor = true;
            this.gvPrice.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_p,
            this.DateComand_p,
            this.WName,
            this.MName,
            this.AName,
            this.Price,
            this.UName,
            this.Comment_p});
            this.gvPrice.GridControl = this.gcDict;
            this.gvPrice.Name = "gvPrice";
            this.gvPrice.OptionsSelection.MultiSelect = true;
            this.gvPrice.OptionsView.EnableAppearanceEvenRow = true;
            this.gvPrice.OptionsView.EnableAppearanceOddRow = true;
            this.gvPrice.DoubleClick += new System.EventHandler(this.gvPrice_DoubleClick);
            // 
            // Id_p
            // 
            this.Id_p.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_p.Caption = "Id";
            this.Id_p.FieldName = "Id";
            this.Id_p.Name = "Id_p";
            this.Id_p.OptionsColumn.AllowEdit = false;
            // 
            // DateComand_p
            // 
            this.DateComand_p.AppearanceCell.Options.UseTextOptions = true;
            this.DateComand_p.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand_p.AppearanceHeader.Options.UseTextOptions = true;
            this.DateComand_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateComand_p.Caption = "���� �������";
            this.DateComand_p.ColumnEdit = this.leOrder;
            this.DateComand_p.DisplayFormat.FormatString = "d";
            this.DateComand_p.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateComand_p.FieldName = "Id_main";
            this.DateComand_p.Name = "DateComand_p";
            this.DateComand_p.OptionsColumn.AllowEdit = false;
            this.DateComand_p.Visible = true;
            this.DateComand_p.VisibleIndex = 0;
            this.DateComand_p.Width = 126;
            // 
            // leOrder
            // 
            this.leOrder.AutoHeight = false;
            this.leOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateOrder", "���� �������")});
            this.leOrder.DisplayMember = "DateOrder";
            this.leOrder.Name = "leOrder";
            this.leOrder.ValueMember = "Id";
            // 
            // WName
            // 
            this.WName.AppearanceHeader.Options.UseTextOptions = true;
            this.WName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.WName.Caption = "��� �����";
            this.WName.ColumnEdit = this.leWork;
            this.WName.FieldName = "Id_work";
            this.WName.Name = "WName";
            this.WName.OptionsColumn.AllowEdit = false;
            this.WName.Visible = true;
            this.WName.VisibleIndex = 1;
            this.WName.Width = 133;
            // 
            // MName
            // 
            this.MName.AppearanceHeader.Options.UseTextOptions = true;
            this.MName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MName.Caption = "������";
            this.MName.ColumnEdit = this.leMobitel;
            this.MName.FieldName = "Id_mobitel";
            this.MName.Name = "MName";
            this.MName.OptionsColumn.AllowEdit = false;
            this.MName.Visible = true;
            this.MName.VisibleIndex = 2;
            this.MName.Width = 145;
            // 
            // leMobitel
            // 
            this.leMobitel.AutoHeight = false;
            this.leMobitel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leMobitel.DisplayMember = "Name";
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.ValueMember = "Id";
            // 
            // AName
            // 
            this.AName.AppearanceHeader.Options.UseTextOptions = true;
            this.AName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AName.Caption = "�������� ������������";
            this.AName.ColumnEdit = this.leAgregat;
            this.AName.FieldName = "Id_agregat";
            this.AName.Name = "AName";
            this.AName.OptionsColumn.AllowEdit = false;
            this.AName.Visible = true;
            this.AName.VisibleIndex = 3;
            this.AName.Width = 263;
            // 
            // leAgregat
            // 
            this.leAgregat.AutoHeight = false;
            this.leAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leAgregat.DisplayMember = "Name";
            this.leAgregat.Name = "leAgregat";
            this.leAgregat.ValueMember = "Id";
            // 
            // Price
            // 
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Price.Caption = "��������, ���";
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 4;
            this.Price.Width = 124;
            // 
            // UName
            // 
            this.UName.AppearanceHeader.Options.UseTextOptions = true;
            this.UName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.UName.Caption = "��.���";
            this.UName.ColumnEdit = this.leUnit;
            this.UName.FieldName = "Id_unit";
            this.UName.Name = "UName";
            this.UName.OptionsColumn.AllowEdit = false;
            this.UName.Visible = true;
            this.UName.VisibleIndex = 5;
            this.UName.Width = 102;
            // 
            // leUnit
            // 
            this.leUnit.AutoHeight = false;
            this.leUnit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leUnit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leUnit.DisplayMember = "Name";
            this.leUnit.Name = "leUnit";
            this.leUnit.ValueMember = "Id";
            // 
            // Comment_p
            // 
            this.Comment_p.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment_p.Caption = "����������";
            this.Comment_p.FieldName = "Comment";
            this.Comment_p.Name = "Comment_p";
            this.Comment_p.OptionsColumn.AllowEdit = false;
            this.Comment_p.Visible = true;
            this.Comment_p.VisibleIndex = 6;
            this.Comment_p.Width = 197;
            // 
            // gvUnit
            // 
            this.gvUnit.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvUnit.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvUnit.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvUnit.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUnit.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUnit.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvUnit.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvUnit.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvUnit.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvUnit.Appearance.Empty.Options.UseBackColor = true;
            this.gvUnit.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUnit.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUnit.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUnit.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUnit.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvUnit.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvUnit.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvUnit.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvUnit.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvUnit.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvUnit.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvUnit.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvUnit.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvUnit.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvUnit.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvUnit.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvUnit.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvUnit.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvUnit.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvUnit.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvUnit.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUnit.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUnit.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvUnit.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvUnit.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvUnit.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvUnit.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvUnit.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvUnit.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvUnit.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvUnit.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvUnit.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvUnit.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvUnit.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvUnit.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUnit.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUnit.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvUnit.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUnit.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUnit.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.OddRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.OddRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvUnit.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvUnit.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvUnit.Appearance.Preview.Options.UseBackColor = true;
            this.gvUnit.Appearance.Preview.Options.UseFont = true;
            this.gvUnit.Appearance.Preview.Options.UseForeColor = true;
            this.gvUnit.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUnit.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.Row.Options.UseBackColor = true;
            this.gvUnit.Appearance.Row.Options.UseForeColor = true;
            this.gvUnit.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUnit.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvUnit.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvUnit.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvUnit.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUnit.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvUnit.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvUnit.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvUnit.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvUnit.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvUnit.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUnit.Appearance.VertLine.Options.UseBackColor = true;
            this.gvUnit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_u,
            this.Name_u,
            this.NameShort_u,
            this.Factor,
            this.Comment_u});
            this.gvUnit.GridControl = this.gcDict;
            this.gvUnit.Name = "gvUnit";
            this.gvUnit.OptionsSelection.MultiSelect = true;
            this.gvUnit.OptionsView.EnableAppearanceEvenRow = true;
            this.gvUnit.OptionsView.EnableAppearanceOddRow = true;
            this.gvUnit.DoubleClick += new System.EventHandler(this.gvUnit_DoubleClick);
            // 
            // Id_u
            // 
            this.Id_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_u.Caption = "Id";
            this.Id_u.FieldName = "Id";
            this.Id_u.Name = "Id_u";
            this.Id_u.OptionsColumn.AllowEdit = false;
            // 
            // Name_u
            // 
            this.Name_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Name_u.Caption = "��������";
            this.Name_u.FieldName = "Name";
            this.Name_u.Name = "Name_u";
            this.Name_u.OptionsColumn.AllowEdit = false;
            this.Name_u.Visible = true;
            this.Name_u.VisibleIndex = 0;
            this.Name_u.Width = 357;
            // 
            // NameShort_u
            // 
            this.NameShort_u.AppearanceHeader.Options.UseTextOptions = true;
            this.NameShort_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NameShort_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameShort_u.Caption = "������� �����������";
            this.NameShort_u.FieldName = "NameShort";
            this.NameShort_u.Name = "NameShort_u";
            this.NameShort_u.OptionsColumn.AllowEdit = false;
            this.NameShort_u.Visible = true;
            this.NameShort_u.VisibleIndex = 1;
            this.NameShort_u.Width = 161;
            // 
            // Factor
            // 
            this.Factor.AppearanceCell.Options.UseTextOptions = true;
            this.Factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Factor.AppearanceHeader.Options.UseTextOptions = true;
            this.Factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Factor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Factor.Caption = "�����������";
            this.Factor.FieldName = "Factor";
            this.Factor.Name = "Factor";
            this.Factor.OptionsColumn.AllowEdit = false;
            this.Factor.Visible = true;
            this.Factor.VisibleIndex = 2;
            this.Factor.Width = 156;
            // 
            // Comment_u
            // 
            this.Comment_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment_u.Caption = "����������";
            this.Comment_u.FieldName = "Comment";
            this.Comment_u.Name = "Comment_u";
            this.Comment_u.OptionsColumn.AllowEdit = false;
            this.Comment_u.Visible = true;
            this.Comment_u.VisibleIndex = 3;
            this.Comment_u.Width = 416;
            // 
            // gvGAgr
            // 
            this.gvGAgr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvGAgr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvGAgr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGAgr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGAgr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvGAgr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvGAgr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvGAgr.Appearance.Empty.Options.UseBackColor = true;
            this.gvGAgr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGAgr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGAgr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGAgr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGAgr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvGAgr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGAgr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvGAgr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvGAgr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvGAgr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvGAgr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvGAgr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvGAgr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvGAgr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvGAgr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGAgr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGAgr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvGAgr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvGAgr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvGAgr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvGAgr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGAgr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvGAgr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvGAgr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvGAgr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvGAgr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGAgr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGAgr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvGAgr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGAgr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGAgr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvGAgr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvGAgr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvGAgr.Appearance.Preview.Options.UseBackColor = true;
            this.gvGAgr.Appearance.Preview.Options.UseFont = true;
            this.gvGAgr.Appearance.Preview.Options.UseForeColor = true;
            this.gvGAgr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGAgr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.Row.Options.UseBackColor = true;
            this.gvGAgr.Appearance.Row.Options.UseForeColor = true;
            this.gvGAgr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGAgr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvGAgr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvGAgr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvGAgr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGAgr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGAgr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvGAgr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvGAgr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvGAgr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvGAgr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGAgr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvGAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_gagr,
            this.Name_gagr,
            this.Remark_gagr,
            this.Work_gagr});
            this.gvGAgr.GridControl = this.gcDict;
            this.gvGAgr.Name = "gvGAgr";
            this.gvGAgr.OptionsSelection.MultiSelect = true;
            this.gvGAgr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvGAgr.OptionsView.EnableAppearanceOddRow = true;
            this.gvGAgr.DoubleClick += new System.EventHandler(this.gvGAgr_DoubleClick);
            // 
            // Id_gagr
            // 
            this.Id_gagr.Caption = "Id_gagr";
            this.Id_gagr.FieldName = "Id";
            this.Id_gagr.Name = "Id_gagr";
            this.Id_gagr.OptionsColumn.AllowEdit = false;
            // 
            // Name_gagr
            // 
            this.Name_gagr.AppearanceCell.Options.UseTextOptions = true;
            this.Name_gagr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_gagr.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_gagr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_gagr.Caption = "��������";
            this.Name_gagr.FieldName = "Name";
            this.Name_gagr.Name = "Name_gagr";
            this.Name_gagr.OptionsColumn.AllowEdit = false;
            this.Name_gagr.Visible = true;
            this.Name_gagr.VisibleIndex = 0;
            // 
            // Remark_gagr
            // 
            this.Remark_gagr.AppearanceCell.Options.UseTextOptions = true;
            this.Remark_gagr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Remark_gagr.AppearanceHeader.Options.UseTextOptions = true;
            this.Remark_gagr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Remark_gagr.Caption = "����������";
            this.Remark_gagr.FieldName = "Comment";
            this.Remark_gagr.Name = "Remark_gagr";
            this.Remark_gagr.OptionsColumn.AllowEdit = false;
            this.Remark_gagr.Visible = true;
            this.Remark_gagr.VisibleIndex = 2;
            // 
            // Work_gagr
            // 
            this.Work_gagr.AppearanceCell.Options.UseTextOptions = true;
            this.Work_gagr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Work_gagr.AppearanceHeader.Options.UseTextOptions = true;
            this.Work_gagr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Work_gagr.Caption = "��� �����";
            this.Work_gagr.ColumnEdit = this.leWork;
            this.Work_gagr.FieldName = "Id_work";
            this.Work_gagr.Name = "Work_gagr";
            this.Work_gagr.OptionsColumn.AllowEdit = false;
            this.Work_gagr.Visible = true;
            this.Work_gagr.VisibleIndex = 1;
            // 
            // gvGWorks
            // 
            this.gvGWorks.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvGWorks.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvGWorks.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGWorks.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGWorks.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvGWorks.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvGWorks.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvGWorks.Appearance.Empty.Options.UseBackColor = true;
            this.gvGWorks.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGWorks.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvGWorks.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGWorks.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGWorks.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvGWorks.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGWorks.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvGWorks.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvGWorks.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvGWorks.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvGWorks.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvGWorks.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvGWorks.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvGWorks.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvGWorks.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGWorks.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvGWorks.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvGWorks.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvGWorks.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvGWorks.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvGWorks.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvGWorks.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvGWorks.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvGWorks.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvGWorks.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvGWorks.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGWorks.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGWorks.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvGWorks.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGWorks.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGWorks.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.OddRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.OddRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvGWorks.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvGWorks.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvGWorks.Appearance.Preview.Options.UseBackColor = true;
            this.gvGWorks.Appearance.Preview.Options.UseFont = true;
            this.gvGWorks.Appearance.Preview.Options.UseForeColor = true;
            this.gvGWorks.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvGWorks.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.Row.Options.UseBackColor = true;
            this.gvGWorks.Appearance.Row.Options.UseForeColor = true;
            this.gvGWorks.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvGWorks.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvGWorks.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvGWorks.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvGWorks.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvGWorks.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvGWorks.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvGWorks.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvGWorks.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvGWorks.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvGWorks.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvGWorks.Appearance.VertLine.Options.UseBackColor = true;
            this.gvGWorks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_gw,
            this.Name_gw,
            this.SpeedControl,
            this.DepthControl,
            this.Comment_gw});
            this.gvGWorks.GridControl = this.gcDict;
            this.gvGWorks.Name = "gvGWorks";
            this.gvGWorks.OptionsSelection.MultiSelect = true;
            this.gvGWorks.OptionsView.EnableAppearanceEvenRow = true;
            this.gvGWorks.OptionsView.EnableAppearanceOddRow = true;
            this.gvGWorks.DoubleClick += new System.EventHandler(this.gvGWorks_DoubleClick);
            // 
            // Id_gw
            // 
            this.Id_gw.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_gw.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_gw.Caption = "Id";
            this.Id_gw.FieldName = "Id";
            this.Id_gw.Name = "Id_gw";
            this.Id_gw.OptionsColumn.AllowEdit = false;
            this.Id_gw.OptionsColumn.ReadOnly = true;
            // 
            // Name_gw
            // 
            this.Name_gw.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_gw.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_gw.Caption = "��������";
            this.Name_gw.FieldName = "Name";
            this.Name_gw.Name = "Name_gw";
            this.Name_gw.OptionsColumn.AllowEdit = false;
            this.Name_gw.OptionsColumn.ReadOnly = true;
            this.Name_gw.Visible = true;
            this.Name_gw.VisibleIndex = 0;
            this.Name_gw.Width = 343;
            // 
            // SpeedControl
            // 
            this.SpeedControl.AppearanceCell.Options.UseTextOptions = true;
            this.SpeedControl.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedControl.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedControl.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedControl.Caption = "�������� ��������";
            this.SpeedControl.FieldName = "IsSpeedControl";
            this.SpeedControl.Name = "SpeedControl";
            this.SpeedControl.OptionsColumn.AllowEdit = false;
            this.SpeedControl.OptionsColumn.ReadOnly = true;
            this.SpeedControl.Visible = true;
            this.SpeedControl.VisibleIndex = 1;
            this.SpeedControl.Width = 128;
            // 
            // DepthControl
            // 
            this.DepthControl.AppearanceCell.Options.UseTextOptions = true;
            this.DepthControl.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DepthControl.AppearanceHeader.Options.UseTextOptions = true;
            this.DepthControl.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DepthControl.Caption = "�������� �������";
            this.DepthControl.FieldName = "IsDepthControl";
            this.DepthControl.Name = "DepthControl";
            this.DepthControl.OptionsColumn.AllowEdit = false;
            this.DepthControl.OptionsColumn.ReadOnly = true;
            this.DepthControl.Visible = true;
            this.DepthControl.VisibleIndex = 2;
            this.DepthControl.Width = 115;
            // 
            // Comment_gw
            // 
            this.Comment_gw.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_gw.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_gw.Caption = "����������";
            this.Comment_gw.FieldName = "Comment";
            this.Comment_gw.Name = "Comment_gw";
            this.Comment_gw.OptionsColumn.AllowEdit = false;
            this.Comment_gw.OptionsColumn.ReadOnly = true;
            this.Comment_gw.Visible = true;
            this.Comment_gw.VisibleIndex = 3;
            this.Comment_gw.Width = 566;
            // 
            // gvDtReasons
            // 
            this.gvDtReasons.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.Empty.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDtReasons.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvDtReasons.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDtReasons.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDtReasons.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvDtReasons.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvDtReasons.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvDtReasons.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDtReasons.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvDtReasons.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvDtReasons.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDtReasons.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDtReasons.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDtReasons.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDtReasons.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.OddRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.OddRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvDtReasons.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvDtReasons.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvDtReasons.Appearance.Preview.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.Preview.Options.UseFont = true;
            this.gvDtReasons.Appearance.Preview.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvDtReasons.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.Row.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.Row.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvDtReasons.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvDtReasons.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvDtReasons.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvDtReasons.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvDtReasons.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvDtReasons.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvDtReasons.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvDtReasons.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvDtReasons.Appearance.VertLine.Options.UseBackColor = true;
            this.gvDtReasons.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_dtr,
            this.Name_dtr,
            this.Comment_dtr});
            this.gvDtReasons.GridControl = this.gcDict;
            this.gvDtReasons.Name = "gvDtReasons";
            this.gvDtReasons.OptionsSelection.MultiSelect = true;
            this.gvDtReasons.OptionsView.EnableAppearanceEvenRow = true;
            this.gvDtReasons.OptionsView.EnableAppearanceOddRow = true;
            this.gvDtReasons.DoubleClick += new System.EventHandler(this.gvDtReasons_DoubleClick);
            // 
            // Id_dtr
            // 
            this.Id_dtr.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_dtr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_dtr.Caption = "Id";
            this.Id_dtr.FieldName = "Id";
            this.Id_dtr.Name = "Id_dtr";
            this.Id_dtr.OptionsColumn.AllowEdit = false;
            this.Id_dtr.OptionsColumn.ReadOnly = true;
            // 
            // Name_dtr
            // 
            this.Name_dtr.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_dtr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_dtr.Caption = "��������";
            this.Name_dtr.FieldName = "ReasonName";
            this.Name_dtr.Name = "Name_dtr";
            this.Name_dtr.OptionsColumn.AllowEdit = false;
            this.Name_dtr.OptionsColumn.ReadOnly = true;
            this.Name_dtr.Visible = true;
            this.Name_dtr.VisibleIndex = 0;
            // 
            // Comment_dtr
            // 
            this.Comment_dtr.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_dtr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_dtr.Caption = "����������";
            this.Comment_dtr.FieldName = "ReasonComment";
            this.Comment_dtr.Name = "Comment_dtr";
            this.Comment_dtr.OptionsColumn.AllowEdit = false;
            this.Comment_dtr.OptionsColumn.ReadOnly = true;
            this.Comment_dtr.Visible = true;
            this.Comment_dtr.VisibleIndex = 1;
            // 
            // gvSeasons
            // 
            this.gvSeasons.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colsId,
            this.colsDateStart,
            this.colsDateEnd,
            this.colsCulture,
            this.colsSquarePlan,
            this.colsComment});
            this.gvSeasons.GridControl = this.gcDict;
            this.gvSeasons.Name = "gvSeasons";
            this.gvSeasons.OptionsSelection.MultiSelect = true;
            this.gvSeasons.DoubleClick += new System.EventHandler(this.gvSeasons_DoubleClick);
            // 
            // colsId
            // 
            this.colsId.AppearanceHeader.Options.UseTextOptions = true;
            this.colsId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsId.Caption = "Id";
            this.colsId.FieldName = "Id";
            this.colsId.Name = "colsId";
            this.colsId.OptionsColumn.AllowEdit = false;
            this.colsId.OptionsColumn.ReadOnly = true;
            // 
            // colsDateStart
            // 
            this.colsDateStart.AppearanceCell.Options.UseTextOptions = true;
            this.colsDateStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsDateStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colsDateStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsDateStart.Caption = "���� ������";
            this.colsDateStart.FieldName = "DateStart";
            this.colsDateStart.Name = "colsDateStart";
            this.colsDateStart.OptionsColumn.AllowEdit = false;
            this.colsDateStart.OptionsColumn.ReadOnly = true;
            this.colsDateStart.Visible = true;
            this.colsDateStart.VisibleIndex = 0;
            this.colsDateStart.Width = 161;
            // 
            // colsDateEnd
            // 
            this.colsDateEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colsDateEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsDateEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colsDateEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsDateEnd.Caption = "���� �����";
            this.colsDateEnd.FieldName = "DateEnd";
            this.colsDateEnd.Name = "colsDateEnd";
            this.colsDateEnd.OptionsColumn.AllowEdit = false;
            this.colsDateEnd.OptionsColumn.ReadOnly = true;
            this.colsDateEnd.Visible = true;
            this.colsDateEnd.VisibleIndex = 1;
            this.colsDateEnd.Width = 159;
            // 
            // colsCulture
            // 
            this.colsCulture.AppearanceHeader.Options.UseTextOptions = true;
            this.colsCulture.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsCulture.Caption = "��������";
            this.colsCulture.ColumnEdit = this.rleCulture;
            this.colsCulture.FieldName = "Id_culture";
            this.colsCulture.Name = "colsCulture";
            this.colsCulture.OptionsColumn.AllowEdit = false;
            this.colsCulture.OptionsColumn.ReadOnly = true;
            this.colsCulture.Visible = true;
            this.colsCulture.VisibleIndex = 2;
            this.colsCulture.Width = 358;
            // 
            // rleCulture
            // 
            this.rleCulture.AutoHeight = false;
            this.rleCulture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleCulture.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "���")});
            this.rleCulture.DisplayMember = "Name";
            this.rleCulture.Name = "rleCulture";
            this.rleCulture.ValueMember = "Id";
            // 
            // colsSquarePlan
            // 
            this.colsSquarePlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colsSquarePlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsSquarePlan.Caption = "������� ����.";
            this.colsSquarePlan.FieldName = "SquarePlan";
            this.colsSquarePlan.Name = "colsSquarePlan";
            this.colsSquarePlan.OptionsColumn.AllowEdit = false;
            this.colsSquarePlan.OptionsColumn.ReadOnly = true;
            this.colsSquarePlan.Visible = true;
            this.colsSquarePlan.VisibleIndex = 3;
            this.colsSquarePlan.Width = 107;
            // 
            // colsComment
            // 
            this.colsComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colsComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsComment.Caption = "����������";
            this.colsComment.FieldName = "Comment";
            this.colsComment.Name = "colsComment";
            this.colsComment.OptionsColumn.AllowEdit = false;
            this.colsComment.OptionsColumn.ReadOnly = true;
            this.colsComment.Visible = true;
            this.colsComment.VisibleIndex = 4;
            this.colsComment.Width = 367;
            // 
            // gvWorks
            // 
            this.gvWorks.ActiveFilterEnabled = false;
            this.gvWorks.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvWorks.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvWorks.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvWorks.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWorks.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWorks.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvWorks.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvWorks.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvWorks.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvWorks.Appearance.Empty.Options.UseBackColor = true;
            this.gvWorks.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWorks.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvWorks.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWorks.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWorks.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvWorks.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvWorks.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvWorks.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvWorks.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvWorks.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvWorks.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvWorks.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvWorks.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvWorks.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvWorks.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvWorks.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvWorks.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvWorks.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvWorks.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvWorks.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvWorks.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWorks.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvWorks.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvWorks.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvWorks.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvWorks.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvWorks.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvWorks.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvWorks.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvWorks.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvWorks.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvWorks.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvWorks.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvWorks.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvWorks.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWorks.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWorks.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvWorks.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWorks.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWorks.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.OddRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.OddRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvWorks.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvWorks.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvWorks.Appearance.Preview.Options.UseBackColor = true;
            this.gvWorks.Appearance.Preview.Options.UseFont = true;
            this.gvWorks.Appearance.Preview.Options.UseForeColor = true;
            this.gvWorks.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvWorks.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.Row.Options.UseBackColor = true;
            this.gvWorks.Appearance.Row.Options.UseForeColor = true;
            this.gvWorks.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvWorks.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvWorks.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvWorks.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvWorks.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvWorks.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvWorks.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvWorks.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvWorks.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvWorks.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvWorks.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvWorks.Appearance.VertLine.Options.UseBackColor = true;
            this.gvWorks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_work,
            this.colGroupWork,
            this.colTypeWork,
            this.Name_work,
            this.SpeedBottom,
            this.SpeedTop,
            this.Comment_work});
            this.gvWorks.GridControl = this.gcDict;
            this.gvWorks.Name = "gvWorks";
            this.gvWorks.OptionsBehavior.Editable = false;
            this.gvWorks.OptionsSelection.MultiSelect = true;
            this.gvWorks.OptionsView.EnableAppearanceEvenRow = true;
            this.gvWorks.OptionsView.EnableAppearanceOddRow = true;
            this.gvWorks.DoubleClick += new System.EventHandler(this.gvWorks_DoubleClick);
            // 
            // Id_work
            // 
            this.Id_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_work.Caption = "Id_work";
            this.Id_work.FieldName = "Id";
            this.Id_work.Name = "Id_work";
            this.Id_work.OptionsColumn.AllowEdit = false;
            this.Id_work.OptionsColumn.ReadOnly = true;
            // 
            // colGroupWork
            // 
            this.colGroupWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupWork.Caption = "������";
            this.colGroupWork.FieldName = "GroupWork";
            this.colGroupWork.Name = "colGroupWork";
            this.colGroupWork.OptionsColumn.AllowEdit = false;
            this.colGroupWork.Visible = true;
            this.colGroupWork.VisibleIndex = 0;
            this.colGroupWork.Width = 151;
            // 
            // colTypeWork
            // 
            this.colTypeWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeWork.Caption = "���";
            this.colTypeWork.FieldName = "TypeWork";
            this.colTypeWork.Name = "colTypeWork";
            this.colTypeWork.OptionsColumn.AllowEdit = false;
            this.colTypeWork.OptionsColumn.ReadOnly = true;
            this.colTypeWork.Visible = true;
            this.colTypeWork.VisibleIndex = 2;
            this.colTypeWork.Width = 111;
            // 
            // Name_work
            // 
            this.Name_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_work.Caption = "��������";
            this.Name_work.FieldName = "Name";
            this.Name_work.Name = "Name_work";
            this.Name_work.OptionsColumn.AllowEdit = false;
            this.Name_work.OptionsColumn.ReadOnly = true;
            this.Name_work.Visible = true;
            this.Name_work.VisibleIndex = 1;
            this.Name_work.Width = 347;
            // 
            // SpeedBottom
            // 
            this.SpeedBottom.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedBottom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedBottom.Caption = "�������� �� ��/���";
            this.SpeedBottom.FieldName = "SpeedBottom";
            this.SpeedBottom.Name = "SpeedBottom";
            this.SpeedBottom.OptionsColumn.AllowEdit = false;
            this.SpeedBottom.OptionsColumn.ReadOnly = true;
            this.SpeedBottom.Visible = true;
            this.SpeedBottom.VisibleIndex = 3;
            this.SpeedBottom.Width = 179;
            // 
            // SpeedTop
            // 
            this.SpeedTop.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedTop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedTop.Caption = "�������� �� ��/���";
            this.SpeedTop.FieldName = "SpeedTop";
            this.SpeedTop.Name = "SpeedTop";
            this.SpeedTop.OptionsColumn.AllowEdit = false;
            this.SpeedTop.OptionsColumn.ReadOnly = true;
            this.SpeedTop.Visible = true;
            this.SpeedTop.VisibleIndex = 4;
            this.SpeedTop.Width = 179;
            // 
            // Comment_work
            // 
            this.Comment_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_work.Caption = "����������";
            this.Comment_work.FieldName = "Comment";
            this.Comment_work.Name = "Comment_work";
            this.Comment_work.OptionsColumn.AllowEdit = false;
            this.Comment_work.OptionsColumn.ReadOnly = true;
            this.Comment_work.Visible = true;
            this.Comment_work.VisibleIndex = 5;
            this.Comment_work.Width = 283;
            // 
            // ieCulture
            // 
            this.ieCulture.AutoHeight = false;
            this.ieCulture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ieCulture.Name = "ieCulture";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit1.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.NullDate = " ";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.repositoryItemDateEdit2.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.repositoryItemDateEdit2.Mask.EditMask = "";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.NullText = " ";
            this.repositoryItemDateEdit2.NullValuePrompt = " ";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gvbFields
            // 
            this.gvbFields.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.gvbFields.ColumnPanelRowHeight = 60;
            this.gvbFields.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Id1,
            this.Field,
            this.Family,
            this.TimeStart1,
            this.TimeEnd1,
            this.FactTime,
            this.TimeMove1,
            this.TimeStop,
            this.TimeRate,
            this.DistanceM,
            this.FactSquare,
            this.FactSquareCalc,
            this.SpeedAvgField});
            this.gvbFields.GridControl = this.gcOrder;
            this.gvbFields.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gvbFields.LevelIndent = 1;
            this.gvbFields.Name = "gvbFields";
            this.gvbFields.OptionsBehavior.Editable = false;
            this.gvbFields.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "����";
            this.gridBand2.Columns.Add(this.Id1);
            this.gridBand2.Columns.Add(this.Field);
            this.gridBand2.Columns.Add(this.Family);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 200;
            // 
            // Id1
            // 
            this.Id1.AppearanceHeader.Options.UseTextOptions = true;
            this.Id1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id1.Caption = "Id";
            this.Id1.FieldName = "Id";
            this.Id1.Name = "Id1";
            this.Id1.OptionsColumn.AllowEdit = false;
            // 
            // Field
            // 
            this.Field.AppearanceHeader.Options.UseTextOptions = true;
            this.Field.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Field.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Field.Caption = "����";
            this.Field.FieldName = "Name";
            this.Field.Name = "Field";
            this.Field.OptionsColumn.AllowEdit = false;
            this.Field.Visible = true;
            this.Field.Width = 98;
            // 
            // Family
            // 
            this.Family.AppearanceHeader.Options.UseTextOptions = true;
            this.Family.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Family.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Family.Caption = "��������";
            this.Family.FieldName = "Family";
            this.Family.Name = "Family";
            this.Family.OptionsColumn.AllowEdit = false;
            this.Family.Visible = true;
            this.Family.Width = 102;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "�����";
            this.gridBand3.Columns.Add(this.TimeStart1);
            this.gridBand3.Columns.Add(this.TimeEnd1);
            this.gridBand3.Columns.Add(this.FactTime);
            this.gridBand3.Columns.Add(this.TimeMove1);
            this.gridBand3.Columns.Add(this.TimeStop);
            this.gridBand3.Columns.Add(this.TimeRate);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 1;
            this.gridBand3.Width = 531;
            // 
            // TimeStart1
            // 
            this.TimeStart1.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStart1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart1.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStart1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStart1.Caption = "�����";
            this.TimeStart1.DisplayFormat.FormatString = "t";
            this.TimeStart1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStart1.FieldName = "TimeStart";
            this.TimeStart1.Name = "TimeStart1";
            this.TimeStart1.OptionsColumn.AllowEdit = false;
            this.TimeStart1.Visible = true;
            this.TimeStart1.Width = 87;
            // 
            // TimeEnd1
            // 
            this.TimeEnd1.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEnd1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd1.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEnd1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEnd1.Caption = "�����";
            this.TimeEnd1.DisplayFormat.FormatString = "t";
            this.TimeEnd1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEnd1.FieldName = "TimeEnd";
            this.TimeEnd1.Name = "TimeEnd1";
            this.TimeEnd1.OptionsColumn.AllowEdit = false;
            this.TimeEnd1.Visible = true;
            this.TimeEnd1.Width = 87;
            // 
            // FactTime
            // 
            this.FactTime.AppearanceCell.Options.UseTextOptions = true;
            this.FactTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.Options.UseTextOptions = true;
            this.FactTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactTime.Caption = "����� �����, �";
            this.FactTime.DisplayFormat.FormatString = "t";
            this.FactTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FactTime.FieldName = "FactTime";
            this.FactTime.Name = "FactTime";
            this.FactTime.OptionsColumn.AllowEdit = false;
            this.FactTime.Visible = true;
            this.FactTime.Width = 87;
            // 
            // TimeMove1
            // 
            this.TimeMove1.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMove1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove1.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMove1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMove1.Caption = "����� ��������, �";
            this.TimeMove1.DisplayFormat.FormatString = "t";
            this.TimeMove1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeMove1.FieldName = "TimeMove";
            this.TimeMove1.Name = "TimeMove1";
            this.TimeMove1.OptionsColumn.AllowEdit = false;
            this.TimeMove1.Visible = true;
            this.TimeMove1.Width = 87;
            // 
            // TimeStop
            // 
            this.TimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStop.Caption = "����� �������, �";
            this.TimeStop.DisplayFormat.FormatString = "t";
            this.TimeStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStop.FieldName = "TimeStop";
            this.TimeStop.Name = "TimeStop";
            this.TimeStop.OptionsColumn.AllowEdit = false;
            this.TimeStop.Visible = true;
            this.TimeStop.Width = 87;
            // 
            // TimeRate
            // 
            this.TimeRate.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRate.Caption = "��������, �";
            this.TimeRate.DisplayFormat.FormatString = "t";
            this.TimeRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeRate.FieldName = "TimeRate";
            this.TimeRate.Name = "TimeRate";
            this.TimeRate.OptionsColumn.AllowEdit = false;
            this.TimeRate.Visible = true;
            this.TimeRate.Width = 96;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "������";
            this.gridBand4.Columns.Add(this.DistanceM);
            this.gridBand4.Columns.Add(this.FactSquare);
            this.gridBand4.Columns.Add(this.FactSquareCalc);
            this.gridBand4.Columns.Add(this.SpeedAvgField);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 358;
            // 
            // DistanceM
            // 
            this.DistanceM.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DistanceM.Caption = "������";
            this.DistanceM.FieldName = "Distance";
            this.DistanceM.Name = "DistanceM";
            this.DistanceM.OptionsColumn.AllowEdit = false;
            this.DistanceM.Visible = true;
            this.DistanceM.Width = 87;
            // 
            // FactSquare
            // 
            this.FactSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquare.Caption = "���������� (�� �������), ��";
            this.FactSquare.FieldName = "FactSquare";
            this.FactSquare.Name = "FactSquare";
            this.FactSquare.OptionsColumn.AllowEdit = false;
            this.FactSquare.Visible = true;
            this.FactSquare.Width = 87;
            // 
            // FactSquareCalc
            // 
            this.FactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareCalc.Caption = "���������� (�� �������), ��";
            this.FactSquareCalc.FieldName = "FactSquareCalc";
            this.FactSquareCalc.Name = "FactSquareCalc";
            this.FactSquareCalc.OptionsColumn.AllowEdit = false;
            this.FactSquareCalc.Visible = true;
            this.FactSquareCalc.Width = 87;
            // 
            // SpeedAvgField
            // 
            this.SpeedAvgField.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvgField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvgField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvgField.Caption = "������� ��������";
            this.SpeedAvgField.FieldName = "SpeedAvg";
            this.SpeedAvgField.Name = "SpeedAvgField";
            this.SpeedAvgField.OptionsColumn.AllowEdit = false;
            this.SpeedAvgField.Visible = true;
            this.SpeedAvgField.Width = 97;
            // 
            // gcOrder
            // 
            this.gcOrder.DataMember = "agro_field";
            this.gcOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrder.Location = new System.Drawing.Point(0, 0);
            this.gcOrder.MainView = this.bgvOrder;
            this.gcOrder.Name = "gcOrder";
            this.gcOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.leMobitelOrder,
            this.pbValidity,
            this.icbStatuses});
            this.gcOrder.Size = new System.Drawing.Size(1148, 396);
            this.gcOrder.TabIndex = 1;
            this.gcOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvOrder,
            this.gvbFuel,
            this.bandedGridView1,
            this.gvbFields});
            this.gcOrder.DoubleClick += new System.EventHandler(this.gcOrder_DoubleClick);
            // 
            // bgvOrder
            // 
            this.bgvOrder.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bgvOrder.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bgvOrder.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrder.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrder.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bgvOrder.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bgvOrder.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.bgvOrder.Appearance.Empty.Options.UseBackColor = true;
            this.bgvOrder.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrder.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrder.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.EvenRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.EvenRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.EvenRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrder.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrder.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bgvOrder.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvOrder.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bgvOrder.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.bgvOrder.Appearance.FixedLine.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bgvOrder.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bgvOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.bgvOrder.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.bgvOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bgvOrder.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bgvOrder.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrder.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrder.Appearance.GroupButton.Options.UseBackColor = true;
            this.bgvOrder.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bgvOrder.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bgvOrder.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.bgvOrder.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvOrder.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bgvOrder.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bgvOrder.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.GroupRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.GroupRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bgvOrder.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bgvOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrder.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.HorzLine.Options.UseBackColor = true;
            this.bgvOrder.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrder.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrder.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.OddRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.OddRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.OddRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.bgvOrder.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.bgvOrder.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.bgvOrder.Appearance.Preview.Options.UseBackColor = true;
            this.bgvOrder.Appearance.Preview.Options.UseFont = true;
            this.bgvOrder.Appearance.Preview.Options.UseForeColor = true;
            this.bgvOrder.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrder.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.Row.Options.UseBackColor = true;
            this.bgvOrder.Appearance.Row.Options.UseForeColor = true;
            this.bgvOrder.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrder.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.bgvOrder.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bgvOrder.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.bgvOrder.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrder.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrder.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.bgvOrder.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bgvOrder.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bgvOrder.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bgvOrder.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrder.Appearance.VertLine.Options.UseBackColor = true;
            this.bgvOrder.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gbQuontity});
            this.bgvOrder.ColumnPanelRowHeight = 40;
            this.bgvOrder.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ID,
            this.colState,
            this.DateLastRecalc,
            this.DateGrn,
            this.Mobitel,
            this.LocationStart,
            this.LocationEnd,
            this.TimeStart,
            this.TimeEnd,
            this.TimeWork,
            this.TimeMove,
            this.Distance,
            this.SpeedAvg,
            this.CommentOrder,
            this.SquareWorkDescript,
            this.PathWithoutWork,
            this.FuelDUTExpensSquare,
            this.FuelDUTExpensAvgSquare,
            this.FuelDUTExpensWithoutWork,
            this.FuelDUTExpensAvgWithoutWork,
            this.FuelDRTExpensSquare,
            this.FuelDRTExpensAvgSquare,
            this.FuelDRTExpensWithoutWork,
            this.FuelDRTExpensAvgWithoutWork,
            this.FactSquareCol,
            this.colFactSquareCalc,
            this.colFactSquareCalcOverlap,
            this.bcolValidity,
            this.bcolPointsIntervalMax,
            this.bcolCategory});
            this.bgvOrder.GridControl = this.gcOrder;
            this.bgvOrder.IndicatorWidth = 40;
            this.bgvOrder.Name = "bgvOrder";
            this.bgvOrder.OptionsBehavior.Editable = false;
            this.bgvOrder.OptionsSelection.MultiSelect = true;
            this.bgvOrder.OptionsView.AllowHtmlDrawHeaders = true;
            this.bgvOrder.OptionsView.ColumnAutoWidth = false;
            this.bgvOrder.OptionsView.EnableAppearanceEvenRow = true;
            this.bgvOrder.OptionsView.EnableAppearanceOddRow = true;
            this.bgvOrder.OptionsView.ShowFooter = true;
            this.bgvOrder.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.bgvOrder_CustomDrawRowIndicator);
            this.bgvOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bgvOrder_KeyDown);
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "����� ����������";
            this.gridBand15.Columns.Add(this.ID);
            this.gridBand15.Columns.Add(this.colState);
            this.gridBand15.Columns.Add(this.DateLastRecalc);
            this.gridBand15.Columns.Add(this.DateGrn);
            this.gridBand15.Columns.Add(this.Mobitel);
            this.gridBand15.Columns.Add(this.bcolCategory);
            this.gridBand15.Columns.Add(this.SquareWorkDescript);
            this.gridBand15.Columns.Add(this.FactSquareCol);
            this.gridBand15.Columns.Add(this.colFactSquareCalc);
            this.gridBand15.Columns.Add(this.colFactSquareCalcOverlap);
            this.gridBand15.Columns.Add(this.PathWithoutWork);
            this.gridBand15.Columns.Add(this.CommentOrder);
            this.gridBand15.MinWidth = 20;
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 0;
            this.gridBand15.Width = 1173;
            // 
            // ID
            // 
            this.ID.AppearanceHeader.Options.UseTextOptions = true;
            this.ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ID.Caption = "�����";
            this.ID.FieldName = "Id";
            this.ID.MinWidth = 10;
            this.ID.Name = "ID";
            this.ID.OptionsColumn.AllowEdit = false;
            this.ID.OptionsColumn.ReadOnly = true;
            this.ID.Visible = true;
            this.ID.Width = 79;
            // 
            // colState
            // 
            this.colState.ColumnEdit = this.icbStatuses;
            this.colState.FieldName = "StateOrder";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.OptionsColumn.ReadOnly = true;
            this.colState.OptionsColumn.ShowCaption = false;
            this.colState.ToolTip = "������ ������: �����, �����������,�����������,��������";
            this.colState.Visible = true;
            this.colState.Width = 28;
            // 
            // icbStatuses
            // 
            this.icbStatuses.AutoHeight = false;
            this.icbStatuses.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbStatuses.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbStatuses.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("0", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1", 1, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2", 2, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3", 3, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4", 4, 5)});
            this.icbStatuses.Name = "icbStatuses";
            this.icbStatuses.SmallImages = this.imCollection;
            // 
            // DateLastRecalc
            // 
            this.DateLastRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.DateLastRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateLastRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.DateLastRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateLastRecalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateLastRecalc.Caption = "���� ���������";
            this.DateLastRecalc.DisplayFormat.FormatString = "g";
            this.DateLastRecalc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateLastRecalc.FieldName = "DateLastRecalc";
            this.DateLastRecalc.Name = "DateLastRecalc";
            this.DateLastRecalc.OptionsColumn.AllowEdit = false;
            this.DateLastRecalc.OptionsColumn.ReadOnly = true;
            this.DateLastRecalc.Visible = true;
            this.DateLastRecalc.Width = 100;
            // 
            // DateGrn
            // 
            this.DateGrn.AppearanceCell.Options.UseTextOptions = true;
            this.DateGrn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateGrn.AppearanceHeader.Options.UseTextOptions = true;
            this.DateGrn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateGrn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateGrn.Caption = "����";
            this.DateGrn.DisplayFormat.FormatString = "d";
            this.DateGrn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateGrn.FieldName = "Date";
            this.DateGrn.Name = "DateGrn";
            this.DateGrn.OptionsColumn.AllowEdit = false;
            this.DateGrn.OptionsColumn.ReadOnly = true;
            this.DateGrn.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedCell;
            this.DateGrn.Visible = true;
            this.DateGrn.Width = 100;
            // 
            // Mobitel
            // 
            this.Mobitel.AppearanceHeader.Options.UseTextOptions = true;
            this.Mobitel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Mobitel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Mobitel.Caption = "�������";
            this.Mobitel.ColumnEdit = this.leMobitelOrder;
            this.Mobitel.FieldName = "Id_mobitel";
            this.Mobitel.Name = "Mobitel";
            this.Mobitel.OptionsColumn.AllowEdit = false;
            this.Mobitel.OptionsColumn.ReadOnly = true;
            this.Mobitel.Visible = true;
            this.Mobitel.Width = 100;
            // 
            // leMobitelOrder
            // 
            this.leMobitelOrder.AutoHeight = false;
            this.leMobitelOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitelOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leMobitelOrder.DisplayMember = "Name";
            this.leMobitelOrder.Name = "leMobitelOrder";
            this.leMobitelOrder.ShowLines = false;
            this.leMobitelOrder.ValueMember = "Id";
            // 
            // bcolCategory
            // 
            this.bcolCategory.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolCategory.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolCategory.Caption = "���������";
            this.bcolCategory.FieldName = "Category";
            this.bcolCategory.Name = "bcolCategory";
            this.bcolCategory.Visible = true;
            // 
            // SquareWorkDescript
            // 
            this.SquareWorkDescript.AppearanceHeader.Options.UseTextOptions = true;
            this.SquareWorkDescript.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SquareWorkDescript.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SquareWorkDescript.Caption = "������������ ����";
            this.SquareWorkDescript.FieldName = "SquareWorkDescript";
            this.SquareWorkDescript.Name = "SquareWorkDescript";
            this.SquareWorkDescript.OptionsColumn.AllowEdit = false;
            this.SquareWorkDescript.Visible = true;
            this.SquareWorkDescript.Width = 100;
            // 
            // FactSquareCol
            // 
            this.FactSquareCol.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareCol.Caption = "���������� (�� �������), ��";
            this.FactSquareCol.FieldName = "FactSquare";
            this.FactSquareCol.Name = "FactSquareCol";
            this.FactSquareCol.OptionsColumn.AllowEdit = false;
            this.FactSquareCol.OptionsColumn.AllowFocus = false;
            this.FactSquareCol.OptionsColumn.ReadOnly = true;
            this.FactSquareCol.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FactSquareCol.Visible = true;
            this.FactSquareCol.Width = 119;
            // 
            // colFactSquareCalc
            // 
            this.colFactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFactSquareCalc.Caption = "���������� (�� �������) ��������, ��";
            this.colFactSquareCalc.FieldName = "FactSquareCalc";
            this.colFactSquareCalc.Name = "colFactSquareCalc";
            this.colFactSquareCalc.OptionsColumn.AllowEdit = false;
            this.colFactSquareCalc.OptionsColumn.AllowFocus = false;
            this.colFactSquareCalc.OptionsColumn.ReadOnly = true;
            this.colFactSquareCalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFactSquareCalc.Visible = true;
            this.colFactSquareCalc.Width = 135;
            // 
            // colFactSquareCalcOverlap
            // 
            this.colFactSquareCalcOverlap.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactSquareCalcOverlap.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalcOverlap.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFactSquareCalcOverlap.Caption = "���������� (�� �������) �����, ��";
            this.colFactSquareCalcOverlap.FieldName = "FactSquareCalcOverlap";
            this.colFactSquareCalcOverlap.Name = "colFactSquareCalcOverlap";
            this.colFactSquareCalcOverlap.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFactSquareCalcOverlap.Visible = true;
            this.colFactSquareCalcOverlap.Width = 137;
            // 
            // PathWithoutWork
            // 
            this.PathWithoutWork.AppearanceHeader.Options.UseTextOptions = true;
            this.PathWithoutWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PathWithoutWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PathWithoutWork.Caption = "��������, ��";
            this.PathWithoutWork.FieldName = "PathWithoutWork";
            this.PathWithoutWork.Name = "PathWithoutWork";
            this.PathWithoutWork.OptionsColumn.AllowEdit = false;
            this.PathWithoutWork.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PathWithoutWork", " {0:N2}")});
            this.PathWithoutWork.Visible = true;
            this.PathWithoutWork.Width = 100;
            // 
            // CommentOrder
            // 
            this.CommentOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.CommentOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CommentOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.CommentOrder.Caption = "�����������";
            this.CommentOrder.FieldName = "Comment";
            this.CommentOrder.Name = "CommentOrder";
            this.CommentOrder.OptionsColumn.AllowEdit = false;
            this.CommentOrder.Visible = true;
            this.CommentOrder.Width = 100;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand16.AppearanceHeader.Options.UseFont = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "��������� ��������";
            this.gridBand16.Columns.Add(this.LocationStart);
            this.gridBand16.Columns.Add(this.LocationEnd);
            this.gridBand16.Columns.Add(this.Distance);
            this.gridBand16.Columns.Add(this.SpeedAvg);
            this.gridBand16.MinWidth = 20;
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 1;
            this.gridBand16.Width = 400;
            // 
            // LocationStart
            // 
            this.LocationStart.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LocationStart.Caption = "����� ������ ��������";
            this.LocationStart.FieldName = "LocationStart";
            this.LocationStart.Name = "LocationStart";
            this.LocationStart.OptionsColumn.AllowEdit = false;
            this.LocationStart.OptionsColumn.ReadOnly = true;
            this.LocationStart.Visible = true;
            this.LocationStart.Width = 100;
            // 
            // LocationEnd
            // 
            this.LocationEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LocationEnd.Caption = "����� ��������� ��������";
            this.LocationEnd.FieldName = "LocationEnd";
            this.LocationEnd.Name = "LocationEnd";
            this.LocationEnd.OptionsColumn.AllowEdit = false;
            this.LocationEnd.OptionsColumn.ReadOnly = true;
            this.LocationEnd.Visible = true;
            this.LocationEnd.Width = 100;
            // 
            // Distance
            // 
            this.Distance.AppearanceHeader.Options.UseTextOptions = true;
            this.Distance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Distance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Distance.Caption = "���������� ����, ��";
            this.Distance.FieldName = "Distance";
            this.Distance.Name = "Distance";
            this.Distance.OptionsColumn.AllowEdit = false;
            this.Distance.OptionsColumn.ReadOnly = true;
            this.Distance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.Distance.Visible = true;
            this.Distance.Width = 100;
            // 
            // SpeedAvg
            // 
            this.SpeedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvg.Caption = "������� ��������, ��/�";
            this.SpeedAvg.FieldName = "SpeedAvg";
            this.SpeedAvg.Name = "SpeedAvg";
            this.SpeedAvg.OptionsColumn.AllowEdit = false;
            this.SpeedAvg.OptionsColumn.ReadOnly = true;
            this.SpeedAvg.Visible = true;
            this.SpeedAvg.Width = 100;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand17.AppearanceHeader.Options.UseFont = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "��������� �������";
            this.gridBand17.Columns.Add(this.TimeWork);
            this.gridBand17.Columns.Add(this.TimeStart);
            this.gridBand17.Columns.Add(this.TimeEnd);
            this.gridBand17.Columns.Add(this.TimeMove);
            this.gridBand17.MinWidth = 20;
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 2;
            this.gridBand17.Width = 400;
            // 
            // TimeWork
            // 
            this.TimeWork.AppearanceCell.Options.UseTextOptions = true;
            this.TimeWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeWork.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeWork.Caption = "����������������� �����, �";
            this.TimeWork.DisplayFormat.FormatString = "t";
            this.TimeWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeWork.FieldName = "TimeWork";
            this.TimeWork.Name = "TimeWork";
            this.TimeWork.OptionsColumn.AllowEdit = false;
            this.TimeWork.OptionsColumn.ReadOnly = true;
            this.TimeWork.Visible = true;
            this.TimeWork.Width = 100;
            // 
            // TimeStart
            // 
            this.TimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStart.Caption = "����� ������ ��������";
            this.TimeStart.DisplayFormat.FormatString = "t";
            this.TimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStart.FieldName = "TimeStart";
            this.TimeStart.Name = "TimeStart";
            this.TimeStart.OptionsColumn.AllowEdit = false;
            this.TimeStart.OptionsColumn.ReadOnly = true;
            this.TimeStart.Visible = true;
            this.TimeStart.Width = 100;
            // 
            // TimeEnd
            // 
            this.TimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEnd.Caption = "����� ��������� ��������";
            this.TimeEnd.DisplayFormat.FormatString = "t";
            this.TimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEnd.FieldName = "TimeEnd";
            this.TimeEnd.Name = "TimeEnd";
            this.TimeEnd.OptionsColumn.AllowEdit = false;
            this.TimeEnd.OptionsColumn.ReadOnly = true;
            this.TimeEnd.Visible = true;
            this.TimeEnd.Width = 100;
            // 
            // TimeMove
            // 
            this.TimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMove.Caption = "����� ����� ��������, �";
            this.TimeMove.DisplayFormat.FormatString = "t";
            this.TimeMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeMove.FieldName = "TimeMove";
            this.TimeMove.Name = "TimeMove";
            this.TimeMove.OptionsColumn.AllowEdit = false;
            this.TimeMove.OptionsColumn.ReadOnly = true;
            this.TimeMove.Visible = true;
            this.TimeMove.Width = 100;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand18.AppearanceHeader.Options.UseFont = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "�������, ���";
            this.gridBand18.Columns.Add(this.FuelDUTExpensSquare);
            this.gridBand18.Columns.Add(this.FuelDUTExpensAvgSquare);
            this.gridBand18.Columns.Add(this.FuelDUTExpensWithoutWork);
            this.gridBand18.Columns.Add(this.FuelDUTExpensAvgWithoutWork);
            this.gridBand18.MinWidth = 20;
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 3;
            this.gridBand18.Width = 400;
            // 
            // FuelDUTExpensSquare
            // 
            this.FuelDUTExpensSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDUTExpensSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDUTExpensSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDUTExpensSquare.Caption = "������ � �����,  �����";
            this.FuelDUTExpensSquare.FieldName = "FuelDUTExpensSquare";
            this.FuelDUTExpensSquare.Name = "FuelDUTExpensSquare";
            this.FuelDUTExpensSquare.OptionsColumn.AllowEdit = false;
            this.FuelDUTExpensSquare.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDUTExpensSquare.Visible = true;
            this.FuelDUTExpensSquare.Width = 100;
            // 
            // FuelDUTExpensAvgSquare
            // 
            this.FuelDUTExpensAvgSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDUTExpensAvgSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDUTExpensAvgSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDUTExpensAvgSquare.Caption = "������ � �����, �/��";
            this.FuelDUTExpensAvgSquare.FieldName = "FuelDUTExpensAvgSquare";
            this.FuelDUTExpensAvgSquare.Name = "FuelDUTExpensAvgSquare";
            this.FuelDUTExpensAvgSquare.OptionsColumn.AllowEdit = false;
            this.FuelDUTExpensAvgSquare.Visible = true;
            this.FuelDUTExpensAvgSquare.Width = 100;
            // 
            // FuelDUTExpensWithoutWork
            // 
            this.FuelDUTExpensWithoutWork.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDUTExpensWithoutWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDUTExpensWithoutWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDUTExpensWithoutWork.Caption = "������ �� ���������, �����";
            this.FuelDUTExpensWithoutWork.FieldName = "FuelDUTExpensWithoutWork";
            this.FuelDUTExpensWithoutWork.Name = "FuelDUTExpensWithoutWork";
            this.FuelDUTExpensWithoutWork.OptionsColumn.AllowEdit = false;
            this.FuelDUTExpensWithoutWork.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDUTExpensWithoutWork.Visible = true;
            this.FuelDUTExpensWithoutWork.Width = 100;
            // 
            // FuelDUTExpensAvgWithoutWork
            // 
            this.FuelDUTExpensAvgWithoutWork.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDUTExpensAvgWithoutWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDUTExpensAvgWithoutWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDUTExpensAvgWithoutWork.Caption = "������ �� ���������, �/100��";
            this.FuelDUTExpensAvgWithoutWork.FieldName = "FuelDUTExpensAvgWithoutWork";
            this.FuelDUTExpensAvgWithoutWork.Name = "FuelDUTExpensAvgWithoutWork";
            this.FuelDUTExpensAvgWithoutWork.OptionsColumn.AllowEdit = false;
            this.FuelDUTExpensAvgWithoutWork.Visible = true;
            this.FuelDUTExpensAvgWithoutWork.Width = 100;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "�������, ���";
            this.gridBand19.Columns.Add(this.FuelDRTExpensSquare);
            this.gridBand19.Columns.Add(this.FuelDRTExpensAvgSquare);
            this.gridBand19.Columns.Add(this.FuelDRTExpensWithoutWork);
            this.gridBand19.Columns.Add(this.FuelDRTExpensAvgWithoutWork);
            this.gridBand19.MinWidth = 20;
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 4;
            this.gridBand19.Width = 400;
            // 
            // FuelDRTExpensSquare
            // 
            this.FuelDRTExpensSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensSquare.Caption = "������ � �����,  �����";
            this.FuelDRTExpensSquare.FieldName = "FuelDRTExpensSquare";
            this.FuelDRTExpensSquare.Name = "FuelDRTExpensSquare";
            this.FuelDRTExpensSquare.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensSquare.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensSquare.Visible = true;
            this.FuelDRTExpensSquare.Width = 100;
            // 
            // FuelDRTExpensAvgSquare
            // 
            this.FuelDRTExpensAvgSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgSquare.Caption = "������ � �����, �/��";
            this.FuelDRTExpensAvgSquare.FieldName = "FuelDRTExpensAvgSquare";
            this.FuelDRTExpensAvgSquare.Name = "FuelDRTExpensAvgSquare";
            this.FuelDRTExpensAvgSquare.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensAvgSquare.Visible = true;
            this.FuelDRTExpensAvgSquare.Width = 100;
            // 
            // FuelDRTExpensWithoutWork
            // 
            this.FuelDRTExpensWithoutWork.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensWithoutWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensWithoutWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensWithoutWork.Caption = "������ �� ���������, �����";
            this.FuelDRTExpensWithoutWork.FieldName = "FuelDRTExpensWithoutWork";
            this.FuelDRTExpensWithoutWork.Name = "FuelDRTExpensWithoutWork";
            this.FuelDRTExpensWithoutWork.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensWithoutWork.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensWithoutWork.Visible = true;
            this.FuelDRTExpensWithoutWork.Width = 100;
            // 
            // FuelDRTExpensAvgWithoutWork
            // 
            this.FuelDRTExpensAvgWithoutWork.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgWithoutWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgWithoutWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgWithoutWork.Caption = "������ �� ���������, �/100��";
            this.FuelDRTExpensAvgWithoutWork.FieldName = "FuelDRTExpensAvgWithoutWork";
            this.FuelDRTExpensAvgWithoutWork.Name = "FuelDRTExpensAvgWithoutWork";
            this.FuelDRTExpensAvgWithoutWork.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensAvgWithoutWork.Visible = true;
            this.FuelDRTExpensAvgWithoutWork.Width = 100;
            // 
            // gbQuontity
            // 
            this.gbQuontity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gbQuontity.AppearanceHeader.Options.UseFont = true;
            this.gbQuontity.AppearanceHeader.Options.UseTextOptions = true;
            this.gbQuontity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbQuontity.Caption = "�������� ������";
            this.gbQuontity.Columns.Add(this.bcolValidity);
            this.gbQuontity.Columns.Add(this.bcolPointsIntervalMax);
            this.gbQuontity.MinWidth = 20;
            this.gbQuontity.Name = "gbQuontity";
            this.gbQuontity.VisibleIndex = 5;
            this.gbQuontity.Width = 150;
            // 
            // bcolValidity
            // 
            this.bcolValidity.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolValidity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolValidity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bcolValidity.Caption = "������� ������";
            this.bcolValidity.ColumnEdit = this.pbValidity;
            this.bcolValidity.FieldName = "PointsValidity";
            this.bcolValidity.Name = "bcolValidity";
            this.bcolValidity.OptionsColumn.AllowEdit = false;
            this.bcolValidity.OptionsColumn.AllowFocus = false;
            this.bcolValidity.OptionsColumn.ReadOnly = true;
            this.bcolValidity.Visible = true;
            // 
            // pbValidity
            // 
            this.pbValidity.Name = "pbValidity";
            this.pbValidity.ShowTitle = true;
            // 
            // bcolPointsIntervalMax
            // 
            this.bcolPointsIntervalMax.AppearanceCell.Options.UseTextOptions = true;
            this.bcolPointsIntervalMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolPointsIntervalMax.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolPointsIntervalMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolPointsIntervalMax.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bcolPointsIntervalMax.Caption = "MAX �������� ����� �������";
            this.bcolPointsIntervalMax.FieldName = "PointsIntervalMax";
            this.bcolPointsIntervalMax.Name = "bcolPointsIntervalMax";
            this.bcolPointsIntervalMax.OptionsColumn.AllowEdit = false;
            this.bcolPointsIntervalMax.OptionsColumn.AllowFocus = false;
            this.bcolPointsIntervalMax.OptionsColumn.ReadOnly = true;
            this.bcolPointsIntervalMax.Visible = true;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gvbFuel
            // 
            this.gvbFuel.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gvbFuel.ColumnPanelRowHeight = 60;
            this.gvbFuel.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.IdF,
            this.FName,
            this.FamilyF,
            this.FuelStart,
            this.FuelAdd,
            this.FuelSub,
            this.FuelEnd,
            this.FuelExpens,
            this.FuelExpensAvg,
            this.Fuel_ExpensMove,
            this.Fuel_ExpensStop,
            this.Fuel_ExpensTotal,
            this.Fuel_ExpensAvg,
            this.Fuel_ExpensAvgRate});
            this.gvbFuel.GridControl = this.gcOrder;
            this.gvbFuel.Name = "gvbFuel";
            this.gvbFuel.OptionsBehavior.Editable = false;
            this.gvbFuel.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "����";
            this.gridBand6.Columns.Add(this.FName);
            this.gridBand6.Columns.Add(this.FamilyF);
            this.gridBand6.Columns.Add(this.IdF);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 0;
            this.gridBand6.Width = 200;
            // 
            // FName
            // 
            this.FName.AppearanceHeader.Options.UseTextOptions = true;
            this.FName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FName.Caption = "����";
            this.FName.FieldName = "FName";
            this.FName.Name = "FName";
            this.FName.Visible = true;
            this.FName.Width = 100;
            // 
            // FamilyF
            // 
            this.FamilyF.AppearanceHeader.Options.UseTextOptions = true;
            this.FamilyF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FamilyF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FamilyF.Caption = "��������";
            this.FamilyF.FieldName = "FamilyF";
            this.FamilyF.Name = "FamilyF";
            this.FamilyF.Visible = true;
            this.FamilyF.Width = 100;
            // 
            // IdF
            // 
            this.IdF.AppearanceHeader.Options.UseTextOptions = true;
            this.IdF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IdF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IdF.Caption = "IdF";
            this.IdF.FieldName = "IdF";
            this.IdF.Name = "IdF";
            this.IdF.Width = 20;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand7.Caption = "������ ������ ������� (���)";
            this.gridBand7.Columns.Add(this.FuelStart);
            this.gridBand7.Columns.Add(this.FuelAdd);
            this.gridBand7.Columns.Add(this.FuelSub);
            this.gridBand7.Columns.Add(this.FuelEnd);
            this.gridBand7.Columns.Add(this.FuelExpens);
            this.gridBand7.Columns.Add(this.FuelExpensAvg);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 1;
            this.gridBand7.Width = 478;
            // 
            // FuelStart
            // 
            this.FuelStart.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelStart.Caption = "������� � ������, �";
            this.FuelStart.FieldName = "FuelStart";
            this.FuelStart.Name = "FuelStart";
            this.FuelStart.Visible = true;
            this.FuelStart.Width = 77;
            // 
            // FuelAdd
            // 
            this.FuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAdd.Caption = "����������, �";
            this.FuelAdd.FieldName = "FuelAdd";
            this.FuelAdd.Name = "FuelAdd";
            this.FuelAdd.Visible = true;
            this.FuelAdd.Width = 77;
            // 
            // FuelSub
            // 
            this.FuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSub.Caption = "�����, �";
            this.FuelSub.FieldName = "FuelSub";
            this.FuelSub.Name = "FuelSub";
            this.FuelSub.Visible = true;
            this.FuelSub.Width = 77;
            // 
            // FuelEnd
            // 
            this.FuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEnd.Caption = "������� � �����, �";
            this.FuelEnd.FieldName = "FuelEnd";
            this.FuelEnd.Name = "FuelEnd";
            this.FuelEnd.Visible = true;
            this.FuelEnd.Width = 77;
            // 
            // FuelExpens
            // 
            this.FuelExpens.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpens.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpens.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpens.Caption = "����� ������, �";
            this.FuelExpens.FieldName = "FuelExpens";
            this.FuelExpens.Name = "FuelExpens";
            this.FuelExpens.Visible = true;
            this.FuelExpens.Width = 77;
            // 
            // FuelExpensAvg
            // 
            this.FuelExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvg.Caption = "�������  ������, �/��";
            this.FuelExpensAvg.FieldName = "FuelExpensAvg";
            this.FuelExpensAvg.Name = "FuelExpensAvg";
            this.FuelExpensAvg.Visible = true;
            this.FuelExpensAvg.Width = 93;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand8.Caption = "������ ������� ������� (���/CAN)";
            this.gridBand8.Columns.Add(this.Fuel_ExpensMove);
            this.gridBand8.Columns.Add(this.Fuel_ExpensStop);
            this.gridBand8.Columns.Add(this.Fuel_ExpensTotal);
            this.gridBand8.Columns.Add(this.Fuel_ExpensAvg);
            this.gridBand8.Columns.Add(this.Fuel_ExpensAvgRate);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 2;
            this.gridBand8.Width = 411;
            // 
            // Fuel_ExpensMove
            // 
            this.Fuel_ExpensMove.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMove.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMove.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Name = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Visible = true;
            this.Fuel_ExpensMove.Width = 79;
            // 
            // Fuel_ExpensStop
            // 
            this.Fuel_ExpensStop.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStop.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStop.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Name = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Visible = true;
            this.Fuel_ExpensStop.Width = 79;
            // 
            // Fuel_ExpensTotal
            // 
            this.Fuel_ExpensTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotal.Caption = "����� ������, �";
            this.Fuel_ExpensTotal.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Name = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Visible = true;
            this.Fuel_ExpensTotal.Width = 79;
            // 
            // Fuel_ExpensAvg
            // 
            this.Fuel_ExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvg.Caption = "������� ������, �/��";
            this.Fuel_ExpensAvg.FieldName = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Name = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Visible = true;
            this.Fuel_ExpensAvg.Width = 79;
            // 
            // Fuel_ExpensAvgRate
            // 
            this.Fuel_ExpensAvgRate.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvgRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvgRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvgRate.Caption = "������� ������, �/�������";
            this.Fuel_ExpensAvgRate.FieldName = "Fuel_ExpensAvgRate";
            this.Fuel_ExpensAvgRate.Name = "Fuel_ExpensAvgRate";
            this.Fuel_ExpensAvgRate.Visible = true;
            this.Fuel_ExpensAvgRate.Width = 95;
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridView1.GridControl = this.gcOrder;
            this.bandedGridView1.Name = "bandedGridView1";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1,
            this.bar4});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.pbStatus,
            this.lbStatus,
            this.sbtAdd,
            this.sbtDelete,
            this.sbtEdit,
            this.sbtRefresh,
            this.sbtExcel,
            this.bleGroupe,
            this.bleMobitelGrn,
            this.btOrdersCreate,
            this.teTimeCreate,
            this.chTimeCreate,
            this.btStart,
            this.bdeStart,
            this.bdeEnd,
            this.btFilterClear,
            this.btStartReport,
            this.bdeStartReport,
            this.bdeEndReport,
            this.bsiSettings,
            this.btColumnsSet,
            this.btColumnsSave,
            this.btColumnRestore,
            this.bsiFact,
            this.btFactControl,
            this.btFact,
            this.bdeStartReportAgr,
            this.bdeEndReportAgr,
            this.btStartReportAgr,
            this.btExcelImport,
            this.btSettings,
            this.bsiNumber,
            this.beiNumber,
            this.bsiActiveReport,
            this.btnStopExecutingData,
            this.bleCategory,
            this.btXmlExport});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 64;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpbStatus,
            this.leGroupe,
            this.leMobitelGrn,
            this.repositoryItemTextEdit5,
            this.rchTimeCreate,
            this.repositoryItemTextEdit3,
            this.deStart,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit6,
            this.repositoryItemTextEdit7,
            this.deEnd,
            this.deStartReport,
            this.deEndReport,
            this.deStartReportAgr,
            this.deEndReportAgr,
            this.rbAnaliz,
            this.leAgregats,
            this.riteNumber,
            this.leCategory,
            this.leFieldGroups,
            this.leField});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btExcelImport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btXmlExport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // sbtAdd
            // 
            this.sbtAdd.Caption = "��������";
            this.sbtAdd.Glyph = global::Agro.Properties.ResourcesImages.Add_16;
            this.sbtAdd.Id = 2;
            this.sbtAdd.Name = "sbtAdd";
            this.sbtAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtAdd_ItemClick);
            // 
            // sbtDelete
            // 
            this.sbtDelete.Caption = "�������";
            this.sbtDelete.Glyph = global::Agro.Properties.ResourcesImages.Remove_16;
            this.sbtDelete.Id = 3;
            this.sbtDelete.Name = "sbtDelete";
            this.sbtDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtDelete_ItemClick);
            // 
            // sbtEdit
            // 
            this.sbtEdit.Caption = "�������������";
            this.sbtEdit.Glyph = global::Agro.Properties.ResourcesImages.Edit_P_02_18;
            this.sbtEdit.Id = 4;
            this.sbtEdit.Name = "sbtEdit";
            this.sbtEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtEdit_ItemClick);
            // 
            // sbtRefresh
            // 
            this.sbtRefresh.Caption = "��������";
            this.sbtRefresh.Glyph = global::Agro.Properties.ResourcesImages.ref_16;
            this.sbtRefresh.Id = 5;
            this.sbtRefresh.Name = "sbtRefresh";
            this.sbtRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtRefresh_ItemClick);
            // 
            // bsiFact
            // 
            this.bsiFact.Caption = "����";
            this.bsiFact.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFact.Glyph")));
            this.bsiFact.Id = 38;
            this.bsiFact.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btFactControl),
            new DevExpress.XtraBars.LinkPersistInfo(this.btFact)});
            this.bsiFact.Name = "bsiFact";
            // 
            // btFactControl
            // 
            this.btFactControl.Caption = "� ������ ����������� ������";
            this.btFactControl.Id = 39;
            this.btFactControl.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D1));
            this.btFactControl.Name = "btFactControl";
            this.btFactControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFactControl_ItemClick);
            // 
            // btFact
            // 
            this.btFact.Caption = "��� ����� ����������� ������";
            this.btFact.Id = 40;
            this.btFact.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D0));
            this.btFact.Name = "btFact";
            this.btFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFact_ItemClick);
            // 
            // sbtExcel
            // 
            this.sbtExcel.Caption = "������� � Excel";
            this.sbtExcel.Glyph = global::Agro.Properties.ResourcesImages.doc_excel_table;
            this.sbtExcel.Id = 6;
            this.sbtExcel.Name = "sbtExcel";
            this.sbtExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtExcel_ItemClick);
            // 
            // btExcelImport
            // 
            this.btExcelImport.Caption = "������ �� Excel";
            this.btExcelImport.Glyph = global::Agro.Properties.ResourcesImages.doc_excel_table;
            this.btExcelImport.Id = 52;
            this.btExcelImport.Name = "btExcelImport";
            this.btExcelImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcelImport_ItemClick);
            // 
            // btXmlExport
            // 
            this.btXmlExport.Caption = "������� � XML";
            this.btXmlExport.Glyph = ((System.Drawing.Image)(resources.GetObject("btXmlExport.Glyph")));
            this.btXmlExport.Id = 63;
            this.btXmlExport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btXmlExport.LargeGlyph")));
            this.btXmlExport.Name = "btXmlExport";
            this.btXmlExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btXmlExport_ItemClick);
            // 
            // bsiSettings
            // 
            this.bsiSettings.Caption = "��������� ������� ";
            this.bsiSettings.Glyph = global::Agro.Properties.ResourcesImages.clipboard__pencil;
            this.bsiSettings.Id = 32;
            this.bsiSettings.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnsSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnsSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnRestore)});
            this.bsiSettings.Name = "bsiSettings";
            // 
            // btColumnsSet
            // 
            this.btColumnsSet.Caption = "����� ��������";
            this.btColumnsSet.Glyph = global::Agro.Properties.ResourcesImages.clipboard_task1;
            this.btColumnsSet.Id = 33;
            this.btColumnsSet.Name = "btColumnsSet";
            this.btColumnsSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnsSet_ItemClick);
            // 
            // btColumnsSave
            // 
            this.btColumnsSave.Caption = "���������";
            this.btColumnsSave.Glyph = global::Agro.Properties.ResourcesImages.clipboard_sign_out;
            this.btColumnsSave.Id = 34;
            this.btColumnsSave.Name = "btColumnsSave";
            this.btColumnsSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnsSave_ItemClick);
            // 
            // btColumnRestore
            // 
            this.btColumnRestore.Caption = "������������";
            this.btColumnRestore.Glyph = global::Agro.Properties.ResourcesImages.clipboard_sign;
            this.btColumnRestore.Id = 35;
            this.btColumnRestore.Name = "btColumnRestore";
            this.btColumnRestore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnRestore_ItemClick);
            // 
            // btSettings
            // 
            this.btSettings.Caption = "���������";
            this.btSettings.Glyph = global::Agro.Properties.ResourcesImages.Settings;
            this.btSettings.Id = 54;
            this.btSettings.Name = "btSettings";
            this.btSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btSettings_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.pbStatus, "", false, true, true, 250),
            new DevExpress.XtraBars.LinkPersistInfo(this.lbStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnStopExecutingData)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // pbStatus
            // 
            this.pbStatus.Caption = "barEditItem1";
            this.pbStatus.Edit = this.rpbStatus;
            this.pbStatus.Id = 0;
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // rpbStatus
            // 
            this.rpbStatus.Name = "rpbStatus";
            // 
            // lbStatus
            // 
            this.lbStatus.Caption = "������";
            this.lbStatus.Id = 1;
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnStopExecutingData
            // 
            this.btnStopExecutingData.Glyph = ((System.Drawing.Image)(resources.GetObject("btnStopExecutingData.Glyph")));
            this.btnStopExecutingData.Hint = "���������� ����������";
            this.btnStopExecutingData.Id = 58;
            this.btnStopExecutingData.Name = "btnStopExecutingData";
            this.btnStopExecutingData.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bar1
            // 
            this.bar1.BarName = "Order Bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiNumber),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiNumber, "", false, true, true, 65),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeStart, "", false, true, true, 119),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEnd, "", false, true, true, 113),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bleGroupe, "", false, true, true, 138),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bleCategory, "", false, true, true, 113),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bleMobitelGrn, "", false, true, true, 136),
            new DevExpress.XtraBars.LinkPersistInfo(this.btFilterClear),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btOrdersCreate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.chTimeCreate, "", false, true, true, 20),
            new DevExpress.XtraBars.LinkPersistInfo(this.teTimeCreate)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Order bar";
            // 
            // btStart
            // 
            this.btStart.Caption = "����";
            this.btStart.Glyph = ((System.Drawing.Image)(resources.GetObject("btStart.Glyph")));
            this.btStart.Id = 20;
            this.btStart.Name = "btStart";
            this.btStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btStart_ItemClick);
            // 
            // bsiNumber
            // 
            this.bsiNumber.Caption = "�����";
            this.bsiNumber.Id = 55;
            this.bsiNumber.Name = "bsiNumber";
            this.bsiNumber.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // beiNumber
            // 
            this.beiNumber.Caption = "0";
            this.beiNumber.Edit = this.riteNumber;
            this.beiNumber.Id = 56;
            this.beiNumber.Name = "beiNumber";
            // 
            // riteNumber
            // 
            this.riteNumber.Appearance.Options.UseTextOptions = true;
            this.riteNumber.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.riteNumber.AutoHeight = false;
            this.riteNumber.Name = "riteNumber";
            // 
            // bdeStart
            // 
            this.bdeStart.Caption = "���� �";
            this.bdeStart.Edit = this.deStart;
            this.bdeStart.Id = 23;
            this.bdeStart.Name = "bdeStart";
            this.bdeStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deStart
            // 
            this.deStart.AutoHeight = false;
            this.deStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStart.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.DisplayFormat.FormatString = "g";
            this.deStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.EditFormat.FormatString = "g";
            this.deStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Mask.EditMask = "g";
            this.deStart.Name = "deStart";
            // 
            // bdeEnd
            // 
            this.bdeEnd.Caption = "��";
            this.bdeEnd.Edit = this.deEnd;
            this.bdeEnd.Id = 27;
            this.bdeEnd.Name = "bdeEnd";
            this.bdeEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deEnd
            // 
            this.deEnd.AutoHeight = false;
            this.deEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEnd.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.DisplayFormat.FormatString = "g";
            this.deEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.EditFormat.FormatString = "g";
            this.deEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Mask.EditMask = "g";
            this.deEnd.Name = "deEnd";
            // 
            // bleGroupe
            // 
            this.bleGroupe.Caption = "������ �����";
            this.bleGroupe.Edit = this.leGroupe;
            this.bleGroupe.Id = 14;
            this.bleGroupe.Name = "bleGroupe";
            this.bleGroupe.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleGroupe.EditValueChanged += new System.EventHandler(this.bleGroupe_EditValueChanged);
            // 
            // leGroupe
            // 
            this.leGroupe.AutoHeight = false;
            this.leGroupe.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.DisplayMember = "Name";
            this.leGroupe.DropDownRows = 20;
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.NullText = "";
            this.leGroupe.ValueMember = "Id";
            // 
            // bleCategory
            // 
            this.bleCategory.Caption = "���������";
            this.bleCategory.Edit = this.leCategory;
            this.bleCategory.Id = 59;
            this.bleCategory.Name = "bleCategory";
            this.bleCategory.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleCategory.EditValueChanged += new System.EventHandler(this.bleCategory_EditValueChanged);
            // 
            // leCategory
            // 
            this.leCategory.AutoHeight = false;
            this.leCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leCategory.DisplayMember = "Name";
            this.leCategory.Name = "leCategory";
            this.leCategory.NullText = "";
            this.leCategory.ValueMember = "Id";
            // 
            // bleMobitelGrn
            // 
            this.bleMobitelGrn.AutoFillWidth = true;
            this.bleMobitelGrn.Caption = "������";
            this.bleMobitelGrn.Edit = this.leMobitelGrn;
            this.bleMobitelGrn.Id = 16;
            this.bleMobitelGrn.Name = "bleMobitelGrn";
            this.bleMobitelGrn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // leMobitelGrn
            // 
            this.leMobitelGrn.AutoHeight = false;
            this.leMobitelGrn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitelGrn.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leMobitelGrn.DisplayMember = "Info";
            this.leMobitelGrn.DropDownRows = 20;
            this.leMobitelGrn.Name = "leMobitelGrn";
            this.leMobitelGrn.NullText = "";
            this.leMobitelGrn.ValueMember = "MobitelId";
            // 
            // btFilterClear
            // 
            this.btFilterClear.Glyph = ((System.Drawing.Image)(resources.GetObject("btFilterClear.Glyph")));
            this.btFilterClear.Id = 28;
            this.btFilterClear.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btFilterClear.LargeGlyph")));
            this.btFilterClear.Name = "btFilterClear";
            this.btFilterClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFilterClear_ItemClick);
            // 
            // btOrdersCreate
            // 
            this.btOrdersCreate.Caption = "������������ �������";
            this.btOrdersCreate.Glyph = global::Agro.Properties.ResourcesImages.connect_creating;
            this.btOrdersCreate.Id = 17;
            this.btOrdersCreate.Name = "btOrdersCreate";
            this.btOrdersCreate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btOrdersCreate_ItemClick);
            // 
            // chTimeCreate
            // 
            this.chTimeCreate.Caption = "�� �������";
            this.chTimeCreate.Edit = this.rchTimeCreate;
            this.chTimeCreate.Id = 19;
            this.chTimeCreate.Name = "chTimeCreate";
            this.chTimeCreate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.chTimeCreate.Width = 20;
            this.chTimeCreate.EditValueChanged += new System.EventHandler(this.chTimeCreate_EditValueChanged);
            // 
            // rchTimeCreate
            // 
            this.rchTimeCreate.AutoHeight = false;
            this.rchTimeCreate.Caption = "Check";
            this.rchTimeCreate.Name = "rchTimeCreate";
            this.rchTimeCreate.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.rchTimeCreate.ValueGrayed = false;
            // 
            // teTimeCreate
            // 
            this.teTimeCreate.Edit = this.repositoryItemTextEdit5;
            this.teTimeCreate.EditValue = "05:00";
            this.teTimeCreate.Enabled = false;
            this.teTimeCreate.Id = 18;
            this.teTimeCreate.Name = "teTimeCreate";
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEdit5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.repositoryItemTextEdit5.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit5.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit5.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.repositoryItemTextEdit5.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "90:00";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            this.repositoryItemTextEdit5.NullText = "05:00";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1176, 31);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // bar4
            // 
            this.bar4.BarName = "Report bar";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(810, 287);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btStartReport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeStartReport, "", false, true, true, 126),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEndReport, "", false, true, true, 136)});
            this.bar4.OptionsBar.AllowRename = true;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar4.Text = "Report bar";
            // 
            // btStartReport
            // 
            this.btStartReport.Caption = "����";
            this.btStartReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btStartReport.Glyph")));
            this.btStartReport.Id = 29;
            this.btStartReport.Name = "btStartReport";
            this.btStartReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btStartReport_ItemClick);
            // 
            // bdeStartReport
            // 
            this.bdeStartReport.Caption = "���� �";
            this.bdeStartReport.Edit = this.deStartReport;
            this.bdeStartReport.Id = 30;
            this.bdeStartReport.Name = "bdeStartReport";
            this.bdeStartReport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deStartReport
            // 
            this.deStartReport.AutoHeight = false;
            this.deStartReport.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartReport.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStartReport.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStartReport.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStartReport.DisplayFormat.FormatString = "g";
            this.deStartReport.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStartReport.EditFormat.FormatString = "g";
            this.deStartReport.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStartReport.Mask.EditMask = "g";
            this.deStartReport.Name = "deStartReport";
            // 
            // bdeEndReport
            // 
            this.bdeEndReport.Caption = "��";
            this.bdeEndReport.Edit = this.deEndReport;
            this.bdeEndReport.Id = 31;
            this.bdeEndReport.Name = "bdeEndReport";
            this.bdeEndReport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deEndReport
            // 
            this.deEndReport.AutoHeight = false;
            this.deEndReport.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndReport.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEndReport.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEndReport.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEndReport.DisplayFormat.FormatString = "g";
            this.deEndReport.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEndReport.EditFormat.FormatString = "g";
            this.deEndReport.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEndReport.Mask.EditMask = "g";
            this.deEndReport.Name = "deEndReport";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1176, 39);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1182, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 706);
            this.barDockControlBottom.Size = new System.Drawing.Size(1182, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 680);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1182, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 680);
            // 
            // bdeStartReportAgr
            // 
            this.bdeStartReportAgr.Edit = null;
            this.bdeStartReportAgr.Id = 49;
            this.bdeStartReportAgr.Name = "bdeStartReportAgr";
            // 
            // bdeEndReportAgr
            // 
            this.bdeEndReportAgr.Edit = null;
            this.bdeEndReportAgr.Id = 50;
            this.bdeEndReportAgr.Name = "bdeEndReportAgr";
            // 
            // btStartReportAgr
            // 
            this.btStartReportAgr.Id = 51;
            this.btStartReportAgr.Name = "btStartReportAgr";
            // 
            // bsiActiveReport
            // 
            this.bsiActiveReport.Caption = "�������� �����";
            this.bsiActiveReport.Id = 57;
            this.bsiActiveReport.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bsiActiveReport.ItemAppearance.Normal.Options.UseFont = true;
            this.bsiActiveReport.Name = "bsiActiveReport";
            this.bsiActiveReport.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // deStartReportAgr
            // 
            this.deStartReportAgr.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStartReportAgr.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStartReportAgr.Name = "deStartReportAgr";
            // 
            // deEndReportAgr
            // 
            this.deEndReportAgr.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEndReportAgr.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEndReportAgr.Name = "deEndReportAgr";
            // 
            // rbAnaliz
            // 
            this.rbAnaliz.Name = "rbAnaliz";
            // 
            // leAgregats
            // 
            this.leAgregats.Name = "leAgregats";
            // 
            // leFieldGroups
            // 
            this.leFieldGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leFieldGroups.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leFieldGroups.DisplayMember = "Name";
            this.leFieldGroups.Name = "leFieldGroups";
            this.leFieldGroups.NullText = "";
            this.leFieldGroups.ValueMember = "Id";
            // 
            // leField
            // 
            this.leField.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leField.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leField.DisplayMember = "Name";
            this.leField.Name = "leField";
            this.leField.NullText = "";
            this.leField.ValueMember = "Id";
            // 
            // xtbView
            // 
            this.xtbView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtbView.Location = new System.Drawing.Point(0, 26);
            this.xtbView.Name = "xtbView";
            this.xtbView.SelectedTabPage = this.xtraTabPage1;
            this.xtbView.Size = new System.Drawing.Size(1182, 680);
            this.xtbView.TabIndex = 4;
            this.xtbView.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7});
            this.xtbView.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtbView_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage1.Text = "�����������";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tlDictionary);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcDict);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1176, 652);
            this.splitContainerControl1.SplitterPosition = 273;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tlDictionary
            // 
            this.tlDictionary.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.tlDictionary.Appearance.Empty.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.EvenRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.tlDictionary.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedCell.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedCell.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.tlDictionary.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupButton.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HorzLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.OddRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.tlDictionary.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.tlDictionary.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.tlDictionary.Appearance.Preview.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Preview.Options.UseFont = true;
            this.tlDictionary.Appearance.Preview.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.Row.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Row.Options.UseForeColor = true;
            this.tlDictionary.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.tlDictionary.Appearance.TreeLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.VertLine.Options.UseBackColor = true;
            this.tlDictionary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.DicCode,
            this.DicName});
            this.tlDictionary.CustomizationFormBounds = new System.Drawing.Rectangle(60, 493, 208, 168);
            this.tlDictionary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlDictionary.Location = new System.Drawing.Point(0, 0);
            this.tlDictionary.Name = "tlDictionary";
            this.tlDictionary.BeginUnboundLoad();
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������ ����� �����"}, -1, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "���� �����"}, 0);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������ ��������� ������������"}, -1, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "�������� ������������"}, 2);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������ �����"}, -1, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "����"}, 4, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� ��������� �������"}, 5);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� ������� �������"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������"}, -1);
            this.tlDictionary.EndUnboundLoad();
            this.tlDictionary.OptionsBehavior.Editable = false;
            this.tlDictionary.OptionsBehavior.ExpandNodeOnDrag = false;
            this.tlDictionary.OptionsBehavior.PopulateServiceColumns = true;
            this.tlDictionary.OptionsView.EnableAppearanceEvenRow = true;
            this.tlDictionary.OptionsView.EnableAppearanceOddRow = true;
            this.tlDictionary.SelectImageList = this.imCollection;
            this.tlDictionary.Size = new System.Drawing.Size(273, 652);
            this.tlDictionary.TabIndex = 0;
            this.tlDictionary.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlDictionary_FocusedNodeChanged);
            // 
            // DicCode
            // 
            this.DicCode.Caption = "���";
            this.DicCode.FieldName = "���";
            this.DicCode.Name = "DicCode";
            // 
            // DicName
            // 
            this.DicName.AppearanceHeader.Options.UseTextOptions = true;
            this.DicName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DicName.Caption = "�������� �����������";
            this.DicName.FieldName = "�������� �����������";
            this.DicName.MinWidth = 87;
            this.DicName.Name = "DicName";
            this.DicName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.DicName.Visible = true;
            this.DicName.VisibleIndex = 0;
            this.DicName.Width = 87;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage2.Text = "������";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.xtbOrder);
            this.splitContainerControl2.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1176, 652);
            this.splitContainerControl2.SplitterPosition = 433;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // xtbOrder
            // 
            this.xtbOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtbOrder.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtbOrder.Location = new System.Drawing.Point(0, 31);
            this.xtbOrder.Name = "xtbOrder";
            this.xtbOrder.SelectedTabPage = this.xtbpOrderMain;
            this.xtbOrder.Size = new System.Drawing.Size(1176, 402);
            this.xtbOrder.TabIndex = 3;
            this.xtbOrder.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtbpOrderMain,
            this.xtbpOrderDetal});
            // 
            // xtbpOrderMain
            // 
            this.xtbpOrderMain.Controls.Add(this.gcOrder);
            this.xtbpOrderMain.Name = "xtbpOrderMain";
            this.xtbpOrderMain.Size = new System.Drawing.Size(1148, 396);
            this.xtbpOrderMain.Text = "�������� ������";
            // 
            // xtbpOrderDetal
            // 
            this.xtbpOrderDetal.Controls.Add(this.gcOrderContent);
            this.xtbpOrderDetal.Name = "xtbpOrderDetal";
            this.xtbpOrderDetal.Size = new System.Drawing.Size(1148, 427);
            this.xtbpOrderDetal.Text = "��������� ������";
            // 
            // gcOrderContent
            // 
            this.gcOrderContent.DataMember = "agro_field";
            this.gcOrderContent.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode12.RelationName = "Level1";
            this.gcOrderContent.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode12});
            this.gcOrderContent.Location = new System.Drawing.Point(0, 0);
            this.gcOrderContent.MainView = this.bgvOrderContent;
            this.gcOrderContent.Name = "gcOrderContent";
            this.gcOrderContent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit8,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemProgressBar1,
            this.repositoryItemImageComboBox1,
            this.rtePersent,
            this.rteTime});
            this.gcOrderContent.Size = new System.Drawing.Size(1148, 427);
            this.gcOrderContent.TabIndex = 2;
            this.gcOrderContent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvOrderContent,
            this.bandedGridView3,
            this.bandedGridView4,
            this.bandedGridView5});
            this.gcOrderContent.DoubleClick += new System.EventHandler(this.gcOrderContent_DoubleClick);
            // 
            // bgvOrderContent
            // 
            this.bgvOrderContent.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.Empty.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrderContent.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvOrderContent.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.EvenRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.EvenRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.EvenRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrderContent.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrderContent.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.bgvOrderContent.Appearance.FixedLine.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.bgvOrderContent.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.bgvOrderContent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrderContent.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvOrderContent.Appearance.GroupButton.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.bgvOrderContent.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.GroupRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.GroupRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrderContent.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrderContent.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.HorzLine.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrderContent.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrderContent.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.OddRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.OddRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.OddRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.bgvOrderContent.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.bgvOrderContent.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.bgvOrderContent.Appearance.Preview.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.Preview.Options.UseFont = true;
            this.bgvOrderContent.Appearance.Preview.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvOrderContent.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.Row.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.Row.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvOrderContent.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.bgvOrderContent.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvOrderContent.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvOrderContent.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.bgvOrderContent.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bgvOrderContent.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bgvOrderContent.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bgvOrderContent.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvOrderContent.Appearance.VertLine.Options.UseBackColor = true;
            this.bgvOrderContent.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13});
            this.bgvOrderContent.ColumnPanelRowHeight = 40;
            this.bgvOrderContent.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colMakeCar,
            this.colCarModel,
            this.colNumberPlate,
            this.colInvNumber,
            this.colAgrName,
            this.colFamily,
            this.colFieldGroupName,
            this.colFieldName,
            this.colOutLinkId,
            this.colWorkName,
            this.colFactSquareCalcContent,
            this.colPersSquare,
            this.colIdOrder,
            this.colComment,
            this.colTimeStart,
            this.colTimeEnd,
            this.colTimeWork,
            this.colTimeMove,
            this.colTimeRate,
            this.colDistance,
            this.colSpeedAvg,
            this.colFuelExpens,
            this.colFuelExpensAvg,
            this.colFuelExpensRun,
            this.colFuelExpensRunKm,
            this.FuelDRTExpensSquare_,
            this.FuelDRTExpensAvgSquare_,
            this.FuelDRTExpensWithoutWork_,
            this.FuelDRTExpensAvgWithoutWork_,
            this.FuelDRTExpensAvgRate_});
            this.bgvOrderContent.GridControl = this.gcOrderContent;
            this.bgvOrderContent.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquareCalc", this.colFactSquareCalcContent, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colDistance, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelExpens", this.colFuelExpens, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "FuelExpensAvg", this.colFuelExpensAvg, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelExpensRun", this.colFuelExpensRun, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelExpensRunKm", this.colFuelExpensRunKm, "{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeMove", this.colTimeMove, "")});
            this.bgvOrderContent.Name = "bgvOrderContent";
            this.bgvOrderContent.OptionsBehavior.Editable = false;
            this.bgvOrderContent.OptionsSelection.MultiSelect = true;
            this.bgvOrderContent.OptionsView.AllowHtmlDrawHeaders = true;
            this.bgvOrderContent.OptionsView.ColumnAutoWidth = false;
            this.bgvOrderContent.OptionsView.EnableAppearanceEvenRow = true;
            this.bgvOrderContent.OptionsView.EnableAppearanceOddRow = true;
            this.bgvOrderContent.OptionsView.ShowFooter = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "������ ��������";
            this.gridBand5.Columns.Add(this.colMakeCar);
            this.gridBand5.Columns.Add(this.colCarModel);
            this.gridBand5.Columns.Add(this.colNumberPlate);
            this.gridBand5.Columns.Add(this.colInvNumber);
            this.gridBand5.Columns.Add(this.colAgrName);
            this.gridBand5.Columns.Add(this.colFamily);
            this.gridBand5.MinWidth = 20;
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 0;
            this.gridBand5.Width = 524;
            // 
            // colMakeCar
            // 
            this.colMakeCar.AppearanceHeader.Options.UseTextOptions = true;
            this.colMakeCar.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMakeCar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMakeCar.Caption = "����� ������������� ��������";
            this.colMakeCar.FieldName = "MakeCar";
            this.colMakeCar.Name = "colMakeCar";
            this.colMakeCar.OptionsColumn.AllowEdit = false;
            this.colMakeCar.OptionsColumn.ReadOnly = true;
            this.colMakeCar.Visible = true;
            this.colMakeCar.Width = 149;
            // 
            // colCarModel
            // 
            this.colCarModel.AppearanceHeader.Options.UseTextOptions = true;
            this.colCarModel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarModel.Caption = "������";
            this.colCarModel.FieldName = "CarModel";
            this.colCarModel.Name = "colCarModel";
            this.colCarModel.OptionsColumn.AllowEdit = false;
            this.colCarModel.OptionsColumn.ReadOnly = true;
            this.colCarModel.Visible = true;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.Caption = "���.�����";
            this.colNumberPlate.FieldName = "NumberPlate";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.Visible = true;
            // 
            // colInvNumber
            // 
            this.colInvNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvNumber.Caption = "���.�����";
            this.colInvNumber.FieldName = "InvNumber";
            this.colInvNumber.Name = "colInvNumber";
            this.colInvNumber.OptionsColumn.AllowEdit = false;
            this.colInvNumber.OptionsColumn.ReadOnly = true;
            this.colInvNumber.Visible = true;
            // 
            // colAgrName
            // 
            this.colAgrName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgrName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgrName.Caption = "������";
            this.colAgrName.FieldName = "AgrName";
            this.colAgrName.Name = "colAgrName";
            this.colAgrName.OptionsColumn.AllowEdit = false;
            this.colAgrName.OptionsColumn.ReadOnly = true;
            this.colAgrName.Visible = true;
            // 
            // colFamily
            // 
            this.colFamily.AppearanceHeader.Options.UseTextOptions = true;
            this.colFamily.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFamily.Caption = "��������";
            this.colFamily.FieldName = "Family";
            this.colFamily.Name = "colFamily";
            this.colFamily.OptionsColumn.AllowEdit = false;
            this.colFamily.OptionsColumn.ReadOnly = true;
            this.colFamily.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "���������� ������";
            this.gridBand9.Columns.Add(this.colFieldGroupName);
            this.gridBand9.Columns.Add(this.colFieldName);
            this.gridBand9.Columns.Add(this.colOutLinkId);
            this.gridBand9.Columns.Add(this.colWorkName);
            this.gridBand9.Columns.Add(this.colFactSquareCalcContent);
            this.gridBand9.Columns.Add(this.colPersSquare);
            this.gridBand9.Columns.Add(this.colIdOrder);
            this.gridBand9.Columns.Add(this.colComment);
            this.gridBand9.MinWidth = 20;
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 1;
            this.gridBand9.Width = 828;
            // 
            // colFieldGroupName
            // 
            this.colFieldGroupName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldGroupName.Caption = "������ �����";
            this.colFieldGroupName.FieldName = "FieldGroupName";
            this.colFieldGroupName.Name = "colFieldGroupName";
            this.colFieldGroupName.OptionsColumn.AllowEdit = false;
            this.colFieldGroupName.OptionsColumn.ReadOnly = true;
            this.colFieldGroupName.Visible = true;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "�������� ����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.Width = 104;
            // 
            // colOutLinkId
            // 
            this.colOutLinkId.AppearanceCell.Options.UseTextOptions = true;
            this.colOutLinkId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutLinkId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutLinkId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutLinkId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutLinkId.Caption = "��� ������� ����";
            this.colOutLinkId.FieldName = "OutLinkId";
            this.colOutLinkId.Name = "colOutLinkId";
            this.colOutLinkId.OptionsColumn.AllowEdit = false;
            this.colOutLinkId.OptionsColumn.ReadOnly = true;
            this.colOutLinkId.Visible = true;
            this.colOutLinkId.Width = 76;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.Caption = "��� �����";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.Width = 121;
            // 
            // colFactSquareCalcContent
            // 
            this.colFactSquareCalcContent.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactSquareCalcContent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalcContent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFactSquareCalcContent.Caption = "���������� (�� �������) ��������, ��";
            this.colFactSquareCalcContent.FieldName = "FactSquareCalc";
            this.colFactSquareCalcContent.Name = "colFactSquareCalcContent";
            this.colFactSquareCalcContent.OptionsColumn.AllowEdit = false;
            this.colFactSquareCalcContent.OptionsColumn.AllowFocus = false;
            this.colFactSquareCalcContent.OptionsColumn.ReadOnly = true;
            this.colFactSquareCalcContent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFactSquareCalcContent.Visible = true;
            this.colFactSquareCalcContent.Width = 158;
            // 
            // colPersSquare
            // 
            this.colPersSquare.AppearanceCell.Options.UseTextOptions = true;
            this.colPersSquare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPersSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colPersSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPersSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPersSquare.Caption = "% �� ������� ����";
            this.colPersSquare.ColumnEdit = this.rtePersent;
            this.colPersSquare.FieldName = "PersSquare";
            this.colPersSquare.Name = "colPersSquare";
            this.colPersSquare.OptionsColumn.AllowEdit = false;
            this.colPersSquare.OptionsColumn.ReadOnly = true;
            this.colPersSquare.Visible = true;
            this.colPersSquare.Width = 94;
            // 
            // rtePersent
            // 
            this.rtePersent.AutoHeight = false;
            this.rtePersent.Mask.EditMask = "P";
            this.rtePersent.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.rtePersent.Mask.UseMaskAsDisplayFormat = true;
            this.rtePersent.Name = "rtePersent";
            // 
            // colIdOrder
            // 
            this.colIdOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdOrder.Caption = "����� ������";
            this.colIdOrder.FieldName = "Id";
            this.colIdOrder.MinWidth = 10;
            this.colIdOrder.Name = "colIdOrder";
            this.colIdOrder.OptionsColumn.AllowEdit = false;
            this.colIdOrder.OptionsColumn.ReadOnly = true;
            this.colIdOrder.Visible = true;
            this.colIdOrder.Width = 100;
            // 
            // colComment
            // 
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.Caption = "�����������";
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.Visible = true;
            this.colComment.Width = 100;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "��������� �������";
            this.gridBand10.Columns.Add(this.colTimeStart);
            this.gridBand10.Columns.Add(this.colTimeEnd);
            this.gridBand10.Columns.Add(this.colTimeWork);
            this.gridBand10.Columns.Add(this.colTimeMove);
            this.gridBand10.Columns.Add(this.colTimeRate);
            this.gridBand10.MinWidth = 20;
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 559;
            // 
            // colTimeStart
            // 
            this.colTimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStart.Caption = "����� ������ ��������";
            this.colTimeStart.DisplayFormat.FormatString = "dd.MM.yyyy (HH:mm)";
            this.colTimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStart.FieldName = "TimeStart";
            this.colTimeStart.Name = "colTimeStart";
            this.colTimeStart.OptionsColumn.AllowEdit = false;
            this.colTimeStart.OptionsColumn.ReadOnly = true;
            this.colTimeStart.Visible = true;
            this.colTimeStart.Width = 128;
            // 
            // colTimeEnd
            // 
            this.colTimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEnd.Caption = "����� ��������� ��������";
            this.colTimeEnd.DisplayFormat.FormatString = "dd.MM.yyyy (HH:mm)";
            this.colTimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEnd.FieldName = "TimeEnd";
            this.colTimeEnd.Name = "colTimeEnd";
            this.colTimeEnd.OptionsColumn.AllowEdit = false;
            this.colTimeEnd.OptionsColumn.ReadOnly = true;
            this.colTimeEnd.Visible = true;
            this.colTimeEnd.Width = 119;
            // 
            // colTimeWork
            // 
            this.colTimeWork.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWork.Caption = "����������������� �����, �";
            this.colTimeWork.DisplayFormat.FormatString = "t";
            this.colTimeWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeWork.FieldName = "FactTime";
            this.colTimeWork.Name = "colTimeWork";
            this.colTimeWork.OptionsColumn.AllowEdit = false;
            this.colTimeWork.OptionsColumn.ReadOnly = true;
            this.colTimeWork.Visible = true;
            this.colTimeWork.Width = 128;
            // 
            // colTimeMove
            // 
            this.colTimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMove.Caption = "����� ����� ��������, �";
            this.colTimeMove.DisplayFormat.FormatString = "t";
            this.colTimeMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeMove.FieldName = "TimeMove";
            this.colTimeMove.Name = "colTimeMove";
            this.colTimeMove.OptionsColumn.AllowEdit = false;
            this.colTimeMove.OptionsColumn.ReadOnly = true;
            this.colTimeMove.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeMove.Visible = true;
            this.colTimeMove.Width = 105;
            // 
            // colTimeRate
            // 
            this.colTimeRate.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeRate.Caption = "��������";
            this.colTimeRate.DisplayFormat.FormatString = "t";
            this.colTimeRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeRate.FieldName = "TimeRate";
            this.colTimeRate.Name = "colTimeRate";
            this.colTimeRate.OptionsColumn.AllowEdit = false;
            this.colTimeRate.OptionsColumn.ReadOnly = true;
            this.colTimeRate.Visible = true;
            this.colTimeRate.Width = 79;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "��������� ��������\t\t\t";
            this.gridBand11.Columns.Add(this.colDistance);
            this.gridBand11.Columns.Add(this.colSpeedAvg);
            this.gridBand11.MinWidth = 20;
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 3;
            this.gridBand11.Width = 200;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "���������� ����, ��";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDistance.Visible = true;
            this.colDistance.Width = 100;
            // 
            // colSpeedAvg
            // 
            this.colSpeedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedAvg.Caption = "������� ��������, ��/�";
            this.colSpeedAvg.FieldName = "SpeedAvg";
            this.colSpeedAvg.Name = "colSpeedAvg";
            this.colSpeedAvg.OptionsColumn.AllowEdit = false;
            this.colSpeedAvg.OptionsColumn.ReadOnly = true;
            this.colSpeedAvg.Visible = true;
            this.colSpeedAvg.Width = 100;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "�������, ���";
            this.gridBand12.Columns.Add(this.colFuelExpens);
            this.gridBand12.Columns.Add(this.colFuelExpensAvg);
            this.gridBand12.Columns.Add(this.colFuelExpensRun);
            this.gridBand12.Columns.Add(this.colFuelExpensRunKm);
            this.gridBand12.MinWidth = 20;
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 4;
            this.gridBand12.Width = 517;
            // 
            // colFuelExpens
            // 
            this.colFuelExpens.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpens.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpens.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpens.Caption = "������ � �����,  �����";
            this.colFuelExpens.FieldName = "FuelExpens";
            this.colFuelExpens.Name = "colFuelExpens";
            this.colFuelExpens.OptionsColumn.AllowEdit = false;
            this.colFuelExpens.OptionsColumn.ReadOnly = true;
            this.colFuelExpens.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpens.Visible = true;
            this.colFuelExpens.Width = 131;
            // 
            // colFuelExpensAvg
            // 
            this.colFuelExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpensAvg.Caption = "������ � �����, �/��";
            this.colFuelExpensAvg.FieldName = "FuelExpensAvg";
            this.colFuelExpensAvg.Name = "colFuelExpensAvg";
            this.colFuelExpensAvg.OptionsColumn.AllowEdit = false;
            this.colFuelExpensAvg.OptionsColumn.ReadOnly = true;
            this.colFuelExpensAvg.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpensAvg.Visible = true;
            this.colFuelExpensAvg.Width = 152;
            // 
            // colFuelExpensRun
            // 
            this.colFuelExpensRun.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpensRun.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpensRun.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpensRun.Caption = "������ �� ���������, �����";
            this.colFuelExpensRun.FieldName = "FuelExpensRun";
            this.colFuelExpensRun.Name = "colFuelExpensRun";
            this.colFuelExpensRun.OptionsColumn.AllowEdit = false;
            this.colFuelExpensRun.OptionsColumn.ReadOnly = true;
            this.colFuelExpensRun.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpensRun.Visible = true;
            this.colFuelExpensRun.Width = 109;
            // 
            // colFuelExpensRunKm
            // 
            this.colFuelExpensRunKm.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpensRunKm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpensRunKm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpensRunKm.Caption = "������ �� ���������, �/100��";
            this.colFuelExpensRunKm.DisplayFormat.FormatString = "N";
            this.colFuelExpensRunKm.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelExpensRunKm.FieldName = "FuelExpensRunKm";
            this.colFuelExpensRunKm.Name = "colFuelExpensRunKm";
            this.colFuelExpensRunKm.OptionsColumn.AllowEdit = false;
            this.colFuelExpensRunKm.OptionsColumn.ReadOnly = true;
            this.colFuelExpensRunKm.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelExpensRunKm", "{0:N2}")});
            this.colFuelExpensRunKm.Visible = true;
            this.colFuelExpensRunKm.Width = 125;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridBand13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridBand13.Caption = "�������, ���";
            this.gridBand13.Columns.Add(this.FuelDRTExpensSquare_);
            this.gridBand13.Columns.Add(this.FuelDRTExpensAvgSquare_);
            this.gridBand13.Columns.Add(this.FuelDRTExpensWithoutWork_);
            this.gridBand13.Columns.Add(this.FuelDRTExpensAvgWithoutWork_);
            this.gridBand13.Columns.Add(this.FuelDRTExpensAvgRate_);
            this.gridBand13.MinWidth = 20;
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 5;
            this.gridBand13.Width = 375;
            // 
            // FuelDRTExpensSquare_
            // 
            this.FuelDRTExpensSquare_.AppearanceCell.Options.UseTextOptions = true;
            this.FuelDRTExpensSquare_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensSquare_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensSquare_.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.FuelDRTExpensSquare_.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensSquare_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensSquare_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensSquare_.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensSquare_.Caption = "������ � �����, �����";
            this.FuelDRTExpensSquare_.FieldName = "Fuel_ExpensTotal";
            this.FuelDRTExpensSquare_.Name = "FuelDRTExpensSquare_";
            this.FuelDRTExpensSquare_.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensSquare_.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensSquare_.ToolTip = "������ � �����, �����";
            this.FuelDRTExpensSquare_.Visible = true;
            // 
            // FuelDRTExpensAvgSquare_
            // 
            this.FuelDRTExpensAvgSquare_.AppearanceCell.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgSquare_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgSquare_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgSquare_.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.FuelDRTExpensAvgSquare_.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgSquare_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgSquare_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgSquare_.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgSquare_.Caption = "������ � �����, �/��";
            this.FuelDRTExpensAvgSquare_.FieldName = "Fuel_ExpensAvg";
            this.FuelDRTExpensAvgSquare_.Name = "FuelDRTExpensAvgSquare_";
            this.FuelDRTExpensAvgSquare_.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensAvgSquare_.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensAvgSquare_.ToolTip = "������ � �����, �/��";
            this.FuelDRTExpensAvgSquare_.Visible = true;
            // 
            // FuelDRTExpensWithoutWork_
            // 
            this.FuelDRTExpensWithoutWork_.AppearanceCell.Options.UseTextOptions = true;
            this.FuelDRTExpensWithoutWork_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensWithoutWork_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensWithoutWork_.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.FuelDRTExpensWithoutWork_.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensWithoutWork_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensWithoutWork_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensWithoutWork_.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensWithoutWork_.Caption = "������ ������� � ��������, �";
            this.FuelDRTExpensWithoutWork_.FieldName = "Fuel_ExpensMove";
            this.FuelDRTExpensWithoutWork_.Name = "FuelDRTExpensWithoutWork_";
            this.FuelDRTExpensWithoutWork_.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensWithoutWork_.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensWithoutWork_.ToolTip = "������ ������� � ��������, �";
            this.FuelDRTExpensWithoutWork_.Visible = true;
            // 
            // FuelDRTExpensAvgWithoutWork_
            // 
            this.FuelDRTExpensAvgWithoutWork_.AppearanceCell.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgWithoutWork_.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgWithoutWork_.Caption = "������ ������� �� ��������, �";
            this.FuelDRTExpensAvgWithoutWork_.FieldName = "Fuel_ExpensStop";
            this.FuelDRTExpensAvgWithoutWork_.Name = "FuelDRTExpensAvgWithoutWork_";
            this.FuelDRTExpensAvgWithoutWork_.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensAvgWithoutWork_.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.FuelDRTExpensAvgWithoutWork_.ToolTip = "������ ������� �� ��������, �";
            this.FuelDRTExpensAvgWithoutWork_.Visible = true;
            // 
            // FuelDRTExpensAvgRate_
            // 
            this.FuelDRTExpensAvgRate_.AppearanceCell.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgRate_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgRate_.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgRate_.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgRate_.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelDRTExpensAvgRate_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelDRTExpensAvgRate_.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelDRTExpensAvgRate_.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelDRTExpensAvgRate_.Caption = "������� ������, �/�������";
            this.FuelDRTExpensAvgRate_.FieldName = "Fuel_ExpensAvgRate";
            this.FuelDRTExpensAvgRate_.Name = "FuelDRTExpensAvgRate_";
            this.FuelDRTExpensAvgRate_.OptionsColumn.AllowEdit = false;
            this.FuelDRTExpensAvgRate_.ToolTip = "������� ������, �/�������";
            this.FuelDRTExpensAvgRate_.Visible = true;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEdit1.DisplayMember = "Name";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ShowLines = false;
            this.repositoryItemLookUpEdit1.ValueMember = "Id";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ShowTitle = true;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("0", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1", 1, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2", 2, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3", 3, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4", 4, 5)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imCollection;
            // 
            // rteTime
            // 
            this.rteTime.AutoHeight = false;
            this.rteTime.Mask.EditMask = "hh:mm";
            this.rteTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom;
            this.rteTime.Mask.UseMaskAsDisplayFormat = true;
            this.rteTime.Name = "rteTime";
            // 
            // bandedGridView3
            // 
            this.bandedGridView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand14,
            this.gridBand20,
            this.gridBand21});
            this.bandedGridView3.ColumnPanelRowHeight = 60;
            this.bandedGridView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn31,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn38,
            this.bandedGridColumn39,
            this.bandedGridColumn40,
            this.bandedGridColumn41,
            this.bandedGridColumn42});
            this.bandedGridView3.GridControl = this.gcOrderContent;
            this.bandedGridView3.Name = "bandedGridView3";
            this.bandedGridView3.OptionsBehavior.Editable = false;
            this.bandedGridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "����";
            this.gridBand14.Columns.Add(this.bandedGridColumn29);
            this.gridBand14.Columns.Add(this.bandedGridColumn30);
            this.gridBand14.Columns.Add(this.bandedGridColumn31);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 0;
            this.gridBand14.Width = 200;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn29.Caption = "����";
            this.bandedGridColumn29.FieldName = "FName";
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 100;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn30.Caption = "��������";
            this.bandedGridColumn30.FieldName = "FamilyF";
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 100;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn31.Caption = "IdF";
            this.bandedGridColumn31.FieldName = "IdF";
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Width = 20;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand20.Caption = "������ ������ ������� (���)";
            this.gridBand20.Columns.Add(this.bandedGridColumn32);
            this.gridBand20.Columns.Add(this.bandedGridColumn33);
            this.gridBand20.Columns.Add(this.bandedGridColumn34);
            this.gridBand20.Columns.Add(this.bandedGridColumn35);
            this.gridBand20.Columns.Add(this.bandedGridColumn36);
            this.gridBand20.Columns.Add(this.bandedGridColumn37);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 1;
            this.gridBand20.Width = 478;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn32.Caption = "������� � ������, �";
            this.bandedGridColumn32.FieldName = "FuelStart";
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            this.bandedGridColumn32.Width = 77;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn33.Caption = "����������, �";
            this.bandedGridColumn33.FieldName = "FuelAdd";
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Visible = true;
            this.bandedGridColumn33.Width = 77;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn34.Caption = "�����, �";
            this.bandedGridColumn34.FieldName = "FuelSub";
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.Visible = true;
            this.bandedGridColumn34.Width = 77;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn35.Caption = "������� � �����, �";
            this.bandedGridColumn35.FieldName = "FuelEnd";
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.Visible = true;
            this.bandedGridColumn35.Width = 77;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn36.Caption = "����� ������, �";
            this.bandedGridColumn36.FieldName = "FuelExpens";
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.Visible = true;
            this.bandedGridColumn36.Width = 77;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn37.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn37.Caption = "�������  ������, �/��";
            this.bandedGridColumn37.FieldName = "FuelExpensAvg";
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.Visible = true;
            this.bandedGridColumn37.Width = 93;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand21.Caption = "������ ������� ������� (���/CAN)";
            this.gridBand21.Columns.Add(this.bandedGridColumn38);
            this.gridBand21.Columns.Add(this.bandedGridColumn39);
            this.gridBand21.Columns.Add(this.bandedGridColumn40);
            this.gridBand21.Columns.Add(this.bandedGridColumn41);
            this.gridBand21.Columns.Add(this.bandedGridColumn42);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 2;
            this.gridBand21.Width = 411;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn38.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn38.Caption = "������ ������� � ��������, �";
            this.bandedGridColumn38.FieldName = "Fuel_ExpensMove";
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.Visible = true;
            this.bandedGridColumn38.Width = 79;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn39.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn39.Caption = "������ ������� �� ��������, �";
            this.bandedGridColumn39.FieldName = "Fuel_ExpensStop";
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.Visible = true;
            this.bandedGridColumn39.Width = 79;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn40.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn40.Caption = "����� ������, �";
            this.bandedGridColumn40.FieldName = "Fuel_ExpensTotal";
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.Visible = true;
            this.bandedGridColumn40.Width = 79;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn41.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn41.Caption = "������� ������, �/��";
            this.bandedGridColumn41.FieldName = "Fuel_ExpensAvg";
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.Visible = true;
            this.bandedGridColumn41.Width = 79;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn42.Caption = "������� ������, �/�������";
            this.bandedGridColumn42.FieldName = "Fuel_ExpensAvgRate";
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.Visible = true;
            this.bandedGridColumn42.Width = 95;
            // 
            // bandedGridView4
            // 
            this.bandedGridView4.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand22});
            this.bandedGridView4.GridControl = this.gcOrderContent;
            this.bandedGridView4.Name = "bandedGridView4";
            // 
            // gridBand22
            // 
            this.gridBand22.Caption = "gridBand1";
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 0;
            // 
            // bandedGridView5
            // 
            this.bandedGridView5.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand23,
            this.gridBand24,
            this.gridBand25});
            this.bandedGridView5.ColumnPanelRowHeight = 60;
            this.bandedGridView5.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn43,
            this.bandedGridColumn44,
            this.bandedGridColumn45,
            this.bandedGridColumn46,
            this.bandedGridColumn47,
            this.bandedGridColumn48,
            this.bandedGridColumn49,
            this.bandedGridColumn50,
            this.bandedGridColumn51,
            this.bandedGridColumn52,
            this.bandedGridColumn53,
            this.bandedGridColumn54,
            this.bandedGridColumn55});
            this.bandedGridView5.GridControl = this.gcOrderContent;
            this.bandedGridView5.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.bandedGridView5.LevelIndent = 1;
            this.bandedGridView5.Name = "bandedGridView5";
            this.bandedGridView5.OptionsBehavior.Editable = false;
            this.bandedGridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand23.AppearanceHeader.Options.UseFont = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "����";
            this.gridBand23.Columns.Add(this.bandedGridColumn43);
            this.gridBand23.Columns.Add(this.bandedGridColumn44);
            this.gridBand23.Columns.Add(this.bandedGridColumn45);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 0;
            this.gridBand23.Width = 200;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn43.Caption = "Id";
            this.bandedGridColumn43.FieldName = "Id";
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.OptionsColumn.AllowEdit = false;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn44.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn44.Caption = "����";
            this.bandedGridColumn44.FieldName = "Name";
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn44.Visible = true;
            this.bandedGridColumn44.Width = 98;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn45.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn45.Caption = "��������";
            this.bandedGridColumn45.FieldName = "Family";
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn45.Visible = true;
            this.bandedGridColumn45.Width = 102;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand24.AppearanceHeader.Options.UseFont = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "�����";
            this.gridBand24.Columns.Add(this.bandedGridColumn46);
            this.gridBand24.Columns.Add(this.bandedGridColumn47);
            this.gridBand24.Columns.Add(this.bandedGridColumn48);
            this.gridBand24.Columns.Add(this.bandedGridColumn49);
            this.gridBand24.Columns.Add(this.bandedGridColumn50);
            this.gridBand24.Columns.Add(this.bandedGridColumn51);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 1;
            this.gridBand24.Width = 531;
            // 
            // bandedGridColumn46
            // 
            this.bandedGridColumn46.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn46.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn46.Caption = "�����";
            this.bandedGridColumn46.DisplayFormat.FormatString = "t";
            this.bandedGridColumn46.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn46.FieldName = "TimeStart";
            this.bandedGridColumn46.Name = "bandedGridColumn46";
            this.bandedGridColumn46.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn46.Visible = true;
            this.bandedGridColumn46.Width = 87;
            // 
            // bandedGridColumn47
            // 
            this.bandedGridColumn47.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn47.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn47.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn47.Caption = "�����";
            this.bandedGridColumn47.DisplayFormat.FormatString = "t";
            this.bandedGridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn47.FieldName = "TimeEnd";
            this.bandedGridColumn47.Name = "bandedGridColumn47";
            this.bandedGridColumn47.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn47.Visible = true;
            this.bandedGridColumn47.Width = 87;
            // 
            // bandedGridColumn48
            // 
            this.bandedGridColumn48.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn48.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn48.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn48.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn48.Caption = "����� �����, �";
            this.bandedGridColumn48.DisplayFormat.FormatString = "t";
            this.bandedGridColumn48.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn48.FieldName = "FactTime";
            this.bandedGridColumn48.Name = "bandedGridColumn48";
            this.bandedGridColumn48.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn48.Visible = true;
            this.bandedGridColumn48.Width = 87;
            // 
            // bandedGridColumn49
            // 
            this.bandedGridColumn49.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn49.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn49.Caption = "����� ��������, �";
            this.bandedGridColumn49.DisplayFormat.FormatString = "t";
            this.bandedGridColumn49.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn49.FieldName = "TimeMove";
            this.bandedGridColumn49.Name = "bandedGridColumn49";
            this.bandedGridColumn49.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn49.Visible = true;
            this.bandedGridColumn49.Width = 87;
            // 
            // bandedGridColumn50
            // 
            this.bandedGridColumn50.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn50.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn50.Caption = "����� �������, �";
            this.bandedGridColumn50.DisplayFormat.FormatString = "t";
            this.bandedGridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn50.FieldName = "TimeStop";
            this.bandedGridColumn50.Name = "bandedGridColumn50";
            this.bandedGridColumn50.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn50.Visible = true;
            this.bandedGridColumn50.Width = 87;
            // 
            // bandedGridColumn51
            // 
            this.bandedGridColumn51.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn51.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn51.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn51.Caption = "��������, �";
            this.bandedGridColumn51.DisplayFormat.FormatString = "t";
            this.bandedGridColumn51.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn51.FieldName = "TimeRate";
            this.bandedGridColumn51.Name = "bandedGridColumn51";
            this.bandedGridColumn51.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn51.Visible = true;
            this.bandedGridColumn51.Width = 96;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand25.AppearanceHeader.Options.UseFont = true;
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "������";
            this.gridBand25.Columns.Add(this.bandedGridColumn52);
            this.gridBand25.Columns.Add(this.bandedGridColumn53);
            this.gridBand25.Columns.Add(this.bandedGridColumn54);
            this.gridBand25.Columns.Add(this.bandedGridColumn55);
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 2;
            this.gridBand25.Width = 358;
            // 
            // bandedGridColumn52
            // 
            this.bandedGridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn52.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn52.Caption = "������";
            this.bandedGridColumn52.FieldName = "Distance";
            this.bandedGridColumn52.Name = "bandedGridColumn52";
            this.bandedGridColumn52.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn52.Visible = true;
            this.bandedGridColumn52.Width = 87;
            // 
            // bandedGridColumn53
            // 
            this.bandedGridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn53.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn53.Caption = "���������� (�� �������), ��";
            this.bandedGridColumn53.FieldName = "FactSquare";
            this.bandedGridColumn53.Name = "bandedGridColumn53";
            this.bandedGridColumn53.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn53.Visible = true;
            this.bandedGridColumn53.Width = 87;
            // 
            // bandedGridColumn54
            // 
            this.bandedGridColumn54.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn54.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn54.Caption = "���������� (�� �������), ��";
            this.bandedGridColumn54.FieldName = "FactSquareCalc";
            this.bandedGridColumn54.Name = "bandedGridColumn54";
            this.bandedGridColumn54.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn54.Visible = true;
            this.bandedGridColumn54.Width = 87;
            // 
            // bandedGridColumn55
            // 
            this.bandedGridColumn55.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn55.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn55.Caption = "������� ��������";
            this.bandedGridColumn55.FieldName = "SpeedAvg";
            this.bandedGridColumn55.Name = "bandedGridColumn55";
            this.bandedGridColumn55.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn55.Visible = true;
            this.bandedGridColumn55.Width = 97;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControl3.Panel1.Controls.Add(this.gcFieldsList);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "������ ����� ";
            this.splitContainerControl3.Panel2.Controls.Add(this.pnMap);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1176, 214);
            this.splitContainerControl3.SplitterPosition = 421;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gcFieldsList
            // 
            this.gcFieldsList.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gcFieldsList.AppearanceCaption.Options.UseFont = true;
            this.gcFieldsList.AppearanceCaption.Options.UseTextOptions = true;
            this.gcFieldsList.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcFieldsList.Controls.Add(this.gcFields);
            this.gcFieldsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFieldsList.Location = new System.Drawing.Point(0, 0);
            this.gcFieldsList.Name = "gcFieldsList";
            this.gcFieldsList.Size = new System.Drawing.Size(421, 214);
            this.gcFieldsList.TabIndex = 3;
            this.gcFieldsList.Text = "������ ���������������� �����";
            // 
            // gcFields
            // 
            this.gcFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFields.Location = new System.Drawing.Point(2, 22);
            this.gcFields.MainView = this.gvFieldsOrder;
            this.gcFields.Name = "gcFields";
            this.gcFields.Size = new System.Drawing.Size(417, 190);
            this.gcFields.TabIndex = 1;
            this.gcFields.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFieldsOrder});
            this.gcFields.Click += new System.EventHandler(this.gcFields_Click);
            // 
            // gvFieldsOrder
            // 
            this.gvFieldsOrder.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.Empty.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFieldsOrder.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFieldsOrder.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFieldsOrder.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFieldsOrder.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvFieldsOrder.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvFieldsOrder.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvFieldsOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFieldsOrder.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFieldsOrder.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvFieldsOrder.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFieldsOrder.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFieldsOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFieldsOrder.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFieldsOrder.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.OddRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvFieldsOrder.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvFieldsOrder.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvFieldsOrder.Appearance.Preview.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.Preview.Options.UseFont = true;
            this.gvFieldsOrder.Appearance.Preview.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFieldsOrder.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.Row.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.Row.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFieldsOrder.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvFieldsOrder.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFieldsOrder.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFieldsOrder.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvFieldsOrder.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvFieldsOrder.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvFieldsOrder.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvFieldsOrder.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFieldsOrder.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFieldsOrder.ColumnPanelRowHeight = 40;
            this.gvFieldsOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Zone_ID,
            this.FieldId,
            this.GrpName,
            this.FieldName,
            this.SquareF,
            this.ORDERS});
            this.gvFieldsOrder.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gvFieldsOrder.GridControl = this.gcFields;
            this.gvFieldsOrder.Name = "gvFieldsOrder";
            this.gvFieldsOrder.OptionsBehavior.Editable = false;
            this.gvFieldsOrder.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFieldsOrder.OptionsView.EnableAppearanceOddRow = true;
            this.gvFieldsOrder.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvFieldsOrder_FocusedRowChanged);
            // 
            // Zone_ID
            // 
            this.Zone_ID.Caption = "Zone_ID";
            this.Zone_ID.FieldName = "Zone_ID";
            this.Zone_ID.Name = "Zone_ID";
            this.Zone_ID.OptionsColumn.AllowEdit = false;
            // 
            // FieldId
            // 
            this.FieldId.Caption = "FieldId";
            this.FieldId.FieldName = "FieldId";
            this.FieldId.Name = "FieldId";
            this.FieldId.OptionsColumn.AllowEdit = false;
            // 
            // GrpName
            // 
            this.GrpName.AppearanceHeader.Options.UseTextOptions = true;
            this.GrpName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GrpName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GrpName.Caption = "������ �����";
            this.GrpName.FieldName = "GrpName";
            this.GrpName.Name = "GrpName";
            this.GrpName.OptionsColumn.AllowEdit = false;
            this.GrpName.Visible = true;
            this.GrpName.VisibleIndex = 0;
            this.GrpName.Width = 115;
            // 
            // FieldName
            // 
            this.FieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.FieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FieldName.Caption = "����";
            this.FieldName.FieldName = "FieldName";
            this.FieldName.Name = "FieldName";
            this.FieldName.OptionsColumn.AllowEdit = false;
            this.FieldName.Visible = true;
            this.FieldName.VisibleIndex = 1;
            this.FieldName.Width = 117;
            // 
            // SquareF
            // 
            this.SquareF.AppearanceHeader.Options.UseTextOptions = true;
            this.SquareF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SquareF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SquareF.Caption = "�������, ��";
            this.SquareF.FieldName = "Sq";
            this.SquareF.Name = "SquareF";
            this.SquareF.OptionsColumn.AllowEdit = false;
            this.SquareF.Visible = true;
            this.SquareF.VisibleIndex = 2;
            this.SquareF.Width = 114;
            // 
            // ORDERS
            // 
            this.ORDERS.AppearanceHeader.Options.UseTextOptions = true;
            this.ORDERS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ORDERS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ORDERS.Caption = "������� �� ����������";
            this.ORDERS.FieldName = "ORDERS";
            this.ORDERS.Name = "ORDERS";
            this.ORDERS.OptionsColumn.AllowEdit = false;
            this.ORDERS.Visible = true;
            this.ORDERS.VisibleIndex = 3;
            this.ORDERS.Width = 116;
            // 
            // pnMap
            // 
            this.pnMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMap.Location = new System.Drawing.Point(0, 0);
            this.pnMap.Name = "pnMap";
            this.pnMap.Size = new System.Drawing.Size(750, 214);
            this.pnMap.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage3.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage3.Text = "������";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 39);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.nbReports);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1176, 613);
            this.splitContainerControl4.SplitterPosition = 144;
            this.splitContainerControl4.TabIndex = 30;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // nbReports
            // 
            this.nbReports.ActiveGroup = this.nbgReports;
            this.nbReports.ContentButtonHint = null;
            this.nbReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbReports.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgReports});
            this.nbReports.Location = new System.Drawing.Point(0, 0);
            this.nbReports.Name = "nbReports";
            this.nbReports.OptionsNavPane.ExpandedWidth = 144;
            this.nbReports.Size = new System.Drawing.Size(144, 613);
            this.nbReports.TabIndex = 0;
            this.nbReports.Text = "������";
            // 
            // nbgReports
            // 
            this.nbgReports.Appearance.Options.UseTextOptions = true;
            this.nbgReports.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nbgReports.Caption = "������";
            this.nbgReports.Expanded = true;
            this.nbgReports.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.nbgReports.Name = "nbgReports";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.pnGraficAgr);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage4.Text = "������������� ������������";
            // 
            // pnGraficAgr
            // 
            this.pnGraficAgr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGraficAgr.Location = new System.Drawing.Point(0, 0);
            this.pnGraficAgr.Name = "pnGraficAgr";
            this.pnGraficAgr.Size = new System.Drawing.Size(1176, 652);
            this.pnGraficAgr.TabIndex = 0;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.pnGraficDrv);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage5.Text = "������������� ���������";
            // 
            // pnGraficDrv
            // 
            this.pnGraficDrv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGraficDrv.Location = new System.Drawing.Point(0, 0);
            this.pnGraficDrv.Name = "pnGraficDrv";
            this.pnGraficDrv.Size = new System.Drawing.Size(1176, 652);
            this.pnGraficDrv.TabIndex = 1;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.pnPlan);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage6.Text = "�������� �������";
            // 
            // pnPlan
            // 
            this.pnPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnPlan.Location = new System.Drawing.Point(0, 0);
            this.pnPlan.Name = "pnPlan";
            this.pnPlan.Size = new System.Drawing.Size(1176, 652);
            this.pnPlan.TabIndex = 0;
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.pnFieldsTc);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(1176, 652);
            this.xtraTabPage7.Text = "��������������� ����� �����";
            // 
            // pnFieldsTc
            // 
            this.pnFieldsTc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFieldsTc.Location = new System.Drawing.Point(0, 0);
            this.pnFieldsTc.Name = "pnFieldsTc";
            this.pnFieldsTc.Size = new System.Drawing.Size(1176, 652);
            this.pnFieldsTc.TabIndex = 1;
            // 
            // tmCreateOrders
            // 
            this.tmCreateOrders.Enabled = true;
            this.tmCreateOrders.Interval = 60000;
            this.tmCreateOrders.Tick += new System.EventHandler(this.tmCreateOrders_Tick);
            // 
            // MainAgro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 733);
            this.Controls.Add(this.xtbView);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainAgro";
            this.Text = "AGRO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainAgro_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gvGFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGrope)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCalt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupeAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGWorks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDtReasons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeasons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ieCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvbFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatuses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitelOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvbFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitelGrn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchTimeCreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReport.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReport.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbAnaliz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregats)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).EndInit();
            this.xtbView.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtbOrder)).EndInit();
            this.xtbOrder.ResumeLayout(false);
            this.xtbpOrderMain.ResumeLayout(false);
            this.xtbpOrderDetal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcOrderContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvOrderContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtePersent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFieldsList)).EndInit();
            this.gcFieldsList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFieldsOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbReports)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnGraficAgr)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnGraficDrv)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnPlan)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnFieldsTc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarEditItem pbStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar rpbStatus;
        private DevExpress.XtraBars.BarStaticItem lbStatus;
        private DevExpress.XtraBars.BarButtonItem sbtAdd;
        private DevExpress.XtraBars.BarButtonItem sbtDelete;
        private DevExpress.XtraBars.BarButtonItem sbtEdit;
        private DevExpress.XtraBars.BarButtonItem sbtRefresh;
        private DevExpress.XtraBars.BarButtonItem sbtExcel;
        private DevExpress.XtraTab.XtraTabControl xtbView;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTreeList.TreeList tlDictionary;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicName;
        private DevExpress.XtraGrid.GridControl gcDict;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGFields;
        private DevExpress.XtraGrid.Views.Grid.GridView gvWorks;
        private DevExpress.XtraGrid.Columns.GridColumn Id_work;
        private DevExpress.XtraGrid.Columns.GridColumn Name_work;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedBottom;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedTop;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_work;
        private DevExpress.XtraGrid.Columns.GridColumn Id_gf;
        private DevExpress.XtraGrid.Columns.GridColumn Name_gf;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_gf;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFields;
        private DevExpress.XtraGrid.Columns.GridColumn Id_f;
        private DevExpress.XtraGrid.Columns.GridColumn FGROUPE;
        private DevExpress.XtraGrid.Columns.GridColumn FZONE;
        private DevExpress.XtraGrid.Columns.GridColumn Name_f;
        private DevExpress.XtraGrid.Columns.GridColumn Square;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_f;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCalt;
        private DevExpress.XtraGrid.Columns.GridColumn Id_c;
        private DevExpress.XtraGrid.Columns.GridColumn Name_c;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_c;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_a;
        private DevExpress.XtraGrid.Columns.GridColumn Name_a;
        private DevExpress.XtraGrid.Columns.GridColumn Width_a;
        private DevExpress.XtraGrid.Columns.GridColumn Identifier;
        private DevExpress.XtraGrid.Columns.GridColumn Def;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_a;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOrder;
        private DevExpress.XtraGrid.Columns.GridColumn Id_o;
        private DevExpress.XtraGrid.Columns.GridColumn DateInit;
        private DevExpress.XtraGrid.Columns.GridColumn DateComand;
        private DevExpress.XtraGrid.Columns.GridColumn Name_o;
        private DevExpress.XtraGrid.Columns.GridColumn Comment;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPrice;
        private DevExpress.XtraGrid.Columns.GridColumn Id_p;
        private DevExpress.XtraGrid.Columns.GridColumn DateComand_p;
        private DevExpress.XtraGrid.Columns.GridColumn WName;
        private DevExpress.XtraGrid.Columns.GridColumn MName;
        private DevExpress.XtraGrid.Columns.GridColumn AName;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn UName;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_p;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leFieldGrope;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leZone;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUnit;
        private DevExpress.XtraGrid.Columns.GridColumn Id_u;
        private DevExpress.XtraGrid.Columns.GridColumn Name_u;
        private DevExpress.XtraGrid.Columns.GridColumn NameShort_u;
        private DevExpress.XtraGrid.Columns.GridColumn Factor;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_u;
        private DevExpress.XtraGrid.Columns.GridColumn IconC;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit ieCulture;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit peIcon;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leWork;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leMobitel;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leAgregat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leUnit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraGrid.GridControl gcOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gvbFields;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Field;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Family;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStart1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeEnd1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeMove1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeRate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DistanceM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquareCalc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SpeedAvgField;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gvbFuel;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FamilyF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn IdF;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelAdd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelSub;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpens;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensAvgRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraBars.BarEditItem bleGroupe;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leGroupe;
        private DevExpress.XtraBars.BarEditItem bleMobitelGrn;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leMobitelGrn;
        private DevExpress.XtraBars.BarButtonItem btOrdersCreate;
        private DevExpress.XtraBars.BarEditItem teTimeCreate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraBars.BarEditItem chTimeCreate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchTimeCreate;
        private DevExpress.XtraBars.BarButtonItem btStart;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gcFields;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFieldsOrder;
        private DevExpress.XtraGrid.Columns.GridColumn Zone_ID;
        private DevExpress.XtraGrid.Columns.GridColumn FieldId;
        private DevExpress.XtraGrid.Columns.GridColumn GrpName;
        private DevExpress.XtraGrid.Columns.GridColumn FieldName;
        private DevExpress.XtraGrid.Columns.GridColumn SquareF;
        private DevExpress.XtraGrid.Columns.GridColumn ORDERS;
        private DevExpress.XtraEditors.PanelControl pnMap;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraBars.BarEditItem bdeStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStart;
        private DevExpress.XtraBars.BarEditItem bdeEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraBars.BarButtonItem btFilterClear;
        private System.Windows.Forms.Timer tmCreateOrders;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem btStartReport;
        private DevExpress.XtraBars.BarEditItem bdeStartReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStartReport;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.BarEditItem bdeEndReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEndReport;
        private DevExpress.XtraBars.BarSubItem bsiSettings;
        private DevExpress.XtraBars.BarButtonItem btColumnsSet;
        private DevExpress.XtraBars.BarButtonItem btColumnsSave;
        private DevExpress.XtraBars.BarButtonItem btColumnRestore;
        private DevExpress.XtraBars.BarSubItem bsiFact;
        private DevExpress.XtraBars.BarButtonItem btFactControl;
        private DevExpress.XtraBars.BarButtonItem btFact;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraBars.BarEditItem bdeStartReportAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStartReportAgr;
        private DevExpress.XtraBars.BarEditItem bdeEndReportAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEndReportAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup rbAnaliz;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leAgregats;
        private DevExpress.XtraBars.BarButtonItem btStartReportAgr;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.PanelControl pnGraficAgr;
        private DevExpress.XtraEditors.PanelControl pnGraficDrv;
        private DevExpress.XtraBars.BarButtonItem btExcelImport;
        private DevExpress.XtraGrid.Columns.GridColumn InvNumber;
        private DevExpress.XtraGrid.Columns.GridColumn GroupeAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_work_agr;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leGroupeAgr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_gagr;
        private DevExpress.XtraGrid.Columns.GridColumn Name_gagr;
        private DevExpress.XtraGrid.Columns.GridColumn Remark_gagr;
        private DevExpress.XtraGrid.Columns.GridColumn Work_gagr;
        private DevExpress.XtraBars.BarButtonItem btSettings;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DateGrn;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Mobitel;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leMobitelOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LocationStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LocationEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Distance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SpeedAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CommentOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SquareWorkDescript;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PathWithoutWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDUTExpensSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDUTExpensAvgSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDUTExpensWithoutWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDUTExpensAvgWithoutWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensAvgSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensWithoutWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensAvgWithoutWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FactSquareCol;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactSquareCalc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactSquareCalcOverlap;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolValidity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolPointsIntervalMax;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar pbValidity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colState;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbStatuses;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraNavBar.NavBarControl nbReports;
        private DevExpress.XtraNavBar.NavBarGroup nbgReports;
        private DevExpress.XtraBars.BarStaticItem bsiNumber;
        private DevExpress.XtraBars.BarEditItem beiNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riteNumber;
        private DevExpress.XtraBars.BarStaticItem bsiActiveReport;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeWork;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGWorks;
        private DevExpress.XtraGrid.Columns.GridColumn Id_gw;
        private DevExpress.XtraGrid.Columns.GridColumn Name_gw;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_gw;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupWork;
        private DevExpress.XtraTab.XtraTabControl xtbOrder;
        private DevExpress.XtraTab.XtraTabPage xtbpOrderMain;
        private DevExpress.XtraTab.XtraTabPage xtbpOrderDetal;
        private DevExpress.XtraGrid.GridControl gcOrderContent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvOrderContent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIdOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactSquareCalcContent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colComment;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDistance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpeedAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFuelExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFuelExpens;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn49;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn50;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn51;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn52;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn54;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn55;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colMakeCar;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCarModel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumberPlate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colInvNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAgrName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFamily;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFieldName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWorkName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPersSquare;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rtePersent;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFuelExpensRun;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFuelExpensRunKm;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteTime;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.PanelControl pnPlan;
        private DevExpress.XtraEditors.GroupControl gcFieldsList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDtReasons;
        private DevExpress.XtraGrid.Columns.GridColumn Id_dtr;
        private DevExpress.XtraGrid.Columns.GridColumn Name_dtr;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_dtr;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraBars.BarButtonItem btnStopExecutingData;
        private DevExpress.XtraBars.BarEditItem bleCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCategory;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolCategory;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSeasons;
        private DevExpress.XtraGrid.Columns.GridColumn colsId;
        private DevExpress.XtraGrid.Columns.GridColumn colsDateStart;
        private DevExpress.XtraGrid.Columns.GridColumn colsDateEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colsCulture;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleCulture;
        private DevExpress.XtraGrid.Columns.GridColumn colsSquarePlan;
        private DevExpress.XtraGrid.Columns.GridColumn colsComment;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraEditors.PanelControl pnFieldsTc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DateLastRecalc;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbQuontity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOutLinkId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leFieldGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leField;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFieldGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn IsOutOfDate;
        private DevExpress.XtraBars.BarButtonItem btXmlExport;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensSquare_;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensAvgSquare_;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensWithoutWork_;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensAvgWithoutWork_;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelDRTExpensAvgRate_;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedControl;
        private DevExpress.XtraGrid.Columns.GridColumn DepthControl;
    }
}