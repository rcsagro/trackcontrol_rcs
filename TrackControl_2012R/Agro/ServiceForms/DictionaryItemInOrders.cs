using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Agro.Dictionaries;
using Agro.Properties;

namespace Agro.ServiceForms
{
    public partial class DictionaryItemInOrders : DevExpress.XtraEditors.XtraForm
    {
        public DictionaryItemInOrders(DictionaryItem di)
        {
            InitializeComponent();
            this.Text = di.Name;
            gcOrders.DataSource = di.GetDocumentsList();
        }
        void Localization()
        {

            colId.Caption = Resources.Number;
            colDate.Caption = Resources.Date;
            colVehicle.Caption = Resources.Technics;
            colFields.Caption = Resources.ProceededFields;
        }
        
        private void ViewOrder()
        {
            int idOrder = 0;

            if (!gvOrders.IsValidRowHandle(gvOrders.FocusedRowHandle))
            {
                XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                return;
            }
            Int32.TryParse(gvOrders.GetRowCellValue(gvOrders.FocusedRowHandle, "Id").ToString(), out idOrder);

            if (idOrder > 0) AgroController.Instance.ViewOrder(idOrder);
        }

        private void gcOrders_DoubleClick(object sender, EventArgs e)
        {
            ViewOrder();
        }
    }
}