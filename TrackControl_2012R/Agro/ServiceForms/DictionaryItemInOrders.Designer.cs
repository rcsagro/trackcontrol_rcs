namespace Agro.ServiceForms
{
    partial class DictionaryItemInOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DictionaryItemInOrders));
            this.gcOrders = new DevExpress.XtraGrid.GridControl();
            this.gvOrders = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFields = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // gcOrders
            // 
            this.gcOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrders.Location = new System.Drawing.Point(0, 0);
            this.gcOrders.MainView = this.gvOrders;
            this.gcOrders.Name = "gcOrders";
            this.gcOrders.Size = new System.Drawing.Size(730, 402);
            this.gcOrders.TabIndex = 0;
            this.gcOrders.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOrders});
            this.gcOrders.DoubleClick += new System.EventHandler(this.gcOrders_DoubleClick);
            // 
            // gvOrders
            // 
            this.gvOrders.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvOrders.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvOrders.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvOrders.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrders.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrders.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvOrders.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvOrders.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvOrders.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvOrders.Appearance.Empty.Options.UseBackColor = true;
            this.gvOrders.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrders.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrders.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrders.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrders.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvOrders.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvOrders.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvOrders.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrders.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvOrders.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvOrders.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvOrders.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvOrders.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvOrders.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvOrders.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvOrders.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvOrders.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvOrders.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvOrders.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvOrders.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvOrders.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrders.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrders.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvOrders.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvOrders.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvOrders.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvOrders.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvOrders.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvOrders.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrders.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvOrders.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvOrders.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvOrders.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvOrders.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrders.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvOrders.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrders.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrders.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.OddRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.OddRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvOrders.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvOrders.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvOrders.Appearance.Preview.Options.UseBackColor = true;
            this.gvOrders.Appearance.Preview.Options.UseFont = true;
            this.gvOrders.Appearance.Preview.Options.UseForeColor = true;
            this.gvOrders.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrders.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.Row.Options.UseBackColor = true;
            this.gvOrders.Appearance.Row.Options.UseForeColor = true;
            this.gvOrders.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrders.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvOrders.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvOrders.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvOrders.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrders.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrders.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvOrders.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvOrders.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvOrders.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvOrders.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrders.Appearance.VertLine.Options.UseBackColor = true;
            this.gvOrders.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colDate,
            this.colVehicle,
            this.colFields});
            this.gvOrders.GridControl = this.gcOrders;
            this.gvOrders.Name = "gvOrders";
            this.gvOrders.OptionsView.EnableAppearanceEvenRow = true;
            this.gvOrders.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colId
            // 
            this.colId.AppearanceCell.Options.UseTextOptions = true;
            this.colId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "�����";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            this.colId.Width = 175;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.Caption = "����";
            this.colDate.DisplayFormat.FormatString = "d";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            this.colDate.Width = 198;
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "�������";
            this.colVehicle.FieldName = "Vehicle";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.OptionsColumn.ReadOnly = true;
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 2;
            this.colVehicle.Width = 294;
            // 
            // colFields
            // 
            this.colFields.AppearanceHeader.Options.UseTextOptions = true;
            this.colFields.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFields.Caption = "������������ ����";
            this.colFields.FieldName = "SquareWorkDescript";
            this.colFields.Name = "colFields";
            this.colFields.OptionsColumn.AllowEdit = false;
            this.colFields.OptionsColumn.ReadOnly = true;
            this.colFields.Visible = true;
            this.colFields.VisibleIndex = 3;
            this.colFields.Width = 583;
            // 
            // DictionaryItemInOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 402);
            this.Controls.Add(this.gcOrders);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DictionaryItemInOrders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������������";
            ((System.ComponentModel.ISupportInitialize)(this.gcOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


        private DevExpress.XtraGrid.GridControl gcOrders;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOrders;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colFields;

    }
}