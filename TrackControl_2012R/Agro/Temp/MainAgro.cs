using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using LocalCache;
using AGis = Agro.Controls.AgroMap;
using Agro.Utilites;
using TUtil = Agro.Utilites.TotUtilites;
using System.IO;
using System.Diagnostics;
using Agro.GridEditForms;
using Agro.Dictionaries;

namespace Agro.Temp
{
    public partial class MainAgro : DevExpress.XtraEditors.XtraForm
    {
        public MainAgro()
        {
            InitializeComponent();
        }
        #region �����������
        private enum Dictionaries 
        {
            ���������, 
            �����������,
            ����,
            ����������������������� ,
            �������� ,
            �������������������� ,
            �������,
            �������� 
        }
        private void tlDictionary_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            LoadDictionary();
        }
        private void LoadDictionary()
        {
            if (tlDictionary.FocusedNode == null) return;

            switch ((Dictionaries)tlDictionary.FocusedNode.Id)
            {

                case Dictionaries.���������:
                    {
                        gcDict.MainView = gvWorks;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT Id, Name ,SpeedBottom,SpeedTop, Comment  FROM  agro_work ORDER BY Name";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.�����������:
                    {
                        gcDict.MainView = gvGFields;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT Id, Name , Comment  FROM  agro_fieldgroupe ORDER BY Name";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.����:
                    {
                        gcDict.MainView = gvFields;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT   agro_field.Id, agro_field.Id_main as FGROUPE, agro_field.Id_zone as FZONE, agro_field.Name, 100 * zones.Square as Square, agro_field.Comment " +
                            " FROM   agro_field   LEFT OUTER JOIN   zones ON agro_field.Id_zone = zones.Zone_ID ";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT zones.Zone_ID as Id, zones.Name FROM zones ORDER BY zones.Name";
                            leZone.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM    agro_fieldgroupe ORDER BY  agro_fieldgroupe.Name";
                            leFieldGrope.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.�����������������������:
                    {
                        gcDict.MainView = gvUnit;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT Id, Name ,NameShort , Factor, Comment  FROM  agro_unit ORDER BY Name";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.��������:
                    {
                        gcDict.MainView = gvCalt;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT Id,Name ,Icon, Comment  FROM  agro_culture ORDER BY Name";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.��������������������:
                    {
                        gcDict.MainView = gvAgr;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT Id, Name ,Width,Identifier,Def , Comment FROM  agro_agregat ORDER BY Name"; 
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.�������:
                    {
                        gcDict.MainView = gvOrder;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = "SELECT  Id, DateInit, DateComand,  Name, Comment FROM   agro_price";
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
                case Dictionaries.��������:
                    {
                        gcDict.MainView = gvPrice;
                        using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        {
                            string sSQL = ""; ------

                            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                            {
                                sSQL = "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.`Comment`"
                                + " FROM   agro_pricet ";   
                            }
                            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                            {
                                sSQL = "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.Comment"
                                + " FROM   agro_pricet ";
                            }
                            gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   agro_price.Id, agro_price.DateComand as DateOrder FROM agro_price ORDER BY  agro_price.DateComand DESC";
                            leOrder.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   agro_work.Id, agro_work.Name FROM    agro_work ORDER BY   agro_work.Name";
                            leWork.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   vehicle.Mobitel_id as Id, CONCAT(vehicle.MakeCar,' | ' , vehicle.CarModel) AS Name FROM   vehicle";
                            leMobitel.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   agro_agregat.Id, agro_agregat.Name FROM    agro_agregat ORDER BY   agro_agregat.Name";
                            leAgregat.DataSource = _cnMySQL.GetDataTable(sSQL);
                            sSQL = "SELECT   agro_unit.Id, agro_unit.NameShort as Name FROM    agro_unit ORDER BY   agro_unit.NameShort";
                            leUnit.DataSource = _cnMySQL.GetDataTable(sSQL);
                        }
                        break;
                    }
            }
        }
        /// <summary>
        /// �������� ����� �����������
        /// </summary>
        /// <param name="bNew"></param>
        /// <param name="bDelete"></param>
        private void ViewForm(bool bNew, bool bDelete)
        {
            if (tlDictionary.FocusedNode == null) return;
            switch ((Dictionaries)tlDictionary.FocusedNode.Id)
            {
                case Dictionaries.���������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroWorkType di_awt = new DictionaryAgroWorkType((int)gvWorks.GetRowCellValue(gvWorks.FocusedRowHandle, gvWorks.Columns["Id"])))
                            {
                                if (di_awt.DeleteItem())
                                {
                                    DataRow row = gvWorks.GetDataRow(gvWorks.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new WorkType(gvWorks, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.�����������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroFieldGrp di_afg = new DictionaryAgroFieldGrp((int)gvGFields.GetRowCellValue(gvGFields.FocusedRowHandle, gvGFields.Columns["Id"])))
                            {
                                if (di_afg.DeleteItem())
                                {
                                    DataRow row = gvGFields.GetDataRow(gvGFields.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new FieldGrp(gvGFields, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.�����������������������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroUnit di_au = new DictionaryAgroUnit((int)gvUnit.GetRowCellValue(gvUnit.FocusedRowHandle, gvUnit.Columns["Id"])))
                            {
                                if (di_au.DeleteItem())
                                {
                                    DataRow row = gvUnit.GetDataRow(gvUnit.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Unit(gvUnit, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.��������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroCulture di_ac = new DictionaryAgroCulture((int)gvCalt.GetRowCellValue(gvCalt.FocusedRowHandle, gvCalt.Columns["Id"])))
                            {
                                if (di_ac.DeleteItem())
                                {
                                    DataRow row = gvCalt.GetDataRow(gvCalt.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Culture(gvCalt, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.��������������������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroAgregat di_aa = new DictionaryAgroAgregat((int)gvAgr.GetRowCellValue(gvAgr.FocusedRowHandle, gvAgr.Columns["Id"])))
                            {
                                if (di_aa.DeleteItem())
                                {
                                    DataRow row = gvAgr.GetDataRow(gvAgr.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Agregat(gvAgr, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.����:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroField di_af = new DictionaryAgroField((int)gvFields.GetRowCellValue(gvFields.FocusedRowHandle, gvFields.Columns["Id"])))
                            {
                                if (di_af.DeleteItem())
                                {
                                    DataRow row = gvFields.GetDataRow(gvFields.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Field(gvFields, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.�������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroCommand di_ac = new DictionaryAgroCommand((int)gvOrder.GetRowCellValue(gvOrder.FocusedRowHandle, gvOrder.Columns["Id"])))
                            {
                                if (di_ac.DeleteItem())
                                {
                                    DataRow row = gvOrder.GetDataRow(gvOrder.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Command(gvOrder, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.��������:
                    {
                        if (bDelete)
                        {
                            using (DictionaryAgroPrice di_ap = new DictionaryAgroPrice((int)gvPrice.GetRowCellValue(gvPrice.FocusedRowHandle, gvPrice.Columns["Id"])))
                            {
                                if (di_ap.DeleteItem())
                                {
                                    DataRow row = gvPrice.GetDataRow(gvPrice.FocusedRowHandle);
                                    row.Delete();
                                }
                            }
                        }
                        else
                            new Price(gvPrice, bNew).ShowDialog();
                        break;
                    }
            }
        }
        #endregion

        private void sbtAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        ViewForm(true, false);
                        break;
                    }
                case 1:
                    {
                        //new Order(ref gvOrder, true, _listCodeMap).ShowDialog();
                        break;
                    }
            }
        }

        private void gvGFields_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void LoadTabGrid()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        LoadDictionary();
                        break;
                    }
                case 1:
                    {
                        break;
                    }
                case 2:
                    {
                        //ReportDrivers();
                        break;
                    }
            }
            //SetControls();
        }

        private void sbtRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadTabGrid();
        }
        /// <summary>
        /// �������� �������� ������� ����� �� �������
        /// </summary>
        private void ViewDoc()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (tlDictionary.FocusedNode == null)
                        {
                            XtraMessageBox.Show("�������� �������� ��� ��������������!", "�������������� ������");
                            return;
                        }
                        ViewForm(false, false);
                        break;
                    }
                case 1:
                    //{
                    //    if (gvTask.FocusedRowHandle == -1) return;
                    //    if (!gvTask.IsValidRowHandle(gvTask.FocusedRowHandle))
                    //    {
                    //        XtraMessageBox.Show("�������� �������� ��� ��������������!", "�������������� ������");
                    //        return;
                    //    }

                    //    if (gvTask.GetRowCellValue(gvTask.FocusedRowHandle, "Id") != null)
                    //    {
                    //        try
                    //        {
                    //            new Task(ref gvTask, false).ShowDialog();
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            XtraMessageBox.Show(ex.Message, "Error!");
                    //        }
                    //    }
                        break;
                    }
            }

        private void sbtEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ViewDoc();
        }

        private void sbtDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (tlDictionary.FocusedNode == null)
                        {
                            XtraMessageBox.Show("�������� �������� ��� ��������!", "�������� ������");
                            return;
                        }
                        ViewForm(false, true);
                        break;
                    }
                case 1:
                    {
                        //if (!gvTask.IsValidRowHandle(gvTask.FocusedRowHandle))
                        //{
                        //    XtraMessageBox.Show("�������� �������� ��� ��������!", "�������� ������");
                        //    return;
                        //}
                        //int _Id = (int)gvTask.GetRowCellValue(gvTask.FocusedRowHandle, gvTask.Columns["Id"]);
                        //TaskItem ti = new TaskItem(_Id);
                        //if (ti.DeleteDoc())
                        //{
                        //    DataRow row = gvTask.GetDataRow(gvTask.FocusedRowHandle);
                        //    row.Delete();
                        //}
                        break;
                    }
            }
            SetControls();
        }

        private void SetControls()
        {
        }

        private void gvFields_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvWorks_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvCalt_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvAgr_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvOrder_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvPrice_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }

        private void gvUnit_DoubleClick(object sender, EventArgs e)
        {
            ViewForm(false, false);
        }
     }

}