namespace Agro.Temp
{
    partial class FormSample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSample));
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.pnButtons = new DevExpress.XtraEditors.PanelControl();
            this.btCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btSave = new DevExpress.XtraEditors.SimpleButton();
            this.btDelete = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.meComment = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txId = new DevExpress.XtraEditors.TextEdit();
            this.txName = new DevExpress.XtraEditors.TextEdit();
            this.lbName = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnButtons)).BeginInit();
            this.pnButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // pnButtons
            // 
            this.pnButtons.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnButtons.Controls.Add(this.btCancel);
            this.pnButtons.Controls.Add(this.btSave);
            this.pnButtons.Controls.Add(this.btDelete);
            this.pnButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnButtons.Location = new System.Drawing.Point(0, 261);
            this.pnButtons.Name = "pnButtons";
            this.pnButtons.Size = new System.Drawing.Size(631, 50);
            this.pnButtons.TabIndex = 7;
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Image = global::Agro.Properties.Resources.gtk_cancel;
            this.btCancel.Location = new System.Drawing.Point(529, 11);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(85, 27);
            this.btCancel.TabIndex = 9;
            this.btCancel.Text = "������";
            // 
            // btSave
            // 
            this.btSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSave.Image = global::Agro.Properties.Resources.Save;
            this.btSave.Location = new System.Drawing.Point(438, 11);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(85, 27);
            this.btSave.TabIndex = 8;
            this.btSave.Text = "���������";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btDelete
            // 
            this.btDelete.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btDelete.Image = global::Agro.Properties.Resources.Remove_16;
            this.btDelete.Location = new System.Drawing.Point(347, 11);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(85, 27);
            this.btDelete.TabIndex = 7;
            this.btDelete.Text = "�������";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.meComment);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txId);
            this.panelControl1.Controls.Add(this.txName);
            this.panelControl1.Controls.Add(this.lbName);
            this.panelControl1.Location = new System.Drawing.Point(13, 46);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(601, 119);
            this.panelControl1.TabIndex = 8;
            // 
            // meComment
            // 
            this.meComment.Location = new System.Drawing.Point(15, 72);
            this.meComment.Name = "meComment";
            this.meComment.Size = new System.Drawing.Size(570, 41);
            this.meComment.TabIndex = 15;
            this.meComment.EditValueChanged += new System.EventHandler(this.meComment_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(524, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(21, 14);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "���";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(15, 52);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 14);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "�����������";
            // 
            // txId
            // 
            this.txId.EditValue = "0";
            this.txId.Location = new System.Drawing.Point(515, 25);
            this.txId.Name = "txId";
            this.txId.Properties.AllowFocused = false;
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txId.Properties.ReadOnly = true;
            this.txId.Size = new System.Drawing.Size(70, 21);
            this.txId.TabIndex = 12;
            // 
            // txName
            // 
            this.txName.Location = new System.Drawing.Point(15, 25);
            this.txName.Name = "txName";
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            this.txName.Size = new System.Drawing.Size(494, 21);
            this.txName.TabIndex = 11;
            this.txName.EditValueChanged += new System.EventHandler(this.txName_EditValueChanged);
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            this.lbName.Location = new System.Drawing.Point(15, 5);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(52, 14);
            this.lbName.TabIndex = 10;
            this.lbName.Text = "��������";
            // 
            // FormSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.pnButtons);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSample";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AGRO";
            this.Load += new System.EventHandler(this.FormSample_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSample_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnButtons)).EndInit();
            this.pnButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        private DevExpress.XtraEditors.PanelControl pnButtons;
        public DevExpress.XtraEditors.SimpleButton btCancel;
        public DevExpress.XtraEditors.SimpleButton btSave;
        public DevExpress.XtraEditors.SimpleButton btDelete;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.XtraEditors.MemoEdit meComment;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.TextEdit txId;
        public DevExpress.XtraEditors.TextEdit txName;
        public DevExpress.XtraEditors.LabelControl lbName;

    }
}