using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class Field : Agro.Temp.FormSample
    {
        public Field()
        {
            InitThis();
        }
        public Field(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroField(_Id);
            this.Text = this.Text + ": " + "����"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP) || !DicUtilites.ValidateLookUp(leGroupe, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["FGROUPE"], leGroupe.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["FZONE"], leZone.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Square"],txSquare.Text);
            //_gvDict.RefreshData();
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroField).Id_main = (int)(leGroupe.EditValue ?? 0);
            (di as DictionaryAgroField).Id_zone = (int)(leZone.EditValue ?? 0);
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            leGroupe.EditValue = (di as DictionaryAgroField).Id_main;
            leZone.EditValue = (di as DictionaryAgroField).Id_zone;
            txSquare.Text   = (di as DictionaryAgroField).Square.ToString() ; 
        }

        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void leZone_EditValueChanged(object sender, EventArgs e)
        {
            int iZone;
            if (Int32.TryParse(Convert.ToString(leZone.EditValue), out iZone))
            {
                    (di as DictionaryAgroField).Id_zone = iZone;
                    txSquare.Text = (di as DictionaryAgroField).Square.ToString() ;
                    if (txName.Text.Length == 0) txName.Text = leZone.Text;
                    ValidateFieldsAction();
            }
        }
        private void InitThis()
        {
            InitializeComponent();
            DicUtilites.LookUpLoad(leGroupe, "SELECT   agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM    agro_fieldgroupe ORDER BY  agro_fieldgroupe.Name");
            DicUtilites.LookUpLoad(leZone, "SELECT   zones.Zone_ID, zones.Name FROM zones ORDER BY zones.Name"); 
        }
    }
}

