namespace Agro.Temp
{
    partial class Price
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leOrder = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.leWork = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.leMobitel = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.leAgregat = new DevExpress.XtraEditors.LookUpEdit();
            this.tePrice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.leUnit = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            this.txName.Visible = false;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // leOrder
            // 
            this.leOrder.Location = new System.Drawing.Point(69, 15);
            this.leOrder.Name = "leOrder";
            this.leOrder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leOrder.Properties.Appearance.Options.UseFont = true;
            this.leOrder.Properties.Appearance.Options.UseTextOptions = true;
            this.leOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.leOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leOrder.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "���� ��������", 20, DevExpress.Utils.FormatType.DateTime, "d", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leOrder.Properties.DisplayFormat.FormatString = "d";
            this.leOrder.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.leOrder.Properties.DisplayMember = "DateComand";
            this.leOrder.Properties.EditFormat.FormatString = "d";
            this.leOrder.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.leOrder.Properties.NullText = "";
            this.leOrder.Properties.ValueMember = "Id";
            this.leOrder.Size = new System.Drawing.Size(140, 21);
            this.leOrder.TabIndex = 16;
            this.leOrder.EditValueChanged += new System.EventHandler(this.leOrder_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(12, 18);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 14);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "������";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(215, 18);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 14);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "������";
            // 
            // leWork
            // 
            this.leWork.Location = new System.Drawing.Point(270, 16);
            this.leWork.Name = "leWork";
            this.leWork.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leWork.Properties.Appearance.Options.UseFont = true;
            this.leWork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leWork.Properties.DisplayMember = "Name";
            this.leWork.Properties.NullText = "";
            this.leWork.Properties.ValueMember = "Id";
            this.leWork.Size = new System.Drawing.Size(344, 21);
            this.leWork.TabIndex = 18;
            this.leWork.EditValueChanged += new System.EventHandler(this.leWork_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(13, 183);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 14);
            this.labelControl5.TabIndex = 21;
            this.labelControl5.Text = "������";
            // 
            // leMobitel
            // 
            this.leMobitel.Location = new System.Drawing.Point(69, 180);
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leMobitel.Properties.Appearance.Options.UseFont = true;
            this.leMobitel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leMobitel.Properties.DisplayMember = "Name";
            this.leMobitel.Properties.NullText = "";
            this.leMobitel.Properties.ValueMember = "Id";
            this.leMobitel.Size = new System.Drawing.Size(204, 21);
            this.leMobitel.TabIndex = 20;
            this.leMobitel.EditValueChanged += new System.EventHandler(this.leMobitel_EditValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(279, 183);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(139, 14);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "�������� ������������";
            // 
            // leAgregat
            // 
            this.leAgregat.Location = new System.Drawing.Point(424, 181);
            this.leAgregat.Name = "leAgregat";
            this.leAgregat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leAgregat.Properties.Appearance.Options.UseFont = true;
            this.leAgregat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leAgregat.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leAgregat.Properties.DisplayMember = "Name";
            this.leAgregat.Properties.NullText = "";
            this.leAgregat.Properties.ValueMember = "Id";
            this.leAgregat.Size = new System.Drawing.Size(190, 21);
            this.leAgregat.TabIndex = 22;
            this.leAgregat.EditValueChanged += new System.EventHandler(this.leAgregat_EditValueChanged);
            // 
            // tePrice
            // 
            this.tePrice.EditValue = "0";
            this.tePrice.Location = new System.Drawing.Point(187, 218);
            this.tePrice.Name = "tePrice";
            this.tePrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tePrice.Properties.Appearance.Options.UseFont = true;
            this.tePrice.Properties.Appearance.Options.UseTextOptions = true;
            this.tePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tePrice.Size = new System.Drawing.Size(86, 21);
            this.tePrice.TabIndex = 24;
            this.tePrice.EditValueChanged += new System.EventHandler(this.tePrice_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(83, 221);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(79, 14);
            this.labelControl8.TabIndex = 25;
            this.labelControl8.Text = "��������, ���";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(292, 220);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(114, 14);
            this.labelControl9.TabIndex = 27;
            this.labelControl9.Text = "������� ���������";
            // 
            // leUnit
            // 
            this.leUnit.Location = new System.Drawing.Point(424, 219);
            this.leUnit.Name = "leUnit";
            this.leUnit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leUnit.Properties.Appearance.Options.UseFont = true;
            this.leUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leUnit.Properties.DisplayMember = "Name";
            this.leUnit.Properties.NullText = "";
            this.leUnit.Properties.ValueMember = "Id";
            this.leUnit.Size = new System.Drawing.Size(83, 21);
            this.leUnit.TabIndex = 26;
            this.leUnit.EditValueChanged += new System.EventHandler(this.leUnit_EditValueChanged);
            // 
            // Price
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.leUnit);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.tePrice);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.leAgregat);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.leMobitel);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.leWork);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.leOrder);
            this.Name = "Price";
            this.Controls.SetChildIndex(this.leOrder, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.leWork, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.leMobitel, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.leAgregat, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.tePrice, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.leUnit, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leOrder;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.LookUpEdit leWork;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.LookUpEdit leMobitel;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.LookUpEdit leAgregat;
        private DevExpress.XtraEditors.TextEdit tePrice;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        public DevExpress.XtraEditors.LookUpEdit leUnit;
    }
}
