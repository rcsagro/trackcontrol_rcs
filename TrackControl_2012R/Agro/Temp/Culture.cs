using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class Culture : Agro.Temp.FormSample
    {
        public Culture()
        {
            InitThis();
        }

        public Culture(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroCulture(_Id);
            this.Text = this.Text + ": " + "��������";
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Icon"], (di as DictionaryAgroCulture).Icon);
            _gvDict.RefreshData();
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            if ((di as DictionaryAgroCulture).Icon != null)
            {
                System.IO.MemoryStream mStr = new System.IO.MemoryStream((di as DictionaryAgroCulture).Icon, false);
                Bitmap bmp = new Bitmap(mStr);
                peIcon.Image = bmp;
            }
        }
        private void btIcon_Click(object sender, EventArgs e)
        {
            if (ofdIcon.ShowDialog() == DialogResult.OK)
            {
                peIcon.Image = Image.FromStream(ofdIcon.OpenFile());
                (di as DictionaryAgroCulture).Icon = new byte[ofdIcon.OpenFile().Length];
                ofdIcon.OpenFile().Read((di as DictionaryAgroCulture).Icon, 0, (di as DictionaryAgroCulture).Icon.Length);
                ValidateFieldsAction();
            }
        }
        private void InitThis()
        {
            InitializeComponent();
        }

    }
}

