using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Agro.Utilites;
using Agro.Dictionaries;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro.Temp
{
    public partial class FormSample : DevExpress.XtraEditors.XtraForm, IFormDictionary
    {
        /// <summary>
        /// ������ �����������
        /// </summary>
        protected  GridView _gvDict;
        /// <summary>
        /// ������������� �����������
        /// </summary>
        protected int _Id = 0;
        /// <summary>
        /// ������ �����������
        /// </summary>
        protected DictionaryItem di; 
        /// <summary>
        /// ��������� �������� ������
        /// </summary>
        protected bool bFormLoading;
        public FormSample()
        {
            InitializeComponent();

        }
        public FormSample(GridView gvDict, bool bNew)
            : this()
        {
            _gvDict = gvDict;
            if (!bNew)
            {
                _Id = (int)_gvDict.GetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"]);
                txId.Text = _Id.ToString();
                btDelete.Enabled = true;
            }
            btSave.Enabled = false;
        }
        #region IFormDictionary Members
        public virtual bool ValidateFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool ValidateFieldsAction()
        {
            if (!ValidateFields())
            {
                btSave.Enabled = false;
                return false;
            }
            else
            {
                btSave.Enabled = true;
                return true;
            }
        }
        public virtual void SaveChanges()
        {
                bool bUpdateGRN = false;
                SetFields();
                if (_Id == 0)
                {
                    _Id = di.AddItem();
                    txId.Text = _Id.ToString();
                    if (_Id > 0)
                    {
                        _gvDict.AddNewRow();
                        bUpdateGRN = true;
                    }
                }
                else
                {
                    if (di.EditItem())
                    {
                        bUpdateGRN = true;
                    }
                }
                if (bUpdateGRN)
                {
                    UpdateGRN();
                }
        }
        public virtual bool TestLinks()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool DeleteRecord()
        {
                if (di.DeleteItem())
                {
                    if (_Id == (int)_gvDict.GetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"]))
                    {
                        DataRow row = _gvDict.GetDataRow(_gvDict.FocusedRowHandle);
                        row.Delete();
                    }
                    return true;
                }
                else
                    return false;
        }
        public virtual void SetControls()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void GetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void UpdateGRN()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
        private void FormSample_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Yes)
                if (!DeleteRecord()) e.Cancel = true;
            di.Dispose(); 
        }
        private void txName_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void meComment_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            SaveChanges(); 
        }

        private void FormSample_Load(object sender, EventArgs e)
        {
            if (txName.Visible)
                lbName.Visible = true;
            else
                lbName.Visible = false;
        }
    }
}