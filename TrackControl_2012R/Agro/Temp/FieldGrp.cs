using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class FieldGrp : Agro.Temp.FormSample
    {
        public FieldGrp()
        {
            InitializeComponent();
        }
        public FieldGrp(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            di = new DictionaryAgroFieldGrp(_Id);
            this.Text = this.Text + ": " + "������ �����"; 
            GetFields();
        }
        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.RefreshData();
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment; 
        }
    }
}
