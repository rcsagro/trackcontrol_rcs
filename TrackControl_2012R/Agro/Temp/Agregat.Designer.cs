namespace Agro.Temp
{
    partial class Agregat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.teIdentifier = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.teWidth = new DevExpress.XtraEditors.TextEdit();
            this.chDef = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teIdentifier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDef.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(14, 186);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(91, 14);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "�������������";
            // 
            // teIdentifier
            // 
            this.teIdentifier.Location = new System.Drawing.Point(129, 184);
            this.teIdentifier.Name = "teIdentifier";
            this.teIdentifier.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teIdentifier.Properties.Appearance.Options.UseFont = true;
            this.teIdentifier.Properties.Appearance.Options.UseTextOptions = true;
            this.teIdentifier.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teIdentifier.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.teIdentifier.Properties.Mask.EditMask = "00000";
            this.teIdentifier.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.teIdentifier.Properties.Mask.SaveLiteral = false;
            this.teIdentifier.Properties.Mask.ShowPlaceHolders = false;
            this.teIdentifier.Size = new System.Drawing.Size(78, 21);
            this.teIdentifier.TabIndex = 10;
            this.teIdentifier.EditValueChanged += new System.EventHandler(this.teIdentifier_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(225, 187);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(44, 14);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "������";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(358, 188);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(41, 14);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "������";
            // 
            // teWidth
            // 
            this.teWidth.EditValue = "0";
            this.teWidth.Location = new System.Drawing.Point(279, 185);
            this.teWidth.Name = "teWidth";
            this.teWidth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teWidth.Properties.Appearance.Options.UseFont = true;
            this.teWidth.Properties.Appearance.Options.UseTextOptions = true;
            this.teWidth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teWidth.Size = new System.Drawing.Size(62, 21);
            this.teWidth.TabIndex = 13;
            this.teWidth.TabStop = false;
            this.teWidth.EditValueChanged += new System.EventHandler(this.teWidth_EditValueChanged);
            // 
            // chDef
            // 
            this.chDef.Location = new System.Drawing.Point(12, 223);
            this.chDef.Name = "chDef";
            this.chDef.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.chDef.Properties.Appearance.Options.UseFont = true;
            this.chDef.Properties.Caption = "������������ �� ��������� ��� �������";
            this.chDef.Size = new System.Drawing.Size(283, 19);
            this.chDef.TabIndex = 14;
            this.chDef.EditValueChanged += new System.EventHandler(this.chDef_EditValueChanged);
            // 
            // Agregat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.chDef);
            this.Controls.Add(this.teWidth);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.teIdentifier);
            this.Name = "Agregat";
            this.Controls.SetChildIndex(this.teIdentifier, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.teWidth, 0);
            this.Controls.SetChildIndex(this.chDef, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teIdentifier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDef.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit teIdentifier;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit teWidth;
        private DevExpress.XtraEditors.CheckEdit chDef;
    }
}
