namespace Agro.Temp
{
    partial class FieldGrp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // meComment
            // 
            // 
            // FieldGrpN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(626, 360);
            this.Name = "FieldGrpN";
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
