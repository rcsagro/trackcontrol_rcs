using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;


namespace Agro.Temp
{
    public partial class Price : Agro.Temp.FormSample
    {
        public Price()
        {
            InitThis();
        }
        public Price(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroPrice(_Id);
            this.Text = this.Text + ": " + "��������"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateLookUp(leOrder, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositive(tePrice, dxErrP) |
                !DicUtilites.ValidateLookUp(leWork, dxErrP) |
                !DicUtilites.ValidateLookUp(leMobitel, dxErrP) |
                !DicUtilites.ValidateLookUp(leAgregat, dxErrP) |
                !DicUtilites.ValidateLookUp(leUnit, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_main"], leOrder.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_work"], leWork.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_mobitel"], leMobitel.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_agregat"], leAgregat.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_unit"], leUnit.EditValue);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Price"], tePrice.Text);
        }
        public override void SetFields()
        {
            di.Comment = meComment.Text;
            (di as DictionaryAgroPrice).Id_main = (int)(leOrder.EditValue ?? 0);
            (di as DictionaryAgroPrice).Id_work = (int)(leWork.EditValue ?? 0);
            (di as DictionaryAgroPrice).Id_mobitel = (int)(leMobitel.EditValue ?? 0);
            (di as DictionaryAgroPrice).Id_agregat = (int)(leAgregat.EditValue ?? 0);
            (di as DictionaryAgroPrice).Id_unit = (int)(leUnit.EditValue ?? 0);
            (di as DictionaryAgroPrice).Price = Convert.ToDouble(tePrice.Text);
        }
        public override void GetFields()
        {
            meComment.Text = di.Comment;
            leOrder.EditValue = (di as DictionaryAgroPrice).Id_main;
            leWork.EditValue = (di as DictionaryAgroPrice).Id_work;
            leMobitel.EditValue = (di as DictionaryAgroPrice).Id_mobitel;
            leAgregat.EditValue = (di as DictionaryAgroPrice).Id_agregat;
            leUnit.EditValue = (di as DictionaryAgroPrice).Id_unit;
            tePrice.Text = (di as DictionaryAgroPrice).Price.ToString() ;
        }

        private void InitThis()
        {
            InitializeComponent();
            DicUtilites.LookUpLoad(leOrder, "SELECT   agro_price.Id, DATE_FORMAT(agro_price.DateComand, '%d.%m.%Y') as DateComand FROM    agro_price ORDER BY  agro_price.DateComand DESC");
            DicUtilites.LookUpLoad(leWork, "SELECT   agro_work.Id, agro_work.Name FROM    agro_work ORDER BY   agro_work.Name");
            DicUtilites.LookUpLoad(leMobitel, "SELECT   vehicle.Mobitel_id as Id, CONCAT(vehicle.MakeCar,' | ' , vehicle.CarModel) AS Namr, vehicle.Team_id FROM   vehicle");
            DicUtilites.LookUpLoad(leAgregat, "SELECT   agro_agregat.Id, agro_agregat.Name FROM    agro_agregat ORDER BY   agro_agregat.Name");
            DicUtilites.LookUpLoad(leUnit, "SELECT   agro_unit.Id, agro_unit.NameShort as Name FROM    agro_unit ORDER BY   agro_unit.NameShort");
        }

        private void leOrder_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void leWork_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void leMobitel_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void leAgregat_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void tePrice_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void leUnit_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

    }
}

