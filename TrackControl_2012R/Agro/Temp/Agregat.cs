using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class Agregat : Agro.Temp.FormSample
    {
    public Agregat()
        {
            InitThis();
        }
        public Agregat(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroAgregat(_Id);
            this.Text = this.Text + ": " + "�������� ������������"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP) | !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(teWidth, dxErrP) | !ValidateIdentifierUnique())
            {
                return false;
            }
            else
            {
                dxErrP.ClearErrors(); 
                return true;
            }
        }
        public override void UpdateGRN()
        {
            int iRow = _gvDict.FocusedRowHandle;
            if (chDef.Checked)
            {
                for (int i = 0; i <= _gvDict.RowCount; i++)
			    {
    			 _gvDict.SetRowCellValue(i, _gvDict.Columns["Def"],0);
			    }
            }
            _gvDict.FocusedRowHandle = iRow;
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Width"], teWidth.Text );
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Identifier"], teIdentifier.Text );
            _gvDict.SetRowCellValue(iRow, _gvDict.Columns["Def"],chDef.Checked);
            
             
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroAgregat).Identifier = Convert.ToUInt16(teIdentifier.Text);
            (di as DictionaryAgroAgregat).Width  = Convert.ToDouble(teWidth.Text);
            (di as DictionaryAgroAgregat).Def  = chDef.Checked;
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            teIdentifier.Text = (di as DictionaryAgroAgregat).Identifier.ToString() ;
            teWidth.Text = (di as DictionaryAgroAgregat).Width.ToString(); 
            chDef.Checked = (di as DictionaryAgroAgregat).Def;
        }

        private void InitThis()
        {
            InitializeComponent();
        }

        private bool ValidateIdentifierUnique()
        {
                UInt16 iIdent;
                if (UInt16.TryParse(teIdentifier.Text, out iIdent))
                {
                    if (iIdent == 0) return true;
                    if (DicUtilites.Test_UNIQUE("agro_agregat", "Identifier", iIdent, Convert.ToInt32(txId.Text)))
                    {
                        return true;
                    }
                    else
                    {
                        dxErrP.SetError(teIdentifier, "������������� �� ����� �����������!");
                        return false;
                    }
                }
                else
                    return true;


        }

        private void teIdentifier_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void teWidth_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void chDef_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}

