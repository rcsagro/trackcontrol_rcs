using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class WorkType : Agro.Temp.FormSample
    {
        public WorkType()
        {
            InitThis();
        }
        public WorkType(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroWorkType(_Id);
            this.Text = this.Text + ": " + "���� �����"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(teSpeedBottom, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(teSpeedTop, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["SpeedBottom"], teSpeedBottom.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["SpeedTop"], teSpeedTop.Text);
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroWorkType).SpeedBottom = Convert.ToDouble(teSpeedBottom.Text);
            (di as DictionaryAgroWorkType).SpeedTop = Convert.ToDouble(teSpeedTop.Text);
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            teSpeedBottom.Text = (di as DictionaryAgroWorkType).SpeedBottom.ToString()  ;
            teSpeedTop.Text = (di as DictionaryAgroWorkType).SpeedTop.ToString();
        }

        private void InitThis()
        {
            InitializeComponent();
        }

        private void teSpeedTop_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void teSpeedBottom_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }
    }
}

