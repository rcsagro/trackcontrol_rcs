using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;
namespace Agro.Temp
{
    public partial class Command : Agro.Temp.FormSample
    {
        public Command()
        {
            InitThis();
        }
        public Command(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroCommand(_Id);
            this.Text = this.Text + ": " + "������� � ����� ��������"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["DateInit"], (deDateInit.DateTime.Year > 1900) ? deDateInit.DateTime.ToString() : null);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["DateComand"], (deDateComand.DateTime.Year > 1900) ? deDateComand.DateTime.ToString() : null);
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            if (deDateInit.DateTime.Year > 1900)
                (di as DictionaryAgroCommand).DateInit = deDateInit.DateTime;
            else
                (di as DictionaryAgroCommand).DateInit = null;
            if (deDateComand.DateTime.Year >1900)
                (di as DictionaryAgroCommand).DateComand = deDateComand.DateTime;
            else
                (di as DictionaryAgroCommand).DateComand = null;
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            if ((di as DictionaryAgroCommand).DateInit != null)
            deDateInit.DateTime = (di as DictionaryAgroCommand).DateInit ?? DateTime.Today ;
        if ((di as DictionaryAgroCommand).DateComand != null)
            deDateComand.DateTime = (di as DictionaryAgroCommand).DateComand ?? DateTime.Today;
        }

        private void InitThis()
        {
            InitializeComponent();
        }
        private void deDateComand_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void deDateInit_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}

