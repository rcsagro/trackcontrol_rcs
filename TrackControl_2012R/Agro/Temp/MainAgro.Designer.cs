namespace Agro.Temp
{
    partial class MainAgro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAgro));
            this.gvGFields = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_gf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDict = new DevExpress.XtraGrid.GridControl();
            this.gvFields = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FGROUPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leFieldGrope = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.FZONE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leZone = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Name_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Square = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvCalt = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Icon = new DevExpress.XtraGrid.Columns.GridColumn();
            this.peIcon = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Name_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_c = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.gvAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Width = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Identifier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Def = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_a = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_o = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateInit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateComand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_o = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvPrice = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateComand_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.WName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leWork = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.MName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leMobitel = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.AName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leUnit = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Comment_p = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvUnit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameShort_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Factor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_u = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvWorks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedBottom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeedTop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment_work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ieCulture = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.sbtAdd = new DevExpress.XtraBars.BarButtonItem();
            this.sbtDelete = new DevExpress.XtraBars.BarButtonItem();
            this.sbtEdit = new DevExpress.XtraBars.BarButtonItem();
            this.sbtRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sbtExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.pbStatus = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.lbStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.xtbView = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tlDictionary = new DevExpress.XtraTreeList.TreeList();
            this.DicCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.DicName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.gvGFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGrope)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCalt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ieCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).BeginInit();
            this.xtbView.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).BeginInit();
            this.SuspendLayout();
            // 
            // gvGFields
            // 
            this.gvGFields.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_gf,
            this.Name_gf,
            this.Comment_gf});
            this.gvGFields.GridControl = this.gcDict;
            this.gvGFields.Name = "gvGFields";
            this.gvGFields.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gvGFields.OptionsBehavior.Editable = false;
            this.gvGFields.DoubleClick += new System.EventHandler(this.gvGFields_DoubleClick);
            // 
            // Id_gf
            // 
            this.Id_gf.Caption = "Id";
            this.Id_gf.FieldName = "Id";
            this.Id_gf.Name = "Id_gf";
            this.Id_gf.OptionsColumn.AllowEdit = false;
            // 
            // Name_gf
            // 
            this.Name_gf.AppearanceCell.Options.UseTextOptions = true;
            this.Name_gf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_gf.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_gf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_gf.Caption = "��������";
            this.Name_gf.FieldName = "Name";
            this.Name_gf.Name = "Name_gf";
            this.Name_gf.OptionsColumn.AllowEdit = false;
            this.Name_gf.Visible = true;
            this.Name_gf.VisibleIndex = 0;
            // 
            // Comment_gf
            // 
            this.Comment_gf.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_gf.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_gf.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_gf.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_gf.Caption = "����������";
            this.Comment_gf.FieldName = "Comment";
            this.Comment_gf.Name = "Comment_gf";
            this.Comment_gf.OptionsColumn.AllowEdit = false;
            this.Comment_gf.Visible = true;
            this.Comment_gf.VisibleIndex = 1;
            // 
            // gcDict
            // 
            this.gcDict.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvGFields;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.gvFields;
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode3.LevelTemplate = this.gvCalt;
            gridLevelNode3.RelationName = "Level3";
            gridLevelNode4.LevelTemplate = this.gvAgr;
            gridLevelNode4.RelationName = "Level4";
            gridLevelNode5.LevelTemplate = this.gvOrder;
            gridLevelNode5.RelationName = "Level5";
            gridLevelNode6.LevelTemplate = this.gvPrice;
            gridLevelNode6.RelationName = "Level6";
            gridLevelNode7.LevelTemplate = this.gvUnit;
            gridLevelNode7.RelationName = "Level7";
            this.gcDict.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7});
            this.gcDict.Location = new System.Drawing.Point(0, 0);
            this.gcDict.MainView = this.gvWorks;
            this.gcDict.Name = "gcDict";
            this.gcDict.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leFieldGrope,
            this.leZone,
            this.ieCulture,
            this.peIcon,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2,
            this.repositoryItemTextEdit1,
            this.leOrder,
            this.leWork,
            this.leMobitel,
            this.leAgregat,
            this.leUnit});
            this.gcDict.Size = new System.Drawing.Size(654, 625);
            this.gcDict.TabIndex = 1;
            this.gcDict.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFields,
            this.gvCalt,
            this.gvAgr,
            this.gvOrder,
            this.gvPrice,
            this.gvUnit,
            this.gvWorks,
            this.gvGFields});
            // 
            // gvFields
            // 
            this.gvFields.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_f,
            this.FGROUPE,
            this.FZONE,
            this.Name_f,
            this.Square,
            this.Comment_f});
            this.gvFields.GridControl = this.gcDict;
            this.gvFields.Name = "gvFields";
            this.gvFields.DoubleClick += new System.EventHandler(this.gvFields_DoubleClick);
            // 
            // Id_f
            // 
            this.Id_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_f.Caption = "Id";
            this.Id_f.FieldName = "Id";
            this.Id_f.Name = "Id_f";
            this.Id_f.OptionsColumn.AllowEdit = false;
            // 
            // FGROUPE
            // 
            this.FGROUPE.AppearanceCell.Options.UseTextOptions = true;
            this.FGROUPE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.FGROUPE.AppearanceHeader.Options.UseTextOptions = true;
            this.FGROUPE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FGROUPE.Caption = "������ �����";
            this.FGROUPE.ColumnEdit = this.leFieldGrope;
            this.FGROUPE.FieldName = "FGROUPE";
            this.FGROUPE.Name = "FGROUPE";
            this.FGROUPE.OptionsColumn.AllowEdit = false;
            this.FGROUPE.Visible = true;
            this.FGROUPE.VisibleIndex = 0;
            this.FGROUPE.Width = 158;
            // 
            // leFieldGrope
            // 
            this.leFieldGrope.AutoHeight = false;
            this.leFieldGrope.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leFieldGrope.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leFieldGrope.DisplayMember = "Name";
            this.leFieldGrope.Name = "leFieldGrope";
            this.leFieldGrope.ValueMember = "Id";
            // 
            // FZONE
            // 
            this.FZONE.AppearanceCell.Options.UseTextOptions = true;
            this.FZONE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.FZONE.AppearanceHeader.Options.UseTextOptions = true;
            this.FZONE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FZONE.Caption = "�������� ����";
            this.FZONE.ColumnEdit = this.leZone;
            this.FZONE.FieldName = "FZONE";
            this.FZONE.Name = "FZONE";
            this.FZONE.OptionsColumn.AllowEdit = false;
            this.FZONE.Visible = true;
            this.FZONE.VisibleIndex = 1;
            this.FZONE.Width = 177;
            // 
            // leZone
            // 
            this.leZone.AutoHeight = false;
            this.leZone.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leZone.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leZone.DisplayMember = "Name";
            this.leZone.Name = "leZone";
            this.leZone.ValueMember = "Id";
            // 
            // Name_f
            // 
            this.Name_f.AppearanceCell.Options.UseTextOptions = true;
            this.Name_f.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_f.Caption = "��������";
            this.Name_f.FieldName = "Name";
            this.Name_f.Name = "Name_f";
            this.Name_f.OptionsColumn.AllowEdit = false;
            this.Name_f.Visible = true;
            this.Name_f.VisibleIndex = 2;
            this.Name_f.Width = 443;
            // 
            // Square
            // 
            this.Square.AppearanceCell.Options.UseTextOptions = true;
            this.Square.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Square.AppearanceHeader.Options.UseTextOptions = true;
            this.Square.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Square.Caption = "�������, ��";
            this.Square.FieldName = "Square";
            this.Square.Name = "Square";
            this.Square.OptionsColumn.AllowEdit = false;
            this.Square.Visible = true;
            this.Square.VisibleIndex = 3;
            this.Square.Width = 126;
            // 
            // Comment_f
            // 
            this.Comment_f.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_f.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_f.Caption = "����������";
            this.Comment_f.FieldName = "Comment";
            this.Comment_f.Name = "Comment_f";
            this.Comment_f.OptionsColumn.AllowEdit = false;
            this.Comment_f.Visible = true;
            this.Comment_f.VisibleIndex = 4;
            this.Comment_f.Width = 186;
            // 
            // gvCalt
            // 
            this.gvCalt.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_c,
            this.Icon,
            this.Name_c,
            this.Comment_c});
            this.gvCalt.GridControl = this.gcDict;
            this.gvCalt.Images = this.imCollection;
            this.gvCalt.Name = "gvCalt";
            this.gvCalt.DoubleClick += new System.EventHandler(this.gvCalt_DoubleClick);
            // 
            // Id_c
            // 
            this.Id_c.AppearanceCell.Options.UseTextOptions = true;
            this.Id_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_c.Caption = "Id";
            this.Id_c.FieldName = "Id";
            this.Id_c.Name = "Id_c";
            this.Id_c.OptionsColumn.AllowEdit = false;
            // 
            // Icon
            // 
            this.Icon.AppearanceCell.Options.UseTextOptions = true;
            this.Icon.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Icon.AppearanceHeader.Options.UseTextOptions = true;
            this.Icon.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Icon.Caption = "Image";
            this.Icon.ColumnEdit = this.peIcon;
            this.Icon.FieldName = "Icon";
            this.Icon.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Icon.ImageIndex = 2;
            this.Icon.Name = "Icon";
            this.Icon.OptionsColumn.AllowEdit = false;
            this.Icon.Visible = true;
            this.Icon.VisibleIndex = 0;
            this.Icon.Width = 52;
            // 
            // peIcon
            // 
            this.peIcon.AppearanceDisabled.Image = global::Agro.Properties.Resources.image_edit;
            this.peIcon.AppearanceDisabled.Options.UseImage = true;
            this.peIcon.Name = "peIcon";
            this.peIcon.NullText = "  ";
            this.peIcon.ReadOnly = true;
            // 
            // Name_c
            // 
            this.Name_c.AppearanceCell.Options.UseTextOptions = true;
            this.Name_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_c.Caption = "��������";
            this.Name_c.FieldName = "Name";
            this.Name_c.Name = "Name_c";
            this.Name_c.OptionsColumn.AllowEdit = false;
            this.Name_c.Visible = true;
            this.Name_c.VisibleIndex = 1;
            this.Name_c.Width = 516;
            // 
            // Comment_c
            // 
            this.Comment_c.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_c.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_c.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_c.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_c.Caption = "����������";
            this.Comment_c.FieldName = "Comment";
            this.Comment_c.Name = "Comment_c";
            this.Comment_c.OptionsColumn.AllowEdit = false;
            this.Comment_c.Visible = true;
            this.Comment_c.VisibleIndex = 2;
            this.Comment_c.Width = 522;
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "bullet_green.png");
            this.imCollection.Images.SetKeyName(1, "bullet_red.png");
            this.imCollection.Images.SetKeyName(2, "image_edit.png");
            // 
            // gvAgr
            // 
            this.gvAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_a,
            this.Name_a,
            this.Width,
            this.Identifier,
            this.Def,
            this.Comment_a});
            this.gvAgr.GridControl = this.gcDict;
            this.gvAgr.Name = "gvAgr";
            this.gvAgr.DoubleClick += new System.EventHandler(this.gvAgr_DoubleClick);
            // 
            // Id_a
            // 
            this.Id_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_a.Caption = "Id";
            this.Id_a.FieldName = "Id";
            this.Id_a.Name = "Id_a";
            this.Id_a.OptionsColumn.AllowEdit = false;
            // 
            // Name_a
            // 
            this.Name_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_a.Caption = "��������";
            this.Name_a.FieldName = "Name";
            this.Name_a.Name = "Name_a";
            this.Name_a.OptionsColumn.AllowEdit = false;
            this.Name_a.Visible = true;
            this.Name_a.VisibleIndex = 0;
            this.Name_a.Width = 455;
            // 
            // Width
            // 
            this.Width.AppearanceCell.Options.UseTextOptions = true;
            this.Width.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width.AppearanceHeader.Options.UseTextOptions = true;
            this.Width.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width.Caption = "������, �";
            this.Width.FieldName = "Width";
            this.Width.Name = "Width";
            this.Width.OptionsColumn.AllowEdit = false;
            this.Width.Visible = true;
            this.Width.VisibleIndex = 1;
            this.Width.Width = 98;
            // 
            // Identifier
            // 
            this.Identifier.AppearanceCell.Options.UseTextOptions = true;
            this.Identifier.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier.AppearanceHeader.Options.UseTextOptions = true;
            this.Identifier.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier.Caption = "�������������";
            this.Identifier.FieldName = "Identifier";
            this.Identifier.Name = "Identifier";
            this.Identifier.OptionsColumn.AllowEdit = false;
            this.Identifier.Visible = true;
            this.Identifier.VisibleIndex = 2;
            this.Identifier.Width = 120;
            // 
            // Def
            // 
            this.Def.AppearanceCell.Options.UseTextOptions = true;
            this.Def.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Def.AppearanceHeader.Options.UseTextOptions = true;
            this.Def.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Def.Caption = "�� ���������";
            this.Def.FieldName = "Def";
            this.Def.Name = "Def";
            this.Def.OptionsColumn.AllowEdit = false;
            this.Def.Visible = true;
            this.Def.VisibleIndex = 3;
            this.Def.Width = 115;
            // 
            // Comment_a
            // 
            this.Comment_a.AppearanceCell.Options.UseTextOptions = true;
            this.Comment_a.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment_a.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_a.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_a.Caption = "����������";
            this.Comment_a.FieldName = "Comment";
            this.Comment_a.Name = "Comment_a";
            this.Comment_a.OptionsColumn.AllowEdit = false;
            this.Comment_a.Visible = true;
            this.Comment_a.VisibleIndex = 4;
            this.Comment_a.Width = 302;
            // 
            // gvOrder
            // 
            this.gvOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_o,
            this.DateInit,
            this.DateComand,
            this.Name_o,
            this.Comment});
            this.gvOrder.GridControl = this.gcDict;
            this.gvOrder.Name = "gvOrder";
            this.gvOrder.DoubleClick += new System.EventHandler(this.gvOrder_DoubleClick);
            // 
            // Id_o
            // 
            this.Id_o.Caption = "Id";
            this.Id_o.FieldName = "Id";
            this.Id_o.Name = "Id_o";
            this.Id_o.OptionsColumn.AllowEdit = false;
            // 
            // DateInit
            // 
            this.DateInit.AppearanceCell.Options.UseTextOptions = true;
            this.DateInit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateInit.AppearanceHeader.Options.UseTextOptions = true;
            this.DateInit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateInit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateInit.Caption = "����";
            this.DateInit.FieldName = "DateInit";
            this.DateInit.Name = "DateInit";
            this.DateInit.OptionsColumn.AllowEdit = false;
            this.DateInit.Visible = true;
            this.DateInit.VisibleIndex = 0;
            this.DateInit.Width = 175;
            // 
            // DateComand
            // 
            this.DateComand.AppearanceCell.Options.UseTextOptions = true;
            this.DateComand.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand.AppearanceHeader.Options.UseTextOptions = true;
            this.DateComand.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateComand.Caption = "���� �����";
            this.DateComand.FieldName = "DateComand";
            this.DateComand.Name = "DateComand";
            this.DateComand.OptionsColumn.AllowEdit = false;
            this.DateComand.Visible = true;
            this.DateComand.VisibleIndex = 1;
            this.DateComand.Width = 183;
            // 
            // Name_o
            // 
            this.Name_o.AppearanceCell.Options.UseTextOptions = true;
            this.Name_o.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_o.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_o.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_o.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Name_o.Caption = "��������";
            this.Name_o.FieldName = "Name";
            this.Name_o.Name = "Name_o";
            this.Name_o.OptionsColumn.AllowEdit = false;
            this.Name_o.Visible = true;
            this.Name_o.VisibleIndex = 2;
            this.Name_o.Width = 415;
            // 
            // Comment
            // 
            this.Comment.AppearanceCell.Options.UseTextOptions = true;
            this.Comment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment.Caption = "����������";
            this.Comment.FieldName = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.OptionsColumn.AllowEdit = false;
            this.Comment.Visible = true;
            this.Comment.VisibleIndex = 3;
            this.Comment.Width = 317;
            // 
            // gvPrice
            // 
            this.gvPrice.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_p,
            this.DateComand_p,
            this.WName,
            this.MName,
            this.AName,
            this.Price,
            this.UName,
            this.Comment_p});
            this.gvPrice.GridControl = this.gcDict;
            this.gvPrice.Name = "gvPrice";
            this.gvPrice.DoubleClick += new System.EventHandler(this.gvPrice_DoubleClick);
            // 
            // Id_p
            // 
            this.Id_p.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_p.Caption = "Id";
            this.Id_p.FieldName = "Id";
            this.Id_p.Name = "Id_p";
            this.Id_p.OptionsColumn.AllowEdit = false;
            // 
            // DateComand_p
            // 
            this.DateComand_p.AppearanceCell.Options.UseTextOptions = true;
            this.DateComand_p.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand_p.AppearanceHeader.Options.UseTextOptions = true;
            this.DateComand_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateComand_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateComand_p.Caption = "���� �������";
            this.DateComand_p.ColumnEdit = this.leOrder;
            this.DateComand_p.DisplayFormat.FormatString = "d";
            this.DateComand_p.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateComand_p.FieldName = "Id_main";
            this.DateComand_p.Name = "DateComand_p";
            this.DateComand_p.OptionsColumn.AllowEdit = false;
            this.DateComand_p.Visible = true;
            this.DateComand_p.VisibleIndex = 0;
            this.DateComand_p.Width = 126;
            // 
            // leOrder
            // 
            this.leOrder.AutoHeight = false;
            this.leOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateOrder", "���� �������")});
            this.leOrder.DisplayMember = "DateOrder";
            this.leOrder.Name = "leOrder";
            this.leOrder.ValueMember = "Id";
            // 
            // WName
            // 
            this.WName.AppearanceHeader.Options.UseTextOptions = true;
            this.WName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.WName.Caption = "��� �����";
            this.WName.ColumnEdit = this.leWork;
            this.WName.FieldName = "Id_work";
            this.WName.Name = "WName";
            this.WName.OptionsColumn.AllowEdit = false;
            this.WName.Visible = true;
            this.WName.VisibleIndex = 1;
            this.WName.Width = 133;
            // 
            // leWork
            // 
            this.leWork.AutoHeight = false;
            this.leWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leWork.DisplayMember = "Name";
            this.leWork.Name = "leWork";
            this.leWork.ValueMember = "Id";
            // 
            // MName
            // 
            this.MName.AppearanceHeader.Options.UseTextOptions = true;
            this.MName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MName.Caption = "������";
            this.MName.ColumnEdit = this.leMobitel;
            this.MName.FieldName = "Id_mobitel";
            this.MName.Name = "MName";
            this.MName.OptionsColumn.AllowEdit = false;
            this.MName.Visible = true;
            this.MName.VisibleIndex = 2;
            this.MName.Width = 145;
            // 
            // leMobitel
            // 
            this.leMobitel.AutoHeight = false;
            this.leMobitel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leMobitel.DisplayMember = "Name";
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.ValueMember = "Id";
            // 
            // AName
            // 
            this.AName.AppearanceHeader.Options.UseTextOptions = true;
            this.AName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.AName.Caption = "�������� ������������";
            this.AName.ColumnEdit = this.leAgregat;
            this.AName.FieldName = "Id_agregat";
            this.AName.Name = "AName";
            this.AName.OptionsColumn.AllowEdit = false;
            this.AName.Visible = true;
            this.AName.VisibleIndex = 3;
            this.AName.Width = 263;
            // 
            // leAgregat
            // 
            this.leAgregat.AutoHeight = false;
            this.leAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leAgregat.DisplayMember = "Name";
            this.leAgregat.Name = "leAgregat";
            this.leAgregat.ValueMember = "Id";
            // 
            // Price
            // 
            this.Price.AppearanceHeader.Options.UseTextOptions = true;
            this.Price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Price.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Price.Caption = "��������, ���";
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 4;
            this.Price.Width = 124;
            // 
            // UName
            // 
            this.UName.AppearanceHeader.Options.UseTextOptions = true;
            this.UName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.UName.Caption = "��.���";
            this.UName.ColumnEdit = this.leUnit;
            this.UName.FieldName = "Id_unit";
            this.UName.Name = "UName";
            this.UName.OptionsColumn.AllowEdit = false;
            this.UName.Visible = true;
            this.UName.VisibleIndex = 5;
            this.UName.Width = 102;
            // 
            // leUnit
            // 
            this.leUnit.AutoHeight = false;
            this.leUnit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leUnit.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leUnit.DisplayMember = "Name";
            this.leUnit.Name = "leUnit";
            this.leUnit.ValueMember = "Id";
            // 
            // Comment_p
            // 
            this.Comment_p.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_p.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_p.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment_p.Caption = "����������";
            this.Comment_p.FieldName = "Comment";
            this.Comment_p.Name = "Comment_p";
            this.Comment_p.OptionsColumn.AllowEdit = false;
            this.Comment_p.Visible = true;
            this.Comment_p.VisibleIndex = 6;
            this.Comment_p.Width = 197;
            // 
            // gvUnit
            // 
            this.gvUnit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_u,
            this.Name_u,
            this.NameShort_u,
            this.Factor,
            this.Comment_u});
            this.gvUnit.GridControl = this.gcDict;
            this.gvUnit.Name = "gvUnit";
            this.gvUnit.DoubleClick += new System.EventHandler(this.gvUnit_DoubleClick);
            // 
            // Id_u
            // 
            this.Id_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_u.Caption = "Id";
            this.Id_u.FieldName = "Id";
            this.Id_u.Name = "Id_u";
            this.Id_u.OptionsColumn.AllowEdit = false;
            // 
            // Name_u
            // 
            this.Name_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Name_u.Caption = "��������";
            this.Name_u.FieldName = "Name";
            this.Name_u.Name = "Name_u";
            this.Name_u.OptionsColumn.AllowEdit = false;
            this.Name_u.Visible = true;
            this.Name_u.VisibleIndex = 0;
            this.Name_u.Width = 357;
            // 
            // NameShort_u
            // 
            this.NameShort_u.AppearanceHeader.Options.UseTextOptions = true;
            this.NameShort_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NameShort_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameShort_u.Caption = "������� �����������";
            this.NameShort_u.FieldName = "NameShort";
            this.NameShort_u.Name = "NameShort_u";
            this.NameShort_u.OptionsColumn.AllowEdit = false;
            this.NameShort_u.Visible = true;
            this.NameShort_u.VisibleIndex = 1;
            this.NameShort_u.Width = 161;
            // 
            // Factor
            // 
            this.Factor.AppearanceCell.Options.UseTextOptions = true;
            this.Factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Factor.AppearanceHeader.Options.UseTextOptions = true;
            this.Factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Factor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Factor.Caption = "�����������";
            this.Factor.FieldName = "Factor";
            this.Factor.Name = "Factor";
            this.Factor.OptionsColumn.AllowEdit = false;
            this.Factor.Visible = true;
            this.Factor.VisibleIndex = 2;
            this.Factor.Width = 156;
            // 
            // Comment_u
            // 
            this.Comment_u.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_u.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_u.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment_u.Caption = "����������";
            this.Comment_u.FieldName = "Comment";
            this.Comment_u.Name = "Comment_u";
            this.Comment_u.OptionsColumn.AllowEdit = false;
            this.Comment_u.Visible = true;
            this.Comment_u.VisibleIndex = 3;
            this.Comment_u.Width = 416;
            // 
            // gvWorks
            // 
            this.gvWorks.ActiveFilterEnabled = false;
            this.gvWorks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_work,
            this.Name_work,
            this.SpeedBottom,
            this.SpeedTop,
            this.Comment_work});
            this.gvWorks.GridControl = this.gcDict;
            this.gvWorks.Name = "gvWorks";
            this.gvWorks.OptionsBehavior.Editable = false;
            this.gvWorks.DoubleClick += new System.EventHandler(this.gvWorks_DoubleClick);
            // 
            // Id_work
            // 
            this.Id_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_work.Caption = "Id_work";
            this.Id_work.FieldName = "Id";
            this.Id_work.Name = "Id_work";
            this.Id_work.OptionsColumn.AllowEdit = false;
            // 
            // Name_work
            // 
            this.Name_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_work.Caption = "��������";
            this.Name_work.FieldName = "Name";
            this.Name_work.Name = "Name_work";
            this.Name_work.OptionsColumn.AllowEdit = false;
            this.Name_work.Visible = true;
            this.Name_work.VisibleIndex = 0;
            this.Name_work.Width = 286;
            // 
            // SpeedBottom
            // 
            this.SpeedBottom.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedBottom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedBottom.Caption = "�������� �� ��/���";
            this.SpeedBottom.FieldName = "SpeedBottom";
            this.SpeedBottom.Name = "SpeedBottom";
            this.SpeedBottom.OptionsColumn.AllowEdit = false;
            this.SpeedBottom.Visible = true;
            this.SpeedBottom.VisibleIndex = 1;
            this.SpeedBottom.Width = 120;
            // 
            // SpeedTop
            // 
            this.SpeedTop.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedTop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedTop.Caption = "�������� �� ��/���";
            this.SpeedTop.FieldName = "SpeedTop";
            this.SpeedTop.Name = "SpeedTop";
            this.SpeedTop.OptionsColumn.AllowEdit = false;
            this.SpeedTop.Visible = true;
            this.SpeedTop.VisibleIndex = 2;
            this.SpeedTop.Width = 120;
            // 
            // Comment_work
            // 
            this.Comment_work.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment_work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment_work.Caption = "����������";
            this.Comment_work.FieldName = "Comment";
            this.Comment_work.Name = "Comment_work";
            this.Comment_work.OptionsColumn.AllowEdit = false;
            this.Comment_work.Visible = true;
            this.Comment_work.VisibleIndex = 3;
            this.Comment_work.Width = 183;
            // 
            // ieCulture
            // 
            this.ieCulture.AutoHeight = false;
            this.ieCulture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ieCulture.Name = "ieCulture";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.NullDate = " ";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.NullText = " ";
            this.repositoryItemDateEdit2.NullValuePrompt = " ";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.pbStatus,
            this.lbStatus,
            this.sbtAdd,
            this.sbtDelete,
            this.sbtEdit,
            this.sbtRefresh,
            this.sbtExcel});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 7;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.sbtExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // sbtAdd
            // 
            this.sbtAdd.Caption = "��������";
            this.sbtAdd.Glyph = global::Agro.Properties.Resources.Add_16;
            this.sbtAdd.Id = 2;
            this.sbtAdd.Name = "sbtAdd";
            this.sbtAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtAdd_ItemClick);
            // 
            // sbtDelete
            // 
            this.sbtDelete.Caption = "�������";
            this.sbtDelete.Glyph = global::Agro.Properties.Resources.Remove_16;
            this.sbtDelete.Id = 3;
            this.sbtDelete.Name = "sbtDelete";
            this.sbtDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtDelete_ItemClick);
            // 
            // sbtEdit
            // 
            this.sbtEdit.Caption = "�������������";
            this.sbtEdit.Glyph = global::Agro.Properties.Resources.Edit_P_02_18;
            this.sbtEdit.Id = 4;
            this.sbtEdit.Name = "sbtEdit";
            this.sbtEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtEdit_ItemClick);
            // 
            // sbtRefresh
            // 
            this.sbtRefresh.Caption = "��������";
            this.sbtRefresh.Glyph = global::Agro.Properties.Resources.ref_16;
            this.sbtRefresh.Id = 5;
            this.sbtRefresh.Name = "sbtRefresh";
            this.sbtRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.sbtRefresh_ItemClick);
            // 
            // sbtExcel
            // 
            this.sbtExcel.Caption = "������� � Excel";
            this.sbtExcel.Glyph = global::Agro.Properties.Resources.doc_excel_table;
            this.sbtExcel.Id = 6;
            this.sbtExcel.Name = "sbtExcel";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pbStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.lbStatus)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // pbStatus
            // 
            this.pbStatus.Caption = "barEditItem1";
            this.pbStatus.Edit = this.repositoryItemProgressBar1;
            this.pbStatus.Id = 0;
            this.pbStatus.Name = "pbStatus";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // lbStatus
            // 
            this.lbStatus.Caption = "������";
            this.lbStatus.Id = 1;
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // xtbView
            // 
            this.xtbView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtbView.Location = new System.Drawing.Point(0, 28);
            this.xtbView.Name = "xtbView";
            this.xtbView.SelectedTabPage = this.xtraTabPage1;
            this.xtbView.Size = new System.Drawing.Size(966, 654);
            this.xtbView.TabIndex = 4;
            this.xtbView.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(959, 625);
            this.xtraTabPage1.Text = "�����������";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tlDictionary);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcDict);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(959, 625);
            this.splitContainerControl1.SplitterPosition = 299;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tlDictionary
            // 
            this.tlDictionary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.DicCode,
            this.DicName});
            this.tlDictionary.CustomizationFormBounds = new System.Drawing.Rectangle(60, 661, 208, 168);
            this.tlDictionary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlDictionary.Location = new System.Drawing.Point(0, 0);
            this.tlDictionary.Name = "tlDictionary";
            this.tlDictionary.BeginUnboundLoad();
            this.tlDictionary.AppendNode(new object[] {
            null,
            "���� �����"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������ �����"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "����"}, 1, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� ��������� �������"}, 2, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "�������� ������������"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� � ���������"}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������"}, 6, 1, 1, -1);
            this.tlDictionary.EndUnboundLoad();
            this.tlDictionary.OptionsBehavior.ExpandNodeOnDrag = false;
            this.tlDictionary.SelectImageList = this.imCollection;
            this.tlDictionary.Size = new System.Drawing.Size(299, 625);
            this.tlDictionary.TabIndex = 0;
            this.tlDictionary.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlDictionary_FocusedNodeChanged);
            // 
            // DicCode
            // 
            this.DicCode.Caption = "���";
            this.DicCode.FieldName = "���";
            this.DicCode.Name = "DicCode";
            // 
            // DicName
            // 
            this.DicName.AppearanceHeader.Options.UseTextOptions = true;
            this.DicName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DicName.Caption = "�������� �����������";
            this.DicName.FieldName = "�������� �����������";
            this.DicName.MinWidth = 73;
            this.DicName.Name = "DicName";
            this.DicName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.DicName.Visible = true;
            this.DicName.VisibleIndex = 0;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(959, 625);
            this.xtraTabPage2.Text = "������";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(959, 625);
            this.xtraTabPage3.Text = "������ �� ��������";
            // 
            // MainAgro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 705);
            this.Controls.Add(this.xtbView);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MainAgro";
            this.Text = "AGRO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.gvGFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leFieldGrope)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCalt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ieCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).EndInit();
            this.xtbView.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarEditItem pbStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraBars.BarStaticItem lbStatus;
        private DevExpress.XtraBars.BarButtonItem sbtAdd;
        private DevExpress.XtraBars.BarButtonItem sbtDelete;
        private DevExpress.XtraBars.BarButtonItem sbtEdit;
        private DevExpress.XtraBars.BarButtonItem sbtRefresh;
        private DevExpress.XtraBars.BarButtonItem sbtExcel;
        private DevExpress.XtraTab.XtraTabControl xtbView;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTreeList.TreeList tlDictionary;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicName;
        private DevExpress.XtraGrid.GridControl gcDict;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGFields;
        private DevExpress.XtraGrid.Views.Grid.GridView gvWorks;
        private DevExpress.XtraGrid.Columns.GridColumn Id_work;
        private DevExpress.XtraGrid.Columns.GridColumn Name_work;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedBottom;
        private DevExpress.XtraGrid.Columns.GridColumn SpeedTop;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_work;
        private DevExpress.XtraGrid.Columns.GridColumn Id_gf;
        private DevExpress.XtraGrid.Columns.GridColumn Name_gf;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_gf;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFields;
        private DevExpress.XtraGrid.Columns.GridColumn Id_f;
        private DevExpress.XtraGrid.Columns.GridColumn FGROUPE;
        private DevExpress.XtraGrid.Columns.GridColumn FZONE;
        private DevExpress.XtraGrid.Columns.GridColumn Name_f;
        private DevExpress.XtraGrid.Columns.GridColumn Square;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_f;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCalt;
        private DevExpress.XtraGrid.Columns.GridColumn Id_c;
        private DevExpress.XtraGrid.Columns.GridColumn Name_c;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_c;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_a;
        private DevExpress.XtraGrid.Columns.GridColumn Name_a;
        private DevExpress.XtraGrid.Columns.GridColumn Width;
        private DevExpress.XtraGrid.Columns.GridColumn Identifier;
        private DevExpress.XtraGrid.Columns.GridColumn Def;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_a;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOrder;
        private DevExpress.XtraGrid.Columns.GridColumn Id_o;
        private DevExpress.XtraGrid.Columns.GridColumn DateInit;
        private DevExpress.XtraGrid.Columns.GridColumn DateComand;
        private DevExpress.XtraGrid.Columns.GridColumn Name_o;
        private DevExpress.XtraGrid.Columns.GridColumn Comment;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPrice;
        private DevExpress.XtraGrid.Columns.GridColumn Id_p;
        private DevExpress.XtraGrid.Columns.GridColumn DateComand_p;
        private DevExpress.XtraGrid.Columns.GridColumn WName;
        private DevExpress.XtraGrid.Columns.GridColumn MName;
        private DevExpress.XtraGrid.Columns.GridColumn AName;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn UName;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_p;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leFieldGrope;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leZone;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUnit;
        private DevExpress.XtraGrid.Columns.GridColumn Id_u;
        private DevExpress.XtraGrid.Columns.GridColumn Name_u;
        private DevExpress.XtraGrid.Columns.GridColumn NameShort_u;
        private DevExpress.XtraGrid.Columns.GridColumn Factor;
        private DevExpress.XtraGrid.Columns.GridColumn Comment_u;
        private DevExpress.XtraGrid.Columns.GridColumn Icon;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit ieCulture;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit peIcon;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leWork;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leMobitel;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leAgregat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leUnit;
    }
}