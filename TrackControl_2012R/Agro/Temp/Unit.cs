using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using Agro.Dictionaries;
using Agro.Utilites;

namespace Agro.Temp
{
    public partial class Unit : Agro.Temp.FormSample
    {
        public Unit()
        {
            InitThis();
        }
        public Unit(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroUnit(_Id);
            this.Text = this.Text + ": " + "������� ��������� �������"; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP) |
                !DicUtilites.ValidateFieldsTextWFocus(txNameShort, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(txFactor, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["NameShort"], txNameShort.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Factor"], txFactor.Text);
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroUnit).NameShort = txNameShort.Text;
            (di as DictionaryAgroUnit).Factor = Convert.ToDouble (txFactor.Text) ;
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            txNameShort.Text = (di as DictionaryAgroUnit).NameShort;
            txFactor.Text = (di as DictionaryAgroUnit).Factor.ToString();
        }

        private void InitThis()
        {
            InitializeComponent();
        }

        private void txNameShort_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void txFactor_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}

