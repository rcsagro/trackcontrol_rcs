﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraBars;

namespace Agro.Utilites
{
    /// <summary>
    /// контроль интервалом дат в журналах 
    /// </summary>
    public class ControlDataInterval
    {
        DateTime _begin;
        public DateTime Begin
        {
            get
            {
                return _begin;
            }
            set
            {
                if (_begin == value)
                    return;
                _begin = value;
            }
        }
        DateTime _end;
        public DateTime End
        {
            get
            {
                return _end;
            }
            set
            {
                if (_end == value)
                    return;
            }
        }

        public ControlDataInterval(DateTime Begin,DateTime End)
        {
            _begin = Begin;
            _end = End;
        }
        public void UpdateInterval(BarEditItem Begin,BarEditItem End,bool ChangeEnd)
        {
            DateTime _newEnd;
            DateTime _newBegin;
            if (Begin.EditValue == null || End.EditValue == null) return;
            if (DateTime.TryParse(End.EditValue.ToString(), out _newEnd) && DateTime.TryParse(Begin.EditValue.ToString(), out _newBegin))
            {
                if (_newEnd < _newBegin)
                {
                    if (ChangeEnd)
                    {
                        _end = new DateTime(_begin.Year, _begin.Month, _begin.Day, 23, 59, 59);

                        End.EditValue = _end;
                    }
                    else
                    {
                        _begin = new DateTime(_end.Year, _end.Month, _end.Day, 0, 0, 0);
                        Begin.EditValue = _begin;
                    }
                    return;
                }
                if (ChangeEnd && (_newEnd.Subtract(_end).Days != 0 || _newEnd.Day !=_end.Day ))
                {
                    _end = new DateTime(_newEnd.Year, _newEnd.Month, _newEnd.Day, 23, 59, 59);
                    End.EditValue = _end;
                }
                if (!ChangeEnd && (_newBegin.Subtract(_begin).Days != 0 || _newBegin.Day != _begin.Day))
                {
                    _begin = new DateTime(_newBegin.Year, _newBegin.Month, _newBegin.Day, 0, 0, 0);
                    Begin.EditValue = _begin;
                }

            }
        }
        
    }
}
