using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid.Views.Grid;
using GeoData;
using TrackControl.General.DatabaseDriver;

namespace Agro.Utilites
{
    public static class DicUtilites
    {
    #region Combo
                public static bool ComboSet(DataGridView dgvGrn, System.Windows.Forms.ComboBox  cb, string sId, string sName)
                {
                    if (dgvGrn.SelectedRows[0].Cells[sId].Value != DBNull.Value)
                    {
                        cb.SelectedValue = (int)dgvGrn.SelectedRows[0].Cells[sId].Value;
                        cb.SelectedText = Convert.ToString(dgvGrn.SelectedRows[0].Cells[sName].Value);
                        return true;
                    }
                    else
                        return false;

                }
                public static bool ComboSet(GridView gvGrn, System.Windows.Forms.ComboBox cb, string sId, string sName)
            {
                if (gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sId) != DBNull.Value)
                {
                    cb.SelectedValue = (int)gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sId);
                    cb.SelectedText = Convert.ToString(gvGrn.GetRowCellValue(gvGrn.FocusedRowHandle, sName));
                    return true;
                }
                else
                    return false;

            }


            #endregion
    #region LookUp
            public static void LookUpLoad(LookUpEdit leWork, string sSQL)
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    DataTable dtWork = driverDb.GetDataTable(sSQL);
                    leWork.Properties.DataSource = dtWork;
                    leWork.Properties.DisplayMember = dtWork.Columns[1].ColumnName;
                    leWork.Properties.ValueMember = dtWork.Columns[0].ColumnName;
                    // ������� ��������� ������
                    LookUpColumnInfoCollection clsLE = leWork.Properties.Columns;
                    clsLE.Clear();
                    clsLE.Add(new LookUpColumnInfo(dtWork.Columns[1].ColumnName, 0, ""));
                    //  Set column widths according to their contents and resize the popup, if required.
                    leWork.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                    //// Enable auto completion search mode.
                    //leWork.Properties.SearchMode = SearchMode.AutoComplete;
                    //// Specify the column against which to perform the search.
                    //leWork.Properties.AutoSearchColumnIndex = 1;
                }
            }
            #endregion
    #region Validation
            public static bool ValidateCombo(System.Windows.Forms.ComboBox  cbTest, ErrorProvider errP)
            {
                    if (cbTest.Items.Count !=0 )
                    {
                        if (cbTest.SelectedValue == null || cbTest.SelectedValue.GetType().Name !="Int32" )
                        {
                            errP.SetError(cbTest, Resources.ValueSelect);
                            cbTest.Focus();
                            return false;
                        }
                        else
                        {
                            errP.SetError(cbTest, "");
                            return true;
                        }
                    }
                    else
                    {
                        errP.SetError(cbTest, Resources.ValueSelect);
                        cbTest.Focus();
                        return false;
                    }

            }
            public static bool ValidateLookUp(LookUpEdit leWork, DXErrorProvider dxErrP)
            {
                //Console.WriteLine(leWork.Name);      
                if (leWork.EditValue == null)
                    {
                        dxErrP.SetError(leWork, Resources.ValueSelect);
                        leWork.Focus();
                        return false;
                    }
                    else if (leWork.EditValue.ToString()  == "0")
                    {
                        dxErrP.SetError(leWork, Resources.ValueSelect);
                        leWork.Focus();
                        return false;
                    }
                    else 
                    {
                        dxErrP.SetError(leWork, "");
                        return true;
                    }
            }
            public static bool ValidateLookUp(LookUpEdit leWork)
            {
                if (leWork.EditValue == null)
                {
                    return false;
                }
                else if (leWork.EditValue.ToString() == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            public static bool ValidateDateEdit(DateEdit deWork, DXErrorProvider dxErrP)
            {
                DateTime dtWork;
                if (!DateTime.TryParse(deWork.DateTime.ToString(),out dtWork ))
                {
                    dxErrP.SetError(deWork, Resources.DateSelect);
                    deWork.Focus();
                    return false;
                }
                else if (deWork.DateTime.Subtract(DateTime.MinValue).TotalDays == 0)
                {
                    dxErrP.SetError(deWork, Resources.DateSelect);
                    deWork.Focus();
                    return false;
                }
                else
                {
                    dxErrP.SetError(deWork, "");
                    return true;
                }
            }
            public static bool ValidateDateEditPair(DateEdit deMin, DateEdit deMax, DXErrorProvider dxErrP)
            {
                if (ValidateDateEdit(deMax, dxErrP) && ValidateDateEdit(deMin, dxErrP))
                {
                    if (deMax.DateTime.Subtract(deMin.DateTime).TotalMinutes >= 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            public static bool ValidateCombo(System.Windows.Forms.ComboBox cbTest)
            {
                if (cbTest.Items.Count != 0)
                {
                    if (cbTest.SelectedValue == null || cbTest.SelectedValue.GetType().Name != "Int32")
                    {

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }

            }
            public static bool ValidateFieldsText(TextBox txTest, ErrorProvider errP)
            {
                if (txTest.Text.Trim().Length == 0)
                {
                    errP.SetError(txTest, Resources.ValueNEmpty);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    errP.SetError(txTest, "");
                    return true;
                }
            }

            public static bool ValidateFieldsCombo(ComboBoxEdit cbTest, DXErrorProvider errP)
            {
                if (cbTest.EditValue  == null)
                {
                    errP.SetError(cbTest, Resources.ValueNEmpty);
                    cbTest.Focus();
                    return false;
                }
                else
                {
                    errP.SetError(cbTest, "");
                    return true;
                }
            } 
            public static bool ValidateFieldsText(TextEdit txTest, DXErrorProvider errP)
            {
                if (txTest.Text.Trim().Length == 0)
                {
                    errP.SetError(txTest, Resources.ValueNEmpty);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    errP.SetError(txTest, "");
                    return true;
                }
            }
            public static bool ValidateFieldsTextWFocus(TextEdit txTest, DXErrorProvider errP)
            {
                if (txTest.Text.Trim().Length == 0)
                {
                    errP.SetError(txTest, Resources.ValueNEmpty);
                    return false;
                }
                else
                {
                    errP.SetError(txTest, "");
                    return true;
                }
            }
            public static bool ValidateFieldsTextDoublePositive(TextBox txTest, ErrorProvider errP)
            {
                double dbOut;
                if (!Double.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    if (dbOut < 0)
                    {
                        errP.SetError(txTest, Resources.ValueMustPositive);
                        txTest.Focus();
                        return false;
                    }
                    else
                    {
                        errP.SetError(txTest, "");
                        return true;
                    }
                }
            }

            public static bool ValidateFieldsTextDoublePositive(TextEdit txTest, DXErrorProvider errP)
            {
                double dbOut;
                if (!Double.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    if (dbOut < 0)
                    {
                        errP.SetError(txTest, Resources.ValueMustPositive);
                        txTest.Focus();
                        return false;
                    }
                    else
                    {
                        errP.SetError(txTest, "");
                        return true;
                    }
                }
            }

            public static bool ValidateFieldsTextIntPositive(TextEdit txTest, DXErrorProvider errP)
            {
                int dbOut;
                if (!Int32.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    if (dbOut < 0)
                    {
                        errP.SetError(txTest, Resources.ValueMustPositive);
                        txTest.Focus();
                        return false;
                    }
                    else
                    {
                        errP.SetError(txTest, "");
                        return true;
                    }
                }
            }

            public static bool ValidateFieldsTextDoublePositiveWFocus(TextEdit txTest, DXErrorProvider errP)
            {
                double dbOut;
                if (!Double.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    return false;
                }
                else
                {
                    if (dbOut < 0)
                    {
                        errP.SetError(txTest, Resources.ValueMustPositive);
                        return false;
                    }
                    else
                    {
                        errP.SetError(txTest, "");
                        return true;
                    }
                }
            }
            public static bool ValidateFieldsTextDouble(TextBox txTest, ErrorProvider errP)
            {
                double dbOut;
                if (!Double.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    txTest.Focus();
                    return false;
                }
                else
                {
                        errP.SetError(txTest, "");
                        return true;
                }
            }



            public static bool ValidateFieldsTextDouble(TextEdit txTest, DXErrorProvider errP)
            {
                double dbOut;
                if (!Double.TryParse(txTest.Text, out dbOut))
                {
                    errP.SetError(txTest, Resources.ValueMustDigital);
                    txTest.Focus();
                    return false;
                }
                else
                {
                    errP.SetError(txTest, "");
                    return true;
                }
            }

            /// <summary>
            /// �������� ������������ ���������� �������������� ��������
            /// </summary>
            public static bool ValidateIdentifier(TextBox txTest, ErrorProvider errP)
            {
                int cursorPos = txTest.SelectionStart;
                string tmp = txTest.Text.Trim();

                // ������� ������� �������, ������� �� �������� �������.
                for (int i = tmp.Length - 1; i >= 0; i--)
                {
                    if (!Char.IsDigit(tmp[i]))
                    {
                        tmp = tmp.Remove(i, 1);
                        cursorPos = i;
                    }
                }

                // �������� �� ������������ ��������.
                if ((!String.IsNullOrEmpty(tmp)) && (Convert.ToUInt32(tmp) > UInt16.MaxValue))
                {
                    errP.SetError(txTest, String.Format(
                      Resources.ValueMustDiapason,
                      0, UInt16.MaxValue));
                    txTest.Text = "";
                    txTest.Focus();
                    return false;
                }
                else
                {
                    txTest.Text = tmp;
                    txTest.SelectionStart = cursorPos;
                    return true;
                }
            }
            #endregion
    #region UNIQUE
            //public struct UNIQUE
            //{
            //    string sTable,
            //    string sField,
                
            //}
            public static bool Test_UNIQUE(string sTable, string sField, int iValue,int iId_Where)
            {
                int cnt = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string SQLselect = string.Format( AgroQuery.DicUtilites.SelectSensors, sTable, sField, iValue, iId_Where );
                    cnt = Convert.ToInt32(driverDb.GetScalarValue(SQLselect));
                }
                if (cnt == 0)
                    return true;
                else
                    return false;
                
            }
            #endregion

    #region GetDatabaseValues
            public static int GetDriverId_Mobitel(int Mobitel_Id)
            {
                int DriverId = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 
                    string SQLselect = AgroQuery.DicUtilites.SelectDriverId + Mobitel_Id;

                    DriverId = driverDb.GetScalarValueNull<Int32>(SQLselect,0);
                }
                return DriverId;
            }
            public static SensorAtributs GetDriverAtributes(ushort Ident)
            {
                SensorAtributs sa = new SensorAtributs(0, "");
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 

                    string _SQL = AgroQuery.DicUtilites.SelectFromDriver + Ident;

                    driverDb.GetDataReader (_SQL);
                    if (driverDb.Read())
                    {
                        sa.Id = driverDb.GetInt32("id");
                        sa.Name = driverDb.GetString("Family");
                    }
                    driverDb.CloseDataReader();
                }
                return sa;
            }
            public static ushort GetDriverIdent(int Id)
            {
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 

                    string _sSql = AgroQuery.DicUtilites.SelectDriverIdentifier + Id;

                    return (ushort)driverDb.GetScalarValueNull<Int32>(_sSql,0); 
                }
            }

            public static void GetZonePoints(int Zone_Id, ref List<TRealPoint> rpoints)
            {
                string _sSql = string.Format(AgroQuery.DicUtilites.SelectPointsLatLon, Zone_Id);

                MapUtilites.SetRealPoints(_sSql, ref rpoints);
            }

            public static double GetZoneSquare(int Zone_ID)
            {
                double dbSquare = 0;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb(); 

                    string _sSql = AgroQuery.DicUtilites.SelectZones + Zone_ID;

                    dbSquare = Math.Round(100 * driverDb.GetScalarValueNull<Double>(_sSql, 0), 2);
                }
                return dbSquare;
            }
            public static string GetMobitelName(int Mobitel_Id)
            {
                string MobName = "";
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    string SQLselect = AgroQuery.DicUtilites.SelectFromVehicle + Mobitel_Id;
                    
                    MobName = driverDb.GetScalarValue(SQLselect).ToString() ;
                }
                return MobName;
            }
    #endregion
    }
}
