using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Utilites
{
    public interface IFormDictionary
    {
        /// <summary>
        /// �������� �� ������������ ����� ������
        /// </summary>
         bool ValidateFields();
         /// <summary>
         /// �������� ��� ���������� ������
         /// </summary>
        bool ValidateFieldsAction();
        /// <summary>
        /// ���������� ���������
        /// </summary>
        void SaveChanges(bool question);
        /// <summary>
        /// �������� �� ������� ������ � ��������� ��������
        /// </summary>
         bool TestLinks();
        /// <summary>
        /// �������� ������
        /// </summary>
         bool DeleteRecord(bool question);
        /// <summary>
        /// ��������� ��������� ����� � ����������� �� �� ��������� 
        /// </summary>
         void SetControls();
         /// <summary>
         /// �������� �������� ����� 
         /// </summary>
        void GetFields();
        /// <summary>
        /// �������� �������� ����� 
        /// </summary>
        void SetFields();
        /// <summary>
        /// ���������� ������ � ������� ����������� ����� 
        /// </summary>
        void UpdateGRN();
    }
}
