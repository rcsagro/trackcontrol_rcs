using System;
using System.Collections.Generic;
using System.Text;

namespace Agro
{
    interface IDictionary
    {
        /// <summary>
        /// �������� �� ������������ ����� ������
        /// </summary>
        bool ValidateFields();
        /// <summary>
        /// ���������� ���������
        /// </summary>
        void SaveChanges();
        /// <summary>
        /// �������� �� ������� ������ � ��������� ��������
        /// </summary>
        bool TestLinks();
        /// <summary>
        /// �������� ������
        /// </summary>
        bool DeleteRecord();

    }
}
