using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using TrackControl.General ;

namespace Agro.Utilites
{
    public static class Consts
    {
        public const double SPEED_MIN = 2;
        public const double SPEED_MAX = 5;
        public const double MILE_2_KM = 1.852;

        public static String APP_DATA_ROUTER = String.Format("{0}{1}{2}", Globals.APP_DATA, Path.DirectorySeparatorChar,
            "Router");

        public static String APP_DATA_AGRO = String.Format("{0}{1}{2}", Globals.APP_DATA, Path.DirectorySeparatorChar,
            "Agro");

        public const int PROGRESS_BAR_STOP = -1;
        public const int ROUTE_LIMIT = 100;
        public const int AGRO_LIMIT = 100;

        public enum OrderRegimes : int
        {
            ������ = 0,
            ��������������,
            ������
        }

        /// <summary>
        /// ������� ���������� �������� �������
        /// </summary>
        public enum TypeControlObject
        {
            Driver = 1,
            Agregat,
            Field,
            Confirm
        }

        /// <summary>
        /// ������� ��������
        /// </summary>
        public enum RouteEvent
        {
            Entry = 1,
            Exit
        }

        /// <summary>
        /// ���� ��������
        /// </summary>
        public enum RouteType
        {
            ChackZone = 1,
            Location
        }

        public const int L_NUMBER_AGRO = 10;
        public const int L_NUMBER_ROUTE = 11;

        public enum RegimeType
        {
            Demo = 1,
            Work
        }

        //-----------------------------
        //TIME values may range from '-838:59:59' to '838:59:59'.
        public const int MY_SQL_DATATYPE_TIME_MAX_HOUER = 838;

        public const int ZONE_OUTSIDE_THE_LIST = -1; //������������� ������ � �����,��� �� ���������� �� ����� 

    }
}
