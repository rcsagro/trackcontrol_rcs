using System;
using System.Collections.Generic;
using System.Text;
using LocalCache;
using TrackControl.General;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Agro.Utilites
{
    /// <summary>
    /// ������� ��������� ���������� atlantaDataSet
    /// </summary>
    public class LocalCacheItem
    {
        //atlantaDataSet.mobitelsRow m_row;
        int _Mobitel_Id = 0;
        DateTime _dtStart;
        DateTime _dtEnd;
        string CONNECTION_STRING = ConnectMySQL.ConnectionString();
        bool bDataSetReady = false;
        atlantaDataSet _dsAtlanta;
        //BackgroundWorker - ������������� � MainForm TrackControl
        System.ComponentModel.BackgroundWorker bwCreateData = new System.ComponentModel.BackgroundWorker();
        public LocalCacheItem(DateTime dtStart, DateTime dtEnd, int Mobitel_Id)
        {
            _dtStart = dtStart;
            _dtEnd = dtEnd;
            _Mobitel_Id = Mobitel_Id;
            
        }
        public int CreateDSAtlanta(ref atlantaDataSet dsAtlanta)
        {
            _dsAtlanta = dsAtlanta;
            //�������� ������� ������ �� ���� ���� � DataGPS ��� ��������� ������
            //if (RecordsDataGPS() == 0) return 0;
            if (dsAtlanta.dataview.Rows.Count > 0) return dsAtlanta.dataview.Rows.Count;
            // ������� �������
            dsAtlanta.dataview.Clear();
            //int k = dsAtlanta.mobitels.Rows.Count;
            if (dsAtlanta.mobitels.Rows.Count == 0)
            {
                FillMobitelsTable();
                FillSensorsTable();
                FillStateTable();
                FillTransitionTable();
                FillRelationAlgorithmsTable();
                FillSensorCoefficientTable();
                FillSettingTable();
                FillTeamTable();
                FillDriverTable();
                FillVehicleTable();
                
            }
            atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
            if (m_row == null) return 0;
            m_row.Check = true;
            bwCreateData.WorkerReportsProgress = true;
            bwCreateData.DoWork += new System.ComponentModel.DoWorkEventHandler(bwCreateData_DoWork);
            bwCreateData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bwCreateData_RunWorkerCompleted);
            bwCreateData.RunWorkerAsync();
            while (!bDataSetReady)
            {
                Application.DoEvents();
                continue;
            }
            return dsAtlanta.dataview.Rows.Count;

        }
        private void bwCreateData_RunWorkerCompleted(object sender, EventArgs e)
        {
            bDataSetReady = true;
        }
        private void bwCreateData_DoWork(object sender, EventArgs e)
        {
            LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter taDataView = 
              new LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter(CONNECTION_STRING);
            string name = "";
            taDataView.Fill(_dsAtlanta, _dtStart, _dtEnd, bwCreateData, ref name);
        }

        private int RecordsDataGPS() -----------
        {
            ConnectMySQL cnMySQL = new ConnectMySQL();
            string SQLselect = "";
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                SQLselect = "SELECT datagps.DataGps_ID "
                + " FROM  datagps WHERE   FROM_UNIXTIME(datagps.UnixTime) >= ?TimeStart"
                + " AND FROM_UNIXTIME(datagps.UnixTime) <= ?TimeLast"
                + " AND datagps.Mobitel_ID = " + _Mobitel_Id
                + " AND datagps.speed > 0 and datagps.Valid=1";
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SQLselect = "SELECT datagps.DataGps_ID "
                + " FROM  datagps WHERE   dbo.FROM_UNIXTIME(datagps.UnixTime) >= @TimeStart"
                + " AND dbo.FROM_UNIXTIME(datagps.UnixTime) <= @TimeLast"
                + " AND datagps.Mobitel_ID = " + _Mobitel_Id
                + " AND datagps.speed > 0 and datagps.Valid=1";
            }

            MySqlParameter[] parDate = new MySqlParameter[2];
            parDate[0] = new MySqlParameter(driverDb.ParamPrefics + "TimeStart", MySqlDbType.DateTime);
            parDate[0].Value = _dtStart;
            parDate[1] = new MySqlParameter( driverDb.ParamPrefics + "TimeLast", MySqlDbType.DateTime );
            //parDate[1].Value = _dtEnd.AddDays(1).ToString("dd.MM.yyyy");
            parDate[1].Value = _dtEnd;
            int iRecords = cnMySQL.GetDataReaderRecordsCount(SQLselect, parDate);
            cnMySQL.CloseMySQLConnection();
            return iRecords;
        }
        private void FillMobitelsTable()
        {
            MobitelsProvider mobitelsTableAdapter = new MobitelsProvider(CONNECTION_STRING);
            mobitelsTableAdapter.FillMobitelsTable(_dsAtlanta.mobitels);
        }
        private void FillStateTable()
        {
            LocalCache.atlantaDataSetTableAdapters.StateTableAdapter stateTableAdapter;
            stateTableAdapter = new LocalCache.atlantaDataSetTableAdapters.StateTableAdapter();
            stateTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            stateTableAdapter.Fill(_dsAtlanta.State);
        }
        private void FillTransitionTable()
        {
            LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter transTableAdapter;
            transTableAdapter = new LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter();
            transTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            transTableAdapter.Fill(_dsAtlanta.Transition);
        } 
        private void FillSensorsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter sensorsTableAdapter;
            sensorsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();
            sensorsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensorsTableAdapter.Fill(_dsAtlanta.sensors);
        } // FillSensorsTable()

        /// <summary>
        /// ���������� ������� RelationAlgorithms
        /// </summary>
        private void FillRelationAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter relationalgorithmsTableAdapter;
            relationalgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();
            relationalgorithmsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            relationalgorithmsTableAdapter.Fill(_dsAtlanta.relationalgorithms);
        }

        /// <summary>
        /// ���������� ������� SensorCoefficient
        /// </summary>
        private void FillSensorCoefficientTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter sensorcoefficientTableAdapter;
            sensorcoefficientTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter();
            sensorcoefficientTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensorcoefficientTableAdapter.Fill(_dsAtlanta.sensorcoefficient);
        }

        /// <summary>
        /// ���������� ������� SensorAlgorithms
        /// </summary>
        private void FillSensorAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter sensoralgorithmsTableAdapter;
            sensoralgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter();
            sensoralgorithmsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensoralgorithmsTableAdapter.Fill(_dsAtlanta.sensoralgorithms);
        }
        private void FillSettingTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
            settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
            settingTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            settingTableAdapter.Fill(_dsAtlanta.setting);
        }
        private void FillDriverTable()
        {
            LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter driverTableAdapter;
            driverTableAdapter = new LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter();
            driverTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            driverTableAdapter.Fill(_dsAtlanta.driver);
        } // FillDriverTable()

        /// <summary>
        /// ���������� ������� Team
        /// </summary>
        private void FillTeamTable()
        {
            LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter teamTableAdapter;
            teamTableAdapter = new LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter();
            teamTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            teamTableAdapter.Fill(_dsAtlanta.team);
        }

        /// <summary>
        /// ���������� ������� Vehicle
        /// </summary>
        private void FillVehicleTable()
        {
            LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
            vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
            vehicleTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            vehicleTableAdapter.Fill(_dsAtlanta.vehicle);
        }
    }
}
