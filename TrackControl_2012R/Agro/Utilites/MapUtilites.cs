using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using GeoData;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace Agro.Utilites
{
    public static class MapUtilites
    {
        /// <summary>
        /// ������ ��������� ��� ������������ �������� 
        /// </summary>
        /// <param name="SQLselect"></param>
        /// <param name="rpoints"></param>
        public static void SetRealPoints(string SQLselect, ref List<TRealPoint> rpoints)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                driverDb.GetDataReader(SQLselect);

                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while (driverDb.Read())
                    {
                        TRealPoint rpoint = new TRealPoint();
                        rpoint.X = driverDb.GetDouble("Lon") / 600000;
                        rpoint.Y = driverDb.GetDouble("Lat") / 600000;
                        rpoints.Add(rpoint);
                    }
                }
                else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while( driverDb.Read() )
                    {
                        TRealPoint rpoint = new TRealPoint();
                        int k = driverDb.GetOrdinal("Lon");
                        rpoint.X = Convert.ToDouble(driverDb.GetValue( k ).ToString()) / 600000;
                        k = driverDb.GetOrdinal("Lat");
                        rpoint.Y = Convert.ToDouble(driverDb.GetValue( k ).ToString()) / 600000;
                        rpoints.Add( rpoint );
                    }
                }
                driverDb.CloseDataReader(); 
            }
        }
        /// <summary>
        /// ������� �� �������� �������� �������
        /// </summary>
        /// <param name="gisMap"></param>
        /// <param name="rpoints"></param>
        public static void SetRectangle(GDL gisMap, List<TRealPoint> rpoints)
        {
            TRealPoint[] arRpoints = new TRealPoint[rpoints.Count];
            rpoints.CopyTo(arRpoints);
            SetRectangle(gisMap, arRpoints);

        }
        public static void SetRectangle(GDL gisMap, TRealPoint[] rpoints)
        {
            
            TRealRect rect = new TRealRect();
            for (int i = 0; i < rpoints.Length; i++)
            {
                double Lat = rpoints[i].Y;
                double Lon = rpoints[i].X;
                if (i == 0)
                {
                    rect.XMax = Lon;
                    rect.XMin = Lon;
                    rect.YMin = Lat;
                    rect.YMax = Lat;
                }
                //Console.WriteLine("Draw {0} ����� � {3}  {1}.{2} LatLon {4}.{5}", _ZoneId, spoints[i].X, spoints[i].Y, i, Math.Round(Lat, 3), Math.Round(Lon, 3)); 
                if (Lon > rect.XMax)
                    rect.XMax = Lon;
                if (Lon < rect.XMin)
                    rect.XMin = Lon;
                if (Lat > rect.YMax)
                    rect.YMax = Lat;
                if (Lat < rect.YMin)
                    rect.YMin = Lat;
            }
            double dbScale = 0;
            rect.XMax = rect.XMax + dbScale * (rect.XMax - rect.XMin);
            rect.XMin = rect.XMin - dbScale * (rect.XMax - rect.XMin);
            rect.YMax = rect.YMax + dbScale * (rect.YMax - rect.YMin);
            rect.YMin = rect.YMin - dbScale * (rect.YMax - rect.YMin);
            gisMap.Map.NoEraseBackground();
            gisMap.Map.View.ViewRect(ref rect);
        }

        public static void DrawPoligon(Graphics gr, Color color, System.Drawing.Point[] spoints)
        {
            GraphicsPath path = new GraphicsPath();
            path.AddLines(spoints);
            path.CloseFigure();
            gr.DrawPath(new Pen(Color.Gray, 1), path);
            Color veryTransparentColor = Color.FromArgb(77, color.R, color.G, color.B);
            SolidBrush shadowBrush = new SolidBrush(veryTransparentColor);
            gr.FillPath(shadowBrush, path);
        }
        /// <summary>
        /// ����� ������ ������ ��� ��������� ������ ������� �� ��������� ����
        /// </summary>
        /// <param name="color_delta"></param>
        /// <returns></returns>
        public static Color GetColorForRecordsSeparation(int color_delta)
        {
            Color color = Color.Gray;
            switch (color_delta % 10)
            {
                case 0:
                    {
                        color = Color.Yellow;
                        break;
                    }
                case 1:
                    {
                        color = Color.Blue;
                        break;
                    }
                case 2:
                    {
                        color = Color.BlueViolet;
                        break;
                    }
                case 3:
                    {
                        color = Color.Aqua;
                        break;
                    }
                case 4:
                    {
                        color = Color.Orange;
                        break;
                    }
                case 5:
                    {
                        color = Color.Chartreuse;
                        break;
                    }
                case 6:
                    {
                        color = Color.Black;
                        break;
                    }
                case 7:
                    {
                        color = Color.Firebrick;
                        break;
                    }
                case 8:
                    {
                        color = Color.Coral;
                        break;
                    }
                case 9:
                    {
                        color = Color.Indigo;
                        break;
                    }
                default:
                    {
                        if (color_delta % 2 == 0)
                            color = Color.Gray;
                        else
                            color = Color.DarkSalmon;
                        break;
                    }
                    
            }
            return color;
        }
        /// <summary>
        /// ���������� ��� ���������� �����
        /// </summary>
        public static float sizeEndCap = 4F;
        /// <summary>
        /// ������������ ����������� �� �������� ��� �����
        /// </summary>
        /// <returns></returns>
        public static Pen GetLineCap()
        {
            // ������������ ��������� �����
            GraphicsPath path = new GraphicsPath();

            PointF p1 = new PointF(0, 0);
            PointF p2 = new PointF(-1 * sizeEndCap / 3, -1 * (sizeEndCap + 0));
            PointF p3 = new PointF(0, -1 * sizeEndCap);
            PointF p4 = new PointF(sizeEndCap / 3, -1 * (sizeEndCap + 0));
            //PointF p1 = new PointF(0, 0);
            //PointF p2 = new PointF(-1 * sizeEndCap / 3, (sizeEndCap + 0));
            //PointF p3 = new PointF(0,  sizeEndCap);
            //PointF p4 = new PointF(sizeEndCap / 3, (sizeEndCap + 0));
            PointF[] points = { p1, p2, p3, p4, p1 };
            path.AddLines(points);
            CustomLineCap cust =  new CustomLineCap(path, null);
            Pen endLine = new Pen(Color.DarkBlue, 1);
            endLine.CustomEndCap = cust;
            return endLine;
        }
        /// <summary>
        /// ��������� ���������� ����� ����� ������� � ����������� ���������
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static bool CheckLength(System.Drawing.Point p1, System.Drawing.Point p2, int size)
        {
            if (Math.Sqrt(Math.Pow((Math.Abs(p1.X) - Math.Abs(p2.X)), 2)
              + Math.Pow((Math.Abs(p1.Y) - Math.Abs(p2.Y)), 2)) > size)
            {
                return true;
            }
            return false;
        }

        public static System.Drawing.Point Geo2BmpPix(TRealPoint rpoint, int Level)
        {
            int LevelSize = (int)Math.Pow(2, 8 + Level);
            System.Drawing.Point p = new System.Drawing.Point();
            p.X = (int)(LevelSize / 2 + rpoint.X * LevelSize / 360);
            double k = Math.Sin(rpoint.Y * Math.PI / 180);
            p.Y = (int)(LevelSize / 2 - 0.5 * Math.Log((1 + k) / (1 - k)) * LevelSize / (2 * Math.PI));
            return p;
        }
        public static System.Drawing.Point Geo2BmpPixLevel(TRealPoint rpoint)
        {
            return Geo2BmpPix(rpoint, 15);
        }
        public static List<TRealPoint> GetZonePonts(int Zone_Id)
        {
            List<TRealPoint> rpoints = new List<TRealPoint>();

            string SQLselect = string.Format(AgroQuery.MapUtilites.SelectPoints, Zone_Id);

            SetRealPoints(SQLselect, ref rpoints);
            return rpoints;
        }
    }
}
