using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using TrackControl.General;
using GeoData;
using System.Drawing;
namespace Agro.Utilites
{
    public  class CompressData
    {
        private int _Id_main;
        private string _TableName;
        private string _sSQL;

        private readonly double Lon_KF = 600000;
        private readonly double Lat_KF = 600000;
        private readonly double Speed_KF = 1.852;

        public delegate Point Map2Screen(TRealPoint rpoint);
        public  CompressData(string sTableName,int Id_main)
        {
            _Id_main = Id_main;
            _TableName = sTableName;
        }
        public bool Compress(List<TRealPointSpeed> rspoints)
        {
            if (rspoints.Count == 0) return false;
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                _sSQL = "DELETE FROM " + _TableName + " WHERE Id_main = " + _Id_main;
                _cnMySQL.ExecuteNonQueryCommand(_sSQL);
                int Max_Lench = 255;
                CompressOneField Lon = new CompressOneField(Max_Lench, Lon_KF);
                CompressOneField Lat = new CompressOneField(Max_Lench, Lat_KF);
                CompressOneField Speed = new CompressOneField(Max_Lench, Speed_KF);
                bool bFirst = true;
                foreach (TRealPointSpeed rspoint in rspoints)
                {
                    if (bFirst)
                    {
                        Lon.StartRecord(rspoint.X);
                        Lat.StartRecord(rspoint.Y);
                        Speed.StartRecord(rspoint.Speed);
                        bFirst = false;
                    }
                    else
                    {
                        if (Lon.AddValueTest(rspoint.X) && Lat.AddValueTest(rspoint.Y) && Speed.AddValueTest(rspoint.Speed))
                        {
                            Lon.AddValue(rspoint.X);
                            Lat.AddValue(rspoint.Y);
                            Speed.AddValue(rspoint.Speed);
                        }
                        else ---------
                        {
                            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                            {
                                _sSQL = "INSERT INTO " + _TableName + " (Id_main,Lon_base,Lat_base,Speed_base,Lon_dev,Lat_dev,Speed_dev)"
                                + " VALUES (" + _Id_main + ",?Lon_base,?Lat_base,?Speed_base,?Lon_dev,?Lat_dev,?Speed_dev)";
                            }
                            else if(DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                _sSQL = "INSERT INTO " + _TableName + " (Id_main,Lon_base,Lat_base,Speed_base,Lon_dev,Lat_dev,Speed_dev)"
                                + " VALUES (" + _Id_main + ",@Lon_base,@Lat_base,@Speed_base,@Lon_dev,@Lat_dev,@Speed_dev)";
                            }

                            MySqlParameter[] pars = new MySqlParameter[6];
                            pars[0] = new MySqlParameter(driverDb.ParamPrefics + "Lon_base", MySqlDbType.Double);
                            pars[0].Value = Lon.Val_base;
                            pars[1] = new MySqlParameter(driverDb.ParamPrefics + "Lat_base", MySqlDbType.Double);
                            pars[1].Value = Lat.Val_base;
                            pars[2] = new MySqlParameter(driverDb.ParamPrefics + "Speed_base", MySqlDbType.Double);
                            pars[2].Value = Speed.Val_base;
                            pars[3] = new MySqlParameter(driverDb.ParamPrefics + "Lon_dev", MySqlDbType.String);
                            pars[3].Value = Lon.SDev;
                            pars[4] = new MySqlParameter(driverDb.ParamPrefics + "Lat_dev", MySqlDbType.String);
                            pars[4].Value = Lat.SDev;
                            pars[5] = new MySqlParameter(driverDb.ParamPrefics + "Speed_dev", MySqlDbType.String);
                            pars[5].Value = Speed.SDev;
                            _cnMySQL.ExecuteNonQueryCommand(_sSQL, pars);
                            Lon.StartRecord(rspoint.X);
                            Lat.StartRecord(rspoint.Y);
                            Speed.StartRecord(rspoint.Speed);
                        }
                    }


                }
                if (Lon.SDev.Length > 0)
                {
                    if(DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                    _sSQL = "INSERT INTO " + _TableName + " (Id_main,Lon_base,Lat_base,Speed_base,Lon_dev,Lat_dev,Speed_dev)"
                    + " VALUES (" + _Id_main + ",?Lon_base,?Lat_base,?Speed_base,?Lon_dev,?Lat_dev,?Speed_dev)";
                    }
                    else if(DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                         _sSQL = "INSERT INTO " + _TableName + " (Id_main,Lon_base,Lat_base,Speed_base,Lon_dev,Lat_dev,Speed_dev)"
                    + " VALUES (" + _Id_main + ",@Lon_base,@Lat_base,@Speed_base,@Lon_dev,@Lat_dev,@Speed_dev)";
                    }
                    MySqlParameter[] pars = new MySqlParameter[6];
                    pars[0] = new MySqlParameter(driverDb.ParamPrefics + "Lon_base", MySqlDbType.Double);
                    pars[0].Value = Lon.Val_base;
                    pars[1] = new MySqlParameter(driverDb.ParamPrefics + "Lat_base", MySqlDbType.Double);
                    pars[1].Value = Lat.Val_base;
                    pars[2] = new MySqlParameter(driverDb.ParamPrefics + "Speed_base", MySqlDbType.Double);
                    pars[2].Value = Speed.Val_base;
                    pars[3] = new MySqlParameter(driverDb.ParamPrefics + "Lon_dev", MySqlDbType.String);
                    pars[3].Value = Lon.SDev;
                    pars[4] = new MySqlParameter(driverDb.ParamPrefics + "Lat_dev", MySqlDbType.String);
                    pars[4].Value = Lat.SDev;
                    pars[5] = new MySqlParameter(driverDb.ParamPrefics + "Speed_dev", MySqlDbType.String);

                    pars[5].Value = Speed.SDev;
                    _cnMySQL.ExecuteNonQueryCommand(_sSQL, pars);
                }
            }
            return true; --
        }
        public List<PointSpeed> DeCompressSpeed(Map2Screen MapToScreen)
        {
            List<PointSpeed> rspoints = new List<PointSpeed>();
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                _sSQL = "SELECT  " + _TableName + ".* FROM  " + _TableName + " WHERE   " + _TableName + ".Id_main = " + _Id_main
                + " ORDER BY   " + _TableName + ".Id";
                MySqlDataReader dr = _cnMySQL.GetDataReader(_sSQL);
                while (dr.Read())
                {
                    DeCompressOneField Lon = new DeCompressOneField(dr.GetDouble("Lon_base"), dr.GetString("Lon_dev"), Lon_KF);
                    DeCompressOneField Lat = new DeCompressOneField(dr.GetDouble("Lat_base"), dr.GetString("Lat_dev"),Lat_KF );
                    DeCompressOneField Speed = new DeCompressOneField(dr.GetDouble("Speed_base"), dr.GetString("Speed_dev"),Speed_KF);
                    List<double> lstLon = Lon.Decompress();
                    List<double> lstLat = Lat.Decompress();
                    List<double> lstSpeed = Speed.Decompress();
                    PointSpeed rspoint;
                    TRealPoint rpoint;
                    Point spoint ;
                    int Min_lenght = Math.Min(lstLon.Count, Math.Min(lstSpeed.Count,lstLat.Count));
                    for (int i = 0; i < Min_lenght; i++)
                    {
                        rpoint.X = lstLon[i];
                        rpoint.Y = lstLat[i];
                        spoint = MapToScreen(rpoint);
                        rspoint.X = spoint.X;
                        rspoint.Y = spoint.Y;
                        rspoint.Speed = lstSpeed[i];
                        rspoints.Add(rspoint); 
                    }
                }
            }
            return rspoints;
        }
        public List<Point> DeCompress(Map2Screen MapToScreen,out double dbWidthPixelMeter)
        {
            List<Point> spoints = new List<Point>();
            dbWidthPixelMeter = 0;
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                _sSQL = "SELECT  " + _TableName + ".* FROM  " + _TableName + " WHERE   " + _TableName + ".Id_main = " + _Id_main
                + " ORDER BY   " + _TableName + ".Id";
                MySqlDataReader dr = _cnMySQL.GetDataReader(_sSQL);

                while (dr.Read())
                {
                    DeCompressOneField Lon = new DeCompressOneField(dr.GetDouble("Lon_base"), dr.GetString("Lon_dev"), Lon_KF);
                    DeCompressOneField Lat = new DeCompressOneField(dr.GetDouble("Lat_base"), dr.GetString("Lat_dev"), Lat_KF);
                    List<double> lstLon = Lon.Decompress();
                    List<double> lstLat = Lat.Decompress();
                    TRealPoint rpoint;
                    Point spoint;
                    int Min_lenght = Math.Min(lstLon.Count,  lstLat.Count);
                    for (int i = 0; i < Min_lenght; i++)
                    {
                        rpoint.X = lstLon[i];
                        rpoint.Y = lstLat[i];
                        spoint = MapToScreen(rpoint);
                        spoints.Add(spoint); 
                            //������ ����� � �������� ��� ���������� ����� � ���������
                        if (i > 0 && dbWidthPixelMeter == 0)
                        {
                            double dist = 1000 * �GeoDistance.CalculateFromGrad(lstLat[i - 1], lstLon[i - 1], lstLat[i], lstLon[i]);
                            if (dist > 0)
                            {
                                dbWidthPixelMeter=(Math.Pow(Math.Pow((spoints[i].X - spoints[i - 1].X), 2) + Math.Pow((spoints[i].Y - spoints[i - 1].Y), 2), 0.5)) / dist;
                            }
                        }
                        
                    }
                    
                }
            }
            return spoints;
        }
        public List<TRealPoint> DeCompressReal()
        {
            List<TRealPoint> rpoints = new List<TRealPoint>();
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                _sSQL = "SELECT  " + _TableName + ".* FROM  " + _TableName + " WHERE   " + _TableName + ".Id_main = " + _Id_main
                + " ORDER BY   " + _TableName + ".Id";
                MySqlDataReader dr = _cnMySQL.GetDataReader(_sSQL);
                while (dr.Read())
                {
                    DeCompressOneField Lon = new DeCompressOneField(dr.GetDouble("Lon_base"), dr.GetString("Lon_dev"), Lon_KF);
                    DeCompressOneField Lat = new DeCompressOneField(dr.GetDouble("Lat_base"), dr.GetString("Lat_dev"), Lat_KF);
                    List<double> lstLon = Lon.Decompress();
                    List<double> lstLat = Lat.Decompress();
                    TRealPoint rpoint;
                    int Min_lenght = Math.Min(lstLon.Count, lstLat.Count);
                    for (int i = 0; i < Min_lenght; i++)
                    {
                        rpoint.X = lstLon[i];
                        rpoint.Y = lstLat[i];
                        rpoints.Add(rpoint);

                    }

                }
            }
            return rpoints;
        }
        public struct TRealPointSpeed 
        {
            public double X;
            public double Y;
            public double Speed;
        }
        public struct PointSpeed
        {
            public int X;
            public int Y;
            public double Speed;
        }
        private class CompressOneField 
        {
            int _val_base;
            public int Val_base
            {
                get { return _val_base; }
                set { _val_base = value; }
            }
            int _val_cur;
            string _sCompress;
            public string SDev
            {
                get {
                    if (_sCompress.Length == 0)
                        return _sCompress;
                    else
                    _sCompress = _sCompress.Substring(0,_sCompress.Length-1);
                    CompactString();
                    return _sCompress;
                }
                set { _sCompress = value; }
            }
            int _iMax;
            double _iKF;

            public CompressOneField(int iMax, double iKF)
            {
                _iMax = iMax;
                _iKF = iKF;
            }
            public void AddValue(double val)
            {
                int delta = (int)(_val_cur - Math.Round(val * _iKF, 0));
                _val_cur = (int)Math.Round(val * _iKF, 0);
                string new_value = delta.ToString();
                _sCompress = _sCompress + delta.ToString() + Separators.Sep_Main;

            }
            public bool AddValueTest(double val)
            {
                int delta = (int)(_val_cur - Math.Round(val * _iKF, 0));
                if ((delta.ToString().Length + 1 + _sCompress.Length) < _iMax)
                    return true;
                else
                    CompactString();
                    if ((delta.ToString().Length + 1 + _sCompress.Length) < _iMax)
                        return true;
                    else
                        return false;
            }
            public void StartRecord(double val)
            {
                _val_base = (int)Math.Round(val * _iKF,0);
                _val_cur = _val_base;
                _sCompress = "";
            }
            private void CompactString()
            {
                for (int i = 0; i <= Separators.Sep_Repl.GetUpperBound(1)  ; i++)
                {
                    _sCompress = _sCompress.Replace(Separators.Sep_Repl[0,i], Separators.Sep_Repl[1,i]);
                }
            }
        }
        private class DeCompressOneField
        {
            private double _val_base;
            public double Val_base
            {
                get { return _val_base; }
                set { _val_base = value; }
            }
            private string _sCompress;
            public string sCompress
            {
                get { return _sCompress; }
                set { _sCompress = value; }
            }
            double _iKF;
            public DeCompressOneField(double val_base, string compress, double iKF)
            {
                _iKF = iKF;
                _val_base = val_base;
                _sCompress = compress;
                
            }
            public List<double> Decompress()
            {
                List<double> values = new List<double>();
                    for (int i = 0; i <= Separators.Sep_Repl.GetUpperBound(1); i++)
                    {
                        _sCompress = _sCompress.Replace(Separators.Sep_Repl[1, i], Separators.Sep_Repl[0, i]);
                    }
                    string[] increments = _sCompress.Split(Separators.Sep_Main);
                    
                    values.Add(Math.Round(_val_base / _iKF, 8));
                    double value_work = _val_base;
                    //System.Diagnostics.Debug.Print(_sCompress);   
                    for (int i = 0; i < increments.Length; i++)
                    {
                        {
                            int val;
                            if (Int32.TryParse(increments[i],out val))
                            {
                            value_work = value_work - val;
                            values.Add(Math.Round(value_work / _iKF, 8));
                            }
                        }
                    }
                return values;
            }

        }
        public static class Separators
        {
            public static readonly char Sep_Main = '/';
            public static readonly string[,] Sep_Repl = new string[,] 
            {
                { "0/", "-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9"},
                { "A", "B", "C", "D", "E", "F", "G", "H", "K", "M"}
            };
        }
    }
}
