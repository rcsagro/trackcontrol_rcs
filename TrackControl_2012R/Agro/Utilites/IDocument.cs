using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Utilites
{
    public interface IDocument
    {
        /// <summary>
        /// ��� ���������
        /// </summary>
        
        int ID
        {
            get;
            set;
        }

        /// <summary>
        /// ���� ���������
        /// </summary>
        DateTime Date
        {
            get;
            set;
        }

        /// <summary>
        /// ���������� ���������
        /// </summary>
        string Remark
        {
            get;
            set;
        }
        /// <summary>
        /// �������� ���������
        /// </summary>
        int AddDoc();
        /// <summary>
        /// �������������� ���������
        /// </summary>
        bool UpdateDoc();
        /// <summary>
        /// �������� ���������
        /// </summary>
        bool DeleteDoc();

        /// <summary>
        /// ���������� ��������� � ����� ���������
        /// </summary>
        bool SaveDoc();

        /// <summary>
        /// �������� ���������
        /// </summary>
        bool LoadDoc();

        /// <summary>
        /// ���������� ������ ����������
        /// </summary>
        bool AddCont();

        /// <summary>
        /// �������� ������ ����������
        /// </summary>
        bool DeleteCont();

        /// <summary>
        /// ���������� ������ ����������
        /// </summary>
        bool SaveCont();

        /// <summary>
        /// �������� ����������
        /// </summary>
        bool LoadCont();

        /// <summary>
        /// �������� ������� ������ ���������
        /// </summary>
        bool DeleteDocTest();

        /// <summary>
        /// �������� ������� ������ ������ ����������
        /// </summary>
        bool DeleteContTest();
    }
}
