using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Repository;
using TrackControl.General.DatabaseDriver;
using TrackControl.Hasp;

namespace Agro.Utilites
{
    public static class TotUtilites
    {
        public static DateTime ConvertFromUnixTimes(int timestamp)
        {
            //DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            //return origin.AddSeconds(timestamp);

            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return dtDateTime.AddSeconds(timestamp).ToLocalTime();
        }

        /// <summary>
        /// проверка величины на DBNull(
        /// </summary>
        /// <param name="obValue"></param>
        /// <param name="retVal"></param>
        /// <returns></returns>
        public static object NDBNull(object obValue, object retVal)
        {
            if (obValue != DBNull.Value)
                return obValue;
            else
                return retVal;
        }

        /// <summary>
        /// проверка поля DataReader на DBNull
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="sFieldName"></param>
        /// <param name="retVal"></param>
        /// <returns></returns>
        public static object NdbNullReader(object drs, string sFieldName, object retVal)
        {
            var dr = drs as DriverDb;
            if (dr == null)
                throw new Exception("Type DataReader is not present!");

            if (dr.GetValue(dr.GetOrdinal(sFieldName)) == DBNull.Value)
                return retVal;
            else
                return dr.GetValue(dr.GetOrdinal(sFieldName));
        }

        public static void SetButtonExecuting(BarButtonItem btnStopExecutData, bool visible)
        {
            if (visible)
            {
                btnStopExecutData.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnStopExecutData.Visibility = BarItemVisibility.Never;
            }
        }

        public static void SetProgressBar(BarEditItem pbStatus,  int iValue)
        {
            if (iValue == Consts.PROGRESS_BAR_STOP)
            {
                pbStatus.Visibility = BarItemVisibility.Never;
                pbStatus.EditValue = 0;
                Application.DoEvents();
                return;
            }
            if ((pbStatus.Visibility == BarItemVisibility.Never))
            {
                pbStatus.EditValue = 0;
                (pbStatus.Edit as RepositoryItemProgressBar).Maximum = iValue;
                //rpbStatus.Maximum = iValue;
                pbStatus.Visibility = BarItemVisibility.Always;
            }
            else
            {
                pbStatus.EditValue = iValue;
                //if (rpbStatus.Maximum == iValue)
                 if((pbStatus.Edit as RepositoryItemProgressBar).Maximum == iValue)
                {
                    pbStatus.Visibility = BarItemVisibility.Never;
                    pbStatus.EditValue = 0;
                }
            }
            Application.DoEvents();
        }

        public static string GetMainFormCaption(string MainCaption, int L_NUMBER)
        {
            string Caption = "";

            //HaspState State = HaspService.GetHaspNumberState(L_NUMBER);
            HaspState State = HaspWrap.GetHaspNumberState(L_NUMBER);
            //HaspState State = Agro.GlobalVars.ResultLicenseAgro;

            if (L_NUMBER == Consts.L_NUMBER_AGRO)
            {
                if (HaspState.OK == State)
                {
                    GlobalVars.g_AGRO = (int) Consts.RegimeType.Work;
                    Caption = MainCaption + " - AGRO";
                }
                else
                {
                    GlobalVars.g_AGRO = (int) Consts.RegimeType.Demo;
                    Caption = MainCaption + " - AGRO (DEMO - " + Consts.AGRO_LIMIT + ")";
                }
            }
            else if (L_NUMBER == Consts.L_NUMBER_ROUTE)
            {
                if (HaspState.OK == State)
                {
                    GlobalVars.g_ROUTE = (int) Consts.RegimeType.Work;
                    Caption = MainCaption;
                }
                else
                {
                    GlobalVars.g_ROUTE = (int) Consts.RegimeType.Demo;
                    Caption = MainCaption + " (DEMO - " + Consts.ROUTE_LIMIT + ")";
                }
            }

            return Caption;
        }

        public static void FormSize800_600(Form f)
        {
            if (Screen.PrimaryScreen.WorkingArea.Width <= 960)
            {
                f.WindowState = FormWindowState.Maximized;
            }
            else
            {
                f.Width = 9*Screen.PrimaryScreen.WorkingArea.Width/10;
                f.Height = 5*Screen.PrimaryScreen.WorkingArea.Height/6;
            }
        }

        public static void ToolTipChangeText(DevExpress.XtraBars.BarItem ctr, string newText)
        {
            ctr.SuperTip.Items.Clear();
            DevExpress.Utils.ToolTipItem toolTipItem = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip = new DevExpress.Utils.SuperToolTip();
            toolTipItem.Text = newText;
            superToolTip.Items.Add(toolTipItem);
            ctr.SuperTip = superToolTip;
        }

        public static DateTime GetOrderDate(DateTime start)
        {
            AgroSetItem asi = new AgroSetItem();
            TimeSpan tsWork = asi.TimeStartOrderTimeSpan;
            DateTime orderDate = start;
            orderDate = new DateTime(orderDate.Year, orderDate.Month, orderDate.Day, tsWork.Hours, tsWork.Minutes, 0);
            return orderDate;
        }


        public static void SetTimeParams(DateTime start, DateTime end, DriverDb driverDb)
        {
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", start);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
        }
    }
}
