using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;

namespace Agro
{
    public partial class GraficSensor : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// ��� ������� ��� ������� : 
        /// (int)AlgorithmType.AGREGAT 
        /// (int)AlgorithmType.DRIVER
        /// </summary>
        private short _Type;
        /// <summary>
        /// ������ ���� �� ������� ���� �� ���������
        /// </summary>
        private enum SENSOR_REGIMES
        {
            TT = 1,
            SENSOR
        }
        public GraficSensor(short inpType)
        {
            InitializeComponent();
            Localization();
            _Type = inpType; 
            bdeStartReportAgr.EditValue = DateTime.Today;
            bdeEndReportAgr.EditValue = DateTime.Today.AddDays(1).AddMinutes(-1);
            rgCommutRegime.EditValue = (int)SENSOR_REGIMES.TT; 
            SelectGridView();
        }
        public void Reset()
        {
            SelectGridView();
            gcReportAgr.DataSource = null;
            chartSensors.Series.Clear();
        }
        public void  ExportExcel()
        {
            string Title = "";
            switch (_Type)
            {
                case (int)AlgorithmType.AGREGAT:
                    {
                        Title = Resources.AgregatIdentification;
                        break;
                    }
                case (int)AlgorithmType.DRIVER:
                    {
                        Title = Resources.DriverIdentification;
                        break;
                    }
            }
            string xslFilePath =StaticMethods.ShowSaveFileDialog(Title + StaticMethods.SuffixDateToFileName(), "Excel", "Excel|*.xls");
            if (xslFilePath.Length > 0)
            {
                gvReportAgr.OptionsPrint.ExpandAllDetails = true;
                gvReportAgr.OptionsPrint.PrintDetails = true;
                gvReportAgr.OptionsPrint.AutoWidth = true;
                gvReportAgr.ExportToXls(xslFilePath);
                System.Diagnostics.Process.Start(xslFilePath);
            }
        }
        private void btStartReportAgr_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadGraficSensors();
        }

        #region GRID
        private void gvListTT_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.ToString() == "Sel")
            {
                DataRow[] rows = new DataRow[gvListTT.RowCount];
                for (int i = 0; i < rows.Length; i++)
                {
                    if (e.RowHandle == i)
                    {
                        bool Sel = (bool)gvListTT.GetRowCellValue(i, gvListTT.Columns["Sel"]);
                        gvListTT.SetRowCellValue(i, gvListTT.Columns["Sel"], !Sel);
                    }
                    else
                    {
                        rows[i] = gvListTT.GetDataRow(i);
                        rows[i]["Sel"] = false;
                    }
                }
            }
        }
        private void gvListAgr_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.ToString() == "Sel")
            {
                DataRow[] rows = new DataRow[gvListAgr.RowCount];
                for (int i = 0; i < rows.Length; i++)
                {
                    if (e.RowHandle == i)
                    {
                        bool Sel = (bool)gvListAgr.GetRowCellValue(i, gvListAgr.Columns["Sel"]);
                        gvListAgr.SetRowCellValue(i, gvListAgr.Columns["Sel"], !Sel);
                    }
                    else
                    {
                        rows[i] = gvListAgr.GetDataRow(i);
                        rows[i]["Sel"] = false;
                    }
                }
            }
        }
        private void gvListDrv_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.ToString() == "Sel")
            {
                DataRow[] rows = new DataRow[gvListDrv.RowCount];
                for (int i = 0; i < rows.Length; i++)
                {
                    if (e.RowHandle == i)
                    {
                        bool Sel = (bool)gvListDrv.GetRowCellValue(i, gvListDrv.Columns["Sel"]);
                        gvListDrv.SetRowCellValue(i, gvListDrv.Columns["Sel"], !Sel);
                    }
                    else
                    {
                        rows[i] = gvListDrv.GetDataRow(i);
                        rows[i]["Sel"] = false;
                    }
                }
            }
        }
        private int GetRowChecked(GridView gv)
        {
            for (int i = 0; i < gv.RowCount; i++)
            {
                bool Sel = (bool)gv.GetRowCellValue(i, gv.Columns["Sel"]);
                if (Sel) return i;
            }
            return ConstsGen.RECORD_MISSING;
        }
        #endregion

        private void SensorGetFactData(int SensorId)
        {
            RepSensor rs = null;
            switch (_Type)
            {
                case (int)AlgorithmType.AGREGAT:
                    {
                        rs = new RepSensorAgr(SensorId, (DateTime)bdeStartReportAgr.EditValue, (DateTime)bdeEndReportAgr.EditValue,
                            (int)rgCommutRegime.EditValue == (int)SENSOR_REGIMES.TT ? ReportsList.AGR_SENSOR_TT : ReportsList.AGR_SENSOR_AGR);
                        break;
                    }
                case (int)AlgorithmType.DRIVER:
                    {
                        rs = new RepSensorDrv(SensorId, (DateTime)bdeStartReportAgr.EditValue, (DateTime)bdeEndReportAgr.EditValue,
                            (int)rgCommutRegime.EditValue == (int)SENSOR_REGIMES.TT ? ReportsList.DRV_SENSOR_TT : ReportsList.DRV_SENSOR_DRV);
                        break;
                    }
                default :
                    return;
            }
            //RepSensorAgr ra = new RepSensorAgr(SensorId, (DateTime)bdeStartReportAgr.EditValue, (DateTime)bdeEndReportAgr.EditValue,
            //    (int)rgCommutRegime.EditValue == (int)SENSOR_REGIMES.TT ? ReportsList.AGR_SENSOR_TT : ReportsList.AGR_SENSOR_AGR);
            rs.ChangeStatusEvent += SetStatusParent;
            rs.ChangeProgressBar += SetProgressParent;
            rs.ChangeButtonBar += SetButtonParent;
            DataTable dt = rs.GetReport();
            gcReportAgr.DataSource = SensorConvertData(dt);
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSQL = AgroQuery.GraficSensor.SelectVehicleMobitel;

                rleMobitel.DataSource = driverDb.GetDataTable(sSQL);
                //sSQL = " SELECT   agro_agregat.Id,   agro_agregat.Name FROM agro_agregat";
                //rleAgregat.DataSource = cnMySQL.GetDataTable(sSQL);
            }

            SetStatusParent(Resources.GrafCreate);

            if ((int)rgCommutRegime.EditValue == (int)SENSOR_REGIMES.TT)
            {
                CreateChartSetSeriesSensor(dt);
            }
            else
            {
                CreateChartSetSeriesTT(dt);
            }

            rs.ChangeStatusEvent -= SetStatusParent;
            rs.ChangeProgressBar -= SetProgressParent;
            SetButtonParent( false );
            rs.ChangeButtonBar -= SetButtonParent;
            SetStatusParent(Resources.Ready);
        }
        private void CreateChartSetSeriesSensor(DataTable dt)
        {
            chartSensors.Series.Clear();
            // ����������� ������� ���������
            List<SensorAtributs> Agregats = new List<SensorAtributs>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int Agregat = (int)dt.Rows[i]["SensorInBase_Id"];
                short State = (short)Convert.ToInt32(dt.Rows[i]["State"]);
                if ((Agregat != 0) && (State == 1))
                {
                    SensorAtributs sa = new SensorAtributs((int)dt.Rows[i]["SensorInBase_Id"], dt.Rows[i]["SensorName"].ToString());
                    if (!Agregats.Contains(sa))
                    {
                        Agregats.Add(sa);
                    }
                }
            }
            if (Agregats.Count == 0) return;
            // ���������� �������� ��������� ������� ��������
            DataColumn col;
            for (int i = 0; i < Agregats.Count; i++)
            {
                col = new DataColumn("S" + Agregats[i].Id, System.Type.GetType("System.Int32"));
                dt.Columns.Add(col);
            }
            //���������� �������� ��������� ��������� , ����������� �� 1 ��� �������� ��������� �������
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //int State = (int)dt.Rows[i]["State"];
                int Agregat = (int)dt.Rows[i]["SensorInBase_Id"];
                for (int j = 0; j < Agregats.Count; j++)
                {
                    if (Agregats[j].Id == Agregat)
                    {
                        dt.Rows[i]["S" + Agregats[j].Id] = j * 2 + 1;
                    }
                    else
                    {
                        dt.Rows[i]["S" + Agregats[j].Id] = j * 2;
                    }
                    //Debug.Print("time {0} state {1}", dt.Rows[i]["TimeEvent"], dt.Rows[i]["S" + Agregats[j].Id]); 
                }
            }
            // ����������� ����� ��� ������� �������
            SetProgressParent(Agregats.Count);
            for (int i = 0; i < Agregats.Count; i++)
            {
                DataView dv = dt.DefaultView;
                CreateSeries(dv, Agregats[i]);
                SetProgressParent(i);
                Application.DoEvents();  
            }
            SetProgressParent(Consts.PROGRESS_BAR_STOP); 
            //-----------------------------------------------
        }
        private void CreateChartSetSeriesTT(DataTable dt)
        {
            chartSensors.Series.Clear();
            // ����������� ������� ����������
            List<int> TTs = new List<int>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int TT = (int)dt.Rows[i]["Mobitel_ID"];
                short State = (short)Convert.ToInt32(dt.Rows[i]["State"]);
                if ((TT > 0) && (State == 1))
                {
                    if (!TTs.Contains(TT))
                    {
                        TTs.Add(TT);
                    }
                }
            }
            if (TTs.Count == 0) return;
            //// ������� �� ������ ������� ��
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    bool TTexist = false;
            //    int TT = (int)dt.Rows[i]["Mobitel_ID"];
            //    for (int j = 0; j < TTs.Count; j++)
            //    {
            //        if (TT ==TTs[j] )
            //        {
            //            TTexist=true;
            //            break;
            //        }
            //    }
            //    if (!TTexist)
            //}
            // ���������� �������� ��������� ������� ���������
            DataColumn col;
            for (int i = 0; i < TTs.Count; i++)
            {
                col = new DataColumn("S" + TTs[i], System.Type.GetType("System.Int32"));
                dt.Columns.Add(col);
            }
            //���������� �������� ��������� ��������� , ����������� �� 1 ��� �������� ��������� �������
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int State = (int)dt.Rows[i]["State"];
                int TT = (int)dt.Rows[i]["Mobitel_ID"];
                for (int j = 0; j < TTs.Count; j++)
                {
                    if ((TTs[j] == TT) && (State == 1))
                    {
                        dt.Rows[i]["S" + TTs[j]] = j * 2 + 1;
                    }
                    else
                    {
                        dt.Rows[i]["S" + TTs[j]] = j * 2;
                    }
                    //Debug.Print("time {0} state {1}", dt.Rows[i]["TimeEvent"], dt.Rows[i]["S" + TT]); 
                }
            }
            //TotUtilites.DebugPrintDataTable(dt);  
            SetProgressParent(TTs.Count); 
            // ����������� ����� ��� ������� �������
            for (int i = 0; i < TTs.Count; i++)
            {

                string TT_name = DicUtilites.GetMobitelName(TTs[i]);
                SensorAtributs sa = new SensorAtributs(TTs[i], TT_name);
                DataView dv = dt.DefaultView;
                dv.RowFilter = "Mobitel_ID = " + TTs[i];
                CreateSeries(dv, sa);
                SetProgressParent(i);
                Application.DoEvents();  
            }
            SetProgressParent(Consts.PROGRESS_BAR_STOP); 
            //-----------------------------------------------
        }
        private void CreateSeries(DataView dv, SensorAtributs sa)
        {
            Series series = new Series(sa.Name, ViewType.StepLine);
            series.DataSource = dv;
            series.ArgumentScaleType = ScaleType.DateTime;
            series.ArgumentDataMember = "TimeEvent";
            series.ValueScaleType = ScaleType.Numerical;
            series.ValueDataMembers.AddRange(new string[] { "S" + sa.Id });
            StepLineSeriesView serView = (StepLineSeriesView)series.View;
            serView.LineMarkerOptions.Visible = false;
            series.Label.Visible = false;
            chartSensors.Series.Add(series);
        }
        private void LoadGraficSensors()
        {

            if ((((DateTime)bdeStartReportAgr.EditValue).Subtract((DateTime)bdeEndReportAgr.EditValue)).TotalMinutes > 0)
            {
                XtraMessageBox.Show(Resources.DatesControl, Resources.ApplicationName);
                return;
            }
            switch ((int)rgCommutRegime.EditValue)
            {
                case ((int)SENSOR_REGIMES.TT):
                    {
                        if (gvListTT.RowCount == 0)
                        {
                            XtraMessageBox.Show(Resources.CarListAbsence, Resources.ApplicationName);
                            return;
                        }
                        int RowChecked = GetRowChecked(gvListTT);
                        if (RowChecked == ConstsGen.RECORD_MISSING)
                        {
                            XtraMessageBox.Show(Resources.CarSelect, Resources.ApplicationName);
                            return;
                        }
                        int TTId;
                        if (Int32.TryParse(gvListTT.GetRowCellValue(RowChecked, gvListTT.Columns["mobitel_id"]).ToString(), out TTId))
                        {
                            SensorGetFactData(TTId);
                        }
                        else
                        {
                            XtraMessageBox.Show(Resources.CarSelect, Resources.ApplicationName);
                        }
                        break;
                    }
                case ((int)SENSOR_REGIMES.SENSOR):
                    {
                        switch (_Type)
                        {
                            case (int)AlgorithmType.AGREGAT:
                                {
                                    SensorSelect(gvListAgr);
                                    break;
                                }
                            case (int)AlgorithmType.DRIVER:
                                {
                                    SensorSelect(gvListDrv); 
                                    break;
                                }
                        }
                        break;
                    }
            }
        }
        /// <summary>
        /// �������� ����� � �������������� ��������
        /// </summary>
        private void LoadListTTwithSensor()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                gcListAgr.MainView = gvListTT;
                string sSQL = string.Format(AgroQuery.GraficSensor.SelectSensor, _Type);
               
                DataTable dtMain = driverDb.GetDataTable( sSQL );
                // ������������� ������ ������ ��� �������
                DataColumn dcBool = new DataColumn("Sel");
                dcBool.DataType = System.Type.GetType("System.Boolean");
                dcBool.DefaultValue = false;
                dtMain.Columns.Add(dcBool);
                gcListAgr.DataSource = dtMain;

                if (gvListTT.RowCount > 0)
                    gvListTT.SetRowCellValue(0, gvListTT.Columns["Sel"], true); ;

            }
        }
        private void LoadListAgr()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSQL = AgroQuery.GraficSensor.SelectAgroAgregat;
               
                DataTable dtMain = driverDb.GetDataTable(sSQL);
                // ������������� ������ ������ ��� �������
                DataColumn dcBool = new DataColumn("Sel");
                dcBool.DataType = System.Type.GetType("System.Boolean");
                dcBool.DefaultValue = false;
                dtMain.Columns.Add(dcBool);
                gcListAgr.DataSource = dtMain;
                gcListAgr.MainView = gvListAgr;
                if (gvListAgr.RowCount > 0)
                    gvListAgr.SetRowCellValue(0, gvListTT.Columns["Sel"], true);

            }
        }
        private void LoadListDrv()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSQL = AgroQuery.GraficSensor.SelectDriverId;
               
                DataTable dtMain = driverDb.GetDataTable(sSQL);
                // ������������� ������ �������� ��� �������
                DataColumn dcBool = new DataColumn("Sel");
                dcBool.DataType = System.Type.GetType("System.Boolean");
                dcBool.DefaultValue = false;
                dtMain.Columns.Add(dcBool);
                gcListAgr.DataSource = dtMain;
                gcListAgr.MainView = gvListDrv;
                if (gvListDrv.RowCount > 0)
                    gvListDrv.SetRowCellValue(0, gvListDrv.Columns["Sel"], true);

            }
        }
        private DataTable SensorConvertData(DataTable dt)
        {
            DataTable ConvertData = new DataTable();
            DataColumn col;
            //��� TT
            col = new DataColumn("Mobitel_ID", System.Type.GetType("System.Int32"));
            ConvertData.Columns.Add(col);
            //��� ��������
            col = new DataColumn("SensorInBase_Id", System.Type.GetType("System.Int32"));
            ConvertData.Columns.Add(col);
            //�������� ��������
            col = new DataColumn("SensorName", System.Type.GetType("System.String"));
            ConvertData.Columns.Add(col);
            //���� ���������
            col = new DataColumn("TimeEvent", System.Type.GetType("System.DateTime"));
            ConvertData.Columns.Add(col);
            //�������� ��� ������� � ������ ���������
            col = new DataColumn("StopMove", System.Type.GetType("System.Int32"));
            ConvertData.Columns.Add(col);
            //���� ����������
            col = new DataColumn("TimeEventOut", System.Type.GetType("System.DateTime"));
            ConvertData.Columns.Add(col);
            //�������� ��� ������� � ������ ����������
            col = new DataColumn("StopMoveOut", System.Type.GetType("System.Int32"));
            ConvertData.Columns.Add(col);
            bool RecordCreate = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                short State = (short)Convert.ToInt16((dt.Rows[i]["State"]));
                if (RecordCreate)
                {
                    DataRow row = ConvertData.NewRow();
                    row["Mobitel_ID"] = dt.Rows[i - 1]["Mobitel_ID"];
                    row["SensorInBase_Id"] = dt.Rows[i - 1]["SensorInBase_Id"];
                    row["SensorName"] = dt.Rows[i - 1]["SensorName"];
                    row["TimeEvent"] = dt.Rows[i - 1]["TimeEvent"];
                    row["TimeEventOut"] = dt.Rows[i]["TimeEvent"];
                    row["StopMove"] = dt.Rows[i - 1]["StopMove"];
                    row["StopMoveOut"] = dt.Rows[i]["StopMove"];
                    ConvertData.Rows.Add(row);
                    RecordCreate = false;
                }
                if ((State == 1) && ((int)(Convert.ToInt32(dt.Rows[i]["SensorInBase_Id"])) != 0) && !RecordCreate)
                {
                    RecordCreate = true;
                }
                Application.DoEvents();  
            }
            return ConvertData;
        }
        private void SelectGridView()
        {
            if ((int)rgCommutRegime.EditValue == (int)SENSOR_REGIMES.TT)
            {
                LoadListTTwithSensor();
            }
            else
            {
                switch (_Type)
	                {
                        case (int)AlgorithmType.AGREGAT:
                            {
                                LoadListAgr(); 
                                break;
                            }
                        case (int)AlgorithmType.DRIVER:
                            {
                                LoadListDrv();
                                break;
                            }                 
	                }
            }
        }
        private void rgCommutRegime_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectGridView();
        }
        private void SetStatusParent(string sMessage)
        {
            ((MainAgro)ParentForm).SetStatus(sMessage);
        }
        private void SetProgressParent(int iValue)
        {
            ((MainAgro)ParentForm).SetProgress(iValue);
        }

        private void SetButtonParent( bool visibility )
        {
            ( (MainAgro)ParentForm ).SetButton( visibility );
        }

        private void SensorSelect(GridView gv)
        {
            if (gv.RowCount == 0)
            {
                XtraMessageBox.Show(Resources.SensorListAbsence, Resources.ApplicationName);
                return;
            }
            int RowChecked = GetRowChecked(gv);
            if (RowChecked == ConstsGen.RECORD_MISSING)
            {
                XtraMessageBox.Show(Resources.SensorSelect, Resources.ApplicationName);
                return;
            }
            int SensorId;
            if (Int32.TryParse(gv.GetRowCellValue(RowChecked, gv.Columns["Id"]).ToString(), out SensorId))
            {
                SensorGetFactData(SensorId);
            }
            else
            {
                XtraMessageBox.Show(Resources.SensorSelect, Resources.ApplicationName);
            }
        }
        #region �����������
        private void Localization()
        {
            Name_agr.Caption = Resources.NameRes;
            Iden.Caption = Resources.Identifier;
            Width_agr.Caption = Resources.WidthM;
            Family.Caption = Resources.Family;
            Name_drv.Caption = Resources.Name;
            Identifier_drv.Caption = Resources.Identifier;
            GroupeAgrM.Caption = Resources.Groupe ;
            CarModelAgr.Caption = Resources.Car;
            NameAgr.Caption = Resources.NameRes;
            StartBitAgr.Caption = Resources.StartBit;
            LengthAgr.Caption = Resources.Lenght;
            bdeStartReportAgr.Caption = Resources.DateFrom;
            bdeEndReportAgr.Caption = Resources.DateTo;
            rgCommutRegime.Properties.Items[0].Description = Resources.Cars;
            rgCommutRegime.Properties.Items[1].Description = Resources.Sensors;
            labelControl2.Text = Resources.SystemIdentification;
            Mobitel_ID_Agr.Caption = Resources.Car;
            rleMobitel.Columns[0].Caption = Resources.NameRes;
            SensorNameAgr.Caption = Resources.Identifier;
            TimeEvent.Caption = Resources.TimeSwOn;
            TimeEventOut.Caption = Resources.TimeSwOff;
            rleAgregat.Columns[0].Caption = Resources.NameRes;
            chartSensors.Series[0].Name = Resources.Sensor;
            btStartReportAgr.Caption = Resources.PUSK; 
        }
        #endregion
    }
}
