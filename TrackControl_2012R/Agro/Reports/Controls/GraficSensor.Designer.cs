namespace Agro
{
    partial class GraficSensor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GraficSensor));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.SeriesPoint seriesPoint1 = new DevExpress.XtraCharts.SeriesPoint(1D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint2 = new DevExpress.XtraCharts.SeriesPoint(2D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint3 = new DevExpress.XtraCharts.SeriesPoint(3D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint4 = new DevExpress.XtraCharts.SeriesPoint(4D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint5 = new DevExpress.XtraCharts.SeriesPoint(5D, new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint6 = new DevExpress.XtraCharts.SeriesPoint(6D, new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint7 = new DevExpress.XtraCharts.SeriesPoint(7D, new object[] {
            ((object)(1D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint8 = new DevExpress.XtraCharts.SeriesPoint(8D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint9 = new DevExpress.XtraCharts.SeriesPoint(9D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint10 = new DevExpress.XtraCharts.SeriesPoint(10D, new object[] {
            ((object)(0D))});
            DevExpress.XtraCharts.StepLineSeriesView stepLineSeriesView1 = new DevExpress.XtraCharts.StepLineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.StepLineSeriesView stepLineSeriesView2 = new DevExpress.XtraCharts.StepLineSeriesView();
            this.gvListAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sel_agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Iden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Width_agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcListAgr = new DevExpress.XtraGrid.GridControl();
            this.gvListDrv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_drv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sel_drv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Family = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Name_drv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Identifier_drv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvListTT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_TTagr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sel_TT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mobitel_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupeAgrM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CarModelAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StartBitAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LengthAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btStartReportAgr = new DevExpress.XtraBars.BarButtonItem();
            this.bdeStartReportAgr = new DevExpress.XtraBars.BarEditItem();
            this.deStartReportAgr = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEndReportAgr = new DevExpress.XtraBars.BarEditItem();
            this.deEndReportAgr = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.rgCommutRegime = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.gcReportAgr = new DevExpress.XtraGrid.GridControl();
            this.gvReportAgr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Mobitel_ID_Agr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleMobitel = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SensorNameAgr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StopMoveStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbStopMove = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.StopMove = new DevExpress.Utils.ImageCollection(this.components);
            this.TimeEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StopMoveEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEventOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chartSensors = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.gvListAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListDrv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgCommutRegime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReportAgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleMobitel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStopMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StopMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gvListAgr
            // 
            this.gvListAgr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvListAgr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvListAgr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListAgr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListAgr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvListAgr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvListAgr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvListAgr.Appearance.Empty.Options.UseBackColor = true;
            this.gvListAgr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListAgr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListAgr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListAgr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListAgr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvListAgr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListAgr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvListAgr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvListAgr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvListAgr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvListAgr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvListAgr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvListAgr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvListAgr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvListAgr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListAgr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListAgr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvListAgr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvListAgr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvListAgr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvListAgr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListAgr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvListAgr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvListAgr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvListAgr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvListAgr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListAgr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListAgr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvListAgr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListAgr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListAgr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvListAgr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvListAgr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvListAgr.Appearance.Preview.Options.UseBackColor = true;
            this.gvListAgr.Appearance.Preview.Options.UseFont = true;
            this.gvListAgr.Appearance.Preview.Options.UseForeColor = true;
            this.gvListAgr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListAgr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.Row.Options.UseBackColor = true;
            this.gvListAgr.Appearance.Row.Options.UseForeColor = true;
            this.gvListAgr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListAgr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvListAgr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvListAgr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvListAgr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListAgr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListAgr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvListAgr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvListAgr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvListAgr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvListAgr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListAgr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvListAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_agr,
            this.Sel_agr,
            this.Name_agr,
            this.Iden,
            this.Width_agr});
            this.gvListAgr.GridControl = this.gcListAgr;
            this.gvListAgr.Name = "gvListAgr";
            this.gvListAgr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvListAgr.OptionsView.EnableAppearanceOddRow = true;
            this.gvListAgr.OptionsView.ShowGroupPanel = false;
            this.gvListAgr.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvListAgr_CellValueChanging);
            // 
            // Id_agr
            // 
            this.Id_agr.Caption = "Id";
            this.Id_agr.FieldName = "Id";
            this.Id_agr.Name = "Id_agr";
            // 
            // Sel_agr
            // 
            this.Sel_agr.AppearanceHeader.Options.UseTextOptions = true;
            this.Sel_agr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel_agr.Caption = " ";
            this.Sel_agr.FieldName = "Sel";
            this.Sel_agr.Name = "Sel_agr";
            this.Sel_agr.Visible = true;
            this.Sel_agr.VisibleIndex = 0;
            this.Sel_agr.Width = 36;
            // 
            // Name_agr
            // 
            this.Name_agr.AppearanceCell.Options.UseTextOptions = true;
            this.Name_agr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_agr.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_agr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_agr.Caption = "��������";
            this.Name_agr.FieldName = "Name";
            this.Name_agr.Name = "Name_agr";
            this.Name_agr.Visible = true;
            this.Name_agr.VisibleIndex = 1;
            this.Name_agr.Width = 569;
            // 
            // Iden
            // 
            this.Iden.AppearanceCell.Options.UseTextOptions = true;
            this.Iden.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Iden.AppearanceHeader.Options.UseTextOptions = true;
            this.Iden.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Iden.Caption = "�������������";
            this.Iden.FieldName = "Identifier";
            this.Iden.Name = "Iden";
            this.Iden.Visible = true;
            this.Iden.VisibleIndex = 2;
            this.Iden.Width = 230;
            // 
            // Width_agr
            // 
            this.Width_agr.AppearanceCell.Options.UseTextOptions = true;
            this.Width_agr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_agr.AppearanceHeader.Options.UseTextOptions = true;
            this.Width_agr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_agr.Caption = "������, �";
            this.Width_agr.FieldName = "Width";
            this.Width_agr.Name = "Width_agr";
            this.Width_agr.Visible = true;
            this.Width_agr.VisibleIndex = 3;
            this.Width_agr.Width = 255;
            // 
            // gcListAgr
            // 
            this.gcListAgr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.LevelTemplate = this.gvListAgr;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.gvListDrv;
            gridLevelNode2.RelationName = "Level2";
            this.gcListAgr.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this.gcListAgr.Location = new System.Drawing.Point(1, 35);
            this.gcListAgr.MainView = this.gvListTT;
            this.gcListAgr.MenuManager = this.barManager1;
            this.gcListAgr.Name = "gcListAgr";
            this.gcListAgr.Size = new System.Drawing.Size(548, 395);
            this.gcListAgr.TabIndex = 3;
            this.gcListAgr.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListDrv,
            this.gvListTT,
            this.gvListAgr});
            // 
            // gvListDrv
            // 
            this.gvListDrv.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvListDrv.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvListDrv.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListDrv.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListDrv.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvListDrv.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvListDrv.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvListDrv.Appearance.Empty.Options.UseBackColor = true;
            this.gvListDrv.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListDrv.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListDrv.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListDrv.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListDrv.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvListDrv.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListDrv.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvListDrv.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvListDrv.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvListDrv.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvListDrv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvListDrv.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvListDrv.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvListDrv.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvListDrv.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListDrv.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListDrv.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvListDrv.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvListDrv.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvListDrv.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvListDrv.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListDrv.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvListDrv.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvListDrv.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvListDrv.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvListDrv.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListDrv.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListDrv.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvListDrv.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListDrv.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListDrv.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.OddRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.OddRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvListDrv.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvListDrv.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvListDrv.Appearance.Preview.Options.UseBackColor = true;
            this.gvListDrv.Appearance.Preview.Options.UseFont = true;
            this.gvListDrv.Appearance.Preview.Options.UseForeColor = true;
            this.gvListDrv.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListDrv.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.Row.Options.UseBackColor = true;
            this.gvListDrv.Appearance.Row.Options.UseForeColor = true;
            this.gvListDrv.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListDrv.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvListDrv.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvListDrv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvListDrv.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListDrv.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListDrv.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvListDrv.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvListDrv.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvListDrv.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvListDrv.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListDrv.Appearance.VertLine.Options.UseBackColor = true;
            this.gvListDrv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_drv,
            this.Sel_drv,
            this.Family,
            this.Name_drv,
            this.Identifier_drv});
            this.gvListDrv.GridControl = this.gcListAgr;
            this.gvListDrv.Name = "gvListDrv";
            this.gvListDrv.OptionsView.EnableAppearanceEvenRow = true;
            this.gvListDrv.OptionsView.EnableAppearanceOddRow = true;
            this.gvListDrv.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvListDrv_CellValueChanging);
            // 
            // Id_drv
            // 
            this.Id_drv.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_drv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_drv.Caption = "Id";
            this.Id_drv.FieldName = "Id";
            this.Id_drv.Name = "Id_drv";
            // 
            // Sel_drv
            // 
            this.Sel_drv.AppearanceCell.Options.UseTextOptions = true;
            this.Sel_drv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel_drv.AppearanceHeader.Options.UseTextOptions = true;
            this.Sel_drv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel_drv.Caption = " ";
            this.Sel_drv.FieldName = "Sel";
            this.Sel_drv.Name = "Sel_drv";
            this.Sel_drv.Visible = true;
            this.Sel_drv.VisibleIndex = 0;
            this.Sel_drv.Width = 28;
            // 
            // Family
            // 
            this.Family.AppearanceCell.Options.UseTextOptions = true;
            this.Family.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Family.AppearanceHeader.Options.UseTextOptions = true;
            this.Family.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Family.Caption = "�������";
            this.Family.FieldName = "Family";
            this.Family.Name = "Family";
            this.Family.Visible = true;
            this.Family.VisibleIndex = 1;
            this.Family.Width = 353;
            // 
            // Name_drv
            // 
            this.Name_drv.AppearanceCell.Options.UseTextOptions = true;
            this.Name_drv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Name_drv.AppearanceHeader.Options.UseTextOptions = true;
            this.Name_drv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Name_drv.Caption = "���";
            this.Name_drv.FieldName = "Name";
            this.Name_drv.Name = "Name_drv";
            this.Name_drv.Visible = true;
            this.Name_drv.VisibleIndex = 2;
            this.Name_drv.Width = 353;
            // 
            // Identifier_drv
            // 
            this.Identifier_drv.AppearanceCell.Options.UseTextOptions = true;
            this.Identifier_drv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier_drv.AppearanceHeader.Options.UseTextOptions = true;
            this.Identifier_drv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Identifier_drv.Caption = "�������������";
            this.Identifier_drv.FieldName = "Identifier";
            this.Identifier_drv.Name = "Identifier_drv";
            this.Identifier_drv.Visible = true;
            this.Identifier_drv.VisibleIndex = 3;
            this.Identifier_drv.Width = 356;
            // 
            // gvListTT
            // 
            this.gvListTT.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvListTT.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvListTT.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvListTT.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListTT.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListTT.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvListTT.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvListTT.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvListTT.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvListTT.Appearance.Empty.Options.UseBackColor = true;
            this.gvListTT.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListTT.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvListTT.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListTT.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListTT.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvListTT.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvListTT.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvListTT.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListTT.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvListTT.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvListTT.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvListTT.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvListTT.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvListTT.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvListTT.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvListTT.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvListTT.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvListTT.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvListTT.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvListTT.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvListTT.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListTT.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvListTT.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvListTT.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvListTT.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvListTT.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvListTT.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvListTT.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvListTT.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvListTT.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvListTT.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvListTT.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvListTT.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvListTT.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvListTT.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListTT.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListTT.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvListTT.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListTT.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListTT.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.OddRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.OddRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvListTT.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvListTT.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvListTT.Appearance.Preview.Options.UseBackColor = true;
            this.gvListTT.Appearance.Preview.Options.UseFont = true;
            this.gvListTT.Appearance.Preview.Options.UseForeColor = true;
            this.gvListTT.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvListTT.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.Row.Options.UseBackColor = true;
            this.gvListTT.Appearance.Row.Options.UseForeColor = true;
            this.gvListTT.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvListTT.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvListTT.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvListTT.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvListTT.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvListTT.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvListTT.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvListTT.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvListTT.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvListTT.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvListTT.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvListTT.Appearance.VertLine.Options.UseBackColor = true;
            this.gvListTT.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_TTagr,
            this.Sel_TT,
            this.mobitel_id,
            this.GroupeAgrM,
            this.CarModelAgr,
            this.NameAgr,
            this.StartBitAgr,
            this.LengthAgr});
            this.gvListTT.GridControl = this.gcListAgr;
            this.gvListTT.Name = "gvListTT";
            this.gvListTT.OptionsView.EnableAppearanceEvenRow = true;
            this.gvListTT.OptionsView.EnableAppearanceOddRow = true;
            this.gvListTT.OptionsView.ShowGroupPanel = false;
            this.gvListTT.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvListTT_CellValueChanging);
            // 
            // Id_TTagr
            // 
            this.Id_TTagr.Caption = "Id_agr";
            this.Id_TTagr.FieldName = "Id";
            this.Id_TTagr.Name = "Id_TTagr";
            // 
            // Sel_TT
            // 
            this.Sel_TT.AppearanceCell.Options.UseTextOptions = true;
            this.Sel_TT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel_TT.AppearanceHeader.Options.UseImage = true;
            this.Sel_TT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel_TT.Caption = " ";
            this.Sel_TT.FieldName = "Sel";
            this.Sel_TT.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Sel_TT.ImageIndex = 3;
            this.Sel_TT.Name = "Sel_TT";
            this.Sel_TT.Visible = true;
            this.Sel_TT.VisibleIndex = 0;
            this.Sel_TT.Width = 44;
            // 
            // mobitel_id
            // 
            this.mobitel_id.Caption = "Id_mobitel";
            this.mobitel_id.FieldName = "mobitel_id";
            this.mobitel_id.Name = "mobitel_id";
            // 
            // GroupeAgrM
            // 
            this.GroupeAgrM.AppearanceCell.Options.UseTextOptions = true;
            this.GroupeAgrM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GroupeAgrM.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeAgrM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeAgrM.Caption = "������";
            this.GroupeAgrM.FieldName = "Groupe";
            this.GroupeAgrM.Name = "GroupeAgrM";
            this.GroupeAgrM.OptionsColumn.AllowEdit = false;
            this.GroupeAgrM.Visible = true;
            this.GroupeAgrM.VisibleIndex = 1;
            this.GroupeAgrM.Width = 208;
            // 
            // CarModelAgr
            // 
            this.CarModelAgr.AppearanceCell.Options.UseTextOptions = true;
            this.CarModelAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CarModelAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.CarModelAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CarModelAgr.Caption = "������";
            this.CarModelAgr.FieldName = "CarModel";
            this.CarModelAgr.Name = "CarModelAgr";
            this.CarModelAgr.OptionsColumn.AllowEdit = false;
            this.CarModelAgr.Visible = true;
            this.CarModelAgr.VisibleIndex = 2;
            this.CarModelAgr.Width = 263;
            // 
            // NameAgr
            // 
            this.NameAgr.AppearanceCell.Options.UseTextOptions = true;
            this.NameAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.NameAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.NameAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NameAgr.Caption = "��������";
            this.NameAgr.FieldName = "Name";
            this.NameAgr.Name = "NameAgr";
            this.NameAgr.OptionsColumn.AllowEdit = false;
            this.NameAgr.Visible = true;
            this.NameAgr.VisibleIndex = 3;
            this.NameAgr.Width = 296;
            // 
            // StartBitAgr
            // 
            this.StartBitAgr.AppearanceCell.Options.UseTextOptions = true;
            this.StartBitAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StartBitAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.StartBitAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StartBitAgr.Caption = "��.���";
            this.StartBitAgr.FieldName = "StartBit";
            this.StartBitAgr.Name = "StartBitAgr";
            this.StartBitAgr.OptionsColumn.AllowEdit = false;
            this.StartBitAgr.Visible = true;
            this.StartBitAgr.VisibleIndex = 4;
            this.StartBitAgr.Width = 138;
            // 
            // LengthAgr
            // 
            this.LengthAgr.AppearanceCell.Options.UseTextOptions = true;
            this.LengthAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LengthAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.LengthAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LengthAgr.Caption = "�����";
            this.LengthAgr.FieldName = "Length";
            this.LengthAgr.Name = "LengthAgr";
            this.LengthAgr.OptionsColumn.AllowEdit = false;
            this.LengthAgr.Visible = true;
            this.LengthAgr.VisibleIndex = 5;
            this.LengthAgr.Width = 141;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btStartReportAgr,
            this.bdeStartReportAgr,
            this.bdeEndReportAgr});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.deStartReportAgr,
            this.deEndReportAgr});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btStartReportAgr, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.bdeStartReportAgr, "", false, true, true, 135, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.bdeEndReportAgr, "", false, true, true, 130, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btStartReportAgr
            // 
            this.btStartReportAgr.Caption = "����";
            this.btStartReportAgr.Glyph = ((System.Drawing.Image)(resources.GetObject("btStartReportAgr.Glyph")));
            this.btStartReportAgr.Id = 48;
            this.btStartReportAgr.Name = "btStartReportAgr";
            this.btStartReportAgr.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btStartReportAgr_ItemClick);
            // 
            // bdeStartReportAgr
            // 
            this.bdeStartReportAgr.Caption = "���� �";
            this.bdeStartReportAgr.Edit = this.deStartReportAgr;
            this.bdeStartReportAgr.Id = 1;
            this.bdeStartReportAgr.Name = "bdeStartReportAgr";
            // 
            // deStartReportAgr
            // 
            this.deStartReportAgr.AutoHeight = false;
            this.deStartReportAgr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartReportAgr.DisplayFormat.FormatString = "g";
            this.deStartReportAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStartReportAgr.EditFormat.FormatString = "g";
            this.deStartReportAgr.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStartReportAgr.Mask.EditMask = "g";
            this.deStartReportAgr.Name = "deStartReportAgr";
            this.deStartReportAgr.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // bdeEndReportAgr
            // 
            this.bdeEndReportAgr.Caption = "��";
            this.bdeEndReportAgr.Edit = this.deEndReportAgr;
            this.bdeEndReportAgr.Id = 2;
            this.bdeEndReportAgr.Name = "bdeEndReportAgr";
            // 
            // deEndReportAgr
            // 
            this.deEndReportAgr.AutoHeight = false;
            this.deEndReportAgr.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndReportAgr.DisplayFormat.FormatString = "g";
            this.deEndReportAgr.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEndReportAgr.EditFormat.FormatString = "g";
            this.deEndReportAgr.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEndReportAgr.Mask.EditMask = "g";
            this.deEndReportAgr.Name = "deEndReportAgr";
            this.deEndReportAgr.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartSensors);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(914, 644);
            this.splitContainerControl1.SplitterPosition = 430;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gcListAgr);
            this.splitContainerControl2.Panel1.Controls.Add(this.panelControl2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gcReportAgr);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(914, 430);
            this.splitContainerControl2.SplitterPosition = 550;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.rgCommutRegime);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Location = new System.Drawing.Point(3, 3);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(548, 29);
            this.panelControl2.TabIndex = 2;
            // 
            // rgCommutRegime
            // 
            this.rgCommutRegime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rgCommutRegime.Location = new System.Drawing.Point(394, 1);
            this.rgCommutRegime.MenuManager = this.barManager1;
            this.rgCommutRegime.Name = "rgCommutRegime";
            this.rgCommutRegime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgCommutRegime.Properties.Appearance.Options.UseBackColor = true;
            this.rgCommutRegime.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.rgCommutRegime.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.rgCommutRegime.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "�������")});
            this.rgCommutRegime.Size = new System.Drawing.Size(154, 25);
            this.rgCommutRegime.TabIndex = 2;
            this.rgCommutRegime.SelectedIndexChanged += new System.EventHandler(this.rgCommutRegime_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl2.Location = new System.Drawing.Point(228, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(160, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "�������  ������������� ";
            // 
            // gcReportAgr
            // 
            this.gcReportAgr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReportAgr.Location = new System.Drawing.Point(0, 0);
            this.gcReportAgr.MainView = this.gvReportAgr;
            this.gcReportAgr.MenuManager = this.barManager1;
            this.gcReportAgr.Name = "gcReportAgr";
            this.gcReportAgr.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleMobitel,
            this.rleAgregat,
            this.icbStopMove});
            this.gcReportAgr.Size = new System.Drawing.Size(358, 430);
            this.gcReportAgr.TabIndex = 2;
            this.gcReportAgr.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReportAgr});
            // 
            // gvReportAgr
            // 
            this.gvReportAgr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.Empty.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReportAgr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReportAgr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReportAgr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReportAgr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvReportAgr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvReportAgr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvReportAgr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReportAgr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReportAgr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvReportAgr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReportAgr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReportAgr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReportAgr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReportAgr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvReportAgr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvReportAgr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvReportAgr.Appearance.Preview.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.Preview.Options.UseFont = true;
            this.gvReportAgr.Appearance.Preview.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReportAgr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.Row.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.Row.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReportAgr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvReportAgr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReportAgr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvReportAgr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvReportAgr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvReportAgr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvReportAgr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvReportAgr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReportAgr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReportAgr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Mobitel_ID_Agr,
            this.SensorNameAgr,
            this.StopMoveStart,
            this.TimeEvent,
            this.StopMoveEnd,
            this.TimeEventOut});
            this.gvReportAgr.GridControl = this.gcReportAgr;
            this.gvReportAgr.Name = "gvReportAgr";
            this.gvReportAgr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReportAgr.OptionsView.EnableAppearanceOddRow = true;
            // 
            // Mobitel_ID_Agr
            // 
            this.Mobitel_ID_Agr.AppearanceCell.Options.UseTextOptions = true;
            this.Mobitel_ID_Agr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Mobitel_ID_Agr.AppearanceHeader.Options.UseTextOptions = true;
            this.Mobitel_ID_Agr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Mobitel_ID_Agr.Caption = "������";
            this.Mobitel_ID_Agr.ColumnEdit = this.rleMobitel;
            this.Mobitel_ID_Agr.FieldName = "Mobitel_ID";
            this.Mobitel_ID_Agr.Name = "Mobitel_ID_Agr";
            this.Mobitel_ID_Agr.OptionsColumn.AllowEdit = false;
            this.Mobitel_ID_Agr.Visible = true;
            this.Mobitel_ID_Agr.VisibleIndex = 0;
            this.Mobitel_ID_Agr.Width = 257;
            // 
            // rleMobitel
            // 
            this.rleMobitel.AutoHeight = false;
            this.rleMobitel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleMobitel.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleMobitel.DisplayMember = "Name";
            this.rleMobitel.Name = "rleMobitel";
            this.rleMobitel.ValueMember = "Id";
            // 
            // SensorNameAgr
            // 
            this.SensorNameAgr.AppearanceCell.Options.UseTextOptions = true;
            this.SensorNameAgr.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SensorNameAgr.AppearanceHeader.Options.UseTextOptions = true;
            this.SensorNameAgr.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SensorNameAgr.Caption = "�������������";
            this.SensorNameAgr.FieldName = "SensorName";
            this.SensorNameAgr.Name = "SensorNameAgr";
            this.SensorNameAgr.OptionsColumn.AllowEdit = false;
            this.SensorNameAgr.Visible = true;
            this.SensorNameAgr.VisibleIndex = 1;
            this.SensorNameAgr.Width = 257;
            // 
            // StopMoveStart
            // 
            this.StopMoveStart.AppearanceCell.Options.UseImage = true;
            this.StopMoveStart.AppearanceCell.Options.UseTextOptions = true;
            this.StopMoveStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StopMoveStart.Caption = " ";
            this.StopMoveStart.ColumnEdit = this.icbStopMove;
            this.StopMoveStart.FieldName = "StopMove";
            this.StopMoveStart.Name = "StopMoveStart";
            this.StopMoveStart.OptionsColumn.AllowEdit = false;
            this.StopMoveStart.OptionsColumn.ShowCaption = false;
            this.StopMoveStart.Visible = true;
            this.StopMoveStart.VisibleIndex = 2;
            this.StopMoveStart.Width = 35;
            // 
            // icbStopMove
            // 
            this.icbStopMove.AutoHeight = false;
            this.icbStopMove.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbStopMove.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1)});
            this.icbStopMove.Name = "icbStopMove";
            this.icbStopMove.SmallImages = this.StopMove;
            // 
            // StopMove
            // 
            this.StopMove.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("StopMove.ImageStream")));
            this.StopMove.Images.SetKeyName(0, "Parking.png");
            this.StopMove.Images.SetKeyName(1, "Highway.png");
            // 
            // TimeEvent
            // 
            this.TimeEvent.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEvent.Caption = "����� ���������";
            this.TimeEvent.DisplayFormat.FormatString = "f";
            this.TimeEvent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEvent.FieldName = "TimeEvent";
            this.TimeEvent.Name = "TimeEvent";
            this.TimeEvent.OptionsColumn.AllowEdit = false;
            this.TimeEvent.Visible = true;
            this.TimeEvent.VisibleIndex = 3;
            this.TimeEvent.Width = 234;
            // 
            // StopMoveEnd
            // 
            this.StopMoveEnd.AppearanceCell.Options.UseTextOptions = true;
            this.StopMoveEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StopMoveEnd.Caption = " ";
            this.StopMoveEnd.ColumnEdit = this.icbStopMove;
            this.StopMoveEnd.FieldName = "StopMoveOut";
            this.StopMoveEnd.Name = "StopMoveEnd";
            this.StopMoveEnd.OptionsColumn.AllowEdit = false;
            this.StopMoveEnd.OptionsColumn.ShowCaption = false;
            this.StopMoveEnd.Visible = true;
            this.StopMoveEnd.VisibleIndex = 4;
            this.StopMoveEnd.Width = 35;
            // 
            // TimeEventOut
            // 
            this.TimeEventOut.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEventOut.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEventOut.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEventOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEventOut.Caption = "����� ����������";
            this.TimeEventOut.DisplayFormat.FormatString = "f";
            this.TimeEventOut.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEventOut.FieldName = "TimeEventOut";
            this.TimeEventOut.Name = "TimeEventOut";
            this.TimeEventOut.OptionsColumn.AllowEdit = false;
            this.TimeEventOut.Visible = true;
            this.TimeEventOut.VisibleIndex = 5;
            this.TimeEventOut.Width = 272;
            // 
            // rleAgregat
            // 
            this.rleAgregat.AutoHeight = false;
            this.rleAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleAgregat.DisplayMember = "Name";
            this.rleAgregat.Name = "rleAgregat";
            this.rleAgregat.ValueMember = "Id";
            // 
            // chartSensors
            // 
            xyDiagram1.AxisX.DateTimeGridAlignment = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram1.AxisX.DateTimeMeasureUnit = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "dd/MM HH:mm";
            xyDiagram1.AxisX.Interlaced = true;
            xyDiagram1.AxisX.Label.Staggered = true;
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.Visible = false;
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Visible = false;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXScrolling = true;
            xyDiagram1.EnableAxisYScrolling = true;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisYZooming = true;
            this.chartSensors.Diagram = xyDiagram1;
            this.chartSensors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartSensors.Location = new System.Drawing.Point(0, 0);
            this.chartSensors.Name = "chartSensors";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pointSeriesLabel1.LineVisible = true;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "������";
            series1.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint1,
            seriesPoint2,
            seriesPoint3,
            seriesPoint4,
            seriesPoint5,
            seriesPoint6,
            seriesPoint7,
            seriesPoint8,
            seriesPoint9,
            seriesPoint10});
            stepLineSeriesView1.LineMarkerOptions.Visible = false;
            series1.View = stepLineSeriesView1;
            this.chartSensors.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pointSeriesLabel2.LineVisible = true;
            this.chartSensors.SeriesTemplate.Label = pointSeriesLabel2;
            this.chartSensors.SeriesTemplate.View = stepLineSeriesView2;
            this.chartSensors.Size = new System.Drawing.Size(914, 208);
            this.chartSensors.TabIndex = 1;
            // 
            // GraficSensor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "GraficSensor";
            this.Size = new System.Drawing.Size(914, 670);
            ((System.ComponentModel.ISupportInitialize)(this.gvListAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListDrv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgCommutRegime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReportAgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleMobitel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStopMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StopMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(stepLineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSensors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btStartReportAgr;
        private DevExpress.XtraBars.BarEditItem bdeStartReportAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStartReportAgr;
        private DevExpress.XtraBars.BarEditItem bdeEndReportAgr;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEndReportAgr;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraCharts.ChartControl chartSensors;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gcReportAgr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvReportAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Mobitel_ID_Agr;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleMobitel;
        private DevExpress.XtraGrid.Columns.GridColumn SensorNameAgr;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEvent;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEventOut;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleAgregat;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.RadioGroup rgCommutRegime;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl gcListAgr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListAgr;
        private DevExpress.XtraGrid.Columns.GridColumn Id_agr;
        private DevExpress.XtraGrid.Columns.GridColumn Sel_agr;
        private DevExpress.XtraGrid.Columns.GridColumn Name_agr;
        private DevExpress.XtraGrid.Columns.GridColumn Iden;
        private DevExpress.XtraGrid.Columns.GridColumn Width_agr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListTT;
        private DevExpress.XtraGrid.Columns.GridColumn Id_TTagr;
        private DevExpress.XtraGrid.Columns.GridColumn Sel_TT;
        private DevExpress.XtraGrid.Columns.GridColumn mobitel_id;
        private DevExpress.XtraGrid.Columns.GridColumn GroupeAgrM;
        private DevExpress.XtraGrid.Columns.GridColumn CarModelAgr;
        private DevExpress.XtraGrid.Columns.GridColumn NameAgr;
        private DevExpress.XtraGrid.Columns.GridColumn StartBitAgr;
        private DevExpress.XtraGrid.Columns.GridColumn LengthAgr;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListDrv;
        private DevExpress.XtraGrid.Columns.GridColumn Id_drv;
        private DevExpress.XtraGrid.Columns.GridColumn Sel_drv;
        private DevExpress.XtraGrid.Columns.GridColumn Family;
        private DevExpress.XtraGrid.Columns.GridColumn Name_drv;
        private DevExpress.XtraGrid.Columns.GridColumn Identifier_drv;
        private DevExpress.XtraGrid.Columns.GridColumn StopMoveStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbStopMove;
        private DevExpress.Utils.ImageCollection StopMove;
        private DevExpress.XtraGrid.Columns.GridColumn StopMoveEnd;
    }
}
