using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Agro
{
    public interface IReport
    {
        DataTable GetReport();
    }
}
