using System;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;

namespace Agro
{
    public class RepSensorDrv : RepSensor
    {
        int _Driver = 0;

        public RepSensorDrv(int ID, DateTime Start, DateTime End, ReportsList RL)
        {
            switch (RL)
            {
                case ReportsList.DRV_SENSOR_TT:
                    {
                        _TT = ID;
                        _Driver = 0;
                        _identSensor = 0;
                        break;
                    }
                case ReportsList.DRV_SENSOR_DRV:
                    {
                        _TT = 0;
                        _Driver = ID;
                        _identSensor = DicUtilites.GetDriverIdent(_Driver);
                        SensorAtributs sa = DicUtilites.GetDriverAtributes(_identSensor); 
                        Sensor_Atributs.Add(_identSensor, sa);
                        break;
                    }
            }
            _RL = RL;
            _Start = Start;
            _End = End;
        }

        /// <summary>
        /// ����������� ���� �������� �� �������������� � ��� ���������
        /// </summary>
        /// <param name="dfdr"></param>
        protected override void SensorNameStateIdentify(ref DataForDataRow dfdr)
        {
            if (dfdr.Identifier != 0)
            {
                if (Sensor_Atributs.ContainsKey(dfdr.Identifier))
                {
                    dfdr.State = 1;
                    dfdr.SensorInBase = Sensor_Atributs[dfdr.Identifier].Id;
                    dfdr.SensorName = Sensor_Atributs[dfdr.Identifier].Name;
                }
                else
                {
                    SensorAtributs sa = DicUtilites.GetDriverAtributes(dfdr.Identifier);
                    if (sa.Id != 0)
                    {
                        dfdr.State = 1;
                        dfdr.SensorInBase = sa.Id;
                        dfdr.SensorName = sa.Name;
                        SensorAtributs sib = new SensorAtributs(dfdr.SensorInBase, dfdr.SensorName);
                        Sensor_Atributs.Add(dfdr.Identifier, sib);
                    }
                    else
                    {
                        LimitUnknownId(ref dfdr);    
                    }
                }
            }
            else
            {
                dfdr.SensorInBase = 0;
                dfdr.SensorName = "";
                dfdr.State = 0;
            }
        }

        protected override bool GetMobitelsStartLength()
        {
            bool ExistStartLen = false;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 

                _SQL = AgroQuery.RepSensorDrv.SelectSensorMobId + (int)AlgorithmType.DRIVER;

                if (_RL == ReportsList.DRV_SENSOR_TT)
                    _SQL = _SQL + " AND sensors.mobitel_id = " + _TT;
                driverDb.GetDataReader(_SQL);
                TT_SLs.Clear();
                while (driverDb.Read())
                {
                    TTStartLen TTSL = new TTStartLen((short)driverDb.GetInt32("StartBit"), (short)driverDb.GetInt32("Length"));
                    TT_SLs.Add(driverDb.GetInt32("mobitel_id"), TTSL);
                    ExistStartLen = true;
                }
                driverDb.CloseDataReader(); 
            }
            return ExistStartLen;
        }
    }
}
