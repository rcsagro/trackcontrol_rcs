using System;
using System.Collections.Generic;
using System.Text;

namespace Agro
{
   /// <summary>
    /// идентификатор + имя
   /// </summary>
    public struct SensorAtributs
    {
        public int Id;
        public string Name;
        public SensorAtributs(int inpId, string inpMame)
        {
            Id = inpId;
            Name = inpMame;
        }
    }
}
