using System;
using Agro.Dictionaries;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;

namespace Agro
{
    /// <summary>
    /// ����� �� ��������� ������ �� (AGR_SENSOR_TT) ��� ���� �� ������ �������� (AGR_SENSOR_AGR)
    /// </summary>
    public class RepSensorAgr : RepSensor
    {
        private int _AGR = 0;

        public RepSensorAgr(int ID, DateTime Start, DateTime End, ReportsList RL)
        {
            switch (RL)
            {
                case ReportsList.AGR_SENSOR_TT:
                {
                    _TT = ID;
                    _AGR = 0;
                    _identSensor = 0;
                    break;
                }
                case ReportsList.AGR_SENSOR_AGR:
                {
                    _TT = 0;
                    _AGR = ID;
                    using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat(_AGR))
                    {
                        _identSensor = daa.Identifier;
                        SensorAtributs sa = new SensorAtributs(_AGR, daa.Name + " " + daa.InvNumber);
                        Sensor_Atributs.Add(_identSensor, sa);
                    }
                    break;
                }
            }
            _RL = RL;
            _Start = Start;
            _End = End;
        }

        /// <summary>
        /// ����������� ���� �������� �� �������������� � ��� ���������
        /// </summary>
        /// <param name="dfdr"></param>
        protected override void SensorNameStateIdentify(ref DataForDataRow dfdr)
        {
            if (dfdr.Identifier != 0)
            {
                if (Sensor_Atributs.ContainsKey(dfdr.Identifier))
                {
                    dfdr.State = 1;
                    dfdr.SensorInBase = Sensor_Atributs[dfdr.Identifier].Id;
                    dfdr.SensorName = Sensor_Atributs[dfdr.Identifier].Name;
                }
                else
                {

                    using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat())
                    {
                        if (daa.InitFromIdentifier(dfdr.Identifier))
                        {
                            dfdr.State = 1;
                            dfdr.SensorInBase = daa.Id;
                            dfdr.SensorName = daa.Name + " " + daa.InvNumber;
                            SensorAtributs sib = new SensorAtributs(dfdr.SensorInBase, dfdr.SensorName);
                            Sensor_Atributs.Add(dfdr.Identifier, sib);
                        }
                        else
                        {
                            LimitUnknownId(ref dfdr);
                        }
                    }
                }
            }
            else
            {
                dfdr.SensorInBase = 0;
                dfdr.SensorName = "";
                dfdr.State = 0;
            }
        }

        protected override bool GetMobitelsStartLength()
        {
            bool existStartLen = false;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                _SQL = string.Format(AgroQuery.RepSensorAgr.SelectSensors, (int) AlgorithmType.AGREGAT);

                if (_RL == ReportsList.AGR_SENSOR_TT)
                    _SQL = _SQL + " AND sensors.mobitel_id = " + _TT;

                driverDb.GetDataReader(_SQL);
                TT_SLs.Clear();
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    while (driverDb.Read())
                    {
                        var ttsl = new TTStartLen((short) driverDb.GetUInt16("StartBit"),
                            (short) driverDb.GetUInt16("Length"));
                        TT_SLs.Add(driverDb.GetInt32("mobitel_id"), ttsl);
                        existStartLen = true;
                    }
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    while (driverDb.Read())
                    {
                        var ttsl = new TTStartLen((short)driverDb.GetUInt16("StartBit"),
                            (short)driverDb.GetUInt16("Length"));

                        TT_SLs.Add(driverDb.GetInt32("mobitel_id"), ttsl);
                        existStartLen = true;
                    }
                }
                driverDb.CloseDataReader();
            }
            return existStartLen;
        }
    }
}
