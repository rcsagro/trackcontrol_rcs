using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using TrackControl.General.DatabaseDriver;
namespace Agro
{
    /// <summary>
    /// ���������� ������ ��������� �������� �� DataGPS 
    /// </summary>
    public abstract  class RepSensor:IReport
    {
        #region ��������
        /// <summary>
        /// ���������� ������������ ��������� ������.������ ����� �������� ������ �� ����,������ �� �����������
        /// </summary>
        /// <param name="mobitelId">������</param>
        /// <param name="datStart">������ �������</param>
        /// <param name="datEnd">����� �������</param>
        /// <returns></returns>
        private delegate DataTable GetData(int mobitelId, DateTime datStart, DateTime datEnd);
        #endregion

        bool _flagStopExecuting = false;

        #region �������
        public event StatusMessage ChangeStatusEvent;
        public event ProgressBarValue ChangeProgressBar;
        public event ButtonBarValue ChangeButtonBar;
        #endregion

        #region �����������
        /// <summary>
        /// ������ ������������� ���������� � ��������� ����� � �������
        /// </summary>
        protected Dictionary<int, TTStartLen> TT_SLs = new Dictionary<int, TTStartLen>();
        /// <summary>
        /// key - (��������� ��� + ��� �� )
        /// value - ��������� ������� 
        /// </summary>
        protected Dictionary<string, short> Sensor_State = new Dictionary<string, short>();
        /// <summary>
        /// key - (��������� ��� + ��� �� )
        /// value - ��� �������
        /// </summary>
        protected Dictionary<string, ushort> Sensor_Ident = new Dictionary<string, ushort>();
        /// <summary>
        /// key - ������������� � ������� ��������, value - ��� ������� � ����
        /// </summary>
        protected Dictionary<ushort, SensorAtributs> Sensor_Atributs = new Dictionary<ushort, SensorAtributs>();
        #endregion

        #region ���������
        protected struct TTStartLen
        {
            public short StartBit;
            public short Length;
            public TTStartLen(short inpStartBit, short inpLength)
            {
                StartBit = inpStartBit;
                Length = inpLength;
            }

        }
        /// <summary>
        /// ��������� ��� ������������ DataGPS
        /// </summary>
        protected struct DataForDataRow
        {
            public int TT;
            public int SensorInBase;
            public string SensorName;
            public ushort Identifier;
            /// <summary>
            /// // 0 - ��� ������� 1 - ����
            /// </summary>
            public short State;
            public int StartBit;
            public DateTime timeEvent;
            /// <summary>
            /// ������� ����� ��� ��������
            /// </summary>
            public int cBounce;
            /// <summary>
            /// // 0 - ������� 1 - ��������
            /// </summary>
            public short StopMove;
            public DataForDataRow(DateTime timeEventInit)
            {
                State = 0;
                TT = 0;
                SensorInBase = 0;
                Identifier = 0;
                StartBit = 0;
                cBounce = 0;
                timeEvent = timeEventInit;
                SensorName = "";
                StopMove = 0;
            }
        }
        #endregion

        #region �����������
        public RepSensor()
        {
            // to do
        }
        #endregion

        #region ����������
        /// <summary>
        /// ���������� ����� ������������
        /// </summary>
        private const int POINTS_MAX_BOUNDS = 3;
        protected const int MAX_UNKNOWN_ID = 10;
        #endregion

        #region ����
        protected DataTable dtReport;
        protected string _SQL;
        protected DateTime _Start;
        protected DateTime _End;
        protected int _TT = 0;
        protected ushort _identSensor;
        private bool _UnknownIdMessageShown;
        private List<int> _UnknownId = new List<int>();
        /// <summary>
        /// ��� ��������� ������
        /// </summary>
        protected ReportsList _RL;
        #endregion
        /// <summary>
        /// �������� �������� �������� DataTable
        /// </summary>
        protected void CreateReportColumns()
        {
            dtReport.Columns.AddRange(new DataColumn[]{
            new DataColumn("Mobitel_ID", typeof(int)),     //��� TT        
            new DataColumn("SensorInBase_Id", typeof(int)),//��� �������       
            new DataColumn("SensorName", typeof(string)),//�������� �������     
            new DataColumn("Identifier", typeof(int)),//������������� �������      
            new DataColumn("StartBit", typeof(int)),//��������� ���       
            new DataColumn("State", typeof(int)),//���������      
            new DataColumn("TimeEvent", typeof(DateTime)),//����� ������������      
            new DataColumn("StopMove", typeof(int))}); //�������� ��� ������� � ������ ������������  
        }
        /// <summary>
        /// ���������� �� � ������������� ���������� (���,��������� ���,������)
        /// </summary>
        protected virtual bool GetMobitelsStartLength()
        {
            return false;
        }

        /// <summary>
        /// ��������������� ���� � unixtime
        /// </summary>
        /// <param name="date">�������� ����</param>
        /// <returns></returns>
        static double ConvertToUnixTimestamp( DateTime date )
        {
            TimeSpan dt = new DateTime( DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond ) - DateTime.UtcNow;
            DateTime origin = new DateTime( 1970, 1, 1, 0, 0, 0, 0 ) + dt;
            TimeSpan diff = date - origin;

            return Convert.ToDouble( diff.TotalSeconds );
            //DateTime origin = new DateTime( 1970, 1, 1, 0, 0, 0, 0 );
        }

        /// <summary>
        /// ��������� ������ � ������ ������
        /// </summary>
        /// <param name="mobitelId">��������</param>
        /// <param name="DatStart">���� ������ ����������</param>
        /// <param name="DatEnd">���� ��������� ����������</param>
        /// <returns></returns>
        protected DataTable EnvokenGetData(int mobitelId, DateTime DatStart, DateTime DatEnd)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                _SQL = AgroQuery.RepSensor.SelectDatagps;
                
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", ConvertToUnixTimestamp( DatStart));
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", ConvertToUnixTimestamp( DatEnd ));
                bool First = true;
                foreach (KeyValuePair<int, TTStartLen> kvp in TT_SLs)  
                {
                    if (First)
                    {
                        _SQL = _SQL + " AND ( datagps.Mobitel_ID = " + kvp.Key;
                        First = false;
                    }
                    else
                        _SQL = _SQL + " OR  datagps.Mobitel_ID = " + kvp.Key;
                }

                if (TT_SLs.Count > 0) 
                    _SQL = _SQL + ")";

                _SQL = _SQL + AgroQuery.RepSensor.AndDatagpsValid; 

                DataTable table = driverDb.GetDataTable(_SQL, driverDb.GetSqlParameterArray); 

                if( table.Rows.Count > 0 )
                {
                   string tm = table.Rows[0]["FromUT"].ToString();
                }

                return table; 
            }
        }

        /// <summary>
        /// ���������� ������ � �������������� ����� ������
        /// </summary>
        /// <param name="dfdr"></param>
        protected void AddRow(ref DataForDataRow dfdr)
        {
            //Debug.Print (dtReport.Rows.Count);
            DataRow row = dtReport.NewRow();
            row["Mobitel_ID"] = dfdr.TT;
            row["SensorInBase_Id"] = dfdr.SensorInBase;
            row["SensorName"] = dfdr.SensorName;
            row["Identifier"] = dfdr.Identifier;
            row["State"] = dfdr.State;
            row["StartBit"] = dfdr.StartBit;
            row["TimeEvent"] = dfdr.timeEvent;
            row["StopMove"] = dfdr.StopMove;
            dtReport.Rows.Add(row);
            dfdr.cBounce = 0;
            if (dfdr.SensorInBase < 0)
            {
                if (!_UnknownId.Contains(dfdr.SensorInBase))
                {
                    _UnknownId.Add(dfdr.SensorInBase);
                }
            }
            //Debug.Print("state {0} time {1} Sensor {2} TT {3} id_b {4} id {5}", dfdr.State, dfdr.timeEvent.ToString(), dfdr.SensorName, dfdr.TT, dfdr.SensorInBase, dfdr.Identifier);
        }
        /// <summary>
        /// ������ ���������� EnvokenGetData ������
        /// </summary>
        /// <param name="dt"></param>
        protected void AnalizData(DataTable dt, ref DataForDataRow dfdr)
        {
            if (dt.Rows.Count == 0) return;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                ulong Sensors = (ulong)Convert.ToInt32(dt.Rows[j]["Sensor1"]);
                for (int i = 2; i < 9; i++)
                {
                    Sensors = Sensors << (8);
                    Sensors += (ulong)Convert.ToInt32(dt.Rows[j]["Sensor" + (i)]);
                }
                    if (dfdr.cBounce == 0) dfdr.timeEvent = (DateTime)dt.Rows[j]["FromUT"];
                    dfdr.TT = Convert.ToInt32(dt.Rows[j]["Mobitel_ID"]);
                    dfdr.StartBit = TT_SLs[dfdr.TT].StartBit;
                    dfdr.StopMove = (short)((Convert.ToInt32(dt.Rows[j]["Speed"]) == 0) ? 0 : 1);
                    dfdr.Identifier = (ushort)Convert.ToUInt16(Calibrate.ulongSector(BitConverter.GetBytes(Sensors), TT_SLs[dfdr.TT].Length, TT_SLs[dfdr.TT].StartBit));
                    //((ushort)((1 << TT_SLs[dfdr.TT].Length) - 1)) = 1023 ��� 4095 - 10 ��� 12 ��������� ����� �������� �������
                    if (dfdr.Identifier == ((ulong)((1 << TT_SLs[dfdr.TT].Length) - 1))) dfdr.Identifier = 0;    
                    dfdr.SensorInBase = 0;
                    dfdr.SensorName = "";
                // ������ �� �������
                    if (_RL == ReportsList.AGR_SENSOR_AGR || _RL == ReportsList.DRV_SENSOR_DRV)
                    {
                        if ((dfdr.Identifier != 0) && (dfdr.Identifier != _identSensor))
                        {
                            dfdr.Identifier = 0;
                        }
                        WriteSensorStateFilterSensor(ref dfdr);
                    }
                    else
                    {
                        WriteSensorStateFilterTT(ref dfdr);
                    }
                
                Application.DoEvents();
            }
            dt.Clear();
        }
        /// <summary>
        /// ������ ��������� ��������� �������� - ������ �� ��������
        /// </summary>
        /// <param name="dfdr"></param>
        protected void WriteSensorStateFilterSensor(ref DataForDataRow dfdr)
        {
            SensorNameStateIdentify(ref dfdr);
            //Debug.Print("SensorInBase {0} time {1} state {2} tt {3} iden {4}", dfdr.StartBit, dfdr.timeEvent.ToString(), dfdr.State, dfdr.TT, dfdr.Identifier);
            string key = dfdr.StartBit.ToString() + "_" + dfdr.TT.ToString() ;
            if (Sensor_State.ContainsKey(key))
            {
                if ((Sensor_State[key] != dfdr.State))
                {
                    if (dfdr.cBounce > POINTS_MAX_BOUNDS)
                    {
                        Sensor_State[key] = dfdr.State;
                        AddRow(ref dfdr);
                    }
                    else
                        dfdr.cBounce++;
                }
                //��������� ���������� �������
                else if (dfdr.timeEvent == _End)
                {
                        AddRow(ref dfdr);
                }
                // ����� ���������� ��������
                else
                    dfdr.cBounce = 0;
            }
            else
            {
                Sensor_State.Add(key, dfdr.State);
                AddRow(ref dfdr);
            }
        }
        /// <summary>
        /// ������ ��������� ��������� �������� - ������ �� ���������
        /// </summary>
        /// <param name="dfdr"></param>
        protected void WriteSensorStateFilterTT(ref DataForDataRow dfdr)
        {
            //Debug.Print("SensorInBase {0} time {1} state {2} tt {3} iden {4}", dfdr.StartBit, dfdr.timeEvent.ToString(), dfdr.State, dfdr.TT, dfdr.Identifier);
            SensorNameStateIdentify(ref dfdr);
            string key = dfdr.StartBit.ToString() + "_" + dfdr.TT.ToString();
            if (Sensor_State.ContainsKey(key))
            {
                if ((Sensor_State[key] != dfdr.State) || ((Sensor_Ident[key] != dfdr.Identifier) && (dfdr.Identifier!=0)))
                {
                    if (dfdr.cBounce > POINTS_MAX_BOUNDS)
                    {
                        if ((Sensor_Ident[key] != dfdr.Identifier) && (dfdr.Identifier != 0))
                        {
                                // ������ ����� ������� ��� ������ � 0.�������� ������:
                                //Identifier 63 time 15.09.2010 17:06:21 state 1 tt 115
                                //Identifier 63 time 15.09.2010 17:06:46 state 1 tt 115
                                //Identifier 47 time 15.09.2010 17:06:53 state 1 tt 115
                                //Identifier 47 time 15.09.2010 17:06:55 state 1 tt 115
                                // ������ ���������� � 0
                                ushort Ident_tmp = dfdr.Identifier;
                                if (Sensor_State[key] == 1)
                                {
                                    dfdr.Identifier = Sensor_Ident[key];
                                    SensorNameStateIdentify(ref dfdr);
                                    dfdr.State = 0;
                                    AddRow(ref dfdr);
                                }
                                // ��������� ������
                                dfdr.State = 1;
                                dfdr.Identifier = Ident_tmp;
                                Sensor_Ident[key] = dfdr.Identifier;
                                SensorNameStateIdentify(ref dfdr);
                                Sensor_State[key] = dfdr.State;
                                AddRow(ref dfdr);
                        }
                        else
                        {
                            if ((Sensor_State[key] != dfdr.State))
                            {
                                Sensor_State[key] = dfdr.State;
                                AddRow(ref dfdr);
                            }
                            else
                                dfdr.cBounce = 0;
                        }
                    }
                    else
                        dfdr.cBounce++;
                }
                //��������� ���������� �������
                else if (dfdr.timeEvent == _End)
                {
                    AddRow(ref dfdr);
                }
                // ����� ���������� ��������
                else
                    dfdr.cBounce = 0;
            }
            else
            {
                Sensor_Ident.Add(key, dfdr.Identifier);
                Sensor_State.Add(key, dfdr.State);
                AddRow(ref dfdr);
            }
        }
        /// <summary>
        /// ����������� ������� �� �������������� � DataGPS
        /// </summary>
        /// <param name="dfdr"></param>
        protected virtual void SensorNameStateIdentify(ref DataForDataRow dfdr)
        {
        }
        /// <summary>
        /// ����������� ���������� ������������ ���������������.���� � ������ �������
        /// ���� ����� ���������� ������ , �� ������ ������������ � �������� ���������� 
        /// </summary>
        /// <param name="dfdr"></param>
        protected void LimitUnknownId(ref DataForDataRow dfdr)
        {
            if ((!_UnknownId.Contains(-dfdr.Identifier)) && (_UnknownId.Count >= MAX_UNKNOWN_ID))
            {
                if (!_UnknownIdMessageShown)
                {
                    XtraMessageBox.Show(Resources.ExceedLimit, Resources.Restriction + ":" + MAX_UNKNOWN_ID);
                    _UnknownIdMessageShown = true;
                }
                dfdr.Identifier  = 0;
                dfdr.SensorInBase = 0;
                dfdr.SensorName = "";
                dfdr.State = 0;
            }
            else
            {
                dfdr.State = 1;
                dfdr.SensorInBase = -dfdr.Identifier;
                dfdr.SensorName = "ID_" + dfdr.Identifier.ToString();
            }
        }

        private void StopExecuting()
        {
            _flagStopExecuting = false;
        }

        #region IReport Members
        public virtual DataTable GetReport()
        {
            dtReport = new DataTable();
            ChangeStatusEvent( Resources.CreateReportsFields );
            CreateReportColumns();
            ChangeStatusEvent( Resources.SensorPresenceTest );

            if( GetMobitelsStartLength() )
            {
                DataForDataRow dfdr = new DataForDataRow( _Start );
                ChangeStatusEvent( Resources.DataSelect );
                //using (daa = new DictionaryAgroAgregat())
                //{
                // ��������� ����� ��������.���� �������� ������ (EnvokenGetData), ������ (AnalizData) �� ������������ 
                var gd = new GetData( EnvokenGetData /* ��� ������� ������ ������� ������ */);
                DateTime WorkTime = _Start;
                ChangeProgressBar( _End.Subtract( _Start ).Days + 1 );
                int cReads = 0;

                IAsyncResult iftAR = null;
                _flagStopExecuting = true;
                CallBacks.ButtonStopExec += StopExecuting;
                ChangeButtonBar( true );
                iftAR = gd.BeginInvoke( _TT, WorkTime, WorkTime.AddDays( 1 ).AddMinutes( -1 ), null, null );

                while( _End.Subtract( WorkTime ).TotalMinutes > 0 )
                {
                    cReads++;

                    while( !iftAR.IsCompleted )
                    {
                        Application.DoEvents();
                    }

                    if( !_flagStopExecuting )
                    {
                        break;
                    }

                    DataTable dt = gd.EndInvoke( iftAR );
                    WorkTime = WorkTime.AddDays( 1 );
                    iftAR = gd.BeginInvoke( _TT, WorkTime, WorkTime.AddDays( 1 ).AddMinutes( -1 ), null, null );
                    ChangeStatusEvent( Resources.Analiz + " " + WorkTime.AddDays( -1 ).ToString( "dd.MM.yyyy" ) );
                    AnalizData( dt, ref dfdr );
                    ChangeProgressBar( cReads );
                } // while

                CallBacks.ButtonStopExec -= StopExecuting;

                //��������� �������� (��� �������) 
                if( DateTime.Now.Subtract( _End ).TotalSeconds > 0 )
                    dfdr.timeEvent = _End;
                else
                    dfdr.timeEvent = DateTime.Now;

                dfdr.StopMove = 0;

                if( dfdr.State == 0 )
                {
                    foreach( KeyValuePair<int, TTStartLen> kvp in TT_SLs )
                    {
                        dfdr.TT = kvp.Key;
                        AddRow( ref dfdr );
                    }
                }
                else
                    AddRow( ref dfdr );
                //}
            } // if

            ChangeProgressBar( Consts.PROGRESS_BAR_STOP );
            return dtReport;
        } // GetReport
        #endregion
    }
}
