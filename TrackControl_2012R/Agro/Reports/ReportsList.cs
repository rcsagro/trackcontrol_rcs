using System;
using System.Collections.Generic;
using System.Text;

namespace Agro
{
    public enum ReportsList
    {
        /// <summary>
        /// ����� ������������ �������� ��������� ������������ ������ ���������
        /// </summary>
        AGR_SENSOR_TT,
        /// <summary>
        /// ����� ������������ ������� ������ ��������� ������������ �� ���� ����������
        /// </summary>
        AGR_SENSOR_AGR,
        /// <summary>
        /// ����� ������������ �������� �������� ������ ���������
        /// </summary>
        DRV_SENSOR_TT,
        /// <summary>
        /// ����� ������������ ������� ������ �������� �� ���� ����������
        /// </summary>
        DRV_SENSOR_DRV
    }
}
