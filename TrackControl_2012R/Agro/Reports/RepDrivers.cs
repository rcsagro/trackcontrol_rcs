using System;
using System.Collections.Generic;
using System.Text;
using System.Data ;
using TrackControl.General;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Agro
{
    /// <summary>
    /// ������ �� �������
    /// </summary>
    public class RepDrivers
    {
        public RepDrivers()
        {
        }
        private enum RowStatus {Summary,Day}
        /// <summary>
        /// ������������ ������ �� ���������
        /// </summary>
        public DataTable DriversSummary(DateTime dtBegin, DateTime dtEnd,string sTableName)
        {
            DateTime _dtBegin = new DateTime(dtBegin.Year, dtBegin.Month, dtBegin.Day, 0, 0, 0);
            DateTime _dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 23, 59, 59);
            //������ ��� �� ������� ���� ������ - �������� �� ���������� �������
            List<DateTime> lsDateOrder = new List<DateTime>(); 
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                string sSQLselect = "";
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    sSQLselect = @"SELECT   d.id , Concat(d.Family , ' ' , d.Name) as Family,agro_order.`Date`, agro_ordert.*
             FROM    agro_ordert 
             INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
             INNER JOIN  driver d ON agro_ordert.Id_driver = d.id 
             WHERE   agro_order.`Date` >= ?TimeStart
             AND agro_order.`Date`<= ?TimeEnd
             ORDER BY d.Family,d.id";
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    sSQLselect = @"SELECT   d.id , Concat(d.Family , ' ' , d.Name) as Family,agro_order.Date, agro_ordert.*
             FROM    agro_ordert 
             INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
             INNER JOIN  driver d ON agro_ordert.Id_driver = d.id 
             WHERE   agro_order.Date >= @TimeStart
             AND agro_order.Date<= @TimeEnd
             ORDER BY d.Family,d.id";
                }
            MySqlParameter[] parDate = new MySqlParameter[2];
            parDate[0] = new MySqlParameter();
            parDate[0].ParameterName = driverDb.ParamPrefics + "TimeStart";
            parDate[0].Value = _dtBegin;
            parDate[0].MySqlDbType = MySqlDbType.DateTime;
            parDate[1] = new MySqlParameter();
            parDate[1].ParameterName = driverDb.ParamPrefics + "TimeEnd";
            parDate[1].Value = _dtEnd;
            parDate[1].MySqlDbType = MySqlDbType.DateTime;
            MySqlDataReader dr = _cnMySQL.GetDataReader(sSQLselect, parDate);
            DataTable dtReport = new DataTable(sTableName);
            //���� ������
            CreateReportColumns(dtReport, RowStatus.Summary);
            int Id_drv = 0;
            double dbDUT = 0;
            double dbDRT = 0;
            DataRow row = null;
            DateTime dtWork = DateTime.Today;
            while (dr.Read())
            {
                if (Id_drv != dr.GetInt32(dr.GetOrdinal("ID")))
                {
                    if (Id_drv > 0)
                    {
                        if (dbDRT > 0)
                            row["Fuel"] = dbDRT;
                        else
                        {
                            if (dbDUT > 0)
                                row["Fuel"] = dbDUT;
                        }
                        dtReport.Rows.Add(row);
                    }
                    Id_drv = dr.GetInt32(dr.GetOrdinal("ID"));
                    row = dtReport.NewRow();
                    row["ID"] = Id_drv;
                    row["Family"] = dr.GetString(dr.GetOrdinal("Family"));
                    dtWork = dr.GetDateTime(dr.GetOrdinal("Date"));
                    if (!lsDateOrder.Contains(dtWork)) lsDateOrder.Add(dtWork); 
                    row["Days"] = 1;
                    dbDRT = 0;
                    dbDUT = 0;
                 }
                 if (dtWork != dr.GetDateTime(dr.GetOrdinal("Date")))
                 {
                     dtWork = dr.GetDateTime(dr.GetOrdinal("Date"));
                     row["Days"] = (int)row["Days"] + 1;
                 }
                 row = AddReaderDrv(row, dr, ref dbDUT, ref dbDRT);
            }
            if (Id_drv > 0)
            {
                if (dbDRT > 0)
                    row["Fuel"] = dbDRT;
                else
                {
                    if (dbDUT > 0)
                        row["Fuel"] = dbDUT;
                }
                dtReport.Rows.Add(row);
            }
            return dtReport;
            }
            
        }
        /// <summary>
        /// ��������� ���������� �� ����
        /// </summary>
        /// <param name="sTableName"> ��� ����������� ������� </param>
        /// <returns></returns>
        public DataTable DriversDays(DateTime dtBegin, DateTime dtEnd, string sTableName)
        {
            DateTime _dtBegin = new DateTime(dtBegin.Year, dtBegin.Month, dtBegin.Day, 0, 0, 0);
            DateTime _dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 23, 59, 59);
            //������ ��� �� ������� ���� ������ - �������� �� ���������� �������
            List<DateTime> lsDateOrder = new List<DateTime>();
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                string sSQLselect = "";
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    sSQLselect = @"SELECT   agro_ordert.Id_driver,agro_order.`Date`, agro_ordert.*
                FROM    agro_ordert 
                INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                INNER JOIN driver ON agro_ordert.Id_driver = driver.id 
                WHERE   agro_order.`Date` >= ?TimeStart
                AND agro_order.`Date`<= ?TimeEnd 
                ORDER BY agro_ordert.Id_driver,agro_order.`Date`";
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    sSQLselect = @"SELECT   agro_ordert.Id_driver,agro_order.Date, agro_ordert.*
                FROM    agro_ordert 
                INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                INNER JOIN driver ON agro_ordert.Id_driver = driver.id 
                WHERE   agro_order.Date >= @TimeStart
                AND agro_order.Date<= @TimeEnd 
                ORDER BY agro_ordert.Id_driver,agro_order.Date";
                }
                MySqlParameter[] parDate = new MySqlParameter[2];
                parDate[0] = new MySqlParameter();
                parDate[0].ParameterName = driverDb.ParamPrefics + "TimeStart";
                parDate[0].Value = _dtBegin;
                parDate[0].MySqlDbType = MySqlDbType.DateTime;
                parDate[1] = new MySqlParameter();
                parDate[1].ParameterName = driverDb.ParamPrefics + "?TimeEnd";
                parDate[1].Value = _dtEnd;
                parDate[1].MySqlDbType = MySqlDbType.DateTime;
                MySqlDataReader dr = _cnMySQL.GetDataReader(sSQLselect, parDate);
                DataTable dtReport = new DataTable(sTableName);
                //���� ������
                CreateReportColumns(dtReport, RowStatus.Day);
                int Id_drv = 0;
                double dbDUT = 0;
                double dbDRT = 0;
                DataRow row = null;
                DateTime dtWork = DateTime.MinValue;
                while (dr.Read())
                {
                    if (dtWork != dr.GetDateTime(dr.GetOrdinal("Date")) || Id_drv != dr.GetInt32(dr.GetOrdinal("Id_driver")))
                    {
                        if (Id_drv > 0)
                        {
                            if (dbDRT > 0)
                                row["Fuel"] = dbDRT;
                            else
                            {
                                if (dbDUT > 0)
                                    row["Fuel"] = dbDUT;
                            }
                            dtReport.Rows.Add(row);
                        }
                        Id_drv = dr.GetInt32(dr.GetOrdinal("Id_driver"));
                        dtWork = dr.GetDateTime(dr.GetOrdinal("Date"));
                        row = dtReport.NewRow();
                        row["Id_driver"] = Id_drv;
                        row["Date"] = dtWork;
                        dbDRT = 0;
                        dbDUT = 0;
                    }
                    row = AddReaderDrv(row, dr, ref dbDUT, ref dbDRT);
                }
                if (Id_drv > 0)
                {
                    if (dbDRT > 0)
                        row["Fuel"] = dbDRT;
                    else
                    {
                        if (dbDUT > 0)
                            row["Fuel"] = dbDUT;
                    }
                    dtReport.Rows.Add(row);
                }
                return dtReport;
            }
        }
        /// <summary>
        /// ���������� ���� � ���������� ��� ������
        /// </summary>
        /// <returns></returns>
        private int CountDaysWOrder(DateTime dtBegin, DateTime dtEnd, List<DateTime> lsDateOrder)
        {
            int iCountDaysWOrder = 0;
            using (ConnectMySQL _cnMySQL = new ConnectMySQL())
            {
                for (int i = 0; i < dtEnd.Subtract(dtBegin).TotalDays; i++)
                {
                    DateTime dtWork = dtBegin.AddDays(i);
                    if (!lsDateOrder.Contains(dtWork))
                    {
                        string SQLselect = "SELECT  datagps.DataGps_ID FROM  datagps "
                        + " WHERE  Date_Format(FROM_UNIXTIME(datagps.UnixTime), '%d-%m-%Y') = '"
                        + dtWork.Day.ToString() + "-" + dtWork.Month.ToString() + "-" + dtWork.Year.ToString() + "'  Limit 1";
                        if (_cnMySQL.GetScalarValueNull<Int32>(SQLselect, 0) != 0) iCountDaysWOrder++;
                        Application.DoEvents();
                    }
                }
                return iCountDaysWOrder;
            }
        }
        private  DataRow AddReaderDrv(DataRow row, MySqlDataReader dr,ref double dbDUT,ref double dbDRT)
        {
            if (!dr.IsDBNull(dr.GetOrdinal("TimeMove"))) 
            row["Dist"] = ((double)row["Dist"]) + dr.GetDouble(dr.GetOrdinal("Distance"));
        if (!dr.IsDBNull(dr.GetOrdinal("FactSquareCalc"))) 
            row["SquareCont"] = ((double)row["SquareCont"]) + dr.GetDouble(dr.GetOrdinal("FactSquareCalc"));
        if (!dr.IsDBNull(dr.GetOrdinal("FactSquare"))) 
            row["SquareDist"] = ((double)row["SquareDist"]) + dr.GetDouble(dr.GetOrdinal("FactSquare"));
            if (dr.GetDouble(dr.GetOrdinal("Id_field")) == 0)
            {
                if (!dr.IsDBNull(dr.GetOrdinal("Distance"))) 
                row["DistFieldOut"] = ((double)row["DistFieldOut"]) + dr.GetDouble(dr.GetOrdinal("Distance"));
                if (!dr.IsDBNull(dr.GetOrdinal("TimeMove"))) 
                row["WorkTimeFieldOut"] = ((TimeSpan)row["WorkTimeFieldOut"]).Add(dr.GetTimeSpan(dr.GetOrdinal("TimeMove")));
            }
            else
            {
                if (!dr.IsDBNull(dr.GetOrdinal("TimeMove")))  
                row["WorkTimeField"] = ((TimeSpan)row["WorkTimeField"]).Add(dr.GetTimeSpan(dr.GetOrdinal("TimeMove")));
                if (!dr.IsDBNull(dr.GetOrdinal("TimeStop")))  
                row["WorkTimeFieldStop"] = ((TimeSpan)row["WorkTimeFieldStop"]).Add(dr.GetTimeSpan(dr.GetOrdinal("TimeStop")));
            }
            row["WorkTime"] = ((TimeSpan)row["WorkTime"]).Add(dr.GetDateTime(dr.GetOrdinal("TimeEnd")).Subtract(dr.GetDateTime(dr.GetOrdinal("TimeStart"))));
            dbDUT += dr.GetDouble(dr.GetOrdinal("FuelExpens"));
            dbDRT += dr.GetDouble(dr.GetOrdinal("Fuel_ExpensTotal"));
            return row;
        }
        private void CreateReportColumns(DataTable dtReport,RowStatus rs)
        {
            DataColumn col;
            if (rs == RowStatus.Summary)
            {
                //�������� ���
                col = new DataColumn("ID", System.Type.GetType("System.Int32"));
                dtReport.Columns.Add(col);
                //��������
                col = new DataColumn("Family", System.Type.GetType("System.String"));
                dtReport.Columns.Add(col);
                //������� ���� 
                col = new DataColumn("Days", System.Type.GetType("System.Int32"));
                col.DefaultValue = 0;
                dtReport.Columns.Add(col);
            }
            else
            {
                //�������� ���
                col = new DataColumn("Id_driver", System.Type.GetType("System.Int32"));
                col.DefaultValue = 0;
                dtReport.Columns.Add(col);
                //����
                col = new DataColumn("Date", System.Type.GetType("System.DateTime"));
                dtReport.Columns.Add(col);
            }
                //������� �����
            col = new DataColumn("WorkTime", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������ �����
            col = new DataColumn("Dist", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������ ��� �����
            col = new DataColumn("DistFieldOut", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������ � �����
            col = new DataColumn("WorkTimeField", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������� � �����
            col = new DataColumn("WorkTimeFieldStop", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� �������� ��� �����
            col = new DataColumn("WorkTimeFieldOut", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������������ ������� �� �������
            col = new DataColumn("SquareCont", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������������ ������� �� �������
            col = new DataColumn("SquareDist", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������ �������
            col = new DataColumn("Fuel", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
        }
    }
}
 