﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Agro.Utilites;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public class RepDriverVehicleCategoryData
    {
        public RepDriverVehicleCategoryData()
        {

        }

        public RepDriverVehicleCategoryView Viewer { private get; set; }

        public DataTable GetData(DateTime begin, DateTime end)
        {
            DataTable dataSource;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.RepDriverVehicleCategoryData.SelectDriverRecords,
                                           AgroQuery.SqlVehicleIdent, driverDb.ParamPrefics,
                                           AgroQuery.SqlFieldIdent, AgroQuery.SqlDriverIdent,
                                           Viewer.Group, Viewer.Vehicle, Viewer.Category, Viewer.Category2,
                                           Viewer.Category3,Viewer.Category4);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", end);
                dataSource = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
                foreach (DataRow dr in dataSource.Rows)
                {
                    if ((double)dr["FuelExpens"] == 0 && (double)dr["Fuel_ExpensTotal"] > 0)
                    {
                        dr["FuelExpens"] = dr["Fuel_ExpensTotal"];
                    }
                    if ((double)dr["FuelExpensAvg"] == 0 && (double)dr["Fuel_ExpensAvg"] > 0)
                    {
                        dr["FuelExpensAvg"] = dr["Fuel_ExpensAvg"];
                    }
                }
            }
            return dataSource; 
        }
    }
}
