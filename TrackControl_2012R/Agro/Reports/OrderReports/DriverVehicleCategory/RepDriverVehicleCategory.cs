﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro.Agro.Reports.OrderReports.DriversAgregatsWorks;
using Agro.Properties;
using Agro.XtraReports;
using DevExpress.XtraReports.UI;
using TrackControl.General;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public class RepDriverVehicleCategory : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = Resources.DriverVehicleCategory;
        readonly Image _largeImage = Shared.NoUser;
        readonly Image _smallImage = Shared.NoUser;

        private DataTable _dtContent;
        private DateTime _dateStart;
        private DateTime _dateEnd;
        private int _driverId;

        public RepDriverVehicleCategory()
        {
            _dataViewer = new RepDriverVehicleCategoryView {Dock = DockStyle.Fill};
        }
        
        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            _dateStart = begin;
            _dateEnd = end;
            var repotrsData = new RepDriverVehicleCategoryData {Viewer = (RepDriverVehicleCategoryView) _dataViewer};
            _dtContent = repotrsData.GetData(begin, end);
            ((RepDriverVehicleCategoryView)_dataViewer).gcReport.DataSource = _dtContent;
            ((RepDriverVehicleCategoryView)_dataViewer).Begin = begin;
            ((RepDriverVehicleCategoryView)_dataViewer).End = end;
        }

        //, ((RepDriverVehicleCategoryView)_dataViewer).Driver

        public bool IsHasData
        {
            get { return ((RepDriverVehicleCategoryView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepDriverVehicleCategoryView)_dataViewer).ExportToExcel();
        }

        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }

    }
}
