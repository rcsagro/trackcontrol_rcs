namespace Agro.Reports.OrderReports
{
    partial class RepDriverVehicleCategoryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepDriverVehicleCategoryView));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCategoryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverOutCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleOutCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeOutCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatOutCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldOutCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcCont = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClarified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpence = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenceInFields = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPricing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bleVehiclesGroups = new DevExpress.XtraBars.BarEditItem();
            this.leVehiclesGroups = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleCategory = new DevExpress.XtraBars.BarEditItem();
            this.leCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleCategory2 = new DevExpress.XtraBars.BarEditItem();
            this.leCategory2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleCategory3 = new DevExpress.XtraBars.BarEditItem();
            this.leCategory3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleCategory4 = new DevExpress.XtraBars.BarEditItem();
            this.leCategory4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.leVehicles = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.bleVehicle = new DevExpress.XtraBars.BarEditItem();
            this.leVehicle = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bbiFilterClear = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehiclesGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicle)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 46);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teTime});
            this.gcReport.Size = new System.Drawing.Size(891, 448);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCategoryName,
            this.colCategoryName2,
            this.colCategoryName3,
            this.colCategoryName4,
            this.colDriver,
            this.colDriverOutCode,
            this.colVehicle,
            this.colVehicleOutCode,
            this.colWorkType,
            this.colWorkTypeOutCode,
            this.colAgregat,
            this.colAgregatOutCode,
            this.colField,
            this.colFieldOutCode,
            this.colEnter,
            this.colExit,
            this.colDistance,
            this.colProcDistance,
            this.colProcCont,
            this.colClarified,
            this.colFuelExpence,
            this.colFuelExpenceInFields,
            this.colSpeedAvg,
            this.colPricing,
            this.colSalary});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colCategoryName
            // 
            this.colCategoryName.AppearanceCell.Options.UseTextOptions = true;
            this.colCategoryName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategoryName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategoryName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategoryName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategoryName.Caption = "���������";
            this.colCategoryName.FieldName = "CategoryName";
            this.colCategoryName.Name = "colCategoryName";
            this.colCategoryName.OptionsColumn.AllowEdit = false;
            this.colCategoryName.OptionsColumn.ReadOnly = true;
            this.colCategoryName.Visible = true;
            this.colCategoryName.VisibleIndex = 0;
            // 
            // colCategoryName2
            // 
            this.colCategoryName2.AppearanceCell.Options.UseTextOptions = true;
            this.colCategoryName2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategoryName2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategoryName2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategoryName2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategoryName2.Caption = "��������� 2";
            this.colCategoryName2.FieldName = "CategoryName2";
            this.colCategoryName2.Name = "colCategoryName2";
            this.colCategoryName2.OptionsColumn.AllowEdit = false;
            this.colCategoryName2.OptionsColumn.ReadOnly = true;
            this.colCategoryName2.Visible = true;
            this.colCategoryName2.VisibleIndex = 1;
            // 
            // colCategoryName3
            // 
            this.colCategoryName3.AppearanceCell.Options.UseTextOptions = true;
            this.colCategoryName3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategoryName3.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategoryName3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategoryName3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategoryName3.Caption = "��������� 3";
            this.colCategoryName3.FieldName = "CategoryName3";
            this.colCategoryName3.Name = "colCategoryName3";
            this.colCategoryName3.OptionsColumn.AllowEdit = false;
            this.colCategoryName3.OptionsColumn.ReadOnly = true;
            this.colCategoryName3.Visible = true;
            this.colCategoryName3.VisibleIndex = 2;
            // 
            // colCategoryName4
            // 
            this.colCategoryName4.AppearanceCell.Options.UseTextOptions = true;
            this.colCategoryName4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCategoryName4.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategoryName4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategoryName4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCategoryName4.Caption = "��������� 4";
            this.colCategoryName4.FieldName = "CategoryName4";
            this.colCategoryName4.Name = "colCategoryName4";
            this.colCategoryName4.OptionsColumn.AllowEdit = false;
            this.colCategoryName4.OptionsColumn.ReadOnly = true;
            this.colCategoryName4.Visible = true;
            this.colCategoryName4.VisibleIndex = 3;
            // 
            // colDriver
            // 
            this.colDriver.AppearanceCell.Options.UseTextOptions = true;
            this.colDriver.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriver.Caption = "��������";
            this.colDriver.FieldName = "DriverName";
            this.colDriver.Name = "colDriver";
            this.colDriver.OptionsColumn.AllowEdit = false;
            this.colDriver.OptionsColumn.ReadOnly = true;
            this.colDriver.Visible = true;
            this.colDriver.VisibleIndex = 4;
            // 
            // colDriverOutCode
            // 
            this.colDriverOutCode.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverOutCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverOutCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverOutCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverOutCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverOutCode.Caption = "��� ������� ����";
            this.colDriverOutCode.FieldName = "DriverOutLinkId";
            this.colDriverOutCode.Name = "colDriverOutCode";
            this.colDriverOutCode.OptionsColumn.AllowEdit = false;
            this.colDriverOutCode.OptionsColumn.ReadOnly = true;
            this.colDriverOutCode.Visible = true;
            this.colDriverOutCode.VisibleIndex = 5;
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicle.Caption = "������������ ��������";
            this.colVehicle.FieldName = "VehicleName";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.OptionsColumn.ReadOnly = true;
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 6;
            this.colVehicle.Width = 111;
            // 
            // colVehicleOutCode
            // 
            this.colVehicleOutCode.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleOutCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleOutCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleOutCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleOutCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleOutCode.Caption = "��� ������� ����";
            this.colVehicleOutCode.FieldName = "VehicleOutLinkId";
            this.colVehicleOutCode.Name = "colVehicleOutCode";
            this.colVehicleOutCode.OptionsColumn.AllowEdit = false;
            this.colVehicleOutCode.OptionsColumn.ReadOnly = true;
            this.colVehicleOutCode.Visible = true;
            this.colVehicleOutCode.VisibleIndex = 7;
            this.colVehicleOutCode.Width = 42;
            // 
            // colWorkType
            // 
            this.colWorkType.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWorkType.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkType.Caption = "��� �����";
            this.colWorkType.FieldName = "WorkName";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            this.colWorkType.Visible = true;
            this.colWorkType.VisibleIndex = 8;
            this.colWorkType.Width = 111;
            // 
            // colWorkTypeOutCode
            // 
            this.colWorkTypeOutCode.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkTypeOutCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTypeOutCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkTypeOutCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTypeOutCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkTypeOutCode.Caption = "��� ������� ����";
            this.colWorkTypeOutCode.FieldName = "WorkOutLinkId";
            this.colWorkTypeOutCode.Name = "colWorkTypeOutCode";
            this.colWorkTypeOutCode.OptionsColumn.AllowEdit = false;
            this.colWorkTypeOutCode.OptionsColumn.ReadOnly = true;
            this.colWorkTypeOutCode.Visible = true;
            this.colWorkTypeOutCode.VisibleIndex = 9;
            this.colWorkTypeOutCode.Width = 44;
            // 
            // colAgregat
            // 
            this.colAgregat.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregat.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregat.Caption = "�������� ������������";
            this.colAgregat.FieldName = "AgregatName";
            this.colAgregat.Name = "colAgregat";
            this.colAgregat.OptionsColumn.AllowEdit = false;
            this.colAgregat.OptionsColumn.ReadOnly = true;
            this.colAgregat.Visible = true;
            this.colAgregat.VisibleIndex = 10;
            this.colAgregat.Width = 130;
            // 
            // colAgregatOutCode
            // 
            this.colAgregatOutCode.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregatOutCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatOutCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatOutCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatOutCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregatOutCode.Caption = "��� ������� ����";
            this.colAgregatOutCode.FieldName = "AgregatOutLinkId";
            this.colAgregatOutCode.Name = "colAgregatOutCode";
            this.colAgregatOutCode.OptionsColumn.AllowEdit = false;
            this.colAgregatOutCode.OptionsColumn.ReadOnly = true;
            this.colAgregatOutCode.Visible = true;
            this.colAgregatOutCode.VisibleIndex = 11;
            this.colAgregatOutCode.Width = 32;
            // 
            // colField
            // 
            this.colField.AppearanceCell.Options.UseTextOptions = true;
            this.colField.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colField.AppearanceHeader.Options.UseTextOptions = true;
            this.colField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colField.Caption = "����";
            this.colField.FieldName = "FieldName";
            this.colField.Name = "colField";
            this.colField.OptionsColumn.AllowEdit = false;
            this.colField.OptionsColumn.ReadOnly = true;
            this.colField.Visible = true;
            this.colField.VisibleIndex = 12;
            this.colField.Width = 114;
            // 
            // colFieldOutCode
            // 
            this.colFieldOutCode.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldOutCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldOutCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldOutCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldOutCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldOutCode.Caption = "��� ������� ����";
            this.colFieldOutCode.FieldName = "ZoneOutLinkId";
            this.colFieldOutCode.Name = "colFieldOutCode";
            this.colFieldOutCode.OptionsColumn.AllowEdit = false;
            this.colFieldOutCode.OptionsColumn.ReadOnly = true;
            this.colFieldOutCode.Visible = true;
            this.colFieldOutCode.VisibleIndex = 13;
            this.colFieldOutCode.Width = 55;
            // 
            // colEnter
            // 
            this.colEnter.AppearanceCell.Options.UseTextOptions = true;
            this.colEnter.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnter.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEnter.Caption = "�����";
            this.colEnter.DisplayFormat.FormatString = "t";
            this.colEnter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEnter.FieldName = "TimeStart";
            this.colEnter.Name = "colEnter";
            this.colEnter.OptionsColumn.AllowEdit = false;
            this.colEnter.OptionsColumn.ReadOnly = true;
            this.colEnter.Visible = true;
            this.colEnter.VisibleIndex = 14;
            this.colEnter.Width = 36;
            // 
            // colExit
            // 
            this.colExit.AppearanceCell.Options.UseTextOptions = true;
            this.colExit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExit.AppearanceHeader.Options.UseTextOptions = true;
            this.colExit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExit.Caption = "�����";
            this.colExit.DisplayFormat.FormatString = "t";
            this.colExit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExit.FieldName = "TimeEnd";
            this.colExit.Name = "colExit";
            this.colExit.OptionsColumn.AllowEdit = false;
            this.colExit.OptionsColumn.ReadOnly = true;
            this.colExit.Visible = true;
            this.colExit.VisibleIndex = 15;
            this.colExit.Width = 46;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "���������� ����";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 16;
            this.colDistance.Width = 55;
            // 
            // colProcDistance
            // 
            this.colProcDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colProcDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colProcDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProcDistance.Caption = "���������� (�� �������), ��";
            this.colProcDistance.FieldName = "FactSquare";
            this.colProcDistance.Name = "colProcDistance";
            this.colProcDistance.OptionsColumn.AllowEdit = false;
            this.colProcDistance.OptionsColumn.ReadOnly = true;
            this.colProcDistance.Visible = true;
            this.colProcDistance.VisibleIndex = 17;
            this.colProcDistance.Width = 51;
            // 
            // colProcCont
            // 
            this.colProcCont.AppearanceCell.Options.UseTextOptions = true;
            this.colProcCont.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcCont.AppearanceHeader.Options.UseTextOptions = true;
            this.colProcCont.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcCont.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProcCont.Caption = "���������� (�� �������), ��";
            this.colProcCont.FieldName = "FactSquareCalc";
            this.colProcCont.Name = "colProcCont";
            this.colProcCont.OptionsColumn.AllowEdit = false;
            this.colProcCont.OptionsColumn.ReadOnly = true;
            this.colProcCont.Visible = true;
            this.colProcCont.VisibleIndex = 18;
            this.colProcCont.Width = 46;
            // 
            // colClarified
            // 
            this.colClarified.AppearanceCell.Options.UseTextOptions = true;
            this.colClarified.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colClarified.AppearanceHeader.Options.UseTextOptions = true;
            this.colClarified.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colClarified.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colClarified.Caption = "���������, ��";
            this.colClarified.FieldName = "FactSquareClarified";
            this.colClarified.Name = "colClarified";
            this.colClarified.Visible = true;
            this.colClarified.VisibleIndex = 19;
            this.colClarified.Width = 67;
            // 
            // colFuelExpence
            // 
            this.colFuelExpence.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpence.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpence.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpence.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpence.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpence.Caption = "����� ������, �";
            this.colFuelExpence.FieldName = "FuelExpens";
            this.colFuelExpence.Name = "colFuelExpence";
            this.colFuelExpence.OptionsColumn.AllowEdit = false;
            this.colFuelExpence.OptionsColumn.ReadOnly = true;
            this.colFuelExpence.Visible = true;
            this.colFuelExpence.VisibleIndex = 20;
            this.colFuelExpence.Width = 57;
            // 
            // colFuelExpenceInFields
            // 
            this.colFuelExpenceInFields.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenceInFields.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenceInFields.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenceInFields.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenceInFields.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenceInFields.Caption = "������ � �����, �/��";
            this.colFuelExpenceInFields.FieldName = "FuelExpensAvg";
            this.colFuelExpenceInFields.Name = "colFuelExpenceInFields";
            this.colFuelExpenceInFields.OptionsColumn.AllowEdit = false;
            this.colFuelExpenceInFields.OptionsColumn.ReadOnly = true;
            this.colFuelExpenceInFields.Visible = true;
            this.colFuelExpenceInFields.VisibleIndex = 21;
            this.colFuelExpenceInFields.Width = 51;
            // 
            // colSpeedAvg
            // 
            this.colSpeedAvg.AppearanceCell.Options.UseTextOptions = true;
            this.colSpeedAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedAvg.Caption = "������� ��������";
            this.colSpeedAvg.FieldName = "SpeedAvg";
            this.colSpeedAvg.Name = "colSpeedAvg";
            this.colSpeedAvg.OptionsColumn.AllowEdit = false;
            this.colSpeedAvg.OptionsColumn.ReadOnly = true;
            this.colSpeedAvg.Visible = true;
            this.colSpeedAvg.VisibleIndex = 22;
            this.colSpeedAvg.Width = 61;
            // 
            // colPricing
            // 
            this.colPricing.AppearanceCell.Options.UseTextOptions = true;
            this.colPricing.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricing.AppearanceHeader.Options.UseTextOptions = true;
            this.colPricing.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricing.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPricing.Caption = "��������";
            this.colPricing.FieldName = "Pricing";
            this.colPricing.Name = "colPricing";
            this.colPricing.Visible = true;
            this.colPricing.VisibleIndex = 23;
            this.colPricing.Width = 48;
            // 
            // colSalary
            // 
            this.colSalary.AppearanceCell.Options.UseTextOptions = true;
            this.colSalary.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalary.AppearanceHeader.Options.UseTextOptions = true;
            this.colSalary.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalary.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSalary.Caption = "������ �����";
            this.colSalary.FieldName = "Salary";
            this.colSalary.Name = "colSalary";
            this.colSalary.Visible = true;
            this.colSalary.VisibleIndex = 24;
            this.colSalary.Width = 20;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.teTime.Name = "teTime";
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bleVehiclesGroups,
            this.bleCategory,
            this.bleCategory2,
            this.bleCategory3,
            this.bleCategory4,
            this.bleVehicle,
            this.bbiFilterClear});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leVehiclesGroups,
            this.leVehicles,
            this.leCategory,
            this.leCategory2,
            this.leCategory3,
            this.leCategory4,
            this.leVehicle});
            // 
            // bar2
            // 
            this.bar2.BarName = "������� ����";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleVehiclesGroups, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(this.bleCategory),
            new DevExpress.XtraBars.LinkPersistInfo(this.bleCategory2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bleCategory3),
            new DevExpress.XtraBars.LinkPersistInfo(this.bleCategory4),
            new DevExpress.XtraBars.LinkPersistInfo(this.bleVehicle),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiFilterClear, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "������� ����";
            // 
            // bleVehiclesGroups
            // 
            this.bleVehiclesGroups.AutoFillWidth = true;
            this.bleVehiclesGroups.Caption = "����� ����������";
            this.bleVehiclesGroups.Edit = this.leVehiclesGroups;
            this.bleVehiclesGroups.Id = 0;
            this.bleVehiclesGroups.Name = "bleVehiclesGroups";
            this.bleVehiclesGroups.Width = 120;
            this.bleVehiclesGroups.EditValueChanged += new System.EventHandler(this.bleVehiclesGroups_EditValueChanged);
            // 
            // leVehiclesGroups
            // 
            this.leVehiclesGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leVehiclesGroups.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leVehiclesGroups.DisplayMember = "Name";
            this.leVehiclesGroups.DropDownRows = 20;
            this.leVehiclesGroups.Name = "leVehiclesGroups";
            this.leVehiclesGroups.NullText = "";
            this.leVehiclesGroups.ShowHeader = false;
            this.leVehiclesGroups.ValueMember = "Id";
            // 
            // bleCategory
            // 
            this.bleCategory.AutoFillWidth = true;
            this.bleCategory.Caption = "���������";
            this.bleCategory.Edit = this.leCategory;
            this.bleCategory.Id = 3;
            this.bleCategory.Name = "bleCategory";
            this.bleCategory.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleCategory.Width = 100;
            this.bleCategory.EditValueChanged += new System.EventHandler(this.bleCategory_EditValueChanged);
            // 
            // leCategory
            // 
            this.leCategory.AutoHeight = false;
            this.leCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leCategory.DisplayMember = "Name";
            this.leCategory.DropDownRows = 20;
            this.leCategory.Name = "leCategory";
            this.leCategory.NullText = "";
            this.leCategory.ShowHeader = false;
            this.leCategory.ValueMember = "Id";
            // 
            // bleCategory2
            // 
            this.bleCategory2.AutoFillWidth = true;
            this.bleCategory2.Caption = "��������� 2";
            this.bleCategory2.Edit = this.leCategory2;
            this.bleCategory2.Id = 3;
            this.bleCategory2.Name = "bleCategory2";
            this.bleCategory2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleCategory2.Width = 100;
            // 
            // leCategory2
            // 
            this.leCategory2.AutoHeight = false;
            this.leCategory2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leCategory2.DisplayMember = "Name";
            this.leCategory2.DropDownRows = 20;
            this.leCategory2.Name = "leCategory2";
            this.leCategory2.NullText = "";
            this.leCategory2.ShowHeader = false;
            this.leCategory2.ValueMember = "Id";
            // 
            // bleCategory3
            // 
            this.bleCategory3.AutoFillWidth = true;
            this.bleCategory3.Caption = "��������� 3";
            this.bleCategory3.Edit = this.leCategory3;
            this.bleCategory3.Id = 3;
            this.bleCategory3.Name = "bleCategory3";
            this.bleCategory3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleCategory3.Width = 100;
            // 
            // leCategory3
            // 
            this.leCategory3.AutoHeight = false;
            this.leCategory3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory3.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leCategory3.DisplayMember = "Name";
            this.leCategory3.DropDownRows = 20;
            this.leCategory3.Name = "leCategory3";
            this.leCategory3.NullText = "";
            this.leCategory3.ShowHeader = false;
            this.leCategory3.ValueMember = "Id";
            // 
            // bleCategory4
            // 
            this.bleCategory4.AutoFillWidth = true;
            this.bleCategory4.Caption = "��������� 4";
            this.bleCategory4.Edit = this.leCategory4;
            this.bleCategory4.Id = 3;
            this.bleCategory4.Name = "bleCategory4";
            this.bleCategory4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleCategory4.Width = 100;
            // 
            // leCategory4
            // 
            this.leCategory4.AutoHeight = false;
            this.leCategory4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leCategory4.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leCategory4.DisplayMember = "Name";
            this.leCategory4.DropDownRows = 20;
            this.leCategory4.Name = "leCategory4";
            this.leCategory4.NullText = "";
            this.leCategory4.ShowHeader = false;
            this.leCategory4.ValueMember = "Id";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(891, 46);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(891, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 46);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 448);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(891, 46);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 448);
            // 
            // leVehicles
            // 
            this.leVehicles.AutoHeight = false;
            this.leVehicles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leVehicles.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Info")});
            this.leVehicles.DisplayMember = "Info";
            this.leVehicles.DropDownRows = 30;
            this.leVehicles.Name = "leVehicles";
            this.leVehicles.NullText = "";
            this.leVehicles.ShowHeader = false;
            this.leVehicles.ValueMember = "Id";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "����� ����������";
            this.barEditItem1.Edit = this.leVehiclesGroups;
            this.barEditItem1.Id = 0;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.Width = 124;
            // 
            // bleVehicle
            // 
            this.bleVehicle.AutoFillWidth = true;
            this.bleVehicle.Caption = "���������";
            this.bleVehicle.Edit = this.leVehicle;
            this.bleVehicle.Id = 4;
            this.bleVehicle.Name = "bleVehicle";
            this.bleVehicle.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.bleVehicle.Width = 120;
            // 
            // leVehicle
            // 
            this.leVehicle.AutoHeight = false;
            this.leVehicle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leVehicle.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leVehicle.DisplayMember = "Info";
            this.leVehicle.DropDownRows = 20;
            this.leVehicle.Name = "leVehicle";
            this.leVehicle.NullText = "";
            this.leVehicle.ShowHeader = false;
            this.leVehicle.ValueMember = "Id";
            // 
            // bbiFilterClear
            // 
            this.bbiFilterClear.Caption = "�������� ������";
            this.bbiFilterClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFilterClear.Glyph")));
            this.bbiFilterClear.Id = 5;
            this.bbiFilterClear.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFilterClear.LargeGlyph")));
            this.bbiFilterClear.Name = "bbiFilterClear";
            this.bbiFilterClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFilterClear_ItemClick);
            // 
            // RepDriverVehicleCategoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "RepDriverVehicleCategoryView";
            this.Size = new System.Drawing.Size(891, 494);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehiclesGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCategory4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gcReport;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleOutCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeOutCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatOutCode;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregat;
        private DevExpress.XtraGrid.Columns.GridColumn colField;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldOutCode;
        private DevExpress.XtraGrid.Columns.GridColumn colEnter;
        private DevExpress.XtraGrid.Columns.GridColumn colExit;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem bleVehiclesGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leVehiclesGroups;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colProcDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colProcCont;
        private DevExpress.XtraGrid.Columns.GridColumn colClarified;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpence;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenceInFields;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedAvg;
        private DevExpress.XtraGrid.Columns.GridColumn colPricing;
        private DevExpress.XtraGrid.Columns.GridColumn colSalary;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryName;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryName2;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryName3;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryName4;
        private DevExpress.XtraGrid.Columns.GridColumn colDriver;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverOutCode;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraBars.BarEditItem bleCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCategory;
        private DevExpress.XtraBars.BarEditItem bleCategory2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCategory2;
        private DevExpress.XtraBars.BarEditItem bleCategory3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCategory3;
        private DevExpress.XtraBars.BarEditItem bleCategory4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leCategory4;
        private DevExpress.XtraBars.BarEditItem bleVehicle;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leVehicle;
        private DevExpress.XtraBars.BarButtonItem bbiFilterClear;
    }
}
