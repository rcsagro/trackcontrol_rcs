using System;
using Agro.Properties;
using TrackControl.General;
using TrackControl.Vehicles;

namespace Agro.Reports.OrderReports
{
    public partial class  RepDriverVehicleCategoryView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public int Vehicle
        {
            get
            {
                if (bleVehicle.EditValue == null)
                    return 0;
                else
                    return (int)bleVehicle.EditValue;
            }
        }

        public int Group
        {
            get
            {
                if (bleVehiclesGroups.EditValue == null)
                    return 0;
                else
                    return (int)bleVehiclesGroups.EditValue;
            }
        }

        public int Category
        {
            get
            {
                if (bleCategory.EditValue == null)
                    return 0;
                else
                    return (int)bleCategory.EditValue;
            }
        }

        public int Category2
        {
            get
            {
                if (bleCategory2.EditValue == null)
                    return 0;
                else
                    return (int)bleCategory2.EditValue;
            }
        }

        public int Category3
        {
            get
            {
                if (bleCategory3.EditValue == null)
                    return 0;
                else
                    return (int)bleCategory3.EditValue;
            }
        }

        public int Category4
        {
            get
            {
                if (bleCategory4.EditValue == null)
                    return 0;
                else
                    return (int)bleCategory4.EditValue;
            }
        }

        public RepDriverVehicleCategoryView()
        {
            InitializeComponent();
            leVehiclesGroups.DataSource = DocItem.VehiclesModel.RootGroupsWithItems();
            leCategory.DataSource = VehicleCategoryProvader.GetList();
            leCategory2.DataSource = VehicleCategoryProvader.GetList2();
            leCategory3.DataSource = VehicleCategoryProvader.GetList3();
            leCategory4.DataSource = VehicleCategoryProvader.GetList4();
            SetVehiclesDataSource();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }
        void Localization()
        {
            //bleVehiclesGroups.Caption = Resources.Driver;
            colCategoryName.Caption = Resources.Category;
            colCategoryName2.Caption = string.Format("{0} 2", Resources.Category);
            colCategoryName3.Caption = string.Format("{0} 3", Resources.Category);
            colCategoryName4.Caption = string.Format("{0} 4", Resources.Category);
            colDriver.Caption = Resources.Driver;
            colDriverOutCode.Caption = Resources.colOutLinkId;
            colVehicle.Caption = Resources.Vehicle;
            colVehicleOutCode.Caption = Resources.colOutLinkId;
            colWorkType.Caption = Resources.WorkType;
            colWorkTypeOutCode.Caption = Resources.colOutLinkId;
            colAgregat.Caption = Resources.Agregat;
            colAgregatOutCode.Caption = Resources.colOutLinkId;
            colField.Caption = Resources.Field;
            colFieldOutCode.Caption = Resources.colOutLinkId;
            colEnter.Caption = Resources.Entry;
            colExit.Caption = Resources.Exit;
            colDistance.Caption = Resources.colDistance;
            colProcDistance.Caption = Resources.SquareRunGa;
            colProcCont.Caption = Resources.SquareContourGa;
            colClarified.Caption = Resources.ClarifiedGa;
            colFuelExpence.Caption = Resources.FuelExpenseTotalL;
            colFuelExpenceInFields.Caption = Resources.FuelExpenseFieldLGa;
            colSpeedAvg.Caption = Resources.SpeedAvg;
            colPricing.Caption = Resources.Price;
            colSalary.Caption = Resources.Salary;

            bleVehiclesGroups.Caption = Resources.VehicleGroupe;
            bleVehicle.Caption = Resources.Vehicle;
            bleCategory.Caption = Resources.Category;
            bleCategory2.Caption = string.Format("{0} 2", Resources.Category);
            bleCategory3.Caption = string.Format("{0} 3", Resources.Category);
            bleCategory4.Caption = string.Format("{0} 4", Resources.Category);
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.DriverVehicleCategory;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }

        private void SetVehiclesDataSource()
        {
            var groupe = (VehiclesGroup)leVehiclesGroups.GetDataSourceRowByKeyValue(bleVehiclesGroups.EditValue);
            if (groupe == null)
            {
                leVehicle.DataSource = DocItem.VehiclesModel.Vehicles;
            }
            else
            {
                var category =
                    (VehicleCategory)leCategory.GetDataSourceRowByKeyValue(bleCategory.EditValue);
                leVehicle.DataSource = category == null ? groupe.OwnRealItems() : groupe.OwnRealItemsWithCategory(category.Id);
            }

        }

        private void bleVehiclesGroups_EditValueChanged(object sender, EventArgs e)
        {
            SetVehiclesDataSource();
        }

        private void bleCategory_EditValueChanged(object sender, EventArgs e)
        {
            SetVehiclesDataSource();
        }

        private void bbiFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleVehiclesGroups.EditValue = null;
            bleCategory.EditValue = null;
            bleCategory2.EditValue = null;
            bleCategory3.EditValue = null;
            bleCategory4.EditValue = null;
            bleVehicle.EditValue = null; 
        }

    }
}
