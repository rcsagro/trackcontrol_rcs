using Agro.Reports.OrderReports.DriversAgregatsWorks;

namespace Agro.Agro.Reports.OrderReports.DriversAgregatsWorks
{
    partial class ReportDrivers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subContent = new ReportDriversContent(); 
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDateStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDateEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFuel = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellDriverNameH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDriverName = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOutCodeH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellOutCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquareH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquare = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.CellPlaceEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquareAvgH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquareAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellDistH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDist = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDistance = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPathH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPath = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellAreaH = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellArea = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSpeedAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPathAvgH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPathAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelTotalH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelTotalFactH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelTotalFact = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDevH = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDev = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.Detail.HeightF = 23F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Id = 0;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subContent;
            //this.xrSubreport1.ReportSource = new Agro.Reports.OrderReports.DriversAgregatsWorks.ReportDriversContent();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(1119F, 23F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrHeaderTable,
            this.xrHeader});
            this.PageHeader.HeightF = 283F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHeaderTable.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(25F, 33F);
            this.xrHeaderTable.Name = "xrHeaderTable";
            this.xrHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow10,
            this.xrTableRow3,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrHeaderTable.SizeF = new System.Drawing.SizeF(1013F, 225F);
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.cellDateStart,
            this.cellDateEnd,
            this.xrTableCell31});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Weight = 2.1638682308600048D;
            // 
            // cellDateStart
            // 
            this.cellDateStart.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.cellDateStart.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDateStart.Name = "cellDateStart";
            this.cellDateStart.StylePriority.UseBorders = false;
            this.cellDateStart.StylePriority.UseFont = false;
            this.cellDateStart.Text = "� 01.01.2016";
            this.cellDateStart.Weight = 2.2374007244957683D;
            // 
            // cellDateEnd
            // 
            this.cellDateEnd.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.cellDateEnd.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDateEnd.Name = "cellDateEnd";
            this.cellDateEnd.StylePriority.UseBorders = false;
            this.cellDateEnd.StylePriority.UseFont = false;
            this.cellDateEnd.StylePriority.UseTextAlignment = false;
            this.cellDateEnd.Text = "�� 01.01.2017";
            this.cellDateEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.cellDateEnd.Weight = 1.7230479821913511D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Weight = 1.6282340828610402D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell16,
            this.xrFuel});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 2.1638682308600048D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 3.6643776096101659D;
            // 
            // xrFuel
            // 
            this.xrFuel.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrFuel.Name = "xrFuel";
            this.xrFuel.StylePriority.UseFont = false;
            this.xrFuel.StylePriority.UseTextAlignment = false;
            this.xrFuel.Text = "�������";
            this.xrFuel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrFuel.Weight = 1.924305179937994D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellDriverNameH,
            this.cellDriverName,
            this.cellOutCodeH,
            this.cellOutCode,
            this.CellFuelSquareH,
            this.CellFuelSquare});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // cellDriverNameH
            // 
            this.cellDriverNameH.BackColor = System.Drawing.Color.White;
            this.cellDriverNameH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDriverNameH.Name = "cellDriverNameH";
            this.cellDriverNameH.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 100F);
            this.cellDriverNameH.StylePriority.UseBackColor = false;
            this.cellDriverNameH.StylePriority.UseFont = false;
            this.cellDriverNameH.StylePriority.UsePadding = false;
            this.cellDriverNameH.StylePriority.UseTextAlignment = false;
            this.cellDriverNameH.Text = "��������";
            this.cellDriverNameH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellDriverNameH.Weight = 0.92939701565557742D;
            // 
            // cellDriverName
            // 
            this.cellDriverName.BackColor = System.Drawing.Color.White;
            this.cellDriverName.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.cellDriverName.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDriverName.Name = "cellDriverName";
            this.cellDriverName.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.cellDriverName.StylePriority.UseBackColor = false;
            this.cellDriverName.StylePriority.UseBorders = false;
            this.cellDriverName.StylePriority.UseFont = false;
            this.cellDriverName.StylePriority.UsePadding = false;
            this.cellDriverName.StylePriority.UseTextAlignment = false;
            this.cellDriverName.Text = "������ �.�.";
            this.cellDriverName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.cellDriverName.Weight = 2.3859747975944146D;
            // 
            // cellOutCodeH
            // 
            this.cellOutCodeH.BackColor = System.Drawing.Color.White;
            this.cellOutCodeH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellOutCodeH.Name = "cellOutCodeH";
            this.cellOutCodeH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.cellOutCodeH.StylePriority.UseBackColor = false;
            this.cellOutCodeH.StylePriority.UseFont = false;
            this.cellOutCodeH.StylePriority.UsePadding = false;
            this.cellOutCodeH.StylePriority.UseTextAlignment = false;
            this.cellOutCodeH.Text = "���";
            this.cellOutCodeH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOutCodeH.Weight = 0.27104559226894964D;
            // 
            // cellOutCode
            // 
            this.cellOutCode.BackColor = System.Drawing.Color.White;
            this.cellOutCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.cellOutCode.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellOutCode.Name = "cellOutCode";
            this.cellOutCode.StylePriority.UseBackColor = false;
            this.cellOutCode.StylePriority.UseBorders = false;
            this.cellOutCode.StylePriority.UseFont = false;
            this.cellOutCode.StylePriority.UseTextAlignment = false;
            this.cellOutCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellOutCode.Weight = 0.81485154983683139D;
            // 
            // CellFuelSquareH
            // 
            this.CellFuelSquareH.BackColor = System.Drawing.Color.White;
            this.CellFuelSquareH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelSquareH.Name = "CellFuelSquareH";
            this.CellFuelSquareH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.CellFuelSquareH.StylePriority.UseBackColor = false;
            this.CellFuelSquareH.StylePriority.UseFont = false;
            this.CellFuelSquareH.StylePriority.UsePadding = false;
            this.CellFuelSquareH.StylePriority.UseTextAlignment = false;
            this.CellFuelSquareH.Text = "������ � �����,  �����";
            this.CellFuelSquareH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelSquareH.Weight = 2.492203288927993D;
            // 
            // CellFuelSquare
            // 
            this.CellFuelSquare.BackColor = System.Drawing.Color.White;
            this.CellFuelSquare.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelSquare.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelSquare.Name = "CellFuelSquare";
            this.CellFuelSquare.StylePriority.UseBackColor = false;
            this.CellFuelSquare.StylePriority.UseBorders = false;
            this.CellFuelSquare.StylePriority.UseFont = false;
            this.CellFuelSquare.StylePriority.UseTextAlignment = false;
            this.CellFuelSquare.Text = "0";
            this.CellFuelSquare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelSquare.Weight = 0.85907877612439743D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.CellPlaceEnd,
            this.CellFuelSquareAvgH,
            this.CellFuelSquareAvg});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // CellPlaceEnd
            // 
            this.CellPlaceEnd.BackColor = System.Drawing.Color.White;
            this.CellPlaceEnd.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CellPlaceEnd.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellPlaceEnd.Name = "CellPlaceEnd";
            this.CellPlaceEnd.StylePriority.UseBackColor = false;
            this.CellPlaceEnd.StylePriority.UseBorders = false;
            this.CellPlaceEnd.StylePriority.UseFont = false;
            this.CellPlaceEnd.StylePriority.UseTextAlignment = false;
            this.CellPlaceEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellPlaceEnd.Weight = 4.4012689553557731D;
            // 
            // CellFuelSquareAvgH
            // 
            this.CellFuelSquareAvgH.BackColor = System.Drawing.Color.White;
            this.CellFuelSquareAvgH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelSquareAvgH.Name = "CellFuelSquareAvgH";
            this.CellFuelSquareAvgH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.CellFuelSquareAvgH.StylePriority.UseBackColor = false;
            this.CellFuelSquareAvgH.StylePriority.UseFont = false;
            this.CellFuelSquareAvgH.StylePriority.UsePadding = false;
            this.CellFuelSquareAvgH.StylePriority.UseTextAlignment = false;
            this.CellFuelSquareAvgH.Text = "������ � �����, �/��";
            this.CellFuelSquareAvgH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelSquareAvgH.Weight = 2.4922037560337809D;
            // 
            // CellFuelSquareAvg
            // 
            this.CellFuelSquareAvg.BackColor = System.Drawing.Color.White;
            this.CellFuelSquareAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelSquareAvg.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelSquareAvg.Name = "CellFuelSquareAvg";
            this.CellFuelSquareAvg.StylePriority.UseBackColor = false;
            this.CellFuelSquareAvg.StylePriority.UseBorders = false;
            this.CellFuelSquareAvg.StylePriority.UseFont = false;
            this.CellFuelSquareAvg.StylePriority.UseTextAlignment = false;
            this.CellFuelSquareAvg.Text = "0";
            this.CellFuelSquareAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelSquareAvg.Weight = 0.8590783090186096D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellDistH,
            this.cellDist,
            this.CellDistance,
            this.CellFuelPathH,
            this.CellFuelPath});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // cellDistH
            // 
            this.cellDistH.BackColor = System.Drawing.Color.White;
            this.cellDistH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDistH.Name = "cellDistH";
            this.cellDistH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.cellDistH.StylePriority.UseBackColor = false;
            this.cellDistH.StylePriority.UseFont = false;
            this.cellDistH.StylePriority.UsePadding = false;
            this.cellDistH.StylePriority.UseTextAlignment = false;
            this.cellDistH.Text = "��������, ��";
            this.cellDistH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellDistH.Weight = 0.92939701183595569D;
            // 
            // cellDist
            // 
            this.cellDist.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.cellDist.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellDist.Name = "cellDist";
            this.cellDist.StylePriority.UseBorders = false;
            this.cellDist.StylePriority.UseFont = false;
            this.cellDist.StylePriority.UseTextAlignment = false;
            this.cellDist.Text = "0";
            this.cellDist.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellDist.Weight = 0.75937363719320827D;
            // 
            // CellDistance
            // 
            this.CellDistance.BackColor = System.Drawing.Color.White;
            this.CellDistance.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CellDistance.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDistance.Name = "CellDistance";
            this.CellDistance.StylePriority.UseBackColor = false;
            this.CellDistance.StylePriority.UseBorders = false;
            this.CellDistance.StylePriority.UseFont = false;
            this.CellDistance.StylePriority.UseTextAlignment = false;
            this.CellDistance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDistance.Weight = 2.7124983063266086D;
            // 
            // CellFuelPathH
            // 
            this.CellFuelPathH.BackColor = System.Drawing.Color.White;
            this.CellFuelPathH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelPathH.Name = "CellFuelPathH";
            this.CellFuelPathH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.CellFuelPathH.StylePriority.UseBackColor = false;
            this.CellFuelPathH.StylePriority.UseFont = false;
            this.CellFuelPathH.StylePriority.UsePadding = false;
            this.CellFuelPathH.StylePriority.UseTextAlignment = false;
            this.CellFuelPathH.Text = "������ �� ���������, �����";
            this.CellFuelPathH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelPathH.Weight = 2.4922037560337804D;
            // 
            // CellFuelPath
            // 
            this.CellFuelPath.BackColor = System.Drawing.Color.White;
            this.CellFuelPath.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelPath.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelPath.Name = "CellFuelPath";
            this.CellFuelPath.StylePriority.UseBackColor = false;
            this.CellFuelPath.StylePriority.UseBorders = false;
            this.CellFuelPath.StylePriority.UseFont = false;
            this.CellFuelPath.StylePriority.UseTextAlignment = false;
            this.CellFuelPath.Text = "0";
            this.CellFuelPath.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelPath.Weight = 0.8590783090186096D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellAreaH,
            this.cellArea,
            this.CellSpeedAvg,
            this.CellFuelPathAvgH,
            this.CellFuelPathAvg});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // cellAreaH
            // 
            this.cellAreaH.BackColor = System.Drawing.Color.White;
            this.cellAreaH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellAreaH.Name = "cellAreaH";
            this.cellAreaH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.cellAreaH.StylePriority.UseBackColor = false;
            this.cellAreaH.StylePriority.UseFont = false;
            this.cellAreaH.StylePriority.UsePadding = false;
            this.cellAreaH.StylePriority.UseTextAlignment = false;
            this.cellAreaH.Text = "������� �����, ��";
            this.cellAreaH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.cellAreaH.Weight = 0.92939701183595624D;
            // 
            // cellArea
            // 
            this.cellArea.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.cellArea.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cellArea.Name = "cellArea";
            this.cellArea.StylePriority.UseBorders = false;
            this.cellArea.StylePriority.UseFont = false;
            this.cellArea.StylePriority.UseTextAlignment = false;
            this.cellArea.Text = "0";
            this.cellArea.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellArea.Weight = 0.75937366638732018D;
            // 
            // CellSpeedAvg
            // 
            this.CellSpeedAvg.BackColor = System.Drawing.Color.White;
            this.CellSpeedAvg.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CellSpeedAvg.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSpeedAvg.Name = "CellSpeedAvg";
            this.CellSpeedAvg.StylePriority.UseBackColor = false;
            this.CellSpeedAvg.StylePriority.UseBorders = false;
            this.CellSpeedAvg.StylePriority.UseFont = false;
            this.CellSpeedAvg.StylePriority.UseTextAlignment = false;
            this.CellSpeedAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellSpeedAvg.Weight = 2.712498277132497D;
            // 
            // CellFuelPathAvgH
            // 
            this.CellFuelPathAvgH.BackColor = System.Drawing.Color.White;
            this.CellFuelPathAvgH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelPathAvgH.Name = "CellFuelPathAvgH";
            this.CellFuelPathAvgH.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.CellFuelPathAvgH.StylePriority.UseBackColor = false;
            this.CellFuelPathAvgH.StylePriority.UseFont = false;
            this.CellFuelPathAvgH.StylePriority.UsePadding = false;
            this.CellFuelPathAvgH.StylePriority.UseTextAlignment = false;
            this.CellFuelPathAvgH.Text = "������ �� ���������, �/100��";
            this.CellFuelPathAvgH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelPathAvgH.Weight = 2.4922037560337809D;
            // 
            // CellFuelPathAvg
            // 
            this.CellFuelPathAvg.BackColor = System.Drawing.Color.White;
            this.CellFuelPathAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelPathAvg.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelPathAvg.Name = "CellFuelPathAvg";
            this.CellFuelPathAvg.StylePriority.UseBackColor = false;
            this.CellFuelPathAvg.StylePriority.UseBorders = false;
            this.CellFuelPathAvg.StylePriority.UseFont = false;
            this.CellFuelPathAvg.StylePriority.UseTextAlignment = false;
            this.CellFuelPathAvg.Text = "0";
            this.CellFuelPathAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelPathAvg.Weight = 0.8590783090186096D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.CellFuelTotalH,
            this.CellFuelTotal});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.White;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.Weight = 3.3153714482261125D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.Color.White;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.Weight = 1.0858975071296606D;
            // 
            // CellFuelTotalH
            // 
            this.CellFuelTotalH.BackColor = System.Drawing.Color.White;
            this.CellFuelTotalH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelTotalH.Name = "CellFuelTotalH";
            this.CellFuelTotalH.StylePriority.UseBackColor = false;
            this.CellFuelTotalH.StylePriority.UseFont = false;
            this.CellFuelTotalH.StylePriority.UseTextAlignment = false;
            this.CellFuelTotalH.Text = "����� ������ , �";
            this.CellFuelTotalH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelTotalH.Weight = 2.4922042231395682D;
            // 
            // CellFuelTotal
            // 
            this.CellFuelTotal.BackColor = System.Drawing.Color.White;
            this.CellFuelTotal.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelTotal.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelTotal.Name = "CellFuelTotal";
            this.CellFuelTotal.StylePriority.UseBackColor = false;
            this.CellFuelTotal.StylePriority.UseBorders = false;
            this.CellFuelTotal.StylePriority.UseFont = false;
            this.CellFuelTotal.StylePriority.UseTextAlignment = false;
            this.CellFuelTotal.Text = "0";
            this.CellFuelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelTotal.Weight = 0.859077841912822D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.CellFuelTotalFactH,
            this.CellFuelTotalFact});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.White;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBackColor = false;
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 1.1446393625943525D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.BackColor = System.Drawing.Color.White;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBackColor = false;
            this.xrTableCell17.Weight = 1.0192288682656523D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.Color.White;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.Weight = 1.1515032173661077D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.White;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.Weight = 1.0858975071296606D;
            // 
            // CellFuelTotalFactH
            // 
            this.CellFuelTotalFactH.BackColor = System.Drawing.Color.White;
            this.CellFuelTotalFactH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelTotalFactH.Name = "CellFuelTotalFactH";
            this.CellFuelTotalFactH.StylePriority.UseBackColor = false;
            this.CellFuelTotalFactH.StylePriority.UseBorders = false;
            this.CellFuelTotalFactH.StylePriority.UseFont = false;
            this.CellFuelTotalFactH.StylePriority.UseTextAlignment = false;
            this.CellFuelTotalFactH.Text = "����� ������, � ����";
            this.CellFuelTotalFactH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellFuelTotalFactH.Weight = 2.4922042231395682D;
            // 
            // CellFuelTotalFact
            // 
            this.CellFuelTotalFact.BackColor = System.Drawing.Color.White;
            this.CellFuelTotalFact.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelTotalFact.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellFuelTotalFact.Name = "CellFuelTotalFact";
            this.CellFuelTotalFact.StylePriority.UseBackColor = false;
            this.CellFuelTotalFact.StylePriority.UseBorders = false;
            this.CellFuelTotalFact.StylePriority.UseFont = false;
            this.CellFuelTotalFact.StylePriority.UseTextAlignment = false;
            this.CellFuelTotalFact.Text = "0";
            this.CellFuelTotalFact.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelTotalFact.Weight = 0.859077841912822D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.CellDevH,
            this.CellDev});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 4.40126878388074D;
            // 
            // CellDevH
            // 
            this.CellDevH.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDevH.Name = "CellDevH";
            this.CellDevH.StylePriority.UseFont = false;
            this.CellDevH.StylePriority.UseTextAlignment = false;
            this.CellDevH.Text = "% ����������";
            this.CellDevH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.CellDevH.Weight = 2.4922032258948503D;
            // 
            // CellDev
            // 
            this.CellDev.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDev.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDev.Name = "CellDev";
            this.CellDev.StylePriority.UseBorders = false;
            this.CellDev.StylePriority.UseFont = false;
            this.CellDev.StylePriority.UseTextAlignment = false;
            this.CellDev.Text = "0";
            this.CellDev.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDev.Weight = 0.85907901063257341D;
            // 
            // xrHeader
            // 
            this.xrHeader.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeader.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrHeader.Name = "xrHeader";
            this.xrHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeader.SizeF = new System.Drawing.SizeF(1013F, 33F);
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            this.xrHeader.Text = "����� �� ������ �������� �� ������";
            this.xrHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(942F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 50F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 50F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportDrivers
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 0, 50, 50);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

       
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        protected DevExpress.XtraReports.UI.XRTable xrHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellDateStart;
        private DevExpress.XtraReports.UI.XRTableCell cellDateEnd;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell cellDriverNameH;
        public DevExpress.XtraReports.UI.XRTableCell cellDriverName;
        private DevExpress.XtraReports.UI.XRTableCell cellOutCode;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquare;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquareAvgH;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquareAvg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell cellDistH;
        private DevExpress.XtraReports.UI.XRTableCell CellDistance;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPathH;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPath;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell cellAreaH;
        private DevExpress.XtraReports.UI.XRTableCell CellSpeedAvg;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPathAvgH;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPathAvg;
        protected DevExpress.XtraReports.UI.XRLabel xrHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        public DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelTotalH;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelTotal;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelTotalFactH;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelTotalFact;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell CellDev;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell cellOutCodeH;
        private DevExpress.XtraReports.UI.XRTableCell xrFuel;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquareH;
        private DevExpress.XtraReports.UI.XRTableCell CellPlaceEnd;
        private DevExpress.XtraReports.UI.XRTableCell cellDist;
        private DevExpress.XtraReports.UI.XRTableCell cellArea;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell CellDevH;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private ReportDriversContent subContent;	
        //private ReportDriversContent subContent ;	
       //this.subContent = new ReportDriversContent(); 	
        
        //this.xrSubreport1.ReportSource = this.subContent;	
    }
}
