﻿namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    partial class ReportDriversContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrVehicle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrWorks = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrAgregat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrField = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrEnter = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrExit = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDist = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquare = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquareCalc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquareClarified = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFuelExpens = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFuelExpensAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSpeedAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPricing = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSalary = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrVehicleH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrWorksH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrAgregatH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFieldH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrEnterH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrExitH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDistH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquareH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquareCalcH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFactSquareClarifiedH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFuelExpensH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFuelExpensAvgH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSpeedAvgH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPricingH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrSalaryH = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrVehicleHCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrVehicleCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrWorksHCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrWorksCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrAgregatHCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrAgregatCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFieldHCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrFieldCode = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1059F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrVehicle,
            this.xrVehicleCode,
            this.xrWorks,
            this.xrWorksCode,
            this.xrAgregat,
            this.xrAgregatCode,
            this.xrField,
            this.xrFieldCode,
            this.xrEnter,
            this.xrExit,
            this.xrDist,
            this.xrFactSquare,
            this.xrFactSquareCalc,
            this.xrFactSquareClarified,
            this.xrFuelExpens,
            this.xrFuelExpensAvg,
            this.xrSpeedAvg,
            this.xrPricing,
            this.xrSalary});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow2.Weight = 0.7142857142857143D;
            // 
            // xrVehicle
            // 
            this.xrVehicle.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrVehicle.Name = "xrVehicle";
            this.xrVehicle.StylePriority.UseFont = false;
            this.xrVehicle.StylePriority.UseTextAlignment = false;
            this.xrVehicle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrVehicle.Weight = 0.86886720399530826D;
            // 
            // xrWorks
            // 
            this.xrWorks.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrWorks.Name = "xrWorks";
            this.xrWorks.StylePriority.UseFont = false;
            this.xrWorks.StylePriority.UseTextAlignment = false;
            this.xrWorks.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrWorks.Weight = 0.63225973590903339D;
            // 
            // xrAgregat
            // 
            this.xrAgregat.Multiline = true;
            this.xrAgregat.Name = "xrAgregat";
            this.xrAgregat.StylePriority.UseTextAlignment = false;
            this.xrAgregat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrAgregat.Weight = 0.63228735573733863D;
            // 
            // xrField
            // 
            this.xrField.Name = "xrField";
            this.xrField.StylePriority.UseTextAlignment = false;
            this.xrField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrField.Weight = 0.56887818376855492D;
            // 
            // xrEnter
            // 
            this.xrEnter.Name = "xrEnter";
            this.xrEnter.Weight = 0.33491570591718134D;
            // 
            // xrExit
            // 
            this.xrExit.Name = "xrExit";
            this.xrExit.Weight = 0.35172300248487726D;
            // 
            // xrDist
            // 
            this.xrDist.Name = "xrDist";
            this.xrDist.Weight = 0.57922233971847059D;
            // 
            // xrFactSquare
            // 
            this.xrFactSquare.Name = "xrFactSquare";
            this.xrFactSquare.Weight = 0.52039914055761038D;
            // 
            // xrFactSquareCalc
            // 
            this.xrFactSquareCalc.Name = "xrFactSquareCalc";
            this.xrFactSquareCalc.Weight = 0.662647033433531D;
            // 
            // xrFactSquareClarified
            // 
            this.xrFactSquareClarified.Name = "xrFactSquareClarified";
            this.xrFactSquareClarified.Weight = 0.43003467613163493D;
            // 
            // xrFuelExpens
            // 
            this.xrFuelExpens.Name = "xrFuelExpens";
            this.xrFuelExpens.Weight = 0.65217116514438955D;
            // 
            // xrFuelExpensAvg
            // 
            this.xrFuelExpensAvg.Name = "xrFuelExpensAvg";
            this.xrFuelExpensAvg.Weight = 0.48678602457552933D;
            // 
            // xrSpeedAvg
            // 
            this.xrSpeedAvg.Name = "xrSpeedAvg";
            this.xrSpeedAvg.Weight = 0.36279831641048277D;
            // 
            // xrPricing
            // 
            this.xrPricing.Name = "xrPricing";
            this.xrPricing.Weight = 0.4220017261834198D;
            // 
            // xrSalary
            // 
            this.xrSalary.Name = "xrSalary";
            this.xrSalary.Weight = 0.34003164395706725D;
            // 
            // TopMargin
            // 
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportHeader.HeightF = 35F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseTextAlignment = false;
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1059F, 35F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrVehicleH,
            this.xrVehicleHCode,
            this.xrWorksH,
            this.xrWorksHCode,
            this.xrAgregatH,
            this.xrAgregatHCode,
            this.xrFieldH,
            this.xrFieldHCode,
            this.xrEnterH,
            this.xrExitH,
            this.xrDistH,
            this.xrFactSquareH,
            this.xrFactSquareCalcH,
            this.xrFactSquareClarifiedH,
            this.xrFuelExpensH,
            this.xrFuelExpensAvgH,
            this.xrSpeedAvgH,
            this.xrPricingH,
            this.xrSalaryH});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrVehicleH
            // 
            this.xrVehicleH.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrVehicleH.Name = "xrVehicleH";
            this.xrVehicleH.StylePriority.UseFont = false;
            this.xrVehicleH.Text = "Транспортное средство";
            this.xrVehicleH.Weight = 0.86886709893062852D;
            // 
            // xrWorksH
            // 
            this.xrWorksH.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrWorksH.Name = "xrWorksH";
            this.xrWorksH.StylePriority.UseFont = false;
            this.xrWorksH.Text = "Вид работ";
            this.xrWorksH.Weight = 0.63225979594599324D;
            // 
            // xrAgregatH
            // 
            this.xrAgregatH.Multiline = true;
            this.xrAgregatH.Name = "xrAgregatH";
            this.xrAgregatH.Text = "Навесное оборудование\r\n";
            this.xrAgregatH.Weight = 0.63228735573733863D;
            // 
            // xrFieldH
            // 
            this.xrFieldH.Name = "xrFieldH";
            this.xrFieldH.Text = "Поле";
            this.xrFieldH.Weight = 0.56886640151519219D;
            // 
            // xrEnterH
            // 
            this.xrEnterH.Name = "xrEnterH";
            this.xrEnterH.Text = "Въезд";
            this.xrEnterH.Weight = 0.33491570591718134D;
            // 
            // xrExitH
            // 
            this.xrExitH.Name = "xrExitH";
            this.xrExitH.Text = "Выезд";
            this.xrExitH.Weight = 0.35172300248487726D;
            // 
            // xrDistH
            // 
            this.xrDistH.Name = "xrDistH";
            this.xrDistH.Text = "Пройденный путь, км";
            this.xrDistH.Weight = 0.57922233971847059D;
            // 
            // xrFactSquareH
            // 
            this.xrFactSquareH.Name = "xrFactSquareH";
            this.xrFactSquareH.Text = "Обработано (по пробегу), га";
            this.xrFactSquareH.Weight = 0.52039914055761038D;
            // 
            // xrFactSquareCalcH
            // 
            this.xrFactSquareCalcH.Name = "xrFactSquareCalcH";
            this.xrFactSquareCalcH.Text = "Обработано (по контуру), га";
            this.xrFactSquareCalcH.Weight = 0.662647033433531D;
            // 
            // xrFactSquareClarifiedH
            // 
            this.xrFactSquareClarifiedH.Name = "xrFactSquareClarifiedH";
            this.xrFactSquareClarifiedH.Text = "Уточненно, га";
            this.xrFactSquareClarifiedH.Weight = 0.43003467613163493D;
            // 
            // xrFuelExpensH
            // 
            this.xrFuelExpensH.Name = "xrFuelExpensH";
            this.xrFuelExpensH.Text = "Общий расход, л";
            this.xrFuelExpensH.Weight = 0.65217116514438955D;
            // 
            // xrFuelExpensAvgH
            // 
            this.xrFuelExpensAvgH.Name = "xrFuelExpensAvgH";
            this.xrFuelExpensAvgH.Text = "Расход в полях, л/га";
            this.xrFuelExpensAvgH.Weight = 0.48678602457552933D;
            // 
            // xrSpeedAvgH
            // 
            this.xrSpeedAvgH.Name = "xrSpeedAvgH";
            this.xrSpeedAvgH.Text = "Средняя скорость";
            this.xrSpeedAvgH.Weight = 0.36279831641048277D;
            // 
            // xrPricingH
            // 
            this.xrPricingH.Name = "xrPricingH";
            this.xrPricingH.Text = "Расценка";
            this.xrPricingH.Weight = 0.4220017261834198D;
            // 
            // xrSalaryH
            // 
            this.xrSalaryH.Name = "xrSalaryH";
            this.xrSalaryH.Text = "Оплата труда";
            this.xrSalaryH.Weight = 0.34003164395706725D;
            // 
            // xrVehicleHCode
            // 
            this.xrVehicleHCode.Name = "xrVehicleHCode";
            this.xrVehicleHCode.Weight = 0.13113284673130651D;
            // 
            // xrVehicleCode
            // 
            this.xrVehicleCode.Name = "xrVehicleCode";
            this.xrVehicleCode.Weight = 0.13113280170358671D;
            // 
            // xrWorksHCode
            // 
            this.xrWorksHCode.Name = "xrWorksHCode";
            this.xrWorksHCode.Weight = 0.12404284864203902D;
            // 
            // xrWorksCode
            // 
            this.xrWorksCode.Name = "xrWorksCode";
            this.xrWorksCode.Weight = 0.12404284864203902D;
            // 
            // xrAgregatHCode
            // 
            this.xrAgregatHCode.Name = "xrAgregatHCode";
            this.xrAgregatHCode.Weight = 0.10703218940565783D;
            // 
            // xrAgregatCode
            // 
            this.xrAgregatCode.Name = "xrAgregatCode";
            this.xrAgregatCode.Weight = 0.10702063229089442D;
            // 
            // xrFieldHCode
            // 
            this.xrFieldHCode.Name = "xrFieldHCode";
            this.xrFieldHCode.Weight = 0.12622578415494326D;
            // 
            // xrFieldCode
            // 
            this.xrFieldCode.Name = "xrFieldCode";
            this.xrFieldCode.Weight = 0.12622555901634391D;
            // 
            // ReportDriversContent
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(24, 17, 100, 100);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "13.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrVehicleH;
        private DevExpress.XtraReports.UI.XRTableCell xrWorksH;
        private DevExpress.XtraReports.UI.XRTableCell xrAgregatH;
        private DevExpress.XtraReports.UI.XRTableCell xrFieldH;
        private DevExpress.XtraReports.UI.XRTableCell xrEnterH;
        private DevExpress.XtraReports.UI.XRTableCell xrExitH;
        private DevExpress.XtraReports.UI.XRTableCell xrDistH;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquareH;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquareCalcH;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquareClarifiedH;
        private DevExpress.XtraReports.UI.XRTableCell xrFuelExpensH;
        private DevExpress.XtraReports.UI.XRTableCell xrFuelExpensAvgH;
        private DevExpress.XtraReports.UI.XRTableCell xrSpeedAvgH;
        private DevExpress.XtraReports.UI.XRTableCell xrPricingH;
        private DevExpress.XtraReports.UI.XRTableCell xrSalaryH;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrVehicle;
        private DevExpress.XtraReports.UI.XRTableCell xrWorks;
        private DevExpress.XtraReports.UI.XRTableCell xrAgregat;
        private DevExpress.XtraReports.UI.XRTableCell xrField;
        private DevExpress.XtraReports.UI.XRTableCell xrEnter;
        private DevExpress.XtraReports.UI.XRTableCell xrExit;
        private DevExpress.XtraReports.UI.XRTableCell xrDist;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquare;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquareCalc;
        private DevExpress.XtraReports.UI.XRTableCell xrFactSquareClarified;
        private DevExpress.XtraReports.UI.XRTableCell xrFuelExpens;
        private DevExpress.XtraReports.UI.XRTableCell xrFuelExpensAvg;
        private DevExpress.XtraReports.UI.XRTableCell xrPricing;
        private DevExpress.XtraReports.UI.XRTableCell xrSalary;
        private DevExpress.XtraReports.UI.XRTableCell xrSpeedAvg;
        private DevExpress.XtraReports.UI.XRTableCell xrVehicleCode;
        private DevExpress.XtraReports.UI.XRTableCell xrVehicleHCode;
        private DevExpress.XtraReports.UI.XRTableCell xrWorksCode;
        private DevExpress.XtraReports.UI.XRTableCell xrWorksHCode;
        private DevExpress.XtraReports.UI.XRTableCell xrAgregatCode;
        private DevExpress.XtraReports.UI.XRTableCell xrFieldCode;
        private DevExpress.XtraReports.UI.XRTableCell xrAgregatHCode;
        private DevExpress.XtraReports.UI.XRTableCell xrFieldHCode;
    }
}
