﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Agro.Utilites;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public class RepDriversAgregatsWorksData
    {
        public RepDriversAgregatsWorksData()
        {

        }

        public DataTable GetData(DateTime begin, DateTime end,int driverId)
        {
            DataTable dataSource;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.RepDriversAgregatsWorksData.SelectDriverRecords,
                                           AgroQuery.SqlVehicleIdent, driverDb.ParamPrefics,
                                           AgroQuery.SqlFieldIdent, driverId);
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", end);
                dataSource = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
                foreach (DataRow dr in dataSource.Rows)
                {
                    if ((double)dr["FuelExpens"] == 0 && (double)dr["Fuel_ExpensTotal"] > 0)
                    {
                        dr["FuelExpens"] = dr["Fuel_ExpensTotal"];
                    }
                    if ((double)dr["FuelExpensAvg"] == 0 && (double)dr["Fuel_ExpensAvg"] > 0)
                    {
                        dr["FuelExpensAvg"] = dr["Fuel_ExpensAvg"];
                    }
                }
            }
            return dataSource; 
        }
    }
}
