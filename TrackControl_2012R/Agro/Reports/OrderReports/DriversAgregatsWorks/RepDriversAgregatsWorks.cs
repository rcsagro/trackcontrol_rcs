﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro.Agro.Reports.OrderReports.DriversAgregatsWorks;
using Agro.Properties;
using Agro.XtraReports;
using DevExpress.XtraReports.UI;
using TrackControl.General;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public class RepDriversAgregatsWorks : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = Resources.DriverVehicleAgregat;
        readonly Image _largeImage = Shared.NoUser;
        readonly Image _smallImage = Shared.NoUser;

        private DataTable _dtContent;
        private DateTime _dateStart;
        private DateTime _dateEnd;
        private int _driverId;

        public RepDriversAgregatsWorks()
        {
            _dataViewer = new RepDriversAgregatsWorksView();
            _dataViewer.Dock = DockStyle.Fill;
        }
        
        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            _dateStart = begin;
            _dateEnd = end;
            _driverId = ((RepDriversAgregatsWorksView) _dataViewer).Driver;
            var repotrsData = new RepDriversAgregatsWorksData();
            _dtContent = repotrsData.GetData(begin, end, ((RepDriversAgregatsWorksView)_dataViewer).Driver);
            ((RepDriversAgregatsWorksView)_dataViewer).gcReport.DataSource = _dtContent;
            ((RepDriversAgregatsWorksView)_dataViewer).Begin = begin;
            ((RepDriversAgregatsWorksView)_dataViewer).End = end;
        }


        public bool IsHasData
        {
            get { return ((RepDriversAgregatsWorksView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            var report = new ReportDrivers(_driverId,_dateStart, _dateEnd, _dtContent);
            report.ShowPreview(); 
        }

        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }

    }
}
