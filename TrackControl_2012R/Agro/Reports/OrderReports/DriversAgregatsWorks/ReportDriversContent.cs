﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using Agro.Properties;
using DevExpress.XtraReports.UI;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public partial class ReportDriversContent : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportDriversContent()
        {
            InitializeComponent();
            Localization();
        }

        public void Init(DataTable dtContent)
        {

            DataSource = dtContent;
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrVehicle.DataBindings.Add("Text", DataSource, "VehicleName");
            xrVehicleCode.DataBindings.Add("Text", DataSource, "VehicleOutLinkId");
            xrWorks.DataBindings.Add("Text", DataSource, "WorkName");
            xrWorksCode.DataBindings.Add("Text", DataSource, "WorkOutLinkId");
            xrAgregat.DataBindings.Add("Text", DataSource, "AgregatName");
            xrAgregatCode.DataBindings.Add("Text", DataSource, "AgregatOutLinkId");
            xrField.DataBindings.Add("Text", DataSource, "FieldName");
            xrFieldCode.DataBindings.Add("Text", DataSource, "ZoneOutLinkId");
            xrEnter.DataBindings.Add("Text", DataSource, "TimeStart", "{0:t}");
            xrExit.DataBindings.Add("Text", DataSource, "TimeEnd", "{0:t}");
            xrDist.DataBindings.Add("Text", DataSource, "Distance");
            xrFactSquare.DataBindings.Add("Text", DataSource, "FactSquare");
            xrFactSquareCalc.DataBindings.Add("Text", DataSource, "FactSquareCalc");
            xrFactSquareClarified.DataBindings.Add("Text", DataSource, "FactSquareClarified");
            xrFuelExpens.DataBindings.Add("Text", DataSource, "FuelExpens");
            xrFuelExpensAvg.DataBindings.Add("Text", DataSource, "FuelExpensAvg");
            xrSpeedAvg.DataBindings.Add("Text", DataSource, "SpeedAvg");
            xrPricing.DataBindings.Add("Text", DataSource, "Pricing");
            xrSalary.DataBindings.Add("Text", DataSource, "Salary");
        }
        private void Localization()
        {
            xrVehicleH.Text = Resources.Vehicle;
            xrWorksH.Text = Resources.WorkType;
            xrAgregatH.Text = Resources.Agregat;
            xrFieldH.Text = Resources.Field;
            xrEnterH.Text = Resources.Entry;
            xrExitH.Text = Resources.Exit;
            xrDistH.Text = Resources.colDistance;
            xrFactSquareH.Text = Resources.SquareRunGa;
            xrFactSquareCalcH.Text = Resources.SquareContourGa;
            xrFactSquareClarifiedH.Text = Resources.ClarifiedGa;
            xrFuelExpensH.Text = Resources.FuelExpenseTotalL;
            xrFuelExpensAvgH.Text = Resources.FuelExpenseFieldLGa;
            xrSpeedAvgH.Text = Resources.SpeedAvg;
            xrPricingH.Text = Resources.Price;
            xrSalaryH.Text = Resources.Salary;
        }

    }
}
