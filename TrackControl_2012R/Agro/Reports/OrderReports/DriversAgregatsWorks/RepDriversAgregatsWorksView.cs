using System;
using Agro.Properties;
using TrackControl.General;
using TrackControl.Vehicles;

namespace Agro.Reports.OrderReports
{
    public partial class RepDriversAgregatsWorksView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public int Driver
        {
            get
            {
                if (bleDrivers.EditValue == null)
                    return 0;
                else
                    return (int)bleDrivers.EditValue;
            }
        }

        public RepDriversAgregatsWorksView()
        {
            InitializeComponent();
            leDrivers.DataSource = DocItem.VehiclesModel.Drivers;
            bleDrivers.EditValue = leDrivers.GetDataSourceValue(leDrivers.ValueMember, 0);
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }
        void Localization()
        {
            bleDrivers.Caption = Resources.Driver;
            
            colVehicle.Caption = Resources.Vehicle;
            colVehicleOutCode.Caption = Resources.colOutLinkId;
            colWorkType.Caption = Resources.WorkType;
            colWorkTypeOutCode.Caption = Resources.colOutLinkId;
            colAgregat.Caption = Resources.Agregat;
            colAgregatOutCode.Caption = Resources.colOutLinkId;
            colField.Caption = Resources.Field;
            colFieldOutCode.Caption = Resources.colOutLinkId;
            colEnter.Caption = Resources.Entry;
            colExit.Caption = Resources.Exit;
            colDistance.Caption = Resources.colDistance;
            colProcDistance.Caption = Resources.SquareRunGa;
            colProcCont.Caption = Resources.SquareContourGa;
            colClarified.Caption = Resources.ClarifiedGa;
            colFuelExpence.Caption = Resources.FuelExpenseTotalL;
            colFuelExpenceInFields.Caption = Resources.FuelExpenseFieldLGa;
            colSpeedAvg.Caption = Resources.SpeedAvg;
            colPricing.Caption = Resources.Price;
            colSalary.Caption = Resources.Salary;

        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.VehicleAggregateRow1;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }


    }
}
