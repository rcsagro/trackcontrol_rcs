using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using Agro.Dictionaries;
using Agro.Reports.OrderReports.DriversAgregatsWorks;
using DevExpress.XtraReports.UI;
using Agro.Properties;
using TrackControl.Vehicles;

namespace Agro.Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public partial class ReportDrivers : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportDrivers()
        {
            InitializeComponent();
            Localization();
        }
        public ReportDrivers(int DriverId,DateTime dtStart,DateTime dtEnd, DataTable dtContent)
            : this()
        {
            subContent.Init(dtContent);
            var driver = new Driver(DriverId,0);
            var totals = new DriversAgregatsWorksTotals();
            foreach (DataRow dr in dtContent.Rows)
            {
                int typeWork;
                Int32.TryParse(dr["TypeWork"].ToString(), out typeWork);
                if ((int)dr["FieldId"] > 0 && typeWork == (int)DictionaryAgroWorkType.WorkTypes.WorkInField)
                {
                    totals.FuelFields += (double)dr["FuelExpens"];
                    totals.Area += (double)dr["FactSquareClarified"];
                }
                else
                {
                    totals.CrossDistance += (double)dr["Distance"];
                    totals.FuelCross += (double)dr["FuelExpens"];
                }
            }
            totals.Area = Math.Round(totals.Area, 2);
            totals.FuelCross = Math.Round(totals.FuelCross, 2);
            totals.CrossDistance = Math.Round(totals.CrossDistance, 2);
            totals.FuelFields = Math.Round(totals.FuelFields, 2);
            if (totals.CrossDistance>0) totals.FuelCrossOn100 = Math.Round(100 * totals.FuelCross / totals.CrossDistance, 2);
            if (totals.Area > 0) totals.FuelFieldsAvg = Math.Round(totals.FuelFields / totals.Area, 2);
            totals.FuelTotal = totals.FuelCross + totals.FuelFields;

            cellDateStart.Text = string.Format("{0}", dtStart);
            cellDateEnd.Text = string.Format("{0}", dtEnd);
            cellDriverName.Text = driver.FullName;
            cellOutCode.Text = driver.IdOutLink;

            cellDist.Text = totals.CrossDistance.ToString();
            cellArea.Text = totals.Area.ToString();

            CellFuelSquare.Text = totals.FuelFields.ToString();
            CellFuelSquareAvg.Text = totals.FuelFieldsAvg.ToString();
            CellFuelPath.Text = totals.FuelCross.ToString();
            CellFuelPathAvg.Text = totals.FuelCrossOn100.ToString();
            CellFuelTotal.Text = totals.FuelTotal.ToString();

        
        }
        #region �����������
        private void Localization()
        {

            xrHeader.Text = Resources.ReportDriverWorkPeriod;
            cellDriverNameH.Text = Resources.Driver;
            cellOutCodeH.Text = Resources.Code;
            cellDistH.Text = Resources.MoveKm;
            cellAreaH.Text = Resources.SquareGATotal;

            xrFuel.Text = Resources.Fuel;
            CellFuelSquareH.Text = Resources.FuelExpenseFieldTotal;
            CellFuelSquareAvgH.Text = Resources.colFuelExpensAvg;
            CellFuelPathH.Text = Resources.FuelExpenseRunTotal;
            CellFuelPathAvgH.Text = Resources.colFuelExpensRunKm;
            CellFuelTotalH.Text = Resources.FuelExpenseTotalL;
            CellFuelTotalFactH.Text = Resources.FuelExpenseTotalLFact;
            CellDevH.Text = Resources.Deflection;
        }
        #endregion
    }
}
