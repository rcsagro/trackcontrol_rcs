﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agro.Reports.OrderReports.DriversAgregatsWorks
{
    public class DriversAgregatsWorksTotals
    {
        //Переезды, км
        public double CrossDistance { get; set; }

        //Площадь всего, га
        public double Area { get; set; }

        //Расход в полях,  всего
        public double FuelFields { get; set; }

        //Расход в полях, л/га
        public double FuelFieldsAvg { get; set; }

        //Расход на переездах, всего
        public double FuelCross { get; set; }

        //Расход на переездах, л/100км
        public double FuelCrossOn100 { get; set; }

        //Общий расход, л
        public double FuelTotal { get; set; }

    }
}
