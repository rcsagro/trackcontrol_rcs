using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using Agro.Properties;

namespace Agro.Reports.OrderReports
{
    public partial class RepFieldsProcessingExtView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }
        
        public RepFieldsProcessingExtView()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            colVehicleName.Caption = Resources.Vehicle;
            colAgregatName.Caption = Resources.Agregat;
            colDriverName.Caption = Resources.Driver;
            colFieldName.Caption =Resources.Field;
            colCultureName.Caption = Resources.Cultura;
            colFieldGroupName.Caption = Resources.FieldsGroups;
            colFieldSquare.Caption = Resources.SquareGA;
            colFuelExpGa.Caption = Resources.FuelExpenseAvgLGa;
            colWorkTime.Caption = Resources.TimeWorksH;
            colRemark.Caption = Resources.Remark;
            colPersentSquare.Caption = Resources.PersentSquare;
            colSpeedAvg.Caption = Resources.SpeedAvgKmH;
            colFactSquareCalc.Caption = Resources.SquareProcessGa;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.FieldsProcessingExt;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }

        private void gvReport_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
    }
}
