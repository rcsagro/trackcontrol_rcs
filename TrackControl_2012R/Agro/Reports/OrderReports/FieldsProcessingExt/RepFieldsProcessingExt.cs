﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessingExt : IReportItem 
    {

        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.FieldsProcessingExt, Environment.NewLine);
        Image _largeImage = Shared.FieldsProcessing;
        Image _smallImage = Shared.FieldsProcessing;

        public RepFieldsProcessingExt()
        {
            _dataViewer = new RepFieldsProcessingExtView();
            _dataViewer.Dock = DockStyle.Fill;
        }


        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            RepFieldsProcessingExtData dataSource = new RepFieldsProcessingExtData();
            ((RepFieldsProcessingExtView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end);
            ((RepFieldsProcessingExtView)_dataViewer).Begin = begin;
            ((RepFieldsProcessingExtView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return ((RepFieldsProcessingExtView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepFieldsProcessingExtView)_dataViewer).ExportToExcel();
        }

        #endregion
    }
}
