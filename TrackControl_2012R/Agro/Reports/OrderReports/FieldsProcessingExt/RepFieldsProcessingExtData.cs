﻿using System;
using System.Data;
using System.Collections.Generic;  
using Agro;
using Agro.Utilites;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessingExtData
    {
        public RepFieldsProcessingExtData()
        {

        }

        public DataTable GetData(DateTime begin, DateTime end)
        {
            DataTable dataTable;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.RepFieldsProcessingExtData.SelectDriverName,
                    AgroQuery.SqlVehicleIdent,
                    driverDb.ParamPrefics, TrackControlQuery.SqlDriverIdent);
                
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", end);
                dataTable = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray);
                //data.Columns.Add(new DataColumn("FactSquareCalcOverlap", System.Type.GetType("System.Double")));
            }
            foreach (DataRow dRow in dataTable.Rows)
            {
                if ((double) dRow["FuelExpGa"] == 0) dRow["FuelExpGa"] = dRow["FuelExpGaDUT"];

                // Долго выполняется 

                //// наложение треков - нужна общая правильная площадь
                ////if ((long)dRow["Records"] > 1 && (long)dRow["ZoneId"] > 0)
                //if ((int)dRow["ZoneId"] > 0)
                //{
                //    List<int> orderWorks = GetOrderWorksList((int)dRow["MobitelId"], (int)dRow["ZoneId"], (int)dRow["DriverId"], (int)dRow["AgrId"], (int)dRow["WorkId"], begin, end);
                //    if (orderWorks.Count > 0)
                //    {
                //        dRow["FactSquareCalcOverlap"] = GetSquareOverlap((int)dRow["ZoneId"], orderWorks);
                //        if ((double)dRow["FactSquareCalcOverlap"] > (double)dRow["FactSquareCalc"])
                //        {
                //            dRow["FactSquareCalcOverlap"] = dRow["FactSquareCalc"];
                //        }
                //    }
                //}
                //else
                //{
                //    dRow["FactSquareCalcOverlap"] = dRow["FactSquareCalc"];
                //}
            }

            return dataTable;
        }

        private List<int> GetOrderWorksList(int idMobitel, int idZone, int idDriver, int idAgr, int idWork,
            DateTime begin, DateTime end)
        {
            List<int> orderWorks = new List<int>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.RepFieldsProcessingExtData.SelectAgroOrderT, driverDb.ParamPrefics,
                    idZone, idMobitel);

                if (idDriver > 0)
                    sql = string.Format("{0} AND agro_ordert.Id_driver = {1} ", sql, idDriver);

                if (idAgr > 0)
                    sql = string.Format("{0} AND agro_ordert.Id_agregat = {1} ", sql, idAgr);

                if (idWork > 0)
                    sql = string.Format("{0} AND agro_ordert.Id_work = {1} ", sql, idWork);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", end);
                driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                while (driverDb.Read())
                {
                    orderWorks.Add(driverDb.GetInt32("Id"));
                }
                driverDb.CloseDataReader();
            }
            return orderWorks;
        }

        private double GetSquareOverlap(long idZone, List<int> orderWorks)
        {
            double squareOverlap = 0;
            IZone zone = DocItem.ZonesModel.GetById(Convert.ToInt32(idZone));
            if (zone != null)
            {
                using (
                    OrderSquareBitmap osb =
                        new OrderSquareBitmap(
                            (zone.AreaGa > GlobalVars.MAX_SQ_FOR_CHANGE_KF
                                ? GlobalVars.SCALE_BIG_ZONE
                                : GlobalVars.SCALE_WORK), zone.Points))
                {
                    squareOverlap = osb.CalcSquareOverlapToWork(orderWorks);
                }
            }
            return squareOverlap;
        }
    }
}
