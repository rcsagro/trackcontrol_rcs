namespace Agro.Reports.OrderReports
{
    partial class RepFieldsProcessing3View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepFieldsProcessing3View));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcessSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDutDrtFieldAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceTransit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtTransit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDutAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuellingsL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelDischargesL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.colProcessSquareOverLap = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.Size = new System.Drawing.Size(827, 470);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDateOrder,
            this.colVehicleName,
            this.colAgregatName,
            this.colWorkName,
            this.colFieldGroupName,
            this.colFieldName,
            this.colProcessSquare,
            this.colProcessSquareOverLap,
            this.colExpenseDUT,
            this.colExpenseDRT,
            this.colFuelExpenseDutDrtFieldAvgGa,
            this.colDistanceTransit,
            this.colFuelExpenseDrtTransit,
            this.colFuelExpenseDrtTotal,
            this.colFuelExpenseDutAvgGa,
            this.colFuelExpenseDrtAvgGa,
            this.colTotalFuellingsL,
            this.colTotalFuelDischargesL});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            this.gvReport.OptionsView.ShowFooter = true;
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOrder.Caption = "����";
            this.colDateOrder.FieldName = "DateOrder";
            this.colDateOrder.GroupFormat.FormatString = "d";
            this.colDateOrder.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 0;
            this.colDateOrder.Width = 62;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "������������ ��������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 1;
            this.colVehicleName.Width = 62;
            // 
            // colAgregatName
            // 
            this.colAgregatName.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregatName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregatName.Caption = "�������� ������������";
            this.colAgregatName.FieldName = "AgregatName";
            this.colAgregatName.Name = "colAgregatName";
            this.colAgregatName.OptionsColumn.AllowEdit = false;
            this.colAgregatName.OptionsColumn.ReadOnly = true;
            this.colAgregatName.Visible = true;
            this.colAgregatName.VisibleIndex = 2;
            this.colAgregatName.Width = 62;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkName.Caption = "��� �����";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.VisibleIndex = 3;
            this.colWorkName.Width = 62;
            // 
            // colFieldGroupName
            // 
            this.colFieldGroupName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldGroupName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldGroupName.Caption = "������ �����";
            this.colFieldGroupName.FieldName = "FieldGroupName";
            this.colFieldGroupName.Name = "colFieldGroupName";
            this.colFieldGroupName.OptionsColumn.AllowEdit = false;
            this.colFieldGroupName.OptionsColumn.ReadOnly = true;
            this.colFieldGroupName.Visible = true;
            this.colFieldGroupName.VisibleIndex = 4;
            this.colFieldGroupName.Width = 62;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldName.Caption = "����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 5;
            this.colFieldName.Width = 62;
            // 
            // colProcessSquare
            // 
            this.colProcessSquare.AppearanceCell.Options.UseTextOptions = true;
            this.colProcessSquare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colProcessSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colProcessSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcessSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProcessSquare.Caption = "������������ �������, ��";
            this.colProcessSquare.FieldName = "ProcessSquare";
            this.colProcessSquare.Name = "colProcessSquare";
            this.colProcessSquare.OptionsColumn.AllowEdit = false;
            this.colProcessSquare.OptionsColumn.ReadOnly = true;
            this.colProcessSquare.Visible = true;
            this.colProcessSquare.VisibleIndex = 6;
            this.colProcessSquare.Width = 78;
            // 
            // colExpenseDUT
            // 
            this.colExpenseDUT.AppearanceCell.Options.UseTextOptions = true;
            this.colExpenseDUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colExpenseDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseDUT.Caption = "������ ������� ���, �";
            this.colExpenseDUT.FieldName = "FuelExpenseDut";
            this.colExpenseDUT.Name = "colExpenseDUT";
            this.colExpenseDUT.OptionsColumn.AllowEdit = false;
            this.colExpenseDUT.OptionsColumn.ReadOnly = true;
            this.colExpenseDUT.Visible = true;
            this.colExpenseDUT.VisibleIndex = 8;
            this.colExpenseDUT.Width = 59;
            // 
            // colExpenseDRT
            // 
            this.colExpenseDRT.AppearanceCell.Options.UseTextOptions = true;
            this.colExpenseDRT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colExpenseDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseDRT.Caption = "������ ������� ���, �";
            this.colExpenseDRT.FieldName = "FuelExpenseDrtField";
            this.colExpenseDRT.Name = "colExpenseDRT";
            this.colExpenseDRT.OptionsColumn.AllowEdit = false;
            this.colExpenseDRT.OptionsColumn.ReadOnly = true;
            this.colExpenseDRT.Visible = true;
            this.colExpenseDRT.VisibleIndex = 9;
            this.colExpenseDRT.Width = 59;
            // 
            // colFuelExpenseDutDrtFieldAvgGa
            // 
            this.colFuelExpenseDutDrtFieldAvgGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDutDrtFieldAvgGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFuelExpenseDutDrtFieldAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDutDrtFieldAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutDrtFieldAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDutDrtFieldAvgGa.Caption = "�������  ������ (���������), �/��";
            this.colFuelExpenseDutDrtFieldAvgGa.FieldName = "FuelExpenseDutDrtFieldAvgGa";
            this.colFuelExpenseDutDrtFieldAvgGa.Name = "colFuelExpenseDutDrtFieldAvgGa";
            this.colFuelExpenseDutDrtFieldAvgGa.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDutDrtFieldAvgGa.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDutDrtFieldAvgGa.Visible = true;
            this.colFuelExpenseDutDrtFieldAvgGa.VisibleIndex = 10;
            this.colFuelExpenseDutDrtFieldAvgGa.Width = 86;
            // 
            // colDistanceTransit
            // 
            this.colDistanceTransit.AppearanceCell.Options.UseTextOptions = true;
            this.colDistanceTransit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colDistanceTransit.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistanceTransit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistanceTransit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistanceTransit.Caption = "��������, ��";
            this.colDistanceTransit.FieldName = "DistanceTransit";
            this.colDistanceTransit.Name = "colDistanceTransit";
            this.colDistanceTransit.OptionsColumn.AllowEdit = false;
            this.colDistanceTransit.OptionsColumn.ReadOnly = true;
            this.colDistanceTransit.Visible = true;
            this.colDistanceTransit.VisibleIndex = 11;
            this.colDistanceTransit.Width = 55;
            // 
            // colFuelExpenseDrtTransit
            // 
            this.colFuelExpenseDrtTransit.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTransit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFuelExpenseDrtTransit.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTransit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtTransit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtTransit.Caption = "������ ������� ��� (��������), �\"";
            this.colFuelExpenseDrtTransit.FieldName = "FuelExpenseDrtTransit";
            this.colFuelExpenseDrtTransit.Name = "colFuelExpenseDrtTransit";
            this.colFuelExpenseDrtTransit.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtTransit.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtTransit.Visible = true;
            this.colFuelExpenseDrtTransit.VisibleIndex = 12;
            this.colFuelExpenseDrtTransit.Width = 55;
            // 
            // colFuelExpenseDrtTotal
            // 
            this.colFuelExpenseDrtTotal.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFuelExpenseDrtTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtTotal.Caption = "����� ������ ������� ���, �";
            this.colFuelExpenseDrtTotal.FieldName = "FuelExpenseDrtTotal";
            this.colFuelExpenseDrtTotal.Name = "colFuelExpenseDrtTotal";
            this.colFuelExpenseDrtTotal.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtTotal.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtTotal.Visible = true;
            this.colFuelExpenseDrtTotal.VisibleIndex = 13;
            this.colFuelExpenseDrtTotal.Width = 55;
            // 
            // colFuelExpenseDutAvgGa
            // 
            this.colFuelExpenseDutAvgGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDutAvgGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDutAvgGa.Caption = "�������  ������ ���, �/��";
            this.colFuelExpenseDutAvgGa.FieldName = "FuelExpenseDutAvgGa";
            this.colFuelExpenseDutAvgGa.Name = "colFuelExpenseDutAvgGa";
            this.colFuelExpenseDutAvgGa.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDutAvgGa.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDutAvgGa.Visible = true;
            this.colFuelExpenseDutAvgGa.VisibleIndex = 14;
            this.colFuelExpenseDutAvgGa.Width = 55;
            // 
            // colFuelExpenseDrtAvgGa
            // 
            this.colFuelExpenseDrtAvgGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtAvgGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtAvgGa.Caption = "�������  ������ ���, �/��";
            this.colFuelExpenseDrtAvgGa.FieldName = "FuelExpenseDrtAvgGa";
            this.colFuelExpenseDrtAvgGa.Name = "colFuelExpenseDrtAvgGa";
            this.colFuelExpenseDrtAvgGa.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtAvgGa.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtAvgGa.Visible = true;
            this.colFuelExpenseDrtAvgGa.VisibleIndex = 15;
            this.colFuelExpenseDrtAvgGa.Width = 55;
            // 
            // colTotalFuellingsL
            // 
            this.colTotalFuellingsL.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuellingsL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalFuellingsL.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuellingsL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuellingsL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuellingsL.Caption = "����� ����������, �";
            this.colTotalFuellingsL.FieldName = "FuelDutAdd";
            this.colTotalFuellingsL.Name = "colTotalFuellingsL";
            this.colTotalFuellingsL.OptionsColumn.AllowEdit = false;
            this.colTotalFuellingsL.OptionsColumn.ReadOnly = true;
            this.colTotalFuellingsL.Visible = true;
            this.colTotalFuellingsL.VisibleIndex = 16;
            this.colTotalFuellingsL.Width = 55;
            // 
            // colTotalFuelDischargesL
            // 
            this.colTotalFuelDischargesL.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelDischargesL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalFuelDischargesL.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelDischargesL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelDischargesL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelDischargesL.Caption = "����� �����,�";
            this.colTotalFuelDischargesL.FieldName = "FuelDutSub";
            this.colTotalFuelDischargesL.Name = "colTotalFuelDischargesL";
            this.colTotalFuelDischargesL.OptionsColumn.AllowEdit = false;
            this.colTotalFuelDischargesL.OptionsColumn.ReadOnly = true;
            this.colTotalFuelDischargesL.Visible = true;
            this.colTotalFuelDischargesL.VisibleIndex = 17;
            this.colTotalFuelDischargesL.Width = 110;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // colProcessSquareOverLap
            // 
            this.colProcessSquareOverLap.AppearanceCell.Options.UseTextOptions = true;
            this.colProcessSquareOverLap.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcessSquareOverLap.AppearanceHeader.Options.UseTextOptions = true;
            this.colProcessSquareOverLap.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcessSquareOverLap.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProcessSquareOverLap.Caption = "������������ ������� (���������), ��";
            this.colProcessSquareOverLap.FieldName = "ProcessSquareOverlap";
            this.colProcessSquareOverLap.Name = "colProcessSquareOverLap";
            this.colProcessSquareOverLap.OptionsColumn.AllowEdit = false;
            this.colProcessSquareOverLap.OptionsColumn.ReadOnly = true;
            this.colProcessSquareOverLap.Visible = true;
            this.colProcessSquareOverLap.VisibleIndex = 7;
            this.colProcessSquareOverLap.Width = 58;
            // 
            // RepFieldsProcessing3View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Name = "RepFieldsProcessing3View";
            this.Size = new System.Drawing.Size(827, 470);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colExpenseDRT;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkName;
        private DevExpress.XtraGrid.Columns.GridColumn colProcessSquare;
        public DevExpress.XtraGrid.GridControl gcReport;
        public DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDutDrtFieldAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseDUT;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceTransit;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtTransit;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDutAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuellingsL;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelDischargesL;
        private DevExpress.XtraGrid.Columns.GridColumn colProcessSquareOverLap;
    }
}
