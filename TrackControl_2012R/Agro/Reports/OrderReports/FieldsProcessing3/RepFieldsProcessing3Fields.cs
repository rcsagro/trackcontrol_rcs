﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing3Fields
    {
        public DateTime DateOrder { get; set; }

        public string VehicleName { get; set; }

        public string AgregatName { get; set; }

        public string WorkName { get; set; }

        public string FieldName { get; set; }
        public string FieldNameOrig { get; set; }

        public int FieldId { get; set; }

        public string FieldGroupName { get; set; }

        public double ProcessSquare { get; set; }

        public double DistanceTransit { get; set; }

        public double FuelDutStart { get; set; }

        public double FuelDutEnd { get; set; }

        public double FuelDutAdd { get; set; }

        public double FuelDutSub { get; set; }

        public double FuelExpenseDut { get; set; }

        public double FuelExpenseDrtField { get; set; }

        public double FuelExpenseDrtTransit { get; set; }

        public double FuelExpenseDutDrtFieldAvgGa { get; set; }

        public double FuelExpenseDrtTotal { get; set; }

        public double FuelExpenseDutAvgGa { get; set; }

        public double FuelExpenseDrtAvgGa { get; set; }

        public double ProcessSquareOverlap { get; set; }
        

    }
}
