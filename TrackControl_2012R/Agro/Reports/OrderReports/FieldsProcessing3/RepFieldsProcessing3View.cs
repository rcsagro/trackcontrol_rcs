using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using Agro.Properties;

namespace Agro.Reports.OrderReports
{
    public partial class RepFieldsProcessing3View : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public RepFieldsProcessing3View()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            colDateOrder.Caption = Resources.Date;
            colVehicleName.Caption = Resources.Vehicle;
            colAgregatName.Caption = Resources.Agregat;
            colWorkName.Caption = Resources.WorkType;
            colFieldGroupName.Caption = Resources.FieldsGroup;
            colFieldName.Caption = string.Format("{0} ({1})", Resources.Field, Resources.SquareGA);
            colProcessSquare.Caption = Resources.SquareProcessGa;
            colProcessSquareOverLap.Caption = Resources.SquareContourGaOverlap;
            colExpenseDUT.Caption = Resources.FuelExpensDUT;
            colExpenseDRT.Caption = Resources.FuelExpensDRT;
            colFuelExpenseDutDrtFieldAvgGa.Caption = Resources.FuelExpenseAvgLGa;
            colDistanceTransit.Caption = Resources.MoveKm;
            colFuelExpenseDrtTransit.Caption = Resources.FuelExpensTransL;
            colFuelExpenseDrtTotal.Caption = Resources.TotalFuelConsumptionDPT_L;
            colFuelExpenseDutAvgGa.Caption = Resources.FuelAvgDutLGa;
            colFuelExpenseDrtAvgGa.Caption = Resources.FuelAvgDrtLGa;
            colTotalFuellingsL.Caption = Resources.TotalChargedL;
            colTotalFuelDischargesL.Caption = Resources.TotalDrainFuelL;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.FieldsProcessing3;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }
    }
}
