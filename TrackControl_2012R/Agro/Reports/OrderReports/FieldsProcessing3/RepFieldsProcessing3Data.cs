﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using Agro.Documents.Order;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing3Data
    {
        //private readonly Dictionary<long, List<FuelAddSubData>> _fuelRecords = new Dictionary<long, List<FuelAddSubData>>();
        public event Action<string, int> ChangeStatus;
        private int _idField;
        private int _idAgregat;
        private int _idWork;
        public RepFieldsProcessing3Data()
        {
            // to do
        }

        public List<RepFieldsProcessing3Fields> GetData(DateTime begin, DateTime end)
        {
            var records = new List<RepFieldsProcessing3Fields>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GetDataReader(begin, end, driverDb);
                string identRecord = "";
                int idOrder = 0;
                List<RepFieldsProcessing3Fields> recordsOrder = null;
                RepFieldsProcessing3Fields record = null;
                string squareWorkDescripOverlap = "";
                while (driverDb.Read())
                {
                    string orderDate = driverDb.GetDateTime("Date").ToString("dd.MM.yyyy");
                    string identReader = string.Format("{0}_{1}", orderDate, driverDb.GetInt32("Id_mobitel"));
                    ChangeStatus(string.Format("{0} {1} {2} {3}", orderDate, driverDb.GetString("VehicleName"), TotUtilites.NdbNullReader(driverDb, "AgregatName", ""),TotUtilites.NdbNullReader(driverDb, "WorkName", "")), Consts.PROGRESS_BAR_STOP);
                    if (idOrder != driverDb.GetInt32("Id"))
                    {
                        if (recordsOrder != null) DistributeProcessSquareByRecords(recordsOrder, squareWorkDescripOverlap);
                        recordsOrder = new List<RepFieldsProcessing3Fields>();
                        squareWorkDescripOverlap = TotUtilites.NdbNullReader(driverDb, "SquareWorkDescripOverlap", "").ToString();
                        idOrder = driverDb.GetInt32("Id");
                    }
                    if (IsNeedNewRecord(identRecord, identReader, driverDb))
                    {
                        if (record != null)
                        {
                            SetFinalSettings(record);
                            records.Add(record);
                           
                        }
                        identRecord = identReader;
                        record = new RepFieldsProcessing3Fields();
                        AddConstPart(driverDb, record);
                        AddVariablePart(driverDb, record,true);
                        if (recordsOrder != null) recordsOrder.Add(record);
                    }
                    else
                    {
                        AddVariablePart(driverDb, record);

                    }
                }
                if (record != null)
                {
                    SetFinalSettings(record);
                    records.Add(record);
                    if (recordsOrder != null) DistributeProcessSquareByRecords(recordsOrder, squareWorkDescripOverlap);
                }
            }
            ChangeStatus(Resources.Ready, Consts.PROGRESS_BAR_STOP);
            return records;
        }

        private bool IsNeedNewRecord(string identRecord, string identReader, DriverDb driverDb)
        {
            if (identRecord != identReader) return true;
            var idField = driverDb.GetInt32("Id_field");
            var idAgregat = driverDb.GetInt32("Id_agregat");
            var idWork = driverDb.GetInt32("Id_work");
            if (_idField != idField & idField > 0 & _idField > 0) return true;
            if (_idAgregat != idAgregat & idAgregat > 0 & _idAgregat > 0) return true;
            if (_idWork != idWork & idWork > 0 & _idWork > 0) return true;
            return false;
        }

        private  void GetDataReader(DateTime begin, DateTime end, DriverDb driverDb)
        {
            string sql = string.Format(AgroQuery.RepFieldsProcessing3Data.Select, AgroQuery.SqlVehicleIdent, AgroQuery.SqlFieldIdent, driverDb.ParamPrefics);

            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", begin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
            driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
        }

        private  void SetFinalSettings(RepFieldsProcessing3Fields record)
        {

            record.FuelExpenseDut = Math.Round(record.FuelDutStart + record.FuelDutAdd - record.FuelDutEnd, 2);
            record.FuelExpenseDrtTotal = Math.Round(record.FuelExpenseDrtField + record.FuelExpenseDrtTransit, 2);
            if (record.ProcessSquare > 0)
            {
                if (record.FuelExpenseDut > 0)
                    record.FuelExpenseDutAvgGa = Math.Round(record.FuelExpenseDut / record.ProcessSquare, 2);
                if (record.FuelExpenseDrtTotal > 0)
                    record.FuelExpenseDrtAvgGa = Math.Round(record.FuelExpenseDrtTotal / record.ProcessSquare, 2);

                record.FuelExpenseDutDrtFieldAvgGa = Math.Round((record.FuelExpenseDut + record.FuelExpenseDrtField )/ record.ProcessSquare, 2);
            }
            record.ProcessSquare = Math.Round(record.ProcessSquare, 2);
            _idField = 0;
            _idAgregat = 0;
            _idWork = 0;
        }

        private void AddConstPart(DriverDb driverDb, RepFieldsProcessing3Fields record)
        {
            record.DateOrder = driverDb.GetDateTime("Date");
            record.VehicleName = driverDb.GetString("VehicleName");
        }

        private void AddVariablePart(DriverDb driverDb, RepFieldsProcessing3Fields record,bool first = false)
        {

            if (first)
            {
                record.FuelDutStart += driverDb.GetDouble("FuelStart");
            }
            //if ((record.FieldName??"").Length == 0 && driverDb.GetInt32("Id_field") > 0)
            //{

            //}
            if ((int)TotUtilites.NdbNullReader(driverDb, "TypeWork", 0) == (int)DictionaryAgroWorkType.WorkTypes.WorkInField && (int)TotUtilites.NdbNullReader(driverDb, "Id_field", 0) > 0)
            {
                _idField = driverDb.GetInt32("Id_field");
                _idAgregat = driverDb.GetInt32("Id_agregat");
                _idWork = driverDb.GetInt32("Id_work");
                record.FieldName = driverDb.GetString("FieldName");
                record.FieldNameOrig = driverDb.GetString("FieldNameOrig");
                record.FieldId = driverDb.GetInt32("Id_field");
                record.FieldGroupName = driverDb.GetString("FieldGroupName");
                record.WorkName = TotUtilites.NdbNullReader(driverDb, "WorkName", "").ToString();
                record.ProcessSquare += driverDb.GetDouble("FactSquareCalc");
                record.FuelExpenseDrtField += driverDb.GetDouble("Fuel_ExpensTotal");
                record.AgregatName = TotUtilites.NdbNullReader(driverDb, "AgregatName", "").ToString();
            }
            else
            {
                record.FuelExpenseDrtTransit += driverDb.GetDouble("Fuel_ExpensTotal");
                record.DistanceTransit += driverDb.GetDouble("Distance");
            }
               
            record.FuelDutEnd = driverDb.GetDouble("FuelEnd");
            record.FuelDutAdd += driverDb.GetDouble("FuelAdd");
            record.FuelDutSub += driverDb.GetDouble("FuelSub");
            

            //record.TotalFuellingsL += driverDb.GetDouble("FuelAdd");
            //record.TotalFuelDischargesL += driverDb.GetDouble("FuelSub");
            //record.ExpenseDut += driverDb.GetDouble("FuelExpens");
            //record.ExpenseDrt += driverDb.GetDouble("Fuel_ExpensTotal");
            
            //SetFuelAddSub(driverDb.GetInt64("Id"), driverDb.GetDateTime("TimeStart"), driverDb.GetDateTime("TimeEnd"), record);
        }

        private void DistributeProcessSquareByRecords(IEnumerable<RepFieldsProcessing3Fields> recordsOrder,string squareWorkDescripOverlap)
        {
            var fieldsRecords = new Dictionary<string, List<RepFieldsProcessing3Fields>>();
            foreach (var recordOrder in recordsOrder)
            {
                if (recordOrder.FieldId > 0)
                {
                    if (fieldsRecords.ContainsKey(recordOrder.FieldNameOrig))
                    {
                        fieldsRecords[recordOrder.FieldNameOrig].Add(recordOrder);
                    }
                        else
                    {
                        var records = new List<RepFieldsProcessing3Fields> {recordOrder};
                        fieldsRecords.Add(recordOrder.FieldNameOrig, records);
                    }
                }
            }
            if (fieldsRecords.Count > 0)
            {
                foreach (KeyValuePair<string, List<RepFieldsProcessing3Fields>> fieldRecord in fieldsRecords)
                {
                    var oi = new OrderItem {SquareWorkDescriptOverlap = squareWorkDescripOverlap};
                    double sqOverlap = oi.GetFieldSquareFromFieldsString(squareWorkDescripOverlap, fieldRecord.Key);
                    if (sqOverlap > 0)
                    {
                        if (fieldRecord.Value.Count == 1)
                        {
                            fieldRecord.Value[0].ProcessSquareOverlap = sqOverlap;
                        }
                        else
                        {
                            double sqCalc = fieldRecord.Value.Sum(record => record.ProcessSquare);
                            double kf = sqOverlap / sqCalc;
                            if (kf > 1) kf = 1; 
                            foreach (var record in fieldRecord.Value)
                            {
                                record.ProcessSquareOverlap = Math.Round(record.ProcessSquare * kf, 2);
                            }
                        }
                    }

                }
            }

        }

    }

    
}
