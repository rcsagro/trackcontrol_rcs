﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public  class RepVehiclesAgregatsFields
    {
        string _vehicleName;

        public string VehicleName
        {
            get { return _vehicleName; }
            set { _vehicleName = value; }
        }

        string _agregatName;

        public string AgregatName
        {
            get { return _agregatName; }
            set { _agregatName = value; }
        }

        DateTime _startWork;

        public DateTime StartWork
        {
            get { return _startWork; }
            set { _startWork = value; }
        }

        DateTime _endWork;

        public DateTime EndWork
        {
            get { return _endWork; }
            set { _endWork = value; }
        }

        TimeSpan _totalHours;

        public TimeSpan TotalHours
        {
            get { return _totalHours; }
            set { _totalHours = value; }
        }

        double _totalSquare;

        public double TotalSquare
        {
            get { return _totalSquare; }
            set { _totalSquare = value; }
        }

        TimeSpan _rateTotal;

        public TimeSpan RateTotal
        {
            get { return _rateTotal; }
            set { _rateTotal = value; }
        }

        TimeSpan _rateWork;

        public TimeSpan RateWork
        {
            get { return _rateWork; }
            set { _rateWork = value; }
        }

        TimeSpan _rateMove;

        public TimeSpan RateMove
        {
            get { return _rateMove; }
            set { _rateMove = value; }
        }

        double _expenseTotalDUT;

        public double ExpenseTotalDUT
        {
            get { return _expenseTotalDUT; }
            set { _expenseTotalDUT = value; }
        }

        double _expenseTotalDRT;

        public double ExpenseTotalDRT
        {
            get { return _expenseTotalDRT; }
            set { _expenseTotalDRT = value; }
        }

        double _expenseOnRateDUT;

        public double ExpenseOnRateDUT
        {
            get { return _expenseOnRateDUT; }
            set { _expenseOnRateDUT = value; }
        }

        double _expenseOnRateDRT;

        public double ExpenseOnRateDRT
        {
            get { return _expenseOnRateDRT; }
            set { _expenseOnRateDRT = value; }
        }


    }
}
