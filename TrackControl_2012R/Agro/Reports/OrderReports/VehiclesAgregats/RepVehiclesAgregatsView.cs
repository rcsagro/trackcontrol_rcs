using System;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public partial class RepVehiclesAgregatsView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }
        
        public RepVehiclesAgregatsView()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }
        void Localization()
        {
            colVehicleName.Caption = Resources.Vehicle;
            colAgregatName.Caption = Resources.Agregat;
            colStartWork.Caption = Resources.TimeStart;
            colEndWork.Caption = Resources.TimeEnd;
            colTotalHours.Caption = Resources.TotalHours;
            colTotalSquare.Caption = Resources.SquareGA;
            colRateTotal.Caption = Resources.MotoHourTotal;
            colRateWork.Caption = Resources.MotoHourWork;
            colRateMove.Caption = Resources.MotoHourMove;
            colExpenseTotalDUT.Caption = Resources.FuelExpensDUT;
            colExpenseOnRateDUT.Caption = Resources.FuelExpensOnRateDUT;
            colExpenseTotalDRT.Caption = Resources.FuelExpensDRT;
            colExpenseOnRateDRT.Caption = Resources.FuelExpensOnRateDRT;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.VehicleAggregateRow1;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }


    }
}
