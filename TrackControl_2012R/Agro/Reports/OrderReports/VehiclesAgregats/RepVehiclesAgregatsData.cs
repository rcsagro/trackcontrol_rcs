﻿using System;
using System.Collections.Generic;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepVehiclesAgregatsData
    {
        public RepVehiclesAgregatsData()
        {
            // to do
        }

        public List<RepVehiclesAgregatsFields> GetData(DateTime begin, DateTime end)
        {
            var records = new List<RepVehiclesAgregatsFields>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GetDataReader(begin, end, driverDb);
                string identRecord = "";
                string identReader = "";
                RepVehiclesAgregatsFields record = null;
                while (driverDb.Read())
                {
                    identReader = string.Format("{0}_{1}", driverDb.GetInt32("vehicleId"),
                        driverDb.GetInt32("AgregatId"));
                    if (identRecord != identReader)
                    {
                        if (record != null)
                        {
                            SetFinalSettings(record);
                            records.Add(record);
                        }
                        identRecord = identReader;
                        record = new RepVehiclesAgregatsFields();
                        AddConstPart(driverDb, record);
                        AddVariablePart(driverDb, record);
                    }
                    else
                    {
                        AddVariablePart(driverDb, record);

                    }
                }
                if (record != null)
                {
                    SetFinalSettings(record);
                    records.Add(record);
                }
            }

            return records;
        }

        private static void GetDataReader(DateTime begin, DateTime end, DriverDb driverDb)
        {
            string sql = string.Format(AgroQuery.RepVehiclesAgregatsData.SelectAgro_Ordert, AgroQuery.SqlVehicleIdent);

            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", begin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
            driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
        }

        private static void SetFinalSettings(RepVehiclesAgregatsFields record)
        {
            if (record.RateTotal.TotalHours > 0)
            {
                record.ExpenseOnRateDUT = Math.Round(record.ExpenseTotalDUT/record.RateTotal.TotalHours, 2);
                record.ExpenseOnRateDRT = Math.Round(record.ExpenseTotalDRT/record.RateTotal.TotalHours, 2);
            }
            record.ExpenseTotalDRT = Math.Round(record.ExpenseTotalDRT, 2);
            record.ExpenseTotalDUT = Math.Round(record.ExpenseTotalDUT, 2);
        }

        private void AddConstPart(DriverDb driverDb, RepVehiclesAgregatsFields record)
        {
            record.VehicleName = driverDb.GetString("VehicleName");
            record.AgregatName = driverDb.GetString("AgregatName");
            record.StartWork = driverDb.GetDateTime("TimeStart");
        }

        private void AddVariablePart(DriverDb driverDb, RepVehiclesAgregatsFields record)
        {
            record.EndWork = driverDb.GetDateTime("TimeEnd");
            record.TotalHours += driverDb.GetDateTime("TimeEnd").Subtract(driverDb.GetDateTime("TimeStart"));
            record.RateTotal =
                record.RateTotal.Add(driverDb.IsDbNull(driverDb.GetOrdinal("TimeRate"))
                    ? TimeSpan.Zero
                    : driverDb.GetTimeSpan("TimeRate"));
            if (driverDb.GetInt32("Id_field") > 0 && driverDb.GetInt16("Confirm") != 0)
            {
                record.TotalSquare += driverDb.GetDouble("FactSquareCalc");
                record.RateWork =record.RateWork.Add(driverDb.IsDbNull(driverDb.GetOrdinal("TimeRate"))
                        ? TimeSpan.Zero
                        : driverDb.GetTimeSpan("TimeRate"));
            }
            else
            {
                record.RateMove =
                    record.RateMove.Add(driverDb.IsDbNull(driverDb.GetOrdinal("TimeRate"))
                        ? TimeSpan.Zero
                        : driverDb.GetTimeSpan("TimeRate"));
            }
            record.ExpenseTotalDUT += driverDb.GetDouble("FuelExpens");
            record.ExpenseTotalDRT += driverDb.GetDouble("Fuel_ExpensTotal");
        }
    }
}
