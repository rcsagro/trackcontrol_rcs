﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{

    public class RepVehiclesAgregats : IReportItem 
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.VehicleAggregateRow2, Environment.NewLine);
        Image _largeImage = Shared.Tractor64;
        Image _smallImage = Shared.Tractor64;

        public RepVehiclesAgregats()
        {
            _dataViewer = new RepVehiclesAgregatsView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            RepVehiclesAgregatsData dataSource = new RepVehiclesAgregatsData();
            ((RepVehiclesAgregatsView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end);
            ((RepVehiclesAgregatsView)_dataViewer).Begin = begin;
            ((RepVehiclesAgregatsView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return ((RepVehiclesAgregatsView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepVehiclesAgregatsView)_dataViewer).ExportToExcel();
        }
        #endregion
    }

}
