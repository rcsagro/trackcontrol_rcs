namespace Agro.Reports.OrderReports
{
    partial class RepVehiclesAgregatsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepVehiclesAgregatsView));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRateMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseTotalDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseOnRateDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseTotalDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseOnRateDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teTime});
            this.gcReport.Size = new System.Drawing.Size(891, 494);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVehicleName,
            this.colAgregatName,
            this.colStartWork,
            this.colEndWork,
            this.colTotalHours,
            this.colTotalSquare,
            this.colRateTotal,
            this.colRateWork,
            this.colRateMove,
            this.colExpenseTotalDUT,
            this.colExpenseOnRateDUT,
            this.colExpenseTotalDRT,
            this.colExpenseOnRateDRT});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "�������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.AllowFocus = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 0;
            // 
            // colAgregatName
            // 
            this.colAgregatName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregatName.Caption = "�������";
            this.colAgregatName.FieldName = "AgregatName";
            this.colAgregatName.Name = "colAgregatName";
            this.colAgregatName.OptionsColumn.AllowEdit = false;
            this.colAgregatName.OptionsColumn.AllowFocus = false;
            this.colAgregatName.OptionsColumn.ReadOnly = true;
            this.colAgregatName.Visible = true;
            this.colAgregatName.VisibleIndex = 1;
            // 
            // colStartWork
            // 
            this.colStartWork.AppearanceCell.Options.UseTextOptions = true;
            this.colStartWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStartWork.Caption = "����� ������";
            this.colStartWork.DisplayFormat.FormatString = "g";
            this.colStartWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colStartWork.FieldName = "StartWork";
            this.colStartWork.Name = "colStartWork";
            this.colStartWork.OptionsColumn.AllowEdit = false;
            this.colStartWork.OptionsColumn.AllowFocus = false;
            this.colStartWork.OptionsColumn.ReadOnly = true;
            this.colStartWork.Visible = true;
            this.colStartWork.VisibleIndex = 2;
            // 
            // colEndWork
            // 
            this.colEndWork.AppearanceCell.Options.UseTextOptions = true;
            this.colEndWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colEndWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndWork.Caption = "����� ���������";
            this.colEndWork.DisplayFormat.FormatString = "g";
            this.colEndWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEndWork.FieldName = "EndWork";
            this.colEndWork.Name = "colEndWork";
            this.colEndWork.OptionsColumn.AllowEdit = false;
            this.colEndWork.OptionsColumn.AllowFocus = false;
            this.colEndWork.OptionsColumn.ReadOnly = true;
            this.colEndWork.Visible = true;
            this.colEndWork.VisibleIndex = 3;
            // 
            // colTotalHours
            // 
            this.colTotalHours.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalHours.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHours.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalHours.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalHours.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalHours.Caption = "����� �����";
            this.colTotalHours.ColumnEdit = this.teTime;
            this.colTotalHours.DisplayFormat.FormatString = "t";
            this.colTotalHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTotalHours.FieldName = "TotalHours";
            this.colTotalHours.Name = "colTotalHours";
            this.colTotalHours.OptionsColumn.AllowEdit = false;
            this.colTotalHours.OptionsColumn.AllowFocus = false;
            this.colTotalHours.OptionsColumn.ReadOnly = true;
            this.colTotalHours.Visible = true;
            this.colTotalHours.VisibleIndex = 4;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.teTime.Name = "teTime";
            // 
            // colTotalSquare
            // 
            this.colTotalSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalSquare.Caption = "�������, ��";
            this.colTotalSquare.FieldName = "TotalSquare";
            this.colTotalSquare.Name = "colTotalSquare";
            this.colTotalSquare.OptionsColumn.AllowEdit = false;
            this.colTotalSquare.OptionsColumn.AllowFocus = false;
            this.colTotalSquare.OptionsColumn.ReadOnly = true;
            this.colTotalSquare.Visible = true;
            this.colTotalSquare.VisibleIndex = 5;
            // 
            // colRateTotal
            // 
            this.colRateTotal.AppearanceCell.Options.UseTextOptions = true;
            this.colRateTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.colRateTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRateTotal.Caption = "�������� �����";
            this.colRateTotal.ColumnEdit = this.teTime;
            this.colRateTotal.DisplayFormat.FormatString = "t";
            this.colRateTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRateTotal.FieldName = "RateTotal";
            this.colRateTotal.Name = "colRateTotal";
            this.colRateTotal.OptionsColumn.AllowEdit = false;
            this.colRateTotal.OptionsColumn.AllowFocus = false;
            this.colRateTotal.OptionsColumn.ReadOnly = true;
            this.colRateTotal.Visible = true;
            this.colRateTotal.VisibleIndex = 6;
            // 
            // colRateWork
            // 
            this.colRateWork.AppearanceCell.Options.UseTextOptions = true;
            this.colRateWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colRateWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRateWork.Caption = "�������� � ������";
            this.colRateWork.ColumnEdit = this.teTime;
            this.colRateWork.DisplayFormat.FormatString = "t";
            this.colRateWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRateWork.FieldName = "RateWork";
            this.colRateWork.Name = "colRateWork";
            this.colRateWork.OptionsColumn.AllowEdit = false;
            this.colRateWork.OptionsColumn.AllowFocus = false;
            this.colRateWork.OptionsColumn.ReadOnly = true;
            this.colRateWork.Visible = true;
            this.colRateWork.VisibleIndex = 7;
            // 
            // colRateMove
            // 
            this.colRateMove.AppearanceCell.Options.UseTextOptions = true;
            this.colRateMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colRateMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRateMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRateMove.Caption = "�������� �� ��������";
            this.colRateMove.ColumnEdit = this.teTime;
            this.colRateMove.DisplayFormat.FormatString = "t";
            this.colRateMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRateMove.FieldName = "RateMove";
            this.colRateMove.Name = "colRateMove";
            this.colRateMove.OptionsColumn.AllowEdit = false;
            this.colRateMove.OptionsColumn.AllowFocus = false;
            this.colRateMove.OptionsColumn.ReadOnly = true;
            this.colRateMove.Visible = true;
            this.colRateMove.VisibleIndex = 8;
            // 
            // colExpenseTotalDUT
            // 
            this.colExpenseTotalDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseTotalDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseTotalDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseTotalDUT.Caption = "������ ���, �";
            this.colExpenseTotalDUT.FieldName = "ExpenseTotalDUT";
            this.colExpenseTotalDUT.Name = "colExpenseTotalDUT";
            this.colExpenseTotalDUT.OptionsColumn.AllowEdit = false;
            this.colExpenseTotalDUT.OptionsColumn.AllowFocus = false;
            this.colExpenseTotalDUT.OptionsColumn.ReadOnly = true;
            this.colExpenseTotalDUT.Visible = true;
            this.colExpenseTotalDUT.VisibleIndex = 9;
            // 
            // colExpenseOnRateDUT
            // 
            this.colExpenseOnRateDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseOnRateDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseOnRateDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseOnRateDUT.Caption = "������ ��� � �� �������";
            this.colExpenseOnRateDUT.FieldName = "ExpenseOnRateDUT";
            this.colExpenseOnRateDUT.Name = "colExpenseOnRateDUT";
            this.colExpenseOnRateDUT.OptionsColumn.AllowEdit = false;
            this.colExpenseOnRateDUT.OptionsColumn.AllowFocus = false;
            this.colExpenseOnRateDUT.OptionsColumn.ReadOnly = true;
            this.colExpenseOnRateDUT.Visible = true;
            this.colExpenseOnRateDUT.VisibleIndex = 10;
            // 
            // colExpenseTotalDRT
            // 
            this.colExpenseTotalDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseTotalDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseTotalDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseTotalDRT.Caption = "������ ���, �";
            this.colExpenseTotalDRT.FieldName = "ExpenseTotalDRT";
            this.colExpenseTotalDRT.Name = "colExpenseTotalDRT";
            this.colExpenseTotalDRT.OptionsColumn.AllowEdit = false;
            this.colExpenseTotalDRT.OptionsColumn.AllowFocus = false;
            this.colExpenseTotalDRT.OptionsColumn.ReadOnly = true;
            this.colExpenseTotalDRT.Visible = true;
            this.colExpenseTotalDRT.VisibleIndex = 11;
            // 
            // colExpenseOnRateDRT
            // 
            this.colExpenseOnRateDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseOnRateDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseOnRateDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseOnRateDRT.Caption = "������ ��� � �� �������";
            this.colExpenseOnRateDRT.FieldName = "ExpenseOnRateDRT";
            this.colExpenseOnRateDRT.Name = "colExpenseOnRateDRT";
            this.colExpenseOnRateDRT.OptionsColumn.AllowEdit = false;
            this.colExpenseOnRateDRT.OptionsColumn.AllowFocus = false;
            this.colExpenseOnRateDRT.OptionsColumn.ReadOnly = true;
            this.colExpenseOnRateDRT.Visible = true;
            this.colExpenseOnRateDRT.VisibleIndex = 12;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepVehiclesAgregatsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Name = "RepVehiclesAgregatsView";
            this.Size = new System.Drawing.Size(891, 494);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gcReport;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatName;
        private DevExpress.XtraGrid.Columns.GridColumn colStartWork;
        private DevExpress.XtraGrid.Columns.GridColumn colEndWork;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHours;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSquare;
        private DevExpress.XtraGrid.Columns.GridColumn colRateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRateWork;
        private DevExpress.XtraGrid.Columns.GridColumn colRateMove;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseTotalDUT;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseOnRateDUT;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseTotalDRT;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseOnRateDRT;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
    }
}
