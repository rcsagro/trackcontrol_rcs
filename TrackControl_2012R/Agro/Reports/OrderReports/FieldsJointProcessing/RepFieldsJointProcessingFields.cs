﻿using System;
using System.Collections.Generic;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsJointProcessingFields
    {
        public string FieldName { get; set; }
        public string FieldGroupName { get; set; }
        public string WorkTypeName { get; set; }
        public int IdZone { get; set; }
        public Decimal SquareFactGa { get; set; }
        public Decimal SquareAfterRecalc { get; set; }
        public List<TechOperationFactRecord> FactRecords { get; set; }
    }
}
