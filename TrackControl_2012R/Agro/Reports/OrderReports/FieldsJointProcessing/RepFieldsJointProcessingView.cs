using System;
using System.Windows.Forms;
using Agro.Dictionaries;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid ;
using TrackControl.General;
using Agro.Properties;

namespace Agro.Reports.OrderReports
{
    public partial class RepFieldJointProcessingView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public int Group {
            get
            {
                if (bleFieldGroups.EditValue == null)
                    return 0;
                else
                return (int)bleFieldGroups.EditValue;
            }
        }

        public int Field
        {
            get
            {
                if (bleField.EditValue == null)
                    return 0;
                else
                return (int)bleField.EditValue;
            }
        }



        public RepFieldJointProcessingView()
        {
            InitializeComponent();
            Localization();
            leFieldGroups.DataSource = DictionaryAgroFieldGrp.GetList();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            colVehicleName.Caption = Resources.Vehicle;
            colAgregateName.Caption = Resources.Agregat;
            colDriverName.Caption = Resources.Driver;
            colFieldName.Caption = string.Format("{0} ({1})", Resources.Field, Resources.SquareGA);
            colWorkName.Caption = Resources.WorkTypes;
            colFieldGroupName.Caption = Resources.FieldsGroup;

            colNumberOrder.Caption = Resources.Number;
            colDateOrder.Caption = Resources.Date;

            colSquareFactGa.Caption = Resources.FactSquareGa;
            colSqFactGa.Caption = Resources.FactSquareGa;
            colSquareAfterRecalc.Caption = Resources.FactSquareGaJoint;
            colSqAfterRecalc.Caption = Resources.FactSquareGaJoint;


        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.FieldsJointProcessing;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }

        private void gvReport_DoubleClick(object sender, EventArgs e)
        {
            var record = gvReport.GetRow(gvReport.FocusedRowHandle) as RepFieldsJointProcessingFields;
            if (record != null)
            {
                if (record.FactRecords.Count > 0)
                {
                    var sa = new SquareAnalizer(record.FactRecords, record.IdZone);
                    sa.StartDraw(false);
                }
            }
        }

        private void gvReport_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gvReport_MasterRowGetRelationDisplayCaption(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0: { e.RelationName = "������"; break; } 
            }
        }

        private void gvReport_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0: { e.RelationName = "Orders"; break; } 
            }
        }

        private void gvReport_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0:
                    {
                        e.ChildList = (gvReport.GetRow(e.RowHandle) as RepFieldsJointProcessingFields).FactRecords; 
                        break;
                    }
            }
        }

        private void bleFieldGroups_EditValueChanged(object sender, EventArgs e)
        {
            if (bleFieldGroups.EditValue != null)
                leField.DataSource = DictionaryAgroField.GetList((int)bleFieldGroups.EditValue);
        }

        private void bbiFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleFieldGroups.EditValue = null;
            bleField.EditValue = null; 
        }

        private void gvReportDetal_DoubleClick(object sender, EventArgs e)
        {
            GridView detailView = gvReport.GetDetailView(gvReport.FocusedRowHandle, 0) as GridView;
            int idOrder = 0;
            if (Int32.TryParse(detailView.GetRowCellValue(detailView.FocusedRowHandle, "NumberOrder").ToString(), out idOrder))
            {
                using (Form f = new Order(idOrder, false))
                {
                    f.ShowDialog();
                }
            }

        }
    }
}
