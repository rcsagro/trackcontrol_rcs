﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsJointProcessing : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.FieldsJointProcessing, Environment.NewLine);
        readonly Image _largeImage = Shared.FieldsProcessing;
        readonly Image _smallImage = Shared.FieldsProcessing;

        public RepFieldsJointProcessing()
        {
            _dataViewer = new RepFieldJointProcessingView {Dock = DockStyle.Fill};
        }


        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dataSource = new RepFieldsJointProcessingData();
            dataSource.ChangeStatus += OnChangeStatus;
            ((RepFieldJointProcessingView) _dataViewer).gcReport.DataSource = null;
            ((RepFieldJointProcessingView) _dataViewer).gcReport.RefreshDataSource();
            ((RepFieldJointProcessingView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end, ((RepFieldJointProcessingView)_dataViewer).Group, ((RepFieldJointProcessingView)_dataViewer).Field);
            ((RepFieldJointProcessingView)_dataViewer).Begin = begin;
            ((RepFieldJointProcessingView)_dataViewer).End = end;
            dataSource.ChangeStatus -= OnChangeStatus; 
        }

        public bool IsHasData
        {
            get { return ((RepFieldJointProcessingView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepFieldJointProcessingView)_dataViewer).ExportToExcel();
        }

        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }
    }
}
