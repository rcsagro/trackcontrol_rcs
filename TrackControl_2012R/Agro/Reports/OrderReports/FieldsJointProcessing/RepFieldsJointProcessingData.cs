﻿using Agro.Properties;
using Agro.Utilites;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsJointProcessingData
    {
        public event Action<string, int> ChangeStatus;


        public IList<RepFieldsJointProcessingFields> GetData(DateTime begin, DateTime end, int group, int field)
        {
            IList<RepFieldsJointProcessingFields> fields = null;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql =
                    string.Format(AgroQuery.RepFieldsJointProcessingData.SelectFromOrders(group, field), AgroQuery.SqlVehicleIdent, driverDb.ParamPrefics,
                    TrackControlQuery.SqlDriverIdent, AgroQuery.SqlFieldIdent);
                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DS", begin);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "DE", end );
                    driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
                    ChangeStatus(Resources.DataSelect, GetFieldsCount(begin, end, group, field));
                    fields = GetParsetList(driverDb);
                    ChangeStatus(Resources.Ready,Consts.PROGRESS_BAR_STOP);

            }
            return fields; }

        IList<RepFieldsJointProcessingFields> GetParsetList(DriverDb driverDb)
        {
            var records = new BindingList<RepFieldsJointProcessingFields>();
            int curField = 0;
            int curWork = 0;
            int curDriver = 0;
            int curOrder = 0;
            int cntFields = 0;
            RepFieldsJointProcessingFields record = null;
            while (driverDb.Read())
            {
                int workField = driverDb.GetInt32("FieldId");
                int workWork = driverDb.GetInt32("WorkId");
                int workDriver = driverDb.GetInt32("DriverId");
                int idOrder = driverDb.GetInt32("Id_main");
                if (workField != curField)
                {
                    cntFields++;
                    ChangeStatus(driverDb.GetString("FieldName"), cntFields);
                }
                if (workField != curField || workWork != curWork)
                {
                    curField = workField;
                    curWork = workWork;
                    curDriver = workDriver;
                    AddRecord(record, records);
                    record = new RepFieldsJointProcessingFields
                        {
                            FactRecords = new List<TechOperationFactRecord>(),
                            FieldName = driverDb.GetString("FieldName"),
                            WorkTypeName = driverDb.GetString("WorkName"),
                            IdZone = driverDb.GetInt32("Id_zone"),
                            FieldGroupName = driverDb.GetString("FieldGroupName")
                        };
                    AddFactRecord(driverDb, record, true,true);
                }
                else
                {

                    bool isNewDriver = curDriver != workDriver;
                    curDriver = workDriver;
                    bool isNewOrder = idOrder != curOrder;
                    AddFactRecord(driverDb, record, isNewOrder, isNewDriver);
                   
                }
                curOrder = idOrder;
            }
            AddRecord(record, records);
            driverDb.CloseDataReader();
            return records;

        }

        private static void AddRecord(RepFieldsJointProcessingFields record, BindingList<RepFieldsJointProcessingFields> records)
        {
            if (record != null && record.FactRecords != null && record.FactRecords.Count > 0)
            {
                var sa = new SquareAnalizer(record.FactRecords, record.IdZone);
                sa.CalcSquares();
                foreach (var factRecord in record.FactRecords)
                {
                    record.SquareFactGa += factRecord.SquareFactGa;
                    record.SquareAfterRecalc += factRecord.SquareAfterRecalc;
                    foreach (OrderItemRecord orderRecord in factRecord.OrderTs)
                    {
                        if (factRecord.SquareFactGa > 0)
                        {
                            orderRecord.UpdateFactSquareJointProcessing(Math.Round(factRecord.SquareAfterRecalc *
                                                                        (decimal)orderRecord.SquareCalcCont /
                                                                        factRecord.SquareFactGa, 3));
                        }
                        else
                        {
                            orderRecord.UpdateFactSquareJointProcessing(0);
                        }

                    }
                }
                records.Add(record);
            }
        }

        private static void AddFactRecord(DriverDb driverDb, RepFieldsJointProcessingFields record,bool isNewOrder,bool isNewDriver)
        {

            if (isNewOrder || isNewDriver)
            {
                var factRecord = new TechOperationFactRecord
                {
                    AgregateName = TotUtilites.NdbNullReader(driverDb,"AgrName","").ToString(),
                    DriverName = TotUtilites.NdbNullReader(driverDb, "DriverName", "").ToString(),
                    VehicleName = TotUtilites.NdbNullReader(driverDb, "VehicleName", "").ToString(),
                    NumberOrder = driverDb.GetInt32("Id_main"),
                    DateOrder = driverDb.GetDateTime("Date"),
                    SquareFactGa = (decimal)driverDb.GetDouble("FactSquareCalc"),
                    IdOrderTs = new List<int>(),
                    OrderTs = new List<OrderItemRecord>()
                };
                factRecord.IdOrderTs.Add(driverDb.GetInt32("Id"));
                var orderTRecord = new OrderItemRecord
                    {
                        IdOrderT = driverDb.GetInt32("Id"),
                        SquareCalcCont = driverDb.GetDouble("FactSquareCalc")
                    };
                factRecord.OrderTs.Add(orderTRecord);
                record.FactRecords.Add(factRecord);
            }
            else
            {
                record.FactRecords[record.FactRecords.Count - 1].IdOrderTs.Add(driverDb.GetInt32("Id"));
                var orderTRecord = new OrderItemRecord
                {
                    IdOrderT = driverDb.GetInt32("Id"),
                    SquareCalcCont = driverDb.GetDouble("FactSquareCalc")
                };
                record.FactRecords[record.FactRecords.Count - 1].OrderTs.Add(orderTRecord);
                record.FactRecords[record.FactRecords.Count - 1].SquareFactGa += (decimal)driverDb.GetDouble("FactSquareCalc");
            }

        }

        private static int GetFieldsCount(DateTime begin, DateTime end, int group, int field)
        {
            int fieldsCount = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql =
                    string.Format(AgroQuery.RepFieldsJointProcessingData.GetFieldsCount(group, field), driverDb.ParamPrefics);
                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DS", begin);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "DE", end );
                    fieldsCount = driverDb.GetScalarValueNull<int>(sql, driverDb.GetSqlParameterArray,0);
            }
            return fieldsCount; 
        }
    }
}
