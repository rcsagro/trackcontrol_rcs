﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Agro.Reports.OrderReports.DriversAgregatsWorks;
using Agro.Utilites;
using DevExpress.XtraBars;
using DevExpress.XtraNavBar;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepController
    {
        List<IReportItem> _reportsList;
        IReportItem _reportActive;
        readonly Control _parentControl;
        readonly NavBarGroup _barGroup;
        readonly NavBarControl _bar;
        private BarEditItem _pbStatus;
        private BarStaticItem _lbStatus;

        public event Action<string>  ChangeActiveReport;


        public RepController(NavBarControl bar, NavBarGroup barGroup, Control parentControl)
        {
            _bar = bar;
            _barGroup = barGroup;
            _parentControl = parentControl;
        }
        public void InitReportsList()
        {
            _reportsList = new List<IReportItem>();
            _reportActive = AddReport<RepDrivers>();
            AddReport<RepDriversAgregatsWorks>();
            AddReport<RepDriverVehicleCategory>();
            AddReport<RepVehiclesAgregats>();
            AddReport<RepVehiclesAgregatsWorks>();
            AddReport<RepFieldsProcessing>();
            AddReport<RepFieldsProcessingExt>();
            AddReport<RepFieldsProcessing3>();
            AddReport<RepFieldsProcessing4>();
            AddReport<RepFieldsJointProcessing>();
            AddReport<RepVehicleUnloading>();
            AddReport<RepSpeedControl>();
            foreach (IReportItem repOrder in _reportsList)
            {
                var item = new NavBarItem(repOrder.Caption) {LargeImage = repOrder.LargeImage, Tag = repOrder};
                item.LinkClicked += OnSelectReport;
                _barGroup.ItemLinks.Add(item);
                _bar.Items.Add(item);
            }
            //_reportActive = repDrivers;
            SelectReport(_reportActive);
        }

        private T AddReport<T>() where T : new()
        {
            var report = new T();
            ((IReportItem)report).ChangeStatus += SetProgress;
            _reportsList.Add((IReportItem)report);
            return report;
        }

        void OnSelectReport(object sender, NavBarLinkEventArgs e)
        {
            var item = (NavBarItem)sender;
            SelectReport((IReportItem)item.Tag);
        }

        private void SelectReport(IReportItem report)
        {
            _parentControl.Controls.Clear();
            report.ParentDataViewer = _parentControl;
            _parentControl.Controls.Add(report.DataViewer);
            foreach (NavBarItem item in _bar.Items)
            {
                if ((IReportItem)item.Tag == report)
                {
                    item.Appearance.Font = new Font(item.Appearance.Font.Name, item.Appearance.Font.Size, FontStyle.Bold);
                }
                else
                {
                    item.Appearance.Font = new Font(item.Appearance.Font.Name, item.Appearance.Font.Size, FontStyle.Regular);
                }
            }
            SetActiveReport(report);

        }

        public void RunReport(DateTime begin, DateTime end, BarEditItem pbStatus, BarStaticItem lbStatus)
        {
            DateTime beginConvert = TotUtilites.GetOrderDate(begin);
            DateTime endConvert = beginConvert.AddDays(1).AddMinutes(-1);
            _pbStatus = pbStatus;
            _lbStatus = lbStatus;
            _reportActive.Run(begin, end);
        }

        public bool IsReportHasData
        {
            get { return _reportActive.IsHasData; }
        }

        private void SetActiveReport(IReportItem report)
        {
            _reportActive = report;
            if (ChangeActiveReport != null) ChangeActiveReport(GetActiveReportCuption());
        }

        public void ExportToExcel()
        {
            _reportActive.ExportToExcel(); 
        }

        private string GetActiveReportCuption()
        {
            return _reportActive.Caption;
        }

        private void SetProgress(string sMessage,int iValue)
        {
            TotUtilites.SetProgressBar(_pbStatus, iValue);
            _lbStatus.Caption = sMessage;
        }

    }
}
