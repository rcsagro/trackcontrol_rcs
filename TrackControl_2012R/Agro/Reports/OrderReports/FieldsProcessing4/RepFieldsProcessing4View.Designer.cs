namespace Agro.Reports.OrderReports
{
    partial class RepFieldsProcessing4View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepFieldsProcessing4View));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDrvOutLinkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDevIdShort = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZonesOutLinkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCultureName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgrOutLinkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersentSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvgSpeedInField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFactSquareCalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDutInField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDutTransit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDutAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtInField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseDrtTransit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelDutAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.Size = new System.Drawing.Size(827, 470);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDateOrder,
            this.colDriverName,
            this.colDrvOutLinkId,
            this.colVehicleName,
            this.colDevIdShort,
            this.colFieldGroupName,
            this.colFieldName,
            this.colZonesOutLinkId,
            this.colCultureName,
            this.colAgregatName,
            this.colAgrOutLinkId,
            this.colWorkName,
            this.colFieldSquare,
            this.colWorkTime,
            this.colTimeStart,
            this.colTimeEnd,
            this.colPersentSquare,
            this.colAvgSpeedInField,
            this.colDistance,
            this.colRemark,
            this.colFactSquareCalc,
            this.colFuelExpenseDut,
            this.colFuelExpenseDutInField,
            this.colFuelExpenseDutTransit,
            this.colFuelExpenseDutAvgGa,
            this.colFuelExpenseDrt,
            this.colFuelExpenseDrtAvgGa,
            this.colFuelExpenseDrtInField,
            this.colFuelExpenseDrtTransit,
            this.colFuelDutAdd});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactSquareCalc", null, "����� ���������� {0} ��")});
            this.gvReport.IndicatorWidth = 30;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            this.gvReport.OptionsView.ShowFooter = true;
            this.gvReport.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvReport_CustomDrawRowIndicator);
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOrder.Caption = "����";
            this.colDateOrder.DisplayFormat.FormatString = "d";
            this.colDateOrder.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateOrder.FieldName = "Date";
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 0;
            this.colDateOrder.Width = 43;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.Caption = "��������";
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 1;
            this.colDriverName.Width = 56;
            // 
            // colDrvOutLinkId
            // 
            this.colDrvOutLinkId.AppearanceHeader.Options.UseTextOptions = true;
            this.colDrvOutLinkId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDrvOutLinkId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDrvOutLinkId.Caption = "ID";
            this.colDrvOutLinkId.FieldName = "DrvOutLinkId";
            this.colDrvOutLinkId.Name = "colDrvOutLinkId";
            this.colDrvOutLinkId.OptionsColumn.AllowEdit = false;
            this.colDrvOutLinkId.OptionsColumn.ReadOnly = true;
            this.colDrvOutLinkId.Visible = true;
            this.colDrvOutLinkId.VisibleIndex = 2;
            this.colDrvOutLinkId.Width = 20;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "�������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 3;
            this.colVehicleName.Width = 59;
            // 
            // colDevIdShort
            // 
            this.colDevIdShort.AppearanceHeader.Options.UseTextOptions = true;
            this.colDevIdShort.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDevIdShort.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDevIdShort.Caption = "ID";
            this.colDevIdShort.FieldName = "DevIdShort";
            this.colDevIdShort.Name = "colDevIdShort";
            this.colDevIdShort.OptionsColumn.AllowEdit = false;
            this.colDevIdShort.OptionsColumn.ReadOnly = true;
            this.colDevIdShort.Visible = true;
            this.colDevIdShort.VisibleIndex = 4;
            this.colDevIdShort.Width = 20;
            // 
            // colFieldGroupName
            // 
            this.colFieldGroupName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldGroupName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldGroupName.Caption = "������ �����";
            this.colFieldGroupName.FieldName = "FieldGroupName";
            this.colFieldGroupName.Name = "colFieldGroupName";
            this.colFieldGroupName.OptionsColumn.AllowEdit = false;
            this.colFieldGroupName.OptionsColumn.ReadOnly = true;
            this.colFieldGroupName.Visible = true;
            this.colFieldGroupName.VisibleIndex = 5;
            this.colFieldGroupName.Width = 46;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldName.Caption = "����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 6;
            this.colFieldName.Width = 40;
            // 
            // colZonesOutLinkId
            // 
            this.colZonesOutLinkId.AppearanceHeader.Options.UseTextOptions = true;
            this.colZonesOutLinkId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZonesOutLinkId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colZonesOutLinkId.Caption = "ID";
            this.colZonesOutLinkId.FieldName = "ZonesOutLinkId";
            this.colZonesOutLinkId.Name = "colZonesOutLinkId";
            this.colZonesOutLinkId.OptionsColumn.AllowEdit = false;
            this.colZonesOutLinkId.OptionsColumn.ReadOnly = true;
            this.colZonesOutLinkId.Visible = true;
            this.colZonesOutLinkId.VisibleIndex = 7;
            this.colZonesOutLinkId.Width = 20;
            // 
            // colCultureName
            // 
            this.colCultureName.AppearanceCell.Options.UseTextOptions = true;
            this.colCultureName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCultureName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCultureName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCultureName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCultureName.Caption = "��������";
            this.colCultureName.FieldName = "CultureName";
            this.colCultureName.Name = "colCultureName";
            this.colCultureName.OptionsColumn.AllowEdit = false;
            this.colCultureName.OptionsColumn.ReadOnly = true;
            this.colCultureName.Visible = true;
            this.colCultureName.VisibleIndex = 8;
            this.colCultureName.Width = 20;
            // 
            // colAgregatName
            // 
            this.colAgregatName.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregatName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregatName.Caption = "�������� ������������";
            this.colAgregatName.FieldName = "AgregatName";
            this.colAgregatName.Name = "colAgregatName";
            this.colAgregatName.OptionsColumn.AllowEdit = false;
            this.colAgregatName.OptionsColumn.ReadOnly = true;
            this.colAgregatName.Visible = true;
            this.colAgregatName.VisibleIndex = 9;
            this.colAgregatName.Width = 20;
            // 
            // colAgrOutLinkId
            // 
            this.colAgrOutLinkId.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgrOutLinkId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgrOutLinkId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgrOutLinkId.Caption = "ID";
            this.colAgrOutLinkId.FieldName = "AgrOutLinkId";
            this.colAgrOutLinkId.Name = "colAgrOutLinkId";
            this.colAgrOutLinkId.OptionsColumn.AllowEdit = false;
            this.colAgrOutLinkId.OptionsColumn.ReadOnly = true;
            this.colAgrOutLinkId.Visible = true;
            this.colAgrOutLinkId.VisibleIndex = 10;
            this.colAgrOutLinkId.Width = 20;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkName.Caption = "��� �����";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.VisibleIndex = 11;
            this.colWorkName.Width = 20;
            // 
            // colFieldSquare
            // 
            this.colFieldSquare.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldSquare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldSquare.Caption = "�������, ��";
            this.colFieldSquare.FieldName = "FieldSquare";
            this.colFieldSquare.Name = "colFieldSquare";
            this.colFieldSquare.OptionsColumn.AllowEdit = false;
            this.colFieldSquare.OptionsColumn.ReadOnly = true;
            this.colFieldSquare.Visible = true;
            this.colFieldSquare.VisibleIndex = 12;
            this.colFieldSquare.Width = 20;
            // 
            // colWorkTime
            // 
            this.colWorkTime.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkTime.Caption = "������������ �����, �";
            this.colWorkTime.FieldName = "FactTime";
            this.colWorkTime.Name = "colWorkTime";
            this.colWorkTime.OptionsColumn.AllowEdit = false;
            this.colWorkTime.OptionsColumn.ReadOnly = true;
            this.colWorkTime.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWorkTime.Visible = true;
            this.colWorkTime.VisibleIndex = 17;
            this.colWorkTime.Width = 50;
            // 
            // colTimeStart
            // 
            this.colTimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStart.Caption = "������ �����";
            this.colTimeStart.DisplayFormat.FormatString = "t";
            this.colTimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStart.FieldName = "TimeStart";
            this.colTimeStart.Name = "colTimeStart";
            this.colTimeStart.OptionsColumn.AllowEdit = false;
            this.colTimeStart.OptionsColumn.ReadOnly = true;
            this.colTimeStart.Visible = true;
            this.colTimeStart.VisibleIndex = 15;
            this.colTimeStart.Width = 47;
            // 
            // colTimeEnd
            // 
            this.colTimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEnd.Caption = "��������� �����";
            this.colTimeEnd.DisplayFormat.FormatString = "t";
            this.colTimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEnd.FieldName = "TimeEnd";
            this.colTimeEnd.Name = "colTimeEnd";
            this.colTimeEnd.OptionsColumn.AllowEdit = false;
            this.colTimeEnd.OptionsColumn.ReadOnly = true;
            this.colTimeEnd.Visible = true;
            this.colTimeEnd.VisibleIndex = 16;
            this.colTimeEnd.Width = 57;
            // 
            // colPersentSquare
            // 
            this.colPersentSquare.AppearanceCell.Options.UseTextOptions = true;
            this.colPersentSquare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPersentSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colPersentSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPersentSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPersentSquare.Caption = "% ������������ �������";
            this.colPersentSquare.FieldName = "PersentSquare";
            this.colPersentSquare.Name = "colPersentSquare";
            this.colPersentSquare.OptionsColumn.AllowEdit = false;
            this.colPersentSquare.OptionsColumn.ReadOnly = true;
            this.colPersentSquare.Visible = true;
            this.colPersentSquare.VisibleIndex = 14;
            this.colPersentSquare.Width = 64;
            // 
            // colAvgSpeedInField
            // 
            this.colAvgSpeedInField.AppearanceCell.Options.UseTextOptions = true;
            this.colAvgSpeedInField.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgSpeedInField.AppearanceHeader.Options.UseTextOptions = true;
            this.colAvgSpeedInField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAvgSpeedInField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAvgSpeedInField.Caption = "������� �������� � ����, ��/���";
            this.colAvgSpeedInField.FieldName = "AvgSpeedInField";
            this.colAvgSpeedInField.Name = "colAvgSpeedInField";
            this.colAvgSpeedInField.OptionsColumn.AllowEdit = false;
            this.colAvgSpeedInField.OptionsColumn.ReadOnly = true;
            this.colAvgSpeedInField.Visible = true;
            this.colAvgSpeedInField.VisibleIndex = 19;
            this.colAvgSpeedInField.Width = 56;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "��������, ��";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 18;
            this.colDistance.Width = 59;
            // 
            // colRemark
            // 
            this.colRemark.AppearanceCell.Options.UseTextOptions = true;
            this.colRemark.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRemark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRemark.Caption = "����������";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.OptionsColumn.AllowEdit = false;
            this.colRemark.OptionsColumn.ReadOnly = true;
            this.colRemark.Visible = true;
            this.colRemark.VisibleIndex = 29;
            this.colRemark.Width = 20;
            // 
            // colFactSquareCalc
            // 
            this.colFactSquareCalc.AppearanceCell.Options.UseTextOptions = true;
            this.colFactSquareCalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFactSquareCalc.Caption = "������������ ������� , ��";
            this.colFactSquareCalc.FieldName = "FactSquareCalc";
            this.colFactSquareCalc.Name = "colFactSquareCalc";
            this.colFactSquareCalc.OptionsColumn.AllowEdit = false;
            this.colFactSquareCalc.OptionsColumn.ReadOnly = true;
            this.colFactSquareCalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFactSquareCalc.Visible = true;
            this.colFactSquareCalc.VisibleIndex = 13;
            this.colFactSquareCalc.Width = 67;
            // 
            // colFuelExpenseDut
            // 
            this.colFuelExpenseDut.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDut.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDut.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDut.Caption = "������ ������� (���), �";
            this.colFuelExpenseDut.FieldName = "FuelExpenseDut";
            this.colFuelExpenseDut.Name = "colFuelExpenseDut";
            this.colFuelExpenseDut.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDut.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDut.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDut.Visible = true;
            this.colFuelExpenseDut.VisibleIndex = 20;
            this.colFuelExpenseDut.Width = 51;
            // 
            // colFuelExpenseDutInField
            // 
            this.colFuelExpenseDutInField.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDutInField.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutInField.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDutInField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutInField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDutInField.Caption = "������ ������� � ����� (���), �";
            this.colFuelExpenseDutInField.FieldName = "FuelExpenseDutInField";
            this.colFuelExpenseDutInField.Name = "colFuelExpenseDutInField";
            this.colFuelExpenseDutInField.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDutInField.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDutInField.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDutInField.Visible = true;
            this.colFuelExpenseDutInField.VisibleIndex = 22;
            this.colFuelExpenseDutInField.Width = 46;
            // 
            // colFuelExpenseDutTransit
            // 
            this.colFuelExpenseDutTransit.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDutTransit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutTransit.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDutTransit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutTransit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDutTransit.Caption = "������ ������� �� ��������� (���), �";
            this.colFuelExpenseDutTransit.FieldName = "FuelExpenseDutTransit";
            this.colFuelExpenseDutTransit.Name = "colFuelExpenseDutTransit";
            this.colFuelExpenseDutTransit.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDutTransit.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDutTransit.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDutTransit.Visible = true;
            this.colFuelExpenseDutTransit.VisibleIndex = 23;
            this.colFuelExpenseDutTransit.Width = 46;
            // 
            // colFuelExpenseDutAvgGa
            // 
            this.colFuelExpenseDutAvgGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDutAvgGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDutAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDutAvgGa.Caption = "������� ������ ������� (���), �/��";
            this.colFuelExpenseDutAvgGa.FieldName = "FuelExpenseDutAvgGa";
            this.colFuelExpenseDutAvgGa.Name = "colFuelExpenseDutAvgGa";
            this.colFuelExpenseDutAvgGa.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDutAvgGa.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDutAvgGa.Visible = true;
            this.colFuelExpenseDutAvgGa.VisibleIndex = 21;
            this.colFuelExpenseDutAvgGa.Width = 48;
            // 
            // colFuelExpenseDrt
            // 
            this.colFuelExpenseDrt.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrt.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrt.Caption = "������ ������� (���), �";
            this.colFuelExpenseDrt.FieldName = "FuelExpenseDrt";
            this.colFuelExpenseDrt.Name = "colFuelExpenseDrt";
            this.colFuelExpenseDrt.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrt.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDrt.Visible = true;
            this.colFuelExpenseDrt.VisibleIndex = 24;
            this.colFuelExpenseDrt.Width = 40;
            // 
            // colFuelExpenseDrtAvgGa
            // 
            this.colFuelExpenseDrtAvgGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtAvgGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtAvgGa.Caption = "������� ������ ������� (���), �/��";
            this.colFuelExpenseDrtAvgGa.FieldName = "FuelExpenseDrtAvgGa";
            this.colFuelExpenseDrtAvgGa.Name = "colFuelExpenseDrtAvgGa";
            this.colFuelExpenseDrtAvgGa.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtAvgGa.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtAvgGa.Visible = true;
            this.colFuelExpenseDrtAvgGa.VisibleIndex = 25;
            this.colFuelExpenseDrtAvgGa.Width = 32;
            // 
            // colFuelExpenseDrtInField
            // 
            this.colFuelExpenseDrtInField.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtInField.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtInField.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtInField.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtInField.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtInField.Caption = "������ ������� � ����� (���), �";
            this.colFuelExpenseDrtInField.FieldName = "FuelExpenseDrtInField";
            this.colFuelExpenseDrtInField.Name = "colFuelExpenseDrtInField";
            this.colFuelExpenseDrtInField.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtInField.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtInField.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDrtInField.Visible = true;
            this.colFuelExpenseDrtInField.VisibleIndex = 26;
            this.colFuelExpenseDrtInField.Width = 20;
            // 
            // colFuelExpenseDrtTransit
            // 
            this.colFuelExpenseDrtTransit.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTransit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtTransit.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseDrtTransit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseDrtTransit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseDrtTransit.Caption = "������ ������� �� ��������� (���), �";
            this.colFuelExpenseDrtTransit.FieldName = "FuelExpenseDrtTransit";
            this.colFuelExpenseDrtTransit.Name = "colFuelExpenseDrtTransit";
            this.colFuelExpenseDrtTransit.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseDrtTransit.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseDrtTransit.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpenseDrtTransit.Visible = true;
            this.colFuelExpenseDrtTransit.VisibleIndex = 27;
            this.colFuelExpenseDrtTransit.Width = 20;
            // 
            // colFuelDutAdd
            // 
            this.colFuelDutAdd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelDutAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelDutAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelDutAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelDutAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelDutAdd.Caption = "����������, �";
            this.colFuelDutAdd.FieldName = "FuelDutAdd";
            this.colFuelDutAdd.Name = "colFuelDutAdd";
            this.colFuelDutAdd.OptionsColumn.AllowEdit = false;
            this.colFuelDutAdd.OptionsColumn.ReadOnly = true;
            this.colFuelDutAdd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelDutAdd.Visible = true;
            this.colFuelDutAdd.VisibleIndex = 28;
            this.colFuelDutAdd.Width = 20;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepFieldsProcessing4View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Name = "RepFieldsProcessing4View";
            this.Size = new System.Drawing.Size(827, 470);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkName;
        public DevExpress.XtraGrid.GridControl gcReport;
        public DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldSquare;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTime;
        private DevExpress.XtraGrid.Columns.GridColumn colPersentSquare;
        private DevExpress.XtraGrid.Columns.GridColumn colAvgSpeedInField;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colFactSquareCalc;
        private DevExpress.XtraGrid.Columns.GridColumn colCultureName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDrvOutLinkId;
        private DevExpress.XtraGrid.Columns.GridColumn colDevIdShort;
        private DevExpress.XtraGrid.Columns.GridColumn colZonesOutLinkId;
        private DevExpress.XtraGrid.Columns.GridColumn colAgrOutLinkId;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDut;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDutInField;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDutTransit;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDutAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrt;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtInField;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseDrtTransit;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelDutAdd;
    }
}
