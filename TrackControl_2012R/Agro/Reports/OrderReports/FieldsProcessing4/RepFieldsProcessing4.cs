﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing4 : IReportItem 
    {

        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.FieldsProcessing4, Environment.NewLine);
        readonly Image _largeImage = Shared.FieldsProcessing;
        readonly Image _smallImage = Shared.FieldsProcessing;

        public RepFieldsProcessing4()
        {
            _dataViewer = new RepFieldsProcessing4View {Dock = DockStyle.Fill};
        }


        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dataSource = new RepFieldsProcessing4Data();
            dataSource.ChangeStatus += OnChangeStatus;
            ((RepFieldsProcessing4View)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end);
            ((RepFieldsProcessing4View)_dataViewer).Begin = begin;
            ((RepFieldsProcessing4View)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return ((RepFieldsProcessing4View)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepFieldsProcessing4View)_dataViewer).ExportToExcel();
        }

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }
        #endregion
    }
}
