﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing4Fields
    {
        public Int64 Id { get; set; }

        public DateTime Date { get; set; }
        
        public string DriverName { get; set; }

        public string DrvOutLinkId { get; set; }
        
        public string VehicleName { get; set; }

        public string DevIdShort { get; set; }

        public string AgregatName { get; set; }

        public string AgrOutLinkId { get; set; }

        public string WorkName { get; set; }
        
        public string FieldName { get; set; }

        public double FieldSquare { get; set; }

        public string ZonesOutLinkId { get; set; }

        public string FieldGroupName { get; set; }

        public string CultureName { get; set; }

        public double Distance { get; set; }

        public double DistanceInField { get; set; }

        public double FactSquareCalc { get; set; }

        public double PersentSquare { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public TimeSpan TimeMove { get; set; }

        public double FactTime { get; set; }

        public TimeSpan FactTimeSpan { get; set; }

        public TimeSpan FactTimeSpanInField { get; set; }

        public double AvgSpeedInField { get; set; }

        public double FuelDutAdd { get; set; }

        public double FuelExpenseDut { get; set; }

        public double FuelExpenseDutInField { get; set; }

        public double FuelExpenseDutTransit { get; set; }

        public double FuelExpenseDutAvgGa { get; set; }

        public double FuelExpenseDrt { get; set; }

        public double FuelExpenseDrtInField { get; set; }

        public double FuelExpenseDrtTransit { get; set; }

        public double FuelExpenseDrtAvgGa { get; set; }
    }

    public class TransitTempRecord
    {
        public double Distance { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public TimeSpan FactTimeSpan { get; set; }

        public double FuelExpenseDut { get; set; }

        public double FuelExpenseDrt { get; set; }

        public double FuelDutAdd { get; set; }
    }
}
