﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Agro;
using Agro.Controls;
using Agro.Properties;
using Agro.Utilites;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing4Data
    {
        public event Action<string, int> ChangeStatus;

        public RepFieldsProcessing4Data()
        {

        }

        public List<RepFieldsProcessing4Fields> GetData(DateTime begin, DateTime end)
        {
            //DataTable dataTable;
            var repRecords = new List<RepFieldsProcessing4Fields>();
            Dictionary<string, RepFieldsProcessing4Fields> dictRecords = null;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GetDataReader(begin, end, driverDb);
                RepFieldsProcessing4Fields record = null;
                TransitTempRecord recordTransit = null;
                var idOrder = 0;
                
                while (driverDb.Read())
                {
                    string identReader = string.Format("{0}_{1}_{2}_{3}", driverDb.GetInt32("Id_zone"), driverDb.GetInt32("Id_driver")
                        , driverDb.GetInt32("Id_work"), driverDb.GetInt32("Id_agregat"));
                    if (idOrder != driverDb.GetInt32("Id"))
                    {
                        if (idOrder>0) SetFinalSettings(record, recordTransit, dictRecords);
                        if (dictRecords != null) repRecords.AddRange(dictRecords.Values.ToList());
                        dictRecords = new Dictionary<string, RepFieldsProcessing4Fields>();
                        recordTransit = null;
                        record = null;
                        idOrder = driverDb.GetInt32("Id");
                        ChangeStatus(string.Format("Order# {0}", idOrder), Consts.PROGRESS_BAR_STOP);
                    }
                    if (driverDb.GetBoolean("Confirm"))
                    {
                        if (dictRecords != null && dictRecords.ContainsKey(identReader))
                        {
                            record = AddVariablePart(record, dictRecords, identReader, driverDb, recordTransit);
                            recordTransit = null;
                        }
                        else
                        {
                            record = AddConstPart(record, driverDb, recordTransit);
                            if (dictRecords != null) dictRecords.Add(identReader,record);
                            recordTransit = null;
                        }
                    }
                    else
                    {
                        recordTransit = SetTransitTempRecord(recordTransit, driverDb);
                    }
                }
                if (record != null) SetFinalSettings(record, recordTransit, dictRecords);
                if (dictRecords != null) repRecords.AddRange(dictRecords.Values.ToList());
            }
            ChangeStatus(Resources.Ready, Consts.PROGRESS_BAR_STOP);
            return repRecords;
        }

        private static void GetDataReader(DateTime begin, DateTime end, DriverDb driverDb)
        {
            string sql = string.Format(AgroQuery.RepFieldsProcessing4Data.SelectReportsData,
                                       AgroQuery.SqlVehicle_NumberPlateCarModel,
                                       driverDb.ParamPrefics, TrackControlQuery.SqlDriverIdent);

            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateEnd", end);
            driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
            //driverDb.GetDataReader(sql);
        }



        private static RepFieldsProcessing4Fields AddConstPart(RepFieldsProcessing4Fields record, DriverDb driverDb,
                                                       TransitTempRecord recordTransit)
        {
            record = new RepFieldsProcessing4Fields
            {
                Date = driverDb.GetDateTime("Date"),
                TimeStart = driverDb.GetDateTime("TimeStart"),
                TimeEnd = driverDb.GetDateTime("TimeEnd"),
                DriverName = TotUtilites.NdbNullReader(driverDb, "DriverName", "").ToString(),
                DrvOutLinkId = TotUtilites.NdbNullReader(driverDb, "DrvOutLinkId", "").ToString(),
                VehicleName = driverDb.GetString("VehicleName"),
                DevIdShort = driverDb.GetString("DevIdShort"),
                FieldGroupName = TotUtilites.NdbNullReader(driverDb, "FieldGroupName", "").ToString(),
                FieldName = TotUtilites.NdbNullReader(driverDb, "FieldName", "").ToString(),
                ZonesOutLinkId = TotUtilites.NdbNullReader(driverDb, "ZonesOutLinkId", "").ToString(),
                AgregatName = TotUtilites.NdbNullReader(driverDb, "AgregatName", "").ToString(),
                AgrOutLinkId = TotUtilites.NdbNullReader(driverDb, "AgrOutLinkId", "").ToString(),
                WorkName = TotUtilites.NdbNullReader(driverDb, "WorkName", "").ToString(),
                CultureName = TotUtilites.NdbNullReader(driverDb, "CultureName", "").ToString(),
                FieldSquare = driverDb.GetDouble("FieldSquare"),
                FactSquareCalc = driverDb.GetDouble("FactSquareCalc"),
                FactTimeSpan = driverDb.GetTimeSpan("FactTime"),
                TimeMove = driverDb.GetTimeSpan("TimeMove"),
                Distance = 0,// driverDb.GetDouble("Distance"),
                DistanceInField = driverDb.GetDouble("Distance"),
                FactTimeSpanInField = driverDb.GetTimeSpan("FactTime"),
                FuelExpenseDut = driverDb.GetDouble("FuelExpens"),
                FuelExpenseDutInField = driverDb.GetDouble("FuelExpens"),
                FuelExpenseDrt = driverDb.GetDouble("Fuel_ExpensTotal"),
                FuelExpenseDrtInField = driverDb.GetDouble("Fuel_ExpensTotal"),
                FuelDutAdd = driverDb.GetDouble("FuelAdd")
            };
            if (recordTransit != null)
            {
                record.Distance += recordTransit.Distance;
                record.FuelExpenseDut += recordTransit.FuelExpenseDut;
                record.FuelExpenseDrt += recordTransit.FuelExpenseDrt;
                record.FuelDutAdd += recordTransit.FuelDutAdd;
                record.FactTimeSpan += recordTransit.FactTimeSpan;
                //record.TimeStart = recordTransit.TimeStart;
            }
            return record;
        }

        private static RepFieldsProcessing4Fields AddVariablePart(RepFieldsProcessing4Fields record, Dictionary<string, RepFieldsProcessing4Fields> dictRecords,
                                                                  string identReader, DriverDb driverDb, TransitTempRecord recordTransit)
        {
            if (record == null) throw new ArgumentNullException("record");
            record = dictRecords[identReader];
            //record.Distance += driverDb.GetDouble("Distance");
            record.FactTimeSpan += driverDb.GetTimeSpan("FactTime");
            record.TimeMove += driverDb.GetTimeSpan("TimeMove");
            record.FactSquareCalc += driverDb.GetDouble("FactSquareCalc");
            record.TimeEnd = driverDb.GetDateTime("TimeEnd");
            record.FactTimeSpanInField += driverDb.GetTimeSpan("FactTime");
            record.DistanceInField += driverDb.GetDouble("Distance");
            record.FuelExpenseDut += driverDb.GetDouble("FuelExpens");
            record.FuelExpenseDutInField += driverDb.GetDouble("FuelExpens");
            record.FuelExpenseDrt += driverDb.GetDouble("Fuel_ExpensTotal");
            record.FuelExpenseDrtInField += driverDb.GetDouble("Fuel_ExpensTotal");
            record.FuelDutAdd += driverDb.GetDouble("FuelAdd");
            if (recordTransit != null)
            {
                record.Distance += recordTransit.Distance;
                record.FuelExpenseDut += recordTransit.FuelExpenseDut;
                record.FuelExpenseDrt += recordTransit.FuelExpenseDrt;
                record.FuelDutAdd += recordTransit.FuelDutAdd;
                record.FactTimeSpan += recordTransit.FactTimeSpan;
            }
            return record;
        }



        private static void SetFinalSettings(RepFieldsProcessing4Fields record, TransitTempRecord recordTransit, Dictionary<string, RepFieldsProcessing4Fields> dictRecords)
        {
            if (record != null & recordTransit != null)
            {
                record.Distance += recordTransit.Distance;
                record.FuelExpenseDut += recordTransit.FuelExpenseDut;
                record.FuelExpenseDrt += recordTransit.FuelExpenseDrt;
                record.FuelDutAdd += recordTransit.FuelDutAdd;
                record.FactTimeSpan += recordTransit.FactTimeSpan;
                //record.TimeEnd = recordTransit.TimeEnd;
            }

            if (dictRecords != null)
                foreach (var repRecord in dictRecords.Values)
                {
                    repRecord.FactTime = Math.Round(repRecord.FactTimeSpan.TotalHours, 2);
                    if ((repRecord.TimeMove.Hours + repRecord.TimeMove.Minutes / 60.0) > 0)
                        repRecord.AvgSpeedInField =
                            Math.Round(
                                repRecord.DistanceInField/(repRecord.TimeMove.Hours + repRecord.TimeMove.Minutes/60.0), 2);
                    repRecord.Distance = Math.Round(repRecord.Distance, 2);
                    repRecord.FuelExpenseDut = Math.Round(repRecord.FuelExpenseDut, 2);
                    repRecord.FuelExpenseDutInField = Math.Round(repRecord.FuelExpenseDutInField, 2);
                    repRecord.FuelExpenseDutTransit = Math.Round(repRecord.FuelExpenseDut - repRecord.FuelExpenseDutInField, 2);
                    repRecord.FuelExpenseDrt = Math.Round(repRecord.FuelExpenseDrt, 2);
                    repRecord.FuelExpenseDrtInField = Math.Round(repRecord.FuelExpenseDrtInField, 2);
                    repRecord.FuelExpenseDrtTransit = Math.Round(repRecord.FuelExpenseDrt - repRecord.FuelExpenseDrtInField, 2);
                    repRecord.FuelDutAdd = Math.Round(repRecord.FuelDutAdd, 2);
                    if (repRecord.FieldSquare > 0)
                    {
                        repRecord.PersentSquare = 100 * Math.Round(repRecord.FactSquareCalc / repRecord.FieldSquare, 2);
                        repRecord.FuelExpenseDutAvgGa = Math.Round(repRecord.FuelExpenseDut / repRecord.FieldSquare, 2);
                        repRecord.FuelExpenseDrtAvgGa = Math.Round(repRecord.FuelExpenseDrt / repRecord.FieldSquare, 2);
                    }
                }
        }
        private static TransitTempRecord SetTransitTempRecord(TransitTempRecord recordTransit, DriverDb driverDb)
        {
            if (recordTransit == null)
            {
                recordTransit = new TransitTempRecord
                {
                    Distance = driverDb.GetDouble("Distance"),
                    FuelExpenseDut = driverDb.GetDouble("FuelExpens"),
                    FuelExpenseDrt = driverDb.GetDouble("Fuel_ExpensTotal"),
                    FuelDutAdd = driverDb.GetDouble("FuelAdd"),
                    //TimeStart = driverDb.GetDateTime("TimeStart"),
                    //TimeEnd = driverDb.GetDateTime("TimeEnd"),
                    FactTimeSpan = driverDb.GetTimeSpan("FactTime")
                };
            }
            else
            {
                recordTransit.Distance += driverDb.GetDouble("Distance");
                recordTransit.FuelExpenseDut += driverDb.GetDouble("FuelExpens");
                recordTransit.FuelExpenseDrt += driverDb.GetDouble("Fuel_ExpensTotal");
                recordTransit.FuelDutAdd += driverDb.GetDouble("FuelAdd");
                recordTransit.FactTimeSpan += driverDb.GetTimeSpan("FactTime");
                //recordTransit.TimeEnd = driverDb.GetDateTime("TimeEnd");
            }
            return recordTransit;
        }
    }
}
