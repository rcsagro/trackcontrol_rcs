using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using Agro.Properties;

namespace Agro.Reports.OrderReports
{
    public partial class RepFieldsProcessing4View : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }
        
        public RepFieldsProcessing4View()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            colDateOrder.Caption = Resources.Date;
            colDriverName.Caption = Resources.Driver;
            colVehicleName.Caption = Resources.Vehicle;
            colFieldGroupName.Caption = Resources.FieldsGroups;
            colFieldName.Caption = Resources.Field;
            colCultureName.Caption = Resources.Cultura;
            colAgregatName.Caption = Resources.Agregat;
            colWorkName.Caption = Resources.WorkType;
            colFieldSquare.Caption = Resources.SquareGA;
            colWorkTime.Caption = Resources.TimeWorksH;
            colTimeStart.Caption = Resources.StartShift;
            colTimeEnd.Caption = Resources.EndShift;
            colAvgSpeedInField.Caption = Resources.AverageSpeedInField;
            colDistance.Caption = Resources.MoveKm;
            colRemark.Caption = Resources.Remark;
            colFactSquareCalc.Caption = Resources.SquareProcessGa;
            colPersentSquare.Caption = Resources.PersentSquare;
            colFuelExpenseDut.Caption = Resources.FuelExpensDUT;
            colFuelExpenseDutInField.Caption = Resources.FuelExpensFieldsDUT;
            colFuelExpenseDutTransit.Caption = Resources.FuelConsumptionCrossingsDUT;
            colFuelExpenseDutAvgGa.Caption = Resources.FuelAvgDutLGa;
            colFuelExpenseDrt.Caption = Resources.FuelExpensDRT;
            colFuelExpenseDrtAvgGa.Caption = Resources.FuelAvgDrtLGa;
            colFuelExpenseDrtInField.Caption = Resources.FuelExpensFieldsDRT;
            colFuelExpenseDrtTransit.Caption = Resources.FuelConsumptionCrossingsDRT;
            colFuelDutAdd.Caption = Resources.FuelAddL;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.FieldsProcessing4;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }

        private void gvReport_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
    }
}
