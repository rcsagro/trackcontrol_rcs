namespace Agro.Reports.OrderReports
{
    partial class RepVehiclesUnloadingView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepVehiclesUnloadingView));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEventName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleInfo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriversRfid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldsGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldsAreaGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldsProcAreaGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStartEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem();
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bleVehiclesGroups = new DevExpress.XtraBars.BarEditItem();
            this.leVehiclesGroups = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleVehicles = new DevExpress.XtraBars.BarEditItem();
            this.leVehicles = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bbiFilterClear = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehiclesGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicles)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 24);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teTime});
            this.gcReport.Size = new System.Drawing.Size(891, 470);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEventName,
            this.colVehicleInfo,
            this.colDriversRfid,
            this.colFieldsGroupName,
            this.colFieldName,
            this.colFieldsAreaGa,
            this.colFieldsProcAreaGa,
            this.colDateEvent,
            this.colTimeStartEvent,
            this.colTimeDuration});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colEventName
            // 
            this.colEventName.AppearanceCell.Options.UseTextOptions = true;
            this.colEventName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colEventName.AppearanceHeader.Options.UseTextOptions = true;
            this.colEventName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEventName.Caption = "�������";
            this.colEventName.FieldName = "EventName";
            this.colEventName.Name = "colEventName";
            this.colEventName.OptionsColumn.AllowEdit = false;
            this.colEventName.OptionsColumn.ReadOnly = true;
            this.colEventName.Visible = true;
            this.colEventName.VisibleIndex = 0;
            this.colEventName.Width = 87;
            // 
            // colVehicleInfo
            // 
            this.colVehicleInfo.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleInfo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleInfo.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleInfo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleInfo.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleInfo.Caption = "�������";
            this.colVehicleInfo.FieldName = "VehicleInfo";
            this.colVehicleInfo.Name = "colVehicleInfo";
            this.colVehicleInfo.OptionsColumn.AllowEdit = false;
            this.colVehicleInfo.OptionsColumn.ReadOnly = true;
            this.colVehicleInfo.Visible = true;
            this.colVehicleInfo.VisibleIndex = 1;
            this.colVehicleInfo.Width = 185;
            // 
            // colDriversRfid
            // 
            this.colDriversRfid.AppearanceCell.Options.UseTextOptions = true;
            this.colDriversRfid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriversRfid.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriversRfid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriversRfid.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriversRfid.Caption = "Rfid";
            this.colDriversRfid.FieldName = "DriversRfid";
            this.colDriversRfid.Name = "colDriversRfid";
            this.colDriversRfid.OptionsColumn.AllowEdit = false;
            this.colDriversRfid.OptionsColumn.ReadOnly = true;
            this.colDriversRfid.Visible = true;
            this.colDriversRfid.VisibleIndex = 2;
            this.colDriversRfid.Width = 66;
            // 
            // colFieldsGroupName
            // 
            this.colFieldsGroupName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldsGroupName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldsGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldsGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldsGroupName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldsGroupName.Caption = "������ �����";
            this.colFieldsGroupName.FieldName = "FieldsGroupName";
            this.colFieldsGroupName.Name = "colFieldsGroupName";
            this.colFieldsGroupName.OptionsColumn.AllowEdit = false;
            this.colFieldsGroupName.OptionsColumn.ReadOnly = true;
            this.colFieldsGroupName.Visible = true;
            this.colFieldsGroupName.VisibleIndex = 3;
            this.colFieldsGroupName.Width = 165;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 4;
            this.colFieldName.Width = 141;
            // 
            // colFieldsAreaGa
            // 
            this.colFieldsAreaGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldsAreaGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldsAreaGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldsAreaGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldsAreaGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFieldsAreaGa.Caption = "�������, ��";
            this.colFieldsAreaGa.FieldName = "FieldsAreaGa";
            this.colFieldsAreaGa.Name = "colFieldsAreaGa";
            this.colFieldsAreaGa.OptionsColumn.AllowEdit = false;
            this.colFieldsAreaGa.OptionsColumn.ReadOnly = true;
            this.colFieldsAreaGa.Visible = true;
            this.colFieldsAreaGa.VisibleIndex = 5;
            this.colFieldsAreaGa.Width = 98;
            // 
            // colFieldsProcAreaGa
            // 
            this.colFieldsProcAreaGa.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldsProcAreaGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldsProcAreaGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldsProcAreaGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldsProcAreaGa.Caption = "������, ��";
            this.colFieldsProcAreaGa.FieldName = "FieldsProcAreaGa";
            this.colFieldsProcAreaGa.Name = "colFieldsProcAreaGa";
            this.colFieldsProcAreaGa.OptionsColumn.AllowEdit = false;
            this.colFieldsProcAreaGa.OptionsColumn.ReadOnly = true;
            this.colFieldsProcAreaGa.Visible = true;
            this.colFieldsProcAreaGa.VisibleIndex = 6;
            this.colFieldsProcAreaGa.Width = 159;
            // 
            // colDateEvent
            // 
            this.colDateEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.Caption = "����";
            this.colDateEvent.DisplayFormat.FormatString = "d";
            this.colDateEvent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateEvent.FieldName = "DateEvent";
            this.colDateEvent.Name = "colDateEvent";
            this.colDateEvent.OptionsColumn.AllowEdit = false;
            this.colDateEvent.OptionsColumn.ReadOnly = true;
            this.colDateEvent.Visible = true;
            this.colDateEvent.VisibleIndex = 7;
            this.colDateEvent.Width = 92;
            // 
            // colTimeStartEvent
            // 
            this.colTimeStartEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStartEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStartEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStartEvent.Caption = "�����";
            this.colTimeStartEvent.DisplayFormat.FormatString = "t";
            this.colTimeStartEvent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStartEvent.FieldName = "DateEvent";
            this.colTimeStartEvent.Name = "colTimeStartEvent";
            this.colTimeStartEvent.OptionsColumn.AllowEdit = false;
            this.colTimeStartEvent.OptionsColumn.ReadOnly = true;
            this.colTimeStartEvent.Visible = true;
            this.colTimeStartEvent.VisibleIndex = 8;
            this.colTimeStartEvent.Width = 73;
            // 
            // colTimeDuration
            // 
            this.colTimeDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.Caption = "������������";
            this.colTimeDuration.DisplayFormat.FormatString = "t";
            this.colTimeDuration.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeDuration.FieldName = "TimeDuration";
            this.colTimeDuration.Name = "colTimeDuration";
            this.colTimeDuration.OptionsColumn.AllowEdit = false;
            this.colTimeDuration.OptionsColumn.ReadOnly = true;
            this.colTimeDuration.Visible = true;
            this.colTimeDuration.VisibleIndex = 9;
            this.colTimeDuration.Width = 86;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.teTime.Name = "teTime";
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bleVehiclesGroups,
            this.bleVehicles,
            this.bbiFilterClear});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leVehiclesGroups,
            this.leVehicles});
            // 
            // bar2
            // 
            this.bar2.BarName = "������� ����";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleVehiclesGroups, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleVehicles, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFilterClear)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "������� ����";
            // 
            // bleVehiclesGroups
            // 
            this.bleVehiclesGroups.Caption = "������ �������";
            this.bleVehiclesGroups.Edit = this.leVehiclesGroups;
            this.bleVehiclesGroups.Id = 0;
            this.bleVehiclesGroups.Name = "bleVehiclesGroups";
            this.bleVehiclesGroups.Width = 218;
            this.bleVehiclesGroups.EditValueChanged += new System.EventHandler(this.bleVehiclesGroups_EditValueChanged);
            // 
            // leVehiclesGroups
            // 
            this.leVehiclesGroups.AutoHeight = false;
            this.leVehiclesGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leVehiclesGroups.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name")});
            this.leVehiclesGroups.DisplayMember = "Name";
            this.leVehiclesGroups.DropDownRows = 20;
            this.leVehiclesGroups.Name = "leVehiclesGroups";
            this.leVehiclesGroups.NullText = "";
            this.leVehiclesGroups.ShowHeader = false;
            this.leVehiclesGroups.ValueMember = "Id";
            // 
            // bleVehicles
            // 
            this.bleVehicles.Caption = "�������";
            this.bleVehicles.Edit = this.leVehicles;
            this.bleVehicles.Id = 1;
            this.bleVehicles.Name = "bleVehicles";
            this.bleVehicles.Width = 236;
            // 
            // leVehicles
            // 
            this.leVehicles.AutoHeight = false;
            this.leVehicles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leVehicles.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info")});
            this.leVehicles.DisplayMember = "Info";
            this.leVehicles.DropDownRows = 30;
            this.leVehicles.Name = "leVehicles";
            this.leVehicles.NullText = "";
            this.leVehicles.ShowHeader = false;
            this.leVehicles.ValueMember = "Id";
            // 
            // bbiFilterClear
            // 
            this.bbiFilterClear.Caption = "�������� ������";
            this.bbiFilterClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFilterClear.Glyph")));
            this.bbiFilterClear.Id = 2;
            this.bbiFilterClear.Name = "bbiFilterClear";
            this.bbiFilterClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFilterClear_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(891, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(891, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 470);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(891, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 470);
            // 
            // RepVehiclesUnloadingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "RepVehiclesUnloadingView";
            this.Size = new System.Drawing.Size(891, 494);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehiclesGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leVehicles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gcReport;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleInfo;
        private DevExpress.XtraGrid.Columns.GridColumn colDriversRfid;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldsGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldsAreaGa;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEventName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldsProcAreaGa;
        private DevExpress.XtraGrid.Columns.GridColumn colDateEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStartEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeDuration;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem bleVehiclesGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leVehiclesGroups;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarEditItem bleVehicles;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leVehicles;
        private DevExpress.XtraBars.BarButtonItem bbiFilterClear;
    }
}
