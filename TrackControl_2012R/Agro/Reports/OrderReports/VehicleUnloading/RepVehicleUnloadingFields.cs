﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public class RepVehicleUnloadingFields
    {

        public string EventName { get; set; }
        
        public string VehicleInfo { get; set; }

        public string DriversRfid { get; set; }

        public string FieldsGroupName { get; set; }

        public string FieldName { get; set; }

        public double FieldsAreaGa { get; set; }

        public double FieldsProcAreaGa { get; set; }

        public DateTime DateEvent { get; set; }

        public TimeSpan TimeDuration { get; set; }

    }
}
