﻿using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using BaseReports.Procedure;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.Vehicles; 


namespace Agro.Reports.OrderReports
{
    public class RepVehicleUnloadingData
    {
        private delegate IList<GpsData> GetDataGps(int mobitelId, DateTime begin, DateTime end);

        public event Action<string, int> ChangeStatus;

        // последняя запись наряда, площадь которой попала в отчет
        private int _lastOrderTid;

        public IList<RepVehicleUnloadingFields> GetData(DateTime begin, DateTime end, int teamId, int vehicleId)
        {
            var fields = new List<RepVehicleUnloadingFields>();

            List<Vehicle> vehiclesWithUnloadSensor = SetVehiclesWithUnloadSensor(teamId, vehicleId);
            if (vehiclesWithUnloadSensor.Count == 0) return null;
            ChangeStatus(Resources.DataSelect, vehiclesWithUnloadSensor.Count);
            //обработка двумя потоками.Один выбирает данные (EnvokenGetData), второй (AnalizData) их обрабатывает 
            var getDg = new GetDataGps(EnvokenGetData /* это функция потока выборки данных */);
            IAsyncResult iftAr = null;
            iftAr = getDg.BeginInvoke(vehiclesWithUnloadSensor[0].MobitelId, begin, end, null, null);
            IList<GpsData> gpsDatas;
            for (int i = 1; i < vehiclesWithUnloadSensor.Count(); i++)
            {
                ChangeStatus(vehiclesWithUnloadSensor[i - 1].Info, i-1);
                while (!iftAr.IsCompleted)
                {
                    Application.DoEvents();
                }
                gpsDatas = getDg.EndInvoke(iftAr);
                iftAr = getDg.BeginInvoke(vehiclesWithUnloadSensor[i].MobitelId, begin, end, null, null);
                if (gpsDatas != null && gpsDatas.Count > 0)
                {
                    var vehFields = AnalizData(vehiclesWithUnloadSensor[i-1], gpsDatas);
                    if (vehFields != null && vehFields.Count >0) fields.AddRange(vehFields);
                }
            }
            gpsDatas = getDg.EndInvoke(iftAr);
            if (gpsDatas!=null && gpsDatas.Count > 0)
            {
                ChangeStatus(vehiclesWithUnloadSensor[vehiclesWithUnloadSensor.Count() - 1].Info, vehiclesWithUnloadSensor.Count);
                var vehFields = AnalizData(vehiclesWithUnloadSensor[vehiclesWithUnloadSensor.Count() - 1], gpsDatas);
                if (vehFields != null && vehFields.Count > 0) fields.AddRange(vehFields);
            }
            ChangeStatus(Resources.Ready, Consts.PROGRESS_BAR_STOP);
            return fields;}

        private List<Vehicle> SetVehiclesWithUnloadSensor(int teamId, int vehicleId)
        {
            List<Vehicle> vehiclesWithUnloadSensor = DocItem.VehiclesModel.Vehicles.Where(veh => veh.LogicSensorUpload != null).ToList();

            if (vehicleId > 0)
            {
                vehiclesWithUnloadSensor = vehiclesWithUnloadSensor.Where(veh => veh.Id == vehicleId).ToList();
            }
            else if (teamId > 0)
            {
                vehiclesWithUnloadSensor = vehiclesWithUnloadSensor.Where(veh => veh.Group.Id == teamId).ToList();
            }
            if (vehiclesWithUnloadSensor.Count == 0)
                XtraMessageBox.Show(Resources.NoTransportSensorHopper, Resources.ApplicationName); 
            return vehiclesWithUnloadSensor;
        }

        /// <summary>
        /// получение данных в другом потоке
        /// </summary>
        private IList<GpsData> EnvokenGetData(int mobitelId, DateTime begin, DateTime end)
        {
                return GpsDataProvider.GetGpsDataAllProtocols(mobitelId, begin, end);
        }

        /// <summary>
        /// анализ полученных EnvokenGetData данных
        /// </summary>
        private IList<RepVehicleUnloadingFields> AnalizData(Vehicle vehicle, IList<GpsData> gpsDatas)
        {
            _lastOrderTid = 0;
            var vehicleFields = new List<RepVehicleUnloadingFields>();
            bool active = false;
            TimeSpan duration = TimeSpan.Zero;
            DateTime prevTime = DateTime.MinValue;
            var firstPoint = new GpsData();
            var sensor = new Sensor((int)AlgorithmType.DRIVER, vehicle.MobitelId);
            foreach (GpsData gpsData in gpsDatas)
            {
                if (vehicle.LogicSensorUpload.IsActive(gpsData.Sensors))
                    //if (vehicle.LogicSensorUpload.IsActiveInTime( gpsData.Time))
                {
                    if (active)
                    {
                        duration += gpsData.Time.Subtract(prevTime);
                    }
                    else
                    {
                        // начало выгрузки
                        active = true;
                        firstPoint = gpsData;
                    }
                }
                else
                {
                    // завершение выгрузки
                    if (active & !IsBounceExist(vehicle, gpsDatas, gpsData))
                    {
                        active = false;
                        duration += gpsData.Time.Subtract(prevTime);
                        var record = GetNewRecord(vehicle, gpsDatas, firstPoint, sensor, gpsData, duration);
                        duration = TimeSpan.Zero;
                        if (record != null)
                            vehicleFields.Add(record);
                    }
                }
                prevTime = gpsData.Time;
            }
            if (active){
                var record = GetNewRecord(vehicle, gpsDatas, firstPoint, sensor, gpsDatas[gpsDatas.Count - 1], duration);
                if (record != null) 
                    vehicleFields.Add(record);
            }

            return vehicleFields;
        }

        /// <summary>
        /// наличие дребезга
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="gpsDatas"></param>
        /// <param name="gpsData"></param>
        /// <returns></returns>
        bool IsBounceExist(Vehicle vehicle,IList<GpsData> gpsDatas, GpsData gpsData)
        {
            var suprBounce = gpsDatas.FirstOrDefault(gps => gps.Time > gpsData.Time);// антидребезг в одну точку
            if (suprBounce == null || !vehicle.LogicSensorUpload.IsActive(suprBounce.Sensors))
                return false;
            else
                return true;

        }

        private RepVehicleUnloadingFields GetNewRecord(Vehicle vehicle, IList<GpsData> gpsDatas, GpsData firstPoint, Sensor sensor,
                                                       GpsData gpsData, TimeSpan duration)
        {
            if (duration == TimeSpan.Zero) return null;
            var rfid = GetRfid(gpsDatas, firstPoint, sensor, gpsData);
            var field = GetField(gpsDatas, firstPoint);
            double area = 0;
            double areaField = 0;
            string fieldName = "";
            string fielGroupdName = "";
            if (field != null)
            {
                fieldName = field.Name;
                fielGroupdName = field.Group.Name;
                areaField = field.AreaGa;
                area = GetFieldProcAreaGa(firstPoint, vehicle, field);
            }
            var record = new RepVehicleUnloadingFields
                {
                    EventName = sensor.Name ,// Resources.UnloadingHopper,
                    DateEvent = firstPoint.Time,
                    TimeDuration = duration,
                    VehicleInfo = vehicle.Info,
                    FieldName = fieldName,
                    FieldsGroupName = fielGroupdName,
                    FieldsAreaGa = areaField,
                    FieldsProcAreaGa = area,
                    DriversRfid = rfid
                };
            return record;
        }

        private string GetRfid(IList<GpsData> gpsDatas, GpsData firstPoint, Sensor sensor, GpsData gpsData)
        {
            string rfid = Resources.RfidDisable;
            if (sensor!= null && sensor.Id > 0)
            {
                var gpsDatasToFindRfid =
                    gpsDatas.Where(
                        gps =>
                        gps.Time <= gpsData.Time &&
                        gps.Time >= firstPoint.Time.Subtract(new TimeSpan(0, 0, 10)))
                            .OrderByDescending(gps => gps.Time)
                            .ToList();

                rfid = GetRfidName(sensor, gpsDatasToFindRfid);
            }
            return rfid;
        }

        private DictionaryAgroField GetField(IList<GpsData> gpsDatas, GpsData firstPoint)
        {
            var gpsDatasToFindZone =
                gpsDatas.Where(
                    gps =>
                    gps.Time <= firstPoint.Time &&
                    gps.Time >= firstPoint.Time.Subtract(new TimeSpan(0, 10, 0))).OrderByDescending(gps => gps.Time).ToList();
            return GetFieldFromZone(gpsDatasToFindZone);
        }

        DictionaryAgroField GetFieldFromZone(IEnumerable<GpsData> gpsDatas)
        {
            DictionaryAgroField fieldDict = null;
            foreach (GpsData gpsData in gpsDatas)
            {
                var zones = Algorithm.ZonesLocator.GetZonesWithPoint(gpsData.LatLng);
                if (zones != null)
                {
                    foreach (IZone zone in zones)
                    {
                        if (zone.AreaGa > 0)
                        {
                            using (var field = new DictionaryAgroField())
                            {
                                int idField = field.GetIdFromZoneId(zone.Id);
                                if (idField > 0)
                                {
                                    field.GetProperties(idField);
                                    fieldDict = field;
                                }
                            }
                            if (fieldDict != null) return fieldDict;
                        }
                    }
                }
                
            }
            return fieldDict;
        }

        string GetRfidName(Sensor sensor,IEnumerable<GpsData> gpsDatas)
        {
            ushort rfid = 0;
            foreach (GpsData gpsData in gpsDatas)
             {
                 rfid = (ushort) sensor.GetValue(gpsData.Sensors);
                 if (rfid > 0 && rfid < sensor.GetMaxRfidValue())
                 {
                    var driver = new Driver(0, rfid);
                    if (driver.Id > 0)
                        return String.Format("{0} ({1})", driver.DriverFullName, rfid);
                    else
                        return rfid.ToString();
                 }
             }
        if (rfid == sensor.GetMaxRfidValue())
            return Resources.RfidNoCard;
        else
            return Resources.RfidDisable;
        }

        double GetFieldProcAreaGa(GpsData gpsData, Vehicle vehicle,DictionaryAgroField field)
        {
            if (field == null || vehicle == null) return 0; 
            string sql = "";
            double area = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

            // если событие первое берем записи наряда от его создания
                if (_lastOrderTid == 0)
            {

                DateTime dateOrderInit = TotUtilites.GetOrderDate(gpsData.Time);
                if (dateOrderInit > gpsData.Time) dateOrderInit = dateOrderInit.AddDays(-1);
                sql = string.Format(AgroQuery.OrderReports.GetFieldProcAreaGaDate, driverDb.ParamPrefics,
                                               field.Id, vehicle.MobitelId);
                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DB", dateOrderInit);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DE", gpsData.Time);
                }
                // для последующих  - от предыдущей записи наряда
            else
            {
                sql = string.Format(AgroQuery.OrderReports.GetFieldProcAreaGaId, driverDb.ParamPrefics, _lastOrderTid,
                                                   field.Id, vehicle.MobitelId);
                    driverDb.NewSqlParameterArray(1);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DE", gpsData.Time);

                }
            driverDb.GetDataReader(sql , driverDb.GetSqlParameterArray);    
            if (driverDb.Read())
            {
                _lastOrderTid = (int)TotUtilites.NdbNullReader(driverDb, "MaxOrderTid", 0);
                area= (double)TotUtilites.NdbNullReader(driverDb, "AreaSum", 0.0);
            }
                driverDb.CloseDataReader();
                driverDb.CloseDbConnection();
            }

            return area;
        }
    }
}
