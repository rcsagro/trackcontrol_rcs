using System;
using Agro.Properties;
using TrackControl.General;
using TrackControl.Vehicles;

namespace Agro.Reports.OrderReports
{
    public partial class RepVehiclesUnloadingView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public int VehicleGroup
        {
            get
            {
                if (bleVehiclesGroups.EditValue == null)
                    return 0;
                else
                    return (int)bleVehiclesGroups.EditValue;
            }
        }

        public int Vehicle
        {
            get
            {
                if (bleVehicles.EditValue == null)
                    return 0;
                else
                    return (int)bleVehicles.EditValue;
            }
        }

        public RepVehiclesUnloadingView()
        {
            InitializeComponent();
            leVehiclesGroups.DataSource = DocItem.VehiclesModel.RootGroupsWithItems();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }
        void Localization()
        {
            colEventName.Caption = Resources.Event;
            colVehicleInfo.Caption = Resources.Vehicle;
            colDriversRfid.Caption = @"Rfid";
            colFieldsGroupName.Caption = Resources.FieldsGroup;
            colFieldName.Caption = Resources.Field;
            colFieldsAreaGa.Caption = Resources.SquareGA;
            colFieldsProcAreaGa.Caption = Resources.SquareContourGa;
            colDateEvent.Caption = Resources.Date;
            colTimeStartEvent.Caption = Resources.Time;
            colTimeDuration.Caption = Resources.Duration;
            bleVehiclesGroups.Caption = Resources.CarGroupe;
            bleVehicles.Caption = Resources.Car;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.VehicleAggregateRow1;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }

        private void bbiFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleVehiclesGroups.EditValue = null;
            bleVehicles.EditValue = null; 
        }

        private void bleVehiclesGroups_EditValueChanged(object sender, EventArgs e)
        {
            var groupe = (VehiclesGroup)leVehiclesGroups.GetDataSourceRowByKeyValue(bleVehiclesGroups.EditValue);
            leVehicles.DataSource = groupe == null ? null : groupe.OwnRealItems();
        }
    }
}
