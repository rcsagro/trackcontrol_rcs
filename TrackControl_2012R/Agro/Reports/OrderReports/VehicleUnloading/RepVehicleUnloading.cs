﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepVehicleUnloading : IReportItem 
    {
        readonly UserControl _dataViewer;
        public event Action<string, int> ChangeStatus;
        private readonly string _caption = Resources.ReportCombines;
        readonly Image _largeImage = Shared.Harvester;
        readonly Image _smallImage = Shared.Harvester;

        public RepVehicleUnloading()
        {
            _dataViewer = new RepVehiclesUnloadingView {Dock = DockStyle.Fill};
        }

        #region IOrderReport Members

        public UserControl DataViewer{
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dataSource = new RepVehicleUnloadingData();
            dataSource.ChangeStatus += OnChangeStatus;
            ((RepVehiclesUnloadingView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin
             , end
             , ((RepVehiclesUnloadingView)_dataViewer).VehicleGroup
             ,((RepVehiclesUnloadingView)_dataViewer).Vehicle);
            ((RepVehiclesUnloadingView)_dataViewer).Begin = begin;
            ((RepVehiclesUnloadingView)_dataViewer).End = end;
            dataSource.ChangeStatus -= OnChangeStatus;
        }

        public bool IsHasData
        {
            get { return ((RepVehiclesUnloadingView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepVehiclesUnloadingView)_dataViewer).ExportToExcel();
        }

        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }
    }
}
