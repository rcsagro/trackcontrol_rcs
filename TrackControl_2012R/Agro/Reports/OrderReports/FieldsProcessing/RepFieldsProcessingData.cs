﻿using System;
using System.Data;
using Agro.Utilites;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessingData
    {
        public RepFieldsProcessingData()
        {

        }

        public DataTable GetData(DateTime begin, DateTime end)
        {
            DataTable data = new DataTable();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql =
                    string.Format( AgroQuery.RepFieldsProcessingData.SelectDriverName, AgroQuery.SqlVehicleIdent, driverDb.ParamPrefics,
                    TrackControlQuery.SqlDriverIdent, AgroQuery.SqlFieldIdent);

                    driverDb.NewSqlParameterArray(2);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DateStart", begin);
                    driverDb.SetNewSqlParameter( driverDb.ParamPrefics + "DateEnd", end );
                    data = driverDb.GetDataTable(sql, driverDb.GetSqlParameterArray); 
            }
            return data; 
        }
    }

    
}
