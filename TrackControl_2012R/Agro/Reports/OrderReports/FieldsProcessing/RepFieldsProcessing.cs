﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepFieldsProcessing : IReportItem 
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.FieldsProcessing, Environment.NewLine);
        Image _largeImage = Shared.FieldsProcessing;
        Image _smallImage = Shared.FieldsProcessing;

        public RepFieldsProcessing()
        {
            _dataViewer = new RepFieldsProcessingView();
            _dataViewer.Dock = DockStyle.Fill;
        }


        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            RepFieldsProcessingData dataSource = new RepFieldsProcessingData();
            ((RepFieldsProcessingView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end);
            ((RepFieldsProcessingView)_dataViewer).Begin = begin;
            ((RepFieldsProcessingView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return ((RepFieldsProcessingView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepFieldsProcessingView)_dataViewer).ExportToExcel();
        }

        #endregion
    }
}
