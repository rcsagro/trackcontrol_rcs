namespace Agro.Reports.OrderReports
{
    partial class RepFieldsProcessingView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepFieldsProcessingView));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem();
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.Size = new System.Drawing.Size(827, 470);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDriverName,
            this.colDepartment,
            this.colVehicleName,
            this.colAgregatName,
            this.colFieldGroupName,
            this.colFieldName,
            this.colWorkName,
            this.colTotalSquare});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalSquare", null, "����� ���������� {0} ��")});
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            this.gvReport.OptionsView.ShowFooter = true;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.Caption = "��������";
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 0;
            // 
            // colDepartment
            // 
            this.colDepartment.AppearanceHeader.Options.UseTextOptions = true;
            this.colDepartment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDepartment.Caption = "�����������";
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 1;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.Caption = "�������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 2;
            // 
            // colAgregatName
            // 
            this.colAgregatName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatName.Caption = "�������� ������������";
            this.colAgregatName.FieldName = "AgregatName";
            this.colAgregatName.Name = "colAgregatName";
            this.colAgregatName.OptionsColumn.AllowEdit = false;
            this.colAgregatName.OptionsColumn.ReadOnly = true;
            this.colAgregatName.Visible = true;
            this.colAgregatName.VisibleIndex = 3;
            // 
            // colFieldGroupName
            // 
            this.colFieldGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldGroupName.Caption = "������ �����";
            this.colFieldGroupName.FieldName = "FieldGroupName";
            this.colFieldGroupName.Name = "colFieldGroupName";
            this.colFieldGroupName.Visible = true;
            this.colFieldGroupName.VisibleIndex = 4;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 5;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.Caption = "��� �����";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.VisibleIndex = 6;
            // 
            // colTotalSquare
            // 
            this.colTotalSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalSquare.Caption = "������������ �������, ��";
            this.colTotalSquare.FieldName = "TotalSquare";
            this.colTotalSquare.Name = "colTotalSquare";
            this.colTotalSquare.OptionsColumn.AllowEdit = false;
            this.colTotalSquare.OptionsColumn.ReadOnly = true;
            this.colTotalSquare.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTotalSquare.Visible = true;
            this.colTotalSquare.VisibleIndex = 7;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepFieldsProcessingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Name = "RepFieldsProcessingView";
            this.Size = new System.Drawing.Size(827, 470);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatName;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkName;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalSquare;
        public DevExpress.XtraGrid.GridControl gcReport;
        public DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldGroupName;
    }
}
