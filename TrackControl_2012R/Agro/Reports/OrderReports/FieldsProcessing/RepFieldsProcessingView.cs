using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using Agro.Properties;

namespace Agro.Reports.OrderReports
{
    public partial class RepFieldsProcessingView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }
        
        public RepFieldsProcessingView()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            colVehicleName.Caption = Resources.Vehicle;
            colAgregatName.Caption = Resources.Agregat;
            colTotalSquare.Caption = Resources.SquareProcessGa;
            colDriverName.Caption = Resources.Driver;
            colFieldName.Caption = string.Format("{0} ({1})", Resources.Field, Resources.SquareGA);
            gvReport.GroupSummary[0].DisplayFormat = Resources.TotalProcessGa;
            colDepartment.Caption = Resources.Department;
            colFieldGroupName.Caption = Resources.FieldsGroup;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.FieldsProcessing;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }
    }
}
