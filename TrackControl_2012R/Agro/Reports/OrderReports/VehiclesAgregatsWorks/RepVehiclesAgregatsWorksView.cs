using System;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public partial class RepVehiclesAgregatsWorksView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public RepVehiclesAgregatsWorksView()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }
        void Localization()
        {
            colVehicleName.Caption = Resources.Vehicle;
            colAgregatName.Caption = Resources.Agregat;
            colWorkName.Caption = Resources.WorkType;
            colProcessSquare.Caption = Resources.SquareProcessGa;
            colExpenseDUT.Caption = Resources.FuelExpensDUT;
            colExpenseDRT.Caption = Resources.FuelExpensDRT;
            colExpenseAvgGa.Caption = Resources.FuelExpenseAvgLGa;
            colNumberFuellings.Caption = Resources.NumberFuellings;
            colNumberFuelDischarges.Caption = Resources.NumberFuelDischarges;
            colTotalFuellingsL.Caption = Resources.FuelAddL;
            colTotalFuelDischargesL.Caption = Resources.FuelSubL;
            colAverageConsumptionL.Caption = Resources.AvgFuelConsumptionDriving;

        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.VehicleAggregateRow1;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }


    }
}
