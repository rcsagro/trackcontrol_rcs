﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public class RepVehiclesAgregatsWorksFields
    {
        public string VehicleName { get; set; }

        public string AgregatName { get; set; }

        public string WorkName { get; set; }

        public double ProcessSquare { get; set; }

        public double ExpenseDut { get; set; }

        public double ExpenseDrt { get; set; }

        public double ExpenseAvgGa { get; set; }

        public int NumberFuellings { get; set; }

        public int NumberFuelDischarges { get; set; }

        public double TotalFuellingsL { get; set; }

        public double TotalFuelDischargesL { get; set; }

        public double AverageConsumptionL { get; set; }

        public double Distance { get; set; }
        

        
    }
}
