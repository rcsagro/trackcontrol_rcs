﻿using System;
using System.Collections.Generic;
using System.Data;
using Agro.Properties;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepVehiclesAgregatsWorksData
    {
        private Dictionary<long, List<FuelAddSubData>> fuelRecords = new Dictionary<long, List<FuelAddSubData>>();
        public event Action<string, int> ChangeStatus;
        public RepVehiclesAgregatsWorksData()
        {
            // to do
        }

        public List<RepVehiclesAgregatsWorksFields> GetData(DateTime begin, DateTime end)
        {
            var records = new List<RepVehiclesAgregatsWorksFields>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GetDataReader(begin, end, driverDb);
                string identRecord = "";
                RepVehiclesAgregatsWorksFields record = null;
                while (driverDb.Read())
                {
                    string identReader = string.Format("{0}_{1}_{2}", driverDb.GetInt32("Id_mobitel"),
                                                       driverDb.GetInt32("Id_agregat"), driverDb.GetInt32("Id_work"));
                    ChangeStatus(string.Format("{0} {1} {2}", driverDb.GetString("VehicleName"), driverDb.GetString("AgregatName"), driverDb.GetString("WorkName")),Consts.PROGRESS_BAR_STOP);
                    if (identRecord != identReader)
                    {
                        if (record != null)
                        {
                            SetFinalSettings(record);
                            records.Add(record);
                        }
                        identRecord = identReader;
                        record = new RepVehiclesAgregatsWorksFields();
                        AddConstPart(driverDb, record);
                        AddVariablePart(driverDb, record);
                    }
                    else
                    {
                        AddVariablePart(driverDb, record);

                    }
                }
                if (record != null)
                {
                    SetFinalSettings(record);
                    records.Add(record);
                }
            }

            return records;
        }

        private  void GetDataReader(DateTime begin, DateTime end, DriverDb driverDb)
        {
            string sql = string.Format(AgroQuery.RepVehiclesAgregatWorkssData.SelectAgroWork_Ordert, AgroQuery.SqlVehicleIdent);

            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", begin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
            driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
        }

        private  void SetFinalSettings(RepVehiclesAgregatsWorksFields record)
        {
            if (record.ProcessSquare > 0)
            {
                if (record.ExpenseDut >0)
                    record.ExpenseAvgGa = Math.Round(record.ExpenseDut / record.ProcessSquare, 2);
                else if (record.ExpenseDrt >0)
                    record.ExpenseAvgGa = Math.Round(record.ExpenseDrt / record.ProcessSquare, 2);
            }
            if (record.Distance > 0)
            {
                if (record.ExpenseDut > 0)
                    record.AverageConsumptionL = Math.Round(100 * record.ExpenseDut / record.Distance, 2);
                else if (record.ExpenseDrt > 0)
                    record.AverageConsumptionL = Math.Round(100 * record.ExpenseDrt / record.Distance, 2);
            }
            record.ExpenseDut = Math.Round(record.ExpenseDut, 2);
            record.ExpenseDrt = Math.Round(record.ExpenseDrt, 2);
            record.ProcessSquare = Math.Round(record.ProcessSquare, 2);
            ChangeStatus(Resources.Ready, Consts.PROGRESS_BAR_STOP);
        }

        private void AddConstPart(DriverDb driverDb, RepVehiclesAgregatsWorksFields record)
        {
            record.VehicleName = driverDb.GetString("VehicleName");
            record.AgregatName = driverDb.GetString("AgregatName");
            record.WorkName = driverDb.GetString("WorkName");
        }

        private void AddVariablePart(DriverDb driverDb, RepVehiclesAgregatsWorksFields record)
        {
            record.Distance += driverDb.GetDouble("Distance");
            record.TotalFuellingsL += driverDb.GetDouble("FuelAdd");
            record.TotalFuelDischargesL += driverDb.GetDouble("FuelSub");
            record.ExpenseDut += driverDb.GetDouble("FuelExpens");
            record.ExpenseDrt += driverDb.GetDouble("Fuel_ExpensTotal");
            record.ProcessSquare += driverDb.GetDouble("FactSquareCalc");
            SetFuelAddSub(driverDb.GetInt64("Id"), driverDb.GetDateTime("TimeStart"), driverDb.GetDateTime("TimeEnd"), record);
        }

        private void SetFuelAddSub(long idMain, DateTime dtStart, DateTime dtEnd, RepVehiclesAgregatsWorksFields record)
        {
            if (!fuelRecords.ContainsKey(idMain))
            {
                fuelRecords[idMain] = GetOrderFuelAddSub(idMain);
            }

            foreach (var fuelAddSubData in fuelRecords[idMain])
            {
                if (fuelAddSubData.DateFueling >= dtStart && fuelAddSubData.DateFueling <= dtEnd)
                    if (fuelAddSubData.FuelValue > 0)
                        record.NumberFuellings++;
                    else if (fuelAddSubData.FuelValue < 0)
                        record.NumberFuelDischarges ++;
            }
        }

        private List<FuelAddSubData> GetOrderFuelAddSub(long idMain)
        {
            var fuelRecords = new List<FuelAddSubData>();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(AgroQuery.OrderFueling.SelectFromAgroFueling, idMain);
                driverDb.GetDataReader(sql);
                while (driverDb.Read())
                {
                    var record = new FuelAddSubData();
                    record.FuelValue = driverDb.GetDecimal("ValueDisp");
                    record.DateFueling = driverDb.GetDateTime("DateFueling");
                    fuelRecords.Add(record);
                }
            }
            return fuelRecords;
        }

        private class FuelAddSubData
        {
            public decimal FuelValue;
            public DateTime DateFueling;
        }
    }
}
