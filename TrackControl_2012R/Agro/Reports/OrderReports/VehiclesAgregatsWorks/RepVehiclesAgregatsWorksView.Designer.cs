namespace Agro.Reports.OrderReports
{
    partial class RepVehiclesAgregatsWorksView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepVehiclesAgregatsWorksView));
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregatName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcessSquare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseDRT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpenseAvgGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberFuellings = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberFuelDischarges = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuellingsL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelDischargesL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageConsumptionL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teTime});
            this.gcReport.Size = new System.Drawing.Size(891, 494);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.ColumnPanelRowHeight = 60;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVehicleName,
            this.colAgregatName,
            this.colWorkName,
            this.colProcessSquare,
            this.colExpenseDUT,
            this.colExpenseDRT,
            this.colExpenseAvgGa,
            this.colNumberFuellings,
            this.colNumberFuelDischarges,
            this.colTotalFuellingsL,
            this.colTotalFuelDischargesL,
            this.colAverageConsumptionL});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVehicleName.Caption = "�������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.AllowFocus = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 0;
            // 
            // colAgregatName
            // 
            this.colAgregatName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregatName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregatName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAgregatName.Caption = "�������";
            this.colAgregatName.FieldName = "AgregatName";
            this.colAgregatName.Name = "colAgregatName";
            this.colAgregatName.OptionsColumn.AllowEdit = false;
            this.colAgregatName.OptionsColumn.AllowFocus = false;
            this.colAgregatName.OptionsColumn.ReadOnly = true;
            this.colAgregatName.Visible = true;
            this.colAgregatName.VisibleIndex = 1;
            // 
            // colWorkName
            // 
            this.colWorkName.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkName.Caption = "��� �����";
            this.colWorkName.FieldName = "WorkName";
            this.colWorkName.Name = "colWorkName";
            this.colWorkName.OptionsColumn.AllowEdit = false;
            this.colWorkName.OptionsColumn.ReadOnly = true;
            this.colWorkName.Visible = true;
            this.colWorkName.VisibleIndex = 2;
            // 
            // colProcessSquare
            // 
            this.colProcessSquare.AppearanceHeader.Options.UseTextOptions = true;
            this.colProcessSquare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProcessSquare.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProcessSquare.Caption = "�������, ��";
            this.colProcessSquare.FieldName = "ProcessSquare";
            this.colProcessSquare.Name = "colProcessSquare";
            this.colProcessSquare.OptionsColumn.AllowEdit = false;
            this.colProcessSquare.OptionsColumn.ReadOnly = true;
            this.colProcessSquare.Visible = true;
            this.colProcessSquare.VisibleIndex = 3;
            // 
            // colExpenseDUT
            // 
            this.colExpenseDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseDUT.Caption = "������ ������� ���, �";
            this.colExpenseDUT.FieldName = "ExpenseDut";
            this.colExpenseDUT.Name = "colExpenseDUT";
            this.colExpenseDUT.OptionsColumn.AllowEdit = false;
            this.colExpenseDUT.OptionsColumn.AllowFocus = false;
            this.colExpenseDUT.OptionsColumn.ReadOnly = true;
            this.colExpenseDUT.Visible = true;
            this.colExpenseDUT.VisibleIndex = 4;
            // 
            // colExpenseDRT
            // 
            this.colExpenseDRT.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseDRT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseDRT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseDRT.Caption = "������ ������� ���, �";
            this.colExpenseDRT.FieldName = "ExpenseDrt";
            this.colExpenseDRT.Name = "colExpenseDRT";
            this.colExpenseDRT.OptionsColumn.AllowEdit = false;
            this.colExpenseDRT.OptionsColumn.ReadOnly = true;
            this.colExpenseDRT.Visible = true;
            this.colExpenseDRT.VisibleIndex = 5;
            // 
            // colExpenseAvgGa
            // 
            this.colExpenseAvgGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpenseAvgGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpenseAvgGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colExpenseAvgGa.Caption = "������� ������ �/��";
            this.colExpenseAvgGa.FieldName = "ExpenseAvgGa";
            this.colExpenseAvgGa.Name = "colExpenseAvgGa";
            this.colExpenseAvgGa.OptionsColumn.AllowEdit = false;
            this.colExpenseAvgGa.OptionsColumn.ReadOnly = true;
            this.colExpenseAvgGa.Visible = true;
            this.colExpenseAvgGa.VisibleIndex = 6;
            // 
            // colNumberFuellings
            // 
            this.colNumberFuellings.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberFuellings.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberFuellings.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberFuellings.Caption = "���������� ��������";
            this.colNumberFuellings.FieldName = "NumberFuellings";
            this.colNumberFuellings.Name = "colNumberFuellings";
            this.colNumberFuellings.OptionsColumn.AllowEdit = false;
            this.colNumberFuellings.OptionsColumn.ReadOnly = true;
            this.colNumberFuellings.Visible = true;
            this.colNumberFuellings.VisibleIndex = 7;
            // 
            // colNumberFuelDischarges
            // 
            this.colNumberFuelDischarges.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberFuelDischarges.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberFuelDischarges.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberFuelDischarges.Caption = "���������� ������";
            this.colNumberFuelDischarges.FieldName = "NumberFuelDischarges";
            this.colNumberFuelDischarges.Name = "colNumberFuelDischarges";
            this.colNumberFuelDischarges.OptionsColumn.AllowEdit = false;
            this.colNumberFuelDischarges.OptionsColumn.ReadOnly = true;
            this.colNumberFuelDischarges.Visible = true;
            this.colNumberFuelDischarges.VisibleIndex = 8;
            // 
            // colTotalFuellingsL
            // 
            this.colTotalFuellingsL.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuellingsL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuellingsL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuellingsL.Caption = "����� ����������, �";
            this.colTotalFuellingsL.FieldName = "TotalFuellingsL";
            this.colTotalFuellingsL.Name = "colTotalFuellingsL";
            this.colTotalFuellingsL.OptionsColumn.AllowEdit = false;
            this.colTotalFuellingsL.OptionsColumn.ReadOnly = true;
            this.colTotalFuellingsL.Visible = true;
            this.colTotalFuellingsL.VisibleIndex = 9;
            // 
            // colTotalFuelDischargesL
            // 
            this.colTotalFuelDischargesL.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelDischargesL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelDischargesL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelDischargesL.Caption = "����� �����,�";
            this.colTotalFuelDischargesL.FieldName = "TotalFuelDischargesL";
            this.colTotalFuelDischargesL.Name = "colTotalFuelDischargesL";
            this.colTotalFuelDischargesL.OptionsColumn.AllowEdit = false;
            this.colTotalFuelDischargesL.OptionsColumn.ReadOnly = true;
            this.colTotalFuelDischargesL.Visible = true;
            this.colTotalFuelDischargesL.VisibleIndex = 10;
            // 
            // colAverageConsumptionL
            // 
            this.colAverageConsumptionL.AppearanceHeader.Options.UseTextOptions = true;
            this.colAverageConsumptionL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAverageConsumptionL.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAverageConsumptionL.Caption = "������� ������ �� ����� ��������, �/100 ��";
            this.colAverageConsumptionL.FieldName = "AverageConsumptionL";
            this.colAverageConsumptionL.Name = "colAverageConsumptionL";
            this.colAverageConsumptionL.OptionsColumn.AllowEdit = false;
            this.colAverageConsumptionL.OptionsColumn.ReadOnly = true;
            this.colAverageConsumptionL.Visible = true;
            this.colAverageConsumptionL.VisibleIndex = 11;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.teTime.Name = "teTime";
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepVehiclesAgregatsWorksView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Name = "RepVehiclesAgregatsWorksView";
            this.Size = new System.Drawing.Size(891, 494);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraGrid.GridControl gcReport;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregatName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseDUT;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkName;
        private DevExpress.XtraGrid.Columns.GridColumn colProcessSquare;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseDRT;
        private DevExpress.XtraGrid.Columns.GridColumn colExpenseAvgGa;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberFuellings;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberFuelDischarges;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuellingsL;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelDischargesL;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageConsumptionL;
    }
}
