﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepVehiclesAgregatsWorks : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = string.Format(Resources.VehicleAggregateWork, Environment.NewLine);
        readonly Image _largeImage = Shared.Tractor64;
        readonly Image _smallImage = Shared.Tractor64;

        public RepVehiclesAgregatsWorks()
        {
            _dataViewer = new RepVehiclesAgregatsWorksView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public Image LargeImage
        {
            get { return _largeImage; }
        }

        public Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dataSource = new RepVehiclesAgregatsWorksData();
            dataSource.ChangeStatus += OnChangeStatus;
            ((RepVehiclesAgregatsWorksView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end);
            ((RepVehiclesAgregatsWorksView)_dataViewer).Begin = begin;
            ((RepVehiclesAgregatsWorksView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return ((RepVehiclesAgregatsWorksView)_dataViewer).gvReport.RowCount > 0; }
        }

        public void ExportToExcel()
        {
            ((RepVehiclesAgregatsWorksView)_dataViewer).ExportToExcel();
        }
        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }

    }
}
