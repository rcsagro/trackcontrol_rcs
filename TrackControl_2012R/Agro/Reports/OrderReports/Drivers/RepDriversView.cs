using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Agro.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public partial class RepDriversView : DevExpress.XtraEditors.XtraUserControl
    {
        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        public RepDriversView()
        {
            InitializeComponent();
            Localization();
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, false, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            gridColumn2.Caption = Resources.Driver;
            gridColumn3.Caption = Resources.DaysWork;
            gridColumn4.Caption = Resources.WorkTime;
            gridColumn5.Caption = Resources.RunTotal;
            gridColumn6.Caption = Resources.RunFieldOut;
            gridColumn7.Caption = Resources.TimeWorkField;
            gridColumn8.Caption = Resources.TimeStopField;
            gridColumn9.Caption = Resources.TimeMoveFieldOut;
            colSquareCont.Caption = Resources.SquareContourOverlap;
            gridColumn11.Caption = Resources.SquareRun;
            gridColumn12.Caption = Resources.FuelExpenseTotal;
            gridColumn14.Caption = Resources.Date;
            gridColumn13.Caption = Resources.WorkTime;
            gridColumn15.Caption = Resources.RunTotal;
            gridColumn16.Caption = Resources.RunFieldOut;
            gridColumn17.Caption = Resources.TimeWorkField;
            gridColumn18.Caption = Resources.TimeStopField;
            gridColumn19.Caption = Resources.TimeMoveFieldOut;
            gridColumn20.Caption = Resources.SquareContourOverlap;
            gridColumn21.Caption = Resources.SquareRun;
            gridColumn22.Caption = Resources.FuelExpenseTotal;

            gridBand5.Caption = Resources.Field;
            bandedGridColumn2.Caption = Resources.FieldsGroup;
            bandedGridColumn3.Caption = Resources.Field;
            bandedGridColumn4.Caption = Resources.SquareGA;
            gridBand9.Caption = Resources.Time;
            bandedGridColumn5.Caption = Resources.Entry;
            bandedGridColumn6.Caption = Resources.Exit;
            bandedGridColumn7.Caption = Resources.TimeWorksH;
            bandedGridColumn8.Caption = Resources.TimeMoveH;
            bandedGridColumn9.Caption = Resources.TimeStopH;
            bandedGridColumn10.Caption = Resources.MotoHourH;
            gridBand10.Caption = Resources.Work;
            bandedGridColumn11.Caption = Resources.SquareRunGa;
            bandedGridColumn12.Caption = Resources.SquareContourGa;
            bandedGridColumn13.Caption = Resources.SpeedAvg;
            bandedGridColumn14.Caption = Resources.CheckZone;
            gridBand12.Caption = Resources.SensorDUT;
            bandedGridColumn16.Caption = Resources.FuelStartL;
            bandedGridColumn17.Caption = Resources.FuelAddL;
            bandedGridColumn18.Caption = Resources.FuelSubL;
            bandedGridColumn19.Caption = Resources.FuelEndL;
            bandedGridColumn20.Caption = Resources.FuelExpenseTotalL;
            bandedGridColumn21.Caption = Resources.FuelExpenseAvgLGa;
            bandedGridColumn22.Caption = Resources.FuelExpenseAvgLMHour;
            gridBand13.Caption = Resources.SensorDRT_CAN;
            bandedGridColumn23.Caption = Resources.FuelExpenseMovingL;
            bandedGridColumn24.Caption = Resources.FuelExpenseStopL;
            bandedGridColumn25.Caption = Resources.FuelExpenseTotalL;
            bandedGridColumn26.Caption = Resources.FuelExpenseAvgLGa;
            bandedGridColumn27.Caption = Resources.FuelExpenseAvgLMHour;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.DriverReports;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);
        }


        private void gvReport_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Column.FieldName != "colSquareCont") return;
            if (!e.IsGetData) return;
            DataRow row = ((view.DataSource as IList)[e.ListSourceRowIndex] as DataRowView).Row;
            double subTotal = 0;
            foreach (DataRow childRow in row.GetChildRows(Resources.Detail))
                subTotal += (double)childRow["SquareCont"];
            e.Value = Math.Round(subTotal,3);
        }
    }
}
