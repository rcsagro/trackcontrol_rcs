using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Properties;
using Agro.Utilites;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    /// <summary>
    /// ������ �� �������
    /// </summary>
    public class RepDriversData
    {
        public event Action<string, int> ChangeStatus;
        private int _idDriver;
        public RepDriversData()
        {
        }
        private enum RowStatus {Summary,Day}
        /// <summary>
        /// ������������ ������ �� ���������
        /// </summary>
        public DataTable GetDriversSummary(DateTime dtBegin, DateTime dtEnd,string sTableName)
        {
            List<DateTime> lsDateOrder = new List<DateTime>();
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSqLselect = AgroQuery.RepDriversData.SelectDId;
                
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", dtBegin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", dtEnd);
            driverDb.GetDataReader(sSqLselect, driverDb.GetSqlParameterArray);
            var dtReport = new DataTable(sTableName);
            //���� ������
            CreateReportColumns(dtReport, RowStatus.Summary);
            _idDriver = 0;
            double dbDUT = 0;
            double dbDRT = 0;
            DataRow row = null;
            DateTime dtWork = DateTime.Today;
            while (driverDb.Read())
            {
                if (_idDriver != driverDb.GetInt32(driverDb.GetOrdinal("ID")))
                {
                    if (_idDriver > 0)
                    {
                        AddRowWithParamsSummary(dbDRT, dbDUT, row, dtReport);
                    }
                    ChangeStatus(driverDb.GetString(driverDb.GetOrdinal("Family")), Consts.PROGRESS_BAR_STOP);
                    _idDriver = driverDb.GetInt32(driverDb.GetOrdinal("ID"));
                    row = dtReport.NewRow();
                    row["ID"] = _idDriver;
                    row["Family"] = driverDb.GetString(driverDb.GetOrdinal("Family"));

                    dtWork = driverDb.GetDateTime(driverDb.GetOrdinal("Date"));
                    if (!lsDateOrder.Contains(dtWork)) lsDateOrder.Add(dtWork); 
                    row["Days"] = 1;
                    dbDRT = 0;
                    dbDUT = 0;
                 }
                 if (dtWork != driverDb.GetDateTime(driverDb.GetOrdinal("Date")))
                 {
                     dtWork = driverDb.GetDateTime(driverDb.GetOrdinal("Date"));
                     row["Days"] = (int)row["Days"] + 1;
                 }
                 row = AddReaderDrv(row, driverDb, ref dbDUT, ref dbDRT);
            }
            if (_idDriver > 0)
            {
                //AddSquareOverlap(row, driverDb);
                AddRowWithParamsSummary(dbDRT,  dbDUT, row, dtReport);
            }
            return dtReport;
            }  
        }

        private static void AddRowWithParamsSummary(double dbDRT, double dbDUT, DataRow row,  DataTable dtReport)
        {
            if (dbDRT > 0)
                row["Fuel"] = dbDRT;
            else
            {
                if (dbDUT > 0)
                    row["Fuel"] = dbDUT;
            }
            dtReport.Rows.Add(row);
        }

        /// <summary>
        /// ��������� ���������� �� ����
        /// </summary>
        /// <param name="sTableName"> ��� ����������� ������� </param>
        /// <returns></returns>
        public DataTable GetDriversDays(DateTime dtBegin, DateTime dtEnd, string sTableName)
        {
            ChangeStatus(Resources.DataSelect, Consts.PROGRESS_BAR_STOP);
            //26.09.2016 Message: "���������� �������� ��� �����������, �.�. �� ��� �������� ����� ��������������� ������������ ��������."
            DateTime _dtBegin = dtBegin;// new DateTime(dtBegin.Year, dtBegin.Month, dtBegin.Day, 0, 0, 0);
            DateTime _dtEnd = dtEnd;//new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, 23, 59, 59);
            //----------------------------------------------------------------------------------
            //������ ��� �� ������� ���� ������ - �������� �� ����������� �������
            List<DateTime> lsDateOrder = new List<DateTime>();
            _idDriver = 0;
            var dos = new DriverOrderSquare(); 
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSqLselect = AgroQuery.RepDriversData.SelectAgro_OrderT;
               
                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", _dtBegin);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", _dtEnd);
                driverDb.GetDataReader(sSqLselect, driverDb.GetSqlParameterArray);
                DataTable dtReport = new DataTable(sTableName);
                //���� ������
                CreateReportColumns(dtReport, RowStatus.Day);
                double dbDut = 0;
                double dbDrt = 0;
                DataRow row = null;
                DateTime dtWork = DateTime.MinValue;

                while (driverDb.Read())
                {
                    if (dtWork != driverDb.GetDateTime(driverDb.GetOrdinal("Date")) || _idDriver != driverDb.GetInt32(driverDb.GetOrdinal("Id_driver")))
                    {
                        ChangeStatus(string.Format("{0} {1}", driverDb.GetDateTime(driverDb.GetOrdinal("Date")),
                            driverDb.GetString(driverDb.GetOrdinal("Family"))), Consts.PROGRESS_BAR_STOP);
                        if (_idDriver > 0)
                        {
                            AddRowWithParamsSummary( dbDrt, dbDut, row, dtReport);
                        }
                        _idDriver = driverDb.GetInt32(driverDb.GetOrdinal("Id_driver"));
                        dtWork = driverDb.GetDateTime(driverDb.GetOrdinal("Date"));
                        row = dtReport.NewRow();
                        row["Id_driver"] = _idDriver;
                        row["Date"] = dtWork;
                        dbDrt = 0;
                        dbDut = 0;
                    }

                    row = AddReaderDrv(row, driverDb, ref dbDut, ref dbDrt);
                    row["SquareCont"] = ((double)row["SquareCont"]) + dos.GetSquareOverlap(driverDb);
                }
                if (_idDriver > 0)
                {
                    AddRowWithParamsSummary( dbDrt, dbDut, row, dtReport);
                }
                ChangeStatus(Resources.Ready, Consts.PROGRESS_BAR_STOP);
                return dtReport;
            }
        }


        /// <summary>
        /// ���������� ���� � ���������� ��� ������
        /// </summary>
        /// <returns></returns>
        private int CountDaysWOrder(DateTime dtBegin, DateTime dtEnd, List<DateTime> lsDateOrder)
        {
            int iCountDaysWOrder = 0;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                for (int i = 0; i < dtEnd.Subtract(dtBegin).TotalDays; i++)
                {
                    DateTime dtWork = dtBegin.AddDays(i);
                    if (!lsDateOrder.Contains(dtWork))
                    {
                        string sqLselect = string.Format( AgroQuery.RepDriversData.SelectDatagps, dtWork.Day.ToString(), dtWork.Month.ToString(),
                            dtWork.Year.ToString() );
                      
                        if (driverDb.GetScalarValueNull<Int32>(sqLselect, 0) != 0) iCountDaysWOrder++;
                        Application.DoEvents();
                    }
                }
                return iCountDaysWOrder;
            }
        }

        private DataRow AddReaderDrv(DataRow row, DriverDb driverDb, ref double dbDUT, ref double dbDRT)
        {
            if (!driverDb.IsDbNull(driverDb.GetOrdinal("TimeMove"))) 
            row["Dist"] = ((double)row["Dist"]) + driverDb.GetDouble(driverDb.GetOrdinal("Distance"));

            //if (!driverDb.IsDbNull(driverDb.GetOrdinal("FactSquareCalc")))
            //    row["SquareCont"] = ((double)row["SquareCont"]) + driverDb.GetDouble(driverDb.GetOrdinal("FactSquareCalc"));

        if (!driverDb.IsDbNull(driverDb.GetOrdinal("FactSquare"))) 
            row["SquareDist"] = ((double)row["SquareDist"]) + driverDb.GetDouble(driverDb.GetOrdinal("FactSquare"));

            if (driverDb.GetInt32(driverDb.GetOrdinal("Id_field")) == 0)
            {
                if (!driverDb.IsDbNull(driverDb.GetOrdinal("Distance"))) 
                row["DistFieldOut"] = ((double)row["DistFieldOut"]) + driverDb.GetDouble(driverDb.GetOrdinal("Distance"));
                if (!driverDb.IsDbNull(driverDb.GetOrdinal("TimeMove"))) 
                row["WorkTimeFieldOut"] = ((TimeSpan)row["WorkTimeFieldOut"]).Add(driverDb.GetTimeSpan(driverDb.GetOrdinal("TimeMove")));
            }
            else
            {
                if (!driverDb.IsDbNull(driverDb.GetOrdinal("TimeMove")))  
                row["WorkTimeField"] = ((TimeSpan)row["WorkTimeField"]).Add(driverDb.GetTimeSpan(driverDb.GetOrdinal("TimeMove")));
                if (!driverDb.IsDbNull(driverDb.GetOrdinal("TimeStop")))  
                row["WorkTimeFieldStop"] = ((TimeSpan)row["WorkTimeFieldStop"]).Add(driverDb.GetTimeSpan(driverDb.GetOrdinal("TimeStop")));
            }
            row["WorkTime"] = ((TimeSpan)row["WorkTime"]).Add(driverDb.GetDateTime(driverDb.GetOrdinal("TimeEnd")).Subtract(driverDb.GetDateTime(driverDb.GetOrdinal("TimeStart"))));
            dbDUT += driverDb.GetDouble(driverDb.GetOrdinal("FuelExpens"));
            dbDRT += driverDb.GetDouble(driverDb.GetOrdinal("Fuel_ExpensTotal"));
            return row;
        }



        private void CreateReportColumns(DataTable dtReport,RowStatus rs)
        {
            DataColumn col;
            if (rs == RowStatus.Summary)
            {
                //�������� ���
                col = new DataColumn("ID", System.Type.GetType("System.Int32"));
                dtReport.Columns.Add(col);
                //��������
                col = new DataColumn("Family", System.Type.GetType("System.String"));
                dtReport.Columns.Add(col);
                //������� ���� 
                col = new DataColumn("Days", System.Type.GetType("System.Int32"));
                col.DefaultValue = 0;
                dtReport.Columns.Add(col);
            }
            else
            {
                //�������� ���
                col = new DataColumn("Id_driver", System.Type.GetType("System.Int32"));
                col.DefaultValue = 0;
                dtReport.Columns.Add(col);
                //����
                col = new DataColumn("Date", System.Type.GetType("System.DateTime"));
                dtReport.Columns.Add(col);
            }
                //������� �����
            col = new DataColumn("WorkTime", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������ �����
            col = new DataColumn("Dist", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������ ��� �����
            col = new DataColumn("DistFieldOut", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������ � �����
            col = new DataColumn("WorkTimeField", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������� � �����
            col = new DataColumn("WorkTimeFieldStop", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� �������� ��� �����
            col = new DataColumn("WorkTimeFieldOut", System.Type.GetType("System.TimeSpan"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������������ ������� �� �������
            col = new DataColumn("SquareCont", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //������������ ������� �� �������
            col = new DataColumn("SquareDist", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
            //����� ������ �������
            col = new DataColumn("Fuel", System.Type.GetType("System.Double"));
            col.DefaultValue = 0;
            dtReport.Columns.Add(col);
        }
    }

    class DriverOrderSquare
    {
        private int _idDriver;
        private List<int> orderIds = new List<int>();
        private int _idOrder; 
        public double GetSquareOverlap( DriverDb driverDb)
        {
            _idOrder = driverDb.GetInt32(driverDb.GetOrdinal("OR_ID"));
            if (_idDriver != driverDb.GetInt32(driverDb.GetOrdinal("Id_driver")))
            {
                _idDriver = driverDb.GetInt32(driverDb.GetOrdinal("Id_driver"));
                orderIds.Clear();
                orderIds.Add(_idOrder);
                return GetOrderSquare(driverDb);
            }
           else
           {
               if (!orderIds.Contains(_idOrder ))
               {
                   orderIds.Add(_idOrder);
                   return GetOrderSquare(driverDb);
               }
           }
            return 0;
        }

        private double GetOrderSquare(DriverDb driverDb)
        {
            var order = new OrderItem();
            int countDrivers = order.GetDriversCount(_idOrder);
            if (countDrivers == 1)
            {
                return driverDb.GetDouble(driverDb.GetOrdinal("FactSquareCalcOverlap"));
            }
            else
            {
                return Math.Round(order.CalcSquareOverlapDriver(_idOrder, _idDriver), 3);
            }
        }
    }
}
 