﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepDrivers : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        readonly string _caption = Resources.Driver;
        readonly Image _largeImage = Shared.NoUser;
        readonly Image _smallImage = Shared.NoUser;

        DataTable dtDrv;

        public RepDrivers()
        {
            _dataViewer = new RepDriversView();
            _dataViewer.Dock = DockStyle.Fill;
        }
        
        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dsDrv = new DataSet();
            var rdrv = new RepDriversData();
            rdrv.ChangeStatus += OnChangeStatus;
            dtDrv = rdrv.GetDriversSummary(begin, end, "DrvsSummary");
            if (dtDrv.Rows.Count > 0)
            {
                dsDrv.Tables.Add(dtDrv);
                DataTable dtDrvDet = rdrv.GetDriversDays(begin, end, "DrvsDays");
                dsDrv.Tables.Add(dtDrvDet);
                DataColumn keyColumn = dsDrv.Tables["DrvsSummary"].Columns["Id"];
                DataColumn foreignKeyColumn = dsDrv.Tables["DrvsDays"].Columns["Id_driver"];
                dsDrv.Relations.Add(Resources.Detail, keyColumn, foreignKeyColumn);
                ((RepDriversView)_dataViewer).gcReport.DataSource = dsDrv.Tables["DrvsSummary"];
                ((RepDriversView)_dataViewer).gcReport.ForceInitialize();
                ((RepDriversView)_dataViewer).gcReport.LevelTree.Nodes.Add(Resources.Detail, ((RepDriversView)_dataViewer).gvReportDetal);
                ((RepDriversView)_dataViewer).Begin = begin;
                ((RepDriversView)_dataViewer).End = end;
            }
            else
            {
                ((RepDriversView)_dataViewer).gcReport.DataSource = null;
            }
            rdrv.ChangeStatus -= OnChangeStatus;
        }


        public bool IsHasData
        {
            get { return dtDrv==null ? false : !(dtDrv.Rows.Count == 0); }
        }

        public void ExportToExcel()
        {
            ((RepDriversView)_dataViewer).ExportToExcel(); 
        }

        #endregion

        void OnChangeStatus(string sMessage, int iValue)
        {
            ChangeStatus(sMessage, iValue);
        }


    }
}
