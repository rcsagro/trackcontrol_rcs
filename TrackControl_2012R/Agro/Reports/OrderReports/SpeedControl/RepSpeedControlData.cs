﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agro.Properties;
using TrackControl.General.DatabaseDriver;

namespace Agro.Reports.OrderReports
{
    public class RepSpeedControlData
    {
        Dictionary<string, RepSpeedControlFields> _reportDictRecords;
        private double _distanceForFinal;
        private double _percentForFinal;
        private double _speedMin;
        private double _speedMax;

        public List<RepSpeedControlFields> GetData(DateTime begin, DateTime end, int groupId, int vehicleId, double speedMin, double speedMax)
        {
            _reportDictRecords = new Dictionary<string, RepSpeedControlFields>();
            _speedMin = speedMin;
            _speedMax = speedMax;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GetDataReader(begin, end, groupId, vehicleId, driverDb);
                while (driverDb.Read())
                {
                    string keyRecord = string.Format("{0}_{1}", driverDb.GetInt32("FieldId"), driverDb.GetInt32("VehicleId"));
                    if (!_reportDictRecords.ContainsKey(keyRecord)) 
                        AddToDictionary(keyRecord, driverDb);
                    else
                        UpdateDictionary(keyRecord, driverDb);
                }
            }
            return ConvertDictToList();
        }

        private void GetDataReader(DateTime begin, DateTime end, int groupId, int vehicleId, DriverDb driverDb)
        {
            string sql = string.Format(AgroQuery.RepSpeedControlData.SelectOrdersInterval(groupId, vehicleId), AgroQuery.SqlVehicleIdent,
                                       driverDb.ParamPrefics);
            driverDb.NewSqlParameterArray(2);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeStart", begin);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "TimeEnd", end);
            driverDb.GetDataReader(sql, driverDb.GetSqlParameterArray);
        }

        private void AddToDictionary(string keyRecord,DriverDb driverDb)
        {
            var counters = new int[3];
            SetPointsRange(driverDb, counters);
            var record = new RepSpeedControlFields
                {
                    FieldId = driverDb.GetInt32("FieldId"),
                    FieldName = driverDb.GetString("FieldName"),
                    VehicleId = driverDb.GetInt32("VehicleId"),
                    VehicleName = driverDb.GetString("VehicleName"),
                    Distance = driverDb.GetDouble("Distance"),
                    TimeMove = driverDb.GetTimeSpan("TimeMove"),
                    TimeStop = driverDb.GetTimeSpan("TimeStop"),
                    PontsMin=counters[0],
                    PontsAvg = counters[1],
                    PontsMax =counters[2]
                };
            _reportDictRecords.Add(keyRecord, record);
        }

        private void SetPointsRange(DriverDb driverDb, int[] counters)
        {

            var cd = new CompressData("agro_datagps", driverDb.GetInt32("Id"));
            List<double> pointsSpeed = cd.GetDeCompressSpeed();
            var pointsSpeedNoZero = pointsSpeed.Where(p => p > 0).ToList();
            foreach (var pointSpeed in pointsSpeedNoZero)
            {
                if (pointSpeed < _speedMin)
                    counters[0]++;
                else if (pointSpeed < _speedMax)
                    counters[1]++;
                else
                    counters[2]++;
            }
        }

        private void UpdateDictionary(string keyRecord, DriverDb driverDb)
        {
            var counters = new int[3];
            SetPointsRange(driverDb, counters);
            _reportDictRecords[keyRecord].Distance += driverDb.GetDouble("Distance");
            _reportDictRecords[keyRecord].PontsMin += counters[0];
            _reportDictRecords[keyRecord].PontsAvg += counters[1];
            _reportDictRecords[keyRecord].PontsMax += counters[2];
            _reportDictRecords[keyRecord].TimeMove = _reportDictRecords[keyRecord].TimeMove.Add(driverDb.IsDbNull(driverDb.GetOrdinal("TimeMove"))
                    ? TimeSpan.Zero
                    : driverDb.GetTimeSpan("TimeMove"));
            _reportDictRecords[keyRecord].TimeStop = _reportDictRecords[keyRecord].TimeMove.Add(driverDb.IsDbNull(driverDb.GetOrdinal("TimeStop"))
                    ? TimeSpan.Zero
                    : driverDb.GetTimeSpan("TimeStop"));
        }

        List<RepSpeedControlFields>  ConvertDictToList()
        {
            var reportRecords = new List<RepSpeedControlFields>();
            foreach (var item in _reportDictRecords)
            {
                var recordMin = GetRecord(item, Resources.Slowly);
                reportRecords.Add(recordMin);
                var recordAvg = GetRecord(item, Resources.Norm);
                reportRecords.Add(recordAvg);
                var recordMax = GetRecord(item, Resources.Quickly);
                reportRecords.Add(recordMax);
            }
            return reportRecords;
        }

        private  RepSpeedControlFields GetRecord(KeyValuePair<string, RepSpeedControlFields> item,string regime)
        {

            var record = new RepSpeedControlFields
                {
                    FieldId = item.Value.FieldId,
                    FieldName = item.Value.FieldName,
                    VehicleId = item.Value.VehicleId,
                    VehicleName = item.Value.VehicleName,
                    Distance = item.Value.Distance,
                    TimeMove = item.Value.TimeMove,
                    TimeStop = item.Value.TimeStop,
                    RegimeName = regime
                };
            if (regime == Resources.Slowly)
            {
                SetRegimeParams(item, record, item.Value.PontsMin);
                _distanceForFinal = record.RegimeDistance;
                _percentForFinal = record.RegimePercent;
            }
            else if (regime == Resources.Norm)
            {
                SetRegimeParams(item, record, item.Value.PontsAvg);
                _distanceForFinal += record.RegimeDistance;
                _percentForFinal += record.RegimePercent;
            }
            else if (regime == Resources.Quickly)
            {
                record.RegimePonts = item.Value.PontsMax;
                record.RegimeDistance = Math.Round(record.Distance - _distanceForFinal,3);
                record.RegimePercent = Math.Round(1 - _percentForFinal,2);
            }
            return record;
        }

        private static void SetRegimeParams(KeyValuePair<string, RepSpeedControlFields> item, RepSpeedControlFields record,int pointsCount)
        {
            record.RegimePonts =pointsCount;
            record.RegimeDistance =
                Math.Round(item.Value.Distance * pointsCount / (item.Value.PontsMin + item.Value.PontsAvg + item.Value.PontsMax)  , 2);
            record.RegimePercent = Math.Round(record.RegimeDistance/record.Distance, 2);
        }
    }
}
