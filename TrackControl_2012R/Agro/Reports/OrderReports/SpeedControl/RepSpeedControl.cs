﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Agro.Properties;
using TrackControl.General;

namespace Agro.Reports.OrderReports
{
    public class RepSpeedControl : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        readonly UserControl _dataViewer;
        private  string _caption = Resources.SpeedControl; 
        readonly Image _largeImage = Shared.SpeedControl;
        readonly Image _smallImage = Shared.SpeedControl;

        bool _isHasData;

        public RepSpeedControl()
        {
            _dataViewer = new RepSpeedControlView() {Dock = DockStyle.Fill};
        }

        #region IOrderReport Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run(DateTime begin, DateTime end)
        {
            var dataSource = new RepSpeedControlData();
            var groupId = ((RepSpeedControlView) _dataViewer).Group;
            var vehicleId = ((RepSpeedControlView)_dataViewer).Vehicle;
            var speedMin = ((RepSpeedControlView)_dataViewer).SpeedMin;
            var speedMax = ((RepSpeedControlView)_dataViewer).SpeedMax;
            List<RepSpeedControlFields> records = dataSource.GetData(begin, end, groupId, vehicleId, speedMin, speedMax);
            _isHasData = records.Count > 0 ? true : false; 
            ((RepSpeedControlView)_dataViewer).gcReport.DataSource = dataSource.GetData(begin, end, groupId, vehicleId, speedMin, speedMax);
            ((RepSpeedControlView)_dataViewer).Begin = begin;
            ((RepSpeedControlView)_dataViewer).End = end;
        }


        public bool IsHasData
        {
            get { return _isHasData; }
        }

        public void ExportToExcel()
        {
            ((RepSpeedControlView)_dataViewer).ExportToExcel();
        }

        #endregion
    }
}