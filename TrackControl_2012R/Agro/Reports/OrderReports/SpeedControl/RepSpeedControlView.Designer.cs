namespace Agro.Reports.OrderReports
{
    partial class RepSpeedControlView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepSpeedControlView));
            this.gvReportDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNumberOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgregateName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSqFactGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSqAfterRecalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegimeDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegimePercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bleGroup = new DevExpress.XtraBars.BarEditItem();
            this.leGroup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bleItem = new DevExpress.XtraBars.BarEditItem();
            this.leItem = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bbiFilterClear = new DevExpress.XtraBars.BarButtonItem();
            this.beiMinSpeed = new DevExpress.XtraBars.BarEditItem();
            this.rteMinSpeed = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.beiMaxSpeed = new DevExpress.XtraBars.BarEditItem();
            this.rteMaxSpeed = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.gvReportDetal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteMinSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteMaxSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // gvReportDetal
            // 
            this.gvReportDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNumberOrder,
            this.colDateOrder,
            this.colVehicleName,
            this.colDriverName,
            this.colAgregateName,
            this.colSqFactGa,
            this.colSqAfterRecalc});
            this.gvReportDetal.GridControl = this.gcReport;
            this.gvReportDetal.Name = "gvReportDetal";
            this.gvReportDetal.OptionsDetail.EnableMasterViewMode = false;
            this.gvReportDetal.OptionsView.ShowGroupPanel = false;
            // 
            // colNumberOrder
            // 
            this.colNumberOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.Caption = "�����";
            this.colNumberOrder.FieldName = "NumberOrder";
            this.colNumberOrder.Name = "colNumberOrder";
            this.colNumberOrder.OptionsColumn.AllowEdit = false;
            this.colNumberOrder.OptionsColumn.ReadOnly = true;
            this.colNumberOrder.Visible = true;
            this.colNumberOrder.VisibleIndex = 0;
            this.colNumberOrder.Width = 117;
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.Caption = "����";
            this.colDateOrder.FieldName = "DateOrder";
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 1;
            this.colDateOrder.Width = 134;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.Caption = "���������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 2;
            this.colVehicleName.Width = 209;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.Caption = "��������";
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 3;
            this.colDriverName.Width = 160;
            // 
            // colAgregateName
            // 
            this.colAgregateName.AppearanceCell.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colAgregateName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAgregateName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAgregateName.Caption = "�������� ������������";
            this.colAgregateName.FieldName = "AgregateName";
            this.colAgregateName.Name = "colAgregateName";
            this.colAgregateName.OptionsColumn.AllowEdit = false;
            this.colAgregateName.OptionsColumn.ReadOnly = true;
            this.colAgregateName.Visible = true;
            this.colAgregateName.VisibleIndex = 4;
            this.colAgregateName.Width = 160;
            // 
            // colSqFactGa
            // 
            this.colSqFactGa.AppearanceCell.Options.UseTextOptions = true;
            this.colSqFactGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSqFactGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colSqFactGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSqFactGa.Caption = "���� ������������ �������, ��";
            this.colSqFactGa.FieldName = "SquareFactGa";
            this.colSqFactGa.Name = "colSqFactGa";
            this.colSqFactGa.OptionsColumn.AllowEdit = false;
            this.colSqFactGa.OptionsColumn.ReadOnly = true;
            this.colSqFactGa.Visible = true;
            this.colSqFactGa.VisibleIndex = 5;
            this.colSqFactGa.Width = 179;
            // 
            // colSqAfterRecalc
            // 
            this.colSqAfterRecalc.AppearanceCell.Options.UseTextOptions = true;
            this.colSqAfterRecalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSqAfterRecalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colSqAfterRecalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSqAfterRecalc.Caption = "���� ������������ �������, �� (���������)";
            this.colSqAfterRecalc.FieldName = "SquareAfterRecalc";
            this.colSqAfterRecalc.Name = "colSqAfterRecalc";
            this.colSqAfterRecalc.OptionsColumn.AllowEdit = false;
            this.colSqAfterRecalc.OptionsColumn.ReadOnly = true;
            this.colSqAfterRecalc.Visible = true;
            this.colSqAfterRecalc.VisibleIndex = 6;
            this.colSqAfterRecalc.Width = 193;
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 24);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.Name = "gcReport";
            this.gcReport.Size = new System.Drawing.Size(1193, 446);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport,
            this.gvReportDetal});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvReport.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseFont = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVehicle,
            this.colFieldName,
            this.colRegime,
            this.colRegimeDistance,
            this.colRegimePercent,
            this.colDistance,
            this.colTimeMove,
            this.colTimeStop});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalSquare", null, "����� ���������� {0} ��")});
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsBehavior.ReadOnly = true;
            this.gvReport.OptionsView.AllowCellMerge = true;
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            this.gvReport.OptionsView.ShowFooter = true;
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceCell.Options.UseTextOptions = true;
            this.colVehicle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "������������ ��������";
            this.colVehicle.FieldName = "VehicleName";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 0;
            this.colVehicle.Width = 304;
            // 
            // colFieldName
            // 
            this.colFieldName.AppearanceCell.Options.UseTextOptions = true;
            this.colFieldName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFieldName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFieldName.Caption = "����";
            this.colFieldName.FieldName = "FieldName";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.OptionsColumn.AllowEdit = false;
            this.colFieldName.OptionsColumn.ReadOnly = true;
            this.colFieldName.Visible = true;
            this.colFieldName.VisibleIndex = 1;
            this.colFieldName.Width = 151;
            // 
            // colRegime
            // 
            this.colRegime.AppearanceHeader.Options.UseTextOptions = true;
            this.colRegime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRegime.Caption = "���������� �����";
            this.colRegime.FieldName = "RegimeName";
            this.colRegime.Name = "colRegime";
            this.colRegime.OptionsColumn.AllowEdit = false;
            this.colRegime.OptionsColumn.ReadOnly = true;
            this.colRegime.Visible = true;
            this.colRegime.VisibleIndex = 2;
            this.colRegime.Width = 112;
            // 
            // colRegimeDistance
            // 
            this.colRegimeDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colRegimeDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRegimeDistance.Caption = "������";
            this.colRegimeDistance.FieldName = "RegimeDistance";
            this.colRegimeDistance.Name = "colRegimeDistance";
            this.colRegimeDistance.OptionsColumn.AllowEdit = false;
            this.colRegimeDistance.OptionsColumn.ReadOnly = true;
            this.colRegimeDistance.Visible = true;
            this.colRegimeDistance.VisibleIndex = 3;
            this.colRegimeDistance.Width = 81;
            // 
            // colRegimePercent
            // 
            this.colRegimePercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colRegimePercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRegimePercent.Caption = "����, %";
            this.colRegimePercent.DisplayFormat.FormatString = "p0";
            this.colRegimePercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRegimePercent.FieldName = "RegimePercent";
            this.colRegimePercent.Name = "colRegimePercent";
            this.colRegimePercent.Visible = true;
            this.colRegimePercent.VisibleIndex = 4;
            this.colRegimePercent.Width = 72;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.Caption = "����� ������";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 5;
            this.colDistance.Width = 133;
            // 
            // colTimeMove
            // 
            this.colTimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.Caption = "����� ��������";
            this.colTimeMove.FieldName = "TimeMove";
            this.colTimeMove.Name = "colTimeMove";
            this.colTimeMove.OptionsColumn.AllowEdit = false;
            this.colTimeMove.Visible = true;
            this.colTimeMove.VisibleIndex = 6;
            this.colTimeMove.Width = 138;
            // 
            // colTimeStop
            // 
            this.colTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.Caption = "����� �������";
            this.colTimeStop.FieldName = "TimeStop";
            this.colTimeStop.Name = "colTimeStop";
            this.colTimeStop.OptionsColumn.AllowEdit = false;
            this.colTimeStop.Visible = true;
            this.colTimeStop.VisibleIndex = 7;
            this.colTimeStop.Width = 161;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcReport;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bleGroup,
            this.bleItem,
            this.bbiFilterClear,
            this.beiMinSpeed,
            this.beiMaxSpeed});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leGroup,
            this.leItem,
            this.rteMinSpeed,
            this.rteMaxSpeed});
            // 
            // bar2
            // 
            this.bar2.BarName = "������� ����";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleGroup, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bleItem, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiFilterClear, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.beiMinSpeed, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.beiMaxSpeed, DevExpress.XtraBars.BarItemPaintStyle.Caption)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "������� ����";
            // 
            // bleGroup
            // 
            this.bleGroup.Caption = "������ �����";
            this.bleGroup.Edit = this.leGroup;
            this.bleGroup.Id = 0;
            this.bleGroup.Name = "bleGroup";
            this.bleGroup.Width = 188;
            this.bleGroup.EditValueChanged += new System.EventHandler(this.bleGroup_EditValueChanged);
            // 
            // leGroup
            // 
            this.leGroup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroup.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leGroup.DisplayMember = "Name";
            this.leGroup.DropDownRows = 30;
            this.leGroup.Name = "leGroup";
            this.leGroup.NullText = "";
            this.leGroup.ShowHeader = false;
            this.leGroup.ValueMember = "Id";
            // 
            // bleItem
            // 
            this.bleItem.Caption = "������";
            this.bleItem.Edit = this.leItem;
            this.bleItem.Id = 1;
            this.bleItem.Name = "bleItem";
            this.bleItem.Width = 265;
            // 
            // leItem
            // 
            this.leItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leItem.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Info", "Name")});
            this.leItem.DisplayMember = "Info";
            this.leItem.DropDownRows = 30;
            this.leItem.Name = "leItem";
            this.leItem.NullText = "";
            this.leItem.ShowHeader = false;
            this.leItem.ValueMember = "Id";
            // 
            // bbiFilterClear
            // 
            this.bbiFilterClear.Caption = "�������� ������";
            this.bbiFilterClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFilterClear.Glyph")));
            this.bbiFilterClear.Id = 2;
            this.bbiFilterClear.Name = "bbiFilterClear";
            this.bbiFilterClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFilterClear_ItemClick);
            // 
            // beiMinSpeed
            // 
            this.beiMinSpeed.Caption = "����������� �������� ��/�";
            this.beiMinSpeed.Edit = this.rteMinSpeed;
            this.beiMinSpeed.Id = 4;
            this.beiMinSpeed.Name = "beiMinSpeed";
            this.beiMinSpeed.EditValueChanged += new System.EventHandler(this.beiMinSpeed_EditValueChanged);
            // 
            // rteMinSpeed
            // 
            this.rteMinSpeed.Appearance.Options.UseTextOptions = true;
            this.rteMinSpeed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rteMinSpeed.AutoHeight = false;
            this.rteMinSpeed.Name = "rteMinSpeed";
            // 
            // beiMaxSpeed
            // 
            this.beiMaxSpeed.Caption = "������������ �������� ��/�";
            this.beiMaxSpeed.Edit = this.rteMaxSpeed;
            this.beiMaxSpeed.Id = 5;
            this.beiMaxSpeed.Name = "beiMaxSpeed";
            this.beiMaxSpeed.EditValueChanged += new System.EventHandler(this.beiMaxSpeed_EditValueChanged);
            // 
            // rteMaxSpeed
            // 
            this.rteMaxSpeed.Appearance.Options.UseTextOptions = true;
            this.rteMaxSpeed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rteMaxSpeed.AutoHeight = false;
            this.rteMaxSpeed.Name = "rteMaxSpeed";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1193, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 470);
            this.barDockControlBottom.Size = new System.Drawing.Size(1193, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 446);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1193, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 446);
            // 
            // RepSpeedControlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcReport);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "RepSpeedControlView";
            this.Size = new System.Drawing.Size(1193, 470);
            ((System.ComponentModel.ISupportInitialize)(this.gvReportDetal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteMinSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteMaxSpeed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn colFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegime;
        private DevExpress.XtraGrid.Columns.GridColumn colRegimeDistance;
        public DevExpress.XtraGrid.GridControl gcReport;
        public DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Columns.GridColumn colRegimePercent;
        private DevExpress.XtraGrid.Views.Grid.GridView gvReportDetal;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colAgregateName;
        private DevExpress.XtraGrid.Columns.GridColumn colSqFactGa;
        private DevExpress.XtraGrid.Columns.GridColumn colSqAfterRecalc;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarEditItem bleGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leGroup;
        private DevExpress.XtraBars.BarEditItem bleItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leItem;
        private DevExpress.XtraBars.BarButtonItem bbiFilterClear;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStop;
        private DevExpress.XtraBars.BarEditItem beiMinSpeed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteMinSpeed;
        private DevExpress.XtraBars.BarEditItem beiMaxSpeed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteMaxSpeed;
    }
}
