﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Agro.Reports.OrderReports
{
    public class RepSpeedControlFields
    {
        public int FieldId { get; set; }
        public string FieldName { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public TimeSpan TimeMove { get; set; }
        public TimeSpan TimeStop { get; set; }
        public int PontsMin { get; set; }
        public int PontsAvg { get; set; }
        public int PontsMax { get; set; }
        public double Distance { get; set; }
        public string RegimeName { get; set; }
        public int RegimePonts{ get; set; }
        public double RegimeDistance { get; set; }
        public double RegimePercent { get; set; }
    }
}
