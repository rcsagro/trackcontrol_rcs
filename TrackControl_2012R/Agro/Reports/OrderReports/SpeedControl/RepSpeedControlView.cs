using System;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid ;
using TrackControl.General;
using Agro.Properties;
using TrackControl.Vehicles;

namespace Agro.Reports.OrderReports
{
    public partial class RepSpeedControlView : DevExpress.XtraEditors.XtraUserControl
    {
        public DateTime Begin { get; set; }

        public DateTime End { get; set; }

        public int Group
        {
            get
            {
                if (bleGroup.EditValue == null)
                    return 0;
                else
                    return (int)bleGroup.EditValue;
            }
        }

        public int Vehicle
        {
            get
            {
                if (bleItem.EditValue == null)
                    return 0;
                else
                    return (int)bleItem.EditValue;
            }
        }

        public double SpeedMin
        {
            get
            {
                double par = 0;
                if (Double.TryParse(beiMinSpeed.EditValue.ToString(), out par))
                {
                    return par;
                }
                else
                    return 0;
            }
        }

        public double SpeedMax
        {
            get
            {
                double par = 0;
                if (Double.TryParse(beiMaxSpeed.EditValue.ToString(), out par))
                {
                    return  par;
                }
                else
                    return 0;
            }
        }


        public RepSpeedControlView()
        {
            InitializeComponent();
            Localization();
            leGroup.DataSource =  DocItem.VehiclesModel.RootGroupsWithItems();
            beiMinSpeed.EditValue = Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMin, Consts.SPEED_MIN.ToString()).Replace(".",","));
            beiMaxSpeed.EditValue = Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMax, Consts.SPEED_MAX.ToString()).Replace(".", ","));
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(gvReport, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {

            bleGroup.Caption = Resources.CarGroupe;
            bleItem.Caption = Resources.Car;
            beiMinSpeed.Caption = Resources.MinSpeedKmH;
            beiMaxSpeed.Caption = Resources.MaxSpeedKmH;
            colVehicle.Caption = Resources.Vehicle;
            colFieldName.Caption = Resources.Field;
            colRegimeDistance.Caption = Resources.Run;
            colTimeMove.Caption = Resources.TimeMoveH;
            colTimeStop.Caption = Resources.TimeStopH;
            colRegime.Caption = Resources.SpeedRegime;
            colRegimePercent.Caption = Resources.Share;
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = Resources.SpeedControl;
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            Begin.ToString("dd.MM.yyyy"), End.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);

        }



        private void bbiFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleGroup.EditValue = null;
            bleItem.EditValue = null; 
        }


        private void bleGroup_EditValueChanged(object sender, EventArgs e)
        {
            var groupe = (VehiclesGroup)leGroup.GetDataSourceRowByKeyValue(bleGroup.EditValue);
            leItem.DataSource = groupe == null ? null : groupe.OwnRealItems();
        }

        private void beiMinSpeed_EditValueChanged(object sender, EventArgs e)
        {
            double par = 0;
            if (Double.TryParse(beiMinSpeed.EditValue.ToString(),out par))
            {
                Params.SetParTotal(Params.NUMPAR.SpeedMin, par.ToString());
            }
            
        }

        private void beiMaxSpeed_EditValueChanged(object sender, EventArgs e)
        {
            double par = 0;
            if (Double.TryParse(beiMaxSpeed.EditValue.ToString(), out par))
            {
                Params.SetParTotal(Params.NUMPAR.SpeedMax, par.ToString());
            }
        }
    }
}
