﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General.DatabaseDriver;

namespace Agro
{
    public class AgroQuery
    {
        private static int TypeDataBaseForUsing = DriverDb.TypeDataBaseUsing;
        private const int MySqlUse = 0;
        private const int MssqlUse = 1;

        // глобальные запросы
        private const string mySqlvehicleIdent =
            "CONCAT(IFNULL(vehicle.NumberPlate,''),' | ' , IFNULL(vehicle.CarModel,''),' | ' , IFNULL(vehicle.MakeCar,''))";

        private const string msSqlvehicleIdent =
            "convert(VARCHAR, ISNULL(vehicle.NumberPlate,'')) + ' | ' + convert(VARCHAR, ISNULL(vehicle.CarModel,'')) + ' | ' + convert(varchar, ISNULL(vehicle.MakeCar,''))";

        private const string _sSQLinsertHead =
            "INSERT INTO `agro_calct`(Id_main, Lon, Lat, Selected, Border, BorderNode, Number) VALUES ";

        private const string _mSsSQLinsertHead =
            "INSERT INTO agro_calct(Id_main, Lon, Lat, Selected, Border, BorderNode, Number) VALUES ";

        public static string SqlVehicleIdent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return mySqlvehicleIdent;

                    case MssqlUse:
                        return msSqlvehicleIdent;
                }

                return "";
            }
        }

        public class MainAgro
        {
            public static string SelectAgroPrice
            {
                get
                {
                    return
                        "SELECT agro_price.Id, agro_price.DateComand as DateOrder FROM agro_price ORDER BY  agro_price.DateComand DESC";
                }
            }

            public static string SelectVehicleMobitel
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + mySqlvehicleIdent + " AS Name FROM vehicle";

                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id as Id, " + msSqlvehicleIdent + " AS Name FROM vehicle";
                    }

                    return "";
                }
            }
        } // Agro_MainAgro

        public class AgroMap
        {
            public static string SelectAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + mySqlvehicleIdent +
                                   @" as Name,driver.Family,
                                    agro_order.`Date`, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND 
                                    (agro_order.`Date` >= ?start and agro_order.`Date` < ?end)
                                    ORDER BY agro_order.`Date`, " + mySqlvehicleIdent;

                        case MssqlUse:
                            return @"SELECT  agro_order.Id,agro_ordert.Id as Id_ordert, " + msSqlvehicleIdent +
                                   @" as Name,driver.Family,
                                    agro_order.Date, agro_ordert.FactSquareCalc, agro_order.Id_mobitel,agro_agregat.Width,agro_work.Id as WorkId,agro_work.Name as WorkName
                                    FROM    agro_ordert
                                    INNER JOIN  agro_order ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_work ON agro_ordert.Id_work = agro_work.Id
                                    LEFT OUTER JOIN  agro_agregat  ON agro_agregat.Id = agro_ordert.Id_agregat 
                                    INNER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID
                                    INNER JOIN vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id 
                                    WHERE   agro_ordert.Id_zone = {0} AND (agro_ordert.Confirm = 1) AND
                                    (agro_order.Date >= @start and agro_order.Date < @end)
                                    ORDER BY agro_order.Date, " + msSqlvehicleIdent;
                    }

                    return "";
                }
            }
        } // AgroMap

        public class AgroMapOrder
        {
            public static string SelectAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT   agro_ordert.Id,agro_ordert.Id_zone,IF(agro_ordert.Id_zone= {2},'{3}', IFNULL(agro_field.Name, '{0}')) AS FName, driver.Family, agro_ordert.TimeStart, agro_ordert.TimeEnd,
                                    TIME_Format(TIMEDIFF(agro_ordert.TimeEnd, agro_ordert.TimeStart), '%H:%i') as Time,
                                    agro_ordert.Id_work ,agro_ordert.Distance,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.FactSquareCalc) as FactSquareCalc,
                                    agro_ordert.Confirm,agro_agregat.Width,LockRecord,PointsValidity
                                    FROM agro_ordert 
                                    LEFT OUTER JOIN  agro_field ON agro_ordert.Id_field = agro_field.Id 
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id 
                                    LEFT OUTER JOIN  agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id 
                                    WHERE agro_ordert.Id_main = {1} ORDER BY TimeStart,TimeEnd";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert.Id,agro_ordert.Id_zone, (CASE WHEN agro_ordert.Id_zone = {2} then '{3}' else ISNULL(agro_field.Name, '{0}')END) AS FName, 
                                    driver.Family, agro_ordert.TimeStart, agro_ordert.TimeEnd,
                                    dbo.get_hh_min(DATEDIFF(mi, agro_ordert.TimeStart, agro_ordert.TimeEnd)) as Time,
                                    agro_ordert.Id_work ,agro_ordert.Distance,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FactSquareCalc end) as FactSquareCalc,
                                    agro_ordert.Confirm,agro_agregat.Width,LockRecord,PointsValidity
                                    FROM agro_ordert 
                                    LEFT OUTER JOIN  agro_field ON agro_ordert.Id_field = agro_field.Id 
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id 
                                    LEFT OUTER JOIN  agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id 
                                    WHERE agro_ordert.Id_main = {1} ORDER BY TimeStart,TimeEnd";
                    }

                    return "";
                }
            }
        } // AgroMapOrder

        public class DictionaryAgroAgregat
        {
            public static string InsertIntoAgroAgregatIdent
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_agregat`(Name,Width,Identifier,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId,HasSensor) 
                                    VALUES (?Name,?Width,?Identifier,?Def,?Comment,?Id_main,?InvNumber,?Id_work,0,?OutLinkId,?HasSensor)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_agregat(Name,Width,Identifier,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId,HasSensor) 
                                    VALUES (@Name,@Width,@Identifier,@Def,@Comment,@Id_main,@InvNumber,@Id_work,0,@OutLinkId,@HasSensor)";
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_agregat`(Name,Width,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId,HasSensor) 
                                    VALUES (?Name,?Width,?Def,?Comment,?Id_main,?InvNumber,?Id_work,0,?OutLinkId,?HasSensor)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_agregat(Name,Width,Def,Comment,Id_main,InvNumber,Id_work,IsGroupe,OutLinkId,HasSensor) 
                                    VALUES (@Name,@Width,@Def,@Comment,@Id_main,@InvNumber,@Id_work,0,@OutLinkId,@HasSensor)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregatIdent
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_agregat SET Name = ?Name ,Width = ?Width ,Identifier = ?Identifier ,Def = ?Def," +
                                "Comment = ?Comment,Id_main = ?Id_main,InvNumber = ?InvNumber,Id_work = ?Id_work ,OutLinkId=?OutLinkId , HasSensor=?HasSensor WHERE (ID = " +
                                "{0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_agregat SET Name = @Name ,Width = @Width ,Identifier = @Identifier ,Def = @Def," +
                                "Comment = @Comment,Id_main = @Id_main,InvNumber = @InvNumber,Id_work = @Id_work ,OutLinkId=@OutLinkId , HasSensor=@HasSensor WHERE (ID = " +
                                "{0})";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_agregat SET Name = ?Name ,Width = ?Width ,Def = ?Def," +
                                   "Comment = ?Comment,Id_main = ?Id_main,InvNumber = ?InvNumber,Id_work = ?Id_work ,OutLinkId=?OutLinkId , HasSensor=?HasSensor WHERE (ID = " +
                                   "{0})";

                        case MssqlUse:
                            return "UPDATE agro_agregat SET Name = @Name ,Width = @Width ,Def = @Def," +
                                   "Comment = @Comment,Id_main = @Id_main,InvNumber = @InvNumber,Id_work = @Id_work ,OutLinkId=@OutLinkId , HasSensor=@HasSensor WHERE (ID = " +
                                   "{0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroAgregatVehicle
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_agregat_vehicle.Id , team.id as TID, vehicle.id as VID, sensors.id AS SID
                                    FROM agro_agregat_vehicle
                                    INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                                    LEFT OUTER JOIN team ON team.id = vehicle.Team_id
                                    LEFT OUTER JOIN sensors ON agro_agregat_vehicle.Id_sensor = sensors.id 
                                    WHERE   agro_agregat_vehicle.Id_agregat = {0}
                                    ORDER BY team.Name,vehicle.MakeCar";

                        case MssqlUse:
                            return
                                @"SELECT agro_agregat_vehicle.Id, team.id as [TID], vehicle.id as [VID], sensors.id AS [SID]
                                    FROM agro_agregat_vehicle
                                    INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                                    LEFT OUTER JOIN team ON team.id = vehicle.Team_id
                                    LEFT OUTER JOIN sensors ON agro_agregat_vehicle.Id_sensor = sensors.id 
                                    WHERE   agro_agregat_vehicle.Id_agregat = {0}
                                    ORDER BY team.Name,vehicle.MakeCar";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregatVehicle
            {
                get
                {
                    return "UPDATE agro_agregat_vehicle SET Id_vehicle = {1}, Id_sensor = {2} WHERE (ID = {0})";
                }
            }

            public static string InsertIntoAgroAgregatVehicle
            {
                get
                {
                    return "INSERT INTO agro_agregat_vehicle (Id_agregat,Id_vehicle,Id_sensor) VALUES ({0},{1},{2})";
                }
            }

            public static string DeleteAgroAgregatVehicle
            {
                get
                {
                    return "DELETE FROM agro_agregat_vehicle WHERE (ID = {0})";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id, agro_agregat.Name FROM agro_agregat WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroAgregatJornal
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_agregat.Id
                            , agro_agregat.Id_main
                            , agro_agregat.Name 
                            , agro_agregat.InvNumber 
                            , agro_agregat.Width 
                            , agro_agregat.Identifier
                            , agro_agregat.Def 
                            , (SELECT " + mySqlvehicleIdent + @" FROM agro_agregat_vehicle   
                            INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                            INNER JOIN team ON vehicle.Team_id = team.id
                            WHERE agro_agregat_vehicle.Id_agregat = agro_agregat.Id ORDER BY team.Name,vehicle.MakeCar LIMIT 1) as VehicleAgr
                            , agro_agregat.Comment
                            ,agro_agregat.Id_work
                            FROM   agro_agregat 
                            WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";

                        case MssqlUse:
                            return @"SELECT agro_agregat.Id
                            , agro_agregat.Id_main
                            , agro_agregat.Name 
                            , agro_agregat.InvNumber 
                            , agro_agregat.Width 
                            , agro_agregat.Identifier
                            , agro_agregat.Def 
                            , (SELECT TOP 1 " + msSqlvehicleIdent + @" FROM agro_agregat_vehicle   
                            INNER JOIN vehicle     ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                            INNER JOIN team ON vehicle.Team_id = team.id
                            WHERE agro_agregat_vehicle.Id_agregat = agro_agregat.Id ORDER BY team.Name,vehicle.MakeCar) as VehicleAgr
                            , agro_agregat.Comment
                            , agro_agregat.Id_work
                            FROM   agro_agregat 
                            WHERE agro_agregat.IsGroupe = {0} ORDER BY agro_agregat.Name";
                    }

                    return "";
                }
            }
        } // DictionaryAgroAgregat

        public class DictionaryAgroAgregatGrp
        {
            public static string InsertIntoAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_agregat`(Name, Comment, Id_work, IsGroupe, OutLinkId) VALUES (?Name, ?Comment, ?Id_work, 1, ?OutLinkId)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_agregat(Name, Comment, Id_work, IsGroupe, OutLinkId) VALUES (@Name, @Comment, @Id_work, 1, @OutLinkId)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroAgregat
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_agregat SET Name = ?Name ,Comment = ?Comment,Id_work = ?Id_work,OutLinkId=?OutLinkId WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_agregat SET Name = @Name ,Comment = @Comment,Id_work = @Id_work,OutLinkId=@OutLinkId WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroAgregatItem
            {
                get
                {
                    return "DELETE FROM agro_agregat WHERE ID = {0}";
                }
            }

            public static string SelectAgroAgregatCount
            {
                get
                {
                    return "SELECT COUNT(agro_agregat.Id) FROM agro_agregat WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroAgregatId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_agregat.Id FROM agro_agregat WHERE agro_agregat.Name = ?Name AND agro_agregat.Id <> {0} AND IsGroupe = 1";

                        case MssqlUse:
                            return
                                @"SELECT agro_agregat.Id FROM agro_agregat WHERE agro_agregat.Name = @Name AND agro_agregat.Id <> {0} AND IsGroupe = 1";
                    }

                    return "";
                }
            }

            public static string SelectAgroAgregatList
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id, agro_agregat.Name FROM agro_agregat WHERE agro_agregat.IsGroupe = 1 ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroAgregatJornal
            {
                get
                {
                    return
                        "SELECT agro_agregat.Id, agro_agregat.Name ,agro_agregat.Id_work,agro_agregat.Comment FROM agro_agregat WHERE agro_agregat.IsGroupe = 1 ORDER BY agro_agregat.Name";
                }
            }
        } // DictionaryAgroAgregatGrp

        public class DictionaryAgroCommand
        {
            public static string InsertIntoAgroPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_price`(Name, DateComand, DateInit, Comment) VALUES (?Name, ?DateComand, ?DateInit, ?Comment)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_price(Name, DateComand, DateInit, Comment) VALUES (@Name, @DateComand, @DateInit, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_price SET Name = ?Name, DateInit = ?DateInit, DateComand = ?DateComand, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_price SET Name = @Name, DateInit = @DateInit, DateComand = @DateComand, Comment = @Comment WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroPrice
            {
                get
                {
                    return "DELETE FROM agro_price WHERE ID = {0}";
                }
            }

            public static string SelectAgroPricet
            {
                get
                {
                    return "SELECT COUNT(agro_pricet.Id) FROM agro_pricet WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroPriceJornal
            {
                get
                {
                    return "SELECT Id, DateInit, DateComand, Name, Comment FROM agro_price";
                }
            }
        } // DictionaryAgroCommand

        public class DictionaryAgroCulture
        {
            public static string InsertIntoAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_culture`(Name, Comment, Icon) VALUES (?Name, ?Comment, ?Icon)";

                        case MssqlUse:
                            return "INSERT INTO agro_culture(Name, Comment, Icon) VALUES (@Name, @Comment, @Icon)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_culture SET Name = ?Name, Icon = ?Icon, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_culture SET Name = @Name, Icon = @Icon, Comment = @Comment WHERE (ID = {0})";
                            ;
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroCulture
            {
                get
                {
                    return "DELETE FROM agro_culture WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroField
            {
                get
                {
                    return "SELECT COUNT(agro_field.Id) FROM agro_field WHERE Id_main  = {0}";
                }
            }

            public static string SelectAgroCulture
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_culture.Id FROM agro_culture WHERE agro_culture.Name = ?Name AND agro_culture.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_culture.Id FROM agro_culture WHERE agro_culture.Name = @Name AND agro_culture.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroCultureJornal
            {
                get
                {
                    return "SELECT Id, Name, Icon, Comment FROM agro_culture ORDER BY Name";
                }
            }
        } // DictionaryAgroCulture

        public class DictionaryAgroField
        {
            public static string InsertIntoAgroField
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_field`(Id_main, Id_zone, Name, Comment) VALUES (?Id_main, ?Id_zone, ?Name, ?Comment)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_field(Id_main, Id_zone, Name, Comment) VALUES (@Id_main, @Id_zone, @Name, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroField
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_field SET Id_main = ?Id_main, Id_zone = ?Id_zone, Name = ?Name, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_field SET Id_main = @Id_main, Id_zone = @Id_zone, Name = @Name, Comment = @Comment WHERE (ID ={0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroField
            {
                get
                {
                    return "DELETE FROM agro_field WHERE ID = {0}";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return "SELECT COUNT(agro_OrderT.Id) FROM agro_OrderT WHERE Id_field = {0}";
                }
            }

            public static string SelectAgroOrdertControl
            {
                get
                {
                    return
                        "SELECT COUNT(agro_ordert_control.Id) FROM agro_ordert_control WHERE Id_object  = {0} AND Type_object = {1}";
                }
            }

            public static string SelectAgroField
            {
                get
                {
                    return "SELECT agro_field.Id, agro_field.Name FROM agro_field ORDER BY agro_field.Name";
                }
            }

            public static string SelectAgroFieldZone
            {
                get
                {
                    return "SELECT agro_field.Id FROM agro_field WHERE agro_field.Id_zone = {0}";
                }
            }

            public static string SelectAgroFieldJornal
            {
                get
                {
                    return
                        @"SELECT agro_field.Id, agro_field.Id_main as FGROUPE, agro_field.Id_zone as FZONE, agro_field.Name, 100 * zones.Square as Square, agro_field.Comment FROM agro_field LEFT OUTER JOIN zones ON agro_field.Id_zone = zones.Zone_ID ";
                }
            }
        } // DictionaryAgroField

        public class DictionaryAgroFieldGrp
        {
            public static string SelectAgroFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_fieldgroupe`(Name, Comment, Id_Zonesgroup) VALUES (?Name, ?Comment, ?Id_Zonesgroup)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_fieldgroupe(Name, Comment, Id_Zonesgroup) VALUES (@Name, @Comment, @Id_Zonesgroup)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_fieldgroupe SET Name = ?Name, Comment = ?Comment, Id_Zonesgroup = ?Id_Zonesgroup WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_fieldgroupe SET Name = @Name, Comment = @Comment, Id_Zonesgroup = @Id_Zonesgroup WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroFieldGroup
            {
                get
                {
                    return "DELETE FROM agro_fieldgroupe WHERE ID = {0}";
                }
            }

            public static string SelectCountAgroField
            {
                get
                {
                    return "SELECT COUNT(agro_field.Id) FROM agro_field WHERE Id_main  = {0}";
                }
            }

            public static string SelectAgroFromFieldGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_fieldgroupe.Id FROM agro_fieldgroupe WHERE agro_fieldgroupe.Name = ?Name AND agro_fieldgroupe.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_fieldgroupe.Id FROM agro_fieldgroupe WHERE agro_fieldgroupe.Name = @Name AND agro_fieldgroupe.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectFromZones
            {
                get
                {
                    return "SELECT zones.Zone_ID, zones.Name FROM zones WHERE ZonesGroupId = {0}";
                }
            }

            public static string SelectFromZonesJoin
            {
                get
                {
                    return @"SELECT  zones.Zone_ID, zones.Name
                            FROM agro_field
                            RIGHT OUTER JOIN zones
                            ON agro_field.Id_zone = zones.Zone_ID
                            WHERE zones.ZonesGroupId = {0} AND agro_field.Id IS NULL";
                }
            }

            public static string SelectFromAgroFieldJoin
            {
                get
                {
                    return @"SELECT agro_field.Id
                            FROM agro_field LEFT OUTER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                            WHERE agro_field.Id_main = {0} AND zones.Zone_ID IS NULL";
                }
            }

            public static string SelectFromAgroFieldGroupe
            {
                get
                {
                    return "SELECT Id, Name, Comment FROM  agro_fieldgroupe ORDER BY Name";
                }
            }

            public static string SelectAgroFieldGroupeList
            {
                get
                {
                    return
                        "SELECT agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM agro_fieldgroupe ORDER BY  agro_fieldgroupe.Name";
                    ;
                }
            }

            public static string SelectZones
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT zones.ZonesGroupId, count(zones.Zone_ID) AS Cnt
                                    FROM agro_field INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                                    WHERE agro_field.Id_main = {0} GROUP BY zones.ZonesGroupId
                                    ORDER BY Cnt DESC LIMIT 1";

                        case MssqlUse:
                            return @"SELECT TOP 1 zones.ZonesGroupId, count(zones.Zone_ID) AS Cnt
                                    FROM agro_field INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID
                                    WHERE agro_field.Id_main = {0} GROUP BY zones.ZonesGroupId
                                    ORDER BY Cnt DESC";
                    }

                    return "";
                }
            }
        } // DictionaryAgroFieldGrp

        public class DictionaryAgroPrice
        {
            public static string InsertIntoAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_pricet`(Id_main, Id_work, Id_mobitel, Id_agregat, Id_unit, Price,Comment) "
                                   +
                                   " VALUES (?Id_main, ?Id_work, ?Id_mobitel, ?Id_agregat, ?Id_unit, ?Price, ?Comment)";

                        case MssqlUse:
                            return "INSERT INTO agro_pricet(Id_main, Id_work, Id_mobitel, Id_agregat, Id_unit, Price, Comment) "
                                   +
                                   " VALUES (@Id_main, @Id_work, @Id_mobitel, @Id_agregat, @Id_unit, @Price, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_pricet SET Id_main = ?Id_main, Id_work = ?Id_work, Id_mobitel = ?Id_mobitel"
                                   +
                                   ", Id_agregat = ?Id_agregat, Id_unit = ?Id_unit, Price = ?Price, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_pricet SET Id_main = @Id_main, Id_work = @Id_work, Id_mobitel = @Id_mobitel" +
                                ", Id_agregat = @Id_agregat, Id_unit = @Id_unit, Price = @Price, Comment = @Comment WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroPricet
            {
                get
                {
                    return "DELETE FROM agro_pricet WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroPricet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.`Comment`"
                                   + " FROM agro_pricet";

                        case MssqlUse:
                            return "SELECT agro_pricet.Id, agro_pricet.Id_main, agro_pricet.Id_work, agro_pricet.Price, agro_pricet.Id_unit, agro_pricet.Id_mobitel, agro_pricet.Id_agregat, agro_pricet.Comment"
                                   + " FROM agro_pricet";
                    }

                    return "";
                }
            }
        } // DictionaryAgroPrice

        public class DictionaryAgroUnit
        {
            public static string InsertIntoAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "INSERT INTO `agro_unit`(Name, NameShort, Factor, Comment) VALUES (?Name, ?NameShort, ?Factor, ?Comment)";

                        case MssqlUse:
                            return
                                "INSERT INTO agro_unit(Name, NameShort, Factor, Comment) VALUES (@Name, @NameShort, @Factor, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "UPDATE agro_unit SET Name = ?Name, NameShort = ?NameShort, Comment = ?Comment, Factor = ?Factor WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                "UPDATE agro_unit SET Name = @Name, NameShort = @NameShort, Comment = @Comment, Factor = @Factor WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroUnit
            {
                get
                {
                    return "DELETE FROM agro_unit WHERE ID = {0}";
                }
            }

            public static string SelectAgroUnitCount
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE `Id_unit` = {0}";

                        case MssqlUse:
                            return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE Id_unit = {0}";
                    }

                    return "";
                }
            }

            public static string SelectFromAgroUnit
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_unit.Id FROM agro_unit WHERE agro_unit.Name = ?Name AND agro_unit.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_unit.Id FROM agro_unit WHERE agro_unit.Name = @Name AND agro_unit.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroUnitList
            {
                get
                {
                    return
                        "SELECT agro_unit.Id, agro_unit.NameShort as Name FROM agro_unit ORDER BY agro_unit.NameShort";
                }
            }

            public static string SelectAgroUnitJornal
            {
                get
                {
                    return "SELECT Id, Name ,NameShort , Factor, Comment  FROM  agro_unit ORDER BY Name";
                }
            }
        } // DictionaryAgroUnit

        public class DictionaryAgroWorkType
        {
            public static string UpdateAgroWork
            {
                get
                {
                    return
                        "UPDATE agro_work SET Name = {1}Name, SpeedBottom = {1}SpeedBottom, Comment = {1}Comment, SpeedTop = {1}SpeedTop, TypeWork = {1}TypeWork, Id_main = {1}Id_main, OutLinkId = {1}OutLinkId  WHERE (ID = {0})";
                }
            }

            public static string DeleteAgroWork
            {
                get
                {
                    return "DELETE FROM agro_work WHERE ID = {0}";
                }
            }

            public static string SelectAgroPriceT
            {
                get
                {
                    return "SELECT COUNT(agro_PriceT.Id) FROM agro_PriceT WHERE Id_work = {0}";
                }
            }

            public static string SelectAgroOrderT
            {
                get
                {
                    return "SELECT COUNT(agro_OrderT.Id) FROM agro_OrderT WHERE Id_work = {0}";
                }
            }

            public static string SelectFromAgroWork
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_work.Id FROM agro_work WHERE agro_work.Name = ?Name AND agro_work.Id <> {0}";

                        case MssqlUse:
                            return
                                "SELECT agro_work.Id FROM agro_work WHERE agro_work.Name = @Name AND agro_work.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkList
            {
                get
                {
                    return "SELECT agro_work.Id, agro_work.Name FROM agro_work ORDER BY agro_work.Name";
                }
            }

            public static string SelectAgroWorkJornal
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work_types.Name as TypeWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.`Comment`
                                    FROM agro_work
                                    INNER JOIN agro_work_types
                                    ON agro_work.TypeWork = agro_work_types.Id
                                    LEFT OUTER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY Name";

                        case MssqlUse:
                            return @"SELECT agro_work.Id, 
                                    agro_workgroup.Name as GroupWork,
                                    agro_work_types.Name as TypeWork,
                                    agro_work.Name,
                                    agro_work.SpeedBottom,
                                    agro_work.SpeedTop,
                                    agro_work.Comment
                                    FROM agro_work
                                    INNER JOIN agro_work_types
                                    ON agro_work.TypeWork = agro_work_types.Id
                                    LEFT OUTER JOIN agro_workgroup
                                    ON agro_work.Id_main = agro_workgroup.Id ORDER BY Name";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkTypes
            {
                get
                {
                    return
                        " SELECT agro_work_types.Id, agro_work_types.Name FROM agro_work_types ORDER BY agro_work_types.Name";
                }
            }

            public static string SelectAgroWorkTypeWork
            {
                get
                {
                    return "SELECT agro_work.Id FROM agro_work WHERE agro_work.TypeWork = {0}";
                }
            }
        } // DictionaryAgroWorkType

        public class DictionaryAgroWorkTypeGroup
        {
            public static string SelectAgroWorkGroup
            {
                get
                {
                    return "SELECT agro_workgroup.* FROM agro_workgroup WHERE agro_workgroup.Id = {0}";
                }
            }

            public static string InsertIntoAgroWorkGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO `agro_workgroup`(Name, Comment) VALUES (?Name, ?Comment)";

                        case MssqlUse:
                            return "INSERT INTO agro_workgroup(Name, Comment) VALUES (@Name, @Comment)";
                    }

                    return "";
                }
            }

            public static string UpdateAgroWorkGroup
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_workgroup SET Name = ?Name, Comment = ?Comment WHERE (ID = {0})";

                        case MssqlUse:
                            return "UPDATE agro_workgroup SET Name = @Name, Comment = @Comment WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteAgroWorkGroup
            {
                get
                {
                    return "DELETE FROM agro_workgroup WHERE ID = {0}";
                }
            }

            public static string SelectFromAgroWorkGroup
            {
                get
                {
                    return "SELECT COUNT(agro_work.Id) FROM agro_work WHERE Id_main = {0}";
                }
            }

            public static string SelectAgroWorkGroupId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_workgroup.Id FROM agro_workgroup WHERE agro_workgroup.Name = ?Name AND agro_workgroup.Id <> {0}";

                        case MssqlUse:
                            return
                                @"SELECT agro_workgroup.Id FROM agro_workgroup WHERE agro_workgroup.Name = @Name AND agro_workgroup.Id <> {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroWorkGroupList
            {
                get
                {
                    return
                        "SELECT agro_workgroup.Id, agro_workgroup.Name FROM agro_workgroup ORDER BY agro_workgroup.Name";
                }
            }

            public static string SelectAgroWorkGroupJornal
            {
                get
                {
                    return
                        "SELECT agro_workgroup.Id, agro_workgroup.Name ,agro_workgroup.Comment FROM agro_workgroup ORDER BY agro_workgroup.Name";
                }
            }
        } // DictionaryAgroWorkTypeGroup

        public class DictionaryItem
        {
            public static string SelectFromAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DISTINCT agro_order.Id, 
                                    agro_order.`Date`, 
                                    {0} as Vehicle, 
                                    agro_order.SquareWorkDescript
                                    FROM agro_ordert
                                    INNER JOIN agro_order
                                    ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle
                                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert.{3} = {1}
                                    UNION SELECT DISTINCT agro_order.Id, agro_order.`Date`, {0} as Vehicle, 
                                    agro_order.SquareWorkDescript FROM agro_ordert_control
                                    INNER JOIN agro_order ON agro_ordert_control.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert_control.Type_object = {2}
                                    AND agro_ordert_control.Id_object = {1} ORDER BY ID";

                        case MssqlUse:
                            return @"SELECT DISTINCT agro_order.Id, 
                                    agro_order.Date, {0} as Vehicle, 
                                    agro_order.SquareWorkDescript
                                    FROM agro_ordert
                                    INNER JOIN agro_order
                                    ON agro_ordert.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle
                                    ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert.{3} = {1} UNION
                                    SELECT DISTINCT agro_order.Id, agro_order.Date, 
                                    {0} as Vehicle, agro_order.SquareWorkDescript
                                    FROM agro_ordert_control INNER JOIN agro_order
                                    ON agro_ordert_control.Id_main = agro_order.Id
                                    LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_ordert_control.Type_object = {2}
                                    AND agro_ordert_control.Id_object = {1}
                                    ORDER BY ID";
                    }

                    return "";
                }
            }
        } // DictionaryItem

        public class Agregat
        {
            public static string SelectSensors
            {
                get
                {
                    return @"SELECT sensors.id , sensors.Name , vehicle.id as VID
                            FROM  sensors
                            INNER JOIN vehicle ON sensors.mobitel_id = vehicle.Mobitel_id 
                            WHERE   sensors.Length = 1 
                            ORDER BY sensors.Name";
                }
            }

            public static string SelectVehicle
            {
                get
                {
                    return
                        String.Format("SELECT vehicle.id as Id, {0} as Name, vehicle.Team_id FROM vehicle ORDER BY {0}",
                            DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse ? msSqlvehicleIdent : mySqlvehicleIdent);
                }
            }
        } // Agregat

        public class Price
        {
            public static string SelectAgroPrice
            {
                get
                {
                    return
                        "SELECT agro_price.Id, DATE_FORMAT(agro_price.DateComand, '%d.%m.%Y') as DateComand FROM agro_price ORDER BY agro_price.DateComand DESC";
                }
            }

            public static string SelectAgroWork
            {
                get
                {
                    return "SELECT agro_work.Id, agro_work.Name FROM agro_work ORDER BY agro_work.Name";
                }
            }

            public static string SelectVehicleId
            {
                get
                {
                    return "SELECT vehicle.Mobitel_id as Id, " + AgroQuery.SqlVehicleIdent +
                           " AS Namr, vehicle.Team_id FROM vehicle";
                }
            }

            public static string SelectAgroAgregat
            {
                get
                {
                    return "SELECT agro_agregat.Id, agro_agregat.Name FROM agro_agregat ORDER BY agro_agregat.Name";
                }
            }

            public static string SelectAgroUnit
            {
                get
                {
                    return
                        "SELECT agro_unit.Id, agro_unit.NameShort as Name FROM agro_unit ORDER BY agro_unit.NameShort";
                }
            }
        } // Price

        public class CompressData
        {
            public static string DeleteTable
            {
                get
                {
                    return "DELETE FROM {0} WHERE Id_main = {1}";
                }
            }

            public static string InsertIntoTable
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO {0} (Id_main, Lon_base, Lat_base, Speed_base, Lon_dev, Lat_dev, Speed_dev)"
                                   + " VALUES ({1}, ?Lon_base, ?Lat_base, ?Speed_base, ?Lon_dev, ?Lat_dev, ?Speed_dev)";

                        case MssqlUse:
                            return "INSERT INTO {0} (Id_main, Lon_base, Lat_base, Speed_base, Lon_dev, Lat_dev, Speed_dev)"
                                   + " VALUES ({1},@Lon_base,@Lat_base,@Speed_base,@Lon_dev,@Lat_dev,@Speed_dev)";
                    }

                    return "";
                }
            }

            public static string SelectFromTable
            {
                get
                {
                    return "SELECT {0}.* FROM {0} WHERE {0}.Id_main = {1} ORDER BY {0}.Id";
                }
            }
        } // CompressData

        public class OrderFueling
        {
            public static string InsertIntoAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO agro_fueling (Id_main, Lat, Lng, DateFueling, ValueStart, ValueSystem, ValueDisp, Location) 
                                    values (?Id_main, ?Lat, ?Lng, ?DateFueling, ?ValueStart, ?ValueSystem, ?ValueDisp, ?Location)";

                        case MssqlUse:
                            return
                                @"INSERT INTO agro_fueling (Id_main, Lat, Lng, DateFueling, ValueStart, ValueSystem, ValueDisp, Location) 
                                    values (@Id_main, @Lat, @Lng, @DateFueling, @ValueStart, @ValueSystem, @ValueDisp, @Location)";
                    }

                    return "";
                }
            }

            public static string DeleteFromAgroFueling
            {
                get
                {
                    return "DELETE FROM agro_fueling WHERE Id_main = {0} AND LockRecord = 0";
                }
            }

            public static string SelectFromAgroFueling
            {
                get
                {
                    return "SELECT * FROM  agro_fueling WHERE Id_main = {0} ORDER BY DateFueling";
                }
            }

            public static string UpdateFromAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set LockRecord = ?LockRecord WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set LockRecord = @LockRecord WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFueling
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set ValueDisp = ?ValueDisp WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set ValueDisp = @ValueDisp WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFuelingRem
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set Remark = ?Remark WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set Remark = @Remark WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroFuelingSet
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE agro_fueling Set DateFueling = ?DateFueling WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_fueling Set DateFueling = @DateFueling WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroFuelingSum
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT sum(agro_fueling.ValueDisp) AS sumDisp
                                    FROM agro_fueling WHERE agro_fueling.Id_main = {0}
                                    AND agro_fueling.ValueDisp {1} 0
                                    AND agro_fueling.DateFueling >= ?TimeStart 
                                    AND agro_fueling.DateFueling <= ?TimeEnd
                                    GROUP BY agro_fueling.Id_main, agro_fueling.DateFueling";

                        case MssqlUse:
                            return @"SELECT sum(agro_fueling.ValueDisp) AS sumDisp
                                    FROM agro_fueling WHERE agro_fueling.Id_main = {0}
                                    AND agro_fueling.ValueDisp {1} 0
                                    AND agro_fueling.DateFueling >= @TimeStart 
                                    AND agro_fueling.DateFueling <= @TimeEnd
                                    GROUP BY agro_fueling.Id_main, agro_fueling.DateFueling";
                    }

                    return "";
                }
            }

            public static string SelectAgroFuelingId
            {
                get
                {
                    return @"SELECT agro_fueling.* FROM agro_fueling WHERE Id_main = {0} AND LockRecord = 1";
                }
            }

            public static string DeleteAgroFuelingId
            {
                get
                {
                    return "DELETE FROM agro_fueling WHERE Id = {0}";
                }
            }
        } // OrderFueling

        public class OrderItem
        {
            public static string sqlInsertHead
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return _sSQLinsertHead;

                        case MssqlUse:
                            return _mSsSQLinsertHead;
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"INSERT INTO `agro_order`(`Date`, Id_mobitel, Comment, StateOrder, UserCreated) VALUES
                                    (?Date, ?Mobitel_Id, ?Comment, ?StateOrder, ?UserCreated)";

                        case MssqlUse:
                            return @"INSERT INTO agro_order(Date, Id_mobitel, Comment, StateOrder, UserCreated) VALUES 
                                    (@Date, @Mobitel_Id, @Comment, @StateOrder, @UserCreated)";
                    }

                    return "";
                }
            }

            public static string DeleteAgroOrder
            {
                get
                {
                    return "DELETE FROM agro_order WHERE ID = {0}";
                }
            }

            public static string UpdateAgroOrder
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "UPDATE `agro_order` Set `Date` = ?Date, Id_mobitel = ?Mobitel_Id, Comment = ?Comment "
                                   + " WHERE Id = {0}";

                        case MssqlUse:
                            return "UPDATE agro_order Set Date = @Date, Id_mobitel = @Mobitel_Id, Comment = @Comment "
                                   + " WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderCount
            {
                get
                {
                    return "SELECT COUNT(agro_order.Id) AS CNT FROM agro_order";
                }
            }

            public static string SelectAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT   agro_ordert.Id, agro_fieldgroupe.Name AS GFName, 
                                    IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', 
                                    IFNULL(agro_field.Name, '{0}'))) AS FName, zones.Zone_ID, zones.Square * 100 AS Square, driver.Family, 
                                    agro_ordert.DrvInputSource, agro_ordert.Id_agregat, agro_agregat.Name AS AName, agro_ordert.AgrInputSource, 
                                    agro_agregat.Width,agro_ordert.TimeStart, agro_ordert.TimeEnd, agro_ordert.TimeMove, agro_ordert.TimeStop, 
                                    agro_ordert.TimeRate, agro_ordert.FactTime, agro_ordert.Distance, agro_ordert.FactSquare, 
                                    agro_ordert.FactSquareCalc,  agro_ordert.SpeedAvg FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id
                                    LEFT OUTER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID
                                    WHERE   agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT   agro_ordert.Id, agro_fieldgroupe.Name AS GFName, (CASE WHEN agro_ordert.Id_zone = {2} THEN '{3}' ELSE
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN '{0}' ELSE ISNULL(agro_field.Name, '{0}')END)END) AS 
                                    FName, zones.Zone_ID, zones.Square * 100 AS Square, driver.Family, agro_ordert.Id_agregat, 
                                    agro_agregat.Name AS AName, agro_ordert.AgrInputSource, agro_agregat.Width,agro_ordert.TimeStart,
                                    agro_ordert.TimeEnd, agro_ordert.TimeMove, agro_ordert.TimeStop, agro_ordert.TimeRate, 
                                    agro_ordert.FactTime, agro_ordert.Distance, agro_ordert.FactSquare, agro_ordert.FactSquareCalc,  
                                    agro_ordert.SpeedAvg FROM agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.id
                                    LEFT OUTER JOIN zones ON agro_ordert.Id_zone = zones.Zone_ID
                                    WHERE   agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectIfAgroOrdert
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', IFNULL(agro_field.Name, '{0}'))) AS FName,
                            IF(agro_ordert.Confirm = 0, 0, IFNULL(zones.Square, 0))*100 AS FSquare,
                            agro_ordert.Id_driver, agro_ordert.Id_agregat,agro_ordert.AgrInputSource, SUM(agro_ordert.Distance) AS D, 
                            SUM(IF(agro_ordert.Confirm = 0,0,agro_ordert.FactSquare)) AS FS, SUM(IF(agro_ordert.Confirm = 0,0,agro_ordert.FactSquareCalc)) AS FSC, 
                             driver.Family AS Driver, agro_agregat.Name AS Agregat,agro_ordert.AgrInputSource, agro_ordert.DrvInputSource
                            FROM  agro_ordert 
                            Left Outer Join  agro_field ON agro_ordert.Id_field = agro_field.Id 
                            Left Outer Join  zones   ON zones.Zone_ID = agro_ordert.Id_zone 
                            LEFT OUTER JOIN agro_agregat     ON agro_ordert.Id_agregat = agro_agregat.Id 
                            LEFT OUTER JOIN  driver ON driver.id = agro_ordert.Id_driver
                            WHERE agro_ordert.Id_main = {1}
                            GROUP BY FName,FSquare , agro_ordert.Id_driver, agro_ordert.Id_agregat,agro_ordert.AgrInputSource, agro_ordert.DrvInputSource
                            ORDER BY FName";

                        case MssqlUse:
                            return
                                @"SELECT (CASE WHEN (agro_ordert.Id_zone = {2}) THEN '{3}' ELSE (CASE WHEN (agro_ordert.Confirm = 0) THEN '{0}' ELSE isnull(agro_field.Name, '{0}') END)END) AS FName, 
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE isnull(zones.Square, 0) END) * 100 AS FSquare, 
                                    agro_ordert.Id_driver, agro_ordert.Id_agregat, sum(agro_ordert.Distance) AS D, 
                                    sum((CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FactSquare END)) AS FS, 
                                    sum((CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FactSquareCalc END)) AS FSC, 
                                    driver.Family AS Driver, agro_agregat.Name AS Agregat FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN zones ON zones.Zone_ID = agro_ordert.Id_zone
                                    LEFT OUTER JOIN agro_agregat ON agro_ordert.Id_agregat = agro_agregat.Id
                                    LEFT OUTER JOIN driver ON driver.id = agro_ordert.Id_driver
                                    WHERE agro_ordert.Id_main = {1}
                                    GROUP BY agro_ordert.Id_zone, agro_ordert.Confirm, agro_ordert.Id_driver, 
                                    agro_ordert.Id_agregat, agro_field.Name, zones.Square, driver.Family, agro_agregat.Name
                                    ORDER BY FName";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert.Id, IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', IFNULL(agro_field.Name, '{0}'))) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.FuelAdd,
                                    agro_ordert.FuelStart, agro_ordert.FuelSub, agro_ordert.FuelEnd, agro_ordert.FuelExpens,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.FuelExpensAvg) as FuelExpensAvg, agro_ordert.FuelExpensAvgRate
                                    FROM    agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE   agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT   agro_ordert.Id, (CASE WHEN agro_ordert.Id_zone= {2} THEN '{3}' ELSE (CASE WHEN agro_ordert.Confirm = 0 then '{0}' else ISNULL(agro_field.Name, '{0}')end)end) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.FuelAdd,
                                    agro_ordert.FuelStart, agro_ordert.FuelSub, agro_ordert.FuelEnd, agro_ordert.FuelExpens,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.FuelExpensAvg end) as FuelExpensAvg, agro_ordert.FuelExpensAvgRate
                                    FROM    agro_ordert 
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE   agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertIf
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert.Id, IF(agro_ordert.Id_zone= {2},'{3}',IF(agro_ordert.Confirm = 0, '{0}', IFNULL(agro_field.Name, '{0}'))) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.Fuel_ExpensMove,
                                    agro_ordert.Fuel_ExpensStop, agro_ordert.Fuel_ExpensTotal, agro_ordert.Fuel_ExpensAvg,
                                    IF(agro_ordert.Confirm = 0 ,0, agro_ordert.Fuel_ExpensAvgRate) as Fuel_ExpensAvgRate
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE agro_ordert.Id_main = {1}";

                        case MssqlUse:
                            return
                                @"SELECT agro_ordert.Id, (CASE WHEN agro_ordert.Id_zone = {2} THEN '{3}' ELSE (CASE WHEN agro_ordert.Confirm = 0 then '{0}' else ISNULL(agro_field.Name, '{0}')end)end) AS FName, driver.Family,
                                    agro_ordert.TimeStart, agro_ordert.TimeEnd,agro_ordert.Distance,agro_ordert.Fuel_ExpensMove,
                                    agro_ordert.Fuel_ExpensStop, agro_ordert.Fuel_ExpensTotal, agro_ordert.Fuel_ExpensAvg,
                                    (CASE WHEN agro_ordert.Confirm = 0 THEN 0 ELSE agro_ordert.Fuel_ExpensAvgRate end) as Fuel_ExpensAvgRate
                                    FROM agro_ordert
                                    LEFT OUTER JOIN agro_field ON agro_ordert.Id_field = agro_field.Id
                                    LEFT OUTER JOIN  driver ON agro_ordert.Id_driver = driver.id
                                    WHERE agro_ordert.Id_main = {1}";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertControl
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_ordert_control.Id, agro_ordert_control.Id_object, agro_ordert_control.TimeStart, agro_ordert_control.TimeEnd, 
                                    TIME_Format(TIMEDIFF(agro_ordert_control.TimeEnd, agro_ordert_control.TimeStart), '%H:%i') as Time
                                    FROM  agro_ordert_control WHERE   agro_ordert_control.Type_object = {0}
                                    AND agro_ordert_control.Id_main = {1} ORDER BY agro_ordert_control.TimeStart";

                        case MssqlUse:
                            return
                                @"SELECT   agro_ordert_control.Id, agro_ordert_control.Id_object, agro_ordert_control.TimeStart, agro_ordert_control.TimeEnd,
                                    dbo.get_hh_min(DATEDIFF(mi, agro_ordert_control.TimeStart, agro_ordert_control.TimeEnd)) as Time
                                    FROM  agro_ordert_control WHERE   agro_ordert_control.Type_object = {0}
                                    AND agro_ordert_control.Id_main = {1} ORDER BY agro_ordert_control.TimeStart";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertControlFrom
            {
                get
                {
                    return
                        "SELECT agro_ordert_control.* FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}";
                }
            }

            public static string SelectAgroOrdertFrom
            {
                get
                {
                    return
                        "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.Id_main = {0} AND agro_ordert.LockRecord =1 ORDER BY TimeStart";
                }
            }

            public static string SelectAgroOrderId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"SELECT agro_order.Id,agro_order.`Date`,{0} as Veh FROM   agro_order LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_order.Id_mobitel = {1} AND Year(agro_order.`Date`) = ?Y AND Month(agro_order.`Date`) = ?M AND Day(agro_order.`Date`) = ?D";

                        case MssqlUse:
                            return
                                @"SELECT agro_order.Id,agro_order.Date,{0} as Veh FROM   agro_order LEFT OUTER JOIN vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_id
                                    WHERE agro_order.Id_mobitel = {1} AND Year(agro_order.Date) = @Y AND Month(agro_order.Date) = @M AND Day(agro_order.Date) = @D";
                    }

                    return "";
                }
            }

            public static string UpdateAgroOrderExist
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"UPDATE agro_order SET 
                                  TimeStart = ?TimeStart
                                , TimeEnd = ?TimeEnd
                                , TimeWork = ?TimeWork
                                , TimeMove = ?TimeMove
                                , LocationStart = ?LocationStart
                                , LocationEnd = ?LocationEnd
                                , Distance = ?Distance
                                , SpeedAvg = ?SpeedAvg
                                , SquareWorkDescript = ?SquareWorkDescript
                                , SquareWorkDescripOverlap = ?SquareWorkDescripOverlap
                                , FactSquareCalcOverlap = ?FactSquareCalcOverlap
                                , PathWithoutWork = ?PathWithoutWork
                                , FuelDUTExpensSquare = ?FuelDUTExpensSquare
                                , FuelDUTExpensAvgSquare = ?FuelDUTExpensAvgSquare
                                , FuelDUTExpensWithoutWork = ?FuelDUTExpensWithoutWork
                                , FuelDUTExpensAvgWithoutWork = ?FuelDUTExpensAvgWithoutWork
                                , FuelDRTExpensSquare = ?FuelDRTExpensSquare
                                , FuelDRTExpensAvgSquare = ?FuelDRTExpensAvgSquare
                                , FuelDRTExpensWithoutWork = ?FuelDRTExpensWithoutWork
                                , FuelDRTExpensAvgWithoutWork = ?FuelDRTExpensAvgWithoutWork
                                , DateLastRecalc = ?DateLastRecalc
                                , PointsValidity = ?PointsValidity
                                , PointsCalc = ?PointsCalc
                                , PointsFact = ?PointsFact
                                , PointsIntervalMax=?PointsIntervalMax
                                , StateOrder=?StateOrder
                                WHERE (ID = {0})";

                        case MssqlUse:
                            return @"UPDATE agro_order SET 
                                  TimeStart = @TimeStart
                                , TimeEnd = @TimeEnd
                                , TimeWork = @TimeWork
                                , TimeMove = @TimeMove
                                , LocationStart = @LocationStart
                                , LocationEnd = @LocationEnd
                                , Distance = @Distance
                                , SpeedAvg = @SpeedAvg
                                , SquareWorkDescript = @SquareWorkDescript
                                , SquareWorkDescripOverlap = @SquareWorkDescripOverlap
                                , FactSquareCalcOverlap = @FactSquareCalcOverlap
                                , PathWithoutWork = @PathWithoutWork
                                , FuelDUTExpensSquare = @FuelDUTExpensSquare
                                , FuelDUTExpensAvgSquare = @FuelDUTExpensAvgSquare
                                , FuelDUTExpensWithoutWork = @FuelDUTExpensWithoutWork
                                , FuelDUTExpensAvgWithoutWork = @FuelDUTExpensAvgWithoutWork
                                , FuelDRTExpensSquare = @FuelDRTExpensSquare
                                , FuelDRTExpensAvgSquare = @FuelDRTExpensAvgSquare
                                , FuelDRTExpensWithoutWork = @FuelDRTExpensWithoutWork
                                , FuelDRTExpensAvgWithoutWork = @FuelDRTExpensAvgWithoutWork
                                , DateLastRecalc = @DateLastRecalc
                                , PointsValidity = @PointsValidity
                                , PointsCalc = @PointsCalc
                                , PointsFact = @PointsFact
                                , PointsIntervalMax=@PointsIntervalMax
                                , StateOrder=@StateOrder
                                 WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderVehicle
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT agro_order.*, vehicle.Team_id,{0} AS MobitelName"
                                   +
                                   " FROM   agro_order   LEFT OUTER JOIN  vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_Id WHERE agro_order.Id = {1}";

                        case MssqlUse:
                            return @"SELECT agro_order.*, vehicle.Team_id,{0} AS MobitelName"
                                   +
                                   " FROM   agro_order   LEFT OUTER JOIN  vehicle ON agro_order.Id_mobitel = vehicle.Mobitel_Id WHERE agro_order.Id = {1}";
                    }

                    return "";
                }
            }

            public static string UpdateAgroOrderState
            {
                get
                {
                    return @"UPDATE agro_order SET StateOrder = {0} WHERE (ID = {1})";
                }
            }

            public static string UpdateAgroOrderBlock
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                @"UPDATE agro_order SET BlockUserId = ?BlockUserId,BlockDate = ?BlockDate WHERE (ID = {0})";

                        case MssqlUse:
                            return
                                @"UPDATE agro_order SET BlockUserId = @BlockUserId,BlockDate = @BlockDate WHERE (ID = {0})";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrderObj
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.TimeEnd > ?LastControlTime AND agro_ordert.Id_main = {0} ORDER BY Id";

                        case MssqlUse:
                            return
                                "SELECT agro_ordert.* FROM agro_ordert WHERE agro_ordert.TimeEnd > @LastControlTime AND agro_ordert.Id_main = {0} ORDER BY Id";
                    }

                    return "";
                }
            }

            public static string InsertIntoAgroOrdertControl
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main,Id_object,Type_object,TimeStart,TimeEnd)"
                                   + " VALUES ({0},{1},{2},?TimeStart,?TimeEnd)";

                        case MssqlUse:
                            return "INSERT INTO agro_ordert_control (Id_main,Id_object,Type_object,TimeStart,TimeEnd)"
                                   + " VALUES ({0},{1},{2},@TimeStart,@TimeEnd)";
                    }

                    return "";
                }
            }

            public static string DeleteAgroOrdertControl
            {
                get
                {
                    return "DELETE FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}";
                }
            }

            public static string SelectAgroOrdertControlTime
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT agro_ordert_control.TimeEnd FROM agro_ordert_control WHERE agro_ordert_control.Id_main = {0}"
                                   +
                                   " AND agro_ordert_control.Type_object = {1} ORDER BY agro_ordert_control.TimeEnd DESC Limit 1";

                        case MssqlUse:
                            return "SELECT TOP 1 agro_ordert_control.TimeEnd FROM  agro_ordert_control WHERE agro_ordert_control.Id_main = {0}"
                                   +
                                   " AND agro_ordert_control.Type_object = {1} ORDER BY agro_ordert_control.TimeEnd DESC";
                    }

                    return "";
                }
            }

            public static string SelectAgroOrdertCount
            {
                get
                {
                    return "SELECT count(agro_ordert.Id) AS CNT FROM agro_ordert WHERE agro_ordert.Id_main = {0}";
                }
            }

            public static string UpdateAgroOrdertRecord
            {
                get
                {
                    return @"UPDATE agro_ordert SET LockRecord = {0} WHERE (Id_main = {1})";
                }
            }

            public static string SelectAgroPricetPrice
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                                   + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                                   + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                                   + " WHERE   agro_price.DateComand < ?DateComand AND agro_pricet.Id_work = " +
                                   "{0}" + " AND agro_pricet.Id_mobitel = " + "{1}"
                                   + " ORDER BY   agro_price.DateComand DESC";

                        case MssqlUse:
                            return "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                                   + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                                   + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                                   + " WHERE   agro_price.DateComand < @DateComand AND agro_pricet.Id_work = " +
                                   "{0}" + " AND agro_pricet.Id_mobitel = " + "{0}"
                                   + " ORDER BY   agro_price.DateComand DESC";
                    }

                    return "";
                }
            }
        } // OrderItem
    } // AgroQuery
} // Agro
