namespace Agro.Dialogs
{
  partial class SpeedModeDlg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.label1 = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.colorButton3 = new System.Windows.Forms.Button();
        this.colorPanel3 = new System.Windows.Forms.Panel();
        this.colorButton2 = new System.Windows.Forms.Button();
        this.colorPanel2 = new System.Windows.Forms.Panel();
        this.txSpeedMax = new System.Windows.Forms.TextBox();
        this.colorButton1 = new System.Windows.Forms.Button();
        this.label6 = new System.Windows.Forms.Label();
        this.colorPanel1 = new System.Windows.Forms.Panel();
        this.label5 = new System.Windows.Forms.Label();
        this.txSpeedMin = new System.Windows.Forms.TextBox();
        this.label3 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.OKbutton = new System.Windows.Forms.Button();
        this.canselbutton = new System.Windows.Forms.Button();
        this.colorDialog1 = new System.Windows.Forms.ColorDialog();
        this.lbWork = new DevExpress.XtraEditors.LabelControl();
        this.groupBox1.SuspendLayout();
        this.SuspendLayout();
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(38, 35);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(22, 13);
        this.label1.TabIndex = 0;
        this.label1.Text = "��";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.colorButton3);
        this.groupBox1.Controls.Add(this.colorPanel3);
        this.groupBox1.Controls.Add(this.colorButton2);
        this.groupBox1.Controls.Add(this.colorPanel2);
        this.groupBox1.Controls.Add(this.txSpeedMax);
        this.groupBox1.Controls.Add(this.colorButton1);
        this.groupBox1.Controls.Add(this.label6);
        this.groupBox1.Controls.Add(this.colorPanel1);
        this.groupBox1.Controls.Add(this.label5);
        this.groupBox1.Controls.Add(this.txSpeedMin);
        this.groupBox1.Controls.Add(this.label3);
        this.groupBox1.Controls.Add(this.label2);
        this.groupBox1.Controls.Add(this.label1);
        this.groupBox1.Location = new System.Drawing.Point(12, 31);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(268, 129);
        this.groupBox1.TabIndex = 1;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "��������";
        // 
        // colorButton3
        // 
        this.colorButton3.Location = new System.Drawing.Point(232, 88);
        this.colorButton3.Name = "colorButton3";
        this.colorButton3.Size = new System.Drawing.Size(27, 23);
        this.colorButton3.TabIndex = 3;
        this.colorButton3.Text = "...";
        this.colorButton3.UseVisualStyleBackColor = true;
        this.colorButton3.Click += new System.EventHandler(this.colorButton3_Click);
        // 
        // colorPanel3
        // 
        this.colorPanel3.BackColor = System.Drawing.Color.Red;
        this.colorPanel3.Location = new System.Drawing.Point(196, 90);
        this.colorPanel3.Name = "colorPanel3";
        this.colorPanel3.Size = new System.Drawing.Size(25, 20);
        this.colorPanel3.TabIndex = 2;
        // 
        // colorButton2
        // 
        this.colorButton2.Location = new System.Drawing.Point(232, 59);
        this.colorButton2.Name = "colorButton2";
        this.colorButton2.Size = new System.Drawing.Size(27, 23);
        this.colorButton2.TabIndex = 3;
        this.colorButton2.Text = "...";
        this.colorButton2.UseVisualStyleBackColor = true;
        this.colorButton2.Click += new System.EventHandler(this.colorButton2_Click);
        // 
        // colorPanel2
        // 
        this.colorPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
        this.colorPanel2.Location = new System.Drawing.Point(196, 61);
        this.colorPanel2.Name = "colorPanel2";
        this.colorPanel2.Size = new System.Drawing.Size(25, 20);
        this.colorPanel2.TabIndex = 2;
        // 
        // txSpeedMax
        // 
        this.txSpeedMax.Location = new System.Drawing.Point(65, 90);
        this.txSpeedMax.Name = "txSpeedMax";
        this.txSpeedMax.Size = new System.Drawing.Size(40, 20);
        this.txSpeedMax.TabIndex = 1;
        this.txSpeedMax.Text = "7";
        this.txSpeedMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        this.txSpeedMax.TextChanged += new System.EventHandler(this.txSpeedMax_TextChanged);
        // 
        // colorButton1
        // 
        this.colorButton1.Location = new System.Drawing.Point(232, 30);
        this.colorButton1.Name = "colorButton1";
        this.colorButton1.Size = new System.Drawing.Size(27, 23);
        this.colorButton1.TabIndex = 3;
        this.colorButton1.Text = "...";
        this.colorButton1.UseVisualStyleBackColor = true;
        this.colorButton1.Click += new System.EventHandler(this.colorButton1_Click);
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Location = new System.Drawing.Point(111, 93);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(31, 13);
        this.label6.TabIndex = 0;
        this.label6.Text = "��\\�";
        // 
        // colorPanel1
        // 
        this.colorPanel1.BackColor = System.Drawing.Color.Yellow;
        this.colorPanel1.Location = new System.Drawing.Point(196, 32);
        this.colorPanel1.Name = "colorPanel1";
        this.colorPanel1.Size = new System.Drawing.Size(25, 20);
        this.colorPanel1.TabIndex = 2;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(18, 93);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(42, 13);
        this.label5.TabIndex = 0;
        this.label5.Text = "�����";
        // 
        // txSpeedMin
        // 
        this.txSpeedMin.Location = new System.Drawing.Point(65, 32);
        this.txSpeedMin.Name = "txSpeedMin";
        this.txSpeedMin.Size = new System.Drawing.Size(40, 20);
        this.txSpeedMin.TabIndex = 1;
        this.txSpeedMin.Text = "2";
        this.txSpeedMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        this.txSpeedMin.TextChanged += new System.EventHandler(this.txSpeedMin_TextChanged);
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(18, 64);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(86, 13);
        this.label3.TabIndex = 0;
        this.label3.Text = "������� �����";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(111, 35);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(31, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = "��\\�";
        // 
        // OKbutton
        // 
        this.OKbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.OKbutton.Location = new System.Drawing.Point(126, 169);
        this.OKbutton.Name = "OKbutton";
        this.OKbutton.Size = new System.Drawing.Size(75, 23);
        this.OKbutton.TabIndex = 2;
        this.OKbutton.Text = "OK";
        this.OKbutton.UseVisualStyleBackColor = true;
        this.OKbutton.Click += new System.EventHandler(this.OKbutton_Click);
        // 
        // canselbutton
        // 
        this.canselbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this.canselbutton.Location = new System.Drawing.Point(208, 169);
        this.canselbutton.Name = "canselbutton";
        this.canselbutton.Size = new System.Drawing.Size(75, 23);
        this.canselbutton.TabIndex = 3;
        this.canselbutton.Text = "������";
        this.canselbutton.UseVisualStyleBackColor = true;
        this.canselbutton.Click += new System.EventHandler(this.canselbutton_Click);
        // 
        // lbWork
        // 
        this.lbWork.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.lbWork.Appearance.Options.UseFont = true;
        this.lbWork.Location = new System.Drawing.Point(12, 12);
        this.lbWork.Name = "lbWork";
        this.lbWork.Size = new System.Drawing.Size(142, 13);
        this.lbWork.TabIndex = 4;
        this.lbWork.Text = "�������� �� ���������";
        // 
        // SpeedModeDlg
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(299, 204);
        this.Controls.Add(this.lbWork);
        this.Controls.Add(this.canselbutton);
        this.Controls.Add(this.OKbutton);
        this.Controls.Add(this.groupBox1);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        this.Name = "SpeedModeDlg";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "��������� ����������� ����������� ������";
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox txSpeedMin;
    private System.Windows.Forms.Button colorButton1;
    private System.Windows.Forms.Panel colorPanel1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button colorButton3;
    private System.Windows.Forms.Panel colorPanel3;
    private System.Windows.Forms.Button colorButton2;
    private System.Windows.Forms.Panel colorPanel2;
      private System.Windows.Forms.TextBox txSpeedMax;
      private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button OKbutton;
    private System.Windows.Forms.Button canselbutton;
      private System.Windows.Forms.ColorDialog colorDialog1;
      private DevExpress.XtraEditors.LabelControl lbWork;
  }
}