using System;
using System.Drawing;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraPrinting.Native.Lines;

namespace Agro.Dialogs
{
    public partial class SpeedModeDlg : Form
    {
        private double speedMin =
            Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMin, Consts.SPEED_MIN.ToString()));

        private double speedMax =
            Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMax, Consts.SPEED_MAX.ToString()));

        private Color colorMin =
            Color.FromArgb(
                Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorMin, Color.Yellow.ToArgb().ToString())));

        private Color colorNorm =
            Color.FromArgb(
                Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorNorm, Color.Green.ToArgb().ToString())));

        private Color colorMax =
            Color.FromArgb(
                Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorMax, Color.Red.ToArgb().ToString())));

        private int _Id_Work;
        private bool bSave;
        private DictionaryAgroWorkType dwt;

        public SpeedModeDlg(int Id_Work)
        {
            InitializeComponent();
            Localize();
            colorPanel1.BackColor = colorMin;
            colorPanel2.BackColor = colorNorm;
            colorPanel3.BackColor = colorMax;
            _Id_Work = Id_Work;
            if (_Id_Work == 0)
            {
                txSpeedMin.Text = speedMin.ToString();
                txSpeedMax.Text = speedMax.ToString();
            }
            else
            {
                using (dwt = new DictionaryAgroWorkType(_Id_Work))
                {
                    txSpeedMin.Text = dwt.SpeedBottom.ToString();
                    txSpeedMax.Text = dwt.SpeedTop.ToString();
                    lbWork.Text = dwt.Name;
                }

            }
        }

        private void Localize()
        {
            Text = Resources.SpeedModeDlgName;
            lbWork.Text = Resources.lbWork;
            groupBox1.Text = Resources.groupBox1;
            label1.Text = Resources.To;
            label3.Text = Resources.WorkMode;
            label5.Text = Resources.Ower;
            label2.Text = Resources.KMH;
            label6.Text = Resources.KMH;
            OKbutton.Text = Resources.OK;
            canselbutton.Text = Resources.Cancel;
        }

        private void colorButton1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colorPanel1.BackColor = colorDialog1.Color;
                Params.SetParTotal(Params.NUMPAR.SpeedColorMin, colorPanel1.BackColor.ToArgb().ToString());
            }
        }

        private void colorButton2_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colorPanel2.BackColor = colorDialog1.Color;
                Params.SetParTotal(Params.NUMPAR.SpeedColorNorm, colorPanel2.BackColor.ToArgb().ToString());
            }
        }

        private void colorButton3_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                colorPanel3.BackColor = colorDialog1.Color;
                Params.SetParTotal(Params.NUMPAR.SpeedColorMax, colorPanel3.BackColor.ToArgb().ToString());
            }
        }

        private void OKbutton_Click(object sender, EventArgs e)
        {
            if (bSave) SaveChanges();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void canselbutton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void txSpeedMin_TextChanged(object sender, EventArgs e)
        {
            bSave = true;
        }

        private void txSpeedMax_TextChanged(object sender, EventArgs e)
        {
            bSave = true;
        }

        private void SaveChanges()
        {
            if (_Id_Work == 0)
            {
                Params.SetParTotal(Params.NUMPAR.SpeedMin, txSpeedMin.Text);
                Params.SetParTotal(Params.NUMPAR.SpeedMax, txSpeedMax.Text);
            }
            else
            {
                using (dwt = new DictionaryAgroWorkType(_Id_Work))
                {
                    dwt.SpeedBottom = Convert.ToDouble(this.txSpeedMin.Text.Replace(".", ","));
                    dwt.SpeedTop = Convert.ToDouble(this.txSpeedMax.Text.Replace(".", ","));
                    dwt.EditItem();
                }
            }
        }
    }
}