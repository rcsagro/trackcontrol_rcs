using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Agro.Properties; 

namespace Agro.XtraReports
{
    public partial class SubOrderFuelDRT : DevExpress.XtraReports.UI.XtraReport
    {
        public SubOrderFuelDRT()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_order)
        {
            using (OrderItem oi = new OrderItem(Id_order))
            {
                DataSource = oi.GetFuelDRT();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalCell1.DataBindings.Add("Text", DataSource, "FName");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "TimeStart", "{0:t}");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "TimeEnd", "{0:t}");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "Distance");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "Fuel_ExpensMove");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "Fuel_ExpensStop");
            xrDetalCell7.DataBindings.Add("Text", DataSource, "Fuel_ExpensTotal");
        }
        #region �����������
        private void Localization()
        {
            xrTableCell6.Text = Resources.Field;
            xrTableCell7.Text = Resources.Entry;
            xrTableCell8.Text = Resources.Exit;
            xrTableCell2.Text = Resources.PathKm;
            xrTableCell3.Text = Resources.FuelExpenseMovingL;
            xrTableCell4.Text = Resources.FuelExpenseStopL;
            xrTableCell5.Text = Resources.FuelExpenseTotalL;
            xrLabel1.Text = Resources.FuelDRT;
            xrDetalCell1.Text = Resources.Field;
        }
        #endregion
    }
}
