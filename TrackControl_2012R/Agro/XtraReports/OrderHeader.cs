using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Agro.Properties; 

namespace Agro.XtraReports
{
    public partial class OrderHeader : DevExpress.XtraReports.UI.XtraReport
    {
        public OrderHeader()
        {
            InitializeComponent();
            Localization();
        }
        public OrderHeader(int idOrder)
            : this()
        {
            xrHeader.Text =string.Format("{0} {1}",Resources.OrderNumberWork,idOrder);
            using (var oi = new OrderItem(idOrder))
            {
                DataTable content = oi.GetContent();
                TimeSpan timeWorkInField = TimeSpan.Zero;
                TimeSpan timeOverField = TimeSpan.Zero; 
                foreach (DataRow rowCont in content.Rows)
                {
                    TimeSpan tsWork;
                    int zoneId =Convert.IsDBNull(rowCont["Zone_ID"]) ? 0 :Convert.ToInt32(rowCont["Zone_ID"]);
                    if (zoneId > 0)
                    {
                        if (TimeSpan.TryParse(rowCont["FactTime"].ToString(), out tsWork))
                        {
                            timeWorkInField += tsWork;
                        }
                    }
                    else
                    {
                        if (TimeSpan.TryParse(rowCont["FactTime"].ToString(), out tsWork))
                        {
                            timeOverField += tsWork;
                        }
                    }
                  }
                //--------����� ���������� ---------------------
                CellDate.Text = oi.Date.ToString("D");
                CellVehicle.Text = oi.MobitelName;
                CellSquare.Text = oi.SquareWorkDescript;
                CellPath.Text = oi.PathWithoutWork.ToString();
                CellSquareTotal.Text = oi.FactSquareCalcOverlap.ToString();
                //--------��������� ��������------------------
                CellPlaceStart.Text = oi.LocationStart;
                CellPlaceEnd.Text = oi.LocationEnd;
                CellDistance.Text = oi.Distance.ToString()  ;
                CellSpeedAvg.Text = oi.SpeedAvg.ToString();
                //--------��������� �������-------------
                CellTimeStart.Text = oi.TimeStart.ToString("t");
                CellTimeEnd.Text = oi.TimeEnd.ToString("t");
                CellTimeWork.Text = oi.TimeWork;
                CellTimeStop.Text = oi.TimeStop;
                CellTimeWorkInField.Text = timeWorkInField.ToString(@"hh\:mm");
                CellTimeOverField.Text = timeOverField.ToString(@"hh\:mm");
                //--------�������-------------------------------
                CellFuelSquare.Text = oi.FuelDUTExpensSquare == 0 ? oi.FuelDRTExpensSquare.ToString() : oi.FuelDUTExpensSquare.ToString();
                CellFuelSquareAvg.Text = oi.FuelDUTExpensAvgSquare == 0 ? oi.FuelDRTExpensAvgSquare.ToString() : oi.FuelDUTExpensAvgSquare.ToString();
                CellFuelPath.Text = oi.FuelDUTExpensWithoutWork == 0 ? oi.FuelDRTExpensWithoutWork.ToString() : oi.FuelDUTExpensWithoutWork.ToString();
                CellFuelPathAvg.Text = oi.FuelDUTExpensAvgWithoutWork == 0 ? oi.FuelDRTExpensAvgWithoutWork.ToString() : oi.FuelDUTExpensAvgWithoutWork.ToString();
                CellFuelTotal.Text = (oi.FuelDUTExpensSquare + oi.FuelDUTExpensWithoutWork) == 0 ? (oi.FuelDRTExpensSquare + oi.FuelDRTExpensWithoutWork).ToString() : (oi.FuelDUTExpensSquare + oi.FuelDUTExpensWithoutWork).ToString();
                //--------�����������-------------
                CellRemark.Text = oi.Remark;           
            }
        }
        #region �����������
        private void Localization()
        {
            xrTableCell22.Text = Resources.InformationTotal;
            xrTableCell24.Text = Resources.MovingParameters ;
            xrTableCell29.Text = Resources.TimeParameters;
            xrTableCell31.Text = Resources.Fuel;
            xrHeaderCell1.Text = Resources.Date;
            xrHeaderCell11.Text = Resources.PlaceStart;
            xrTableCell1.Text = Resources.TimeStart;
            xrTableCell11.Text = Resources.FuelExpenseFieldTotal;
            xrHeaderCell2.Text = Resources.Car;
            xrHeaderCell12.Text = Resources.PlaceEnd;
            xrTableCell2.Text = Resources.TimeEnd;
            xrTableCell12.Text = Resources.FuelExpenseFieldLGa;
            xrHeaderCell3.Text = Resources.SquareProcessGa;
            xrHeaderCell13.Text = Resources.PathKm;
            xrTableCell3.Text = Resources.DurationRotation;
            xrTableCell13.Text = Resources.FuelExpenseRunTotal;
            lbCellStart.Text = Resources.MoveKm;
            xrHeaderCell14.Text = Resources.SpeedAvgKmH;
            xrTableCell4.Text =Resources.TimeStopH;
            xrTableCell14.Text = Resources.FuelExpenseRunL100Km;
            xrTableCell21.Text = Resources.Comment;
            xrHeader.Text = Resources.OrderNumberWork;
            xrTableCell9.Text = Resources.TimeWorkingInFieldsH;
            xrTableCell20.Text = Resources.TimeOverFieldsH;

            xrTableCell5.Text = Resources.SquareGATotal;
            xrTableCell15.Text = Resources.FuelExpenseTotalL;
        }
        #endregion
    }
}
