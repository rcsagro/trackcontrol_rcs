namespace Agro.XtraReports
{
    partial class SubOrderFuelDUT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrDetalCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDetalTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDetalTable});
            this.Detail.Height = 25;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable1});
            this.PageHeader.Height = 50;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(0, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(1066, 25);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "������� (���)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Location = new System.Drawing.Point(0, 25);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable1.Size = new System.Drawing.Size(1067, 25);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell9,
            this.xrTableCell1,
            this.xrTableCell10});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "����";
            this.xrTableCell6.Weight = 1.42;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "�����";
            this.xrTableCell7.Weight = 0.58000000000000018;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "�����";
            this.xrTableCell8.Weight = 0.58000000000000018;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "���������� ���� , ��";
            this.xrTableCell2.Weight = 1.4225000000000003;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "������� � ������, �";
            this.xrTableCell3.Weight = 1.3337499999999998;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "���������, �";
            this.xrTableCell4.Weight = 0.8318749999999997;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "�����, �";
            this.xrTableCell5.Weight = 0.6759375;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "������� � �����, �";
            this.xrTableCell9.Weight = 1.2364843749999999;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "������� ������ ,� /��";
            this.xrTableCell10.Weight = 1.5129687499999998;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "����� ������, �";
            this.xrTableCell1.Weight = 1.0764843750000002;
            // 
            // xrDetalTable
            // 
            this.xrDetalTable.BorderColor = System.Drawing.Color.Silver;
            this.xrDetalTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrDetalTable.Location = new System.Drawing.Point(0, 0);
            this.xrDetalTable.Name = "xrDetalTable";
            this.xrDetalTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrDetalTable.Size = new System.Drawing.Size(1067, 25);
            this.xrDetalTable.StylePriority.UseBorderColor = false;
            this.xrDetalTable.StylePriority.UseBorders = false;
            this.xrDetalTable.StylePriority.UseTextAlignment = false;
            this.xrDetalTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrDetalCell1,
            this.xrDetalCell2,
            this.xrDetalCell3,
            this.xrDetalCell4,
            this.xrDetalCell5,
            this.xrDetalCell6,
            this.xrDetalCell7,
            this.xrDetalCell8,
            this.xrDetalCell9,
            this.xrDetalCell10});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrDetalCell1
            // 
            this.xrDetalCell1.Name = "xrDetalCell1";
            this.xrDetalCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrDetalCell1.StylePriority.UsePadding = false;
            this.xrDetalCell1.StylePriority.UseTextAlignment = false;
            this.xrDetalCell1.Text = "����";
            this.xrDetalCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrDetalCell1.Weight = 1.42;
            // 
            // xrDetalCell2
            // 
            this.xrDetalCell2.Name = "xrDetalCell2";
            this.xrDetalCell2.Text = "00:00";
            this.xrDetalCell2.Weight = 0.58000000000000018;
            // 
            // xrDetalCell3
            // 
            this.xrDetalCell3.Name = "xrDetalCell3";
            this.xrDetalCell3.StylePriority.UseTextAlignment = false;
            this.xrDetalCell3.Text = "00:00";
            this.xrDetalCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrDetalCell3.Weight = 0.58;
            // 
            // xrDetalCell4
            // 
            this.xrDetalCell4.Name = "xrDetalCell4";
            this.xrDetalCell4.StylePriority.UseTextAlignment = false;
            this.xrDetalCell4.Text = "0";
            this.xrDetalCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrDetalCell4.Weight = 1.42;
            // 
            // xrDetalCell5
            // 
            this.xrDetalCell5.Name = "xrDetalCell5";
            this.xrDetalCell5.StylePriority.UseTextAlignment = false;
            this.xrDetalCell5.Text = "0";
            this.xrDetalCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrDetalCell5.Weight = 1.33;
            // 
            // xrDetalCell6
            // 
            this.xrDetalCell6.Name = "xrDetalCell6";
            this.xrDetalCell6.Text = "0";
            this.xrDetalCell6.Weight = 0.83000000000000007;
            // 
            // xrDetalCell7
            // 
            this.xrDetalCell7.Name = "xrDetalCell7";
            this.xrDetalCell7.Text = "0";
            this.xrDetalCell7.Weight = 0.67999999999999972;
            // 
            // xrDetalCell8
            // 
            this.xrDetalCell8.Name = "xrDetalCell8";
            this.xrDetalCell8.Text = "0";
            this.xrDetalCell8.Weight = 1.235;
            // 
            // xrDetalCell9
            // 
            this.xrDetalCell9.Name = "xrDetalCell9";
            this.xrDetalCell9.Text = "0";
            this.xrDetalCell9.Weight = 1.0775000000000001;
            // 
            // xrDetalCell10
            // 
            this.xrDetalCell10.Name = "xrDetalCell10";
            this.xrDetalCell10.Text = "0";
            this.xrDetalCell10.Weight = 1.5175;
            // 
            // SubOrderFuelDUT
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrDetalTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTable xrDetalTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell10;
    }
}
