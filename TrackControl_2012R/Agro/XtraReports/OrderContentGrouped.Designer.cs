namespace Agro.XtraReports
{
    partial class OrderContentGrouped
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrSub = new DevExpress.XtraReports.UI.XRSubreport();
            this.subOrderContentGrouped1 = new SubOrderContentGrouped();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContentGrouped1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellDate
            // 
            this.CellDate.StylePriority.UseBorders = false;
            this.CellDate.StylePriority.UseFont = false;
            this.CellDate.StylePriority.UsePadding = false;
            this.CellDate.StylePriority.UseTextAlignment = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellSquare
            // 
            this.CellSquare.StylePriority.UseBorders = false;
            this.CellSquare.StylePriority.UseFont = false;
            this.CellSquare.StylePriority.UsePadding = false;
            this.CellSquare.StylePriority.UseTextAlignment = false;
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSub});
            this.Detail.Height = 17;
            // 
            // xrSub
            // 
            this.xrSub.Location = new System.Drawing.Point(0, 0);
            this.xrSub.Name = "xrSub";
            this.xrSub.ReportSource = this.subOrderContentGrouped1;
            this.xrSub.Size = new System.Drawing.Size(1042, 17);
            // 
            // subOrderContentGrouped1
            // 
            this.subOrderContentGrouped1.Landscape = true;
            this.subOrderContentGrouped1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subOrderContentGrouped1.Name = "subOrderContentGrouped1";
            this.subOrderContentGrouped1.PageColor = System.Drawing.Color.White;
            this.subOrderContentGrouped1.PageHeight = 827;
            this.subOrderContentGrouped1.PageWidth = 1169;
            this.subOrderContentGrouped1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subOrderContentGrouped1.Version = "9.2";
            // 
            // OrderContentGrouped
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContentGrouped1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRSubreport xrSub;
        private SubOrderContentGrouped subOrderContentGrouped1;
    }
}
