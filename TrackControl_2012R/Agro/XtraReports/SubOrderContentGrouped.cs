using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Agro.Properties; 

namespace Agro.XtraReports
{
    public partial class SubOrderContentGrouped : DevExpress.XtraReports.UI.XtraReport, ISubReport
    {
        private double _totalsSquare;
        public SubOrderContentGrouped()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_order)
        {
            using (OrderItem oi = new OrderItem(Id_order))
            {
                DataSource = oi.GetContentGrouped();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalCell1.DataBindings.Add("Text", DataSource, "FName");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "FSquare");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "Driver");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "Agregat");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "D");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "FS");
            xrDetalCell7.DataBindings.Add("Text", DataSource, "FSCOverLap");

            xrDetalCell2Total.DataBindings.Add("Text", DataSource, "FSquare");
            xrDetalCell5Total.DataBindings.Add("Text", DataSource, "D");
            xrDetalCell6Total.DataBindings.Add("Text", DataSource, "FS");
            xrDetalCell7Total.DataBindings.Add("Text", DataSource, "FSCOverLap");
        }

        #region �����������
        private void Localization()
        {
            xrDetalCell1.Text = Resources.Field;
            xrDetalCell3.Text = Resources.Driver;
            xrDetalCell4.Text = Resources.Agregat;
            xrLabel1.Text = Resources.Content;
            xrTableCell6.Text = Resources.Field;
            xrTableCell7.Text = Resources.SquareGA;
            xrTableCell8.Text = Resources.Driver;
            xrTableCell1.Text = Resources.Agregat;
            xrTableCell2.Text = Resources.PathKm;
            xrTableCell3.Text = Resources.SquareRunGa;
            xrTableCell4.Text = Resources.SquareContourGaOverlap;
            xrDetalCell1Total.Text = Resources.TotalRecord;

        }
        #endregion


    }
}
