using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Agro.Properties; 

namespace Agro.XtraReports
{
    public partial class SubOrderFuelDUT : DevExpress.XtraReports.UI.XtraReport
    {
        public SubOrderFuelDUT()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_order)
        {
            using (OrderItem oi = new OrderItem(Id_order))
            {
                DataSource = oi.GetFuelDUT();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalCell1.DataBindings.Add("Text", DataSource, "FName");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "TimeStart", "{0:t}");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "TimeEnd", "{0:t}");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "Distance");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "FuelStart");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "FuelAdd");
            xrDetalCell7.DataBindings.Add("Text", DataSource, "FuelSub");
            xrDetalCell8.DataBindings.Add("Text", DataSource, "FuelEnd");
            xrDetalCell9.DataBindings.Add("Text", DataSource, "FuelExpens");
            xrDetalCell10.DataBindings.Add("Text", DataSource, "FuelExpensAvg");
        }
        #region �����������
        private void Localization()
        {
            xrLabel1.Text = Resources.FuelDUT;
            xrTableCell6.Text = Resources.Field;
            xrTableCell7.Text = Resources.Entry;
            xrTableCell8.Text = Resources.Exit;
            xrTableCell2.Text = Resources.PathKm;
            xrTableCell3.Text = Resources.FuelStartL;
            xrTableCell4.Text = Resources.FuelAddL;
            xrTableCell5.Text = Resources.FuelSubL;
            xrTableCell9.Text = Resources.FuelEndL;
            xrTableCell10.Text = Resources.FuelExpenseAvgLGa;
            xrTableCell1.Text = Resources.FuelExpenseTotalL;
            xrDetalCell1.Text = Resources.Field;
        }
        #endregion
    }
}
