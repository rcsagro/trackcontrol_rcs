using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Agro.XtraReports
{
    public partial class OrderFuelDUT : OrderHeader
    {
        public OrderFuelDUT(int Id_order)
            : base(Id_order)
        {
            InitializeComponent();
            subOrderFuelDUT1.Init(Id_order); 
        }

    }
}
