using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Agro.XtraReports
{
    public partial class OrderFuelDRT : OrderHeader
    {
        public OrderFuelDRT(int Id_order)
            : base(Id_order)
        {
            InitializeComponent();
            subOrderFuelDRT1.Init(Id_order);  
        }

    }
}
