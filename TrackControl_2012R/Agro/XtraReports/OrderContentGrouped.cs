using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Agro.XtraReports
{
    public partial class OrderContentGrouped : OrderHeader
    {
        public OrderContentGrouped(int Id_order)
            : base(Id_order)
        {
            InitializeComponent();
            subOrderContentGrouped1.Init(Id_order); 
        }
    }
}
