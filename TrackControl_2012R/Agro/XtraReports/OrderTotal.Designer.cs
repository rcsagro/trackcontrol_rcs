namespace Agro.XtraReports
{
    partial class OrderTotal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport4 = new DevExpress.XtraReports.UI.XRSubreport();
            this.subOrderContentGrouped1 = new SubOrderContentGrouped();
            this.subOrderContent1 = new SubOrderContent();
            this.subOrderFuelDUT1 = new SubOrderFuelDUT();
            this.subOrderFuelDRT1 = new SubOrderFuelDRT();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContentGrouped1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderFuelDUT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderFuelDRT1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellDate
            // 
            this.CellDate.StylePriority.UseBorders = false;
            this.CellDate.StylePriority.UseFont = false;
            this.CellDate.StylePriority.UsePadding = false;
            this.CellDate.StylePriority.UseTextAlignment = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellSquare
            // 
            this.CellSquare.StylePriority.UseBorders = false;
            this.CellSquare.StylePriority.UseFont = false;
            this.CellSquare.StylePriority.UsePadding = false;
            this.CellSquare.StylePriority.UseTextAlignment = false;
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrSubreport2,
            this.xrSubreport3,
            this.xrSubreport4});
            this.Detail.Height = 100;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Location = new System.Drawing.Point(0, 0);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.subOrderContentGrouped1;
            this.xrSubreport1.Size = new System.Drawing.Size(1042, 25);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Location = new System.Drawing.Point(0, 25);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ReportSource = this.subOrderContent1;
            this.xrSubreport2.Size = new System.Drawing.Size(1042, 25);
            // 
            // xrSubreport3
            // 
            this.xrSubreport3.Location = new System.Drawing.Point(0, 50);
            this.xrSubreport3.Name = "xrSubreport3";
            this.xrSubreport3.ReportSource = this.subOrderFuelDUT1;
            this.xrSubreport3.Size = new System.Drawing.Size(1042, 25);
            // 
            // xrSubreport4
            // 
            this.xrSubreport4.Location = new System.Drawing.Point(0, 75);
            this.xrSubreport4.Name = "xrSubreport4";
            this.xrSubreport4.ReportSource = this.subOrderFuelDRT1;
            this.xrSubreport4.Size = new System.Drawing.Size(1042, 25);
            // 
            // subOrderContentGrouped1
            // 
            this.subOrderContentGrouped1.Landscape = true;
            this.subOrderContentGrouped1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subOrderContentGrouped1.Name = "subOrderContentGrouped1";
            this.subOrderContentGrouped1.PageColor = System.Drawing.Color.White;
            this.subOrderContentGrouped1.PageHeight = 827;
            this.subOrderContentGrouped1.PageWidth = 1169;
            this.subOrderContentGrouped1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subOrderContentGrouped1.Version = "9.2";
            // 
            // subOrderContent1
            // 
            this.subOrderContent1.Landscape = true;
            this.subOrderContent1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subOrderContent1.Name = "subOrderContent1";
            this.subOrderContent1.PageColor = System.Drawing.Color.White;
            this.subOrderContent1.PageHeight = 827;
            this.subOrderContent1.PageWidth = 1169;
            this.subOrderContent1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subOrderContent1.Version = "9.2";
            // 
            // subOrderFuelDUT1
            // 
            this.subOrderFuelDUT1.Landscape = true;
            this.subOrderFuelDUT1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subOrderFuelDUT1.Name = "subOrderFuelDUT1";
            this.subOrderFuelDUT1.PageColor = System.Drawing.Color.White;
            this.subOrderFuelDUT1.PageHeight = 827;
            this.subOrderFuelDUT1.PageWidth = 1169;
            this.subOrderFuelDUT1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subOrderFuelDUT1.Version = "9.2";
            // 
            // subOrderFuelDRT1
            // 
            this.subOrderFuelDRT1.Landscape = true;
            this.subOrderFuelDRT1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subOrderFuelDRT1.Name = "subOrderFuelDRT1";
            this.subOrderFuelDRT1.PageColor = System.Drawing.Color.White;
            this.subOrderFuelDRT1.PageHeight = 827;
            this.subOrderFuelDRT1.PageWidth = 1169;
            this.subOrderFuelDRT1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subOrderFuelDRT1.Version = "9.2";
            // 
            // OrderTotal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContentGrouped1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderContent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderFuelDUT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subOrderFuelDRT1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport3;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport4;
        private SubOrderContentGrouped subOrderContentGrouped1;
        private SubOrderContent subOrderContent1;
        private SubOrderFuelDUT subOrderFuelDUT1;
        private SubOrderFuelDRT subOrderFuelDRT1;
    }
}
