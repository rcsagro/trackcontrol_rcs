namespace Agro.XtraReports
{
    partial class OrderHeader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellPlaceStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquare = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellVehicle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellPlaceEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSquareAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSquare = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDistance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeWork = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPath = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbCellStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellPath = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSpeedAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeStop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelPathAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSquareTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeWorkInField = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeOverField = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellRemark = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 20F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrHeaderTable,
            this.xrHeader});
            this.PageHeader.HeightF = 258F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHeaderTable.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(25F, 33F);
            this.xrHeaderTable.Name = "xrHeaderTable";
            this.xrHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow8,
            this.xrTableRow1,
            this.xrTableRow9});
            this.xrHeaderTable.SizeF = new System.Drawing.SizeF(1013F, 225F);
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell24,
            this.xrTableCell29,
            this.xrTableCell31});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "����� ���������� ";
            this.xrTableCell22.Weight = 2.1638682308600048D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "��������� ��������";
            this.xrTableCell24.Weight = 2.2374007244957683D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "��������� �������";
            this.xrTableCell29.Weight = 1.7230479821913511D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Text = "�������";
            this.xrTableCell31.Weight = 1.6282340828610402D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell1,
            this.CellDate,
            this.xrHeaderCell11,
            this.CellPlaceStart,
            this.xrTableCell1,
            this.CellTimeStart,
            this.xrTableCell11,
            this.CellFuelSquare});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrHeaderCell1
            // 
            this.xrHeaderCell1.Name = "xrHeaderCell1";
            this.xrHeaderCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell1.StylePriority.UsePadding = false;
            this.xrHeaderCell1.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell1.Text = "����        ";
            this.xrHeaderCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell1.Weight = 1.144639362594353D;
            // 
            // CellDate
            // 
            this.CellDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDate.Name = "CellDate";
            this.CellDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellDate.StylePriority.UseBorders = false;
            this.CellDate.StylePriority.UseFont = false;
            this.CellDate.StylePriority.UsePadding = false;
            this.CellDate.StylePriority.UseTextAlignment = false;
            this.CellDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellDate.Weight = 1.0192288682656516D;
            // 
            // xrHeaderCell11
            // 
            this.xrHeaderCell11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrHeaderCell11.Name = "xrHeaderCell11";
            this.xrHeaderCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell11.StylePriority.UseBackColor = false;
            this.xrHeaderCell11.StylePriority.UsePadding = false;
            this.xrHeaderCell11.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell11.Text = "����� ������ ��������";
            this.xrHeaderCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell11.Weight = 1.1515032173661077D;
            // 
            // CellPlaceStart
            // 
            this.CellPlaceStart.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellPlaceStart.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellPlaceStart.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellPlaceStart.Name = "CellPlaceStart";
            this.CellPlaceStart.StylePriority.UseBackColor = false;
            this.CellPlaceStart.StylePriority.UseBorders = false;
            this.CellPlaceStart.StylePriority.UseFont = false;
            this.CellPlaceStart.StylePriority.UseTextAlignment = false;
            this.CellPlaceStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellPlaceStart.Weight = 1.0858975071296606D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "����� ������ ��������";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 1.2121608381058211D;
            // 
            // CellTimeStart
            // 
            this.CellTimeStart.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeStart.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeStart.Name = "CellTimeStart";
            this.CellTimeStart.StylePriority.UseBorders = false;
            this.CellTimeStart.StylePriority.UseFont = false;
            this.CellTimeStart.StylePriority.UseTextAlignment = false;
            this.CellTimeStart.Text = " ";
            this.CellTimeStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeStart.Weight = 0.5108871440855296D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseBackColor = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "������ � �����,  �����";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 1.1278925516346017D;
            // 
            // CellFuelSquare
            // 
            this.CellFuelSquare.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellFuelSquare.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelSquare.Name = "CellFuelSquare";
            this.CellFuelSquare.StylePriority.UseBackColor = false;
            this.CellFuelSquare.StylePriority.UseBorders = false;
            this.CellFuelSquare.StylePriority.UseTextAlignment = false;
            this.CellFuelSquare.Text = "0";
            this.CellFuelSquare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelSquare.Weight = 0.50034153122643821D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell2,
            this.CellVehicle,
            this.xrHeaderCell12,
            this.CellPlaceEnd,
            this.xrTableCell2,
            this.CellTimeEnd,
            this.xrTableCell12,
            this.CellFuelSquareAvg});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrHeaderCell2
            // 
            this.xrHeaderCell2.Name = "xrHeaderCell2";
            this.xrHeaderCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell2.StylePriority.UsePadding = false;
            this.xrHeaderCell2.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell2.Text = "������";
            this.xrHeaderCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell2.Weight = 1.1446393625943525D;
            // 
            // CellVehicle
            // 
            this.CellVehicle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellVehicle.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellVehicle.Name = "CellVehicle";
            this.CellVehicle.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            this.CellVehicle.Text = " ";
            this.CellVehicle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellVehicle.Weight = 1.0192288682656518D;
            // 
            // xrHeaderCell12
            // 
            this.xrHeaderCell12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrHeaderCell12.Name = "xrHeaderCell12";
            this.xrHeaderCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell12.StylePriority.UseBackColor = false;
            this.xrHeaderCell12.StylePriority.UsePadding = false;
            this.xrHeaderCell12.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell12.Text = "����� ��������� ��������";
            this.xrHeaderCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell12.Weight = 1.1515032173661077D;
            // 
            // CellPlaceEnd
            // 
            this.CellPlaceEnd.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellPlaceEnd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellPlaceEnd.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellPlaceEnd.Name = "CellPlaceEnd";
            this.CellPlaceEnd.StylePriority.UseBackColor = false;
            this.CellPlaceEnd.StylePriority.UseBorders = false;
            this.CellPlaceEnd.StylePriority.UseFont = false;
            this.CellPlaceEnd.StylePriority.UseTextAlignment = false;
            this.CellPlaceEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellPlaceEnd.Weight = 1.0858975071296606D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "����� ��������� ��������";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 1.2121608381058211D;
            // 
            // CellTimeEnd
            // 
            this.CellTimeEnd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeEnd.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeEnd.Name = "CellTimeEnd";
            this.CellTimeEnd.StylePriority.UseBorders = false;
            this.CellTimeEnd.StylePriority.UseFont = false;
            this.CellTimeEnd.StylePriority.UseTextAlignment = false;
            this.CellTimeEnd.Text = " ";
            this.CellTimeEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeEnd.Weight = 0.5108871440855296D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseBackColor = false;
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "������ � �����, �/��";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 1.1202394904101118D;
            // 
            // CellFuelSquareAvg
            // 
            this.CellFuelSquareAvg.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellFuelSquareAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelSquareAvg.Name = "CellFuelSquareAvg";
            this.CellFuelSquareAvg.StylePriority.UseBackColor = false;
            this.CellFuelSquareAvg.StylePriority.UseBorders = false;
            this.CellFuelSquareAvg.StylePriority.UseTextAlignment = false;
            this.CellFuelSquareAvg.Text = "0";
            this.CellFuelSquareAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelSquareAvg.Weight = 0.50799459245092793D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell3,
            this.CellSquare,
            this.xrHeaderCell13,
            this.CellDistance,
            this.xrTableCell3,
            this.CellTimeWork,
            this.xrTableCell13,
            this.CellFuelPath});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrHeaderCell3
            // 
            this.xrHeaderCell3.Name = "xrHeaderCell3";
            this.xrHeaderCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell3.StylePriority.UsePadding = false;
            this.xrHeaderCell3.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell3.Text = "������������ �������, ��";
            this.xrHeaderCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell3.Weight = 1.1446393625943523D;
            // 
            // CellSquare
            // 
            this.CellSquare.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellSquare.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSquare.Name = "CellSquare";
            this.CellSquare.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellSquare.StylePriority.UseBorders = false;
            this.CellSquare.StylePriority.UseFont = false;
            this.CellSquare.StylePriority.UsePadding = false;
            this.CellSquare.StylePriority.UseTextAlignment = false;
            this.CellSquare.Text = " ";
            this.CellSquare.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellSquare.Weight = 1.0192288682656518D;
            // 
            // xrHeaderCell13
            // 
            this.xrHeaderCell13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrHeaderCell13.Name = "xrHeaderCell13";
            this.xrHeaderCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell13.StylePriority.UseBackColor = false;
            this.xrHeaderCell13.StylePriority.UsePadding = false;
            this.xrHeaderCell13.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell13.Text = "���������� ����, ��";
            this.xrHeaderCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell13.Weight = 1.1515032173661077D;
            // 
            // CellDistance
            // 
            this.CellDistance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellDistance.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDistance.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDistance.Name = "CellDistance";
            this.CellDistance.StylePriority.UseBackColor = false;
            this.CellDistance.StylePriority.UseBorders = false;
            this.CellDistance.StylePriority.UseFont = false;
            this.CellDistance.StylePriority.UseTextAlignment = false;
            this.CellDistance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDistance.Weight = 1.0858975071296606D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "����������������� �����,�";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 1.2121608381058207D;
            // 
            // CellTimeWork
            // 
            this.CellTimeWork.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeWork.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeWork.Name = "CellTimeWork";
            this.CellTimeWork.StylePriority.UseBorders = false;
            this.CellTimeWork.StylePriority.UseFont = false;
            this.CellTimeWork.StylePriority.UseTextAlignment = false;
            this.CellTimeWork.Text = " ";
            this.CellTimeWork.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeWork.Weight = 0.5108871440855296D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UseBackColor = false;
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "������ �� ���������, �����";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 1.1202394904101118D;
            // 
            // CellFuelPath
            // 
            this.CellFuelPath.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellFuelPath.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelPath.Name = "CellFuelPath";
            this.CellFuelPath.StylePriority.UseBackColor = false;
            this.CellFuelPath.StylePriority.UseBorders = false;
            this.CellFuelPath.StylePriority.UseTextAlignment = false;
            this.CellFuelPath.Text = "0";
            this.CellFuelPath.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelPath.Weight = 0.50799459245092793D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbCellStart,
            this.CellPath,
            this.xrHeaderCell14,
            this.CellSpeedAvg,
            this.xrTableCell4,
            this.CellTimeStop,
            this.xrTableCell14,
            this.CellFuelPathAvg});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // lbCellStart
            // 
            this.lbCellStart.Name = "lbCellStart";
            this.lbCellStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.lbCellStart.StylePriority.UsePadding = false;
            this.lbCellStart.StylePriority.UseTextAlignment = false;
            this.lbCellStart.Text = "��������, ��";
            this.lbCellStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbCellStart.Weight = 1.1446393625943525D;
            // 
            // CellPath
            // 
            this.CellPath.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellPath.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellPath.Name = "CellPath";
            this.CellPath.StylePriority.UseBorders = false;
            this.CellPath.StylePriority.UseFont = false;
            this.CellPath.StylePriority.UseTextAlignment = false;
            this.CellPath.Text = " ";
            this.CellPath.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellPath.Weight = 1.0192288682656523D;
            // 
            // xrHeaderCell14
            // 
            this.xrHeaderCell14.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrHeaderCell14.Name = "xrHeaderCell14";
            this.xrHeaderCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell14.StylePriority.UseBackColor = false;
            this.xrHeaderCell14.StylePriority.UsePadding = false;
            this.xrHeaderCell14.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell14.Text = "������� ��������, ��/�";
            this.xrHeaderCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell14.Weight = 1.1515032173661077D;
            // 
            // CellSpeedAvg
            // 
            this.CellSpeedAvg.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellSpeedAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellSpeedAvg.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSpeedAvg.Name = "CellSpeedAvg";
            this.CellSpeedAvg.StylePriority.UseBackColor = false;
            this.CellSpeedAvg.StylePriority.UseBorders = false;
            this.CellSpeedAvg.StylePriority.UseFont = false;
            this.CellSpeedAvg.StylePriority.UseTextAlignment = false;
            this.CellSpeedAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellSpeedAvg.Weight = 1.0858975071296606D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "����� ����� �������, �";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 1.2121608381058211D;
            // 
            // CellTimeStop
            // 
            this.CellTimeStop.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeStop.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeStop.Name = "CellTimeStop";
            this.CellTimeStop.StylePriority.UseBorders = false;
            this.CellTimeStop.StylePriority.UseFont = false;
            this.CellTimeStop.StylePriority.UseTextAlignment = false;
            this.CellTimeStop.Text = " ";
            this.CellTimeStop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeStop.Weight = 0.5108871440855296D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell14.StylePriority.UseBackColor = false;
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "������ �� ���������, �/100��";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 1.1202394904101118D;
            // 
            // CellFuelPathAvg
            // 
            this.CellFuelPathAvg.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellFuelPathAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelPathAvg.Name = "CellFuelPathAvg";
            this.CellFuelPathAvg.StylePriority.UseBackColor = false;
            this.CellFuelPathAvg.StylePriority.UseBorders = false;
            this.CellFuelPathAvg.StylePriority.UseTextAlignment = false;
            this.CellFuelPathAvg.Text = "0";
            this.CellFuelPathAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelPathAvg.Weight = 0.50799459245092793D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.CellSquareTotal,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.CellTimeWorkInField,
            this.xrTableCell15,
            this.CellFuelTotal});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "������� ����� , ��";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 1.1446393625943525D;
            // 
            // CellSquareTotal
            // 
            this.CellSquareTotal.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellSquareTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSquareTotal.Name = "CellSquareTotal";
            this.CellSquareTotal.StylePriority.UseBorders = false;
            this.CellSquareTotal.StylePriority.UseFont = false;
            this.CellSquareTotal.StylePriority.UseTextAlignment = false;
            this.CellSquareTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellSquareTotal.Weight = 1.0192288682656523D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.Weight = 1.1515032173661077D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBackColor = false;
            this.xrTableCell8.Weight = 1.0858975071296606D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "����� ������ � ����, �";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell9.Weight = 1.2121608381058211D;
            // 
            // CellTimeWorkInField
            // 
            this.CellTimeWorkInField.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeWorkInField.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeWorkInField.Name = "CellTimeWorkInField";
            this.CellTimeWorkInField.StylePriority.UseBorders = false;
            this.CellTimeWorkInField.StylePriority.UseFont = false;
            this.CellTimeWorkInField.StylePriority.UseTextAlignment = false;
            this.CellTimeWorkInField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeWorkInField.Weight = 0.5108871440855296D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBackColor = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "������ �����, �";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 1.1202394904101118D;
            // 
            // CellFuelTotal
            // 
            this.CellFuelTotal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CellFuelTotal.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelTotal.Name = "CellFuelTotal";
            this.CellFuelTotal.StylePriority.UseBackColor = false;
            this.CellFuelTotal.StylePriority.UseBorders = false;
            this.CellFuelTotal.StylePriority.UseTextAlignment = false;
            this.CellFuelTotal.Text = "0";
            this.CellFuelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelTotal.Weight = 0.50799459245092793D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.CellTimeOverField,
            this.xrTableCell25,
            this.xrTableCell26});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Weight = 1.1446393625943525D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 1.0192288682656523D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBackColor = false;
            this.xrTableCell18.Weight = 1.1515032173661077D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.Weight = 1.0858975071296606D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "����� ��������, �";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 1.2121608381058211D;
            // 
            // CellTimeOverField
            // 
            this.CellTimeOverField.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeOverField.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeOverField.Name = "CellTimeOverField";
            this.CellTimeOverField.StylePriority.UseBorders = false;
            this.CellTimeOverField.StylePriority.UseFont = false;
            this.CellTimeOverField.StylePriority.UseTextAlignment = false;
            this.CellTimeOverField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeOverField.Weight = 0.5108871440855296D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBackColor = false;
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 1.1202394904101118D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBackColor = false;
            this.xrTableCell26.Weight = 0.50799459245092793D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.CellRemark});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "�����������";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 1.1527196932633579D;
            // 
            // CellRemark
            // 
            this.CellRemark.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellRemark.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellRemark.Name = "CellRemark";
            this.CellRemark.StylePriority.UseBorders = false;
            this.CellRemark.StylePriority.UseFont = false;
            this.CellRemark.StylePriority.UseTextAlignment = false;
            this.CellRemark.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellRemark.Weight = 6.5998313271448055D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 7.7525510204081636D;
            // 
            // xrHeader
            // 
            this.xrHeader.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeader.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrHeader.Name = "xrHeader";
            this.xrHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeader.SizeF = new System.Drawing.SizeF(938F, 33F);
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            this.xrHeader.Text = "����� �� ������ �";
            this.xrHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(942F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 50F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 50F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // OrderHeader
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        protected DevExpress.XtraReports.UI.XRTable xrHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell1;
        public DevExpress.XtraReports.UI.XRTableCell CellDate;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell11;
        private DevExpress.XtraReports.UI.XRTableCell CellPlaceStart;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeStart;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquare;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell2;
        public DevExpress.XtraReports.UI.XRTableCell CellVehicle;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell12;
        private DevExpress.XtraReports.UI.XRTableCell CellPlaceEnd;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeEnd;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSquareAvg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell3;
        public DevExpress.XtraReports.UI.XRTableCell CellSquare;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell13;
        private DevExpress.XtraReports.UI.XRTableCell CellDistance;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeWork;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPath;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lbCellStart;
        private DevExpress.XtraReports.UI.XRTableCell CellPath;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell14;
        private DevExpress.XtraReports.UI.XRTableCell CellSpeedAvg;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeStop;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelPathAvg;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell CellRemark;
        protected DevExpress.XtraReports.UI.XRLabel xrHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        public DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell CellSquareTotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeWorkInField;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelTotal;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeOverField;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    }
}
