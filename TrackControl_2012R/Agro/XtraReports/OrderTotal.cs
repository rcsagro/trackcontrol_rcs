using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Agro.XtraReports
{
    public partial class OrderTotal : OrderHeader
    {

                public OrderTotal(int Id_order)
            : base(Id_order)
        {
            InitializeComponent();
                subOrderContentGrouped1.Init(Id_order); 
                subOrderContent1.Init(Id_order); 
                subOrderFuelDUT1.Init(Id_order); 
                subOrderFuelDRT1.Init(Id_order); 
        }

    }
}
