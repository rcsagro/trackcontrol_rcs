using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Agro.XtraReports
{
    public partial class OrderContent : OrderHeader
    {
        public OrderContent(int Id_order)
            : base(Id_order)
        {
            InitializeComponent();
            subOrderContent1.Init(Id_order); 
        }

    }
}
