using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Agro.Properties; 

namespace Agro.XtraReports
{
    public partial class SubOrderContent : DevExpress.XtraReports.UI.XtraReport, ISubReport
    {
        public SubOrderContent()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_order)
        {
            using (OrderItem oi = new OrderItem(Id_order))
            {
                DataSource = oi.GetContent();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalCell1.DataBindings.Add("Text", DataSource, "FName");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "TimeStart","{0:t}");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "TimeEnd", "{0:t}");
            xrDetalCell9.DataBindings.Add("Text", DataSource, "FactTime", "{0:t}");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "Family");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "AName");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "Distance");
            xrDetalCell7.DataBindings.Add("Text", DataSource, "FactSquareCalc");
            xrDetalCell8.DataBindings.Add("Text", DataSource, "SpeedAvg");
        }
        #region �����������
        private void Localization()
        {
            xrTableCell6.Text = Resources.Field;
            xrTableCell7.Text = Resources.Entry;
            xrTableCell8.Text = Resources.Exit;
            xrTableCell1.Text = Resources.Driver;
            xrTableCell2.Text = Resources.Agregat;
            xrTableCell3.Text = Resources.PathKm;
            xrTableCell4.Text = Resources.ProcessGA;
            xrLabel1.Text = Resources.Run;
            xrTableCell5.Text = Resources.SpeedAvg;
            xrDetalCell1.Text = Resources.Field;

        }
        #endregion
    }
}
