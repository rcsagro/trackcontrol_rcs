using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;
using TrackControl.Hasp;

namespace Agro
{
    /// <summary>
    /// глобальные перменные
    /// </summary>
    public static class GlobalVars
    {
        public static int g_AGRO = (int)Consts.RegimeType.Demo;
        public static int g_ROUTE = (int)Consts.RegimeType.Demo;
        public static int MAX_SQ_FOR_CHANGE_KF = 4000;
        public static int SCALE_WORK = 16;
        public static int SCALE_BIG_ZONE = 12;
        public static HaspState ResultLicenseAgro;
    }
}
