﻿using System;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro.GridEditForms
{
    public partial class DtReasons : Agro.GridEditForms.FormSample
    {
        public DtReasons()
        {
            InitThis();
        }

        public DtReasons(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroPlanDtReason(_Id);
            this.Text = this.Text + ": " + Resources.DownTownReasons;
            GetFields();
            bFormLoading = false;
        }

        private void InitThis()
        {
            InitializeComponent();
        }

        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["ReasonName"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["ReasonComment"], meComment.Text);
        }

        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
        }

        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
        }
    }
}
