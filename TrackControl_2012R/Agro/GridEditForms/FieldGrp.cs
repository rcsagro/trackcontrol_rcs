using System;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid; 
namespace Agro.GridEditForms
{
    public partial class FieldGrp : FormSample
    {
        public FieldGrp()
        {
            InitThis();
        }
        public FieldGrp(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryAgroFieldGrp(_Id);
            this.Text = this.Text + ": " + Resources.FieldsGroups;
            GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryAgroFieldGrp).Id_Zonesgroup = (int)(leGroupe.EditValue ?? 0);
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            leGroupe.EditValue = ((DictionaryAgroFieldGrp)di).Id_Zonesgroup;
        }

        private void btAutoCreate_Click(object sender, EventArgs e)
        {
            if (leGroupe.EditValue == null)
            {
                XtraMessageBox.Show(Resources.GroupeFieldsSelect, Resources.ApplicationName);
                return;
            }
            if (btAutoCreate.Text == Resources.AutoCreate)
                GroupeAutoCreate();
            else
                GroupeSynhro();
        }

        private void GroupeAutoCreate()
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.GroupeFieldsCreateConfirm + " " + leGroupe.Text + " " + Resources.GroupeFieldsCreateConfirmNext, Resources.ApplicationName, MessageBoxButtons.YesNo)) return;
            txId.Text = (di as DictionaryAgroFieldGrp).CreateFromCheckZoneGrp(leGroupe.Text, Convert.ToInt32(leGroupe.EditValue)).ToString();
            if (txId.Text != "0")
            {
                txName.Text = leGroupe.Text;
                meComment.Text = (di as DictionaryAgroFieldGrp).CommentForCreateFromCheckZoneGrp();
                SetButtonText();
                btSave.Enabled = false;
                _gvDict.AddNewRow();
                UpdateGRN();
            }
        }

        private void GroupeSynhro()
        {
            if (leGroupe.EditValue == null) return;
            int cntAdd;
            int cntDelete;
            (di as DictionaryAgroFieldGrp).SynhroWithCheckZoneGrp(Convert.ToInt32(leGroupe.EditValue),out cntAdd,out cntDelete);
            XtraMessageBox.Show(string.Format("{0}{1}{2} :{3}{1}{4} :{5}", Resources.SynhroFinished, Environment.NewLine, Resources.CountAddedFields, cntAdd, Resources.CountDeletedFields, cntDelete),Resources.ApplicationName);   
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            SetButtonText();
            DicUtilites.LookUpLoad(leGroupe, "SELECT   zonesgroup.Id,   zonesgroup.Title FROM  zonesgroup ORDER BY zonesgroup.Title");
        }

        private void SetButtonText()
        {
            if (_Id == 0)
            {
                btAutoCreate.Text = Resources.AutoCreate;
                leGroupe.Enabled = true;
            }
            else
            {
                btAutoCreate.Text = Resources.Synhro;
                if (leGroupe.EditValue  == null) leGroupe.Enabled = false;
            }
                
        }
        #region �����������
        private void Localization()
        {
            leGroupe.Properties.NullText = Resources.GroupeSelect;
            lbGroupe.Text = Resources.CheckZoneGroupe;
            btAutoCreate.Text = Resources.AutoCreate;
            btAutoCreate.ToolTip = Resources.GroupeFieldsCreateAuto;

        }
        #endregion

        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            if (_Id == 0) return;
            int id_ZoneGroupe;
            if (Int32.TryParse(Convert.ToString(leGroupe.EditValue), out id_ZoneGroupe))
            {
                (di as DictionaryAgroFieldGrp).Id_Zonesgroup = id_ZoneGroupe;
                ValidateFieldsAction();
            }
        }
    }
}
