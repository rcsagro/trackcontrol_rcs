using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace Agro.GridEditForms
{
    public partial class Agregat : FormSample
    {
 

        /// <summary>
        /// ������������ ��� ���������� ���������� �� ������ � ������ � �����
        /// </summary>
        DataView _dvClone = null;

        List<int> RowUpdated = new List<int>();

        public Agregat()
        {
            InitThis();
        }

        public Agregat( GridView gvDict, bool bNew )
            : base( gvDict, bNew )
        {
            InitThis();
            bFormLoading = true;
            di = new DictionaryAgroAgregat( _Id );
            this.Text = this.Text + ": " + Resources.Agregat;

            init_GridView();

            if( !bNew ) 
                GetFields();

            bFormLoading = false;
        }

        private void InitThis()
        {
            InitializeComponent();
            Localizations();
            leGroupe.Properties.DataSource = DictionaryAgroAgregat.GetGroupsList();
            leWork.DataSource = DictionaryAgroWorkType.GetList();
            btOrders.Visible = true;
        }

        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            pgProperties.SelectedObject = ( di as DictionaryAgroAgregat );
            leGroupe.EditValue = ( di as DictionaryAgroAgregat ).Id_main;

            if( _Id != 0 ) 
                LoadContent();
        }

        private void Localizations()
        {
            crProperties.Properties.Caption = Resources.Properties;
            erIdentifier.Properties.Caption = Resources.Identifier;
            erWidth.Properties.Caption = string.Format( "{0},{1}", Resources.Width, Resources.Meters );
            erUseDefault.Properties.Caption = Resources.OrdersDefaultUse;
            erInvNumber.Properties.Caption = Resources.NumberInv;
            erWorkDefault.Properties.Caption = Resources.WorkDefault;
            erSensorPresent.Properties.Caption = Resources.LogicalSensorPresent;
            erOutId.Properties.Caption = Resources.OuterBaseLink;

            lbGroup.Text = Resources.Groupe;
            lbVehicles.Text = Resources.VehicleDefaultUse;

            VID.Caption = Resources.Vehicle;
            TID.Caption = Resources.Groupe;
            SID.Caption = Resources.Sensor;
        }

        private bool ValidateIdentifierUnique()
        {
            ushort ident;

            if ( ushort.TryParse( ( di as DictionaryAgroAgregat ).Identifier.ToString(), out ident ) )
            {
                if ( ident == 0 ) 
                    return true;

                if ( DicUtilites.Test_UNIQUE( "agro_agregat", "Identifier", ident, Convert.ToInt32( txId.Text ) ) )
                {
                    return true;
                }
                else
                {
                    pgProperties.SetRowError( erIdentifier.Properties, Resources.ValueMustUnicum );
                    return false;
                }
            }
            else
                return true;
        }

        public override void UpdateGRN()
        {
            int iRow = _gvDict.FocusedRowHandle;

            if ( ( di as DictionaryAgroAgregat ).Def )
            {
                for ( int i = 0; i <= _gvDict.RowCount; i++ )
                {
                    _gvDict.SetRowCellValue( i, _gvDict.Columns["Def"], 0 );
                }
            }

            _gvDict.FocusedRowHandle = iRow;
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id"], txId.Text );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Name"], txName.Text );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Comment"], meComment.Text );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Width"], ( di as DictionaryAgroAgregat ).Width );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Identifier"], ( di as DictionaryAgroAgregat ).Identifier );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Def"], ( di as DictionaryAgroAgregat ).Def );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id_work"], ( di as DictionaryAgroAgregat ).Id_work );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["Id_main"], (int)( leGroupe.EditValue ?? 0 ) );
            _gvDict.SetRowCellValue( iRow, _gvDict.Columns["InvNumber"], ( di as DictionaryAgroAgregat ).InvNumber );
        }

        public override void SetFields()
        {
            di.Name = txName.Text;

            if ( di.Name.Length == 0 ) 
                return;

            di.Comment = meComment.Text;
            ( di as DictionaryAgroAgregat ).Id_main = (int)( leGroupe.EditValue ?? 0 );
        }

        public override bool ValidateFields()
        {
            if ( bFormLoading ) return false;
            if ( !DicUtilites.ValidateFieldsTextWFocus( txName, dxErrP ) | !ValidateIdentifierUnique() )
            {
                return false;
            }
            else
            {
                dxErrP.ClearErrors();
                return true;
            }
        }

        private void init_GridView()
        {
            if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
            {
                gcVehicles.DataSource = ( di as DictionaryAgroAgregat ).GetContent();
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                DataTable tbs = new DataTable();
                tbs.Columns.Add( "Id", typeof( int ) );
                tbs.Columns.Add( "TID", typeof( string ) );
                tbs.Columns.Add( "VID", typeof( int ) );
                tbs.Columns.Add( "SID", typeof( string ) );

                DataTable dtbs = ( di as DictionaryAgroAgregat ).GetContent();

                for (int i = 0; i < dtbs.Rows.Count; i++)
                {
                    DataRow row = tbs.NewRow();
                    row["Id"] = dtbs.Rows[i][0];
                    row["TID"] = dtbs.Rows[i][1];
                    row["VID"] = dtbs.Rows[i][2];
                    row["SID"] = dtbs.Rows[i][3];
                    tbs.Rows.Add(row);
                }

                gcVehicles.DataSource = tbs;
            }

            // leVehicleGrope
            rleTeam.ValueMember = "Id";
            rleTeam.DisplayMember = "Name";
            LookUpColumnInfoCollection coll1 = rleTeam.Columns;
            coll1.Add( new LookUpColumnInfo( "Name", "�������� ������" ) );
            rleTeam.BestFitMode = BestFitMode.BestFitResizePopup;
            rleTeam.SearchMode = SearchMode.AutoComplete;
            rleTeam.AutoSearchColumnIndex = 0;
            rleTeam.NullText = "[�������� ������]";

            // leVehicle
            rleVehicles.ValueMember = "Id";
            rleVehicles.DisplayMember = "Name";
            LookUpColumnInfoCollection coll2 = rleVehicles.Columns;
            coll2.Add( new LookUpColumnInfo( "Name", "��������" ) );
            rleVehicles.BestFitMode = BestFitMode.BestFitResizePopup;
            rleVehicles.SearchMode = SearchMode.AutoComplete;
            rleVehicles.AutoSearchColumnIndex = 0;
            rleVehicles.NullText = "[�������� ������]";

            // leSensors
            rleSensors.ValueMember = "id";
            rleSensors.DisplayMember = "Name";
            LookUpColumnInfoCollection coll3 = rleSensors.Columns;
            coll3.Add( new LookUpColumnInfo( "Name", "�������" ) );
            rleSensors.BestFitMode = BestFitMode.BestFitResizePopup;
            rleSensors.SearchMode = SearchMode.AutoComplete;
            rleSensors.AutoSearchColumnIndex = 0;
            rleSensors.NullText = "[�������� ������]";
            rleSensors.ButtonClick += new ButtonPressedEventHandler( LookUpEdit3_ButtonClick );

            rleSensors.Columns[0].Caption = Resources.NameRes;
            rleVehicles.Columns[0].Caption = Resources.NameRes;
            rleTeam.Columns[0].Caption = Resources.NameRes;

            gvVehicles.ShownEditor += new EventHandler( gvVehicles_ShownEditor );
            gvVehicles.HiddenEditor += new EventHandler( gvVehicles_HiddenEditor );
            gvVehicles.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( gvVehicles_CellValueChanged );
            gcVehicles.EmbeddedNavigator.ButtonClick +=
                new NavigatorButtonClickEventHandler( EmbeddedNavigator_ButtonClick );
        }

        void gvVehicles_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            if ( e.Column.FieldName == "TID" )
            {
                int IdRecord = 0;

                if ( !Int32.TryParse( gvVehicles.GetRowCellValue( e.RowHandle, gvVehicles.Columns["Id"] ).ToString(), out IdRecord ) )
                {
                    gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["Id"], -1 );
                    gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["VID"], -1 );
                }
            }
            else if ( e.Column.FieldName == "VID" )
            {
                int IdVehicle = 0;
                if ( Int32.TryParse( gvVehicles.GetRowCellValue( e.RowHandle, gvVehicles.Columns["VID"] ).ToString(), out IdVehicle ) )
                {
                    if ( IdVehicle > 0 )
                    {
                        gvVehicles.SetRowCellValue( e.RowHandle, gvVehicles.Columns["SID"], 0 );
                        ValidateFieldsAction();
                        bContentChange = true;
                        if ( e.RowHandle >= 0 && !RowUpdated.Contains( e.RowHandle ) )
                            RowUpdated.Add( e.RowHandle );
                    }
                }
            }
            else if ( e.Column.FieldName == "SID" )
            {
                ValidateFieldsAction();
                bContentChange = true;
                if ( e.RowHandle >= 0 && !RowUpdated.Contains( e.RowHandle ) )
                    RowUpdated.Add( e.RowHandle );
            }
        }

        private void EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if ( e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove )
            {
                int IdRecord = 0;

                if ( Int32.TryParse( gvVehicles.GetRowCellValue( gvVehicles.FocusedRowHandle, gvVehicles.Columns["Id"] ).ToString(), out IdRecord ) )
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName,
                                                                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question,
                                                                MessageBoxDefaultButton.Button2))
                    {
                        (di as DictionaryAgroAgregat).DeleteContent(IdRecord);

                        e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void gvVehicles_HiddenEditor(object sender, EventArgs e)
        {
            if ( _dvClone != null )
            {
                _dvClone.Dispose();
                _dvClone = null;
            }
        }

        private void gvVehicles_ShownEditor(object sender, EventArgs e)
        {
            if( gvVehicles.FocusedColumn.FieldName == "VID" && gvVehicles.ActiveEditor is LookUpEdit )
            {
                DataRow row = gvVehicles.GetDataRow( gvVehicles.FocusedRowHandle );
                int iGroup = 0;

                if( !Int32.TryParse( row["TID"].ToString(), out iGroup ) )
                    return;

                LookUpEdit edit = (LookUpEdit)gvVehicles.ActiveEditor;
                DataTable table = edit.Properties.DataSource as DataTable;
                _dvClone = new DataView( table );
                _dvClone.RowFilter = "[Team_id]=" + iGroup.ToString();
                edit.Properties.DataSource = _dvClone;
            }
            else if( gvVehicles.FocusedColumn.FieldName == "SID" && gvVehicles.ActiveEditor is LookUpEdit )
            {
                DataRow row = gvVehicles.GetDataRow( gvVehicles.FocusedRowHandle );
                int vehicle = 0;

                if( !Int32.TryParse( row["VID"].ToString(), out vehicle ) )
                    return;

                LookUpEdit edit = (LookUpEdit)gvVehicles.ActiveEditor;
                DataTable table = edit.Properties.DataSource as DataTable;
                _dvClone = new DataView( table );
                _dvClone.RowFilter = "[VID]=" + vehicle.ToString();
                edit.Properties.DataSource = _dvClone;
            }
        }

        private void LookUpEdit3_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            ( sender as LookUpEdit ).EditValue = null;
            ( sender as LookUpEdit ).Text = "";
        }

        public override bool LoadContent()
        {
            try
            {
                rleTeam.DataSource = GetVehicleGroup();
                rleVehicles.DataSource = GetListVehicle();
                rleSensors.DataSource = GetListLogics();

                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("{0}", ex.Message));
                return false;
            }
        }

        private object GetListLogics()
        {
            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                string sSQL = AgroQuery.Agregat.SelectSensors;
                return db.GetDataTable( sSQL );
            }
        }

        private object GetListVehicle()
        {
            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                string _sSQL = AgroQuery.Agregat.SelectVehicle;
                return db.GetDataTable( _sSQL );
            }
        }

        private object GetVehicleGroup()
        {
            IList<VehiclesGroup> listgroup = new List<VehiclesGroup>();

            listgroup = OrderItem.VehiclesModel.RootGroupsWithItems();

            return listgroup;
        }

        private void leGroupe_ButtonClick( object sender, ButtonPressedEventArgs e )
        {
            leGroupe.EditValue = null;
            leGroupe.Text = "";
        }

        private void leGroupe_EditValueChanged( object sender, EventArgs e )
        {
            ValidateFieldsAction();
        }

        private void pgProperties_CellValueChanged( object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e )
        {
            ValidateFieldsAction();
        }

        public override bool SaveContent()
        {
            try
            {
                SaveRows();
                return true;
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message );
                return false;
            }
        }

        private void SaveRows()
        {
            for( int i = 0; i < gvVehicles.RowCount; i++ )
            {
                int IdRecord = 0;

                if( Int32.TryParse( gvVehicles.GetRowCellValue( i, gvVehicles.Columns["Id"] ).ToString(), out IdRecord ) )
                {
                    if( IdRecord < 0 )
                    {
                        SaveRow( i );
                    }
                    else if( RowUpdated.Count > 0 && RowUpdated.Contains( i ) )
                    {
                        SaveRow( i );
                        RowUpdated.Remove( i );
                    }
                }
            }
        }

        private void SaveRow( int Row )
        {
            int IdRecord = 0;
            int IdVehical = 0;
            int IdSensor = 0;

            if( !Int32.TryParse( gvVehicles.GetRowCellValue( Row, gvVehicles.Columns["VID"] ).ToString(), out IdVehical ) || IdVehical == -1 ) 
                return;

            Int32.TryParse( gvVehicles.GetRowCellValue( Row, gvVehicles.Columns["SID"] ).ToString(), out IdSensor );

            if( Int32.TryParse( gvVehicles.GetRowCellValue( Row, gvVehicles.Columns["Id"] ).ToString(), out IdRecord ) )
            {
                string linkedAgregatName;

                if( !( di as DictionaryAgroAgregat ).GetLinkForVehicle( IdVehical, out linkedAgregatName ) )
                {
                    if( IdRecord > 0 )
                        ( di as DictionaryAgroAgregat ).EditContent( IdRecord, IdVehical, IdSensor );
                    else
                        gvVehicles.SetRowCellValue( Row, gvVehicles.Columns["Id"], ( di as DictionaryAgroAgregat ).AddContent( IdVehical, IdSensor ) );
                }
                else
                    XtraMessageBox.Show( string.Format( "{0}{1}{1}{2}", Resources.RecordDontSave, Environment.NewLine, linkedAgregatName ), Resources.ApplicationName );
            }
        }
    }
}