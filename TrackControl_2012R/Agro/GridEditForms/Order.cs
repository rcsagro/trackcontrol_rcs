using System;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.Reports;
using TrackControl.General.Maps; 
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Card;
using LocalCache;
using CheckZones;
using BaseReports;
using BaseReports.Procedure;
using ReportsOnCheckZones;
//using GIS;
//using System.Drawing;


namespace Agro.GridEditForms
{
    public partial class Order : Form, IDictionary
    {

        public DateTime DateOrder
        {
            get { return dtDateInit.Value; }
            set { dtDateInit.Value = value; }
        }
        //���������� ��� ��������� �������� �������
        int BASE_X = 0;
        int BASE_Y = 0;
        int PRV_X = 0;
        int PRV_Y = 0;
        int I_START = 0;
        int X_START = 0;
        int Y_START = 0;
        int MAX_X =0;
        int MAX_Y =0;
        //������ ��� ������ ���������� ����� �� ������� � ������
        //List<ItemSqClass> lSq = new List<ItemSqClass>();
        private atlantaDataSet _dsAtlanta = new atlantaDataSet();
        private string CONNECTION_STRING;
        private GridView _gvOrder;
        private bool bLoad = true;
        private bool bDataSetReady;
        //������������� ������ ��� �������� ������� �� �������
        private int ID_main_ext;
        //������ ������������
        double dbWidth;
        //�������������� ������� ������� � ����� ����������� ���
        private enum WorkIDzone : int
        {
            ���_��� = -1,
            �����_����� = -2
        }
        private enum WorkRegimes : int
        {
            ������ = 0,
            ��������������,
            ������
        }
        //http://comsql:88/redmine/issues/show/32 - ������� ����������� ������� ���� (���� �� ��������)
        TimeSpan tsDelta = new TimeSpan(0, 1, 0);
        #region ������������
        public Order()
        {

            CONNECTION_STRING = ConnectMySQL.ConnectionString();
            InitializeComponent();
            DicUtilites.ComboLoad(cbGroupe, "SELECT   team.id, team.Name FROM    team ORDER BY  team.Name");
            DicUtilites.ComboLoad(cbDriver, "SELECT   driver.id, driver.Family FROM   driver ORDER BY   driver.Family");
            DicUtilites.ComboLoad(cbAgregat, "SELECT   agro_agregat.Id, agro_agregat.Name FROM   agro_agregat ORDER BY   agro_agregat.Name");
            //DicUtilites.ComboLoad(cbGroupeField, "SELECT   agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM   agro_fieldgroupe ORDER BY   agro_fieldgroupe.Name");
            //DicUtilites.ComboLoad(cbWorkType, " SELECT   agro_work.Id, agro_work.Name FROM   agro_work ORDER BY   agro_work.Name");
        }
        public Order(ref GridView gvOrder, bool bNew)
            : this()
        {
            _gvOrder = gvOrder;
            //_dsAtlanta = dsAtlanta;
            EventArgs e = new EventArgs();

            if (!bNew)
            {
                txId.Text = _gvOrder.GetRowCellValue(_gvOrder.FocusedRowHandle, "ID").ToString();
                ConnectMySQL cnMySQL = new ConnectMySQL();

                string SQLselect = "";
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    SQLselect = "SELECT   agro_order.Id, agro_order.`Date`, agro_order.Id_mobitel, mobitels.Name AS MName,"
                    + " agro_order.Id_driver, driver.Name AS DName, agro_order.Id_agregat, agro_agregat.Name AS AName, agro_order.`Comment`,"
                    + " vehicle.Team_id,team.Name AS TName,agro_order.Regime"
                    + " FROM    agro_order "
                    + " LEFT OUTER JOIN  agro_agregat ON agro_order.Id_agregat = agro_agregat.Id "
                    + " LEFT OUTER JOIN  driver ON agro_order.Id_driver = driver.id AND agro_order.Id_driver = driver.id "
                    + " LEFT OUTER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID"
                    + " LEFT OUTER JOIN  vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id"
                    + " LEFT OUTER JOIN  team ON vehicle.Team_id = team.id"
                    + " WHERE agro_order.Id = " + txId.Text;
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SQLselect = "SELECT   agro_order.Id, agro_order.Date, agro_order.Id_mobitel, mobitels.Name AS MName,"
                   + " agro_order.Id_driver, driver.Name AS DName, agro_order.Id_agregat, agro_agregat.Name AS AName, agro_order.Comment,"
                   + " vehicle.Team_id,team.Name AS TName,agro_order.Regime"
                   + " FROM    agro_order "
                   + " LEFT OUTER JOIN  agro_agregat ON agro_order.Id_agregat = agro_agregat.Id "
                   + " LEFT OUTER JOIN  driver ON agro_order.Id_driver = driver.id AND agro_order.Id_driver = driver.id "
                   + " LEFT OUTER JOIN  mobitels ON agro_order.Id_mobitel = mobitels.Mobitel_ID"
                   + " LEFT OUTER JOIN  vehicle ON mobitels.Mobitel_ID = vehicle.Mobitel_id"
                   + " LEFT OUTER JOIN  team ON vehicle.Team_id = team.id"
                   + " WHERE agro_order.Id = " + txId.Text;
                }

                MySqlDataReader rdOrder = cnMySQL.GetDataReader(SQLselect);
                if (rdOrder.Read())
                {
                    txComment.Text = rdOrder.GetString(rdOrder.GetOrdinal("Comment"));
                    dtDateInit.Value = rdOrder.GetDateTime(rdOrder.GetOrdinal("Date"));
                    DicUtilites.ComboSet(rdOrder, cbGroupe, "Team_id", "TName");
                    cbGroupe_SelectedIndexChanged(cbGroupe, e);
                    DicUtilites.ComboSet(rdOrder, cbMobitel, "Id_mobitel", "MName");
                    DicUtilites.ComboSet(rdOrder, cbDriver, "Id_driver", "DName");
                    DicUtilites.ComboSet(rdOrder, cbAgregat, "Id_agregat", "AName");
                    if ((cbAgregat.SelectedValue != null) && (DicUtilites.ValidateCombo(cbGroupe, errP)))
                        txAgregatWidth.Text = GetAgregatWidth((int)cbAgregat.SelectedValue);
                    SetRegime(rdOrder.GetInt32(rdOrder.GetOrdinal("Regime")), true);
                }
                //�������� ����� ���������� �� ����
                LoadForm();
                btDelete.Enabled = TestLinks();
            }
            else
            {

                cbGroupe_SelectedIndexChanged(cbGroupe, e);
                cbGroupeField_SelectedIndexChanged(cbGroupeField, e);
                SetRegime((int)WorkRegimes.������, true);
                btDelete.Enabled = false;
            }

            LoadSubForm();
            //gvOrder.OptionsView.ShowGroupPanel = false;
            LoadSubFormFuel();
            //ValidateFields();
            cbGroupeField_SelectedIndexChanged(cbGroupeField, e);

            //cbAgregat_SelectedIndexChanged(cbAgregat, e);
            //cbField_SelectedIndexChanged(cbField, e);
            bLoad = false;
            btSave.Enabled = false;

        }
        public Order(DateTime dtWork)
            : this()
        {
            dtDateInit.Value = dtWork;

        }
        #endregion
        #region IDictionary Members

        public bool ValidateFields()
        {
            btSave.Enabled = true;
            if (!DicUtilites.ValidateCombo(cbMobitel, errP))
                //if (!DicUtilites.ValidateCombo(cbAgregat, errP) || !DicUtilites.ValidateCombo(cbDriver, errP) || !DicUtilites.ValidateCombo(cbMobitel, errP))
                btSave.Enabled = false;
            else
                btSave.Enabled = true;
            return btSave.Enabled;
        }

        public void SaveChanges()
        {
            if (ValidateFields())
            {
                ConnectMySQL _cnMySQL = new ConnectMySQL();
                if (txId.Text == "0")
                {
                    string sSQLinsert = "";
                    if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                    {
                        sSQLinsert = "INSERT INTO `agro_order`(`Date`,Id_mobitel,Id_driver,Id_agregat,Regime,`Comment`) VALUES ('"
                            + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
                            + cbMobitel.SelectedValue + ","
                            + ( cbDriver.SelectedValue == null ? 0 : cbDriver.SelectedValue ) + ","
                            + ( cbAgregat.SelectedValue == null ? 0 : cbAgregat.SelectedValue ) + "," + GetRegime() + ",'" + txComment.Text + "')";
                    }
                    else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                    {
                        sSQLinsert = "INSERT INTO agro_order(Date,Id_mobitel,Id_driver,Id_agregat,Regime,Comment) VALUES ('"
                            + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
                            + cbMobitel.SelectedValue + ","
                            + ( cbDriver.SelectedValue == null ? 0 : cbDriver.SelectedValue ) + ","
                            + ( cbAgregat.SelectedValue == null ? 0 : cbAgregat.SelectedValue ) + "," + GetRegime() + ",'" + txComment.Text + "')";
                    }
                    _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);
                    _gvOrder.AddNewRow();
                    txId.Text = Convert.ToString(_cnMySQL.GetScalarValue("SELECT Id FROM agro_order WHERE ID = LAST_INSERT_ID()"));
                    btAdd.Enabled = true;
                }
                else
                {
                    string sSQLupdate = "";
                    if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                    {
                        sSQLupdate = "UPDATE agro_order SET Id_mobitel = " + cbMobitel.SelectedValue
                            + ", Date = '" + dtDateInit.Value.ToString( "yyyy-MM-dd" )
                            + "',Id_driver = " + ( cbDriver.SelectedValue == null ? 0 : cbDriver.SelectedValue )
                            + ", Id_agregat = " + ( cbAgregat.SelectedValue == null ? 0 : cbAgregat.SelectedValue )
                            + ", Regime = " + GetRegime()
                            + ",Comment = '" + this.txComment.Text
                            + "' WHERE (ID = " + txId.Text + ")";
                    }
                    else if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                    {
                        sSQLupdate = "UPDATE agro_order SET Id_mobitel = " + cbMobitel.SelectedValue
                           + ", Date = '" + dtDateInit.Value.ToString( "yyyy-MM-dd" )
                           + "',Id_driver = " + ( cbDriver.SelectedValue == null ? 0 : cbDriver.SelectedValue )
                           + ", Id_agregat = " + ( cbAgregat.SelectedValue == null ? 0 : cbAgregat.SelectedValue )
                           + ", Regime = " + GetRegime()
                           + ",Comment = '" + this.txComment.Text
                           + "' WHERE (ID = " + txId.Text + ")";
                    }
                    //Console.WriteLine(sSQLupdate);   
                    _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                }

                _gvOrder.SetRowCellValue(_gvOrder.FocusedRowHandle, _gvOrder.Columns.ColumnByName("ID"), txId.Text);
                _gvOrder.SetRowCellValue(_gvOrder.FocusedRowHandle, _gvOrder.Columns.ColumnByName("Date"), dtDateInit.Value);
                _gvOrder.SetRowCellValue(_gvOrder.FocusedRowHandle, _gvOrder.Columns.ColumnByName("Driver"), cbDriver.Text);
                _gvOrder.SetRowCellValue(_gvOrder.FocusedRowHandle, _gvOrder.Columns.ColumnByName("Mobitel"), cbMobitel.Text);
                //_gvOrder.SetRowCellValue(_gvOrder.FocusedRowHandle, _gvOrder.Columns.ColumnByName("Agregat"), cbAgregat.Text);
                btSave.Enabled = false;
                _cnMySQL.CloseMySQLConnection();
            }
        }

        public bool TestLinks()
        {
            //throw new Exception("The method or operation is not implemented.");
            return true;
        }

        public bool DeleteRecord()
        {
            return true;
        }
        public bool DeleteRecordContentTotals()
        {
            try
            {
                string sSQLdel = "DELETE FROM agro_ordert WHERE ID_main = " + txId.Text
                + " AND  (Id_zone = " + Convert.ToString((int)WorkIDzone.�����_�����)
                + " OR  Id_zone = " + Convert.ToString((int)WorkIDzone.���_���) + ")";
                ConnectMySQL _cnMySQL = new ConnectMySQL();
                _cnMySQL.ExecuteNonQueryCommand(sSQLdel);
                _cnMySQL.CloseMySQLConnection();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

#endregion
        #region ����� ������
        private void cbGroupe_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (DicUtilites.ValidateCombo(cbGroupe, errP))
            {
                cbMobitel.DataSource = null;
                cbMobitel.Items.Clear();
                DicUtilites.ComboLoad(cbMobitel, "SELECT   vehicle.Mobitel_id, vehicle.MakeCar, vehicle.Team_id FROM   vehicle"
                + " WHERE   (vehicle.Team_id = " + (int)cbGroupe.SelectedValue + " OR vehicle.Team_id IS NULL)");
            }
        }
        private void cbAgregat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((cbAgregat.SelectedValue != null) && (DicUtilites.ValidateCombo(cbGroupe, errP)))
            {
                if (!bLoad)
                {
                    txAgregatWidth.Text = GetAgregatWidth((int)cbAgregat.SelectedValue);
                    if (txAgregatWidth.Text == "") txAgregatWidth.Text = "0";

                    if (ValidateFields()) btSave.Enabled = true;
                    if (DicUtilites.ValidateCombo(cbWorkType, errP)) GetPrice((int)cbWorkType.SelectedValue);
                    double dbWidth=0;
                    if (Double.TryParse(txAgregatWidth.Text,out dbWidth))
                        RecalcSquare(dbWidth);
                }
            }
        }
        private void cbGroupeField_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLoad)
            {
                if (DicUtilites.ValidateCombo(cbGroupeField, errP))
                {
                    cbField.DataSource = null;
                    cbField.Items.Clear();
                    DicUtilites.ComboLoad(cbField, "SELECT   agro_field.Id, agro_field.Name FROM   agro_field"
                    + " WHERE   (agro_field.Id_main = " + (int)cbGroupeField.SelectedValue + ")");
                }
            }
        }
        private void btOk_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }
        private void txComment_TextChanged(object sender, EventArgs e)
        {
            if (!bLoad)
                if (ValidateFields()) btSave.Enabled = true;
        }
        private void cbMobitel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLoad)
            {
                if (ValidateFields()) btSave.Enabled = true;
                if (DicUtilites.ValidateCombo(cbWorkType, errP)) GetPrice((int)cbWorkType.SelectedValue);
                if (cbMobitel.SelectedIndex > 0)
                {
                    string SQLselect = "SELECT   driver.id, driver.Family "
                    + " FROM   vehicle INNER JOIN driver ON vehicle.driver_id = driver.id "
                    + " WHERE   vehicle.Mobitel_id = " + cbMobitel.SelectedValue.ToString();
                    ConnectMySQL _cnMySQL = new ConnectMySQL();
                    MySqlDataReader rdMobitel = _cnMySQL.GetDataReader(SQLselect);
                    if (rdMobitel.Read())
                    {
                        //DicUtilites.ComboSet(rdMobitel, cbDriver, "id", "Family");
                        cbDriver.SelectedValue = rdMobitel.GetInt32(rdMobitel.GetOrdinal("id"));
                    }
                    rdMobitel.Close();
                    _cnMySQL.CloseMySQLConnection();
                }

            }
        }
        private void cbDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLoad)
                if (ValidateFields()) btSave.Enabled = true;
        }
        private void dtDateInit_ValueChanged(object sender, EventArgs e)
        {
            if (!bLoad)
                if (ValidateFields()) btSave.Enabled = true;
        }
        private void cbWorkType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLoad)
            {
                if (ValidateFields()) btSave.Enabled = true;
                if (DicUtilites.ValidateCombo(cbWorkType, errP)) GetPrice((int)cbWorkType.SelectedValue);
            }
        }
        private void cbField_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!bLoad)
            //    if (cbField.SelectedValue != null) GetZone();
        }
        private void cbGroupeField_Enter(object sender, EventArgs e)
        {
            if (cbGroupeField.Items.Count == 0)
                DicUtilites.ComboLoad(cbGroupeField, "SELECT   agro_fieldgroupe.Id, agro_fieldgroupe.Name FROM   agro_fieldgroupe ORDER BY   agro_fieldgroupe.Name");

        }
        private void cbWorkType_Enter(object sender, EventArgs e)
        {
            if (cbWorkType.Items.Count == 0)
                DicUtilites.ComboLoad(cbWorkType, " SELECT   agro_work.Id, agro_work.Name FROM   agro_work ORDER BY   agro_work.Name");

        }
        private void rbHandle_Click(object sender, EventArgs e)
        {
            if (rbHandle.Checked)
            {
                SetRegime((int)WorkRegimes.������, false);
                btSave.Enabled = true;
            }
        }
        private void rbAuto_Click(object sender, EventArgs e)
        {
            if (rbAuto.Checked)
            {
                SetRegime((int)WorkRegimes.��������������, false);
                btSave.Enabled = true;
            }
        }
        private void rbClose_Click(object sender, EventArgs e)
        {
            if (rbClose.Checked)
            {
                SetRegime((int)WorkRegimes.������, false);
                btSave.Enabled = true;
            }

        }
        private string GetAgregatWidth(int ID_Agregat)
        {
            ConnectMySQL cnMySQL = new ConnectMySQL();
            return Convert.ToString(cnMySQL.GetScalarValue("SELECT   agro_agregat.WIDTH FROM   agro_agregat WHERE   agro_agregat.Id = " + cbAgregat.SelectedValue));
            cnMySQL.CloseMySQLConnection();
        }
        //������ ���������� �������
        private void SetRegime(int iRegime, bool bSetRadioButton)
        {
            switch (iRegime)
            {
                case (int)WorkRegimes.������:
                    {
                        gbHandle.Enabled = false;
                        gbAuto.Enabled = false;
                        gbPrice.Enabled = false;
                        btRecalcContent.Enabled = false;
                        if (bSetRadioButton) rbClose.Checked = true;
                        btDelete.Enabled = false;
                        break;
                    }
                case (int)WorkRegimes.��������������:
                    {
                        gbHandle.Enabled = false;
                        gbAuto.Enabled = true;
                        gbPrice.Enabled = true;
                        btRecalcContent.Enabled = true;
                        if (bSetRadioButton) rbAuto.Checked = true;
                        btDelete.Enabled = true;
                        break;
                    }
                default:
                    {
                        gbHandle.Enabled = true;
                        gbAuto.Enabled = false;
                        gbPrice.Enabled = true;
                        btRecalcContent.Enabled = true;
                        if (bSetRadioButton) rbHandle.Checked = true;
                        btDelete.Enabled = true;
                        break;
                    }
            }
        }
        private int GetRegime()
        {
            if (rbClose.Checked) return (int)WorkRegimes.������;
            if (rbAuto.Checked) return (int)WorkRegimes.��������������;
            return (int)WorkRegimes.������;
        }
        //�������� ����� ���������� �� ����
        public void LoadForm()
        {
            ConnectMySQL cnMySQL = new ConnectMySQL();
            string sSQLselect = "SELECT   agro_order.LocationStart, agro_order.LocationEnd, agro_order.TimeStart,"
            + " agro_order.TimeEnd, agro_order.TimeWork, agro_order.TimeMove, agro_order.Distance, agro_order.SpeedAvg"
            + " FROM   agro_order"
            + " WHERE   agro_order.Id = " + txId.Text;
            //Console.WriteLine(sSQLselect);
            DataTable dtMain = cnMySQL.GetDataTable(sSQLselect);
            gcMain.DataSource = dtMain;
            cnMySQL.CloseMySQLConnection();
        }
        //��������� �������� ����� ���������� �� ����
        public void GetTotals(int Mobitel_Id)
        {
            if (ID_main_ext == 0) Create_dsAtlanta(Mobitel_Id);
            tslbOrder.Text = "������������ ������ �� �����";
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(Mobitel_Id);
            BaseReports.Procedure.IAlgorithm brKmDay = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.KilometrageDays();
            brKmDay.SelectItem(m_row);
            brKmDay.Run();
            atlantaDataSet.KilometrageReportDayRow[] km_Day_rows = (atlantaDataSet.KilometrageReportDayRow[])_dsAtlanta.KilometrageReportDay.Select("MobitelId = " + Mobitel_Id);
            if (km_Day_rows.Length > 0)
            {
                foreach (atlantaDataSet.KilometrageReportDayRow km_Day_row in km_Day_rows)
                {
                    if (km_Day_row.InitialTime.ToShortDateString() == dtDateInit.Value.ToShortDateString())
                    {

                        //  agro_order.LocationStart, agro_order.LocationEnd, 
                        //agro_order.TimeStart, agro_order.TimeEnd, 
                        //agro_order.TimeWork, agro_order.TimeMove, 
                        //agro_order.Distance, agro_order.SpeedAvg

                        ConnectMySQL _cnMySQL = new ConnectMySQL();
                        int ID_main = 0;
                        if (ID_main_ext > 0) ID_main = ID_main_ext;
                        else ID_main = Convert.ToInt32(txId.Text);
                        string sSQLupdate = "UPDATE agro_order SET TimeStart = ?TimeStart"
                            + ", TimeEnd = ?TimeEnd"
                            + ", TimeWork = '" + km_Day_row.IntervalWork
                            + "', TimeMove = '" + km_Day_row.IntervalMove
                            + "', LocationStart = '" + km_Day_row.LocationStart.Replace("'", "")
                            + "', LocationEnd = '" + km_Day_row.LocationEnd.Replace("'", "")
                            + "', Distance = " + Math.Round(km_Day_row.Distance, 2).ToString().Replace(",", ".")
                            + ", SpeedAvg = " + Math.Round(km_Day_row.AverageSpeed, 2).ToString().Replace(",", ".")
                            + " WHERE (ID = " + ID_main.ToString() + ")";
                        //Console.WriteLine(sSQLupdate);  
                        //+ ", Sum = " + Math.Round(zw.Distance * (double)gbvContent.GetRowCellValue(zw.RowHandle, gbvContent.Columns.ColumnByName("Price")), 2).ToString().Replace(",", ".")
                        MySqlParameter[] parDate = new MySqlParameter[2];
                        parDate[0] = new MySqlParameter();
                        parDate[0].ParameterName = "?TimeStart";
                        parDate[0].Value = km_Day_row.InitialTime;
                        parDate[0].MySqlDbType = MySqlDbType.DateTime;
                        parDate[1] = new MySqlParameter();
                        parDate[1].ParameterName = "?TimeEnd";
                        parDate[1].Value = km_Day_row.FinalTime;
                        parDate[1].MySqlDbType = MySqlDbType.DateTime;
                        _cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
                        _cnMySQL.CloseMySQLConnection();
                        if (ID_main_ext == 0)
                        {
                            LoadForm();
                            tslbOrder.Text = "������";
                        }
                        //Algorithm.bDisableAction =false;
                        return;
                    }
                }

            }
            tslbOrder.Text = "������";
        }

#endregion
        #region ����� �� ����
        //��������� ��� ����������� ���������� ������ � ����
        private struct ZoneWork
        {
            public List<ZoneWork> list_zw;
            public int ZoneID;
            public int ID_orderT;
            public int ID_mobitel;
            public Double Distance;
            public Double Square;
            public Double SquareCalc;
            public DateTime dtStart;
            public DateTime dtEnd;
            public DateTime dtEndDay;
            public TimeSpan tsMove;
            public TimeSpan tsStop;
            public TimeSpan tsOut;
            public TimeSpan tsEngineOn;
            public int RowHandle;
            public bool AddNewRow;
            public bool InZone;
            public double Price;
            public double FuelStart;
            public double FuelEnd;
            public double FuelAdd;
            public double FuelSub;
            public double FuelExpens;
            public double FuelExpensAvg;
            public double FuelExpensAvgRate;
            public double Fuel_ExpensMove;
            public double Fuel_ExpensStop;
            public double Fuel_ExpensTotal;
            public double Fuel_ExpensAvg;
            public double Fuel_ExpensAvgRate;
            public ZoneWork(int iZoneID, int iRowHandle, DateTime dtInit, bool bAddNewRow, double dbPrice, int iMobitelID)
            {
                ZoneID = iZoneID;
                RowHandle = iRowHandle;
                Distance = 0;
                tsMove = new TimeSpan();
                tsStop = tsMove;
                tsEngineOn = tsMove;
                tsOut = tsMove;
                dtStart = new DateTime(dtInit.Year, dtInit.Month, dtInit.Day);
                dtEnd = dtStart.AddDays(1);
                dtEndDay = dtEnd.AddMinutes(-1) ;
                AddNewRow = bAddNewRow;
                InZone = false;
                ID_orderT = 0;
                Price = dbPrice;
                list_zw = new List<ZoneWork>();
                FuelStart = 0;
                FuelEnd = 0;
                FuelAdd = 0;
                FuelSub = 0;
                FuelExpens = 0;
                FuelExpensAvg = 0;
                FuelExpensAvgRate = 0;
                Fuel_ExpensMove = 0;
                Fuel_ExpensStop = 0;
                Fuel_ExpensTotal = 0;
                Fuel_ExpensAvg = 0;
                Fuel_ExpensAvgRate = 0;
                Square = 0;
                SquareCalc = 0;
                ID_mobitel = iMobitelID;
            }
        }
        #region ������ ����������
        //������ ������������ ������ 
        private void btAdd_Click(object sender, EventArgs e)
        {
            if (DicUtilites.ValidateCombo(cbField, errP))
            {
                AddRecord((int)cbField.SelectedValue, 0);
                LoadSubForm();
            }
        }
        //����� �����
        private void btSeek_Click(object sender, EventArgs e)
        {
            //if (DicUtilites.ValidateCombo(cbMobitel, errP) && DicUtilites.ValidateCombo(cbAgregat, errP))
            if (DicUtilites.ValidateCombo(cbMobitel, errP))
            {
                GetFieldsAuto((int)cbMobitel.SelectedValue, true);
            }
        }
        //�������� ���������� ������ � ����
        private void btRecalcContent_Click(object sender, EventArgs e)
        {
            GetFact();

        }
        //�������� ���� ������� � ������ � ����
        private void btDelete_Click(object sender, EventArgs e)
        {
            if (gbvContent == null || gbvContent.RowCount == 0) return;
            if (DialogResult.Yes == MessageBox.Show("������������� ������� ����������� ������?", "��������", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                DataRow[] rows = new DataRow[gbvContent.RowCount];

                for (int i = 0; i < gbvContent.RowCount; i++)
                    rows[i] = gbvContent.GetDataRow(i);
                gbvContent.BeginSort();
                try
                {
                    foreach (DataRow row in rows)
                    {

                        DeleteRecordContent((int)row["Id"]);
                        row.Delete();
                    }
                }
                finally
                {
                    gbvContent.EndSort();
                }
            }
        }
        #endregion
        //�������� ����������� ������
        public void LoadSubForm()
        {
            ConnectMySQL cnMySQL = new ConnectMySQL();
            //string sSQLselect = "SELECT  agro_ordert.Id, agro_fieldgroupe.Id AS GroupeFieldId, agro_fieldgroupe.Name AS GroupeField, agro_ordert.Id_field as  FieldId,"
            //+ " agro_field.Name AS FName,  zones.Zone_ID, zones.Square * 100 AS Square, agro_ordert.TimeStart, agro_ordert.TimeEnd,"
            //+ " agro_ordert.TimeMove, agro_ordert.TimeStop, agro_ordert.TimeRate, agro_ordert.Id_work AS Work,"
            //+ " agro_ordert.FactTime,agro_ordert.TimeOut, agro_ordert.Distance,agro_ordert.FactSquare,agro_ordert.FactSquareCalc, agro_ordert.Price, agro_ordert.Sum, agro_ordert.SpeedAvg, agro_ordert.Comment"
            //+ " FROM    agro_field "
            //+ " INNER JOIN  agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id"
            //+ " INNER JOIN  agro_ordert ON agro_ordert.Id_field = agro_field.Id"
            //+ " INNER JOIN  zones ON agro_ordert.Id_zone = zones.Zone_ID"
            //+ " WHERE   agro_ordert.Id_main = " + txId.Text;
            //Console.WriteLine(sSQLselect);
            string sSQLselect = "SELECT  agro_ordert.Id, agro_fieldgroupe.Id AS GroupeFieldId, agro_fieldgroupe.Name AS GroupeField, agro_ordert.Id_field as  FieldId,"
            + " agro_field.Name AS FName,  zones.Zone_ID, zones.Square * 100 AS Square, agro_ordert.TimeStart, agro_ordert.TimeEnd,"
            + " agro_ordert.TimeMove, agro_ordert.TimeStop, agro_ordert.TimeRate, agro_ordert.Id_work AS Work,"
            + " agro_ordert.FactTime,agro_ordert.TimeOut, agro_ordert.Distance,agro_ordert.FactSquare,agro_ordert.FactSquareCalc, agro_ordert.Price, agro_ordert.Sum, agro_ordert.SpeedAvg"
            + " FROM    agro_field "
            + " INNER JOIN  agro_fieldgroupe ON agro_field.Id_main = agro_fieldgroupe.Id"
            + " INNER JOIN  agro_ordert ON agro_ordert.Id_field = agro_field.Id"
            + " INNER JOIN  zones ON agro_ordert.Id_zone = zones.Zone_ID"
            + " WHERE   agro_ordert.Id_main = " + txId.Text;
            DataSet dsOrder = cnMySQL.GetDataSet(sSQLselect, "agro_ordert");
            sSQLselect = "SELECT  agro_ordert.Id as IdP, agro_ordert.Id_work AS Work,agro_ordert.FactSquare as FSP, agro_ordert.Price, agro_ordert.Sum,  agro_ordert.Comment"
            + " FROM    agro_ordert "
            + " WHERE   agro_ordert.Id_main = " + txId.Text;
            cnMySQL.FillDataSet(sSQLselect, dsOrder, "agro_ordert_Price");
            DataColumn keyColumn = dsOrder.Tables["agro_ordert"].Columns["Id"];
            DataColumn foreignKeyColumn = dsOrder.Tables["agro_ordert_Price"].Columns["IdP"];
            dsOrder.Relations.Add("����������� ������", keyColumn, foreignKeyColumn);
            gcContent.DataSource = dsOrder.Tables["agro_ordert"];
            gcContent.ForceInitialize();
            gcContent.LevelTree.Nodes.Add("����������� ������", gvContentPrice);
            sSQLselect = " SELECT   agro_work.Id, agro_work.Name FROM   agro_work ORDER BY   agro_work.Name";
            WorkLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
            cnMySQL.CloseMySQLConnection();
        }
        //����������� ���� �� ������
        public double GetPrice(int iWorkType)
        {
            double dbPrice = 0;
            if (DicUtilites.ValidateCombo(cbAgregat, errP) && DicUtilites.ValidateCombo(cbMobitel, errP))
            {
                ConnectMySQL cnMySQL = new ConnectMySQL();
                string SQLselect = "";
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    SQLselect = "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                    + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                    + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                    + " WHERE   agro_price.DateComand < ?DateComand AND agro_pricet.Id_work = " + iWorkType + " AND agro_pricet.Id_mobitel = " + cbMobitel.SelectedValue + " AND agro_pricet.Id_agregat = " + cbAgregat.SelectedValue
                    + " ORDER BY   agro_price.DateComand DESC";
                }
                else if((DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SQLselect = "SELECT   agro_pricet.Price, agro_unit.Factor FROM   agro_pricet"
                    + " INNER JOIN agro_price ON agro_pricet.Id_main = agro_price.Id "
                    + " INNER JOIN agro_unit ON agro_pricet.Id_unit = agro_unit.Id "
                    + " WHERE   agro_price.DateComand < @DateComand AND agro_pricet.Id_work = " + iWorkType + " AND agro_pricet.Id_mobitel = " + cbMobitel.SelectedValue + " AND agro_pricet.Id_agregat = " + cbAgregat.SelectedValue
                    + " ORDER BY   agro_price.DateComand DESC";
                }
                //Console.WriteLine(SQLselect);  
                MySqlParameter[] parDate = new MySqlParameter[1];
                parDate[0] = new MySqlParameter();
                parDate[0].ParameterName = driverDb.ParamPrefics + "DateComand";
                parDate[0].Value = dtDateInit.Value;
                parDate[0].MySqlDbType = MySqlDbType.DateTime;
                MySqlDataReader rdOrder = cnMySQL.GetDataReader(SQLselect, parDate);
                if (rdOrder.Read())
                {
                    double dbFactor = rdOrder.GetDouble(rdOrder.GetOrdinal("Factor"));
                    dbPrice = Math.Round(rdOrder.GetDouble(rdOrder.GetOrdinal("Price")) * dbFactor / 100, 2);
                    //���������� � �������
                    txPrice.Text = Convert.ToString(dbPrice);

                }
                else
                {
                    txPrice.Text = "0";
                }
                cnMySQL.CloseMySQLConnection();

            }
            //txSum.Text = Convert.ToString(Math.Round(dbPrice * Convert.ToDouble(txSquare.Text),2)); 
            return dbPrice;
        }
        //���� ������� ��������� ����
        public int GetZone(int ID_Field)
        {
            ConnectMySQL cnMySQL = new ConnectMySQL();
            string SQLselect = "SELECT   zones.Zone_ID FROM   agro_field "
            + " INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID"
            + " WHERE   agro_field.Id = " + ID_Field;
            int ID_zone = (int)cnMySQL.GetScalarValue(SQLselect);
            cnMySQL.CloseMySQLConnection();
            return ID_zone;
        }
        //��������� ����������� ������ � ������ � ������������ ��� �����
        private void GetFact()
        {
            tslbOrder.Text = "���������� ������";
            Create_dsAtlanta((int)cbMobitel.SelectedValue);
            DateTime dtSeek = new DateTime(dtDateInit.Value.Year, dtDateInit.Value.Month, dtDateInit.Value.Day);
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + cbMobitel.SelectedValue.ToString() + " AND time >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and time  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#", "time");
            RunRotation((int)cbMobitel.SelectedValue);
            dbWidth = Convert.ToDouble(txAgregatWidth.Text) * 0.001;
            tspbZone.Visible = true;
            for (int i = 0; i < gbvContent.RowCount; i++)
            {
                tslbOrder.Text = "��������� ������ ��� ���� " + (string)gbvContent.GetRowCellValue(i, "Field");
                ZoneWork zw = new ZoneWork((int)gbvContent.GetRowCellValue(i, gbvContent.Columns.ColumnByName("Zone_ID")),
                    i, dtDateInit.Value, false, (double)gbvContent.GetRowCellValue(i, gbvContent.Columns.ColumnByName("PriceC")), (int)cbMobitel.SelectedValue);
                zw.ID_orderT = (int)gbvContent.GetRowCellValue(i, gbvContent.Columns.ColumnByName("Id"));
                //double er = 100 * GetFactItemSqWAgregat(ref zw);
                
                tspbZone.Text = (string)gbvContent.GetRowCellValue(i, gbvContent.Columns.ColumnByName("Field"));
                zw.SquareCalc = 100 * GetFactItemSqWAgregat(ref zw);
                GetFactZone(ref zw);
                zw.Square = 100 * zw.Distance * Convert.ToDouble(txAgregatWidth.Text) * 0.001;
                UpdateRecordFact(ref zw);
            }
            tspbZone.Visible = false; 
            tslbOrder.Text = "�������� ������ ";
            LoadSubForm();
            tslbOrder.Text = "������";
            GetTotals((int)cbMobitel.SelectedValue);
        }
        //���������� ������������ � ���� � ���������� ���������� � ���
        private void GetFactZone(ref ZoneWork zw)
        {
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + zw.ID_mobitel.ToString() + " AND time >= #" + zw.dtStart.ToString("MM.dd.yyyy") + "# and time  < #" + zw.dtEnd.ToString("MM.dd.yyyy") + "#", "time");
            if (dataRows.Length == 0) return;
            atlantaDataSet.zonesRow z_row = (atlantaDataSet.zonesRow)_dsAtlanta.zones.FindByZone_ID(zw.ZoneID);
            CheckZone cz_Work = new CheckZone(z_row);
            bool bInZone = false;// ���������� � ����� �� ��������� � ���� � ������� ������� ������
            DateTime dtPrev = new DateTime();
            DateTime dtInZoneLast = new DateTime();
            DateTime dtStart = new DateTime();
            int d_row_start = 0;
            //int d_row_end = 0;
            int d_row_index = 0;
            PartialAlgorithms PA = new PartialAlgorithms();
            foreach (LocalCache.atlantaDataSet.dataviewRow d_row in dataRows)
            {
                if (cz_Work.IncludeCheckPoint(d_row.Lon, d_row.Lat))
                {
                    if (bInZone)
                    {
                        //if (GetFactZoneRotation(d_row.DataGps_ID)) zw.tsEngineOn = zw.tsEngineOn.Add(d_row.time.Subtract(dtPrev));
                        zw.Distance += d_row.dist;
                        dtInZoneLast = d_row.time;
                        if (GetFactZoneRotation(d_row.DataGps_ID)) zw.tsEngineOn = zw.tsEngineOn.Add(d_row.time.Subtract(dtPrev));
                    }
                    else
                    {
                        if (!zw.InZone) // ������ ���� � ����
                        {
                            zw.dtStart = d_row.time;
                            zw.InZone = true;
                        }
                        else // ����������� ���� � ���� ����� ������
                        {
                            zw.tsOut = zw.tsOut.Add(d_row.time.Subtract(zw.dtEnd));
                        }
                        bInZone = true;
                        d_row_start = d_row_index;
                        dtStart = d_row.time;
                    }

                }
                else
                {
                    if (bInZone)
                    {
                        // ����� ����� �� ����
                        if ((d_row.time.Subtract(dtInZoneLast) > tsDelta) || (d_row.time.Subtract(zw.dtEndDay) < tsDelta))
                        {
                            LocalCache.atlantaDataSet.dataviewRow[] dRows = new LocalCache.atlantaDataSet.dataviewRow[d_row_index - d_row_start];
                            Array.Copy(dataRows, d_row_start, dRows, 0, dRows.Length);
                            TimeSpan tsMove = PA.GetTimeMotion(dRows);
                            zw.tsMove = zw.tsMove.Add(tsMove);
                            zw.tsStop = zw.tsStop.Add(d_row.time.Subtract(dtStart).Subtract(tsMove));
                            zw.dtEnd = d_row.time;
                            bInZone = false;
                        }
                    }
                }
                dtPrev = d_row.time;
                d_row_index++;
            }
            if (bInZone)// ��������� �� ������� ���� �� ��������� �����
            {
                LocalCache.atlantaDataSet.dataviewRow[] dRows = new LocalCache.atlantaDataSet.dataviewRow[dataRows.Length - d_row_start];
                Array.Copy(dataRows, d_row_start, dRows, 0, dRows.Length);
                //for (int i = d_row_start; i < dataRows.Length; i++)
                //{
                //    dRows[i - d_row_start] = dataRows[i];
                //}
                TimeSpan tsMove = PA.GetTimeMotion(dRows);
                zw.tsMove = zw.tsMove.Add(tsMove);
                zw.tsStop = zw.tsStop.Add(zw.dtEndDay.Subtract(dtStart).Subtract(tsMove));
                zw.dtEnd = zw.dtEndDay;
                bInZone = false;
            }

            //if (zw.InZone && zw.ID_orderT >0) UpdateRecordFact(ref zw);
        }
#region �������������� ������������ �������
        //������� �� ������� ����������� ����
        private class ItemSqClass
        {
            private double _Lon;
            private double _Lat;
            private bool _Selected;
            private bool _Border;
            private bool _Out;
            // ����������� ��� ����� ������� - ��� �������������� ����������
            private static int _NumberCnt = 0;
            private  int _Number = 0;

            public double Lon
            {
                get { return _Lon; }
                set { _Lon = value; }
            }
            public double Lat
            {
                get { return _Lat; }
                set { _Lat = value; }
            }
            public bool Selected
            {
                get { return _Selected; }
                set { _Selected = value; }
            }
            public bool Border
            {
                get { return _Border; }
                set { 
                    _Border = value;
                    if (_Border)
                    {
                        _NumberCnt++;
                        _Number = _NumberCnt;
                    }  
                }
            }
            public bool Out
            {
                get { return _Out; }
                set { _Out = value; }
            }
            public int Number
            {
                get { return _Number; }
                set { _NumberCnt = value; }
            }
            public ItemSqClass(double iLon, double iLat)
            {
                _Selected = false;
                _Border = false;
                _Out = false;
                _Lon = iLon;
                _Lat = iLat;
            }
        }
        private struct ItemSq
        {
            public double Lon;
            public double Lat;
            public bool Selected;
            public ItemSq(double iLon, double iLat)
            {
                Selected = false;
                Lon = iLon;
                Lat = iLat;
            }
        }
        //����������� ������������ ������� � ����
        private double GetFactItemSqWAgregat(ref ZoneWork zw)
        {
            //return 0;
            tslbOrder.Text = "������ �������  ";
            Application.DoEvents();
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + zw.ID_mobitel.ToString() + " AND time >= #" + zw.dtStart.ToString("MM.dd.yyyy") + "# and time  < #" + zw.dtEnd.ToString("MM.dd.yyyy") + "#", "time");
            if (dataRows.Length == 0) return 0;
            atlantaDataSet.zonesRow z_row = (atlantaDataSet.zonesRow)_dsAtlanta.zones.FindByZone_ID(zw.ZoneID);
            CheckZone cz_Work = new CheckZone(z_row);
            double LonMax = 0;
            double LonMin = 0;
            double LatMax = 0;
            double LatMin = 0;
            List<LocalCache.atlantaDataSet.dataviewRow> d_rows_zone = new List<LocalCache.atlantaDataSet.dataviewRow>();
            bool bFirst = true;
            //GeoData.GDL _gisMap = new GeoData.GDL();
            //����� ���� �����, �������� � ���� + ������ ����� ����� ����� � ����� ����
            atlantaDataSet.dataviewRow d_row_prev = null;
            bool bInZonePrev = false;
            foreach (atlantaDataSet.dataviewRow d_row in dataRows)
            {
                if (cz_Work.IncludeCheckPoint(d_row.Lon, d_row.Lat))
                {
                    //����� ����� �����
                    if (!bInZonePrev && !bFirst)
                    {
                        d_rows_zone.Add(d_row_prev);
                        if (LonMax < d_row_prev.Lon) LonMax = d_row_prev.Lon;
                        if (LonMin > d_row_prev.Lon) LonMin = d_row_prev.Lon;
                        if (LatMax < d_row_prev.Lat) LatMax = d_row_prev.Lat;
                        if (LatMin > d_row_prev.Lat) LatMin = d_row_prev.Lat;
                    }
                    d_rows_zone.Add(d_row);
                    if (bFirst)
                    {
                        LonMax = d_row.Lon;
                        LonMin = d_row.Lon;
                        LatMax = d_row.Lat;
                        LatMin = d_row.Lat;
                        bFirst = false;
                    }
                    else
                    {
                        if (LonMax < d_row.Lon) LonMax = d_row.Lon;
                        if (LonMin > d_row.Lon) LonMin = d_row.Lon;
                        if (LatMax < d_row.Lat) LatMax = d_row.Lat;
                        if (LatMin > d_row.Lat) LatMin = d_row.Lat;
                    }

                        bInZonePrev = true;
                    //Console.WriteLine("Lon {0} Lat {1} GPS {2}", d_row.Lon, d_row.Lat, d_row.DataGps_ID);
                }
                else
                {
                    //����� ����� �� �����
                    if (bInZonePrev)
                    {
                        d_rows_zone.Add(d_row);
                        if (LonMax < d_row.Lon) LonMax = d_row.Lon;
                        if (LonMin > d_row.Lon) LonMin = d_row.Lon;
                        if (LatMax < d_row.Lat) LatMax = d_row.Lat;
                        if (LatMin > d_row.Lat) LatMin = d_row.Lat;
                        bInZonePrev = false;
                    }
                        d_row_prev = d_row;
                }
                
            }
            if (d_rows_zone.Count == 0) return 0;
            //������ ���������������
            double stepLat = 0.0000090;
            double stepLon = 0.000014;
            if (txAgregatWidth.Text == "0")
            {
                dbWidth = 8;
            } 
            else
            {
                dbWidth = Convert.ToDouble(txAgregatWidth.Text)/2;
            }
            stepLat = dbWidth * 0.0000090;
            stepLon = dbWidth * 0.000014;
            //lSq.Clear(); 
            MAX_X = Convert.ToInt32((LonMax - LonMin) / stepLon) + 2;
            MAX_Y = Convert.ToInt32((LatMax - LatMin) / stepLat) + 2;
            LatMax = LatMax + stepLat;
            ItemSqClass[,] arSq = new ItemSqClass[MAX_X, MAX_Y];
            //arSq[0, 0].Number = 0;
            for (int x =0; x < MAX_X; x++)
            {
                for (int y = MAX_Y - 1; y >= 0; y--)
                {
                    ItemSqClass Sq = new ItemSqClass(Math.Round(LonMin + stepLon * x, 7), Math.Round(LatMax - stepLat * y, 7));
                    //lSq.Add(Sq);
                    arSq[x, y] = Sq;
                    //����� �������� ������� ����� ���������
                    if ((x == 0) && (y == 0)) arSq[0, 0].Number = 0;
                }

            }
            //����� ���� �����
            if (d_rows_zone.Count > 1)
            {
                tspbOrder.Visible = true;
                tspbOrder.Value = 0;
                tspbOrder.Maximum = d_rows_zone.Count;
                BASE_X = 0;
                BASE_Y = 0;
                bool bStartTrack = false;
                for (int i = 0; i < d_rows_zone.Count; i++)
                {
                    if (i == 0)
                    {
                        //������ ����� � ������� ���������
                        bStartTrack = PointInSquare(d_rows_zone[0].Lon, d_rows_zone[0].Lat, stepLat, stepLon, ref arSq);
                    }
                    else
                    //PointsBetweenPoints(d_rows_zone[i - 1], d_rows_zone[i], stepLat, stepLon, ref lSq);
                    {
                        if (!bStartTrack) bStartTrack = PointInSquare(d_rows_zone[0].Lon, d_rows_zone[0].Lat, stepLat, stepLon, ref arSq);
                        PointsBetweenPoints(d_rows_zone[i - 1], d_rows_zone[i], stepLat, stepLon, ref arSq);
                    }
                    tspbOrder.Increment(1);
                    Application.DoEvents();
                }
            }
            // �������� ���� ��� �� � ����
            for (int x = 0; x < MAX_X; x++)
            {
                for (int y = 0; y < MAX_Y; y++)
                {
                    if (arSq[x, y].Selected)
                    {
                        if (!(cz_Work.IncludeCheckPoint(arSq[x, y].Lon, arSq[x, y].Lat) || cz_Work.IncludeCheckPoint(arSq[x, y].Lon + stepLon, arSq[x, y].Lat - stepLat)))
                        {
                            arSq[x, y].Selected=false;
                        }
                    }
                }
            }
            //����������� ��������� ����� 
            //����� ������ ��������� ����� � ��������� ������� �������
            BASE_X = 0;
            BASE_Y = 0;
            bool bBorderPresent = false;
            List<System.Drawing.Point> p_out = new List<System.Drawing.Point>();
            for (int x = 0; x < MAX_X; x++)
            {
                if (bBorderPresent) break;
                for (int y = 0; y < MAX_Y; y++)
                {
                    if (!bBorderPresent)
                    {
                        if (arSq[x, y].Selected)
                        {
                            BASE_X = x;
                            BASE_Y = y;
                            X_START = BASE_X;
                            Y_START = BASE_Y;
                            bBorderPresent = true;
                            arSq[x, y].Border = true;
                            System.Drawing.Point p = new System.Drawing.Point(x, y);
                            p_out.Add(p);
                            break;
                        }
                    }
                    else
                        break;
                }
            }
            //���������� �������
            if (!bBorderPresent)
            {
                tspbOrder.Visible = false;
                tslbOrder.Text = "��� ����� � ����!";
                return 0;
            }
            int cnt = 0;
            while (PointNextInBorder_01(ref arSq, ref p_out))
            {
                //arSq[BASE_X, BASE_Y].Out = true;
                Application.DoEvents();
                //cnt++;
                //if (cnt == 4)
                //{
                //    break;
                //}
            }
            //��������� ������� �����
            PRV_X = 0;
            PRV_Y = 0;
            for (int i = 0; i < p_out.Count; i++)
            {
                //������������� ��������� � � �
                if ((p_out[i].X != PRV_X) && (p_out[i].Y != PRV_Y))
                {
                    arSq[p_out[i].X, p_out[i].Y].Out = true;
                }
                else
                //����� � ����
                {
                    if (p_out[i].X < (MAX_X -1))
                    {
                        if (p_out[i].Y < (MAX_Y - 1)) { if (arSq[p_out[i].X + 1, p_out[i].Y].Border && arSq[p_out[i].X, p_out[i].Y + 1].Border) arSq[p_out[i].X, p_out[i].Y].Out = true; }
                        if (p_out[i].Y >0) { if (arSq[p_out[i].X + 1, p_out[i].Y].Border && arSq[p_out[i].X, p_out[i].Y - 1].Border) arSq[p_out[i].X, p_out[i].Y].Out = true; }
                    }
                    if (p_out[i].X > 0)
                    {
                        if (p_out[i].Y < (MAX_Y - 1)) { if (arSq[p_out[i].X - 1, p_out[i].Y].Border && arSq[p_out[i].X, p_out[i].Y + 1].Border) arSq[p_out[i].X, p_out[i].Y].Out = true; }
                        if (p_out[i].Y > 0) { if (arSq[p_out[i].X - 1, p_out[i].Y].Border && arSq[p_out[i].X, p_out[i].Y - 1].Border) arSq[p_out[i].X, p_out[i].Y].Out = true; }
                    }
                }
                PRV_X = p_out[i].X;
                PRV_Y = p_out[i].Y;
                

            }
            //������� ������� + ������ ��������� � �������
            int cntSelected = 0;
            ConnectMySQL mSQL = new ConnectMySQL();
            string sSQLdelete = "DELETE FROM agro_calc WHERE agro_calc.Id_zone = " + zw.ZoneID + " AND agro_calc.Id_order = " + txId.Text;
            mSQL.ExecuteNonQueryCommand(sSQLdelete);
            tspbOrder.Visible = true;
            tspbOrder.Value = 0;
            tspbOrder.Maximum =MAX_X * MAX_Y;// lSq.Count; //arSq.GetUpperBound(0) * arSq.GetUpperBound(1);
            tslbOrder.Text = "������ � ����  ";

            string sSQLinsert = "";
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                sSQLinsert = "INSERT INTO `agro_calc`(DateCalc,Id_mobitel,Id_zone,Id_order,StepLat,StepLon) VALUES ("
               + "'" + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
               + zw.ID_mobitel + ","
               + zw.ZoneID + ","
               + txId.Text + ","
               + stepLat.ToString().Replace( ",", "." ) + ","
               + stepLon.ToString().Replace( ",", "." ) + ")";
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                sSQLinsert = "INSERT INTO agro_calc(DateCalc,Id_mobitel,Id_zone,Id_order,StepLat,StepLon) VALUES ("
               + "'" + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
               + zw.ID_mobitel + ","
               + zw.ZoneID + ","
               + txId.Text + ","
               + stepLat.ToString().Replace( ",", "." ) + ","
               + stepLon.ToString().Replace( ",", "." ) + ")";
            }

           int Id_calc = mSQL.ExecuteReturnLastInsert(sSQLinsert, "agro_calc"); 
            //foreach (ItemSqClass s in lSq)
            //{
            for (int x = 0; x < MAX_X; x++)
            {
                for (int y = 0; y < MAX_Y; y++)
                {
                    if (arSq[x, y].Selected )
                    {
                        //if (cz_Work.IncludeCheckPoint(arSq[x, y].Lon, arSq[x, y].Lat) || cz_Work.IncludeCheckPoint(arSq[x, y].Lon + stepLon, arSq[x, y].Lat - stepLat))
                        //{
                            cntSelected++;
                            if (arSq[x, y].Out)
                            {
                                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                                {
                                    sSQLinsert = "INSERT INTO `agro_calct`(Id_main,Lon,Lat,Selected,Border,BorderNode,Number) VALUES ("
                                   + Id_calc + ","
                                   + arSq[x, y].Lon.ToString().Replace( ",", "." ) + ","
                                   + arSq[x, y].Lat.ToString().Replace( ",", "." ) + ","
                                   + arSq[x, y].Selected + "," + arSq[x, y].Border + "," + arSq[x, y].Out + "," + arSq[x, y].Number + ")";
                                }
                                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                                {
                                    sSQLinsert = "INSERT INTO agro_calct(Id_main,Lon,Lat,Selected,Border,BorderNode,Number) VALUES ("
                                   + Id_calc + ","
                                   + arSq[x, y].Lon.ToString().Replace( ",", "." ) + ","
                                   + arSq[x, y].Lat.ToString().Replace( ",", "." ) + ","
                                   + arSq[x, y].Selected + "," + arSq[x, y].Border + "," + arSq[x, y].Out + "," + arSq[x, y].Number + ")";
                                }
                                mSQL.ExecuteNonQueryCommand(sSQLinsert);
                            }
                        //}
                    }
                    tspbOrder.Increment(1);
                    Application.DoEvents();
                }

            }
            mSQL.CloseMySQLConnection();
            double side1 = �GeoDistance.CalculateFromGrad(arSq[0, 0].Lat, arSq[0, 0].Lon, arSq[0, 0].Lat + stepLat, arSq[0, 0].Lon);
            double side2 = �GeoDistance.CalculateFromGrad(arSq[0, 0].Lat, arSq[0, 0].Lon, arSq[0, 0].Lat, arSq[0, 0].Lon + stepLon);
            double SquareCalc = cntSelected * side1 * side2;
            tspbOrder.Visible = false;
            tslbOrder.Text = "������� " + (100*SquareCalc).ToString()+ " ��" ;
            Application.DoEvents();  
            return SquareCalc;
        }
        //����������� �� ����� ��������
        private bool PointInSquare(double Lon, double Lat, double stepLat, double stepLon, ref ItemSqClass[,] arSq)
        {
            bool InSquare = false;
            for (int x = 0; x < arSq.GetUpperBound(0) ; x++)
            {
                for (int y = 0; y < arSq.GetUpperBound(1); y++)
                {
                    //i++;
                        //Console.WriteLine("s.Lat {0} - stepLat {5} s.Lon {1} + stepLon {6} Lat {2} Lon {3} i {4}", s.Lat, s.Lon, Lat, Lon, i, (s.Lat - stepLat), (s.Lon + stepLon));  
                        if ((Lat <= arSq[x, y].Lat && Lat >= (arSq[x, y].Lat - stepLat)) && (Lon >= arSq[x, y].Lon && Lon <= (arSq[x, y].Lon + stepLon)))
                        {
                            arSq[x, y].Selected = true;
                            BASE_X = x;
                            BASE_Y = y;
                            //lSq.Add(arSq[x, y]);
                            InSquare = true;
                            return InSquare;

                        }

                }
            }

            return InSquare;
        }
        //����������� �� ����� ������ �� ��������� ����� ��������
        private bool PointInSquareFromBase(double Lon, double Lat, double stepLat, double stepLon, ref ItemSqClass[,] arSq)
        {
            int STEP = 2;
            bool InSquare = false;
            int i = 0;
            for (int x = ((BASE_X - STEP) < 0 ? 0 : (BASE_X - STEP)); x <= ((BASE_X + STEP) > arSq.GetUpperBound(0) ? arSq.GetUpperBound(0) : (BASE_X + STEP)); x++)
            {
                for (int y = ((BASE_Y - STEP) < 0 ? 0 : (BASE_Y - STEP)); y <= ((BASE_Y + STEP) > arSq.GetUpperBound(1) ? arSq.GetUpperBound(1) : (BASE_Y + STEP)); y++)
                {
                    //Console.WriteLine("s.Lat {0} - stepLat {5} s.Lon {1} + stepLon {6} Lat {2} Lon {3} i {4}", arSq[x, y].Lat, arSq[x, y].Lon, Lat, Lon, (arSq[x, y].Lat - stepLat), (arSq[x, y].Lon + stepLon));  
                    //i++;
                    //Console.WriteLine("SLat {0} - stepLat {5} SLon {1} + stepLon {6} Lat {2} Lon {3} i {4}", arSq[x, y].Lat, arSq[x, y].Lon, Lat, Lon,i, (arSq[x, y].Lat - stepLat), (arSq[x, y].Lon + stepLon));  
                    //if ((Lat < arSq[x, y].Lat && Lat > (arSq[x, y].Lat - stepLat)) && (Lon > arSq[x, y].Lon && Lon < (arSq[x, y].Lon + stepLon)))
                    //if (y == 1)
                    //{
                    //    break;
                    //}
                    //Console.WriteLine(y.ToString()); 
                    if ((Lat <= arSq[x, y].Lat && Lat >= (arSq[x, y].Lat - stepLat)) && (Lon >= arSq[x, y].Lon && Lon <= (arSq[x, y].Lon + stepLon)))
                        {
                            arSq[x, y].Selected = true;
                            BASE_X = x;
                            BASE_Y = y;
                            //lSq.Add(arSq[x, y]);
                            InSquare = true;
                            return true; 
                        }
                }
            }

            return InSquare;
        }
       //private bool PointInSquare(double Lon, double Lat, double stepLat, double stepLon, ref List<ItemSqClass> lSq)
       // {
       //     bool InSquare = false;
       //     //int i = 0;
       //     //Console.WriteLine("Start");
       //     foreach (ItemSqClass s in lSq)
       //     {
       //         //i++;
       //         if (!s.Selected)
       //         {
       //             //Console.WriteLine("s.Lat {0} - stepLat {5} s.Lon {1} + stepLon {6} Lat {2} Lon {3} i {4}", s.Lat, s.Lon, Lat, Lon, i, (s.Lat - stepLat), (s.Lon + stepLon));  
       //             if ((Lat <= s.Lat && Lat >= (s.Lat - stepLat)) && (Lon >= s.Lon && Lon <= (s.Lon + stepLon)))
       //             {
       //                 s.Selected = true;
       //                 InSquare = true;

       //             }
       //         }
       //     }
       //     return InSquare;
       // }
        //�������� ����� ����� ������� ref ItemSqClass[,] arSq
        private void PointsBetweenPoints(LocalCache.atlantaDataSet.dataviewRow d_row_prev, LocalCache.atlantaDataSet.dataviewRow d_row_cur,double stepLat,double stepLon, ref ItemSqClass[,] arSq)
        {
            //���������� ����� ����� ������� 
            double dist = �GeoDistance.CalculateFromGrad(d_row_prev.Lat, d_row_prev.Lon, d_row_cur.Lat, d_row_cur.Lon);
            //��� - �������� ������ ������������
            int CountStep = (Convert.ToInt32(dist/(dbWidth*0.001))+1) * 2;
            double distStep = dist / CountStep;
            //����������� �� ��� �
            double distLon = d_row_cur.Lon - d_row_prev.Lon;// �GeoDistance.CalculateFromGrad(d_row_prev.Lat, d_row_prev.Lon, d_row_prev.Lat, d_row_cur.Lon);
            //��� �� ��� �
            double stepLonL = distLon / CountStep;
            //����������� �� ��� Y
            double distLat = d_row_cur.Lat - d_row_prev.Lat;// �GeoDistance.CalculateFromGrad(d_row_prev.Lat, d_row_prev.Lon, d_row_prev.Lat, d_row_cur.Lon);
            //��� �� ��� y
            double stepLatL = distLat / CountStep;
            int KF = 1;
                for (int i = 0; i < CountStep; i ++)
                {
                    PointInSquareFromBase(d_row_prev.Lon + i * stepLonL, d_row_prev.Lat + i * stepLatL, stepLat, stepLon, ref arSq);
                    PointInSquareFromBase(d_row_prev.Lon + i * stepLonL + stepLon / KF, d_row_prev.Lat + i * stepLatL, stepLat, stepLon, ref arSq);
                    PointInSquareFromBase(d_row_prev.Lon + i * stepLonL - stepLon / KF, d_row_prev.Lat + i * stepLatL, stepLat, stepLon, ref arSq);
                    PointInSquareFromBase(d_row_prev.Lon + i * stepLonL, d_row_prev.Lat + i * stepLatL - stepLat / KF, stepLat, stepLon, ref arSq);
                    PointInSquareFromBase(d_row_prev.Lon + i * stepLonL, d_row_prev.Lat + i * stepLatL + stepLat / KF, stepLat, stepLon, ref arSq);
                    
                    //PointInSquareFromBase(d_row_prev.Lon + (i - 1) * stepLonL, d_row_prev.Lat + i * stepLatL, stepLat, stepLon, ref arSq);
                    //PointInSquareFromBase(d_row_prev.Lon + (i + 1) * stepLonL, d_row_prev.Lat + i * stepLatL, stepLat, stepLon, ref arSq);
                    //PointInSquareFromBase(d_row_prev.Lon + i * stepLonL, d_row_prev.Lat + (i - 1) * stepLatL, stepLat, stepLon, ref arSq);
                    //PointInSquareFromBase(d_row_prev.Lon + i * stepLonL, d_row_prev.Lat + (i + 1) * stepLatL, stepLat, stepLon, ref arSq);
                    Application.DoEvents();  
                }
                return;
        }
        //�������� �� ����� ���������
        private bool  PointIsBorder(int X,int Y, ref ItemSqClass[,] arSq)
        {
            if (!arSq[X, Y].Selected) return false;

            int iCountNoSelect = 0;
            int STEP = 1;
            for (int x = ((X - STEP) < 0 ? 0 : (X - STEP)); x <= ((X + STEP) > arSq.GetUpperBound(0) ? arSq.GetUpperBound(0) : (X + STEP)); x++)
            {
                for (int y = ((Y - STEP) < 0 ? 0 : (Y - STEP)); y <= ((Y + STEP) > arSq.GetUpperBound(1) ? arSq.GetUpperBound(1) : (Y + STEP)); y++)
                {
                    if (!arSq[x, y].Selected) iCountNoSelect++; 
                }
            }
            if (iCountNoSelect > 1 && iCountNoSelect < 8)
            {
                if (X > 0 && Y > 0)
                {
                    if (arSq[(X == 0) ? 0 : (X - 1), Y].Selected && arSq[(X + 1) > arSq.GetUpperBound(0) ? arSq.GetUpperBound(0) : (X + 1), Y].Selected
                    && arSq[X, (Y == 0) ? 0 : (Y - 1)].Selected && arSq[X, (Y + 1) > arSq.GetUpperBound(1) ? arSq.GetUpperBound(1) : (Y + 1)].Selected)
                        return false;
                }
                return true;
            }
            else
                return false;
            //if (arSq[x, y].Selected)
            //{
            //    if (x <= 0 || x >= MAX_X) return true;
            //    if (y <= 0 || y >= MAX_Y) return true;
            //    if (!arSq[x - 1, y].Selected || !arSq[x + 1, y].Selected 
            //        || !arSq[x, y - 1].Selected || !arSq[x, y + 1].Selected) return true;
            //}
            //return false;
        }
        private bool PointIsBorderNew(int X, int Y, ref ItemSqClass[,] arSq)
        {
            //���� ������ �� ���� ���� � ����� �� 4 ����������� �� ����� ����� �������
            if (!arSq[X, Y].Selected) return false;
            bool bSelect = false;
            for (int x = X +1 ; x < MAX_X ; x++)
            {
                if (arSq[x, Y].Selected)
                {
                    bSelect = true;
                    break;
                }
            }
            if (!bSelect) return true;
            bSelect = false;
            for (int x = X-1; x >= 0; x--)
            {
                if (arSq[x, Y].Selected)
                {
                    bSelect = true;
                    break;
                }
            }
            if (!bSelect) return true;
            bSelect = false;
            for (int y = Y+1; y < MAX_Y; ++y)
            {
                if (arSq[X, y].Selected)
                {
                    bSelect = true;
                    break;
                }
            }
            if (!bSelect) return true;
            bSelect = false;
            for (int y = Y - 1; y >= 0; y--)
            {
                if (arSq[X, y].Selected)
                {
                    bSelect = true;
                    break;
                }
            }
            if (!bSelect) 
                return true;
            else
                return false;
        }
        //private bool PointNextInBorder(ref ItemSqClass[,] arSq,ref List<System.Drawing.Point> p_out)
        //{
        //    int STEP = 1;
        //    for (int x = ((BASE_X - STEP) < 0 ? 0 : (BASE_X - STEP)); x <= ((BASE_X + STEP) > arSq.GetUpperBound(0) ? arSq.GetUpperBound(0) : (BASE_X + STEP)); x++)
        //    {
        //        for (int y = ((BASE_Y - STEP) < 0 ? 0 : (BASE_Y - STEP)); y <= ((BASE_Y + STEP) > arSq.GetUpperBound(1) ? arSq.GetUpperBound(1) : (BASE_Y + STEP)); y++)
        //        {
        //            //if (arSq[x, y].Border)
        //            if (PointIsBorder(x,y,ref arSq))
        //            {
        //                System.Drawing.Point p = new System.Drawing.Point(x, y);
        //                if (!p_out.Contains(p))
        //                {
        //                    p_out.Add(p);
        //                    BASE_X = x;
        //                    BASE_Y = y;
        //                    arSq[x, y].Out = true;
        //                    return true;
        //                }
        //            }
        //        }
        //    }
        //    return false;          
        //}
        private bool PointNextInBorder_01(ref ItemSqClass[,] arSq, ref List<System.Drawing.Point> p_out)
        {
            //����� �� ������� �������
            int x0 = (BASE_X == 0) ?  0 :(BASE_X - 1);
            int y0 = (BASE_Y == 0) ? 0 : (BASE_Y - 1);
            int xmax = (BASE_X == (MAX_X - 1)) ? BASE_X : (BASE_X + 1);
            int ymax = (BASE_Y == (MAX_Y - 1)) ? BASE_Y : (BASE_Y + 1);
            int[] x = { BASE_X, xmax, xmax, xmax, BASE_X, x0, x0, x0 };
            int[] y = { y0, y0, BASE_Y, ymax, ymax, ymax, BASE_Y, y0 };
            //int[] y = { ymax, ymax, BASE_Y, y0, y0, y0, BASE_Y, ymax };
            for (int i = 0; i < 8; i++)
            {
                if ((x[i] == PRV_X) && (y[i] == PRV_Y))
                {
                    I_START = i;
                    break;
                }
            }
            for (int i = I_START; i < 8; i++)
            {
                if (((X_START == x[i]) && (Y_START == y[i])) && p_out.Count >1) return false;
                if (arSq[x[i], y[i]].Selected)
                {
                    System.Drawing.Point p = new System.Drawing.Point(x[i], y[i]);
                    if (!p_out.Contains(p))
                    {
                        p_out.Add(p);
                        I_START = i;// (i - 1);
                        if (i > 0)
                        {
                            PRV_X = x[i - 1];
                            PRV_Y = y[i - 1];
                        }
                        else
                        {
                            PRV_X = x[i];
                            PRV_Y = y[i];
                        }
                        BASE_X = x[i];
                        BASE_Y = y[i];
                        //Console.WriteLine("BASE_X {0},BASE_Y {1},PRV_X {2},PRV_Y {3}", BASE_X, BASE_Y, PRV_X, PRV_Y); 
                        arSq[x[i], y[i]].Border = true;
                        return true;
                    }
                }
            }
            for (int i = 0; i < I_START; i++)
            {
                if (((X_START == x[i]) && (Y_START == y[i])) && p_out.Count > 1) return false;
                if (arSq[x[i], y[i]].Selected)
                {
                    System.Drawing.Point p = new System.Drawing.Point(x[i], y[i]);
                    if (!p_out.Contains(p))
                    {
                        p_out.Add(p);
                        I_START = i;
                        BASE_X = x[i+1];
                        BASE_Y = y[i+1];
                        PRV_X = x[i];
                        PRV_Y = y[i];
                        //Console.WriteLine("BASE_X {0},BASE_Y {1},PRV_X {2},PRV_Y {3}", BASE_X, BASE_Y, PRV_X, PRV_Y);  
                        arSq[x[i], y[i]].Border= true;
                        return true;
                    }
                }
            }
            return false;
        }
#endregion
        //���������� ������ � �����
        private int AddRecord(int ID_Field, int Id_Zone)
        {
            if (txId.Text == "0") SaveChanges();
            if (txId.Text == "0") return 0;
            if (Id_Zone == 0) Id_Zone = GetZone((int)cbField.SelectedValue);
            if (Id_Zone == 0)
            {
                MessageBox.Show("� ���� ��� ������������� ����!", "��������� �����������!");
                return 0;
            }
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            string sSQLinsert = "insert into agro_ordert ( agro_ordert.Id_main, agro_ordert.Id_field,agro_ordert.Id_Zone, agro_ordert.Id_work, agro_ordert.Price)"
             + " VALUES (" + txId.Text
             + "," + ID_Field
             + "," + Id_Zone
             + "," + (cbWorkType.SelectedValue == null ? "0" : cbWorkType.SelectedValue.ToString())
             + "," + txPrice.Text.Replace(",", ".") + ")";
            _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);
            int iId = (int)_cnMySQL.GetScalarValue("SELECT Id FROM agro_ordert WHERE ID = LAST_INSERT_ID()");
            _cnMySQL.CloseMySQLConnection();
            btDelete.Enabled = true;
            return iId;
        }
        //���������� ������ � ����� �� ������� �������
        private int AddRecord(int ID_Field, int Id_Zone, int Mobitel_Id)
        {
            string sSQLinsert = "";
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            if (ID_main_ext == 0) AddExternal(Mobitel_Id);
            sSQLinsert = "insert into agro_ordert ( agro_ordert.Id_main, agro_ordert.Id_field,agro_ordert.Id_Zone)"
                + " VALUES (" + ID_main_ext
                + "," + ID_Field
                + "," + Id_Zone + ")";
            _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);

            int iId = (int)_cnMySQL.GetScalarValue("SELECT Id FROM agro_ordert WHERE ID = LAST_INSERT_ID()");
            _cnMySQL.CloseMySQLConnection();
            return iId;
        }
        //���������� ������ � ������ ����������� ������ � ����
        private void UpdateRecordFact(ref ZoneWork zw)
        {

            ConnectMySQL _cnMySQL = new ConnectMySQL();
             string sSQLupdate = "";
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                sSQLupdate = "UPDATE agro_orderT SET TimeStart = ?TimeStart"
                    + ", TimeEnd = ?TimeEnd, TimeRate = '" + zw.tsEngineOn.ToString()
                    + "', TimeStop = '" + zw.tsStop.ToString()
                    + "', TimeMove = '" + zw.tsMove.ToString()
                    + "', TimeOut = '" + zw.tsOut.ToString()
                    + "', FactTime = '" + zw.dtEnd.Subtract( zw.dtStart ).ToString()
                    + "', Distance = " + Math.Round( zw.Distance, 3 ).ToString().Replace( ",", "." )
                    + ", FactSquare = " + Math.Round( zw.Square, 2 ).ToString().Replace( ",", "." )
                    + ", FactSquareCalc = " + Math.Round( zw.SquareCalc, 2 ).ToString().Replace( ",", "." )
                    + ", Sum = " + Math.Round( zw.Square * zw.Price, 2 ).ToString().Replace( ",", "." )
                    + ", SpeedAvg = " + ( zw.tsMove.TotalSeconds == 0 ? "0" : Math.Round( zw.Distance / ( zw.tsMove.TotalSeconds / 3600 ), 2 ).ToString().Replace( ",", "." ) )
                    + " WHERE (ID = " + zw.ID_orderT + ")";
            }
            else if((DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                sSQLupdate = "UPDATE agro_orderT SET TimeStart = @TimeStart"
                    + ", TimeEnd = @TimeEnd, TimeRate = '" + zw.tsEngineOn.ToString()
                    + "', TimeStop = '" + zw.tsStop.ToString()
                    + "', TimeMove = '" + zw.tsMove.ToString()
                    + "', TimeOut = '" + zw.tsOut.ToString()
                    + "', FactTime = '" + zw.dtEnd.Subtract( zw.dtStart ).ToString()
                    + "', Distance = " + Math.Round( zw.Distance, 3 ).ToString().Replace( ",", "." )
                    + ", FactSquare = " + Math.Round( zw.Square, 2 ).ToString().Replace( ",", "." )
                    + ", FactSquareCalc = " + Math.Round( zw.SquareCalc, 2 ).ToString().Replace( ",", "." )
                    + ", Sum = " + Math.Round( zw.Square * zw.Price, 2 ).ToString().Replace( ",", "." )
                    + ", SpeedAvg = " + ( zw.tsMove.TotalSeconds == 0 ? "0" : Math.Round( zw.Distance / ( zw.tsMove.TotalSeconds / 3600 ), 2 ).ToString().Replace( ",", "." ) )
                    + " WHERE (ID = " + zw.ID_orderT + ")";
            }
            //+ ", Sum = " + Math.Round(zw.Distance * (double)gbvContent.GetRowCellValue(zw.RowHandle, gbvContent.Columns.ColumnByName("Price")), 2).ToString().Replace(",", ".")
            MySqlParameter[] parDate = new MySqlParameter[2];
            parDate[0] = new MySqlParameter();
            parDate[0].ParameterName = driverDb.ParamPrefics + "TimeStart";
            parDate[0].Value = zw.dtStart;
            parDate[0].MySqlDbType = MySqlDbType.DateTime;
            parDate[1] = new MySqlParameter();
            parDate[1].ParameterName = driverDb.Paramprafics + "TimeEnd";
            parDate[1].Value = zw.dtEnd;
            parDate[1].MySqlDbType = MySqlDbType.DateTime;
            _cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
            _cnMySQL.CloseMySQLConnection();
        }
        //����� ����� �� ������� ���� ������������ �������� � ������� ������� ������
        public int GetFieldsAuto(int Mobitel_Id, bool bLocal)
        {
            int FieldsCount = 0;
            if (bLocal) tslbOrder.Text = "���������� ������";
            else ID_main_ext = 0;
            if (bLocal)
            {
                int iRecords = Create_dsAtlanta(Mobitel_Id);
                if (iRecords == 0)
                {
                    MessageBox.Show("��� ������ � ��������!", cbMobitel.Text);
                    return 0;
                }
            }
            //int kdv = _dsAtlanta.dataview.Rows.Count;   
            RunRotation(Mobitel_Id);
            //�������� �� � ������ 
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            string SQLselect = "SELECT  DISTINCT zones.Zone_ID, agro_field.Id,zones.Name FROM agro_field "
            + " INNER JOIN zones ON agro_field.Id_zone = zones.Zone_ID ";
            //Console.WriteLine(SQLselect);
            MySqlDataReader rdZones = _cnMySQL.GetDataReader(SQLselect);
            while (rdZones.Read())
            {
                if (bLocal) tslbOrder.Text = "������ ���� " + rdZones.GetString(rdZones.GetOrdinal("Name"));
                Application.DoEvents();
                int Field_Id = 0;
                ZoneWork zw = new ZoneWork(rdZones.GetInt32(rdZones.GetOrdinal("Zone_ID")), 0, dtDateInit.Value, true, Convert.ToDouble(txPrice.Text), Mobitel_Id);
                if (!rdZones.IsDBNull(rdZones.GetOrdinal("Id")))
                    Field_Id = rdZones.GetInt32(rdZones.GetOrdinal("Id"));

                //�������� �� ������� ���� � ����� ��� ������� ������ ������
                if (!bLocal || (GridControl.InvalidRowHandle == gbvContent.LocateByValue(0, gbvContent.Columns.ColumnByName("FieldId"), Field_Id)))
                {
                    int k = rdZones.GetInt32(rdZones.GetOrdinal("Zone_ID"));
                    GetFactZone(ref zw);
                    if (zw.InZone)
                    {
                        FieldsCount++;
                        if (bLocal)
                            zw.ID_orderT = AddRecord(Field_Id, rdZones.GetInt32(rdZones.GetOrdinal("Zone_ID")));
                        else
                            zw.ID_orderT = AddRecord(Field_Id, rdZones.GetInt32(rdZones.GetOrdinal("Zone_ID")), Mobitel_Id);
                        if (zw.ID_orderT == 0) return 0;
                        double dbWidth = 0;
                        if (bLocal) dbWidth = Convert.ToDouble(txAgregatWidth.Text) * 0.001;
                        zw.Square = zw.Distance * dbWidth * 0.001 * 100;
                        if (bLocal)  zw.SquareCalc = 100 * GetFactItemSqWAgregat(ref zw);
                        UpdateRecordFact(ref zw);
                    }
                }

            }
            if (bLocal)
            {
                tslbOrder.Text = "�������� ������";
                LoadSubForm();
                tslbOrder.Text = "������";
                GetTotals(Mobitel_Id);
            }
            return FieldsCount;

        }
        //�������� ������ � ������
        public bool DeleteRecordContent(int iID)
        {
            try
            {
                string sSQLdel = "DELETE FROM agro_ordert WHERE ID = " + iID.ToString();
                if (TestLinks())
                {
                    ConnectMySQL _cnMySQL = new ConnectMySQL();
                    _cnMySQL.ExecuteNonQueryCommand(sSQLdel);
                    _cnMySQL.CloseMySQLConnection();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //���� �������
        private void DeleteItem(object sender, System.EventArgs e)
        {
            //DevExpress.Utils.Menu.DXMenuItem Item = (DevExpress.Utils.Menu.DXMenuItem)sender;
            //DevExpress.XtraGrid.Menu.GridViewMenu menu = (DevExpress.XtraGrid.Menu.GridViewMenu)Item.Tag;
            //MessageBox.Show(menu.View.FocusedColumn.Caption);
            //MessageBox.Show("Delete");
            if (DialogResult.Yes == MessageBox.Show("������������� �������� ������?", "��������", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                if (DeleteRecordContent((int)gbvContent.GetRowCellValue(gbvContent.FocusedRowHandle, "Id")))
                    gbvContent.DeleteRow(gbvContent.FocusedRowHandle);
            }
        }
        //���� �� ������ ������
        private void gbvContent_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu)e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem("�������", new EventHandler(DeleteItem));
            menuItem.Tag = e.Menu;
            gvMenu.Items.Add(menuItem);
        }
        //�������� �������� ���������
        private void RunRotation(int Id_mobitel)
        {
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(Id_mobitel);
            BaseReports.Procedure.IAlgorithm brRot = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Rotation();
            brRot.SelectItem(m_row);
            brRot.Run();
        }
        //����������� ��������� ��������� ��� ����� GPS
        private bool GetFactZoneRotation(int DataGps_ID)
        {
            atlantaDataSet.RotateReportRow[] rt_rows = (atlantaDataSet.RotateReportRow[])_dsAtlanta.RotateReport.Select("InitialPointId <= " + DataGps_ID + " AND FinalPointId > =" + DataGps_ID);
            if (rt_rows.Length == 0) return false;
            switch (rt_rows[0].State)
            {
                case "��������":
                    {
                        return true;
                        break;
                    }
                case "�� ��������":
                    {
                        return false;
                        break;
                    }
                default:
                    return false;
                    break;

            }
        }
        //�������������� �������� � �����
        private void gbvContent_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //int iID = 0;
            //if (Int32.TryParse(gbvContent.GetRowCellValue(e.RowHandle, gbvContent.Columns.ColumnByName("Id")).ToString(), out iID))
            //{
            //    if (iID == 0) return;
            //    if (e.Column.FieldName.ToString() == "Work")
            //    {
            //        int iWork = 0;
            //        double dbPrice = 0;
            //        double dbSum = 0;
            //        double dbSquare = 0;
            //        Int32.TryParse(e.Value.ToString(), out iWork);
            //        Double.TryParse(gbvContent.GetRowCellValue(e.RowHandle, gbvContent.Columns.ColumnByName("FactSquare")).ToString(), out dbSquare);
            //        if (iWork > 0) dbPrice = GetPrice((int)e.Value);
            //        dbSum = Math.Round(dbPrice * dbSquare, 2);
            //        gbvContent.SetRowCellValue(e.RowHandle, gbvContent.Columns.ColumnByName("Price"), dbPrice);
            //        gbvContent.SetRowCellValue(e.RowHandle, gbvContent.Columns.ColumnByName("Sum"), dbSum);
            //        ConnectMySQL _cnMySQL = new ConnectMySQL();
            //        string sSQLupdate = "UPDATE agro_ordert SET Price = " + dbPrice.ToString().Replace(",", ".")
            //        + " , agro_ordert.Id_work = " + iWork
            //        + " , agro_ordert.Sum = " + dbSum.ToString().Replace(",", ".")
            //        + " WHERE (ID = " + iID + ")";
            //        _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
            //        _cnMySQL.CloseMySQLConnection();
            //        //
            //    }
            //    else
            //        if (e.Column.FieldName.ToString() == "Comment")
            //        {
            //            ConnectMySQL _cnMySQL = new ConnectMySQL();
            //            string sSQLupdate = "UPDATE agro_ordert SET Comment = '" + e.Value
            //            + "' WHERE (ID = " + iID + ")";
            //            _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
            //            _cnMySQL.CloseMySQLConnection();
            //        }
            //}

        }
        private void gbvContent_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                DoShowMenu(gbvContent.CalcHitInfo(new System.Drawing.Point(e.X, e.Y)));
        }
        //����������� ����
        private void DoShowMenu(DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi)
        {
            //DevExpress.XtraGrid.Menu.GridViewMenu menu = null;
            if (hi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = new DevExpress.XtraGrid.Menu.GridViewMenu(gbvContent);
                DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem("�������", new EventHandler(DeleteItem));
                gvMenu.Init(hi);
                gvMenu.Items.Add(menuItem);
                gvMenu.Show(hi.HitPoint);
            }
        }
        // �������� ������������ ������� � �������� ��� ����� ������������
        private void RecalcSquare(double dbWidthNew)
        {
            if (gbvContent == null || gbvContent.RowCount == 0) return;
            if (DialogResult.Yes == MessageBox.Show("������������� �������� ����������� ������?", "����� ��������� ������������", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {
                ConnectMySQL _cnMySQL = new ConnectMySQL();
                ConnectMySQL _cnMySQLupdate = new ConnectMySQL();
                string sSQLselect = "SELECT  agro_ordert.Id, agro_ordert.Id_work, agro_ordert.Id_work ,agro_ordert.Distance"
                + " FROM    agro_ordert "
                + " WHERE   agro_ordert.Id_main = " + txId.Text;
                MySqlDataReader rd = _cnMySQL.GetDataReader(sSQLselect);
                while (rd.Read())
                {
                    double dbSquareNew = rd.GetDouble(rd.GetOrdinal("Distance")) * dbWidthNew * 0.001 * 100;
                    double dpPrice = 0;
                    if (rd.GetValue(rd.GetOrdinal("Id_work")) != DBNull.Value)
                    {
                        dpPrice = GetPrice(rd.GetInt32(rd.GetOrdinal("Id_work")));
                    }
                    string sSQLupdate = "UPDATE agro_orderT SET FactSquare = " + Math.Round(dbSquareNew,2).ToString().Replace(",", ".")
                        + ", Price = " + dpPrice.ToString().Replace(",", ".")
                        + ", Sum = " + Math.Round(dbSquareNew * dpPrice, 2).ToString().Replace(",", ".")
                        + " WHERE (ID = " + rd.GetInt32(rd.GetOrdinal("Id")) + ")";

                    _cnMySQLupdate.ExecuteNonQueryCommand(sSQLupdate);
                }
                rd.Close();
                _cnMySQL.CloseMySQLConnection();
                _cnMySQLupdate.CloseMySQLConnection();
                LoadSubForm(); 
            }
        }
        // �������������� ���� ����� � ����������
        private void gvContentPrice_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int iID = 0;
            //Console.WriteLine(e.Value.ToString()); 
            GridView gv = (GridView)gcContent.FocusedView;
            //Console.WriteLine(gcContent.FocusedView.Name);
            if (Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["IdP"]).ToString(), out iID))
            {
                //e.RowHandle
                if (iID == 0) return;
                if (e.Column.FieldName.ToString() == "Work")
                {
                    int iWork = 0;
                    double dbPrice = 0;
                    double dbSum = 0;
                    double dbSquare = 0;
                    Int32.TryParse(e.Value.ToString(), out iWork);
                    Double.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("FSP")).ToString(), out dbSquare);
                    if (iWork > 0) dbPrice = GetPrice((int)e.Value);
                    dbSum = Math.Round(dbPrice * dbSquare, 2);
                    gv.SetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("Price"), dbPrice);
                    gv.SetRowCellValue(e.RowHandle, gv.Columns.ColumnByName("Sum"), dbSum);
                    ConnectMySQL _cnMySQL = new ConnectMySQL();
                    string sSQLupdate = "UPDATE agro_ordert SET Price = " + dbPrice.ToString().Replace(",", ".")
                    + " , agro_ordert.Id_work = " + iWork
                    + " , agro_ordert.Sum = " + dbSum.ToString().Replace(",", ".")
                    + " WHERE (ID = " + iID + ")";
                    _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                    _cnMySQL.CloseMySQLConnection();
                    //
                }
                else
                    if (e.Column.FieldName.ToString() == "Comment")
                    {
                        ConnectMySQL _cnMySQL = new ConnectMySQL();
                        string sSQLupdate = "UPDATE agro_ordert SET Comment = '" + e.Value
                        + "' WHERE (ID = " + iID + ")";
                        _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                        _cnMySQL.CloseMySQLConnection();
                    }
            }
        }
#endregion
        #region ������ �������
        #region ������ ����������
        //�������� ��������� �������
        private void btRecalcFuel_Click(object sender, EventArgs e)
        {

            GetFactFuel((int)cbMobitel.SelectedValue, true);
            //GetFactFuelDUTDRT();
            //GetZonesFromCZDetailed();
        }
        #endregion
        //�������� �������� ������� �������
        public void LoadSubFormFuel()
        {
            //      
            //���� ����������� ��� � ����������� ������� �������
            ConnectMySQL cnMySQL = new ConnectMySQL();
            //string sSQLselect = "SELECT  agro_order.Id as ZoneID,agro_order.`Comment` as ZoneName, agro_order.FuelStart, agro_order.FuelAdd, agro_order.FuelSub, agro_order.FuelEnd, agro_order.FuelExpens, agro_order.FuelExpensSq "
            //+ " FROM   agro_order WHERE   agro_order.Id = 0" ;
            //
            string sSQLselect = "SELECT   agro_ordertf.Id as IdF, agro_ordertf.Id_zone as ZoneID, zones.Name as ZoneName, agro_ordertf.FuelAdd,"
            + " agro_ordertf.FuelStart, agro_ordertf.FuelSub, agro_ordertf.FuelEnd, agro_ordertf.FuelExpens,"
            + " agro_ordertf.FuelExpensAvg, agro_ordertf.FuelExpensAvgRate, agro_ordertf.Fuel_ExpensMove,"
            + " agro_ordertf.Fuel_ExpensStop, agro_ordertf.Fuel_ExpensTotal, agro_ordertf.Fuel_ExpensAvg,"
            + " agro_ordertf.Fuel_ExpensAvgRate, "
            + " agro_ordertf.Fuel_ExpensMove, agro_ordertf.Fuel_ExpensStop, agro_ordertf.Fuel_ExpensTotal,"
            + " agro_ordertf.Fuel_ExpensAvg, agro_ordertf.Fuel_ExpensAvgRate,"
            + " agro_ordertf.FactSquare, agro_ordertf.TimeRate,agro_ordertf.TimeStart,agro_ordertf.TimeEnd"
            + " FROM  agro_ordertf"
            + " LEFT OUTER JOIN zones ON agro_ordertf.Id_zone = zones.Zone_ID"
            + " WHERE  agro_ordertf.Level = 1 AND agro_ordertf.Id_main = " + txId.Text;
            DataSet dsFuel = cnMySQL.GetDataSet(sSQLselect, "agro_ordertf");
            sSQLselect = " SELECT   agro_ordertf_D.Id, agro_ordertf_D.Id_node, agro_ordertf_D.TimeStart, agro_ordertf_D.TimeEnd,"
            + " agro_ordertf_D.TimeRate, agro_ordertf_D.FactSquare, agro_ordertf_D.FuelStart, "
            + " agro_ordertf_D.FuelAdd, agro_ordertf_D.FuelSub, agro_ordertf_D.FuelEnd, "
            + " agro_ordertf_D.FuelExpens, agro_ordertf_D.Fuel_ExpensMove, agro_ordertf_D.Fuel_ExpensStop, "
            + " agro_ordertf_D.Fuel_ExpensTotal"
            + " FROM   agro_ordertf as agro_ordertf_D"
            + " WHERE  agro_ordertf_D.Level = 2 AND agro_ordertf_D.Id_main = " + txId.Text; ;
            cnMySQL.FillDataSet(sSQLselect, dsFuel, "agro_ordertf_D");
            DataColumn keyColumn = dsFuel.Tables["agro_ordertf"].Columns["IdF"];
            DataColumn foreignKeyColumn = dsFuel.Tables["agro_ordertf_D"].Columns["Id_node"];
            dsFuel.Relations.Add("��������� ����", keyColumn, foreignKeyColumn);
            gcFuel.DataSource = dsFuel.Tables["agro_ordertf"];
            gcFuel.ForceInitialize();
            gcFuel.LevelTree.Nodes.Add("��������� ����", gvFuelDetal);
            cnMySQL.CloseMySQLConnection();
            //gbvFuel.Appearance.Row.BackColor = SystemColors.ControlDark;
            Algorithm.bDisableAction = false;
        }
        // ����� � ����������� � ����� ����������� ���, � ������� ������������ �������� ���� � ������� ����� 
        public int GetFactFuel(int Mobitel_Id, bool bLocal)
        {
            Algorithm.bDisableAction = true;
            int iZones = 0;// ���������� ��� � ������� ���� ������ ��������
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            if (bLocal)
            {
                Create_dsAtlanta(Mobitel_Id);
                if (txId.Text == "0") SaveChanges();
                if (txId.Text == "0") return 0;
                //�������� ���� ��
                tslbOrder.Text = "������� ����������� ������� �������";

                string SQLdelete = "DELETE FROM   agro_ordertf WHERE agro_ordertf.Id_main = " + txId.Text;
                _cnMySQL.ExecuteNonQueryCommand(SQLdelete);
                Application.DoEvents();
                gbvFuel.RefreshData();
                tslbOrder.Text = "���������� ������ ����������";
                Application.DoEvents();
            }
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(Mobitel_Id);
            BaseReports.Procedure.IAlgorithm brKlm = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Kilometrage();
            brKlm.SelectItem(m_row);
            brKlm.Run();
            BaseReports.Procedure.IAlgorithm brFuel1 = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Fuel(AlgorithmType.FUEL1);
            brFuel1.SelectItem(m_row);
            brFuel1.Run();
            BaseReports.Procedure.IAlgorithm brFlowMeter = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.FlowMeter(AlgorithmType.FUEL2);
            brFlowMeter.SelectItem(m_row);
            brFlowMeter.Run();
            RunRotation(Mobitel_Id);
            //LoadSubFormFuel(); 
            tslbOrder.Text = "������� ����������� ���";
            Application.DoEvents();
            string SQLselect = "SELECT   zones.Zone_ID, zones.Name, zones.Square FROM   zones";
            //Console.WriteLine(SQLselect);  
            MySqlDataReader rdZones = _cnMySQL.GetDataReader(SQLselect);
            //������ ������������
            if (bLocal) dbWidth = Convert.ToDouble(txAgregatWidth.Text) * 0.001;
            while (rdZones.Read())
            {
                tslbOrder.Text = "������ ���� " + rdZones.GetString(rdZones.GetOrdinal("Name"));
                Application.DoEvents();
                ZoneWork zw = new ZoneWork(rdZones.GetInt32(rdZones.GetOrdinal("Zone_ID")), 0, dtDateInit.Value, true, 0, Mobitel_Id);
                GetTimesZoneFuel(ref zw);
                if (zw.InZone)
                {
                    iZones++;
                    if (!bLocal) AddExternal(Mobitel_Id);
                    List<ZoneWork> zw_tmp_list = new List<ZoneWork>();
                    foreach (ZoneWork zw_cycle in zw.list_zw)
                    {
                        ZoneWork zw_tmp = zw_cycle;
                        SetFuelFactRowDUT(m_row, ref zw_tmp);
                        SetFuelFactRowDRT(m_row, ref  zw_tmp);
                        zw.FuelAdd += zw_tmp.FuelAdd;
                        zw.FuelSub += zw_tmp.FuelSub;
                        zw.FuelExpens += zw_tmp.FuelExpens;
                        // ������ ������� � ��������, �
                        zw.Fuel_ExpensMove += zw_tmp.Fuel_ExpensMove;
                        // ������ ������� �� ��������, �
                        zw.Fuel_ExpensStop += zw_tmp.Fuel_ExpensStop;
                        // ����� ������ ������� � ����, �
                        zw.Fuel_ExpensTotal += zw_tmp.Fuel_ExpensTotal;
                        //Console.WriteLine("������ {0} ����� {1} ���� {2} �������� {3}", zw_cycle.dtStart, zw_cycle.dtEnd, zw_cycle.Distance, zw_cycle.tsEngineOn);
                        zw_tmp_list.Add(zw_tmp);
                    }
                    zw.FuelStart = zw_tmp_list[0].FuelStart;
                    zw.FuelEnd = zw_tmp_list[zw_tmp_list.Count - 1].FuelEnd;
                    Double dbHMoto = zw.tsEngineOn.TotalHours;
                    if (dbHMoto > 0)
                    {
                        zw.FuelExpensAvgRate = Math.Round(zw.FuelExpens / dbHMoto, 2);
                        zw.Fuel_ExpensAvgRate = Math.Round(zw.Fuel_ExpensTotal / dbHMoto, 2);
                    }
                    zw.Square = zw.Distance * 0.001 * dbWidth * 100;//(��) 
                    if (zw.Square > 0)
                    {
                        zw.FuelExpensAvg = Math.Round(zw.FuelExpens / zw.Square, 2);
                        zw.Fuel_ExpensAvg = Math.Round(zw.Fuel_ExpensTotal / zw.Square, 2);
                    }
                    int Id = AddZone(ref zw, 1, 0);
                    foreach (ZoneWork zw_cycle in zw_tmp_list)
                    {
                        ZoneWork zw_tmp = zw_cycle;
                        AddZone(ref zw_tmp, 2, Id);
                    }
                }
            }
            tslbOrder.Text = "������� �������� ��������";
            Application.DoEvents();
            ZoneWork zw_tot = new ZoneWork((int)WorkIDzone.�����_�����, 0, dtDateInit.Value, false, 0, Mobitel_Id);
            SetFuelFactRowDUT(m_row, ref zw_tot);
            SetFuelFactRowDRT(m_row, ref zw_tot);
            if (!bLocal && (ID_main_ext == 0)) AddExternal(Mobitel_Id);
            AddZone(ref zw_tot, 1, 0);
            Application.DoEvents();
            if (bLocal)
            {
                LoadSubFormFuel();
                tslbOrder.Text = "������";
                GetTotals(Mobitel_Id);
            }
            return iZones;
            //MessageBox.Show("Finish!"); 
        }
        //���������� ������������ � ���� � ���������� ������� ������� � ���
        private void GetTimesZoneFuel(ref ZoneWork zw)
        {
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(zw.ID_mobitel);
            BaseReports.Procedure.IAlgorithm brRot = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Rotation();
            brRot.SelectItem(m_row);
            brRot.Run();
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + zw.ID_mobitel.ToString() + " AND time >= #" + zw.dtStart.ToString("MM.dd.yyyy") + "# and time  < #" + zw.dtEnd.ToString("MM.dd.yyyy") + "#", "time");
            atlantaDataSet.zonesRow z_row = (atlantaDataSet.zonesRow)_dsAtlanta.zones.FindByZone_ID(zw.ZoneID);
            CheckZone cz_Work = new CheckZone(z_row);
            bool bInZone = false;// ���������� � ����� �� ��������� � ���� � ������� ������� ������
            bool bOutZone = false;
            DateTime dtPrev = new DateTime();
            DateTime dtInZoneLast = new DateTime();
            DateTime dtOutZoneFirst = new DateTime();
            ZoneWork zw_loc = new ZoneWork(); ;
            //List<ZoneWork> list_zw = new List<ZoneWork>();
            foreach (LocalCache.atlantaDataSet.dataviewRow d_row in dataRows)
            {
                if (cz_Work.IncludeCheckPoint(d_row.Lon, d_row.Lat))
                {
                    //Console.WriteLine("ID {0} ���� {1} ����� {2} YES", d_row.DataGps_ID, d_row.dist, d_row.time);

                    if (bInZone)
                    {
                        zw_loc.Distance += d_row.dist;
                        zw.Distance += d_row.dist;
                        dtInZoneLast = d_row.time;
                        bOutZone = false;
                        if (GetFactZoneRotation(d_row.DataGps_ID))
                        {
                            zw_loc.tsEngineOn = zw_loc.tsEngineOn.Add(d_row.time.Subtract(dtPrev));
                            zw.tsEngineOn = zw_loc.tsEngineOn.Add(d_row.time.Subtract(dtPrev));
                        }
                    }
                    else
                    {
                        if (!bInZone) // ������ ���� � ����
                        {
                            zw_loc = new ZoneWork();
                            zw_loc.dtStart = d_row.time;
                            zw_loc.ID_mobitel = (int)zw.ID_mobitel;
                            zw.InZone = true;
                            zw.dtStart = d_row.time;
                            dtInZoneLast = d_row.time;
                        }
                        bInZone = true;
                    }

                }
                else
                {
                    //Console.WriteLine("ID {0} ���� {1} ����� {2} NO", d_row.DataGps_ID, d_row.dist, d_row.time);
                    if (bInZone)
                    {
                        // ����� ����� �� ����
                        if (d_row.time.Subtract(dtInZoneLast) > tsDelta)
                        {
                            zw_loc.dtEnd = dtOutZoneFirst;
                            zw.dtEnd = dtOutZoneFirst;
                            zw.list_zw.Add(zw_loc);
                            bInZone = false;
                        }
                        else
                            if (!bOutZone)
                            {
                                dtOutZoneFirst = d_row.time;
                                bOutZone = true;
                            }
                    }
                }
                dtPrev = d_row.time;
            }
            if (bInZone)// ��������� �� ������� ���� �� ��������� �����
            {
                zw_loc.dtEnd = zw.dtEndDay;
                zw.list_zw.Add(zw_loc);
                bInZone = false;
            }
            //if (zw.InZone)
            //{
            //    foreach (ZoneWork zw_cycle in zw.list_zw)
            //    {
            //        Console.WriteLine("������ {0} ����� {1} ���� {2} �������� {3}", zw_cycle.dtStart, zw_cycle.dtEnd, zw_cycle.Distance, zw_cycle.tsEngineOn);
            //    }
            //}

        }
        //����������� ������ ������� ������� �� ������� ������ ������� (���)
        private void SetFuelFactRowDUT(LocalCache.atlantaDataSet.mobitelsRow m_row, ref ZoneWork zw)
        {

            PartialAlgorithms PA = new PartialAlgorithms();
            double dbFuelStart = 0;
            double dbFuelEnd = 0;
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = PartialAlgorithms.GetDataViewRows(m_row, zw.dtStart, zw.dtEnd);
            if (dataRows.Length > 0)
            {
                LocalCache.atlantaDataSet.FuelValueRow[] fuel_val_rows =
                  (LocalCache.atlantaDataSet.FuelValueRow[])dataRows[0].GetChildRows("dataview_FuelValue");
                if (fuel_val_rows.Length > 0)
                { dbFuelStart = fuel_val_rows[0].Average; }
                fuel_val_rows = (LocalCache.atlantaDataSet.FuelValueRow[])dataRows[dataRows.Length - 1].GetChildRows("dataview_FuelValue");
                if (fuel_val_rows.Length > 0)
                {
                    dbFuelEnd = fuel_val_rows[0].Average;
                }
            }
            LocalCache.atlantaDataSet.FuelReportRow[] FRRows = PA.GetFuelReportRow(dataRows);
            double dbZapr = 0;
            double dbSliv = 0;
            //������� � ������, �
            zw.FuelStart = Math.Round(dbFuelStart, 2);
            //������� � �����, �
            zw.FuelEnd = Math.Round(dbFuelEnd, 2);
            if (FRRows.Length > 0)
            {
                foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                {
                    if (tmp_FR_row.dValue > 0)
                        dbZapr += tmp_FR_row.dValue;
                    else
                        dbSliv += tmp_FR_row.dValue;
                }
            }
            //����������, �
            zw.FuelAdd = Math.Round(dbZapr, 2);
            //�����, �
            zw.FuelSub = Math.Round(-dbSliv, 2);
            //����� ������, �
            zw.FuelExpens = Math.Round((dbFuelStart + dbZapr - dbFuelEnd), 2);
            //����������� �������, (��)
            zw.Square = zw.Distance * dbWidth * 0.001 * 100;
            // ������ ������� �� ������, �
            if (zw.Square > 0)
            {
                zw.FuelExpensAvg = Math.Round(zw.FuelExpens / zw.Square, 2);
            }
            // ������ ������� � �������, �
            Double dbHMoto = zw.tsEngineOn.TotalHours;
            if (dbHMoto > 0)
            {
                zw.FuelExpensAvgRate = Math.Round(zw.FuelExpens / dbHMoto, 2);
            }

        }
        //����������� ������ ������� ������� �� ������� ������� ������� (���)
        private void SetFuelFactRowDRT(LocalCache.atlantaDataSet.mobitelsRow m_row, ref ZoneWork zw)
        {
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = PartialAlgorithms.GetDataViewRows(m_row, zw.dtStart, zw.dtEnd);
            PartialAlgorithms PA = new PartialAlgorithms(m_row);
            if (dataRows.Length > 0)
            {
                // ������ ������� � ��������, �
                zw.Fuel_ExpensMove = Math.Round(PA.GetFuelUseInMotionDrt(dataRows, 0), 2);
                // ������ ������� �� ��������, �
                zw.Fuel_ExpensStop = Math.Round(PA.GetFuelUseInStopDrt(dataRows, 0, 0), 2);
                // ����� ������ ������� � ����, �
                zw.Fuel_ExpensTotal = Math.Round(zw.Fuel_ExpensMove + zw.Fuel_ExpensStop, 2);
                // ������ ������� �� ������, �
                if (zw.Square > 0)
                {
                    zw.Fuel_ExpensAvg = Math.Round(zw.Fuel_ExpensTotal / zw.Square, 2);
                }
                // ������ ������� � �������, �
                Double dbHMoto = zw.tsEngineOn.TotalHours;
                if (dbHMoto > 0)
                {
                    zw.Fuel_ExpensAvgRate = Math.Round(zw.Fuel_ExpensTotal / dbHMoto, 2);
                }

            }
        }
        //������ ������ � ���� 
        private int AddZone(ref ZoneWork zw, int iLevel, int Id_node)
        {
            int ID_main = 0;
            if (ID_main_ext > 0) ID_main = ID_main_ext;
            else ID_main = Convert.ToInt32(txId.Text);
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            string sSQLinsert = "";
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                sSQLinsert = "insert into agro_ordertf (agro_ordertf.Id_main, agro_ordertf.Id_zone, agro_ordertf.TimeStart,"
             + " agro_ordertf.TimeEnd, agro_ordertf.TimeRate, agro_ordertf.FactSquare,"
             + " agro_ordertf.FuelStart, agro_ordertf.FuelAdd, agro_ordertf.FuelSub, agro_ordertf.FuelEnd,"
             + " agro_ordertf.FuelExpens, agro_ordertf.FuelExpensAvg, agro_ordertf.FuelExpensAvgRate,"
             + " agro_ordertf.Fuel_ExpensMove, agro_ordertf.Fuel_ExpensStop, agro_ordertf.Fuel_ExpensTotal,"
             + " agro_ordertf.Fuel_ExpensAvg, agro_ordertf.Fuel_ExpensAvgRate, agro_ordertf.Level, agro_ordertf.Id_node )"
                 + " VALUES (" + ID_main.ToString()
                 + "," + zw.ZoneID
                 + ",?TimeStart"
                 + ",?TimeEnd"
                 + ",'" + zw.tsEngineOn
                 + "'," + zw.Square.ToString().Replace( ",", "." )
                 + "," + zw.FuelStart.ToString().Replace( ",", "." )
                 + "," + zw.FuelAdd.ToString().Replace( ",", "." )
                 + "," + zw.FuelSub.ToString().Replace( ",", "." )
                 + "," + zw.FuelEnd.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpens.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpensAvg.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpensAvgRate.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensMove.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensStop.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensTotal.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensAvg.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensAvgRate.ToString().Replace( ",", "." )
                 + "," + iLevel
                 + "," + Id_node
                 + ")";
            }
            else if((DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                sSQLinsert = "insert into agro_ordertf (agro_ordertf.Id_main, agro_ordertf.Id_zone, agro_ordertf.TimeStart,"
             + " agro_ordertf.TimeEnd, agro_ordertf.TimeRate, agro_ordertf.FactSquare,"
             + " agro_ordertf.FuelStart, agro_ordertf.FuelAdd, agro_ordertf.FuelSub, agro_ordertf.FuelEnd,"
             + " agro_ordertf.FuelExpens, agro_ordertf.FuelExpensAvg, agro_ordertf.FuelExpensAvgRate,"
             + " agro_ordertf.Fuel_ExpensMove, agro_ordertf.Fuel_ExpensStop, agro_ordertf.Fuel_ExpensTotal,"
             + " agro_ordertf.Fuel_ExpensAvg, agro_ordertf.Fuel_ExpensAvgRate, agro_ordertf.Level, agro_ordertf.Id_node )"
                 + " VALUES (" + ID_main.ToString()
                 + "," + zw.ZoneID
                 + ",@TimeStart"
                 + ",@TimeEnd"
                 + ",'" + zw.tsEngineOn
                 + "'," + zw.Square.ToString().Replace( ",", "." )
                 + "," + zw.FuelStart.ToString().Replace( ",", "." )
                 + "," + zw.FuelAdd.ToString().Replace( ",", "." )
                 + "," + zw.FuelSub.ToString().Replace( ",", "." )
                 + "," + zw.FuelEnd.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpens.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpensAvg.ToString().Replace( ",", "." )
                 + "," + zw.FuelExpensAvgRate.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensMove.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensStop.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensTotal.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensAvg.ToString().Replace( ",", "." )
                 + "," + zw.Fuel_ExpensAvgRate.ToString().Replace( ",", "." )
                 + "," + iLevel
                 + "," + Id_node
                 + ")";
            }
            MySqlParameter[] parDate = new MySqlParameter[2];
            parDate[0] = new MySqlParameter();
            parDate[0].ParameterName = driverDb.ParamPrefics + "TimeStart";
            parDate[0].Value = zw.dtStart;
            parDate[0].MySqlDbType = MySqlDbType.DateTime;
            parDate[1] = new MySqlParameter();
            parDate[1].ParameterName = driverDb.ParamPrefics + "TimeEnd";
            parDate[1].Value = zw.dtEnd;
            parDate[1].MySqlDbType = MySqlDbType.DateTime;
            //Console.WriteLine(sSQLinsert);  
            _cnMySQL.ExecuteNonQueryCommand(sSQLinsert, parDate);
            int Id = (int)_cnMySQL.GetScalarValue("SELECT Id FROM agro_ordertf WHERE ID = LAST_INSERT_ID()");
            _cnMySQL.CloseMySQLConnection();
            return Id;
        }
        //�������� ������ � ������
        public bool DeleteRecordContentFuel(int iID)
        {
            try
            {
                string sSQLdel = "DELETE FROM agro_ordertf WHERE ID = " + iID.ToString();
                if (TestLinks())
                {
                    ConnectMySQL _cnMySQL = new ConnectMySQL();
                    _cnMySQL.ExecuteNonQueryCommand(sSQLdel);
                    sSQLdel = "DELETE FROM agro_ordertf WHERE ID_node = " + iID.ToString();
                    _cnMySQL.ExecuteNonQueryCommand(sSQLdel);
                    _cnMySQL.CloseMySQLConnection();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //���� �������
        private void DeleteItemFuel(object sender, System.EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("������������� �������� ������?", "��������", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {

                if (DeleteRecordContentFuel((int)gbvFuel.GetRowCellValue(gbvFuel.FocusedRowHandle, "IdF")))
                {
                    gbvFuel.DeleteRow(gbvFuel.FocusedRowHandle);
                }
            }
        }
        //���� �� ������ ������
        private void gbvFuel_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu)e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem("�������", new EventHandler(DeleteItemFuel));
            menuItem.Tag = e.Menu;
            gvMenu.Items.Add(menuItem);
        }
        //��������� �������� ��������
        private void gbvFuel_RowStyle(object sender, RowStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                int iZone = Convert.ToInt32(View.GetRowCellDisplayText(e.RowHandle, View.Columns["ZoneID"]));
                if (iZone == (int)WorkIDzone.�����_�����)
                {
                    //e.Appearance.BackColor = Color.Salmon;
                    //e.Appearance.BackColor2 = Color.SeaShell;
                    if (View.GetRowCellDisplayText(e.RowHandle, View.Columns["ZoneName"]) != "����� �����")
                        View.SetRowCellValue(e.RowHandle, View.Columns.ColumnByName("ZoneName"), "����� �����");
                    //e.Appearance.ForeColor = Color.Blue;
                    if (e.Appearance.Font.Bold == false)
                    {
                        e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                    }

                }
            }

        }
#endregion
        #region ������������ ���������� ������ ������

        public int Create_dsAtlanta(int Mobitel_Id)
        {
            ID_main_ext = 0;
            Algorithm.bDisableAction = true;
            DateTime dtSeek = new DateTime(dtDateInit.Value.Year, dtDateInit.Value.Month, dtDateInit.Value.Day);
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + Mobitel_Id.ToString() + " AND time >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and time  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#", "time");
            if (dataRows.Length > 0) return dataRows.Length;
            bDataSetReady = false;
            tspbOrder.Value = 0;
            tspbOrder.Maximum = 100;
            tspbOrder.Visible = true;
            tslbOrder.Visible = true;
            tslbOrder.Text = "���� ������ � ��";
            if (_dsAtlanta.mobitels.Rows.Count == 0)
            {
                FillMobitelsTable();
                FillZonesTable();
                FillPointsTable();
                FillSensorsTable();
                FillRelationAlgorithmsTable();
                FillSensorCoefficientTable();
                FillSettingTable();
                FillTeamTable();
                FillDriverTable();
                FillVehicleTable();
            }
            // ������� �������
            _dsAtlanta.dataview.Clear();
            //int k = _dsAtlanta.mobitels.Rows.Count;
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID((int)Mobitel_Id);
            m_row.Check = true;
            // ������ ������ ���������� � ������
            bwCreateData.RunWorkerAsync(0);
            while (!bDataSetReady)
            {
                Application.DoEvents();
                continue;
            }
            dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("Mobitel_Id=" + Mobitel_Id.ToString() + " AND time >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and time  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#", "time");
            return dataRows.Length;

        }
        private void bwCreateData_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e != null)
            {
                tspbOrder.Value = e.ProgressPercentage;
            }
        }
        private void bwCreateData_DoWork(object sender, DoWorkEventArgs e)
        {
            taDataView.Connection.ConnectionString = CONNECTION_STRING;
            string name = "";
            DateTime dtSeek = new DateTime(dtDateInit.Value.Year, dtDateInit.Value.Month, dtDateInit.Value.Day);
            taDataView.FillByID(_dsAtlanta, dtSeek, dtSeek.AddDays(1), ref bwCreateData, ref name);
        }
        private void bwCreateData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                string msg = String.Format(
                  "������ ���������� ������ ������� ������ ��������� �� ��.\r\n{0}", e.Error.Message);
                //RW_Log.ExecuteLogging.Log("App", msg);
                MessageBox.Show(msg, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            tspbOrder.Visible = false;
            tslbOrder.Text = "������.�������:" + _dsAtlanta.dataview.Rows.Count.ToString();
            Algorithm.AtlantaDataSet = _dsAtlanta;
            bDataSetReady = true;
        }
        private void FillMobitelsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.MobitelsTableAdapter mobitelsTableAdapter;
            mobitelsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.MobitelsTableAdapter();
            mobitelsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            mobitelsTableAdapter.GetParam(_dsAtlanta.mobitels);
        }
        private void FillZonesTable()
        {
            LocalCache.atlantaDataSetTableAdapters.ZonesTableAdapter zonesTableAdapter;
            zonesTableAdapter = new LocalCache.atlantaDataSetTableAdapters.ZonesTableAdapter();
            zonesTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            zonesTableAdapter.Fill(_dsAtlanta.zones);
        } // FillZonesTable()
        private void FillSensorsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter sensorsTableAdapter;
            sensorsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();
            sensorsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensorsTableAdapter.Fill(_dsAtlanta.sensors);
        } // FillSensorsTable()

        /// <summary>
        /// ���������� ������� RelationAlgorithms
        /// </summary>
        private void FillRelationAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter relationalgorithmsTableAdapter;
            relationalgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();
            relationalgorithmsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            relationalgorithmsTableAdapter.Fill(_dsAtlanta.relationalgorithms);
        }

        /// <summary>
        /// ���������� ������� SensorCoefficient
        /// </summary>
        private void FillSensorCoefficientTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter sensorcoefficientTableAdapter;
            sensorcoefficientTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter();
            sensorcoefficientTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensorcoefficientTableAdapter.Fill(_dsAtlanta.sensorcoefficient);
        }

        /// <summary>
        /// ���������� ������� SensorAlgorithms
        /// </summary>
        private void FillSensorAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter sensoralgorithmsTableAdapter;
            sensoralgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter();
            sensoralgorithmsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            sensoralgorithmsTableAdapter.Fill(_dsAtlanta.sensoralgorithms);
        }
        private void FillSettingTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
            settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
            settingTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            settingTableAdapter.Fill(_dsAtlanta.setting);
        }
        private void FillPointsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.PointsTableAdapter pointsTableAdapter;
            pointsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.PointsTableAdapter();
            pointsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            pointsTableAdapter.Fill(_dsAtlanta.points);
        }
        private void FillDriverTable()
        {
            LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter driverTableAdapter;
            driverTableAdapter = new LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter();
            driverTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            driverTableAdapter.Fill(_dsAtlanta.driver);
        } // FillDriverTable()

        /// <summary>
        /// ���������� ������� Team
        /// </summary>
        private void FillTeamTable()
        {
            LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter teamTableAdapter;
            teamTableAdapter = new LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter();
            teamTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            teamTableAdapter.Fill(_dsAtlanta.team);
        }

        /// <summary>
        /// ���������� ������� Vehicle
        /// </summary>
        private void FillVehicleTable()
        {
            LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
            vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
            vehicleTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            vehicleTableAdapter.Fill(_dsAtlanta.vehicle);
        }
        private void Free_dsAtlanta()
        {
            Algorithm.bDisableAction = false;
            _dsAtlanta.Clear();
            _dsAtlanta.Dispose();
        }
#endregion

        //private int AddRecordZone(int ZoneId, string ZoneNamne)
        //{
        //    if (txId.Text == "0") SaveChanges();
        //    if (txId.Text == "0") return 0;
        //    ConnectMySQL _cnMySQL = new ConnectMySQL();
        //    string sSQLinsert = "insert into agro_ordertf (agro_ordertf.Id_main, agro_ordertf.Id_zone, agro_ordertf.TimeStart,"
        // + " agro_ordertf.TimeEnd, agro_ordertf.TimeRate, agro_ordertf.FactSquare,"
        // + " agro_ordertf.FuelStart, agro_ordertf.FuelAdd, agro_ordertf.FuelSub, agro_ordertf.FuelEnd,"
        // + " agro_ordertf.FuelExpens, agro_ordertf.FuelExpensAvg, agro_ordertf.FuelExpensAvgRate,"
        // + " agro_ordertf.Fuel_ExpensMove, agro_ordertf.Fuel_ExpensStop, agro_ordertf.Fuel_ExpensTotal,"
        // + " agro_ordertf.Fuel_ExpensAvg, agro_ordertf.Fuel_ExpensAvgRate, agro_ordertf.Level, agro_ordertf.Id_node )"
        //     + " VALUES (" + txId.Text
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + "," + ZoneId
        //     + ")";
        //    _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);


        //    int Id = (int)_cnMySQL.GetScalarValue("SELECT Id FROM agro_ordertf WHERE ID = LAST_INSERT_ID()");
        //    _cnMySQL.CloseMySQLConnection();
        //    //Console.WriteLine(sSQLinsert);  
        //    gbvFuel.AddNewRow();
        //    gbvFuel.SetRowCellValue(gbvFuel.FocusedRowHandle, gbvFuel.Columns.ColumnByName("IdF"), Id);
        //    gbvFuel.SetRowCellValue(gbvFuel.FocusedRowHandle, gbvFuel.Columns.ColumnByName("ZoneID"), ZoneId);
        //    gbvFuel.SetRowCellValue(gbvFuel.FocusedRowHandle, gbvFuel.Columns.ColumnByName("ZoneName"), ZoneNamne);
        //    gbvFuel.SetRowCellValue(gbvFuel.FocusedRowHandle, gbvFuel.Columns.ColumnByName("FactSquareF"), 0);
        //    return Id;

        //}
        //��������� ����������� ������ � ������� ������� ���
        private void GetFactFuelDUTDRT_old()
        {
            //���������� ������ 
            LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID((int)cbMobitel.SelectedValue);
            BaseReports.Procedure.IAlgorithm brKlm = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Kilometrage();
            brKlm.SelectItem(m_row);
            brKlm.Run();
            BaseReports.Procedure.IAlgorithm brFuel1 = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Fuel(AlgorithmType.FUEL1);
            brFuel1.SelectItem(m_row);
            brFuel1.Run();
            //AddAlgorithm(new Kilometrage());
            //AddAlgorithm(new Rotate(ALG_TYPE.ROTATE_E));
            //AddAlgorithm(new BaseReports.Procedure.FlowMeter(ALG_TYPE.FUEL2));
            //AddAlgorithm(new CrossZonesAlgorithm());
            //AddAlgorithm(new FuelCZInkomDrtAlgorithm());
            //BaseReports.Procedure.IAlgorithm brRotate = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Rotate(ALG_TYPE.ROTATE_E);
            //brRotate.SelectItem(m_row);
            //brRotate.Run(); 

            BaseReports.Procedure.IAlgorithm brFlowMeter = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.FlowMeter(AlgorithmType.FUEL2);
            brFlowMeter.SelectItem(m_row);
            brFlowMeter.Run();

            //BaseReports.Procedure.IAlgorithm brCZAlgorithm = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.CrossZonesAlgorithm();
            //brCZAlgorithm.SelectItem(m_row);
            //brCZAlgorithm.Run(); 
            //BaseReports.Procedure.IAlgorithm brFuel2 = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Fuel(ALG_TYPE.FUEL2);
            //brFuel2.SelectItem(m_row);
            //brFuel2.Run();
            //������� ��� ����������� ���
            //LoadSubForm();
            //�������� �������� ��������
            DeleteRecordContentTotals();
            LoadSubFormFuel();
            //GetFact(); 
            for (int i = 0; i < gbvFuel.RowCount; i++)
            {
                ////DUT    
                //SetFuelFactRowDUT(m_row,(DateTime)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("TimeStartF")),
                //    (DateTime)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("TimeEndF")),i);
                //SetFactZoneUpdateFuelDUT(i);
                //Double dbDebit = (double)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("FuelExpens"));
                //Double dbSquare = (double)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("FactSquareF"));
                //TimeSpanConverter tsc = new TimeSpanConverter();
                //Double dbHMoto = ((TimeSpan)tsc.ConvertFromInvariantString((string)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("TimeRateF")))).TotalHours;
                //if (dbSquare > 0) dbSquare = Math.Round(dbDebit / dbSquare, 2);
                ////�������  ������, �/��
                //gbvFuel.SetRowCellValue(i, gbvFuel.Columns.ColumnByName("FuelExpensAvg"), dbSquare);
                //if (dbHMoto > 0) dbHMoto = Math.Round(dbDebit / dbHMoto, 2);
                ////������� ������ �/�������
                //gbvFuel.SetRowCellValue(i, gbvFuel.Columns.ColumnByName("FuelExpensAvgRate"), dbHMoto);
                ////DRT
                //SetFuelFactRowDRT(m_row, (DateTime)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("TimeStartF")),
                //    (DateTime)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("TimeEndF")), i);
                //SetFactZoneUpdateFuelDRT(i);

            }

            ////// //������� ������ � ����� ����������� ��� - ��������� �� ���� � ��� ���
            ////// int Id_���_��� = AddRecordZone((int)WorkIDzone.���_���, WorkIDzone.���_���.ToString());
            ////////��������� �� ���� 
            ////// int Id_�����_����� =  AddRecordZone((int)WorkIDzone.�����_�����, (string)WorkIDzone.�����_�����.ToString());
            ////////int gbvFuel.FocusedRowHandle = gbvFuel.FocusedRowHandle;
            ////// LoadSubFormFuel();
            //////DateTime dtSeek = new DateTime(dtDateInit.Value.Year, dtDateInit.Value.Month, dtDateInit.Value.Day);
            //////SetFuelFactRowDUT(m_row, dtSeek, dtSeek.AddDays(1), gbvFuel.RowCount - 1);
            //////SetFuelFactRowDRT(m_row, dtSeek, dtSeek.AddDays(1), gbvFuel.RowCount -1);
            //////SetFactZoneUpdateFuelDRT(gbvFuel.RowCount - 1);

            //////TimeSpan tsTotal = GetFactZoneRotationDay(m_row);
            //////Double dbHMotoT = tsTotal.TotalHours;

            //////Double dbDebitT = (double)gbvFuel.GetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("FuelExpens"));
            //////if (dbHMotoT > 0)
            //////{ dbDebitT = Math.Round(dbDebitT / dbHMotoT, 2); }
            //////else dbDebitT = 0;
            //////gbvFuel.SetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("FuelExpensAvgRate"), dbDebitT);
            //////gbvFuel.SetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("FuelExpensAvg"), 0);
            //////SetFactZoneUpdateFuelDUT(gbvFuel.RowCount - 1);
            //////dbDebitT = (double)gbvFuel.GetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("Fuel_ExpensTotal"));
            //////if (dbHMotoT > 0)
            //////{ dbDebitT = Math.Round(dbDebitT / dbHMotoT, 2); }
            //////else dbDebitT = 0;
            //////gbvFuel.SetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("Fuel_ExpensAvgRate"), dbDebitT);
            //////gbvFuel.SetRowCellValue(gbvFuel.RowCount - 1, gbvFuel.Columns.ColumnByName("Fuel_ExpensAvg"), 0);
            //////SetFactZoneUpdateFuelDRT(gbvFuel.RowCount - 1);
            ////// Set���_���();
            //int RowHandleTotal = gbvFuel.FocusedRowHandle;
            //by calling the ColumnView.UpdateCurrentRow method. gbvFuel.FocusedRowHandle
            //Immediately after the row was added, its handle is equal to BaseView.DataRowCount - 1
            //For rows added using the ColumnView.AddNewRow method, you can write cell initialization code within a ColumnView.InitNewRow event handler. 
            //int rowHandle = View.GetDataRowHandleByGroupRowHandle(view.FocusedRowHandle);
            //http://www.devexpress.com/Help/?document=xtragrid/customdocument752.htm

        }
        private void SetFuelFactRowDUT_old(LocalCache.atlantaDataSet.mobitelsRow m_row, DateTime dtStart, DateTime dtEnd, int RowHandle)
        {
            double dbFuelStart = 0;
            double dbFuelEnd = 0;
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = PartialAlgorithms.GetDataViewRows(m_row, dtStart, dtEnd);
            if (dataRows.Length > 0)
            {
                LocalCache.atlantaDataSet.FuelValueRow[] fuel_val_rows =
                  (LocalCache.atlantaDataSet.FuelValueRow[])dataRows[0].GetChildRows("dataview_FuelValue");
                if (fuel_val_rows.Length > 0)
                { dbFuelStart = fuel_val_rows[0].Average; }
                fuel_val_rows = (LocalCache.atlantaDataSet.FuelValueRow[])dataRows[dataRows.Length - 1].GetChildRows("dataview_FuelValue");
                if (fuel_val_rows.Length > 0)
                {
                    dbFuelEnd = fuel_val_rows[0].Average;
                }
            }
            LocalCache.atlantaDataSet.FuelReportRow[] FRRows = (LocalCache.atlantaDataSet.FuelReportRow[])_dsAtlanta.FuelReport.Select("mobitel_id=" + cbMobitel.SelectedValue.ToString() + " AND time_ >= #" + dtStart.ToString("g", CultureInfo.CreateSpecificCulture("en-us")) + "# and time_  < #" + dtEnd.ToString("g", CultureInfo.CreateSpecificCulture("en-us")) + "#", "time_");
            double dbZapr = 0;
            double dbSliv = 0;
            //������� � ������, �
            gbvFuel.SetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelStart"), Math.Round(dbFuelStart, 2));
            //������� � �����, �
            gbvFuel.SetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelEnd"), Math.Round(dbFuelEnd, 2));
            if (FRRows.Length > 0)
            {
                foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                {
                    if (tmp_FR_row.dValue > 0)
                        dbZapr += tmp_FR_row.dValue;
                    else
                        dbSliv += tmp_FR_row.dValue;
                }
            }
            //����������, �
            gbvFuel.SetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelAdd"), Math.Round(dbZapr, 2));
            //�����, �
            gbvFuel.SetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelSub"), Math.Round(-dbSliv, 2));
            //����� ������, �
            gbvFuel.SetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelExpens"), Math.Round((dbFuelStart + dbZapr - dbFuelEnd), 2));
        }
        //��������� ����������� ������ � ������� ������� ���
        private void SetFuelFactRowDRT_old(LocalCache.atlantaDataSet.mobitelsRow m_row, DateTime dtStart, DateTime dtEnd, int RowHandle)
        {
            LocalCache.atlantaDataSet.dataviewRow[] dataRows = PartialAlgorithms.GetDataViewRows(m_row, dtStart, dtEnd);
            PartialAlgorithms PA = new PartialAlgorithms(m_row);
            if (dataRows.Length > 0)
            {
                // ������ ������� � ��������, �
                Double dbExpensMove = PA.GetFuelUseInMotionDrt(dataRows, 0);
                gbvFuel.SetRowCellValue(RowHandle, "Fuel_ExpensMove", Math.Round(dbExpensMove, 2));
                // ������ ������� �� ��������, �
                Double dbExpensStop = PA.GetFuelUseInStopDrt(dataRows, 0, 0);
                gbvFuel.SetRowCellValue(RowHandle, "Fuel_ExpensStop", Math.Round(dbExpensStop, 2));
                // ����� ������ ������� � ����, �
                gbvFuel.SetRowCellValue(RowHandle, "Fuel_ExpensTotal", Math.Round((dbExpensMove + dbExpensStop), 2));
                if ((int)gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("ZoneID")) > 0)
                {
                    Double dbSquare = (double)gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FactSquareF"));
                    TimeSpanConverter tsc = new TimeSpanConverter();
                    Double dbHMoto = ((TimeSpan)tsc.ConvertFromInvariantString((string)gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("TimeRateF")))).TotalHours;
                    if (dbSquare > 0) dbSquare = Math.Round((dbExpensMove + dbExpensStop) / dbSquare, 2);
                    //������� ������, �/��
                    gbvFuel.SetRowCellValue(RowHandle, "Fuel_ExpensAvg", dbSquare);
                    if (dbHMoto > 0) dbHMoto = Math.Round((dbExpensMove + dbExpensStop) / dbHMoto, 2);
                    //������� ������, �/�������
                    gbvFuel.SetRowCellValue(RowHandle, "Fuel_ExpensAvgRate", dbHMoto);
                }
            }
        }
        private void UpdateRecordFactFuel(ref ZoneWork zw)
        {
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                sSQLupdate = "UPDATE agro_orderTf SET TimeStart = ?TimeStart"
                    + ", TimeEnd = ?TimeEnd, TimeRate = '" + zw.tsEngineOn.ToString()
                    + "', FactSquare = " + Math.Round( zw.Distance, 2 ).ToString().Replace( ",", "." )
                    + " WHERE (ID = " + zw.ID_orderT + ")";
            }
            else if((DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                sSQLupdate = "UPDATE agro_orderTf SET TimeStart = @TimeStart"
                    + ", TimeEnd = @TimeEnd, TimeRate = '" + zw.tsEngineOn.ToString()
                    + "', FactSquare = " + Math.Round( zw.Distance, 2 ).ToString().Replace( ",", "." )
                    + " WHERE (ID = " + zw.ID_orderT + ")";
            }
            //+ ", Sum = " + Math.Round(zw.Distance * (double)gbvContent.GetRowCellValue(zw.RowHandle, gbvContent.Columns.ColumnByName("Price")), 2).ToString().Replace(",", ".")
            MySqlParameter[] parDate = new MySqlParameter[2];
            parDate[0] = new MySqlParameter();
            parDate[0].ParameterName = driverDb.ParamPrefics + "TimeStart";
            parDate[0].Value = zw.dtStart;
            parDate[0].MySqlDbType = MySqlDbType.DateTime;
            parDate[1] = new MySqlParameter();
            parDate[1].ParameterName = driverDb.ParamPrefics + "?TimeEnd";
            parDate[1].Value = zw.dtEnd;
            parDate[1].MySqlDbType = MySqlDbType.DateTime;
            _cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
            _cnMySQL.CloseMySQLConnection();
        }
        private void SetFactZoneUpdateFuelDUT(int RowHandle)
        {
            //FuelAdd FuelStart FuelSub FuelEnd FuelExpens FuelExpensAvg FuelExpensAvgRate
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            string sSQLupdate = "UPDATE agro_ordertf SET FuelAdd = " + gbvFuel.GetRowCellValue(RowHandle, "FuelAdd").ToString().Replace(",", ".")
                + ", FuelSub = " + gbvFuel.GetRowCellValue(RowHandle, "FuelSub").ToString().Replace(",", ".")
                + ", FuelStart = " + gbvFuel.GetRowCellValue(RowHandle, "FuelStart").ToString().Replace(",", ".")
                + ", FuelEnd = " + gbvFuel.GetRowCellValue(RowHandle, "FuelEnd").ToString().Replace(",", ".")
                + ", FuelExpens = " + gbvFuel.GetRowCellValue(RowHandle, "FuelExpens").ToString().Replace(",", ".")
                + ", FuelExpensAvg = " + gbvFuel.GetRowCellValue(RowHandle, "FuelExpensAvg").ToString().Replace(",", ".")
                + ", FuelExpensAvgRate = " + gbvFuel.GetRowCellValue(RowHandle, "FuelExpensAvgRate").ToString().Replace(",", ".")
                + " WHERE (ID = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("IdF")) + ")";
            //Console.WriteLine(sSQLupdate);
            _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
            _cnMySQL.CloseMySQLConnection();
        }
        private void SetFactZoneUpdateFuelDRT(int RowHandle)
        {
            //Fuel_ExpensMove Fuel_ExpensStop Fuel_ExpensTotal Fuel_ExpensAvg Fuel_ExpensAvgRate
            ConnectMySQL _cnMySQL = new ConnectMySQL();
            string sSQLupdate = "UPDATE agro_ordertf SET FuelAdd = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("FuelAdd")).ToString().Replace(",", ".")
                + ", Fuel_ExpensMove = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("Fuel_ExpensMove")).ToString().Replace(",", ".")
                + ", Fuel_ExpensStop = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("Fuel_ExpensStop")).ToString().Replace(",", ".")
                + ", Fuel_ExpensTotal = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("Fuel_ExpensTotal")).ToString().Replace(",", ".")
                + ", Fuel_ExpensAvg = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("Fuel_ExpensAvg")).ToString().Replace(",", ".")
                + ", Fuel_ExpensAvgRate = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("Fuel_ExpensAvgRate")).ToString().Replace(",", ".")
                + " WHERE (ID = " + gbvFuel.GetRowCellValue(RowHandle, gbvFuel.Columns.ColumnByName("IdF")) + ")";
            //Console.WriteLine(sSQLupdate);
            _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
            _cnMySQL.CloseMySQLConnection();
        }
        private bool GetFactZoneStopMove(int DataGps_ID)
        {
            atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])_dsAtlanta.KilometrageReport.Select("InitialPointId <= " + DataGps_ID + " AND FinalPointId > =" + DataGps_ID);
            if (km_rows.Length == 0) return false;
            switch (km_rows[0].State)
            {
                case "��������":
                    {
                        return true;
                        break;
                    }
                case "�������":
                    {
                        return false;
                        break;
                    }
                default:
                    return false;
                    break;

            }
        }
        private TimeSpan GetFactZoneRotationDay(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            BaseReports.Procedure.IAlgorithm brRot = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Rotation();
            brRot.SelectItem(m_row);
            brRot.Run();
            DateTime dtSeek = new DateTime(dtDateInit.Value.Year, dtDateInit.Value.Month, dtDateInit.Value.Day);
            //atlantaDataSet.RotateReportRow[] rt_rows = (atlantaDataSet.RotateReportRow[])_dsAtlanta.RotateReport.Select("MobitelId = " + cbMobitel.SelectedValue.ToString() + " AND InitialTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# AND FinalTime <= #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#");
            atlantaDataSet.RotateReportRow[] rt_rows = (atlantaDataSet.RotateReportRow[])_dsAtlanta.RotateReport.Select();
            TimeSpan tsTotal = new TimeSpan();
            if (rt_rows.Length == 0) return tsTotal;
            foreach (atlantaDataSet.RotateReportRow rt_row in rt_rows)
            {
                if (rt_row.State == "��������")
                {
                    tsTotal = tsTotal.Add(rt_row.Interval);
                }
            }
            return tsTotal;

        }
        private void Set���_���()
        {
            //Fuel_ExpensTotal
            int RowHandle_���_��� = gbvFuel.LocateByValue(0, gbvFuel.Columns.ColumnByName("ZoneID"), (int)WorkIDzone.���_���);
            if (RowHandle_���_��� != GridControl.InvalidRowHandle)
            {
                double dbAdd = 0;
                double dbSub = 0;
                double dbTotalDUT = 0;
                double dbMove = 0;
                double dbStop = 0;
                double dbTotalDRT = 0;
                double dbHMotoDUT = 0;
                double dbHMotoDRT = 0;
                for (int i = 0; i < gbvFuel.RowCount; i++)
                {
                    if (gbvFuel.GetRowCellValue(i, "ZoneID") != null)
                    {
                        switch ((int)gbvFuel.GetRowCellValue(i, gbvFuel.Columns.ColumnByName("ZoneID")))
                        {
                            case (int)WorkIDzone.���_���:
                                {
                                    break;
                                }
                            case (int)WorkIDzone.�����_�����:
                                {
                                    dbAdd += (double)gbvFuel.GetRowCellValue(i, "FuelAdd");
                                    dbSub += (double)gbvFuel.GetRowCellValue(i, "FuelSub");
                                    dbTotalDUT += (double)gbvFuel.GetRowCellValue(i, "FuelExpens");
                                    dbMove += (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensMove");
                                    dbStop += (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensStop");
                                    dbTotalDRT += (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensTotal");
                                    if ((double)gbvFuel.GetRowCellValue(i, "FuelExpensAvgRate") > 0)
                                        dbHMotoDUT += (double)gbvFuel.GetRowCellValue(i, "FuelExpens")
                                            / (double)gbvFuel.GetRowCellValue(i, "FuelExpensAvgRate");
                                    if ((double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensAvgRate") > 0)
                                        dbHMotoDRT += (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensTotal")
                                            / (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensAvgRate");
                                    break;
                                }
                            default:
                                {
                                    dbAdd -= (double)gbvFuel.GetRowCellValue(i, "FuelAdd");
                                    dbSub -= (double)gbvFuel.GetRowCellValue(i, "FuelSub");
                                    dbTotalDUT -= (double)gbvFuel.GetRowCellValue(i, "FuelExpens");
                                    dbMove -= (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensMove");
                                    dbStop -= (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensStop");
                                    dbTotalDRT -= (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensTotal");
                                    if ((double)gbvFuel.GetRowCellValue(i, "FuelExpensAvgRate") > 0)
                                        dbHMotoDUT -= (double)gbvFuel.GetRowCellValue(i, "FuelExpens")
                                            / (double)gbvFuel.GetRowCellValue(i, "FuelExpensAvgRate");
                                    if ((double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensAvgRate") > 0)
                                        dbHMotoDRT -= (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensTotal")
                                            / (double)gbvFuel.GetRowCellValue(i, "Fuel_ExpensAvgRate");
                                    break;
                                }
                        }
                    }
                }
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelAdd", Math.Round(dbAdd, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelSub", Math.Round(dbSub, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelExpens", Math.Round(dbTotalDUT, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "Fuel_ExpensMove", Math.Round(dbMove, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "Fuel_ExpensStop", Math.Round(dbStop, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "Fuel_ExpensTotal", Math.Round(dbTotalDRT, 2));
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelStart", 0);
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelEnd", 0);
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelExpensAvg", 0);
                gbvFuel.SetRowCellValue(RowHandle_���_���, "FuelExpensAvgRate", 0);
                gbvFuel.SetRowCellValue(RowHandle_���_���, "Fuel_ExpensAvgRate", 0);
                if (dbHMotoDUT > 0) gbvFuel.SetRowCellValue(RowHandle_���_���, gbvFuel.Columns.ColumnByName("FuelExpensAvgRate"), Math.Round(dbTotalDUT / dbHMotoDUT, 2));
                if (dbHMotoDRT > 0) gbvFuel.SetRowCellValue(RowHandle_���_���, gbvFuel.Columns.ColumnByName("Fuel_ExpensAvgRate"), Math.Round(dbTotalDRT / dbHMotoDRT, 2));

                SetFactZoneUpdateFuelDUT(RowHandle_���_���);
                SetFactZoneUpdateFuelDRT(RowHandle_���_���);
            }

        }
        private void GetZonesFromCZDetailed()
        {
            //LocalCache.atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID((int)cbMobitel.SelectedValue);
            ////�������� ��� ���� ��� �������
            //foreach (atlantaDataSet.zonesRow row in _dsAtlanta.zones.Rows)
            //{
            //    row.Checked = true;
            //}
            //CheckZonesDataSet czDataSet = CheckZonesDataSetCreator.GetDataSet();
            //if (czDataSet != null) czDataSet.Clear();
            //if (_dsAtlanta.CrossZones != null) _dsAtlanta.CrossZones.Clear();  
            //CrossZonesAlgorithm brCzAlg = new CrossZonesAlgorithm();
            //brCzAlg.SelectItem(m_row);
            //brCzAlg.Run();
            //CzDetailedAlgorithm brCzDet = new CzDetailedAlgorithm();
            //brCzDet.SelectItem(m_row);
            //brCzDet.Run();
            //DataTable dt = czDataSet.CZ_Detailed;
            //Console.WriteLine(dt.Rows.Count.ToString()); 
            //CheckZonesDataSet.CZ_DetailedRow[] cz_det_rows = (CheckZonesDataSet.CZ_DetailedRow[])czDataSet.CZ_Detailed.Select();
            //foreach (CheckZonesDataSet.CZ_DetailedRow  cz_det_row in cz_det_rows)
            //{
            //    Console.WriteLine(cz_det_row.NameCZ1); 
            //}

        }
        private void bgvContent_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                //e.Info.DisplayText =  (e.RowHandle + 1).ToString();
                //if (!icon) e.Info.ImageIndex = -1;
            }
        }
        private void Order_FormClosed(object sender, FormClosedEventArgs e)
        {
            Free_dsAtlanta();
        }
        //���������� ������ � ������ �� ������� �������
        private void AddExternal(int Mobitel_Id)
        {
            if (ID_main_ext == 0)
            {
                ConnectMySQL _cnMySQL = new ConnectMySQL();
                string sSQLinsert = "";
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    sSQLinsert = "INSERT INTO `agro_order`(`Date`,Id_mobitel,Regime,`Comment`) VALUES ('"
                      + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
                      + Mobitel_Id + ","
                      + (int)WorkRegimes.�������������� + ",'������ �� �������')";
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    sSQLinsert = "INSERT INTO agro_order(Date,Id_mobitel,Regime,Comment) VALUES ('"
                       + dtDateInit.Value.ToString( "yyyy-MM-dd" ) + "',"
                       + Mobitel_Id + ","
                       + (int)WorkRegimes.�������������� + ",'������ �� �������')";
                }
                _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);
                ID_main_ext = (int)_cnMySQL.GetScalarValue("SELECT Id FROM agro_order WHERE ID = LAST_INSERT_ID()");
                _cnMySQL.CloseMySQLConnection();
                GetTotals(Mobitel_Id);
            }
        }

        private void txAgregatWidth_TextChanged(object sender, EventArgs e)
        {

        }

        private void btMapView_Click(object sender, EventArgs e)
        {
            //Form_Utils.callbackEventHandler((DateTime)gvOrder.GetRowCellValue(gvOrder.FocusedRowHandle, gvOrder.Columns.ColumnByName("DateGrn")),
            // (int)gvOrder.GetRowCellValue(gvOrder.FocusedRowHandle,"Id_mobitel"));
            if (cbMobitel.SelectedValue  != null)
            {
                this.WindowState = FormWindowState.Minimized;
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.GetType().ToString() == "Agro.MainAgro")
                    {
                        frm.WindowState = FormWindowState.Minimized;
                    }
                } 
                Form_Utils.DrawEventArgs ea = new Form_Utils.DrawEventArgs(dtDateInit.Value,(int)cbMobitel.SelectedValue);
                Form_Utils.DrawMapEventHandler(this, ea);
            }
        }


    }       //private void gcContent_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        //{
        //    switch (e.Button.ButtonType)
        //    {
        //        case DevExpress.XtraEditors.NavigatorButtonType.Append:
        //            e.Handled = true;
        //            AddRecord();
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.CancelEdit:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Custom:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Edit:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.EndEdit:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.First:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Last:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Next:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.NextPage:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Prev:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.PrevPage:
        //            break;
        //        case DevExpress.XtraEditors.NavigatorButtonType.Remove:
        //            {
        //                if (DeleteRecordContent((int)gbvContent.GetRowCellValue(gbvContent.FocusedRowHandle, "Id"))) e.Handled = false; ;
        //            }
        //            break;
        //        default:
        //            break;
        //    }

        //}
}