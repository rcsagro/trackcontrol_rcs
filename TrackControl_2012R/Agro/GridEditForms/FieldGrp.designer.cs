namespace Agro.GridEditForms
{
    partial class FieldGrp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FieldGrp));
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGroupe = new DevExpress.XtraEditors.LabelControl();
            this.btAutoCreate = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(509, 11);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(418, 11);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(327, 11);
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(169, 179);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "�������� ������";
            this.leGroupe.Properties.ValueMember = "id";
            this.leGroupe.Size = new System.Drawing.Size(302, 20);
            this.leGroupe.TabIndex = 17;
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbGroupe
            // 
            this.lbGroupe.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbGroupe.Appearance.Options.UseFont = true;
            this.lbGroupe.Location = new System.Drawing.Point(13, 182);
            this.lbGroupe.Name = "lbGroupe";
            this.lbGroupe.Size = new System.Drawing.Size(140, 14);
            this.lbGroupe.TabIndex = 16;
            this.lbGroupe.Text = "������ ����������� ���";
            // 
            // btAutoCreate
            // 
            this.btAutoCreate.Appearance.Font = new System.Drawing.Font("Tahoma", 8F);
            this.btAutoCreate.Appearance.Options.UseFont = true;
            this.btAutoCreate.Image = ((System.Drawing.Image)(resources.GetObject("btAutoCreate.Image")));
            this.btAutoCreate.Location = new System.Drawing.Point(477, 180);
            this.btAutoCreate.Name = "btAutoCreate";
            this.btAutoCreate.Size = new System.Drawing.Size(137, 19);
            this.btAutoCreate.TabIndex = 20;
            this.btAutoCreate.Text = "������������";
            this.btAutoCreate.ToolTip = "�������������� �������� ����� ����� � �������� � ��� �����";
            this.btAutoCreate.Click += new System.EventHandler(this.btAutoCreate_Click);
            // 
            // FieldGrp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(626, 360);
            this.Controls.Add(this.btAutoCreate);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.lbGroupe);
            this.Name = "FieldGrp";
            this.Controls.SetChildIndex(this.lbGroupe, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            this.Controls.SetChildIndex(this.btAutoCreate, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbGroupe;
        private DevExpress.XtraEditors.SimpleButton btAutoCreate;
    }
}
