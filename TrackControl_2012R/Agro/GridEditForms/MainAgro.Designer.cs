namespace Agro.GridEditForms
{
    partial class Agregat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGroup = new DevExpress.XtraEditors.LabelControl();
            this.lbVehicles = new DevExpress.XtraEditors.LabelControl();
            this.pgProperties = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.teNumeric = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ceBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.teMax50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.teMax20 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.leWork = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.crProperties = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIdentifier = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erWidth = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erInvNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erWorkDefault = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erUseDefault = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSensorPresent = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcVehicles = new DevExpress.XtraGrid.GridControl();
            this.gvVehicles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.VID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.SID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(72, 12);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leGroupe.Properties.Appearance.Options.UseFont = true;
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "Id";
            this.leGroupe.Size = new System.Drawing.Size(526, 21);
            this.leGroupe.TabIndex = 17;
            this.leGroupe.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.leGroupe_ButtonClick);
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbGroup
            // 
            this.lbGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbGroup.Appearance.Options.UseFont = true;
            this.lbGroup.Location = new System.Drawing.Point(19, 15);
            this.lbGroup.Name = "lbGroup";
            this.lbGroup.Size = new System.Drawing.Size(39, 14);
            this.lbGroup.TabIndex = 16;
            this.lbGroup.Text = "������";
            // 
            // lbVehicles
            // 
            this.lbVehicles.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbVehicles.Appearance.Options.UseFont = true;
            this.lbVehicles.Location = new System.Drawing.Point(12, 326);
            this.lbVehicles.Name = "lbVehicles";
            this.lbVehicles.Size = new System.Drawing.Size(321, 14);
            this.lbVehicles.TabIndex = 44;
            this.lbVehicles.Text = "������������ �� ��������� ��� ������������ �������";
            // 
            // pgProperties
            // 
            this.pgProperties.Location = new System.Drawing.Point(13, 171);
            this.pgProperties.Name = "pgProperties";
            this.pgProperties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teNumeric,
            this.ceBox,
            this.teMax50,
            this.teMax20,
            this.leWork});
            this.pgProperties.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crProperties,
            this.erIdentifier,
            this.erWidth,
            this.erInvNumber,
            this.erWorkDefault,
            this.erUseDefault,
            this.erSensorPresent,
            this.erOutId});
            this.pgProperties.Size = new System.Drawing.Size(601, 149);
            this.pgProperties.TabIndex = 47;
            this.pgProperties.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgProperties_CellValueChanged);
            // 
            // teNumeric
            // 
            this.teNumeric.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.teNumeric.AutoHeight = false;
            this.teNumeric.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.teNumeric.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.teNumeric.Mask.BeepOnError = true;
            this.teNumeric.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.teNumeric.Name = "teNumeric";
            this.teNumeric.ValidateOnEnterKey = true;
            // 
            // ceBox
            // 
            this.ceBox.AutoHeight = false;
            this.ceBox.Caption = "";
            this.ceBox.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ceBox.Name = "ceBox";
            // 
            // teMax50
            // 
            this.teMax50.AutoHeight = false;
            this.teMax50.MaxLength = 50;
            this.teMax50.Name = "teMax50";
            // 
            // teMax20
            // 
            this.teMax20.AutoHeight = false;
            this.teMax20.MaxLength = 20;
            this.teMax20.Name = "teMax20";
            // 
            // leWork
            // 
            this.leWork.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.leWork.AutoHeight = false;
            this.leWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leWork.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leWork.DisplayMember = "Name";
            this.leWork.DropDownRows = 10;
            this.leWork.Name = "leWork";
            this.leWork.NullText = "";
            this.leWork.ValueMember = "Id";
            // 
            // crProperties
            // 
            this.crProperties.Name = "crProperties";
            this.crProperties.Properties.Caption = "��������";
            // 
            // erIdentifier
            // 
            this.erIdentifier.Name = "erIdentifier";
            this.erIdentifier.Properties.Caption = "�������������";
            this.erIdentifier.Properties.FieldName = "Identifier";
            // 
            // erWidth
            // 
            this.erWidth.Name = "erWidth";
            this.erWidth.Properties.Caption = "������, ����";
            this.erWidth.Properties.FieldName = "Width";
            this.erWidth.Properties.RowEdit = this.teNumeric;
            // 
            // erInvNumber
            // 
            this.erInvNumber.Name = "erInvNumber";
            this.erInvNumber.Properties.Caption = "����������� �����";
            this.erInvNumber.Properties.FieldName = "InvNumber";
            this.erInvNumber.Properties.RowEdit = this.teMax50;
            // 
            // erWorkDefault
            // 
            this.erWorkDefault.Height = 16;
            this.erWorkDefault.Name = "erWorkDefault";
            this.erWorkDefault.Properties.Caption = "������ �� ���������";
            this.erWorkDefault.Properties.FieldName = "Id_work";
            this.erWorkDefault.Properties.RowEdit = this.leWork;
            // 
            // erUseDefault
            // 
            this.erUseDefault.Name = "erUseDefault";
            this.erUseDefault.Properties.Caption = "������������ �� ��������� ��� �������";
            this.erUseDefault.Properties.FieldName = "Def";
            this.erUseDefault.Properties.RowEdit = this.ceBox;
            // 
            // erSensorPresent
            // 
            this.erSensorPresent.Name = "erSensorPresent";
            this.erSensorPresent.Properties.Caption = "������������ ���������� ������";
            this.erSensorPresent.Properties.FieldName = "HasSensor";
            this.erSensorPresent.Properties.RowEdit = this.ceBox;
            // 
            // erOutId
            // 
            this.erOutId.Name = "erOutId";
            this.erOutId.Properties.Caption = "��� ������� ����";
            this.erOutId.Properties.FieldName = "IdOutLink";
            this.erOutId.Properties.RowEdit = this.teMax20;
            // 
            // gcVehicles
            // 
            this.gcVehicles.Location = new System.Drawing.Point(13, 346);
            this.gcVehicles.MainView = this.gvVehicles;
            this.gcVehicles.Name = "gcVehicles";
            this.gcVehicles.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LookUpEdit1,
            this.LookUpEdit2,
            this.repositoryItemLookUpEdit1,
            this.LookUpEdit3});
            this.gcVehicles.Size = new System.Drawing.Size(601, 175);
            this.gcVehicles.TabIndex = 48;
            this.gcVehicles.UseEmbeddedNavigator = true;
            this.gcVehicles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicles});
            // 
            // gvVehicles
            // 
            this.gvVehicles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.TID,
            this.VID,
            this.SID});
            this.gvVehicles.GridControl = this.gcVehicles;
            this.gvVehicles.Name = "gvVehicles";
            // 
            // Id
            // 
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            // 
            // TID
            // 
            this.TID.Caption = "������";
            this.TID.ColumnEdit = this.LookUpEdit1;
            this.TID.FieldName = "TID";
            this.TID.Name = "TID";
            this.TID.Visible = true;
            this.TID.VisibleIndex = 0;
            // 
            // LookUpEdit1
            // 
            this.LookUpEdit1.AutoHeight = false;
            this.LookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit1.Name = "LookUpEdit1";
            // 
            // VID
            // 
            this.VID.Caption = "������������ ��������";
            this.VID.ColumnEdit = this.LookUpEdit2;
            this.VID.FieldName = "VID";
            this.VID.Name = "VID";
            this.VID.Visible = true;
            this.VID.VisibleIndex = 1;
            // 
            // LookUpEdit2
            // 
            this.LookUpEdit2.AutoHeight = false;
            this.LookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit2.Name = "LookUpEdit2";
            // 
            // SID
            // 
            this.SID.Caption = "���������� ������";
            this.SID.ColumnEdit = this.LookUpEdit3;
            this.SID.FieldName = "SID";
            this.SID.Name = "SID";
            this.SID.Visible = true;
            this.SID.VisibleIndex = 2;
            // 
            // LookUpEdit3
            // 
            this.LookUpEdit3.AutoHeight = false;
            this.LookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit3.Name = "LookUpEdit3";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // Agregat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 577);
            this.Controls.Add(this.gcVehicles);
            this.Controls.Add(this.pgProperties);
            this.Controls.Add(this.lbVehicles);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.lbGroup);
            this.Name = "Agregat";
            this.Controls.SetChildIndex(this.lbGroup, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            this.Controls.SetChildIndex(this.lbVehicles, 0);
            this.Controls.SetChildIndex(this.pgProperties, 0);
            this.Controls.SetChildIndex(this.gcVehicles, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teMax20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbGroup;
        private DevExpress.XtraEditors.LabelControl lbVehicles;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgProperties;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crProperties;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIdentifier;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erWidth;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erInvNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erWorkDefault;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erUseDefault;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSensorPresent;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutId;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teNumeric;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teMax50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teMax20;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leWork;
        private DevExpress.XtraGrid.GridControl gcVehicles;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn TID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn VID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn SID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
    }
}
