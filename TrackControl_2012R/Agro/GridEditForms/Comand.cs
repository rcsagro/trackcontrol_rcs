using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using Agro.Utilites;
using DevExpress.XtraEditors;

namespace Agro.GridEditForms
{
    public partial class Comand : Form, IFormDictionary
    {
        private DataTable _dtGrn;
        private DataGridView  _dgvGrn;
        private string sId; 
                public Comand()
        {
            InitializeComponent();  
        }
        public Comand(ref DataGridView dgvGrn, bool bNew)
            : this()
        {
            _dgvGrn = dgvGrn;
            _dtGrn = (DataTable)_dgvGrn.DataSource;
            if (!bNew)
            {
                txId.Text = Convert.ToString(_dgvGrn.SelectedRows[0].Cells["Id"].Value);
                txName.Text = (string)_dgvGrn.SelectedRows[0].Cells["Name"].Value;
                if (_dgvGrn.SelectedRows[0].Cells["Comment"].Value != DBNull.Value)
                    txComment.Text = (string)_dgvGrn.SelectedRows[0].Cells["Comment"].Value;
                if (_dgvGrn.SelectedRows[0].Cells["DateInit"].Value != DBNull.Value)
                    dtDateInit.Value  = (DateTime)_dgvGrn.SelectedRows[0].Cells["DateInit"].Value; ;
                if (_dgvGrn.SelectedRows[0].Cells["DateComand"].Value != DBNull.Value)
                    dtDateComand.Value = (DateTime)_dgvGrn.SelectedRows[0].Cells["DateComand"].Value; ;
                sId = txId.Text;
                btDelete.Enabled = true;
            }
            else
            {
                dtDateInit.Value = DateTime.Today; 
                dtDateComand.Value = dtDateInit.Value;
            }
            ValidateFields();
            btSave.Enabled = false; 
        }
          // ����������� ��� ��������� � ���������� �������� �� �������
        public Comand(ref DataGridView dgvGrn)
        {
            _dgvGrn = dgvGrn;
            _dtGrn = (DataTable)_dgvGrn.DataSource;
            sId = Convert.ToString(_dgvGrn.SelectedRows[0].Cells["Id"].Value);
        }
        private void txName_TextChanged(object sender, EventArgs e)
        {
            ValidateFields(); 
        }
        private void txComment_TextChanged(object sender, EventArgs e)
        {
            if (ValidateFields())  btSave.Enabled = true;  
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }
        #region IFormDictionary Members
            public bool ValidateFields()
            {
                btSave.Enabled = true;
                if (!ValidateFieldsText(txName))
                    btSave.Enabled = false;
                else
                    btSave.Enabled = true;
                return btSave.Enabled;
            }
            public bool ValidateFieldsText(TextBox txField)
                {
                    if (txField.Text.Trim().Length == 0)
                    {
                        errP.SetError(txField, "���� �� ����� ���� ������");
                        txField.Focus();
                        return false;
                    }
                    else
                    {
                        errP.SetError(txField, "");
                        return true;
                    }
                }
            public void SaveChanges()
            {

                //DataTable _dtGrn = (DataTable)_dtGrn.DataSource;   #" + dtSeek.ToString("MM.dd.yyyy") + "# 
                object[] newRow = new object[5];
                if (ValidateFields())
                {
                    ConnectMySQL _cnMySQL = new ConnectMySQL(); tttt
                    if (txId.Text == "0")
                    {
                        string sSQLinsert = AgroQuery.Command.InsertIntoAgroPrice + this.txName.Text
                                + "','" + dtDateInit.Value.ToString( "yyyy-MM-dd" )
                                + "','" + dtDateComand.Value.ToString( "yyyy-MM-dd" )
                                + "','" + this.txComment.Text
                                + "')";
                        }

                        _cnMySQL.ExecuteNonQueryCommand(sSQLinsert);
                        txId.Text = Convert.ToString(_cnMySQL.GetScalarValue("SELECT Id FROM agro_Price WHERE ID = LAST_INSERT_ID()"));
                        sId = txId.Text; 
                    }
                    else
                    {
                        string sSQLupdate = "UPDATE agro_Price SET Name = '" 
                            + this.txName.Text
                            + "', DateInit = '" + dtDateInit.Value.ToString("yyyy-MM-dd")
                            + "', DateComand = '" + dtDateComand.Value.ToString("yyyy-MM-dd")
                            + "',Comment = '" + this.txComment.Text 
                            + "' WHERE (ID = " + txId.Text + ")";
                        Console.WriteLine(sSQLupdate);
                        _cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                        
                    }
                    newRow[0] = txId.Text;
                    newRow[1] = dtDateInit.Value;
                    newRow[2] = dtDateComand.Value;
                    newRow[3] = txName.Text;
                    newRow[4] = txComment.Text;
                    _dtGrn.BeginLoadData();
                    _dtGrn.LoadDataRow(newRow, LoadOption.Upsert);
                    _dtGrn.EndLoadData();
                    btSave.Enabled = false;
                    _cnMySQL.CloseMySQLConnection(); 
                }
            }
            public bool TestLinks()
            {
                //ConnectMySQL _cnMySQL = new ConnectMySQL();
                //string sSQLtest = "SELECT   COUNT(agro_field.Id) FROM   agro_field WHERE   Id_main  = " + sId;
                //int iLinks = Convert.ToInt32 (_cnMySQL.GetScalarValue(sSQLtest));
                //_cnMySQL.CloseMySQLConnection();
                //if (iLinks == 0)
                    return true;
                //else
                //    //MessageBox.Show("������ ����� ������: " + Convert.ToString(iLinks),"�������� ���������");
                //    return false;
            }
            public bool DeleteRecord()
        {
            string sSQLdel = "DELETE FROM agro_Price WHERE ID = " + sId;
            if (TestLinks())
            {
                if (DialogResult.Yes == XtraMessageBox.Show(Consts.MSG_DELETE_QUESTION, Consts.MSG_CAPTION_AGRO, MessageBoxButtons.YesNo))
                {
                    ConnectMySQL _cnMySQL = new ConnectMySQL();
                    _cnMySQL.ExecuteNonQueryCommand(sSQLdel);
                    _cnMySQL.CloseMySQLConnection();
                    DataRow row = _dtGrn.Rows.Find(sId);
                    _dtGrn.Rows.Remove(row);  
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion
        private void Comand_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Yes )
                 if (!DeleteRecord()) e.Cancel = true; ;
        }

        private void dtDateInit_ValueChanged(object sender, EventArgs e)
        {
            ValidateFields(); 
        }

        private void dtDateComand_ValueChanged(object sender, EventArgs e)
        {
            ValidateFields(); 
        }


        #region IFormDictionary Members


        public void SetControls()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IFormDictionary Members


        public void GetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IFormDictionary Members


        public bool ValidateFieldsAction()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void UpdateGRN()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}