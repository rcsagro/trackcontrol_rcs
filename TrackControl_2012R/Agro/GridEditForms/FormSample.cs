using System;
using System.Data;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.ServiceForms;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid;

namespace Agro.GridEditForms
{
    public partial class FormSample : DevExpress.XtraEditors.XtraForm, IFormDictionary
    {
        /// <summary>
        /// ������ �����������
        /// </summary>
        protected GridView _gvDict;

        /// <summary>
        /// ������������� �����������
        /// </summary>
        protected int _Id = 0;

        /// <summary>
        /// ������ �����������
        /// </summary>
        protected DictionaryItem di;

        /// <summary>
        /// ��������� �������� ������
        /// </summary>
        protected bool bFormLoading;

        /// <summary>
        /// ���������� ���������� ����� �����
        /// </summary>
        protected bool bContentChange;

        /// <summary>
        /// ������� � ����� ����������� ����� � ������������ �������
        /// </summary>
        protected bool IsContentExist;

        public FormSample()
        {
            InitializeComponent();
            LocalizationSample();
        }

        public FormSample(GridView gvDict, bool bNew)
            : this()
        {
            bFormLoading = true;
            _gvDict = gvDict;
            if (!bNew)
            {
                _Id = (int) _gvDict.GetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"]);
                txId.Text = _Id.ToString();
                btDelete.Enabled = true;
            }
            btSave.Enabled = false;

        }

        #region IFormDictionary Members

        public virtual bool ValidateFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ValidateFieldsAction()
        {
            if (bFormLoading) return false;
            if (!ValidateFields())
            {
                btSave.Enabled = false;
                return false;
            }
            else
            {
                btSave.Enabled = true;
                return true;
            }
        }

        public virtual void SaveChanges(bool Question)
        {
            bool bUpdateGRN = false;
            SetFields();
            if (_Id == 0)
            {
                _Id = di.AddItem();
                txId.Text = _Id.ToString();
                if (_Id > 0)
                {
                    _gvDict.AddNewRow();
                    UpdateGRN();
                    _gvDict.UpdateCurrentRow();
                    LoadContent();
                    bUpdateGRN = true;
                }
                if (IsContentExist)
                    this.DialogResult = DialogResult.No;
            }
            else
            {
                if (di.EditItem())
                {
                    bUpdateGRN = true;
                }
            }
            if (bContentChange)
            {
                if (SaveContent()) bContentChange = false;
            }
            if (bUpdateGRN)
            {
                UpdateGRN();
                bUpdateGRN = false;
            }
        }

        public virtual bool TestLinks()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool SaveContent()
        {
            return false;
        }

        public virtual bool LoadContent()
        {
            return false;
        }

        public virtual bool DeleteRecord(bool Question)
        {
            if (di.DeleteItem(true))
            {
                if (_Id == (int) _gvDict.GetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"]))
                {
                    DataRow row = _gvDict.GetDataRow(_gvDict.FocusedRowHandle);
                    row.Delete();
                }
                return true;
            }
            else
                return false;
        }

        public virtual void SetControls()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void GetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateGRN()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SetFields()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        private void FormSample_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Yes)
            {
                if (!DeleteRecord(true))
                {
                    e.Cancel = true;
                }
                else
                    di.Dispose();
            }
                // ��� �������� ������ ����������� � �������� ����������� ������ ����� �� �����������
                // ��������������� ����������� ������� ������ � ����
            else if (this.DialogResult == DialogResult.No)
            {
                e.Cancel = true;
                btSave.Enabled = false;
            }
        }

        private void txName_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void meComment_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            SaveChanges(true);
        }

        private void btOrders_Click(object sender, EventArgs e)
        {
            new DictionaryItemInOrders(di).ShowDialog();
        }

        private void FormSample_Load(object sender, EventArgs e)
        {
            if (txName.Visible)
                lbName.Visible = true;
            else
                lbName.Visible = false;
        }

        #region �����������

        protected void LocalizationSample()
        {
            btCancel.Text = Resources.Out;
            btSave.Text = Resources.Save;
            btDelete.Text = Resources.Delete;
            btOrders.Text = Resources.Orders;
            labelControl3.Text = Resources.Code;
            labelControl2.Text = Resources.Comment;
            lbName.Text = Resources.NameRes;
        }

        #endregion


    }
}