using System;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraGrid.Views.Grid; 

namespace Agro.GridEditForms
{
    public partial class WorkType : FormSample
    {
        public WorkType()
        {
            InitThis();
        }
        public WorkType(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            bFormLoading = true;
            InitThis();
            di = new DictionaryAgroWorkType(_Id);
            this.Text = this.Text + ": " + Resources.WorkTypes; 
            if (!bNew) GetFields();
            bFormLoading = false;
        }

        public override bool ValidateFields()
        {
            if (bFormLoading) return false;
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(teSpeedBottom, dxErrP) |
                !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(teSpeedTop, dxErrP) |
                !DicUtilites.ValidateLookUp(leTypeWork , dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["GroupWork"], leGroupe.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["TypeWork"], leTypeWork.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Comment"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["SpeedBottom"], teSpeedBottom.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["SpeedTop"], teSpeedTop.Text);
        }

        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            di.IdOutLink =teOutLinkId.Text ;
            (di as DictionaryAgroWorkType).SpeedBottom = Convert.ToDouble(teSpeedBottom.Text);
            (di as DictionaryAgroWorkType).SpeedTop = Convert.ToDouble(teSpeedTop.Text);
            (di as DictionaryAgroWorkType).TypeWork = (int)leTypeWork.EditValue;
            (di as DictionaryAgroWorkType).Id_main = (int)(leGroupe.EditValue ?? 0);
            
             
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            teOutLinkId.Text = di.IdOutLink;
            teSpeedBottom.Text = (di as DictionaryAgroWorkType).SpeedBottom.ToString()  ;
            teSpeedTop.Text = (di as DictionaryAgroWorkType).SpeedTop.ToString();
            leTypeWork.EditValue = (di as DictionaryAgroWorkType).TypeWork;
            leGroupe.EditValue = (di as DictionaryAgroWorkType).Id_main;
        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            btOrders.Visible = true;
            leTypeWork.Properties.DataSource = DictionaryAgroWorkType.GetWorkTypesList();
            leTypeWork.EditValue = DictionaryAgroWorkType.WorkTypes.WorkInField;
            leGroupe.Properties.DataSource = DictionaryAgroWorkTypeGroup.GetList();
        
        }

        private void teSpeedTop_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        private void teSpeedBottom_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction(); 
        }

        #region �����������
        private void Localization()
        {
            labelControl4.Text = Resources.SpeedDownLimit;
            labelControl5.Text = Resources.SpeedUpperBound;
            labelControl6.Text = Resources.KMH;
            labelControl7.Text = Resources.KMH;
            lbType.Text = Resources.Type;
            lbGroup.Text = Resources.Groupe;
            lbOutLinkId.Text = Resources.OuterBaseLink; 

        }
        #endregion

        private void leTypeWork_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

        private void teOutLinkId_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}

