namespace Agro.GridEditForms
{
    partial class WorkType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.teOutLinkId = new DevExpress.XtraEditors.TextEdit();
            this.lbOutLinkId = new DevExpress.XtraEditors.LabelControl();
            this.lbType = new DevExpress.XtraEditors.LabelControl();
            this.leTypeWork = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.teSpeedTop = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.teSpeedBottom = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbGroup = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLinkId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTypeWork.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedTop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedBottom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbName.Appearance.Options.UseFont = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.teOutLinkId);
            this.panelControl2.Controls.Add(this.lbOutLinkId);
            this.panelControl2.Controls.Add(this.lbType);
            this.panelControl2.Controls.Add(this.leTypeWork);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.teSpeedTop);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.teSpeedBottom);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Location = new System.Drawing.Point(13, 171);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(601, 69);
            this.panelControl2.TabIndex = 9;
            // 
            // teOutLinkId
            // 
            this.teOutLinkId.EditValue = "";
            this.teOutLinkId.Location = new System.Drawing.Point(209, 41);
            this.teOutLinkId.Name = "teOutLinkId";
            this.teOutLinkId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teOutLinkId.Properties.Appearance.Options.UseFont = true;
            this.teOutLinkId.Properties.Appearance.Options.UseTextOptions = true;
            this.teOutLinkId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teOutLinkId.Size = new System.Drawing.Size(111, 21);
            this.teOutLinkId.TabIndex = 16;
            this.teOutLinkId.EditValueChanged += new System.EventHandler(this.teOutLinkId_EditValueChanged);
            // 
            // lbOutLinkId
            // 
            this.lbOutLinkId.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbOutLinkId.Appearance.Options.UseFont = true;
            this.lbOutLinkId.Location = new System.Drawing.Point(99, 44);
            this.lbOutLinkId.Name = "lbOutLinkId";
            this.lbOutLinkId.Size = new System.Drawing.Size(104, 14);
            this.lbOutLinkId.TabIndex = 15;
            this.lbOutLinkId.Text = "��� ������� ����";
            // 
            // lbType
            // 
            this.lbType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbType.Appearance.Options.UseFont = true;
            this.lbType.Location = new System.Drawing.Point(15, 15);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(22, 14);
            this.lbType.TabIndex = 14;
            this.lbType.Text = "���";
            // 
            // leTypeWork
            // 
            this.leTypeWork.Location = new System.Drawing.Point(62, 13);
            this.leTypeWork.Name = "leTypeWork";
            this.leTypeWork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leTypeWork.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leTypeWork.Properties.DisplayMember = "Name";
            this.leTypeWork.Properties.NullText = "";
            this.leTypeWork.Properties.ValueMember = "Id";
            this.leTypeWork.Size = new System.Drawing.Size(257, 20);
            this.leTypeWork.TabIndex = 13;
            this.leTypeWork.TabStop = false;
            this.leTypeWork.EditValueChanged += new System.EventHandler(this.leTypeWork_EditValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(565, 43);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(26, 14);
            this.labelControl7.TabIndex = 12;
            this.labelControl7.Text = "��/�";
            // 
            // teSpeedTop
            // 
            this.teSpeedTop.EditValue = "0";
            this.teSpeedTop.Location = new System.Drawing.Point(510, 41);
            this.teSpeedTop.Name = "teSpeedTop";
            this.teSpeedTop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teSpeedTop.Properties.Appearance.Options.UseFont = true;
            this.teSpeedTop.Properties.Appearance.Options.UseTextOptions = true;
            this.teSpeedTop.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teSpeedTop.Size = new System.Drawing.Size(49, 21);
            this.teSpeedTop.TabIndex = 11;
            this.teSpeedTop.EditValueChanged += new System.EventHandler(this.teSpeedTop_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(565, 14);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(26, 14);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "��/�";
            // 
            // teSpeedBottom
            // 
            this.teSpeedBottom.EditValue = "0";
            this.teSpeedBottom.Location = new System.Drawing.Point(510, 12);
            this.teSpeedBottom.Name = "teSpeedBottom";
            this.teSpeedBottom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teSpeedBottom.Properties.Appearance.Options.UseFont = true;
            this.teSpeedBottom.Properties.Appearance.Options.UseTextOptions = true;
            this.teSpeedBottom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teSpeedBottom.Size = new System.Drawing.Size(49, 21);
            this.teSpeedBottom.TabIndex = 2;
            this.teSpeedBottom.EditValueChanged += new System.EventHandler(this.teSpeedBottom_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(344, 44);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(150, 14);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "������� ������ ��������";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(344, 15);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(148, 14);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "������ ������ ��������";
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(72, 12);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "�������� ������";
            this.leGroupe.Properties.ValueMember = "Id";
            this.leGroupe.Size = new System.Drawing.Size(542, 20);
            this.leGroupe.TabIndex = 19;
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbGroup
            // 
            this.lbGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbGroup.Appearance.Options.UseFont = true;
            this.lbGroup.Location = new System.Drawing.Point(19, 15);
            this.lbGroup.Name = "lbGroup";
            this.lbGroup.Size = new System.Drawing.Size(43, 14);
            this.lbGroup.TabIndex = 18;
            this.lbGroup.Text = "������ ";
            // 
            // WorkType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(631, 311);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.lbGroup);
            this.Controls.Add(this.panelControl2);
            this.Name = "WorkType";
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.lbGroup, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLinkId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTypeWork.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedTop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedBottom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit teSpeedBottom;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit teSpeedTop;
        private DevExpress.XtraEditors.LookUpEdit leTypeWork;
        private DevExpress.XtraEditors.LabelControl lbType;
        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbGroup;
        private DevExpress.XtraEditors.TextEdit teOutLinkId;
        private DevExpress.XtraEditors.LabelControl lbOutLinkId;
    }
}
