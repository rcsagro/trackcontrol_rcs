using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.GridEditForms;
using Agro.Properties;
using Agro.Reports.OrderReports;
using Agro.Utilites;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;
using AGis = Agro.Controls.AgroMap;
using TUtil = Agro.Utilites.TotUtilites;
using DEnum = Agro.Dictionaries.DictionariesEnum;

namespace Agro
{
    public partial class MainAgro : DevExpress.XtraEditors.XtraForm
    {

        #region �����������

        public MainAgro()
        {
            InitializeComponent();
            //------------------------------------------------------------

            string mainCaption = System.Reflection.Assembly.GetCallingAssembly().GetName().Name + " "
                                 + System.Reflection.Assembly.GetCallingAssembly().GetName().Version;

            try
            {
                Text = TUtil.GetMainFormCaption(mainCaption, (int) Consts.L_NUMBER_AGRO);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error MainAgro", MessageBoxButtons.OK);
            }
            //------------------------------------------------------------

            Localization();
            InitControls();
            // ���� ����������� ����� - ��������� �� ������� ������������
            //DriverDb db = new DriverDb();
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                if (driverDb.GetDataReaderRecordsCount(AgroQuery.MainAgro.SelectFromAgroField) == 0)
                {
                    xtbView.SelectedTabPageIndex = 0;
                }
                else
                {
                    XtraGridService.LayOutSave(gcOrder, bgvOrder, Consts.APP_DATA_AGRO, sFileInitXml);
                    XtraGridService.LayOutSave(gcOrderContent, bgvOrderContent, Consts.APP_DATA_AGRO,
                        sFileInitXmlContent);
                    xtbView.SelectedTabPageIndex = 1;
                }
            }
            //db.CloseDbConnection();
            SetControls();
            tlDictionary.ExpandAll();
            btnStopExecutingData.ItemClick += btnStopExecutingData_ItemClick;
        }

        private void InitControls()
        {
            SetControlDates();
            leGroupe.DataSource = DocItem.VehiclesModel.RootGroupsWithItems();
            leCategory.DataSource = VehicleCategoryProvader.GetList();
            _map = new AGis {Dock = DockStyle.Fill};
            pnMap.Controls.Add(_map);
            formViewOrder += new vieworder(ViewOrderFromControl);
            _map.UserViewOrder = formViewOrder;
        }

        private void SetControlDates()
        {
            DateTime stardData = TotUtilites.GetOrderDate(DateTime.Today);
            _cdeOrders = new ControlDataInterval(stardData, stardData.AddDays(1).AddMinutes(-1));
            _cdeDrivers = _cdeOrders;
            bdeStart.EditValue = _cdeOrders.Begin;
            bdeEnd.EditValue = _cdeOrders.End;
            bdeStartReport.EditValue = _cdeDrivers.Begin;
            bdeEndReport.EditValue = _cdeDrivers.End;
        }

        #endregion

        #region ����

        private AGis _map;
        private GraficSensor _gsAgr;
        private GraficSensor _gsDrv;
        private PlanSetView _planSetView;
        private FieldsTcSetView _fieldsTcSetView;
        // ���������� ����-��������� - ���� ��� � ����
        private bool bCreateOrderDay = true;
        //��������� �������� ������� �������
        private const string sFileXml = "OrderBound.xml";
        private const string sFileInitXml = "OrderBoundInit.xml";
        private const string sFileXmlContent = "OrderBoundContent.xml";
        private const string sFileInitXmlContent = "OrderBoundInitContent.xml";

        private enum SensorRegimes
        {
            TT = 1,
            SENSOR
        }

        private ControlDataInterval _cdeOrders;
        private ControlDataInterval _cdeDrivers;
        private RepController _reportsController;

        #endregion

        #region �������� �������

        /// <summary>
        /// �������� ����� �� �������� � ������
        /// </summary>
        /// <param name="Id_Order"></param>
        public delegate void vieworder(int Id_Order);

        private event vieworder formViewOrder;

        #endregion

        #region ������

        private void sbtAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                {
                    ViewFormDictionary(true, false);
                    break;
                }
                case 5:
                {
                    using (PlanController pc = new PlanController())
                    {
                        pc.AddNewPlanFromView();
                    }
                    break;
                }
                case 6:
                {
                    using (FieldsTcController fc = new FieldsTcController())
                    {
                        fc.AddNewFromView();
                    }
                    break;
                }
            }
        }

        private void sbtRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadTabGrid();
        }

        private void sbtEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ViewDoc();
        }

        private void sbtDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                {
                    if (tlDictionary.FocusedNode == null)
                    {
                        XtraMessageBox.Show(Resources.SelectValueForDelete, Resources.ApplicationName);
                        return;
                    }
                    ViewFormDictionary(false, true);
                    break;
                }
                case 1:
                {
                    using (OrderItem oi = new OrderItem())
                    {
                        oi.DeleteSelectedFromGrid(bgvOrder);
                    }
                    break;
                }
                case 5:
                {
                    _planSetView.DeleteSelectedFromGrid();
                    break;
                }
                case 6:
                {
                    _fieldsTcSetView.DeleteSelectedFromGrid();
                    break;
                }
            }
            SetControls();
        }

        private void sbtExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExcelExport();
        }

        private void ExcelExport()
        {
            if (DialogResult.No ==
                XtraMessageBox.Show(Resources.ExcelOutputConfim, Resources.ApplicationName, MessageBoxButtons.YesNo))
                return;
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    switch (xtbOrder.SelectedTabPageIndex)
                    {
                        case 0:
                        {
                            XtraGridService.ExportToExcel(bgvOrder, xtbOrder.SelectedTabPage.Text);
                            break;
                        }
                        case 1:
                        {
                            XtraGridService.ExportToExcel(bgvOrderContent, xtbOrder.SelectedTabPage.Text);
                            break;
                        }
                    }
                    break;
                }
                case 2:
                {
                    _reportsController.ExportToExcel();
                    break;
                }
                case 3:
                {
                    _gsAgr.ExportExcel();
                    break;
                }
                case 4:
                {
                    _gsDrv.ExportExcel();
                    break;
                }
                case 5:
                {
                    _planSetView.ExportExcel();
                    break;
                }
                case 6:
                {
                    _fieldsTcSetView.ExportExcel();
                    break;
                }
            }
        }

        private void btFactControl_ItemClick(object sender, ItemClickEventArgs e)
        {
            FactCals(true);
        }

        private void btFact_ItemClick(object sender, ItemClickEventArgs e)
        {
            FactCals(false);
        }

        private void FactCals(bool bControl)
        {
            if (bgvOrder == null || bgvOrder.SelectedRowsCount == 0)
                return;
            if (DialogResult.No ==
                XtraMessageBox.Show(Resources.ConfirmFactRecalc, Resources.ApplicationName, MessageBoxButtons.YesNo))
                return;
            if (bgvOrder.SelectedRowsCount > 1)
                SetProgress(bgvOrder.SelectedRowsCount + 1);
            LockMenuBar(true);
            for (int i = 0; i < bgvOrder.SelectedRowsCount; i++)
            {
                if (pbStatus.Visibility == BarItemVisibility.Always)
                    SetProgress(i + 1);
                int iOrder = 0;
                if (
                    Int32.TryParse(
                        bgvOrder.GetRowCellValue(bgvOrder.GetSelectedRows()[i], bgvOrder.Columns["Id"]).ToString(),
                        out iOrder))
                {
                    SetStatus(Resources.OrderRecordsRefresh + ":" + iOrder.ToString());
                    using (OrderItem oi = new OrderItem(iOrder))
                    {
                        if (!oi.IsDocBlocked)
                        {
                            oi.ChangeStatusEvent += SetStatus;
                            oi.ContentAutoCreate(bControl);
                            oi.ChangeStatusEvent -= SetStatus;
                        }
                    }
                }

            }
            LoadTabGrid();
            LockMenuBar(false);
        }

        private void btExcelImport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExcelImport();
        }

        private void btStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadOrdesMainOrDetal();

        }

        private void LoadOrdesMainOrDetal()
        {
            switch (xtbOrder.SelectedTabPageIndex)
            {
                case 0:
                {
                    LoadOrders();
                    break;
                }
                case 1:
                {
                    LoadOrdersContent();
                    break;
                }
            }
        }

        private void btFilterClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bleGroupe.EditValue = null;
            bleMobitelGrn.EditValue = null;
            bleCategory.EditValue = null;
        }

        private void btColumnsSet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbOrder.SelectedTabPageIndex)
            {
                case 0:
                {
                    bgvOrder.ColumnsCustomization();
                    break;
                }
                case 1:
                {
                    bgvOrderContent.ColumnsCustomization();
                    break;
                }
            }
        }

        private void btColumnsSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbOrder.SelectedTabPageIndex)
            {
                case 0:
                {
                    if (XtraGridService.LayOutSave(gcOrder, bgvOrder, Consts.APP_DATA_AGRO, sFileXml))
                        XtraMessageBox.Show(Resources.JournalSettingSaved, Resources.ApplicationName);
                    break;
                }
                case 1:
                {
                    if (XtraGridService.LayOutSave(gcOrderContent, bgvOrderContent, Consts.APP_DATA_AGRO,
                        sFileXmlContent))
                        XtraMessageBox.Show(Resources.JournalSettingSaved, Resources.ApplicationName);
                    break;
                }
            }

        }

        private void btColumnRestore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbOrder.SelectedTabPageIndex)
            {
                case 0:
                {
                    XtraGridService.LayOutRead(bgvOrder, Consts.APP_DATA_AGRO, sFileInitXml);
                    break;
                }
                case 1:
                {
                    XtraGridService.LayOutRead(bgvOrderContent, Consts.APP_DATA_AGRO, sFileInitXmlContent);
                    break;
                }
            }
            //gcOrder.ForceInitialize();  
        }

        private void btSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            new AgroSet().ShowDialog();
        }

        #endregion#region �����������

        private void tlDictionary_FocusedNodeChanged(object sender,
            DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            LoadDictionary();
            SetControls();
        }

        private void LoadDictionary()
        {
            if (tlDictionary.FocusedNode == null)
                return;
            switch ((DEnum.Dictionaries) tlDictionary.FocusedNode.Id)
            {
                case DEnum.Dictionaries.���������:
                {
                    gcDict.MainView = gvWorks;
                    gcDict.DataSource = DictionaryAgroWorkType.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = true;
                    break;
                }
                case DEnum.Dictionaries.����������������:
                {
                    gcDict.MainView = gvGWorks;
                    gcDict.DataSource = DictionaryAgroWorkTypeGroup.GetListForJournal();
                    btExcelImport.Enabled = true;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.�����������:
                {
                    gcDict.MainView = gvGFields;
                    gcDict.DataSource = DictionaryAgroFieldGrp.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.����:
                {
                    gcDict.MainView = gvFields;
                    gcDict.DataSource = DictionaryAgroField.GetListForJournal();
                    leFieldGrope.DataSource = DictionaryAgroFieldGrp.GetList();
                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();

                        string sSQL = AgroQuery.MainAgro.SelectZonesId;
                        leZone.DataSource = driverDb.GetDataTable(sSQL);
                    }
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.�����������������������:
                {
                    gcDict.MainView = gvUnit;
                    gcDict.DataSource = DictionaryAgroUnit.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    gcDict.MainView = gvCalt;
                    gcDict.DataSource = DictionaryAgroCulture.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.���������������������������:
                {
                    gcDict.MainView = gvGAgr;
                    gcDict.DataSource = DictionaryAgroAgregatGrp.GetListForJournal();
                    leWork.DataSource = DictionaryAgroWorkType.GetList();
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.��������������������:
                {
                    gcDict.MainView = gvAgr;
                    gcDict.DataSource = DictionaryAgroAgregat.GetListForJournal();
                    leWork.DataSource = DictionaryAgroWorkType.GetList();
                    leGroupeAgr.DataSource = DictionaryAgroAgregatGrp.GetList();
                    btExcelImport.Enabled = true;
                    btXmlExport.Enabled = true;
                    break;
                }
                case DEnum.Dictionaries.�������:
                {
                    gcDict.MainView = gvOrder;
                    gcDict.DataSource = DictionaryAgroCommand.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    gcDict.MainView = gvPrice;
                    leAgregat.DataSource = DictionaryAgroAgregat.GetList();
                    leUnit.DataSource = DictionaryAgroUnit.GetList();
                    gcDict.DataSource = DictionaryAgroPrice.GetListForJournal();
                    leWork.DataSource = DictionaryAgroWorkType.GetList();
                    using (DriverDb driverDb = new DriverDb())
                    {
                        driverDb.ConnectDb();
                        leOrder.DataSource = driverDb.GetDataTable(AgroQuery.MainAgro.SelectAgroPrice);
                        leMobitel.DataSource = driverDb.GetDataTable(AgroQuery.MainAgro.SelectVehicleMobitel);
                    }
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.���������������������:
                {
                    gcDict.MainView = gvDtReasons;
                    gcDict.DataSource = DictionaryAgroPlanDtReason.GetListForJournal();
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case DEnum.Dictionaries.������:
                {
                    gcDict.MainView = gvSeasons;
                    rleCulture.DataSource = DictionaryAgroCulture.GetList();
                    gcDict.DataSource = DictionaryAgroSeason.GetListForJournal();
                    btExcelImport.Enabled = true;
                    btXmlExport.Enabled = false;
                    break;
                }
            }
        }

        /// <summary>
        /// �������� ����� �����������
        /// </summary>
        /// <param name="bNew"></param>
        /// <param name="bDelete"></param>
        private void ViewFormDictionary(bool bNew, bool bDelete)
        {
            if (tlDictionary.FocusedNode == null || !UserBaseCurrent.Instance.Admin)
                return;

            switch ((DEnum.Dictionaries) tlDictionary.FocusedNode.Id)
            {
                case DEnum.Dictionaries.���������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroWorkType diAwt = new DictionaryAgroWorkType())
                        {
                            diAwt.DeleteSelectedFromGrid(gvWorks);
                        }
                    }
                    else
                        new WorkType(gvWorks, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.����������������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroWorkTypeGroup di_awtg = new DictionaryAgroWorkTypeGroup())
                        {
                            di_awtg.DeleteSelectedFromGrid(gvGWorks);
                        }
                    }
                    else
                        new WorkTypeGroup(gvGWorks, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.�����������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroFieldGrp di_afg = new DictionaryAgroFieldGrp())
                        {
                            di_afg.DeleteSelectedFromGrid(gvGFields);
                        }
                    }
                    else
                        new FieldGrp(gvGFields, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.�����������������������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroUnit di_au = new DictionaryAgroUnit())
                        {
                            di_au.DeleteSelectedFromGrid(gvUnit);
                        }
                    }
                    else
                        new Unit(gvUnit, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroCulture di_ac = new DictionaryAgroCulture())
                        {
                            di_ac.DeleteSelectedFromGrid(gvCalt);
                        }
                    }
                    else
                        new Culture(gvCalt, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.���������������������������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroAgregatGrp di_aag = new DictionaryAgroAgregatGrp())
                        {
                            di_aag.DeleteSelectedFromGrid(gvGAgr);
                        }
                    }
                    else
                        new AgregatGrp(gvGAgr, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.��������������������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroAgregat di_aa = new DictionaryAgroAgregat())
                        {
                            di_aa.DeleteSelectedFromGrid(gvAgr);
                        }
                    }
                    else
                    //using (Form f = new Agregat(gvAgr, bNew))
                    //{
                    //    f.ShowDialog();
                    //}
                        new Agregat(gvAgr, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.����:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroField di_af = new DictionaryAgroField())
                        {
                            di_af.DeleteSelectedFromGrid(gvFields);
                        }
                    }
                    else
                        new Field(gvFields, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.�������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroCommand di_ac = new DictionaryAgroCommand())
                        {
                            di_ac.DeleteSelectedFromGrid(gvOrder);
                        }
                    }
                    else
                        new Command(gvOrder, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroPrice di_ap = new DictionaryAgroPrice())
                        {
                            di_ap.DeleteSelectedFromGrid(gvPrice);
                        }
                    }
                    else
                        new Price(gvPrice, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.���������������������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroPlanDtReason di_dtr = new DictionaryAgroPlanDtReason())
                        {
                            di_dtr.DeleteSelectedFromGrid(gvDtReasons);
                        }
                    }
                    else
                        new DtReasons(gvDtReasons, bNew).ShowDialog();
                    break;
                }
                case DEnum.Dictionaries.������:
                {
                    if (bDelete)
                    {
                        using (DictionaryAgroSeason di_season = new DictionaryAgroSeason())
                        {
                            di_season.DeleteSelectedFromGrid(gvSeasons);
                        }
                    }
                    else
                        new Season(gvSeasons, bNew).ShowDialog();
                    break;
                }
            }
        }

        private void ExcelImport()
        {
            if (tlDictionary.FocusedNode == null)
                return;

            switch ((DEnum.Dictionaries) tlDictionary.FocusedNode.Id)
            {
                case DEnum.Dictionaries.����������������:
                {
                    using (var dawg = new DictionaryAgroWorkTypeGroup())
                    {
                        dawg.ChangeStatusEvent += SetStatus;
                        dawg.ChangeProgressBar += SetProgress;
                        dawg.ExcelRead();
                        LoadDictionary();
                        dawg.ChangeStatusEvent -= SetStatus;
                        dawg.ChangeProgressBar -= SetProgress;
                        SetStatus(Resources.Ready);

                    }
                    break;
                }
                case DEnum.Dictionaries.���������:
                {
                    break;
                }
                case DEnum.Dictionaries.�����������:
                {
                    break;
                }
                case DEnum.Dictionaries.����:
                {
                    break;
                }
                case DEnum.Dictionaries.�����������������������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������������������:
                {
                    using (var daa = new DictionaryAgroAgregat())
                    {
                        daa.ChangeStatusEvent += SetStatus;
                        daa.ChangeProgressBar += SetProgress;
                        daa.ExcelRead();
                        LoadDictionary();
                        daa.ChangeStatusEvent -= SetStatus;
                        daa.ChangeProgressBar -= SetProgress;
                        SetStatus(Resources.Ready);

                    }
                    break;
                }
                case DEnum.Dictionaries.�������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    break;
                }
                case DEnum.Dictionaries.������:
                {
                    using (var das = new DictionaryAgroSeason())
                    {
                        das.ChangeStatusEvent += SetStatus;
                        das.ChangeProgressBar += SetProgress;
                        das.ExcelRead();
                        LoadDictionary();
                        das.ChangeStatusEvent -= SetStatus;
                        das.ChangeProgressBar -= SetProgress;
                        SetStatus(Resources.Ready);
                    }
                    break;
                }
            }

        }

        private void XmlExport()
        {
            if (tlDictionary.FocusedNode == null)
                return;

            switch ((DEnum.Dictionaries) tlDictionary.FocusedNode.Id)
            {
                case DEnum.Dictionaries.����������������:
                {
                    break;
                }
                case DEnum.Dictionaries.���������:
                {
                    using (var dwt = new DictionaryAgroWorkType())
                    {
                        dwt.WriteToXml();
                    }
                    break;
                }
                case DEnum.Dictionaries.�����������:
                {
                    break;
                }
                case DEnum.Dictionaries.����:
                {
                    break;
                }
                case DEnum.Dictionaries.�����������������������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������������������:
                {
                    using (var daa = new DictionaryAgroAgregat())
                    {
                        daa.WriteToXml();
                    }
                    break;
                }
                case DEnum.Dictionaries.�������:
                {
                    break;
                }
                case DEnum.Dictionaries.��������:
                {
                    break;
                }
                case DEnum.Dictionaries.������:
                {
                    break;
                }
            }

        }

        #region GRID

        private void gvFields_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvWorks_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvCalt_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvAgr_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvOrder_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvPrice_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvUnit_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvGFields_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvGAgr_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvGWorks_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvDtReasons_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        private void gvSeasons_DoubleClick(object sender, EventArgs e)
        {
            ViewFormDictionary(false, false);
        }

        #endregion


        #region ������

        /// <summary>
        /// ������ ������� �� ������
        /// </summary>
        private void LoadOrders()
        {
            //OrderItem oi = new OrderItem();
            int numberOrder = 0;
            DataTable dtOrder = null;
            if (Int32.TryParse((beiNumber.EditValue == null ? "" : beiNumber.EditValue.ToString()), out numberOrder) &&
                numberOrder > 0)
            {
                dtOrder = OrderLists.GetListMain(numberOrder, (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue,
                    0, 0);
            }
            else
            {
                int idTeam = 0;
                int idMobitel = 0;
                if (bleGroupe.EditValue != null)
                    idTeam = (int) bleGroupe.EditValue;
                if (bleMobitelGrn.EditValue != null)
                    idMobitel = (int) bleMobitelGrn.EditValue;
                dtOrder = OrderLists.GetListMain(0, (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue, idTeam,
                    idMobitel);
            }
            if (dtOrder.Rows.Count > 0)
            {
                FilterOrdersVehicles(ref dtOrder);
                gcOrder.DataSource = dtOrder;
                using (DriverDb driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    leMobitelOrder.DataSource = driverDb.GetDataTable(AgroQuery.MainAgro.SelectVehicleMobitel);
                }
                LoadFields(numberOrder);
                sbtExcel.Enabled = true;
            }
            else
            {
                gcOrder.DataSource = null;
                sbtExcel.Enabled = false;
                gcFields.DataSource = null;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    _map.DrawZone(0, DateTime.MinValue, DateTime.MinValue);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    DateTime mintime = new DateTime(1753, 1, 1);
                    _map.DrawZone(0, mintime, mintime);
                }
            }
            SetControls();
            //XtraGridService.LayOutRead(bgvOrderContent,Consts.APP_DATA_AGRO, sFileXmlContent);
        }

        private void FilterOrdersVehicles(ref DataTable dtOrder)
        {
            if (bleGroupe.EditValue != null)
                return;
            OrderFilterVehicles.SetFilter(ref dtOrder);
        }

        private void LoadOrdersContent()
        {
            //OrderItem oi = new OrderItem();
            int numberOrder = 0;
            DataTable dtContent = null;
            if (Int32.TryParse((beiNumber.EditValue == null ? "" : beiNumber.EditValue.ToString()), out numberOrder) &&
                numberOrder > 0)
            {
                dtContent = OrderLists.GetListContent(numberOrder, (DateTime) bdeStart.EditValue,
                    (DateTime) bdeEnd.EditValue, 0, 0);
            }
            else
            {
                int idTeam = 0;
                int idMobitel = 0;
                if (bleGroupe.EditValue != null)
                    idTeam = (int) bleGroupe.EditValue;
                if (bleMobitelGrn.EditValue != null)
                    idMobitel = (int) bleMobitelGrn.EditValue;
                dtContent = OrderLists.GetListContent(0, (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue,
                    idTeam, idMobitel);
            }

            int p = 0;
            if (dtContent.Rows.Count > 0)
            {
                FilterOrdersVehicles(ref dtContent);
                gcOrderContent.DataSource = dtContent;
                sbtExcel.Enabled = true;
            }
            else
            {
                gcOrderContent.DataSource = null;
                sbtExcel.Enabled = false;
            }
            XtraGridService.LayOutRead(bgvOrderContent, Consts.APP_DATA_AGRO, sFileXmlContent);
        }

        /// <summary>
        /// �������� ������ ����� � ����������� �������
        /// </summary>
        private void LoadFields(int idOrder)
        {
            int idTeam = 0;
            int idMobitel = 0;
            if (bleGroupe.EditValue != null)
                idTeam = (int) bleGroupe.EditValue;
            if (bleMobitelGrn.EditValue != null)
                idMobitel = (int) bleMobitelGrn.EditValue;
            DataTable dtMain = OrderLists.GetListFields(idOrder, (DateTime) bdeStart.EditValue,
                (DateTime) bdeEnd.EditValue,
                idTeam, idMobitel);
            gcFields.DataSource = dtMain;
            //������ �� ������ ���� ����������� �� �����
            if (dtMain.Rows.Count > 0)
            {
                //_map.SetFieldName((gvFieldsOrder.GetRowCellValue(0, "FieldName") ?? "").ToString());
                _map.DrawZone(Convert.ToInt32(gvFieldsOrder.GetRowCellValue(0, "Zone_ID")),
                    (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue);
            }
            else
            {
                _map.DrawZone(0, DateTime.MinValue, DateTime.MinValue);
            }
        }

        /// <summary>
        /// ������� �� �����
        /// </summary>
        private void gcOrder_DoubleClick(object sender, EventArgs e)
        {
            ViewOrder();
        }

        private void gcOrderContent_DoubleClick(object sender, EventArgs e)
        {
            if (bgvOrderContent.FocusedRowHandle >= 0)
                ViewOrder();
        }

        /// <summary>
        /// �������� �������� �������
        /// </summary>
        private void ViewDoc()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                {
                    if (tlDictionary.FocusedNode == null)
                    {
                        XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                        return;
                    }
                    ViewFormDictionary(false, false);
                    break;
                }
                case 1:
                {
                    ViewOrder();
                    break;
                }
                case 5:
                {
                    _planSetView.ViewPlanFromMainGrid();
                    break;
                }
                case 6:
                {
                    _fieldsTcSetView.ViewDocumentFromGrid();
                    break;
                }
            }
        }

        private void ViewOrder()
        {
            int idOrder = 0;
            switch (xtbOrder.SelectedTabPageIndex)
            {
                case 0:
                {

                    if (!bgvOrder.IsValidRowHandle(bgvOrder.FocusedRowHandle))
                    {
                        XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                        return;
                    }
                    Int32.TryParse(bgvOrder.GetRowCellValue(bgvOrder.FocusedRowHandle, "Id").ToString(), out idOrder);
                    break;
                }
                case 1:
                {
                    if (!bgvOrderContent.IsValidRowHandle(bgvOrderContent.FocusedRowHandle))
                    {
                        XtraMessageBox.Show(Resources.SelectValueForEdit, Resources.ApplicationName);
                        return;
                    }
                    Int32.TryParse(bgvOrderContent.GetRowCellValue(bgvOrderContent.FocusedRowHandle, "Id").ToString(),
                        out idOrder);
                    break;
                }

            }
            if (idOrder > 0) AgroController.Instance.ViewOrder(idOrder);
        }

        private void gvFieldsOrder_FocusedRowChanged(object sender,
            DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ViewOrderOnMap();
        }

        private void gcFields_Click(object sender, EventArgs e)
        {
            ViewOrderOnMap();
        }

        /// <summary>
        /// �������� ������ �� ���������� �����
        /// </summary>
        private void ViewOrderOnMap()
        {
            if (gvFieldsOrder.GetRowCellValue(gvFieldsOrder.FocusedRowHandle, "Zone_ID") == null)
                return;
            int numberOrder = 0;
            DataTable dtOrder = null;
            if (Int32.TryParse((beiNumber.EditValue == null ? "" : beiNumber.EditValue.ToString()), out numberOrder) &&
                numberOrder > 0)
            {
                _map.DrawZone(Convert.ToInt32(gvFieldsOrder.GetRowCellValue(gvFieldsOrder.FocusedRowHandle, "Zone_ID")),
                    (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue, numberOrder);
            }
            else
            {
                _map.DrawZone(Convert.ToInt32(gvFieldsOrder.GetRowCellValue(gvFieldsOrder.FocusedRowHandle, "Zone_ID")),
                    (DateTime) bdeStart.EditValue, (DateTime) bdeEnd.EditValue);
            }

        }

        private void MainAgro_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (GetStatus() != Resources.Ready)
            {
                e.Cancel = true;
                return;
            }
        }

        private void bleGroupe_EditValueChanged(object sender, EventArgs e)
        {
            SetLeMobitel();
        }

        private void bleCategory_EditValueChanged(object sender, EventArgs e)
        {
            SetLeMobitel();
        }

        private void SetLeMobitel()
        {
            VehiclesGroup groupe = (VehiclesGroup) leGroupe.GetDataSourceRowByKeyValue(bleGroupe.EditValue);
            if (groupe == null)
            {
                leMobitelGrn.DataSource = null;
            }
            else
            {
                VehicleCategory category =
                    (VehicleCategory) leCategory.GetDataSourceRowByKeyValue(bleCategory.EditValue);
                if (category == null)
                {
                    leMobitelGrn.DataSource = groupe.OwnRealItems();
                }
                else
                {
                    leMobitelGrn.DataSource = groupe.OwnRealItemsWithCategory(category.Id);
                }
            }
        }

        private void ViewOrderFromControl(int Id_order)
        {
            int Row_Find = bgvOrder.LocateByValue(0, bgvOrder.Columns["Id"], Id_order);
            if (Row_Find >= 0)
            {
                bgvOrder.FocusedRowHandle = Row_Find;
                if (bgvOrder.IsValidRowHandle(bgvOrder.FocusedRowHandle))
                    ViewDoc();
            }
        }

        public void LockMenuBar(bool bLock)
        {
            sbtDelete.Enabled = !bLock;
            sbtEdit.Enabled = !bLock;
            sbtRefresh.Enabled = !bLock;
            bsiFact.Enabled = !bLock;
            sbtExcel.Enabled = !bLock;
            bsiSettings.Enabled = !bLock;
            btSettings.Enabled = !bLock;
            btOrdersCreate.Enabled = !bLock;
            if (!bLock)
            {
                SetProgress(Consts.PROGRESS_BAR_STOP);
                SetStatus(Resources.Ready);
            }
        }

        #region ������������ �������

        /// <summary>
        /// ���������� ����������� ������������ ������� �� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chTimeCreate_EditValueChanged(object sender, EventArgs e)
        {
            if (bleGroupe.EditValue == null)
            {
                XtraMessageBox.Show(Resources.CarGroupeSelect, Resources.ApplicationName);
                return;
            }
            if (chTimeCreate.EditValue.ToString() == "True")
            {
                teTimeCreate.Enabled = true;
                tmCreateOrders.Enabled = true;
                bCreateOrderDay = true;
            }
            else
            {
                teTimeCreate.Enabled = false;
                tmCreateOrders.Enabled = false;
                bCreateOrderDay = false;
            }
        }

        /// <summary>
        /// ������������ ������� �� �������
        /// </summary>
        private void tmCreateOrders_Tick(object sender, EventArgs e)
        {
            if ((chTimeCreate.EditValue == null) || (chTimeCreate.EditValue.ToString() != "True"))
                return;
            if ((teTimeCreate.EditValue == null) || (teTimeCreate.EditValue.ToString().Length < 5))
            {
                chTimeCreate.EditValue = 0;
                return;
            }
            int iHoure = 0;
            int iMinute = 0;
            if (Int32.TryParse(teTimeCreate.EditValue.ToString().Substring(0, 2), out iHoure) &&
                Int32.TryParse(teTimeCreate.EditValue.ToString().Substring(3, 2), out iMinute))
            {

                if (iHoure == 0)
                    bCreateOrderDay = true;
                if ((iHoure == DateTime.Now.Hour) && (iMinute == DateTime.Now.Minute) && bCreateOrderDay)
                {
                    OrderCreate(false);
                    bCreateOrderDay = false;
                }
            }
        }

        /// <summary>
        /// ������������ ������� �� ������
        /// </summary>
        private void btOrdersCreate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OrderCreate();
        }

        /// <summary>
        /// ������������ �������
        /// </summary>
        private void OrderCreate(bool viewQuestion = true)
        {
            if (bleGroupe.EditValue == null)
            {
                XtraMessageBox.Show(Resources.CarGroupeSelect, Resources.ApplicationName);
                return;
            }
            LockMenuBar(true);
            bool bReloadGrn = false;
            string MessAboutExistingOrders = "";
            SetStatus(Resources.CarSelection);
            DateTime orderDate = TUtil.GetOrderDate((DateTime) bdeStart.EditValue);
            int ordersDays = (int) ((DateTime) bdeEnd.EditValue).Subtract((DateTime) bdeStart.EditValue).TotalDays;
            IList<Vehicle> vehicles = new List<Vehicle>();
            if (bleMobitelGrn.EditValue == null)
            {
                VehiclesGroup groupeVehicles = (VehiclesGroup) leGroupe.GetDataSourceRowByKeyValue(bleGroupe.EditValue);
                VehicleCategory category =
                    (VehicleCategory) leCategory.GetDataSourceRowByKeyValue(bleCategory.EditValue);
                if (viewQuestion)
                {
                    string textQuestion = "";
                    if (category == null)
                        textQuestion = String.Format(Resources.OrderCreateConfirmGroupe, groupeVehicles.Name, orderDate,
                            orderDate.AddDays(ordersDays + 1).AddMinutes(-1), Environment.NewLine);
                    else
                        textQuestion = String.Format(Resources.OrderCreateConfirmGroupeCategory, groupeVehicles.Name,
                            orderDate, orderDate.AddDays(ordersDays + 1).AddMinutes(-1), Environment.NewLine,
                            category.Name);
                    if (DialogResult.No ==
                        XtraMessageBox.Show(textQuestion, Resources.ApplicationName, MessageBoxButtons.YesNo))
                    {
                        LockMenuBar(false);
                        return;
                    }
                }


                if (category == null)
                {
                    vehicles = groupeVehicles.OwnRealItems();
                }
                else
                {
                    vehicles = groupeVehicles.OwnRealItemsWithCategory(category.Id);
                }
            }
            else
            {
                var vehicle = (Vehicle) leMobitelGrn.GetDataSourceRowByKeyValue(bleMobitelGrn.EditValue);
                if (vehicle != null)
                {
                    if (viewQuestion)
                    {
                        if (DialogResult.No ==
                            XtraMessageBox.Show(
                                String.Format(Resources.OrderCreateConfirmCar, vehicle.Info, orderDate,
                                              orderDate.AddDays(ordersDays + 1).AddMinutes(-1), Environment.NewLine),
                                Resources.ApplicationName, MessageBoxButtons.YesNo))
                        {
                            LockMenuBar(false);
                            return;
                        }
                    }

                    vehicles.Add(vehicle);
                }
            }

            if (vehicles.Count == 0)
            {
                XtraMessageBox.Show(Resources.CarSelectionNItems, Resources.ApplicationName);
                LockMenuBar(false);
                return;
            }
            pbStatus.Visibility = BarItemVisibility.Always;
            pbStatus.EditValue = 0;
            SetStatus(Resources.OrdersCreating);
            rpbStatus.Maximum =
                (int)
                    (vehicles.Count*
                     (((DateTime) bdeEnd.EditValue).Subtract((DateTime) bdeStart.EditValue).TotalDays + 1));
            int cnt = 0;
            bool BreakLoop = false;
            for (int i = 0; i <= ordersDays; i++)
            {
                if (BreakLoop)
                    break;
                for (int m = 0; m < vehicles.Count; m++)
                {
                    pbStatus.EditValue = cnt++;
                    using (var oi = new OrderItem(orderDate.AddDays(i), vehicles[m].Mobitel.Id))
                    {
                        if (!oi.TestDemo())
                        {
                            BreakLoop = true;
                            break;
                        }
                        oi.ChangeStatusEvent += SetStatus;
                        if (!oi.IsExist(ref MessAboutExistingOrders))
                        {
                            if (oi.ContentAutoCreate(false))
                                bReloadGrn = true;
                            Application.DoEvents();
                        }
                        else
                            SetStatus(Resources.OrderExist + " " + oi.Date + " " + vehicles[m].Info);

                        oi.ChangeStatusEvent -= SetStatus;
                    }
                }

            }

            if (bReloadGrn)
            {
                if (MessAboutExistingOrders.Length > 0)
                {
                    MessAboutExistingOrders = string.Format("{0}\n{1}", Resources.OrderExist, MessAboutExistingOrders);
                    XtraMessageBox.Show(MessAboutExistingOrders, Resources.ApplicationName);
                }
                LoadOrders();
            }
            else
            {
                if (MessAboutExistingOrders.Length > 0)
                {
                    MessAboutExistingOrders = string.Format("{0}\n{1}\n{2}", Resources.OrderNCreate,
                        Resources.OrderExist, MessAboutExistingOrders);
                    XtraMessageBox.Show(MessAboutExistingOrders, Resources.ApplicationName);
                }
                else
                    XtraMessageBox.Show(Resources.OrderNCreate, Resources.ApplicationName);
            }

            LockMenuBar(false);
            Application.DoEvents();
        }

        #endregion

        private void bgvOrder_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void bgvOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                ViewOrder();
            }
        }

        #endregion

        #region ���������� ���������

        public void SetStatus(string sMessage)
        {
            lbStatus.Caption = sMessage;
            Application.DoEvents();
        }

        private string GetStatus()
        {
            return (lbStatus.Caption);
        }

        public void SetProgress(int iValue)
        {
            TotUtilites.SetProgressBar(pbStatus, iValue);
        }

        public void SetButton(bool visible)
        {
            TotUtilites.SetButtonExecuting(btnStopExecutingData, visible);
        }

        private void btnStopExecutingData_ItemClick(object sender, ItemClickEventArgs e)
        {
            CallBacks.ButtonStopExec();
        }

        private void SetControls()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0: //�����������
                {
                    var gv = (GridView) gcDict.FocusedView;
                    if (gv.RowCount == 0 || !UserBaseCurrent.Instance.Admin)
                    {
                        sbtDelete.Enabled = false;
                        sbtEdit.Enabled = false;
                    }
                    else
                    {
                        sbtDelete.Enabled = true;
                        sbtEdit.Enabled = true;
                    }
                    if (UserBaseCurrent.Instance.Admin)
                        sbtAdd.Enabled = true;
                    sbtExcel.Enabled = false;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    //btExcelImport.Enabled = true;
                    break;
                }
                case 1: //������
                default:
                {
                    sbtDelete.Enabled = true;
                    sbtAdd.Enabled = false;
                    sbtEdit.Enabled = true;
                    bsiSettings.Enabled = true;
                    if (bgvOrder.RowCount == 0)
                    {
                        sbtExcel.Enabled = false;
                        bsiFact.Enabled = false;
                    }
                    else
                    {
                        sbtExcel.Enabled = true;
                        bsiFact.Enabled = true;
                    }
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case 2: //������ 
                {
                    if (_reportsController == null)
                    {
                        _reportsController = new RepController(nbReports, nbgReports, splitContainerControl4.Panel2);
                        //_reportsController.ChangeActiveReport += onChangeActiveReport;
                        _reportsController.InitReportsList();
                    }

                    sbtDelete.Enabled = false;
                    sbtAdd.Enabled = false;
                    sbtEdit.Enabled = false;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    if (_reportsController.IsReportHasData)
                    {
                        sbtExcel.Enabled = true;
                    }
                    else
                    {
                        sbtExcel.Enabled = false;
                    }
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case 3: //������� ���������
                {
                    sbtDelete.Enabled = false;
                    sbtAdd.Enabled = false;
                    sbtEdit.Enabled = false;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    sbtExcel.Enabled = true;
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case 4: //������� ���������
                {
                    sbtDelete.Enabled = false;
                    sbtAdd.Enabled = false;
                    sbtEdit.Enabled = false;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    sbtExcel.Enabled = true;
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case 5: //�������� �������
                {
                    sbtDelete.Enabled = true;
                    sbtAdd.Enabled = true;
                    sbtEdit.Enabled = true;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    sbtExcel.Enabled = true;
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
                case 6: //��������������� ����� ����� 
                {
                    sbtDelete.Enabled = true;
                    sbtAdd.Enabled = true;
                    sbtEdit.Enabled = true;
                    bsiSettings.Enabled = false;
                    bsiFact.Enabled = false;
                    sbtExcel.Enabled = true;
                    btExcelImport.Enabled = false;
                    btXmlExport.Enabled = false;
                    break;
                }
            }
        }

        private void LoadTabGrid()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                {
                    LoadDictionary();
                    break;
                }
                case 1:
                {
                    LoadOrdesMainOrDetal();
                    break;
                }
                case 2:
                {
                    //ReportDrivers();
                    break;
                }
                case 3:
                {
                    if (_gsAgr == null)
                    {
                        _gsAgr = new GraficSensor((int) AlgorithmType.AGREGAT);
                        pnGraficAgr.Controls.Add(_gsAgr);
                        _gsAgr.Dock = DockStyle.Fill;
                    }
                    break;
                }
                case 4:
                {
                    if (_gsDrv == null)
                    {
                        _gsDrv = new GraficSensor((int) AlgorithmType.DRIVER);
                        pnGraficDrv.Controls.Add(_gsDrv);
                        _gsDrv.Dock = DockStyle.Fill;
                    }
                    break;
                }
                case 5:
                {
                    if (_planSetView == null)
                    {
                        _planSetView = new PlanSetView();
                        pnPlan.Controls.Add(_planSetView);
                        _planSetView.Dock = DockStyle.Fill;
                    }
                    _planSetView.RefreshData();
                    break;
                }
                case 6:
                {
                    if (_fieldsTcSetView == null)
                    {
                        _fieldsTcSetView = new FieldsTcSetView();
                        pnFieldsTc.Controls.Add(_fieldsTcSetView);
                        _fieldsTcSetView.Dock = DockStyle.Fill;
                    }
                    _fieldsTcSetView.RefreshData();
                    break;
                }
            }
            SetControls();
        }

        private void xtbView_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            SetControls();
            LoadTabGrid();
        }

        private void bdeEnd_EditValueChanged(object sender, EventArgs e)
        {
            _cdeOrders.UpdateInterval(bdeStart, bdeEnd, true);
        }

        private void bdeStart_EditValueChanged(object sender, EventArgs e)
        {
            _cdeOrders.UpdateInterval(bdeStart, bdeEnd, false);
        }

        private void bdeStartReport_EditValueChanged(object sender, EventArgs e)
        {
            _cdeDrivers.UpdateInterval(bdeStartReport, bdeEndReport, false);
        }

        private void bdeEndReport_EditValueChanged(object sender, EventArgs e)
        {
            _cdeDrivers.UpdateInterval(bdeStartReport, bdeEndReport, true);
        }

        #endregion

        #region ������

        private void btStartReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            RunReport();
        }

        private void RunReport()
        {
            _reportsController.RunReport((DateTime) bdeStartReport.EditValue, (DateTime) bdeEndReport.EditValue,
                pbStatus, lbStatus);
            if (_reportsController.IsReportHasData)
            {
                sbtExcel.Enabled = true;
            }
            else
            {
                sbtExcel.Enabled = false;
            }
        }


        #endregion

        #region �����������

        private void Localization()
        {

            Text = String.Format("TrackControl - AGRO {0} � Server: {1}, DB: {2}, {3}: {4}",
                Application.ProductVersion,
                UserBaseCurrent.Instance.Server,
                UserBaseCurrent.Instance.Database,
                Resources.User,
                UserBaseCurrent.Instance.Name);

            gridBand5.Caption = Resources.AgregatComponent;
            gridBand9.Caption = Resources.WorkResult;
            gridBand17.Caption = Resources.ParametersTime;
            gridBand10.Caption = Resources.ParametersTime;
            gridBand16.Caption = Resources.ParametersMoving;
            gridBand11.Caption = Resources.ParametersMoving;
            gridBand18.Caption = Resources.FuelDuut;
            gridBand12.Caption = Resources.FuelDuut;

            colMakeCar.Caption = Resources.colMakeCar;
            colMakeCar.ToolTip = Resources.colMakeCar;
            colCarModel.Caption = Resources.colCarModel;
            colCarModel.ToolTip = Resources.colCarModel;
            colNumberPlate.Caption = Resources.colNumberPlate;
            colNumberPlate.ToolTip = Resources.colNumberPlate;
            colInvNumber.Caption = Resources.colInvNumber;
            colInvNumber.ToolTip = Resources.colInvNumber;
            colAgrName.Caption = Resources.colAgrName;
            colAgrName.ToolTip = Resources.colAgrName;
            colFamily.Caption = Resources.colFamily;
            colFamily.ToolTip = Resources.colFamily;

            colFieldName.Caption = Resources.colFieldName;
            colFieldName.ToolTip = Resources.colFieldName;
            colFieldGroupName.Caption = Resources.fieldGroupName;
            colFieldGroupName.ToolTip = Resources.fieldGroupName;
            colOutLinkId.Caption = Resources.colOutLinkId;
            colOutLinkId.ToolTip = Resources.colOutLinkId;
            colWorkName.Caption = Resources.colWorkName;
            colWorkName.ToolTip = Resources.colWorkName;
            colFactSquareCalcContent.Caption = Resources.SquareContourGa;
            colFactSquareCalcContent.ToolTip = Resources.SquareContourGa;
            colPersSquare.Caption = Resources.colPersSquare;
            colPersSquare.ToolTip = Resources.colPersSquare;
            colIdOrder.Caption = Resources.colIdOrder;
            colIdOrder.ToolTip = Resources.colIdOrder;
            colComment.Caption = Resources.colComment;
            colComment.ToolTip = Resources.colComment;

            colTimeStart.Caption = Resources.colTimeStart;
            colTimeStart.ToolTip = Resources.colTimeStart;
            colTimeEnd.Caption = Resources.colTimeEnd;
            colTimeEnd.ToolTip = Resources.colTimeEnd;
            colTimeWork.Caption = Resources.colTimeWork;
            colTimeWork.ToolTip = Resources.colTimeWork;
            colTimeMove.Caption = Resources.colTimeMove;
            colTimeMove.ToolTip = Resources.colTimeMove;
            colTimeRate.Caption = Resources.colTimeRate;
            colTimeRate.ToolTip = Resources.colTimeRate;

            colDistance.Caption = Resources.colDistance;
            colDistance.ToolTip = Resources.colDistance;
            colSpeedAvg.Caption = Resources.colSpeedAvg;
            colSpeedAvg.ToolTip = Resources.colSpeedAvg;

            colFuelExpens.Caption = Resources.colFuelExpens;
            colFuelExpens.ToolTip = Resources.colFuelExpens;
            colFuelExpensAvg.Caption = Resources.colFuelExpensAvg;
            colFuelExpensAvg.ToolTip = Resources.colFuelExpensAvg;
            colFuelExpensRun.Caption = Resources.colFuelExpensRun;
            colFuelExpensRun.ToolTip = Resources.colFuelExpensRun;
            colFuelExpensRunKm.Caption = Resources.colFuelExpensRunKm;
            colFuelExpensRunKm.ToolTip = Resources.colFuelExpensRunKm;

            bleCategory.Caption = Resources.Category;
            bcolCategory.Caption = Resources.Category;
            xtraTabPage7.Text = Resources.nameTabPage7;
            xtraTabPage6.Text = Resources.planTask;
            xtbpOrderMain.Text = Resources.MainLog;
            xtbpOrderDetal.Text = Resources.DetailedLog;
            Name_dtr.Caption = Resources.NameRes;
            Comment_dtr.Caption = Resources.Remark;
            Name_gw.Caption = Resources.NameRes;
            Comment_gw.Caption = Resources.Remark;
            SpeedControl.Caption = Resources.SpeedControl;
            DepthControl.Caption = Resources.DepthControl;
            Name_gf.Caption = Resources.NameRes;
            Comment_gf.Caption = Resources.Remark;
            FGROUPE.Caption = Resources.FieldsGroup;
            leFieldGrope.Columns[0].Caption = Resources.NameRes;
            FZONE.Caption = Resources.CheckZoneName;
            leZone.Columns[0].Caption = Resources.NameRes;
            Name_f.Caption = Resources.NameRes;
            Square.Caption = Resources.SquareGA;
            Comment_f.Caption = Resources.Remark;
            Name_c.Caption = Resources.NameRes;
            Comment_c.Caption = Resources.Remark;
            GroupeAgr.Caption = Resources.Groupe;
            leGroupeAgr.Columns[0].Caption = Resources.NameRes;
            Name_a.Caption = Resources.NameRes;
            InvNumber.Caption = Resources.NumberInv;
            Width_a.Caption = Resources.WidthM;
            Identifier.Caption = Resources.Identifier;
            Def.Caption = Resources.Default;
            Comment_a.Caption = Resources.Remark;
            Id_work_agr.Caption = Resources.WorkType;
            leWork.Columns[0].Caption = Resources.NameRes;
            DateInit.Caption = Resources.Date;
            DateComand.Caption = Resources.DateInput;
            Name_o.Caption = Resources.NameRes;
            Comment.Caption = Resources.Remark;
            DateComand_p.Caption = Resources.DateOrder;
            leOrder.Columns[0].Caption = Resources.DateOrder;
            WName.Caption = Resources.WorkType;
            MName.Caption = Resources.Car;
            leMobitel.Columns[0].Caption = Resources.NameRes;
            AName.Caption = Resources.Agregat;
            leAgregat.Columns[0].Caption = Resources.NameRes;
            Price.Caption = Resources.Price;
            UName.Caption = Resources.Unit;
            leUnit.Columns[0].Caption = Resources.NameRes;
            Comment_p.Caption = Resources.Remark;
            Name_u.Caption = Resources.NameRes;
            NameShort_u.Caption = Resources.NameBrief;
            Factor.Caption = Resources.Coefficient;
            Comment_u.Caption = Resources.Remark;
            Name_gagr.Caption = Resources.NameRes;
            Remark_gagr.Caption = Resources.Remark;
            Work_gagr.Caption = Resources.WorkType;
            Name_work.Caption = Resources.NameRes;
            SpeedBottom.Caption = Resources.SpeedFromKmH;
            SpeedTop.Caption = Resources.SpeedToKmH;
            Comment_work.Caption = Resources.Remark;
            gridBand2.Caption = Resources.Field;
            Field.Caption = Resources.Field;
            Family.Caption = Resources.Driver;
            gridBand3.Caption = Resources.Time;
            TimeStart1.Caption = Resources.Entry;
            TimeEnd1.Caption = Resources.Exit;
            FactTime.Caption = Resources.TimeWorksH;
            TimeMove1.Caption = Resources.TimeMoveH;
            TimeStop.Caption = Resources.TimeStopH;
            TimeRate.Caption = Resources.MotoHourH;
            gridBand4.Caption = Resources.Work;
            DistanceM.Caption = Resources.Run;
            FactSquare.Caption = Resources.SquareRunGa;
            FactSquareCalc.Caption = Resources.SquareContourGa;
            SpeedAvgField.Caption = Resources.SpeedAvg;
            gridBand15.Caption = Resources.InformationTotal;
            ID.Caption = Resources.Number;
            DateGrn.Caption = Resources.Date;
            DateLastRecalc.Caption = Resources.DateLastOrderRecalc;
            Mobitel.Caption = Resources.Technics;
            leMobitelOrder.Columns[0].Caption = Resources.NameRes;
            SquareWorkDescript.Caption = Resources.ProceededFields;
            PathWithoutWork.Caption = Resources.MoveKm;
            CommentOrder.Caption = Resources.Comment;
            gridBand16.Caption = Resources.MovingParameters;
            LocationStart.Caption = Resources.PlaceStart;
            LocationEnd.Caption = Resources.PlaceEnd;
            Distance.Caption = Resources.PathKm;
            SpeedAvg.Caption = Resources.SpeedAvgKmH;
            gridBand17.Caption = Resources.TimeParameters;
            TimeWork.Caption = Resources.DurationRotation;
            TimeStart.Caption = Resources.TimeStart;
            TimeEnd.Caption = Resources.TimeEnd;
            TimeMove.Caption = Resources.TimeTotalH;
            gridBand18.Caption = Resources.FuelDUT;
            FuelDUTExpensSquare.Caption = Resources.FuelExpenseFieldTotal;
            FuelDUTExpensAvgSquare.Caption = Resources.FuelExpenseFieldLGa;
            FuelDUTExpensWithoutWork.Caption = Resources.FuelExpenseRunTotal;
            FuelDUTExpensAvgWithoutWork.Caption = Resources.FuelExpenseRunL100Km;
            gridBand19.Caption = Resources.FuelDRT;

            FuelDRTExpensSquare.Caption = Resources.FuelExpenseFieldTotal;
            FuelDRTExpensAvgSquare.Caption = Resources.FuelExpenseFieldLGa;
            FuelDRTExpensWithoutWork.Caption = Resources.FuelExpenseRunTotal;
            FuelDRTExpensAvgWithoutWork.Caption = Resources.FuelExpenseRunL100Km;

            FuelDRTExpensSquare_.Caption = Resources.FuelDRTExpensSquare;
            FuelDRTExpensAvgSquare_.Caption = Resources.FuelDRTExpensAvgSquare;
            FuelDRTExpensWithoutWork_.Caption = Resources.FuelDRTExpensWithoutWork;
            FuelDRTExpensAvgWithoutWork_.Caption = Resources.FuelDRTExpensAvgWithoutWork;
            FuelDRTExpensAvgRate_.Caption = Resources.FuelDRTExpensAvgRate;

            gridBand6.Caption = Resources.Field;
            FName.Caption = Resources.Field;
            FamilyF.Caption = Resources.Driver;
            gridBand7.Caption = Resources.SensorDUT;
            FuelStart.Caption = Resources.FuelStartL;
            FuelAdd.Caption = Resources.FuelAddL;
            FuelSub.Caption = Resources.FuelSubL;
            FuelEnd.Caption = Resources.FuelEndL;
            FuelExpens.Caption = Resources.FuelExpenseTotalL;
            FuelExpensAvg.Caption = Resources.FuelExpenseAvgLGa;
            gridBand8.Caption = Resources.SensorDRT_CAN;
            Fuel_ExpensMove.Caption = Resources.FuelExpenseMovingL;
            Fuel_ExpensStop.Caption = Resources.FuelExpenseStopL;
            Fuel_ExpensTotal.Caption = Resources.FuelExpenseTotalL;
            Fuel_ExpensAvg.Caption = Resources.FuelExpenseAvgLGa;
            Fuel_ExpensAvgRate.Caption = Resources.FuelExpenseAvgLMHour;
            sbtAdd.Caption = Resources.Add;
            sbtDelete.Caption = Resources.Delete;
            sbtEdit.Caption = Resources.Edit;
            sbtRefresh.Caption = Resources.Refresh;
            bsiFact.Caption = Resources.Fact;
            btFactControl.Caption = Resources.DataControlTaking;
            btFact.Caption = Resources.DataControlNTaking;
            sbtExcel.Caption = Resources.ExcelExport;
            btExcelImport.Caption = Resources.ExcelImport;
            bsiSettings.Caption = Resources.GournalSettings;

            btColumnsSet.Caption = Resources.ColumnsSetting;
            btColumnsSave.Caption = Resources.Save;
            btColumnRestore.Caption = Resources.Restore;

            btSettings.Caption = Resources.Settings;
            lbStatus.Caption = Resources.Ready;
            bdeStart.Caption = Resources.DateFrom;
            bdeEnd.Caption = Resources.DateTo;
            bleGroupe.Caption = Resources.CarGroupe;
            leGroupe.Columns[0].Caption = Resources.NameRes;
            bleMobitelGrn.Caption = Resources.Car;
            leMobitelGrn.Columns[0].Caption = Resources.NameRes;
            btOrdersCreate.Caption = Resources.OrderAutoCreation;
            chTimeCreate.Caption = Resources.OnTimer;
            bdeStartReport.Caption = Resources.DateFrom;
            bdeEndReport.Caption = Resources.DateTo;
            xtraTabPage1.Text = Resources.Dictionaries;

            tlDictionary.Columns[0].Caption = Resources.Code;
            tlDictionary.Columns[1].Caption = Resources.DictionaryName;

            tlDictionary.Nodes[0][1] = Resources.WorkTypesGroups;
            tlDictionary.Nodes[0].Nodes[0][1] = Resources.WorkTypes;
            tlDictionary.Nodes[1][1] = Resources.AgregatGroups;
            tlDictionary.Nodes[1].Nodes[0][1] = Resources.Agregat;
            tlDictionary.Nodes[2][1] = Resources.FieldsGroups;
            tlDictionary.Nodes[2].Nodes[0][1] = Resources.Fields;
            tlDictionary.Nodes[2].Nodes[0].Nodes[0][1] = Resources.SquareUnits;
            tlDictionary.Nodes[3][1] = Resources.Cultures;
            tlDictionary.Nodes[4][1] = Resources.machineDownTime;
            tlDictionary.Nodes[5][1] = Resources.Seasons;

            //tlDictionary.Nodes[4][1] = Resources.CommandsPrices;
            //tlDictionary.Nodes[4].Nodes[0][1] = Resources.Prices;

            xtraTabPage2.Text = Resources.Orders;
            splitContainerControl3.Panel1.Text = Resources.FieldsList;
            GrpName.Caption = Resources.FieldsGroup;
            FieldName.Caption = Resources.Field;
            SquareF.Caption = Resources.SquareGA;
            ORDERS.Caption = Resources.OrdersForInterval;
            gcFieldsList.Text = Resources.FieldsProcessList;
            xtraTabPage3.Text = Resources.Reports;
            xtraTabPage4.Text = Resources.AgregatIdentification;
            xtraTabPage5.Text = Resources.DriverIdentification;

            btStart.Caption = Resources.PUSK;
            btStartReport.Caption = Resources.PUSK;

            FactSquareCol.Caption = Resources.SquareRunGa;
            colFactSquareCalc.Caption = Resources.SquareContourGa;
            colFactSquareCalcOverlap.Caption = Resources.SquareContourGaOverlap;

            gbQuontity.Caption = Resources.DataQuality;
            bcolValidity.Caption = Resources.DataValidity;
            bcolPointsIntervalMax.Caption = Resources.Max_Interval;

            colState.ToolTip = Resources.OrderState;
            bsiNumber.Caption = Resources.Number;

            colTypeWork.Caption = Resources.Type;
            colGroupWork.Caption = Resources.Groupe;

            btnStopExecutingData.Hint = Resources.HintButtonStopExecuting;
            btnStopExecutingData.Caption = "";

            btXmlExport.Caption = Resources.XMLExport;
        }

        #endregion

        private void MainAgro_FormClosed(object sender, FormClosedEventArgs e)
        {
            AgroController.Instance.Dispose();
        }

        private void btXmlExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            XmlExport();
        }
    }
}