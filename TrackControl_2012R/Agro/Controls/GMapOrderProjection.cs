﻿using Agro.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Zones;


namespace Agro.Controls
{
    public class GMapOrderProjection
    {
        public event VoidHandler RefreshData;

        readonly GoogleMapControl _mcGoogle;
        List<IGeoPoint> _orderPoints;
        Dictionary<int, Color> _zoneColors;
        Dictionary<int, AgregatForTrack> _agregatForTrack;
        int _idOrderTForSplit;
        OrderRecordSplitter _ors;
        bool _isDrawMarkers;
        bool _isDrawDgps;
        private readonly List<Marker> _markers;
        internal int MobitelId { get; set; } 
        internal DateTime DateOrder { get; set; } 
        

        public GMapOrderProjection(PanelControl pnGis)
        {
            _mcGoogle = new GoogleMapControl();
            pnGis.Controls.Add(_mcGoogle);
            _mcGoogle.Dock = DockStyle.Fill;
            _mcGoogle.MapType = MapType.YandexHybridMap;
            _orderPoints = new List<IGeoPoint>();
            _markers = new List<Marker>();
            GMapInitControl();
        }

        public void GMapInitControl()
        {
            _mcGoogle.State = new MapState(12, new PointLatLng(ConstsGen.KievLat,ConstsGen.KievLng));
            _orderPoints.Clear();
            _markers.Clear(); 
            _mcGoogle.ClearAll();

        }

        public Image Screenshot
        {
            get { return _mcGoogle.Screenshot; }
        }

        public void DrawOrder(Dictionary<int, AgregatForTrack> agregatForTrack, Dictionary<int, Color> zoneColors, bool isDrawMarkers, bool isDrawDgps = false)
        {
            _agregatForTrack = agregatForTrack;
            _zoneColors = zoneColors;
            _isDrawMarkers = isDrawMarkers;
            _isDrawDgps = isDrawDgps;
            DrawOrdersAndZonesOnMap();

        } 

        public void DrawOrderSensorValues(Dictionary<float, Color> codes,int Algorithm)
        {
            _mcGoogle.SetValueCodes(codes);
            _mcGoogle.DrawTrackMode = DrawTrackModes.DiffColors;
            _mcGoogle.ClearTracks();
            //if ((int) AlgorithmType.ANGLE == Algorithm)
            //    GetTrackWithSensorAngleValues();
            //else
            GetTrackWithSensorCalibratedValues(Algorithm);
            _mcGoogle.Repaint();

        }

        public void DrawOrderSensorValues(Dictionary<float, Color> codes, int Algorithm,DateTime DateStart,DateTime DateEnd)
        {
            _mcGoogle.SetValueCodes(codes);
            _mcGoogle.DrawTrackMode = DrawTrackModes.DiffColors;
            GetTrackWithSensorCalibratedValues(Algorithm, DateStart, DateEnd);
            _mcGoogle.Repaint();
        }
        
        public void DrawOrder(Dictionary<int, AgregatForTrack> agregatForTrack, bool isDrawMarkers)
        {
            _agregatForTrack = agregatForTrack;
            _isDrawMarkers = isDrawMarkers;
            DrawOrdersOnMap();
        }

        private void DrawOrdersOnMap()
        {
            _mcGoogle.ClearTracks();
            _mcGoogle.ClearMarkers();
            _mcGoogle.DrawTrackMode = DrawTrackModes.Thick;
            GetTracks();
            if (_isDrawDgps) GetTrackDgps(); 
            if (_isDrawMarkers) _mcGoogle.AddMarkers(_markers);
        }

        private void DrawOrdersAndZonesOnMap()
        {
            _mcGoogle.ClearAll();
            _mcGoogle.DrawTrackMode = DrawTrackModes.Thick;
            if (_isDrawDgps) GetTrackDgps();
            bool isZonesExixt = GetZones();
            if (GetTracks() || isZonesExixt || _isDrawDgps)
            {
                _mcGoogle.ZoomAndCenterAllAlways();
                if (_isDrawMarkers) _mcGoogle.AddMarkers(_markers);
                
            }
            
        }

        public void DrawOrderRecordWithSpeedControl(Dictionary<float, Color> codes)
        {
            _mcGoogle.SetValueCodes(codes);
            _mcGoogle.DrawTrackMode = DrawTrackModes.DiffColors;
            _mcGoogle.Repaint();
        }

        public void DrawZone(int idZone)
        {
                IZone zone = DocItem.ZonesModel.GetById(idZone);
                if (zone != null)
                {
                    _mcGoogle.ClearAll();
                    _mcGoogle.AddZone(zone);
                    _mcGoogle.ZoomAndCenterZones();
                }
        }
        /// <summary>
        /// Выбор данных наряда из таблицы agro_datagps
        /// </summary>
        /// <returns></returns>
        private bool GetTracks()
        {
            if (_agregatForTrack.Count == 0) return false;
            int i = 0;
            _markers.Clear(); 
            foreach (KeyValuePair<int, AgregatForTrack> kvp in _agregatForTrack)
            {
                Track tr = GetTrack(i, kvp.Key);
                if (tr != null)
                {
                    tr.WidthAgregatMeter = kvp.Value.WidthAgregat;
                    tr.ColorAgregat = kvp.Value.ColorAgregat;
                    _mcGoogle.AddTrack(tr);
                    i++;
                }
            }
            return true;
        }

        /// <summary>
        /// Прорисовка DGPS красным цветом
        /// </summary>
        /// <returns></returns>
        private void GetTrackDgps()
        {
            List<GpsData> gpoints = DataSetManager.GetGpsData64FromDbMobitelId( MobitelId,DateOrder,DateOrder.AddDays(1).AddMinutes(-1 ));
            if (gpoints != null && gpoints.Count > 0)
            {
                var tr = new Track(MobitelId, gpoints) {WidthAgregatMeter = 0, Color = Color.Red};
                _mcGoogle.AddTrack(tr);
            }
        }

        /// <summary>
        /// прорисовка значений угломерного датчика
        /// </summary>
        /// <returns></returns>
        private void GetTrackWithSensorAngleValues()
        {
            var gpoints = GpsDataProvider.GetGpsDataAllProtocols(MobitelId, DateOrder, DateOrder.AddDays(1).AddMinutes(-1));
            if (gpoints != null && gpoints.Count > 0)
            {
                var sensorAngle = new Sensor((int) AlgorithmType.ANGLE, MobitelId);
                if (sensorAngle.Id > 0)
                {
                    foreach (GpsData gpsData in gpoints)
                    {
                        gpsData.Speed = (float) sensorAngle.GetAngleSensorValue(gpsData.Sensors);
                    }
                    var tr = new Track(MobitelId, gpoints);
                    _mcGoogle.AddTrack(tr);
                }
            }
        }

        /// <summary>
        /// прорисовка значений  датчика
        /// </summary>
        /// <returns></returns>
        private void GetTrackWithSensorCalibratedValues(int Algirithm)
        {
            var gpoints = GpsDataProvider.GetGpsDataAllProtocols(MobitelId, DateOrder, DateOrder.AddDays(1).AddMinutes(-1));
            if (gpoints != null && gpoints.Count > 0)
            {
                var sensor = new SensorCalibrated(Algirithm, MobitelId);
                if (sensor.Id > 0)
                {
                    foreach (GpsData gpsData in gpoints)
                    {
                        gpsData.Speed = (float)sensor.GetValue(gpsData.Sensors);
                    }
                    var tr = new Track(MobitelId, gpoints);
                    _mcGoogle.AddTrack(tr);
                }
            }
        }

        /// <summary>
        /// прорисовка значений  датчика
        /// </summary>
        /// <returns></returns>
        private void GetTrackWithSensorCalibratedValues(int Algirithm, DateTime DateStart, DateTime DateEnd)
        {
            var gpoints = GpsDataProvider.GetGpsDataAllProtocols(MobitelId, DateStart, DateEnd);
            if (gpoints != null && gpoints.Count > 0)
            {
                var sensor = new SensorCalibrated(Algirithm, MobitelId);
                if (sensor.Id > 0)
                {
                    foreach (GpsData gpsData in gpoints)
                    {
                        gpsData.Speed = (float)sensor.GetValue(gpsData.Sensors);
                    }
                    var tr = new Track(MobitelId, gpoints);
                    _mcGoogle.AddTrack(tr);
                }
            }
        }
        
        private Track GetTrack(int i, int idOrderT)
        {
            Track tr = null;
            var cd = new CompressData("agro_datagps", idOrderT);
            List<IGeoPoint> gpoints = cd.DeCompressIGeoData();
            if (gpoints.Count > 0)
            {
                AddMarker(i, gpoints);
                tr = new Track(idOrderT, gpoints);
            }
            return tr;
        }

        private Track GetTrackFromDB(int idOrderT,int idMobitel)
        {
            var oir = new OrderItemRecord(idOrderT);
            var mobitel = VehicleProvider2.GetMobitel(idMobitel);
            Track tr = null;
            if (mobitel.Is64BitPackets)
            {
                List<GpsData> gpsDatas = DataSetManager.GetGpsData64FromDbMobitelId(idMobitel, oir.TimeStart,
                                                                                    oir.TimeEnd); 
               tr = new Track(idOrderT, gpsDatas);
            }
            else
            {
                var gpsProv = new GpsDataProvider();
                IList<GpsData> gpoints = gpsProv.GetValidDataForPeriod(idMobitel, oir.TimeStart, oir.TimeEnd);
                if (gpoints.Count > 0)
                {
                    var geopoints = gpoints.Cast<IGeoPoint>().ToList();
                    tr = new Track(idOrderT, geopoints);
                }
            }
            return tr;
        }

        private void AddMarker(int i, List<IGeoPoint> gpoints)
        {
            if (gpoints == null || gpoints.Count == 0) return;
            Marker m;

            if (i == _agregatForTrack.Count - 1)
            {
                m = new Marker(MarkerType.Finish, 0, new PointLatLng(gpoints[gpoints.Count - 1].LatLng.Lat, gpoints[gpoints.Count - 1].LatLng.Lng), "", "");
                _markers.Add(m);
            }
            if (i == 0)
            {
                m = new Marker(MarkerType.Pin, 0, new PointLatLng(gpoints[0].LatLng.Lat, gpoints[0].LatLng.Lng), (i + 1).ToString(), "");
                _markers.Add(m);
                return;
            }
            else
            {
                m = new Marker(MarkerType.BallGreen, 0, new PointLatLng(gpoints[0].LatLng.Lat, gpoints[0].LatLng.Lng), (i + 1).ToString(), "");
                _markers.Add(m);
            }
        }

        /// <summary>
        /// Прорисовка зон наряда цветами , определенными в гриде с записями наряда
        /// </summary>
        /// <returns></returns>
        private bool GetZones()
        {
 //Маємо зараз ще одну проблему, пов'язану зі зміною контурів поля.
 //У списку оброблених полів запис по цьому полю залишається, однак візуально треків не видно.
 //При спробі перерахувати наряд тих ТЗ, які працювали в даній КЗ - така помилка.
            if (_zoneColors.Count == 0) return false;
            foreach (KeyValuePair<int,Color>  kvp in _zoneColors)
            {
                IZone zone = DocItem.ZonesModel.GetById(kvp.Key);
                if (zone != null)
                {
                    IZoneStyle zoneStyle = new ZonesStyle();
                    zoneStyle.Color = kvp.Value;
                    zone.Style = zoneStyle; 
                    _mcGoogle.AddZone(zone);
                }
            }
            return true;
        }

        public void SelectRecord(int idOrderT)
        {
            DrawOrdersAndZonesOnMap();
            _mcGoogle.SelectTrackColor(idOrderT, Color.Red, Color.Navy, false);
        }

        public bool SplitRecord(int idOrderT, int idMobitel)
        {
            _mcGoogle.RemoveTrack(idOrderT);
            Track tr = GetTrackFromDB(idOrderT, idMobitel);
            if (tr == null)
            {
                XtraMessageBox.Show(Resources.PointsMissing, Resources.ApplicationName); 
                return false;
            }
                _mcGoogle.AddTrack(tr);
            _mcGoogle.IsTrackAnalizer = true; 
            _mcGoogle.SelectTrackColor(idOrderT, Color.Red, Color.Navy, true);
            _idOrderTForSplit = idOrderT;
            _ors = new OrderRecordSplitter();
            _ors.RecordSplitted += OnAfterSplit;
            _ors.ResetPointSelection += OnResetPointSelection;
            _ors.Start(idOrderT, _mcGoogle, tr);
            return true;
        }

        void OnAfterSplit()
        {
            _ors.RecordSplitted -= OnAfterSplit;
            RefreshData();
        }

        void OnResetPointSelection()
        {
            RefreshData();
            _ors.ResetPointSelection -= OnResetPointSelection;
            SelectRecord(_idOrderTForSplit);
        }

        public void ViewMarkers(bool isDrawMarkers)
        {
            if (_markers.Count == 0)
            {
                _mcGoogle.ClearMarkers();
                return;
            }
            if (!_isDrawMarkers && isDrawMarkers)
            {
                _mcGoogle.AddMarkers(_markers);
            }
            else
            {
                _mcGoogle.ClearMarkers();
            }
            _isDrawMarkers = isDrawMarkers;
        }

        public void DrawFieldsTcOnMap(FieldsTcEntity fieldsTc)
        {
            if (fieldsTc == null) return;
            if (fieldsTc.Operations == null) return;
            _mcGoogle.ClearTracks();
            _mcGoogle.ClearMarkers();
            _mcGoogle.DrawTrackMode = DrawTrackModes.Thick;
            foreach (FieldTechOperation oper in fieldsTc.Operations.Where(op => op.DrawTrack).ToList())
            {
                foreach (TechOperationFactRecord factRecord in oper.FactRecords)
                {
                    if (factRecord.IdOrderTs == null)
                    {
                        factRecord.IdOrderTs = TechOpersFactController.GetListIdOrderT(factRecord);
                    }
                    if (factRecord.IdOrderTs != null) DrawTrackList(factRecord.IdOrderTs, oper.WorkType.TrackColor);
                }
            }   
        }

        private void DrawTrackList(List<int> idOrderTs, Color trackColor)
        {
            idOrderTs.ForEach(idot => AddTrack(idot, trackColor)); 
        }

        private void AddTrack(int idOrderT,Color trackColor)
        {
            var cd = new CompressData("agro_datagps", idOrderT);
            List<IGeoPoint> gpoints = cd.DeCompressIGeoData();
            if (gpoints.Count > 0)
            {
                _mcGoogle.AddTrack(new Track(idOrderT, trackColor, 10, gpoints));
            }
        }

        public void ClearTracks()
        {
            _mcGoogle.ClearTracks(); 
        }

    }
}
