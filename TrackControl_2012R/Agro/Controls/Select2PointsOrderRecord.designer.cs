namespace Agro.Controls
{
    partial class Select2PointsOrderRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Select2PointsOrderRecord));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.vgSecond = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.riMemo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.riMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.lookUpFieldsS = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookUpDriverS = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookUpAgregatS = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemZoomTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.lookUpWorkS = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this._second = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.dataGpsIdRowS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.indexS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.dateTimeRowS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.fieldS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.driverS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.agregatS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.workS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.vgFirst = new DevExpress.XtraVerticalGrid.VGridControl();
            this._locationRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.riteFirst = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.riMemoEditF = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.lookUpFields = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookUpWork = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.lookUpDriver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.lookUpAgregat = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this._first = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.dataGpsIdRowF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.indexF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.dateTimeRowF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.fieldF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.driverF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.agregatF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.workF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rgSelectPoint = new DevExpress.XtraEditors.RadioGroup();
            this.peTrack = new DevExpress.XtraEditors.PictureEdit();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.sbtExit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtRecordCreate = new DevExpress.XtraEditors.SimpleButton();
            this.lbPath = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vgSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFieldsS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpDriverS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpAgregatS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWorkS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vgFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEditF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpAgregat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelectPoint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrack.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.Controls.Add(this.vgSecond, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.vgFirst, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.rgSelectPoint, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.peTrack, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbPath, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, -1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(350, 643);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // vgSecond
            // 
            this.vgSecond.Appearance.ReadOnlyRecordValue.BackColor = System.Drawing.SystemColors.Window;
            this.vgSecond.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.ReadOnlyRecordValue.Options.UseBackColor = true;
            this.vgSecond.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vgSecond.Appearance.ReadOnlyRow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vgSecond.Appearance.RowHeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vgSecond.Appearance.RowHeaderPanel.Options.UseTextOptions = true;
            this.vgSecond.Appearance.RowHeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vgSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vgSecond.Enabled = false;
            this.vgSecond.Location = new System.Drawing.Point(3, 201);
            this.vgSecond.Name = "vgSecond";
            this.vgSecond.RecordWidth = 174;
            this.vgSecond.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.riMemo,
            this.riMemoEdit,
            this.lookUpFieldsS,
            this.lookUpDriverS,
            this.lookUpAgregatS,
            this.repositoryItemZoomTrackBar2,
            this.lookUpWorkS});
            this.vgSecond.RowHeaderWidth = 163;
            this.vgSecond.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._second});
            this.vgSecond.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vgSecond.Size = new System.Drawing.Size(344, 166);
            this.vgSecond.TabIndex = 14;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            // 
            // riMemo
            // 
            this.riMemo.AutoHeight = false;
            this.riMemo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riMemo.Name = "riMemo";
            this.riMemo.ReadOnly = true;
            // 
            // riMemoEdit
            // 
            this.riMemoEdit.Name = "riMemoEdit";
            this.riMemoEdit.ReadOnly = true;
            // 
            // lookUpFieldsS
            // 
            this.lookUpFieldsS.AutoHeight = false;
            this.lookUpFieldsS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpFieldsS.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpFieldsS.DisplayMember = "Name";
            this.lookUpFieldsS.Name = "lookUpFieldsS";
            this.lookUpFieldsS.NullText = "";
            this.lookUpFieldsS.ValueMember = "Id";
            this.lookUpFieldsS.EditValueChanged += new System.EventHandler(this.lookUpFieldsS_EditValueChanged);
            // 
            // lookUpDriverS
            // 
            this.lookUpDriverS.AutoHeight = false;
            this.lookUpDriverS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpDriverS.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "Name")});
            this.lookUpDriverS.DisplayMember = "Family";
            this.lookUpDriverS.Name = "lookUpDriverS";
            this.lookUpDriverS.NullText = "";
            this.lookUpDriverS.ValueMember = "id";
            this.lookUpDriverS.EditValueChanged += new System.EventHandler(this.lookUpDriverS_EditValueChanged);
            // 
            // lookUpAgregatS
            // 
            this.lookUpAgregatS.AutoHeight = false;
            this.lookUpAgregatS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpAgregatS.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpAgregatS.DisplayMember = "Name";
            this.lookUpAgregatS.Name = "lookUpAgregatS";
            this.lookUpAgregatS.NullText = "";
            this.lookUpAgregatS.ValueMember = "Id";
            this.lookUpAgregatS.EditValueChanged += new System.EventHandler(this.lookUpAgregatS_EditValueChanged);
            // 
            // repositoryItemZoomTrackBar2
            // 
            this.repositoryItemZoomTrackBar2.Name = "repositoryItemZoomTrackBar2";
            this.repositoryItemZoomTrackBar2.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // lookUpWorkS
            // 
            this.lookUpWorkS.AutoHeight = false;
            this.lookUpWorkS.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpWorkS.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpWorkS.DisplayMember = "Name";
            this.lookUpWorkS.Name = "lookUpWorkS";
            this.lookUpWorkS.NullText = "";
            this.lookUpWorkS.ValueMember = "Id";
            this.lookUpWorkS.EditValueChanged += new System.EventHandler(this.lookUpWorkS_EditValueChanged);
            // 
            // _second
            // 
            this._second.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.dataGpsIdRowS,
            this.indexS,
            this.dateTimeRowS,
            this.fieldS,
            this.driverS,
            this.agregatS,
            this.workS});
            this._second.Height = 19;
            this._second.Name = "_second";
            this._second.OptionsRow.AllowFocus = false;
            this._second.OptionsRow.AllowMove = false;
            this._second.OptionsRow.AllowMoveToCustomizationForm = false;
            this._second.OptionsRow.ShowInCustomizationForm = false;
            this._second.Properties.Caption = "��������� �����";
            // 
            // dataGpsIdRowS
            // 
            this.dataGpsIdRowS.Height = 20;
            this.dataGpsIdRowS.Name = "dataGpsIdRowS";
            this.dataGpsIdRowS.OptionsRow.AllowMove = false;
            this.dataGpsIdRowS.OptionsRow.AllowMoveToCustomizationForm = false;
            this.dataGpsIdRowS.OptionsRow.ShowInCustomizationForm = false;
            this.dataGpsIdRowS.Properties.Caption = "DataGps_ID";
            this.dataGpsIdRowS.Properties.ReadOnly = true;
            // 
            // indexS
            // 
            this.indexS.Height = 20;
            this.indexS.Name = "indexS";
            this.indexS.Properties.Caption = "����� � �����";
            this.indexS.Properties.ReadOnly = true;
            // 
            // dateTimeRowS
            // 
            this.dateTimeRowS.Height = 18;
            this.dateTimeRowS.Name = "dateTimeRowS";
            this.dateTimeRowS.OptionsRow.AllowMove = false;
            this.dateTimeRowS.OptionsRow.AllowMoveToCustomizationForm = false;
            this.dateTimeRowS.OptionsRow.ShowInCustomizationForm = false;
            this.dateTimeRowS.Properties.Caption = "���� � �����";
            this.dateTimeRowS.Properties.ReadOnly = true;
            // 
            // fieldS
            // 
            this.fieldS.Height = 17;
            this.fieldS.Name = "fieldS";
            this.fieldS.OptionsRow.AllowMove = false;
            this.fieldS.OptionsRow.AllowMoveToCustomizationForm = false;
            this.fieldS.OptionsRow.ShowInCustomizationForm = false;
            this.fieldS.Properties.Caption = "����";
            this.fieldS.Properties.RowEdit = this.lookUpFieldsS;
            // 
            // driverS
            // 
            this.driverS.Height = 18;
            this.driverS.Name = "driverS";
            this.driverS.OptionsRow.AllowMove = false;
            this.driverS.OptionsRow.AllowMoveToCustomizationForm = false;
            this.driverS.OptionsRow.ShowInCustomizationForm = false;
            this.driverS.Properties.Caption = "��������";
            this.driverS.Properties.RowEdit = this.lookUpDriverS;
            // 
            // agregatS
            // 
            this.agregatS.Expanded = false;
            this.agregatS.Height = 20;
            this.agregatS.Name = "agregatS";
            this.agregatS.Properties.Caption = "�������� ������������";
            this.agregatS.Properties.RowEdit = this.lookUpAgregatS;
            // 
            // workS
            // 
            this.workS.Name = "workS";
            this.workS.Properties.Caption = "��� ������";
            this.workS.Properties.RowEdit = this.lookUpWorkS;
            // 
            // vgFirst
            // 
            this.vgFirst.Appearance.ReadOnlyRecordValue.BackColor = System.Drawing.SystemColors.Window;
            this.vgFirst.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.ReadOnlyRecordValue.Options.UseBackColor = true;
            this.vgFirst.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vgFirst.Appearance.ReadOnlyRow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vgFirst.Appearance.RowHeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vgFirst.Appearance.RowHeaderPanel.Options.UseTextOptions = true;
            this.vgFirst.Appearance.RowHeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vgFirst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vgFirst.Location = new System.Drawing.Point(3, 31);
            this.vgFirst.Name = "vgFirst";
            this.vgFirst.RecordWidth = 174;
            this.vgFirst.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._locationRepo,
            this.riteFirst,
            this.riMemoEditF,
            this.lookUpFields,
            this.lookUpWork,
            this.lookUpDriver,
            this.repositoryItemZoomTrackBar1,
            this.lookUpAgregat});
            this.vgFirst.RowHeaderWidth = 162;
            this.vgFirst.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._first});
            this.vgFirst.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vgFirst.Size = new System.Drawing.Size(344, 164);
            this.vgFirst.TabIndex = 11;
            // 
            // _locationRepo
            // 
            this._locationRepo.Name = "_locationRepo";
            this._locationRepo.ReadOnly = true;
            // 
            // riteFirst
            // 
            this.riteFirst.AutoHeight = false;
            this.riteFirst.Name = "riteFirst";
            // 
            // riMemoEditF
            // 
            this.riMemoEditF.Name = "riMemoEditF";
            this.riMemoEditF.ReadOnly = true;
            // 
            // lookUpFields
            // 
            this.lookUpFields.AutoHeight = false;
            this.lookUpFields.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpFields.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpFields.DisplayMember = "Name";
            this.lookUpFields.Name = "lookUpFields";
            this.lookUpFields.NullText = "";
            this.lookUpFields.ValueMember = "Id";
            this.lookUpFields.EditValueChanged += new System.EventHandler(this.lookUpFields_EditValueChanged);
            // 
            // lookUpWork
            // 
            this.lookUpWork.AutoHeight = false;
            this.lookUpWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpWork.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpWork.DisplayMember = "Name";
            this.lookUpWork.Name = "lookUpWork";
            this.lookUpWork.NullText = "";
            this.lookUpWork.ValueMember = "Id";
            this.lookUpWork.EditValueChanged += new System.EventHandler(this.lookUpWork_EditValueChanged);
            // 
            // lookUpDriver
            // 
            this.lookUpDriver.AutoHeight = false;
            this.lookUpDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpDriver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "Name")});
            this.lookUpDriver.DisplayMember = "Family";
            this.lookUpDriver.Name = "lookUpDriver";
            this.lookUpDriver.NullText = "";
            this.lookUpDriver.ValueMember = "id";
            this.lookUpDriver.EditValueChanged += new System.EventHandler(this.lookUpDriver_EditValueChanged);
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            this.repositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // lookUpAgregat
            // 
            this.lookUpAgregat.AutoHeight = false;
            this.lookUpAgregat.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpAgregat.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.lookUpAgregat.DisplayMember = "Name";
            this.lookUpAgregat.Name = "lookUpAgregat";
            this.lookUpAgregat.NullText = "";
            this.lookUpAgregat.ValueMember = "Id";
            this.lookUpAgregat.EditValueChanged += new System.EventHandler(this.lookUpAgregat_EditValueChanged);
            // 
            // _first
            // 
            this._first.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.dataGpsIdRowF,
            this.indexF,
            this.dateTimeRowF,
            this.fieldF,
            this.driverF,
            this.agregatF,
            this.workF});
            this._first.Height = 19;
            this._first.Name = "_first";
            this._first.OptionsRow.AllowFocus = false;
            this._first.OptionsRow.AllowMove = false;
            this._first.OptionsRow.AllowMoveToCustomizationForm = false;
            this._first.OptionsRow.ShowInCustomizationForm = false;
            this._first.Properties.Caption = "������ �����";
            // 
            // dataGpsIdRowF
            // 
            this.dataGpsIdRowF.Height = 20;
            this.dataGpsIdRowF.Name = "dataGpsIdRowF";
            this.dataGpsIdRowF.OptionsRow.AllowMove = false;
            this.dataGpsIdRowF.OptionsRow.AllowMoveToCustomizationForm = false;
            this.dataGpsIdRowF.OptionsRow.ShowInCustomizationForm = false;
            this.dataGpsIdRowF.Properties.Caption = "DataGps_ID";
            this.dataGpsIdRowF.Properties.ReadOnly = true;
            // 
            // indexF
            // 
            this.indexF.Height = 20;
            this.indexF.Name = "indexF";
            this.indexF.Properties.Caption = "����� � �����";
            this.indexF.Properties.ReadOnly = true;
            // 
            // dateTimeRowF
            // 
            this.dateTimeRowF.Height = 18;
            this.dateTimeRowF.Name = "dateTimeRowF";
            this.dateTimeRowF.OptionsRow.AllowMove = false;
            this.dateTimeRowF.OptionsRow.AllowMoveToCustomizationForm = false;
            this.dateTimeRowF.OptionsRow.ShowInCustomizationForm = false;
            this.dateTimeRowF.Properties.Caption = "���� � �����";
            this.dateTimeRowF.Properties.ReadOnly = true;
            // 
            // fieldF
            // 
            this.fieldF.Height = 17;
            this.fieldF.Name = "fieldF";
            this.fieldF.OptionsRow.AllowMoveToCustomizationForm = false;
            this.fieldF.OptionsRow.ShowInCustomizationForm = false;
            this.fieldF.Properties.Caption = "����";
            this.fieldF.Properties.RowEdit = this.lookUpFields;
            // 
            // driverF
            // 
            this.driverF.Height = 18;
            this.driverF.Name = "driverF";
            this.driverF.OptionsRow.AllowMove = false;
            this.driverF.OptionsRow.AllowMoveToCustomizationForm = false;
            this.driverF.OptionsRow.ShowInCustomizationForm = false;
            this.driverF.Properties.Caption = "��������";
            this.driverF.Properties.RowEdit = this.lookUpDriver;
            // 
            // agregatF
            // 
            this.agregatF.Appearance.Options.UseTextOptions = true;
            this.agregatF.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.agregatF.Height = 20;
            this.agregatF.Name = "agregatF";
            this.agregatF.Properties.Caption = "�������� ������������";
            this.agregatF.Properties.RowEdit = this.lookUpAgregat;
            // 
            // workF
            // 
            this.workF.Name = "workF";
            this.workF.Properties.Caption = "��� ������";
            this.workF.Properties.RowEdit = this.lookUpWork;
            // 
            // rgSelectPoint
            // 
            this.rgSelectPoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgSelectPoint.EditValue = 1;
            this.rgSelectPoint.Location = new System.Drawing.Point(3, 3);
            this.rgSelectPoint.Name = "rgSelectPoint";
            this.rgSelectPoint.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "������ �����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "��������� �����")});
            this.rgSelectPoint.Size = new System.Drawing.Size(344, 22);
            this.rgSelectPoint.TabIndex = 0;
            this.rgSelectPoint.SelectedIndexChanged += new System.EventHandler(this.rgSelectPoint_SelectedIndexChanged);
            // 
            // peTrack
            // 
            this.peTrack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peTrack.Location = new System.Drawing.Point(3, 373);
            this.peTrack.Name = "peTrack";
            this.peTrack.Properties.NullText = "�������� �����";
            this.peTrack.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peTrack.Size = new System.Drawing.Size(344, 204);
            this.peTrack.TabIndex = 15;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.32099F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.67901F));
            this.tableLayoutPanel2.Controls.Add(this.sbtExit, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.sbtRecordCreate, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 603);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(324, 40);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // sbtExit
            // 
            this.sbtExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtExit.Image = ((System.Drawing.Image)(resources.GetObject("sbtExit.Image")));
            this.sbtExit.Location = new System.Drawing.Point(208, 5);
            this.sbtExit.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.sbtExit.Name = "sbtExit";
            this.sbtExit.Size = new System.Drawing.Size(106, 25);
            this.sbtExit.TabIndex = 12;
            this.sbtExit.Text = "�����";
            this.sbtExit.Click += new System.EventHandler(this.sbtExit_Click);
            // 
            // sbtRecordCreate
            // 
            this.sbtRecordCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtRecordCreate.Enabled = false;
            this.sbtRecordCreate.Image = ((System.Drawing.Image)(resources.GetObject("sbtRecordCreate.Image")));
            this.sbtRecordCreate.Location = new System.Drawing.Point(14, 5);
            this.sbtRecordCreate.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.sbtRecordCreate.Name = "sbtRecordCreate";
            this.sbtRecordCreate.Size = new System.Drawing.Size(152, 25);
            this.sbtRecordCreate.TabIndex = 13;
            this.sbtRecordCreate.Text = "������� ������ ������";
            this.sbtRecordCreate.Click += new System.EventHandler(this.sbtRecordCreate_Click);
            // 
            // lbPath
            // 
            this.lbPath.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbPath.Location = new System.Drawing.Point(59, 583);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(231, 13);
            this.lbPath.TabIndex = 16;
            this.lbPath.Text = "�������� �����: ������ - 0 ��, ����� - 0 ���";
            // 
            // Select2PointsOrderRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 642);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Select2PointsOrderRecord";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� ������ � ��������� ����� ����� ������";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vgSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFieldsS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpDriverS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpAgregatS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWorkS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vgFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEditF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpAgregat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelectPoint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrack.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.RadioGroup rgSelectPoint;
        private DevExpress.XtraVerticalGrid.VGridControl vgFirst;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _locationRepo;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow _first;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow dataGpsIdRowF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow dateTimeRowF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow fieldF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow driverF;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton sbtRecordCreate;
        private DevExpress.XtraEditors.SimpleButton sbtExit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow agregatF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow indexF;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riteFirst;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit riMemoEditF;
        private DevExpress.XtraEditors.PictureEdit peTrack;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow workF;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpFields;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpWork;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpDriver;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpAgregat;
        private DevExpress.XtraVerticalGrid.VGridControl vgSecond;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit riMemo;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit riMemoEdit;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow _second;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow dataGpsIdRowS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow indexS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow dateTimeRowS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow fieldS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow driverS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow agregatS;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpFieldsS;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpDriverS;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpAgregatS;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lookUpWorkS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow workS;
        private DevExpress.XtraEditors.LabelControl lbPath;
    }
}