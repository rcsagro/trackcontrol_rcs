using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using GeoData;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites; 

namespace Agro.Controls
{
    public partial class AgroMap : DevExpress.XtraEditors.XtraUserControl
    {
        #region Fields
        GMapOrderProjection _gmapOrder;
        /// <summary>
        /// ������ + ����� ��������� ������� �������
        /// </summary>
        Dictionary<int, AgregatForTrack> _agregatForTrack;
        double _defaultWidthAgr;
        /// <summary>
        /// �������� ����� �����
        /// </summary>
        System.Drawing.Point[] _spointsV;
        int _zoneId;
        int _orderTId;
        int _orderId;
        int _workId;
        DateTime _dtStart = DateTime.Today;
        DateTime _dtEnd = DateTime.Today;
        // ������ ������ ������� � ����
        Dictionary<int, Color> OrderColors = new Dictionary<int, Color>();
        public Delegate UserViewOrder;
        OrderSquareBitmap _osb;
        int _iVideoCounter;
        private bool _isDrawMarkers;
        #endregion

        #region �����������
        public AgroMap()
        {
            InitializeComponent();
            Localization();
            this.Dock = DockStyle.Fill;
            InitGoogleMapControl();
            SetDefaultAgregatWidth();
            _agregatForTrack = new Dictionary<int, AgregatForTrack>();
            //pEndLine = MapUtilites.GetLineCap();
        }
        #endregion

        #region Draw

        private void ShowSpeedParameters(object sender, System.EventArgs e)
        {
            ShowSpeedParameters();
        }

        private void ShowSpeedParameters()
        {
            SetSelectedWorkId();
            Dialogs.SpeedModeDlg speedDlg = new Dialogs.SpeedModeDlg(_workId);
            if (speedDlg.ShowDialog() == DialogResult.OK)
            {

            }
        }

        /// <summary>
        /// ���������� ����
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        public void DrawZone(int zoneId, DateTime dtStart, DateTime dtEnd,int idOrder = 0)
        {
            if (_zoneId != zoneId)
            {
                gcMap.DataSource = null;
                OrderColors.Clear();
                _zoneId = zoneId;
            }
            _gmapOrder.DrawZone(zoneId);
            _dtStart = dtStart;
            _dtEnd = dtEnd;
            _orderId = idOrder;
            SetVideoRegime(false);
            SetOrdersColors();
            DrawTracksInZone();
            
        }

        /// <summary>
        /// ���������� ���� ������ � ���������� 
        /// </summary>
        /// <param name="gr"></param>
        private void DrawTracksInZone(bool ReloadGrid = true)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                if (ReloadGrid) LoadOrderRecordsWorkingInZone();
                if (driverDb.TablePresent("agro_datagps"))
                {
                    _agregatForTrack.Clear();
                    var dt = gcMap.DataSource as DataTable;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        
                        foreach (DataRow drow in dt.Rows)
                        {
                            int idOrderT = (int)drow["Id_ordert"];
                            int idOrder = (int)drow["Id"];
                            if ((bool)drow["Sel"])
                            {
                                double widthAgregat = 0;
                                Double.TryParse(drow["Width"].ToString(), out widthAgregat);
                                if (widthAgregat == 0) widthAgregat = _defaultWidthAgr;
                                Color oderColor = MapUtilites.GetColorForRecordsSeparation(0);
                                oderColor = OrderColors.ContainsKey(idOrder) ? OrderColors[idOrder] : Color.FromArgb(77, oderColor.R, oderColor.G, oderColor.B);
                                if (!_agregatForTrack.ContainsKey(idOrderT)) _agregatForTrack.Add(idOrderT, new AgregatForTrack(oderColor, widthAgregat));
                            }

                        }
                        _gmapOrder.DrawOrder(_agregatForTrack, _isDrawMarkers);
                    }
                    
                }
            }
        }

        void SetOrdersColors()
        {
            if (gvMap.RowCount == 0) LoadOrderRecordsWorkingInZone();
            OrderColors.Clear();
            DataTable dt = gcMap.DataSource as DataTable;
            int colorDelta = 0;
            int orderDraw = 0;
            foreach (DataRow drow in dt.Rows)
            {
                int idOrderT = (int)drow["Id_ordert"];
                int idOrder = (int)drow["Id"];
                if ((bool)drow["Sel"])
                {
                    //Color colorForOrder = Color.Empty;
                    SetOrderColor(ref colorDelta, ref orderDraw,  idOrder);
                }

            }
        }

        private void DrawTracksInZoneWithSpeedControl(object sender, System.EventArgs e)
        {
            DrawTracksInZoneWithSpeedControl();
        }

        private void DrawTracksInZoneWithSpeedControl()
        {
            if (gvMap.RowCount == 0) return;
            Dictionary<float, Color> codes = new Dictionary<float, Color>(3);
            SetSelectedWorkId();
            SetSpeedColorDictionary(codes, _workId);
            _gmapOrder.DrawOrderRecordWithSpeedControl(codes);
        }

        void SetSelectedWorkId()
        {
            if (gvMap.RowCount == 0)
            {
                _workId = 0;
                return;
            }
            int workId=0;
            Int32.TryParse(gvMap.GetRowCellValue(gvMap.FocusedRowHandle, colWorkId).ToString(),out workId);
            _workId = workId;
        }

        private  void SetSpeedColorDictionary(Dictionary<float, Color> codes, int workId)
        {
            Color colorMin = Color.FromArgb(Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorMin, Color.Yellow.ToArgb().ToString())));
            Color colorNorm = Color.FromArgb(Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorNorm, Color.Green.ToArgb().ToString())));
            Color colorMax = Color.FromArgb(Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.SpeedColorMax, Color.Red.ToArgb().ToString())));
            if (workId == 0)
            {
                double speedMin = Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMin, Consts.SPEED_MIN.ToString()));
                double speedMax = Convert.ToDouble(Params.ReadParTotal(Params.NUMPAR.SpeedMax, Consts.SPEED_MAX.ToString()));
                if (!codes.ContainsKey((float)speedMin)) codes.Add((float)speedMin, colorMin);
                if (!codes.ContainsKey((float)speedMax)) codes.Add((float)speedMax, colorNorm);
                if (!codes.ContainsKey(10000)) codes.Add(10000, colorMax);
            }
            else
            {
                using (DictionaryAgroWorkType dwt = new DictionaryAgroWorkType(workId))
                {
                    if (!codes.ContainsKey((float)dwt.SpeedBottom)) codes.Add((float)dwt.SpeedBottom, colorMin);
                    if (!codes.ContainsKey((float)dwt.SpeedTop)) codes.Add((float)dwt.SpeedTop, colorNorm);
                    if (!codes.ContainsKey(1000)) codes.Add(10000, colorMax);
                }
            }
        }

        private void SetOrderColor(ref int colorDelta, ref int orderDraw,  int idOrder)
        {
            if (orderDraw == 0) orderDraw = idOrder;
            if (orderDraw != idOrder)
            {
                colorDelta += 1;
                orderDraw = idOrder;
            }
            Color colorOrder = MapUtilites.GetColorForRecordsSeparation(colorDelta);
            colorOrder = Color.FromArgb(77, colorOrder.R, colorOrder.G, colorOrder.B);
            if (!OrderColors.ContainsKey(idOrder))
                OrderColors.Add(idOrder, colorOrder);
            else
                OrderColors[idOrder] = colorOrder;
        }

        public void SetDefaultAgregatWidth()
        {
                _defaultWidthAgr = AgroController.Instance.WidthAgregatDefault;  
        }


        private void bchMarkers_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _isDrawMarkers = bchMarkers.Checked;
            _gmapOrder.ViewMarkers(_isDrawMarkers);
        }
        #endregion

        #region Grid
        private void gvMap_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.RowHandle >= 0)
            {
                int idOrder = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Id"]));
                if (OrderColors.ContainsKey(idOrder))
                {
                    e.Appearance.BackColor = OrderColors[idOrder];
                    e.Appearance.BackColor2 = Color.White;// Color.YellowGreen;
                }
            }

        }

        private void gvMap_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                UserViewOrder.DynamicInvoke((int)gvMap.GetRowCellValue(gvMap.FocusedRowHandle, gvMap.Columns.ColumnByName("Id")));
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message,"Error"); 
            }
        }

        private void gvMap_ShowGridMenu(object sender, GridMenuEventArgs e)
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            DevExpress.XtraGrid.Menu.GridViewMenu gvMenu = (DevExpress.XtraGrid.Menu.GridViewMenu)e.Menu;
            DevExpress.Utils.Menu.DXMenuItem menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.SpeedControl, new EventHandler(DrawTracksInZoneWithSpeedControl));
            gvMenu.Items.Add(menuItem);
            menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.SpeedParameters, new EventHandler(ShowSpeedParameters));
            gvMenu.Items.Add(menuItem);
            menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.DataRefresh, new EventHandler(LoadOrderRecordsWorkingInZoneFromGrid));
            gvMenu.Items.Add(menuItem);
            menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.Video, new EventHandler(VideoTrack));
            gvMenu.Items.Add(menuItem);
            menuItem = new DevExpress.Utils.Menu.DXMenuItem(Resources.SquareCountour, new EventHandler(ShowSquare));
            gvMenu.Items.Add(menuItem);
        }

        private void gvMap_Click(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            Point p = view.GridControl.PointToClient(Control.MousePosition);
            GridHitInfo ghi = view.CalcHitInfo(p);
            if (ghi.HitTest == GridHitTest.Column)
            {
                if (ghi.Column.Name == "Sel")
                {
                    if (ghi.Column.ImageIndex == 1)
                        ghi.Column.ImageIndex = 0;
                    else
                        ghi.Column.ImageIndex = 1;
                    DataRow[] rows = new DataRow[gvMap.RowCount];
                    for (int i = 0; i < rows.Length; i++)
                    {
                        rows[i] = gvMap.GetDataRow(i);
                        rows[i]["Sel"] = ghi.Column.ImageIndex;
                    }
                    DrawTracksInZone(false);
                }
            }
        }

        private void gvMap_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            gvMap.SetRowCellValue(e.RowHandle, e.Column, e.Value);
            DrawTracksInZone(false); 
        }

        private void LoadOrderRecordsWorkingInZoneFromGrid(object sender, System.EventArgs e)
        {
            DrawTracksInZone();
        }

        private void ShowSquare(object sender, System.EventArgs e)
        {
            ShowSquare();
        }

        private void ShowSquare()
        {
            if (gvMap.RowCount == 0) return;
            _orderTId = (int)gvMap.GetRowCellValue(gvMap.FocusedRowHandle, "Id_ordert");
            new DrawSquare(_orderTId).ShowDialog();
        }

        private void PrintReport()
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.PrintReportConfim, Resources.ApplicationName, MessageBoxButtons.YesNo))
                return;
            XtraGridService.SetupGidViewForPrint(gvMap, true, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            IZone zone = DocItem.ZonesModel.GetById(_zoneId);
            XtraGridService.DevExpressReportHeader(string.Format("{0}: {1}", Resources.Field, zone.Name), e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _dtStart.ToString("dd.MM.yyyy"), _dtEnd.ToString("dd.MM.yyyy"), Resources.PeriodFrom, Resources.PeriodTo), 60, e);
            Image newimage = _gmapOrder.Screenshot;
            e.Graph.DrawImage(newimage, new RectangleF(120, 50, 370, 300));

        }
        /// <summary>
        /// �������� ����� ��� ������ ����������� ����� ����������, ����������� � ��������� ����
        /// </summary>
        private void LoadOrderRecordsWorkingInZone()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                DataTable dtMain = null;
                string sSQL = "";
                driverDb.ConnectDb();
                if (_orderId > 0)
                {

                    sSQL = string.Format(AgroQuery.AgroMap.SelectAgroOrderById, _zoneId, _orderId);
                    dtMain = driverDb.GetDataTable(sSQL);
                }
                else
                {
                    sSQL = string.Format(AgroQuery.AgroMap.SelectAgroOrder, _zoneId);
                    driverDb.NewSqlParameterArray(2);

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (_dtStart.Year <= 1900)
                            _dtStart = new DateTime(1900, 1, 1);

                        if (_dtEnd.Year <= 1900)
                            _dtEnd = new DateTime(1900, 1, 1);
                    }

                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "start", _dtStart);
                    driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "end", _dtEnd.AddDays(1));
                    dtMain = driverDb.GetDataTable(sSQL, driverDb.GetSqlParameterArray);
                }
                OrderFilterVehicles.SetFilter(ref dtMain); 
                DataColumn dcBool = new DataColumn("Sel");
                dcBool.DataType = System.Type.GetType("System.Boolean");
                dcBool.DefaultValue = true;
                dtMain.Columns.Add(dcBool);
                gcMap.DataSource = dtMain;
            }
        }

        #endregion

        #region GoogleMaps
        private void InitGoogleMapControl()
        {
            _gmapOrder = new GMapOrderProjection(pnGIS);
        }
        #endregion

        #region Video
        private void VideoTrack(object sender, System.EventArgs e)
        {
            VideoTrack();
        }

        private void VideoTrack()
        {
            if (gvMap.RowCount == 0) return;
            SetVideoRegime(true);
            List<TRealPoint> rpoints = new List<TRealPoint>();
            // �������� BitMap ��� ����������� ����
            DicUtilites.GetZonePoints(_zoneId, ref rpoints);
            _spointsV = new System.Drawing.Point[rpoints.Count];
            _osb = new OrderSquareBitmap(DicUtilites.GetZoneSquare(_zoneId) > GlobalVars.MAX_SQ_FOR_CHANGE_KF ? GlobalVars.SCALE_BIG_ZONE : GlobalVars.SCALE_WORK);
            _osb.SetScreenPoints(true, ref rpoints, ref _spointsV);
            _osb.DrawZone(ref _spointsV);
            int Id_ordert_draw;
            if (Int32.TryParse(gvMap.GetRowCellValue(gvMap.FocusedRowHandle, "Id_ordert").ToString(), out Id_ordert_draw))
            {
                CompressData cd = new CompressData("agro_datagps", Id_ordert_draw);
                double dbWidthPixelMeter = 0;
                List<Point> spoints_cs = cd.DeCompress(MapUtilites.Geo2BmpPixLevel, out dbWidthPixelMeter);
                rpoints = cd.DeCompressReal();
                if (rpoints.Count == 0) return;
                using (DictionaryAgroAgregat da = new DictionaryAgroAgregat())
                {
                    double dbWidth = da.GetWidthInOrderRecord(Id_ordert_draw) * dbWidthPixelMeter;
                
                _spointsV = new System.Drawing.Point[rpoints.Count];
                _osb.SetScreenPoints(false, ref rpoints, ref _spointsV);
                _osb.InitTrack(dbWidth);
                }
                rbeTrackBar.Minimum = 0;
                rbeTrackBar.Maximum = rpoints.Count - 1;
            }
            peVideo.Image = _osb.BtmField;
            _iVideoCounter = 0;
            tmVideo.Enabled = true;
        }

        private void tmVideo_Tick(object sender, EventArgs e)
        {
            _iVideoCounter++;
            if (_iVideoCounter >= (_spointsV.Length - 1))
            {
                tmVideo.Enabled = false;
                return;
            }
            if (_iVideoCounter > 0)
            {
                _osb.DrawTrackSegment(ref _spointsV, _iVideoCounter);
                peVideo.Refresh();
            }
            beTrackBar.EditValue = _iVideoCounter;
        }
        /// <summary>
        /// ��������� ��������� ���������� �����
        /// </summary>
        /// <param name="bSet"></param>
        private void SetVideoRegime(bool bSet)
        {
            peVideo.Visible = bSet;
            barVideo.Visible = bSet;
            tmVideo.Enabled = bSet; ;
            if (bSet) 
            { 
                 _iVideoCounter = 0;
                 beTrackBar.EditValue = 0;
            }
        }

        private void btExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SetVideoRegime(false);
        }

        private void btPause_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            tmVideo.Enabled = false;
        }

        private void btStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            tmVideo.Enabled = true;
            _iVideoCounter = Convert.ToInt32(beTrackBar.EditValue); ;
        }

        private void beTrackBar_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _iVideoCounter = Convert.ToInt32(beTrackBar.EditValue);
        }

        private void btStop_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                _iVideoCounter = 0;
                tmVideo.Enabled = false;
                beTrackBar.EditValue = 0;
                VideoTrack(this, new EventArgs());
            }
       #endregion

        #region �����������
        private void Localization()
        {
            peVideo.Properties.NullText = Resources.ImageAbsence;
            TUtil.ToolTipChangeText(btStart, Resources.Start);
            TUtil.ToolTipChangeText(btPause, Resources.Pause);
            TUtil.ToolTipChangeText(btStop, Resources.Reset);
            barStaticItem2.Caption =Resources.Path + ":";
            NameT.Caption = Resources.TechnicsWorked;
            Family.Caption = Resources.Driver;
            Date.Caption = Resources.Date;
            FactSquareCalc.Caption = "S, " + Resources.HectareBrief;
            colWork.Caption = Resources.WorkType;

            bbiRefresh.Caption = Resources.Refresh;
            bbiSpeedControl.Caption = Resources.SpeedControl;
            bbiSpeedControlSetup.Caption = Resources.SpeedParameters;
            bbiSquare.Caption = Resources.SquareCountour;
            bbiVideo.Caption = Resources.Video;

            bbiRefresh.Hint = Resources.Refresh;
            bbiSpeedControl.Hint = Resources.SpeedControl;
            bbiSpeedControlSetup.Hint = Resources.SpeedParameters;
            bbiSquare.Hint = Resources.SquareCountour;
            bbiVideo.Hint = Resources.Video;

            bchMarkers.Caption = Resources.MarkersControl;
            bchMarkers.Hint = Resources.MarkersSwitch;


        }
       #endregion

        #region Control buttons
        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DrawTracksInZone();
        }

        private void bbiSpeedControl_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DrawTracksInZoneWithSpeedControl();
        }

        private void bbiSpeedControlSetup_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowSpeedParameters();
        }

        private void bbiVideo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            VideoTrack();
        }

        private void bbiSquare_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowSquare();
        }

        private void bbiPrintReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintReport();
        }
        #endregion

    }

}
