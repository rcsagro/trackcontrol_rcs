namespace Agro.Controls
{
    partial class AgroGraphFuel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.Columns.GridColumn FuelStartD;
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcFuelFilling = new DevExpress.XtraGrid.GridControl();
            this.bgvFuelFilling = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLocation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDateFueling = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.teFueling = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFillingSystem = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFillingDisp = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLockRecord = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRemark = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFillingStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.tmDateFueling = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.deFueling = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gcFuel = new DevExpress.XtraGrid.GridControl();
            this.gvFuel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FNameF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FamilyF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStartDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEndDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceDUT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelSub = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelExpens = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelExpensAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelExpensAvgRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.gvFuelDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_node = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStartD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEndD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeRateD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FactSquareD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelAddD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelSubD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelEndD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FuelExpensD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensMoveD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensStopD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fuel_ExpensTotalD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gbFuelDrain = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gbAppproved = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            FuelStartD = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelFilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvFuelFilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFueling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmDateFueling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFueling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFueling.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDetal)).BeginInit();
            this.SuspendLayout();
            // 
            // FuelStartD
            // 
            FuelStartD.Name = "FuelStartD";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcFuel);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(959, 399);
            this.splitContainerControl1.SplitterPosition = 265;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gcFuelFilling);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(959, 265);
            this.splitContainerControl2.SplitterPosition = 685;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gcFuelFilling
            // 
            this.gcFuelFilling.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFuelFilling.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcFuelFilling_EmbeddedNavigator_ButtonClick);
            this.gcFuelFilling.Location = new System.Drawing.Point(0, 0);
            this.gcFuelFilling.MainView = this.bgvFuelFilling;
            this.gcFuelFilling.Name = "gcFuelFilling";
            this.gcFuelFilling.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.tmDateFueling,
            this.deFueling,
            this.teFueling});
            this.gcFuelFilling.Size = new System.Drawing.Size(268, 265);
            this.gcFuelFilling.TabIndex = 0;
            this.gcFuelFilling.UseEmbeddedNavigator = true;
            this.gcFuelFilling.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvFuelFilling});
            // 
            // bgvFuelFilling
            // 
            this.bgvFuelFilling.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.Empty.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvFuelFilling.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.bgvFuelFilling.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.EvenRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.EvenRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.EvenRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvFuelFilling.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvFuelFilling.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.bgvFuelFilling.Appearance.FixedLine.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.FocusedCell.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FocusedCell.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.bgvFuelFilling.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.bgvFuelFilling.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.FooterPanel.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvFuelFilling.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.bgvFuelFilling.Appearance.GroupButton.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.GroupFooter.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.bgvFuelFilling.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.GroupRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.GroupRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.GroupRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvFuelFilling.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvFuelFilling.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.HorzLine.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvFuelFilling.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvFuelFilling.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.OddRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.OddRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.OddRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.bgvFuelFilling.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.bgvFuelFilling.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.bgvFuelFilling.Appearance.Preview.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.Preview.Options.UseFont = true;
            this.bgvFuelFilling.Appearance.Preview.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.bgvFuelFilling.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.Row.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.Row.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.bgvFuelFilling.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.bgvFuelFilling.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.bgvFuelFilling.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.bgvFuelFilling.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.bgvFuelFilling.Appearance.SelectedRow.Options.UseForeColor = true;
            this.bgvFuelFilling.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.bgvFuelFilling.Appearance.TopNewRow.Options.UseBackColor = true;
            this.bgvFuelFilling.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.bgvFuelFilling.Appearance.VertLine.Options.UseBackColor = true;
            this.bgvFuelFilling.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gbFuelDrain,
            this.gbAppproved});
            this.bgvFuelFilling.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colId,
            this.colLocation,
            this.colDateFueling,
            this.colFillingStart,
            this.colFillingSystem,
            this.colFillingDisp,
            this.colLockRecord,
            this.colRemark});
            this.bgvFuelFilling.GridControl = this.gcFuelFilling;
            this.bgvFuelFilling.Name = "bgvFuelFilling";
            this.bgvFuelFilling.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.bgvFuelFilling.OptionsNavigation.AutoFocusNewRow = true;
            this.bgvFuelFilling.OptionsView.EnableAppearanceEvenRow = true;
            this.bgvFuelFilling.OptionsView.EnableAppearanceOddRow = true;
            this.bgvFuelFilling.OptionsView.ShowGroupPanel = false;
            this.bgvFuelFilling.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.bgvFuelFilling_FocusedRowChanged);
            this.bgvFuelFilling.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bgvFuelFilling_CellValueChanged);
            this.bgvFuelFilling.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.bgvFuelFilling_InitNewRow);
            this.bgvFuelFilling.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bgvFuelFilling_CellValueChanging);
            this.bgvFuelFilling.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.bgvFuelFilling_ShowingEditor);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.Caption = "�����";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.Visible = true;
            this.colLocation.Width = 271;
            // 
            // colDateFueling
            // 
            this.colDateFueling.AppearanceCell.Options.UseTextOptions = true;
            this.colDateFueling.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateFueling.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateFueling.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateFueling.Caption = "�����";
            this.colDateFueling.ColumnEdit = this.teFueling;
            this.colDateFueling.DisplayFormat.FormatString = "g";
            this.colDateFueling.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateFueling.FieldName = "DateFueling";
            this.colDateFueling.Name = "colDateFueling";
            this.colDateFueling.Visible = true;
            this.colDateFueling.Width = 231;
            // 
            // teFueling
            // 
            this.teFueling.AutoHeight = false;
            this.teFueling.DisplayFormat.FormatString = "g";
            this.teFueling.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teFueling.EditFormat.FormatString = "g";
            this.teFueling.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teFueling.Mask.EditMask = "g";
            this.teFueling.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.teFueling.Name = "teFueling";
            // 
            // colFillingSystem
            // 
            this.colFillingSystem.AppearanceCell.Options.UseTextOptions = true;
            this.colFillingSystem.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFillingSystem.AppearanceHeader.Options.UseTextOptions = true;
            this.colFillingSystem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFillingSystem.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFillingSystem.Caption = "�������";
            this.colFillingSystem.FieldName = "ValueSystem";
            this.colFillingSystem.Name = "colFillingSystem";
            this.colFillingSystem.OptionsColumn.AllowEdit = false;
            this.colFillingSystem.OptionsColumn.AllowFocus = false;
            this.colFillingSystem.OptionsColumn.ReadOnly = true;
            this.colFillingSystem.Visible = true;
            this.colFillingSystem.Width = 218;
            // 
            // colFillingDisp
            // 
            this.colFillingDisp.AppearanceCell.Options.UseTextOptions = true;
            this.colFillingDisp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFillingDisp.AppearanceHeader.Options.UseTextOptions = true;
            this.colFillingDisp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFillingDisp.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFillingDisp.Caption = "���������";
            this.colFillingDisp.FieldName = "ValueDisp";
            this.colFillingDisp.Name = "colFillingDisp";
            this.colFillingDisp.Visible = true;
            this.colFillingDisp.Width = 179;
            // 
            // colLockRecord
            // 
            this.colLockRecord.AppearanceHeader.Options.UseTextOptions = true;
            this.colLockRecord.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLockRecord.FieldName = "LockRecord";
            this.colLockRecord.Name = "colLockRecord";
            this.colLockRecord.OptionsColumn.ShowCaption = false;
            this.colLockRecord.Visible = true;
            this.colLockRecord.Width = 35;
            // 
            // colRemark
            // 
            this.colRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRemark.Caption = "����������";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.Visible = true;
            this.colRemark.Width = 316;
            // 
            // colFillingStart
            // 
            this.colFillingStart.Caption = "�� ��������, �";
            this.colFillingStart.FieldName = "ValueStart";
            this.colFillingStart.Name = "colFillingStart";
            // 
            // tmDateFueling
            // 
            this.tmDateFueling.AutoHeight = false;
            this.tmDateFueling.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tmDateFueling.DisplayFormat.FormatString = "g";
            this.tmDateFueling.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tmDateFueling.EditFormat.FormatString = "g";
            this.tmDateFueling.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tmDateFueling.Name = "tmDateFueling";
            // 
            // deFueling
            // 
            this.deFueling.AutoHeight = false;
            this.deFueling.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFueling.DisplayFormat.FormatString = "g";
            this.deFueling.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFueling.EditFormat.FormatString = "g";
            this.deFueling.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFueling.Name = "deFueling";
            this.deFueling.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gcFuel
            // 
            this.gcFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "DetalTime";
            this.gcFuel.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcFuel.Location = new System.Drawing.Point(0, 0);
            this.gcFuel.MainView = this.gvFuel;
            this.gcFuel.Name = "gcFuel";
            this.gcFuel.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1});
            this.gcFuel.Size = new System.Drawing.Size(959, 128);
            this.gcFuel.TabIndex = 32;
            this.gcFuel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuel,
            this.gvFuelDetal});
            // 
            // gvFuel
            // 
            this.gvFuel.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuel.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuel.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvFuel.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvFuel.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvFuel.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvFuel.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvFuel.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvFuel.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvFuel.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuel.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuel.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.OddRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvFuel.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvFuel.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvFuel.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuel.Appearance.Preview.Options.UseFont = true;
            this.gvFuel.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuel.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.Row.Options.UseBackColor = true;
            this.gvFuel.Appearance.Row.Options.UseForeColor = true;
            this.gvFuel.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuel.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvFuel.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvFuel.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuel.ColumnPanelRowHeight = 40;
            this.gvFuel.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdF,
            this.FNameF,
            this.FamilyF,
            this.TimeStartDUT,
            this.TimeEndDUT,
            this.DistanceDUT,
            this.FuelStart,
            this.FuelAdd,
            this.FuelSub,
            this.FuelEnd,
            this.FuelExpens,
            this.FuelExpensAvg,
            this.FuelExpensAvgRate});
            this.gvFuel.GridControl = this.gcFuel;
            this.gvFuel.Name = "gvFuel";
            this.gvFuel.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuel.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuel.OptionsView.ShowFooter = true;
            this.gvFuel.OptionsView.ShowGroupPanel = false;
            this.gvFuel.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvFuel_FocusedRowChanged);
            // 
            // IdF
            // 
            this.IdF.AppearanceHeader.Options.UseTextOptions = true;
            this.IdF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IdF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IdF.Caption = "IdF";
            this.IdF.FieldName = "Id";
            this.IdF.Name = "IdF";
            this.IdF.OptionsColumn.AllowEdit = false;
            this.IdF.OptionsColumn.AllowFocus = false;
            this.IdF.OptionsColumn.ReadOnly = true;
            // 
            // FNameF
            // 
            this.FNameF.AppearanceHeader.Options.UseTextOptions = true;
            this.FNameF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FNameF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FNameF.Caption = "����";
            this.FNameF.FieldName = "FName";
            this.FNameF.Name = "FNameF";
            this.FNameF.OptionsColumn.AllowEdit = false;
            this.FNameF.OptionsColumn.AllowFocus = false;
            this.FNameF.Visible = true;
            this.FNameF.VisibleIndex = 0;
            // 
            // FamilyF
            // 
            this.FamilyF.AppearanceHeader.Options.UseTextOptions = true;
            this.FamilyF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FamilyF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FamilyF.Caption = "��������";
            this.FamilyF.FieldName = "Family";
            this.FamilyF.Name = "FamilyF";
            this.FamilyF.OptionsColumn.AllowEdit = false;
            this.FamilyF.OptionsColumn.AllowFocus = false;
            this.FamilyF.SummaryItem.DisplayFormat = "�����";
            this.FamilyF.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.FamilyF.Visible = true;
            this.FamilyF.VisibleIndex = 1;
            // 
            // TimeStartDUT
            // 
            this.TimeStartDUT.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStartDUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStartDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStartDUT.Caption = "�����";
            this.TimeStartDUT.DisplayFormat.FormatString = "t";
            this.TimeStartDUT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStartDUT.FieldName = "TimeStart";
            this.TimeStartDUT.Name = "TimeStartDUT";
            this.TimeStartDUT.OptionsColumn.AllowEdit = false;
            this.TimeStartDUT.OptionsColumn.AllowFocus = false;
            this.TimeStartDUT.Visible = true;
            this.TimeStartDUT.VisibleIndex = 2;
            // 
            // TimeEndDUT
            // 
            this.TimeEndDUT.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEndDUT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEndDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEndDUT.Caption = "�����";
            this.TimeEndDUT.DisplayFormat.FormatString = "t";
            this.TimeEndDUT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndDUT.FieldName = "TimeEnd";
            this.TimeEndDUT.Name = "TimeEndDUT";
            this.TimeEndDUT.OptionsColumn.AllowEdit = false;
            this.TimeEndDUT.OptionsColumn.AllowFocus = false;
            this.TimeEndDUT.Visible = true;
            this.TimeEndDUT.VisibleIndex = 3;
            // 
            // DistanceDUT
            // 
            this.DistanceDUT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceDUT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceDUT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DistanceDUT.Caption = "���������� ����, ��";
            this.DistanceDUT.FieldName = "Distance";
            this.DistanceDUT.Name = "DistanceDUT";
            this.DistanceDUT.OptionsColumn.AllowEdit = false;
            this.DistanceDUT.OptionsColumn.AllowFocus = false;
            this.DistanceDUT.SummaryItem.DisplayFormat = "{0:#.##}";
            this.DistanceDUT.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.DistanceDUT.Visible = true;
            this.DistanceDUT.VisibleIndex = 4;
            // 
            // FuelStart
            // 
            this.FuelStart.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelStart.Caption = "������� � ������, �";
            this.FuelStart.FieldName = "FuelStart";
            this.FuelStart.Name = "FuelStart";
            this.FuelStart.OptionsColumn.AllowEdit = false;
            this.FuelStart.OptionsColumn.AllowFocus = false;
            this.FuelStart.Visible = true;
            this.FuelStart.VisibleIndex = 5;
            this.FuelStart.Width = 73;
            // 
            // FuelAdd
            // 
            this.FuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAdd.Caption = "����������, �";
            this.FuelAdd.FieldName = "FuelAdd";
            this.FuelAdd.Name = "FuelAdd";
            this.FuelAdd.OptionsColumn.AllowEdit = false;
            this.FuelAdd.OptionsColumn.AllowFocus = false;
            this.FuelAdd.OptionsColumn.ReadOnly = true;
            this.FuelAdd.SummaryItem.DisplayFormat = "{0:#.##}";
            this.FuelAdd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.FuelAdd.Visible = true;
            this.FuelAdd.VisibleIndex = 6;
            this.FuelAdd.Width = 81;
            // 
            // FuelSub
            // 
            this.FuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSub.Caption = "�����, �";
            this.FuelSub.FieldName = "FuelSub";
            this.FuelSub.Name = "FuelSub";
            this.FuelSub.OptionsColumn.AllowEdit = false;
            this.FuelSub.OptionsColumn.AllowFocus = false;
            this.FuelSub.OptionsColumn.ReadOnly = true;
            this.FuelSub.SummaryItem.DisplayFormat = "{0:#.##}";
            this.FuelSub.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.FuelSub.Visible = true;
            this.FuelSub.VisibleIndex = 7;
            this.FuelSub.Width = 62;
            // 
            // FuelEnd
            // 
            this.FuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEnd.Caption = "������� � �����, �";
            this.FuelEnd.FieldName = "FuelEnd";
            this.FuelEnd.Name = "FuelEnd";
            this.FuelEnd.OptionsColumn.AllowEdit = false;
            this.FuelEnd.OptionsColumn.AllowFocus = false;
            this.FuelEnd.Visible = true;
            this.FuelEnd.VisibleIndex = 8;
            this.FuelEnd.Width = 68;
            // 
            // FuelExpens
            // 
            this.FuelExpens.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpens.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpens.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpens.Caption = "����� ������, �";
            this.FuelExpens.FieldName = "FuelExpens";
            this.FuelExpens.Name = "FuelExpens";
            this.FuelExpens.OptionsColumn.AllowEdit = false;
            this.FuelExpens.OptionsColumn.AllowFocus = false;
            this.FuelExpens.SummaryItem.DisplayFormat = "{0:#.##}";
            this.FuelExpens.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.FuelExpens.Visible = true;
            this.FuelExpens.VisibleIndex = 9;
            this.FuelExpens.Width = 89;
            // 
            // FuelExpensAvg
            // 
            this.FuelExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvg.Caption = "�������  ������, �/��";
            this.FuelExpensAvg.FieldName = "FuelExpensAvg";
            this.FuelExpensAvg.Name = "FuelExpensAvg";
            this.FuelExpensAvg.OptionsColumn.AllowEdit = false;
            this.FuelExpensAvg.OptionsColumn.AllowFocus = false;
            this.FuelExpensAvg.Visible = true;
            this.FuelExpensAvg.VisibleIndex = 10;
            this.FuelExpensAvg.Width = 71;
            // 
            // FuelExpensAvgRate
            // 
            this.FuelExpensAvgRate.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvgRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvgRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvgRate.Caption = "������� ������ �/�������";
            this.FuelExpensAvgRate.FieldName = "FuelExpensAvgRate";
            this.FuelExpensAvgRate.Name = "FuelExpensAvgRate";
            this.FuelExpensAvgRate.OptionsColumn.AllowEdit = false;
            this.FuelExpensAvgRate.OptionsColumn.AllowFocus = false;
            this.FuelExpensAvgRate.Visible = true;
            this.FuelExpensAvgRate.VisibleIndex = 11;
            this.FuelExpensAvgRate.Width = 126;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Appearance.BackColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ShowTitle = true;
            this.repositoryItemProgressBar1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            // 
            // gvFuelDetal
            // 
            this.gvFuelDetal.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvFuelDetal.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gvFuelDetal.ColumnPanelRowHeight = 40;
            this.gvFuelDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdD,
            this.Id_node,
            this.TimeStartD,
            this.TimeEndD,
            this.TimeRateD,
            this.FactSquareD,
            FuelStartD,
            this.FuelAddD,
            this.FuelSubD,
            this.FuelEndD,
            this.FuelExpensD,
            this.Fuel_ExpensMoveD,
            this.Fuel_ExpensStopD,
            this.Fuel_ExpensTotalD});
            this.gvFuelDetal.GridControl = this.gcFuel;
            this.gvFuelDetal.Name = "gvFuelDetal";
            this.gvFuelDetal.OptionsBehavior.Editable = false;
            this.gvFuelDetal.OptionsView.ShowGroupPanel = false;
            // 
            // IdD
            // 
            this.IdD.Caption = "IdD";
            this.IdD.FieldName = "Id";
            this.IdD.Name = "IdD";
            // 
            // Id_node
            // 
            this.Id_node.Caption = "Id_node";
            this.Id_node.FieldName = "Id_node";
            this.Id_node.Name = "Id_node";
            // 
            // TimeStartD
            // 
            this.TimeStartD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStartD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStartD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStartD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStartD.Caption = "�����";
            this.TimeStartD.DisplayFormat.FormatString = "t";
            this.TimeStartD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStartD.FieldName = "TimeStart";
            this.TimeStartD.Name = "TimeStartD";
            this.TimeStartD.Visible = true;
            this.TimeStartD.VisibleIndex = 0;
            this.TimeStartD.Width = 61;
            // 
            // TimeEndD
            // 
            this.TimeEndD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEndD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEndD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEndD.Caption = "�����";
            this.TimeEndD.DisplayFormat.FormatString = "t";
            this.TimeEndD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndD.FieldName = "TimeEnd";
            this.TimeEndD.Name = "TimeEndD";
            this.TimeEndD.Visible = true;
            this.TimeEndD.VisibleIndex = 1;
            this.TimeEndD.Width = 70;
            // 
            // TimeRateD
            // 
            this.TimeRateD.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRateD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRateD.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRateD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRateD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRateD.Caption = "��������";
            this.TimeRateD.FieldName = "TimeRate";
            this.TimeRateD.Name = "TimeRateD";
            this.TimeRateD.Visible = true;
            this.TimeRateD.VisibleIndex = 2;
            this.TimeRateD.Width = 78;
            // 
            // FactSquareD
            // 
            this.FactSquareD.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareD.Caption = "���������, ��";
            this.FactSquareD.FieldName = "FactSquare";
            this.FactSquareD.Name = "FactSquareD";
            this.FactSquareD.Visible = true;
            this.FactSquareD.VisibleIndex = 3;
            this.FactSquareD.Width = 106;
            // 
            // FuelAddD
            // 
            this.FuelAddD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAddD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAddD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAddD.Caption = "����������, �";
            this.FuelAddD.FieldName = "FuelAdd";
            this.FuelAddD.Name = "FuelAddD";
            this.FuelAddD.Visible = true;
            this.FuelAddD.VisibleIndex = 4;
            this.FuelAddD.Width = 104;
            // 
            // FuelSubD
            // 
            this.FuelSubD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSubD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSubD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSubD.Caption = "�����, �";
            this.FuelSubD.FieldName = "FuelSub";
            this.FuelSubD.Name = "FuelSubD";
            this.FuelSubD.Visible = true;
            this.FuelSubD.VisibleIndex = 5;
            this.FuelSubD.Width = 76;
            // 
            // FuelEndD
            // 
            this.FuelEndD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEndD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEndD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEndD.Caption = "������� � �����, �";
            this.FuelEndD.FieldName = "FuelEnd";
            this.FuelEndD.Name = "FuelEndD";
            this.FuelEndD.Visible = true;
            this.FuelEndD.VisibleIndex = 6;
            this.FuelEndD.Width = 110;
            // 
            // FuelExpensD
            // 
            this.FuelExpensD.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensD.Caption = "����� ������, �";
            this.FuelExpensD.FieldName = "FuelExpens";
            this.FuelExpensD.Name = "FuelExpensD";
            this.FuelExpensD.Visible = true;
            this.FuelExpensD.VisibleIndex = 7;
            this.FuelExpensD.Width = 108;
            // 
            // Fuel_ExpensMoveD
            // 
            this.Fuel_ExpensMoveD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMoveD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensMoveD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensMoveD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMoveD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMoveD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMoveD.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMoveD.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMoveD.Name = "Fuel_ExpensMoveD";
            this.Fuel_ExpensMoveD.Visible = true;
            this.Fuel_ExpensMoveD.VisibleIndex = 8;
            this.Fuel_ExpensMoveD.Width = 113;
            // 
            // Fuel_ExpensStopD
            // 
            this.Fuel_ExpensStopD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStopD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensStopD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensStopD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStopD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStopD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStopD.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStopD.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStopD.Name = "Fuel_ExpensStopD";
            this.Fuel_ExpensStopD.Visible = true;
            this.Fuel_ExpensStopD.VisibleIndex = 9;
            this.Fuel_ExpensStopD.Width = 118;
            // 
            // Fuel_ExpensTotalD
            // 
            this.Fuel_ExpensTotalD.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotalD.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Fuel_ExpensTotalD.AppearanceCell.Options.UseBackColor = true;
            this.Fuel_ExpensTotalD.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotalD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotalD.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotalD.Caption = "����� ������, �";
            this.Fuel_ExpensTotalD.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotalD.Name = "Fuel_ExpensTotalD";
            this.Fuel_ExpensTotalD.Visible = true;
            this.Fuel_ExpensTotalD.VisibleIndex = 10;
            this.Fuel_ExpensTotalD.Width = 119;
            // 
            // gbFuelDrain
            // 
            this.gbFuelDrain.AppearanceHeader.Options.UseTextOptions = true;
            this.gbFuelDrain.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbFuelDrain.Caption = "���������� / �����";
            this.gbFuelDrain.Columns.Add(this.colId);
            this.gbFuelDrain.Columns.Add(this.colLocation);
            this.gbFuelDrain.Columns.Add(this.colDateFueling);
            this.gbFuelDrain.Columns.Add(this.colFillingSystem);
            this.gbFuelDrain.Columns.Add(this.colFillingDisp);
            this.gbFuelDrain.MinWidth = 20;
            this.gbFuelDrain.Name = "gbFuelDrain";
            this.gbFuelDrain.Width = 899;
            // 
            // gbAppproved
            // 
            this.gbAppproved.AppearanceHeader.Options.UseTextOptions = true;
            this.gbAppproved.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gbAppproved.Caption = "����������";
            this.gbAppproved.Columns.Add(this.colLockRecord);
            this.gbAppproved.Columns.Add(this.colRemark);
            this.gbAppproved.MinWidth = 20;
            this.gbAppproved.Name = "gbAppproved";
            this.gbAppproved.Width = 351;
            // 
            // AgroGraphFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "AgroGraphFuel";
            this.Size = new System.Drawing.Size(959, 399);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelFilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvFuelFilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFueling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmDateFueling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFueling.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFueling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelDetal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gcFuel;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuel;
        private DevExpress.XtraGrid.Columns.GridColumn IdF;
        private DevExpress.XtraGrid.Columns.GridColumn FNameF;
        private DevExpress.XtraGrid.Columns.GridColumn FamilyF;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStartDUT;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEndDUT;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceDUT;
        private DevExpress.XtraGrid.Columns.GridColumn FuelStart;
        private DevExpress.XtraGrid.Columns.GridColumn FuelAdd;
        private DevExpress.XtraGrid.Columns.GridColumn FuelSub;
        private DevExpress.XtraGrid.Columns.GridColumn FuelEnd;
        private DevExpress.XtraGrid.Columns.GridColumn FuelExpens;
        private DevExpress.XtraGrid.Columns.GridColumn FuelExpensAvg;
        private DevExpress.XtraGrid.Columns.GridColumn FuelExpensAvgRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelDetal;
        private DevExpress.XtraGrid.Columns.GridColumn IdD;
        private DevExpress.XtraGrid.Columns.GridColumn Id_node;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStartD;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEndD;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRateD;
        private DevExpress.XtraGrid.Columns.GridColumn FactSquareD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelAddD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelSubD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelEndD;
        private DevExpress.XtraGrid.Columns.GridColumn FuelExpensD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensMoveD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensStopD;
        private DevExpress.XtraGrid.Columns.GridColumn Fuel_ExpensTotalD;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gcFuelFilling;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvFuelFilling;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLocation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDateFueling;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFillingSystem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFillingDisp;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLockRecord;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRemark;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit tmDateFueling;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deFueling;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teFueling;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFillingStart;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbFuelDrain;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gbAppproved;
    }
}
