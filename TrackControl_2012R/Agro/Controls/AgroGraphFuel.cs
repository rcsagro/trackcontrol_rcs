using System.Linq;
using Agro.Properties;
using BaseReports.Procedure;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Graph;
using LocalCache;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;


namespace Agro.Controls
{
    public partial class AgroGraphFuel : DevExpress.XtraEditors.XtraUserControl
    {
        readonly OrderItem _oi;
        ZGraphControl _zGraphFuel;
        readonly bool _readOnly;
        bool _isGraphPainted;
        DateTime _prevDateFueling;
        atlantaDataSet _atlDataset;
        atlantaDataSet.mobitelsRow _mRow;
        bool _secondGraphMouseClick;
        double _beginValue;
        PointLatLng _graphPoint;

        public AgroGraphFuel(OrderItem oi, bool readOnly)
        {
            _oi = oi;
            _readOnly = readOnly;
            InitializeComponent();
            Localization();
            ShowGraphFuel();
            if (_readOnly)
            {
                gcFuel.Enabled = false;
                gcFuelFilling.Enabled = false; 
            }
        }

        /// <summary>
        /// �������� ������� ������� �� ������� ������
        /// </summary>
        public void LoadSubFormFuel_DUT()
        {

            LoadFuelRecords();
            gcFuelFilling.DataSource = _oi.GetFuelDUTFueling();  
        }

        private void LoadFuelRecords()
        {
            gcFuel.DataSource = _oi.GetFuelDUT();
        }

        public GridView GridViewFuel
        {
            get { return gvFuel; }
        }
        
        private void Localization()
        {
            FNameF.Caption = Resources.Field;
            FamilyF.Caption = Resources.Driver ;
            TimeStartDUT.Caption = Resources.Entry;
            TimeEndDUT.Caption = Resources.Exit ;
            DistanceDUT.Caption = Resources.PathKm;
            FuelStart.Caption = Resources.FuelStartL;
            FuelAdd.Caption = Resources.FuelAddL ;
            FuelSub.Caption = Resources.FuelSubL;
            FuelEnd.Caption = Resources.FuelEndL ;
            FuelExpens.Caption = Resources.FuelExpenseTotalL;
            FuelExpensAvg.Caption  = Resources.FuelExpenseAvgLGa ;
            FuelExpensAvgRate.Caption  = Resources.FuelExpenseAvgLMHour;
            TimeStartD.Caption  = Resources.Entry;
            TimeEndD.Caption  = Resources.Exit;
            TimeRateD.Caption  = Resources.MotoHour;
            FactSquareD.Caption  = Resources.ExecuteGa;
            FuelAddD.Caption  = Resources.FuelAddL;
            FuelSubD.Caption  = Resources.FuelSubL;
            FuelEndD.Caption  = Resources.FuelEndL;
            FuelExpensD.Caption  = Resources.FuelExpenseTotalL;
            Fuel_ExpensMoveD.Caption  = Resources.FuelExpenseMovingL;
            Fuel_ExpensStopD.Caption  = Resources.FuelExpenseStopL;
            Fuel_ExpensTotalD.Caption  = Resources.FuelExpenseTotalL;

            colLocation.Caption = Resources.Lockation;
            colDateFueling.Caption = Resources.Time;
            colFillingSystem.Caption = Resources.System;
            colFillingDisp.Caption = Resources.Dispetcher;
            colRemark.Caption = Resources.Remark;
            gbFuelDrain.Caption = Resources.FuelDrain;
            gbAppproved.Caption = Resources.Approved; 

        }

        private void ShowGraphFuel()
        {
            _zGraphFuel = new ZGraphControl() ;
            _zGraphFuel.SetRunVisible();
            splitContainerControl2.Panel1.Controls.Add(_zGraphFuel);
            _zGraphFuel.Dock = DockStyle.Fill; 
            _zGraphFuel.ClearGraphZoom();
            _zGraphFuel.ClearRegion();
            _zGraphFuel.ShowSeries("");//Resources.FuelGraph
            _zGraphFuel.StartView += OnStartView;
             
        }

        void OnStartView()
        {
            _oi.ChangeStatusEvent += onSetStatusInParentForm;
            if (_oi.Create_dsAtlanta(_oi.Date, _oi.Date.AddDays(1).AddMinutes(-1)) > 0)
            {
                var buildGraph = new BuildGraphs();
                _atlDataset = _oi.GetDataset();
                _mRow = (atlantaDataSet.mobitelsRow)_atlDataset.mobitels.FindByMobitel_ID(_oi.Mobitel_Id);
                // ����� ����� ��� ��������� GetKilometrageReportRows() � ��������� ���������� �������� ������� �������
                var brKlm = (BaseReports.Procedure.IAlgorithm)new BaseReports.Procedure.Kilometrage();
                brKlm.SelectItem(_mRow);
                brKlm.Run();
                buildGraph.AddGraphFuel(AlgorithmType.FUEL1, _zGraphFuel, _atlDataset, _mRow);
                AddSensorsGraphs(buildGraph);
                buildGraph.AddGraphSensorUnloading(_zGraphFuel, _mRow.Mobitel_ID);
                _isGraphPainted = true;
                DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                double[] speeds = Algorithm.GpsDatas.Select(gps => (double)gps.Speed).ToArray();
                _zGraphFuel.AddSeriesR(Resources.Speed , Color.LightCoral, speeds,
                            times, AlgorithmType.SPEED, true);

                // ������ ������ ��������.
                if (_atlDataset.TachometerValue.SelectByMobitel(_mRow).Length > 0)
                {
                    _zGraphFuel.AddSeriesR(Resources.RotationCtrlCaption, Color.CornflowerBlue,
                        _atlDataset.TachometerValue.SelectByMobitel(_mRow), times,
                        AlgorithmType.ROTATE_E, true);
                }
                _zGraphFuel.ShowSeries("");//Resources.FuelGraph

                SetTimeRegion();
            }
            //_oi.ContentDataSetReturnClear(); 
            _oi.ChangeStatusEvent -= onSetStatusInParentForm;
            onSetStatusInParentForm(Resources.Ready); 
        }

        private void AddSensorsGraphs(BuildGraphs buildGraph)
        {
            var sensorAngle = new SensorCalibrated((int) AlgorithmType.ANGLE, _mRow.Mobitel_ID);
            if (sensorAngle.Id > 0)
            {
                double[] sensorValuesAngle = _oi.GetSensorsCalibratedData((int) AlgorithmType.ANGLE, sensorAngle,
                                                                          Algorithm.GpsDatas);
                buildGraph.AddGraphSensorAgregat(_zGraphFuel, sensorAngle, sensorValuesAngle);
            }
            var sensorRf = new SensorCalibrated((int)AlgorithmType.RANGEFINDER, _mRow.Mobitel_ID);
            if (sensorRf.Id > 0)
            {
                double[] sensorValuesRf = _oi.GetSensorsCalibratedData((int)AlgorithmType.RANGEFINDER, sensorRf,
                                                                       Algorithm.GpsDatas);
                buildGraph.AddGraphSensorAgregat(_zGraphFuel, sensorRf, sensorValuesRf);
            }
            //buildGraph.AddGraphSensorAngle(_zGraphFuel, _mRow.Mobitel_ID);
            //buildGraph.AddGraphSensorRangeFinder(_zGraphFuel, _mRow.Mobitel_ID);
            buildGraph.AddGraphSensorAgregatLogic(_zGraphFuel, _mRow.Mobitel_ID);
        }

        void SetTimeRegion()
        {
            if (!_isGraphPainted) return;
            _zGraphFuel.ClearRegion();
            DateTime startTime;
            DateTime endTime;
            if (gvFuel.RowCount == 0) return;
            if (DateTime.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, TimeStartDUT).ToString(), out startTime)
                && DateTime.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, TimeEndDUT).ToString(), out endTime))
                _zGraphFuel.AddTimeRegion(startTime, endTime);
        }

        void SetFuelLabel(int RowHandle)
        {
            _zGraphFuel.ClearLabel();
            int id = 0;
            DateTime dateFueling;
            if (IsRecordValid(RowHandle, out id, out dateFueling))
            {
                double valueDisp;
                double valueSystem;
                double valueStart;
                if (double.TryParse(bgvFuelFilling.GetRowCellValue(RowHandle, colFillingDisp).ToString(), out valueDisp)
                    && double.TryParse(bgvFuelFilling.GetRowCellValue(RowHandle, colFillingSystem).ToString(), out valueSystem)
                    && double.TryParse(bgvFuelFilling.GetRowCellValue(RowHandle, colFillingStart).ToString(), out valueStart))
                {
                    double valueToLabel = valueDisp != 0 ? valueDisp : valueSystem;
                    string val;
                    Color color;
                    if (valueToLabel > 0)
                      {
                          val = String.Format("{0} {1}", Resources.Fuelling, valueToLabel);
                          color = Color.Green;
                      }
                      else
                      {
                          val = String.Format("{0} {1}", Resources.FuelLarceny, valueToLabel);
                          color = Color.Red;
                      }
                    valueToLabel += valueStart;
                    _zGraphFuel.AddLabel(dateFueling, valueStart, valueToLabel, val, color);
                    _zGraphFuel.SellectZoom();
                }
            }
        }


        private void onSetStatusInParentForm(string sMessage)
        {
            ((Order)ParentForm).SetStatus(sMessage);  
        }

        private bool IsRecordLocked(int row)
        {
            bool locked = false;
            if (bgvFuelFilling.GetRowCellValue(row, colLockRecord) == null) return false;
            if (bool.TryParse(Convert.ToString(bgvFuelFilling.GetRowCellValue(row, colLockRecord)), out locked))
                return locked;
            else
                return false;
        }

        private bool IsRecordNew(int row)
        {
            int id = 0;
            if (Int32.TryParse(bgvFuelFilling.GetRowCellValue(row, colId).ToString(), out id))
                if (id > 0)
                    return false;
                else
                    return true;
            else
                return true;
        }

        private bool IsExistSystemValue(int row)
        {
            if (bgvFuelFilling.GetRowCellValue(row, colFillingSystem) == null) return false;
            double valueSystem = 0;
            if (double.TryParse(bgvFuelFilling.GetRowCellValue(row, colFillingSystem).ToString(), out valueSystem))
                if (valueSystem == 0)
                    return false;
                else
                    return true;
            else
                return false;
        }

        private bool IsRecordValid(int RowHandle, out int id, out DateTime dateFueling)
        {
            id = 0;
            dateFueling = DateTime.MinValue;
            if (bgvFuelFilling.GetRowCellValue(RowHandle, colId) == null) return false;
            if (Int32.TryParse(bgvFuelFilling.GetRowCellValue(RowHandle, colId).ToString(), out id)
                && DateTime.TryParse(bgvFuelFilling.GetRowCellValue(RowHandle, colDateFueling).ToString(), out dateFueling))
            {
                return true;
            }
            else
                return false;
        }

        #region GridView
        private void gvFuel_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SetTimeRegion();
        }

		private void bgvFuelFilling_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int id = 0;
            DateTime dateFueling;
            if (IsRecordValid(e.RowHandle, out id, out dateFueling))
            {
                var of = new OrderFueling(_oi.ID);
                if (e.Column == colFillingDisp)
                {
                    double valueDisp;
                    double valueStart;
                    if (double.TryParse(bgvFuelFilling.GetRowCellValue(e.RowHandle, colFillingDisp).ToString(), out valueDisp)
                        && double.TryParse(bgvFuelFilling.GetRowCellValue(e.RowHandle, colFillingStart).ToString(), out valueStart))
                    {
                        if (IsRecordNew(e.RowHandle))
                        {
                            DataRow row = bgvFuelFilling.GetDataRow(e.RowHandle);
                            row["Id"] = of.AddRecord(_graphPoint, dateFueling, valueStart, 0, valueDisp, bgvFuelFilling.GetRowCellValue(e.RowHandle, colLocation).ToString());
                        }
                        else
                        {
                            of.UpdateValueDisp(id, valueDisp, dateFueling);
                        }
                        if (of.UpdateOrderRecord(dateFueling)) LoadFuelRecords();
                        SetFuelLabel(e.RowHandle); 
                    }
                }
                else if (e.Column == colRemark)
                {
                        of.UpdateRemark(id, bgvFuelFilling.GetRowCellValue(e.RowHandle, colRemark).ToString(), dateFueling);
                }
                else if (e.Column == colDateFueling)
                {
                    of.UpdateDateFueling(id, dateFueling); 
                    of.UpdateOrderRecord(_prevDateFueling);
                    if (of.UpdateOrderRecord(dateFueling)) LoadFuelRecords();
                    SetFuelLabel(e.RowHandle); 
                }
            }
        }

        private void bgvFuelFilling_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int id;
            DateTime dateFueling;
            if (IsRecordValid(e.RowHandle, out id, out dateFueling) && id > 0)
            {
                OrderFueling of = new OrderFueling(_oi.ID);
                if (e.Column == colLockRecord)
                {
                    bool locked;
                    if (bool.TryParse(bgvFuelFilling.GetRowCellValue(e.RowHandle, colLockRecord).ToString(), out locked))
                    {
                        
                        of.UpdateLock(id, locked, dateFueling);
                    }
                }
                else if (e.Column == colDateFueling)
                {
                    _prevDateFueling = dateFueling;
                }
            }
        }

        private void bgvFuelFilling_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedColumn.FieldName == "ValueDisp" && IsRecordLocked(view.FocusedRowHandle))
                e.Cancel = true;
            if (view.FocusedColumn.FieldName == "DateFueling" && (IsRecordLocked(view.FocusedRowHandle) || IsExistSystemValue(view.FocusedRowHandle)))
                e.Cancel = true;
        }

        private void bgvFuelFilling_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            DataRow row = bgvFuelFilling.GetDataRow(e.RowHandle);
            row["DateFueling"] = _oi.Date.AddHours(12) ;
            row["ValueSystem"] = 0;
            row["ValueStart"] = 0;
            row["ValueDisp"] = 0;
            row["LockRecord"] = 0;
            row["Id"] = 0;
            row["Lat"] = 0;
            row["Lng"] = 0;

            _zGraphFuel.Action += new ActionEventHandler(ZGraphControl_Action);
            _zGraphFuel.AddMouseEventHandler();

            XtraMessageBox.Show(Resources.FuelCtrlAddFuellingStartText, Resources.FuelCtrlAddFuellingCaption);
        }

        private void gcFuelFilling_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

            if (e.Button.ButtonType == NavigatorButtonType.Remove )
            {
                int id;
                DateTime dateFueling;
                if (IsRecordLocked(bgvFuelFilling.FocusedRowHandle)) 
                {
                    e.Handled = true;
                    return;
                }
                    if (IsExistSystemValue(bgvFuelFilling.FocusedRowHandle))
                {
                    XtraMessageBox.Show(Resources.BanDeleteSystemInfor, Resources.ApplicationName);
                    e.Handled = true;
                    return;
                }
                    if (IsRecordValid(bgvFuelFilling.FocusedRowHandle, out id, out dateFueling) && id > 0 )
                {
                    double valueDisp;
                    if (double.TryParse(bgvFuelFilling.GetRowCellValue(bgvFuelFilling.FocusedRowHandle, colFillingDisp).ToString(), out valueDisp))
                    {
                           if (DialogResult.Yes == XtraMessageBox.Show(Resources.DeleteQuestion, Resources.ApplicationName,MessageBoxButtons.YesNo ))
                           {
                            var of = new OrderFueling(_oi.ID);
                            if (of.DeleteRecord(id, valueDisp, dateFueling))
                            {
                                if (of.UpdateOrderRecord(dateFueling)) LoadFuelRecords();
                                e.Handled = false;
                            }
                           }
                    }
                }
                e.Handled = false;
            }
        }

	    private void bgvFuelFilling_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SetFuelLabel(e.FocusedRowHandle); 
        }
        #endregion

        private void ZGraphControl_Action(object sender, ActionCancelEventArgs ev)
        {
            //System.Diagnostics.Trace.WriteLine(ev.Point.time.ToString());
            //System.Diagnostics.Trace.WriteLine(ev.Point.value.ToString());
            //������!
            if (!_secondGraphMouseClick)
            {
                //atlantaDataSet.dataviewRow dRow = _atlDataset.dataview.FindByTime(_mRow, ev.Point.time);
                var gpsData = Algorithm.GpsDatas.FirstOrDefault(gps => gps.Time == ev.Point.time);
                if (gpsData != null)
                {
                    _graphPoint = gpsData.LatLng;
                    bgvFuelFilling.SetRowCellValue(bgvFuelFilling.FocusedRowHandle, colDateFueling, ev.Point.time);
                    bgvFuelFilling.SetRowCellValue(bgvFuelFilling.FocusedRowHandle, colFillingStart,
                                                   Math.Round(ev.Point.value, 2));
                    bgvFuelFilling.SetRowCellValue(bgvFuelFilling.FocusedRowHandle, colLocation,
                                                   Algorithm.FindLocation(gpsData.LatLng));
                    _beginValue = ev.Point.value;
                    _secondGraphMouseClick = true;
                    XtraMessageBox.Show(Resources.FuelCtrlAddFuellingFinishText, Resources.FuelCtrlAddFuellingCaption);
                }
        }
            else
            {
                bgvFuelFilling.SetRowCellValue(bgvFuelFilling.FocusedRowHandle, colFillingDisp , Math.Round(ev.Point.value - _beginValue,2));
                _zGraphFuel.RemoveMouseEventHandler();
                _zGraphFuel.Action -= ZGraphControl_Action;
                _secondGraphMouseClick = false;
                _zGraphFuel.ClearLabel(); 
                SetFuelLabel(bgvFuelFilling.FocusedRowHandle); 
            }
        }
    }
}
