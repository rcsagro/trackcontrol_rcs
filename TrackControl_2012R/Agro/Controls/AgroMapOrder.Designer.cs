namespace Agro.Controls
{
    sealed partial class AgroMapOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            } 
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgroMapOrder));
            this.barMap = new DevExpress.XtraBars.Bar();
            this.pnGIS = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcMap = new DevExpress.XtraGrid.GridControl();
            this.gvMap = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdOrdert = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_zone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointsValidity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icValidity = new DevExpress.Utils.ImageCollection(this.components);
            this.colLockRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbLock = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.NameF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Driver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Confirm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Work = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Square = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Distance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Width_s = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.iiValidity = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.ichEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSplitRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bchMarkers = new DevExpress.XtraBars.BarCheckItem();
            this.bchDgps = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSetupSensorAngle = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewSensorAngle = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iiValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ichEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // barMap
            // 
            this.barMap.BarName = "Main menu";
            this.barMap.DockCol = 0;
            this.barMap.DockRow = 0;
            this.barMap.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.barMap.OptionsBar.MultiLine = true;
            this.barMap.OptionsBar.UseWholeRow = true;
            this.barMap.Text = "Main menu";
            // 
            // pnGIS
            // 
            this.pnGIS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGIS.Location = new System.Drawing.Point(0, 0);
            this.pnGIS.Name = "pnGIS";
            this.pnGIS.Size = new System.Drawing.Size(397, 319);
            this.pnGIS.TabIndex = 7;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(32, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pnGIS);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcMap);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(797, 319);
            this.splitContainerControl1.SplitterPosition = 397;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gcMap
            // 
            this.gcMap.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gcMap.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcMap.Location = new System.Drawing.Point(0, 0);
            this.gcMap.MainView = this.gvMap;
            this.gcMap.Name = "gcMap";
            this.gcMap.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.WorkLookUp,
            this.icbValidity,
            this.repositoryItemProgressBar1,
            this.iiValidity,
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox2,
            this.icbLock,
            this.ichEdit});
            this.gcMap.Size = new System.Drawing.Size(395, 319);
            this.gcMap.TabIndex = 6;
            this.gcMap.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMap});
            // 
            // gvMap
            // 
            this.gvMap.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdOrdert,
            this.Id_zone,
            this.colPointsValidity,
            this.colLockRecord,
            this.NameF,
            this.Driver,
            this.TimeStart,
            this.TimeEnd,
            this.Time,
            this.Confirm,
            this.Work,
            this.Square,
            this.Distance,
            this.Width_s});
            this.gvMap.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gvMap.GridControl = this.gcMap;
            this.gvMap.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvMap.Images = this.icValidity;
            this.gvMap.IndicatorWidth = 30;
            this.gvMap.Name = "gvMap";
            this.gvMap.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvMap.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvMap.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvMap.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvMap.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvMap_CustomDrawRowIndicator);
            this.gvMap.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvMap_RowCellStyle);
            this.gvMap.ShowGridMenu += new DevExpress.XtraGrid.Views.Grid.GridMenuEventHandler(this.gvMap_ShowGridMenu);
            this.gvMap.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gvMap_ShowingEditor);
            this.gvMap.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvMap_FocusedRowChanged);
            this.gvMap.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvMap_CellValueChanging);
            this.gvMap.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvMap_InvalidRowException);
            this.gvMap.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvMap_ValidateRow);
            this.gvMap.Click += new System.EventHandler(this.gvMap_Click);
            this.gvMap.DoubleClick += new System.EventHandler(this.gvMap_DoubleClick);
            // 
            // colIdOrdert
            // 
            this.colIdOrdert.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdOrdert.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdOrdert.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdOrdert.Caption = "Id_ordert";
            this.colIdOrdert.FieldName = "Id";
            this.colIdOrdert.Name = "colIdOrdert";
            this.colIdOrdert.OptionsColumn.AllowEdit = false;
            this.colIdOrdert.OptionsColumn.AllowFocus = false;
            this.colIdOrdert.OptionsColumn.ReadOnly = true;
            // 
            // Id_zone
            // 
            this.Id_zone.Caption = "����";
            this.Id_zone.FieldName = "Id_zone";
            this.Id_zone.Name = "Id_zone";
            this.Id_zone.OptionsColumn.AllowEdit = false;
            this.Id_zone.OptionsColumn.ReadOnly = true;
            // 
            // colPointsValidity
            // 
            this.colPointsValidity.AppearanceCell.Options.UseTextOptions = true;
            this.colPointsValidity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPointsValidity.ColumnEdit = this.icbValidity;
            this.colPointsValidity.FieldName = "PointsValidity";
            this.colPointsValidity.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colPointsValidity.Name = "colPointsValidity";
            this.colPointsValidity.OptionsColumn.AllowEdit = false;
            this.colPointsValidity.OptionsColumn.AllowFocus = false;
            this.colPointsValidity.OptionsColumn.ReadOnly = true;
            this.colPointsValidity.OptionsColumn.ShowCaption = false;
            this.colPointsValidity.ToolTip = "���������� ������ ������ ������";
            this.colPointsValidity.Visible = true;
            this.colPointsValidity.VisibleIndex = 0;
            this.colPointsValidity.Width = 69;
            // 
            // icbValidity
            // 
            this.icbValidity.Appearance.Options.UseTextOptions = true;
            this.icbValidity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbValidity.AutoHeight = false;
            this.icbValidity.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbValidity.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbValidity.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 2)});
            this.icbValidity.Name = "icbValidity";
            this.icbValidity.SmallImages = this.icValidity;
            // 
            // icValidity
            // 
            this.icValidity.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icValidity.ImageStream")));
            this.icValidity.Images.SetKeyName(0, "status-busy.png");
            this.icValidity.Images.SetKeyName(1, "status-away.png");
            this.icValidity.Images.SetKeyName(2, "status.png");
            this.icValidity.Images.SetKeyName(3, "pin.png");
            this.icValidity.Images.SetKeyName(4, "network-status.png");
            this.icValidity.Images.SetKeyName(5, "Zone.gif");
            this.icValidity.Images.SetKeyName(6, "lock-small.png");
            this.icValidity.Images.SetKeyName(7, "checkbox_no.png");
            this.icValidity.Images.SetKeyName(8, "checkbox_yes.png");
            // 
            // colLockRecord
            // 
            this.colLockRecord.AppearanceHeader.Options.UseImage = true;
            this.colLockRecord.AppearanceHeader.Options.UseTextOptions = true;
            this.colLockRecord.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLockRecord.Caption = "LockRecord";
            this.colLockRecord.ColumnEdit = this.icbLock;
            this.colLockRecord.FieldName = "LockRecordBool";
            this.colLockRecord.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colLockRecord.ImageIndex = 7;
            this.colLockRecord.Name = "colLockRecord";
            this.colLockRecord.ToolTip = "������������ ������ ��� ����������� ����������";
            this.colLockRecord.Visible = true;
            this.colLockRecord.VisibleIndex = 1;
            this.colLockRecord.Width = 65;
            // 
            // icbLock
            // 
            this.icbLock.AutoHeight = false;
            this.icbLock.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbLock.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbLock.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", true, 6)});
            this.icbLock.Name = "icbLock";
            this.icbLock.SmallImages = this.icValidity;
            // 
            // NameF
            // 
            this.NameF.AppearanceCell.Options.UseTextOptions = true;
            this.NameF.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameF.AppearanceHeader.Options.UseTextOptions = true;
            this.NameF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NameF.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameF.Caption = "����";
            this.NameF.FieldName = "FName";
            this.NameF.Name = "NameF";
            this.NameF.OptionsColumn.AllowEdit = false;
            this.NameF.OptionsColumn.ReadOnly = true;
            this.NameF.Visible = true;
            this.NameF.VisibleIndex = 2;
            this.NameF.Width = 124;
            // 
            // Driver
            // 
            this.Driver.AppearanceHeader.Options.UseTextOptions = true;
            this.Driver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Driver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Driver.Caption = "��������";
            this.Driver.FieldName = "Driver";
            this.Driver.Name = "Driver";
            this.Driver.OptionsColumn.AllowEdit = false;
            this.Driver.OptionsColumn.ReadOnly = true;
            this.Driver.Visible = true;
            this.Driver.VisibleIndex = 3;
            this.Driver.Width = 109;
            // 
            // TimeStart
            // 
            this.TimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStart.Caption = "�����";
            this.TimeStart.DisplayFormat.FormatString = "t";
            this.TimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeStart.FieldName = "TimeStart";
            this.TimeStart.Name = "TimeStart";
            this.TimeStart.OptionsColumn.AllowEdit = false;
            this.TimeStart.OptionsColumn.ReadOnly = true;
            this.TimeStart.Visible = true;
            this.TimeStart.VisibleIndex = 4;
            this.TimeStart.Width = 80;
            // 
            // TimeEnd
            // 
            this.TimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeEnd.Caption = "�����";
            this.TimeEnd.DisplayFormat.FormatString = "t";
            this.TimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEnd.FieldName = "TimeEnd";
            this.TimeEnd.Name = "TimeEnd";
            this.TimeEnd.OptionsColumn.AllowEdit = false;
            this.TimeEnd.OptionsColumn.ReadOnly = true;
            this.TimeEnd.Visible = true;
            this.TimeEnd.VisibleIndex = 5;
            this.TimeEnd.Width = 80;
            // 
            // Time
            // 
            this.Time.AppearanceCell.Options.UseTextOptions = true;
            this.Time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Time.AppearanceHeader.Options.UseTextOptions = true;
            this.Time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Time.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Time.Caption = "�����";
            this.Time.DisplayFormat.FormatString = "t";
            this.Time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.Time.FieldName = "Time";
            this.Time.Name = "Time";
            this.Time.OptionsColumn.AllowEdit = false;
            this.Time.OptionsColumn.ReadOnly = true;
            this.Time.Visible = true;
            this.Time.VisibleIndex = 6;
            this.Time.Width = 80;
            // 
            // Confirm
            // 
            this.Confirm.AppearanceHeader.Options.UseTextOptions = true;
            this.Confirm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Confirm.FieldName = "ConfirmBool";
            this.Confirm.ImageIndex = 5;
            this.Confirm.Name = "Confirm";
            this.Confirm.OptionsColumn.ShowCaption = false;
            this.Confirm.Visible = true;
            this.Confirm.VisibleIndex = 7;
            this.Confirm.Width = 80;
            // 
            // Work
            // 
            this.Work.AppearanceHeader.Options.UseTextOptions = true;
            this.Work.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Work.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Work.Caption = "��� ������";
            this.Work.ColumnEdit = this.WorkLookUp;
            this.Work.FieldName = "Id_work";
            this.Work.Name = "Work";
            this.Work.Visible = true;
            this.Work.VisibleIndex = 8;
            this.Work.Width = 212;
            // 
            // WorkLookUp
            // 
            this.WorkLookUp.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WorkLookUp.AutoHeight = false;
            this.WorkLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��� �����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.WorkLookUp.DisplayMember = "Name";
            this.WorkLookUp.DropDownRows = 10;
            this.WorkLookUp.Name = "WorkLookUp";
            this.WorkLookUp.NullText = "��� ������";
            this.WorkLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.WorkLookUp.ValueMember = "Id";
            // 
            // Square
            // 
            this.Square.AppearanceHeader.Options.UseTextOptions = true;
            this.Square.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Square.Caption = "������� , ��";
            this.Square.FieldName = "FactSquareCalc";
            this.Square.Name = "Square";
            this.Square.OptionsColumn.AllowEdit = false;
            this.Square.OptionsColumn.ReadOnly = true;
            this.Square.Visible = true;
            this.Square.VisibleIndex = 9;
            this.Square.Width = 109;
            // 
            // Distance
            // 
            this.Distance.AppearanceHeader.Options.UseTextOptions = true;
            this.Distance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Distance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Distance.Caption = "����, ��";
            this.Distance.FieldName = "Distance";
            this.Distance.Name = "Distance";
            this.Distance.OptionsColumn.AllowEdit = false;
            this.Distance.OptionsColumn.ReadOnly = true;
            this.Distance.Visible = true;
            this.Distance.VisibleIndex = 10;
            this.Distance.Width = 80;
            // 
            // Width_s
            // 
            this.Width_s.AppearanceCell.Options.UseTextOptions = true;
            this.Width_s.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_s.AppearanceHeader.Options.UseTextOptions = true;
            this.Width_s.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Width_s.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Width_s.Caption = "������, �";
            this.Width_s.FieldName = "Width";
            this.Width_s.Name = "Width_s";
            this.Width_s.OptionsColumn.AllowEdit = false;
            this.Width_s.OptionsColumn.ReadOnly = true;
            this.Width_s.Visible = true;
            this.Width_s.VisibleIndex = 11;
            this.Width_s.Width = 149;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // iiValidity
            // 
            this.iiValidity.AutoHeight = false;
            this.iiValidity.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.iiValidity.Images = this.icValidity;
            this.iiValidity.Name = "iiValidity";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // ichEdit
            // 
            this.ichEdit.AutoHeight = false;
            this.ichEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ichEdit.Name = "ichEdit";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSplitRecord,
            this.bbiRefreshMap,
            this.bchMarkers,
            this.bchDgps,
            this.bbiViewSensorAngle,
            this.bbiSetupSensorAngle});
            this.barManager1.MainMenu = this.bar3;
            this.barManager1.MaxItemId = 6;
            // 
            // bar3
            // 
            this.bar3.BarName = "Main menu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSplitRecord),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchMarkers),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bchDgps, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetupSensorAngle),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSensorAngle)});
            this.bar3.OptionsBar.MultiLine = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // bbiRefreshMap
            // 
            this.bbiRefreshMap.Caption = "����������";
            this.bbiRefreshMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshMap.Glyph")));
            this.bbiRefreshMap.Hint = "������������ ����� ������� ����� ������";
            this.bbiRefreshMap.Id = 1;
            this.bbiRefreshMap.Name = "bbiRefreshMap";
            this.bbiRefreshMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshMap_ItemClick);
            // 
            // bbiSplitRecord
            // 
            this.bbiSplitRecord.Caption = "����� ������";
            this.bbiSplitRecord.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSplitRecord.Glyph")));
            this.bbiSplitRecord.Hint = "�������� ������ ������ ����� ������ �����";
            this.bbiSplitRecord.Id = 0;
            this.bbiSplitRecord.Name = "bbiSplitRecord";
            this.bbiSplitRecord.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSplitRecord_ItemClick);
            // 
            // bchMarkers
            // 
            this.bchMarkers.Glyph = ((System.Drawing.Image)(resources.GetObject("bchMarkers.Glyph")));
            this.bchMarkers.Id = 2;
            this.bchMarkers.Name = "bchMarkers";
            this.bchMarkers.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchMarkers_CheckedChanged);
            // 
            // bchDgps
            // 
            this.bchDgps.Glyph = ((System.Drawing.Image)(resources.GetObject("bchDgps.Glyph")));
            this.bchDgps.Id = 3;
            this.bchDgps.Name = "bchDgps";
            this.bchDgps.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchDgps_CheckedChanged);
            // 
            // bbiSetupSensorAngle
            // 
            this.bbiSetupSensorAngle.Caption = "��������� ������ ������������ ����������� �������";
            this.bbiSetupSensorAngle.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetupSensorAngle.Glyph")));
            this.bbiSetupSensorAngle.Id = 5;
            this.bbiSetupSensorAngle.Name = "bbiSetupSensorAngle";
            this.bbiSetupSensorAngle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetupSensorAngle_ItemClick);
            // 
            // bbiViewSensorAngle
            // 
            this.bbiViewSensorAngle.Caption = "����������� ����������� ������ ����������� �������";
            this.bbiViewSensorAngle.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiViewSensorAngle.Glyph")));
            this.bbiViewSensorAngle.Id = 4;
            this.bbiViewSensorAngle.Name = "bbiViewSensorAngle";
            this.bbiViewSensorAngle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSensorAngle_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(829, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 319);
            this.barDockControlBottom.Size = new System.Drawing.Size(829, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(32, 319);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(829, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 319);
            // 
            // AgroMapOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "AgroMapOrder";
            this.Size = new System.Drawing.Size(829, 319);
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iiValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ichEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar barMap;
        private DevExpress.XtraEditors.PanelControl pnGIS;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraGrid.GridControl gcMap;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMap;
        private DevExpress.XtraGrid.Columns.GridColumn colIdOrdert;
        private DevExpress.XtraGrid.Columns.GridColumn Id_zone;
        private DevExpress.XtraGrid.Columns.GridColumn NameF;
        private DevExpress.XtraGrid.Columns.GridColumn Driver;
        private DevExpress.XtraGrid.Columns.GridColumn TimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn TimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn Time;
        private DevExpress.XtraGrid.Columns.GridColumn Work;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit WorkLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn Distance;
        private DevExpress.XtraGrid.Columns.GridColumn Width_s;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn Confirm;
        private DevExpress.XtraGrid.Columns.GridColumn Square;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colLockRecord;
        private DevExpress.Utils.ImageCollection icValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit iiValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbLock;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit ichEdit;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiSplitRecord;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshMap;
        private DevExpress.XtraBars.BarCheckItem bchMarkers;
        private DevExpress.XtraBars.BarCheckItem bchDgps;
        private DevExpress.XtraBars.BarButtonItem bbiViewSensorAngle;
        private DevExpress.XtraBars.BarButtonItem bbiSetupSensorAngle;
    }
}
