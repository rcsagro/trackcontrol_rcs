namespace Agro.Controls
{
    partial class AgroFieldTc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            } 
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gvDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDateOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSquareForOperation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMap = new DevExpress.XtraGrid.GridControl();
            this.gvMap = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTOid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDrawTrack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOwork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbeWorkType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colTOSquareGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTOSquarePercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.WorkLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.icbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.iiValidity = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icbLock = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.ichEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.barMap = new DevExpress.XtraBars.Bar();
            this.pnGIS = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeWorkType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iiValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ichEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvDetal
            // 
            this.gvDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDateOrder,
            this.colVehicleName,
            this.colSquareForOperation});
            this.gvDetal.GridControl = this.gcMap;
            this.gvDetal.Name = "gvDetal";
            this.gvDetal.OptionsDetail.EnableMasterViewMode = false;
            this.gvDetal.OptionsView.ShowGroupPanel = false;
            // 
            // colDateOrder
            // 
            this.colDateOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOrder.Caption = "����";
            this.colDateOrder.FieldName = "DateOrder";
            this.colDateOrder.Name = "colDateOrder";
            this.colDateOrder.OptionsColumn.AllowEdit = false;
            this.colDateOrder.OptionsColumn.ReadOnly = true;
            this.colDateOrder.Visible = true;
            this.colDateOrder.VisibleIndex = 0;
            this.colDateOrder.Width = 198;
            // 
            // colVehicleName
            // 
            this.colVehicleName.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleName.Caption = "�������";
            this.colVehicleName.FieldName = "VehicleName";
            this.colVehicleName.Name = "colVehicleName";
            this.colVehicleName.OptionsColumn.AllowEdit = false;
            this.colVehicleName.OptionsColumn.ReadOnly = true;
            this.colVehicleName.Visible = true;
            this.colVehicleName.VisibleIndex = 1;
            this.colVehicleName.Width = 509;
            // 
            // colSquareForOperation
            // 
            this.colSquareForOperation.AppearanceCell.Options.UseTextOptions = true;
            this.colSquareForOperation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareForOperation.AppearanceHeader.Options.UseTextOptions = true;
            this.colSquareForOperation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSquareForOperation.Caption = "���� ������������ �������, ��";
            this.colSquareForOperation.FieldName = "SquareForOperation";
            this.colSquareForOperation.Name = "colSquareForOperation";
            this.colSquareForOperation.OptionsColumn.AllowEdit = false;
            this.colSquareForOperation.OptionsColumn.ReadOnly = true;
            this.colSquareForOperation.Visible = true;
            this.colSquareForOperation.VisibleIndex = 2;
            this.colSquareForOperation.Width = 445;
            // 
            // gcMap
            // 
            this.gcMap.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvDetal;
            gridLevelNode1.RelationName = "FactRecords";
            this.gcMap.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcMap.Location = new System.Drawing.Point(0, 0);
            this.gcMap.MainView = this.gvMap;
            this.gcMap.Name = "gcMap";
            this.gcMap.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.WorkLookUp,
            this.icbValidity,
            this.repositoryItemProgressBar1,
            this.iiValidity,
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox2,
            this.icbLock,
            this.ichEdit,
            this.cbeWorkType});
            this.gcMap.Size = new System.Drawing.Size(291, 319);
            this.gcMap.TabIndex = 6;
            this.gcMap.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMap,
            this.gvDetal});
            // 
            // gvMap
            // 
            this.gvMap.ColumnPanelRowHeight = 40;
            this.gvMap.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTOid,
            this.colDrawTrack,
            this.colTOwork,
            this.colTOSquareGa,
            this.colTOSquarePercent});
            this.gvMap.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gvMap.GridControl = this.gcMap;
            this.gvMap.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvMap.IndicatorWidth = 20;
            this.gvMap.Name = "gvMap";
            this.gvMap.OptionsDetail.ShowDetailTabs = false;
            this.gvMap.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvMap.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvMap.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvMap.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvMap.OptionsView.ShowGroupPanel = false;
            this.gvMap.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvMap_CustomDrawRowIndicator);
            this.gvMap.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvMap_RowCellStyle);
            this.gvMap.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvMap_CellValueChanging);
            // 
            // colTOid
            // 
            this.colTOid.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOid.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOid.Caption = "Id";
            this.colTOid.FieldName = "Id";
            this.colTOid.Name = "colTOid";
            this.colTOid.OptionsColumn.AllowEdit = false;
            this.colTOid.OptionsColumn.AllowFocus = false;
            this.colTOid.OptionsColumn.ReadOnly = true;
            // 
            // colDrawTrack
            // 
            this.colDrawTrack.FieldName = "DrawTrack";
            this.colDrawTrack.Name = "colDrawTrack";
            this.colDrawTrack.OptionsColumn.ShowCaption = false;
            this.colDrawTrack.Visible = true;
            this.colDrawTrack.VisibleIndex = 0;
            this.colDrawTrack.Width = 38;
            // 
            // colTOwork
            // 
            this.colTOwork.AppearanceCell.Options.UseTextOptions = true;
            this.colTOwork.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOwork.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOwork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOwork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOwork.Caption = "��� �����";
            this.colTOwork.ColumnEdit = this.cbeWorkType;
            this.colTOwork.FieldName = "WorkType";
            this.colTOwork.Name = "colTOwork";
            this.colTOwork.OptionsColumn.AllowEdit = false;
            this.colTOwork.OptionsColumn.ReadOnly = true;
            this.colTOwork.Visible = true;
            this.colTOwork.VisibleIndex = 1;
            this.colTOwork.Width = 499;
            // 
            // cbeWorkType
            // 
            this.cbeWorkType.AutoHeight = false;
            this.cbeWorkType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeWorkType.Name = "cbeWorkType";
            // 
            // colTOSquareGa
            // 
            this.colTOSquareGa.AppearanceCell.Options.UseTextOptions = true;
            this.colTOSquareGa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquareGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOSquareGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquareGa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOSquareGa.Caption = "�����. �������, �� (����)";
            this.colTOSquareGa.FieldName = "SquareGa";
            this.colTOSquareGa.Name = "colTOSquareGa";
            this.colTOSquareGa.OptionsColumn.AllowEdit = false;
            this.colTOSquareGa.OptionsColumn.ReadOnly = true;
            this.colTOSquareGa.Visible = true;
            this.colTOSquareGa.VisibleIndex = 2;
            this.colTOSquareGa.Width = 300;
            // 
            // colTOSquarePercent
            // 
            this.colTOSquarePercent.AppearanceCell.Options.UseTextOptions = true;
            this.colTOSquarePercent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquarePercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOSquarePercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOSquarePercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTOSquarePercent.Caption = "�����. �������, % (����)";
            this.colTOSquarePercent.FieldName = "SquarePercent";
            this.colTOSquarePercent.Name = "colTOSquarePercent";
            this.colTOSquarePercent.OptionsColumn.AllowEdit = false;
            this.colTOSquarePercent.OptionsColumn.ReadOnly = true;
            this.colTOSquarePercent.Visible = true;
            this.colTOSquarePercent.VisibleIndex = 3;
            this.colTOSquarePercent.Width = 311;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // WorkLookUp
            // 
            this.WorkLookUp.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WorkLookUp.AutoHeight = false;
            this.WorkLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��� �����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.WorkLookUp.DisplayMember = "Name";
            this.WorkLookUp.DropDownRows = 10;
            this.WorkLookUp.Name = "WorkLookUp";
            this.WorkLookUp.NullText = "��� ������";
            this.WorkLookUp.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Flat;
            this.WorkLookUp.ValueMember = "Id";
            // 
            // icbValidity
            // 
            this.icbValidity.Appearance.Options.UseTextOptions = true;
            this.icbValidity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbValidity.AutoHeight = false;
            this.icbValidity.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbValidity.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbValidity.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 2)});
            this.icbValidity.Name = "icbValidity";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // iiValidity
            // 
            this.iiValidity.AutoHeight = false;
            this.iiValidity.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.iiValidity.Name = "iiValidity";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // icbLock
            // 
            this.icbLock.AutoHeight = false;
            this.icbLock.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbLock.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbLock.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", true, 6)});
            this.icbLock.Name = "icbLock";
            // 
            // ichEdit
            // 
            this.ichEdit.AutoHeight = false;
            this.ichEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ichEdit.Name = "ichEdit";
            // 
            // barMap
            // 
            this.barMap.BarName = "Main menu";
            this.barMap.DockCol = 0;
            this.barMap.DockRow = 0;
            this.barMap.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.barMap.OptionsBar.MultiLine = true;
            this.barMap.OptionsBar.UseWholeRow = true;
            this.barMap.Text = "Main menu";
            // 
            // pnGIS
            // 
            this.pnGIS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGIS.Location = new System.Drawing.Point(0, 0);
            this.pnGIS.Name = "pnGIS";
            this.pnGIS.Size = new System.Drawing.Size(533, 319);
            this.pnGIS.TabIndex = 7;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pnGIS);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcMap);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(829, 319);
            this.splitContainerControl1.SplitterPosition = 533;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Left;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // AgroFieldTc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "AgroFieldTc";
            this.Size = new System.Drawing.Size(829, 319);
            ((System.ComponentModel.ISupportInitialize)(this.gvDetal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeWorkType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iiValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ichEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar barMap;
        private DevExpress.XtraEditors.PanelControl pnGIS;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraGrid.GridControl gcMap;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMap;
        private DevExpress.XtraGrid.Columns.GridColumn colTOid;
        private DevExpress.XtraGrid.Columns.GridColumn colTOwork;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit WorkLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit iiValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbLock;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit ichEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbeWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colTOSquareGa;
        private DevExpress.XtraGrid.Columns.GridColumn colTOSquarePercent;
        private DevExpress.XtraGrid.Columns.GridColumn colDrawTrack;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDetal;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleName;
        private DevExpress.XtraGrid.Columns.GridColumn colSquareForOperation;
    }
}
