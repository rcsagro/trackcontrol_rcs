namespace Agro.Controls
{
    partial class AgroMap
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgroMap));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pnGIS = new DevExpress.XtraEditors.PanelControl();
            this.peVideo = new DevExpress.XtraEditors.PictureEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barVideo = new DevExpress.XtraBars.Bar();
            this.btStart = new DevExpress.XtraBars.BarButtonItem();
            this.btPause = new DevExpress.XtraBars.BarButtonItem();
            this.btStop = new DevExpress.XtraBars.BarButtonItem();
            this.btExit = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.beTrackBar = new DevExpress.XtraBars.BarEditItem();
            this.rbeTrackBar = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSpeedControl = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSpeedControlSetup = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVideo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSquare = new DevExpress.XtraBars.BarButtonItem();
            this.bchMarkers = new DevExpress.XtraBars.BarCheckItem();
            this.bbiPrintReport = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.rchSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.rtbcVideo = new DevExpress.XtraEditors.Repository.RepositoryItemTrackBar();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rbeSpeed = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcMap = new DevExpress.XtraGrid.GridControl();
            this.gvMap = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Sel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_ordert = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Family = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FactSquareCalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icImages = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tmVideo = new System.Windows.Forms.Timer(this.components);
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).BeginInit();
            this.pnGIS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peVideo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbcVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbeSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pnGIS);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gcMap);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(842, 271);
            this.splitContainerControl1.SplitterPosition = 363;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // pnGIS
            // 
            this.pnGIS.Controls.Add(this.peVideo);
            this.pnGIS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnGIS.Location = new System.Drawing.Point(0, 0);
            this.pnGIS.Name = "pnGIS";
            this.pnGIS.Size = new System.Drawing.Size(363, 271);
            this.pnGIS.TabIndex = 7;
            // 
            // peVideo
            // 
            this.peVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peVideo.Location = new System.Drawing.Point(2, 2);
            this.peVideo.MenuManager = this.barManager1;
            this.peVideo.Name = "peVideo";
            this.peVideo.Properties.NullText = "���������� �����������";
            this.peVideo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peVideo.Size = new System.Drawing.Size(359, 267);
            this.peVideo.TabIndex = 0;
            this.peVideo.Visible = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barVideo,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btRefresh,
            this.beTrackBar,
            this.btStart,
            this.btStop,
            this.barStaticItem2,
            this.btPause,
            this.btExit,
            this.bbiRefresh,
            this.barButtonItem1,
            this.bbiSpeedControl,
            this.bbiSpeedControlSetup,
            this.bbiVideo,
            this.bbiSquare,
            this.bchMarkers,
            this.bbiPrintReport});
            this.barManager1.MaxItemId = 39;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rchSelect,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemTrackBar1,
            this.rtbcVideo,
            this.repositoryItemTextEdit2,
            this.rbeTrackBar,
            this.rbeSpeed,
            this.repositoryItemTextEdit3});
            // 
            // barVideo
            // 
            this.barVideo.BarName = "VideoMenu";
            this.barVideo.DockCol = 0;
            this.barVideo.DockRow = 0;
            this.barVideo.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barVideo.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btStart),
            new DevExpress.XtraBars.LinkPersistInfo(this.btPause),
            new DevExpress.XtraBars.LinkPersistInfo(this.btStop),
            new DevExpress.XtraBars.LinkPersistInfo(this.btExit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beTrackBar, "", false, true, true, 411)});
            this.barVideo.OptionsBar.AllowRename = true;
            this.barVideo.OptionsBar.UseWholeRow = true;
            this.barVideo.Text = "VideoMenu";
            this.barVideo.Visible = false;
            // 
            // btStart
            // 
            this.btStart.Glyph = ((System.Drawing.Image)(resources.GetObject("btStart.Glyph")));
            this.btStart.Id = 22;
            this.btStart.Name = "btStart";
            toolTipItem1.Text = "�����";
            superToolTip1.Items.Add(toolTipItem1);
            this.btStart.SuperTip = superToolTip1;
            this.btStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btStart_ItemClick);
            // 
            // btPause
            // 
            this.btPause.Glyph = ((System.Drawing.Image)(resources.GetObject("btPause.Glyph")));
            this.btPause.Id = 27;
            this.btPause.Name = "btPause";
            toolTipItem2.Text = "�����";
            superToolTip2.Items.Add(toolTipItem2);
            this.btPause.SuperTip = superToolTip2;
            this.btPause.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btPause_ItemClick);
            // 
            // btStop
            // 
            this.btStop.Glyph = ((System.Drawing.Image)(resources.GetObject("btStop.Glyph")));
            this.btStop.Id = 23;
            this.btStop.Name = "btStop";
            toolTipItem3.Text = "�����";
            superToolTip3.Items.Add(toolTipItem3);
            this.btStop.SuperTip = superToolTip3;
            this.btStop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btStop_ItemClick);
            // 
            // btExit
            // 
            this.btExit.Glyph = ((System.Drawing.Image)(resources.GetObject("btExit.Glyph")));
            this.btExit.Id = 29;
            this.btExit.Name = "btExit";
            this.btExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExit_ItemClick);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "����:";
            this.barStaticItem2.Id = 26;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // beTrackBar
            // 
            this.beTrackBar.AutoFillWidth = true;
            this.beTrackBar.Caption = "barEditItem2";
            this.beTrackBar.Edit = this.rbeTrackBar;
            this.beTrackBar.Id = 16;
            this.beTrackBar.Name = "beTrackBar";
            this.beTrackBar.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.beTrackBar_ItemPress);
            // 
            // rbeTrackBar
            // 
            this.rbeTrackBar.Name = "rbeTrackBar";
            // 
            // bar1
            // 
            this.bar1.BarName = "GridBar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(775, 169);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpeedControl),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpeedControlSetup),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVideo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSquare),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchMarkers),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrintReport)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.RotateWhenVertical = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "GridBar";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 30;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiSpeedControl
            // 
            this.bbiSpeedControl.Caption = "Speed";
            this.bbiSpeedControl.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSpeedControl.Glyph")));
            this.bbiSpeedControl.Id = 32;
            this.bbiSpeedControl.Name = "bbiSpeedControl";
            this.bbiSpeedControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSpeedControl_ItemClick);
            // 
            // bbiSpeedControlSetup
            // 
            this.bbiSpeedControlSetup.Caption = "barButtonItem2";
            this.bbiSpeedControlSetup.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSpeedControlSetup.Glyph")));
            this.bbiSpeedControlSetup.Id = 33;
            this.bbiSpeedControlSetup.Name = "bbiSpeedControlSetup";
            this.bbiSpeedControlSetup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSpeedControlSetup_ItemClick);
            // 
            // bbiVideo
            // 
            this.bbiVideo.Caption = "barButtonItem3";
            this.bbiVideo.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiVideo.Glyph")));
            this.bbiVideo.Id = 34;
            this.bbiVideo.Name = "bbiVideo";
            this.bbiVideo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVideo_ItemClick);
            // 
            // bbiSquare
            // 
            this.bbiSquare.Caption = "barButtonItem4";
            this.bbiSquare.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSquare.Glyph")));
            this.bbiSquare.Id = 35;
            this.bbiSquare.Name = "bbiSquare";
            this.bbiSquare.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSquare_ItemClick);
            // 
            // bchMarkers
            // 
            this.bchMarkers.Glyph = ((System.Drawing.Image)(resources.GetObject("bchMarkers.Glyph")));
            this.bchMarkers.Id = 37;
            this.bchMarkers.Name = "bchMarkers";
            this.bchMarkers.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchMarkers_CheckedChanged);
            // 
            // bbiPrintReport
            // 
            this.bbiPrintReport.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrintReport.Glyph")));
            this.bbiPrintReport.Id = 38;
            this.bbiPrintReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiPrintReport.LargeGlyph")));
            this.bbiPrintReport.Name = "bbiPrintReport";
            this.bbiPrintReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintReport_ItemClick);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.AutoSize = true;
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(474, 40);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(842, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 271);
            this.barDockControlBottom.Size = new System.Drawing.Size(842, 31);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 271);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(842, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 271);
            // 
            // btRefresh
            // 
            this.btRefresh.Glyph = global::Agro.Properties.ResourcesImages.ref_161;
            this.btRefresh.Id = 0;
            this.btRefresh.Name = "btRefresh";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 31;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // rchSelect
            // 
            this.rchSelect.AllowGrayed = true;
            this.rchSelect.Caption = "";
            this.rchSelect.Name = "rchSelect";
            this.rchSelect.PictureChecked = ((System.Drawing.Image)(resources.GetObject("rchSelect.PictureChecked")));
            this.rchSelect.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("rchSelect.PictureGrayed")));
            this.rchSelect.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("rchSelect.PictureUnchecked")));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTrackBar1
            // 
            this.repositoryItemTrackBar1.Maximum = 100;
            this.repositoryItemTrackBar1.Name = "repositoryItemTrackBar1";
            this.repositoryItemTrackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // rtbcVideo
            // 
            this.rtbcVideo.Maximum = 100;
            this.rtbcVideo.Name = "rtbcVideo";
            this.rtbcVideo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.rtbcVideo.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // rbeSpeed
            // 
            this.rbeSpeed.Maximum = 1250;
            this.rbeSpeed.Middle = 5;
            this.rbeSpeed.Minimum = 250;
            this.rbeSpeed.Name = "rbeSpeed";
            this.rbeSpeed.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gcMap
            // 
            this.gcMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcMap.Location = new System.Drawing.Point(0, 37);
            this.gcMap.MainView = this.gvMap;
            this.gcMap.Name = "gcMap";
            this.gcMap.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcMap.Size = new System.Drawing.Size(470, 231);
            this.gcMap.TabIndex = 6;
            this.gcMap.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMap});
            // 
            // gvMap
            // 
            this.gvMap.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Sel,
            this.Id,
            this.Id_ordert,
            this.NameT,
            this.Family,
            this.colWork,
            this.Date,
            this.FactSquareCalc,
            this.colWidth,
            this.colWorkId});
            this.gvMap.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gvMap.GridControl = this.gcMap;
            this.gvMap.Images = this.icImages;
            this.gvMap.Name = "gvMap";
            this.gvMap.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvMap.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvMap.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvMap.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvMap.OptionsView.ShowGroupPanel = false;
            this.gvMap.OptionsView.ShowIndicator = false;
            this.gvMap.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvMap_RowStyle);
            this.gvMap.ShowGridMenu += new DevExpress.XtraGrid.Views.Grid.GridMenuEventHandler(this.gvMap_ShowGridMenu);
            this.gvMap.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvMap_CellValueChanging);
            this.gvMap.Click += new System.EventHandler(this.gvMap_Click);
            this.gvMap.DoubleClick += new System.EventHandler(this.gvMap_DoubleClick);
            // 
            // Sel
            // 
            this.Sel.AppearanceHeader.Options.UseImage = true;
            this.Sel.AppearanceHeader.Options.UseTextOptions = true;
            this.Sel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sel.FieldName = "Sel";
            this.Sel.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Sel.ImageIndex = 1;
            this.Sel.Name = "Sel";
            this.Sel.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Sel.OptionsColumn.ShowCaption = false;
            this.Sel.Visible = true;
            this.Sel.VisibleIndex = 0;
            this.Sel.Width = 85;
            // 
            // Id
            // 
            this.Id.AppearanceCell.Options.UseTextOptions = true;
            this.Id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id.Caption = "�";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.ReadOnly = true;
            this.Id.Visible = true;
            this.Id.VisibleIndex = 1;
            this.Id.Width = 83;
            // 
            // Id_ordert
            // 
            this.Id_ordert.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_ordert.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_ordert.Caption = "Id_ordert";
            this.Id_ordert.FieldName = "Id_ordert";
            this.Id_ordert.Name = "Id_ordert";
            // 
            // NameT
            // 
            this.NameT.AppearanceCell.Options.UseTextOptions = true;
            this.NameT.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameT.AppearanceHeader.Options.UseTextOptions = true;
            this.NameT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NameT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.NameT.Caption = "�������, ���������� � ����";
            this.NameT.FieldName = "Name";
            this.NameT.Name = "NameT";
            this.NameT.OptionsColumn.AllowEdit = false;
            this.NameT.OptionsColumn.ReadOnly = true;
            this.NameT.Visible = true;
            this.NameT.VisibleIndex = 2;
            this.NameT.Width = 360;
            // 
            // Family
            // 
            this.Family.AppearanceHeader.Options.UseTextOptions = true;
            this.Family.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Family.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Family.Caption = "��������";
            this.Family.FieldName = "Family";
            this.Family.Name = "Family";
            this.Family.OptionsColumn.AllowEdit = false;
            this.Family.Visible = true;
            this.Family.VisibleIndex = 3;
            this.Family.Width = 190;
            // 
            // colWork
            // 
            this.colWork.AppearanceCell.Options.UseTextOptions = true;
            this.colWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWork.Caption = "��� �����";
            this.colWork.FieldName = "WorkName";
            this.colWork.Name = "colWork";
            this.colWork.OptionsColumn.AllowEdit = false;
            this.colWork.OptionsColumn.ReadOnly = true;
            this.colWork.Visible = true;
            this.colWork.VisibleIndex = 4;
            this.colWork.Width = 282;
            // 
            // Date
            // 
            this.Date.AppearanceCell.Options.UseTextOptions = true;
            this.Date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Date.AppearanceHeader.Options.UseTextOptions = true;
            this.Date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Date.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Date.Caption = "����";
            this.Date.FieldName = "Date";
            this.Date.Name = "Date";
            this.Date.OptionsColumn.AllowEdit = false;
            this.Date.OptionsColumn.ReadOnly = true;
            this.Date.Visible = true;
            this.Date.VisibleIndex = 5;
            this.Date.Width = 166;
            // 
            // FactSquareCalc
            // 
            this.FactSquareCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.FactSquareCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FactSquareCalc.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FactSquareCalc.Caption = "S, ��";
            this.FactSquareCalc.FieldName = "FactSquareCalc";
            this.FactSquareCalc.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.FactSquareCalc.Name = "FactSquareCalc";
            this.FactSquareCalc.OptionsColumn.AllowEdit = false;
            this.FactSquareCalc.OptionsColumn.ReadOnly = true;
            this.FactSquareCalc.Visible = true;
            this.FactSquareCalc.VisibleIndex = 6;
            this.FactSquareCalc.Width = 101;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "������";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            // 
            // colWorkId
            // 
            this.colWorkId.Caption = "WorkId";
            this.colWorkId.FieldName = "WorkId";
            this.colWorkId.Name = "colWorkId";
            this.colWorkId.OptionsColumn.AllowEdit = false;
            this.colWorkId.OptionsColumn.ReadOnly = true;
            // 
            // icImages
            // 
            this.icImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImages.ImageStream")));
            this.icImages.Images.SetKeyName(0, "checkbox_no.png");
            this.icImages.Images.SetKeyName(1, "checkbox_yes.png");
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // tmVideo
            // 
            this.tmVideo.Interval = 300;
            this.tmVideo.Tick += new System.EventHandler(this.tmVideo_Tick);
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcMap;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // AgroMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "AgroMap";
            this.Size = new System.Drawing.Size(842, 302);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnGIS)).EndInit();
            this.pnGIS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.peVideo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbcVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbeSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gcMap;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMap;
        private DevExpress.XtraGrid.Columns.GridColumn Sel;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn Id_ordert;
        private DevExpress.XtraGrid.Columns.GridColumn NameT;
        private DevExpress.XtraGrid.Columns.GridColumn Family;
        private DevExpress.XtraGrid.Columns.GridColumn Date;
        private DevExpress.XtraGrid.Columns.GridColumn FactSquareCalc;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.PanelControl pnGIS;
        private DevExpress.Utils.ImageCollection icImages;
        private DevExpress.XtraEditors.PictureEdit peVideo;
        private System.Windows.Forms.Timer tmVideo;
        private DevExpress.XtraGrid.Columns.GridColumn colWork;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarButtonItem btRefresh;
        private DevExpress.XtraBars.Bar barVideo;
        private DevExpress.XtraBars.BarButtonItem btStart;
        private DevExpress.XtraBars.BarButtonItem btPause;
        private DevExpress.XtraBars.BarButtonItem btStop;
        private DevExpress.XtraBars.BarButtonItem btExit;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarEditItem beTrackBar;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar rbeTrackBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar repositoryItemTrackBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTrackBar rtbcVideo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar rbeSpeed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkId;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiSpeedControl;
        private DevExpress.XtraBars.BarButtonItem bbiSpeedControlSetup;
        private DevExpress.XtraBars.BarButtonItem bbiVideo;
        private DevExpress.XtraBars.BarButtonItem bbiSquare;
        private DevExpress.XtraBars.BarCheckItem bchMarkers;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraBars.BarButtonItem bbiPrintReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
    }
}
