﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using Agro.Dictionaries; 
using Agro.Utilites;
using Agro.Properties;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.GMap.UI;
using TrackControl.Vehicles;

namespace Agro.Controls
{
    /// <summary>
    /// инструмент для ручного разделения записей наряда
    /// </summary>
    public class OrderRecordSplitter
    {

        public event VoidHandler RecordSplitted;
        public event VoidHandler ResetPointSelection;

        Select2PointsOrderRecord _s2Por;
        OrderItemRecord _parent;
        GoogleMapControl _mcGoogle;

        public OrderRecordSplitter()
        {
        }

        public void Start(int idOrderT,GoogleMapControl mcGoogle,Track tr)
        {
            _parent = new OrderItemRecord(idOrderT);
            _mcGoogle = mcGoogle;
            _s2Por = new Select2PointsOrderRecord(_parent, tr);
            _s2Por.Owner = mcGoogle.ParentForm;
            mcGoogle.OnTrackPointClicked += _s2Por.UpdateView;
            _s2Por.GetChildRecord += OnGetChildRecord;
            _s2Por.ResetPointSelection += onResetPointSelection;
            _s2Por.Show();
            _s2Por.DrawMainTrack(0,0); 
        }

        void OnGetChildRecord(OrderItemRecord child)
        {
            _s2Por.GetChildRecord -= OnGetChildRecord;
            _s2Por.ResetPointSelection -= onResetPointSelection;
            SplitParentRecord(child);
        }

        void SplitParentRecord(OrderItemRecord child)
        {
            //меняем параметры родительской без создания дочерней
            if (child.TimeStart == _parent.TimeStart && _parent.TimeEnd == child.TimeEnd)
            {
                UpdateParentRecord(child);
                return;
            }
            bool IsSplit = false;
            OrderItemRecord newParent = null;
            //отсекаем начало родительской управляющей
            if (child.TimeStart == _parent.TimeStart && _parent.TimeEnd > child.TimeEnd)
            {
                _parent.TimeStart = child.TimeEnd;
                _parent.UpdateRecordTimeStart();
                IsSplit = true;
            }
            //отсекаем конец родительской управляющей
            else if (_parent.TimeStart < child.TimeStart && _parent.TimeEnd == child.TimeEnd)
            {
                _parent.TimeEnd = child.TimeStart;
                _parent.UpdateRecordTimeEnd();
                IsSplit = true;
            }
            //вставка в середину управляющей
            else if (_parent.TimeStart < child.TimeStart && _parent.TimeEnd > child.TimeEnd)
            {
                //добавляем родительскую по времени (1 часть)
                newParent = new OrderItemRecord(_parent.IdMain, _parent.FieldId, _parent.DriverId, 0, _parent.WorkId, child.TimeEnd, _parent.TimeEnd, _parent.AgrInputSource, _parent.DrvInputSource);
                newParent.AgregatId = _parent.AgregatId;
                //двигаем родительскую по времени (2 часть)
                _parent.TimeEnd = child.TimeStart;
                IsSplit = true;
            }
            if (IsSplit)
            {
                CreateChildeRecord(child, newParent);
            }
        }

        private void CreateChildeRecord(OrderItemRecord child, OrderItemRecord newParent)
        {
            var oi = new OrderItem(_parent.IdMain);
            oi.ChangeStatusEvent += ((Order)_mcGoogle.ParentForm).SetStatus;
            if (CreateRecordInOrder(ref oi, child))
            {
                SplitControlRecords(child);
                CreateRecordInOrder(ref oi, _parent);
                if (newParent != null) CreateRecordInOrder(ref oi, newParent);
                oi.UpdateDocAfterSplitRecords();
                if (RecordSplitted != null) RecordSplitted();
            }
            else
            {
                MessageBox.Show(Resources.RecordDontCreate, Resources.ApplicationName);
                if (ResetPointSelection != null) ResetPointSelection();
            }
            oi.ChangeStatusEvent -= ((Order)_mcGoogle.ParentForm).SetStatus;
        }

        private void UpdateParentRecord(OrderItemRecord child)
        {
            _parent.FieldId = child.FieldId;
            _parent.AgregatId = child.AgregatId;
            _parent.DriverId = child.DriverId;
            _parent.WorkId = child.WorkId;
            if (_parent.UpdateControlParameters())
            {
                SplitControlRecords(child);
                using (OrderItem oi = new OrderItem(_parent.IdMain))
                {
                    oi.ChangeStatusEvent += ((Order)_mcGoogle.ParentForm).SetStatus;
                    oi.UpdateDocAfterSplitRecords();
                    oi.ChangeStatusEvent -= ((Order)_mcGoogle.ParentForm).SetStatus;
                }
            }
            if (RecordSplitted != null) RecordSplitted();
        }

        private bool CreateRecordInOrder(ref OrderItem oi, OrderItemRecord oir)
        {
            bool recordCreate = false;
            if (oir.IdOrderT > 0) oir.DeleteRecord();
            recordCreate = oi.ContentCreateRecord(oir.ZoneId, oir.DriverId, oir.AgregatId,  oir.TimeStart, oir.TimeEnd);
            UserLog.InsertLog(UserLogTypes.AGRO, string.Format("{0} {1} {2}", Resources.OrderRecordHandleCreate,oir.TimeStart,oir.TimeEnd), oi.ID);
            return recordCreate;
        }

        /// <summary>
        /// коррекция управляющих записей наряда
        /// </summary>
        /// <param name=child - новосозданная запись наряда></param>
        private void SplitControlRecords(OrderItemRecord child)
        {
            SplitControlRecord(Consts.TypeControlObject.Field, child.FieldId, child);
            SplitControlRecord(Consts.TypeControlObject.Driver, child.DriverId, child);
            SplitControlRecord(Consts.TypeControlObject.Agregat, child.AgregatId, child);
        }

        void SplitControlRecord(Consts.TypeControlObject typeO,int id_object, OrderItemRecord child)
        {
            OrderItemControl oic = new OrderItemControl(child.IdMain);
            List<OrderItemControlRecord> oicrs = oic.GetListTypeControlBetweenDates(typeO, child.TimeStart, child.TimeEnd);
            if (oicrs != null && oicrs.Count > 0)
            {
                foreach (OrderItemControlRecord parent in oicrs)
                {
                    bool IsSplit = false;
                    if (typeO==Consts.TypeControlObject.Field || parent.Id_object != id_object)
                    {
                        //меняем параметр родительской управляющей
                        if (parent.TimeStart == child.TimeStart && parent.TimeEnd == child.TimeEnd)
                        {
                            oic.UpdateRecord(parent.Id_object, parent.Id_record, parent.TimeEnd, parent.TimeEnd);
                        }
                        //отсекаем начало родительской управляющей
                        if (parent.TimeStart == child.TimeStart && parent.TimeEnd > child.TimeEnd)
                        {
                            //двигаем родительскую по времени
                            oic.UpdateRecord(parent.Id_object, parent.Id_record, child.TimeEnd, parent.TimeEnd);
                            IsSplit = true;
                        }
                        //отсекаем конец родительской управляющей
                        else  if (parent.TimeStart < child.TimeStart && parent.TimeEnd == child.TimeEnd)
                            {
                                //двигаем родительскую по времени
                                oic.UpdateRecord(parent.Id_object, parent.Id_record, parent.TimeStart, child.TimeStart);
                                IsSplit = true;
                            }
                        //вставка в середину управляющей
                        else if (parent.TimeStart <= child.TimeStart && parent.TimeEnd >= child.TimeEnd)
                        {
                            //добавляем родительскую по времени (1 часть)
                            oic.AddRecord(parent.Id_object, typeO, child.TimeEnd, parent.TimeEnd);
                            IsSplit = true;
                            //двигаем родительскую по времени (2 часть)
                            oic.UpdateRecord(parent.Id_object, parent.Id_record, parent.TimeStart, child.TimeStart);
                        }
                        //вставляем дочернюю
                        if (IsSplit) oic.AddRecord(id_object, typeO, child.TimeStart, child.TimeEnd);
                    }
                }
            }
        }

        void onResetPointSelection()
        {
            if (ResetPointSelection != null) ResetPointSelection();
            
        }

    }
}
