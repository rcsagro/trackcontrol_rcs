using Agro.Dictionaries;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System.Windows.Forms;
using System.Drawing;

namespace Agro.Controls
{
    public partial class AgroFieldTc : DevExpress.XtraEditors.XtraUserControl
    {
        GMapOrderProjection _gmapTc;
        FieldsTcEntity _fieldsTc;

        public AgroFieldTc(FieldsTcEntity fieldsTc)
        {
            InitializeComponent();
            _fieldsTc = fieldsTc;
            SetControls();
            Localization();
            this.Dock = DockStyle.Fill;
            DrawTracks();
        }



        #region Grid

        private void gvMap_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }

        }

        private void gvMap_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            gvMap.SetRowCellValue(e.RowHandle, e.Column, e.Value);
            DrawTracks();
        }

        private void gvMap_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column.FieldName == "DrawTrack")
                {
                    FieldTechOperation oper = (FieldTechOperation)gvMap.GetRow(e.RowHandle);
                    if (oper != null)
                    {
                        e.Appearance.BackColor = oper.WorkType.TrackColor;
                    }
                }
            }
        }

        #endregion

        #region GoogleMaps
        private void InitGoogleMapControl()
        {
            _gmapTc = new GMapOrderProjection(pnGIS);
        }



        #endregion

        private void SetControls()
        {
            InitGoogleMapControl();
            DictionaryAgroWorkType.SetComboBoxDataSource(cbeWorkType);
            gvMap.DataController.AllowIEnumerableDetails = true;
            gcMap.DataSource = _fieldsTc.Operations;
            if (_gmapTc != null && _fieldsTc.Field != null) _gmapTc.DrawZone(_fieldsTc.Field.IdZone);
        }

        void DrawTracks()
        {
            _gmapTc.DrawFieldsTcOnMap(_fieldsTc);
        }

        #region �����������
        private void Localization()
        {
            //Id_zone.Caption = Resources.Zone;
            //NameF.Caption = Resources.Field;
            //Family.Caption = Resources.Driver;
            //TimeStart.Caption = Resources.Entry;
            //TimeEnd.Caption = Resources.Exit;
            //Time.Caption = Resources.Time;
            //Work.Caption = Resources.WorkType;
            //WorkLookUp.Columns[0].Caption = Resources.WorkType;
            //WorkLookUp.NullText = Resources.DataNExist;
            //Square.Caption = Resources.SquareGA;
            //Distance.Caption = Resources.PathKm;
            //Width_s.Caption = Resources.WidthM;

            //colPointsValidity.ToolTip = Resources.OrderRecordValidity;
            //colLockRecord.ToolTip = Resources.OrderRecordLock;

            //bbiRefreshMap.Caption = Resources.Refresh;
            //bbiRefreshMap.Hint = Resources.RestoreOrderScale;

            //bbiSplitRecord.Caption = Resources.RecordNew;
            //bbiSplitRecord.Hint = Resources.HandleRecordCreate;

            //bchMarkers.Glyph = Shared.BallGreen;
            //bchMarkers.Caption = Resources.MarkersControl;
            //bchMarkers.Hint = Resources.MarkersSwitch;
        }
        #endregion


    }
}
