using Agro.Dictionaries;
using Agro.GridEditForms;
using Agro.Properties;
using Agro.Utilites;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;
using System.Linq;

namespace Agro.Controls
{
    public sealed partial class AgroMapOrder : DevExpress.XtraEditors.XtraUserControl 
    {
       
        #region Fields
        GMapOrderProjection _gmapOrder;
        private OrderItem _order;
        public OrderItem Order
        {
            get { return _order; }

            set 
            {
                _order = value;
                _listActiveZones.Clear();
                _gmapOrder.GMapInitControl();
                gvMap.ActiveFilter.Clear();
                gvMap.ClearGrouping(); 
                colLockRecord.ImageIndex = 7; // ������ ��������
                SetControls();
                DrawOrder();
            }
        }

        private bool _readOnly ;
        public bool ReadOnly
        {
            get { return _readOnly;} 
            set
            {
                _readOnly = value;
                if (_readOnly)
                    gvMap.OptionsBehavior.Editable = false;
                else
                    gvMap.OptionsBehavior.Editable = true; 
            } 
        }

        /// <summary>
        /// ��� ���������� ������ � �����
        /// </summary>
        int _idOrderTSelected;
        int _rowForSplit;
        /// <summary>
        /// ����� ���
        /// </summary>
        readonly Dictionary<int, Color> _zoneColors ;
        /// <summary>
        /// ������ + ����� ��������� ������� �������
        /// </summary>
        readonly Dictionary<int, AgregatForTrack> _agregatForTrack;
        
        private bool _isDrawMarkers;
        private bool _isDrawDgps;
        readonly List<int> _listActiveZones = null;
        bool _isContentLoaded;
        private DateTime DateOrder { get; set; }

        #endregion

        #region �����������

        public AgroMapOrder()
        {
            InitializeComponent();
            Localization();
            this.Dock = DockStyle.Fill;
            InitGoogleMapControl();
            _zoneColors = new Dictionary<int, Color>();
            _agregatForTrack = new Dictionary<int, AgregatForTrack>();
            _listActiveZones = new List<int>();
        }

        #endregion

        public List<int> GetActiveZones
        {
            get { return _listActiveZones; }
        }

        #region �������
        public delegate void RefreshMainForm();
        public event RefreshMainForm RefreshFromControl;
        public event Action<bool> SetLockButtons;
        #endregion

        #region Grid
        private void gvMap_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                var view = sender as GridView;
                if (e.Column.FieldName == "FName")
                {
                    int idZone = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Id_zone"]));
                    
                    if (idZone > 0)
                    {

                        if (_zoneColors.ContainsKey(idZone))
                        {
                            e.Appearance.BackColor = _zoneColors[idZone];
                            AddingNewZoneToList( idZone );
                        }
                    }
                }
                else
                {
                    bool bConfirm = Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["ConfirmBool"]));
                    if (!bConfirm)
                    {
                            e.Appearance.BackColor = Color.FromArgb(50,128,128,128)  ;
                    }
                }
            }
        }

        private void AddingNewZoneToList( int idZone )
        {
            for( int i = 0; i < _listActiveZones.Count; i++ )
            {
                if( idZone == _listActiveZones[i] )
                {
                    idZone = 0;
                }
            }

            if( idZone != 0 )
                _listActiveZones.Add( idZone );
        }

        private void gvMap_ShowGridMenu( object sender, GridMenuEventArgs e )
        {
            if (e.MenuType != DevExpress.XtraGrid.Views.Grid.GridMenuType.Row) return;
            //gvMenu.Items.Add(menuItem);
            //menuItem = new DevExpress.Utils.Menu.DXMenuItem("�������� ������", new EventHandler(LoadTransFromGrid));
            //gvMenu.Items.Add(menuItem);
            //gvMenu.Show(e.Point); 
        }

        private void gvMap_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }

        }

        private void gvMap_DoubleClick(object sender, EventArgs e)
        {
            ShowRecordOnMap();
        }

        private void ShowRecordOnMap()
        {
            int idOrderT = 0;
            if (Int32.TryParse((gvMap.GetRowCellValue(gvMap.FocusedRowHandle, gvMap.Columns["Id"]) ?? "").ToString(), out idOrderT))
            {
                if (_idOrderTSelected != idOrderT)
                {
                    _gmapOrder.SelectRecord(idOrderT);
                    _idOrderTSelected = idOrderT;
                }
            }
        }

        private void gvMap_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int iId = 0;
            if (Int32.TryParse((gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["Id"])?? "").ToString(), out iId))
            {
                if (iId == 0) return;
                string inpOut = string.Format("{0} - {1}", gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["TimeStart"]), gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["TimeEnd"]));
                if (e.Column.FieldName == "Id_work")
                {
                    int iWork = 0;
//The users may use Ctrl+Del to clear (set to null) the LookUpEdit.
                    if (e.Value == null)
                    {
                        ClearWork(e, iId);
                    }
                    else if (Int32.TryParse(e.Value.ToString(), out iWork))
                    {
                        SetWork(e, iId, iWork);
                    }
                }
                else if (e.Column.FieldName == "ConfirmBool")
                {
                    bool bConfirm;
                    if (Boolean.TryParse(e.Value.ToString(), out bConfirm))
                    {
                        SetConfirm(e, iId, bConfirm, inpOut);
                    }
                }
                else if (e.Column.FieldName == "LockRecordBool")
                {
                    bool bLock;
                    if (Boolean.TryParse(e.Value.ToString(), out bLock))
                    {
                            var oir = new OrderItemRecord(iId);
                            if (oir.LockRecord(bLock))
                            {
                                string message = string.Format("{2} {0} {1} ", bLock, inpOut, Resources.SetLockValue);
                                UserLog.InsertLog(UserLogTypes.AGRO, message, _order.ID);
                                RefreshFromControl();
                            }
                        }
                    }
                }

        }

        private void gvMap_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //������ � ��������� �� ������ ���� ������������
            if (gvMap.FocusedColumn == gvMap.Columns["ConfirmBool"])
            {
                bool confirm;
                if (Boolean.TryParse(gvMap.GetRowCellValue(gvMap.FocusedRowHandle, gvMap.Columns["ConfirmBool"]).ToString(), out confirm))
                {
                    if (Resources.Move1 == gvMap.GetRowCellValue(gvMap.FocusedRowHandle, gvMap.Columns["FName"]).ToString() && !confirm)
                    {
                        e.Cancel = true;
                    }
                }
            }
          
        }

        private void SetConfirm(CellValueChangedEventArgs e, int iId, bool bConfirm, string inpOut)
        {
            string sSqLupdate;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                if (bConfirm)
                {
                    sSqLupdate = string.Format("UPDATE agro_ordert SET Confirm = 1  WHERE (ID = {0})",iId);
                    string sSql = string.Format("SELECT FactSquareCalc FROM agro_ordert WHERE Id = {0}" , iId);
                    var dbSquare = driverDb.GetScalarValueNull<Double>(sSql, 0);
                    gvMap.SetRowCellValue(e.RowHandle, gvMap.Columns["FactSquareCalc"], dbSquare); 
                }
                else
                {
                    int idWork = 0;
                    bool resetWork = false;
                    if (Int32.TryParse((gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["Id_work"])?? "").ToString(), out idWork) && idWork>0)
                    {
                        using (var work = new DictionaryAgroWorkType(idWork))
                        {
                            if (work.TypeWork == (int)DictionaryAgroWorkType.WorkTypes.WorkInField) resetWork = true;
                        }
                    }
                    if (resetWork)
                    {
                        gvMap.SetRowCellValue(e.RowHandle, gvMap.Columns["Id_work"], 0);
                        sSqLupdate = string.Format("UPDATE agro_ordert SET Id_work = 0,Confirm =0 WHERE (ID = {0})", iId);
                    }
                    else
                    {
                        sSqLupdate = string.Format("UPDATE agro_ordert SET Confirm =0 WHERE (ID = {0})", iId);
                    }
                    gvMap.SetRowCellValue(e.RowHandle, gvMap.Columns["FactSquareCalc"], 0);  
                }
                driverDb.ExecuteNonQueryCommand(sSqLupdate);
                string message = string.Format("{2} {0} {1} ", bConfirm, inpOut, Resources.SetConfirmValue);
                UserLog.InsertLog(UserLogTypes.AGRO, message, _order.ID);
                RefreshFromControl();
            }
        }

        private void SetWork(CellValueChangedEventArgs e, int iId, int iWork)
        {
            int confirm = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                using (var work = new DictionaryAgroWorkType(iWork))
                {
                    if (work.TypeWork == (int) DictionaryAgroWorkType.WorkTypes.WorkInField) confirm = 1;
                }
                string sSqLupdate = string.Format("UPDATE agro_ordert SET Id_work = {0},Confirm = {2} WHERE (ID = {1})", iWork, iId, confirm);
                driverDb.ExecuteNonQueryCommand(sSqLupdate);
            }
            if (confirm == 1)
            {
                if (Convert.ToInt16(gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["ConfirmBool"])) != 1)
                    gvMap.SetRowCellValue(e.RowHandle, gvMap.Columns["ConfirmBool"], 1);
            }
        }

        private void ClearWork(CellValueChangedEventArgs e, int iId)
        {
            string sSqLupdate;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                sSqLupdate = string.Format("UPDATE agro_ordert SET Id_work = 0 ,Confirm =0 WHERE (ID = {0})",iId);
                driverDb.ExecuteNonQueryCommand(sSqLupdate);
            }
            if (Convert.ToInt16(gvMap.GetRowCellValue(e.RowHandle, gvMap.Columns["ConfirmBool"])) != 0)
                gvMap.SetRowCellValue(e.RowHandle, gvMap.Columns["ConfirmBool"], 0);
        }

        private void gvMap_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridColumn colLock = gvMap.Columns["LockRecordBool"];
            GridColumn colValidity = gvMap.Columns["PointsValidity"];
            var lockRecord = (bool)gvMap.GetRowCellValue(e.RowHandle, colLock);
            var pointsValidity = (int)gvMap.GetRowCellValue(e.RowHandle, colValidity);
            if (!lockRecord || (pointsValidity == (int) ValidityColors.Green)) return;
            e.Valid = false;
            gvMap.SetColumnError(colLock, Resources.OnlyValidDataBlocked);
        }

        private void gvMap_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void gvMap_Click(object sender, EventArgs e)
        {
            GridView view = sender as GridView;
            Point p = view.GridControl.PointToClient(Control.MousePosition);
            GridHitInfo ghi = view.CalcHitInfo(p);
            if (ghi.HitTest == GridHitTest.Column)
            {
                if (ghi.Column.Name == "colLockRecord")
                {
                    if (ghi.Column.ImageIndex == 7)// ������ ��������
                        ghi.Column.ImageIndex = 8;// ������ �������
                    else
                        ghi.Column.ImageIndex = 7;
                    Application.DoEvents();
                    if (gvMap.RowCount == 0) return;
                    bool lockRecord = (ghi.Column.ImageIndex == 8) ? true : false;
                    LockRecords(lockRecord);
                }
            }
        }

        private void LockRecords(bool lockRecord)
        {
            // ����� ������ 
            if (_order.Date.AddDays(1) < DateTime.Now || !lockRecord)
            {
                _order.LockRecords(lockRecord);
            }
            else
            {
                // ����� ������ - ��������� ����������, ����� ����������
                var dt = gcMap.DataSource as DataTable;
                if (dt == null) return;
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {
                    int idOrderT = 0;
                    if (Int32.TryParse(gvMap.GetRowCellValue(i, colIdOrdert).ToString(), out idOrderT))
                    {
                        if (idOrderT > 0)
                        if (!(bool)gvMap.GetRowCellValue(i, colLockRecord))
                        {
                            var oir = new OrderItemRecord(idOrderT);
                            oir.LockRecord(true);
                        }
                    }
                }
            }
            LoadOrderContent();
            string message = string.Format("{1} {0}  ", lockRecord, Resources.SetLockValueOrder);
            UserLog.InsertLog(UserLogTypes.AGRO, message, _order.ID);
            RefreshFromControl();

        }

        private void gvMap_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (!_isContentLoaded) ShowRecordOnMap();
        }

        /// <summary>
        /// ������� ���������� ������
        /// </summary>
        private void LoadOrderContent()
        {
            _isContentLoaded = true;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format( AgroQuery.AgroMapOrder.SelectAgroOrdert, Resources.Move1, _order.ID,
                                     Consts.ZONE_OUTSIDE_THE_LIST, Resources.FieldUnknown, AgroQuery.SqlDriverIdent);
                DataTable dtMain = driverDb.GetDataTable(sql);
                WorkLookUp.DataSource = DictionaryAgroWorkType.GetList(); 
                AddColumnBoolean(dtMain, "ConfirmBool");
                AddColumnBoolean(dtMain, "LockRecordBool");
                _agregatForTrack.Clear();
                Color evenRowColor = Color.FromArgb(70, Color.Brown.R, Color.Brown.G, Color.Brown.B); ;
                Color oddRowColor = Color.FromArgb(70, Color.GreenYellow.R, Color.GreenYellow.G, Color.GreenYellow.B);
                Color colorForTrack = oddRowColor;
                foreach (DataRow dr in dtMain.Rows)
                {
                    if (IsCellTrue(dr, "Confirm")) dr["ConfirmBool"] = true;
                    if (IsCellTrue(dr, "LockRecord")) dr["LockRecordBool"] = true;
                    dr["PointsValidity"] = DocItem.GetPointsValidityGauge((int)dr["PointsValidity"]);
                    double width = 0;
                    double.TryParse(dr["Width"].ToString(), out width);
                    if (width == 0)
                    {
                        width = AgroController.Instance.WidthAgregatDefault; 
                    }
                    _agregatForTrack.Add((int)dr["Id"], new AgregatForTrack(colorForTrack,width));
                    colorForTrack = colorForTrack == oddRowColor ? evenRowColor : oddRowColor;
                }
                gcMap.DataSource = dtMain;
                SetFieldsColors(dtMain);
                //SetInitLockCheckBox(dtMain);
            }
            _isContentLoaded = false;
        }

        private static void AddColumnBoolean(DataTable dtMain, string colName)
        {
            DataColumn dcBool = new DataColumn(colName, System.Type.GetType("System.Boolean"));
            dcBool.DefaultValue = false;
            dtMain.Columns.Add(dcBool);
        }

        private static bool IsCellTrue(DataRow dr,string fieldName)
        {
            return dr[fieldName].ToString() == "1" || dr[fieldName].ToString().ToUpper() == "TRUE";
        }

        private void SetFieldsColors(DataTable dtMain)
        {
            // ��������� ������ �� ������� �� �������. ���� - ������������� ������ �� �����
            // ����� ���
            if (dtMain.Rows.Count > 0)
            {
                int cntColor = 0;
                for (int i = 0; i < dtMain.Rows.Count; i++)
                {
                    var idZone = (int)dtMain.Rows[i]["Id_zone"];
                    if (idZone > 0)
                    {
                        if (!_zoneColors.ContainsKey(idZone))
                        {
                            Color color = MapUtilites.GetColorForRecordsSeparation(cntColor);
                            _zoneColors.Add(idZone, Color.FromArgb(77, color.R, color.G, color.B));
                            cntColor++;
                        }
                    }
                }
            }
        }

        //private void SetInitLockCheckBox(DataTable dtMain)
        //{
        //    if (gvMap.RowCount == 0) return;

        //    if (dtMain.Rows.Cast<DataRow>().Any(dr => IsCellTrue(dr, "LockRecord")))
        //    {
        //        //gvMap.Columns[3].ImageIndex = 8;
        //        colLockRecord.ImageIndex = 8; // ������ �������
        //        return;
        //    }
        //}

        #endregion

        #region GoogleMaps
        private void InitGoogleMapControl()
        {
            _gmapOrder = new GMapOrderProjection(pnGIS);
        }

        public void DrawOrder()
        {
            LoadOrderContent();
            RefreshMap();
        }

        private void bbiSplitRecord_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvMap.RowCount == 0) return;
            int idOrderT = 0;
            if (Int32.TryParse(gvMap.GetRowCellValue(gvMap.FocusedRowHandle, gvMap.Columns["Id"]).ToString(), out idOrderT))
            {
                SetLockButtons(true);
                if (_gmapOrder.SplitRecord(idOrderT, _order.Mobitel_Id))
                {
                    _gmapOrder.RefreshData += DoAfterSplit;
                    _rowForSplit = gvMap.FocusedRowHandle;
                }
                else
                    SetLockButtons(false);
            }
        }

        private void bbiRefreshMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DrawOrder();
        }

        private void RefreshMap()
        {
            if (_agregatForTrack.Count > 0 || _isDrawDgps)
            {
                if (_isDrawDgps)
                {
                    _gmapOrder.MobitelId = _order.Mobitel_Id;
                    _gmapOrder.DateOrder = DateOrder;
                }
                _gmapOrder.DrawOrder(_agregatForTrack, _zoneColors, _isDrawMarkers, _isDrawDgps);
            }
        }

        void DoAfterSplit()
        {
            LoadOrderContent();
            RefreshFromControl();
            int idOrderT = 0;
            gvMap.FocusedRowHandle = _rowForSplit;
            if (Int32.TryParse(gvMap.GetRowCellValue(_rowForSplit, gvMap.Columns["Id"]).ToString(), out idOrderT))
            {
                _gmapOrder.SelectRecord(idOrderT);
            }
            SetLockButtons(false);
        }

        private void bchMarkers_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _isDrawMarkers = bchMarkers.Checked;
            _gmapOrder.ViewMarkers(_isDrawMarkers);
        }

        private void bchDgps_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _isDrawDgps = bchDgps.Checked;
        }

        private void bbiViewSensorAngle_ItemClick(object sender, ItemClickEventArgs e)
        {
            DrawSensorValues();
        }

        private void bbiSetupSensorAngle_ItemClick(object sender, ItemClickEventArgs e)
        {
            var records = _order.GetOrderItemRecords();
            if (records.Count > 0)
            {
                var recordFirst = records.Where(r => r.AgregatId > 0).FirstOrDefault();
                if (recordFirst != null)
                {
                    using (Form f = new Agregat(recordFirst.AgregatId))
                    {
                        f.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show(Resources.NoVehicleAgregat, Resources.ApplicationName);
                }
            }
        }

        private void DrawSensorValues()
        {
            var args = new Dictionary<int, ExternalDevice>();
            _gmapOrder.MobitelId = _order.Mobitel_Id;
            _gmapOrder.DateOrder = _order.Date;
            var records = _order.GetOrderItemRecords();
            var vehicle = new Vehicle(_order.Mobitel_Id);
            var recordsFiltered = records.Where(r => r.AgregatId > 0).ToList();
            if (recordsFiltered.Count >0)
            {
                _gmapOrder.ClearTracks();
                foreach (var record in recordsFiltered)
                {
                        ExternalDevice agroSensor;
                        if (args.ContainsKey(record.AgregatId))
                        {
                            agroSensor = args[record.AgregatId];
                        }
                           else
                        {
                            var daa = new DictionaryAgroAgregat(record.AgregatId);
                            int idSensor = 0;
                            agroSensor = daa.GetAgroSensor(vehicle.Id, _order.Mobitel_Id, ref idSensor);
                            args[record.AgregatId] = agroSensor;
                        }
                        if (((Sensor)agroSensor).SensState != null)
                        {
                            var thresholds = new Dictionary<float, Color>
                                {
                                    {(float) ((Sensor) agroSensor).SensState.MinValue, Color.Yellow},
                                    {(float) ((Sensor) agroSensor).SensState.MaxValue, Color.Green},
                                    {10000, Color.Red}
                                };
                            _gmapOrder.DrawOrderSensorValues(thresholds, agroSensor.Algoritm, record.TimeStart, record.TimeEnd);
                        }
                }
            }
        }
        #endregion

        public void SetControls()
        {
            if (ReadOnly)
            {
                gcMap.Enabled = false;
            }
            else
            {
                gcMap.Enabled = true;
            }
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                if (!db.IsTableExist("datagps64")) bchDgps.Visibility = BarItemVisibility.Never;
            }
            var sensorAngle = new Sensor((int)AlgorithmType.ANGLE, _order.Mobitel_Id);
            var sensorRf = new Sensor((int)AlgorithmType.RANGEFINDER, _order.Mobitel_Id);
            if (sensorAngle.Id <= 0 & sensorRf.Id <=0 )
            {
                bbiSetupSensorAngle.Visibility = BarItemVisibility.Never;
                bbiViewSensorAngle.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bbiSetupSensorAngle.Visibility = BarItemVisibility.Always;
                bbiViewSensorAngle.Visibility = BarItemVisibility.Always;
            }

        }

        #region �����������
        private void Localization()
        {
            Id_zone.Caption = Resources.Zone;
            NameF.Caption = Resources.Field;
            Driver.Caption = Resources.Driver;
            TimeStart.Caption = Resources.Entry;
            TimeEnd.Caption = Resources.Exit;
            Time.Caption = Resources.Time;
            Work.Caption = Resources.WorkType;
            WorkLookUp.Columns[0].Caption = Resources.WorkType;
            WorkLookUp.NullText = Resources.DataNExist;
            Square.Caption = Resources.SquareGA;
            Distance.Caption = Resources.PathKm;
            Width_s.Caption = Resources.WidthM;

            colPointsValidity.ToolTip = Resources.OrderRecordValidity;
            colLockRecord.ToolTip = Resources.OrderRecordLock;

            bbiRefreshMap.Caption = Resources.Refresh;
            bbiRefreshMap.Hint = Resources.RestoreOrderScale;

            bbiSplitRecord.Caption = Resources.RecordNew;
            bbiSplitRecord.Hint = Resources.HandleRecordCreate;

            bchMarkers.Glyph = Shared.BallGreen;
            bchMarkers.Caption = Resources.MarkersControl;
            bchMarkers.Hint = Resources.MarkersSwitch;

            bchDgps.Caption = "DGPS";
            bchDgps.Hint = Resources.DgpsView;

            bbiViewSensorAngle.Hint = Resources.DisplayAngleSensor;
            bbiSetupSensorAngle.Hint = Resources.ThresholdSensorSetting;
        }
        #endregion



    }
}
