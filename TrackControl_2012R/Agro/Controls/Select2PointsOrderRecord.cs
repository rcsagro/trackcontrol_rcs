using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Agro.Dictionaries;
using Agro.Properties;
using DevExpress.XtraEditors;
using DevExpress.XtraVerticalGrid.Rows;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.UI;
using TrackControl.Vehicles;

namespace Agro.Controls
{
    public partial class Select2PointsOrderRecord : DevExpress.XtraEditors.XtraForm
    {
        const int POINT_DEACTIVE = -1;
        int _ind_first = POINT_DEACTIVE;
        int _ind_second = POINT_DEACTIVE;
        System.Int64 _gps_first;
        System.Int64 _gps_second;
        int _id_field;
        int _id_driver;
        int _id_agregat;
        int _id_work;
        OrderItemRecord _parent;
        Track _track;
        DateTime _dateStart;
        DateTime _dateEnd;

        public event Action<OrderItemRecord> GetChildRecord;
        public event VoidHandler ResetPointSelection;

        public Select2PointsOrderRecord(OrderItemRecord parentRecord, Track tr)
        {
            InitializeComponent();
            _track = tr;
            Localization();
            LoadLookUps();
            _parent = parentRecord;
            SetValuesFromOrderRecord(_parent);
             
        }

        private void SetValuesFromOrderRecord(OrderItemRecord oir)
        {
            _id_field = DictionaryAgroField.Pereezd;
            _id_driver = oir.DriverId;
            _id_agregat = oir.AgregatId;
            _id_work = oir.WorkId;
            fieldF.Properties.Value = _id_field;
            driverF.Properties.Value = _id_driver;
            agregatF.Properties.Value = _id_agregat;
            workF.Properties.Value = _id_work;
            fieldS.Properties.Value = _id_field;
            driverS.Properties.Value = _id_driver;
            agregatS.Properties.Value = _id_agregat;
            workS.Properties.Value = _id_work;
        }

        private void LoadLookUps()
        {
            lookUpWork.DataSource = DictionaryAgroWorkType.GetList();
            lookUpFields.DataSource = DictionaryAgroField.GetList();
            lookUpDriver.DataSource = DriverVehicleProvider.GetList();
            lookUpAgregat.DataSource = DictionaryAgroAgregat.GetList();
            lookUpWorkS.DataSource = lookUpWork.DataSource;
            lookUpFieldsS.DataSource = lookUpFields.DataSource;
            lookUpDriverS.DataSource = lookUpDriver.DataSource;
            lookUpAgregatS.DataSource = lookUpAgregat.DataSource;
        }

        public void UpdateView(GMapTrack tr,IVehicle vehicle, int indexInArray, Point pt)
        {
            try
            {
                if (tr == null || indexInArray == -1) return;
                var gpoint = (GpsData)tr.GeoPoints[indexInArray];
                if ((int)rgSelectPoint.EditValue == 1)
                {
                    clearFirstGrid();
                    dataGpsIdRowF.Properties.Value = gpoint.Id.ToString();
                    _gps_first = gpoint.Id;
                    indexF.Properties.Value = indexInArray.ToString().Trim();
                    _ind_first = GetPointNumber(_gps_first);// indexInArray;
                    dateTimeRowF.Properties.Value = gpoint.Time;
                   if (_ind_second == -1 && _gps_second == 0) rgSelectPoint.SelectedIndex = 1;
                }
                else if ((int)rgSelectPoint.EditValue == 2)
                {
                    clearFecondGrid();
                    dataGpsIdRowS.Properties.Value = gpoint.Id.ToString();
                    _gps_second = gpoint.Id;
                    indexS.Properties.Value = indexInArray.ToString().Trim();
                    //_ind_second = indexInArray;
                    _ind_second = GetPointNumber(_gps_second);
                    dateTimeRowS.Properties.Value = gpoint.Time;
                    if (_ind_second == -1 && _gps_second == 0) rgSelectPoint.SelectedIndex = 1;
                }

                if (_ind_first != POINT_DEACTIVE & _ind_second != POINT_DEACTIVE)
                {
                    if (_ind_first < _ind_second)
                        DrawMainTrack(_ind_second,_ind_first);
                    else
                        DrawMainTrack(_ind_first, _ind_second);
                    sbtRecordCreate.Enabled = true;
                }
                else
                {
                    lbPath.Text = string.Format(Resources.TrackFragmentInfor  ,0,0);
                    sbtRecordCreate.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }

        }

        void clearFirstGrid()
        {
            _ind_first = 0;
            _gps_first = 0;
            dataGpsIdRowF.Properties.Value = String.Empty;
            indexF.Properties.Value = String.Empty; 
            dateTimeRowF.Properties.Value = String.Empty;
        }

        void clearFecondGrid()
        {
            _ind_second = 0;
            _gps_second = 0;
            dataGpsIdRowS.Properties.Value = String.Empty;
            indexS.Properties.Value = String.Empty;
            dateTimeRowS.Properties.Value = String.Empty;
        }

        private void rgSelectPoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((int)rgSelectPoint.EditValue == 1)
            {
                vgFirst.Enabled = true;
                vgSecond.Enabled = false;
            }
            else if ((int)rgSelectPoint.EditValue == 2)
            {
                vgFirst.Enabled = false;
                vgSecond.Enabled = true;
            }
        }

        private void sbtExit_Click(object sender, EventArgs e)
        {
            if (ResetPointSelection != null) ResetPointSelection();
            Close();
        }



        void Localization()
        {
            indexF.Properties.Caption = Resources.NumberInTrack;
            indexS.Properties.Caption = indexF.Properties.Caption;

            dateTimeRowF.Properties.Caption = Resources.DateTime;
            dateTimeRowS.Properties.Caption = dateTimeRowF.Properties.Caption;

            fieldF.Properties.Caption = Resources.Field;
            fieldS.Properties.Caption = fieldF.Properties.Caption;

            driverF.Properties.Caption = Resources.Driver;
            driverS.Properties.Caption = driverF.Properties.Caption;

            agregatF.Properties.Caption = Resources.Agregat;
            agregatS.Properties.Caption = agregatF.Properties.Caption;

            workF.Properties.Value = Resources.Work;
            workS.Properties.Value = workF.Properties.Value;

            Text = Resources.SelectRecordsPoints;

            _first.Properties.Caption = Resources.FirstPoint;
            _second.Properties.Caption = Resources.SecondPoint;

            rgSelectPoint.Properties.Items[0].Description = Resources.FirstPoint;
            rgSelectPoint.Properties.Items[1].Description = Resources.SecondPoint;

            peTrack.Properties.NullText = Resources.CheckZone;

            sbtExit.Text = Resources.Out;
            sbtRecordCreate.Text = Resources.CreateOrdersRecord; 
        }

        public void DrawMainTrack(int startPoint, int endPoint)
        {
            try
            {
                PointLatLng[] points = new PointLatLng[_track.GeoPoints.Count];
                for (int i = 0; i < _track.GeoPoints.Count; i++)
                {
                    points[i] = new PointLatLng(_track.GeoPoints[i].LatLng.Lat, _track.GeoPoints[i].LatLng.Lng);
                }
                BmpLatLng bll = new BmpLatLng(points, peTrack.Width, peTrack.Height);
                //if (peTrack.Image != null) peTrack.Image.Dispose();
                peTrack.Image = bll.DrawTrackOnBitmap(startPoint, endPoint);
                
                if ((startPoint + endPoint) > 0)
                    lbPath.Text = string.Format(Resources.TrackFragmentInfor, GetKilometrage(), GetMinutes());
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show(ex.Message, string.Format("DrawMainTrack {0},{1}", startPoint, endPoint));
            }
        }

        private int GetPointNumber(System.Int64 DataGPD)
        {
            for (int i = 0; i < _track.GeoPoints.Count; i++)
			{
			  if(((GpsData)_track.GeoPoints[i]).Id == DataGPD) return  i;
			}
            return 0;
        }

        private double GetKilometrage()
        {
            double path = 0;
            IGeoPoint[] points = GetSelectedPoints();
            if (points == null) return 0;
            PointLatLng previous = points[0].LatLng;
            for (int i = 1; i < points.Length ; i++)
            {
                path += —GeoDistance.GetDistance(previous, points[i].LatLng );
                previous = points[i].LatLng;
            }
            return Math.Round(path,2);
        }

        private int GetMinutes()
        {
            if (GetStartEndDates())
            {
                return (int)Math.Round(_dateEnd.Subtract(_dateStart).TotalMinutes,0);   
            }
            else
            {
                return 0;
            }
        }

        private IGeoPoint[] GetSelectedPoints()
        {
            int start = Math.Min(_ind_first, _ind_second);
            int end = Math.Max(_ind_first, _ind_second);
            if (end == 0) return null;
            IGeoPoint[] points = new IGeoPoint[end + 1 - start];
            _track.GeoPoints.CopyTo(start, points, 0, end + 1 - start);
            return points;
        }

        private void sbtRecordCreate_Click(object sender, EventArgs e)
        {
            CreateRecord();
        }

        private void CreateRecord()
        {
            if (!TestParametersForCreateRecord()) return;
            IGeoPoint[] points = GetSelectedPoints();
            if (points != null && points.Length > 0 && (DialogResult.Yes == XtraMessageBox.Show(Resources.ConfirmRecordCreation, Resources.ApplicationName, MessageBoxButtons.YesNo)))
            {
                if (GetChildRecord != null)
                {
                    if (GetStartEndDates())
                    {
                        var oir = new OrderItemRecord(_parent.IdMain, _id_field, _id_driver, _id_agregat, _id_work, _dateStart, _dateEnd, _parent.AgrInputSource, _parent.DrvInputSource);
                        GetChildRecord(oir);
                        this.Close();
                    }
                }
            }
        }

        bool TestParametersForCreateRecord()
        {
            var asi = new AgroSetItem();
            int minutesForStart = asi.MinTimeInFieldForCreateRecord;
            if (asi.MinTimeInFieldForCreateRecord >= GetMinutes())
            {
                XtraMessageBox.Show(string.Format(Resources.LimitTimeForRecord, minutesForStart), Resources.ApplicationName);  
                return false;
            }
            else
            return true;
        }

        private bool GetStartEndDates()
        {
            DateTime dateF;
            DateTime dateS;
            if (DateTime.TryParse(dateTimeRowF.Properties.Value.ToString(), out dateF) && DateTime.TryParse(dateTimeRowS.Properties.Value.ToString(), out dateS))
            {
                if (dateS.Subtract(dateF).TotalSeconds > 0)
                {
                    _dateStart = dateF;
                    _dateEnd = dateS;
                }
                else
                {
                    _dateStart = dateS;
                    _dateEnd = dateF;
                }
                return true;
            }
            else
                return false;
        }

        #region CHANGE RECORDS PARAMETERS
        private void lookUpFields_EditValueChanged(object sender, EventArgs e)
        {
            SetField(sender, fieldS);
        }
        private void lookUpFieldsS_EditValueChanged(object sender, EventArgs e)
        {
            SetField(sender, fieldF);
        }

        private void lookUpWork_EditValueChanged(object sender, EventArgs e)
        {
            SetWork(sender, workS);
        }

        private void lookUpWorkS_EditValueChanged(object sender, EventArgs e)
        {
            SetWork(sender, workF);
        }

        private void lookUpDriver_EditValueChanged(object sender, EventArgs e)
        {
            SetDriver(sender, driverS);
        }

        private void lookUpDriverS_EditValueChanged(object sender, EventArgs e)
        {
            SetDriver(sender, driverF);
        }

        private void lookUpAgregat_EditValueChanged(object sender, EventArgs e)
        {
            SetAgregat(sender, agregatS);
        }

        private void lookUpAgregatS_EditValueChanged(object sender, EventArgs e)
        {
            SetAgregat(sender, agregatF);
        }

        void SetField(object sender, EditorRow fieldOtherPoint)
        {
            DataRowView row = GetLookUpRow(sender);
            _id_field = (int)row["Id"];
            fieldOtherPoint.Properties.Value = _id_field;
        }


        void SetWork(object sender, EditorRow workOtherPoint)
        {
            DataRowView row = GetLookUpRow(sender);
            _id_work = (int)row["Id"];
            workOtherPoint.Properties.Value = _id_work;
        }

        void SetAgregat(object sender, EditorRow agrOtherPoint)
        {
            DataRowView row = GetLookUpRow(sender);
            _id_agregat = (int)row["Id"];
            agrOtherPoint.Properties.Value = _id_agregat;
        }

        void SetDriver(object sender, EditorRow driverOtherPoint)
        {
            DataRowView row = GetLookUpRow(sender);
            _id_driver = (int)row["id"];
            driverOtherPoint.Properties.Value = _id_driver;
        }

        private static DataRowView GetLookUpRow(object sender)
        {
            DevExpress.XtraEditors.LookUpEdit editor = (sender as DevExpress.XtraEditors.LookUpEdit);
            DataRowView row = editor.Properties.GetDataSourceRowByKeyValue(editor.EditValue) as
            DataRowView;
            return row;
        } 
        #endregion
    }
}