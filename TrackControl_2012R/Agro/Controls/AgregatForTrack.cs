﻿using System.Drawing; 

namespace Agro.Controls
{
    public struct AgregatForTrack
    {
        public Color ColorAgregat;
        public double WidthAgregat;
        public AgregatForTrack(Color colorAgregat,double widthAgregat)
        {
            ColorAgregat = colorAgregat;
            WidthAgregat = widthAgregat;
        }
    }
}
