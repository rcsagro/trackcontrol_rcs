﻿using System;
using System.Windows.Forms;
using Agro.Controls;
using TrackControl.General.Core;
using Agro.Dictionaries;

namespace Agro
{
    public class AgroController : Singleton<AgroController>, IDisposable 
    {
        private bool _isStarted;
        MainAgro _view;
        public double WidthAgregatDefault { get; set; }
        public AgroMapOrder MapOrderControl { get; set; }

        public AgroController()
        {

        }
        public void Start()
        {
            if (_isStarted && (_view != null))
            {
                 _view.BringToFront();
                return; 
            }
            SetInitValues();
            _view = new MainAgro();
            _view.Show();
            _isStarted = true;
            MapOrderControl = new AgroMapOrder();

        }

        public void Dispose()
        {
            _isStarted = false;
            if (MapOrderControl != null)
            {
                MapOrderControl.Dispose();
                MapOrderControl = null;
            }
            if (_view != null) _view = null;
        }

        private void SetInitValues()
        {
            using (DictionaryAgroAgregat daa = new DictionaryAgroAgregat())
            {
                WidthAgregatDefault = daa.GetWidthDefault();
            }
        }

        public void ViewOrder(int idOrder)
        {
            //new Order(idOrder, false).ShowDialog();
            using (Form frm = new Order(idOrder, false))
            {
                frm.ShowDialog();
            }

        }
    }
}
