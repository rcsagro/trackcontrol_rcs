using System;

namespace TrackControl.DbPatch
{
    /// <summary>
    /// ������� �������� ��������.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Singleton<T> where T : new()
    {
        private Singleton()
        {
        }

        public static T Instance
        {
            get { return SingletonCreator.instance; }
        }

        private class SingletonCreator
        {
            static SingletonCreator()
            {
            }

            internal static readonly T instance = new T();
        }
    }
}
