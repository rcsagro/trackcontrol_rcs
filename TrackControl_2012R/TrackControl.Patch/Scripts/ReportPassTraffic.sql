/* ������ �� ��������������� */

CREATE TABLE IF NOT EXISTS Report_Pass_Zones (
  ID INT(11) AUTO_INCREMENT,
  ZoneID INT(11) NOT NULL COMMENT '������������� ��',
  PRIMARY KEY (ID),
  INDEX IDX_ZoneID USING BTREE (ZoneID),
  CONSTRAINT FK_RPZ_Zones FOREIGN KEY (ZoneID) REFERENCES zones (Zone_ID) ON DELETE CASCADE
)ENGINE = INNODB DEFAULT CHARSET=cp1251 COMMENT '������ ���, ������������ � ������� �� ���������������.';
