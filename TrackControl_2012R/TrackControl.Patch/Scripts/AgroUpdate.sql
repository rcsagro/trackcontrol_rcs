﻿DROP PROCEDURE IF EXISTS UpdateAgro;
CREATE PROCEDURE UpdateAgro ()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Обновление базы AGRO'
BEGIN
  IF EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "agro_order")
  THEN
	
	CREATE TABLE IF NOT EXISTS agro_datagps(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Id_main INT(11) NOT NULL,
  Lon_base DOUBLE NOT NULL,
  Lat_base DOUBLE NOT NULL,
  Lon_dev VARCHAR(255) NOT NULL,
  Lat_dev VARCHAR(255) NOT NULL,
  Speed_base DOUBLE NOT NULL,
  Speed_dev VARCHAR(255) NOT NULL,
  PRIMARY KEY (Id),
  INDEX FK_agro_datagps_agro_ordert_Id (Id_main),
  CONSTRAINT FK_agro_datagps_agro_ordert_Id FOREIGN KEY (Id_main)
  REFERENCES agro_ordert (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

	CREATE TABLE IF NOT EXISTS   agro_agregat_vehicle(
	  Id INT(11) NOT NULL AUTO_INCREMENT,
	  Id_agregat INT(11) NOT NULL DEFAULT 0,
	  Id_vehicle INT(11) NOT NULL DEFAULT 0,
	  PRIMARY KEY (Id),
	  INDEX FK_agro_agregat_vehicle_agro_agregat_Id (Id_agregat),
	  INDEX FK_agro_agregat_vehicle_vehicle_id (Id_vehicle),
	  CONSTRAINT FK_agro_agregat_vehicle_agro_agregat_Id FOREIGN KEY (Id_agregat)
	  REFERENCES  agro_agregat (Id) ON DELETE CASCADE ON UPDATE CASCADE,
	  CONSTRAINT FK_agro_agregat_vehicle_vehicle_id FOREIGN KEY (Id_vehicle)
	  REFERENCES  vehicle (id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Агрегаты по умолчанию для транспортных средств';	
	
	
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_ordert" AND COLUMN_NAME = "Confirm")
    THEN
      ALTER TABLE `agro_ordert` ADD COLUMN `Confirm` BOOL NOT NULL DEFAULT 1 COMMENT 'Подтверждение обработки площади';
    END IF;

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Id_main")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Id_main` INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на элемент верхнего уровня в иерархии';
    END IF;

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "IsGroupe")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `IsGroupe` TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'Запись - группа';
    END IF;      
     
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "InvNumber")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `InvNumber` CHAR (50) DEFAULT NULL COMMENT 'Инвентарный номер агрегата';
    END IF;      
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Id_work")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Id_work` INT (11) NOT NULL DEFAULT 0 COMMENT 'Вид работ по умолчанию';
    END IF; 

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Identifier")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Identifier` SMALLINT (5) UNSIGNED DEFAULT NULL COMMENT '10 битный идентификатор агрегата';
    END IF;
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Def")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Def` TINYINT (1) DEFAULT 0 COMMENT 'Агрегат по умолчанию для нарядов';
    END IF;
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_order" AND COLUMN_NAME = "PathWithoutWork")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PathWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
      ALTER TABLE `agro_order` ADD COLUMN `SquareWorkDescript` TEXT DEFAULT NULL COMMENT 'Обработанная площадь';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях,  всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях,  всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensAvgSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях, л/га:';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensAvgSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях, л/га:';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Расход на переездах, всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Расход на переездах, всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensAvgWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensAvgWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
    END IF;    
    
    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "Date" 
		  AND Data_type ="DATETIME")
    THEN
      ALTER TABLE `agro_order` MODIFY `Date` DATETIME COMMENT 'Дата наряда на работы(new type - DATE->DATETIME)';
    END IF;
    
    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "SquareWorkDescripOverlap")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `SquareWorkDescripOverlap` TEXT DEFAULT NULL COMMENT 'Обработанная площадь с учетом перекрытий всех записей';
      ALTER TABLE `agro_order` ADD COLUMN `FactSquareCalcOverlap` DOUBLE DEFAULT 0 COMMENT 'Факт обработанная площадь полученная расчетом с учетом перекрытий всех записей';
    END IF;
    
    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_fieldgroupe'
		  AND Column_Name = "Id_Zonesgroup")
    THEN
      ALTER TABLE `agro_fieldgroupe` ADD COLUMN `Id_Zonesgroup` INT DEFAULT 0  COMMENT 'Группа контрольных зон на основании которой создана группа полей';
    END IF;
    
    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "DateLastRecalc")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `DateLastRecalc` DATETIME  COMMENT 'Дата последнего пересчета наряда';
      UPDATE `agro_order` SET `DateLastRecalc` = `Date`; 
    END IF;
    
    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "UserLastRecalc")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `UserLastRecalc` VARCHAR(127) DEFAULT NULL  COMMENT 'Пользователь, последней пересчитавший наряд';
    END IF;    
    
        IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "UserCreated")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `UserCreated` VARCHAR(127) DEFAULT NULL  COMMENT 'Пользователь, создавший наряд';
    END IF;    
    
        IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "PointsValidity")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PointsValidity` INT(11) DEFAULT NULL COMMENT 'Валидность данных';
    END IF;      
    
        IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "PointsCalc")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PointsCalc` INT(11) DEFAULT 0 COMMENT 'Количество точек (по Log_ID) расчетное';
    END IF;   
    
        IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "PointsFact")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PointsFact` INT(11) DEFAULT 0 COMMENT 'Количество точек (по Log_ID) фактическое';
    END IF;       
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "PointsIntervalMax")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PointsIntervalMax` VARCHAR(20) DEFAULT NULL COMMENT 'Максимальный интервал между точками';
    END IF;  
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "StateOrder")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `StateOrder` INT(11) NOT NULL DEFAULT 0 COMMENT 'Статус наряда';
    END IF;   
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "BlockUserId")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `BlockUserId` INT(11) NOT NULL DEFAULT 0 COMMENT 'Код пользователя, работающего с документом';
    END IF;   
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "BlockDate")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `BlockDate` DATETIME DEFAULT NULL COMMENT 'Дата начала работы с документом';
    END IF;  
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_ordert'
		  AND Column_Name = "PointsValidity")
    THEN
      ALTER TABLE `agro_ordert` ADD COLUMN `PointsValidity` INT(11) NOT NULL DEFAULT 0 COMMENT 'Валидность данных';
    END IF;  
    
   IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_ordert'
		  AND Column_Name = "LockRecord")
    THEN
      ALTER TABLE `agro_ordert` ADD COLUMN `LockRecord` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Запись заблокирована для пересчетов';
    END IF;  
    
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "agro_agregat") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT 'Код внешней базы';
	END IF;	
	
	    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "agro_work") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `agro_work` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT 'Код внешней базы';
	END IF;	
	
	    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "HasSensor")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `HasSensor` TINYINT (1) DEFAULT 0 COMMENT 'Имеет датчик идентификации рабочего состояния';
    END IF;
    
    	    IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat_vehicle"
		  AND Column_Name = "Id_sensor")
    THEN
      ALTER TABLE `agro_agregat_vehicle` ADD COLUMN `Id_sensor` INT NOT NULL DEFAULT 0 COMMENT 'Датчик транспортного средства';
    END IF;
	
CREATE TABLE IF NOT EXISTS agro_fueling(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Id_main INT(11) DEFAULT NULL COMMENT 'Ссылка на наряд',
  LockRecord TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Запись заблокирована для редактирования',
  Lat DOUBLE NOT NULL DEFAULT 0,
  Lng DOUBLE NOT NULL DEFAULT 0,
  DateFueling DATETIME NOT NULL COMMENT 'Дата заправки / слива',
  ValueSystem DOUBLE NOT NULL DEFAULT 0 COMMENT 'Данные системы',
  ValueDisp DOUBLE NOT NULL DEFAULT 0 COMMENT 'Данные диспетчера',
  ValueStart DOUBLE NOT NULL DEFAULT 0 COMMENT 'Значение перед заправкой / сливом',
  Location VARCHAR(255) DEFAULT NULL,
  Remark VARCHAR(255) DEFAULT NULL COMMENT 'Комментарий к изменению значения диспетчером',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Ручное редактирование значений сливов и заправок';	


CREATE TABLE IF NOT EXISTS agro_work_types(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) NOT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Работа в поле, переезд вне поля,переезд по полю';

 	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_work"
		  AND Column_Name = "TypeWork")
    THEN
		INSERT INTO agro_work_types (`Name`) VALUES ('Работа в поле'); 
		INSERT INTO agro_work_types (`Name`) VALUES ('Переезд вне поля'); 
		INSERT INTO agro_work_types (`Name`) VALUES ('Переезд по полю'); 
		ALTER TABLE `agro_work` ADD COLUMN `TypeWork` INT NOT NULL DEFAULT 1 COMMENT 'Работа в поле, переезд вне поля,переезд по полю';
		ALTER TABLE `agro_work`
		ADD CONSTRAINT `FK_agro_work_agro_work_types_Id` FOREIGN KEY (TypeWork)
		REFERENCES agro_work_types (Id) ON DELETE NO ACTION ON UPDATE NO ACTION;
    END IF;

	CREATE TABLE IF NOT EXISTS agro_workgroup(
	  Id INT(11) NOT NULL AUTO_INCREMENT,
	  Name CHAR(50) NOT NULL COMMENT 'Название',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Группы видов работ';

	 	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_work"
		  AND Column_Name = "Id_main")
    THEN
		ALTER TABLE `agro_work` ADD COLUMN `Id_main` INT  NULL  COMMENT 'Группа видов работ';
		ALTER TABLE `agro_work`
		ADD CONSTRAINT FK_agro_work_agro_workgroup_Id FOREIGN KEY (Id_main)
		REFERENCES agro_workgroup (Id) ON DELETE NO ACTION ON UPDATE NO ACTION;
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_ordert"
		  AND Column_Name = "AgrInputSource")
    THEN
		ALTER TABLE `agro_ordert` ADD COLUMN `AgrInputSource` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Источник появления навесного оборудование: RFID метка,ручной ввод,значение из установок';
    END IF;

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_ordert"
		  AND Column_Name = "DrvInputSource")
    THEN
		ALTER TABLE `agro_ordert` ADD COLUMN `DrvInputSource` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Источник появления водителя: RFID метка,ручной ввод,значение из справочника';
    END IF;


		CREATE TABLE IF NOT EXISTS agro_plan_state (
		  Id tinyint(4) NOT NULL AUTO_INCREMENT,
		  StatusName varchar(50) NOT NULL COMMENT 'Причина простоя',
		  StatusComment text DEFAULT NULL COMMENT 'Комментарий',
		  PRIMARY KEY (Id),
		  UNIQUE INDEX Id (Id)
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COLLATE cp1251_general_ci
		COMMENT = 'Статусы планового задания';

			INSERT INTO agro_plan_state (StatusName)
		SELECT * FROM (SELECT 'Активный') AS tmp
		WHERE NOT EXISTS (
			SELECT StatusName FROM agro_plan_state WHERE Id = 1
		) LIMIT 1;

		INSERT INTO agro_plan_state (StatusName)
		SELECT * FROM (SELECT 'Закрыт') AS tmp
		WHERE NOT EXISTS (
			SELECT StatusName FROM agro_plan_state WHERE Id = 2
		) LIMIT 1;

		INSERT INTO agro_plan_state (StatusName)
		SELECT * FROM (SELECT 'Отменен') AS tmp
		WHERE NOT EXISTS (
			SELECT StatusName FROM agro_plan_state WHERE Id = 3
		) LIMIT 1;


		CREATE TABLE  IF NOT EXISTS  agro_plan_downtimereason (
		  Id int(11) NOT NULL AUTO_INCREMENT,
		  ReasonName varchar(255) NOT NULL COMMENT 'Причина простоя',
		  ReasonComment text DEFAULT NULL COMMENT 'Комментарий',
		  PRIMARY KEY (Id)
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COLLATE cp1251_general_ci
		COMMENT = 'Причины простоя транспортного средства';

		CREATE TABLE IF NOT EXISTS  agro_plan (
		  Id int(11) NOT NULL AUTO_INCREMENT,
		  PlanDate datetime NOT NULL COMMENT 'Дата планового задания',
		  Id_mobitel int(11) NOT NULL DEFAULT 0 COMMENT 'Транспортное средство',
		  SquarePlanned double NOT NULL DEFAULT 0 COMMENT 'Планируемая для обработки площадь',
		  FuelAtStart double NOT NULL DEFAULT 0 COMMENT 'Топливо на начало планового задания',
		  FuelForWork double NOT NULL DEFAULT 0 COMMENT 'Объем топлива, которое разрешено отпустить данной технике для выполнения планового задания, л',
		  IdStatus tinyint(4) DEFAULT NULL COMMENT 'Режим наряд -ручной,автоматический, закрыт,простой',
		  Id_order int(11) DEFAULT NULL COMMENT 'Связь с нарядом',
		  PlanComment text DEFAULT NULL COMMENT 'Комментарий',
		  UserCreated varchar(127) DEFAULT NULL COMMENT 'Пользователь, создавший план',
		  IsDownTime bit(1) NOT NULL DEFAULT b'0' COMMENT 'Техника не учавствует в полевых работах',
		  IdDowntimeReason int(11) DEFAULT NULL COMMENT 'Причина простоя техники',
		  DowntimeStart date DEFAULT NULL COMMENT 'Дата начала простоя',
		  DowntimeEnd date DEFAULT NULL COMMENT 'Дата ввода в эксплуатацию',
		  PRIMARY KEY (Id),
		  UNIQUE INDEX Id (Id),
		  CONSTRAINT FK_agro_plan_agro_plan_downtimereasons_Id FOREIGN KEY (IdDowntimeReason)
		  REFERENCES agro_plan_downtimereason (Id) ON DELETE RESTRICT ON UPDATE RESTRICT,
		  CONSTRAINT FK_agro_plan_agro_plan_status_Id FOREIGN KEY (IdStatus)
		  REFERENCES agro_plan_state (Id) ON DELETE RESTRICT ON UPDATE RESTRICT
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COLLATE cp1251_general_ci
		COMMENT = 'Заголовок планового задания';

		CREATE TABLE  IF NOT EXISTS  agro_plant (
		  Id int(11) NOT NULL AUTO_INCREMENT,
		  Id_main int(11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на план',
		  Id_field int(11) DEFAULT 0 COMMENT 'Ссылка на поле',
		  Id_driver int(11) DEFAULT 0 COMMENT 'Водитель',
		  Id_work int(11) DEFAULT 0 COMMENT 'Ссылка на работу',
		  Id_agregat int(11) DEFAULT 0 COMMENT 'Навесное оборудование',
		  Remark text DEFAULT NULL COMMENT 'Комментарий',
		  TimeStart datetime DEFAULT NULL COMMENT 'Время начала работы',
		  PRIMARY KEY (Id),
		  CONSTRAINT FK_agro_plant_agro_plan_Id FOREIGN KEY (Id_main)
		  REFERENCES agro_plan (Id) ON DELETE CASCADE ON UPDATE CASCADE
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COMMENT = 'Содержимое планового задания';

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_plant"
		  AND Column_Name = "TimeStart")
    THEN
		ALTER TABLE `agro_plant` ADD COLUMN `TimeStart` datetime DEFAULT NULL COMMENT 'Время начала работы';
    END IF;

		CREATE TABLE IF NOT EXISTS agro_season (
	  Id int(11) NOT NULL AUTO_INCREMENT,
	  DateStart date NOT NULL COMMENT 'Дата начала сезона',
	  DateEnd date NOT NULL COMMENT 'Дата окончания сезона',
	  Id_culture int(11) NOT NULL COMMENT 'Культура',
	  SquarePlan decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Запланированная посевная площадь, га',
	  Comment text DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id),
	  CONSTRAINT FK_agro_season_agro_culture_Id FOREIGN KEY (Id_culture)
	  REFERENCES agro_culture (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_field"
		  AND Column_Name = "IsOutOfDate")
    THEN
		ALTER TABLE `agro_field` ADD COLUMN `IsOutOfDate` bit(1)  NOT NULL DEFAULT b'0' COMMENT 'Поле устарело и в расчетах не учавствует';
    END IF;

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_field"
		  AND Column_Name = "IsOutOfDate")
    THEN
		ALTER TABLE `agro_field` ADD COLUMN `IsOutOfDate` bit(1)  NOT NULL DEFAULT b'0' COMMENT 'Поле устарело и в расчетах не учавствует';
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_work"
		  AND Column_Name = "TrackColor")
    THEN
		ALTER TABLE `agro_work` ADD COLUMN `TrackColor` int(11) NOT NULL DEFAULT 0 COMMENT 'Цвет трека работы на карте';
    END IF;

			CREATE TABLE  IF NOT EXISTS  agro_fieldseason_tc (
		  Id int(11) NOT NULL AUTO_INCREMENT,
		  IdSeason int(11) NOT NULL,
		  IdField int(11) NOT NULL,
		  Comment text DEFAULT NULL COMMENT 'Комментарий',
		  DateInit datetime NOT NULL COMMENT 'Дата создания',
		  UserCreated varchar(127) NOT NULL COMMENT 'Пользователь, создавший тех карту ',
		  DateStart datetime NOT NULL COMMENT 'Дата начала работ (по умолчанию - дата начала сезона)',
		  DateEnd datetime NOT NULL COMMENT 'Дата начала работ (по умолчанию - дата окончания сезона)',
		  PRIMARY KEY (Id),
		  CONSTRAINT FK_agro_fieldseason_tc_agro_field_Id FOREIGN KEY (IdField)
		  REFERENCES agro_field (Id) ON DELETE RESTRICT ON UPDATE RESTRICT,
		  CONSTRAINT FK_agro_fieldseason_tc_agro_season_Id FOREIGN KEY (IdSeason)
		  REFERENCES agro_season (Id) ON DELETE RESTRICT ON UPDATE RESTRICT
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COLLATE cp1251_general_ci
		COMMENT = 'Технологическая карта поля';

			CREATE TABLE   IF NOT EXISTS   agro_fieldseason_tct (
	  Id int(11) NOT NULL AUTO_INCREMENT,
	  Id_main int(11) DEFAULT NULL,
	  IdWork int(11) NOT NULL COMMENT 'Вид работ',
	  DateStartPlan datetime NOT NULL COMMENT 'Дата начала (план)',
	  DateEndPlan datetime NOT NULL COMMENT 'Дата окончания (план)',
	  WorkQtyPlan decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Плановое кол-во моточасов ',
	  DateStartFact datetime DEFAULT NULL COMMENT 'Дата начала (факт)',
	  DateEndFact datetime DEFAULT NULL COMMENT 'Дата окончания (факт)',
	  SquareGa decimal(10, 3) NOT NULL DEFAULT 0.000 COMMENT 'Фактически обработанная площадь, га',
	  SquarePercent decimal(5, 2) NOT NULL DEFAULT 0.00 COMMENT 'Фактически обработанная площадь, %',
	  Comment text DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id),
	  CONSTRAINT FK_agro_fieldseason_tct_agro_fieldseason_tc_Id FOREIGN KEY (Id_main)
	  REFERENCES  agro_fieldseason_tc (Id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	  CONSTRAINT FK_agro_fieldseason_tct_agro_work_Id FOREIGN KEY (IdWork)
	  REFERENCES  agro_work (Id) ON DELETE RESTRICT ON UPDATE RESTRICT
		)
		ENGINE = INNODB
		CHARACTER SET cp1251
		COLLATE cp1251_general_ci;

	CREATE TABLE    IF NOT EXISTS     agro_fieldseason_tct_fact (
	Id int(11) NOT NULL AUTO_INCREMENT,
	Id_main int(11) NOT NULL,
	IdOrder int(11) NOT NULL COMMENT 'Ссылка на наряд',
	IdDriver int(11) DEFAULT NULL,
	IdAgreg int(11) DEFAULT NULL,
	SquareFactGa decimal(10, 3) NOT NULL DEFAULT 0.000 COMMENT 'Фактически обработанная площадь (с учетом совм. обработки).',
	JointProcess bit(1) NOT NULL DEFAULT b'0' COMMENT 'Совместная обработка',
	SquareAfterRecalc decimal(10, 3) NOT NULL DEFAULT 0.000 COMMENT 'Фактически обработанная площадь (с учетом совм. обработки).',
	DateRecalc datetime DEFAULT NULL COMMENT 'Дата пересчета совместной обработки',
	UserRecalc varchar(127) NULL COMMENT 'Пользователь, создавший совместную обработку',
	PRIMARY KEY (Id),
	CONSTRAINT FK_agro_fieldseason_tct_fact_agro_agregat_Id FOREIGN KEY (IdAgreg)
	REFERENCES  agro_agregat (Id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT FK_agro_fieldseason_tct_fact_agro_fieldseason_tct_Id FOREIGN KEY (Id_main)
	REFERENCES  agro_fieldseason_tct (Id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT FK_agro_fieldseason_tct_fact_driver_id FOREIGN KEY (IdDriver)
	REFERENCES  driver (id) ON DELETE RESTRICT ON UPDATE RESTRICT
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

	ALTER TABLE `agro_fieldseason_tct_fact` MODIFY `IdDriver` int(11) NULL DEFAULT NULL COMMENT 'Ссылка на водителя';
	ALTER TABLE `agro_fieldseason_tct_fact` MODIFY `IdAgreg` int(11) NULL DEFAULT NULL COMMENT 'Ссылка на навесное оборудование';
	ALTER TABLE `agro_fieldseason_tct_fact` MODIFY `UserRecalc`varchar(127)  NULL COMMENT 'Пользователь, создавший совместную обработку';

	
	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat_vehicle"
		  AND Column_Name = "Id_state")
    THEN
		ALTER TABLE `agro_agregat_vehicle` ADD COLUMN `Id_state` int(11) NOT NULL DEFAULT 0 COMMENT 'Рабочий диапазон угломерного датчика';
	ELSE 
		ALTER TABLE `agro_agregat_vehicle` MODIFY `Id_state` int(11) NOT NULL DEFAULT 0;	
    END IF;

	CREATE TABLE IF NOT EXISTS ntf_agro_works(
  Id int(11) NOT NULL AUTO_INCREMENT,
  Id_main int(11) NOT NULL,
  WorkId int(11) NOT NULL,
  PRIMARY KEY (Id),
  CONSTRAINT FK_ntf_agro_works_ntf_main_Id FOREIGN KEY (Id_main)
  REFERENCES ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "IsUseAgregatState")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `IsUseAgregatState`  tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Использовать параметры рабочего состояния агрегата';
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "MinStateValue")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `MinStateValue` double NOT NULL DEFAULT 0 COMMENT 'Нижний предел состояния датчика агрегата';
    END IF;

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "MaxStateValue")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `MaxStateValue` double NOT NULL DEFAULT 0 COMMENT 'Верхний предел состояния датчика агрегата';
    END IF;

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "LogicState")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `LogicState` tinyint(1) NOT NULL DEFAULT 0  COMMENT 'Логическое состояние датчика агрегата';
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "SensorAlgorithm")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `SensorAlgorithm` SMALLINT (5) NOT NULL DEFAULT 0  COMMENT 'Алгоритм датчика агрегата';
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_ordert"
		  AND Column_Name = "Id_event_manually")
    THEN
		ALTER TABLE `agro_ordert` ADD COLUMN `Id_event_manually` int(11) COMMENT 'Ссылка на запись в таблице agro_event_manually';
    END IF;

	CREATE TABLE IF NOT EXISTS agro_event_manually(
	  Id int(11) NOT NULL AUTO_INCREMENT,
	  Id_mobitel int(11) NOT NULL DEFAULT 0 COMMENT 'Код объекта управления',
	  EventName varchar(127) DEFAULT NULL COMMENT 'Произвольное описание события',
	  TimeStart datetime DEFAULT NULL COMMENT 'Время начала события',
	  TimeEnd datetime DEFAULT NULL COMMENT 'Время окончания события',
	  Id_work int(11) NULL COMMENT 'Работа, соответствующая событию',
	  Info_1 varchar(127) DEFAULT NULL COMMENT 'Служебное информационное поле',
	  Info_2 varchar(127) DEFAULT NULL COMMENT 'Служебное информационное поле',
	  Info_3 varchar(127) DEFAULT NULL COMMENT 'Служебное информационное поле',
	  Info_4 varchar(127) DEFAULT NULL COMMENT 'Служебное информационное поле',
	  Info_5 varchar(127) DEFAULT NULL COMMENT 'Служебное информационное поле',
  PRIMARY KEY (Id)
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

		IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "LogicStateBounce")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `LogicStateBounce` int(11) NOT NULL DEFAULT 0  COMMENT 'Время перехода в неактивное состояние (сек)';
    END IF;


	CREATE TABLE IF NOT EXISTS agro_sensorkoefficients (
	  ID bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	  AgregatID bigint(20) UNSIGNED NOT NULL,
	  UserValue double NOT NULL,
	  SensorValue double NOT NULL,
	  K double NOT NULL,
	  b double NOT NULL,
	  PRIMARY KEY (ID)
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;

  IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "K")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `K` double(10, 5) NOT NULL DEFAULT 1.00000;
    END IF;

	  IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "B")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `B` double DEFAULT 0 COMMENT 'Shift coefficient';
    END IF;

		  IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_agregat"
		  AND Column_Name = "S")
    THEN
		ALTER TABLE `agro_agregat` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
    END IF;

	IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = "agro_ordert"
		  AND Column_Name = "FactSquareJointProcessing")
    THEN
		ALTER TABLE `agro_ordert` ADD COLUMN `FactSquareJointProcessing` double DEFAULT 0 COMMENT 'Факт обработанная площадь совместно с другими нарядами';
    END IF;

	IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_workgroup" AND COLUMN_NAME = "IsSpeedControl")
    THEN
      ALTER TABLE `agro_workgroup` ADD COLUMN `IsSpeedControl` BOOL NOT NULL DEFAULT 0 COMMENT 'Контролировать скорость группы операций';
    END IF;

	IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_workgroup" AND COLUMN_NAME = "IsDepthControl")
    THEN
      ALTER TABLE `agro_workgroup` ADD COLUMN `IsDepthControl` BOOL NOT NULL DEFAULT 0 COMMENT 'Контролировать скорость группы операций';
    END IF;

	IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_fieldseason_tct" AND COLUMN_NAME = "IsClosed")
    THEN
      ALTER TABLE `agro_fieldseason_tct` ADD COLUMN `IsClosed` BOOL NOT NULL DEFAULT 0 COMMENT 'Операция закрыта';
    END IF;

		IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_season" AND COLUMN_NAME = "IsClosed")
    THEN
      ALTER TABLE `agro_season` ADD COLUMN `IsClosed` BOOL NOT NULL DEFAULT 0 COMMENT 'Сезон закрыт';
    END IF;

END IF; 
END;
CALL UpdateAgro();
DROP PROCEDURE IF EXISTS UpdateAgro;