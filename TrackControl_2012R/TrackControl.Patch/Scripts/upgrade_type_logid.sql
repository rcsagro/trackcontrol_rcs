﻿DROP PROCEDURE IF EXISTS UpdateTypeLogID;
CREATE PROCEDURE UpdateTypeLogID ()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Обновление типа LogID'
BEGIN

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer")
    THEN
ALTER TABLE datagpsbuffer MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbufferlite")
    THEN
ALTER TABLE datagpsbufferlite MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_dr")
    THEN
ALTER TABLE datagpsbuffer_dr MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_on")
    THEN
ALTER TABLE datagpsbuffer_on MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_on64")
    THEN
ALTER TABLE datagpsbuffer_on64 MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_ontmp")
    THEN
ALTER TABLE datagpsbuffer_ontmp MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_ontmp64")
    THEN
ALTER TABLE datagpsbuffer_ontmp64 MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpsbuffer_pop")
    THEN
ALTER TABLE datagpsbuffer_pop MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = " datagpslost")
    THEN
ALTER TABLE datagpslost MODIFY Begin_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslost")
    THEN
ALTER TABLE datagpslost MODIFY End_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslosted")
    THEN
ALTER TABLE datagpslosted MODIFY Begin_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslosted")
    THEN
ALTER TABLE datagpslosted MODIFY End_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslost_on")
    THEN
ALTER TABLE datagpslost_on MODIFY Begin_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslost_on")
    THEN
ALTER TABLE datagpslost_on MODIFY End_LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslost_ontmp")
    THEN
ALTER TABLE datagpslost_ontmp MODIFY LogID integer;
END IF;

IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagpslost_rftmp")
    THEN
ALTER TABLE datagpslost_rftmp MODIFY LogID integer;
END IF;
END;
CALL UpdateTypeLogID();
DROP PROCEDURE IF EXISTS UpdateTypeLogID;
