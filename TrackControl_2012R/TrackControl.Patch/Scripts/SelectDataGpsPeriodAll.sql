DROP PROCEDURE IF EXISTS SelectDataGpsPeriodAll;
CREATE PROCEDURE SelectDataGpsPeriodAll(IN mobile int, IN beginperiod datetime, IN endperiod datetime,
IN timebegin varchar(9), IN timeend varchar(9), IN weekdays varchar(15), IN daymonth varchar(90))
BEGIN
  DECLARE time0 time DEFAULT '23:59:59';
  DECLARE time1 time DEFAULT '00:00:00'; 

  DROP TEMPORARY TABLE IF EXISTS tempperiod4;

  CREATE TEMPORARY TABLE tempperiod4 -- ��������� ������� ������� � ��������� ���������
  SELECT
    DataGps_ID AS DataGps_ID,
    Mobitel_ID AS Mobitel_ID,
    ((Latitude * 1.0000) / 600000) AS Lat,
    ((Longitude * 1.0000) / 600000) AS Lon,
    Altitude AS Altitude,
    FROM_UNIXTIME(UnixTime) AS `time`,
    (Speed * 1.852) AS speed,
    Valid AS Valid,
    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + (Sensor2 << 48) + (Sensor1 << 56)) AS sensor,
    Events AS Events,
    LogID AS LogID
  FROM datagps
  WHERE Mobitel_ID = mobile AND
  (UnixTime BETWEEN UNIX_TIMESTAMP(beginperiod) AND UNIX_TIMESTAMP(endperiod)) AND
  (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0)
  ORDER BY UnixTime;

  DROP TEMPORARY TABLE IF EXISTS tempweeks;

  IF weekdays != '' THEN

    CREATE TEMPORARY TABLE tempweeks (
      shead varchar(2)
    );

    SET @saTail = weekdays;
    WHILE @saTail != '' DO
      SET @sHead = SUBSTRING_INDEX(@saTail, ';', 1);
      SET @saTail = SUBSTRING(@saTail, LENGTH(@sHead) + 2);
      INSERT INTO tempweeks (shead)
        VALUES (@sHead);
    END WHILE;
  END IF;

  DROP TEMPORARY TABLE IF EXISTS tempday0;

  IF daymonth != '' THEN

    CREATE TEMPORARY TABLE tempday0 (
      shead varchar(2)
    );

    SET @saTail = daymonth;
    WHILE @saTail != '' DO
      SET @sHead = SUBSTRING_INDEX(@saTail, ';', 1);
      SET @saTail = SUBSTRING(@saTail, LENGTH(@sHead) + 2);
      INSERT INTO tempday0 (shead)
        VALUES (@sHead);
    END WHILE;
  END IF;

  -- getting selecting data
  IF (daymonth != '') AND (weekdays != '') THEN
    IF TIME(timebegin) < TIME(timeend) then
    SELECT
      *
    FROM tempperiod4
    WHERE ((DAY(`time`) IN (SELECT
      *
    FROM tempday0)) AND -- ������ �� ���� ������
    (WEEKDAY(`time`) IN (SELECT
      *
    FROM tempweeks)) AND -- ������ �� ���� ������
    (TIME(`time`) BETWEEN TIME(timebegin) AND TIME(timeend))); -- ������ ���
    END IF;

    IF TIME(timebegin) > TIME(timeend) then
    SELECT
      *
    FROM tempperiod4
    WHERE ((DAY(`time`) IN (SELECT
      *
    FROM tempday0)) AND -- ������ �� ���� ������
    (WEEKDAY(`time`) IN (SELECT
      *
    FROM tempweeks)) AND -- ������ �� ���� ������
    ((time(`time`) > time(timebegin) and time(`time`) < time(time0))
    OR (time(`time`) > time(time1) AND time(`time`) < time(timeend)))); -- ������ ���
    END IF;
    END IF;

      IF (daymonth = '') AND (weekdays != '') THEN
        IF TIME(timebegin) < TIME(timeend) then
        SELECT
          *
        FROM tempperiod4
        WHERE ((WEEKDAY(`time`) IN (SELECT
          *
        FROM tempweeks)) AND -- ������ �� ���� ������
        (TIME(`time`) BETWEEN TIME(timebegin) AND TIME(timeend))); -- ������ ���
          end IF;

        IF TIME(timebegin) > TIME(timeend) then
        SELECT
          *
        FROM tempperiod4
        WHERE ((WEEKDAY(`time`) IN (SELECT
          *
        FROM tempweeks)) AND -- ������ �� ���� ������
        ((time(`time`) > time(timebegin) and time(`time`) < time(time0))
    OR (time(`time`) > time(time1) AND time(`time`) < time(timeend)))); -- ������ ���
          end IF;
        END IF;

          IF (daymonth != '') AND (weekdays = '') THEN
            IF TIME(timebegin) < TIME(timeend) then
            SELECT
              *
            FROM tempperiod4
            WHERE ((DAY(`time`) IN (SELECT
              *
            FROM tempday0)) AND -- ������ �� ���� ������
            (TIME(`time`) BETWEEN TIME(timebegin) AND TIME(timeend))); -- ������ ���
              END IF;

            IF TIME(timebegin) > TIME(timeend) then
            SELECT
              *
            FROM tempperiod4
            WHERE ((DAY(`time`) IN (SELECT
              *
            FROM tempday0)) AND -- ������ �� ���� ������
            ((time(`time`) > time(timebegin) and time(`time`) < time(time0))
              OR (time(`time`) > time(time1) AND time(`time`) < time(timeend)))); -- ������ ���
              END IF;
            END IF;

              IF (daymonth = '') AND (weekdays = '') THEN
                IF TIME(timebegin) < TIME(timeend) then
                SELECT
                  *
                FROM tempperiod4
                WHERE (TIME(`time`) BETWEEN TIME(timebegin) AND TIME(timeend)); -- ������ ���
                  end IF;

                IF TIME(timebegin) > TIME(timeend) then
                SELECT
                  *
                FROM tempperiod4
                WHERE ((time(`time`) > time(timebegin) and time(`time`) < time(time0))
                  OR (time(`time`) > time(time1) AND time(`time`) < time(timeend))); -- ������ ���
                  end IF;
              END IF;
            END;
