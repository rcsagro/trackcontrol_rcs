DROP PROCEDURE IF EXISTS rcs__add_new__Column_to__DataGpsTbl;
CREATE PROCEDURE rcs__add_new__Column_to__DataGpsTbl()
BEGIN
  IF EXISTS
  (
    SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagps"
  )
  THEN
    IF NOT EXISTS(
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagps"
        AND	COLUMN_NAME = "SrvPacketID")
    THEN
      ALTER TABLE datagps ADD COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0' COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';
      ALTER TABLE datagps ADD INDEX IDX_MobitelIDSrvPacketID (Mobitel_ID, SrvPacketID);
    ELSE IF NOT EXISTS(
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "datagps"
        AND COLUMN_NAME = "SrvPacketID"
        AND DATA_TYPE = "bigint")	
      THEN 
        ALTER TABLE datagps MODIFY COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0' COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';
      END IF; 
    END IF;
  END IF;
END;
CALL rcs__add_new__Column_to__DataGpsTbl();
DROP PROCEDURE IF EXISTS rcs__add_new__Column_to__DataGpsTbl;