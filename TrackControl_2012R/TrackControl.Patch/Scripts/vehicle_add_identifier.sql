CREATE PROCEDURE Vehicles_Identifier_Add()
BEGIN
IF NOT EXISTS
	(
		SELECT * FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
                                  (COLUMN_NAME = "Identifier")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN Identifier int(11) DEFAULT 0 COMMENT '������������� ������';
	END IF;	
END;

CALL Vehicles_Identifier_Add();
DROP PROCEDURE IF EXISTS Vehicles_Identifier_Add;