DROP PROCEDURE IF EXISTS Vehicle_Team_Update;                                                                
CREATE PROCEDURE Vehicle_Team_Update ()
  NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '���������� ������ vehicle, team'
BEGIN
IF EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "sensorcoefficient" AND COLUMN_NAME = "b")
    THEN
    	 ALTER TABLE sensorcoefficient MODIFY b DOUBLE NOT NULL DEFAULT 0;
    END IF;

IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "team" AND COLUMN_NAME = "FuelWayKmGrp")
    THEN
      ALTER TABLE `team` ADD COLUMN `FuelWayKmGrp` double DEFAULT 0 COMMENT '����� ������� ������� �/100 ��';
    END IF;

IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "team" AND COLUMN_NAME = "FuelMotoHrGrp")
    THEN
      ALTER TABLE `team` ADD COLUMN `FuelMotoHrGrp` double DEFAULT 0 COMMENT '����� ������� ������� �� �������� ���� �/�������';
    END IF;

IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "vehicle" AND COLUMN_NAME = "FuelWayLiter")
    THEN
      ALTER TABLE `vehicle` ADD COLUMN `FuelWayLiter` double DEFAULT 0 COMMENT '������ ������� �/100 ��';
    END IF;

IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "vehicle" AND COLUMN_NAME = "FuelMotorLiter")
    THEN
      ALTER TABLE `vehicle` ADD COLUMN `FuelMotorLiter` double DEFAULT 0 COMMENT '������ ������� �/�������';
    END IF;

  IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "vehicle" AND COLUMN_NAME = "PcbVersionId")
    THEN
      ALTER TABLE `vehicle` ADD COLUMN `PcbVersionId` int(11) DEFAULT - 1 COMMENT '������ ���� ������� ����������� �� ������';
    END IF;
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "vehicle" AND COLUMN_NAME = "Category_id3")
    THEN
ALTER TABLE `vehicle` ADD COLUMN `Category_id3` int(11) DEFAULT NULL COMMENT '���������3 ����������';
    END IF;
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "vehicle" AND COLUMN_NAME = "Category_id4")
    THEN
ALTER TABLE `vehicle` ADD COLUMN `Category_id4` int(11) DEFAULT NULL COMMENT '���������4 ����������';
    END IF;
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "setting" AND COLUMN_NAME = "speedMin")
    THEN
      ALTER TABLE `setting` ADD COLUMN speedMin float NOT NULL DEFAULT 0 COMMENT '����������� �������� � ��/�';
    END IF;
  END;
CALL Vehicle_Team_Update();
DROP PROCEDURE IF EXISTS Vehicle_Team_Update;