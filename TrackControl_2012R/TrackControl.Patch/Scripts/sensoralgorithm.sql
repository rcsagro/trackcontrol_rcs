DROP PROCEDURE IF EXISTS UpdateTableSensorAlg;

CREATE PROCEDURE UpdateTableSensorAlg()
  SQL SECURITY INVOKER
  COMMENT '�������� ���������� Sensoralgorithm'
BEGIN

CREATE TABLE IF NOT EXISTS sensoralgorithms (
  ID int(11) NOT NULL AUTO_INCREMENT,
  Name char(60) NOT NULL,
  Description char(100) DEFAULT NULL,
  AlgorithmID int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID),
  UNIQUE INDEX ID (ID)
)
ENGINE = INNODB
AUTO_INCREMENT = 22
AVG_ROW_LENGTH = 963
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

ALTER TABLE sensoralgorithms MODIFY COLUMN Name VARCHAR(60);
ALTER TABLE sensoralgorithms MODIFY COLUMN Description VARCHAR(100);

SET @param = (SELECT AlgorithmID FROM sensoralgorithms s WHERE s.ID = 1);
IF (ISNULL(@param))
      THEN
INSERT INTO sensoralgorithms (`Name`, Description, AlgorithmID)
  VALUES ('���������� ������','��� ������� ������������ �������� ���./����.',1);
  END IF;

  set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 2);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(`Name`, Description, AlgorithmID)
  VALUES
  	('������ ��������� ���������','������ ������������ ��� ����������� � ���.',2);
  end IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 3);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������������','��� ������� ������������ �-�� ��������',3);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 4);
IF (ISNULL(@param))
          THEN
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ����������','�������� ���������� � ����',4);
ELSE
UPDATE sensoralgorithms 
SET
  Name = '������ ����������' -- Name - CHAR(45) NOT NULL
 ,Description = '�������� ���������� � ����' -- Description - CHAR(45)
 ,AlgorithmID = 4 -- AlgorithmID - INT(11) NOT NULL
WHERE
  AlgorithmID = 4; -- ID - INT(11) NOT NULL
 END IF;



  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 5);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ��������� ���������','��� ������� ������������ �-�� ��������',5);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 6);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����������','������ �����������',6);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 7);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� �������','������ ������ ������� (���)',7);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 8);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ �������','������ ������� ������� (���)',8);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 9);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������1','��� �������� ��������� ����������1',9);
  end IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 10);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������2','��� �������� ���������� ����������2',10);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 11);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����1','��� ������ ����������1',11);
  END IF;

SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 12);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����2','��� ������ ����������2',12);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 13);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ��������','��� ������������� ���������',13);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 14);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ���������� ����������','��� ������������� ��������� ���������',14);
  END IF;

SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 15);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����������������','��� �������� �������� � �����������������',15);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 16);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('��������-���������� ������','��� ����������� � ������ ��������',16);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 17);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	 ('��������� ������������-���������� ������','��� �������� ��������� ������/�������',17);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 18);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	 ('�����������-���������� ��� X,','��� �������� ���� ������� �� ��� X',18);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 19);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����������-���������� ��� Y,','��� �������� ���� ������� �� ��� Y',19);
  END IF;

  SET @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 20);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ���������','��� ������������� �������� ���������� �������',20);
  END IF;

  set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 21);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ������������� ��������','��� ������������� �����������������',21);
  END IF;

    set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 22);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ������ �������� ���������','��� ������������� �������� � ����',22);
  END IF;
  
      set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 23);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ��������� ������������� ��������','�������� ������� �������� � ����',23);
  END IF;

  set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 24);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ������','������ ������� � ���� ������',24);
  END IF;

  set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 25);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ��������','������ ������� � ���� ��������',25);
  END IF;

    set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 26);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������','��������� - � ���� ������� �������',26);
  END IF;

    set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 27);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ������� �������������','����������� ���� ���, ������ �������������',27);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 28);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����������� ������ ����� � �������','����������� ������ ����� � �������',28);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 29);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ����������������','���������������� ���������',29);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 30);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������������� (����������������)','���������������� �� ����. ���',30);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 31);
IF (ISNULL(@param))
          THEN
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � ��������','������� ������� � ���� ��������',31);
ELSE
UPDATE sensoralgorithms 
SET
  Name = '������� ������� � ��������1' -- Name - CHAR(45) NOT NULL
 ,Description = '������� ������� � ���� ��������1' -- Description - CHAR(45)
 ,AlgorithmID = 31 -- AlgorithmID - INT(11) NOT NULL
WHERE
  AlgorithmID = 31; -- ID - INT(11) NOT NULL
 END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 32);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � ��������2','������� ������� � ���� ��������2',32);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 33);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � ��������3','������� ������� � ���� ��������3',33);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 34);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����� ������� ������� � ���������','����� ������� ������� � ���������',34);
  END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 35);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ���������','����� � ������ ���������',35);
  ELSE
UPDATE sensoralgorithms
SET
  Name = '������ ���������' -- Name - CHAR(45) NOT NULL
 ,Description = '����� � ������ ���������' -- Description - CHAR(45)
 ,AlgorithmID = 35 -- AlgorithmID - INT(11) NOT NULL
WHERE
  AlgorithmID = 35; -- ID - INT(11) NOT NULL
 END IF;

set @param = (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 36);
IF (ISNULL(@param))
          then
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ��������','������ ��� �������� ��� � ���',36);
  END IF;

  END;

  CALL UpdateTableSensorAlg();
  DROP PROCEDURE IF EXISTS UpdateTableSensorAlg;

-- OK
