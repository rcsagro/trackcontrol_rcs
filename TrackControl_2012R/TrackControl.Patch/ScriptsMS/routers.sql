﻿IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_sample]') AND type in (N'U'))
BEGIN
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON

CREATE TABLE [dbo].[rt_sample](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Id_mobitel] [int] NULL,
	[TimeStart] [time](7) NULL,
	[Distance] [float] NULL,
	[Remark] [varchar](max) NULL,
	[Id_main] [int] NOT NULL,
	[IsGroupe] [smallint] NOT NULL,
	[AutoCreate] [smallint] NOT NULL,
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_rt_sample] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ('') FOR [Name]

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ((0)) FOR [Id_mobitel]

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ((0)) FOR [Distance]

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ((0)) FOR [Id_main]

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ((0)) FOR [IsGroupe]

ALTER TABLE [dbo].[rt_sample] ADD  DEFAULT ((0)) FOR [AutoCreate]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Транспортное средство (опц)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_mobitel'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Время начала маршрута (опц)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'TimeStart'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Общий путь, км' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Distance'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Примечание' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Remark'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ссылка на элемент верхнего уровня в иерархии' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_main'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Запись - группа' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'IsGroupe'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Маршрут может автосоздаваться ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'AutoCreate'
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route]') AND type in (N'U'))
BEGIN
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
CREATE TABLE [dbo].[rt_route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_sample] [int] NOT NULL,
	[Id_mobitel] [int] NOT NULL,
	[Id_driver] [int] NOT NULL,
	[TimeStartPlan] [datetime] NULL,
	[Distance] [float] NOT NULL,
	[TimePlanTotal] [varchar](20) NULL,
	[TimeFactTotal] [varchar](20) NULL,
	[Deviation] [varchar](20) NULL,
	[DeviationAr] [varchar](20) NULL,
	[Remark] [varchar](max) NULL,
	[FuelStart] [float] NOT NULL,
	[FuelAdd] [float] NOT NULL,
	[FuelSub] [float] NOT NULL,
	[FuelEnd] [float] NOT NULL,
	[FuelExpens] [float] NOT NULL,
	[FuelExpensAvg] [float] NOT NULL,
	[Fuel_ExpensMove] [float] NOT NULL,
	[Fuel_ExpensStop] [float] NOT NULL,
	[Fuel_ExpensTotal] [float] NOT NULL,
	[Fuel_ExpensAvg] [float] NOT NULL,
	[TimeMove] [varchar](20) NULL,
	[TimeStop] [varchar](20) NULL,
	[SpeedAvg] [float] NULL,
	[FuelAddQty] [int] NULL,
	[FuelSubQty] [int] NULL,
	[PointsValidity] [int] NULL,
	[PointsCalc] [int] NULL,
	[PointsFact] [int] NULL,
	[PointsIntervalMax] [varchar](20) NULL,
	[Type] [smallint] NOT NULL,
	[TimeEndFact] [datetime] NULL,
	[DistLogicSensor] [float] NULL,
	[IsClose] [bit] NOT NULL,
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_rt_route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

SET ANSI_PADDING OFF
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [Id_mobitel]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [Id_driver]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [Distance]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelStart]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelAdd]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelSub]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelEnd]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelExpens]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [FuelExpensAvg]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [Fuel_ExpensMove]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [Fuel_ExpensStop]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [Fuel_ExpensTotal]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [Fuel_ExpensAvg]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [SpeedAvg]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [FuelAddQty]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [FuelSubQty]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [PointsValidity]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [PointsCalc]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0)) FOR [PointsFact]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((1)) FOR [Type]
ALTER TABLE [dbo].[rt_route] ADD  DEFAULT ((0.00)) FOR [DistLogicSensor]
ALTER TABLE [dbo].[rt_route] ADD  CONSTRAINT [DF_rt_route_IsClose]  DEFAULT ((0)) FOR [IsClose]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ссылка на шаблон маршрута ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_sample'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Транспортное средство ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_mobitel'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Водитель' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_driver'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'План время начала маршрута' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStartPlan'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Общий путь, км' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Distance'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Общее время на маршруте (план)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimePlanTotal'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Общее время на маршруте (факт)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeFactTotal'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Отклонение от графика' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Deviation'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Отклонение по времени прибытия' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DeviationAr'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Примечание' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Remark'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Топливо в начале' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelStart'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Заправлено' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAdd'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Слито' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSub'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Топливо в конце' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelEnd'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Общий расход топлива' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpens'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Средний расход,  л/100 км' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpensAvg'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДРТ/CAN Расход топлива в движении, л' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensMove'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДРТ/CAN Расход топлива на стоянках, л' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensStop'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДРТ/CAN Общий расход, л' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensTotal'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДРТ/CAN Средний расход, л/100 км' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvg'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Время движения' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeMove'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Время остановок' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStop'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Средняя скорость' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'SpeedAvg'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Заправлено - кол-во' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAddQty'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ДУТ Слито - кол-во' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSubQty'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Валидность данных' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsValidity'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Количество точек (по Log_ID) расчетное' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsCalc'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Количество точек (по Log_ID) фактическое' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsFact'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Максимальный интервал между точками' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsIntervalMax'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Тип маршрутного листа - КЗ или населенные пункты' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Type'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Фактическое время окончания маршрута' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeEndFact'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Путь с включенным логическим датчиком, км' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DistLogicSensor'
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_shedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_shedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_shedule_dc] [int] NULL,
	[Id_zone_main] [int] NULL,
	[DateShedule] [smalldatetime] NOT NULL,
	[Remark] [nvarchar](127) NULL,
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_rt_shedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_shedulet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_shedulet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_zone_to] [int] NOT NULL,
	[Id_vehicle] [int] NULL,
	[TimePlanStart] [smalldatetime] NOT NULL,
	[TimePlanEnd] [smalldatetime] NULL,
	[TimeFactStart] [smalldatetime] NULL,
	[TimeFactEnd] [smalldatetime] NULL,
	[State] [nvarchar](127) NULL,
	[IsClose] [bit] NOT NULL,
	[Distance] [float] NOT NULL,
 CONSTRAINT [PK_rt_shedulet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[rt_shedulet] ADD  CONSTRAINT [DF_rt_shedulet_IsClose]  DEFAULT ((0)) FOR [IsClose]

ALTER TABLE [dbo].[rt_shedulet] ADD  CONSTRAINT [DF_rt_shedulet_Distance]  DEFAULT ((0)) FOR [Distance]

ALTER TABLE [dbo].[rt_shedulet]  WITH CHECK ADD  CONSTRAINT [FK_rt_shedulet_rt_shedule] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_shedule] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
ALTER TABLE [dbo].[rt_shedulet] CHECK CONSTRAINT [FK_rt_shedulet_rt_shedule]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_shedule_dc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_shedule_dc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_zone_main] [int] NOT NULL,
	[Remark] [nvarchar](127) NULL,
	[Name] [nvarchar](127) NOT NULL,
 CONSTRAINT [PK_rt_shedule_dc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_shedulet_dc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_shedulet_dc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_zone_to] [int] NOT NULL,
	[Distance] [float] NOT NULL,
	[TimePlanStart] [char](5) NOT NULL,
	[TimePlanEnd] [char](5) NULL,
 CONSTRAINT [PK_rt_shedulet_dc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[rt_shedulet_dc] ADD  CONSTRAINT [DF_rt_shedulet_dc_Distance]  DEFAULT ((0)) FOR [Distance]

ALTER TABLE [dbo].[rt_shedulet_dc]  WITH CHECK ADD  CONSTRAINT [FK_rt_shedulet_dc_rt_shedule_dc] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_shedule_dc] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE [dbo].[rt_shedulet_dc] CHECK CONSTRAINT [FK_rt_shedulet_dc_rt_shedule_dc]

END;

IF NOT EXISTS (SELECT *
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE
                TABLE_NAME = 'rt_route'
                AND COLUMN_NAME = 'OutLinkId')
ALTER TABLE rt_route ADD OutLinkId varchar(20) NULL;

IF NOT EXISTS (SELECT *
            FROM
                INFORMATION_SCHEMA.COLUMNS
            WHERE
                TABLE_NAME = 'rt_sample'
                AND COLUMN_NAME = 'OutLinkId')
ALTER TABLE rt_sample ADD OutLinkId varchar(20) NULL;