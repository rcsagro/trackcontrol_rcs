IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'agro_plan_downtimereason') BEGIN
  CREATE TABLE agro_plan_downtimereason(
    Id INT IDENTITY,
    ReasonName VARCHAR(255) NOT NULL,
    ReasonComment TEXT DEFAULT NULL,
    PRIMARY KEY (Id)
  )
END;

IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'agro_plan_state') BEGIN
  CREATE TABLE agro_plan_state(
    Id TINYINT IDENTITY,
    StatusName VARCHAR(50) NOT NULL,
    StatusComment TEXT DEFAULT NULL,
    PRIMARY KEY (Id),
    UNIQUE (Id)
  )
END;

IF NOT EXISTS (SELECT StatusName
               FROM
                 agro_plan_state
               WHERE
                 Id = 1)
  INSERT INTO agro_plan_state(StatusName)
  VALUES
    ('��������');

IF NOT EXISTS (SELECT StatusName
               FROM
                 agro_plan_state
               WHERE
                 Id = 2)
  INSERT INTO agro_plan_state(StatusName)
  VALUES
    ('������');

IF NOT EXISTS (SELECT StatusName
               FROM
                 agro_plan_state
               WHERE
                 Id = 3)
  INSERT INTO agro_plan_state(StatusName)
  VALUES
    ('�������');

IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'agro_plan') BEGIN
  CREATE TABLE agro_plan(
    Id INT IDENTITY,
    PlanDate DATETIME NOT NULL,
    Id_mobitel INT NOT NULL DEFAULT (0),
    SquarePlanned FLOAT NOT NULL DEFAULT (0.0001),
    FuelAtStart FLOAT NOT NULL DEFAULT (0.0001),
    FuelForWork FLOAT NOT NULL DEFAULT (0.0001),
    IdStatus TINYINT DEFAULT NULL,
    Id_order INT DEFAULT NULL,
    PlanComment TEXT DEFAULT NULL,
    UserCreated VARCHAR(127) DEFAULT NULL,
    IsDownTime BIT NOT NULL DEFAULT (0),
    IdDowntimeReason INT DEFAULT NULL,
    DowntimeStart DATE DEFAULT NULL,
    DowntimeEnd DATE DEFAULT NULL,
    PRIMARY KEY (Id),
    UNIQUE (Id),
    CONSTRAINT FK_agro_plan_agro_plan_downtimereasons_Id FOREIGN KEY (IdDowntimeReason) REFERENCES agro_plan_downtimereason (Id), --ON
    -- DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT FK_agro_plan_agro_plan_status_Id FOREIGN KEY (IdStatus) REFERENCES agro_plan_state (Id)
  ) --ON DELETE RESTRICT ON UPDATE RESTRICT);
END;

IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'agro_plant') begin
  CREATE TABLE agro_plant(
  Id INT IDENTITY,
  Id_main INT NOT NULL DEFAULT (0),
  Id_field INT DEFAULT (0),
  Id_driver INT DEFAULT (0),
  Id_work INT DEFAULT (0),
  Id_agregat INT DEFAULT (0),
  Remark TEXT DEFAULT NULL,
  TimeStart DATETIME NULL DEFAULT (NULL),
  PRIMARY KEY (Id),
  CONSTRAINT FK_agro_plant_agro_plan_Id FOREIGN KEY (Id_main) REFERENCES agro_plan (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
END;

IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'agro_work_types')
BEGIN
  CREATE TABLE agro_work_types(
    Id INT NOT NULL,
    Name VARCHAR(50) NOT NULL
  ) ON [PRIMARY]

  EXEC sp_addextendedproperty N'MS_Description', N'������ � ����, ������� ��� ����,������� �� ����', 'SCHEMA', N'dbo', 'TABLE', N'agro_work_types'
END;

IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'agro_workgroup')
BEGIN
  CREATE TABLE agro_workgroup(
    Id INT NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Comment TEXT NULL DEFAULT (NULL)
  ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

  EXEC sp_addextendedproperty N'MS_Description', N'��������', 'SCHEMA', N'dbo', 'TABLE', N'agro_workgroup', 'COLUMN', N'Name'
  EXEC sp_addextendedproperty N'MS_Description', N'�����������', 'SCHEMA', N'dbo', 'TABLE', N'agro_workgroup', 'COLUMN', N'Comment'
END;

IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'agro_work')
BEGIN
  CREATE TABLE agro_work(
    Id INT NOT NULL,
    Name VARCHAR(50) NOT NULL,
    SpeedBottom FLOAT NULL DEFAULT (0),
    SpeedTop FLOAT NULL DEFAULT (0),
    Comment TEXT NULL DEFAULT (NULL),
    TypeWork INT NOT NULL DEFAULT (1),
    Id_main INT NULL DEFAULT (NULL)
  )

  EXEC sp_addextendedproperty N'MS_Description', N'��������', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Name'
  EXEC sp_addextendedproperty N'MS_Description', N'������ ������ ��������', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'SpeedBottom'
  EXEC sp_addextendedproperty N'MS_Description', N'������� ������ ��������', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'SpeedTop'
  EXEC sp_addextendedproperty N'MS_Description', N'�����������', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Comment'
  EXEC sp_addextendedproperty N'MS_Description', N'������ � ����, ������� ��� ����,������� �� ����', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'TypeWork'
  EXEC sp_addextendedproperty N'MS_Description', N'������ ����� �����', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Id_main'
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_season]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_season](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateStart] [date] NOT NULL,
	[DateEnd] [date] NOT NULL,
	[Id_culture] [int] NOT NULL,
	[SquarePlan] [decimal](10, 2) NOT NULL,
	[Comment] [text] NULL,
 CONSTRAINT [PK_agro_season] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[agro_season] ADD  DEFAULT ((0)) FOR [SquarePlan]

ALTER TABLE [dbo].[agro_season]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_season_agro_culture_Id] FOREIGN KEY([Id_culture])
REFERENCES [dbo].[agro_culture] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE [dbo].[agro_season] CHECK CONSTRAINT [fgn_key_FK_agro_season_agro_culture_Id]
END


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldseason_tc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldseason_tc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdSeason] [int] NOT NULL,
	[IdField] [int] NOT NULL,
	[Comment] [varchar](max) NULL,
	[DateInit] [datetime] NOT NULL,
	[UserCreated] [varchar](127) NOT NULL,
	[DateStart] [datetime] NOT NULL,
	[DateEnd] [datetime] NOT NULL,
 CONSTRAINT [PK_agro_fieldseason_tc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[agro_fieldseason_tc] ADD  DEFAULT ('') FOR [UserCreated]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tc', @level2type=N'COLUMN',@level2name=N'Comment'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tc', @level2type=N'COLUMN',@level2name=N'DateInit'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� ��� ����� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tc', @level2type=N'COLUMN',@level2name=N'UserCreated'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ ����� (�� ��������� - ���� ������ ������)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tc', @level2type=N'COLUMN',@level2name=N'DateStart'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ ����� (�� ��������� - ���� ��������� ������)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tc', @level2type=N'COLUMN',@level2name=N'DateEnd'

END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldseason_tct]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldseason_tct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[IdWork] [int] NOT NULL,
	[DateStartPlan] [datetime] NOT NULL,
	[DateEndPlan] [datetime] NOT NULL,
	[WorkQtyPlan] [decimal](10, 2) NOT NULL,
	[DateStartFact] [datetime] NULL,
	[DateEndFact] [datetime] NULL,
	[SquareGa] [decimal](10, 3) NOT NULL,
	[SquarePercent] [decimal](5, 2) NOT NULL,
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_fieldseason_tct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[agro_fieldseason_tct] ADD  DEFAULT ((0)) FOR [WorkQtyPlan]

ALTER TABLE [dbo].[agro_fieldseason_tct] ADD  DEFAULT ((0)) FOR [SquareGa]

ALTER TABLE [dbo].[agro_fieldseason_tct] ADD  DEFAULT ((0)) FOR [SquarePercent]

ALTER TABLE [dbo].[agro_fieldseason_tct]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_fieldseason_t�t_agro_fieldseason_t�_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_fieldseason_tc] ([Id])

ALTER TABLE [dbo].[agro_fieldseason_tct] CHECK CONSTRAINT [fgn_key_FK_agro_fieldseason_t�t_agro_fieldseason_t�_Id]

ALTER TABLE [dbo].[agro_fieldseason_tct]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_fieldseason_t�t_agro_work_Id] FOREIGN KEY([IdWork])
REFERENCES [dbo].[agro_work] ([Id])

ALTER TABLE [dbo].[agro_fieldseason_tct] CHECK CONSTRAINT [fgn_key_FK_agro_fieldseason_t�t_agro_work_Id]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'IdWork'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'DateStartPlan'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'DateEndPlan'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ���-�� ��������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'WorkQtyPlan'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'DateStartFact'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'DateEndFact'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������������ �������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'SquareGa'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������������ �������, %' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'SquarePercent'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct', @level2type=N'COLUMN',@level2name=N'Comment'
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldseason_tct_fact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldseason_tct_fact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[IdOrder] [int] NOT NULL,
	[IdDriver] [int] NULL,
	[IdAgreg] [int] NULL,
	[SquareFactGa] [decimal](10, 3) NOT NULL,
	[JointProcess] [bit] NOT NULL,
	[SquareAfterRecalc] [decimal](10, 3) NOT NULL,
	[DateRecalc] [datetime] NULL,
	[UserRecalc] [varchar](127) NULL,
 CONSTRAINT [PK_agro_fieldseason_tct_fact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[agro_fieldseason_tct_fact] ADD  DEFAULT ((0)) FOR [SquareFactGa]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] ADD  DEFAULT ((0)) FOR [JointProcess]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] ADD  DEFAULT ((0)) FOR [SquareAfterRecalc]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] ADD  DEFAULT ('') FOR [UserRecalc]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_agro_agregat_Id] FOREIGN KEY([IdAgreg])
REFERENCES [dbo].[agro_agregat] ([Id])

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] CHECK CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_agro_agregat_Id]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_agro_fieldseason_tct_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_fieldseason_tct] ([Id])

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] CHECK CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_agro_fieldseason_tct_Id]

ALTER TABLE [dbo].[agro_fieldseason_tct_fact]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_driver_id] FOREIGN KEY([IdDriver])
REFERENCES [dbo].[driver] ([id])

ALTER TABLE [dbo].[agro_fieldseason_tct_fact] CHECK CONSTRAINT [fgn_key_FK_agro_fieldseason_tct_fact_driver_id]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'IdOrder'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������������ ������� (� ������ ����. ���������).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'SquareFactGa'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'JointProcess'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������������ ������� (� ������ ����. ���������).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'SquareAfterRecalc'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������� ���������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'DateRecalc'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� ���������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldseason_tct_fact', @level2type=N'COLUMN',@level2name=N'UserRecalc'
END;
;
