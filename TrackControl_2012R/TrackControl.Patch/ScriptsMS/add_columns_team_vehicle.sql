IF NOT EXISTS (SELECT *
           FROM
             INFORMATION_SCHEMA.COLUMNS
           WHERE
             TABLE_NAME = 'team'
             AND COLUMN_NAME = 'FuelWayKmGrp')
  ALTER TABLE team ADD FuelWayKmGrp FLOAT NULL DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'team'
                 AND COLUMN_NAME = 'FuelMotoHrGrp')
  ALTER TABLE team ADD FuelMotoHrGrp FLOAT NULL DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'FuelWayLiter')
  ALTER TABLE vehicle ADD FuelWayLiter FLOAT NULL DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'FuelMotorLiter')
  ALTER TABLE vehicle ADD FuelMotorLiter FLOAT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'ntf_events'
                 AND COLUMN_NAME = 'ParInt2')
  ALTER TABLE ntf_events ADD ParInt2 INT NULL DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_Id')
  ALTER TABLE vehicle ADD Category_Id INT NULL DEFAULT (NULL);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_id2')
  ALTER TABLE vehicle ADD Category_id2 INT NULL DEFAULT (NULL);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'InclinometerMaxAngleAxisX')
  ALTER TABLE setting ADD InclinometerMaxAngleAxisX FLOAT NOT NULL DEFAULT (5);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'InclinometerMaxAngleAxisY')
  ALTER TABLE setting ADD InclinometerMaxAngleAxisY FLOAT NOT NULL DEFAULT (5);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_work'
                 AND COLUMN_NAME = 'OutLinkId')
ALTER TABLE agro_work ADD OutLinkId VARCHAR(20) DEFAULT (NULL);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'mobitels'
                 AND COLUMN_NAME = 'Is64bitPackets')
ALTER TABLE mobitels ADD Is64bitPackets BIT DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'mobitels'
                 AND COLUMN_NAME = 'IsNotDrawDgps')
ALTER TABLE mobitels ADD IsNotDrawDgps BIT DEFAULT (1);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'speedMin')
ALTER TABLE setting ADD speedMin FLOAT NOT NULL DEFAULT (0);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'zones'
                 AND COLUMN_NAME = 'Category_id')
ALTER TABLE zones ADD Category_id INT NULL DEFAULT (NULL);

IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'zones'
                 AND COLUMN_NAME = 'Category_id2')
ALTER TABLE zones ADD Category_id2 INT NULL DEFAULT (NULL);





