CREATE FUNCTION [dbo].[get_hh_min]
(
  @minute FLOAT
)
RETURNS TIME
AS
BEGIN
	DECLARE @tm VARCHAR(5);
	DECLARE @times TIME;
  if (@minute<0)
  BEGIN
	SET @tm = '00:00';
  END
  ELSE
	BEGIN			
	  DECLARE @hours INT;
	  SET @hours = @minute / 60.0;
	  DECLARE @mn INT;
	  SET @mn = @minute - @hours * 60;
	  SET @tm = convert(VARCHAR(2), @hours) + ':' + convert(VARCHAR(2), @mn);
	END
	SET @times = convert(TIME, @tm);
	RETURN @times;
END;
