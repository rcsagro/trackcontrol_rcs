/****** Object:  UserDefinedFunction [dbo].[UNIX_TIMESTAMPS]    Script Date: 18.10.2013 8:39:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UNIX_TIMESTAMPS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UNIX_TIMESTAMPS]
--GO
/****** Object:  UserDefinedFunction [dbo].[From_UnixTime]    Script Date: 18.10.2013 8:39:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[From_UnixTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[From_UnixTime]
--GO
/****** Object:  UserDefinedFunction [dbo].[From_UnixTime]    Script Date: 18.10.2013 8:39:00 ******/
SET ANSI_NULLS ON
--GO
SET QUOTED_IDENTIFIER ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[From_UnixTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[From_UnixTime]
(
  @timestamp INTEGER
)
RETURNS DATETIME
AS
BEGIN
  /* Function body */
  DECLARE @return DATETIME
  DECLARE @GMT DATETIME;
  DECLARE @tm DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @tm = dateadd(SECOND, @timestamp, { D ''1970-01-01'' });
  SET @return = @tm + @GMT;
RETURN @return
END
' 
END

--GO
/****** Object:  UserDefinedFunction [dbo].[UNIX_TIMESTAMPS]    Script Date: 18.10.2013 8:39:00 ******/
SET ANSI_NULLS ON
--GO
SET QUOTED_IDENTIFIER ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UNIX_TIMESTAMPS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[UNIX_TIMESTAMPS]
(
  @ctimestamp DATETIME
)
RETURNS INTEGER
AS
BEGIN
  /* Function body */
  DECLARE @return INTEGER;
  DECLARE @GMT DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @ctimestamp = @ctimestamp - @GMT;
  SELECT @return = datediff(SECOND, { D ''1970-01-01'' }, @ctimestamp)
  RETURN @return
END

' 
END

--GO
/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig]    Script Date: 19.05.2014 15:47:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnListMobitelConfig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OnListMobitelConfig]
--GO
/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig]    Script Date: 19.05.2014 15:47:52 ******/
SET ANSI_NULLS ON
--GO
SET QUOTED_IDENTIFIER ON
--GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnListMobitelConfig]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnListMobitelConfig]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR LOCAL FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '''')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  IF EXISTS(SELECT * FROM sys.tables WHERE name = ''tmpMobitelsConfig'')
    DROP TABLE dbo.tmpMobitelsConfig;

CREATE TABLE dbo.tmpMobitelsConfig(
  MobitelID INT NOT NULL,
  DevIdShort CHAR(4) NOT NULL,
  LastPacketID BIGINT NOT NULL
);

OPEN CursorMobitels;
FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
WHILE @@fetch_status = 0
BEGIN
  INSERT INTO tmpMobitelsConfig
  SELECT @MobitelIDInCursor
       , @DevIdShortInCursor
       , (SELECT coalesce(max(SrvPacketID), 0)
          FROM
            datagps
          WHERE
            Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
END;
CLOSE CursorMobitels;
--DEALLOCATE CursorMobitels;

SELECT MobitelID
     , DevIdShort
     , LastPacketID
FROM
  tmpMobitelsConfig;

DROP TABLE dbo.tmpMobitelsConfig;
END

' 
END
--GO