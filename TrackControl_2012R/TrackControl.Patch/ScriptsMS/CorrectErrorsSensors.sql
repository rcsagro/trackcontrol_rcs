/****** Object:  View [dbo].[dataview1]    Script Date: 06.03.2014 17:46:41 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[dataview1]'))
DROP VIEW [dbo].[dataview1]
/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodAll]    Script Date: 06.03.2014 17:46:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectDataGpsPeriodAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SelectDataGpsPeriodAll]
/****** Object:  StoredProcedure [dbo].[online_table]    Script Date: 06.03.2014 17:46:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online_table]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[online_table]
/****** Object:  StoredProcedure [dbo].[hystoryOnline]    Script Date: 06.03.2014 17:46:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hystoryOnline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[hystoryOnline]
/****** Object:  StoredProcedure [dbo].[dataview]    Script Date: 06.03.2014 17:46:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dataview]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[dataview]
/****** Object:  StoredProcedure [dbo].[dataview]    Script Date: 06.03.2014 17:46:41 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dataview]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[dataview]
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END
' 
END
/****** Object:  StoredProcedure [dbo].[hystoryOnline]    Script Date: 06.03.2014 17:46:41 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hystoryOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[hystoryOnline]
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS time
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , DataGPS_ID AS ''DataGPS_ID''
  FROM
    online
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END
' 
END
/****** Object:  StoredProcedure [dbo].[online_table]    Script Date: 06.03.2014 17:46:41 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online_table]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[online_table]
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS ''DataGPS_ID''
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS ''time''
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936))
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 Events
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Events

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       online
     GROUP BY
       Mobitel_ID) T1;
END
' 
END
/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodAll]    Script Date: 06.03.2014 17:46:41 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectDataGpsPeriodAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SelectDataGpsPeriodAll]
(
  @mobile      INT,
  @beginperiod DATETIME,
  @endperiod   DATETIME,
  @timebegin   VARCHAR(9),
  @timeend     VARCHAR(9),
  @weekdays    VARCHAR(15),
  @daymonth    VARCHAR(90)
)
AS
BEGIN
  -- DECLARE @begin DATETIME;
  -- SET @begin = convert(DATETIME, ''24/03/2013 00:00:00'');
  -- DECLARE @end DATETIME;
  -- SET @end = convert(DATETIME, ''1/04/2013 23:59:59'');
  DECLARE @saTail VARCHAR(90);
  DECLARE @saHead VARCHAR(2);
  DECLARE @indx INT;
  DECLARE @time0 TIME;
  SET @time0 = ''23:59:59'';
  DECLARE @time1 TIME;
  SET @time1 = ''00:00:00'';

  IF object_id(''tempdb..#tempperiod4'') IS NOT NULL
    DROP TABLE #tempperiod4;

  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , LogID AS LogID
  INTO
    #tempperiod4
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod))
    AND (Valid = 1)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;

  --SELECT * FROM #tempperiod4;
  IF object_id(''tempdb..#tempweeks'') IS NOT NULL
    DROP TABLE #tempweeks;

  IF @weekdays != ''''
  BEGIN
    CREATE TABLE #tempweeks(
      shead VARCHAR(2)
    );

    SET @saTail = @weekdays;

    WHILE @saTail != ''''
    BEGIN
      SET @indx = charindex('';'', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempweeks(shead)
      VALUES
        (@saHead);
    END;
  END -- IF

  IF object_id(''tempdb..#tempday0'') IS NOT NULL
    DROP TABLE #tempday0;

  IF @daymonth != ''''
  BEGIN
    CREATE TABLE #tempday0(
      shead VARCHAR(2)
    );

    SET @saTail = @daymonth;

    WHILE @saTail != ''''
    BEGIN
      SET @indx = charindex('';'', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempday0(shead)
      VALUES
        (@saHead);
    END; -- while
  END; -- IF

  -- getting select data
  IF (@daymonth != '''') AND (@weekdays != '''')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '''') AND (@weekdays != '''')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth != '''') AND (@weekdays = '''')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '''') AND (@weekdays = '''')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;
END
' 
END
/****** Object:  View [dbo].[dataview1]    Script Date: 06.03.2014 17:46:41 ******/
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[dataview1]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[dataview1] 
AS SELECT datagps.DataGps_ID AS DataGps_ID
        , datagps.Mobitel_ID AS Mobitel_ID
        , (datagps.Latitude / 600000.00000) AS Lat
        , (datagps.Longitude / 600000.00000) AS Lon
        , datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
        , (datagps.Speed * 1.852) AS speed
        , ((datagps.Direction * 360) / 255) AS direction
        , datagps.Valid AS Valid
        , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
        , datagps.Events AS Events
        , datagps.LogID AS LogID
   FROM
     datagps
' 
