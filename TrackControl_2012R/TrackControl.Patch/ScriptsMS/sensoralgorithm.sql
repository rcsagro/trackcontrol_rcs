IF EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'sensoralgorithms') BEGIN
		DROP TABLE sensoralgorithms;
                 END;

IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'sensoralgorithms') BEGIN
CREATE TABLE dbo.sensoralgorithms(
  ID INT IDENTITY,
  Name CHAR(45) NOT NULL DEFAULT (''),
  Description CHAR(45) NULL,
  AlgorithmID INT NOT NULL DEFAULT (0),
  CONSTRAINT PK_sensoralgorithms PRIMARY KEY (ID)
  ) ON [PRIMARY]
END;


IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 1)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
    	('������ ������������','��� ������� ������������ �������� ���. ����.',1);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 2)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ��������� ���������','������ ������������ ��� ����������� � ���.',2);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 3)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������������','��� ������� ������������ �-�� ��������',3);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 4)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ����������','�������������� ���������� � ����',4);
ELSE
  UPDATE sensoralgorithms 
SET
  Name = '������ ����������' -- Name - CHAR(45) NOT NULL
 ,Description = '�������������� ���������� � ����' -- Description - CHAR(45)
 ,AlgorithmID = 4 -- AlgorithmID - INT(11) NOT NULL
WHERE
  AlgorithmID = 4;


IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 5)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�������� ��������� ���������','��� ������� ������������ �-�� ��������',5);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 6)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����������','��� ������������� ��������',6);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 7)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� �������','������� ������� � ����',7);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 8)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ �������','������ ������� ����� �������',8);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 9)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������1','��� �������� ���������� ����������1',9);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 10)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������2','��� �������� ���������� ����������2',10);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 11)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����1','��� ����������1',11);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 12)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�����2','��� ����������2',12);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 13)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ��������','��� ������������� ���������',13);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 14)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ���������� ����������','��� ������������� ��������� ���������',14);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 15)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����������������','��� ������ ���������',15);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 16)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('�������� - ���������� ������','��� ����������� � ������ ��������',16);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 17)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	 ('��������� ������������ - ���������� ������','��� �������� ��������� ������/�������',17);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 18)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	 ('����������� - ���������� ��� X,','��� �������� ���� ������� �� ��� �',18);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 19)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����������� - ���������� ��� Y,','��� �������� ���� ������� �� ��� Y',19);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 20)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ���������','��� ������������� �������� ���������� �������',20);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 Id = 21)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������������� ������������� ��������','��� ������������� �����������������',21);

	IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 22)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ������ �������� ���������','��� ������������� ��������� �������� � AGRO',22);

		IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 23)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ��������� ������������� ��������','�������� ������� �������� � AGRO',23);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 24)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ������','������ ������� � ���� ������',24);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 25)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ��������','������ ������� � ���� ��������',25);
IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 26)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������','���������',26);
IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 27)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ������� �������������','���������� ���� ���, ������ �������������',27);


IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 28)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����������� ������ ����� � �������','�������� ����������� ������ ����� � �������',28);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 29)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ������� ����������������','���������������� ���������',29);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 30)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������������� (����������������)','���������������� �� ���������������� ���',30);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 31) 
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � �������� 1','������� ������� � ���� �������� 1',31);
ELSE
  UPDATE sensoralgorithms 
SET
  Name = '������� ������� � �������� 1' -- Name - CHAR(45) NOT NULL
 ,Description = '������� ������� � ���� �������� 1' -- Description - CHAR(45)
 ,AlgorithmID = 31 -- AlgorithmID - INT(11) NOT NULL
WHERE
  AlgorithmID = 31;

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 32)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � �������� 2','������� ������� � ���� �������� 2',32);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 33)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������� ������� � �������� 3','������� ������� � ���� �������� 3',33);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 34)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('����� ������� ������� � ���������','����� ������� ������� � ���������',34);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 35)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('������ ��������� ������������� ��������','����� � ������ ��������� ������������� ������',35);

IF NOT EXISTS (SELECT AlgorithmID
               FROM
                 sensoralgorithms
               WHERE
                 AlgorithmID = 36)
  INSERT INTO sensoralgorithms(Name, Description, AlgorithmID)
  VALUES
  	('���������� ��������','������ ��� �������� ��� � ���',36);

-- OK