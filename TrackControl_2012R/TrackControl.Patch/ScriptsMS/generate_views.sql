IF OBJECT_ID ('datavalue1', 'V') IS NOT NULL
DROP VIEW datavalue1;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.datavalue1
AS SELECT round((datagps.Longitude / 600000), 6) AS lon
        , round((datagps.Latitude / 600000), 6) AS lat
        , datagps.Mobitel_ID AS Mobitel_ID
        , sensordata.value AS Value
        , (datagps.Speed * 1.852) AS speed
        , dbo.from_unixtime(datagps.UnixTime) AS time
        , sensordata.sensor_id AS sensor_id
        , datagps.DataGps_ID AS datavalue_id
   FROM
     (sensordata
     JOIN datagps
       ON ((sensordata.datagps_id = datagps.DataGps_ID)))
   WHERE
     (datagps.Valid = 1)
GO

IF OBJECT_ID ('dataview1', 'V') IS NOT NULL
DROP VIEW dataview1;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.dataview1
AS SELECT datagps.DataGps_ID AS DataGps_ID
        , datagps.Mobitel_ID AS Mobitel_ID
        , (datagps.Latitude / 600000.00000) AS Lat
        , (datagps.Longitude / 600000.00000) AS Lon
        , datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
        , (datagps.Speed * 1.852) AS speed
        , ((datagps.Direction * 360) / 255) AS direction
        , datagps.Valid AS Valid
        , (((((((datagps.Sensor8 + (datagps.Sensor7 * 2 ^ 8)) + (datagps.Sensor6 * 2 ^ 16)) + (datagps.Sensor5 * 2 ^ 24)) + (datagps.Sensor4 * 2 ^ 32)) + (datagps.Sensor3 * 2 ^ 40)) + (datagps.Sensor2 * 2 ^ 48)) + (datagps.Sensor1 * 2 ^ 56)) AS sensor
        , datagps.Events AS Events
        , datagps.LogID AS LogID
   FROM
     datagps
GO

IF OBJECT_ID ('internalmobitelconfigview', 'V') IS NOT NULL
DROP VIEW internalmobitelconfigview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.internalmobitelconfigview
AS (SELECT c.ID AS ID
         , max(c.InternalMobitelConfig_ID) AS LastRecord
   FROM
     internalmobitelconfig c
   WHERE
     (c.devIdShort <> '')
   GROUP BY
     c.ID)
GO

IF OBJECT_ID ('deviceview', 'V') IS NOT NULL
DROP VIEW deviceview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.deviceview
AS (SELECT m.Mobitel_ID AS Mobitel_ID
         , m.Name AS Name
         , m.Descr AS Descr
         , c.devIdShort AS devIdShort
         , m.DeviceType AS DeviceType
         , m.ConfirmedID AS ConfirmedID
         , c.moduleIdRf AS moduleIdRf
   FROM
     ((mobitels m
     JOIN internalmobitelconfigview v
       ON v.ID = m.InternalMobitelConfig_ID)
     JOIN internalmobitelconfig c
       ON v.LastRecord = c.InternalMobitelConfig_ID)
   --WHERE
   --  ((v.ID = m.InternalMobitelConfig_ID)
   --  AND (v.LastRecord = c.InternalMobitelConfig_ID))
   )
GO