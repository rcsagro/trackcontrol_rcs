﻿IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_work'
                 AND COLUMN_NAME = 'TrackColor')
  ALTER TABLE agro_work ADD TrackColor int NOT NULL DEFAULT 0 ;


  	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat_vehicle'
                 AND COLUMN_NAME = 'Id_state')
  ALTER TABLE agro_agregat_vehicle ADD Id_state bit NOT NULL DEFAULT 0 ;


IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'ntf_agro_works')
BEGIN
  CREATE TABLE ntf_agro_works(
    Id INT IDENTITY,
    Id_main INT NOT NULL,
	WorkId INT NOT NULL,
    CONSTRAINT PK_ntf_agro_works PRIMARY KEY (Id)
  ) ON [PRIMARY]

 ALTER TABLE [dbo].[ntf_agro_works]  WITH NOCHECK ADD  CONSTRAINT [FK_ntf_agro_works_ntf_main_Id] FOREIGN KEY([WorkId])
REFERENCES [dbo].[agro_work] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
END;

  	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'IsUseAgregatState')
  ALTER TABLE agro_agregat ADD IsUseAgregatState bit NOT NULL DEFAULT 0 ;

  	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'LogicState')
  ALTER TABLE agro_agregat ADD LogicState bit NOT NULL DEFAULT 0 ;

      IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'MinStateValue')
  ALTER TABLE agro_agregat ADD MinStateValue decimal NOT NULL DEFAULT 0 ;

        IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'MaxStateValue')
  ALTER TABLE agro_agregat ADD MaxStateValue decimal NOT NULL DEFAULT 0 ;

          IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'SensorAlgorithm')
  ALTER TABLE agro_agregat ADD SensorAlgorithm int NOT NULL DEFAULT 0 ;


          IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_ordert'
                 AND COLUMN_NAME = 'Id_event_manually')
  ALTER TABLE agro_ordert ADD Id_event_manually int  ;

  IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'agro_event_manually')
	BEGIN
	  CREATE TABLE agro_event_manually(
		Id INT IDENTITY,
		Id_mobitel INT NOT NULL,
		EventName varchar(127),
		TimeStart smalldatetime,
		TimeEnd smalldatetime,
		Id_work INT,
		Info_1 varchar(127),
		Info_2 varchar(127),
		Info_3 varchar(127),
		Info_4 varchar(127),
		Info_5 varchar(127),
		CONSTRAINT PK_agro_event_manually PRIMARY KEY (Id)
	  ) ON [PRIMARY]

	END;

	IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'LogicStateBounce')
	ALTER TABLE agro_agregat ADD LogicStateBounce INT NOT NULL DEFAULT 0 ;

	IF NOT EXISTS (SELECT TABLE_NAME
           FROM
             information_schema.tables
           WHERE
             TABLE_NAME = 'agro_sensorkoefficients') 
	BEGIN
	CREATE TABLE [dbo].[agro_sensorkoefficients](
		[ID] [bigint] IDENTITY,
		[AgregatID] [bigint] NOT NULL,
		[UserValue] [float] NOT NULL,
		[SensorValue] [float] NOT NULL,
		[K] [float] NOT NULL,
		[b] [float] NOT NULL,
		CONSTRAINT PK_agro_sensorkoefficients PRIMARY KEY (ID)
	) ON [PRIMARY]
	END;

	IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'K')
	ALTER TABLE agro_agregat ADD K decimal(10, 5) NOT NULL DEFAULT 1.00000 ;

	IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'B')
	ALTER TABLE agro_agregat ADD B decimal DEFAULT 0 ;

		IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_agregat'
                 AND COLUMN_NAME = 'S')
	ALTER TABLE agro_agregat ADD S bit DEFAULT 0 ;

	          IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_ordert'
                 AND COLUMN_NAME = 'FactSquareJointProcessing')
  ALTER TABLE agro_ordert ADD FactSquareJointProcessing float DEFAULT 0   ;

    	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_workgroup'
                 AND COLUMN_NAME = 'IsSpeedControl')
  ALTER TABLE agro_workgroup ADD IsSpeedControl bit NOT NULL DEFAULT 0 ;

      	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_workgroup'
                 AND COLUMN_NAME = 'IsDepthControl')
  ALTER TABLE agro_workgroup ADD IsDepthControl bit NOT NULL DEFAULT 0 ;

        	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_fieldseason_tct'
                 AND COLUMN_NAME = 'IsClosed')
  ALTER TABLE agro_fieldseason_tct ADD IsClosed bit NOT NULL DEFAULT 0 ;

          	  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_season'
                 AND COLUMN_NAME = 'IsClosed')
  ALTER TABLE agro_season ADD IsClosed bit NOT NULL DEFAULT 0 ;




