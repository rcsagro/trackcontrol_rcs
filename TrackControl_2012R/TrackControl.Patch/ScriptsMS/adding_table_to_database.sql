IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'vehicle_category')
BEGIN
  CREATE TABLE vehicle_category(
    Id INT NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Tonnage DECIMAL(10, 3) NOT NULL DEFAULT (0.000),
    FuelNormMoving FLOAT NOT NULL DEFAULT (0),
    FuelNormParking FLOAT NOT NULL DEFAULT (0)
  ) ON [PRIMARY]

  EXEC sp_addextendedproperty N'MS_Description', N'��������� ������������� ��������', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category', 'COLUMN', N'Name'
  EXEC sp_addextendedproperty N'MS_Description', N'����������������,  ����', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category', 'COLUMN', N'Tonnage'
  EXEC sp_addextendedproperty N'MS_Description', N'����� ������� ������� � �������� �/100 ��', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category', 'COLUMN', N'FuelNormMoving'
  EXEC sp_addextendedproperty N'MS_Description', N'����� ������� ������� �� �������� �������� �/���', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category', 'COLUMN', N'FuelNormParking'
END;

IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'ntf_popupusers')
BEGIN
  CREATE TABLE ntf_popupusers(
    Id INT NOT NULL,
    Id_main INT NOT NULL,
    Id_user INT NOT NULL
  ) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT TABLE_NAME
               FROM
                 information_schema.tables
               WHERE
                 TABLE_NAME = 'vehicle_category2')
BEGIN
  CREATE TABLE vehicle_category2(
    Id INT IDENTITY,
    Name VARCHAR(255) NOT NULL,
    CONSTRAINT PK_vehicle_category2 PRIMARY KEY (Id)
  ) ON [PRIMARY]
EXEC sp_addextendedproperty N'MS_Description', N'���������2 ������������� ��������', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category2', 'COLUMN', N'Name'
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pcbdata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[pcbdata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Typedata] [varchar](255) NOT NULL,
	[koeffK] [float] NOT NULL,
	[Startbit] [int] NOT NULL,
	[Lengthbit] [int] NOT NULL,
	[Comment] [varchar](255) NULL,
	[Idpcb] [int] NOT NULL,
 CONSTRAINT [PK_pcbdata] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pcbversion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[pcbversion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Comment] [varchar](255) NULL,
 CONSTRAINT [PK_table1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category3]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_category3](
	Id INT IDENTITY,
  Name VARCHAR(255) NOT NULL,
  CONSTRAINT PK_vehicle_category3 PRIMARY KEY (Id) 
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zone_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zone_category](
	Id INT IDENTITY,
  Name VARCHAR(255) NOT NULL,
  CONSTRAINT PK_zone_category PRIMARY KEY (Id) 
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category4]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_category4](
	Id INT IDENTITY,
  Name VARCHAR(255) NOT NULL,
  CONSTRAINT PK_vehicle_category4 PRIMARY KEY (Id) 
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zone_category2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zone_category2](
	Id INT IDENTITY,
  Name VARCHAR(255) NOT NULL,
  CONSTRAINT PK_zone_category2 PRIMARY KEY (Id) 
) ON [PRIMARY]
END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_commentary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_commentary](
	Id INT IDENTITY,
  [Comment] VARCHAR(255) NOT NULL,
  Mobitel_id INT NOT NULL,
  CONSTRAINT PK_vehicle_commentary PRIMARY KEY (Id) 
) ON [PRIMARY]
END;


-- IF EXISTS (SELECT TABLE_NAME
--           FROM
--             information_schema.tables
--           WHERE
--             TABLE_NAME = 'vehicle_category')
-- BEGIN
--  IF object_id('tempdb..#tempcategory') IS NOT NULL
--    DROP TABLE #tempcategory;

--  SELECT *
--  INTO
--    #tempcategory
--  FROM
--    vehicle_category;

--  DROP TABLE vehicle_category;

--  CREATE TABLE vehicle_category(
--    Id INT IDENTITY,
--    Name VARCHAR(50) NOT NULL,
--    Tonnage DECIMAL(10, 3) NOT NULL DEFAULT (0.000),
--    FuelNormMoving FLOAT NOT NULL DEFAULT 0,
--    FuelNormParking FLOAT NOT NULL DEFAULT 0
--  )

--  INSERT INTO dbo.vehicle_category(Name
--                                 , Tonnage
--                                 , FuelNormMoving
--                                 , FuelNormParking)
--  SELECT tm.Name, tm.Tonnage, tm.FuelNormMoving, tm.FuelNormParking
--  FROM
--    #tempcategory tm;

--  DROP TABLE #tempcategory;
-- END
-- ELSE
-- BEGIN
--  CREATE TABLE vehicle_category(
--    Id INT IDENTITY,
--    Name VARCHAR(50) NOT NULL,
--    Tonnage DECIMAL(10, 3) NOT NULL DEFAULT (0.000),
--    FuelNormMoving FLOAT NOT NULL DEFAULT 0,
--   FuelNormParking FLOAT NOT NULL DEFAULT 0
--  )
-- END;
