IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Identifier')
  ALTER TABLE vehicle ADD Identifier INT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'PcbVersionId')
  ALTER TABLE vehicle ADD PcbVersionId INT NULL DEFAULT (-1);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'sensors'
                 AND COLUMN_NAME = 'B')
  ALTER TABLE sensors ADD B FLOAT  NOT NULL DEFAULT (0);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'sensors'
                 AND COLUMN_NAME = 'S')
  ALTER TABLE sensors ADD S  TINYINT  NOT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_plant'
                 AND COLUMN_NAME = 'TimeStart')
  ALTER TABLE agro_plant ADD TimeStart DATETIME NULL DEFAULT (NULL);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_field'
                 AND COLUMN_NAME = 'IsOutOfDate')
  ALTER TABLE agro_field ADD IsOutOfDate BIT NOT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbversion'
                 AND COLUMN_NAME = 'MobitelId')
  ALTER TABLE pcbversion ADD MobitelId INT NULL DEFAULT (-1);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'NameVal')
  ALTER TABLE pcbdata ADD NameVal VARCHAR(32) NOT NULL DEFAULT (' ');
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'NumAlg')
  ALTER TABLE pcbdata ADD NumAlg INT NOT NULL DEFAULT (0);
  
  
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'PcbVersionId')
  ALTER TABLE vehicle ADD PcbVersionId INT NULL DEFAULT (-1);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_id3')
  ALTER TABLE vehicle ADD Category_id3 INT NULL DEFAULT (NULL);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_id4')
  ALTER TABLE vehicle ADD Category_id4 INT NULL DEFAULT (NULL);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'TimeMinimMovingSize')
  ALTER TABLE setting ADD TimeMinimMovingSize TIME NOT NULL DEFAULT ('00:00:00');
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'MinTimeSwitch')
  ALTER TABLE setting ADD MinTimeSwitch INT NOT NULL DEFAULT (0);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'driver'
                 AND COLUMN_NAME = 'numTelephone')
  ALTER TABLE driver ADD numTelephone VARCHAR(32) NOT NULL DEFAULT '';
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'S')
  ALTER TABLE pcbdata ADD S tinyint NOT NULL DEFAULT (0);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'Shifter')
  ALTER TABLE pcbdata ADD Shifter float NOT NULL DEFAULT (0);

  IF NOT EXISTS	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'driver' AND
			      COLUMN_NAME = 'Department')
  ALTER TABLE driver ADD Department varchar(40) DEFAULT NULL;

    IF NOT EXISTS	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'ntf_main' AND
			      COLUMN_NAME = 'Period')
  ALTER TABLE ntf_main ADD Period INT NOT NULL DEFAULT 0;

IF NOT EXISTS	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'datagpslost_on' AND
			      COLUMN_NAME = 'ID')
ALTER TABLE dbo.datagpslost_on ADD ID bigint NOT NULL UNIQUE IDENTITY(1,1);


/****** Object:  StoredProcedure [dbo].[OnTransferBuffer]    Script Date: 08.12.2017 14:58:58 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnTransferBuffer]') AND type in (N'P', N'PC'))
BEGIN
DROP PROCEDURE [dbo].[OnTransferBuffer]
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnTransferBuffer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnTransferBuffer]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */
  DECLARE @StartUnixTime INT
  SET @StartUnixTime = 0
  DECLARE @StartDate datetime, @EndDate datetime

  SET @StartDate = getdate()

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. 6 ms */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID 16 ms */
  DELETE o
  FROM
    [online] o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0) -- 20 ms
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT TOP 1 coalesce(max(UnixTime), 0)
                           FROM
                             [online] WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO [dbo].[online](Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , [Events]
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , [Events]
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      [online]
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT TOP 1 coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */
  /* 57 ms */
  declare @cou int = 0
  Select @cou = count(*)
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);
 if (@cou > 0)  
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, [Events] = [b].[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /* < 1 ms */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* < 1 ms */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on GROUP BY Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID;

  /* ----- < 1 ms -------------- FETCH Mobitels (INSERT SET) ----------------------- */
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    
    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /*IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
  END;*/

  SET @EndDate = getdate()

  SELECT datediff(ms,@StartDate, @EndDate)
 
END;'
END





