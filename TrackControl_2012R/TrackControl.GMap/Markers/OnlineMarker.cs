using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Markers
{
    public class OnlineMarker : GMapMarker
    {
        private string _label = ""; // ������� � �������
        private Image _icon; // ����������� ������� ��� ������ �����
        public bool IsActive; // �������, �������� �� ������ ����������
        public int LogicSensorState; // ��������� ����������� ������� - (��� ������� - ��������� �����)

        /// <summary>
        /// ID ������������� ��������, ������� ������������ ������ ������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        private int _id;

        /// <summary>
        /// �������������� ����� ��������� ������ TrackControl.Map.Markers.OnlineMarker
        /// </summary>
        /// <param name="id">ID ������������� ��������</param>
        /// <param name="point">�������������� ���������� �������</param>
        /// <param name="label">������� � �������</param>
        /// <param name="icon">����������� �������</param>
        protected OnlineMarker(int id, PointLatLng point, string label, Image icon)
            : base(point)
        {
            try
            {
                _id = id;
                _label = label;
                _icon = icon;

                Size = new GSize(22, 20);
                ToolTipOffset = new GPoint(25, -32);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Marker", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ���������������� ����� ��� ��������� �������.
        /// </summary>
        /// <param name="g"></param>
        public override void OnRender(Graphics g)
        {
            try
            {
                if (IsActive)
                {
                    g.DrawImageUnscaled(Shared.GActive, LocalPosition.X - 3, LocalPosition.Y - 60);
                }
                if (LogicSensorState == (short) LogicSensorStates.Off)
                {
                    g.DrawImageUnscaled(Shared.GStrokeGreen, LocalPosition.X - 3, LocalPosition.Y - 35);
                }
                else
                {
                    if (LogicSensorState == (short) LogicSensorStates.On)
                    {
                        g.DrawImageUnscaled(Shared.GStrokeRed, LocalPosition.X - 3, LocalPosition.Y - 35);
                    }
                }
                g.DrawImageUnscaled(_icon, LocalPosition.X, LocalPosition.Y - 32);

                if (_label.Length > 0)
                {
                    int x = LocalPosition.X + 10;
                    int y = LocalPosition.Y - 10;

                    if (IsActive)
                        DrawLabel(g, _label, Brushes.LightPink, Brushes.Black, x, y);
                    else
                        DrawLabel(g, _label, Brushes.White, Brushes.Navy, x, y);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Online Marker", MessageBoxButtons.OK);
                return;
            }
        }

        /// <summary>
        /// �������������� �������, ������� ��������� �� ��������� ���������
        /// ����. ������������ ��� ��������� �������.
        /// </summary>
        protected internal override GRectangle LocalArea
        {
            get
            {
                GRectangle ret = area;
                ret.Offset(1, -32);
                return ret;
            }
        }
    }
}
