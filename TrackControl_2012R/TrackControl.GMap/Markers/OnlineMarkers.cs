using DevExpress.XtraPrinting.Native;
using TrackControl.General;
using System.Drawing;

namespace TrackControl.GMap.Markers
{
    // ������� ������ ������� ��������
    public static class LShared
    {
        const int NUMBER_MARKER = 7; // ����� ��������
        private static Image[] imgMarkers = new Image[NUMBER_MARKER];
        
        public static void InitListMarkers()
        {
            imgMarkers[0] = Shared.GPark;
            imgMarkers[1] = Shared.GParkLong;
            imgMarkers[2] = Shared.GMove;
            imgMarkers[3] = Shared.GStop;
            imgMarkers[4] = Shared.GWarning;
            imgMarkers[5] = Shared.DriverRFID;
            imgMarkers[6] = Shared.DistancePoint;
        }

        public static Image GPark
        {
            get
            {
                return imgMarkers[0];
            }
        }

        public static Image GParkLong
        {
            get
            {
                return imgMarkers[1];
            }
        }

        public static Image GMove
        {
            get
            {
                return imgMarkers[2];
            }
        }

        public static Image GStop
        {
            get
            {
                return imgMarkers[3];
            }
        }

        public static Image GWarning
        {
            get
            {
                return imgMarkers[4];
            }
        }

        public static Image DriverRFID
        {
            get
            {
                return imgMarkers[5];
            }
        }

        public static Image DistancePoint
        {
            get
            {
                return imgMarkers[6];
            }
        }
    }
    /// <summary>
    /// ������, ������������ �������������� ������� ������������� ��������
    /// </summary>
    public class ParkMarker : OnlineMarker
    {
        public ParkMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.GPark)
        {
        }
    }

    /// <summary>
    /// ������, ������������ �������������� ������� ���� ����.������������ �����
    /// </summary>
    public class ParkLongMarker : OnlineMarker
    {
        public ParkLongMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.GParkLong)
        {
        }
    }

    /// <summary>
    /// ������, ������������ �������������� ����������� � ���������� ���������
    /// ������������� ��������
    /// </summary>
    public class MoveMarker : OnlineMarker
    {
        public MoveMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.GMove)
        {
        }
    }

    /// <summary>
    /// ������ ��� ����������� ����� ��������� ������������� ��������.
    /// </summary>
    public class StopMarker : OnlineMarker
    {
        public StopMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.GStop)
        {
        }
    }

    /// <summary>
    /// ������ ��� ����������� �������������� ������������� ��������, �������
    /// ������� �������� ����������.
    /// </summary>
    public class WarningMarker : OnlineMarker
    {
        public WarningMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.GWarning)
        {
        }
    }

    /// <summary>
    /// ������ ��� ����� �������� RFID ��������������
    /// </summary>
    public class RFIDMarker : OnlineMarker
    {
        public RFIDMarker(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.DriverRFID)
        {
        }
    }

    /// <summary>
    /// ������ ������� ����� ����������� �������� ����������
    /// </summary>
    public class DistancePoint : OnlineMarker
    {
        public DistancePoint(int id, PointLatLng point, string label)
            : base(id, point, label, LShared.DistancePoint)
        {
        }
    }

    /// <summary>
    /// �������� - ���������� ������ - ��������
    /// </summary>
    public class MdExecGo : OnlineMarker
    {
        public MdExecGo(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdExecGo)
        {
        }
    }

    /// <summary>
    /// �������� - ���������� ������ - ���������
    /// </summary>
    public class MdExecStop : OnlineMarker
    {
        public MdExecStop(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdExecStop)
        {
        }
    }

    /// <summary>
    /// �������� - ��������  ������ - ��������
    /// </summary>
    public class MdWaitGo : OnlineMarker
    {
        public MdWaitGo(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdWaitGo)
        {
        }
    }

    /// <summary>
    /// �������� - �������� ������ - ���������
    /// </summary>
    public class MdWaitStop : OnlineMarker
    {
        public MdWaitStop(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdWaitStop)
        {
        }
    }

    /// <summary>
    /// �������� - ������ - ��������
    /// </summary>
    public class MdRepareGo : OnlineMarker
    {
        public MdRepareGo(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdRepareGo)
        {
        }
    }

    /// <summary>
    /// �������� - ������ - ���������
    /// </summary>
    public class MdRepareStop : OnlineMarker
    {
        public MdRepareStop(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdRepareStop)
        {
        }
    }

    /// <summary>
    /// �������� - ���� - ��������
    /// </summary>
    public class MdDinnerGo : OnlineMarker
    {
        public MdDinnerGo(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdDinnerGo)
        {
        }
    }

    /// <summary>
    /// �������� - ���� - ���������
    /// </summary>
    public class MdDinnerStop : OnlineMarker
    {
        public MdDinnerStop(int id, PointLatLng point, string label)
            : base(id, point, label, Shared.MdDinnerStop)
        {
        }
    }
}
