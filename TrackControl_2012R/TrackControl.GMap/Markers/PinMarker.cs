﻿using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Markers
{
    /// <summary>
    /// Маркер для трека.
    /// </summary>
    public class PinMarker : GMapMarker
    {
        string _label = ""; // Подпись к маркеру
        PinMarkerTypes _type;//тип изображения
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="point">Точка на карте, где будет располагаться маркер</param>
        /// <param name="label">Подпись к маркеру.</param>
        public PinMarker(PinMarkerTypes type, PointLatLng point, string label)
            : base(point)
        {
            _label = label;
            _type = type;
            Size = new GSize(26, 31);
        }

        /// <summary>
        /// Переопределенный метод для отрисовки маркера.
        /// </summary>
        /// <param name="g"></param>
        public override void OnRender(Graphics g)
        {
            Image image;
            switch (_type)
            {
                case PinMarkerTypes.Pin:
                    {
                        image=Shared.Pin;
                        break;
                    }
                case PinMarkerTypes.Finish:
                    {
                        image = Shared.Finish;
                        break;
                    }
                case PinMarkerTypes.BallGreen:
                    {
                        image = Shared.BallGreen;
                        break;
                    }
                default:
                    {
                        image = Shared.Pin;
                        break;
                    }
            }
            g.DrawImageUnscaled(image, LocalPosition.X - 12, LocalPosition.Y - 27);
            if (_label.Length > 0)
                DrawLabel(g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6);
        }

    }
}
