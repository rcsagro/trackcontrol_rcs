using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Markers
{
  /// <summary>
  /// ������ ��� �����.
  /// </summary>
  public class CarMarker : GMapMarker
  {
    string _label = ""; // ������� � �������
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
    /// <param name="label">������� � �������.</param>
    public CarMarker(PointLatLng point, string label)
      : base(point)
    {
      _label = label;
      Size = new GSize(26, 31);
    }

    /// <summary>
    /// ���������������� ����� ��� ��������� �������.
    /// </summary>
    /// <param name="g"></param>
    public override void OnRender(Graphics g)
    {
      g.DrawImageUnscaled(Shared.ForTrack, LocalPosition.X - 13, LocalPosition.Y - 33);
      if (_label.Length > 0)
        DrawLabel(g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6);
    }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 0 ����
  /// </summary>
  public class ShowPosition0 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition0( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine0, LocalPosition.X - 26 / 2 + 3, LocalPosition.Y - 30 / 2 + 16 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 45 ����
  /// </summary>
  public class ShowPosition45 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition45( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine45, LocalPosition.X - 26 / 2 - 10, LocalPosition.Y - 30 / 2 + 5 + 3 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 90 ����
  /// </summary>
  public class ShowPosition90 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition90( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine90, LocalPosition.X - 26 / 2 - 10, LocalPosition.Y - 30 / 2 + 5 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 135 ����
  /// </summary>
  public class ShowPosition135 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition135( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine135, LocalPosition.X - 26 / 2 - 9 - 2, LocalPosition.Y - 30 / 2 + 5 - 7 - 6 - 1 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 180 ����
  /// </summary>
  public class ShowPosition180 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition180( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine180, LocalPosition.X - 26 / 2 + 1, LocalPosition.Y - 30 / 2 + 5 - 8 - 4 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 225 ����
  /// </summary>
  public class ShowPosition225 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition225( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine225, LocalPosition.X - 26 / 2 + 5, LocalPosition.Y - 30 / 2 + 5 - 10 - 4 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 270 ����
  /// </summary>
  public class ShowPosition270 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition270( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine270, LocalPosition.X - 26 / 2 + 13, LocalPosition.Y - 30 / 2 + 5 - 2 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ������������ ����� 315 ����
  /// </summary>
  public class ShowPosition315 : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowPosition315( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ArrowLine315, LocalPosition.X - 26 / 2 + 10 - 4, LocalPosition.Y - 30 / 2 + 10 - 3 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6 );
      }
  }

  /// <summary>
  /// ������ ��� ����������� �������
  /// </summary>
  public class ShowStop : GMapMarker
  {
      string _label = ""; // ������� � �������
      /// <summary>
      /// �����������.
      /// </summary>
      /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
      /// <param name="label">������� � �������.</param>
      public ShowStop( PointLatLng point, string label )
          : base( point )
      {
          _label = label;
          Size = new GSize( 24, 24 );
      }

      /// <summary>
      /// ���������������� ����� ��� ��������� �������.
      /// </summary>
      /// <param name="g"></param>
      public override void OnRender( Graphics g )
      {
          g.DrawImageUnscaled( Shared.ShowStop, LocalPosition.X - 24 / 2, LocalPosition.Y - 24 / 2 );
          if ( _label.Length > 0 )
              DrawLabel( g, _label, Brushes.White, Brushes.Navy, LocalPosition.X - 25, LocalPosition.Y + 24/2 - 2 );
      }
  }
}
