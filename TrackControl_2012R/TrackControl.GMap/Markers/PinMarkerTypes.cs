﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.GMap.Markers
{
    public enum PinMarkerTypes
    {
        Pin,
        Finish,
        BallGreen
    }
}
