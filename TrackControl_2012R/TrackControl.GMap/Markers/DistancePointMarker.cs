﻿using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Markers
{
    public class DistancePointMarker : GMapMarker
    {
        readonly string separateSymbol = ")";
        string _label = ""; // Подпись к маркеру
        
        /// <summary>
        /// номер маркера в последовательности узлов измерительного инструмента
        /// </summary>
        public int PointNumber
        {
            get { return _pointNumber; }
            set { _pointNumber = value; }
        }
        int _pointNumber;
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="point">Точка на карте, где будет располагаться маркер</param>
    /// <param name="label">Подпись к маркеру.</param>
        public DistancePointMarker(PointLatLng point, string label, int pointNumber)
      : base(point)
    {
      _label = label;
      _pointNumber = pointNumber;
      Size = new GSize(10, 10);
    }

    /// <summary>
    /// Переопределенный метод для отрисовки маркера.
    /// </summary>
    /// <param name="g"></param>
    public override void OnRender(Graphics g)
    {
      g.DrawImageUnscaled(Shared.DistancePoint, LocalPosition.X-7 , LocalPosition.Y-7 );
      if (_label.Length > 0 || _pointNumber>0)
          DrawLabel(g, string.Format("{0}{1} {2}", _pointNumber, separateSymbol,_label), Brushes.White, Brushes.Black, LocalPosition.X + 5, LocalPosition.Y - 6);
    }
    }
}
