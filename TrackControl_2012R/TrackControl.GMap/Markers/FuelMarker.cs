using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Markers
{
  /// <summary>
  /// ������ ��� �����.
  /// </summary>
  public class FuelMarker : GMapMarker
  {
    string _label = ""; // ������� � �������
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="point">����� �� �����, ��� ����� ������������� ������</param>
    /// <param name="label">������� � �������.</param>
    public FuelMarker(PointLatLng point, string label)
      : base(point)
    {
      _label = label;
      Size = new GSize(26, 31);
    }

    /// <summary>
    /// ���������������� ����� ��� ��������� �������.
    /// </summary>
    /// <param name="g"></param>
    public override void OnRender(Graphics g)
    {
      g.DrawImageUnscaled(Shared.Fuel, LocalPosition.X - 13, LocalPosition.Y - 33);
      if (_label.Length > 0)
        DrawLabel(g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6);
    }
  }
}
