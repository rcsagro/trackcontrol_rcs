﻿using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Markers
{
    internal class AreaPointMarker : GMapMarker
    {

        /// <summary>
        /// номер маркера в последовательности узлов измерительного инструмента
        /// </summary>
        public int PointNumber
        {
            get { return _pointNumber; }
            set { _pointNumber = value; }
        }

        private int _pointNumber;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="point">Точка на карте, где будет располагаться маркер</param>
        /// <param name="tooltip">Всплывающий сообщение ToolTip.</param>
        public AreaPointMarker(PointLatLng point, string tooltip, int pointNumber)
            : base(point)
        {
            base.ToolTipText = tooltip;
            _pointNumber = pointNumber;
            Size = new GSize(10, 10);
        }

        /// <summary>
        /// Переопределенный метод для отрисовки маркера.
        /// </summary>
        /// <param name="g"></param>
        public override void OnRender(Graphics g)
        {
            g.DrawImageUnscaled(Shared.DistancePoint, LocalPosition.X - 7, LocalPosition.Y - 7);
        }
    }
}
