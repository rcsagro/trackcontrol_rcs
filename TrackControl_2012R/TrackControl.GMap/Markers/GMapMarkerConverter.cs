using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Markers
{
    public static class GMapMarkerConverter
    {
        public static GMapMarker Convert(Marker marker)
        {
            try
            {
                GMapMarker m;

                switch (marker.MarkerType)
                {
                    case MarkerType.Stop:
                        m = new StopMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.Alarm:
                        m = new WarningMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.Moving:
                        m = new MoveMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.ParkingLong:
                        m = new ParkLongMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.Custom:
                    case MarkerType.Parking:
                        m = new ParkMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.Fueling:
                        m = new FuelMarker(marker.Point, marker.Title);
                        break;
                        //-------------PIN------------------------------
                    case MarkerType.Pin:
                        m = new PinMarker(PinMarkerTypes.Pin, marker.Point, marker.Title);
                        break;
                    case MarkerType.Finish:
                        m = new PinMarker(PinMarkerTypes.Finish, marker.Point, marker.Title);
                        break;
                    case MarkerType.BallGreen:
                        m = new PinMarker(PinMarkerTypes.BallGreen, marker.Point, marker.Title);
                        break;
                        //----------------------------------------------
                    case MarkerType.RFID:
                        m = new RFIDMarker(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.DistancePoint:
                        m = new DistancePointMarker(marker.Point, marker.Title, marker.Id);
                        break;
                    case MarkerType.AreaPoint:
                        m = new AreaPointMarker(marker.Point, marker.Title, marker.Id);
                        break;
                    case MarkerType.MdExecGo:
                        m = new MdExecGo(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdExecStop:
                        m = new MdExecStop(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdWaitGo:
                        m = new MdWaitGo(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdWaitStop:
                        m = new MdWaitStop(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdRepareGo:
                        m = new MdRepareGo(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdRepareStop:
                        m = new MdRepareStop(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdDinnerGo:
                        m = new MdDinnerGo(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.MdDinnerStop:
                        m = new MdDinnerStop(marker.Id, marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid0:
                        m = new ShowPosition0(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid45:
                        m = new ShowPosition45(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid90:
                        m = new ShowPosition90(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid135:
                        m = new ShowPosition135(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid180:
                        m = new ShowPosition180(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid225:
                        m = new ShowPosition225(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid270:
                        m = new ShowPosition270(marker.Point, marker.Title);
                        break;
                    case MarkerType.ArrowVid315:
                        m = new ShowPosition315(marker.Point, marker.Title);
                        break;

                    case MarkerType.ArrowVidStop:
                        m = new ShowStop(marker.Point, marker.Title);
                        break;

                    case MarkerType.Report:
                    default:
                        m = new CarMarker(marker.Point, marker.Title);
                        break;
                }
                m.ToolTipTextDetal = marker.DescriptoinDetailed;
                m.Tag = marker.Tag;
                if (m is OnlineMarker)
                {
                    ((OnlineMarker) m).IsActive = marker.IsActive;
                    ((OnlineMarker) m).LogicSensorState = marker.LogicSensorState;
                }
                return m;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error GMapMarkerConverter", MessageBoxButtons.OK);
                throw e;
            }
        }
    }
}
