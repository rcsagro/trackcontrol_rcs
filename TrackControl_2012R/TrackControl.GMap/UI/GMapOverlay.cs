﻿using System.Linq.Expressions;
using DevExpress.Utils;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.GMap.Core;
using TrackControl.GMap.MapProviders;
using TrackControl.GMap.Markers;
using TrackControl.GMap.Properties;
using TrackControl.Zones;

namespace TrackControl.GMap.UI
{
    /// <summary>
    /// GMap.NET overlay
    /// </summary>
    public class GMapOverlay
    {
        public static IZonesManager ZonesModel;

        /// <summary>
        /// is overlay visible
        /// </summary>
        public bool IsVisibile = true;

        /// <summary>
        /// управляет доступом к получению адреса из интернета (RCS Nominatim)
        /// </summary>
        public static bool IsRcsNominatimUse;

        /// <summary>
        /// управляет доступом к получению адреса из интернета (OpenStreetMap)
        /// </summary>
        public static bool IsOpenStreetMapUse;

        /// <summary>
        /// overlay Id
        /// </summary>
        public string Id;

        /// <summary>
        /// list of markers, should be thread safe
        /// </summary>
        public readonly ObservableCollectionThreadSafe<GMapMarker> Markers =
            new ObservableCollectionThreadSafe<GMapMarker>();

        public readonly ObservableCollectionThreadSafe<GMapTrack> Tracks =
            new ObservableCollectionThreadSafe<GMapTrack>();

        /// <summary>
        /// list of polygons, should be thread safe
        /// </summary>
        public readonly ObservableCollectionThreadSafe<GMapPolygon> Polygons =
            new ObservableCollectionThreadSafe<GMapPolygon>();

        /// <summary>
        /// font for markers tooltip
        /// </summary>
        public Font TooltipFont = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Bold, GraphicsUnit.Point);

        /// <summary>
        /// tooltip pen
        /// </summary>
        public Pen TooltipPen = new Pen(Color.FromArgb(0, Color.Black));

        /// <summary>
        /// tooltip background color
        /// </summary>
        public SolidBrush TooltipBackground = new SolidBrush(Color.FromArgb(120, Color.White));
        //public Brush TooltipBackground = Brushes.AliceBlue;

        /// <summary>
        /// tooltip string format
        /// </summary>
        public StringFormat TooltipFormat = new StringFormat();

        private Pen RoutePen = new Pen(Color.MidnightBlue);
        internal GMapControl Control;

        // Access the Default ToolTipController. 
        private ToolTipController defController;
        private string _markerToolTip = "";
        private GPoint _mouseToolTip;
        private GPoint _mouseCurrent;

        public GMapOverlay(GMapControl control, string id)
        {
            Control = control;
            Control.OnMouseMoveLanLong += OnMouseMove;
            Id = id;
            Markers.CollectionChanged += Markers_CollectionChanged;
            Tracks.CollectionChanged += Tracks_CollectionChanged;
            Polygons.CollectionChanged += Polygons_CollectionChanged;

            RoutePen.LineJoin = LineJoin.Round;
            RoutePen.Width = 1;

            TooltipPen.Width = 1;
            TooltipPen.LineJoin = LineJoin.Round;
            TooltipPen.StartCap = LineCap.RoundAnchor;

            TooltipFormat.Alignment = StringAlignment.Near;
            TooltipFormat.LineAlignment = StringAlignment.Near;

            InitToolTipController();
        }

        private void InitToolTipController()
        {
            // Access the Default ToolTipController. 
            defController = ToolTipController.DefaultController;
            // Customize the controller's settings. 
            defController.Appearance.BackColor = Color.AntiqueWhite;
            defController.Appearance.ForeColor = Color.Black;
            defController.AppearanceTitle.ForeColor = Color.Black;
            defController.Rounded = true;
            defController.ShowBeak = true;
            defController.ShowShadow = true;
            defController.AllowHtmlText = true;
            defController.AutoPopDelay = 10000;
            //defController.ReshowDelay = 40000;
            //defController.CloseOnClick = DefaultBoolean.False;
        }

        private void NewMethod()
        {

        }

        private void Tracks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (GMapTrack track in e.NewItems)
                {
                    Control.UpdateTrackLocalPosition(track);
                }
            }

            Control.Core_OnNeedInvalidation();
        }

        private void Markers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (GMapMarker obj in e.NewItems)
                {
                    if (obj != null)
                    {
                        obj.Overlay = this;
                        obj.Position = obj.Position;
                    }
                }
            }

            Control.Core_OnNeedInvalidation();
        }

        private void Polygons_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (GMapPolygon obj in e.NewItems)
                {
                    if (obj != null)
                    {
                        obj.Overlay = this;
                        if (Control != null)
                        {
                            Control.UpdatePolygonLocalPosition(obj);
                        }
                    }
                }
            }

            if (Control != null)
            {
                if (e.Action == NotifyCollectionChangedAction.Remove || e.Action == NotifyCollectionChangedAction.Reset)
                {
                    if (Control.IsMouseOverPolygon)
                    {
                        Control.IsMouseOverPolygon = false;
                        Control.RestoreCursorOnLeave();
                    }
                }

                if (!Control.HoldInvalidation)
                {
                    Control.Invalidate();
                }
            }
        }

        void drawLabel(Graphics g, Brush lbrush, GPoint point, string name)
        {
            int x = point.X + 3;
            int y = point.Y + 1;
            Brush brush = Brushes.White;

            g.DrawString(name, TooltipFont, brush, x - 1, y - 1);
            g.DrawString(name, TooltipFont, brush, x, y - 1);
            g.DrawString(name, TooltipFont, brush, x + 1, y - 1);
            g.DrawString(name, TooltipFont, brush, x + 1, y);
            g.DrawString(name, TooltipFont, brush, x + 1, y + 1);
            g.DrawString(name, TooltipFont, brush, x, y + 1);
            g.DrawString(name, TooltipFont, brush, x - 1, y + 1);
            g.DrawString(name, TooltipFont, brush, x - 1, y);
            g.DrawString(name, TooltipFont, lbrush, x, y);
        }

        /// <summary>
        /// draws tooltip, override to draw custom
        /// </summary>
        /// <param name="g"></param>
        /// <param name="m"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public virtual void DrawToolTip(Graphics g, GMapMarker m, int x, int y)
        {
            //Font tooltipFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular, GraphicsUnit.Point);
            //SolidBrush tooltipBackground = new SolidBrush(Color.FromArgb(0, Color.Yellow));

            GraphicsState s = g.Save();
            g.SmoothingMode = SmoothingMode.AntiAlias;

            Size st = g.MeasureString(m.ToolTipText, TooltipFont).ToSize();
            Rectangle rect = new Rectangle(x, y, st.Width + 2, st.Height);
            rect.Offset(0, -st.Height);

            //g.FillRectangle(TooltipBackground, rect);
            //g.DrawRectangle(TooltipPen, rect);
            //g.DrawString(m.ToolTipText, TooltipFont, Brushes.Black, rect, TooltipFormat);
            GPoint center = new GPoint(x, y);
            drawLabel(g, Brushes.DarkRed, center, m.ToolTipText);

            g.Restore(s);
        }

        /// <summary>
        /// renders objects and routes
        /// </summary>
        /// <param name="g"></param>
        internal void Render(Graphics g, ref bool showZoneToolTip)
        {
            drawTracks(g);
            drawPolygons(g);
            DrawMarkers(g, ref showZoneToolTip);
        }

        private void drawTracks(Graphics g)
        {
            int i = 0;
            while (i < Tracks.Count)
            {
                GMapTrack track = null;
                lock (Tracks)
                {
                    if (i < Tracks.Count)
                        track = Tracks[i];
                }
                if (null != track)
                {
                    switch (Control.DrawTrackMode)
                    {
                        case DrawTrackModes.DiffColors:
                            if (Control.ValueCodes.Count > 0)
                                track.DrawDiffColors(g, Control.ValueCodes);
                            break;
                        case DrawTrackModes.Route:
                            track.DrawRoute(g);
                            break;
                        case DrawTrackModes.Thick:
                            track.DrawThick(g);
                            break;
                        default:
                            track.DrawSimple(g);
                            break;
                    }
                    if (Control.IsTrackAnalizing && track.IsActive)
                        track.DrawApexes(g);
                }
                i++;
            }
        }

        private void DrawMarkers(Graphics g, ref bool showZoneToolTip)
        {
            try
            {
                int i = 0;

                while (i < Markers.Count)
                {
                    GMapMarker m = null;

                    lock (Markers)
                    {
                        if (i < Markers.Count)
                            m = Markers[i];
                    }

                    if (null != m && m.Visible &&
                        Control.Core.CurrentRegion.Contains(m.LocalPosition.X, m.LocalPosition.Y))
                    {
                        m.OnRender(g);
                    }

                    i++;
                }

                // tooltips above
                i = 0;
                while (i < Markers.Count)
                {
                    GMapMarker m = null;

                    lock (Markers)
                    {
                        if (i < Markers.Count)
                            m = Markers[i];
                    }
                    if (null != m && m.Visible &&
                        Control.Core.CurrentRegion.Contains(m.LocalPosition.X, m.LocalPosition.Y))
                    {
                        if (!string.IsNullOrEmpty(m.ToolTipText))
                        {
                            if (m is AreaPointMarker)
                            {
                                DrawToolTip(g, m, m.LocalPosition.X, m.LocalPosition.Y);
                            }
                            else
                            {
                                if (m.TooltipMode == MarkerTooltipMode.Always ||
                                    (m.TooltipMode == MarkerTooltipMode.OnMouseOver && m.IsMouseOver))
                                {
                                    //if (_markerToolTip.Length == 0)
                                    if (m.ToolTipText != _markerToolTip && DifferenceMoreValue(_mouseCurrent.X, _mouseToolTip.X, 2)
                                            && DifferenceMoreValue(_mouseCurrent.Y, _mouseToolTip.Y, 2))
                                    {
                                        if (m.ToolTipTextDetal != null &&
                                            !m.ToolTipTextDetal.Contains(Resources.Location))
                                        {
                                            string adress = string.Format("{0} {1}", Math.Round(m.Position.Lat, 4),
                                                                          Math.Round(m.Position.Lng, 4));
                                            string zones =
                                                GetZonesWithPoint(new PointLatLng(m.Position.Lat, m.Position.Lng));
                                            if (zones.Length > 0)
                                            {
                                                adress = zones;
                                            }
                                            else if (IsOpenStreetMapUse)
                                            {
                                                GeoCoderStatusCode status;
                                                var pos = GMapProviders.getOpenStreetMap.GetPlacemarkOSM(
                                                    m.Position, out status);

                                                if (status == GeoCoderStatusCode.G_GEO_SUCCESS && pos != null)
                                                {
                                                    adress = pos.Address;
                                                }
                                            }
                                            else if (IsRcsNominatimUse)
                                            {
                                                GeoCoderStatusCode status;
                                                var pos = GMapProviders.getRcsNominatim.GetPlacemark(m.Position,
                                                                                                     out status);

                                                if (status == GeoCoderStatusCode.G_GEO_SUCCESS && pos != null)
                                                {
                                                    adress = pos.Address;
                                                }
                                            }

                                            m.ToolTipTextDetal = string.Format("{1}<b>{2}: </b>{3}"
                                                                               , Environment.NewLine
                                                                               , m.ToolTipTextDetal
                                                                               , Resources.Location
                                                                               , adress);
                                        }
                                        defController.AutoPopDelay = 10000;
                                        defController.ShowHint(m.ToolTipTextDetal, m.ToolTipText);
                                        showZoneToolTip = false;
                                        _markerToolTip = m.ToolTipText;
                                        _mouseToolTip = _mouseCurrent;
                                    }
                                }
                                else
                                {
                                    if (_markerToolTip.Length > 0 && m.ToolTipText == _markerToolTip
                                        && DifferenceMoreValue(_mouseCurrent.X, _mouseToolTip.X, 2)
                                        && DifferenceMoreValue(_mouseCurrent.Y, _mouseToolTip.Y, 2))
                                    {
                                        defController.HideHint();
                                        _markerToolTip = "";
                                        showZoneToolTip = true;
                                    }
                                }
                            }

                        }
                    }

                    i++;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error draw markers", MessageBoxButtons.OK);
            }
        }

        private void drawPolygons(Graphics g)
        {
            if (Control.PolygonsEnabled)
            {
                foreach (GMapPolygon r in Polygons)
                {
                    if (r.IsVisible)
                    {
                        r.OnRender(g);
                    }
                }
            }
        }

        private void OnMouseMove(GPoint pos)
        {
            _mouseCurrent = pos;
        }

        private bool DifferenceMoreValue(int inp1, int inp2, int value)
        {
            if ((inp1 - inp2) > value)
            {
                return true;
            }
            else if ((inp2 - inp1) > value)
            {
                return true;
            }
            else
                return false;
        }

        private string GetZonesWithPoint(PointLatLng p)
        {
            string zonesList = "";
            foreach (Zone z in ZonesModel.Zones)
            {
                if (z.Contains(p))
                {
                    if (zonesList.Length == 0)
                        zonesList = zonesList + z.Name;
                    else
                        zonesList = zonesList + Environment.NewLine + z.Name;
                }
            }
            return zonesList;
        }
    }
}