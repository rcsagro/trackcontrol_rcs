using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.Utils;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Properties;

namespace TrackControl.GMap.UI
{
    public class GMapZone
    {
        const int FONT_HEIGHT = 9;
        static readonly int MIN_SIZE = FONT_HEIGHT * 3;
        static readonly Font lFont = new Font("Tahoma", FONT_HEIGHT, FontStyle.Bold);
        private ToolTipController defController;
        IZone _zone;

        /// <summary>
        /// ��������� �����-������ ����������� ���� � �������������� ������� ���������.
        /// </summary>
        internal readonly List<PointLatLng> Points;

        /// <summary>
        /// ��������� �����-������ ����������� ���� � ������� ��������� ��������.
        /// </summary>
        internal readonly List<GPoint> LocalPoints;

        internal bool IsMouseOver { get; set; }

        private string ToolTipText
        {
            get
            {
                string toolTip = "";   
                if (null != _zone)
                    {
                        toolTip = string.Format("<b>{0}</b>:{1} {2};{3}{4}",
                            _zone.Name.TrimEnd(),
                            Math.Round(_zone.AreaGa, 2),
                            Resources.AreaGaShort, Environment.NewLine, _zone.Description.TrimEnd()); 
                        string culture =  _zone.GetCultureActive();
                        if (culture.Length > 0)
                            toolTip = string.Format("{0}<b>{1}</b>", toolTip,  culture);
                    }
                return toolTip;
            }
        }

        public int Id
        {
            get
            {
                if (null != _zone)
                    return _zone.Id;
                return -1;
            }
        }

        /// <summary>
        /// �������, ��� ����������� ���� �������� �� �����
        /// </summary>
        internal bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        bool _selected;

        public IZone Zone
        {
            get { return _zone; }
        }

        internal bool IsChanged
        {
            get { return _isChanged; }
            set { _isChanged = value; }
        }

        bool _isChanged;

        /// <summary>
        /// ������� ��������� ����� �� ��������� ����� ����
        /// </summary>
        internal bool HitOnBorder
        {
            get { return _hitOnBorder; }
        }

        bool _hitOnBorder;


        internal int InsertAt
        {
            get { return _insertAt; }
        }

        int _insertAt;

        private GMapZone()
        {
            Points = new List<PointLatLng>();
            LocalPoints = new List<GPoint>();
        }

        /// <summary>
        /// ����������� ��� TrackControl.GMap.UI.GMapZone
        /// </summary>
        public GMapZone(IZone zone)
        {
            if (null == zone)
                throw new ArgumentNullException("zone");

            _zone = zone;
            Points = new List<PointLatLng>(_zone.Points);
            LocalPoints = new List<GPoint>(Points.Count);
            InitToolTipController();
        }

        public static GMapZone NewZone
        {
            get { return new GMapZone(); }
        }

        internal void Commit()
        {
            _zone.Points = Points.ToArray();
        }

        /// <summary>
        /// �������� ����������� ���� �� �����.
        /// </summary>
        /// <param name="g"></param>
        internal void Render(Graphics g, bool showTooltip)
        {
            if (Points.Count > 0)
            {
                GraphicsState state = g.Save();
                g.SmoothingMode = SmoothingMode.AntiAlias;

                using (GraphicsPath path = new GraphicsPath())
                {
                    GPoint center;
                    GPoint pMin;
                    GPoint pMax;
                    GPoint last = center = pMin = pMax = LocalPoints[0];
                    GPoint current;

                    for (int i = 1; i < LocalPoints.Count; i++)
                    {
                        current = LocalPoints[i];
                        path.AddLine(last.X, last.Y, current.X, current.Y);
                        last = current;
                        if (current.Y < pMin.Y)
                            pMin.Y = current.Y;
                        if (current.X < pMin.X)
                            pMin.X = current.X;

                        if (current.Y > pMax.Y)
                            pMax.Y = current.Y;
                        if (current.X > pMax.X)
                            pMax.X = current.X;
                    }
                    path.CloseFigure();
                    RectangleF rect = path.GetBounds();
                    if (rect.Height > 5 && rect.Width > 5)
                    {
                        g.DrawPath(new Pen(_zone.Style.Color, 2), path);
                        Color color;
                        color = _zone.Style.Color;
                        Color transpColor = Color.FromArgb(30, color.R, color.G, color.B);
                        SolidBrush brush = new SolidBrush(transpColor);
                        g.FillPath(brush, path);

                        center.X = (int) (pMin.X + (pMax.X - pMin.X)/2);
                        center.Y = (int) (pMin.Y + (pMax.Y - pMin.Y)/2);
                        if (rect.Height > MIN_SIZE || rect.Width > MIN_SIZE)
                        {
                            drawLabel(g, Brushes.Navy, center);
                        }

                        if (Selected)
                            DrawTracker(g);

                        if (IsMouseOver)
                        {
                            center.Y += 30;
                            DrawToolTip(g, center, showTooltip);
                        }
                    }
                }
                g.Restore(state);
            }
        }

        internal void DrawTracker(Graphics g)
        {
            using (Pen pen = new Pen(Brushes.Black, 1f))
            {
                for (int i = 1; i <= LocalPoints.Count; i++)
                {
                    GPoint p = LocalPoints[i - 1];
                    RectangleF rect = getApexRect(i);
                    g.FillEllipse(Brushes.White, rect);
                    g.DrawEllipse(pen, rect);
                    g.DrawRectangle(pen, p.X, p.Y, 1, 1);
                }
            }
        }

        /// <summary>
        /// ��������� ��������� ����� � ������������ ������� ����������� ����
        /// </summary>
        /// <param name="point">����� ��� ��������</param>
        /// <returns>
        ///    -1   -  ��������� ���
        ///     0   -  ����� �������� � ����
        ///   1..n  -  ����� ������� ����, ���� �������� �����
        /// </returns>
        internal int CheckHit(PointF point)
        {
            if (Selected)
            {
                _hitOnBorder = checkHitOnBorder(point);
                for (int i = 1; i <= LocalPoints.Count; i++)
                {
                    if (getApexRect(i).Contains(point))
                        return i;
                }
            }
            using (GraphicsPath path = new GraphicsPath())
            {
                for (int i = 0; i < LocalPoints.Count; i++)
                {
                    GPoint current = LocalPoints[i];
                    if (i == 0)
                    {
                        path.AddLine(current.X, current.Y, current.X, current.Y);
                        continue;
                    }
                    PointF last = path.GetLastPoint();
                    path.AddLine(last.X, last.Y, current.X, current.Y);
                }
                path.CloseFigure();
                if (path.IsVisible(point))
                    return 0;
            }

            return -1;
        }

        internal void MoveApexTo(Point p, int index)
        {
            LocalPoints[index - 1] = new GPoint(p.X, p.Y);
        }

        private bool checkHitOnBorder(PointF point)
        {
            int count = LocalPoints.Count;
            if (count == 0) return false;
            bool retVal = false;
            using (Pen pen = new Pen(Color.Black, 4))
            {
                GPoint last = LocalPoints[count - 1];
                for (int i = 0; i < LocalPoints.Count; i++)
                {
                    GPoint current = LocalPoints[i];
                    if (last != current)
                    {
                        using (GraphicsPath path = new GraphicsPath())
                        {
                            path.AddLine(last.X, last.Y, current.X, current.Y);
                            path.Widen(pen);
                            using (Region region = new Region(path))
                            {
                                retVal = region.IsVisible(point);
                                if (retVal)
                                {
                                    _insertAt = i;
                                    break;
                                }
                            }
                        }
                    }
                    last = current;
                }
            }
            return retVal;
        }

        RectangleF getApexRect(int index)
        {
            index = index < 1 ? 1 : index;
            index = index > LocalPoints.Count ? LocalPoints.Count : index;
            GPoint point = LocalPoints[index - 1];
            return new RectangleF(point.X - 2, point.Y - 2, 5, 5);
        }

        void drawLabel(Graphics g, Brush lbrush, GPoint point)
        {
            int x = point.X + 3;
            int y = point.Y + 1;
            Brush brush = Brushes.White;
            string name = null != _zone ? _zone.Name : Resources.NewZone;
            g.DrawString(name, lFont, brush, x - 1, y - 1);
            g.DrawString(name, lFont, brush, x, y - 1);
            g.DrawString(name, lFont, brush, x + 1, y - 1);
            g.DrawString(name, lFont, brush, x + 1, y);
            g.DrawString(name, lFont, brush, x + 1, y + 1);
            g.DrawString(name, lFont, brush, x, y + 1);
            g.DrawString(name, lFont, brush, x - 1, y + 1);
            g.DrawString(name, lFont, brush, x - 1, y);
            g.DrawString(name, lFont, lbrush, x, y);
        }

        public bool Contains(PointLatLng point)
        {
            return _zone.Contains(point);
        }

        void DrawToolTip(Graphics g, GPoint point, bool showToolTip)
        {
            //if (showToolTip)
            //{
            //    Font tooltipFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular, GraphicsUnit.Point);
            //    //Pen tooltipPen = new Pen(Color.FromArgb(50, Color.Black));
            //    SolidBrush tooltipBackground = new SolidBrush(Color.FromArgb(50, Color.Yellow));
            //    //StringFormat tooltipFormat = new StringFormat();

            //    GraphicsState s = g.Save();
            //    g.SmoothingMode = SmoothingMode.AntiAlias;

            //    Size st = g.MeasureString(ToolTipText, tooltipFont).ToSize();
            //    Rectangle rect = new Rectangle(point.X, point.Y, st.Width + 2, st.Height);
            //    rect.Offset(0, -st.Height);

            //    g.FillRectangle(tooltipBackground, rect);
            //    //g.DrawRectangle(tooltipPen, rect);
            //    g.DrawString(ToolTipText, tooltipFont, Brushes.Black, rect);

            //    g.Restore(s);
            //}

            if (showToolTip)
            {
                defController.HideHint();
                defController.ShowHint(ToolTipText, ToolTipLocation.TopRight);
                IsMouseOver = false;
            }
        }

        private void InitToolTipController()
        {
            // added this code aketner
           
            // Access the Default ToolTipController. 
            defController = ToolTipController.DefaultController;
            // Customize the controller's settings. 
            defController.Appearance.BackColor = Color.AntiqueWhite;
            defController.Appearance.ForeColor = Color.Black;
            defController.AppearanceTitle.ForeColor = Color.Black;
            //defController.ToolTipLocation = ToolTipLocation.BottomRight;
            defController.Rounded = true;
            defController.ShowBeak = true;
            defController.ShowShadow = true;
            defController.AllowHtmlText = true;
            defController.AutoPopDelay = 2000;
            //defController.ReshowDelay = 10000;
            defController.CloseOnClick = DefaultBoolean.False;
        }
    }
}
