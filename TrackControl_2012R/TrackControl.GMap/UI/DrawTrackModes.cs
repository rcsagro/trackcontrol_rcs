﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.GMap
{
    public enum DrawTrackModes
    {
        Simple,
        DiffColors,
        Route,
        Thick
    }
}
