﻿using System;
using System.IO;
using System.Drawing;
using System.Security.Policy;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Collections.Generic;
using DevExpress.XtraEditors.Design;
using DevExpress.XtraPrinting.Export.Pdf;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.GMap.Core;
using TrackControl.GMap.Tools;
using TrackControl.GMap.Internals;
using TrackControl.GMap.Properties;
using TrackControl.Vehicles;
using System.Data;
using System.Xml;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.GMap;

namespace TrackControl.GMap.UI
{
    /// <summary>
    /// Контроль карты GMap
    /// </summary>
    public class GMapControl : UserControl
    {
        public event MouseLanLong OnMouseMoveLanLong;
        public event MouseLanLong OnMouseClickLanLong;

        public event TrackPointClick OnTrackPointClicked = delegate { };

        /// <summary>
        /// Возникает при клике на маркере
        /// </summary>
        public event MarkerClick OnMarkerClick;

        /// <summary>
        /// Возникает, когда указатель мыши помещается над маркером
        /// </summary>
        public event MarkerEnter OnMarkerEnter;

        /// <summary>
        /// Возникает, когда указатель мыши смещается с маркера
        /// </summary>
        public event MarkerLeave OnMarkerLeave;

        /// <summary>
        /// Возникает, когда отображается пустой тайл
        /// </summary>
        public event EmptyTileError OnEmptyTileError;

        /// <summary>
        /// Возникает при выделении контрольной зоны
        /// </summary>
        public event Action<IZone> ZoneSelected;

        public event GeometryChangedHandler NewZoneAdded;

        /// <summary>
        /// Коллекция слоев, должна быть потокобезопасной
        /// </summary>
        public readonly ObservableCollectionThreadSafe<GMapOverlay> Overlays =
            new ObservableCollectionThreadSafe<GMapOverlay>();

        /// <summary>
        /// Слой для инструментов(измерения расстояния,площади и др)
        /// </summary>
        private GMapOverlay _toolLayer;

        public GMapOverlay ToolLayer
        {
            get { return _toolLayer; }
            set { _toolLayer = value; }
        }

        public bool IsKeepZonesSelected { get; set; }

        /// <summary>
        /// Коллекция контрольных зон, должна быть потокобезопасной
        /// </summary>
        public readonly ObservableCollectionThreadSafe<GMapZone> Zones = new ObservableCollectionThreadSafe<GMapZone>();

        /// <summary>
        /// Режим масштабирования карты при движении колесика мыши
        /// </summary>
        public MouseWheelZoomType MouseWheelZoomType = MouseWheelZoomType.MousePositionAndCenter;

        /// <summary>
        /// Карандаш для отрисовки границы пустого тайла
        /// </summary>
        public Pen EmptyTileBorders = new Pen(Brushes.White, 1);

        /// <summary>
        /// Карандаш для отрисовки информации о масштабе
        /// </summary>
        public Pen ScalePen = new Pen(Brushes.Blue, 1);

        /// <summary>
        /// area selection pen
        /// </summary>
        public Pen SelectionPen = new Pen(Brushes.Blue, 4);

        /// <summary>
        /// Кисть для фона пустого тайла
        /// </summary>
        public Brush EmptytileBrush = Brushes.PaleGoldenrod;

        /// <summary>
        /// Центрирование указателя мыши при движении колесика
        /// </summary>
        public bool CenterPositionOnMouseWheel = true;

        /// <summary>
        /// Отображать информацию о текущем масштабе
        /// </summary>
        public bool MapScaleInfoEnabled = true;

        /// <summary>
        /// Кнопка мыши для движения карты
        /// </summary>
        public MouseButtons DragButton = MouseButtons.Left;

        private bool showTileGridLines;

        /// <summary>
        /// Прямоугольная область, отображаемая на карте и заданная в геогр. координатах
        /// </summary>
        internal RectLatLng? _screenRect = null;

        /// <summary>
        /// изменение положения области экрана.Для режима авторазмера при on-line слежении 
        /// при отсутствии изменения отсекается постоянная прорисовка при неизмненном положении объектов 
        /// </summary>
        private bool _screenRectChanged = false;

        public bool ScreenRectChanged
        {
            get { return _screenRectChanged; }
            set { _screenRectChanged = value; }
        }

        private bool _screenMouseDrug = false;

        public RectLatLng ViewArea
        {
            get { return Core.CurrentViewArea; }
        }

        /// <summary>
        /// shows tile gridlines
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowTileGridLines
        {
            get
            {
                return showTileGridLines;
            }
            set
            {
                showTileGridLines = value;
                Invalidate();
            }
        }

        // internal stuff
        internal readonly GCore Core = new GCore();
        internal readonly Font CopyrightFont = new Font(FontFamily.GenericSansSerif, 7, FontStyle.Regular);
        internal readonly Font MissingDataFont = new Font(FontFamily.GenericSansSerif, 11, FontStyle.Bold);
        private Font ScaleFont = new Font(FontFamily.GenericSansSerif, 5, FontStyle.Italic);
        internal readonly StringFormat CenterFormat = new StringFormat();
        internal readonly StringFormat BottomFormat = new StringFormat();
        private bool RaiseEmptyTileError;

        public MapTool ToolDistance = null;
        public MapTool ToolArea = null;
        public MapTool Tool = null;

        public bool IsZoneEditor;

        public bool EditorZonesOn = false;

        public bool zoneEditorOn = false;

        public bool IsAddingNewZone;

        public bool IsTrackAnalizing;

        public bool keyMoveStarter = false;

        public DrawTrackModes DrawTrackMode = DrawTrackModes.Simple;

        public IDictionary<float, Color> ValueCodes;

        private struct MousePos
        {
            public int X;
            public int Y;
        }

        private MousePos pos;

        #region -- .ctor --

        /// <summary>
        /// Конструктор.
        /// </summary>
        public GMapControl()
        {
            if (!DesignMode)
            {
                GMaps.Instance.ImageProxy = new WindowsFormsImageProxy();

                SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
                SetStyle(ControlStyles.AllPaintingInWmPaint, true);
                SetStyle(ControlStyles.UserPaint, true);
                SetStyle(ControlStyles.Opaque, true);
                ResizeRedraw = true;

                Zones.CollectionChanged += Zones_CollectionChanged;

                // to know when to invalidate
                Core.OnNeedInvalidation += Core_OnNeedInvalidation;
                Core.OnMapDrag += GMap_OnMapDrag;

                Core.CurrentRegion = new GRectangle(-50, -50, Size.Width + 100, Size.Height + 100);

                CenterFormat.Alignment = StringAlignment.Center;
                CenterFormat.LineAlignment = StringAlignment.Center;

                BottomFormat.Alignment = StringAlignment.Center;
                BottomFormat.LineAlignment = StringAlignment.Far;

                MapType = MapType.GoogleMap;

                _toolLayer = new GMapOverlay(this, "ToolLayer");
                this.Overlays.Add(_toolLayer);

                ToolDistance = new FreeMoveTool();
                ToolArea = new FreeMoveTool();
                Tool = new FreeMoveTool();
                isTooltipShow = true;
                isDistanceUse = true;
                Zoom = Convert.ToInt32(UseMap.getZoomMap());
            }
        }

        #endregion

        /// <summary>
        /// Обрабатывает изменение в коллекции контрольных зон.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Zones_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (GMapZone zone in e.NewItems)
                {
                    UpdateZoneLocalPosition(zone);
                }
            }

            Core_OnNeedInvalidation();
        }

        /// <summary>
        /// Пересчитываем координаты объектов при движении карты.
        /// </summary>
        private void GMap_OnMapDrag()
        {
            if (IsZoneEditor || ToolDistance is DistanceTool || ToolArea is AreaTool)
            {
                if (ToolArea is AreaTool)
                {
                    ToolArea.OnMapDrag(this);
                }
                else if (ToolDistance is DistanceTool)
                {
                    ToolDistance.OnMapDrag(this);
                }
                else if (Tool != null)
                {
                    Tool.OnMapDrag(this);
                }
                else
                {
                    // to do
                }
            } // if

            try
            {
                foreach (GMapZone zone in Zones)
                {
                    UpdateZoneLocalPosition(zone);
                }

                foreach (GMapOverlay o in Overlays)
                {
                    if (o.IsVisibile)
                    {
                        int i = 0;
                        while (i < o.Markers.Count)
                        {
                            GMapMarker m = null;
                            lock (o.Markers)
                            {
                                if (i < o.Markers.Count)
                                    m = o.Markers[i];
                            }
                            if (null != m)
                            {
                                m.Position = m.Position;
                            }
                            i++;
                        }
                        i = 0;
                        while (i < o.Tracks.Count)
                        {
                            GMapTrack track = null;
                            lock (o.Tracks)
                            {
                                if (i < o.Tracks.Count)
                                    track = o.Tracks[i] as GMapTrack;
                            }
                            if (null != track)
                            {
                                UpdateTrackLocalPosition(track);
                            }
                            i++;
                        }
                        while (i < o.Polygons.Count)
                        {
                            GMapPolygon polygon = null;
                            lock (o.Polygons)
                            {
                                if (i < o.Polygons.Count)
                                    polygon = o.Polygons[i] as GMapPolygon;
                            }
                            if (null != polygon)
                            {
                                UpdatePolygonLocalPosition(polygon);
                            }
                            i++;
                        }
                    }
                }
                _screenMouseDrug = true;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Controller", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Потокобезопасный вызов метода Invalidate.
        /// </summary>
        public void Core_OnNeedInvalidation()
        {
            try
            {
                if (InvokeRequired)
                {
                    MethodInvoker m = delegate
                    {
                        Invalidate(false);
                    };

                    if (this.IsHandleCreated)
                    {
                        Invoke(m);
                    }
                }
                else
                {
                    Invalidate(false);
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error GMapControl. Core_OnNeedInvalidation", MessageBoxButtons.OK);
            }
        }

        public void CreateNewZone(IZone zone)
        {
            Tool = new CreateNewZoneTool(new GMapZone(zone));
            IsAddingNewZone = true;
        }

        public void ReleaseEditor()
        {
            Tool = new FreeMoveTool(); 
            IsAddingNewZone = false;
        }

        public void SelectZone(IZone zone)
        {
            foreach (GMapZone gz in Zones)
            {
                if (gz.Id == zone.Id)
                    gz.Selected = true;
                else
                    gz.Selected = false;
            }
            Invalidate(true);
        }

        /// <summary>
        /// Рендерит карту с использованием GDI+
        /// </summary>
        private void drawMap(Graphics g)
        {
            try
            {
                for (int i = -Core.sizeOfMapArea.Width; i <= Core.sizeOfMapArea.Width; i++)
                {
                    for (int j = -Core.sizeOfMapArea.Height; j <= Core.sizeOfMapArea.Height; j++)
                    {
                        Core.tilePoint = Core.centerTileXYLocation;
                        Core.tilePoint.X += i;
                        Core.tilePoint.Y += j;
                        {
                            Tile t = Core.Matrix[Core.tilePoint];
                            if (t != null)
                            {
                                Core.tileRect.X = Core.tilePoint.X * Core.tileRect.Width;
                                Core.tileRect.Y = Core.tilePoint.Y * Core.tileRect.Height;
                                Core.tileRect.Offset(Core.renderOffset);

                                if (Core.CurrentRegion.IntersectsWith(Core.tileRect))
                                {
                                    bool found = false;

                                    // render tile
                                    lock (t.Overlays)
                                    {
                                        foreach (WindowsFormsImage img in t.Overlays)
                                        {
                                            if (img != null && img.Img != null)
                                            {
                                                if (!found)
                                                    found = true;

                                                g.DrawImage(img.Img, Core.tileRect.X, Core.tileRect.Y, Core.tileRect.Width, Core.tileRect.Height);

                                                if (ShowTileGridLines)
                                                {
                                                    g.DrawRectangle(EmptyTileBorders, Core.tileRect.X, Core.tileRect.Y,
                                                        Core.tileRect.Width, Core.tileRect.Height);

                                                    if (Core.tilePoint == Core.centerTileXYLocation)
                                                    {
                                                        g.DrawString(String.Format("CENTER TILE: {0}", Core.tilePoint),
                                                            MissingDataFont, Brushes.Red,
                                                            new RectangleF(Core.tileRect.X, Core.tileRect.Y,
                                                                Core.tileRect.Width, Core.tileRect.Height),
                                                            CenterFormat);
                                                    }
                                                    else
                                                    {
                                                        g.DrawString(String.Format("TILE: {0}", Core.tilePoint),
                                                            MissingDataFont, Brushes.Red,
                                                            new RectangleF(Core.tileRect.X, Core.tileRect.Y,
                                                                Core.tileRect.Width, Core.tileRect.Height),
                                                            CenterFormat);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    // add text if tile is missing
                                    if (!found)
                                    {
                                        g.FillRectangle(EmptytileBrush,
                                            new RectangleF(Core.tileRect.X, Core.tileRect.Y, Core.tileRect.Width,
                                                Core.tileRect.Height));
                                        g.DrawString(Resources.EmptyTileText, MissingDataFont, Brushes.Black,
                                            new RectangleF(Core.tileRect.X, Core.tileRect.Y, Core.tileRect.Width,
                                                Core.tileRect.Height),
                                            CenterFormat);

                                        if (ShowTileGridLines)
                                        {
                                            g.DrawString(String.Format("TILE: {0}", Core.tilePoint), MissingDataFont,
                                                Brushes.Red,
                                                new RectangleF(Core.tileRect.X, Core.tileRect.Y, Core.tileRect.Width,
                                                    Core.tileRect.Height),
                                                BottomFormat);
                                        }

                                        g.DrawRectangle(EmptyTileBorders, Core.tileRect.X, Core.tileRect.Y,
                                            Core.tileRect.Width, Core.tileRect.Height);

                                        // raise error
                                        if (OnEmptyTileError != null)
                                        {
                                            if (!RaiseEmptyTileError)
                                            {
                                                RaiseEmptyTileError = true;
                                                OnEmptyTileError(t.Zoom, t.Pos);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Mode", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Преобразует координаты точек трека из географической системы
        /// координат в систему координат контрола.
        /// </summary>
        /// <param name="track"></param>
        public void UpdateTrackLocalPosition(GMapTrack track)
        {
            try
            {
                track.LocalPoints.Clear();
                track.GeoPoints.ForEach(delegate(IGeoPoint igp)
                {
                    GPoint p = Projection.FromLatLngToPixel(igp.LatLng, Core.Zoom);
                    p.Offset(Core.renderOffset);
                    track.LocalPoints.Add(p);
                });
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Controller", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Переводит координаты вершин контрольной зоны из географической системы
        /// координат в систему координат контрола.
        /// </summary>
        /// <param name="zone"></param>
        public void UpdateZoneLocalPosition(GMapZone zone)
        {
            zone.LocalPoints.Clear();

            zone.Points.ForEach(delegate(PointLatLng pg)
            {
                GPoint p = Projection.FromLatLngToPixel(pg, Core.Zoom);
                p.Offset(Core.renderOffset);
                zone.LocalPoints.Add(p);
            });
        }

        /// <summary>
        /// updates routes local position
        /// </summary>
        /// <param name="route"></param>
        public void UpdateRouteLocalPosition(GMapRoute route)
        {
            route.LocalPoints.Clear();

            foreach (PointLatLng pg in route.Points)
            {
                GPoint p = FromLatLngToLocal(pg);
                p.OffsetNegative(Core.renderOffset);
                route.LocalPoints.Add(p);
            }
            route.UpdateGraphicsPath();
        }

        /// <summary>
        /// updates polygons local position
        /// </summary>
        /// <param name="polygon"></param>
        public void UpdatePolygonLocalPosition(GMapPolygon polygon)
        {
            polygon.LocalPoints.Clear();

            foreach (PointLatLng pg in polygon.Points)
            {
                GPoint p = Projection.FromLatLngToPixel(pg, Core.Zoom);
                ;
                //p.OffsetNegative(Core.renderOffset);
                p.Offset(Core.renderOffset);
                polygon.LocalPoints.Add(p);
            }
        }

        /// <summary>
        /// sets zoom to max to fit rect
        /// </summary>
        public void SetZoomToFitRect(RectLatLng rect)
        {
            try
            {
                int maxZoom = Core.GetMaxZoomToFitRect(rect);
                IsMouseOverMarker = false;
                if (maxZoom > 0)
                {
                    PointLatLng center = new PointLatLng(rect.Lat - (rect.HeightLat / 2), rect.Lng + (rect.WidthLng / 2));
                    CurrentPosition = center;

                    if (maxZoom > Globals.GMAP_MAX)
                    {
                        maxZoom = Globals.GMAP_MAX;
                    }

                    if (Zoom != maxZoom)
                    {
                        Zoom = maxZoom;
                    }
                }
                Core.ReloadMap();
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error GMapControl", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// zooms and centers all route
        /// </summary>
        /// <param name="overlayId">overlay id or null to check all</param>
        public void ZoomAndCenterTracks(string overlayId)
        {
            RectLatLng? r = GetRectOfAllTracks(overlayId);
            if (r.HasValue)
            {
                RectLatLng rect = r.Value;
                double hPadding = rect.WidthLng * 0.1;
                double vPadding = rect.HeightLat * 0.1;
                if (!_screenRect.HasValue ||
                    rect.Left < _screenRect.Value.Left + hPadding ||
                    rect.Right > _screenRect.Value.Right - hPadding ||
                    rect.Top > _screenRect.Value.Top + vPadding ||
                    rect.Bottom < _screenRect.Value.Bottom - vPadding || _screenRectChanged)
                {
                    rect.Lng -= hPadding;
                    rect.WidthLng += hPadding * 2;
                    rect.Lat += vPadding * 1.5;
                    rect.HeightLat += vPadding * 2.5;
                    SetZoomToFitRect(rect);

                    rect.Lat -= vPadding * 0.5;
                    _screenRect = rect;
                    _screenRectChanged = false;
                }
                //else if (_screenRect.HasValue && _screenRectChanged)
                //{
                //    SetZoomToFitRect(rect);
                //    _screenRect = rect;
                //    _screenRectChanged = false;
                //}
            }
        }

        public void ZoomAndCenterZones()
        {
            if (Zones.Count > 0)
            {
                List<PointLatLng> points = new List<PointLatLng>(Zones[0].Points);
                for (int i = 1; i < Zones.Count; i++)
                {
                    points.AddRange(Zones[i].Points);
                }
                RectLatLng? rect = GetRectOfPointSet(points);
                if (rect.HasValue)
                {
                    SetZoomToFitRect(rect.Value);
                }
            }
        }

        /// <summary>
        /// gets rectangle with all objects inside
        /// </summary>
        /// <param name="overlayId">overlay id or null to check all</param>
        /// <returns></returns>
        public RectLatLng? GetRectOfAllMarkers(string overlayId)
        {
            RectLatLng? ret = null;

            double left = double.MaxValue;
            double top = double.MinValue;
            double right = double.MinValue;
            double bottom = double.MaxValue;

            foreach (GMapOverlay o in Overlays)
            {
                if (overlayId == null || o.Id == overlayId)
                {
                    if (o.IsVisibile && o.Markers.Count > 0)
                    {
                        foreach (GMapMarker m in o.Markers)
                        {
                            if (m.Visible)
                            {
                                // left
                                if (m.Position.Lng < left)
                                {
                                    left = m.Position.Lng;
                                }

                                // top
                                if (m.Position.Lat > top)
                                {
                                    top = m.Position.Lat;
                                }

                                // right
                                if (m.Position.Lng > right)
                                {
                                    right = m.Position.Lng;
                                }

                                // bottom
                                if (m.Position.Lat < bottom)
                                {
                                    bottom = m.Position.Lat;
                                }

                                ret = RectLatLng.FromLTRB(left, top, right, bottom);
                            }
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// gets rectangle with all objects inside
        /// </summary>
        /// <param name="overlayId">overlay id or null to check all</param>
        /// <returns></returns>
        public RectLatLng? GetRectOfAllTracks(string overlayId)
        {
            RectLatLng? ret = null;

            double left = double.MaxValue;
            double top = double.MinValue;
            double right = double.MinValue;
            double bottom = double.MaxValue;

            foreach (GMapOverlay o in Overlays)
            {
                if (overlayId == null || o.Id == overlayId)
                {
                    if (o.IsVisibile && o.Tracks.Count > 0)
                    {
                        foreach (GMapTrack track in o.Tracks)
                        {
                            track.GeoPoints.ForEach(delegate(IGeoPoint igp)
                            {
                                if (igp.LatLng.Lng < left)
                                    left = igp.LatLng.Lng; // LEFT
                                if (igp.LatLng.Lat > top)
                                    top = igp.LatLng.Lat; // TOP
                                if (igp.LatLng.Lng > right)
                                    right = igp.LatLng.Lng; // RIGHT
                                if (igp.LatLng.Lat < bottom)
                                    bottom = igp.LatLng.Lat; // BOTTOM
                            });
                        }

                        ret = RectLatLng.FromLTRB(left, top, right, bottom);
                    }
                }
            }

            return ret;
        }

        public static RectLatLng? GetRectOfPointSet(IEnumerable<PointLatLng> points)
        {
            double left = double.MaxValue;
            double top = double.MinValue;
            double right = double.MinValue;
            double bottom = double.MaxValue;

            foreach (PointLatLng p in points)
            {
                // left
                if (p.Lng < left)
                {
                    left = p.Lng;
                }
                // top
                if (p.Lat > top)
                {
                    top = p.Lat;
                }
                // right
                if (p.Lng > right)
                {
                    right = p.Lng;
                }
                // bottom
                if (p.Lat < bottom)
                {
                    bottom = p.Lat;
                }
            }

            return RectLatLng.FromLTRB(left, top, right, bottom);
        }

        /// <summary>
        /// gets image of the current view
        /// </summary>
        /// <returns></returns>
        public Image Screenshot
        {
            get
            {
                Image result = null;
                try
                {
                    using (Bitmap bitmap = new Bitmap(Width, Height))
                    {
                        using (Graphics g = Graphics.FromImage(bitmap))
                        {
                            g.CopyFromScreen(PointToScreen(new Point()).X, PointToScreen(new Point()).Y, 0, 0,
                                new Size(Width, Height));
                            using (Font f = new Font("Arial", 9f))
                            {
                                string s = String.Format("TrackControl : {0:F}", DateTime.Now);
                                Brush shadowBrush = Brushes.Yellow;
                                g.DrawString(s, f, shadowBrush, 7, 22);
                                g.DrawString(s, f, shadowBrush, 7, 21);
                                g.DrawString(s, f, shadowBrush, 8, 21);
                                g.DrawString(s, f, shadowBrush, 9, 21);
                                g.DrawString(s, f, shadowBrush, 9, 22);
                                g.DrawString(s, f, shadowBrush, 9, 23);
                                g.DrawString(s, f, shadowBrush, 8, 23);
                                g.DrawString(s, f, shadowBrush, 7, 23);
                                g.DrawString(s, f, Brushes.Red, 8, 22);
                            }
                        }

                        using (MemoryStream stream = new MemoryStream())
                        {
                            bitmap.Save(stream, ImageFormat.Png);
                            result = Image.FromStream(stream);
                        }
                    }
                }
                catch
                {
                    return null;
                }
                return result;
            }
        }

        public void ReloadZone(IZone zone)
        {
            foreach (GMapZone gz in Zones)
            {
                if (gz.Id == zone.Id)
                {
                    Zones.Remove(gz);
                    break;
                }
            }
            Zones.Add(new GMapZone(zone));
        }

        #region UserControl Events

        // forces to load map
        private bool isLoaded;

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);

            if (!isLoaded)
            {
                isLoaded = true;
                Core.ReloadMap();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                Core.StartSystem();
            }
        }

        bool showZoneToolTip = true;

        protected override void OnPaint(PaintEventArgs e)
        {
            {
                // Рендерим светлый фон
                e.Graphics.Clear(Color.WhiteSmoke);

                // Рендерим карту
                drawMap(e.Graphics);

                // Рисуем контрольные зоны
                foreach (GMapZone zone in Zones)
                {
                    if (IsKeepZonesSelected)
                        zone.Selected = true;
                    zone.Render(e.Graphics, showZoneToolTip);
                }

                // Рендерим объекты на каждом слое
                foreach (GMapOverlay o in Overlays)
                {
                    if (o.IsVisibile && o != ToolLayer)
                    {
                        o.Render(e.Graphics, ref showZoneToolTip);
                    }
                    // слой инструментов рендерим последним для прорисовки всей служебной информации
                    if (ToolLayer != null)
                        ToolLayer.Render(e.Graphics, ref showZoneToolTip);
                }

                #region -- copyright --

                switch (Core.MapType)
                {
                    case MapType.OpenStreetMap:
                    case MapType.OpenStreetOsm:
                    {
                        e.Graphics.DrawString(Core.openStreetMapCopyright, CopyrightFont, Brushes.Navy, 3,
                            Height - CopyrightFont.Height - 5);
                    }
                        break;

                    case MapType.YahooMap:
                    case MapType.YahooSatellite:
                    case MapType.YahooLabels:
                    case MapType.YahooHybrid:
                    {
                        e.Graphics.DrawString(Core.yahooMapCopyright, CopyrightFont, Brushes.Navy, 3,
                            Height - CopyrightFont.Height - 5);
                    }
                        break;

                    case MapType.BingMap:
                    case MapType.BingSatellite:
                    case MapType.BingHybrid:
                    {
                        e.Graphics.DrawString(Core.virtualEarthCopyright, CopyrightFont, Brushes.Navy, 3,
                            Height - CopyrightFont.Height - 5);
                    }
                        break;

                    case MapType.YandexMap:
                    case MapType.YandexSatelliteMap:
                    case MapType.YandexHybridMap:
                    {
                        e.Graphics.DrawString(Core.yandexMapCopyright, CopyrightFont, Brushes.Navy, 3,
                            Height - CopyrightFont.Height - 5);
                    }
                        break;

                    case MapType.GoogleMap:
                    case MapType.GoogleSatellite:
                    case MapType.GoogleLabels:
                    case MapType.GoogleTerrain:
                    case MapType.GoogleHybrid:
                    case MapType.GisFile:
                    default:
                    {
                        e.Graphics.DrawString(Core.googleCopyright, CopyrightFont, Brushes.Navy, 3,
                            Height - CopyrightFont.Height - 5);
                    }
                        break;
                }

                #endregion

                #region -- Рисуем масштабную сетку --

                if (MapScaleInfoEnabled)
                {
                    if (Width > Core.pxRes5000km)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes5000km, 10);
                        e.Graphics.DrawString("5000км", ScaleFont, ScalePen.Brush, Core.pxRes5000km + 10, 11);
                    }
                    if (Width > Core.pxRes1000km)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes1000km, 10);
                        e.Graphics.DrawString("1000км", ScaleFont, ScalePen.Brush, Core.pxRes1000km + 10, 11);
                    }
                    if (Width > Core.pxRes100km && Zoom > 2)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes100km, 10);
                        e.Graphics.DrawString("100км", ScaleFont, ScalePen.Brush, Core.pxRes100km + 10, 11);
                    }
                    if (Width > Core.pxRes10km && Zoom > 5)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes10km, 10);
                        e.Graphics.DrawString("10км", ScaleFont, ScalePen.Brush, Core.pxRes10km + 10, 11);
                    }
                    if (Width > Core.pxRes1000m && Zoom >= 10)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes1000m, 10);
                        e.Graphics.DrawString("1000м", ScaleFont, ScalePen.Brush, Core.pxRes1000m + 10, 11);
                    }
                    if (Width > Core.pxRes100m && Zoom > 11)
                    {
                        e.Graphics.DrawRectangle(ScalePen, 10, 10, Core.pxRes100m, 10);
                        e.Graphics.DrawString("100м", ScaleFont, ScalePen.Brush, Core.pxRes100m + 9, 11);
                    }
                }

                #endregion
            }

            base.OnPaint(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            if (!DesignMode)
            {
                Core.OnMapSizeChanged(Width, Height);

                // 50px outside control
                Core.CurrentRegion = new GRectangle(-50, -50, Size.Width + 100, Size.Height + 100);

                if (Visible && IsHandleCreated)
                {
                    // keep center on same position
                    Core.GoToCurrentPosition();
                    _screenRectChanged = true;
                }
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            eAm = e;
            
            if (!IsMouseOverMarker && !(IsMouseOverZone && IsZoneEditor) && !IsAddingNewZone /* &&
               !(ToolArea is AreaTool) && !(ToolDistance is AreaTool) && !(Tool is AreaTool)*/)
            {
                if (e.Button == DragButton && CanDragMap)
                {
                    Core.mouseDown.X = e.X;
                    Core.mouseDown.Y = e.Y;
                    Cursor = Cursors.SizeAll;
                    Core.BeginDrag(Core.mouseDown);
                    Invalidate(false);
                } // if
            } // if
            
            if (IsZoneEditor || ToolDistance is DistanceTool || ToolArea is AreaTool)
            {
                if (ToolArea is AreaTool)
                {
                    ToolArea.OnMouseDown(this, e);
                    base.OnMouseDown(e);
                    return;
                }
                else if (ToolDistance is DistanceTool)
                {
                    ToolDistance.OnMouseDown(this, e);
                    base.OnMouseDown(e);
                    return;
                }
                else if (Tool != null)
                    Tool.OnMouseDown(this, e);
            } // if

            base.OnMouseDown(e);
        }
        // OnMouseDown

        protected override void OnMouseUp(MouseEventArgs e)
        {
            eAm = e;

            //base.OnMouseUp(e);
            
            if (Core.IsDragging)
            {
                Core.EndDrag();
                Cursor = Cursors.Default;
                flagUsageFromKeys = false;
                flagInitEvent = true;
            }

            if (IsZoneEditor)
            {
                if (ToolDistance is DistanceTool)
                {
                    ToolDistance.OnMouseUp(this, e);
                }
                else if (ToolArea is AreaTool)
                {
                    ToolArea.OnMouseUp(this, e);
                }
                    
                Tool.OnMouseUp(this, e);
            }
            else
            {
                ToolDistance.OnMouseUp(this, e);
            }

            RaiseEmptyTileError = false;

            if (_screenMouseDrug)
            {
                _screenMouseDrug = false;
                _screenRectChanged = true;
            }

            base.OnMouseUp(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            eAm = e;

            if (e.Button == MouseButtons.Left && IsTrackAnalizing)
            {
                for (int i = Overlays.Count - 1; i >= 0; i--)
                {
                    GMapOverlay o = Overlays[i];
                    if (o != null && o.IsVisibile)
                    {
                        for (int j = o.Tracks.Count - 1; j >= 0; j--)
                        {
                            GMapTrack track = o.Tracks[j];
                            if (track.IsActive)
                            {
                                int indexInArray;
                                IGeoPoint igp = track.CheckHit(e.X, e.Y, out indexInArray);
                                int x = GoogleMapControl.xtabControl.Location.X + e.X + Location.X + GoogleMapControl.mainLocation.X + 3;
                                int y = GoogleMapControl.xtabControl.Location.Y + e.Y + Location.Y + GoogleMapControl.mainLocation.Y + 64;
                                Point ptMous = new Point(x, y);
                                OnTrackPointClicked(track, track.Tag as IVehicle, igp == null ? -1 : indexInArray, ptMous);
                            }
                        }
                    }
                }
            }

            if (e.Button == MouseButtons.Right)
            {
                showZoneToolTip = true;
                ShowToolTipZones(e);
            }

            if (e.Button == MouseButtons.Left && !Core.IsDragging)
            {
                for (int i = Overlays.Count - 1; i >= 0; i--)
                {
                    GMapOverlay o = Overlays[i];
                    if (o != null && o.IsVisibile)
                    {
                        for (int j = o.Markers.Count - 1; j >= 0; j--)
                        {
                            GMapMarker m = o.Markers[j];
                            if (m.Visible)
                            {
                                if (m.LocalArea.Contains(e.X, e.Y))
                                {
                                    if (OnMarkerClick != null)
                                    {
                                        OnMarkerClick(m);
                                        Tool.OnMarkerClick(this, m);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            base.OnMouseClick(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            eAm = e;

            if (flagUsageFromKeys && e.Clicks == 20)
            {
            }
            else
            {
                if (!flagUsageFromKeys && e.Clicks < 2)
                {
                }
                else
                {
                    return;
                }
            }

            GPoint gp = new GPoint(e.X, e.Y);
            OnMouseMoveLanLong(gp);

            if (Core.IsDragging)
            {
                Core.mouseCurrent.X = e.X;
                Core.mouseCurrent.Y = e.Y;
                Core.Drag(Core.mouseCurrent);
            }
                //else if (!IsZoneEditor && !(Tool is AreaTool))
            else if (!IsZoneEditor)
            {
                for (int i = Overlays.Count - 1; i >= 0; i--)
                {
                    GMapOverlay o = Overlays[i];
                    if (o != null && o.IsVisibile)
                    {
                        MouseMoveMarkers(e, o);
                    }
                }

                //MouseMoveZones(e); в связи с измененным алгоритмом отображения тултипов на зонах
            }
            else
            {
                if ((ToolArea != null) && (ToolArea is AreaTool))
                {
                    ToolArea.OnMouseMove(this, e);
                    base.OnMouseMove(e);
                    return;
                }
                else if ((ToolDistance != null) && (ToolDistance is DistanceTool))
                {
                    ToolDistance.OnMouseMove(this, e);
                    base.OnMouseMove(e);
                    return;
                }
                else if (Tool != null)
                {
                    Tool.OnMouseMove(this, e);
                }
            } // else

            base.OnMouseMove(e);
        }
        // OnMouseMove

        private void ShowToolTipZones(MouseEventArgs e)
        {
            eAm = e;
            foreach (var gMapZone in Zones)
            {
                if (gMapZone.Contains(FromLocalToLatLng(e.X, e.Y)))
                {
                    gMapZone.IsMouseOver = true & isTooltipShow;
                    Invalidate(false);
                }
            }
        }

        private void MouseMoveZones(MouseEventArgs e)
        {
            eAm = e;

            foreach (var gMapZone in Zones)
            {
                if (gMapZone.Contains(FromLocalToLatLng(e.X, e.Y)))
                {
                    if (!gMapZone.IsMouseOver)
                    {
                        gMapZone.IsMouseOver = true & isTooltipShow;
                        Invalidate(false);
                    }
                }
                else
                {
                    if (gMapZone.IsMouseOver)
                    {
                        gMapZone.IsMouseOver = false & isTooltipShow;
                        Invalidate(false);
                    }
                }
            }
        }

        private void MouseMoveMarkers(MouseEventArgs e, GMapOverlay o)
        {
            eAm = e;

            int jj = 0;
            while (jj < o.Markers.Count)
            {
                GMapMarker m = null;
                lock (o.Markers)
                {
                    if (jj < o.Markers.Count)
                        m = o.Markers[jj];
                }
                if (null != m && m.Visible)
                {
                    if (m.LocalArea.Contains(e.X, e.Y))
                    {
                        Cursor = Cursors.Hand;
                        m.IsMouseOver = true & isTooltipShow;
                        if(isDistanceUse)
                            IsMouseOverMarker = true;
                        Invalidate(false);

                        if (OnMarkerEnter != null)
                        {
                            OnMarkerEnter(m);
                        }
                    }
                    else if (m.IsMouseOver)
                    {
                        Cursor = Cursors.Default;
                        m.IsMouseOver = false & isTooltipShow;
                        IsMouseOverMarker = false;
                        Invalidate(false);

                        if (OnMarkerLeave != null)
                        {
                            OnMarkerLeave(m);
                        }
                    }
                }
                jj++;
            }
        }

        private void MouseMoveZones(MouseEventArgs e, GMapOverlay o)
        {
            eAm = e;

            int jj = 0;
            
            while (jj < o.Markers.Count)
            {
                GMapMarker m = null;
                lock (o.Markers)
                {
                    if (jj < o.Markers.Count)
                        m = o.Markers[jj];
                }
                if (null != m && m.Visible)
                {
                    if (m.LocalArea.Contains(e.X, e.Y))
                    {
                        Cursor = Cursors.Hand;
                        m.IsMouseOver = true;
                        IsMouseOverMarker = true;
                        Invalidate(false);

                        if (OnMarkerEnter != null)
                        {
                            OnMarkerEnter(m);
                        }
                    }
                    else if (m.IsMouseOver)
                    {
                        Cursor = Cursors.Default;
                        m.IsMouseOver = false;
                        IsMouseOverMarker = false;
                        Invalidate(false);

                        if (OnMarkerLeave != null)
                        {
                            OnMarkerLeave(m);
                        }
                    }
                }
                jj++;
            }
        }

        // controling map from keyboard
        private int delta = 10;
        private MouseEventArgs eAm = null;
        private MousePos mP = new MousePos();
        private bool flagInitEvent = true;
        private bool flagUsageFromKeys = false;
        // =====================================
        
        protected override void OnKeyUp(KeyEventArgs e)
        {
            // отменяем оправление картой через клавиатуру
            if (e.KeyCode == Keys.Escape)
            {
                flagUsageFromKeys = false;
                flagInitEvent = true;
                OnMouseUp(eAm);
                return;
            } // if

            if (e.KeyCode != Keys.Left)
            {
                if (e.KeyCode != Keys.Right)
                {
                    if (e.KeyCode != Keys.Down)
                    {
                        if (e.KeyCode != Keys.Up)
                        {
                            return;
                        } // if
                    } // if
                } // if
            } // if
            
            if (eAm == null)
                return;
            
            if (zoneEditorOn)
                return;
           
            if (flagInitEvent)
            {
                MouseEventArgs mEv = new MouseEventArgs(MouseButtons.Left, 1, eAm.X, eAm.Y, eAm.Delta);
                mP.X = eAm.X;
                mP.Y = eAm.Y;
                flagInitEvent = false;
                CanDragMap = true;
                flagUsageFromKeys = true;
                OnMouseDown(mEv);
            } // if

            if (e.KeyCode == Keys.Left)
            {
                mP.X = mP.X - delta;
                mP.Y = mP.Y;
            }
            else if (e.KeyCode == Keys.Right)
            {
                mP.X = mP.X + delta;
                mP.Y = mP.Y;
            }
            else if (e.KeyCode == Keys.Up)
            {
                mP.X = mP.X;
                mP.Y = mP.Y - delta;
            }
            else if (e.KeyCode == Keys.Down)
            {
                mP.X = mP.X;
                mP.Y = mP.Y + delta;
            }

            MouseEventArgs mEa = new MouseEventArgs(MouseButtons.Left, 20, mP.X, mP.Y, eAm.Delta);
            OnMouseMove(mEa);
            base.OnKeyUp(e);
        }
        // OnKeyUp

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (eAm == null)
                return;

            if (e.KeyChar == '+')
            {
                MouseEventArgs mEa = new MouseEventArgs(eAm.Button, eAm.Clicks, eAm.X, eAm.Y, 10);

                //if( flagWheel )
                //{
                //    OnMouseMove( mEa );
                //    OnMouseDown( mEa );
                //    OnMouseUp( mEa );
                //    flagWheel = false;
                //} // if

                OnMouseWheel(mEa);
            } // if
            else if (e.KeyChar == '-')
            {
                MouseEventArgs mEa = new MouseEventArgs(eAm.Button, eAm.Clicks, eAm.X, eAm.Y, -10);
                
                //if( flagWheel )
                //{
                //    OnMouseMove( mEa );
                //    OnMouseDown( mEa );
                //    OnMouseUp( mEa );
                //    flagWheel = false;
                //} // if

                OnMouseWheel(mEa);
            } // else if
           
            base.OnKeyPress(e);
        }
        // OnKeyPress

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            eAm = e;

            base.OnMouseWheel(e);

            if (!IsMouseOverMarker && !IsDragging)
            {
                if (MouseWheelZoomType == MouseWheelZoomType.MousePositionAndCenter)
                {
                    keyMoveStarter = true;
                    Core.currentPosition = FromLocalToLatLng(e.X, e.Y);
                }
                else if (MouseWheelZoomType == MouseWheelZoomType.ViewCenter)
                {
                    Core.currentPosition = FromLocalToLatLng((int)Width / 2, (int)Height / 2);
                }

                // set mouse position to map center
                if (CenterPositionOnMouseWheel)
                {
                    Point p = PointToScreen(new Point(Width / 2, Height / 2));
                    Stuff.SetCursorPos((int)p.X, (int)p.Y);
                }

                if (e.Delta > 0)
                {
                    if (MapType == MapType.GisFile)
                    {
                        int _zoom = Zoom;
                        _zoom++;
                        if (_zoom < 16)
                        {
                            Zoom++;
                        }
                        else
                        {
                            XtraMessageBox.Show("Для текущей карты достигнуто максимальное увеличение", "Zoom Map",
                                MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                    else
                    {
                        Zoom++;
                    }
                }
                else if (e.Delta < 0)
                {
                    Zoom--;
                }
            } // if

            _screenRectChanged = true;
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);

            MouseEventArgs args = (MouseEventArgs)e;
            eAm = args;

            keyMoveStarter = true;

            if (!IsMouseOverMarker)
            {
                Core.currentPosition = FromLocalToLatLng(args.X, args.Y);
                Zoom++;
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            eAm = e;

            base.OnMouseDoubleClick(e);
            //точка для прокладки маршрутов
            GPoint gp = new GPoint(e.X, e.Y);
            OnMouseClickLanLong(gp);
            
            keyMoveStarter = true;
        }

        #endregion

        internal void UnselectZones()
        {
            foreach (GMapZone z in Zones)
                z.Selected = false;
        }

        internal void OnZoneSelected(IZone zone)
        {
            Action<IZone> handler = ZoneSelected;
            if (null != handler)
                handler(zone);
        }

        internal void OnNewZoneAdded(IList<PointLatLng> points)
        {
            GeometryChangedHandler handler = NewZoneAdded;
            if (null != handler)
                handler(-1, points);
        }

        /// <summary>
        /// gets world coordinate from local control coordinate 
        /// </summary>
        public PointLatLng FromLocalToLatLng(int x, int y)
        {
            return Core.FromLocalToLatLng(x, y);
        }

        /// <summary>
        /// gets local coordinate from world coordinate
        /// </summary>
        public GPoint FromLatLngToLocal(PointLatLng point)
        {
            return Core.FromLatLngToLocal(point);
        }

        /// <summary>
        /// shows map db export dialog
        /// </summary>
        /// <returns></returns>
        public bool ShowExportDialog()
        {
            using (FileDialog dlg = new SaveFileDialog())
            {
                dlg.CheckPathExists = true;
                dlg.CheckFileExists = false;
                dlg.AddExtension = true;
                dlg.DefaultExt = "gmdb";
                dlg.ValidateNames = true;
                dlg.Title = "GMap.NET: Export map to db, if file exsist only new data will be added";
                dlg.FileName = "DataExp";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dlg.Filter = "GMap.NET DB files (*.gmdb)|*.gmdb";
                dlg.FilterIndex = 1;
                dlg.RestoreDirectory = true;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    bool ok = GMaps.ExportToGMDB(dlg.FileName);
                    if (ok)
                    {
                        MessageBox.Show("Complete!", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("  Failed!", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    return ok;
                }
            }
            return false;
        }

        /// <summary>
        /// shows map dbimport dialog
        /// </summary>
        /// <returns></returns>
        public bool ShowImportDialog()
        {
            using (FileDialog dlg = new OpenFileDialog())
            {
                dlg.CheckPathExists = true;
                dlg.CheckFileExists = false;
                dlg.AddExtension = true;
                dlg.DefaultExt = "gmdb";
                dlg.ValidateNames = true;
                dlg.Title = "GMap.NET: Import to db, only new data will be added";
                dlg.FileName = "DataExp";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dlg.Filter = "GMap.NET DB files (*.gmdb)|*.gmdb";
                dlg.FilterIndex = 1;
                dlg.RestoreDirectory = true;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    bool ok = GMaps.ImportFromGMDB(dlg.FileName);
                    if (ok)
                    {
                        MessageBox.Show("Complete!", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("  Failed!", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    return ok;
                }
            }
            return false;
        }

        /// <summary>
        /// map zoom level
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Zoom
        {
            get
            {
                return Core.Zoom;
            }
            set
            {
                if (value > Globals.GMAP_MAX)
                {
                    Core.Zoom = Globals.GMAP_MAX;
                }
                else if (value < Globals.GMAP_MIN)
                {
                    Core.Zoom = Globals.GMAP_MIN;
                }
                else
                {
                    Core.Zoom = value;
                }
            }
        }

        /// <summary>
        /// current map center position
        /// </summary>
        public PointLatLng CurrentPosition
        {
            get
            {
                return Core.CurrentPosition;
            }
            set
            {
                Core.CurrentPosition = value;
            }
        }

        /// <summary>
        /// current marker position in pixel coordinates
        /// </summary>
        public GPoint CurrentPositionGPixel
        {
            get
            {
                return Core.CurrentPositionGPixel;
            }
        }

        /// <summary>
        /// location of cache
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string CacheLocation
        {
            get
            {
                return Cache.Instance.CacheLocation;
            }
            set
            {
                Cache.Instance.CacheLocation = value;
            }
        }

        /// <summary>
        /// is user dragging map
        /// </summary>
        public bool IsDragging
        {
            get
            {
                return Core.IsDragging;
            }
        }

        /// <summary>
        /// is mouse over marker
        /// </summary>
        public bool IsMouseOverMarker;

        public bool IsMouseOverZone;

        private bool isMouseOverRoute;

        /// <summary>
        /// is mouse over route
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool IsMouseOverRoute
        {
            get
            {
                return isMouseOverRoute;
            }
            internal set
            {
                isMouseOverRoute = value;
            }
        }

        private bool isMouseOverPolygon;

        /// <summary>
        /// is mouse over polygon
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool IsMouseOverPolygon
        {
            get
            {
                return isMouseOverPolygon;
            }
            internal set
            {
                isMouseOverPolygon = value;
            }
        }

        /// <summary>
        /// gets current map view top/left coordinate, width in Lng, height in Lat
        /// </summary>
        public RectLatLng CurrentViewArea
        {
            get
            {
                return Core.CurrentViewArea;
            }
        }

        /// <summary>
        /// for tooltip text padding
        /// </summary>
        public GSize TooltipTextPadding
        {
            get
            {
                return Core.TooltipTextPadding;
            }
            set
            {
                Core.TooltipTextPadding = value;
            }
        }

        /// <summary>
        /// type of map
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public MapType MapType
        {
            get
            {
                return Core.MapType;
            }
            set
            {
                Core.MapType = value;
            }
        }

        /// <summary>
        /// map projection
        /// </summary>
        public PureProjection Projection
        {
            get
            {
                return Core.Projection;
            }
        }

        /// <summary>
        /// can user drag map
        /// </summary>
        public bool CanDragMap
        {
            get
            {
                return Core.CanDragMap;
            }
            set
            {
                Core.CanDragMap = value;
            }
        }

        /// <summary>
        /// occurs when current position is changed
        /// </summary>
        public event CurrentPositionChanged OnCurrentPositionChanged
        {
            add
            {
                Core.OnCurrentPositionChanged += value;
            }
            remove
            {
                Core.OnCurrentPositionChanged -= value;
            }
        }

        /// <summary>
        /// occurs when tile set load is complete
        /// </summary>
        public event TileLoadComplete OnTileLoadComplete
        {
            add
            {
                Core.OnTileLoadComplete += value;
            }
            remove
            {
                Core.OnTileLoadComplete -= value;
            }
        }

        /// <summary>
        /// occurs when tile set is starting to load
        /// </summary>
        public event TileLoadStart OnTileLoadStart
        {
            add
            {
                Core.OnTileLoadStart += value;
            }
            remove
            {
                Core.OnTileLoadStart -= value;
            }
        }

        /// <summary>
        /// occurs on map drag
        /// </summary>
        public event MapDrag OnMapDrag
        {
            add
            {
                Core.OnMapDrag += value;
            }
            remove
            {
                Core.OnMapDrag -= value;
            }
        }

        /// <summary>
        /// occurs on map zoom changed
        /// </summary>
        public event MapZoomChanged OnMapZoomChanged
        {
            add
            {
                Core.OnMapZoomChanged += value;
            }
            remove
            {
                Core.OnMapZoomChanged -= value;
            }
        }

        /// <summary>
        /// occures on map type changed
        /// </summary>
        public event MapTypeChanged OnMapTypeChanged
        {
            add
            {
                Core.OnMapTypeChanged += value;
            }
            remove
            {
                Core.OnMapTypeChanged -= value;
            }
        }

        /// <summary>
        /// stops immediate marker/route/polygon invalidations;
        /// call Refresh to perform single refresh and reset invalidation state
        /// </summary>
        public bool HoldInvalidation = false;

        /// <summary>
        /// call this to stop HoldInvalidation and perform single forced instant refresh 
        /// </summary>
        public override void Refresh()
        {
            HoldInvalidation = false;

            lock (Core.invalidationLock)
            {
                Core.lastInvalidation = DateTime.Now;
            }

            base.Refresh();
        }

        private Cursor cursorBefore = Cursors.Default;

        internal void RestoreCursorOnLeave()
        {
            if (cursorBefore != null)
            {
                this.Cursor = this.cursorBefore;
                cursorBefore = null;
            }
        }

        /// <summary>
        /// is polygons enabled
        /// </summary>
        [Category("GMap.NET")]
        public bool PolygonsEnabled
        {
            get
            {
                return Core.PolygonsEnabled;
            }
            set
            {
                Core.PolygonsEnabled = value;
            }
        }

        public bool isTooltipShow { get; set; }
        public bool isDistanceUse { get; set; }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // GMapControl
            // 
            this.Name = "GMapControl";
            this.Size = new System.Drawing.Size(329, 253);
            this.ResumeLayout(false);
        }
    }
}