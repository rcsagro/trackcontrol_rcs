namespace TrackControl.GMap
{
  partial class GoogleMapControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoogleMapControl));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._mapTools = new DevExpress.XtraBars.Bar();
            this._mapTypeCombo = new DevExpress.XtraBars.BarEditItem();
            this._mapTypeRepository = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._zoomItem = new DevExpress.XtraBars.BarEditItem();
            this._zoomRepository = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.bchDistance = new DevExpress.XtraBars.BarCheckItem();
            this.bchArea = new DevExpress.XtraBars.BarCheckItem();
            this._screenshotBtn = new DevExpress.XtraBars.BarButtonItem();
            this.beiPosition = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this._map = new TrackControl.GMap.UI.GMapControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.panelVideo = new System.Windows.Forms.Panel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.buttonResume = new DevExpress.XtraEditors.SimpleButton();
            this.buttonStart = new DevExpress.XtraEditors.SimpleButton();
            this.trackVideo = new DevExpress.XtraEditors.TrackBarControl();
            this.trackBarSpeed = new DevExpress.XtraEditors.TrackBarControl();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._mapTypeRepository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._zoomRepository)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.panelVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVideo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._mapTools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._screenshotBtn,
            this._zoomItem,
            this._mapTypeCombo,
            this.beiPosition,
            this.bchDistance,
            this.bchArea});
            this._barManager.MaxItemId = 8;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._zoomRepository,
            this._mapTypeRepository,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemRadioGroup1});
            // 
            // _mapTools
            // 
            this._mapTools.BarName = "Tools";
            this._mapTools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._mapTools.DockCol = 0;
            this._mapTools.DockRow = 0;
            this._mapTools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._mapTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._mapTypeCombo, "", true, true, true, 142),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._zoomItem, "", false, true, true, 158),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bchDistance, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchArea),
            new DevExpress.XtraBars.LinkPersistInfo(this._screenshotBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiPosition, "", false, true, true, 158)});
            this._mapTools.OptionsBar.AllowQuickCustomization = false;
            this._mapTools.OptionsBar.DisableClose = true;
            this._mapTools.OptionsBar.DisableCustomization = true;
            this._mapTools.OptionsBar.DrawDragBorder = false;
            this._mapTools.OptionsBar.UseWholeRow = true;
            this._mapTools.Text = "Tools";
            // 
            // _mapTypeCombo
            // 
            this._mapTypeCombo.Caption = "���:";
            this._mapTypeCombo.Edit = this._mapTypeRepository;
            this._mapTypeCombo.Id = 2;
            this._mapTypeCombo.Name = "_mapTypeCombo";
            this._mapTypeCombo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // _mapTypeRepository
            // 
            this._mapTypeRepository.AllowFocused = false;
            this._mapTypeRepository.AutoHeight = false;
            this._mapTypeRepository.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._mapTypeRepository.Name = "_mapTypeRepository";
            // 
            // _zoomItem
            // 
            this._zoomItem.Caption = "�������:";
            this._zoomItem.Edit = this._zoomRepository;
            this._zoomItem.EditValue = 7;
            this._zoomItem.Id = 1;
            this._zoomItem.Name = "_zoomItem";
            this._zoomItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // _zoomRepository
            // 
            this._zoomRepository.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this._zoomRepository.AppearanceFocused.Options.UseBackColor = true;
            this._zoomRepository.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._zoomRepository.Middle = 5;
            this._zoomRepository.Name = "_zoomRepository";
            this._zoomRepository.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            // 
            // bchDistance
            // 
            this.bchDistance.AllowAllUp = true;
            this.bchDistance.Glyph = ((System.Drawing.Image)(resources.GetObject("bchDistance.Glyph")));
            this.bchDistance.Id = 4;
            this.bchDistance.Name = "bchDistance";
            this.bchDistance.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchDistance_CheckedChanged);
            // 
            // bchArea
            // 
            this.bchArea.Glyph = ((System.Drawing.Image)(resources.GetObject("bchArea.Glyph")));
            this.bchArea.Id = 5;
            this.bchArea.Name = "bchArea";
            this.bchArea.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bchArea_CheckedChanged);
            // 
            // _screenshotBtn
            // 
            this._screenshotBtn.Caption = "Screenshot";
            this._screenshotBtn.Hint = "������ ������� �����";
            this._screenshotBtn.Id = 0;
            this._screenshotBtn.Name = "_screenshotBtn";
            this._screenshotBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this._screenshotBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._screenshotBtn_ItemClick);
            // 
            // beiPosition
            // 
            this.beiPosition.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiPosition.Caption = "barEditItem1";
            this.beiPosition.Edit = this.repositoryItemTextEdit1;
            this.beiPosition.EditValue = "Lan = 0; Long =0;";
            this.beiPosition.Id = 3;
            this.beiPosition.Name = "beiPosition";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(741, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 466);
            this.barDockControlBottom.Size = new System.Drawing.Size(741, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 435);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(741, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 435);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "1"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "2")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // _map
            // 
            this._map.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._map.CanDragMap = true;
            this._map.Dock = System.Windows.Forms.DockStyle.Fill;
            this._map.isDistanceUse = true;
            this._map.IsKeepZonesSelected = false;
            this._map.isTooltipShow = true;
            this._map.Location = new System.Drawing.Point(0, 31);
            this._map.Name = "_map";
            this._map.PolygonsEnabled = true;
            this._map.ScreenRectChanged = false;
            this._map.Size = new System.Drawing.Size(741, 435);
            this._map.TabIndex = 4;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "media-player--arrow.png");
            this.imageCollection1.Images.SetKeyName(1, "camcorder.png");
            this.imageCollection1.Images.SetKeyName(2, "arrow-000-medium.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow-circle-045-left.png");
            this.imageCollection1.Images.SetKeyName(4, "control.png");
            this.imageCollection1.Images.SetKeyName(5, "control-pause.png");
            // 
            // panelVideo
            // 
            this.panelVideo.Controls.Add(this.labelControl2);
            this.panelVideo.Controls.Add(this.labelControl1);
            this.panelVideo.Controls.Add(this.buttonResume);
            this.panelVideo.Controls.Add(this.buttonStart);
            this.panelVideo.Controls.Add(this.trackVideo);
            this.panelVideo.Controls.Add(this.trackBarSpeed);
            this.panelVideo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelVideo.Location = new System.Drawing.Point(0, 435);
            this.panelVideo.MaximumSize = new System.Drawing.Size(741, 31);
            this.panelVideo.Name = "panelVideo";
            this.panelVideo.Size = new System.Drawing.Size(741, 31);
            this.panelVideo.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(507, 8);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 9;
            this.labelControl2.Text = "��������:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(63, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(29, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "����:";
            // 
            // buttonResume
            // 
            this.buttonResume.Enabled = false;
            this.buttonResume.ImageIndex = 3;
            this.buttonResume.ImageList = this.imageCollection1;
            this.buttonResume.Location = new System.Drawing.Point(31, 3);
            this.buttonResume.Name = "buttonResume";
            this.buttonResume.Size = new System.Drawing.Size(26, 25);
            this.buttonResume.TabIndex = 5;
            this.buttonResume.ToolTip = "������";
            this.buttonResume.Click += new System.EventHandler(this.buttonResume_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.ImageIndex = 4;
            this.buttonStart.ImageList = this.imageCollection1;
            this.buttonStart.Location = new System.Drawing.Point(3, 3);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(22, 25);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.Text = "�����";
            this.buttonStart.ToolTip = "�����";
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // trackVideo
            // 
            this.trackVideo.EditValue = null;
            this.trackVideo.Location = new System.Drawing.Point(98, 3);
            this.trackVideo.Name = "trackVideo";
            this.trackVideo.Properties.AutoSize = false;
            this.trackVideo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.trackVideo.Properties.ReadOnly = true;
            this.trackVideo.Properties.ShowValueToolTip = true;
            this.trackVideo.Size = new System.Drawing.Size(403, 25);
            this.trackVideo.TabIndex = 2;
            // 
            // trackBarSpeed
            // 
            this.trackBarSpeed.EditValue = 1;
            this.trackBarSpeed.Location = new System.Drawing.Point(565, 3);
            this.trackBarSpeed.Name = "trackBarSpeed";
            this.trackBarSpeed.Properties.AutoSize = false;
            this.trackBarSpeed.Properties.Maximum = 50;
            this.trackBarSpeed.Properties.Minimum = 1;
            this.trackBarSpeed.Size = new System.Drawing.Size(170, 25);
            this.trackBarSpeed.TabIndex = 1;
            this.trackBarSpeed.Value = 1;
            // 
            // GoogleMapControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelVideo);
            this.Controls.Add(this._map);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "GoogleMapControl";
            this.Size = new System.Drawing.Size(741, 466);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._mapTypeRepository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._zoomRepository)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.panelVideo.ResumeLayout(false);
            this.panelVideo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackVideo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeed)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _mapTools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarButtonItem _screenshotBtn;
    private TrackControl.GMap.UI.GMapControl _map;
    private DevExpress.XtraBars.BarEditItem _zoomItem;
    private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar _zoomRepository;
    private DevExpress.XtraBars.BarEditItem _mapTypeCombo;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _mapTypeRepository;
    private DevExpress.XtraBars.BarEditItem beiPosition;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraBars.BarCheckItem bchDistance;
    private DevExpress.XtraBars.BarCheckItem bchArea;
    private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
    private DevExpress.Utils.ImageCollection imageCollection1;
    private System.Windows.Forms.Panel panelVideo;
    private DevExpress.XtraEditors.TrackBarControl trackVideo;
    private DevExpress.XtraEditors.TrackBarControl trackBarSpeed;
    private DevExpress.XtraEditors.SimpleButton buttonStart;
    private DevExpress.XtraEditors.SimpleButton buttonResume;
    private DevExpress.XtraEditors.LabelControl labelControl2;
    private DevExpress.XtraEditors.LabelControl labelControl1;
  }
}
