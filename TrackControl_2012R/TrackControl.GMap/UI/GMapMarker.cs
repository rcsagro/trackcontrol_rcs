﻿using System;
using System.Drawing;
using System.ComponentModel;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Markers;

namespace TrackControl.GMap.UI
{
    /// <summary>
    /// GMap.NET marker
    /// </summary>
    public abstract class GMapMarker : INotifyPropertyChanged
    {
        // Шрифт подписи к маркеру
        private static readonly Font LABEL_FONT = new Font("Tahoma", 7f, FontStyle.Bold);

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        internal GMapOverlay Overlay;

        private PointLatLng position;

        public PointLatLng Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                if (Overlay != null)
                {
                    GPoint p = Overlay.Control.FromLatLngToLocal(value);
                    LocalPosition = new GPoint(p.X + Offset.X, p.Y + Offset.Y);
                }
            }
        }

        public object Tag;

        private GPoint offset;

        public GPoint Offset
        {
            get
            {
                return offset;
            }
            set
            {
                offset = value;
            }
        }

        internal GRectangle area;

        public GPoint LocalPosition
        {
            get
            {
                return area.Location;
            }
            internal set
            {
                area.Location = value;
                OnPropertyChanged("LocalPosition");

                if (Overlay != null)
                {
                    Overlay.Control.Core_OnNeedInvalidation();
                }
            }
        }

        public GSize Size
        {
            get
            {
                return area.Size;
            }
            set
            {
                area.Size = value;
            }
        }

        protected internal virtual GRectangle LocalArea
        {
            get
            {
                GRectangle ret = area;
                ret.Offset(-Size.Width/2, -Size.Height);
                return ret;
            }
        }

        public MarkerTooltipMode TooltipMode;
        public GPoint ToolTipOffset;
        public string ToolTipText;
        public string ToolTipTextDetal;
        public bool Visible;

        private bool isDragging;

        public bool IsDragging
        {
            get
            {
                return isDragging;
            }
            internal set
            {
                isDragging = value;
            }
        }

        private bool isMouseOver;

        public bool IsMouseOver
        {
            get
            {
                return isMouseOver;
            }
            internal set
            {
                isMouseOver = value;
            }
        }

        public GMapMarker(PointLatLng pos)
        {
            Position = pos;
            IsDragging = false;
            ToolTipText = string.Empty;
            TooltipMode = MarkerTooltipMode.OnMouseOver;
            Visible = true;
            IsMouseOver = false;
            ToolTipOffset = new GPoint(14, -44);
        }

        public abstract void OnRender(Graphics g);

        /// <summary>
        /// Рисует подпись к маркеру
        /// </summary>
        /// <param name="g">Графический контекст</param>
        public static void DrawLabel(Graphics g, string label, Brush wrapBrush, Brush stringBrush, int x, int y)
        {
            g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y - 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x, y - 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y - 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y + 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x, y + 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y + 1, StringFormat.GenericTypographic);
            g.DrawString(label, LABEL_FONT, stringBrush, x, y, StringFormat.GenericTypographic);
        }

        public bool IsDrawn { get; set; }
    }
}