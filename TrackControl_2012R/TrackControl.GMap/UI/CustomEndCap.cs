using System;
using System.Drawing;

namespace TrackControl.GMap.UI
{
  public class CustomEndCap
  {
    public PointF[] Points
    {
      get { return _points; }
    }
    PointF[] _points;

    public CustomEndCap(float sizeEndCap)
    {
      PointF p_1 = new PointF(0, 0);
      PointF p_2 = new PointF(-1 * sizeEndCap / 3, -1 * (sizeEndCap + 0));
      PointF p_3 = new PointF(0, -1 * sizeEndCap);
      PointF p_4 = new PointF(sizeEndCap / 3, -1 * (sizeEndCap + 0));
      _points = new PointF[]{ p_1, p_2, p_3, p_4, p_1 };
    }
  }
}
