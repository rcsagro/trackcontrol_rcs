using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using TrackControl.GMap.Core;
using TrackControl.General;

namespace TrackControl.GMap.UI
{
  public class GMapTrack
  {
    public int Id
    {
      get { return _id; }
    }
    int _id;

    public List<IGeoPoint> GeoPoints
    {
      get { return _geoPoints; }
    }
    List<IGeoPoint> _geoPoints;

    internal List<GPoint> LocalPoints;
    Dictionary<int, GPoint> _drawnPoints;

    public Color Color
    {
      get { return _color; }
      set { _color = value; }
    }
    Color _color;

    /// <summary>
    /// ������������� �������, ������������ ��� ����� �����
    /// </summary>
    public RectLatLng Bounds
    {
        get { return _bounds; }
        set { _bounds = value; }
    }
    RectLatLng _bounds;

    /// <summary>
    /// ������ ���������� �������� � ������ - �������������� ��������� ��������� ���� ("������� ����")
    /// </summary>
    double _widthAgregatMeter;
    public double WidthAgregatMeter
    {
        get { return _widthAgregatMeter; }
        set { _widthAgregatMeter = value; }
    }

    /// <summary>
    /// ���� �������������� ��������� ��������� ���� ("������� ����")
    /// </summary>
    Color _colorAgregat;
    public Color ColorAgregat
    {
        get { return _colorAgregat; }
        set { _colorAgregat = value; }
    }

    public object Tag;

    public bool IsActive
    {
      get { return _isActive; }
      set { _isActive = value; }
    }
    bool _isActive;

    static CustomEndCap _endCap = new CustomEndCap(4f);

    public GMapTrack(int id, IList<IGeoPoint> geoPoints)
    {
      _id = id;
      _geoPoints = new List<IGeoPoint>(geoPoints);
      _color = Color.FromArgb(140, Color.MidnightBlue);
      LocalPoints = new List<GPoint>(geoPoints.Count);
      _drawnPoints = new Dictionary<int, GPoint>();
    }

    public void DrawSimple(Graphics g)
    {
        GraphicsState st = g.Save();
      g.SmoothingMode = SmoothingMode.AntiAlias;
      using (GraphicsPath endLinePath = new GraphicsPath())
      {
        endLinePath.AddLines(_endCap.Points);
        using (Pen endLinePen = new Pen(_color, 1F))
        {
          endLinePen.CustomEndCap = new CustomLineCap(endLinePath, null);
          using (Pen pen = new Pen(_color, 1F))
          {
            double d1 = 0;
            double d2 = 0;
            GPoint last = LocalPoints[0];
            _drawnPoints.Clear();
            _drawnPoints.Add(0, last);
            for (int i = 0; i < LocalPoints.Count; i++)
            {
              GPoint current = LocalPoints[i];
              d1 = last.Distanse(current);
              if (d1 > 10)
              {
                d2 += d1;
                if (d2 > 50)
                {
                  g.DrawLine(endLinePen, last.X, last.Y, current.X, current.Y);
                  d2 = 0;
                }
                else
                {
                  g.DrawLine(pen, last.X, last.Y, current.X, current.Y);
                }
                last = current;
                _drawnPoints.Add(i, last);
              }
            }
            int lastIndex = LocalPoints.Count - 1;
            if (!_drawnPoints.ContainsKey(lastIndex))
              _drawnPoints.Add(lastIndex, LocalPoints[lastIndex]);
          }
        }
      }
      g.Restore(st);
    }

    public void DrawApexes(Graphics g)
    {
      using (Pen pen = new Pen(_color, 1F))
      {
        for (int i = 0; i < LocalPoints.Count; i++)
        {
          if (_drawnPoints.ContainsKey(i))
          {
            GPoint point = _drawnPoints[i];
            RectangleF rect = new RectangleF(point.X - 2, point.Y - 2, 5, 5);
            g.FillEllipse(Brushes.White, rect);
            g.DrawEllipse(pen, rect);
            g.DrawRectangle(pen, point.X, point.Y, 1, 1);
          }
        }
      }
    }

    public void DrawRoute(Graphics g)
    {
        GraphicsState st = g.Save();
        g.SmoothingMode = SmoothingMode.AntiAlias;
        Pen pEnd = new Pen(Color.FromArgb(120, Color.White.R, Color.White.G, Color.White.B), 5);
        Pen pCenter = new Pen(Color.FromArgb(120, Color.Red.R, Color.Red.G, Color.Red.B), 3);
        GraphicsPath path = new GraphicsPath();
        for (int i = 1; i < LocalPoints.Count; i++)
        {
            path.AddLine(LocalPoints[i - 1].X, LocalPoints[i - 1].Y, LocalPoints[i - 1].X, LocalPoints[i - 1].Y);
        }
        g.DrawPath(pEnd, path);
        g.DrawPath(pCenter, path);
    }

    public void DrawThick(Graphics g)
    {
        if (_widthAgregatMeter > 0)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            float widthInPixelAgregat = GetAgregatWidthInPixels(_widthAgregatMeter);
            Pen pThick = new Pen(_colorAgregat, widthInPixelAgregat);
            GraphicsPath path = new GraphicsPath();
            for (int i = 1; i < LocalPoints.Count; i++)
            {
                path.AddLine(LocalPoints[i-1].X, LocalPoints[i-1].Y,LocalPoints[i-1].X, LocalPoints[i-1].Y);
            }
            g.DrawPath(pThick, path);
        }
        DrawSimple(g);
    }

    public void DrawDiffColors(Graphics g, IDictionary<float, Color> valueCodes)
    {
      GraphicsState st = g.Save();
      g.SmoothingMode = SmoothingMode.AntiAlias;
      using (GraphicsPath endLinePath = new GraphicsPath())
      {
        endLinePath.AddLines(_endCap.Points);
        using (Pen endLinePen = new Pen(_color, 1F))
        {
          endLinePen.CustomEndCap = new CustomLineCap(endLinePath, null);
          using (Pen pen = new Pen(_color, 1F))
          {
            double d1 = 0;
            double d2 = 0;
            GPoint last = LocalPoints[0];
            _drawnPoints.Clear();
            _drawnPoints.Add(0, last);
            for (int i = 0; i < LocalPoints.Count; i++)
            {
              GPoint current = LocalPoints[i];
              d1 = last.Distanse(current);
              if (d1 > 10)
              {
                foreach (float value in valueCodes.Keys)
                {
                  if (GeoPoints[i].Speed < value)
                  {
                    endLinePen.Color = valueCodes[value];
                    pen.Color = valueCodes[value];
                    break;
                  }
                }

                d2 += d1;
                if (d2 > 50)
                {
                  g.DrawLine(endLinePen, last.X, last.Y, current.X, current.Y);
                  d2 = 0;
                }
                else
                {
                  g.DrawLine(pen, last.X, last.Y, current.X, current.Y);
                }
                last = current;
                _drawnPoints.Add(i, last);
              }
            }
            int lastIndex = LocalPoints.Count - 1;
            if (!_drawnPoints.ContainsKey(lastIndex))
              _drawnPoints.Add(lastIndex, LocalPoints[lastIndex]);
          }
        }
      }
      g.Restore(st);
    }

    public IGeoPoint CheckHit(int x, int y, out int indexInArray)
    {
        foreach (int i in _drawnPoints.Keys)
        {
            if (Math.Abs(_drawnPoints[i].X - x) < 3 && Math.Abs(_drawnPoints[i].Y - y) < 3)
            {
                indexInArray = i;
                return GeoPoints[i];
            }
        }
        indexInArray = 0;
        return null;
    }

    private float GetAgregatWidthInPixels(double widthInMeter)
    {
        int cntPoints = 0;
        double metrPixel = 0;
        //--------------------------------------------------------------------
        for (int j = 1; j < _geoPoints.Count; j++)
        {
            //������ � �������� ��� ���������� �������� �����
            double dist = 1000 * �GeoDistance.CalculateFromGrad(_geoPoints[j - 1].LatLng.Lat, _geoPoints[j - 1].LatLng.Lng, _geoPoints[j].LatLng.Lat, _geoPoints[j].LatLng.Lng);
            if (dist > 0)
            {
                double metrPixelLoc = (Math.Pow(Math.Pow((LocalPoints[j].X - LocalPoints[j - 1].X), 2) + Math.Pow((LocalPoints[j].Y - LocalPoints[j - 1].Y), 2), 0.5)) / dist;
                if (metrPixelLoc > 0)
                {
                    metrPixel += metrPixelLoc;
                    cntPoints++;
                }
            }
        }
        return (float)(metrPixel * widthInMeter / cntPoints);
    }
  }
}
