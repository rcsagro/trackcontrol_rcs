using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraTab;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.UI;
using TrackControl.GMap.Tools;
using TrackControl.GMap.Core;
using TrackControl.GMap.Markers;
using TrackControl.GMap.Properties;
using TrackControl.Vehicles;
using TrackControl.Zones;
using System.ComponentModel;
using System.Data;
using System.Text;
using LocalCache;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraGrid;
using System.Threading;
using System.Net;
using System.IO;
using System.Xml;
using DevExpress.Utils;
using DevExpress.XtraSplashScreen;
using TrackControl.GMap.Internals;

namespace TrackControl.GMap
{
    public partial class GoogleMapControl : XtraUserControl, IMap, IZonesEditorMap
    {
        static bool flagStoping; // ���� ��� ��������� ������ ����� �� �����

        public class WorkThread
        {
            public delegate void TrackSegmentsShowNeededWith( IList<Track> segment );
            static public TrackSegmentsShowNeededWith trSegmentsShowNeededWith;
            public delegate void MarkersShowNeededWith( IList<Marker> mark );
            static public MarkersShowNeededWith mrkrShowNeededWith;

            public delegate void OnClearMapObjectsNeeded();
            static public OnClearMapObjectsNeeded clrMapObjectsNeeded;

            class Sectors
            {
                public double sector1 = 22.5;
                public double sector2 = 67.5;
                public double sector3 = 112.5;
                public double sector4 = 157.5;
                public double sector5 = 202.5;
                public double sector6 = 247.5;
                public double sector7 = 292.5;
                public double sector8 = 337.5;
            }

            class TStops
            {
                public TStops( int i )
                {
                    this.iBegin = i;
                }

                public int iBegin; // ������ ������ ����� �������
                public int Count; // ����� ����� �������
            }

            private volatile bool shouldStop = false;
            List<Marker> markers;
            //List<Marker> marker_stop;
            List<Track> segments;
            List<Track> segments1;
            List<int> moveline;
            List<TStops> analize_stops;
            List<IGeoPoint> data;
            atlantaDataSet.mobitelsRow cur_row;
            GMapControl gmap;
            DevExpress.XtraEditors.TrackBarControl trackVid;
            DevExpress.XtraEditors.TrackBarControl trackSpd;
            DevExpress.Utils.ImageCollection images;
            int ValueSpeedVideo = 1;
            int cycleWork = 500; // � ����
            PointLatLng latlng0;
            PointLatLng location0;
            PointLatLng latlng1;
            PointLatLng location1;
            Marker mrk;
            Marker mrks;
            DevExpress.XtraEditors.SimpleButton buttonStateVid;
            int iCount = 0;
            int prevCount = 0;
            int iNdex = 0;
            int prevIndex = 0;
            bool flagState = false;
            //int flagIsStop = 0;
            MarkerType trackMarker;
            Sectors sectors = new Sectors();
            double radian = 180.0 / Math.PI;
            const int NOT_MOVE = 1;
            const int MOVE_YES = 0;
            
            public WorkThread( atlantaDataSet.mobitelsRow cur_row, List<Track> segments, GMapControl map,
                DevExpress.XtraEditors.TrackBarControl track, DevExpress.XtraEditors.TrackBarControl trackspd,
                DevExpress.XtraEditors.SimpleButton buttonStateVid, DevExpress.Utils.ImageCollection images )
            {
                this.gmap = map;
                this.cur_row = cur_row;
                this.segments = segments;
                this.segments1 = new List<Track>();
                this.markers = new List<Marker>();
                // this.marker_stop = new List<Marker>();
                this.data = new List<IGeoPoint>();
                this.moveline = new List<int>();
                this.analize_stops = new List<TStops>();
                this.trackVid = track;
                this.trackSpd = trackspd;
                this.images = images;
                this.buttonStateVid = buttonStateVid;
                this.trackMarker = MarkerType.ArrowVidStop; 

                latlng0 = segments[0].GeoPoints[iCount].LatLng;
                location0 = new PointLatLng( latlng0.Lat, latlng0.Lng );
                latlng1 = segments[0].GeoPoints[segments[0].GeoPoints.Count - 1].LatLng;
                location1 = new PointLatLng( latlng1.Lat, latlng1.Lng );
                mrk = new Marker( trackMarker, cur_row.Mobitel_ID, location0, "", "" );
                mrks = new Marker( MarkerType.Report, cur_row.Mobitel_ID, location1, cur_row.Name, "" );
                markers.Add( mrk );
                markers.Add( mrks );
               
                trSegmentsShowNeededWith( segments );
                mrkrShowNeededWith(markers);
            } // WorkThread

            protected void SetOnBegin()
            {
                ValueSpeedVideo = 1;
                cycleWork = 500;
                iCount = 0;
                prevCount = 0;
                iNdex = 0;
                shouldStop = true;
                //flagIsStop = 0;
                segments1.Clear();
                markers.Clear();
                //marker_stop.Clear();
                data.Clear();
                latlng1 = segments[0].GeoPoints[segments[0].GeoPoints.Count - 1].LatLng;
                location1 = new PointLatLng( latlng1.Lat, latlng1.Lng );
                mrks = new Marker( MarkerType.Report, cur_row.Mobitel_ID, location1, cur_row.Name, "" );
                markers.Add( mrks );

                clrMapObjectsNeeded();
                trSegmentsShowNeededWith( segments );
                mrkrShowNeededWith( markers );
            } // SetOnBegin

            public void ReInital()
            {
                ValueSpeedVideo = 1;
                iCount = 0;
                cycleWork = 500;
                prevCount = 0;
                segments1.Clear();
                markers.Clear();
                data.Clear();
                //marker_stop.Clear();
                //flagIsStop = 0;
                latlng1 = segments[0].GeoPoints[segments[0].GeoPoints.Count - 1].LatLng;
                location1 = new PointLatLng( latlng1.Lat, latlng1.Lng );
                mrks = new Marker( MarkerType.Report, cur_row.Mobitel_ID, location1, cur_row.Name, "" );
                markers.Add( mrk );
                markers.Add( mrks );

                clrMapObjectsNeeded();
                trSegmentsShowNeededWith( segments );
                mrkrShowNeededWith( markers );
            } // ReInital

            private void OutputHandler()
            {
                if ( shouldStop )
                    return;

                buttonStateVid.Invoke( new MethodInvoker( () =>
                    {
                        if ( !( iCount < segments[0].GeoPoints.Count ) )
                        {
                            buttonStateVid.Text = Resources.VideoStart;
                            buttonStateVid.ToolTip = Resources.VideoStart;
                            buttonStateVid.Image = images.Images[4];
                            flagStoping = true;
                            shouldStop = true;
                        }
                    } ) );

                gmap.Invoke( new MethodInvoker( () =>
                {
                    string title_stop = "";

                    if ( !( iCount < segments[0].GeoPoints.Count ) )
                    {
                        shouldStop = true;
                    }
                    else
                    {
                        latlng0 = segments[0].GeoPoints[iCount].LatLng;
                        location0 = new PointLatLng( latlng0.Lat, latlng0.Lng );

                         PointLatLng latlng_1;
                         if ( ( iCount + 1 ) < segments[0].GeoPoints.Count )
                         {
                             latlng_1 = segments[0].GeoPoints[iCount + 1].LatLng;

                             double dlat = latlng_1.Lat - latlng0.Lat;
                             double dlng = latlng_1.Lng - latlng0.Lng;
                             double dl = dlat * dlat + dlng * dlng;

                             if ( dl != 0.0 )
                             {
                                 double phi = dlat / Math.Sqrt( dl );
                                 double gamma = Math.Acos( phi ) * ( radian );

                                 if ( dlng < 0.0 )
                                     gamma = 360.0 - gamma;

                                 if ( ( sectors.sector1 <= gamma ) && ( sectors.sector2 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid45;
                                 }
                                 else if ( ( sectors.sector2 <= gamma ) && ( sectors.sector3 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid90;
                                 }
                                 else if ( ( sectors.sector3 <= gamma ) && ( sectors.sector4 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid135;
                                 }
                                 else if ( ( sectors.sector4 <= gamma ) && ( sectors.sector5 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid180;
                                 }
                                 else if ( ( sectors.sector5 <= gamma ) && ( sectors.sector6 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid225;
                                 }
                                 else if ( ( sectors.sector6 <= gamma ) && ( sectors.sector7 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid270;
                                 }
                                 else if ( ( sectors.sector7 <= gamma ) && ( sectors.sector8 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid315;
                                 }
                                 else if ( ( sectors.sector8 <= gamma ) && ( gamma < 0.0 ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid0;
                                 }
                                 else if ( ( 0.0 <= gamma ) && ( sectors.sector1 > gamma ) )
                                 {
                                     trackMarker = MarkerType.ArrowVid0;
                                 }

                                 //flagIsStop = 0;
                             } // if
                             else
                             {
                                 TimeSpan timestp = new TimeSpan(0, moveline[iCount], 0);
                                 title_stop = Resources.VideoTo + timestp.Hours.ToString() + Resources.VideoH + ":" + 
                                     timestp.Minutes.ToString() + Resources.VideoM; 
                                 trackMarker = MarkerType.ArrowVidStop;
                                 //flagIsStop++;
                              } // else
                            } // if

                         markers[0] = new Marker( trackMarker, 0, location0, title_stop, "" );

                         //if ( flagIsStop == 1 )
                         //{
                         //    marker_stop.Add( markers[0] );
                         //}

                        for ( int i = prevCount + 1; i <= iCount; i++ )
                        {
                            data.Add( segments[0].GeoPoints[i] );
                        }

                        if ( prevCount != iCount )
                        {
                            Track trck = new Track( segments[0].Id, Color.Red, 2f, data );
                            segments1.Clear();
                            segments1.Add( trck );
                        }

                        if ( shouldStop )
                            return;

                        //ReportsControl.OnMarkersShowNeededWith( marker_stop ); // ����� ���������� ������� �������

                        clrMapObjectsNeeded();
                        trSegmentsShowNeededWith( segments );
                        trSegmentsShowNeededWith( segments1 );
                        mrkrShowNeededWith( markers );
                        // mrkrShowNeededWith( marker_stop );
                    } // else
                } ), null );

                trackVid.Invoke( new MethodInvoker( () =>
                {
                    if ( !( iCount < segments[0].GeoPoints.Count ) )
                    {
                        shouldStop = true;
                    }
                    else
                    {
                        prevCount = iCount;
                        iCount++;

                        if ( iCount <= trackVid.Properties.Maximum )
                            trackVid.Value = iCount;
                        else
                            trackVid.Value = trackVid.Properties.Minimum;
                    }
                } ), null );

                trackSpd.Invoke( new MethodInvoker( () =>
                {
                    ValueSpeedVideo = trackSpd.Value;
                    cycleWork = 500 / ValueSpeedVideo;
                } ), null );
            } // OutputHandler

            protected double modul( double cn )
            {
                if ( cn < 0.0 )
                    return ( -cn );

                return cn;
            }

            public bool isWorking()
            {
                return shouldStop;
            }

            public bool stateThread()
            {
                return flagState;
            }

            public void setStateThread( bool st )
            {
                flagState = st;
            }

            public void searchPoint( PointLatLng pll )
            {
                double min = 1e9;
                double delta = 0.01;
                prevIndex = iNdex;

                for ( int i = 0; i < segments[0].GeoPoints.Count; i++ )
                {
                    double mod = Math.Sqrt( ( ( pll.Lat - segments[0].GeoPoints[i].LatLng.Lat ) * ( pll.Lat - segments[0].GeoPoints[i].LatLng.Lat ) ) +
                        ( ( pll.Lng - segments[0].GeoPoints[i].LatLng.Lng ) * ( pll.Lng - segments[0].GeoPoints[i].LatLng.Lng ) ) );

                    if ( mod < delta )
                    {
                        if ( mod < min )
                        {
                            min = mod;
                            iNdex = i;
                        }
                    }
                } // for

                if ( prevIndex != iNdex )
                {
                    ReInital();
                    iCount = iNdex;
                    prevCount = iNdex;
                    setMarker( iNdex );
                } // if
            } // SearchPoint

            protected void setMarker( int index )
            {
                markers[0] = new Marker( trackMarker, cur_row.Mobitel_ID, segments[0].GeoPoints[index].LatLng, "", "" );
                clrMapObjectsNeeded();
                trSegmentsShowNeededWith( segments );
                mrkrShowNeededWith( markers );  
            }

            // ��������� �������
            public void DoWork()
            {
                if ( cur_row == null )
                    return;

                if ( segments == null )
                    return;

                if ( shouldStop )
                {
                    // ����� �� ���� ��� ��������� �����?
                    ValueSpeedVideo = 1;
                    iCount = iNdex;
                    cycleWork = 500;
                    prevCount = iNdex;
                    segments1.Clear();
                    data.Clear();
                    shouldStop = false;
                } // if

                calcMoveLine();

                while ( true )
                {
                    OutputHandler();

                    if ( shouldStop )
                    {
                        return;
                    }

                    Thread.Sleep( cycleWork );
                } // while
            } // DoWork

            protected void calcMoveLine()
            {
                PointLatLng point1;
                PointLatLng point2;
                double mod;

                for ( int i = 0; i < segments[0].GeoPoints.Count - 1; i++ )
                {
                    point1 = segments[0].GeoPoints[i].LatLng;
                    point2 = segments[0].GeoPoints[i + 1].LatLng;

                    mod = Math.Sqrt(( point2.Lat - point1.Lat ) * ( point2.Lat - point1.Lat ) +
                        ( point2.Lat - point1.Lat ) * ( point2.Lat - point1.Lat ));

                    if ( mod <= 0.0 )
                    {
                        moveline.Add( NOT_MOVE );
                    }
                    else
                    {
                        moveline.Add( MOVE_YES );
                    }
                } // for

                analize_stops.Clear();
                bool flagSwt = false;
                TStops stops_t = null;
                int k = 0;
                int j = 0;

                for ( j = 0; j < moveline.Count; j++ )
                {
                    if ( moveline[j] == NOT_MOVE )
                    {
                        k++;
                        if ( !flagSwt )
                        {
                            flagSwt = true;
                            stops_t = new TStops( j );
                        }
                    }
                    else
                    {
                        if ( flagSwt )
                        {
                            flagSwt = false;
                            stops_t.Count = k;
                            analize_stops.Add( stops_t );
                            k = 0;
                        } // if
                    } // else
                } // for

                if ( flagSwt )
                {
                    stops_t.Count = k;
                    analize_stops.Add( stops_t );
                }

                int N = 0;
                flagSwt = false;
                for ( j = 0; j < moveline.Count; j++ )
                {
                    if ( moveline[j] == NOT_MOVE )
                    {
                        moveline[j] = analize_stops[N].Count;
                        flagSwt = true;
                    }
                    else
                    {
                        if ( flagSwt )
                        {
                            N++;
                            flagSwt = false;
                        } // if
                    } // else
                } // for
            } // calcMoveLine

            public void RequestStop()
            {
                shouldStop = true;
                SetOnBegin();
            } // RequestStop

            public void RequestStart()
            {
                shouldStop = false;
                ReInital();
            } // RequestStop
        } // WorkThread



        /// <summary>
        /// ���� ��� ������ � ��������
        /// </summary>
        GMapOverlay _tracks;
        BarButtonItem bbiShowVideo;
        atlantaDataSet _dataset;
        public static atlantaDataSet datasetForMap;
        public static int mobitelId;
        atlantaDataSet.mobitelsRow cur_row = null;
        List<Track> segments = null;
        bool flagStartVideo;
        bool flagStartThread;
        int ValueSpeedVideo = 1;

        public bool IsKeepZonesSelected
        {
            get { return _map.IsKeepZonesSelected; }
            set { _map.IsKeepZonesSelected = value; }
        }

        public  Image  Screenshot
        {
            get { return _map.Screenshot; }
        }
        /// <summary>
        /// �������� ���� �� ��������� �����
        /// </summary>
        List<PointLatLng> _pointsFromTrack = new List<PointLatLng>();

        public List<PointLatLng> PointsFromTrack
        {
            get { return _pointsFromTrack; }
            set { _pointsFromTrack = value; }
        }
        
        public int Zoom
        {
            get { return _map.Zoom; }
        }

        public event Action<Int32> MarkerClick = delegate { };
        public event Action<IZone> ZoneSelected = delegate { };
        public event GeometryChangedHandler NewZoneAdded = delegate { };
        public event TrackPointClick OnTrackPointClicked = delegate { };
        public event PointClick OnPointClicked;
        public WorkThread workerObject;
        Thread workerThread;
        public static XtraTabControl xtabControl = null;
        public static Point mainLocation;
        //private bool isControlZones = true;
        public string UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7";
        public int Timeout = 20 * 1000;
        GMapControl gmap;
        
        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.Maps.GoogleMapControl
        /// </summary>
        public GoogleMapControl()
        {
            InitializeComponent();
            //PointLatLng pointCenter = new PointLatLng(5.50, 3.30);
            //_map.CurrentPosition = pointCenter;
            Localization();
            initMapTypeCombo();
            panelVideo.Visible = false;
            trackBarSpeed.Visible = false;
            trackBarSpeed.Visible = false;
            trackBarSpeed.ValueChanged += new EventHandler( trackBarSpeed_ValueChanged );
            _screenshotBtn.Glyph = Shared.ScreenShot;

            _zoomRepository.Minimum = Globals.GMAP_MIN;
            _zoomRepository.Maximum = Globals.GMAP_MAX;
            _zoomItem.EditValue = Convert.ToInt32(UseMap.getZoomMap());

            _zoomRepository.EditValueChanged += _zoomRepository_EditValueChanged;
            _map.OnMapZoomChanged += _map_OnMapZoomChanged;
            _map.OnMarkerClick += _map_OnMarkerClick;
            _map.ZoneSelected += _map_ZoneSelected;
            _map.NewZoneAdded += _map_NewZoneAdded;
            _map.OnTrackPointClicked += map_OnTrackPointClicked;
            _map.OnMouseMoveLanLong += map_MouseMoveLanLong;
            _map.OnMouseClickLanLong += map_MouseClickLanLong;
            _map.MouseClick += MapOnMouseClick;

            _tracks = new GMapOverlay( _map, "Tracks" );
            _map.Overlays.Add( _tracks );
            LShared.InitListMarkers();
            bchDistance.Glyph = Shared.Distance;
            flagStartVideo = true;
            flagStartThread = true;
            flagStoping = false;
            SizeChanged += GoogleMapControl_SizeChanged;
            
            addVideoButton();
        }

        private void MapOnMouseClick(object sender, MouseEventArgs mouseEventArgs)
        {
            HttpWebRequest request = null;
            GPoint gp;
            ToolTipController defController = null;
          
            try
            {
                if (OnPointClicked != null)
                {
                    if (_map.MapType == MapType.GisFile)
                    {
                        gp = new GPoint(mouseEventArgs.X, mouseEventArgs.Y);
                        PointLatLng pll = _map.FromLocalToLatLng(gp.X, gp.Y);

                        //string url = "http://map.land.gov.ua/kadastrova-karta"; // ����� ������ ���������
                        //request = (HttpWebRequest)WebRequest.Create(url);
                        //request.Proxy = WebRequest.DefaultWebProxy;
                        //request.UserAgent = UserAgent;
                        //request.Timeout = Timeout;
                        //request.ReadWriteTimeout = Timeout * 6;
                        //request.Accept = "*/*";
                        //request.Referer = "http://map.land.gov.ua/kadastrova-karta";
                        //request.ContentType = "text/json"; // ����� ����� �������� � ������
                        //string result;

                        //using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        //{
                        //    result = string.Empty;

                        //    using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8))
                        //    {
                        //        result = reader.ReadToEnd();
                        //    }
                        //    response.Close();
                        //}

                        //string info = result.Substring(0, 10);
                        //File.WriteAllText("D:\\Tmp\\result_http_request.txt", result, Encoding.UTF8);
                        //defController = InitToolTipController(defController);
                        //defController.HideHint();
                        //defController.ShowHint(info, ToolTipLocation.TopRight);
                    }
                }
            }
            catch (Exception ex)
            {
                string urlrequest = "";
                string referer = "";

                if (request != null)
                {
                    urlrequest = request.RequestUri.ToString();
                    referer = request.Referer;
                }

                //XtraMessageBox.Show(ex.Message + "\n" + referer + "\n" + urlrequest, "GetImageFrom error",
                //MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private ToolTipController InitToolTipController(ToolTipController defController)
        {
            // Access the Default ToolTipController. 
            defController = ToolTipController.DefaultController;
            // Customize the controller's settings. 
            defController.Appearance.BackColor = Color.AntiqueWhite;
            defController.Appearance.ForeColor = Color.Black;
            defController.AppearanceTitle.ForeColor = Color.Black;
            //defController.ToolTipLocation = ToolTipLocation.BottomRight;
            defController.Rounded = true;
            defController.ShowBeak = true;
            defController.ShowShadow = true;
            defController.AllowHtmlText = true;
            defController.AutoPopDelay = 2000;
            //defController.ReshowDelay = 10000;
            defController.CloseOnClick = DefaultBoolean.False;
            return defController;
        }

        void GoogleMapControl_SizeChanged( object sender, EventArgs e )
        {
            if(xtabControl == null)
                return;
        }

        public bool ShowVideoTrackControl
        {
            set
            {
                bbiShowVideo.Enabled = value;
                if (!value)
                {
                    bbiShowVideo.Glyph = null;
                    bbiShowVideo.Hint = "";
                }
                else if (value)
                {
                    bbiShowVideo.Glyph = imageCollection1.Images[1];
                    bbiShowVideo.Hint = Resources.VideoPlay;
                }
            }
        }

        /// <summary>
        /// ��� ����� GMap
        /// </summary>
        public MapType MapType
        {
            get
            {
                return _map.MapType;
            }
            set
            {
                _map.MapType = value;
                _mapTypeCombo.EditValue = value;
                Localization();
                initMapTypeCombo();
            }
        }

        /// <summary>
        /// ������� �� ����� �������������� ��������� ���
        /// 
        /// </summary>
        public bool IsZoneEditor
        {
            get { return _map.IsZoneEditor; }
            set { _map.IsZoneEditor = value; }
        }

        /// <summary>
        /// ��������/�� �������� �������� ���
        /// 
        /// </summary>
        public bool EditorZonesOn
        {
            get { return _map.EditorZonesOn; }
            set { _map.EditorZonesOn = value; }
        }

        public void ReloadZone( IZone zone )
        {
            _map.ReloadZone( zone );
        }

        public void CreateNewZone( IZone zone )
        {
            _map.CreateNewZone( zone );
        }

        public void ReleaseEditor()
        {
            _map.ReleaseEditor();
            _map.Cursor = Cursors.Default;
        }

        public void SelectZone( IZone zone )
        {
            _map.SelectZone( zone );
        }

        public bool IsTrackAnalizer
        {
            get { return _map.IsTrackAnalizing; }
            set { _map.IsTrackAnalizing = value; }
        }

        public DrawTrackModes DrawTrackMode
        {
            get { return _map.DrawTrackMode; }
            set { _map.DrawTrackMode = value; }
        }
        //public bool IsSpeedMode
        //{
        //  get { return _map.IsSpeedMode; }
        //  set { _map.IsSpeedMode = value; }
        //}

        //public bool IsRouteMode
        //{
        //    get { return _map.IsRouteMode; }
        //    set { _map.IsRouteMode = value; }
        //}

        //public bool IsThickMode
        //{
        //    get { return _map.IsThickMode; }
        //    set { _map.IsThickMode = value; }
        //}


        public void SetValueCodes( IDictionary<float, Color> codes )
        {
            _map.ValueCodes = codes;
        }

        public void SelectTrack( int id )
        {
            foreach ( GMapTrack track in _tracks.Tracks )
            {
                track.IsActive = id == track.Id;
            }
            _map.Invalidate( true );
        }

        public void RemoveTrack( int id )
        {
            foreach ( GMapTrack track in _tracks.Tracks )
            {
                if ( id == track.Id )
                {
                    _tracks.Tracks.Remove( track );
                    return;
                }

            }
        }

        public void RemoveMarker( Marker m )
        {
            foreach ( GMapMarker gm in _tracks.Markers )
            {
                if ( gm.Position.Lat == m.Point.Lat && gm.Position.Lng == m.Point.Lng )
                {
                    _tracks.Markers.Remove( gm );
                    return;
                }

            }
        }

        public void SelectTrackColor( int id, Color newColor, Color oldColor, bool drawApexes )
        {
            foreach ( GMapTrack track in _tracks.Tracks )
            {
                if ( id == track.Id )
                {
                    track.Color = newColor;
                    _map.SetZoomToFitRect( track.Bounds );
                    if ( drawApexes )
                    {
                        track.IsActive = true;

                    }
                }
                else
                    track.Color = oldColor;
            }
            _map.Invalidate( true );
        }

        #region IMap Members
        /// <summary>
        /// ��������� �����
        /// </summary>
        public MapState State
        {
            get
            {
                return new MapState( _map.Zoom, _map.CurrentPosition );
            }
            set
            {
                _map.Zoom = value.Zoom;
                _map.CurrentPosition = value.Center;
            }
        }

        public XtraTabControl TabControl 
        { 
            set
            {
                xtabControl = value;
            } 
        }

        /// <summary>
        /// ���������� ����� ���� ����� � ��������� �������������� �����
        /// </summary>
        /// <param name="point">�������������� ����� ������ ������ �����</param>
        public void PanTo( PointLatLng point )
        {
            _map.CurrentPosition = point;
        }

        /// <summary>
        /// ������������� ����������� ��������� �������, ����� �������� ���������
        /// �������������� ������������� �������.
        /// </summary>
        /// <param name="rect">������������� ������� � �������������� �����������</param>
        public void PanTo( RectLatLng rect )
        {
            _map.SetZoomToFitRect( rect );
        }

        /// <summary>
        /// ��������� ������ �� �����
        /// </summary>
        /// <param name="marker">������ ��� �����</param>
        public void AddMarker( Marker marker )
        {
            try
            {
                GMapMarker gm = GMapMarkerConverter.Convert(marker);
                gm.TooltipMode = MarkerTooltipMode.OnMouseOver;
                gm.ToolTipText = marker.Descriptoin;
                _tracks.Markers.Add(gm);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show( e.Message + "\n" + e.StackTrace, "Error Online Mode", MessageBoxButtons.OK );
            }
        }

        /// <summary>
        /// ��������� �� ����� ������ ��������
        /// </summary>
        /// <param name="markers">������ �������� ��� �����</param>
        public void AddMarkers( IEnumerable<Marker> markers )
        {
            try
            {
                foreach (Marker m in markers)
                {
                    AddMarker(m);
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Mode", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ������� � ����� ��� ����������� �������
        /// </summary>
        public void ClearMarkers()
        {
            _tracks.Markers.Clear();
        }

        /// <summary>
        /// ��������� �� ����� ����
        /// </summary>
        /// <param name="track">���� ��� �����</param>
        public void AddTrack( Track track )
        {
            try
            {
                if (track == null) 
                    return;

                try
                {
                    if (track.GeoPoints == null)
                        return;
                }
                catch(System.ArgumentNullException ex)
                {
                    return;
                }

                GMapTrack gTrack = new GMapTrack(track.Id, track.GeoPoints);
                gTrack.Color = track.Color;
                gTrack.Bounds = track.Bounds;
                gTrack.IsActive = track.IsActive;
                gTrack.Tag = track.Tag;
                if (DrawTrackMode == DrawTrackModes.Thick)
                {
                    gTrack.WidthAgregatMeter = track.WidthAgregatMeter;
                    gTrack.ColorAgregat = track.ColorAgregat;
                }
                _tracks.Tracks.Add(gTrack);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Google Map Control", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ��������� �� ����� ������ ������
        /// </summary>
        /// <param name="tracks">������ ������ ��� �����</param>
        public void AddTracks( IEnumerable<Track> tracks )
        {
            foreach ( Track t in tracks )
            {
                AddTrack( t );
            }
        }

        protected void trackBarSpeed_ValueChanged( object s, EventArgs e )
        {
            DevExpress.XtraEditors.TrackBarControl bar = ( DevExpress.XtraEditors.TrackBarControl )s;
            ValueSpeedVideo = bar.Value;
        }

        /// <summary>
        /// ������� � ����� ��� ����������� �����
        /// </summary>
        public void ClearTracks()
        {
            _tracks.Tracks.Clear();
        }

        /// <summary>
        /// ������������� ������������ �������, ����� �������� ��� �����������
        /// ������� � �����
        /// </summary>
        public void ZoomAndCenterAll()
        {
            _map.ZoomAndCenterTracks( null );
        }

        public void ZoomAndCenterAllAlways()
        {
            _map.ScreenRectChanged = true;
            _map.ZoomAndCenterTracks( null );
        }

        public void ZoomAndCenterZones()
        {
            _map.ZoomAndCenterZones();
        }
        /// <summary>
        /// ��������� �� ����� ����������� ����
        /// </summary>
        /// <param name="zone">����������� ����</param>
        public void AddZone( IZone zone )
        {
            _map.Zones.Add( new GMapZone( zone ) );
        }

        /// <summary>
        /// ��������� �� ����� ������ ����������� ���
        /// </summary>
        /// <param name="zones">������ ����������� ���</param>
        public void AddZones( IEnumerable<IZone> zones )
        {
            foreach ( IZone z in zones )
                AddZone( z );
        }

        /// <summary>
        /// ������� � ����� ��� ����������� ����������� ����
        /// </summary>
        public void ClearZones()
        {
            _map.Zones.Clear();
        }

        public void ClearAll()
        {
            _map.Zones.Clear();
            _tracks.Tracks.Clear();
            _tracks.Markers.Clear();
        }

        public void Repaint()
        {
            _map.Invalidate( true );
        }
        #endregion

        void Localization()
        {
            _mapTypeCombo.Caption = Resources.MapTypeComboCaption;
            _zoomItem.Caption = Resources.ZoomItemCaption;
            _screenshotBtn.Hint = Resources.ScreenshotBtnHint;
            bchDistance.Caption = Resources.CaptionDistance;
            bchDistance.Hint = Resources.HintDistance;
            bchArea.Caption = Resources.CaptionArea;
            bchArea.Hint = Resources.HintArea;
        }

        void initMapTypeCombo()
        {
            if (_mapTypeRepository.Items.Count > 0) _mapTypeRepository.Items.Clear();
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Google {0}", Resources.Map),
                MapType.GoogleMap));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Google {0}", Resources.Satellite),
                MapType.GoogleSatellite));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Google {0}", Resources.Hybrid),
                MapType.GoogleHybrid));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem("OpenStreetMap", MapType.OpenStreetMap));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Bing {0}", Resources.Satellite),
                MapType.BingSatellite));

            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Yandex {0}", Resources.Map),
                MapType.YandexMap));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Yandex {0}", Resources.Satellite),
                MapType.YandexSatelliteMap));
            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("Yandex {0}", Resources.Hybrid),
                MapType.YandexHybridMap));

            //_mapTypeRepository.Items.Add( new ImageComboBoxItem(String.Format( "Yahoo {0}", Resources.Satellite ), MapType.YahooSatellite ) );

            _mapTypeRepository.Items.Add(new ImageComboBoxItem(String.Format("CadMap Ukraine"), MapType.GisFile));

            _mapTypeRepository.SelectedValueChanged += _mapTypeRepository_EditValueChanged;
        }

        void _mapTypeRepository_EditValueChanged( object sender, EventArgs e )
        {
            ImageComboBoxEdit edit1 = sender as ImageComboBoxEdit;
            string prevmp = _map.MapType.ToString();
            string currmap = ((MapType) edit1.EditValue).ToString();

            if (currmap == prevmp)
                return;

            if ((MapType)edit1.EditValue == MapType.GisFile)
            {
                int zoom = Convert.ToInt32(_zoomItem.EditValue.ToString()); 

                if (zoom >= 17)
                {
                    _zoomItem.EditValue = 16;
                    _map.Zoom = Convert.ToInt32(_zoomItem.EditValue.ToString());
                    _map.MapType = (MapType)edit1.EditValue;
                    string idx1 = edit1.EditValue.ToString();
                    UseMap.saveParamIntoXml(idx1, "type");
                    UseMap.saveParamIntoXml(_map.Zoom.ToString(), "zoom");

                    XtraMessageBox.Show("��� ������� ����� ���������� ������������ ����������", "Zoom Map",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
            }

            ImageComboBoxEdit edit = sender as ImageComboBoxEdit;
            _map.Zoom = Convert.ToInt32(_zoomItem.EditValue.ToString());
            _map.MapType = ( MapType )edit.EditValue;
            string idx = edit.EditValue.ToString();
            UseMap.saveParamIntoXml(idx, "type");
            UseMap.saveParamIntoXml(_map.Zoom.ToString(), "zoom");
        }

        void _zoomRepository_EditValueChanged( object sender, EventArgs e )
        {
            if (_map.MapType == MapType.GisFile)
            {
                TrackBarControl tbc = sender as TrackBarControl;
                if (tbc.Value >= 17)
                {
                    tbc.Value = 16;
                    XtraMessageBox.Show("��� ������� ����� ���������� ������������ ����������", "Zoom Map",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }

                _map.Zoom = tbc.Value;
                UseMap.saveParamIntoXml(tbc.Value.ToString(), "zoom");
            }
            else
            {
                TrackBarControl tbc = sender as TrackBarControl;
                _map.Zoom = tbc.Value;
                UseMap.saveParamIntoXml(tbc.Value.ToString(), "zoom");
            }
        }

        void _screenshotBtn_ItemClick( object sender, ItemClickEventArgs e )
        {
            ImageService.SaveImage( _map.Screenshot, "Map_Shot" );
        }

        /// <summary>
        /// ������������ ��� ��������� �������� �����
        /// </summary>
        void _map_OnMapZoomChanged()
        {
            if ( InvokeRequired && IsHandleCreated )
            {
                MethodInvoker m = delegate()
                {
                    _zoomItem.EditValue = _map.Zoom;
                };
                Invoke( m );
            }
            else
            {
                _zoomItem.EditValue = _map.Zoom;
            }
        }

        void _map_OnMarkerClick( GMapMarker item )
        {
            OnlineMarker marker = item as OnlineMarker;
            //marker.IsDragging = true;
            if ( null != marker )
                MarkerClick( marker.Id );
        }

        void _map_ZoneSelected( IZone zone )
        {
            ZoneSelected( zone );
        }

        void _map_NewZoneAdded( int zoneId, IList<PointLatLng> points )
        {
            NewZoneAdded( zoneId, points );
        }

        void map_OnTrackPointClicked( GMapTrack tr, IVehicle vehicle, int indexInArray, Point point )
        {
            OnTrackPointClicked( tr, vehicle, indexInArray, point );
        }

        void map_MouseMoveLanLong( GPoint gp )
        {
            PointLatLng pll = _map.FromLocalToLatLng( gp.X, gp.Y );
            beiPosition.EditValue = String.Format(" Lat={1:#.####} Lon={0:#.####}", pll.Lng, pll.Lat);
        }

        protected void SearchPoint( PointLatLng pll )
        {
            if ( workerObject == null )
                return;

            if ( !flagStartVideo )
            {
                if ( flagStoping )
                {
                    workerObject.searchPoint( pll );
                }
            } // if
        } // SearchPoint

        void map_MouseClickLanLong( GPoint gp )
        {
            if ( OnPointClicked != null )
            {
                PointLatLng pll = _map.FromLocalToLatLng( gp.X, gp.Y );
                SearchPoint( pll );
                OnPointClicked( pll );
            }
        }

        private void bchDistance_CheckedChanged( object sender, ItemClickEventArgs e )
        {
            if ( bchDistance.Checked )
            {
                bchArea.Checked = false;
                _map.ToolDistance = new DistanceTool();
                _map.isTooltipShow = false;
                _map.isDistanceUse = false;
            }
            else
            {
                if ( _map.ToolDistance is DistanceTool )
                {
                    ( ( DistanceTool )_map.ToolDistance ).Release( _map );
                    _map.ToolDistance = new FreeMoveTool();
                    _map.isTooltipShow = true;
                    _map.isDistanceUse = true;
                }
            }
        }

        public void ResetTools()
        {
            bchDistance.Checked = false;
            bchArea.Checked = false;
            _map.Tool = new FreeMoveTool();
        }

        private void bchArea_CheckedChanged( object sender, ItemClickEventArgs e )
        {
            if ( bchArea.Checked )
            {
                bchDistance.Checked = false;
                _map.ToolArea = new AreaTool();
                _map.IsZoneEditor = false;
                _map.zoneEditorOn = true;
                _map.isTooltipShow = false;
                _map.isDistanceUse = false;
            }
            else
            {
                if ( _map.ToolArea is AreaTool )
                {
                    ( ( AreaTool )_map.ToolArea ).Release( _map );
                    _map.ToolArea = new FreeMoveTool();
                    // _map.IsZoneEditor = false;
                    _map.zoneEditorOn = false;
                    _map.isTooltipShow = true;
                    _map.isDistanceUse = true;
                }
            }
        } // bchArea_CheckedChanged

        // ��������������� ������������� �� ����� �����; aketner
        private void bbiShowVideo_ItemClick( object sender, ItemClickEventArgs e )
        {
            try
            {
                _dataset = datasetForMap;

                if ( _dataset.mobitels == null )
                {
                    XtraMessageBox.Show( Resources.VideoDataError, "Info",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information );
                    return;
                }

                if ( flagStartVideo )
                {
                    flagStartVideo = false;
                    cur_row = null;
                }
                else
                {
                    flagStartVideo = true;
                    workerObject.RequestStop();

                    if ( workerObject.stateThread() )
                    {
                        workerThread.Resume();
                    }

                    if ( workerThread != null )
                        workerThread.Abort();

                    panelVideo.Visible = false;
                    trackVideo.Visible = false;
                    trackVideo.Value = 0;
                    buttonStart.Text = Resources.VideoStart;
                    buttonStart.ToolTip = Resources.VideoStart;
                    buttonStart.Image = imageCollection1.Images[4];
                    trackBarSpeed.Visible = false;
                    trackBarSpeed.Value = 1;
                    flagStoping = false;
                    ValueSpeedVideo = 1;
                    workerObject = null;
                    workerThread = null;
                    return;
                } // else
            } // try
            catch (Exception ex)
            {
                MessageBox.Show( Resources.VideoDeleteThreadError );
                return;
            }

            try
            {
                cur_row = null;

                foreach ( atlantaDataSet.mobitelsRow m_row in _dataset.mobitels )
                {
                    if ( m_row.Check){
                        if ( mobitelId == m_row.Mobitel_ID )
                        {
                            cur_row = m_row;
                            break;
                        }
                    } // if
                } // foreach

                if ( cur_row == null )
                {
                    flagStartVideo = true;
                    XtraMessageBox.Show( Resources.VideoDataError, "Info",
                                MessageBoxButtons.OK, MessageBoxIcon.Information );
                    return;
                } // if

                GpsData[] d_rows;

                if ( cur_row != null )
                {
                    d_rows = DataSetManager.GetDataGpsArray(cur_row);
                    List<IGeoPoint> dataGeoPoint = new List<IGeoPoint>();
                    segments = new List<Track>();

                    if ( d_rows.Length > 0 )
                    {
                        dataGeoPoint.AddRange( d_rows );
                    }

                    if ( dataGeoPoint.Count > 0 )
                    {
                        Track trck = new Track( cur_row.Mobitel_ID, Color.DarkBlue, 2f, dataGeoPoint );
                        segments.Add( trck );
                    }

                    if ( segments.Count > 0 )
                    {
                        panelVideo.Visible = true;
                        trackVideo.Visible = true;
                        trackVideo.Properties.Minimum = 0;
                        trackVideo.Properties.Maximum = segments[0].GeoPoints.Count - 1;
                        trackBarSpeed.Visible = true;
                        ValueSpeedVideo = 1;
                        flagStoping = true;

                        workerObject = new WorkThread( cur_row, segments, _map, trackVideo, trackBarSpeed, buttonStart, imageCollection1 );
                    } // if
                } // if
            } // try
            catch ( Exception ex )
            {
                flagStartVideo = false;

                if ( workerObject != null )
                {
                    workerObject.RequestStop();
                    workerObject.setStateThread( false );
                }

                if ( workerThread != null )
                {
                    if ( workerObject.stateThread() )
                        workerThread.Resume();
                }

                panelVideo.Visible = false;
                buttonStart.Text = Resources.VideoStart;
                buttonStart.ToolTip = Resources.VideoStart;
                buttonStart.Image = imageCollection1.Images[4];
                trackBarSpeed.Visible = false;
                trackBarSpeed.Value = 1;
                trackVideo.Visible = false;
                trackVideo.Value = 0;
                ValueSpeedVideo = 1;

                MessageBox.Show( Resources.VideoThreadError );
            } // catch
        } // bbiShowVideo_ItemClick

        protected void addVideoButton()
        {
            bbiShowVideo = new BarButtonItem();
            _barManager.Items.Add( bbiShowVideo );
            _mapTools.LinksPersistInfo.Insert( 5, new LinkPersistInfo( BarLinkUserDefines.PaintStyle,
                bbiShowVideo, BarItemPaintStyle.CaptionGlyph ) );

            bbiShowVideo.Caption = "";
            bbiShowVideo.Glyph = imageCollection1.Images[1];
            bbiShowVideo.Id = 5;
            bbiShowVideo.Name = "bbiShowVideo";
            bbiShowVideo.Hint = Resources.VideoPlay;
            bbiShowVideo.ItemClick += new ItemClickEventHandler( bbiShowVideo_ItemClick );
            bbiShowVideo.Enabled = true; // aketner - 19.06.2013
        }

        private void buttonStart_Click( object sender, EventArgs e )
        {
            try
            {
                if ( workerObject == null )
                    return;

                if ( buttonStart.Text == Resources.VideoStart )
                {
                    flagStoping = false;
                    flagStartThread = true;
                    buttonStart.Text = Resources.VideoPause;
                    buttonStart.ToolTip = Resources.VideoPause;
                    buttonStart.Image = imageCollection1.Images[5];

                    if ( workerThread == null )
                    {
                        workerThread = new Thread( workerObject.DoWork );
                        workerThread.Start();
                        workerObject.setStateThread( false );
                        buttonResume.Enabled = true;
                    }
                    else
                    {
                        if ( !workerObject.isWorking() )
                        {
                            workerThread.Resume();
                            workerObject.setStateThread( false );
                        }
                        else
                        {
                            workerThread = new Thread( workerObject.DoWork );
                            workerThread.Start();
                            workerObject.setStateThread( false );
                            buttonResume.Enabled = true;
                        }
                    } // else
                } // if
                else if ( buttonStart.Text == Resources.VideoPause )
                {
                    flagStartThread = false;
                    flagStoping = true;
                    buttonStart.Text = Resources.VideoStart;
                    buttonStart.ToolTip = Resources.VideoStart;
                    buttonStart.Image = imageCollection1.Images[4];

                    if ( !workerObject.stateThread() )
                        workerThread.Suspend();

                    workerObject.setStateThread( true );
                } // else if
            } // try
            catch ( Exception ex )
            {
                MessageBox.Show( Resources.VideoStartError );
            }
        } // buttonStart_Click

        private void buttonResume_Click( object sender, EventArgs e )
        {
            try
            {
                if ( flagStartThread && !workerObject.isWorking() )
                {
                    workerThread.Suspend();
                    workerObject.setStateThread( true );
                    workerObject.ReInital();
                    workerThread.Resume();
                    workerObject.setStateThread( false );
                    return;
                } // if

                workerObject.ReInital();
            } // try
            catch ( Exception ex )
            {
                MessageBox.Show( Resources.VideoRestartError );
            }
        } // buttonResume_Click
    }
}