﻿using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.Collections.Generic;
using TrackControl.GMap.Core;
using TrackControl.GMap.Properties;

namespace TrackControl.GMap.CacheProviders
{
   /// <summary>
   /// ultra fast cache system for tiles
   /// </summary>
   internal class SQLitePureImageCache : PureImageCache
   {
      string cache;
      string gtileCache;

      public string GtileCache
      {
         get
         {
            return gtileCache;
         }
      }

      /// <summary>
      /// local cache location
      /// </summary>
      public string CacheLocation
      {
         get
         {
            return cache;
         }
         set
         {
            cache = value;
            gtileCache = cache + "TileDBv3" + Path.DirectorySeparatorChar;
         }
      }

      public SQLitePureImageCache()
      {
         CacheLocation = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "GMap.NET" + Path.DirectorySeparatorChar;
      }

      #region -- import / export --
      public static bool CreateEmptyDB(string file)
      {
         bool ret = true;

         try
         {
            string dir = Path.GetDirectoryName(file);
            if(!Directory.Exists(dir))
            {
               Directory.CreateDirectory(dir);
            }

            using(SQLiteConnection cn = new SQLiteConnection())
            {

               cn.ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=False;", file);

               cn.Open();
               {
                  using(DbTransaction tr = cn.BeginTransaction())
                  {
                     try
                     {
                        using(DbCommand cmd = cn.CreateCommand())
                        {

                           cmd.CommandText = Resources.CreateTileDb;

                           cmd.ExecuteNonQuery();
                        }
                        tr.Commit();
                     }
                     catch
                     {
                        tr.Rollback();
                        ret = false;
                     }
                  }
                  cn.Close();
               }
            }
         }
         catch(Exception ex)
         {
           Debug.WriteLine(String.Format("CreateEmptyDB: {0}", ex));
            ret = false;
         }
         return ret;
      }

     public static bool ExportMapDataToDB(string sourceFile, string destFile)
     {
       bool ret = true;

       try
       {
         if (!File.Exists(destFile))
         {
           ret = CreateEmptyDB(destFile);
         }

         if (ret)
         {
           using (SQLiteConnection cn1 = new SQLiteConnection())
           {

             cn1.ConnectionString = string.Format("Data Source=\"{0}\";", sourceFile);


             cn1.Open();
             if (cn1.State == ConnectionState.Open)
             {
               using (SQLiteConnection cn2 = new SQLiteConnection())
               {

                 cn2.ConnectionString = string.Format("Data Source=\"{0}\";", destFile);

                 cn2.Open();
                 if (cn2.State == ConnectionState.Open)
                 {
                   using (SQLiteCommand cmd = new SQLiteCommand(string.Format("ATTACH DATABASE \"{0}\" AS Source", sourceFile), cn2))
                   {
                     cmd.ExecuteNonQuery();
                   }

                   using (DbTransaction tr = cn2.BeginTransaction())
                   {
                     try
                     {
                       List<long> add = new List<long>();
                       using (SQLiteCommand cmd = new SQLiteCommand("SELECT id, X, Y, Zoom, Type FROM Tiles;", cn1))
                       {
                         using (SQLiteDataReader rd = cmd.ExecuteReader())
                         {
                           while (rd.Read())
                           {
                             long id = rd.GetInt64(0);
                             using (SQLiteCommand cmd2 = new SQLiteCommand(string.Format("SELECT id FROM Tiles WHERE X={0} AND Y={1} AND Zoom={2} AND Type={3};", rd.GetInt32(1), rd.GetInt32(2), rd.GetInt32(3), rd.GetInt32(4)), cn2))
                             {
                               using (SQLiteDataReader rd2 = cmd2.ExecuteReader())
                               {
                                 if (!rd2.Read())
                                 {
                                   add.Add(id);
                                 }
                               }
                             }
                           }
                         }
                       }

                       foreach (long id in add)
                       {
                         using (SQLiteCommand cmd = new SQLiteCommand(string.Format("INSERT INTO Tiles(X, Y, Zoom, Type) SELECT X, Y, Zoom, Type FROM Source.Tiles WHERE id={0}; INSERT INTO TilesData(id, Tile) Values((SELECT last_insert_rowid()), (SELECT Tile FROM Source.TilesData WHERE id={0}));", id), cn2))
                         {
                           cmd.ExecuteNonQuery();
                         }
                       }
                       add.Clear();

                       tr.Commit();
                     }
                     catch
                     {
                       tr.Rollback();
                       ret = false;
                     }
                   }
                 }
               }
             }
           }
         }
       }
       catch (Exception ex)
       {
         Debug.WriteLine(String.Format("ExportMapDataToDB: {0}", ex));
         ret = false;
       }
       return ret;
     }
      #endregion

      #region PureImageCache Members

      bool PureImageCache.PutImageToCache(MemoryStream tile, MapType type, GPoint pos, int zoom)
      {
         bool ret = true;
         try
         {
            string dir = gtileCache + GMaps.Instance.LanguageStr + Path.DirectorySeparatorChar;

            // precrete dir
            if(!Directory.Exists(dir))
            {
               Directory.CreateDirectory(dir);
            }

            // save
            {
              string db = String.Format("{0}Data.gmdb", dir);
              
               if(!File.Exists(db))
               {
                  ret = CreateEmptyDB(db);
               }

               if(ret)
               {
                  using(SQLiteConnection cn = new SQLiteConnection())
                  {

                     cn.ConnectionString = string.Format("Data Source=\"{0}\";", db);


                     cn.Open();
                     {
                        {
                           using(DbTransaction tr = cn.BeginTransaction())
                           {
                              try
                              {
                                 using(DbCommand cmd = cn.CreateCommand())
                                 {
                                    cmd.Transaction = tr;

                                    cmd.CommandText = "INSERT INTO Tiles(X, Y, Zoom, Type) VALUES(@p1, @p2, @p3, @p4)";

                                    cmd.Parameters.Add(new SQLiteParameter("@p1", pos.X));
                                    cmd.Parameters.Add(new SQLiteParameter("@p2", pos.Y));
                                    cmd.Parameters.Add(new SQLiteParameter("@p3", zoom));
                                    cmd.Parameters.Add(new SQLiteParameter("@p4", (int) type));

                                    cmd.ExecuteNonQuery();
                                 }

                                 using(DbCommand cmd = cn.CreateCommand())
                                 {
                                    cmd.Transaction = tr;

                                    cmd.CommandText = "INSERT INTO TilesData(id, Tile) VALUES((SELECT last_insert_rowid()), @p1)";
                                    cmd.Parameters.Add(new SQLiteParameter("@p1", tile.GetBuffer()));

                                    cmd.ExecuteNonQuery();
                                 }
                                 tr.Commit();
                              }
                              catch(Exception ex)
                              {
                                Debug.WriteLine(String.Format("PutImageToCache: {0}", ex));

                                 tr.Rollback();
                                 ret = false;
                              }
                           }
                        }
                     }
                     cn.Close();
                  }
               }
            }
         }
         catch(Exception ex)
         {
           Debug.WriteLine(String.Format("PutImageToCache: {0}", ex));
            ret = false;
         }
         return ret;
      }

      PureImage PureImageCache.GetImageFromCache(MapType type, GPoint pos, int zoom)
      {
         PureImage ret = null;
         try
         {
            string db = gtileCache + GMaps.Instance.LanguageStr + Path.DirectorySeparatorChar + "Data.gmdb";

            // get
            {
               {
                  using(SQLiteConnection cn = new SQLiteConnection())
                  {

                     cn.ConnectionString = string.Format("Data Source=\"{0}\";", db);

                     cn.Open();
                     {
                        using(DbCommand com = cn.CreateCommand())
                        {
                           com.CommandText = string.Format("SELECT Tile FROM TilesData WHERE id = (SELECT id FROM Tiles WHERE X={0} AND Y={1} AND Zoom={2} AND Type={3})", pos.X, pos.Y, zoom, (int) type);

                           using(DbDataReader rd = com.ExecuteReader(System.Data.CommandBehavior.SequentialAccess))
                           {
                              if(rd.Read())
                              {
                                 long length = rd.GetBytes(0, 0, null, 0, 0);
                                 byte[] tile = new byte[length];
                                 rd.GetBytes(0, 0, tile, 0, tile.Length);
                                 {
                                    if(GMaps.Instance.ImageProxy != null)
                                    {
                                       MemoryStream stm = new MemoryStream(tile, 0, tile.Length, false, true);

                                       ret = GMaps.Instance.ImageProxy.FromStream(stm);
                                       if(ret!= null)
                                       {
                                          ret.Data = stm;
                                       }
                                    }
                                 }
                                 tile = null;
                              }
                              rd.Close();
                           }
                        }
                     }
                     cn.Close();
                  }
               }
            }
         }
         catch(Exception ex)
         {
           Debug.WriteLine(String.Format("GetImageFromCache: {0}", ex));
            ret = null;
         }

         return ret;
      }

      #endregion
   }

}
