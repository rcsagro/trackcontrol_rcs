﻿using System.IO;
using System;
using System.Diagnostics;
using System.Text;
using System.Globalization;
using TrackControl.General.Core;
using TrackControl.GMap.CacheProviders;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Internals
{
   /// <summary>
   /// cache system for tiles, geocoding, etc...
   /// </summary>
   internal class Cache : Singleton<Cache>
   {
      string cache;
      string routeCache = "";
      /// <summary>
      /// abstract image cache
      /// </summary>
      public PureImageCache ImageCache;

      /// <summary>
      /// second level abstract image cache
      /// </summary>
      public PureImageCache ImageCacheSecond;

      /// <summary>
      /// local cache location
      /// </summary>
      public string CacheLocation
      {
         get
         {
            return cache;
         }
         set
         {
            cache = value;
            if(ImageCache is SQLitePureImageCache)
            {
               (ImageCache as SQLitePureImageCache).CacheLocation = value;
            }

         }
      }

     public Cache()
     {
       #region singleton check
       if (Instance != null)
       {
         throw (new Exception("You have tried to create a new singleton class where you should have instanced it. Replace your \"new class()\" with \"class.Instance\""));
       }
       #endregion


       ImageCache = new SQLitePureImageCache();


       if (string.IsNullOrEmpty(CacheLocation))
       {

         {
           CacheLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "GMap.NET" + Path.DirectorySeparatorChar;
         }
       }
     }

     public string GetRouteFromCache(string urlEnd)
     {
         string ret = null;

         try
         {
             StringBuilder file = new StringBuilder(routeCache);
             file.AppendFormat(CultureInfo.InvariantCulture, "{0}.dragdir", urlEnd);

             if (File.Exists(file.ToString()))
             {
                 using (StreamReader r = new StreamReader(file.ToString(), Encoding.UTF8))
                 {
                     ret = r.ReadToEnd();
                 }
             }
         }
         catch
         {
             ret = null;
         }

         return ret;
     }

     public void CacheRoute(string urlEnd, string content)
     {
         try
         {
             // precrete dir
             if (!Directory.Exists(routeCache))
             {
                 Directory.CreateDirectory(routeCache);
             }

             StringBuilder file = new StringBuilder(routeCache);
             file.AppendFormat(CultureInfo.InvariantCulture, "{0}.dragdir", urlEnd);

             using (StreamWriter writer = new StreamWriter(file.ToString(), false, Encoding.UTF8))
             {
                 writer.Write(content);
             }
         }
         catch
         {
         }
     }


     #region -- etc cache --

     public void SaveContent(string urlEnd, CacheType type, string content)
     {
         try
         {
             Stuff.RemoveInvalidPathSymbols(ref urlEnd);

             string dir = Path.Combine(cache, type.ToString()) + Path.DirectorySeparatorChar;

             // precrete dir
             if (!Directory.Exists(dir))
             {
                 Directory.CreateDirectory(dir);
             }

             string file = dir + urlEnd;

             switch (type)
             {
                 case CacheType.GeocoderCache:
                     file += ".geo";
                     break;

                 case CacheType.PlacemarkCache:
                     file += ".plc";
                     break;

                 case CacheType.RouteCache:
                     file += ".dragdir";
                     break;

                 case CacheType.UrlCache:
                     file += ".url";
                     break;

                 case CacheType.DirectionsCache:
                     file += ".dir";
                     break;

                 default:
                     file += ".txt";
                     break;
             }

             using (StreamWriter writer = new StreamWriter(file, false, Encoding.UTF8))
             {
                 writer.Write(content);
             }
         }
         catch (Exception ex)
         {
             Debug.WriteLine("SaveContent: " + ex);
         }
     }

     public string GetContent(string urlEnd, CacheType type, TimeSpan stayInCache)
     {
         string ret = null;

         try
         {
             Stuff.RemoveInvalidPathSymbols(ref urlEnd);

             string dir = Path.Combine(cache, type.ToString()) + Path.DirectorySeparatorChar;
             string file = dir + urlEnd;

             switch (type)
             {
                 case CacheType.GeocoderCache:
                     file += ".geo";
                     break;

                 case CacheType.PlacemarkCache:
                     file += ".plc";
                     break;

                 case CacheType.RouteCache:
                     file += ".dragdir";
                     break;

                 case CacheType.UrlCache:
                     file += ".url";
                     break;

                 default:
                     file += ".txt";
                     break;
             }

             if (File.Exists(file))
             {
                 var writeTime = File.GetLastWriteTime(file);
                 if (DateTime.Now - writeTime < stayInCache)
                 {
                     using (StreamReader r = new StreamReader(file, Encoding.UTF8))
                     {
                         ret = r.ReadToEnd();
                     }
                 }
             }
         }
         catch (Exception ex)
         {
             ret = null;
             //Debug.WriteLine("GetContent: " + ex);
         }

         return ret;
     }

     public string GetContent(string urlEnd, CacheType type)
     {
         return GetContent(urlEnd, type, TimeSpan.FromDays(88));
     }

     #endregion
   }

   internal enum CacheType
   {
       GeocoderCache,
       PlacemarkCache,
       RouteCache,
       UrlCache,
       DirectionsCache,
   }
}
