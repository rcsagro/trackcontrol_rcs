﻿using System;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Internals
{
   /// <summary>
   /// struct for raw tile
   /// </summary>
   internal struct RawTile
   {
      public MapType Type;
      public GPoint Pos;
      public int Zoom;

      public RawTile(MapType Type, GPoint Pos, int Zoom)
      {
         this.Type = Type;
         this.Pos = Pos;
         this.Zoom = Zoom;
      }

      public override string ToString()
      {
         return String.Format("{0} at zoom {1}, pos:{2}", Type, Zoom, Pos);
      }
   }
}
