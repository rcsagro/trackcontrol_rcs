﻿using System;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Internals
{
   /// <summary>
   /// tile load task
   /// </summary>
   internal struct LoadTask
   {
      public GPoint Pos;
      public int Zoom;

      public LoadTask(GPoint pos, int zoom)
      {
         Pos = pos;
         Zoom = zoom;
      }

      public override string ToString()
      {
         return String.Format("{0} - {1}", Zoom, Pos);
      }
   }
}
