﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Projections;
using System.ComponentModel;
using System.Drawing;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Internals
{
    /// <summary>
    /// internal map control core
    /// </summary>
    internal class GCore
    {
        public PointLatLng currentPosition;
        public GPoint currentPositionPixel;

        public GPoint renderOffset;
        public GPoint centerTileXYLocation;
        public GPoint centerTileXYLocationLast;
        public GPoint dragPoint;

        public GPoint mouseDown;
        public GPoint mouseCurrent;
        public GPoint mouseLastZoom;

        public MouseWheelZoomType MouseWheelZoomType = MouseWheelZoomType.MousePositionAndCenter;

        public PointLatLng? LastLocationInBounds = null;

        public GSize sizeOfMapArea;
        public GSize minOfTiles;
        public GSize maxOfTiles;

        public GRectangle tileRect;
        public GPoint tilePoint;

        public GRectangle CurrentRegion;

        public readonly TileMatrix Matrix = new TileMatrix();
        private readonly EventWaitHandle waitOnEmptyTasks = new EventWaitHandle(false, EventResetMode.AutoReset);
        public List<GPoint> tileDrawingList = new List<GPoint>();
        public readonly Queue<LoadTask> tileLoadQueue = new Queue<LoadTask>();
        private readonly WaitCallback ProcessLoadTaskCallback;

        public readonly string googleCopyright =
            string.Format("©{0} Google - Map data ©{0} Tele Atlas, Imagery ©{0} TerraMetrics", DateTime.Today.Year);

        public readonly string openStreetMapCopyright = string.Format("© OpenStreetMap - Map data ©{0} OpenStreetMap",
            DateTime.Today.Year);

        public readonly string yahooMapCopyright = string.Format("© Yahoo! Inc. - Map data & Imagery ©{0} NAVTEQ",
            DateTime.Today.Year);

        public readonly string virtualEarthCopyright =
            string.Format("©{0} Microsoft Corporation, ©{0} NAVTEQ, ©{0} Image courtesy of NASA", DateTime.Today.Year);

        public readonly string yandexMapCopyright = string.Format("© YandexMap - Map data ©{0}", DateTime.Today.Year);

        internal bool started;
        private int zoom;
        internal int Width;
        internal int Height;

        internal int pxRes100m; // 100 meters
        internal int pxRes1000m; // 1km  
        internal int pxRes10km; // 10km
        internal int pxRes100km; // 100km
        internal int pxRes1000km; // 1000km
        internal int pxRes5000km; // 5000km

        private PureProjection projection;

        /// <summary>
        /// current peojection
        /// </summary>
        public PureProjection Projection
        {
            get
            {
                return projection;
            }
            set
            {
                projection = value;
                tileRect = new GRectangle(new GPoint(0, 0), value.TileSize);
            }
        }

        public AutoResetEvent Refresh = new AutoResetEvent(false);
        internal readonly object invalidationLock = new object();
        internal DateTime lastInvalidation = DateTime.Now;

        private void invalidatorWatch(object sender, DoWorkEventArgs e)
        {
            var w = sender as BackgroundWorker;

            TimeSpan span = TimeSpan.FromMilliseconds(111);
            int spanMs = (int) span.TotalMilliseconds;
            bool skiped = false;
            TimeSpan delta;
            DateTime now = DateTime.Now;

            while (Refresh != null && (!skiped && Refresh.WaitOne() || (Refresh.WaitOne(spanMs, false) || true)))
            {
                if (w.CancellationPending)
                    break;

                now = DateTime.Now;
                lock (invalidationLock)
                {
                    delta = now - lastInvalidation;
                }

                if (delta > span)
                {
                    lock (invalidationLock)
                    {
                        lastInvalidation = now;
                    }
                    skiped = false;

                    w.ReportProgress(1);
                    Debug.WriteLine("Invalidate delta: " + (int) delta.TotalMilliseconds + "ms");
                }
                else
                {
                    skiped = true;
                }
            }
        }

        /// <summary>
        /// is polygons enabled
        /// </summary>
        public bool PolygonsEnabled = true;

        /// <summary>
        /// is user dragging map
        /// </summary>
        public bool IsDragging;

        /// <summary>
        /// map zoom
        /// </summary>
        public int Zoom
        {
            get
            {
                return zoom;
            }
            set
            {
                if (zoom != value && !IsDragging)
                {
                    zoom = value;

                    minOfTiles = Projection.GetTileMatrixMinXY(value);
                    maxOfTiles = Projection.GetTileMatrixMaxXY(value);

                    CurrentPositionGPixel = Projection.FromLatLngToPixel(CurrentPosition, value);

                    if (started)
                    {
                        lock (tileLoadQueue)
                        {
                            tileLoadQueue.Clear();
                            tileLoadQueue.TrimExcess();
                        }
                        Matrix.Clear();

                        GoToCurrentPositionOnZoom();
                        UpdateBounds();

                        if (OnNeedInvalidation != null)
                        {
                            OnNeedInvalidation();
                        }

                        if (OnMapDrag != null)
                        {
                            OnMapDrag();
                        }

                        if (OnMapZoomChanged != null)
                            OnMapZoomChanged();

                        if (OnCurrentPositionChanged != null)
                            OnCurrentPositionChanged(currentPosition);
                    }
                }
            }
        }

        /// <summary>
        /// current marker position in pixel coordinates
        /// </summary>
        public GPoint CurrentPositionGPixel
        {
            get
            {
                return currentPositionPixel;
            }
            internal set
            {
                currentPositionPixel = value;
            }
        }

        /// <summary>
        /// current marker position
        /// </summary>
        public PointLatLng CurrentPosition
        {
            get
            {
                return currentPosition;
            }
            set
            {
                if (!IsDragging)
                {
                    currentPosition = value;
                    CurrentPositionGPixel = Projection.FromLatLngToPixel(value, Zoom);

                    if (started)
                    {
                        GoToCurrentPosition();

                        if (OnCurrentPositionChanged != null)
                            OnCurrentPositionChanged(currentPosition);
                    }
                }
                else
                {
                    currentPosition = value;
                    CurrentPositionGPixel = Projection.FromLatLngToPixel(value, Zoom);

                    if (started)
                    {
                        if (OnCurrentPositionChanged != null)
                            OnCurrentPositionChanged(currentPosition);
                    }
                }
            }
        }

        /// <summary>
        /// for tooltip text padding
        /// </summary>
        public GSize TooltipTextPadding = new GSize(10, 10);

        private MapType mapType;

        public MapType MapType
        {
            get
            {
                return mapType;
            }
            set
            {
                if (value != MapType)
                {
                    mapType = value;

                    switch (value)
                    {
                        case MapType.YandexHybridMap:
                        case MapType.YandexMap:
                        case MapType.YandexSatelliteMap:
                            Projection = new MercatorProjectionYandex();
                            break;
                        default:
                            Projection = new MercatorProjection();
                        break;
                    }
                    
                    minOfTiles = Projection.GetTileMatrixMinXY(Zoom);
                    maxOfTiles = Projection.GetTileMatrixMaxXY(Zoom);
                    CurrentPositionGPixel = Projection.FromLatLngToPixel(CurrentPosition, Zoom);

                    if (started)
                    {
                        CancelAsyncTasks();
                        OnMapSizeChanged(Width, Height);
                        GoToCurrentPosition();
                        ReloadMap();

                        if (OnMapTypeChanged != null)
                        {
                            OnMapTypeChanged(value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// can user drag map
        /// </summary>
        public bool CanDragMap = true;

        /// <summary>
        /// occurs when current position is changed
        /// </summary>
        public event CurrentPositionChanged OnCurrentPositionChanged;

        /// <summary>
        /// occurs when tile set load is complete
        /// </summary>
        public event TileLoadComplete OnTileLoadComplete;

        /// <summary>
        /// occurs when tile set is starting to load
        /// </summary>
        public event TileLoadStart OnTileLoadStart;

        /// <summary>
        /// occurs on tile loaded
        /// </summary>
        public event NeedInvalidation OnNeedInvalidation;

        /// <summary>
        /// occurs on map drag
        /// </summary>
        public event MapDrag OnMapDrag;

        /// <summary>
        /// occurs on map zoom changed
        /// </summary>
        public event MapZoomChanged OnMapZoomChanged;

        /// <summary>
        /// occurs on map type changed
        /// </summary>
        public event MapTypeChanged OnMapTypeChanged;

        public GCore()
        {
            ProcessLoadTaskCallback = ProcessLoadTask;
        }

        /// <summary>
        /// starts core system
        /// </summary>
        public void StartSystem()
        {
            if (!started)
            {
                started = true;

                ReloadMap();
                GoToCurrentPosition();
            }
        }

        public void UpdateCenterTileXYLocation()
        {
            PointLatLng center = FromLocalToLatLng(Width/2, Height/2);
            GPoint centerPixel = Projection.FromLatLngToPixel(center, Zoom);
            centerTileXYLocation = Projection.FromPixelToTileXY(centerPixel);
        }

        public void OnMapSizeChanged(int width, int height)
        {
            Width = width;
            Height = height;

            sizeOfMapArea.Width = 1 + (Width/Projection.TileSize.Width)/2;
            sizeOfMapArea.Height = 1 + (Height/Projection.TileSize.Height)/2;

            UpdateCenterTileXYLocation();

            if (started)
            {
                UpdateBounds();

                if (OnCurrentPositionChanged != null)
                    OnCurrentPositionChanged(currentPosition);
            }
        }

        public void OnMapClose()
        {
            if (waitOnEmptyTasks != null)
            {
                try
                {
                    waitOnEmptyTasks.Set();
                    waitOnEmptyTasks.Close();
                }
                catch
                {
                }
            }

            CancelAsyncTasks();
        }

        /// <summary>
        /// gets current map view top/left coordinate, width in Lng, height in Lat
        /// </summary>
        public RectLatLng CurrentViewArea
        {
            get
            {
                PointLatLng p = Projection.FromPixelToLatLng(-renderOffset.X, -renderOffset.Y, Zoom);
                double rlng = Projection.FromPixelToLatLng(-renderOffset.X + Width, -renderOffset.Y, Zoom).Lng;
                double blat = Projection.FromPixelToLatLng(-renderOffset.X, -renderOffset.Y + Height, Zoom).Lat;

                return RectLatLng.FromLTRB(p.Lng, p.Lat, rlng, blat);
            }
        }

        /// <summary>
        /// gets lat/lng from local control coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public PointLatLng FromLocalToLatLng(int x, int y)
        {
            return Projection.FromPixelToLatLng(new GPoint(x - renderOffset.X, y - renderOffset.Y), Zoom);
        }

        /// <summary>
        /// return local coordinates from lat/lng
        /// </summary>
        /// <param name="latlng"></param>
        /// <returns></returns>
        public GPoint FromLatLngToLocal(PointLatLng latlng)
        {
            GPoint pLocal = Projection.FromLatLngToPixel(latlng, Zoom);
            pLocal.Offset(renderOffset);
            return pLocal;
        }

        /// <summary>
        /// gets max zoom level to fit rectangle
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public int GetMaxZoomToFitRect(RectLatLng rect)
        {
            int zoom = 0;

            for (int i = 1; i <= GMaps.Instance.MaxZoom; i++)
            {
                GPoint p1 = Projection.FromLatLngToPixel(rect.LocationTopLeft, i);
                GPoint p2 = Projection.FromLatLngToPixel(rect.Bottom, rect.Right, i);

                if (((p2.X - p1.X) <= Width + 10) && (p2.Y - p1.Y) <= Height + 10)
                {
                    zoom = i;
                }
                else
                {
                    break;
                }
            }

            return zoom;
        }

        /// <summary>
        /// initiates map dragging
        /// </summary>
        /// <param name="pt"></param>
        public void BeginDrag(GPoint pt)
        {
            dragPoint.X = pt.X - renderOffset.X;
            dragPoint.Y = pt.Y - renderOffset.Y;
            IsDragging = true;
        }

        /// <summary>
        /// ends map dragging
        /// </summary>
        public void EndDrag()
        {
            IsDragging = false;
            if (OnNeedInvalidation != null)
            {
                OnNeedInvalidation();
            }
        }

        /// <summary>
        /// reloads map
        /// </summary>
        public void ReloadMap()
        {
            if (started)
            {
                Debug.WriteLine("------------------");

                lock (tileLoadQueue)
                {
                    tileLoadQueue.Clear();
                    tileLoadQueue.TrimExcess();
                }

                Matrix.Clear();

                if (OnNeedInvalidation != null)
                {
                    OnNeedInvalidation();
                }

                UpdateBounds();
            }
        }

        /// <summary>
        /// moves current position into map center
        /// </summary>
        public void GoToCurrentPosition()
        {
            try
            {
                // reset stuff
                renderOffset = GPoint.Empty;
                centerTileXYLocationLast = GPoint.Empty;
                dragPoint = GPoint.Empty;

                // goto location
                Drag(new GPoint(-(CurrentPositionGPixel.X - Width/2), -(CurrentPositionGPixel.Y - Height/2)));
            }
            catch (Exception e)
            {
                XtraMessageBox.Show( e.Message + "\n" + e.StackTrace, "Error Online Controller", MessageBoxButtons.OK );
            }
        }

        public bool MouseWheelZooming = false;

        /// <summary>
        /// moves current position into map center
        /// </summary>
        internal void GoToCurrentPositionOnZoom()
        {
            // reset stuff
            renderOffset = GPoint.Empty;
            centerTileXYLocationLast = GPoint.Empty;
            dragPoint = GPoint.Empty;

            // goto location and centering
            if (MouseWheelZooming)
            {
                if (MouseWheelZoomType != MouseWheelZoomType.MousePositionWithoutCenter)
                {
                    GPoint pt = new GPoint(-(CurrentPositionGPixel.X - Width/2), -(CurrentPositionGPixel.Y - Height/2));
                    renderOffset.X = pt.X - dragPoint.X;
                    renderOffset.Y = pt.Y - dragPoint.Y;
                }
                else // without centering
                {
                    renderOffset.X = -CurrentPositionGPixel.X - dragPoint.X;
                    renderOffset.Y = -CurrentPositionGPixel.Y - dragPoint.Y;
                    renderOffset.Offset(mouseLastZoom);
                }
            }
            else // use current map center
            {
                mouseLastZoom = GPoint.Empty;

                GPoint pt = new GPoint(-(CurrentPositionGPixel.X - Width/2), -(CurrentPositionGPixel.Y - Height/2));
                renderOffset.X = pt.X - dragPoint.X;
                renderOffset.Y = pt.Y - dragPoint.Y;
            }

            UpdateCenterTileXYLocation();
        }

        /// <summary>
        /// drag map
        /// </summary>
        /// <param name="pt"></param>
        public void Drag(GPoint pt)
        {
            try
            {
                renderOffset.X = pt.X - dragPoint.X;
                renderOffset.Y = pt.Y - dragPoint.Y;

                UpdateCenterTileXYLocation();

                if (centerTileXYLocation != centerTileXYLocationLast)
                {
                    centerTileXYLocationLast = centerTileXYLocation;
                    UpdateBounds();
                }

                if (IsDragging)
                {
                    LastLocationInBounds = CurrentPosition;
                    CurrentPosition = FromLocalToLatLng((int) Width/2, (int) Height/2);
                }

                if (OnNeedInvalidation != null)
                {
                    OnNeedInvalidation();
                }

                if (OnMapDrag != null)
                {
                    OnMapDrag();
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show( e.Message + "\n" + e.StackTrace, "Error Online Controller", MessageBoxButtons.OK );
            }
        }

        /// <summary>
        /// cancels tile loaders and bounds checker
        /// </summary>
        public void CancelAsyncTasks()
        {
            if (started)
            {
                lock (tileLoadQueue)
                {
                    tileLoadQueue.Clear();
                    tileLoadQueue.TrimExcess();
                }
            }
        }

        private Semaphore loaderLimit = new Semaphore(5, 5);
        int inc = 0;
        private void ProcessLoadTask(object obj)
        {
            if (loaderLimit.WaitOne(GMaps.Instance.Timeout, false))
            {
                bool process = true;
                bool last = false;

                LoadTask task = new LoadTask();

                lock (tileLoadQueue)
                {
                    if (tileLoadQueue.Count > 0)
                    {
                        task = tileLoadQueue.Dequeue();
                        tileLoadQueue.TrimExcess();
                        {
                            last = tileLoadQueue.Count == 0;
                        }
                    }
                    else
                    {
                        process = false;
                    }
                }

                if (process)
                {
                    try
                    {
                        Tile t = new Tile(task.Zoom, task.Pos);
                        {
                            MapType[] layers = GMaps.GetAllLayersOfType(MapType);
                            int retry = 0;

                            do
                            {
                                lock (t.Overlays)
                                {
                                    t.Overlays.Clear();
                                }

                                foreach (MapType tl in layers)
                                {
                                    PureImage img = GMaps.Instance.GetImageFrom(tl, task.Pos, task.Zoom);

                                    if (img != null)
                                    {
                                        lock (t.Overlays)
                                        {
                                            t.Overlays.Add(img);
                                        }
                                        retry = 0;
                                    }
                                    else
                                    {
                                        Debug.WriteLine("ProcessLoadTask: " + task + " -> empty tile, retry " + retry);

                                        if (retry++ > 0)
                                        {
                                            Thread.Sleep(1111);
                                        }
                                        break;
                                    }
                                }
                            } while (retry != 0 && retry < GMaps.Instance.RetryLoadTile);

                            Matrix[task.Pos] = t;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(String.Format("ProcessLoadTask: {0}", ex));
                    }
                    finally
                    {
                        // last buddy cleans stuff ;}
                        if (last)
                        {
                            GMaps.Instance.kiberCacheLock.AcquireWriterLock(-1);
                            try
                            {
                                GMaps.Instance.TilesInMemory.RemoveMemoryOverload();
                            }
                            finally
                            {
                                GMaps.Instance.kiberCacheLock.ReleaseWriterLock();
                            }

                            lock (tileDrawingList)
                            {
                                Matrix.ClearPointsNotIn(ref tileDrawingList);
                            }

                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            GC.Collect();

                            if (OnTileLoadComplete != null)
                            {
                                OnTileLoadComplete();
                            }
                        }

                        if (OnNeedInvalidation != null)
                        {
                            OnNeedInvalidation();
                        }
                    }
                }
                loaderLimit.Release();
            }
        }

        /// <summary>
        /// updates map bounds
        /// </summary>
        private void UpdateBounds()
        {
            lock (tileDrawingList)
            {
                FindTilesAround(ref tileDrawingList);

                if (OnTileLoadStart != null)
                {
                    OnTileLoadStart();
                }

                foreach (GPoint p in tileDrawingList)
                {
                    LoadTask task = new LoadTask(p, Zoom);
                    {
                        lock (tileLoadQueue)
                        {
                            if (!tileLoadQueue.Contains(task))
                            {
                                tileLoadQueue.Enqueue(task);
                                ThreadPool.QueueUserWorkItem(ProcessLoadTaskCallback);
                            }
                        }
                    }
                }
            }

            UpdateGroundResolution();
        }

        /// <summary>
        /// find tiles around to fill screen
        /// </summary>
        private void FindTilesAround(ref List<GPoint> list)
        {
            list.Clear();
            for (int i = -sizeOfMapArea.Width; i <= sizeOfMapArea.Width; i++)
            {
                for (int j = -sizeOfMapArea.Height; j <= sizeOfMapArea.Height; j++)
                {
                    GPoint p = centerTileXYLocation;
                    p.X += i;
                    p.Y += j;

                    if (p.X >= minOfTiles.Width && p.Y >= minOfTiles.Height && p.X <= maxOfTiles.Width &&
                        p.Y <= maxOfTiles.Height)
                    {
                        if (!list.Contains(p))
                        {
                            list.Add(p);
                        }
                    }
                }
            }

            if (GMaps.Instance.ShuffleTilesOnLoad)
            {
                Stuff.Shuffle<GPoint>(list);
            }
        }

        /// <summary>
        /// updates ground resolution info
        /// </summary>
        private void UpdateGroundResolution()
        {
            double rez = Projection.GetGroundResolution(Zoom, CurrentPosition.Lat);
            pxRes100m = (int) (100.0/rez); // 100 meters
            pxRes1000m = (int) (1000.0/rez); // 1km  
            pxRes10km = (int) (10000.0/rez); // 10km
            pxRes100km = (int) (100000.0/rez); // 100km
            pxRes1000km = (int) (1000000.0/rez); // 1000km
            pxRes5000km = (int) (5000000.0/rez); // 5000km
        }

        public void MoveShiftMap(GPoint gPoint)
        {
            PointLatLng center = FromLocalToLatLng( Width / 2, Height / 2 );
            GPoint centerPixel = Projection.FromLatLngToPixel( center, Zoom );
            centerTileXYLocation = Projection.FromPixelToTileXY( centerPixel );

            renderOffset.X = centerPixel.X - dragPoint.X;
            renderOffset.Y = centerPixel.Y - dragPoint.Y;

            {
                centerTileXYLocationLast = centerTileXYLocation;
                UpdateBounds();
            }
        }
    }
}
