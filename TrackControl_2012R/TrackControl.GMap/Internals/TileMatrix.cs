﻿using System.Collections.Generic;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Internals
{
   /// <summary>
   /// matrix for tiles
   /// </summary>
   internal class TileMatrix
   {
      readonly Dictionary<GPoint, Tile> matrix = new Dictionary<GPoint, Tile>(55);
      readonly List<GPoint> removals = new List<GPoint>();

      public void Clear()
      {
         lock(matrix)
         {
            foreach(Tile t in matrix.Values)
            {
               t.Clear();
            }
            matrix.Clear();
         }
      }       

      public void ClearPointsNotIn(ref List<GPoint> list)
      {
         removals.Clear();
         lock(matrix)
         {
            foreach(GPoint p in matrix.Keys)
            {
               if(!list.Contains(p))
               {
                  removals.Add(p);
               }
            }
         }

         foreach(GPoint p in removals)
         {
            Tile t = this[p];
            if(t != null)
            {
               lock(matrix)
               {
                  t.Clear();
                  t = null;

                  matrix.Remove(p);
               }
            }
         }
         removals.Clear();
      }

      public Tile this[GPoint p]
      {
         get
         {
            lock(matrix)
            {
               Tile ret = null;
               if(matrix.TryGetValue(p, out ret))
               {
                  return ret;
               }
               else
               {
                  return null;
               }
            }
         }

         set
         {
            lock(matrix)
            {
               matrix[p] = value;
            }
         }
      }
   }
}
