﻿using System;
using System.Collections.Generic;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Internals
{
   /// <summary>
   /// represent tile
   /// </summary>
   internal class Tile
   {
      GPoint _position;
      int _zoom;
      public readonly List<PureImage> Overlays = new List<PureImage>(1);

      public Tile(int zoom, GPoint position)
      {
         _zoom = zoom;
         _position = position;
      }

      public void Clear()
      {
         lock(Overlays)
         {
           Overlays.ForEach(delegate(PureImage i)
           {
             i.Dispose();
           });

            Overlays.Clear();
            Overlays.TrimExcess();
         }
      }

      public int Zoom
      {
         get { return _zoom; }
      }

      public GPoint Pos
      {
         get { return _position; }
      }
   }
}