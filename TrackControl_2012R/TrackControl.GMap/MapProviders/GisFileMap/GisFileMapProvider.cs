﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using TrackControl.General;
using TrackControl.General.Patameters;
using TrackControl.GMap.Core;
using TrackControl.GMap.Internals;
using TrackControl.GMap.Projections;

namespace TrackControl.GMap.MapProviders.GisFileMap
{
    /// <summary>
    /// GisFileMap provider
    /// </summary>
    public class GisFileMapProvider : GoogleHybridMapProvider //GMapProvider, GeocodingProvider
    {
        public static readonly GisFileMapProvider Instance;

        public GisFileMapProvider()
        {
            SetStartParams();
        }

        GMapProvider[] overlays;
        public override GMapProvider[] Overlays
        {
            get
            {
                if (overlays == null)
                {
                    overlays = new GMapProvider[] { GoogleHybridMapProvider.Instance, this };
                }
                return overlays;
            }
        }

        static GisFileMapProvider()
        {
            try
            {
                Instance = new GisFileMapProvider();
            }
            catch (Exception ex)
            {
                Instance = null;
            }
        }

        static readonly string ReverseGeocoderUrlFormat = "";

        public GeoCoderStatusCode GetPoints(string keywords, out List<General.PointLatLng> pointList)
        {
            throw new NotImplementedException();
        }

        public General.PointLatLng? GetPoint(string keywords, out GeoCoderStatusCode status)
        {
            throw new NotImplementedException();
        }

        public GeoCoderStatusCode GetPlacemarks(General.PointLatLng location, out List<Placemark> placemarkList)
        {
            throw new NotImplementedException();
        }

        public Placemark GetPlacemark(General.PointLatLng location, out GeoCoderStatusCode status)
        {
            return GetPlacemarkFromReverseGeocoderUrl(MakeReverseGeocoderUrl(location), out status);
        }

        string MakeReverseGeocoderUrl(PointLatLng pt)
        {
            var culture = CultureInfo.InvariantCulture;
            return string.Format(culture, ReverseGeocoderUrlFormat, pt.Lng, pt.Lat, Lang);
        }

        Placemark GetPlacemarkFromReverseGeocoderUrl(string url, out GeoCoderStatusCode status)
        {
            status = GeoCoderStatusCode.Unknow;
            Placemark ret = null;
            if (!_isActive) 
                return ret; 
            try
            {
               // to do wthis
            }
            catch (Exception ex)
            {
                ret = null;
                status = GeoCoderStatusCode.ExceptionInCode;
                Debug.WriteLine("GetPlacemarkFromReverseGeocoderUrl: " + ex);
            }

            return ret;
        }

        readonly Guid id = Guid.NewGuid();

        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        readonly string name = "GisFile";
        public override string Name
        {
            get
            {
                return name;
            }
        }

        public override PureProjection Projection
        {
            get
            {
                return MercatorProjection.Instance;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            string url = MakeTileImageUrl(pos, zoom, LanguageStr);
            return GetTileImageUsingHttp(url);
        }

        string MakeTileImageUrl(GPoint pos, int zoom, string language)
        {
            string sec1 = string.Empty; // after &x=...
            string sec2 = string.Empty; // after &zoom=...
            //GetSecureWords(pos, out sec1, out sec2);
            string urlstr = string.Format(UrlFormat, layer, zoom, pos.X, pos.Y);
            return urlstr;
        }

        string _lang = "ru";
        public string Lang
        {
            get { return _lang; }
            set { _lang = value; }
        }

        string _server = "gisfile.com";

        bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public void SetStartParams()
        {
            TotalParamsProvider provParams = new TotalParamsProvider();
            _isActive = provParams.IsGisFileUse;
            _lang = provParams.MapsLang;
            _server = provParams.GisFileServer;
        }

        static readonly string UrlFormatServer = "";
        static readonly string UrlFormatRequest = "";
        static readonly string UrlFormat = "http://gisfile.com/layer/{0}/{1}/{2}/{3}.png";
        static readonly string layer = "cadmap";
    }
}
