﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Internals;
using TrackControl.GMap.MapProviders.GisFileMap;
using TrackControl.GMap.Projections;
using TrackControl.GMap.Properties;

namespace TrackControl.GMap.MapProviders
{

    /// <summary>
    /// providers that are already build in
    /// </summary>
    public class GMapProviders
    {
        static GMapProviders()
        {
        }

        GMapProviders()
        {
        }

        public static readonly EmptyProvider EmptyProvider = EmptyProvider.Instance;

        private static readonly OpenStreetMapProvider OpenStreetMap = OpenStreetMapProvider.Instance;
        //public static readonly OpenStreetOsmProvider OpenStreetOsm = OpenStreetOsmProvider.Instance;
        //public static readonly OpenCycleMapProvider OpenCycleMap = OpenCycleMapProvider.Instance;
        //public static readonly OpenStreetMapSurferProvider OpenStreetMapSurfer = OpenStreetMapSurferProvider.Instance;
        //public static readonly OpenStreetMapSurferTerrainProvider OpenStreetMapSurferTerrain = OpenStreetMapSurferTerrainProvider.Instance;
        //public static readonly OpenSeaMapHybridProvider OpenSeaMapHybrid = OpenSeaMapHybridProvider.Instance;

        //public static readonly WikiMapiaMapProvider WikiMapiaMap = WikiMapiaMapProvider.Instance;

        //public static readonly BingMapProvider BingMap = BingMapProvider.Instance;
        //public static readonly BingSatelliteMapProvider BingSatelliteMap = BingSatelliteMapProvider.Instance;
        //public static readonly BingHybridMapProvider BingHybridMap = BingHybridMapProvider.Instance;

        //public static readonly YahooMapProvider YahooMap = YahooMapProvider.Instance;
        //public static readonly YahooSatelliteMapProvider YahooSatelliteMap = YahooSatelliteMapProvider.Instance;
        //public static readonly YahooHybridMapProvider YahooHybridMap = YahooHybridMapProvider.Instance;

        //private static readonly LuxenaProvider Luxena = LuxenaProvider.Instance;
        private static readonly RcsNominatimProvider RcsNominatim = RcsNominatimProvider.Instance;
        public static readonly GisFileMapProvider GisFileMap = GisFileMapProvider.Instance;

        public static readonly GoogleMapProvider GoogleMap = GoogleMapProvider.Instance;
        public static readonly GoogleSatelliteMapProvider GoogleSatelliteMap = GoogleSatelliteMapProvider.Instance;
        public static readonly GoogleHybridMapProvider GoogleHybridMap = GoogleHybridMapProvider.Instance;
        //public static readonly GoogleTerrainMapProvider GoogleTerrainMap = GoogleTerrainMapProvider.Instance;

        //public static readonly GoogleChinaMapProvider GoogleChinaMap = GoogleChinaMapProvider.Instance;
        //public static readonly GoogleChinaSatelliteMapProvider GoogleChinaSatelliteMap = GoogleChinaSatelliteMapProvider.Instance;
        //public static readonly GoogleChinaHybridMapProvider GoogleChinaHybridMap = GoogleChinaHybridMapProvider.Instance;
        //public static readonly GoogleChinaTerrainMapProvider GoogleChinaTerrainMap = GoogleChinaTerrainMapProvider.Instance;

        //public static readonly GoogleKoreaMapProvider GoogleKoreaMap = GoogleKoreaMapProvider.Instance;
        //public static readonly GoogleKoreaSatelliteMapProvider GoogleKoreaSatelliteMap = GoogleKoreaSatelliteMapProvider.Instance;
        //public static readonly GoogleKoreaHybridMapProvider GoogleKoreaHybridMap = GoogleKoreaHybridMapProvider.Instance;

        //public static readonly NearMapProvider NearMap = NearMapProvider.Instance;
        //public static readonly NearSatelliteMapProvider NearSatelliteMap = NearSatelliteMapProvider.Instance;
        //public static readonly NearHybridMapProvider NearHybridMap = NearHybridMapProvider.Instance;

        //public static readonly OviMapProvider OviMap = OviMapProvider.Instance;
        //public static readonly OviSatelliteMapProvider OviSatelliteMap = OviSatelliteMapProvider.Instance;
        //public static readonly OviHybridMapProvider OviHybridMap = OviHybridMapProvider.Instance;
        //public static readonly OviTerrainMapProvider OviTerrainMap = OviTerrainMapProvider.Instance;

        public static readonly YandexMapProvider YandexMap = YandexMapProvider.Instance;
        public static readonly YandexSatelliteMapProvider YandexSatelliteMap = YandexSatelliteMapProvider.Instance;
        public static readonly YandexHybridMapProvider YandexHybridMap = YandexHybridMapProvider.Instance;

        //public static readonly LithuaniaMapProvider LithuaniaMap = LithuaniaMapProvider.Instance;
        //public static readonly Lithuania3dMapProvider Lithuania3dMap = Lithuania3dMapProvider.Instance;
        //public static readonly LithuaniaOrtoFotoMapProvider LithuaniaOrtoFotoMap = LithuaniaOrtoFotoMapProvider.Instance;
        //public static readonly LithuaniaOrtoFotoNewMapProvider LithuaniaOrtoFotoNewMap = LithuaniaOrtoFotoNewMapProvider.Instance;
        //public static readonly LithuaniaHybridMapProvider LithuaniaHybridMap = LithuaniaHybridMapProvider.Instance;
        //public static readonly LithuaniaHybridNewMapProvider LithuaniaHybridNewMap = LithuaniaHybridNewMapProvider.Instance;

        //public static readonly LatviaMapProvider LatviaMap = LatviaMapProvider.Instance;

        //public static readonly MapBenderWMSProvider MapBenderWMSdemoMap = MapBenderWMSProvider.Instance;

        //public static readonly TurkeyMapProvider TurkeyMap = TurkeyMapProvider.Instance;

        //public static readonly CloudMadeMapProvider CloudMadeMap = CloudMadeMapProvider.Instance;

        //public static readonly SpainMapProvider SpainMap = SpainMapProvider.Instance;

        //public static readonly CzechMapProvider CzechMap = CzechMapProvider.Instance;
        //public static readonly CzechSatelliteMapProvider CzechSatelliteMap = CzechSatelliteMapProvider.Instance;
        //public static readonly CzechHybridMapProvider CzechHybridMap = CzechHybridMapProvider.Instance;
        //public static readonly CzechTuristMapProvider CzechTuristMap = CzechTuristMapProvider.Instance;
        //public static readonly CzechHistoryMapProvider CzechHistoryMap = CzechHistoryMapProvider.Instance;

        //public static readonly ArcGIS_Imagery_World_2D_MapProvider ArcGIS_Imagery_World_2D_Map = ArcGIS_Imagery_World_2D_MapProvider.Instance;
        //public static readonly ArcGIS_ShadedRelief_World_2D_MapProvider ArcGIS_ShadedRelief_World_2D_Map = ArcGIS_ShadedRelief_World_2D_MapProvider.Instance;
        //public static readonly ArcGIS_StreetMap_World_2D_MapProvider ArcGIS_StreetMap_World_2D_Map = ArcGIS_StreetMap_World_2D_MapProvider.Instance;
        //public static readonly ArcGIS_Topo_US_2D_MapProvider ArcGIS_Topo_US_2D_Map = ArcGIS_Topo_US_2D_MapProvider.Instance;

        //public static readonly ArcGIS_World_Physical_MapProvider ArcGIS_World_Physical_Map = ArcGIS_World_Physical_MapProvider.Instance;
        //public static readonly ArcGIS_World_Shaded_Relief_MapProvider ArcGIS_World_Shaded_Relief_Map = ArcGIS_World_Shaded_Relief_MapProvider.Instance;
        //public static readonly ArcGIS_World_Street_MapProvider ArcGIS_World_Street_Map = ArcGIS_World_Street_MapProvider.Instance;
        //public static readonly ArcGIS_World_Terrain_Base_MapProvider ArcGIS_World_Terrain_Base_Map = ArcGIS_World_Terrain_Base_MapProvider.Instance;
        //public static readonly ArcGIS_World_Topo_MapProvider ArcGIS_World_Topo_Map = ArcGIS_World_Topo_MapProvider.Instance;

        //public static readonly ArcGIS_DarbAE_Q2_2011_NAVTQ_Eng_V5_MapProvider ArcGIS_DarbAE_Q2_2011_NAVTQ_Eng_V5_Map = ArcGIS_DarbAE_Q2_2011_NAVTQ_Eng_V5_MapProvider.Instance;

        static List<GMapProvider> list;
        static List<GMapProvider> listActive;

        //public static LuxenaProvider getLuxena 
        //{
        //    get
        //    {
        //        return Luxena;
        //    }
        //}

        public static OpenStreetMapProvider getOpenStreetMap 
        {
            get
            {
                return OpenStreetMap;
            }
        }

        public static RcsNominatimProvider getRcsNominatim
        {
            get
            {
                return RcsNominatim;
            }
        }

        public static GisFileMapProvider getGisFileMap
        {
            get
            {
                return GisFileMap;
            }
        }

        public static void GenerateProvider()
        {
            OpenStreetMap.SetStartParams();
            RcsNominatim.SetStartParams();
            GoogleMap.SetStartParams();
            GisFileMap.SetStartParams();
        }

        public static bool isActiveProvider()
        {
            if ((OpenStreetMap != null && OpenStreetMap.IsActive) || (RcsNominatim != null && RcsNominatim.IsActive) ||
                (GoogleMap != null && GoogleMap.IsActive) || (GisFileMap != null && GisFileMap.IsActive))
                return true;
            return false;
        }

        public static Placemark getActiveProvider(PointLatLng point, out GeoCoderStatusCode status)
        {
            if (OpenStreetMap != null && OpenStreetMap.IsActive)
                return OpenStreetMap.GetPlacemarkOSM(point, out status);
            else if (RcsNominatim != null && RcsNominatim.IsActive)
                return RcsNominatim.GetPlacemark(point, out status);
            else if (GoogleMap != null && GoogleMap.IsActive)
                return GoogleMap.GetPlacemark(point, out status);
            else if (GisFileMap != null && GisFileMap.IsActive)
                return GisFileMap.GetPlacemark(point, out status);
            else
            {
                status = GeoCoderStatusCode.Unknow;
            }
            
            return null;
        }

        public static void SetStartParams()
        {
            try
            {
                GMapProvider.UrlWithErrors.Clear();
            }
            catch (Exception ex)
            {
                // to do
            }

            try
            {
                foreach (var gMapProvider in ListActiveProviders)
                {
                    gMapProvider.SetStartParams();
                }
            }
            catch (Exception ex)
            {
                // to do
            }
            //OpenStreetMap.SetStartParams();
            //RcsNominatim.SetStartParams();
            //GoogleMap.SetStartParams();
        }
        
        public static YandexMapProvider getYandexStreetMap
        {
            get { return YandexMap; }
        }

        public static List<GMapProvider> ListActiveProviders
        {
            get
            {
                if (listActive == null)
                {
                    listActive = new List<GMapProvider>();
                    listActive.Add(OpenStreetMap);
                    listActive.Add(RcsNominatim);
                    listActive.Add(GoogleMap);
                    listActive.Add(GisFileMap);
                }
                return listActive;
            }
        }

        /// <summary>
        /// get all instances of the supported providers
        /// </summary>
        public static List<GMapProvider> List
        {
            get
            {
                if (list == null)
                {
                    list = new List<GMapProvider>();

                    Type type = typeof(GMapProviders);
                    foreach (var p in type.GetFields())
                    {
                        var v = p.GetValue(null) as GMapProvider; // static classes cannot be instanced, so use null...
                        if (v != null)
                        {
                            list.Add(v);
                        }
                    }
                }
                return list;
            }
        }

        static Dictionary<Guid, GMapProvider> hash;

        /// <summary>
        /// get hash table of all instances of the supported providers
        /// </summary>
        public static Dictionary<Guid, GMapProvider> Hash
        {
            get
            {
                if (hash == null)
                {
                    hash = new Dictionary<Guid, GMapProvider>();

                    foreach (var p in List)
                    {
                        hash.Add(p.Id, p);
                    }
                }
                return hash;
            }
        }
    }

    /// <summary>
    /// base class for each map provider
    /// </summary>
    public abstract class GMapProvider
    {
        /// <summary>
        /// unique provider id
        /// </summary>
        public abstract Guid Id
        {
            get;
        }

        /// <summary>
        /// provider name
        /// </summary>
        public abstract string Name
        {
            get;
        }

        /// <summary>
        /// provider projection
        /// </summary>
        public abstract PureProjection Projection
        {
            get;
        }

        /// <summary>
        /// provider overlays
        /// </summary>
        public abstract GMapProvider[] Overlays
        {
            get;
        }

        /// <summary>
        /// gets tile image using implmented provider
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public abstract PureImage GetTileImage(GPoint pos, int zoom);

        static readonly List<GMapProvider> MapProviders = new List<GMapProvider>();
        static readonly SHA1CryptoServiceProvider HashProvider = new SHA1CryptoServiceProvider();

        protected GMapProvider()
        {
            DbId = Math.Abs(BitConverter.ToInt32(HashProvider.ComputeHash(Id.ToByteArray()), 0));

            if (MapProviders.Exists(p => p.Id == Id || p.DbId == DbId))
            {
                throw new Exception("such provider id already exsists, try regenerate your provider guid...");
            }
            MapProviders.Add(this);
        }

        static GMapProvider()
        {
            WebProxy = EmptyWebProxy.Instance;
        }

        bool isInitialized = false;

        /// <summary>
        /// was provider initialized
        /// </summary>
        public bool IsInitialized
        {
            get
            {
                return isInitialized;
            }
            internal set
            {
                isInitialized = value;
            }
        }

        /// <summary>
        /// called before first use
        /// </summary>
        public virtual void OnInitialized()
        {
            // nice place to detect current provider version
        }

        /// <summary>
        /// id for database, a hash of provider guid
        /// </summary>
        public readonly int DbId;

        /// <summary>
        /// area of map
        /// </summary>
        public RectLatLng? Area;

        /// <summary>
        /// minimum level of zoom
        /// </summary>
        public int MinZoom;

        /// <summary>
        /// maximum level of zoom
        /// </summary>
        public int? MaxZoom = 17;

        /// <summary>
        /// proxy for net access
        /// </summary>
        public static IWebProxy WebProxy;

        /// <summary>
        /// NetworkCredential for tile http access
        /// </summary>
        public static ICredentials Credential;

        /// <summary>
        /// Gets or sets the value of the User-agent HTTP header.
        /// It's pseudo-randomized to avoid blockages...
        /// </summary>                  
        public static string UserAgent = string.Format("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:{0}.0) Gecko/{2}{3:00}{4:00} Firefox/{0}.0.{1}", Stuff.random.Next(3, 14), Stuff.random.Next(1, 10), Stuff.random.Next(DateTime.Today.Year - 4, DateTime.Today.Year), Stuff.random.Next(12), Stuff.random.Next(30));

        /// <summary>
        /// timeout for provider connections
        /// </summary>
        public static int TimeoutMs = 22 * 1000;

        /// <summary>
        /// Gets or sets the value of the Referer HTTP header.
        /// </summary>
        public string RefererUrl = string.Empty;

        public string Copyright = string.Empty;

        public static PointLatLng PointUrl;

        static string languageStr = "ru";

        public static string LanguageStr
        {
            get
            {
                return languageStr;
            }
            set
            {
                languageStr = value;
                GMaps.Instance.LanguageStr = value;
            }
        }

        static LanguageType language = LanguageType.Russian;

        public static List<string> UrlWithErrors = new List<string>(); 

        /// <summary>
        /// map language
        /// </summary>
        public static LanguageType Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
                languageStr = Stuff.EnumToString(Language);
            }
        }

        /// <summary>
        /// internal proxy for image managment
        /// </summary>
        public static PureImageProxy TileImageProxy;

        static readonly string requestAccept = "*/*";
        static readonly string responseContentType = "image";

        protected virtual bool CheckTileImageHttpResponse(HttpWebResponse response)
        {
            //Debug.WriteLine(response.StatusCode + "/" + response.StatusDescription + "/" + response.ContentType + " -> " + response.ResponseUri);
            return response.ContentType.Contains(responseContentType);
        }

        public PureImage GetTileImageUsingHttp(string url)
        {
            PureImage ret = null;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (WebProxy != null)
            {
                request.Proxy = WebProxy;
            }

            if (Credential != null)
            {
                request.PreAuthenticate = true;
                request.Credentials = Credential;
            }

            request.UserAgent = UserAgent;
            request.Timeout = TimeoutMs;
            request.ReadWriteTimeout = TimeoutMs * 6;
            request.Accept = requestAccept;
            request.Referer = RefererUrl;

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (CheckTileImageHttpResponse(response))
                {
                    MemoryStream responseStream = Stuff.CopyStream(response.GetResponseStream(), false);
                    {
                        Debug.WriteLine("Response[" + responseStream.Length + " bytes]: " + url);

                        if (responseStream.Length > 0)
                        {
                            ret = TileImageProxy.FromStream(responseStream);
                        }

                        if (ret != null)
                        {
                            ret.Data = responseStream;
                            ret.Data.Position = 0;
                        }
                        else
                        {
                            responseStream.Dispose();
                        }
                    }
                    responseStream = null;
                }
                else
                {
                    Debug.WriteLine("CheckTileImageHttpResponse[false]: " + url);
                }
                response.Close();
            }
            return ret;
        }

        public string GetContentUsingHttp(string url)
        {
            try
            {
                string ret = string.Empty;
                if (UrlWithErrors.Count > 0)
                {
                    string[] urlPasrts = url.Split('/');
                    if (urlPasrts.Length > 1)
                    {
                        if (UrlWithErrors.Contains(urlPasrts[2]))
                        {
                            return "";
                        }
                    }
                }

                if (url.Contains("https"))
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.DefaultConnectionLimit = 9999; ;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0x30 | 0xC0 | 0x300 | 0xC00);
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                if (WebProxy != null)
                {
                    request.Proxy = WebProxy;
                }

                if (Credential != null)
                {
                    request.PreAuthenticate = true;
                    request.Credentials = Credential;
                }

                request.UserAgent = UserAgent;
                request.Timeout = TimeoutMs;
                request.ReadWriteTimeout = TimeoutMs * 6;
                request.Accept = requestAccept;
                request.Referer = RefererUrl;

                HttpWebResponse response = null;
                try
                {
                    response = request.GetResponse() as HttpWebResponse;
                }
                catch (Exception ee)
                {
                    XtraMessageBox.Show(ee.Message, "Error HttpWebRequest", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw ee;
                }


                if(response != null)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            using (StreamReader read = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                ret = read.ReadToEnd();
                            }
                        }
                    }

                    response.Close();
                }


                return ret;
            }
            catch (Exception ex)
            {
                string[] urlPasrts = url.Split('/');
                if (urlPasrts.Length > 1)
                {
                    XtraMessageBox.Show(string.Format("{0} {1}", ex.Message, urlPasrts[2]), "Error GetContentUsingHttp", MessageBoxButtons.OK);
                    UrlWithErrors.Add(urlPasrts[2]);
                }
                return "";
            }
        }

        public static int GetServerNum(GPoint pos, int max)
        {
            return (int)(pos.X + 2 * pos.Y) % max;
        }

        public override int GetHashCode()
        {
            return (int)DbId;
        }

        public override bool Equals(object obj)
        {
            if (obj is GMapProvider)
            {
                return Id.Equals((obj as GMapProvider).Id);
            }
            return false;
        }

        public static GMapProvider TryGetProvider(Guid id)
        {
            GMapProvider ret;
            if (GMapProviders.Hash.TryGetValue(id, out ret))
            {
                return ret;
            }
            return null;
        }

        public override string ToString()
        {
            return Name;
        }

        public virtual void SetStartParams() // virtual
        {
            // to do
        }
    }

    /// <summary>
    /// represents empty provider
    /// </summary>
    public class EmptyProvider : GMapProvider
    {
        public static readonly EmptyProvider Instance;

        EmptyProvider()
        {
            MaxZoom = null;
        }

        static EmptyProvider()
        {
            Instance = new EmptyProvider();
        }

        #region GMapProvider Members

        public override Guid Id
        {
            get
            {
                return Guid.Empty;
            }
        }

        readonly string name = "None";

        public override string Name
        {
            get
            {
                return name;
            }
        }

        readonly MercatorProjection projection = MercatorProjection.Instance;

        public override PureProjection Projection
        {
            get
            {
                return projection;
            }
        }

        public override GMapProvider[] Overlays
        {
            get
            {
                return null;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            return null;
        }

        #endregion
    }

    public sealed class EmptyWebProxy : IWebProxy
    {
        public static readonly EmptyWebProxy Instance = new EmptyWebProxy();

        private ICredentials m_credentials;

        public ICredentials Credentials
        {
            get
            {
                return this.m_credentials;
            }
            set
            {
                this.m_credentials = value;
            }
        }

        public Uri GetProxy(Uri uri)
        {
            return uri;
        }

        public bool IsBypassed(Uri uri)
        {
            return true;
        }
    }
}
