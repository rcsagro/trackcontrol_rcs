﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using TrackControl.General;
using TrackControl.General.Patameters;
using TrackControl.GMap.Core;
using TrackControl.GMap.Internals;

namespace TrackControl.GMap.MapProviders
{
    public class RcsNominatimProvider : GMapProvider, GeocodingProvider
    {
        public static readonly RcsNominatimProvider Instance;

        RcsNominatimProvider()
        {
            SetStartParams();
        }

        static RcsNominatimProvider()
        {
            try
            {
                Instance = new RcsNominatimProvider();
            }
            catch (Exception ex)
            {
                Instance = null;
            }
        }

        static readonly string ReverseGeocoderUrlFormat = "http://gismapnew.teletrack.ua/nominatim/reverse.php?format=xml&lat={0}&lon={1}&accept-language={2}";

        #region Члены GeocodingProvider

        public GeoCoderStatusCode GetPoints(string keywords, out List<General.PointLatLng> pointList)
        {
            throw new NotImplementedException();
        }

        public General.PointLatLng? GetPoint(string keywords, out GeoCoderStatusCode status)
        {
            throw new NotImplementedException();
        }

        public GeoCoderStatusCode GetPlacemarks(General.PointLatLng location, out List<Placemark> placemarkList)
        {
            throw new NotImplementedException();
        }

        public Placemark GetPlacemark(General.PointLatLng location, out GeoCoderStatusCode status)
        {
            return GetPlacemarkFromReverseGeocoderUrl(MakeReverseGeocoderUrl(location), out status);
        }

        string MakeReverseGeocoderUrl(PointLatLng pt)
        {
            return string.Format(CultureInfo.InvariantCulture, ReverseGeocoderUrlFormat, pt.Lat, pt.Lng, Lang);
        }

        Placemark GetPlacemarkFromReverseGeocoderUrl(string url, out GeoCoderStatusCode status)
        {
            status = GeoCoderStatusCode.Unknow;
            Placemark ret = null;
            if (!_isActive) return ret; 
            try
            {
                string geo = GMaps.Instance.UsePlacemarkCache ? Cache.Instance.GetContent(url, CacheType.PlacemarkCache) : string.Empty;

                bool cache = false;

                if (string.IsNullOrEmpty(geo))
                {
                    geo = GetContentUsingHttp(url);

                    if (!string.IsNullOrEmpty(geo))
                    {
                        cache = true;
                    }
                }

                if (!string.IsNullOrEmpty(geo))
                {
                    if (geo.StartsWith("<?xml version") && geo.Contains("<result"))
                    {
                        if (cache && GMaps.Instance.UsePlacemarkCache)
                        {
                            Cache.Instance.SaveContent(url, CacheType.PlacemarkCache, geo);
                        }

                        string address = "";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(geo);
                        {
                            XmlNodeList items = doc.GetElementsByTagName("result");
                            for (int i = 0; i < items.Count ; i++)
                            {
                                if (address.Length == 0)
                                {
                                    address = items[i].InnerText;
                                }
                                else
                                {
                                    address = string.Format("{0},{1}", address, items[i].InnerText);
                                }
                            }
                            ret = new Placemark(address);
                            status = GeoCoderStatusCode.G_GEO_SUCCESS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                status = GeoCoderStatusCode.ExceptionInCode;
                Debug.WriteLine("GetPlacemarkFromReverseGeocoderUrl: " + ex);
            }

            return ret;
        }
        #endregion

        readonly Guid id = new Guid("bb1be286-9e40-4879-8916-d5e7463ef021");
        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        readonly string name = "RcsNominatimProvider";
        public override string Name
        {
            get
            {
                return name;
            }
        }

        public override PureProjection Projection
        {
            get { throw new NotImplementedException(); }
        }

        public override GMapProvider[] Overlays
        {
            get { throw new NotImplementedException(); }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            throw new NotImplementedException();
        }

        string _lang = "uk";

        public string Lang
        {
            get { return _lang; }
            set { _lang = value; }
        }

        string _server = "gismapnew.teletrack.ua";


        bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public override void SetStartParams()
        {
            TotalParamsProvider provParams = new TotalParamsProvider();
            _isActive = provParams.IsRcsNominatimUse;
            _lang = provParams.MapsLang;
        }

        //static readonly string RoutingUrlFormat = "http://www.yournavigation.org/api/1.0/gosmore.php?format=kml&flat={0}&flon={1}&tlat={2}&tlon={3}&v={4}&fast=0&layer=mapnik";
        static readonly string RoutingUrlFormat = "http://gismapnew.teletrack.ua/nominatim/reverse.php?format=kml&lat={0}&lon={1}&accept-language=ru";

        static readonly string TravelTypeFoot = "foot";
        static readonly string TravelTypeMotorCar = "motorcar";

        static readonly string WalkingStr = "Walking";
        static readonly string DrivingStr = "Driving";

        string MakeRoutingUrl(PointLatLng start, PointLatLng end, string travelType)
        {
            return string.Format(CultureInfo.InvariantCulture, RoutingUrlFormat, start.Lat, start.Lng, end.Lat, end.Lng, travelType);
        }

        List<PointLatLng> GetRoutePoints(string url)
        {
            List<PointLatLng> points = null;
            try
            {
                string route = GMaps.Instance.UseRouteCache ? Cache.Instance.GetContent(url, CacheType.RouteCache) : string.Empty;
                if (string.IsNullOrEmpty(route))
                {
                    route = GetContentUsingHttp(url);
                    if (!string.IsNullOrEmpty(route))
                    {
                        if (GMaps.Instance.UseRouteCache)
                        {
                            Cache.Instance.SaveContent(url, CacheType.RouteCache, route);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(route))
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(route);
                    System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmldoc.NameTable);
                    xmlnsManager.AddNamespace("sm", "http://earth.google.com/kml/2.0");

                    ///Folder/Placemark/LineString/coordinates
                    var coordNode = xmldoc.SelectSingleNode("/sm:kml/sm:Document/sm:Folder/sm:Placemark/sm:LineString/sm:coordinates", xmlnsManager);

                    string[] coordinates = coordNode.InnerText.Split('\n');

                    if (coordinates.Length > 0)
                    {
                        points = new List<PointLatLng>();

                        foreach (string coordinate in coordinates)
                        {
                            if (coordinate != string.Empty)
                            {
                                string[] XY = coordinate.Split(',');
                                if (XY.Length == 2)
                                {
                                    double lat = double.Parse(XY[1], CultureInfo.InvariantCulture);
                                    double lng = double.Parse(XY[0], CultureInfo.InvariantCulture);
                                    points.Add(new PointLatLng(lat, lng));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetRoutePoints: " + ex);
            }

            return points;
        }

        public MapRoute GetRoute(PointLatLng start, PointLatLng end, bool avoidHighways, bool walkingMode, int Zoom)
        {
            List<PointLatLng> points = GetRoutePoints(MakeRoutingUrl(start, end, walkingMode ? TravelTypeFoot : TravelTypeMotorCar));
            MapRoute route = points != null ? new MapRoute(points, walkingMode ? WalkingStr : DrivingStr) : null;
            return route;
        }
    }
}
