﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.MapProviders
{
    public abstract class YandexMapProviderBase : GMapProvider, RoutingProvider, GeocodingProvider, DirectionsProvider
    {
        protected string Version = "2.44.0";

        public YandexMapProviderBase()
        {
            MaxZoom = null;
            RefererUrl = "http://maps.yandex.ru";
            Copyright = string.Format("© Yandex - Map data {0}", DateTime.Today.Year);
        }

        public override Guid Id
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string Name
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override PureProjection Projection
        {
            get
            {
                return MercatorProjectionYandex.Instance;
            }
        }

        GMapProvider[] overlays;
        public override GMapProvider[] Overlays
        {
            get
            {
                if (overlays == null)
                {
                    overlays = new GMapProvider[] { this };
                }
                return overlays;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            throw new NotImplementedException();
        }

        public MapRoute GetRoute(string start, string end, bool avoidHighways, bool walkingMode, int Zoom)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public MapRoute GetRoute(PointLatLng start, PointLatLng end, bool avoidHighways, bool walkingMode, int Zoom)
        {
            string tooltip;
            int numLevels;
            int zoomFactor;
            MapRoute ret = null;
            List<PointLatLng> points = GetRoutePoints(MakeRouteUrl(start, end, LanguageStr, avoidHighways, walkingMode), Zoom, out tooltip, out numLevels, out zoomFactor);
            if (points != null)
            {
                ret = new MapRoute(points, tooltip);
            }
            return ret;
        }

        private List<PointLatLng> GetRoutePoints(string url, int zoom, out string tooltipHtml, out int numLevel,
            out int zoomFactor)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public GeoCoderStatusCode GetPoints(string keywords, out List<PointLatLng> pointList)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public PointLatLng? GetPoint(string keywords, out GeoCoderStatusCode status)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        private string MakeRouteUrl(PointLatLng start, PointLatLng end, string language, bool avoidHighways, bool walkingMode)
        {
            //string opt = walkingMode ? WalkingStr : (avoidHighways ? RouteWithoutHighwaysStr : RouteStr);
            //return string.Format(CultureInfo.InvariantCulture, RouteUrlFormatPointLatLng, language, opt, start.Lat, start.Lng, end.Lat, end.Lng, Server);
            return "";
        }

        private string MakeGeocoderUrl(string keywords, string language)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public Placemark GetPlacemark(PointLatLng location, out GeoCoderStatusCode status)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public GeoCoderStatusCode GetPlacemarks(PointLatLng location, out List<Placemark> placemarkList)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        private DirectionsStatusCode GetDirectionsUrl(string url, out GDirections direction)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        private string MakeDirectionsUrl(PointLatLng start, PointLatLng end, string language, bool avoidHighways,
            bool avoidTolls, bool walkingMode, bool sensor, bool metric)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        private string MakeDirectionsUrl(string start, string end, string language, bool avoidHighways, bool walkingMode,
            bool avoidTolls, bool sensor, bool metric)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public DirectionsStatusCode GetDirections(out GDirections direction, PointLatLng start, PointLatLng end, bool avoidHighways, bool avoidTolls, bool walkingMode, bool sensor, bool metric)
        {
            return GetDirectionsUrl(MakeDirectionsUrl(start, end, LanguageStr, avoidHighways, avoidTolls, walkingMode, sensor, metric), out direction);
        }

        public DirectionsStatusCode GetDirections(out GDirections direction, string start, string end, bool avoidHighways, bool avoidTolls, bool walkingMode, bool sensor, bool metric)
        {
            return GetDirectionsUrl(MakeDirectionsUrl(start, end, LanguageStr, avoidHighways, avoidTolls, walkingMode, sensor, metric), out direction);
        }

        public IEnumerable<GDirections> GetDirections(out DirectionsStatusCode status, PointLatLng start, PointLatLng end, bool avoidHighways, bool avoidTolls, bool walkingMode, bool sensor, bool metric)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }

        public IEnumerable<GDirections> GetDirections(out DirectionsStatusCode status, string start, string end, bool avoidHighways, bool avoidTolls, bool walkingMode, bool sensor, bool metric)
        {
            // TODO: add alternative directions

            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// YandexMap provider
    /// </summary>
    public class YandexMapProvider : YandexMapProviderBase
    {
        public static readonly YandexMapProvider Instance;

        YandexMapProvider()
        {
            RefererUrl = "http://maps.yandex.ru/";
        }

        static YandexMapProvider()
        {
            Instance = new YandexMapProvider();
        }

        protected string Version = "2.44.0";

        readonly Guid id = new Guid("82DC969D-0491-40F3-8C21-4D90B67F47EB");
        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        readonly string name = "YandexMap";
        public override string Name
        {
            get
            {
                return name;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            string url = MakeTileImageUrl(pos, zoom, LanguageStr);

            return GetTileImageUsingHttp(url);
        }

        string MakeTileImageUrl(GPoint pos, int zoom, string language)
        {
            // http://vec01.maps.yandex.ru/tiles?l=map&v=2.10.2&x=1494&y=650&z=11
            // http://vec03.maps.yandex.net/tiles?l=map&v=2.19.5&x=579&y=326&z=10&g=Gagarin
            // http://vec02.maps.yandex.net/tiles?l=map&v=2.26.0&x=586&y=327&z=10&lang=ru-RU
            // http://vec03.maps.yandex.net/tiles?l=map&v=2.44.0&x=289&y=164&z=9&lang=en_US

            return string.Format(UrlFormat, UrlServer, GetServerNum(pos, 4) + 1, Version, pos.X, pos.Y, zoom, language);
        }

        static readonly string UrlServer = "vec";
        static readonly string UrlFormat = "http://{0}0{1}.maps.yandex.net/tiles?l=map&v={2}&x={3}&y={4}&z={5}&lang={6}";
    }
}
