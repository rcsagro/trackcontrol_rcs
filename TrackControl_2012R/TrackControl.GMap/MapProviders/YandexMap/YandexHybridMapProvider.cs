﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.MapProviders
{
    public class YandexHybridMapProvider : YandexMapProviderBase
    {
        public static readonly YandexHybridMapProvider Instance;

        private YandexHybridMapProvider()
        {
        }

        static YandexHybridMapProvider()
        {
            Instance = new YandexHybridMapProvider();
        }

        private readonly Guid id = new Guid("78A3830F-5EE3-432C-A32E-91B7AF6BBCB9");

        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        private readonly string name = "YandexHybridMap";

        public override string Name
        {
            get
            {
                return name;
            }
        }

        private GMapProvider[] overlays;

        public override GMapProvider[] Overlays
        {
            get
            {
                if (overlays == null)
                {
                    overlays = new GMapProvider[] {YandexSatelliteMapProvider.Instance, this};
                }
                return overlays;
            }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            string url = MakeTileImageUrl(pos, zoom, LanguageStr);

            return GetTileImageUsingHttp(url);
        }

        private string MakeTileImageUrl(GPoint pos, int zoom, string language)
        {
            // http://vec01.maps.yandex.ru/tiles?l=map&v=2.10.2&x=1494&y=650&z=11
            // http://vec03.maps.yandex.net/tiles?l=skl&v=2.26.0&x=4663&y=2610&z=13&lang=ru-RU
            // http://vec03.maps.yandex.net/tiles?l=skl&v=2.44.0&x=1165&y=652&z=11&lang=en_US

            return string.Format(UrlFormat, UrlServer, GetServerNum(pos, 4) + 1, Version, pos.X, pos.Y, zoom, language);
        }

        private static readonly string UrlServer = "vec";
        private static readonly string UrlFormat = "http://{0}0{1}.maps.yandex.net/tiles?l=skl&v={2}&x={3}&y={4}&z={5}&lang={6}";
    }
}
