﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using TrackControl.General;
using TrackControl.General.Patameters;
using TrackControl.GMap.Core;
using TrackControl.GMap.Internals;

namespace TrackControl.GMap.MapProviders
{

    //https://docs.google.com/document/d/1X1J6FrtUfx9SKneUjHYms92mxCcy8z9_wLZi72uB8dw/edit

    public class LuxenaProvider : GMapProvider, GeocodingProvider
    {
        public static readonly LuxenaProvider Instance;

        LuxenaProvider()
        {
            SetStartParams();
        }

        static LuxenaProvider()
        {
            try
            {
                Instance = new LuxenaProvider();
            }
            catch (Exception ex)
            {
                Instance = null;
            }
        }
        
        static readonly string ReverseGeocoderUrlFormat = "http://api.mq.ua/geocode?lonlat={0},{1}&lang={2}&format=xml";

        #region Члены GeocodingProvider

        public GeoCoderStatusCode GetPoints(string keywords, out List<General.PointLatLng> pointList)
        {
            throw new NotImplementedException();
        }

        public General.PointLatLng? GetPoint(string keywords, out GeoCoderStatusCode status)
        {
            throw new NotImplementedException();
        }

        public GeoCoderStatusCode GetPlacemarks(General.PointLatLng location, out List<Placemark> placemarkList)
        {
            throw new NotImplementedException();
        }

        public Placemark GetPlacemark(General.PointLatLng location, out GeoCoderStatusCode status)
        {
            return GetPlacemarkFromReverseGeocoderUrl(MakeReverseGeocoderUrl(location), out status);
        }

        string MakeReverseGeocoderUrl(PointLatLng pt)
        {
            return string.Format(CultureInfo.InvariantCulture, ReverseGeocoderUrlFormat, pt.Lng, pt.Lat, Lang);
        }

        Placemark GetPlacemarkFromReverseGeocoderUrl(string url, out GeoCoderStatusCode status)
        {
            status = GeoCoderStatusCode.Unknow;
            Placemark ret = null;
            if (!_isActive) return ret; 
            try
            {
                string geo = GMaps.Instance.UsePlacemarkCache ? Cache.Instance.GetContent(url, CacheType.PlacemarkCache) : string.Empty;

                bool cache = false;

                if (string.IsNullOrEmpty(geo))
                {
                    geo = GetContentUsingHttp(url);

                    if (!string.IsNullOrEmpty(geo))
                    {
                        cache = true;
                    }
                }

                if (!string.IsNullOrEmpty(geo))
                {
                    if (geo.StartsWith("<Location xmlns") && geo.Contains("<LocationComponent"))
                    {
                        if (cache && GMaps.Instance.UsePlacemarkCache)
                        {
                            Cache.Instance.SaveContent(url, CacheType.PlacemarkCache, geo);
                        }

                        string address = "";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(geo);
                        {
                            XmlNodeList items = doc.GetElementsByTagName("Name");
                            for (int i = 1; i < items.Count ; i++)
                            {
                                if (address.Length == 0)
                                {
                                    address = items[i].InnerText;
                                }
                                else
                                {
                                    address = string.Format("{0},{1}", address, items[i].InnerText);
                                }
                            }
                            ret = new Placemark(address);
                            status = GeoCoderStatusCode.G_GEO_SUCCESS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ret = null;
                status = GeoCoderStatusCode.ExceptionInCode;
                Debug.WriteLine("GetPlacemarkFromReverseGeocoderUrl: " + ex);
            }

            return ret;
        }
        #endregion

        readonly Guid id = new Guid("065F5F9B-3FA0-437F-8E9B-B34330D285AB");
        public override Guid Id
        {
            get
            {
                return id;
            }
        }

        readonly string name = "Luxena";
        public override string Name
        {
            get
            {
                return name;
            }
        }

        public override PureProjection Projection
        {
            get { throw new NotImplementedException(); }
        }

        public override GMapProvider[] Overlays
        {
            get { throw new NotImplementedException(); }
        }

        public override PureImage GetTileImage(GPoint pos, int zoom)
        {
            throw new NotImplementedException();
        }

        string _lang = "uk";

        public string Lang
        {
            get { return _lang; }
            set { _lang = value; }
        }

        string _server = "api.mq.ua";


        bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public void SetStartParams()
        {
            TotalParamsProvider provParams = new TotalParamsProvider();
            _isActive = provParams.IsLuxenaUse;
            _lang = provParams.MapsLang;
            _server = provParams.LuxenaServer; 
        }
    }
}
