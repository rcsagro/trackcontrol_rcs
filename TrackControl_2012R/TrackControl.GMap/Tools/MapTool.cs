using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using TrackControl.GMap.UI;
using System.IO;

namespace TrackControl.GMap.Tools
{
    public abstract class MapTool
    {
        #region --   Cursors   --

        public static Cursor DelCursor
        {
            get { return _delCursor; }
        }

        static Cursor _delCursor;

        public static Cursor NewCursor
        {
            get { return _newCursor; }
        }

        static Cursor _newCursor;

        public static Cursor MoveCursor
        {
            get { return _moveCursor; }
        }

        static Cursor _moveCursor;

        public static Cursor AddCursor
        {
            get { return _addCursor; }
        }

        static Cursor _addCursor;

        static MapTool()
        {
            Bitmap img;
            Assembly assembly = Assembly.GetExecutingAssembly();

            img = getBitmap("TrackControl.GMap.Cursors.Del.png", assembly);
            _delCursor = new Cursor(img.GetHicon());
            img.Dispose();

            img = getBitmap("TrackControl.GMap.Cursors.Move.png", assembly);
            _moveCursor = new Cursor(img.GetHicon());
            img.Dispose();

            img = getBitmap("TrackControl.GMap.Cursors.New.png", assembly);
            _newCursor = new Cursor(img.GetHicon());
            img.Dispose();

            img = getBitmap("TrackControl.GMap.Cursors.Add.png", assembly);
            _addCursor = new Cursor(img.GetHicon());
            img.Dispose();
        }

        static Bitmap getBitmap(string name, Assembly assembly)
        {
            using (Stream stream = assembly.GetManifestResourceStream(name))
            {
                return new Bitmap(stream);
            }
        }

        #endregion

        public abstract void OnMouseDown(GMapControl map, MouseEventArgs e);
        public abstract void OnMouseMove(GMapControl map, MouseEventArgs e);
        public abstract void OnMouseUp(GMapControl map, MouseEventArgs e);

        public virtual void OnMarkerClick(GMapControl map, GMapMarker m)
        {
        }

        public virtual void OnMapDrag(GMapControl map)
        {
        }
    }
}
