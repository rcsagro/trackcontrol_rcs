﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Markers;
using TrackControl.GMap.Properties;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Tools
{
    /// <summary>
    /// линейка измерения расстояния
    /// </summary>
    public class DistanceTool : MapTool
    {
        GMapPolygon _polygon;
        private bool IsMapDrag = false;

        public override void OnMouseDown(GMapControl map, MouseEventArgs e)
        {
            IsMapDrag = false;

            if (map.IsMouseOverMarker) 
                return;

            //PointLatLng point = map.FromLocalToLatLng(e.Location.X, e.Location.Y);
            //if (null == _polygon)
            //{
            //    List<PointLatLng> points = new List<PointLatLng>();
            //    points.Add(point);
            //    _polygon = new GMapPolygon(points, "Disnance", false);
            //    map.ToolLayer.Polygons.Add(_polygon);
            //    AddMarker(map, _polygon.Points, point);
            //}
            //else
            //{
            //    _polygon.Points.Add(point);
            //    _polygon.LocalPoints.Add(new GPoint(e.Location.X, e.Location.Y));
            //    AddMarker(map, _polygon.Points, point);
            //}  
        }

        private void AddMarker(GMapControl map, List<PointLatLng> points, PointLatLng point)
        {
            string title = string.Format("{0}{1}", calculate(points), Resources.KmShort);
            Marker marker = new Marker(MarkerType.DistancePoint, points.Count, point, title, "");
            map.ToolLayer.Markers.Add(GMapMarkerConverter.Convert(marker));
        }

        public override void OnMouseMove(GMapControl map, MouseEventArgs e)
        {
            // to do
        } // OnMouseMove

        public override void OnMapDrag(GMapControl map)
        {
            IsMapDrag = true;
        }

        public override void OnMouseUp(GMapControl map, MouseEventArgs e)
        {
            if (!IsMapDrag)
            {
                PointLatLng point = map.FromLocalToLatLng(e.Location.X, e.Location.Y);

                if (null == _polygon)
                {
                    List<PointLatLng> points = new List<PointLatLng>();
                    points.Add(point);
                    _polygon = new GMapPolygon(points, "Disnance", false);
                    map.ToolLayer.Polygons.Add(_polygon);
                    AddMarker(map, _polygon.Points, point);
                } // if
                else
                {
                    _polygon.Points.Add(point);
                    _polygon.LocalPoints.Add(new GPoint(e.Location.X, e.Location.Y));
                    AddMarker(map, _polygon.Points, point);
                } // else
            } // if
        } // OnMouseUp

        public override void OnMarkerClick(GMapControl map, GMapMarker m)
        {
            if (m is DistancePointMarker)
            {
                int pointNumber = (m as DistancePointMarker).PointNumber;

                if (pointNumber <= _polygon.Points.Count && pointNumber>0)
                {
                    _polygon.Points.Remove(_polygon.Points[pointNumber-1]);
                    _polygon.LocalPoints.Remove(_polygon.LocalPoints[pointNumber-1]);
                }

                map.ToolLayer.Markers.Clear();
                List<PointLatLng> points = new List<PointLatLng>();
                foreach (PointLatLng point in _polygon.Points)
                {
                    points.Add(point);
                    AddMarker(map, points, point);
                }
                map.IsMouseOverMarker = false; 
            }
        }

        internal void Release(GMapControl map)
        {
            if (null != _polygon && null != map)
            {
                map.ToolLayer.Polygons.Clear();
                map.ToolLayer.Markers.Clear();
                _polygon = null;
                map.Cursor = Cursors.Default;
            }
        }

        double calculate(IEnumerable<PointLatLng> points)
        {
            double distance = 0;
            using (IEnumerator<PointLatLng> en = points.GetEnumerator())
            {
                if (en.MoveNext())
                {
                    PointLatLng previous = en.Current;
                    while (en.MoveNext())
                    {
                        PointLatLng current = en.Current;
                        distance += СGeoDistance.GetDistance(previous, current);
                        previous = current;
                    }
                }
            }

            return Math.Round(distance,2);
        }
    }
}
