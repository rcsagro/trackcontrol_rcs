﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.Markers;
using TrackControl.GMap.Properties;
using TrackControl.GMap.UI;

namespace TrackControl.GMap.Tools
{
    public class AreaTool : MapTool
    {
        private GMapPolygon _polygon;
        private bool _isToolForCreateActive = false;
        private bool _isToolForEditActive = false;
        private GMapMarker _markerSelected;
        private int _indexSelected;
        private bool IsMapDrag = false;

        public override void OnMapDrag(GMapControl map)
        {
            IsMapDrag = true;
        }

        public override void OnMouseDown(GMapControl map, MouseEventArgs e)
        {
            IsMapDrag = false;

            if (MouseButtons.Right == e.Button)
            {
                if (_isToolForCreateActive) 
                    RemoveLastPoint(map, e);

                _isToolForCreateActive = false;
                _isToolForEditActive = false;
                if (!map.EditorZonesOn)
                {
                    map.IsZoneEditor = false;
                }

                map.Cursor = Cursors.Default;
            }

            if (null != _polygon && !_isToolForCreateActive)
            {
                return;
            }

            var points = new List<PointLatLng>();
            PointLatLng point = map.FromLocalToLatLng(e.Location.X, e.Location.Y);

            if (null == _polygon)
            {
                CreatePoligon(map, points, point);
                map.IsZoneEditor = true;
            }
            else
            {
                AddPointToPoligon(map, e, point);
            }
        }

        private void AddPointToPoligon(GMapControl map, MouseEventArgs e, PointLatLng point)
        {
            _polygon.Points[_polygon.Points.Count - 1] = map.FromLocalToLatLng(e.Location.X, e.Location.Y);
            _polygon.Points.Add(point);
            _polygon.LocalPoints.Add(new GPoint(e.Location.X, e.Location.Y));
            AddMarker(map, _polygon.Points, point);
        }

        private void CreatePoligon(GMapControl map, List<PointLatLng> points, PointLatLng point)
        {
            // одна лишняя точка для следующей за мышью вершины (тянется площадь) 
            // при завершении создания полигона лишняя точка удаляется
            points.Add(point);
            points.Add(point);
            _polygon = new GMapPolygon(points, "Area", true);
            map.ToolLayer.Polygons.Add(_polygon);
            AddMarker(map, _polygon.Points, point);
            _isToolForCreateActive = true;
        }

        private void RemoveLastPoint(GMapControl map, MouseEventArgs e)
        {
            _polygon.Points.Remove(_polygon.Points[_polygon.Points.Count - 1]);
            _polygon.LocalPoints.Remove(_polygon.LocalPoints[_polygon.LocalPoints.Count - 1]);
            //_polygon.Points[_polygon.Points.Count - 1] = map.FromLocalToLatLng(e.Location.X, e.Location.Y);
            //_polygon.Points[_polygon.Points.Count - 1] = _polygon.Points[0];

            map.Refresh();
        }

        private void AddMarker(GMapControl map, List<PointLatLng> points, PointLatLng point)
        {
            foreach (GMapMarker gmapMarker in map.ToolLayer.Markers)
            {
                gmapMarker.ToolTipText = "";
            }

            string title = "";

            if (_polygon.Points.Count > 2)
            {
                double area = calculate(points);
                if( area > 0 )
                {
                    title = string.Format( " {0} {1}", area * 100.0, Resources.GaSq /*, area, Resources.KmSq*/ );
                }
            }
                        
            Marker marker = new Marker(MarkerType.AreaPoint, points.Count - 1, point, title, "");
            map.ToolLayer.Markers.Add(GMapMarkerConverter.Convert(marker));
        }

        public override void OnMouseMove(GMapControl map, MouseEventArgs e)
        {
            if (_polygon != null && _isToolForCreateActive)
            {
                _polygon.LocalPoints[_polygon.LocalPoints.Count - 1] = new GPoint(e.Location.X, e.Location.Y);
                map.Invalidate(true);
            }
            else if (_isToolForEditActive)
            {
                _markerSelected.Position = map.FromLocalToLatLng(e.Location.X, e.Location.Y);
                _polygon.Points[_indexSelected] = map.FromLocalToLatLng(e.Location.X, e.Location.Y);
                _polygon.LocalPoints[_indexSelected] = new GPoint(e.Location.X, e.Location.Y);
                double area = calculate(_polygon.Points);
                if (area > 0) _markerSelected.ToolTipText = string.Format("{0}{1}", area, Resources.KmSq);
                map.Invalidate(true);
            }
        }

        public override void OnMouseUp(GMapControl map, MouseEventArgs e)
        {
        }

        public override void OnMarkerClick(GMapControl map, GMapMarker m)
        {
            if (_isToolForCreateActive) 
                return;

            if (m is AreaPointMarker)
            {
                map.IsZoneEditor = true;
                _markerSelected = m;
                _indexSelected = (m as AreaPointMarker).PointNumber - 1;
                _isToolForEditActive = true;

                foreach (GMapMarker marker in map.ToolLayer.Markers)
                {
                    marker.ToolTipText = "";
                }
            }
        }

        internal void Release(GMapControl map)
        {
            if (null != _polygon && null != map)
            {
                map.ToolLayer.Polygons.Clear();
                map.ToolLayer.Markers.Clear();

                if (!map.EditorZonesOn)
                {
                    map.IsZoneEditor = false;
                }

                _polygon = null;
                map.Cursor = Cursors.Default;
            }
        }

        private double calculate(IList<PointLatLng> points)
        {
            double areaKm = СGeoDistance.CalculateArea(points);
            return Math.Round(areaKm, 4);
        }
    }
}
