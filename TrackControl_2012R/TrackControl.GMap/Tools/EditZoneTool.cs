using System;
using System.Windows.Forms;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Tools
{
  public class EditZoneTool : MapTool
  {
    GMapZone _zone;
    int _apexIndex;

    public override void OnMouseDown(GMapControl map, MouseEventArgs e)
    {
      foreach (GMapZone zone in map.Zones)
      {
        _apexIndex = zone.CheckHit(e.Location);
        if (_apexIndex < 0)
          continue;

        map.UnselectZones();
        zone.Selected = true;
        map.OnZoneSelected(zone.Zone);
        _zone = zone;
        if (zone.HitOnBorder && zone.InsertAt >= 0 && (Control.ModifierKeys & Keys.Control) != 0)
        {
          zone.LocalPoints.Insert(zone.InsertAt, new GPoint(e.X, e.Y));
          zone.Points.Insert(zone.InsertAt, map.FromLocalToLatLng(e.X, e.Y));
          zone.Commit();
        }
        else if (_apexIndex > 0 && (Control.ModifierKeys & Keys.Alt) != 0)
        {
          if (zone.Points.Count > 3)
          {
            zone.Points.RemoveAt(_apexIndex - 1);
            zone.LocalPoints.RemoveAt(_apexIndex - 1);
            zone.Commit();
            _apexIndex = -1;
          }
        }
        break;
      }

      map.Invalidate(true);
    }

    public override void OnMouseMove(GMapControl map, MouseEventArgs e)
    {
      map.Cursor = Cursors.Default;
      map.IsMouseOverZone = false;
      if (e.Button == MouseButtons.None)
      {
        foreach (GMapZone zone in map.Zones)
        {
          int n = zone.CheckHit(e.Location);
          if (n < 0)
            continue;

          map.IsMouseOverZone = true;
          if (zone.HitOnBorder && (Control.ModifierKeys & Keys.Control) != 0)
            map.Cursor = AddCursor;
          if (n > 0)
          {
            if ((Control.ModifierKeys & Keys.Alt) != 0)
              map.Cursor = DelCursor;
            else
              map.Cursor = MoveCursor;
          }
          break;
        }
      }

      if (e.Button != MouseButtons.Left)
        return;

      if (_zone != null && _apexIndex > 0)
      {
        map.Cursor = MoveCursor;
        _zone.MoveApexTo(e.Location, _apexIndex);
        map.Invalidate(true);
      }
    }

    public override void OnMouseUp(GMapControl map, MouseEventArgs e)
    {
      if (null == _zone)
      {
        map.UnselectZones();
        map.Tool = new FreeMoveTool();
        map.Invalidate(true);
      }
      else if (_apexIndex > 0)
      {
        GPoint p = _zone.LocalPoints[_apexIndex - 1];
        _zone.Points[_apexIndex - 1] = map.FromLocalToLatLng(p.X, p.Y);
        _zone.Commit();
      }

      _apexIndex = -1;
      _zone = null;
    }



  }
}
