using System;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;

namespace TrackControl.GMap.Tools
{
    public class CreateNewZoneTool : MapTool
    {
        GMapZone _newZone;
        bool _start = true;
        bool _complete;

        public CreateNewZoneTool( GMapZone newZone )
        {
            if( null == newZone )
                throw new ArgumentNullException( "newZone" );

            _newZone = newZone;
        }

        public override void OnMouseDown( GMapControl map, MouseEventArgs e )
        {
            if( MouseButtons.Right == e.Button )
            {
                if( _newZone != null && _newZone.Points.Count > 2 )
                {
                    _complete = true;
                    map.IsAddingNewZone = false;
                    _newZone.Points[_newZone.Points.Count - 1] = map.FromLocalToLatLng( e.X, e.Y );
                    _newZone.Commit();
                }
                return;
            }

            if( _start )
            {
                map.UnselectZones();
                _newZone.Points.Add( map.FromLocalToLatLng( e.X, e.Y ) );
                _newZone.Points.Add( map.FromLocalToLatLng( e.X, e.Y ) );
                _newZone.Selected = true;
                map.Zones.Add( _newZone );
                _start = false;
            }
            else
            {
                _newZone.Points[_newZone.Points.Count - 1] = map.FromLocalToLatLng( e.X, e.Y );
                _newZone.LocalPoints.Add( new GPoint( e.X, e.Y ) );
                _newZone.Points.Add( new PointLatLng() );
            }
            map.Invalidate( true );
        }

        public override void OnMouseMove( GMapControl map, MouseEventArgs e )
        {
            if( !_start )
            {
                map.Cursor = AddCursor;
                _newZone.MoveApexTo( e.Location, _newZone.LocalPoints.Count );
                map.Invalidate( true );
            }
            else
            {
                map.Cursor = NewCursor;
            }
        }

        public override void OnMouseUp( GMapControl map, MouseEventArgs e )
        {
            if( _complete )
                map.Tool = new EditZoneTool();
        }
    }
}
