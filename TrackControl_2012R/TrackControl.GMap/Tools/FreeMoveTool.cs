using System;
using System.Windows.Forms;
using TrackControl.GMap.UI;
using TrackControl.General;

namespace TrackControl.GMap.Tools
{
    public class FreeMoveTool : MapTool
    {
        private bool _zoneSelected;
        private IZone _zone;

        public override void OnMouseDown(GMapControl map, MouseEventArgs e)
        {
            _zoneSelected = false;

            foreach (GMapZone zone in map.Zones)
                zone.Selected = false;

            if (map.IsMouseOverZone)
            {
                foreach (GMapZone zone in map.Zones)
                {
                    if (zone.CheckHit(e.Location) >= 0)
                    {
                        zone.Selected = true;
                        _zoneSelected = true;
                        _zone = zone.Zone;
                        break;
                    }
                }
            }
        }

        public override void OnMouseMove(GMapControl map, MouseEventArgs e)
        {
            map.Cursor = Cursors.Default;
            map.IsMouseOverZone = false;

            if (MouseButtons.None == e.Button)
            {
                foreach (GMapZone zone in map.Zones)
                {
                    if (zone.CheckHit(e.Location) >= 0)
                    {
                        map.IsMouseOverZone = true;
                        break;
                    }
                }
            }
        }

        public override void OnMouseUp(GMapControl map, MouseEventArgs e)
        {
            if (_zoneSelected)
            {
                map.Tool = new EditZoneTool();
                map.OnZoneSelected(_zone);
            }

            map.Invalidate(true);
        }
    }
}
