﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using TrackControl.General;

namespace TrackControl.GMap
{
    /// <summary>
    /// прорисовывает гео-точки на битмапе
    /// </summary>
    public class BmpLatLng
    {
        #region  Поля для прорисовки зоны
        private Point[] s_points;
        int Y_min;
        int X_min;
        int Y_max;
        int X_max;
        int _widtExtControl;
        int _heightExtControl;

        #endregion

        Color _colorZone = Color.Brown;
        Color _colorTrack = Color.BlueViolet;

        public BmpLatLng(PointLatLng[] g_points, int widthBMP, int heightBMP)
        {
            _widtExtControl = widthBMP;
            _heightExtControl = heightBMP;
            SetScreenPoints(g_points);

        }

        public void SetZoneColor(Color color)
        {
            _colorZone = color;
        }

        public Bitmap DrawOnBitmap()
        {
            Bitmap btm = new Bitmap(_widtExtControl + 20, _heightExtControl + 20, PixelFormat.Format32bppArgb);
            GraphicsPath path = new GraphicsPath();
            path.AddLines(s_points);
            path.CloseFigure();
            Graphics gr = Graphics.FromImage(btm);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            gr.DrawPath(new Pen(_colorZone, 2), path);
            gr.FillPath(new SolidBrush(Color.FromArgb(60, _colorZone.R, _colorZone.G, _colorZone.B)), path);
            gr.Dispose();
            return btm;

        }
        public Bitmap DrawTrackOnBitmap(int startPoint, int endPoint)
        {
            Bitmap btm = new Bitmap(_widtExtControl + 20, _heightExtControl + 20, PixelFormat.Format32bppArgb);
            GraphicsPath path = new GraphicsPath();
            path.AddLines(s_points);
            Graphics gr = Graphics.FromImage(btm);
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            gr.DrawPath(new Pen(_colorTrack, 2), path);
            if (startPoint >0 || endPoint>0)
            {
                GraphicsPath pathSelected = new GraphicsPath();
                int startDraw = Math.Min(startPoint,endPoint);
                int endDraw = Math.Max(startPoint,endPoint);
                if (startDraw == 0) startDraw++;

                for (int i = startDraw; i <= endDraw; i++)
                {
                    pathSelected.AddLine(s_points[i - 1].X ,s_points[i - 1].Y ,s_points[i].X ,s_points[i].Y );
                }
                gr.DrawPath(new Pen(Color.Red, 4), pathSelected);
            }
            gr.Dispose();
            return btm;

        }

        private void SetScreenPoints(PointLatLng[] g_points)
        {
            Y_min = 0;
            X_min = 0;
            Y_max = 0;
            X_max = 0;
            s_points = new Point[g_points.Length];
            Projections.MercatorProjection mprj = new Projections.MercatorProjection();
            for (int i = 0; i < s_points.Length; i++)
            {
                Core.GPoint cg_point = mprj.FromLatLngToPixel(g_points[i].Lat, g_points[i].Lng, 14); ;
                s_points[i].X = cg_point.X;
                s_points[i].Y = cg_point.Y;
                if ((X_min > s_points[i].X) | X_min == 0) X_min = s_points[i].X;
                if ((Y_min > s_points[i].Y) | Y_min == 0) Y_min = s_points[i].Y;
            }
            for (int i = 0; i < s_points.Length; i++)
            {
                s_points[i].X = s_points[i].X - X_min;
                s_points[i].Y = s_points[i].Y - Y_min;
                if ((X_max < s_points[i].X) | X_max == 0) X_max = s_points[i].X;
                if ((Y_max < s_points[i].Y) | Y_max == 0) Y_max = s_points[i].Y;
            }
            double kfX = 0;
            if (X_max > 0) kfX = (double)_widtExtControl / (double)X_max;
            double kfY = 0;
            if (Y_max > 0) kfY = (double)_heightExtControl / (double)Y_max;
            if (kfX > 0 && kfY > 0)
            {
                for (int i = 0; i < s_points.Length; i++)
                {
                    if (kfX > 0) s_points[i].X = Convert.ToInt32((double)s_points[i].X * kfX);
                    if (kfY > 0) s_points[i].Y = Convert.ToInt32((double)s_points[i].Y * kfY);
                }
            }

        }
    }
}
