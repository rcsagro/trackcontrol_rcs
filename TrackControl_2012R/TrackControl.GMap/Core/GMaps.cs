﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Diagnostics ;
using System.Threading;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using DevExpress.Data.Selection;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.GMap.Internals;
using TrackControl.GMap.CacheProviders;
using TrackControl.GMap.MapProviders;
using TrackControl.General.Patameters;

namespace TrackControl.GMap.Core
{
    /// <summary>
    /// maps manager
    /// </summary>
    public class GMaps : Singleton<GMaps>
    {

        // Google version strings
        public string VersionGoogleMap = "m@245";
        static protected TotalParamsProvider provParams = new TotalParamsProvider();
        public string VersionGoogleSatellite = provParams.GoogleSatteliteKey; // задается в настройках
        public string VersionGoogleLabels = "h@245";
        public string VersionGoogleTerrain = "t@145,r@245";
        public string SecGoogleWord = "Galileo";

        // Yahoo version strings
        public string VersionYahooMap = "4.3";
        public string VersionYahooSatellite = "1.9";
        public string VersionYahooLabels = "4.3";

        // BingMaps
        public string VersionBingMaps = "671";

        /// <summary>
        /// Bing Maps Customer Identification, more info here
        /// http://msdn.microsoft.com/en-us/library/bb924353.aspx
        /// </summary>
        public string BingMapsClientToken;

        /// <summary>
        /// Gets or sets the value of the User-agent HTTP header.
        /// </summary>
        public string UserAgent =
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7";

        /// <summary>
        /// is map using cache for directions
        /// </summary>
        public bool UseDirectionsCache = true;

        /// <summary>
        /// timeout for map connections
        /// </summary>
        public int Timeout = 30*1000;

        /// <summary>
        /// retry count to get tile 
        /// </summary>
        public int RetryLoadTile = 2;

        /// <summary>
        /// proxy for net access
        /// </summary>
        public IWebProxy Proxy;

        /// <summary>
        /// tile access mode
        /// </summary>
        public AccessMode Mode = AccessMode.ServerAndCache;

        internal string LanguageStr;
        private LanguageType language = LanguageType.English;

        /// <summary>
        /// map language
        /// </summary>
        public LanguageType Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
                LanguageStr = Stuff.EnumToString(Language);
            }
        }

        /// <summary>
        /// is map ussing cache for routing
        /// </summary>
        public bool UseRouteCache = true;

        /// <summary>
        /// is map using cache for geocoder
        /// </summary>
        public bool UseGeocoderCache = true;

        /// <summary>
        /// is map using cache for placemarks
        /// </summary>
        public bool UsePlacemarkCache = true;

        /// <summary>
        /// is map using memory cache for tiles
        /// </summary>
        public bool UseMemoryCache = true;

        /// <summary>
        /// max zoom for maps, 17 is max fo many maps
        /// </summary>
        public readonly int MaxZoom = 17;

        /// <summary>
        /// Radius of the Earth
        /// </summary>
        public double EarthRadiusKm = 6378.137; // WGS-84

        /// <summary>
        /// pure image cache provider, by default: ultra fast SQLite!
        /// </summary>
        public static PureImageCache ImageCacheLocal
        {
            get
            {
                return Cache.Instance.ImageCache;
            }
            set
            {
                Cache.Instance.ImageCache = value;
            }
        }

        /// <summary>
        /// pure image cache second provider, by default: none
        /// looks here after server
        /// </summary>
        public static PureImageCache ImageCacheSecond
        {
            get
            {
                return Cache.Instance.ImageCacheSecond;
            }
            set
            {
                Cache.Instance.ImageCacheSecond = value;
            }
        }

        /// <summary>
        /// internal proxy for image managment
        /// </summary>
        public PureImageProxy ImageProxy;

        /// <summary>
        /// load tiles in random sequence
        /// </summary>
        public bool ShuffleTilesOnLoad = true;

        /// <summary>
        /// tile queue to cache
        /// </summary>
        private readonly Queue<CacheItemQueue> tileCacheQueue = new Queue<CacheItemQueue>();

        /// <summary>
        /// tiles in memmory
        /// </summary>
        internal readonly KiberTileCache TilesInMemory = new KiberTileCache();

        /// <summary>
        /// lock for TilesInMemory
        /// </summary>
        internal readonly ReaderWriterLock kiberCacheLock = new ReaderWriterLock();

        /// <summary>
        /// the amount of tiles in MB to keep in memmory, default: 22MB, if each ~100Kb it's ~222 tiles
        /// </summary>
        public int MemoryCacheCapacity
        {
            get
            {
                kiberCacheLock.AcquireReaderLock(-1);
                try
                {
                    return TilesInMemory.MemoryCacheCapacity;
                }
                finally
                {
                    kiberCacheLock.ReleaseReaderLock();
                }
            }
            set
            {
                kiberCacheLock.AcquireWriterLock(-1);
                try
                {
                    TilesInMemory.MemoryCacheCapacity = value;
                }
                finally
                {
                    kiberCacheLock.ReleaseWriterLock();
                }
            }
        }

        /// <summary>
        /// current memmory cache size in MB
        /// </summary>
        public double MemoryCacheSize
        {
            get
            {
                kiberCacheLock.AcquireReaderLock(-1);
                try
                {
                    return TilesInMemory.MemoryCacheSize;
                }
                finally
                {
                    kiberCacheLock.ReleaseReaderLock();
                }
            }
        }

        private bool isCorrectedGoogleVersions;

        /// <summary>
        /// true if google versions was corrected
        /// </summary>
        internal bool IsCorrectedGoogleVersions
        {
            get
            {
                return isCorrectedGoogleVersions;
            }
            set
            {
                isCorrectedGoogleVersions = value;
            }
        }

        /// <summary>
        /// try correct versions once
        /// </summary>

        public bool CorrectGoogleVersions = true;


        /// <summary>
        /// cache worker
        /// </summary>
        private Thread CacheEngine;

        private AutoResetEvent WaitForCache = new AutoResetEvent(false);

        public GMaps()
        {
            #region singleton check

            if (Instance != null)
            {
                throw (new Exception(
                    "You have tried to create a new singleton class where you should have instanced it. Replace your \"new class()\" with \"class.Instance\""));
            }

            #endregion

            Language = LanguageHelper.GetLanguage();
            ServicePointManager.DefaultConnectionLimit = 444;
        }

        #region -- Stuff --

        private MemoryStream GetTileFromMemoryCache(RawTile tile)
        {
            kiberCacheLock.AcquireReaderLock(-1);
            try
            {
                MemoryStream ret = null;
                if (TilesInMemory.TryGetValue(tile, out ret))
                {
                    return ret;
                }
            }
            finally
            {
                kiberCacheLock.ReleaseReaderLock();
            }
            return null;
        }

        private void AddTileToMemoryCache(RawTile tile, MemoryStream data)
        {
            kiberCacheLock.AcquireWriterLock(-1);
            try
            {
                if (!TilesInMemory.ContainsKey(tile))
                {
                    TilesInMemory.Add(tile, data);
                }
            }
            finally
            {
                kiberCacheLock.ReleaseWriterLock();
            }
        }

        /// <summary>
        /// gets all layers of map type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static MapType[] GetAllLayersOfType(MapType type)
        {
            MapType[] types = null;
            if (MapType.GoogleHybrid == type)
            {
                types = new MapType[2];
                types[0] = MapType.GoogleSatellite;
                types[1] = MapType.GoogleLabels;
            }
            else if (MapType.YahooHybrid == type)
            {
                types = new MapType[2];
                types[0] = MapType.YahooSatellite;
                types[1] = MapType.YahooLabels;
            }
            else if (MapType.YandexHybridMap == type)
            {
                types = new MapType[2];
                types[0] = MapType.YandexSatelliteMap;
                types[1] = MapType.YandexHybridMap;
            }
            else if(MapType.GisFile == type)
            {
                types = new MapType[3];
                types[0] = MapType.GoogleSatellite;
                types[1] = MapType.GoogleLabels;
                types[2] = MapType.GisFile;
            }
            else
            {
                types = new MapType[1];
                types[0] = type;
            }

            return types;
        }

        /// <summary>
        /// exports current map cache to GMDB file
        /// if file exsist only new records will be added
        /// otherwise file will be created and all data exported
        /// </summary>
        public static bool ExportToGMDB(string file)
        {
            if (Cache.Instance.ImageCache is SQLitePureImageCache)
            {
                StringBuilder db = new StringBuilder((Cache.Instance.ImageCache as SQLitePureImageCache).GtileCache);
                db.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}Data.gmdb", GMaps.Instance.LanguageStr,
                    Path.DirectorySeparatorChar);

                return SQLitePureImageCache.ExportMapDataToDB(db.ToString(), file);
            }
            return false;
        }

        /// <summary>
        /// imports GMDB file to current map cache
        /// only new records will be added
        /// </summary>
        public static bool ImportFromGMDB(string file)
        {
            if (Cache.Instance.ImageCache is SQLitePureImageCache)
            {
                StringBuilder db = new StringBuilder((Cache.Instance.ImageCache as SQLitePureImageCache).GtileCache);
                db.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}Data.gmdb", GMaps.Instance.LanguageStr,
                    Path.DirectorySeparatorChar);

                return SQLitePureImageCache.ExportMapDataToDB(file, db.ToString());
            }
            return false;
        }

        /// <summary>
        /// enqueueens tile to cache
        /// </summary>
        /// <param name="task"></param>
        private void EnqueueCacheTask(CacheItemQueue task)
        {
            lock (tileCacheQueue)
            {
                if (!tileCacheQueue.Contains(task))
                {
                    tileCacheQueue.Enqueue(task);

                    if (CacheEngine != null && CacheEngine.IsAlive)
                    {
                        WaitForCache.Set();
                    }

                    else if (CacheEngine == null || CacheEngine.ThreadState == System.Threading.ThreadState.Stopped ||
                             CacheEngine.ThreadState == System.Threading.ThreadState.Unstarted)
                    {
                        CacheEngine = null;
                        CacheEngine = new Thread(new ThreadStart(CacheEngineLoop));
                        CacheEngine.Name = "GMap.NET CacheEngine";
                        CacheEngine.IsBackground = false;
                        CacheEngine.Priority = ThreadPriority.Lowest;
                        CacheEngine.Start();
                    }
                }
            }
        }

        /// <summary>
        /// live for cache ;}
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CacheEngineLoop()
        {
            while (true)
            {
                try
                {
                    CacheItemQueue? task = null;

                    lock (tileCacheQueue)
                    {
                        if (tileCacheQueue.Count > 0)
                        {
                            task = tileCacheQueue.Dequeue();
                            tileCacheQueue.TrimExcess();
                        }
                    }

                    if (task.HasValue)
                    {
                        if ((task.Value.CacheType & CacheUsage.First) == CacheUsage.First && ImageCacheLocal != null)
                        {
                            ImageCacheLocal.PutImageToCache(task.Value.Img, task.Value.Type, task.Value.Pos,
                                task.Value.Zoom);
                        }

                        if ((task.Value.CacheType & CacheUsage.Second) == CacheUsage.Second && ImageCacheSecond != null)
                        {
                            ImageCacheSecond.PutImageToCache(task.Value.Img, task.Value.Type, task.Value.Pos,
                                task.Value.Zoom);
                        }

                        Thread.Sleep(44);
                    }
                    else
                    {
                        if (!WaitForCache.WaitOne(4444, false))
                        {
                            break;
                        }
                    }
                }

                catch (AbandonedMutexException)
                {
                    break;
                }

                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region -- URL generation --

        /// <summary>
        /// makes url for image
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pos"></param>
        /// <param name="zoom"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        internal string MakeImageUrl(MapType type, GPoint pos, int zoom, string language)
        {
            switch (type)
            {
                    #region -- Google --

                case MapType.GoogleMap:
                {
                    string server = "mt";
                    string request = "vt";
                    string sec1 = ""; // after &x=...
                    string sec2 = ""; // after &zoom=...
                    GetSecGoogleWords(pos, out sec1, out sec2);
                    TryCorrectGoogleVersions();

                    //http://mt2.google.com/vt/lyrs=m@107&hl=lt&x=18&y=10&z=5&s=

                    return string.Format("http://{0}{1}.google.com/{2}/lyrs={3}&hl={4}&x={5}{6}&y={7}&z={8}&s={9}",
                        server, GetServerNum(pos, 4), request, VersionGoogleMap, language, pos.X, sec1, pos.Y, zoom,
                        sec2);
                }

                case MapType.GoogleSatellite:
                {
                    string server = "khm";
                    string request = "kh";
                    string sec1 = ""; // after &x=...
                    string sec2 = ""; // after &zoom=...
                    GetSecGoogleWords(pos, out sec1, out sec2);
                    TryCorrectGoogleVersions();
                    string urlsatelit = string.Format("http://{0}{1}.google.com/{2}?v={3}&hl={4}&x={5}{6}&y={7}&z={8}", server,
                        GetServerNum(pos, 4), request, VersionGoogleSatellite, language, pos.X, sec1, pos.Y, zoom);
                    return urlsatelit;
                }

                case MapType.GoogleLabels:
                {
                    string server = "mt";
                    string request = "vt";
                    string sec1 = ""; // after &x=...
                    string sec2 = ""; // after &zoom=...
                    GetSecGoogleWords(pos, out sec1, out sec2);
                    TryCorrectGoogleVersions();

                    // http://mt1.google.com/vt/lyrs=h@107&hl=lt&x=583&y=325&z=10&s=Ga

                    return string.Format("http://{0}{1}.google.com/{2}/lyrs={3}&hl={4}&x={5}{6}&y={7}&z={8}&s={9}",
                        server, GetServerNum(pos, 4), request, VersionGoogleLabels, language, pos.X, sec1, pos.Y, zoom,
                        sec2);
                }

                case MapType.GoogleTerrain:
                {
                    string server = "mt";
                    string request = "vt";
                    string sec1 = ""; // after &x=...
                    string sec2 = ""; // after &zoom=...
                    GetSecGoogleWords(pos, out sec1, out sec2);
                    TryCorrectGoogleVersions();
                    return string.Format("http://{0}{1}.google.com/{2}/v={3}&hl={4}&x={5}{6}&y={7}&z={8}&s={9}", server,
                        GetServerNum(pos, 4), request, VersionGoogleTerrain, language, pos.X, sec1, pos.Y, zoom, sec2);
                }

                    #endregion

                #region -- Yandex --
                case MapType.YandexMap:
                    string UrlServer0 = "vec";
                    string UrlFormat0 = "http://{0}0{1}.maps.yandex.net/tiles?l=map&v={2}&x={3}&y={4}&z={5}&lang={6}";
                    string Version0 = "2.44.0";
                    string vecmap = string.Format(UrlFormat0, UrlServer0, GetServerNum(pos, 4) + 1, Version0, pos.X, pos.Y, zoom, language);
                    return vecmap;
                case MapType.YandexSatelliteMap:
                    string UrlServer1 = "sat";
                    string UrlFormat1 = "http://{0}0{1}.maps.yandex.net/tiles?l=sat&v={2}&x={3}&y={4}&z={5}&lang={6}";
                    string Version1 = "2.44.0";
                    string mpstr = string.Format(UrlFormat1, UrlServer1, GetServerNum(pos, 4) + 1, Version1, pos.X, pos.Y, zoom, language);
                    return mpstr;
                case MapType.YandexHybridMap:
                    string UrlServer2 = "vec";
                    string UrlFormat2 = "http://{0}0{1}.maps.yandex.net/tiles?l=skl&v={2}&x={3}&y={4}&z={5}&lang={6}";
                    string Version2 = "2.44.0";
                    string mapstr = string.Format(UrlFormat2, UrlServer2, GetServerNum(pos, 4) + 1, Version2, pos.X, pos.Y, zoom, language);
                    return mapstr;
                #endregion

                #region -- Yahoo --

                case MapType.YahooMap:
                {
                    return string.Format("http://maps{0}.yimg.com/hx/tl?v={1}&.intl={2}&x={3}&y={4}&z={5}&r=1",
                        ((GetServerNum(pos, 2)) + 1), VersionYahooMap, language, pos.X, (((1 << zoom) >> 1) - 1 - pos.Y),
                        (zoom + 1));
                }

                case MapType.YahooSatellite:
                {
                    return
                        string.Format(
                            "http://maps{0}.yimg.com/ae/ximg?v={1}&t=a&s=256&.intl={2}&x={3}&y={4}&z={5}&r=1", 3,
                            VersionYahooSatellite, language, pos.X, (((1 << zoom) >> 1) - 1 - pos.Y), (zoom + 1));
                }

                case MapType.YahooLabels:
                {
                    return string.Format("http://maps{0}.yimg.com/hx/tl?v={1}&t=h&.intl={2}&x={3}&y={4}&z={5}&r=1", 1,
                        VersionYahooLabels, language, pos.X, (((1 << zoom) >> 1) - 1 - pos.Y), (zoom + 1));
                }

                    #endregion

                    #region -- OpenStreet --

                case MapType.OpenStreetMap:
                {
                    char letter = "abc"[GetServerNum(pos, 3)]; 
                    //return string.Format("https://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png", letter, zoom, pos.X, pos.Y);
                    return string.Format("https://{0}.tile.openstreetmap.de/{1}/{2}/{3}.png", letter, zoom, pos.X, pos.Y);
                }

                case MapType.OpenStreetOsm:
                {
                    char letter = "abc"[GetServerNum(pos, 3)];
                    return string.Format("http://{0}.tah.openstreetmap.org/Tiles/tile/{1}/{2}/{3}.png", letter, zoom, pos.X, pos.Y);
                }

                    #endregion

                    #region -- Bing --

                case MapType.BingMap:
                {
                    string key = TileXYToQuadKey(pos.X, pos.Y, zoom);
                    return string.Format("http://ecn.t{0}.tiles.virtualearth.net/tiles/r{1}.png?g={2}&mkt={3}{4}",
                        GetServerNum(pos, 4), key, VersionBingMaps, language,
                        (!string.IsNullOrEmpty(BingMapsClientToken)
                            ? String.Format("&token={0}", BingMapsClientToken)
                            : string.Empty));
                }

                case MapType.BingSatellite:
                {
                    string key = TileXYToQuadKey(pos.X, pos.Y, zoom);
                    return string.Format("http://ecn.t{0}.tiles.virtualearth.net/tiles/a{1}.jpeg?g={2}&mkt={3}{4}",
                        GetServerNum(pos, 4), key, VersionBingMaps, language,
                        (!string.IsNullOrEmpty(BingMapsClientToken)
                            ? String.Format("&token={0}", BingMapsClientToken)
                            : string.Empty));
                }

                case MapType.BingHybrid:
                {
                    string key = TileXYToQuadKey(pos.X, pos.Y, zoom);
                    return string.Format("http://ecn.t{0}.tiles.virtualearth.net/tiles/h{1}.jpeg?g={2}&mkt={3}{4}",
                        GetServerNum(pos, 4), key, VersionBingMaps, language,
                        (!string.IsNullOrEmpty(BingMapsClientToken)
                            ? String.Format("&token={0}", BingMapsClientToken)
                            : string.Empty));
                }

                    #endregion

#region -- GisFile---
                case MapType.GisFile:
                {
                    string sec1 = ""; // after &x=...
                    string sec2 = ""; // after &zoom=...
                    string layer = "cadmap";
                    GetSecGoogleWords(pos, out sec1, out sec2);
                    string urlsatelit = string.Format("http://gisfile.com/layer/{0}/{1}/{2}/{3}.png", layer, zoom, pos.X, pos.Y);
                    return urlsatelit;
                }
#endregion
            }

            return null;
        }

        /// <summary>
        /// gets secure google words based on position
        /// </summary>
        internal void GetSecGoogleWords(GPoint pos, out string sec1, out string sec2)
        {
            int seclen = ((pos.X*3) + pos.Y)%8;
            sec2 = SecGoogleWord.Substring(0, seclen);
            if (pos.Y >= 10000 && pos.Y < 100000)
                sec1 = "&s=";
            else
                sec1 = "";
        }

        /// <summary>
        /// gets server num based on position
        /// </summary>
        internal static int GetServerNum(GPoint pos, int max)
        {
            int retval = (pos.X + 2 * pos.Y) % max;
            return retval;
        }

        /// <summary>
        /// Converts tile XY coordinates into a QuadKey at a specified level of detail.
        /// </summary>
        /// <param name="tileX">Tile X coordinate.</param>
        /// <param name="tileY">Tile Y coordinate.</param>
        /// <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
        /// to 23 (highest detail).</param>
        /// <returns>A string containing the QuadKey.</returns>
        internal static string TileXYToQuadKey(int tileX, int tileY, int levelOfDetail)
        {
            StringBuilder quadKey = new StringBuilder();
            for (int i = levelOfDetail; i > 0; i--)
            {
                char digit = '0';
                int mask = 1 << (i - 1);
                if ((tileX & mask) != 0)
                {
                    digit++;
                }
                if ((tileY & mask) != 0)
                {
                    digit++;
                    digit++;
                }
                quadKey.Append(digit);
            }
            return quadKey.ToString();
        }

        #endregion

        #region -- Content download --

        /// <summary>
        /// try to correct google versions
        /// </summary>
        internal void TryCorrectGoogleVersions()
        {
            if (CorrectGoogleVersions && !IsCorrectedGoogleVersions)
            {
                IsCorrectedGoogleVersions = true; // try it only once

                string url = @"http://maps.google.com";
                try
                {
                    HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
                    if (Proxy != null)
                    {
                        request.Proxy = Proxy;
                        request.PreAuthenticate = true;
                    }
                    else
                    {
                        request.Proxy = WebRequest.DefaultWebProxy;
                    }
                    request.UserAgent = UserAgent;
                    request.Timeout = Timeout;
                    request.ReadWriteTimeout = Timeout*6;

                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            using (StreamReader read = new StreamReader(responseStream))
                            {
                                string html = read.ReadToEnd();

                                // find it  
                                // apiCallback(["http://mt0.google.com/vt/v\x3dw2.106\x26hl\x3dlt\x26","http://mt1.google.com/vt/v\x3dw2.106\x26hl\x3dlt\x26","http://mt2.google.com/vt/v\x3dw2.106\x26hl\x3dlt\x26","http://mt3.google.com/vt/v\x3dw2.106\x26hl\x3dlt\x26"],
                                // ["http://khm0.google.com/kh/v\x3d45\x26","http://khm1.google.com/kh/v\x3d45\x26","http://khm2.google.com/kh/v\x3d45\x26","http://khm3.google.com/kh/v\x3d45\x26"],
                                // ["http://mt0.google.com/vt/v\x3dw2t.106\x26hl\x3dlt\x26","http://mt1.google.com/vt/v\x3dw2t.106\x26hl\x3dlt\x26","http://mt2.google.com/vt/v\x3dw2t.106\x26hl\x3dlt\x26","http://mt3.google.com/vt/v\x3dw2t.106\x26hl\x3dlt\x26"],
                                // "","","",false,"G",opts,["http://mt0.google.com/vt/v\x3dw2p.106\x26hl\x3dlt\x26","http://mt1.google.com/vt/v\x3dw2p.106\x26hl\x3dlt\x26","http://mt2.google.com/vt/v\x3dw2p.106\x26hl\x3dlt\x26","http://mt3.google.com/vt/v\x3dw2p.106\x26hl\x3dlt\x26"],jslinker,pageArgs);

                                int id = html.LastIndexOf("apiCallback([");
                                if (id > 0)
                                {
                                    int idEnd = html.IndexOf("jslinker,pageArgs", id);
                                    if (idEnd > id)
                                    {
                                        string api = html.Substring(id, idEnd - id);
                                        if (!string.IsNullOrEmpty(api))
                                        {
                                            int i = 0;
                                            string[] opts = api.Split('['); //"[\""
                                            foreach (string opt in opts)
                                            {
                                                if (opt.Contains("http://"))
                                                {
                                                    int start = opt.IndexOf("x3d");
                                                    if (start > 0)
                                                    {
                                                        int end = opt.IndexOf("\\x26", start);
                                                        if (end > start)
                                                        {
                                                            start += 3;
                                                            string u = opt.Substring(start, end - start);

                                                            if (i == 0)
                                                            {
                                                                if (u.StartsWith("m@"))
                                                                {
                                                                    VersionGoogleMap = u;
                                                                }
                                                            }
                                                            else if (i == 1)
                                                            {
                                                                // 45
                                                                if (char.IsDigit(u[0]))
                                                                {
                                                                    VersionGoogleSatellite = u;
                                                                }
                                                            }
                                                            else if (i == 2)
                                                            {
                                                                if (u.StartsWith("h@"))
                                                                {
                                                                    VersionGoogleLabels = u;
                                                                }
                                                            }
                                                            else if (i == 3)
                                                            {
                                                                // t@108,r@120
                                                                if (u.StartsWith("t@"))
                                                                {
                                                                    VersionGoogleTerrain = u;
                                                                }
                                                                break;
                                                            }
                                                            i++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    
                }
            }
        }

        /// <summary>
        /// get route between two points, kml format
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        internal string GetRouteBetweenPointsKmlUrl(string url)
        {
            string ret = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
                request.ServicePoint.ConnectionLimit = 50;
                if (Proxy != null)
                {
                    request.Proxy = Proxy;

                    request.PreAuthenticate = true;

                }
                else
                {

                    request.Proxy = WebRequest.DefaultWebProxy;

                }

                request.UserAgent = UserAgent;
                request.Timeout = Timeout;
                request.ReadWriteTimeout = Timeout*6;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader read = new StreamReader(responseStream))
                        {
                            string kmls = read.ReadToEnd();

                            //XmlSerializer serializer = new XmlSerializer(typeof(KmlType));
                            using (StringReader reader = new StringReader(kmls)) //Substring(kmls.IndexOf("<kml"))
                            {
                                //ret = (KmlType) serializer.Deserialize(reader);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                ret = null;
            }
            return ret;
        }

        /// <summary>
        /// gets image from tile server
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pos"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public PureImage GetImageFrom(MapType type, GPoint pos, int zoom)
        {
            PureImage ret = null;
            HttpWebRequest request = null;
            try
            {
                // let't check memmory first
                if (UseMemoryCache)
                {
                    MemoryStream m = GetTileFromMemoryCache(new RawTile(type, pos, zoom));
                    if (m != null)
                    {
                        if (GMaps.Instance.ImageProxy != null)
                        {
                            ret = GMaps.Instance.ImageProxy.FromStream(m);
                            if (ret == null)
                            {
                                // should never happen
                                m.Dispose();

                            }
                        }
                    }
                }

                if (ret == null)
                {
                    if (Mode != AccessMode.ServerOnly)
                    {
                        if (Cache.Instance.ImageCache != null)
                        {
                            ret = Cache.Instance.ImageCache.GetImageFromCache(type, pos, zoom);

                            if (ret != null)
                            {
                                if (UseMemoryCache)
                                {
                                    AddTileToMemoryCache(new RawTile(type, pos, zoom), ret.Data);
                                }
                                return ret;
                            }
                        }

                        if (Cache.Instance.ImageCacheSecond != null)
                        {
                            ret = Cache.Instance.ImageCacheSecond.GetImageFromCache(type, pos, zoom);
                            if (ret != null)
                            {
                                if (UseMemoryCache)
                                {
                                    AddTileToMemoryCache(new RawTile(type, pos, zoom), ret.Data);
                                }
                                EnqueueCacheTask(new CacheItemQueue(type, pos, zoom, ret.Data, CacheUsage.First));
                                return ret;
                            }
                        }
                    }

                    if (Mode != AccessMode.CacheOnly)
                    {
                        string url = MakeImageUrl(type, pos, zoom, LanguageStr);

                        switch (type)
                        {
                            case MapType.OpenStreetMap:
                            case MapType.OpenStreetOsm:
                            {
                                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                                ServicePointManager.Expect100Continue = true;
                                ServicePointManager.DefaultConnectionLimit = 9999; ;
                                ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0x30 | 0xC0 | 0x300 | 0xC00);
                            } 
                                break;
                        }

                        request = (HttpWebRequest) WebRequest.Create(url);
                        if (Proxy != null)
                        {
                            request.Proxy = Proxy;

                            request.PreAuthenticate = true;

                        }
                        else
                        {

                            request.Proxy = WebRequest.DefaultWebProxy;

                        }

                        request.UserAgent = UserAgent;
                        request.Timeout = Timeout;
                        request.ReadWriteTimeout = Timeout*6;
                        request.Accept = "*/*";

                        switch (type)
                        {
                            case MapType.GoogleMap:
                            case MapType.GoogleSatellite:
                            case MapType.GoogleLabels:
                            case MapType.GoogleTerrain:
                            case MapType.GoogleHybrid:
                            {
                                request.Referer = "http://maps.google.com/";
                            }
                                break;

                            case MapType.YandexMap:
                            case MapType.YandexSatelliteMap:
                            case MapType.YandexHybridMap:
                            {
                                request.Referer = "http://maps.yandex.ru";
                            }
                                break;

                            case MapType.BingHybrid:
                            case MapType.BingMap:
                            case MapType.BingSatellite:
                            {
                                request.Referer = "http://www.bing.com/maps/";
                            }
                                break;

                            case MapType.YahooHybrid:
                                request.Referer = "http://yandex.ua/maps/";
                                break;
                            case MapType.YahooLabels:
                            case MapType.YahooMap:
                            case MapType.YahooSatellite:
                            {
                                request.Referer = "http://maps.yahoo.com/";
                            }
                                break;

                            case MapType.OpenStreetMap:
                            case MapType.OpenStreetOsm:
                            {
                                request.Referer = "http://www.openstreetmap.org/";
                            }
                                break;
                            case MapType.GisFile:
                            {
                                request.Referer = "http://gisfile.com";
                            }
                                break;
                        }

                        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                        {
                            MemoryStream responseStream = Stuff.CopyStream(response.GetResponseStream(), false);
                            {
                                if (GMaps.Instance.ImageProxy != null)
                                {
                                    ret = GMaps.Instance.ImageProxy.FromStream(responseStream);

                                    // Enqueue Cache
                                    if (ret != null)
                                    {
                                        if (UseMemoryCache)
                                        {
                                            AddTileToMemoryCache(new RawTile(type, pos, zoom), responseStream);
                                        }

                                        if (Mode != AccessMode.ServerOnly)
                                        {
                                            EnqueueCacheTask(new CacheItemQueue(type, pos, zoom, responseStream,
                                                CacheUsage.Both));
                                        }
                                    }
                                }
                            }
                            response.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string urlrequest = "";
                string referer = "";

                if (request != null)
                {
                    urlrequest = request.RequestUri.ToString();
                    referer = request.Referer;
                }

                //XtraMessageBox.Show(ex.Message + "\n" + referer + "\n" + urlrequest, "GetImageFrom error",
                    //MessageBoxButtons.OK, MessageBoxIcon.Error);
                ret = null;
            }

            return ret;
        }

        #endregion

        /// <summary>
        /// get route between two points
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="avoidHighways"></param>
        /// <param name="Zoom"></param>
        /// <returns></returns>
        public List<PointLatLng> GetRouteBetweenPoints(PointLatLng start, PointLatLng end, bool avoidHighways, int Zoom, int type)
        {
            try
            {
                if (type == (int) MapType.GoogleMap)
                {
                    MapRoute route = GMapProviders.GoogleMap.GetRoute(start, end, avoidHighways, false, Zoom);
                    if (null == route)
                    {
                        XtraMessageBox.Show("Не возможно получить точки от указанного провайдера!", "Ошибка провайдера GoogleMap", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return null;
                    }

                    return route.Points;
                }
                else if (type == (int) MapType.OpenStreetMap)
                {
                    MapRoute points =
                        GMapProviders.getOpenStreetMap.GetRoute(start, end, avoidHighways, false, Zoom);
                    if (points == null)
                    {
                        XtraMessageBox.Show("Не возможно получить точки от указанного провайдера!", "Ошибка провайдера OpenStreetMap", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return null;
                    }

                    return points.Points;
                }
                else if (type == (int) MapType.RcsNominatim)
                {
                    MapRoute route = GMapProviders.getRcsNominatim.GetRoute(start, end, avoidHighways, false, Zoom);
                    if (null == route)
                    {
                        XtraMessageBox.Show("Не возможно получить точки от указанного провайдера!", "Ошибка провайдера RcsNominatim", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return null;
                    }

                    return route.Points; 
                }
                else if (type == (int) MapType.YandexMap)
                {
                    MapRoute route = GMapProviders.getYandexStreetMap.GetRoute(start, end, avoidHighways, false, Zoom);
                    if (null == route)
                    {
                        XtraMessageBox.Show("Не возможно получить точки от указанного провайдера!", "Ошибка провайдера YandexMap", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return null;
                    }

                    return route.Points;
                }

                return null;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Ошибка провайдера", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
        }
    }
}