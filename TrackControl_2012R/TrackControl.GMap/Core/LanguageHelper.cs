using System;
using System.Globalization;
using System.Threading;

namespace TrackControl.GMap.Core
{
  class LanguageHelper
  {
    public static LanguageType GetLanguage()
    {
      CultureInfo info = Thread.CurrentThread.CurrentUICulture;
      string local = info.Name;
      if (local.Length > 4)
        local = local.Substring(0, 5);
      else
        return LanguageType.English;

      switch (local)
      {
        case "en-AU": return LanguageType.EnglishAustralian;
        case "en-GB": return LanguageType.EnglishGreatBritain;
        case "pt-BR": return LanguageType.PortugueseBrazil;
        case "pt-PT": return LanguageType.PortuguesePortugal;
        case "zh-CN": return LanguageType.ChineseSimplified;
        case "zh-TW": return LanguageType.ChineseTraditional;
        default: break;
      }

      local = local.Substring(0, 3);
      if ("fil" == local)
        return LanguageType.Filipino;

      local = local.Substring(0, 2);
      switch (local)
      {
        case "ru": return LanguageType.Russian;
        case "en": return LanguageType.English;
        case "uk": return LanguageType.Ukrainian;
        case "ar": return LanguageType.Arabic;
        case "bg": return LanguageType.Bulgarian;
        case "bn": return LanguageType.Bengali;
        case "ca": return LanguageType.Catalan;
        case "cs": return LanguageType.Czech;
        case "da": return LanguageType.Danish;
        case "de": return LanguageType.German;
        case "el": return LanguageType.Greek;
        case "es": return LanguageType.Spanish;
        case "eu": return LanguageType.Basque;
        case "fi": return LanguageType.Finnish;
        case "fr": return LanguageType.French;
        case "gl": return LanguageType.Galician;
        case "gu": return LanguageType.Gujarati;
        case "hi": return LanguageType.Hindi;
        case "hr": return LanguageType.Croatian;
        case "hu": return LanguageType.Hungarian;
        case "id": return LanguageType.Indonesian;
        case "it": return LanguageType.Italian;
        case "iw": return LanguageType.Hebrew;
        case "ja": return LanguageType.Japanese;
        case "kn": return LanguageType.Kannada;
        case "ko": return LanguageType.Korean;
        case "lt": return LanguageType.Lithuanian;
        case "lv": return LanguageType.Latvian;
        case "ml": return LanguageType.Malayalam;
        case "mr": return LanguageType.Marathi;
        case "nl": return LanguageType.Dutch;
        case "nn": return LanguageType.NorwegianNynorsk;
        case "no": return LanguageType.Norwegian;
        case "or": return LanguageType.Oriya;
        case "pl": return LanguageType.Polish;
        case "pt": return LanguageType.Portuguese;
        case "rm": return LanguageType.Romansch;
        case "ro": return LanguageType.Romanian;
        case "sk": return LanguageType.Slovak;
        case "sl": return LanguageType.Slovenian;
        case "sr": return LanguageType.Serbian;
        case "sv": return LanguageType.Swedish;
        case "ta": return LanguageType.Tamil;
        case "te": return LanguageType.Telugu;
        case "th": return LanguageType.Thai;
        case "tr": return LanguageType.Turkish;
        case "vi": return LanguageType.Vietnamese;
        default: return LanguageType.English;
      }
    }
  }
}
