using System;

namespace TrackControl.GMap.Core
{
  /// <summary>
  /// ������ ����� �� ������
  /// </summary>
  public struct GPoint
  {
    public static readonly GPoint Empty = new GPoint(0,0);

    public int X
    {
      get { return _x; }
      set { _x = value; }
    }
    int _x;

    public int Y
    {
      get { return _y; }
      set { _y = value; }
    }
    int _y;

    public GPoint(int x, int y)
    {
      _x = x;
      _y = y;
    }

    public GPoint(GSize sz)
    {
      _x = sz.Width;
      _y = sz.Height;
    }

    public GPoint(int dw)
    {
      _x = (short)LOWORD(dw);
      _y = (short)HIWORD(dw);
    }

    public bool IsEmpty
    {
      get
      {
        return _x == 0 && _y == 0;
      }
    }    

    public static explicit operator GSize(GPoint p)
    {
      return new GSize(p.X, p.Y);
    }

    public static GPoint operator +(GPoint pt, GSize sz)
    {
      return Add(pt, sz);
    }

    public static GPoint operator -(GPoint pt, GSize sz)
    {
      return Subtract(pt, sz);
    }

    public static bool operator ==(GPoint left, GPoint right)
    {
      return left.X == right.X && left.Y == right.Y;
    }

    public static bool operator !=(GPoint left, GPoint right)
    {
      return !(left == right);
    }

    public static GPoint Add(GPoint pt, GSize sz)
    {
      return new GPoint(pt.X + sz.Width, pt.Y + sz.Height);
    }

    public static GPoint Subtract(GPoint pt, GSize sz)
    {
      return new GPoint(pt.X - sz.Width, pt.Y - sz.Height);
    }

    public override bool Equals(object obj)
    {
      if (!(obj is GPoint))
        return false;
      GPoint comp = (GPoint)obj;
      return comp.X == X && comp.Y == Y;
    }

    public override int GetHashCode()
    {
      return _x ^ _y;
    }

    public void Offset(int dx, int dy)
    {
      X += dx;
      Y += dy;
    }

    public void Offset(GPoint p)
    {
      Offset(p.X, p.Y);
    }
    public void OffsetNegative(GPoint p)
    {
        Offset(-p.X, -p.Y);
    }

    public double Distanse(GPoint p)
    {
      return Distanse(p.X, p.Y);
    }

    public double Distanse(int x, int y)
    {
      return Math.Sqrt(Math.Pow(x - _x, 2) + Math.Pow(y - _y, 2));
    }

    public override string ToString()
    {
      return String.Format("{{X={0},Y={1}}}", _x, _y);
    }

    private static int HIWORD(int n)
    {
      return (n >> 16) & 0xffff;
    }

    private static int LOWORD(int n)
    {
      return n & 0xffff;
    }
  }
}
