﻿using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.GMap.Core
{

    /// <summary>
    /// geocoding interface
    /// </summary>
    public interface GeocodingProvider
    {
        GeoCoderStatusCode GetPoints(string keywords, out List<PointLatLng> pointList);

        PointLatLng? GetPoint(string keywords, out GeoCoderStatusCode status);

        // ...

        GeoCoderStatusCode GetPlacemarks(PointLatLng location, out List<Placemark> placemarkList);

        Placemark GetPlacemark(PointLatLng location, out GeoCoderStatusCode status);
    }
}
