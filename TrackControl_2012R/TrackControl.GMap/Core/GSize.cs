using System;

namespace TrackControl.GMap.Core
{
  /// <summary>
  /// ������.
  /// </summary>
  public struct GSize
  {
    public static readonly GSize Empty = new GSize();

    int _width;
    int _height;

    public GSize(GPoint pt)
    {
      _width = pt.X;
      _height = pt.Y;
    }

    public GSize(int width, int height)
    {
      _width = width;
      _height = height;
    }

    public static GSize operator +(GSize sz1, GSize sz2)
    {
      return Add(sz1, sz2);
    }

    public static GSize operator -(GSize sz1, GSize sz2)
    {
      return Subtract(sz1, sz2);
    }

    public static bool operator ==(GSize sz1, GSize sz2)
    {
      return sz1.Width == sz2.Width && sz1.Height == sz2.Height;
    }

    public static bool operator !=(GSize sz1, GSize sz2)
    {
      return !(sz1 == sz2);
    }

    public static explicit operator GPoint(GSize size)
    {
      return new GPoint(size.Width, size.Height);
    }

    public bool IsEmpty
    {
      get
      {
        return _width == 0 && _height == 0;
      }
    }

    public int Width
    {
      get
      {
        return _width;
      }
      set
      {
        _width = value;
      }
    }

    public int Height
    {
      get
      {
        return _height;
      }
      set
      {
        _height = value;
      }
    }

    public static GSize Add(GSize sz1, GSize sz2)
    {
      return new GSize(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
    }

    public static GSize Subtract(GSize sz1, GSize sz2)
    {
      return new GSize(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
    }

    public override bool Equals(object obj)
    {
      if (!(obj is GSize))
        return false;

      GSize comp = (GSize)obj;

      return (comp._width == _width) &&
                (comp._height == _height);
    }

    public override int GetHashCode()
    {
      return _width ^ _height;
    }

    public override string ToString()
    {
      return String.Format("Width = {0}, Height = {1}", _width, _height);
    }
  }
}
