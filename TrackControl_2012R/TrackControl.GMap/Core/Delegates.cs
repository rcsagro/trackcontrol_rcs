﻿using System.Drawing;
using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.Vehicles;

namespace TrackControl.GMap.Core
{
  public delegate void CurrentPositionChanged(PointLatLng point);
  internal delegate void NeedInvalidation();

  public delegate void TileLoadComplete();
  public delegate void TileLoadStart();

  public delegate void MapDrag();
  public delegate void MapZoomChanged();
  public delegate void MapTypeChanged(MapType type);

  public delegate void EmptyTileError(int zoom, GPoint pos);

  public delegate void MarkerClick(GMapMarker item);
  public delegate void MarkerEnter(GMapMarker item);
  public delegate void MarkerLeave(GMapMarker item);

  public delegate void TrackPointClick(GMapTrack tr, IVehicle vehicle, int indexInArray, Point pt);

  public delegate void PointClick(PointLatLng pll);

  public delegate void MouseLanLong(GPoint pos);
}
