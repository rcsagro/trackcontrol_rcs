﻿namespace TrackControl.GMap.Core
{
    /// <summary>
    /// types of great maps
    /// </summary>
    public enum MapType
    {
        GoogleMap = 1,
        GoogleSatellite = 4,
        GoogleLabels = 8,
        GoogleTerrain = 16,
        GoogleHybrid = 20,

        OpenStreetMap = 32,
        OpenStreetOsm = 33,
        OpenStreetMapnick = 34,
        RcsNominatim = 35,

        YandexMap = 40,
        YandexSatelliteMap = 41,
        YandexHybridMap = 42,

        YahooMap = 64,
        YahooSatellite = 128,
        YahooLabels = 256,
        YahooHybrid = 333,

        BingMap = 444,
        BingSatellite = 555,
        BingHybrid = 666,

        GisFile = 777
    }
}
