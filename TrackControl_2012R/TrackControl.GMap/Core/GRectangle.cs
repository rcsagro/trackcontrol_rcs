using System;

namespace TrackControl.GMap.Core
{
  /// <summary>
  /// ������������� �������.
  /// </summary>
  public struct GRectangle
  {
    public static readonly GRectangle Empty = new GRectangle();

    public int X
    {
      get { return _x; }
      set { _x = value; }
    }
    int _x;

    public int Y
    {
      get { return _y; }
      set { _y = value; }
    }
    int _y;

    public int Width
    {
      get { return _width; }
      set { _width = value; }
    }
    int _width;

    public int Height
    {
      get { return _height; }
      set { _height = value; }
    }
    int _height;

    public GRectangle(int x, int y, int width, int height)
    {
      _x = x;
      _y = y;
      _width = width;
      _height = height;
    }

    public GRectangle(GPoint location, GSize size)
    {
      _x = location.X;
      _y = location.Y;
      _width = size.Width;
      _height = size.Height;
    }

    public static GRectangle FromLTRB(int left, int top, int right, int bottom)
    {
      return new GRectangle(left, top, right - left, bottom - top);
    }

    public GPoint Location
    {
      get
      {
        return new GPoint(X, Y);
      }
      set
      {
        X = value.X;
        Y = value.Y;
      }
    }

    public GSize Size
    {
      get
      {
        return new GSize(Width, Height);
      }
      set
      {
        Width = value.Width;
        Height = value.Height;
      }
    }

    public int Left
    {
      get { return X; }
    }

    public int Top
    {
      get { return Y; }
    }

    public int Right
    {
      get { return X + Width; }
    }

    public int Bottom
    {
      get { return Y + Height; }
    }

    public bool IsEmpty
    {
      get
      {
        return _height == 0 && _width == 0 && _x == 0 && _y == 0;
      }
    }

    public override bool Equals(object obj)
    {
      if (!(obj is GRectangle))
        return false;

      GRectangle comp = (GRectangle)obj;

      return (comp.X == X) &&
         (comp.Y == Y) &&
         (comp.Width == Width) &&
         (comp.Height == Height);
    }

    public static bool operator ==(GRectangle left, GRectangle right)
    {
      return (left.X == right.X
                 && left.Y == right.Y
                 && left.Width == right.Width
                 && left.Height == right.Height);
    }

    public static bool operator !=(GRectangle left, GRectangle right)
    {
      return !(left == right);
    }

    public bool Contains(int x, int y)
    {
      return X <= x &&
         x < X + Width &&
         Y <= y &&
         y < Y + Height;
    }

    public bool Contains(GPoint pt)
    {
      return Contains(pt.X, pt.Y);
    }

    public bool Contains(GRectangle rect)
    {
      return (X <= rect.X) &&
         ((rect.X + rect.Width) <= (X + Width)) &&
         (Y <= rect.Y) &&
         ((rect.Y + rect.Height) <= (Y + Height));
    }

    public override int GetHashCode()
    {
      return (int)((UInt32)X ^
                     (((UInt32)Y << 13) | ((UInt32)Y >> 19)) ^
                     (((UInt32)Width << 26) | ((UInt32)Width >> 6)) ^
                     (((UInt32)Height << 7) | ((UInt32)Height >> 25)));
    }

    public void Inflate(int width, int height)
    {
      X -= width;
      Y -= height;
      Width += 2 * width;
      Height += 2 * height;
    }

    public void Inflate(GSize size)
    {

      Inflate(size.Width, size.Height);
    }

    public static GRectangle Inflate(GRectangle rect, int x, int y)
    {
      GRectangle r = rect;
      r.Inflate(x, y);
      return r;
    }

    public void Intersect(GRectangle rect)
    {
      GRectangle result = GRectangle.Intersect(rect, this);

      X = result.X;
      Y = result.Y;
      Width = result.Width;
      Height = result.Height;
    }

    public static GRectangle Intersect(GRectangle a, GRectangle b)
    {
      int x1 = Math.Max(a.X, b.X);
      int x2 = Math.Min(a.X + a.Width, b.X + b.Width);
      int y1 = Math.Max(a.Y, b.Y);
      int y2 = Math.Min(a.Y + a.Height, b.Y + b.Height);

      if (x2 >= x1
             && y2 >= y1)
      {

        return new GRectangle(x1, y1, x2 - x1, y2 - y1);
      }
      return GRectangle.Empty;
    }

    public bool IntersectsWith(GRectangle rect)
    {
      return (rect.X < X + Width) &&
         (X < (rect.X + rect.Width)) &&
         (rect.Y < Y + Height) &&
         (Y < rect.Y + rect.Height);
    }

    public static GRectangle Union(GRectangle a, GRectangle b)
    {
      int x1 = Math.Min(a.X, b.X);
      int x2 = Math.Max(a.X + a.Width, b.X + b.Width);
      int y1 = Math.Min(a.Y, b.Y);
      int y2 = Math.Max(a.Y + a.Height, b.Y + b.Height);

      return new GRectangle(x1, y1, x2 - x1, y2 - y1);
    }

    public void Offset(GPoint pos)
    {
      Offset(pos.X, pos.Y);
    }

    public void Offset(int x, int y)
    {
      X += x;
      Y += y;
    }

    public override string ToString()
    {
      return String.Format("{X={0}, Y={1}, Width={2}, Height={3}", X, Y, Width, Height);
    }
  }
}
