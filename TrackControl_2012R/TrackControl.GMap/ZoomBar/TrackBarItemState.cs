﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Map.Custom
{
    public enum TrackBarItemState
    {
        Active = 3,
        Disabled = 5,
        Hot = 2,
        Normal = 1
    }
}
