﻿namespace Graph
{
  partial class DifferentLineGraph
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._grid = new DevExpress.XtraVerticalGrid.VGridControl();
      this.categoryPoint1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
      this.point1_sensorName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.point1_dateTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.point1_value = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.categoryPoint2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
      this.point2_sensorName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.point2_dateTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.point2_value = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.categoryDiff = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
      this.pointDiff_dateTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.pointDiff_value = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this.btnClose = new DevExpress.XtraEditors.SimpleButton();
      this.btnClearSelect = new DevExpress.XtraEditors.SimpleButton();
      ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
      this.SuspendLayout();
      // 
      // _grid
      // 
      this._grid.Location = new System.Drawing.Point(12, 12);
      this._grid.Name = "_grid";
      this._grid.RecordWidth = 127;
      this._grid.RowHeaderWidth = 130;
      this._grid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryPoint1,
            this.categoryPoint2,
            this.categoryDiff});
      this._grid.Size = new System.Drawing.Size(266, 211);
      this._grid.TabIndex = 2;
      // 
      // categoryPoint1
      // 
      this.categoryPoint1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.point1_sensorName,
            this.point1_dateTime,
            this.point1_value});
      this.categoryPoint1.Name = "categoryPoint1";
      this.categoryPoint1.Properties.Caption = "Точка №1";
      // 
      // point1_sensorName
      // 
      this.point1_sensorName.Name = "point1_sensorName";
      this.point1_sensorName.Properties.Caption = "Название датчика";
      // 
      // point1_dateTime
      // 
      this.point1_dateTime.Name = "point1_dateTime";
      this.point1_dateTime.Properties.Caption = "Время";
      // 
      // point1_value
      // 
      this.point1_value.Name = "point1_value";
      this.point1_value.Properties.Caption = "Значение";
      // 
      // categoryPoint2
      // 
      this.categoryPoint2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.point2_sensorName,
            this.point2_dateTime,
            this.point2_value});
      this.categoryPoint2.Name = "categoryPoint2";
      this.categoryPoint2.Properties.Caption = "Точка №2";
      // 
      // point2_sensorName
      // 
      this.point2_sensorName.Name = "point2_sensorName";
      this.point2_sensorName.Properties.Caption = "Название датчика";
      // 
      // point2_dateTime
      // 
      this.point2_dateTime.Name = "point2_dateTime";
      this.point2_dateTime.Properties.Caption = "Время";
      // 
      // point2_value
      // 
      this.point2_value.Name = "point2_value";
      this.point2_value.Properties.Caption = "Значение";
      // 
      // categoryDiff
      // 
      this.categoryDiff.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.pointDiff_dateTime,
            this.pointDiff_value});
      this.categoryDiff.Name = "categoryDiff";
      this.categoryDiff.Properties.Caption = "Разница между точками";
      // 
      // pointDiff_dateTime
      // 
      this.pointDiff_dateTime.Name = "pointDiff_dateTime";
      this.pointDiff_dateTime.Properties.Caption = "Время";
      // 
      // pointDiff_value
      // 
      this.pointDiff_value.Name = "pointDiff_value";
      this.pointDiff_value.Properties.Caption = "Значение";
      // 
      // btnClose
      // 
      this.btnClose.Location = new System.Drawing.Point(203, 229);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 3;
      this.btnClose.Text = "Закрыть";
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // simpleButtonClearSelect
      // 
      this.btnClearSelect.Location = new System.Drawing.Point(12, 229);
      this.btnClearSelect.Name = "simpleButtonClearSelect";
      this.btnClearSelect.Size = new System.Drawing.Size(75, 23);
      this.btnClearSelect.TabIndex = 3;
      this.btnClearSelect.Text = "Очистить";
      this.btnClearSelect.Click += new System.EventHandler(this.simpleButtonClearSelect_Click);
      // 
      // DifferentLineGraph
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(287, 264);
      this.ControlBox = false;
      this.Controls.Add(this.btnClearSelect);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this._grid);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "DifferentLineGraph";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Вычисление разницы между точками";
      this.TopMost = true;
      ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraVerticalGrid.VGridControl _grid;
    private DevExpress.XtraEditors.SimpleButton btnClose;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point1_dateTime;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryPoint1;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryPoint2;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryDiff;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point1_value;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point1_sensorName;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point2_sensorName;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point2_dateTime;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow point2_value;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow pointDiff_dateTime;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow pointDiff_value;
    private DevExpress.XtraEditors.SimpleButton btnClearSelect;
  }
}