using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using ZedGraph;
using System.Runtime.Serialization;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.General;
using Graph.Properties;

namespace Graph
{
    [ToolboxItem(false)]
    public partial class ZGraphControl : UserControl, IGraph
    {
        /// <summary>
        /// �������� ��������� �������
        /// </summary>
        [Serializable]
        public class CurveState : ISerializable
        {
            /// <summary>
            /// ��������� 
            /// </summary>
            public bool isVisible;

            /// <summary>
            /// ����
            /// </summary>
            public Color color;

            /// <summary>
            /// ���
            /// </summary>
            public AlgorithmType type;

            /// <summary>
            /// ������� �����
            /// </summary>
            public float lineWidth;

            /// <summary>
            /// ID �������
            /// </summary>
            public int sensorId;

            public CurveState(SerializationInfo info, StreamingContext context)
            {
                isVisible = info.GetBoolean("Visible");
                color = (Color) info.GetValue("Color", typeof (System.Drawing.Color));
                type = (AlgorithmType) info.GetValue("Type",
                    typeof (AlgorithmType));
                lineWidth = (float) info.GetDouble("Width");
                sensorId = info.GetInt32("Sensor");
            }

            #region ISerializable Members

            void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("Visible", isVisible);
                info.AddValue("Color", color);
                info.AddValue("Type", type);
                info.AddValue("Width", lineWidth);
                info.AddValue("Sensor", sensorId);
            }

            #endregion
        }


        #region Attribute

        public struct MinMax
        {
            public MinMax(double min, double max)
            {
                this.min = min;
                this.max = max;
            }

            public double min;
            public double max;
        }

        private SelPoint selPoint;
        private bool select;
        //CurveList list;

        // ������ �����
        //List<PointPairList> series;
        /// <summary>
        /// ������ ��� ���������
        /// </summary>
        private GraphPane Pane;

        //int indexLeft = 0;
        //int indexRight = 0;
        //MasterPane masterPane;
        private double[] xValue;

        private bool isAutosize = false;
        private DifferentLineGraph diffForm = null; //new DifferentLineGraph();
        private SelPoint oldPointFirst;
        private SelPoint oldPointSecond;

        #endregion
        
        //������������ �������
        //public delegate void ActionEventHandler(object sender, ActionCancelEventArgs ev);
        public event ActionEventHandler Action;
        public event VoidHandler StartView;

        protected void OnAction(object sender, ActionCancelEventArgs ev)
        {
            if (Action != null)
            {
                Action(sender, ev);
            }
        }

        public ZGraphControl()
        {
            InitializeComponent();
            Init();

            _saveStripItem.Image = Shared.Save;
            _printStripItem.Image = Shared.Printer;
            _zoomFitStripItem.Image = Shared.ZoomFit;
            _zoomFitButton.Image = Shared.ZoomFit;
            _dragStripItem.Image = Shared.Hand;
            _zoomStripItem.Image = Shared.ZoomIn;
            _zoomInButton.Image = Shared.ZoomIn;
            _zoomOutButton.Image = Shared.ZoomOut;
            _leftSeriesButton.Image = Shared.ChartLine;
            _rightSeriesButton.Image = Shared.ChartLine;
            _balloonsOffButton.Image = Shared.BalloonMinus;
            _prevZoomButton.Image = Shared.Cancel;
            _prevZoomStripItem.Image = Shared.Cancel;
            _zoomLineButton.Image = Shared.ZoomLine;
            
            RemoveMouseEventHandler();
            //this.zedGraph.MouseDownEvent -= this.zedGraph_MouseDownEvent;

            this.Dock = DockStyle.Fill;
            // ������� ������ ��� ���������
            Pane = zedGraph.GraphPane;
            // Fill the axis background with a color gradient
            Pane.Chart.Fill = new Fill(Color.LightYellow);

            //masterPane = masterPane = zedGraph.MasterPane; 
            // ������� ������ ������ �� ��� ������, ���� �� ����� ������� ��� ���� ����������
            Pane.CurveList.Clear();
            // �������� ���������
            zedGraph.IsZoomOnMouseCenter = true;
            zedGraph.IsShowPointValues = true;
            zedGraph.PointValueFormat = "N1";
            zedGraph.PointDateFormat = Resources.ZGraphPointDateFormat;
            //series = new List<PointPairList>();
            // ��������� ��������������� ��������
            Pane.IsFontsScaled = false;
            //list = new CurveList();
            Pane.YAxis.Title.Text = "";
            Pane.YAxis.MajorGrid.IsZeroLine = false;
            Pane.YAxis.MajorTic.IsInside = false;
            Pane.YAxis.MinorTic.IsInside = false;
            Pane.YAxis.MajorTic.IsOpposite = false;
            Pane.YAxis.MinorTic.IsOpposite = false;
        }

        private void Init()
        {
            this._zoomStripItem.Text = Resources.ZGraphZoom;
            this._dragStripItem.Text = Resources.ZGraphDrag;
            this._zoomFitStripItem.Text = Resources.ZGraphZoomFit;
            this._prevZoomStripItem.Text = Resources.ZGraphPrevZoom;
            this._printStripItem.Text = Resources.ZGraphPrintf;
            this._saveStripItem.Text = Resources.ZGraphSaveAs;
            this.toolStripContainer1.Text = Resources.ZGraphTStrip1;
            this._leftSeriesButton.Text = Resources.ZGraphLeftSeries;
            this._leftSeriesButton.ToolTipText = Resources.ZGraphLeftSeries;
            this._rightSeriesButton.Text = Resources.ZGraphRightSeries;
            this._rightSeriesButton.ToolTipText = Resources.ZGraphRightSeries;
            this._zoomInButton.Text = Resources.ZGraphZoomIn;
            this._zoomInButton.ToolTipText = Resources.ZGraphZoomIn;
            this._zoomLineButton.Text = Resources.ZGraphZoomLine;
            this._zoomLineButton.ToolTipText = Resources.ZGraphZoomLine;
            this._zoomOutButton.Text = Resources.ZGraphZoomOut;
            this._zoomOutButton.ToolTipText = Resources.ZGraphZoomOut;
            this._zoomFitButton.Text = Resources.ZGraphZoomFit;
            this._zoomFitButton.ToolTipText = Resources.ZGraphZoomFit;
            this._dragButton.Text = Resources.ZGraphButtonDrag;
            this._dragButton.ToolTipText = Resources.ZGraphButtonDrag;
            this._prevZoomButton.Text = Resources.ZGraphPrevZoom;
            this._prevZoomButton.ToolTipText = Resources.ZGraphPrevZoom;
            this.toolStripButton3.Text = Resources.ZGraphTStrip3;
            this.toolStripButton3.ToolTipText = Resources.ZGraphZoomTime;
            this.toolStripButton6.Text = Resources.ZGraphTStrip6;
            this.toolStripButton6.ToolTipText = Resources.ZGraphTStrip6;
            this._balloonsOffButton.Text = Resources.ZGraphTtButtonPromptOff;
            this._balloonsOffButton.ToolTipText = Resources.ZGraphTtButtonPromptOff;
            this.printButton.Text = Resources.ZGraphPrintf;
            this.printButton.ToolTipText = Resources.ZGraphPrintf;
            this.ScreenShotsButton.ToolTipText = Resources.Graph_Screen_Shorts;

            _autosizeButton.Text = Resources.ZGraphTtButtonAutosize;
            _autosizeButton.ToolTipText = Resources.ZGraphTtButtonAutosize;
            _differentButton.Text = Resources.ZGraphTtButtonDifference;
            _differentButton.ToolTipText = Resources.ZGraphTtButtonDifference;
        }

        protected void SetAxis(YAxis yAxis, MinMax minmax)
        {
            yAxis.Scale.Min = minmax.min;
            yAxis.Scale.Max = minmax.max;
            yAxis.Scale.FontSpec.IsBold = false;
            yAxis.Scale.FontSpec.Size = 12;
            yAxis.Title.FontSpec.IsBold = false;
            yAxis.Title.FontSpec.Size = 12;
        }

        public void SetAxisYLimits(double minValue, double maxValue)
        {
            Pane.YAxisList[0].Scale.Min = minValue;
            Pane.YAxisList[0].Scale.Max = maxValue;
        }

        /// <summary>
        /// �������� ������������� ������� � ��� ������
        /// </summary>
        /// <param name="type"></param>
        protected void CheckSeries(AlgorithmType type)
        {
            int right = 0;
            int left = 0;
            foreach (CurveItem item in Pane.CurveList)
            {
                if (item.IsY2Axis)
                {
                    if ((AlgorithmType) (item.Tag) == type)
                    {
                        _rightSeriesButton.DropDownItems.RemoveAt(right);
                        Pane.CurveList.Remove(item);
                        break;
                    }
                    right++;
                }
                else
                {
                    if (System.Convert.ToInt32(item.Tag) == (int) type)
                    {
                        _leftSeriesButton.DropDownItems.RemoveAt(left);
                        Pane.CurveList.Remove(item);
                        break;
                    }
                    left++;
                }
            }
        }

        /// <summary>
        /// ���������� ������� �� ������ ��� "OLD"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        /// <param name="Y"></param>
        /// <param name="X"></param>
        /// <param name="type"></param>
        public void AddSeriesR(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type)
        {
            AddSeriesR(name, color, Y, X, type, true);
        }

        /// <summary>
        /// ���������� ������� �� ������ ��� "NEW". 
        /// �������� ���� ����������� ������� �� ���������, ����� ����������.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        /// <param name="Y"></param>
        /// <param name="X"></param>
        /// <param name="type"></param>
        /// <param name="visible"></param>
        public void AddSeriesR(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type, bool visible)
        {
            CheckSeries(type);
            xValue = new double[X.Length];
            for (int i = 0; i < X.Length; i++)
            {
                xValue[i] = new XDate(X[i]);
            }
            LineItem myCurve = Pane.AddCurve(name, xValue, Y, color, SymbolType.None);
            myCurve.IsY2Axis = true;
            myCurve.Tag = type;
            if (!visible) //type == ALG_TYPE.ACCEL || type == ALG_TYPE.TRAVEL)
            {
                myCurve.IsVisible = false; //������� � ���� �����
                myCurve.Label.IsVisible = false; //�������� ������ �������
            }
            AddSeriesItemRight(myCurve);
            myCurve.Line.SmoothTension = 0.5F;
            zedGraph.IsEnableZoom = false; //������ ������ �� ��������� c �������� Zoom
        }



        /// <summary>
        /// Add series item
        /// </summary>
        /// <param name="ds">Ds</param>
        private void AddSeriesItemLeft(CurveItem item)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem(item.Label.Text);
            menuItem.Checked = true;
            menuItem.Tag = item;
            menuItem.CheckOnClick = true;
            menuItem.ForeColor = item.Color;
            if (item.IsVisible)
            {
                menuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            }
            else
            {
                menuItem.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }
            menuItem.CheckStateChanged += new System.EventHandler(menuItemLeft_CheckStateChanged);
            _leftSeriesButton.DropDownItems.Add(menuItem);
        }


        private void menuItemLeft_CheckStateChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem Item = (ToolStripMenuItem) sender;
            CurveItem item = (CurveItem) Item.Tag;
            item.IsVisible = Item.Checked;

            Pane.YAxisList[item.YAxisIndex].IsVisible = Item.Checked;
            Pane.YAxis.Title.Text = "";

            foreach (CurveItem curve in Pane.CurveList)
            {
                if (!curve.IsY2Axis)
                {
                    if (curve.IsVisible)
                    {
                        curve.Label.IsVisible = true; //�������� ������ �������
                        Pane.YAxis.Title.Text += curve.Label.Text + " ";
                        Pane.YAxis.Scale.MaxAuto = true;
                        Pane.YAxis.Scale.MinAuto = true;
                    }
                    else
                    {
                        curve.Label.IsVisible = false;
                        Pane.YAxis.Scale.MaxAuto = true;
                        Pane.YAxis.Scale.MinAuto = true;
                    }
                }
            }
            if (Pane.YAxisList.Count > 0)
                Pane.YAxis.IsVisible = true;
            RefreshEventArgs();
        }

        /// <summary>
        /// ������������� �������, ���������� ������ � ������ ����
        /// </summary>
        /// <param name="seriesName"></param>
        public void SimulateMenuItemRightCheck(string seriesName)
        {
            foreach (ToolStripItem item in _rightSeriesButton.DropDownItems)
            {
                if (item.Text == seriesName)
                {
                    ((ToolStripMenuItem) item).Checked = true;
                    menuItemRight_CheckStateChanged(item, null);
                }
            }
        }

        /// <summary>
        /// ��������� ����������� ������� ���������� ������ � ������ ����
        /// ���������� -1 ���������, ��� 0,1 ������� �������� ����������� ������� 
        /// </summary>
        /// <param name="seriesName"></param>
        public int SeriesIsShow(string seriesName)
        {
            int ret = -1;
            foreach (ToolStripItem item in _rightSeriesButton.DropDownItems)
            {
                if (item.Text == seriesName)
                {
                    ret = 0;
                    if (((ToolStripMenuItem) item).Checked)
                        ret = 1;
                    break;
                }
            }
            return ret;
        }

        private void menuItemRight_CheckStateChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem Item = (ToolStripMenuItem) sender;
            CurveItem item = (CurveItem) Item.Tag;
            item.Label.IsVisible = Item.Checked; //�����t ��� �� ���������� �� �������
            item.IsVisible = Item.Checked; //������ ������ ���������
            zedGraph.Invalidate();
            Pane.Y2AxisList[item.YAxisIndex].IsVisible = Item.Checked;
            //Pane.Y2AxisList[item.YAxisIndex].
        }

        private void RefreshEventArgs()
        {
            // �������� ����� AxisChange (), ����� �������� ������ �� ����.
            // � ��������� ������ �� ������� ����� �������� ������ ����� �������,
            // ������� ��������� � ��������� �� ����, ������������� �� ���������
            try
            {
                zedGraph.AxisChange();

                // ��������� ������
                zedGraph.Invalidate();
            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error refresh event arguments");
            }
        }

        private bool RemoveItemY()
        {
            for (int i = 0; i < Pane.CurveList.Count; i++)
            {
                if (!Pane.CurveList[i].IsY2Axis)
                {
                    Pane.CurveList.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        private bool RemoveRegion(Type t)
        {
            for (int i = 0; i < Pane.GraphObjList.Count; i++)
            {
                if (Pane.GraphObjList[i].GetType().ToString() == (t.FullName))
                {
                    Pane.GraphObjList.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {

        }

        private void zedGraph_ContextMenuBuilder(ZedGraphControl sender, ContextMenuStrip menuStrip, Point mousePt,
            ZedGraphControl.ContextMenuObjectState objState)
        {

        }

        #region Event

        private string zedGraph_PointValueEvent(ZedGraphControl sender, GraphPane pane, CurveItem curve, int iPt)
        {
            PointPair pt = curve[iPt];
            XDate date = new XDate(pt.X);

            return curve.Label.Text + " " + pt.Y.ToString("0.#") + "\n" + Resources.ZGraphPVEdate + " " +
                   date.ToString(Resources.ZGraphPVEformatDate) +
                   "\n" + Resources.ZGraphPVEtime + " " + date.ToString(Resources.ZGraphPVEformatTime);
        }

        private bool zedGraph_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            object nearest;
            int index;
            Graphics graph = Graphics.FromHwnd(this.Handle);
            this.Pane.FindNearestObject(e.Location, graph, out nearest, out index);
            if (nearest is CurveItem)
            {
                CurveItem curve = (CurveItem) nearest;
                if (!curve.IsY2Axis)
                {
                    selPoint.time = new XDate(curve.Points[index].X);
                    selPoint.value = Math.Round(curve.Points[index].Y, 2);
                    selPoint.Label = curve.Label.Text;
                    selPoint.Type = (int)curve.Tag;
                    ActionCancelEventArgs ev = new ActionCancelEventArgs(selPoint);
                    OnAction(this, ev);
                    select = true;

                    SetPointOnGraph(selPoint);
                }
                else
                    select = false;
            }
            else
                select = false;
            graph.Dispose();
            return true;
        }

        private void SetPointOnGraph(SelPoint point)
        {
            ClearLabel(); // ���������� ��� �����

            if (oldPointFirst.Label == null)
            {
                oldPointFirst = point;
            }
            else
            {
                if (oldPointSecond.Label != null)
                {
                    oldPointFirst = oldPointSecond;
                }

                oldPointSecond = point;
                AddLabel(oldPointSecond.time, 0, oldPointSecond.value, oldPointSecond.value.ToString(), Color.BlueViolet);
            }

            AddLabel(oldPointFirst.time, 0, oldPointFirst.value, oldPointFirst.value.ToString(), Color.BlueViolet);
        }

        private void zedGraph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            foreach (GraphObj obj in Pane.GraphObjList)
            {
                if (obj is LineObj)
                {
                    ((LineObj) obj).Location.Height = (Pane.YAxis.Scale.Max - Pane.YAxis.Scale.Min)/100;
                    ((LineObj) obj).Location.Width = (Pane.XAxis.Scale.Max - Pane.XAxis.Scale.Min)/50;
                }
            }
            foreach (GraphObj obj in Pane.GraphObjList)
            {
                if (obj is TextObj)
                {
                    if (((TextObj) obj).Tag is LineObj)
                    {
                        LineObj line = (LineObj) ((TextObj) obj).Tag;
                        ((TextObj) obj).Location.X = line.Location.X2;
                        ((TextObj) obj).Location.Y = line.Location.Y2;
                    }
                }
            }
        }

        #endregion

        #region Button

        private void toolStripButton_Scale_Click(object sender, EventArgs e)
        {
            ////
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zedGraph.SaveAs();
        }

        private void toolStripButton_Zoom_Click(object sender, EventArgs e)
        {
            zedGraph.IsEnableHPan = false;
            zedGraph.IsEnableVPan = false;
            zedGraph.IsEnableZoom = true;
            //zedGraph.IsEnableVZoom = true;
        }

        /// <summary>
        /// ����������� ����� ����������� �� ���������� ������ �� �����������/���������� ����� � �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonVZoom_Click(object sender, EventArgs e)
        {
            zedGraph.IsEnableHPan = false;
            zedGraph.IsEnableVPan = false;
            zedGraph.IsEnableZoom = true;
            zedGraph.IsEnableVZoom = false;
        }

        private void toolStripButton_Full_Click(object sender, EventArgs e)
        {
            ShowFullGraph();
            if (Pane.Title.Text != "Title")
                ShowSeries(Pane.Title.Text);
        }

        private void ShowFullGraph()
        {
            if (Pane.CurveList.Count <= 0)
                return;

            try
            {
                zedGraph.RestoreScale(Pane);
                // ������������� ������������ ��� �������� �� ��� X
                Pane.XAxis.Scale.FontSpec.Size = 12;
                Pane.XAxis.Title.FontSpec.Size = 12;
                Pane.XAxis.Title.Text = Resources.ZGraphPVEtime;
                Pane.XAxis.Scale.Format = Resources.ZGraphPaneXAxisScaleFormat;
                Pane.XAxis.MajorGrid.IsVisible = true;
                Pane.XAxis.MinorGrid.IsVisible = true;
                Pane.XAxis.Scale.MajorStep = 1;
                Pane.XAxis.Scale.MinorStep = 0.5F;
                ////Pane.ZoomStack.Clear();
                zedGraph.AxisChange();
                // ��������� ������
                zedGraph.Invalidate();
            }
            catch(Exception ex)
            {
               // MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error show full graph");
            }
        }

        private void toolStripButton_Translate_Click(object sender, EventArgs e)
        {
            zedGraph.IsEnableZoom = false;
            zedGraph.IsEnableHPan = true;
            zedGraph.IsEnableVPan = true;
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (Pane.XAxis.Type == AxisType.Date)
            {
                Pane.XAxis.Type = AxisType.DateAsOrdinal;
            }
            else
            {
                Pane.XAxis.Type = AxisType.Date;
            }

            zedGraph.RestoreScale(Pane);
            zedGraph.AxisChange();
            // ��������� ������
            zedGraph.Invalidate();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            if (zedGraph.IsShowPointValues)
            {
                _balloonsOffButton.Image = Shared.BalloonPlus;
                _balloonsOffButton.ToolTipText = Resources.ZGraphTtButtonPromptOn;
                zedGraph.IsShowPointValues = false;
            }
            else
            {
                _balloonsOffButton.Image = Shared.BalloonMinus;
                zedGraph.IsShowPointValues = true;
                _balloonsOffButton.ToolTipText = Resources.ZGraphTtButtonPromptOff;
            }
        }

        private void undoZoomButton_Click(object sender, EventArgs e)
        {
            zedGraph.IsEnableVZoom = true;
            zedGraph.ZoomOut(Pane);
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            PrintDocument printDoc = new PrintDocument();
            printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Graph_PrintPage);

            //PrintPreviewDialog ppd = new PrintPreviewDialog();
            //ppd.Document = printDoc;
            //ppd.ShowDialog();

            PrintDialog pDlg = new PrintDialog();
            pDlg.Document = printDoc;
            if (pDlg.ShowDialog() == DialogResult.OK)
                printDoc.Print();


            //try
            //{
            //  if (printDoc != null)
            //  {
            //    printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Graph_PrintPage);
            //    //PrintPreviewDialog ppDlg = new PrintPreviewDialog();
            //    PrintDialog pDlg = new PrintDialog();
            //    pDlg.Document = printDoc;
            //    pDlg.UseEXDialog = true; // NEED TO SET THIS TO TRUE ON 64-BIT SYSTEMS
            //    if (pDlg.ShowDialog() == DialogResult.OK)
            //      printDoc.Print();
            //  }
            //}
            //catch (Exception exception)
            //{
            //  MessageBox.Show(exception.Message);
            //}
        }

        /// <summary>
        /// ������������� ������ ��� ��������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Graph_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap image = zedGraph.MasterPane.GetImage();
            e.Graphics.DrawImage(image, zedGraph.MasterPane.Rect);
        }

        private void toolStripButtonAutosize_Click(object sender, EventArgs e)
        {
            if (!isAutosize)
            {
                isAutosize = true;
                _autosizeButton.Checked = true;
            }
            else
            {
                isAutosize = false;
                _autosizeButton.Checked = false;
            }
            ShowSeries(Pane.Title.Text);
        }

        private void ShowDefaultGraph()
        {
            GraphPane pane = zedGraph.GraphPane;
            int curveList = pane.CurveList.Count;

            for( int i = 0; i < curveList; i++ )
            {
                LineItem crvItem = ( LineItem )pane.CurveList[i];
                // �������� ��������� ����� ��������
                crvItem.Symbol.Type = SymbolType.None;
                zedGraph.Invalidate();
            }

            diffForm.Dispose();
            diffForm = null;
            RemoveMouseEventHandler();
        }

        private void toolStripButtonDifferent_Click(object sender, EventArgs e)
        {
            if(diffForm != null)
                return;

            diffForm = new DifferentLineGraph();
            diffForm.showDefaultGraph += new DifferentLineGraph.showDefaultGraphHandler(ShowDefaultGraph);
            diffForm.Show();
            diffForm.FormClosed += new FormClosedEventHandler(DifferentLineGraph_Close);
            Action += new ActionEventHandler(diffForm.ZGraphControl_Action);
            diffForm.clearEvent += new DifferentLineGraph.FormButtonClearEvent(DiffForm_clearEvent);
            AddMouseEventHandler();

            // ����������� �������
            GraphPane pane = zedGraph.GraphPane;
            int curveList = pane.CurveList.Count;

            for (int i = 0; i < curveList; i++)
            {
                LineItem crvItem = (LineItem) pane.CurveList[i];
                // ���������� ����� ���������
                crvItem.Symbol.Type = SymbolType.Diamond;
                // ���� ���������� ������� (��������) - � ������� ����
                crvItem.Symbol.Fill.Color = crvItem.Color;
                // ��� ���������� - �������� �������
                crvItem.Symbol.Fill.Type = FillType.Solid;
                // ������ ��������
                crvItem.Symbol.Size = 3;
                // ��������� ������
                zedGraph.Invalidate();
            } // for
        }

        private void btRun_Click(object sender, EventArgs e)
        {
            StartView();
        }

        #endregion

        private void DifferentLineGraph_Close(object sender, EventArgs e)
        {
            Action -= new ActionEventHandler(diffForm.ZGraphControl_Action);
            RemoveMouseEventHandler();
            oldPointFirst = new SelPoint();
            oldPointSecond = new SelPoint();
            ClearLabel();
            zedGraph.Refresh();
        }

        private void DiffForm_clearEvent()
        {
            oldPointFirst = new SelPoint();
            oldPointSecond = new SelPoint();
            ClearLabel();
            zedGraph.Refresh();
        }

        #region IGraph Members

        /// <summary>
        /// Add Time Region
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        public void AddTimeRegion(DateTime begin, DateTime end)
        {
            double x = new XDate(begin);
            double width = new XDate(end) - x;
            double heigh = Pane.YAxis.Scale.Max;
            BoxObj box = new BoxObj(x, 0, width, 1,
                Color.Empty, Color.FromArgb(225, 245, 225));
            box.Location.CoordinateFrame = CoordType.XScaleYChartFraction;
            box.Location.AlignH = AlignH.Left;
            box.Location.AlignV = AlignV.Top;
            // place the box behind the axis items, so the grid is drawn on top of it
            box.ZOrder = ZOrder.F_BehindGrid;
            Pane.GraphObjList.Add(box);
            zedGraph.Invalidate();
        }
        
        /// <summary>
        /// Add Time Region
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        public void AddDangerRegion(DateTime begin, DateTime end, Color dangerColor)
        {
            double x = new XDate(begin);
            double width = new XDate(end) - x;
            double heigh = Pane.YAxis.Scale.Max;
            BoxObj box = new BoxObj(x, 0, width, 1, Color.Empty, dangerColor);
            box.Location.CoordinateFrame = CoordType.XScaleYChartFraction;
            box.Location.AlignH = AlignH.Left;
            box.Location.AlignV = AlignV.Top;
            // place the box behind the axis items, so the grid is drawn on top of it
            box.ZOrder = ZOrder.F_BehindGrid;
            Pane.GraphObjList.Add(box);
            zedGraph.Invalidate();
        }

        /// <summary>
        /// Clear Region
        /// </summary>
        public void ClearRegion()
        {
            while (RemoveRegion(typeof (BoxObj))) ;
        }

        /// <summary>
        /// Clear Label
        /// </summary>
        public void ClearLabel()
        {
            while (RemoveRegion(typeof (TextObj))) ;
            while (RemoveRegion(typeof (ArrowObj))) ;
            while (RemoveRegion(typeof (LineObj))) ;
        }

        /// <summary>
        /// Clear series
        /// </summary>
        public void ClearSeries()
        {
            ClearLabel();
            Pane.YAxis.Title.Text = "";
            Pane.CurveList.Clear();
            _rightSeriesButton.DropDownItems.Clear();
            _leftSeriesButton.DropDownItems.Clear();
        }

        /// <summary>
        /// Add Label
        /// </summary>
        /// <param name="time"></param>
        /// <param name="beginY"></param>
        /// <param name="endY"></param>
        /// <param name="label"></param>
        /// <param name="color"></param>
        public void AddLabel(DateTime time, double beginY, double endY, string label, Color color)
        {
            double xValue = new XDate(time);

            // �������� �������
            // ���������� �����, ���� ��������� �������
            // ���������� ��������� � ����
            double xstart = xValue;
            double ystart = endY;

            // ���������� ����� ������ �������
            double xend = xstart + (Pane.XAxis.Scale.Max - Pane.XAxis.Scale.Min)/50;
            double yend = ystart + (Pane.YAxis.Scale.Max - Pane.YAxis.Scale.Min)/100;

            // ��������� ����� � �������
            // �������� �����
            LineObj line = new LineObj(xstart, ystart, xend, yend);
            line.Line.Color = color;
            // ������� ����� � ������ ������������ ��������
            Pane.GraphObjList.Add(line);

            // ������� ����� ����� ����� �����
            // ���������� ��������� � ����
            TextObj text = new TextObj(label, xend, yend);
            text.Tag = line;
            text.Location.AlignV = AlignV.Bottom;
            text.Location.AlignH = AlignH.Left;
            text.FontSpec.FontColor = color;
            // �������� ����� ������ ������
            text.FontSpec.Border.Color = color;
            //Color colorFill = Color.FromArgb(color.R,color.G - 50,color.B);
            //text.FontSpec.Border.GradientFill.Color = colorFill;
            //text.FontSpec.Border.IsVisible = false;

            // ������� ����� � ������ ������������ ��������
            Pane.GraphObjList.Add(text);

            // ��������� ������
            zedGraph.Invalidate();
        }

        /// <summary>
        /// ShowSeries
        /// </summary>
        /// <param name="title"></param>
        public void ShowSeries(string title)
        {
            Pane.Title.Text = title;
            if (xValue == null || xValue.Length == 0)
            {
                return;
            }
            Pane.Y2AxisList.Clear();

            // ������������� ������������ ��� �������� �� ��� X
            Pane.XAxis.Scale.FontSpec.Size = 12;
            Pane.XAxis.Title.FontSpec.Size = 12;
            Pane.XAxis.Title.Text = Resources.ZGraphPVEtime;
            Pane.XAxis.Scale.Min = xValue[0];
            Pane.XAxis.Scale.Max = xValue[xValue.Length - 1];
            Pane.XAxis.Scale.Format = Resources.ZGraphPaneXAxisScaleFormat;
            Pane.XAxis.MajorGrid.IsVisible = true;
            Pane.XAxis.MinorGrid.IsVisible = true;
            Pane.XAxis.Scale.MajorStep = 1;
            Pane.XAxis.Scale.MinorStep = 0.5F;
            Pane.XAxis.Type = AxisType.Date;
            
            int indexRight = 0;
            Pane.YAxis.Title.Text = "";
            Pane.YAxisList[0].Scale.Min = 0;
            Pane.YAxisList[0].Scale.Max = 100;

            foreach (ZedGraph.CurveItem curve in Pane.CurveList)
            {
                double xmin, xmax, ymin, ymax;
                if (curve.IsY2Axis)
                {
                    Y2Axis yAxis = new Y2Axis(curve.Label.Text);
                    yAxis.Title.FontSpec.FontColor = curve.Color;
                    yAxis.Scale.FontSpec.FontColor = curve.Color;
                    yAxis.Color = curve.Color;
                    bool isYAxisVisible = curve.IsVisible;
                    if (curve.Tag is AlgorithmType)
                    {
                        if ((AlgorithmType) curve.Tag == AlgorithmType.INCLINOMETER_Y)
                        {
                            isYAxisVisible = false;
                        }
                    }

                    yAxis.IsVisible = isYAxisVisible; //yAxis.IsVisible = true;
                    yAxis.MajorGrid.IsZeroLine = false;
                    yAxis.MajorTic.IsInside = false;
                    yAxis.MinorTic.IsInside = false;
                    yAxis.MajorTic.IsOpposite = false;
                    yAxis.MinorTic.IsOpposite = false;

                    Pane.Y2AxisList.Add(yAxis);
                    curve.GetRange(out xmin, out xmax, out ymin, out ymax, false /*true*/, false, Pane);

                    if (ymax < -1000000 || (ymax < 1 && ymax > -1))
                    {
                        ymin = -1;
                        ymax = 10;
                    }
                    else
                    {
                        if (ymax == ymin)
                        {
                            ymin = ymin - 1;
                            ymax = ymax + 1;
                        }
                    }

                    Pane.Y2AxisList[indexRight].Scale.Min = ymin;
                    Pane.Y2AxisList[indexRight].Scale.Max = ymax;
                    Pane.Y2Axis.IsVisible = true;
                    Pane.YAxis.IsVisible = true;
                    curve.IsY2Axis = true;
                    curve.YAxisIndex = indexRight++;
                }
                else
                {
                    if (curve.IsVisible)
                    {
                        Pane.YAxis.Title.Text += curve.Label.Text;
                    }

                    Pane.YAxis.MajorGrid.IsZeroLine = false;
                    Pane.YAxis.IsVisible = curve.IsVisible; //Pane.YAxis.IsVisible = true;
                    //Add min-max
                    curve.GetRange(out xmin, out xmax, out ymin, out ymax, false /*true*/, false, Pane);
                    if (ymax < 1 && ymax > -1)
                        if (ymax == 0)
                            ymax = 1;
                    if (ymin > 0 && !isAutosize) // && Math.Abs(ymax - ymin) < 10)
                    {
                        ymin = 0;
                        ymax = ymax*1.2;
                    }
                    else
                        ymax = ymax*1.05;

                    if (Pane.YAxisList[0].Scale.Min > ymin)
                        Pane.YAxisList[0].Scale.Min = ymin;
                    if (Pane.YAxisList[0].Scale.Max < ymax)
                        Pane.YAxisList[0].Scale.Max = ymax;
                }
            }

            if (Pane.CurveList.Count > 0)
                RefreshEventArgs();
        }

        /// <summary>
        /// Add series item
        /// </summary>
        /// <param name="ds">Ds</param>
        private void AddSeriesItemRight(CurveItem item)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem(item.Label.Text);
            menuItem.Checked = true;
            menuItem.Tag = item;
            menuItem.CheckOnClick = true;
            menuItem.ForeColor = item.Color;
            if (item.IsVisible)
            {
                menuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            }
            else
            {
                menuItem.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }
            menuItem.CheckStateChanged += new System.EventHandler(menuItemRight_CheckStateChanged);
            _rightSeriesButton.DropDownItems.Add(menuItem);
        } // AddSeriesItem()

        /// <summary>
        /// ���������� ������� �� ����� ��� "OLD"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        /// <param name="Y"></param>
        /// <param name="X"></param>
        /// <param name="type"></param>
        public void AddSeriesL(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type)
        {
            AddSeriesL(name, color, Y, X, type, true);
        }

        /// <summary>
        /// ���������� ������� �� ����� ��� "NEW". 
        /// �������� ���� ����������� ������� �� ���������, ����� ����������.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        /// <param name="Y"></param>
        /// <param name="X"></param>
        /// <param name="type"></param>
        /// <param name="visible"></param>
        public void AddSeriesL(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type, bool visible)
        {
            CheckSeries(type);
            xValue = new double[X.Length];
            for (int i = 0; i < X.Length; i++)
            {
                xValue[i] = new XDate(X[i]);
            }

            LineItem myCurve = Pane.AddCurve(name, xValue, Y, color, SymbolType.None);
            myCurve.Tag = type;
            if (!visible)
            {
                myCurve.IsVisible = false; //������� � ���� �����
                myCurve.Label.IsVisible = false; //�������� ������ �������
            }
            AddSeriesItemLeft(myCurve);
            myCurve.Line.SmoothTension = 0.5F;
        }

        public bool GetSelectPoint(out DateTime time, out double value)
        {
            bool state = select;
            time = selPoint.time;
            value = selPoint.value;
            select = false;
            return state;
        }

        public void AddMouseEventHandler()
        {
            zedGraph.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zedGraph_MouseDownEvent);
        }

        public void RemoveMouseEventHandler()
        {
            this.zedGraph.MouseDownEvent -=
                new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zedGraph_MouseDownEvent);
        }

        /// <summary>
        /// Clear zoom graphic
        /// </summary>
        public void ClearGraphZoom()
        {
            ShowFullGraph();
        }

        /// <summary>
        /// ToolStrip button Zoom click
        /// </summary>
        public void SellectZoom()
        {
            this.toolStripButton_Zoom_Click(this, null);
        }

        public void VisibleGraph(bool visibl)
        {
            this.zedGraph.Visible = visibl;
        }

        #endregion

        public void SetRunVisible()
        {
            btRun.Visible = true;
        }

        private void ScreenShotsButton_Click(object sender, EventArgs e)
        {
            ImageService.SaveImage( Screenshot, "Graph_Shot" );
        }

        /// <summary>
        /// gets image of the current view
        /// </summary>
        /// <returns></returns>
        public Image Screenshot
        {
            get
            {
                Image result = null;
                try
                {
                    using (Bitmap bitmap = new Bitmap(Width, Height))
                    {
                        using (Graphics g = Graphics.FromImage(bitmap))
                        {
                            g.CopyFromScreen(PointToScreen(new Point()).X, PointToScreen(new Point()).Y, 0, 0,
                                new Size(Width, Height));

                            using (Font f = new Font("Arial", 9f))
                            {
                                string s = String.Format("TrackControl : {0:F}", DateTime.Now);
                                Brush shadowBrush = Brushes.Yellow;
                                g.DrawString(s, f, shadowBrush, 7, 22);
                                g.DrawString(s, f, shadowBrush, 7, 21);
                                g.DrawString(s, f, shadowBrush, 8, 21);
                                g.DrawString(s, f, shadowBrush, 9, 21);
                                g.DrawString(s, f, shadowBrush, 9, 22);
                                g.DrawString(s, f, shadowBrush, 9, 23);
                                g.DrawString(s, f, shadowBrush, 8, 23);
                                g.DrawString(s, f, shadowBrush, 7, 23);
                                g.DrawString(s, f, Brushes.Red, 8, 22);
                            }
                        }

                        using (MemoryStream stream = new MemoryStream())
                        {
                            bitmap.Save(stream, ImageFormat.Png);
                            result = Image.FromStream(stream);
                        }
                    }
                }
                catch
                {
                    return null;
                }
                return result;
            }
        } // Screenshot
    }
}
