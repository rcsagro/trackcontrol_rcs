namespace Graph
{
  partial class ZGraphControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZGraphControl));
            this.zedGraph = new ZedGraph.ZedGraphControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._zoomStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this._dragStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this._zoomFitStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this._prevZoomStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._printStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this._saveStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this._leftSeriesButton = new System.Windows.Forms.ToolStripSplitButton();
            this._rightSeriesButton = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btRun = new System.Windows.Forms.ToolStripButton();
            this._zoomInButton = new System.Windows.Forms.ToolStripButton();
            this._zoomLineButton = new System.Windows.Forms.ToolStripButton();
            this._zoomOutButton = new System.Windows.Forms.ToolStripButton();
            this._zoomFitButton = new System.Windows.Forms.ToolStripButton();
            this._dragButton = new System.Windows.Forms.ToolStripButton();
            this._prevZoomButton = new System.Windows.Forms.ToolStripButton();
            this._autosizeButton = new System.Windows.Forms.ToolStripButton();
            this._differentButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this._balloonsOffButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.ScreenShotsButton = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // zedGraph
            // 
            this.zedGraph.AutoSize = true;
            this.zedGraph.BackColor = System.Drawing.SystemColors.Control;
            this.zedGraph.ContextMenuStrip = this.contextMenuStrip1;
            this.zedGraph.Cursor = System.Windows.Forms.Cursors.Default;
            this.zedGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraph.IsAntiAlias = true;
            this.zedGraph.IsEnableVZoom = false;
            this.zedGraph.Location = new System.Drawing.Point(0, 0);
            this.zedGraph.Name = "zedGraph";
            this.zedGraph.PanModifierKeys = System.Windows.Forms.Keys.None;
            this.zedGraph.PointDateFormat = "T";
            this.zedGraph.ScrollGrace = 0D;
            this.zedGraph.ScrollMaxX = 0D;
            this.zedGraph.ScrollMaxY = 0D;
            this.zedGraph.ScrollMaxY2 = 0D;
            this.zedGraph.ScrollMinX = 0D;
            this.zedGraph.ScrollMinY = 0D;
            this.zedGraph.ScrollMinY2 = 0D;
            this.zedGraph.Size = new System.Drawing.Size(727, 506);
            this.zedGraph.TabIndex = 0;
            this.zedGraph.ContextMenuBuilder += new ZedGraph.ZedGraphControl.ContextMenuBuilderEventHandler(this.zedGraph_ContextMenuBuilder);
            this.zedGraph.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.zedGraph_ZoomEvent);
            this.zedGraph.PointValueEvent += new ZedGraph.ZedGraphControl.PointValueHandler(this.zedGraph_PointValueEvent);
            this.zedGraph.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zedGraph_MouseDownEvent);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._zoomStripItem,
            this._dragStripItem,
            this._zoomFitStripItem,
            this._prevZoomStripItem,
            this.toolStripSeparator3,
            this._printStripItem,
            this._saveStripItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(202, 142);
            // 
            // _zoomStripItem
            // 
            this._zoomStripItem.AutoToolTip = true;
            this._zoomStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_zoomStripItem.Image")));
            this._zoomStripItem.Name = "_zoomStripItem";
            this._zoomStripItem.Size = new System.Drawing.Size(201, 22);
            this._zoomStripItem.Text = "���������������";
            this._zoomStripItem.Click += new System.EventHandler(this.toolStripButton_Zoom_Click);
            // 
            // _dragStripItem
            // 
            this._dragStripItem.AutoToolTip = true;
            this._dragStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_dragStripItem.Image")));
            this._dragStripItem.Name = "_dragStripItem";
            this._dragStripItem.Size = new System.Drawing.Size(201, 22);
            this._dragStripItem.Text = "�����������";
            this._dragStripItem.Click += new System.EventHandler(this.toolStripButton_Translate_Click);
            // 
            // _zoomFitStripItem
            // 
            this._zoomFitStripItem.AutoToolTip = true;
            this._zoomFitStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_zoomFitStripItem.Image")));
            this._zoomFitStripItem.Name = "_zoomFitStripItem";
            this._zoomFitStripItem.Size = new System.Drawing.Size(201, 22);
            this._zoomFitStripItem.Text = "�������� ���";
            this._zoomFitStripItem.Click += new System.EventHandler(this.toolStripButton_Full_Click);
            // 
            // _prevZoomStripItem
            // 
            this._prevZoomStripItem.AutoToolTip = true;
            this._prevZoomStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_prevZoomStripItem.Image")));
            this._prevZoomStripItem.Name = "_prevZoomStripItem";
            this._prevZoomStripItem.Size = new System.Drawing.Size(201, 22);
            this._prevZoomStripItem.Text = "���������� �������";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(198, 6);
            // 
            // _printStripItem
            // 
            this._printStripItem.AutoToolTip = true;
            this._printStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_printStripItem.Image")));
            this._printStripItem.Name = "_printStripItem";
            this._printStripItem.Size = new System.Drawing.Size(201, 22);
            this._printStripItem.Text = "������";
            this._printStripItem.Click += new System.EventHandler(this.printButton_Click);
            // 
            // _saveStripItem
            // 
            this._saveStripItem.AutoToolTip = true;
            this._saveStripItem.Image = ((System.Drawing.Image)(resources.GetObject("_saveStripItem.Image")));
            this._saveStripItem.Name = "_saveStripItem";
            this._saveStripItem.Size = new System.Drawing.Size(201, 22);
            this._saveStripItem.Text = "��������� ���";
            this._saveStripItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.zedGraph);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(727, 506);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(727, 531);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._leftSeriesButton,
            this.toolStripSeparator1,
            this.btRun,
            this._zoomInButton,
            this._zoomLineButton,
            this._zoomOutButton,
            this._zoomFitButton,
            this._dragButton,
            this._prevZoomButton,
            this._autosizeButton,
            this._differentButton,
            this.toolStripButton3,
            this._rightSeriesButton,
            this.toolStripButton6,
            this._balloonsOffButton,
            this.toolStripSeparator2,
            this.printButton,
            this.ScreenShotsButton});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(441, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // _leftSeriesButton
            // 
            this._leftSeriesButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._leftSeriesButton.Image = ((System.Drawing.Image)(resources.GetObject("_leftSeriesButton.Image")));
            this._leftSeriesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._leftSeriesButton.Name = "_leftSeriesButton";
            this._leftSeriesButton.Size = new System.Drawing.Size(32, 22);
            this._leftSeriesButton.Text = "����� ������";
            this._leftSeriesButton.ToolTipText = "����� ������";
            // 
            // _rightSeriesButton
            // 
            this._rightSeriesButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._rightSeriesButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._rightSeriesButton.Image = ((System.Drawing.Image)(resources.GetObject("_rightSeriesButton.Image")));
            this._rightSeriesButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._rightSeriesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._rightSeriesButton.Name = "_rightSeriesButton";
            this._rightSeriesButton.Size = new System.Drawing.Size(32, 22);
            this._rightSeriesButton.Text = "������ ������";
            this._rightSeriesButton.ToolTipText = "������ ������ ";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btRun
            // 
            this.btRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btRun.Image = ((System.Drawing.Image)(resources.GetObject("btRun.Image")));
            this.btRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRun.Name = "btRun";
            this.btRun.Size = new System.Drawing.Size(23, 22);
            this.btRun.Text = "����";
            this.btRun.Visible = false;
            this.btRun.Click += new System.EventHandler(this.btRun_Click);
            // 
            // _zoomInButton
            // 
            this._zoomInButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._zoomInButton.Image = ((System.Drawing.Image)(resources.GetObject("_zoomInButton.Image")));
            this._zoomInButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._zoomInButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._zoomInButton.Name = "_zoomInButton";
            this._zoomInButton.Size = new System.Drawing.Size(23, 22);
            this._zoomInButton.Text = "���������";
            this._zoomInButton.ToolTipText = "���������";
            this._zoomInButton.Click += new System.EventHandler(this.toolStripButton_Zoom_Click);
            // 
            // _zoomLineButton
            // 
            this._zoomLineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._zoomLineButton.Image = ((System.Drawing.Image)(resources.GetObject("_zoomLineButton.Image")));
            this._zoomLineButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._zoomLineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._zoomLineButton.Name = "_zoomLineButton";
            this._zoomLineButton.Size = new System.Drawing.Size(23, 22);
            this._zoomLineButton.Text = "������������� ������ �� �����������";
            this._zoomLineButton.ToolTipText = "������������� ������ �� �����������";
            this._zoomLineButton.Click += new System.EventHandler(this.toolStripButtonVZoom_Click);
            // 
            // _zoomOutButton
            // 
            this._zoomOutButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._zoomOutButton.Image = ((System.Drawing.Image)(resources.GetObject("_zoomOutButton.Image")));
            this._zoomOutButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._zoomOutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._zoomOutButton.Name = "_zoomOutButton";
            this._zoomOutButton.Size = new System.Drawing.Size(23, 22);
            this._zoomOutButton.Text = "���������";
            this._zoomOutButton.ToolTipText = "���������";
            this._zoomOutButton.Visible = false;
            this._zoomOutButton.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // _zoomFitButton
            // 
            this._zoomFitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._zoomFitButton.Image = ((System.Drawing.Image)(resources.GetObject("_zoomFitButton.Image")));
            this._zoomFitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._zoomFitButton.Name = "_zoomFitButton";
            this._zoomFitButton.Size = new System.Drawing.Size(23, 22);
            this._zoomFitButton.Text = "�������� ���";
            this._zoomFitButton.ToolTipText = "�������� ���";
            this._zoomFitButton.Click += new System.EventHandler(this.toolStripButton_Full_Click);
            // 
            // _dragButton
            // 
            this._dragButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._dragButton.Image = ((System.Drawing.Image)(resources.GetObject("_dragButton.Image")));
            this._dragButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._dragButton.Name = "_dragButton";
            this._dragButton.Size = new System.Drawing.Size(23, 22);
            this._dragButton.Text = "�����������";
            this._dragButton.ToolTipText = "�����������";
            this._dragButton.Click += new System.EventHandler(this.toolStripButton_Translate_Click);
            // 
            // _prevZoomButton
            // 
            this._prevZoomButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._prevZoomButton.Image = ((System.Drawing.Image)(resources.GetObject("_prevZoomButton.Image")));
            this._prevZoomButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._prevZoomButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._prevZoomButton.Name = "_prevZoomButton";
            this._prevZoomButton.Size = new System.Drawing.Size(23, 22);
            this._prevZoomButton.Text = "���������� �������";
            this._prevZoomButton.ToolTipText = "���������� �������";
            this._prevZoomButton.Click += new System.EventHandler(this.undoZoomButton_Click);
            // 
            // _autosizeButton
            // 
            this._autosizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._autosizeButton.Image = ((System.Drawing.Image)(resources.GetObject("_autosizeButton.Image")));
            this._autosizeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._autosizeButton.Name = "_autosizeButton";
            this._autosizeButton.Size = new System.Drawing.Size(23, 22);
            this._autosizeButton.Text = "������������������ �������";
            this._autosizeButton.ToolTipText = "������������������ �������";
            this._autosizeButton.Click += new System.EventHandler(this.toolStripButtonAutosize_Click);
            // 
            // _differentButton
            // 
            this._differentButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._differentButton.Image = ((System.Drawing.Image)(resources.GetObject("_differentButton.Image")));
            this._differentButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._differentButton.Name = "_differentButton";
            this._differentButton.Size = new System.Drawing.Size(23, 22);
            this._differentButton.Text = "������� ����� �������";
            this._differentButton.ToolTipText = "������� ����� �������";
            this._differentButton.Click += new System.EventHandler(this.toolStripButtonDifferent_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "�������������� �� �������";
            this.toolStripButton3.Visible = false;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton_Scale_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "toolStripButton6";
            this.toolStripButton6.ToolTipText = "toolStripButton6";
            this.toolStripButton6.Visible = false;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // _balloonsOffButton
            // 
            this._balloonsOffButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._balloonsOffButton.Image = ((System.Drawing.Image)(resources.GetObject("_balloonsOffButton.Image")));
            this._balloonsOffButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._balloonsOffButton.Name = "_balloonsOffButton";
            this._balloonsOffButton.Size = new System.Drawing.Size(23, 22);
            this._balloonsOffButton.Text = "��������� ���������";
            this._balloonsOffButton.ToolTipText = "��������� ���������";
            this._balloonsOffButton.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // printButton
            // 
            this.printButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printButton.Image = ((System.Drawing.Image)(resources.GetObject("printButton.Image")));
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(23, 22);
            this.printButton.Text = "������";
            this.printButton.ToolTipText = "������";
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // ScreenShotsButton
            // 
            this.ScreenShotsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ScreenShotsButton.Image = ((System.Drawing.Image)(resources.GetObject("ScreenShotsButton.Image")));
            this.ScreenShotsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ScreenShotsButton.Name = "ScreenShotsButton";
            this.ScreenShotsButton.Size = new System.Drawing.Size(23, 22);
            this.ScreenShotsButton.Text = "\"\"";
            this.ScreenShotsButton.ToolTipText = "Screen shots this graphic";
            this.ScreenShotsButton.Click += new System.EventHandler(this.ScreenShotsButton_Click);
            // 
            // ZGraphControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "ZGraphControl";
            this.Size = new System.Drawing.Size(727, 531);
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl zedGraph;
    private System.Windows.Forms.ToolStripContainer toolStripContainer1;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripSplitButton _rightSeriesButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton _zoomInButton;
    private System.Windows.Forms.ToolStripButton _zoomOutButton;
    private System.Windows.Forms.ToolStripButton _zoomFitButton;
    private System.Windows.Forms.ToolStripButton toolStripButton3;
    private System.Windows.Forms.ToolStripButton _dragButton;
    private System.Windows.Forms.ToolStripButton toolStripButton6;
    private System.Windows.Forms.ToolStripButton _balloonsOffButton;
    private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    private System.Windows.Forms.ToolStripMenuItem _zoomStripItem;
    private System.Windows.Forms.ToolStripMenuItem _dragStripItem;
    private System.Windows.Forms.ToolStripMenuItem _zoomFitStripItem;
    private System.Windows.Forms.ToolStripButton _prevZoomButton;
    private System.Windows.Forms.ToolStripSplitButton _leftSeriesButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripButton printButton;
    private System.Windows.Forms.ToolStripMenuItem _prevZoomStripItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem _printStripItem;
    private System.Windows.Forms.ToolStripMenuItem _saveStripItem;
    private System.Windows.Forms.ToolStripButton _zoomLineButton;
    private System.Windows.Forms.ToolStripButton _autosizeButton;
    private System.Windows.Forms.ToolStripButton _differentButton;
    private System.Windows.Forms.ToolStripButton btRun;
    private System.Windows.Forms.ToolStripButton ScreenShotsButton;
  }
}
