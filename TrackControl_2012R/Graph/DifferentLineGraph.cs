﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.Reports.Graph;
using Graph.Properties;
using ZedGraph;

namespace Graph
{
    public partial class DifferentLineGraph : Form
    {
        public delegate void FormButtonClearEvent();

        private DateTime point1X = new DateTime();
        private DateTime point2X = new DateTime();
        private double point1Y = 0;
        private double point2Y = 0;
        public event FormButtonClearEvent clearEvent; // = new delegate{};

        public delegate void showDefaultGraphHandler();
        public showDefaultGraphHandler showDefaultGraph;

        public DifferentLineGraph()
        {
            InitializeComponent();

            this.btnClose.Text = Resources.ZGraphDiffBtnCloseText;
            this.btnClearSelect.Text = Resources.ZGraphDiffBtnClearSelectText;
            this.point1_dateTime.Properties.Caption = Resources.ZGraphDiffPointDateTimeName;
            this.categoryPoint1.Properties.Caption = Resources.ZGraphDiffCategoryPoint1Name;
            this.categoryPoint2.Properties.Caption = Resources.ZGraphDiffCategoryPoint2Name;
            this.categoryDiff.Properties.Caption = Resources.ZGraphDiffCategoryDiffName;
            this.point1_value.Properties.Caption = Resources.ZGraphDiffPointValue;
            this.point1_sensorName.Properties.Caption = Resources.ZGraphDiffPointSensorName;
            this.point2_sensorName.Properties.Caption = Resources.ZGraphDiffPointSensorName;
            this.point2_dateTime.Properties.Caption = Resources.ZGraphDiffPointDateTimeName;
            this.point2_value.Properties.Caption = Resources.ZGraphDiffPointValue;
            this.pointDiff_dateTime.Properties.Caption = Resources.ZGraphDiffPointDateTimeName;
            this.pointDiff_value.Properties.Caption = Resources.ZGraphDiffPointValue;
            this.Text = Resources.ZGraphDiffFormName;
        }

        public void ZGraphControl_Action(object sender, ActionCancelEventArgs ev)
        {
            //System.Diagnostics.Trace.WriteLine(ev.Point.time.ToString());
            //System.Diagnostics.Trace.WriteLine(ev.Point.value.ToString());

            if (point1_dateTime.Properties.Value == null)
            {
                point1X = ev.Point.time;
                point1Y = ev.Point.value;
                point1_dateTime.Properties.Value = point1X.ToString();
                point1_value.Properties.Value = point1Y.ToString();
                point1_sensorName.Properties.Value = ev.Point.Label;
            }
            else
            {
                if (point2_dateTime.Properties.Value != null)
                {
                    point1_dateTime.Properties.Value = point2_dateTime.Properties.Value;
                    point1_value.Properties.Value = point2_value.Properties.Value;
                    point1_sensorName.Properties.Value = point2_sensorName.Properties.Value;

                    point1X = point2X;
                    point1Y = point2Y;
                }

                point2X = ev.Point.time;
                point2Y = ev.Point.value;
                point2_dateTime.Properties.Value = point2X.ToString();
                point2_value.Properties.Value = point2Y.ToString();
                point2_sensorName.Properties.Value = ev.Point.Label;

                TimeSpan diffTime = (point2X - point1X);
                pointDiff_dateTime.Properties.Value = diffTime.ToString();
                pointDiff_value.Properties.Value = Math.Round((point2Y - point1Y), 2).ToString();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            showDefaultGraph();

            Close();
        }

        private void simpleButtonClearSelect_Click(object sender, EventArgs e)
        {
            point1X = new DateTime();
            point2X = new DateTime();
            point1Y = 0;
            point2Y = 0;

            point1_dateTime.Properties.Value = null;
            point1_value.Properties.Value = null;
            point1_sensorName.Properties.Value = null;
            point2_dateTime.Properties.Value = null;
            point2_value.Properties.Value = null;
            point2_sensorName.Properties.Value = null;
            pointDiff_dateTime.Properties.Value = null;
            pointDiff_value.Properties.Value = null;

            clearEvent();
        }
    }
}

