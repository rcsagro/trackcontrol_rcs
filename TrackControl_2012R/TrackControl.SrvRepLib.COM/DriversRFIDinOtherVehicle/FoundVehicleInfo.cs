﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.SrvRepLib.COM
{
    public class FoundVehicleInfo
    {
        public string MobitelId { get; set; }

        public string DriverId { get; set; }

        public string DriverName { get; set; }

        public string DriverVehicleRfid { get; set; }

        public string DriverVehicleId { get; set; }

        public string DriverVehicleName { get; set; }

        public string RfidBeginTime { get; set; }

        public string RfidEndTime { get; set; }

        public string FieldId { get; set; }

        public string FieldName { get; set; }

        public string VehicleName { get; set; }


    }
}
