﻿using System;
using System.Collections.Generic;
using Agro.Dictionaries;
using BaseReports.Procedure;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Notification;
using TrackControl.Vehicles;

namespace TrackControl.SrvRepLib.COM
{
    public class SearchVehicles
    {
        DateTime _begin;
        DateTime _end;
        int _rfid;
        //List<IEntity> _vehicles;
        GpsDataProvider _gps_provider;

        public SearchVehicles( int rfid,DateTime begin, DateTime end)
        {
            _begin = begin;
            _end = end;
            _rfid = rfid;
            _gps_provider = new GpsDataProvider(); 
        }

        List<Vehicle> GetVehiclesWithRfid()
        {
            List<Vehicle> vehicles = NoticeProvider.GetMobitelsWithRfidEvent(_begin, _end, _rfid);

            return vehicles;
        }


        List<DriverRFIDRecord> GetReportRfidRecords(Vehicle vehicle)
        {
            using (DriverRFIDRecordsCreator creator = new DriverRFIDRecordsCreator())
            {
                if (Algorithm.DicRfidRecords.ContainsKey(vehicle.Mobitel.Id))
                {
                    Algorithm.DicRfidRecords[vehicle.Mobitel.Id].Clear(); 
                }
                else
                {
                    Algorithm.DicRfidRecords.Add(vehicle.Mobitel.Id, new List<DriverRFIDRecord>());
                }
                creator.IdMobitel = vehicle.Mobitel.Id;
                creator.SetDefaultObservers(); 
                List<GpsData> gpsData = new List<GpsData>(_gps_provider.GetValidDataForPeriod(vehicle.Mobitel.Id, _begin, _end));
                if (gpsData.Count != 0)
                {
                    creator.CuttingDataGpsByRfid(gpsData.ToArray());
                }
            }
            return Algorithm.DicRfidRecords[vehicle.Mobitel.Id];
        }

        public List<FoundVehicleInfo> GetFoundVehicles()
        {
            List<Vehicle> vehicles = GetVehiclesWithRfid();
            List<FoundVehicleInfo> outRecords = new List<FoundVehicleInfo>();
            if (vehicles.Count == 0) return outRecords;
            List<DriverRFIDRecord> inpRecords = null;
            if (vehicles != null)
            {
                inpRecords = new List<DriverRFIDRecord>();
                foreach (Vehicle veh in vehicles)
                {
                    inpRecords.AddRange(GetReportRfidRecords(veh));
                }
                if (inpRecords.Count == 0) return outRecords;
                FindRfidAndPrevRfid(outRecords, inpRecords);
            }
            return outRecords;
        }

        private void FindRfidAndPrevRfid(List<FoundVehicleInfo> outRecords, List<DriverRFIDRecord> inpRecords)
        {
            UInt16? rfidPrev = 0;
            NoticeSetItem nsi = new NoticeSetItem();
            int driverType = nsi.ComDriverType; 
            foreach (DriverRFIDRecord inpRecord in inpRecords)
            {
                if (_rfid == inpRecord.IdentRfid)
                {

                    AddRecordToOutList(outRecords, inpRecord, rfidPrev);
                }
                Driver driver = new Driver(0, inpRecord.IdentRfid);
                if (driver.Id > 0 && driver.TypeDriverId == driverType) rfidPrev = inpRecord.IdentRfid;
            }
        }

        private void AddRecordToOutList(ICollection<FoundVehicleInfo> outRecords, DriverRFIDRecord inpRecord, UInt16? rfidPrev)
        {
            FoundVehicleInfo outRecord = new FoundVehicleInfo();
            outRecord.DriverId = inpRecord.Driver.Id.ToString();
            outRecord.DriverName = inpRecord.Driver.LastName;
            outRecord.RfidBeginTime = inpRecord.TimeStart.ToString();
            outRecord.RfidEndTime = inpRecord.TimeEnd.ToString();
            int idMobitel = inpRecord.IdMobitel;
            outRecord.MobitelId = idMobitel.ToString();
            Vehicle veh = new Vehicle( idMobitel);
            outRecord.VehicleName = veh.Info;
            int idZone = NoticeProvider.GetZoneIdFromTimeControlEvent(_begin, _end, inpRecord.IdMobitel);
            if (idZone != ConstsGen.RECORD_MISSING)
            {
                DictionaryAgroField daf = new DictionaryAgroField();
                int idField = daf.GetIdFromZoneId(idZone);
                outRecord.FieldId = idField.ToString();
                daf.GetProperties(idField);
                outRecord.FieldName = daf.Name;

            }
            if (rfidPrev > 0)
            {
                outRecord.DriverVehicleRfid = rfidPrev.ToString();
                Driver driver = new Driver(0, rfidPrev);
                if (driver.Id > 0)
                {
                    outRecord.DriverVehicleId = driver.Id.ToString();
                    outRecord.DriverVehicleName = driver.DriverFullName;
                }
                else
                {
                    outRecord.DriverVehicleName = rfidPrev.ToString();

                }
            }
            outRecords.Add(outRecord);
        }






    }
}
