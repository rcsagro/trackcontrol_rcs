﻿using System;
using System.Collections.Generic;
using Agro.Dictionaries;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;

namespace TrackControl.SrvRepLib.COM
{
    public class FieldsParameters
    {
        string _fieldEntryTime ;

        public string FieldEntryTime
        {
            get { return _fieldEntryTime; }
            set { _fieldEntryTime = value; }
        }
        string _fieldExitTime = null;

        public string FieldExitTime
        {
            get { return _fieldExitTime; }
            set { _fieldExitTime = value; }
        }

        DateTime _begin;
        DateTime _end;
        int _mobitelId;
        int _fieldId;

        GpsDataProvider _gps_provider;

        public FieldsParameters()
        {
            _gps_provider = new GpsDataProvider(); 
        }

        public void SetParametersForTimesCrossingField(int MobitelId, int FieldId, DateTime beginTime, DateTime endTime)
        {
            _mobitelId = MobitelId;
            _fieldId = FieldId;
            _begin = beginTime;
            _end = endTime;
        }



        private IZone GetZone()
        {
            using (DictionaryAgroField daf = new DictionaryAgroField(_fieldId))
            {
                ZonesProvider zonesProvider = new ZonesProvider();
                return zonesProvider.GetZone(daf.IdZone);
            }}

        public void SetCrossingTimes()
        {
            List<GpsData> gps_data = new List<GpsData>(_gps_provider.GetValidDataForPeriod(_mobitelId, _begin, _end));
            if (gps_data.Count != 0)
            {
                IZone zone = GetZone();
                bool zoneVisited = false;
                bool inZone = false;
                foreach (GpsData gps in gps_data)
                {
                    if (zone.Contains(gps.LatLng))
                    {
                        inZone = true;
                        if (!zoneVisited)
                        {
                            _fieldEntryTime = gps.Time.ToString() ;
                            zoneVisited = true;
                        }
                    }
                    else
                    {
                        if (inZone)
                        {
                            _fieldExitTime = gps.Time.ToString();
                        }
                        inZone = false;
                    }

                }
            }
        }

    }
}
