using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;
using TrackControl.Zones;


namespace TrackControl.SrvRepLib.COM
{
    public struct dataSetMarker
    {
        public int MobitelId;
        public DateTime DateBegin;
        public DateTime DateEnd;
        public int Records;
    }
    [ComVisible(true),
    GuidAttribute("E674E8F8-3765-4eca-929B-B70E06FAE4AB")]
    [ProgId("TrackControl.SrvRepLib.COM.ReporterCOM")]
    public class ReporterCOM : IReporterCOM
    {
        Reporter _reporter;
        Boolean _isInit;
        atlantaDataSet _dsAtlanta;
        int _idMobitel;
        dataSetMarker _marker;
        atlantaDataSet.mobitelsRow _mRow;
        const string MessageInit = "��������� ������������� ������� ������� Init!";
        List<Zone> _zones = new List<Zone>();
        public ReporterCOM()
        {

        }

        #region IReporterCOM Members
        public void Init(string conString)
        {
            _reporter = new Reporter(conString);
            //ConnectMySQL.ConnectionString = conString;
            //DriverDb.ConnectionString = conString;
            _isInit = true;

        }

        public Single GetKilometrage(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            if (IsInit())
            {
                //MessageBox.Show(
                //    string.Format("tt {0} start {1} end {2}", teletrackId, beginTime.ToString(), endTime.ToString()),
                //    "GetKilometrage");
                return (Single) _reporter.GetKilometrage(teletrackId, beginTime, endTime);
            }
            else
            {
                return 0;
            }
                
        }

        public string GetTotalMotionTime(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            if (IsInit())
                return _reporter.GetTotalMotionTime(teletrackId, beginTime, endTime).ToString() ;
            else
                return "";
        }

        public string GetTotalStopsTime(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            if (IsInit())
                return _reporter.GetTotalStopsTime(teletrackId, beginTime, endTime).ToString();
            else
                return "";
    }
        #endregion


        public void ShowMessage(string myMessage)
        {
            MessageBox.Show(myMessage, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public string[,] GetFuelArray(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            if (IsInit())
            {
                Create_dsAtlanta(teletrackId, beginTime, endTime); 
                if (_marker.Records   == 0) return null;
                 string[,] arrayOut = new string[_dsAtlanta.dataview.Rows.Count, 2];
                FuelDictionarys fuelDict = new FuelDictionarys();
                Fuel fuel = new Fuel();
                 fuel.SelectItem(_mRow);
                fuel.GettingValuesDUT(fuelDict);
                int i = 0;
                foreach (GpsData d_row in Algorithm.GpsDatas)
                {
                    arrayOut[i, 0] = d_row.Time.ToString();
                    arrayOut[i, 1] = fuelDict.ValueFuelSum[d_row.Id].value.ToString() ;
                    i++;
                } 
                return arrayOut;
            }
            return null;
        }

        public double[] GetFuelLevel(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            if (IsInit())
            {
                Create_dsAtlanta(teletrackId, beginTime, endTime);
                if (_marker.Records == 0) return null;
                double[] values = new double[4];
                PartialAlgorithms pa = new PartialAlgorithms(_mRow);
                Algorithm.GpsDatas = _reporter.GetGpsData(teletrackId, beginTime, endTime);
                pa.TotalExpenseDUT(Algorithm.GpsDatas, out values[0], out values[1], out values[2], out values[3]);
                return values;
            }
            else
                return null;
        }

        private bool IsInit()
        {
            if (_isInit)
                return true;
            else
            {
                MessageBox.Show("��������� ������������� ������� ������� Init!","Error");
                return false;
            }
        }

        private void Create_dsAtlanta(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            //_idMobitel = MobitelsProvider.GetMobitelIdFromNameTT(teletrackId);
            _idMobitel = _reporter.GetMobitelsRowByTtID(teletrackId).Mobitel_ID;
            if (MarkerValid(beginTime, endTime, _idMobitel)) return;
            _dsAtlanta = new atlantaDataSet();
            LocalCacheItem lci = new LocalCacheItem(beginTime, endTime, _idMobitel);
            _marker.Records = lci.CreateDsAtlanta(ref _dsAtlanta);
            _marker.DateBegin = beginTime;
            _marker.DateEnd = endTime;
            _marker.MobitelId = _idMobitel;
            _mRow = (atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_idMobitel);
            Algorithm.RunFromModule = true;
            Algorithm.AtlantaDataSet = _dsAtlanta;
        }

        /// <summary>
        /// ��� ���������� ������� �� ������������ ������
        /// </summary>
        /// <param name="dateBegin"></param>
        /// <param name="dateEnd"></param>
        /// <param name="mobitelId"></param>
        /// <returns></returns>
        private bool MarkerValid(DateTime dateBegin, DateTime dateEnd, int mobitelId)
        {
            if (_marker.DateBegin == dateBegin & _marker.DateEnd == dateEnd & _marker.MobitelId == mobitelId & _marker.Records > 0)
                return true;
            else
                return false;
        }



        public string[,] GetHarvestSource(int rfid, DateTime beginTime, DateTime endTime, int N)
        {
             
            SearchVehicles sVeh = new SearchVehicles(rfid, beginTime, endTime);
             List<FoundVehicleInfo> vehInfos = sVeh.GetFoundVehicles();
             string[,] arrayOut = new string[Math.Min(N,vehInfos.Count), 10];
             if (vehInfos.Count == 0) return arrayOut;
             int i = 0;
             foreach (FoundVehicleInfo vehInfo in vehInfos)
             {
                 arrayOut[i, 0] = vehInfo.MobitelId;
                 arrayOut[i, 1] = vehInfo.DriverId;
                 arrayOut[i, 2] = vehInfo.DriverVehicleId;
                 arrayOut[i, 3] = vehInfo.RfidBeginTime;
                 arrayOut[i, 4] = vehInfo.RfidEndTime;
                 arrayOut[i, 5] = vehInfo.FieldId;
                 arrayOut[i, 6] = vehInfo.VehicleName;
                 arrayOut[i, 7] = string.Format("{0} (rfid: {1})"
                , (vehInfo.DriverName != null) ? vehInfo.DriverName : "No driver"
                , rfid.ToString());
                 arrayOut[i, 8] = string.Format("{0} (rfid: {1})"
                 , (vehInfo.DriverVehicleId != null) ? vehInfo.DriverVehicleName : "No driver"
                 , (vehInfo.DriverVehicleRfid != null) ? vehInfo.DriverVehicleRfid.ToString() : "No rfid");
                 arrayOut[i, 9] = vehInfo.FieldName;
                 i++;
                 if (arrayOut.GetUpperBound(0)+1 == i) break;
             }
             return arrayOut;
        }


        public string[,] GetCrossingFieldInfo(int mobitelId, int fieldId ,DateTime beginTime, DateTime endTime)
        {
            FieldsParameters fp = new FieldsParameters();
            fp.SetParametersForTimesCrossingField(mobitelId, fieldId, beginTime, endTime);
            fp.SetCrossingTimes();
            string[,] dates = new string[1, 2];
            dates[0, 0] = fp.FieldEntryTime;
            dates[0, 1] = fp.FieldExitTime;
            return dates;
        }

        public void LoadZonez()
        {
            ZonesProvider zoneProv = new ZonesProvider();
            _zones = zoneProv.GetAll();
            foreach (var zone in _zones)
            {
                zoneProv.GetPoints(zone);
            }
        }

        public string[,] GetCrossingZonesInfo(string teletrackId, DateTime beginTime, DateTime endTime,int minTimeInZoneMinutes = 5,int setInStartEndZonesMovesStops = 0)
        {
            List<CrossZoneRecord> records = new List<CrossZoneRecord>();
            int mobitelId = _reporter.GetMobitelsRowByTtID(teletrackId).Mobitel_ID;
            var gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(mobitelId, beginTime, endTime);
            List<CrossZonesObserver> Observers = new List<CrossZonesObserver>();
            foreach (GpsData gpsData in gpsDatas)
            {
                foreach (IZone zone in _zones)
                {
                    if (zone.BoundContains(gpsData.LatLng))
                    {
                        if (zone.Contains(gpsData.LatLng))
                        {
                            var observer = Observers.Where(ob => ob.Zone == zone).SingleOrDefault();
                            if (observer == null)
                            {
                                observer = new CrossZonesObserver(zone, minTimeInZoneMinutes);
                                Observers.Add(observer);
                            }
                            observer.SetInput(gpsData);
                        }
                    }

                }
                foreach (var crossZonesObserver in Observers)
                {
                    crossZonesObserver.CheckOutput(gpsData);
                }
            }

            if (Observers.Count > 0)
            {
                foreach (var crossZonesObserver in Observers)
                {
                    crossZonesObserver.SetLastRecord(gpsDatas.Last());
                    records.AddRange(crossZonesObserver.GetCrossZonesRecords());
                }
            }
            var sortedRecords = records.OrderBy(r => r.DateStart).ToList();
            if (setInStartEndZonesMovesStops!=0 && sortedRecords.Count()>2)
            {
                Create_dsAtlanta(teletrackId, beginTime, endTime); 
                setInStartZonesMovesStart(mobitelId,gpsDatas, sortedRecords.First());
                setInEndZonesStopEnd(mobitelId, gpsDatas, sortedRecords.Last());

            }
            string[,] stringRecords = new string[sortedRecords.Count(), 6];
            int indRec = 0;
            foreach (var record in sortedRecords)
            {
                stringRecords[indRec,0] = record.DateStart.ToString();
                stringRecords[indRec,1] = record.DateEnd.ToString();
                stringRecords[indRec,2] = record.Id.ToString();
                stringRecords[indRec,3] = record.IdOutLink;
                stringRecords[indRec,4] = record.Name;
                stringRecords[indRec,5] = record.Dist.ToString();
                indRec++;
            }
            return stringRecords;
        }

        private void setInStartZonesMovesStart(int mobitelId,IList<GpsData> gpsDatas, CrossZoneRecord record)
        {
            var rows = GetKilometrageReportRows(mobitelId, gpsDatas, record);
            foreach (var row in rows)
            {
                if (row.Distance > 0)
                {
                    record.DateStart = row.InitialTime;
                    return;
                }
            }
        }

        private void setInEndZonesStopEnd(int mobitelId, IList<GpsData> gpsDatas, CrossZoneRecord record)
        {
            var rows = GetKilometrageReportRows(mobitelId, gpsDatas, record);
            foreach (var row in rows.Reverse())
            {
                if (row.Distance == 0)
                {
                    record.DateEnd = row.InitialTime;
                    return;
                }
            }
        }

        private  atlantaDataSet.KilometrageReportRow[] GetKilometrageReportRows(int mobitelId, IList<GpsData> gpsDatas, CrossZoneRecord record)
        {
            var gpsDataReport = gpsDatas.Where(gps => gps.Time >= record.DateStart && gps.Time <= record.DateEnd).ToList();
            Kilometrage brKlm = new Kilometrage();
            brKlm.SelectItem(mobitelId, gpsDataReport);
            brKlm.ClearKilometrageReportRows();
            brKlm.Run();
            var rows = brKlm.GetKilometrageReportRows();
            return rows;
        }
    }
   }
