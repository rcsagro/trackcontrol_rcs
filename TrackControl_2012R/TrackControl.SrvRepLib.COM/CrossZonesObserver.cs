﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace TrackControl.SrvRepLib.COM
{
    public class CrossZonesObserver
    {
        public IZone Zone { get; set; }
        private GpsData _gpsDataInput;
        private GpsData _gpsDataOutput;
        private double _dist;
        private int _minTimeInZoneMinutes;
        private List<CrossZoneRecord> _records = new List<CrossZoneRecord>();

        public CrossZonesObserver(IZone zone,int minTimeInZoneMinutes)
        {
            Zone = zone;
            _minTimeInZoneMinutes = minTimeInZoneMinutes;
        }

        public void SetInput(GpsData gpsData)
        {
            if (_gpsDataInput == null)
            {
                _gpsDataInput = gpsData;
                _dist = 0;
            }
        }

        public void CheckOutput(GpsData gpsData)
        {
            if (_gpsDataInput != null)
            {
                if (gpsData.Id != _gpsDataInput.Id)
                {
                    _dist += gpsData.Dist;
                    if (!Zone.Contains(gpsData.LatLng))
                    {
                        _gpsDataOutput = gpsData;
                        AddCrossZoneRecord();
                        _gpsDataInput = null;
                        _dist = 0;
                    }

                }
            }


        }

        public void SetLastRecord(GpsData gpsData)
        {
            if (_gpsDataInput != null)
            {
                //_dist += gpsData.Dist;
                _gpsDataOutput = gpsData;
                AddCrossZoneRecord();
            }
        }


        public List<CrossZoneRecord> GetCrossZonesRecords()
        {
            return _records;
        }

        private void AddCrossZoneRecord()
        {
            if (_gpsDataOutput.Time.Subtract(_gpsDataInput.Time).TotalMinutes > _minTimeInZoneMinutes)
            {
                var record = new CrossZoneRecord();
                record.DateStart = _gpsDataInput.Time;
                record.DateEnd = _gpsDataOutput.Time;
                record.Id = Zone.Id;
                record.IdOutLink = Zone.IdOutLink;
                record.Name = Zone.Name;
                record.Dist = Math.Round(_dist, 2);
                _records.Add(record);
            }

        }
    }

    public class CrossZoneRecord
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int Id { get; set; }
        public string IdOutLink { get; set; }
        public string Name { get; set; }
        public double Dist { get; set; }
    }
}
