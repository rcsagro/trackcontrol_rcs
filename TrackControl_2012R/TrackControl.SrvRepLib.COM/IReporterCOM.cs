using System;
using System.Runtime.InteropServices; 
using TrackControl.SrvRepLib.Exceptions;

namespace TrackControl.SrvRepLib
{
    [ComVisible(true), GuidAttribute("748985D3-9B62-4e64-A8C4-74AC50E21F29")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    /// <summary>
    /// ��������� �������������� � COM - �������� 
    /// </summary>
    public interface IReporterCOM
    {
        /// <summary>
        /// ������������� ������� - ����������� � ����
        /// </summary>
        /// <param name="conString"></param>
        void Init(string conString);
        /// <summary>
        /// ���������� ������� � ���������� ������������� ��������, 
        /// �� ������� ���������� ��������, � �������� ��������� ���������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>������ ������������� ��������, � ����������.</returns>
        Single GetKilometrage(String teletrackId, DateTime beginTime, DateTime endTime);

        /// <summary>
        /// ����������� ������ ������� � ��������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>����� ����� � ��������.</returns>
        string GetTotalMotionTime(String teletrackId, DateTime beginTime, DateTime endTime);

        /// <summary>
        /// ����������� ������ ������� ������� � ���������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>����� ����� ������� � ���������.</returns>
        string  GetTotalStopsTime(String teletrackId, DateTime beginTime, DateTime endTime);
        /// <summary>
        /// ������� �� ������ � ����� ����������,�������� � ����� �� ����������
        /// </summary>
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>���������� ������ 0 - ������� �� ������ 
        ///                            1 - ������� �� �����
        ///                            2 - ����������
        ///                            3 - ����� 
        /// </returns>
        double[] GetFuelLevel(string teletrackId, DateTime beginTime, DateTime endTime);
        /// <summary>
        /// ���������� ��������� ������ ��������� ��� ����� - ������� �������  
        /// </summary>
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>��������� ������ ��������� ��� ����� - ������� �������</returns>
        string[,] GetFuelArray(string teletrackId, DateTime beginTime, DateTime endTime);
        /// <summary>
        /// ���������� ������ �� N ���������. ������ ������� ������������� ������� �������� ������ ��������� 
        /// </summary>
        /// <param name="rfId"> RFID ��������</param>
        /// <param name="beginTime">��������� ����� �������</param>
        /// <param name="endTime">�������� ����� �������</param>
        /// <param name="n">N ��������� ������� ��� N ��������� ������� ��������, ��������������� � ������ ����� beginTime � endTime � ������������� �� �������� beginTime. ���� �� ���� ������ ������������� ����� N ������� ��������, �� ��������������� �������� ������� ����������� �������� ����������. </param>
        /// <returns> 
        /// int MobitelId � id ������������� �������� (��������), �� ������� ���� ������������� ����������� �������� � ������� RFId;
        ///int DriverId � id �������� ��������,� �������� ������������� ��������;
        ///DateTime RfidBeginTime � ����� ������� �������� ��������-���������������; 
        ///DateTime RfidEndTime � ����� ���������� �������� ��������-���������������;
        ///int FieldId � id ����, � �������� ������������� ��������
        ///</returns>
        string[,] GetHarvestSource(int rfId, DateTime beginTime, DateTime endTime, int n);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobitelId">�������� ��� ���������</param>
        /// <param name="fieldId">��� ����</param>
        /// <param name="beginTime">��������� ����� �������</param>
        /// <param name="endTime">�������� ����� �������</param>
        /// <returns>���������� ��������� ������ �� ���������� �������������:
        ///DateTime FieldInTime - ����� ������� ������ � ������ ���� � �������� �������;
        ///DateTime FieldOutTime - ����� ���������� ������ � ������ ���� � �������� �������
        ///</returns>
        string[,] GetCrossingFieldInfo(int mobitelId, int fieldId, DateTime beginTime, DateTime endTime);

        /// <summary>
        /// ����������� ����������� ����������� ��� (��) �� ����������
        /// </summary>
        /// <param name="teletrackId">����������������� ������������� ���������</param>
        /// <param name="beginTime">��������� ����� �������</param>
        /// <param name="endTime">�������� ����� �������</param>
        /// <param name="minTimeInZoneMinutes">����������� ����� ���������� � ����������� ����(������)</param>
        /// <param name="setInStartEndZonesMovesStops">����  =1, �� � ������ �� ������� ��������� �� ����������� � ������ ��������, � � ��������� ����� ���������</param>
        /// <returns>���������� ������: ����� �������� � ��, ����� ������ �� ��, �������������� ��</returns>
        string[,] GetCrossingZonesInfo(string teletrackId, DateTime beginTime, DateTime endTime, int minTimeInZoneMinutes = 5,int setInStartEndZonesMovesStops = 0);
    }
}