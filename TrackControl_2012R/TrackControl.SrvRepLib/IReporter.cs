using System;
using TrackControl.SrvRepLib.Exceptions;

namespace TrackControl.SrvRepLib
{
  /// <summary>
  /// ��������� �������������� � ��������, 
  /// ��������������� ������, ����������� ��� ����������� ����������.
  /// </summary>
  public interface IReporter
  {
    /// <summary>
    /// ���������� ������� � ���������� ������������� ��������, 
    /// �� ������� ���������� ��������, � �������� ��������� ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
    /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
    /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
    /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
    /// <param name="teletrackId">����������������� ������������� ���������.</param>
    /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
    /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
    /// <returns>������ ������������� ��������, � ����������.</returns>
    Single GetKilometrage(String teletrackId, DateTime beginTime, DateTime endTime);

    /// <summary>
    /// ����������� ������ ������� � ��������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
    /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
    /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
    /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
    /// <param name="teletrackId">����������������� ������������� ���������.</param>
    /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
    /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
    /// <returns>����� ����� � ��������, �������� �������(TimeSpan).</returns>
    TimeSpan GetTotalMotionTime(String teletrackId, DateTime beginTime, DateTime endTime);

    /// <summary>
    /// ����������� ������ ������� ������� � ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
    /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
    /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
    /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
    /// <param name="teletrackId">����������������� ������������� ���������.</param>
    /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
    /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
    /// <returns>����� ����� ������� � ���������, �������� �������(TimeSpan).</returns>
    TimeSpan GetTotalStopsTime(String teletrackId, DateTime beginTime, DateTime endTime);
  }
}
