using System;

namespace TrackControl.SrvRepLib.Exceptions
{
  /// <summary>
  /// ���������� "�� ������ ��������".
  /// </summary>
  public class TeletrackNotFoundException : Exception
  {
    private const String MSG =
      "������������� ��������� {0} �� ������ � �� (������ �����������: {1})";

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="teletrackId">������������� ���������.</param>
    /// <param name="connectionString">������ ����������� � ��.</param>
    public TeletrackNotFoundException(String teletrackId, String connectionString)
      : base(String.Format(MSG, teletrackId, connectionString))
    {
    }
  }
}
