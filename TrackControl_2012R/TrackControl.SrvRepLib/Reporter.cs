using System;
using System.Collections.Generic;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.SrvRepLib.Exceptions;
using TrackControl.Statistics;
using TrackControl.Vehicles;
using TrackControl.Vehicles.DAL;


namespace TrackControl.SrvRepLib
{
    /// <summary>
    /// ������������� ������, ����������� ��� ����������� ����������.
    /// </summary>
    /// <remarks>
    /// <example><code>
    /// String teletrackId = "A001";
    /// DateTime beginTime = new DateTime(2010, 01, 20, 00, 00, 00 );  
    /// DateTime endTime = new DateTime(2010, 01, 23, 23, 59, 59 );  
    ///
    /// IReporter reporter = new Reporter("server=localhost; database=mca_dispatcher; user id=user");
    /// // ����������
    /// Single kilometrage = reporter.GetKilometrage(teletrackId, beginTime, endTime);
    /// // O���� ����� � ��������
    /// TimeSpan totalMotionTime = GetTotalMotionTime(teletrackId, beginTime, endTime);
    /// // O���� ����� ������� � ���������
    /// TimeSpan totalStopsTime = GetTotalStopsTime(teletrackId, beginTime, endTime);
    /// </code></example>
    /// </remarks>
    public class Reporter : IReporter
    {
        private readonly string _connectionString;
        protected atlantaDataSet _dataset;
        private IGpsDataProvider _gpsDataProvider;

        private Reporter()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����������� �� �������� ������ ����������� � ��.</exception>
        /// <param name="connectionString">������ ����������� � ��.</param>
        public Reporter(String connectionString)
        {
            if (String.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException("connectionString",
                    "� ����������� �� �������� ������ ����������� � ��.");
            }
            _connectionString = connectionString;
            Init();
        }

        private void Init()
        {
            //_gpsDataProvider = new GpsDataProvider(new DbCommon(_connectionString));
            _gpsDataProvider = new GpsDataProvider(new DriverDb(_connectionString, true));
            _dataset = new atlantaDataSet();
            new MobitelsProvider(_connectionString).FillMobitelsTable(_dataset.mobitels);
        }

        public atlantaDataSet.mobitelsRow GetMobitelsRowByTtID(String teletrackId)
        {
            if (String.IsNullOrEmpty(teletrackId))
            {
                throw new ArgumentNullException(
                    "teletrackId", "�� ������� ������������� ���������.");
            }
            if (teletrackId.Length != 4)
            {
                throw new ArgumentException(
                    "������������� ��������� ������ �������� �� 4-�� ��������.", "teletrackId");
            }

            foreach (atlantaDataSet.mobitelsRow row in _dataset.mobitels)
            {
                if (row.NumTT == teletrackId)
                {
                    return row;
                }
            }

            throw new TeletrackNotFoundException(teletrackId, _connectionString);
        }

        private Vehicle CreateVehicle(String teletrackId)
        {
            string cmnts = VehicleCommentProvider.GetComment( GetMobitelsRowByTtID( teletrackId ).Mobitel_ID );

            atlantaDataSet.mobitelsRow row = GetMobitelsRowByTtID(teletrackId);
            var mobitel = new Mobitel(row.Mobitel_ID, "", "");
            mobitel.Is64BitPackets = row.Is64bitPackets;
            var vhcl = new Vehicle(mobitel, "", "", "", 0.0, 0.0, 0, cmnts);
            return vhcl;
        }

        private static GpsData[] IListToArray(IList<GpsData> data)
        {
            GpsData[] result = new GpsData[data.Count];
            data.CopyTo(result, 0);
            return result;
        }

        public GpsData[] GetGpsData(String teletrackId, DateTime beginTime, DateTime endTime)
        {
            return IListToArray(DataGpsService.GetValidDataGps(
                CreateVehicle(teletrackId), beginTime, endTime, _gpsDataProvider));
        }

        #region IReporter Members

        /// <summary>
        /// ���������� ������� � ���������� ������������� ��������, 
        /// �� ������� ���������� ��������, � �������� ��������� ���������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>������ ������������� ��������, � ����������.</returns>
        public Single GetKilometrage(String teletrackId, DateTime beginTime, DateTime endTime)
        {
            return (Single) BasicMethods.GetKilometrage(
                GetGpsData(teletrackId, beginTime, endTime));
        }

        /// <summary>
        /// ����������� ������ ������� � ��������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception> 
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>����� ����� � ��������, �������� �������(TimeSpan).</returns>
        public TimeSpan GetTotalMotionTime(String teletrackId, DateTime beginTime, DateTime endTime)
        {
            return BasicMethods.GetTotalMotionTime(
                GetGpsData(teletrackId, beginTime, endTime));
        }

        /// <summary>
        /// ����������� ������ ������� ������� � ���������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� ������� ������������� ���������.</exception>
        /// <exception cref="ArgumentException">������������� ��������� ������ �������� �� 4-�� ��������.</exception>
        /// <exception cref="ArgumentException">��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception> 
        /// <exception cref="TeletrackNotFoundException">������������� ��������� �� ������ � ��.</exception> 
        /// <param name="teletrackId">����������������� ������������� ���������.</param>
        /// <param name="beginTime">��������� ����� ������� (���������� � ������).</param>
        /// <param name="endTime">�������� ����� ������� (���������� � ������).</param>
        /// <returns>����� ����� ������� � ���������, �������� �������(TimeSpan).</returns>
        public TimeSpan GetTotalStopsTime(String teletrackId, DateTime beginTime, DateTime endTime)
        {
            return BasicMethods.GetTotalStopsTime(
                GetGpsData(teletrackId, beginTime, endTime), 0);
        }

        #endregion

        public string[] GetFuelInfor(string teletrackId, DateTime beginTime, DateTime endTime)
        {
            string[] pars = {"0", "0", "0", "0"};
            //Fuel_Dictionarys fuelDict = new FuelDictionarys();
            //Fuel fuel = new Fuel();

            //fuel.SelectItem(mob_row);
            //fuel.GettingValuesDUT(fuelDict);
            //PartialAlgorithms PA = new PartialAlgorithms(m_row); 
            //_fuelExpense = PA.TotalExpenseDUT(_dataGPSs.ToArray(), out _fuelStart, out _fuelEnd, out _fuelAdd, out _fuelSub);
            return pars;
            //if (IsInit)
            //    return reporter.GetTotalStopsTime(teletrackId, beginTime, endTime).ToString();
            //else
            //    throw new ApplicationException(MessageInit);
        }
    }
}
