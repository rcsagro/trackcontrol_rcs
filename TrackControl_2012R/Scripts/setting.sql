-- ���� �������: 11.09.2009

--
-- ��� ���, � ���� � ������� �������� (setting) �����������
-- ������� AvgFuelRatePerHour, FuelApproximationTime, TimeGetFuelAfterStop, TimeGetFuelBeforeMotion 
--
DELIMITER $$
DROP PROCEDURE IF EXISTS rcs__add_some_new__Columns_to__SettingTbl$$
CREATE PROCEDURE rcs__add_some_new__Columns_to__SettingTbl()
BEGIN
	IF EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "setting"
	)
	THEN
	
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND TABLE_NAME = "setting"
				AND	COLUMN_NAME = "AvgFuelRatePerHour"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `AvgFuelRatePerHour` DOUBLE (10, 5) NOT NULL DEFAULT 30.00000 COMMENT '������� ������ ������� � ���';
		END IF;

		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "FuelApproximationTime"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `FuelApproximationTime` TIME NOT NULL DEFAULT '00:10:00' COMMENT '��������� �������� ����� ��������, �� ����� �������� ���������� ������� ������� � ������ �������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "TimeGetFuelAfterStop"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `TimeGetFuelAfterStop` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������� � ������� ������ �������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "TimeGetFuelBeforeMotion"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `TimeGetFuelBeforeMotion` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������ ������� � ������� ��������';
		END IF;
		
	END IF;
END$$
CALL rcs__add_some_new__Columns_to__SettingTbl()$$
DROP PROCEDURE IF EXISTS rcs__add_some_new__Columns_to__SettingTbl$$
DELIMITER ;