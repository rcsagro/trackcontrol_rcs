-- ���� �������: 22.01.2009 10:38:52

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;


DROP TABLE IF EXISTS ver;
CREATE TABLE ver(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Num INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '����� ������� ������ ��',
  `Desc` TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL COMMENT '�������� ������� ������ ��',
  `time` DATETIME DEFAULT NULL COMMENT '����� ����������',
  PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

-- 
-- ����� ������ ��� ������� ver
-- 
INSERT INTO ver VALUES (1, 1, '������ �� 9.12.2008', '2008-12-09 00:00:00');
INSERT INTO ver VALUES (2, 2, '������ �� 10.02.2009', '2009-02-10 00:00:00');

--
-- ������� ������� driver
--
DROP TABLE IF EXISTS driver;
CREATE TABLE driver(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Family CHAR (40) DEFAULT NULL COMMENT '�������',
  Name CHAR (40) DEFAULT NULL COMMENT '���',
  ByrdDay DATE DEFAULT NULL COMMENT '���� ��������',
  Category CHAR (5) DEFAULT NULL COMMENT '���������',
  Permis CHAR (20) DEFAULT NULL COMMENT '�����',
  foto BLOB DEFAULT NULL COMMENT '����',
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

INSERT INTO driver VALUES (1, '', '', '2009-01-01', '�', '1111', NULL);
INSERT INTO driver VALUES (2, '��������', '', '2009-01-22', '�', '1111', NULL);


--
-- ������� ������� setting
--
DROP TABLE IF EXISTS setting;
CREATE TABLE setting(
  id INT (11) NOT NULL AUTO_INCREMENT,
  TimeBreak TIME DEFAULT '00:01:00' COMMENT '����������� ����� ���������',
  RotationMain INT (11) DEFAULT 100 COMMENT '������� ��������� ���������',
  RotationAdd INT (11) DEFAULT 100 COMMENT '������� ���. ���������',
  FuelingEdge INT (11) DEFAULT 0 COMMENT '����������� ��������',
  band INT (11) DEFAULT 25 COMMENT '������ ����������',
  FuelingDischarge INT (11) DEFAULT 0 COMMENT '����������� ���� �������',
  accelMax DOUBLE DEFAULT 1 COMMENT '������������ ���������',
  breakMax DOUBLE DEFAULT 1 COMMENT '������������ ����������',
  speedMax DOUBLE DEFAULT 70 COMMENT '������������ ��������',
  idleRunningMain INT (11) DEFAULT 0 COMMENT '�������� ��� ��������� ���������',
  idleRunningAdd INT (11) DEFAULT 0 COMMENT '������� ��� ���. ���������',
  Name CHAR (20) DEFAULT '���������' COMMENT '��������',
  `Desc` CHAR (80) DEFAULT '�������� ���������' COMMENT '�������� ',
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

-- 
-- ����� ������ ��� ������� setting
-- 
INSERT INTO setting VALUES (1, '00:01:00', 100, 100, 50, 25, 10, 1, 1, 70, 0, 0, '��������� 1', '��������� �� ���������');

--
-- ������� ������� team
--
DROP TABLE IF EXISTS team;
CREATE TABLE team(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Name CHAR (20) DEFAULT NULL COMMENT '�������� ������',
  Descr TEXT DEFAULT NULL COMMENT '��������',
  Setting_id INT (11) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id),
  INDEX Setting_id USING BTREE (Setting_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

-- 
-- ����� ������ ��� ������� team
-- 
INSERT INTO team VALUES (1, '����������', NULL, 1);
INSERT INTO team VALUES (2, '�������', NULL, 1);
INSERT INTO team VALUES (3, '�������', NULL, 1);
INSERT INTO team VALUES (4, '���������', NULL, 1);
INSERT INTO team VALUES (5, '����', NULL, 1);


--
-- ������� ������� vehicle
--
DROP TABLE IF EXISTS vehicle;
CREATE TABLE vehicle(
  id INT (11) NOT NULL AUTO_INCREMENT,
  MakeCar TEXT DEFAULT NULL COMMENT '����� ����������',
  NumberPlate TEXT DEFAULT NULL COMMENT '�������� ����',
  Team_id INT (11) DEFAULT NULL,
  Mobitel_id INT (11) DEFAULT NULL,
  Odometr INT (11) DEFAULT 0 COMMENT '��������� ��������',
  CarModel TEXT DEFAULT NULL COMMENT '������',
  setting_id INT (11) DEFAULT 1,
  odoTime DATETIME DEFAULT NULL COMMENT '���� � ����� ������ ��������',
  driver_id INT (11) DEFAULT -1,
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id),
  INDEX Mobitel_id USING BTREE (Mobitel_id),
  INDEX Team_id USING BTREE (Team_id),
  INDEX setting_id USING BTREE (setting_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

--
-- �������� ��� ������� sensors
--
CREATE TABLE IF NOT EXISTS sensors(
  id INT (11) NOT NULL AUTO_INCREMENT,
  mobitel_id INT (11) NOT NULL,
  Name VARCHAR (45) NOT NULL,
  Description VARCHAR (200) NOT NULL,
  StartBit INT (10) UNSIGNED NOT NULL,
  Length INT (10) UNSIGNED NOT NULL,
  NameUnit VARCHAR (45) NOT NULL,
  K DOUBLE (10, 5) NOT NULL DEFAULT 0.00000,
  PRIMARY KEY (id),
  INDEX Index_2 USING BTREE (mobitel_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

--
-- �������� ��� ������� relationalgorithms
--
CREATE TABLE IF NOT EXISTS relationalgorithms(
  ID INT (11) NOT NULL AUTO_INCREMENT,
  AlgorithmID INT (11) NOT NULL DEFAULT 0,
  SensorID INT (11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID),
  UNIQUE INDEX ID USING BTREE (ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

--
-- �������� ��� ������� sensorcoefficient
--
CREATE TABLE IF NOT EXISTS sensorcoefficient(
  id INT (10) UNSIGNED NOT NULL AUTO_INCREMENT,
  Sensor_id INT (10) UNSIGNED NOT NULL,
  UserValue DOUBLE (10, 5) NOT NULL,
  SensorValue DOUBLE (10, 5) NOT NULL,
  K DOUBLE (10, 5) NOT NULL DEFAULT 1.00000,
  b DOUBLE (10, 5) NOT NULL DEFAULT 0.00000,
  PRIMARY KEY (id),
  UNIQUE INDEX Index_2 USING BTREE (Sensor_id, UserValue)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;


--
-- ������� ������� sensoralgorithms
--
DROP TABLE IF EXISTS sensoralgorithms;
CREATE TABLE sensoralgorithms(
  ID INT (11) NOT NULL AUTO_INCREMENT,
  Name CHAR (45) NOT NULL,
  Description CHAR (45) DEFAULT NULL,
  AlgorithmID INT (11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID),
  UNIQUE INDEX ID USING BTREE (ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

-- 
-- ����� ������ ��� ������� sensoralgorithms
-- 
INSERT INTO sensoralgorithms VALUES (1, '������ ������������', '��� ������� ������������ �������� ���. ����.', 1);
INSERT INTO sensoralgorithms VALUES (2, '������ ��������� ���������', '������ ������������ ��� ����������� � ���.', 2);
INSERT INTO sensoralgorithms VALUES (3, '������� ������������', '��� ������� ������������ �-�� ��������', 3);
INSERT INTO sensoralgorithms VALUES (4, '����������', '������ �������������� ���� ����', 4);
INSERT INTO sensoralgorithms VALUES (5, '�������� ��������� ���������', '��� ������� ������������ �-�� ��������', 5);
INSERT INTO sensoralgorithms VALUES (6, '�����������', '��� ������������� ��������', 6);
INSERT INTO sensoralgorithms VALUES (7, '�������1', '������� ������� � ����1', 7);
INSERT INTO sensoralgorithms VALUES (8, '�������2', '������� ������� � ����2', 8);
INSERT INTO sensoralgorithms VALUES (9, '���������1', '��� �������� ���������� ����������1', 9);
INSERT INTO sensoralgorithms VALUES (10, '���������2', '��� �������� ���������� ����������2', 10);
INSERT INTO sensoralgorithms VALUES (11, '�����1', '��� ����������1', 11);
INSERT INTO sensoralgorithms VALUES (12, '�����2', '��� ����������2', 12);


DROP TABLE IF EXISTS `lines`;
CREATE TABLE `lines`(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Color INT (11) DEFAULT 0 COMMENT '����',
  Width INT (11) DEFAULT 3 COMMENT '������� �����',
  Mobitel_ID INT (11) DEFAULT NULL,
  Icon BLOB DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX lines_FK1 USING BTREE (Mobitel_ID),
  CONSTRAINT lines_FK1 FOREIGN KEY (Mobitel_ID) REFERENCES mobitels (Mobitel_ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251 COMMENT = '�������� �����';




DELIMITER $$

--
-- ������� ��������� CheckOdo
--
DROP PROCEDURE IF EXISTS CheckOdo$$
CREATE PROCEDURE CheckOdo(IN m_id INTEGER(11), IN end_time DATETIME)
  SQL SECURITY INVOKER
BEGIN
SELECT 
 FROM_UNIXTIME(datagps.UnixTime) AS `time` ,
 (datagps.Latitude / 600000.00000) AS Lat,
 (datagps.Longitude / 600000.00000) AS Lon
 
FROM
  datagps
WHERE
  datagps.Mobitel_ID = m_id AND 
  datagps.UnixTime BETWEEN (SELECT UNIX_TIMESTAMP(odoTime)  FROM vehicle WHERE Mobitel_id = m_id) AND 
  UNIX_TIMESTAMP(end_time) AND 
  Valid = 1  ;
END
$$

--
-- ������� ��������� dataview
--
DROP PROCEDURE IF EXISTS dataview$$
CREATE PROCEDURE dataview(IN m_id INTEGER(11), IN val BOOL, IN begin_time DATETIME, IN end_time DATETIME)
  SQL SECURITY INVOKER
BEGIN
select `datagps`.`DataGps_ID` AS `DataGps_ID`,
`datagps`.`Mobitel_ID` AS `Mobitel_ID`,
(`datagps`.`Latitude` / 600000.00000) AS `Lat`,
(`datagps`.`Longitude` / 600000.00000) AS `Lon`,
`datagps`.`Altitude` AS `Altitude`,
from_unixtime(`datagps`.`UnixTime`) AS `time`,
(`datagps`.`Speed` * 1.852) AS `speed`,
((`datagps`.`Direction` * 360) / 255) AS `direction`,
`datagps`.`Valid` AS `Valid`,
(((((((`datagps`.`Sensor8` + 
(`datagps`.`Sensor7` << 8)) + 
(`datagps`.`Sensor6` << 16)) + 
(`datagps`.`Sensor5` << 24)) + 
(`datagps`.`Sensor4` << 32)) + 
(`datagps`.`Sensor3` << 40)) + 
(`datagps`.`Sensor2` << 48)) + 
(`datagps`.`Sensor1` << 56)) AS `sensor`,
`datagps`.`Events` AS `Events`,
`datagps`.`LogID` AS `LogID` 
from `datagps` 
where                                                                                                                                    
    (`datagps`.`Valid` = val) and                                                                                                            
    ( datagps.UnixTime BETWEEN UNIX_TIMESTAMP(begin_time) and UNIX_TIMESTAMP(end_time))                                                    
    and datagps.Mobitel_ID = m_id       
order by `datagps`.`LogID`;

END
$$

--
-- ������� ��������� hystoryOnline
--
DROP PROCEDURE IF EXISTS hystoryOnline$$
CREATE PROCEDURE hystoryOnline(IN id INTEGER(11))
  SQL SECURITY INVOKER
BEGIN
select LogID, Mobitel_Id, 
from_unixtime(unixtime)  as 'time',
round((`Latitude` / 600000.00000),6) AS `Lat`,
round((`Longitude` / 600000.00000),6)  AS `Lon`,
`Altitude`  AS `Altitude`,
(`Speed` * 1.852) AS `speed`,
((`Direction` * 360) / 255) AS `direction`,
`Valid` AS `Valid`,
(((((((`Sensor8` + 
(`Sensor7` << 8)) + 
(`Sensor6` << 16)) + 
(`Sensor5` << 24)) + 
(`Sensor4` << 32)) + 
(`Sensor3` << 40)) + 
(`Sensor2` << 48)) + 
(`Sensor1` << 56)) AS `sensor`,
`Events` AS `Events`,
DataGPS_ID  as 'DataGPS_ID'
from online
where  datagps_id > id
order by datagps_id DESC;
END
$$


--
-- ������� ��������� online
--
DROP PROCEDURE IF EXISTS online$$
CREATE PROCEDURE online()
  SQL SECURITY INVOKER
BEGIN
select Log_ID, MobitelId, 
(select DataGPS_ID
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) as 'DataGPS_ID',
(select from_unixtime(unixtime) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) as 'time',
( select round((`Latitude` / 600000.00000),6)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Lat`,
(select round((`Longitude` / 600000.00000),6) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Lon`,
(select `Altitude` 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Altitude`,
(select (`Speed` * 1.852)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `speed`,
(select ((`Direction` * 360) / 255)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `direction`,
(select `Valid`
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Valid`,
(select (((((((`Sensor8` + 
(`Sensor7` << 8)) + 
(`Sensor6` << 16)) + 
(`Sensor5` << 24)) + 
(`Sensor4` << 32)) + 
(`Sensor3` << 40)) + 
(`Sensor2` << 48)) + 
(`Sensor1` << 56)) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `sensor`,
(select `Events`
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Events`
 
from(
select max(`LogID`) AS `Log_ID`, mobitel_id as MobitelId
from `online` 
group by `Mobitel_ID`) T1;

END
$$

DELIMITER ;


/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
