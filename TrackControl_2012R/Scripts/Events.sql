-- ���� �������: 10.09.2009

--
-- �������� ������� ���������.
--
CREATE TABLE IF NOT EXISTS `State`(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `MinValue` DOUBLE NOT NULL,
    `MaxValue` DOUBLE NOT NULL,
    `Title` VARCHAR(50) NOT NULL,
    `SensorId` INTEGER DEFAULT NULL,
    PRIMARY KEY (`Id`),
    INDEX `State_FK` USING BTREE(`SensorId`)
) ENGINE = INNODB DEFAULT CHARSET=cp1251 COMMENT = '��������� ����������';

--
-- �������� ������� �������, �.�. ��������� �� ������
-- ��������� � ������.
--
CREATE TABLE IF NOT EXISTS `Transition` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `InitialStateId` INTEGER DEFAULT NULL,
  `FinalStateId` INTEGER DEFAULT NULL,
  `Title` VARCHAR(50) NOT NULL,
  `SensorId` INTEGER DEFAULT NULL,
  `IconName` VARCHAR(50) NOT NULL,
  `SoundName` VARCHAR(50) NOT NULL,
  `Enabled` BOOLEAN NOT NULL,
  `ViewInReport` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`),
  INDEX `Sensors_FK` USING BTREE (`SensorId`)
) ENGINE = InnoDB DEFAULT CHARSET=cp1251 COMMENT = '�������� ����� �����������';

--
-- ��� ���, � ���� ������� ������� ��� �������, �� ��� ������� `ViewInReport`
--
DELIMITER $$
DROP PROCEDURE IF EXISTS rcs__add_viewInReportColumn_to__TransitionTbl$$
CREATE PROCEDURE rcs__add_viewInReportColumn_to__TransitionTbl()
BEGIN
        IF NOT EXISTS
        (
                SELECT NULL
                FROM information_schema.columns
                    WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
                          (TABLE_NAME = "Transition") AND
                          (COLUMN_NAME = "ViewInReport")
        )
        THEN
                ALTER TABLE `Transition` ADD COLUMN `ViewInReport` BOOLEAN NOT NULL DEFAULT 0;
        END IF;
END$$
CALL rcs__add_viewInReportColumn_to__TransitionTbl()$$
DROP PROCEDURE IF EXISTS rcs__add_viewInReportColumn_to__TransitionTbl$$
DELIMITER ;

