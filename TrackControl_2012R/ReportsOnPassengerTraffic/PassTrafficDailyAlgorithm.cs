using System;
using System.Collections.Generic;
using LocalCache;
using ReportsOnPassengerTraffic.Properties;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// �������� ����� � ��������� ����������.
  /// </summary>
  class PassTrafficDailyAlgorithm : BasePassTrafficAlgorithm
  {

    public PassTrafficDailyAlgorithm(ReportPassZonesModel model)
      : base(model)
    {
    }

    /// <summary>
    /// ������������ ������.
    /// </summary>
    /// <returns>������������ ������.</returns>
    protected override string GetReportName()
    {
      return Resources.PassTrDailyName;
    }

    /// <summary>
    /// ���������� �������-������.
    /// </summary>
    protected override void FillReport()
    {
      // ������� ������ �� ������ ��������� ������� ��� �������� ���������
      atlantaDataSet.KilometrageReportDayRow[] kilometrage =
        (atlantaDataSet.KilometrageReportDayRow[])
        AtlantaDataSet.KilometrageReportDay.Select(String.Format(
          "MobitelId = {0}", m_row.Mobitel_ID), "InitialTime");

      if (kilometrage.Length == 0)
      {
        return;
      }

      PassTrafficDataSet.PassTrafficDailyRow newRow;
      InternalInit(kilometrage.Length);

      // � ����� ����� �������� ��������� ������ �� ������ ��������� �������
      foreach (atlantaDataSet.KilometrageReportDayRow klmRow in kilometrage)
      {
        newRow = Dataset.PassTrafficDaily.NewPassTrafficDailyRow();

        newRow.Mobitel_id = m_row.Mobitel_ID;
        newRow.Distance = (float)klmRow.Distance;
        newRow.StartTime = klmRow.InitialTime;

        if (newRow.Distance == 0)
        {
          newRow.FinishTime = klmRow.InitialTime;
          newRow.Duration = new TimeSpan();
          newRow.PassCount = 0;
        }
        else
        {
          newRow.FinishTime = klmRow.FinalTime;
          newRow.Duration = klmRow.FinalTime - klmRow.InitialTime;
          newRow.PassCount = CalcTotalPerDay(klmRow.InitialTime);
        }

        Dataset.PassTrafficDaily.AddPassTrafficDailyRow(newRow);
        ProgressIncrement();
      }
    }

    /// <summary>
    /// ������� ���-�� ������������ ���������� �� �������� ����.
    /// </summary>
    /// <param name="currenrDate">����, �� ������� ���������� ����� �������.</param>
    /// <returns>���-�� ����������.</returns>
    private uint CalcTotalPerDay(DateTime currenrDate)
    {
      uint result = 0;
      // ������ ������ ���������, ��������������� ��� ��������
      List<System.Int64> delItems = new List<System.Int64>();

      foreach (KeyValuePair<System.Int64, SensorValuesOnStop> kvp in SensorValuesOnStops)
      {
        if (kvp.Value.Time.Date.Equals(currenrDate.Date))
        {
          result += kvp.Value.Total;
          // �������� ��� ������������ ��������
          delItems.Add(kvp.Key);
        }
      }

      // ������ ��������, ������� ��� �� �����, ����� ��������� ���-��
      // �������� �� ��������� ���� - ��������� ����.
      foreach (int key in delItems)
      {
        SensorValuesOnStops.Remove(key);
      }
      delItems.Clear();

      return (result / 2);
    }
  }
}
