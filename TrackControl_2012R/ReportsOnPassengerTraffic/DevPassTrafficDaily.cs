﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports;
using LocalCache;
using BaseReports.Procedure;
using ReportsOnPassengerTraffic.Properties;
using BaseReports.Properties;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace ReportsOnPassengerTraffic
{
    /// <summary>
    /// Отчет "Пассажиры суточный".
    /// </summary>
    public partial class DevPassTrafficDaily : BaseReports.ReportsDE.BaseControl
    {
        public class reportPass
        {
            DateTime date;
            // Время начала движения
            DateTime startTime;
            // Время окончания движения
            DateTime finishTime;
            // Продолжительность смены
            TimeSpan duration; 
            // Пройденный путь, км
            double distance;
            // Перевезено пассажиров
            uint passCount;

            public reportPass(DateTime date, DateTime startTime, DateTime finishTime, TimeSpan duration, 
                    double distance, uint passCount)
            {
                this.date = date;
                // Время начала движения
                this.startTime = startTime;
                // Время окончания движения
                this.finishTime = finishTime;
                // Продолжительность смены
                this.duration = duration;
                // Пройденный путь, км
                this.distance = distance;
                // Перевезено пассажиров
                this.passCount = passCount;
            } // reportPass

            public DateTime Date
            {
                get { return date; }
            }

            public DateTime StartTime
            {
                get { return startTime; }
            }

            public DateTime FinishTime
            {
                get { return finishTime; }
            }

            public TimeSpan Duration
            {
                get { return duration; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public uint PassCount
            {
                get { return passCount; }
            }
        } // reportPass

        ReportPassZonesModel model;
        /// <summary>
        /// Датасет PassTrafficDataSet.
        /// </summary>
        private PassTrafficDataSet passDataSet;
        /// <summary>
        /// Итоги с группировкой по телетрекам.
        /// </summary>
        private IDictionary<int, PassTrafficDailyTotals> totals;

        protected static atlantaDataSet dataset;
        VehicleInfo vehicleInfo;
        ReportBase<reportPass, TInfo> ReportingPass;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public DevPassTrafficDaily(ReportPassZonesModel model)
        {
            InitializeComponent();
            Init();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel( gridViewPass, gridControlPass, bar3 );

            this.model = model;
            dataset = ReportTabControl.Dataset;
            totals = new Dictionary<int, PassTrafficDailyTotals>();
            passDataSet = PassTrafficDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = passDataSet;
            atlantaDataSetBindingSource.DataMember = "PassTrafficDaily";
            gridControlPass.DataSource = atlantaDataSetBindingSource;
            passTrafficDataSet = null;
            ClearStatusLine();
            AddAlgorithm( new Kilometrage() );
            AddAlgorithm( new KilometrageDays() );
            AddAlgorithm( new PassTrafficDailyAlgorithm( model ) );

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler( composLink_CreateMarginalHeaderArea );

            ReportingPass =
                new ReportBase<reportPass, TInfo>( Controls, compositeLink1, gridViewPass,
                   GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp );
        } // DevPassTrafficDaily

        public override string  Caption
        {
            get { return Resources.PassTrDailyName; }
        } // Captic

        /// <summary>
        /// Очищает данные отчета.
        /// </summary>
        public override void ClearReport()
        {
            passDataSet.PassTrafficDaily.Clear();
            totals.Clear();
        }

        /// <summary>
        /// Нажатие кнопки формирования отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if ( bbiStart.Caption == Resources.Start )
            {
                _stopRun = false;

                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                atlantaDataSetBindingSource.SuspendBinding();

                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && m_row.GetdataviewRows().Length > 0 )
                    {
                        SelectItem( m_row );
                        noData = false;
                        if ( _stopRun )
                            break;
                    }
                }

                if ( noData )
                {
                    XtraMessageBox.Show( Resources.WarningNoData, Resources.Notification );
                }

                atlantaDataSetBindingSource.ResumeBinding();

                Select( curMrow );

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                _stopRun = true;

                StopReport();

                if ( !noData )
                    EnableButton();

                SetStartButton();
            } // else
        } // bbiStart_ItemClick

        public override void Select( atlantaDataSet.mobitelsRow m_row )
        {
            if ( m_row != null )
            {
                ClearStatusLine();

                curMrow = m_row;
                vehicleInfo = new VehicleInfo( m_row );
                atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", m_row.Mobitel_ID );

                PassTrafficDailyTotals passTrafficTotals = GetTotals( m_row.Mobitel_ID );

                if ( passTrafficTotals != null )
                {
                    barStaticItem1.Caption = passTrafficTotals.TotalDistance.ToString( "#,##0.00" );
                    barStaticItem2.Caption = passTrafficTotals.TotalDuration.ToString();
                    barStaticItem3.Caption = passTrafficTotals.TotalPassCount.ToString();
                    barStaticItem4.Caption = passTrafficTotals.AvgPassCount.ToString( "#,##0.00" );
                } // if
            } // if
        } // Select

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem( atlantaDataSet.mobitelsRow m_row )
        {
            DisableButton();
            SetStopButton();
            Application.DoEvents();
            foreach ( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if ( _stopRun )
                    return;
            }
            EnableButton();
            SetStartButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        /// <summary>
        /// Расчет итогов.
        /// </summary>
        /// <param name="mobitelId">Идентификатор телетрека.</param>
        /// <returns>PassTrafficDailyTotals - итоги по данному телетреку.</returns>
        private PassTrafficDailyTotals GetTotals( int mobitelId )
        {
            if ( totals.ContainsKey( mobitelId ) )
            {
                return totals[mobitelId];
            }

            if ( atlantaDataSetBindingSource.Count == 0 )
            {
                return null;
            }

            PassTrafficDataSet.PassTrafficDailyRow row;
            PassTrafficDailyTotals passTrafficTotals = new PassTrafficDailyTotals();

            foreach ( DataRowView dr in atlantaDataSetBindingSource )
            {
                row = ( PassTrafficDataSet.PassTrafficDailyRow ) dr.Row;
                passTrafficTotals.TotalDistance += row.Distance;
                passTrafficTotals.TotalDuration += row.Duration;
                passTrafficTotals.TotalPassCount += row.PassCount;
            } // foreach

            passTrafficTotals.AvgPassCount = ( atlantaDataSetBindingSource.Count == 0 ) ? 0 : 
                ( float ) passTrafficTotals.TotalPassCount / atlantaDataSetBindingSource.Count;

            totals.Add( mobitelId, passTrafficTotals );
            return passTrafficTotals;
        } // GetTotals

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick( object sender, ItemClickEventArgs e )
        {
            // в предыдущем отчете эта функция не реализованная
        }

        //клик по строке таблицы
        private void gViewKlmtTotal_ClickRow( object sender, RowClickEventArgs e )
        {
            if ( e.RowHandle >= 0 )
            {
                // в предыдущем отчете эта функция не реализованная
            }
        }

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick( object sender, ItemClickEventArgs e )
        {
            // в предыдущем отчете эта функция не реализованная
        }

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea( object sender, CreateAreaEventArgs e )
        {
            DevExpressReportHeader( Resources.DetailReportPassag, e );
            TInfo info = ReportingPass.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader( strPeriod, 22, e );
        }  // composLink_CreateMarginalHeaderArea

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingPass.GetInfoStructure;
            return ( Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName );
        }

        /* функция для формирования верхней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ( "" );
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingPass.GetInfoStructure;
            return ( Resources.PassTrDailyTravel + ": " + String.Format( "{0:f2}", info.totalWay ) + Resources.KM + "\n" +
                Resources.PassTrDailyDurationWork + ": " + info.totalTimerTour + "\n" +
                Resources.PassTrDailyCommonCarryOverPassenger + ": " + info.totalPassCount + "\n" +
                Resources.PassTrDailyMiddleCarryPassOnDay + ": " + info.passCountAvg );
        } // GetStringBreackRight

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint( gridViewPass, true, true );

            TInfo tinfo = new TInfo();
            tinfo.periodBeging = Algorithm.Period.Begin;
            tinfo.periodEnd = Algorithm.Period.End;
            VehicleInfo info = new VehicleInfo( curMrow.Mobitel_ID );
            tinfo.infoVehicle = info.Info;
            tinfo.infoDriverName = info.DriverFullName;

            ReportingPass.AddInfoStructToList( tinfo ); /* формируем заголовки таблиц отчета */
            ReportingPass.CreateAndShowReport( gridControlPass );
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint( gridViewPass, true, true );
            foreach ( atlantaDataSet.mobitelsRow _mobitel in dataset.mobitels )
            {
                if ( _mobitel.Check && _mobitel.GetdataviewRows().Length > 0 )
                {
                    TInfo tinfo = new TInfo();
                    tinfo.periodBeging = Algorithm.Period.Begin;
                    tinfo.periodEnd = Algorithm.Period.End;

                    VehicleInfo info = new VehicleInfo( _mobitel );
                    tinfo.infoVehicle = info.Info;
                    tinfo.infoDriverName = info.DriverFullName;

                    atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", _mobitel.Mobitel_ID );

                    PassTrafficDailyTotals passTrafficTotals = GetTotals( _mobitel.Mobitel_ID );

                    if ( passTrafficTotals != null )
                    {
                        tinfo.totalWay = passTrafficTotals.TotalDistance;
                        tinfo.totalTimerTour = passTrafficTotals.TotalDuration;
                        tinfo.totalPassCount = passTrafficTotals.TotalPassCount;
                        tinfo.passCountAvg = passTrafficTotals.AvgPassCount;
                    } // if

                    PassTrafficDataSet.PassTrafficDailyRow row;

                    ReportingPass.AddInfoStructToList( tinfo );
                    ReportingPass.CreateBindDataList();

                    foreach ( DataRowView dr in atlantaDataSetBindingSource )
                    {
                        row = ( PassTrafficDataSet.PassTrafficDailyRow ) dr.Row;
                        // Дата
                        DateTime date = row.StartTime;
                        // Время начала движения
                        DateTime startTime = row.StartTime;
                        // Время окончания движения
                        DateTime finishTime = row.FinishTime;
                        // Продолжительность смены
                        TimeSpan duration = row.Duration;
                        // Пройденный путь, км
                        double distance = row.Distance;
                        // Перевезено пассажиров
                        uint passCount = row.PassCount;

                        ReportingPass.AddDataToBindList( new reportPass( date, startTime, finishTime, duration, 
                            distance, passCount ) );
                    } // foreach 2

                    ReportingPass.CreateElementReport();
                } // if
            } // foreach 1

            ReportingPass.CreateAndShowReport();
            ReportingPass.DeleteData();
        } // ExportAllDevToReport

        /// <summary>
        /// Очистка итогов.
        /// </summary>
        private void ClearStatusLine()
        {
            barStaticItem1.Caption = Resources.PassTrDailyTravel + ": 0;";
            barStaticItem2.Caption = Resources.PassTrDailyDurationWork + ": 00:00";
            barStaticItem3.Caption = Resources.PassTrDailyCommonCarryOverPassenger + ": 0;";
            barStaticItem4.Caption = Resources.PassTrDailyMiddleCarryPassOnDay + ": 0;";
        }

        protected override void barButtonGroupPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            GroupPanel( gridViewPass );
        }

        protected override void barButtonFooterPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            FooterPanel( gridViewPass );
        }

        protected override void barButtonNavigator_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            NavigatorPanel( gridControlPass );
        }

        protected override void barButtonStatusPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            StatusBar( bar3 );
        }                                

        public void Localization()
        {
            colData.Caption = Resources.PassTrDailyDate;
            colData.ToolTip = Resources.PassTrDailyDate;

            colStartTime.Caption = Resources.PassTrDailyTimeBeginMoving;
            colStartTime.ToolTip = Resources.PassTrDailyTimeBeginMoving;

            colFinishTime.Caption = Resources.PassTrDailyTimeEndMoving;
            colFinishTime.ToolTip = Resources.PassTrDailyTimeEndMoving;

            colDuration.Caption = Resources.PassTrDailyDurationWork;
            colDuration.ToolTip = Resources.PassTrDailyDurationWork;

            colDistance.Caption = Resources.PassTrDailyTravel;
            colDistance.ToolTip = Resources.PassTrDailyTravel;

            colPassCount.Caption = Resources.PassTrDailyMiddlePassOnDay;
            colPassCount.ToolTip = Resources.PassTrDailyMiddlePassOnDay;
        } // Localization
    } // DevPassTrafficDaily
} // ReportsOnPassengerTraffic
