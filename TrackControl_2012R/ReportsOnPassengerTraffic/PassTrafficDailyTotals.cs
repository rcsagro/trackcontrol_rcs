using System;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ����� �� ������ "��������� ��������".
  /// </summary>
  class PassTrafficDailyTotals
  {
    private float totalDistance;
    /// <summary>
    /// ����� ���������� ����, ��.
    /// </summary>
    public float TotalDistance
    {
      get { return totalDistance; }
      set { totalDistance = value; }
    }

    private TimeSpan totalDuration;
    /// <summary>
    /// ����� ����������������� ������.
    /// </summary>
    public TimeSpan TotalDuration
    {
      get { return totalDuration; }
      set { totalDuration = value; }
    }

    private uint totalPassCount;
    /// <summary>
    /// ����� ���������� ����������.
    /// </summary>
    public uint TotalPassCount
    {
      get { return totalPassCount; }
      set { totalPassCount = value; }
    }

    private float avgPassCount;
    /// <summary>
    /// � ������� ���������� ���������� �� �����.
    /// </summary>
    public float AvgPassCount
    {
      get { return avgPassCount; }
      set { avgPassCount = value; }
    }
  }
}
