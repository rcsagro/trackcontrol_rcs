using TrackControl.General;

namespace ReportsOnPassengerTraffic
{
    /// <summary>
    /// �������� ������������ ������� � ���������������.
    /// </summary>
    public class ReportPassCZ
    {
        private IZone zone;

        /// <summary>
        /// ������������� ��.
        /// </summary>
        public int ZoneID
        {
            get { return zone.Id; }
        }

        private int reportPassZonesId;

        /// <summary>
        /// ������������� ReportPassZones.
        /// </summary>
        public int ReportPassZonesId
        {
            get { return reportPassZonesId; }
            set { reportPassZonesId = value; }
        }

        /// <summary>
        /// ������������ �������� ������� � �������.
        /// </summary>
        private bool? originalSelected;

        private bool selected;

        /// <summary>
        /// ������� � �������.
        /// </summary>
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                if (!originalSelected.HasValue)
                {
                    originalSelected = value;
                }
            }
        }

        /// <summary>
        /// ������������ ����������� ����.
        /// </summary>
        public string Name
        {
            get { return zone.Name; }
        }

        /// <summary>
        /// �������� ����������� ����.
        /// </summary>
        public string Description
        {
            get { return zone.Description; }
        }

        public bool IsChanged
        {
            get
            {
                return ((originalSelected.HasValue) &&
                        (originalSelected.Value != selected));
            }

            set
            {
                selected = value;
            }
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="zone">����.</param>
        public ReportPassCZ(IZone zone)
        {
            this.zone = zone;
        }

        /// <summary>
        /// ������ ���������
        /// </summary>
        public void RejectChanges()
        {
            if (originalSelected.HasValue)
            {
                selected = originalSelected.Value;
            }
        }

        /// <summary>
        /// ������������� ���������
        /// </summary>
        public void AcceptChanges()
        {
            originalSelected = Selected;
        }
    }
}
