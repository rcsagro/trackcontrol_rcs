using TrackControl.General;

namespace ReportsOnPassengerTraffic
{
    /// <summary>
    /// ���������� ���� ��� ������� � ���������������.
    /// �������� ������� report_pass_zones.
    /// </summary>
    public class ReportPassZone : Entity
    {
        /// <summary>
        /// ������������� ��
        /// </summary>
        public int ZoneID
        {
            get { return _zoneID; }
            set { _zoneID = value; }
        }

        private int _zoneID;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="id">�������������.</param>
        /// <param name="zoneID">������������� ��.</param>
        public ReportPassZone(int id, int zoneID)
        {
            Id = id;
            _zoneID = zoneID;
        }
    }
}
