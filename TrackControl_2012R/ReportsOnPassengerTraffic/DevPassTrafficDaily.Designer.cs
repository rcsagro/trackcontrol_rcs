﻿namespace ReportsOnPassengerTraffic
{
    partial class DevPassTrafficDaily
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevPassTrafficDaily ) );
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.gridControlPass = new DevExpress.XtraGrid.GridControl();
            this.PassTrafficDailyBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.passTrafficDataSet = new ReportsOnPassengerTraffic.PassTrafficDataSet();
            this.gridViewPass = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.barManager1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlPass ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.PassTrafficDailyBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.passTrafficDataSet ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridViewPass ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this._imageComboRepo ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.repositoryItemTextEdit1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).BeginInit();
            this.SuspendLayout();
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar3} );
            this.barManager1.DockControls.Add( this.barDockControl1 );
            this.barManager1.DockControls.Add( this.barDockControl2 );
            this.barManager1.DockControls.Add( this.barDockControl3 );
            this.barManager1.DockControls.Add( this.barDockControl4 );
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4} );
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4)} );
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Пройденный путь, км: 0";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Продолжительность работы: 00:00";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Перевезено пассажиров: 0";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Среднее пассажиры/сутки: 0";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // gridControlPass
            // 
            this.gridControlPass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gridControlPass.DataSource = this.PassTrafficDailyBindingSource;
            this.gridControlPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPass.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPass.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPass.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPass.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPass.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPass.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPass.Location = new System.Drawing.Point( 0, 26 );
            this.gridControlPass.MainView = this.gridViewPass;
            this.gridControlPass.MenuManager = this.barManager1;
            this.gridControlPass.Name = "gridControlPass";
            this.gridControlPass.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1} );
            this.gridControlPass.Size = new System.Drawing.Size( 933, 457 );
            this.gridControlPass.TabIndex = 10;
            this.gridControlPass.UseEmbeddedNavigator = true;
            this.gridControlPass.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPass,
            this.gridView2} );
            // 
            // PassTrafficDailyBindingSource
            // 
            this.PassTrafficDailyBindingSource.DataMember = "PassTrafficDaily";
            this.PassTrafficDailyBindingSource.DataSource = this.passTrafficDataSet;
            // 
            // passTrafficDataSet
            // 
            this.passTrafficDataSet.DataSetName = "PassTrafficDataSet";
            this.passTrafficDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewPass
            // 
            this.gridViewPass.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridViewPass.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridViewPass.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridViewPass.Appearance.Empty.Options.UseBackColor = true;
            this.gridViewPass.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gridViewPass.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gridViewPass.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gridViewPass.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridViewPass.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gridViewPass.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridViewPass.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridViewPass.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridViewPass.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridViewPass.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridViewPass.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPass.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridViewPass.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridViewPass.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewPass.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewPass.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridViewPass.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridViewPass.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridViewPass.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridViewPass.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridViewPass.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewPass.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.gridViewPass.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewPass.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gridViewPass.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridViewPass.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridViewPass.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridViewPass.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.gridViewPass.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridViewPass.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gridViewPass.Appearance.OddRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gridViewPass.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.Preview.Options.UseBackColor = true;
            this.gridViewPass.Appearance.Preview.Options.UseForeColor = true;
            this.gridViewPass.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewPass.Appearance.Row.Options.UseBackColor = true;
            this.gridViewPass.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridViewPass.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gridViewPass.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewPass.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gridViewPass.Appearance.VertLine.Options.UseBackColor = true;
            this.gridViewPass.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gridViewPass.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewPass.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridViewPass.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridViewPass.ColumnPanelRowHeight = 40;
            this.gridViewPass.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colStartTime,
            this.colFinishTime,
            this.colDuration,
            this.colDistance,
            this.colPassCount} );
            this.gridViewPass.GridControl = this.gridControlPass;
            this.gridViewPass.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewPass.Name = "gridViewPass";
            this.gridViewPass.OptionsDetail.AllowZoomDetail = false;
            this.gridViewPass.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewPass.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPass.OptionsDetail.SmartDetailExpand = false;
            this.gridViewPass.OptionsSelection.MultiSelect = true;
            this.gridViewPass.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPass.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewPass.OptionsView.ShowFooter = true;
            // 
            // colData
            // 
            this.colData.AppearanceCell.Options.UseTextOptions = true;
            this.colData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colData.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colData.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colData.AppearanceHeader.Options.UseTextOptions = true;
            this.colData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colData.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colData.Caption = "Дата";
            this.colData.DisplayFormat.FormatString = "dd.MM.yyyy hh:mm";
            this.colData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colData.FieldName = "StartTime";
            this.colData.Name = "colData";
            this.colData.OptionsColumn.AllowEdit = false;
            this.colData.OptionsColumn.AllowFocus = false;
            this.colData.OptionsColumn.ReadOnly = true;
            this.colData.ToolTip = "Дата";
            this.colData.Visible = true;
            this.colData.VisibleIndex = 0;
            this.colData.Width = 80;
            // 
            // colStartTime
            // 
            this.colStartTime.AppearanceCell.Options.UseTextOptions = true;
            this.colStartTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colStartTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStartTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStartTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStartTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStartTime.Caption = "Время начала движения";
            this.colStartTime.DisplayFormat.FormatString = "dd.MM.yyyy hh:mm";
            this.colStartTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.ToolTip = "Время начала движения";
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 1;
            this.colStartTime.Width = 100;
            // 
            // colFinishTime
            // 
            this.colFinishTime.AppearanceCell.Options.UseTextOptions = true;
            this.colFinishTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFinishTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFinishTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFinishTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colFinishTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFinishTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFinishTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFinishTime.Caption = "Время окончания движения";
            this.colFinishTime.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.colFinishTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFinishTime.FieldName = "FinishTime";
            this.colFinishTime.Name = "colFinishTime";
            this.colFinishTime.OptionsColumn.AllowEdit = false;
            this.colFinishTime.OptionsColumn.AllowFocus = false;
            this.colFinishTime.OptionsColumn.ReadOnly = true;
            this.colFinishTime.ToolTip = "Время окончания движения";
            this.colFinishTime.Visible = true;
            this.colFinishTime.VisibleIndex = 2;
            this.colFinishTime.Width = 100;
            // 
            // colDuration
            // 
            this.colDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDuration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.Caption = "Продолжительность смены, ч";
            this.colDuration.DisplayFormat.FormatString = "N2";
            this.colDuration.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDuration.FieldName = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.ToolTip = "Продолжительность смены, ч";
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 3;
            this.colDuration.Width = 100;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Пройденный путь, км";
            this.colDistance.DisplayFormat.FormatString = "N2";
            this.colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.SummaryItem.DisplayFormat = "{0:dd.HH:mm}";
            this.colDistance.SummaryItem.FieldName = "Interval";
            this.colDistance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDistance.ToolTip = "Пройденный путь, км";
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 4;
            this.colDistance.Width = 100;
            // 
            // colPassCount
            // 
            this.colPassCount.AppearanceCell.Options.UseTextOptions = true;
            this.colPassCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPassCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPassCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colPassCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPassCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPassCount.Caption = "Перевезено пассажиров";
            this.colPassCount.DisplayFormat.FormatString = "N2";
            this.colPassCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPassCount.FieldName = "PassCount";
            this.colPassCount.Name = "colPassCount";
            this.colPassCount.OptionsColumn.AllowEdit = false;
            this.colPassCount.OptionsColumn.AllowFocus = false;
            this.colPassCount.OptionsColumn.ReadOnly = true;
            this.colPassCount.SummaryItem.DisplayFormat = "{0:f2}";
            this.colPassCount.SummaryItem.FieldName = "Distance";
            this.colPassCount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPassCount.ToolTip = "Перевезено пассажиров";
            this.colPassCount.Visible = true;
            this.colPassCount.VisibleIndex = 5;
            this.colPassCount.Width = 100;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange( new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)} );
            this._imageComboRepo.Items.AddRange( new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)} );
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControlPass;
            this.gridView2.Name = "gridView2";
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer ) ( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins( 10, 10, 70, 10 );
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins( 10, 10, 15, 10 );
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // DevPassTrafficDaily
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.gridControlPass );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "DevPassTrafficDaily";
            this.Size = new System.Drawing.Size( 933, 532 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.gridControlPass, 0 );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.barManager1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlPass ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.PassTrafficDailyBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.passTrafficDataSet ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridViewPass ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this._imageComboRepo ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.repositoryItemTextEdit1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraGrid.GridControl gridControlPass;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPass;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colPassCount;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource PassTrafficDailyBindingSource;
        private PassTrafficDataSet passTrafficDataSet;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
    }
}
