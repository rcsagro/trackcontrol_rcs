
namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ������� <see cref="ReportsOnPassengerTraffic.PassTrafficDataSet"/>.
  /// ��� �������� � ���������, ������� ��������
  /// � ������ ��������� ������� �������� ��������� �
  /// ������� ������, � �� �������������� ��������������.
  /// </summary>
  class PassTrafficDataSetCreator
  {
    /// <summary>
    /// ������ �������������
    /// </summary>
    private static volatile object syncObject = new object();
    /// <summary>
    /// ��������� ��������
    /// </summary>
    private static volatile PassTrafficDataSet dataSet;

    /// <summary>
    /// ���������� ��������� ��������
    /// </summary>
    /// <returns>PassTrafficDataSet</returns>
    internal static PassTrafficDataSet GetDataSet()
    {
      if (dataSet == null)
      {
        lock (syncObject)
        {
          if (dataSet == null)
          {
            dataSet = new PassTrafficDataSet();
          }
        }
      }
      return dataSet;
    }
  }
}
