using System;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ������ ���-�� �������� � �������� ���������� �� ���������.
  /// </summary>
  class SensorValuesOnStop
  {
    private uint sensor1Value;
    /// <summary>
    /// ���-�� �������� � �������� ���������� �� ���������, 
    /// �� ���������� ������� ��������.
    /// </summary>
    public uint Sensor1Value
    {
      get { return sensor1Value; }
      set { sensor1Value = value; }
    }

    private uint sensor2Value;
    /// <summary>
    /// ���-�� �������� � �������� ���������� �� ���������, 
    /// �� ���������� ������� �������.
    /// </summary>
    public uint Sensor2Value
    {
      get { return sensor2Value; }
      set { sensor2Value = value; }
    }

    /// <summary>
    /// ��������� ���-�� �������� � �������� ���������� �� ���������, 
    /// �� ���������� ���� ��������.
    /// </summary>
    public uint Total
    {
      get { return sensor1Value + sensor2Value; }
    }

    private DateTime time;
    /// <summary>
    /// ����� ���������
    /// </summary>
    public DateTime Time
    {
      get { return time; }
      set { time = value; }
    }
  }
}
