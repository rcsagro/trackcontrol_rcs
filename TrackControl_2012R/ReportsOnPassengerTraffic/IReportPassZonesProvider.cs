using System;
using System.Collections.Generic;

namespace ReportsOnPassengerTraffic
{
  public interface IReportPassZonesProvider
  {
    IList<ReportPassZone> GetAll();
    bool Save(ReportPassZone entity);
    bool Delete(ReportPassZone entity);
  }
}
