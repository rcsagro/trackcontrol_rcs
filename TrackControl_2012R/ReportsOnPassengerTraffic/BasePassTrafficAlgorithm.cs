using System;
using System.Collections.Generic;
using System.Diagnostics;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;
using TrackControl.Zones;
using TrackControl.General;
using ReportsOnPassengerTraffic.Properties;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ������� ����� ������� ��������� � ����������������.
  /// �������� �������� ������ � Doc\��������������.doc. 
  /// � SensorsValues ��������� ������ ������ �������� ���������.
  /// </summary>
  partial class BasePassTrafficAlgorithm : Algorithm
  {
    /// <summary>
    /// ����� {0} ������ ���� �������� ���������. 
    /// �� ������� �������� ��� �������� � ������ BasePassTrafficAlgorithm.
    /// </summary>
    private const string NOT_IMPLEMENTED_EXCEPTION =
      "����� {0} ������ ���� �������� ���������. " +
      "�� ������� �������� ��� �������� � ������ BasePassTrafficAlgorithm.";

    ReportPassZonesModel _model;

    private PassTrafficDataSet dataset;
    /// <summary>
    /// ������� PassTrafficDataSet.
    /// </summary>
    protected PassTrafficDataSet Dataset
    {
      [DebuggerStepThrough]
      get { return dataset; }
    }

    private atlantaDataSet.sensorsRow passSensor1;
    /// <summary>
    /// ������ ��������������� �1.
    /// </summary>
    protected atlantaDataSet.sensorsRow PassSensor1
    {
      [DebuggerStepThrough]
      get { return passSensor1; }
    }

    private atlantaDataSet.sensorsRow passSensor2;
    /// <summary>
    /// ������ ��������������� �2.
    /// </summary>
    protected atlantaDataSet.sensorsRow PassSensor2
    {
      [DebuggerStepThrough]
      get { return passSensor2; }
    }

    private atlantaDataSet.sensorsRow doorSensor1;
    /// <summary>
    /// ������ ����� �1.
    /// </summary>
    protected atlantaDataSet.sensorsRow DoorSensor1
    {
      [DebuggerStepThrough]
      get { return doorSensor1; }
    }

    private atlantaDataSet.sensorsRow doorSensor2;
    /// <summary>
    /// ������ ����� �2.
    /// </summary>
    protected atlantaDataSet.sensorsRow DoorSensor2
    {
      [DebuggerStepThrough]
      get { return doorSensor2; }
    }

    /// <summary>
    /// ���������� �� ������ ��������������� �1.
    /// </summary>
    protected bool PassengerSensor1Exists
    {
      [DebuggerStepThrough]
      get { return PassSensor1 != null; }
    }

    /// <summary>
    /// ���������� �� ������ ��������������� �2.
    /// </summary>
    protected bool PassengerSensor2Exists
    {
      [DebuggerStepThrough]
      get { return PassSensor2 != null; }
    }

    /// <summary>
    /// ���������� �� ������ ����� �1.
    /// </summary>
    protected bool DoorSensor1Exists
    {
      [DebuggerStepThrough]
      get { return DoorSensor1 != null; }
    }

    /// <summary>
    /// ���������� �� ������ ����� �2.
    /// </summary>
    protected bool DoorSensor2Exists
    {
      [DebuggerStepThrough]
      get { return DoorSensor2 != null; }
    }

    private IDictionary<System.Int64, uint> correctValueSeries1;
    /// <summary>
    /// ��� ���������� �������� ������� ��������������� �1.
    /// ������� ��������� ������������ ����� ���� DataGpsID - Value.
    /// </summary>
    protected IDictionary<System.Int64, uint> CorrectValueSeries1
    {
      [DebuggerStepThrough]
      get { return correctValueSeries1; }
    }

    private IDictionary<System.Int64, uint> correctValueSeries2;
    /// <summary>
    /// ��� ���������� �������� ������� ��������������� �2.
    /// ������� ��������� ������������ ����� ���� DataGpsID - Value.
    /// </summary>
    protected IDictionary<System.Int64, uint> CorrectValueSeries2
    {
      [DebuggerStepThrough]
      get { return correctValueSeries2; }
    }

    private IDictionary<System.Int64, SensorValuesOnStop> sensorValuesOnStops;
    /// <summary>
    /// ���-�� �������� � �������� ���������� �� ����������,
    /// �� �������� ���������.
    /// ������� ��������� ������������ ����� ���� DataGpsID - SensorValuesOnStop.
    /// </summary>
    protected IDictionary<System.Int64, SensorValuesOnStop> SensorValuesOnStops
    {
      [DebuggerStepThrough]
      get { return sensorValuesOnStops; }
    }

    /// <summary>
    /// ����������� ��������� �������.
    /// </summary>
    private Calibrate calibrate;

    /// <summary>
    /// ������� �������� ��� ��������-����.
    /// </summary>
    private int progressCounter;

    public BasePassTrafficAlgorithm(ReportPassZonesModel model)
    {
      _model = model;
      dataset = PassTrafficDataSetCreator.GetDataSet();
      sensorValuesOnStops = new SortedDictionary<System.Int64, SensorValuesOnStop>();
      calibrate = new Calibrate();
      CreateSeries();
    }

    /// <summary>
    /// ������ ��� �������.
    /// ������ ���� ��������������� ���� �� ����
    /// ������ �������� ����������.
    /// </summary>
    public override void Run()
    {
      FindSensors();
      if ((GpsDatas.Length == 0) || ((passSensor1 == null) && (passSensor2 == null)))
      {
        FreeSensorReferences();
        return;
      }

      SensorValuesOnStops.Clear();
      SelectValues();
      CorrectValues();

      try
      {
        ProbeTriggeringCount();
        FillReport();
      }
      finally
      {
        ClearSeries();
        FreeSensorReferences();
        AfterReportFilling();
        OnAction(this, null);
      }
    }

    /// <summary>
    /// ��������� ������������� �������� ������������ �
    /// ������������ ������.
    /// ���������� ������������ � ����� FillReport ����� ����,
    /// ��� ������ �������� ���-�� ��������.
    /// </summary>
    /// <param name="maxProgressCount"></param>
    protected void InternalInit(int maxProgressCount)
    {
      BeforeReportFilling(GetReportName(), maxProgressCount);
    }

    /// <summary>
    /// ���������� �������� ��������-����.
    /// </summary>
    protected void ProgressIncrement()
    {
      ReportProgressChanged(++progressCounter);
    }

    /// <summary>
    /// �������� �������� ����� ������������������ �������� 
    /// �������� ���������������.
    /// </summary>
    private void CreateSeries()
    {
        correctValueSeries1 = new Dictionary<System.Int64, uint>();
        correctValueSeries2 = new Dictionary<System.Int64, uint>();
    }

    /// <summary>
    /// ������� ������� � ������ ��������� �������� �������� ���������������.
    /// </summary>
    private void ClearSeries()
    {
      correctValueSeries1.Clear();
      correctValueSeries2.Clear();
    }

    /// <summary>
    /// ����� ������� �������� - ��������� 1 � 2, ����� 1 � 2.
    /// </summary>
    private void FindSensors()
    {
      foreach (atlantaDataSet.sensorsRow s_row in m_row.GetsensorsRows())
      {
        foreach (atlantaDataSet.relationalgorithmsRow r_row in s_row.GetrelationalgorithmsRows())
        {
          switch (r_row.AlgorithmID)
          {
            case (int)AlgorithmType.PASS1:
              passSensor1 = s_row;
              break;
            case (int)AlgorithmType.PASS2:
              passSensor2 = s_row;
              break;
            case (int)AlgorithmType.DOOR1:
              doorSensor1 = s_row;
              break;
            case (int)AlgorithmType.DOOR2:
              doorSensor2 = s_row;
              break;
          }
        }
      }
    }

    /// <summary>
    /// ������������ ������ �� �������� ��������.
    /// </summary>
    private void FreeSensorReferences()
    {
      passSensor1 = null;
      passSensor2 = null;
      doorSensor1 = null;
      doorSensor2 = null;
    }

    /// <summary>
    /// ������������ ������
    /// </summary>
    /// <returns>������������ ������</returns>
    protected virtual string GetReportName()
    {
      throw new NotImplementedException(String.Format(
        NOT_IMPLEMENTED_EXCEPTION, "GetReportName"));
    }

    /// <summary>
    /// ���������� �������-������
    /// </summary>
    protected virtual void FillReport()
    {
      throw new NotImplementedException(String.Format(
        NOT_IMPLEMENTED_EXCEPTION, "FillReport"));
    }

    /// <summary>
    /// ������� ������. ���������� ����� CorrectValueSeries1 � CorrectValueSeries2
    /// ���������� �������� ��������������� ��� ���� - � ���������� ��������� �������������
    /// ��������.
    /// </summary>
    private void SelectValues()
    {
      foreach (GpsData d in GpsDatas)
      {
        if (PassengerSensor1Exists)
        {
          CorrectValueSeries1.Add(d.Id, (uint)calibrate.GetUserValue(
            d.Sensors, PassSensor1.Length, PassSensor1.StartBit, PassSensor1.K, PassSensor1.B, PassSensor1.S));
        }
        if (PassengerSensor2Exists)
        {
          CorrectValueSeries2.Add(d.Id, (uint)calibrate.GetUserValue(
            d.Sensors, PassSensor2.Length, PassSensor2.StartBit, PassSensor2.K, PassSensor2.B, PassSensor2.S));
        }
      }
    }

    /// <summary>
    /// ����� �������� ������� � ������ ������� dataview �������� 
    /// ��������������� dataGpsId. 
    /// </summary>
    /// <exception cref="ApplicationException">
    /// ������ � ������� dataview � �������� ��������������� �� �������</exception>
    /// <param name="dataGpsId">dataGpsId</param>
    /// <returns>���� � �����</returns>
    protected DateTime GetTimeById(System.Int64 dataGpsId)
    {
      atlantaDataSet.dataviewRow row = AtlantaDataSet.dataview.FindByDataGps_ID(dataGpsId);
      if (row == null)
      {
        throw new ApplicationException(String.Format(Resources.BasePassTrNotStringException+" {0}.", dataGpsId));
      }
      return row.time;
    }

    /// <summary>
    /// ������������� �������� ��������� �������� - ��������� ������ � ������������. 
    /// � ���������� �������� ���� � ������������ ����������.
    /// </summary>
    private void CorrectValues()
    {
      if (PassengerSensor1Exists)
      {
        CorrectSeriesValues(CorrectValueSeries1, PassSensor1);
      }
      if (PassengerSensor2Exists)
      {
        CorrectSeriesValues(CorrectValueSeries2, PassSensor2);
      }
    }

    /// <summary>
    /// ����������� ������ ���������� - 3 ���. � �������
    /// </summary>
    byte MAX_PASS_PER_SEC = 6;
    /// <summary>
    /// ������������ �������� ����� �������� = 1016 
    /// </summary>
    ushort MAX_SENSOR_BOARD_VALUE = 1016;

    /// <summary>
    /// ������������� �������� ��������� ������� ��������������� ��������� ���� - 
    /// ��������� ������ � ������������. 
    /// � ���������� �������� ��� � ������������ ����������.
    /// </summary>
    /// <param name="series">��� �������� � ����������� ������� ���������������.</param>
    /// <param name="sensor">������ ���������������.</param>
    private void CorrectSeriesValues(IDictionary<System.Int64, uint> series,
      atlantaDataSet.sensorsRow sensor)
    {
      // ������ ������������ ��������� ����� ��������� ����������
      uint[] buffer = new uint[series.Count];
      buffer[series.Count - 1] = 0;

      // �������� ������� � ������ ��� �������� �������(���������� ����� �������)
      KeyValuePair<System.Int64, uint>[] kvpArray = new KeyValuePair<System.Int64, uint>[series.Count];
      series.CopyTo(kvpArray, 0);

      for (int i = 1; i < kvpArray.Length; i++)
      {
        if (kvpArray[i].Value >= kvpArray[i - 1].Value)
        {
          buffer[i - 1] = kvpArray[i].Value - kvpArray[i - 1].Value;
        }
        else
        {
          // ��������� ������� ����� ��� � ����������������
          buffer[i - 1] = kvpArray[i].Value;
          // ������� ��������� ������ ������� ���������� ��� ������������ ����� �������� 
          // (���������� ����� ��������, ��������� �����)
          if (OverflowOccurred(kvpArray[i - 1], kvpArray[i], sensor.K))
          {
            buffer[i - 1] += (uint)(MAX_SENSOR_BOARD_VALUE * sensor.K - kvpArray[i - 1].Value);
          }
        }
      }

      // ���������� �������������� ����
      series[kvpArray[0].Key] = 0;
      for (int i = 1; i < kvpArray.Length; i++)
      {
        series[kvpArray[i].Key] = series[kvpArray[i - 1].Key] + buffer[i];
        AssertAdjacentValues(series[kvpArray[i - 1].Key], series[kvpArray[i].Key]);
      }
      InspectCollection<uint>(series.Values);
    }

    /// <summary>
    /// ����������� ��������� �� ������������ ����� ��������
    /// </summary>
    /// <param name="previous">���������� �������� �������.</param>
    /// <param name="current">������� �������� �������.</param>
    /// <param name="sensorK">����������� ������� ���������������.</param>
    /// <returns>True - ��������� ������������.</returns>
    private bool OverflowOccurred(KeyValuePair<System.Int64, uint> previous,
      KeyValuePair<System.Int64, uint> current, double sensorK)
    {
      // ������������ ��������� ���-�� ������������ ������� �� �������� ��������� ��������
      double maxValue =
        (GetTimeById(current.Key) - GetTimeById(previous.Key)).TotalSeconds * sensorK * MAX_PASS_PER_SEC;
      // ��������� ��������� ��������
      double assessedValue = MAX_SENSOR_BOARD_VALUE * sensorK - previous.Value + current.Value;
      return maxValue >= assessedValue;
    }

    /// <summary>
    /// ��������� ���� ��������� ��� �������, ����� ����������� � � ������.
    /// </summary>
    /// <typeparam name="T">���.</typeparam>
    /// <param name="collection">���������.</param>
    [Conditional("DEBUG")]
    private void InspectCollection<T>(ICollection<T> collection)
    {
      T[] result = new T[collection.Count];
      collection.CopyTo(result, 0);
    }

    /// <summary>
    /// �������� ����, ��� ������� �������� �� ������ �����������.
    /// ����������� ������ � ����� ������.
    /// </summary>
    /// <param name="previos">���������� ��������.</param>
    /// <param name="current">������� ��������.</param>
    [Conditional("DEBUG")]
    private void AssertAdjacentValues(uint previos, uint current)
    {
      System.Diagnostics.Trace.Assert(current >= previos);
    }

    #region ������� ������������ �� ����������

    /// <summary>
    /// ������� ������������ �� ����������
    /// </summary>
    private void ProbeTriggeringCount()
    {
      if (PassengerSensor1Exists)
      {
        if (DoorSensor1Exists)
        {
          PairedSensorCounting(CorrectValueSeries1, DoorSensor1, true);
        }
        else
        {
          UnpairedSensorCounting(CorrectValueSeries1, true);
        }
      }
      if (PassengerSensor2Exists)
      {
        if (DoorSensor2Exists)
        {
          PairedSensorCounting(CorrectValueSeries2, DoorSensor2, false);
        }
        else
        {
          UnpairedSensorCounting(CorrectValueSeries2, false);
        }
      }
    }

    /// <summary>
    /// ���������� ����������� � SensorValuesOnStops.
    /// ���� ������ � dataGpsId ����������, �� ������ ����������,
    /// ����� - ����������.
    /// </summary>
    /// <param name="dataGpsId">dataGpsId.</param>
    /// <param name="value">���-�� ������������ �������(����-�����).</param>
    /// <param name="time">����� ���������.</param>
    /// <param name="firstSensor">��������� �������� ������� �������?</param>
    private void SavePassCount(System.Int64 dataGpsId, uint value, DateTime time, bool firstSensor)
    {
      if (SensorValuesOnStops.ContainsKey(dataGpsId))
      {
        if (firstSensor)
        {
          SensorValuesOnStops[dataGpsId].Sensor1Value = value;
        }
        else
        {
          SensorValuesOnStops[dataGpsId].Sensor2Value = value;
        }
      }
      else
      {
        SensorValuesOnStop newValues = new SensorValuesOnStop();
        newValues.Time = time;
        if (firstSensor)
        {
          newValues.Sensor1Value = value;
        }
        else
        {
          newValues.Sensor2Value = value;
        }
        SensorValuesOnStops.Add(dataGpsId, newValues);
      }
    }

    /// <summary>
    /// ��������� �� ����� � ��������� ����.
    /// </summary>
    /// <param name="latitude">������.</param>
    /// <param name="longitude">�������.</param>
    /// <returns>True - ����� � ��������� ����.</returns>
    private bool PointInExclusionZones(double latitude, double longitude)
    {
      foreach (ReportPassZone row in _model.RepPassZones)
      {
        IZone zone = ZonesModel.GetById(row.ZoneID);
        if ((zone != null) && (zone.Contains(new PointLatLng(latitude, longitude))))
        {
          return true;
        }
      }
      return false;
    }
    #endregion
  }
}
