using System;
using System.Collections.Generic;
using LocalCache;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ������� ����� ������� ��������� � ����������������.
  /// <para>����� ������� ������ ��������� �������� ������
  /// (����������� ������ �����, ��������� � ������ ��������)
  /// �������� "���������".</para>
  /// </summary>
  partial class BasePassTrafficAlgorithm
  {
    /// <summary>
    /// ������� ������������ �� ���������� ���������� ������� ���������������(��� �����).
    /// ��� ������� ����� ������� � ������� ��������� � �� �������� � ��������� ����.
    /// </summary>
    /// <param name="series">��� �������� �������.</param>
    /// <param name="doorSensor">������ �����.</param>
    /// <param name="firstSensor">���������� �������� ������ ������� ���� �������� -
    /// ������� ��� �������.</param>
      private void PairedSensorCounting(IDictionary<System.Int64, uint> series,
      atlantaDataSet.sensorsRow doorSensor, bool firstSensor)
    {
        KeyValuePair<System.Int64, uint>[] kvpArray = new KeyValuePair<System.Int64, uint>[series.Count];
      series.CopyTo(kvpArray, 0);

      int startIndex = 0;
      int finishIndex = 0;
      int maxIndex = kvpArray.Length - 1;

      while (finishIndex < maxIndex)
      {
        if (LocateDoorOpened(kvpArray, doorSensor, finishIndex, out startIndex))
        {
          if (startIndex == maxIndex)
          {
            return;
          }

          if (!LocateDoorClosed(kvpArray, doorSensor, startIndex, out finishIndex))
          {
            finishIndex = maxIndex;
          }

          if (kvpArray[startIndex].Value < kvpArray[finishIndex].Value)
          {
            SavePassCount(
              kvpArray[finishIndex].Key,
              kvpArray[finishIndex].Value - kvpArray[startIndex].Value,
              GetTimeById(kvpArray[finishIndex].Key),
              firstSensor);
          }
        }
        else
        {
          return;
        }
      }
    }

    /// <summary>
    /// ����� �������� �����.
    /// </summary>
    /// <param name="kvpArray">������ KeyValuePair, 
    /// ��������� �� ���� �������� DataGpsID - Value.</param>
    /// <param name="doorSensor">atlantaDataSet.sensorsRow - ������ "�����".</param>
    /// <param name="start">��������� ������ �������, 
    /// ������� � �������� ����� ������������� �����.</param>
    /// <param name="index">������ �������, ����������� �� ������ ���������.
    /// � ������ ���� ��������� �� �������, �� index ����� ����� kvpArray.Length.</param>
    /// <returns>True - ������� ���������.</returns>
      private bool LocateDoorOpened(KeyValuePair<System.Int64, uint>[] kvpArray,
      atlantaDataSet.sensorsRow doorSensor, int start, out int index)
    {
      atlantaDataSet.dataviewRow startStopPoint;
      index = start;

      for (int i = start; i < kvpArray.Length; i++)
      {
        index = i;
        startStopPoint = AtlantaDataSet.dataview.FindByDataGps_ID(kvpArray[i].Key);

        if ((IsDoorOpened((byte)doorSensor.K, startStopPoint.sensor, doorSensor.StartBit, doorSensor.Length)) &&
          (!PointInExclusionZones(startStopPoint.Lat, startStopPoint.Lon)))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ����� �������� �����.
    /// </summary>
    /// <param name="kvpArray">������ KeyValuePair, 
    /// ��������� �� ���� �������� DataGpsID - Value.</param>
    /// <param name="doorSensor">atlantaDataSet.sensorsRow - ������ "�����".</param>
    /// <param name="start">��������� ������ �������, 
    /// ������� � �������� ����� ������������� �����.</param>
    /// <param name="index">������ �������, ����������� �� ����� ���������.
    /// � ������ ���� �������� �� �������, �� index ����� ����� kvpArray.Length.</param>
    /// <returns>True - ������� �������� ����� ���������.</returns>
      private bool LocateDoorClosed(KeyValuePair<System.Int64, uint>[] kvpArray,
      atlantaDataSet.sensorsRow doorSensor, int start, out int index)
    {
      atlantaDataSet.dataviewRow finishStopPoint;
      index = start;

      for (int i = start; i < kvpArray.Length; i++)
      {
        index = i;
        finishStopPoint = AtlantaDataSet.dataview.FindByDataGps_ID(kvpArray[i].Key);

        if (!IsDoorOpened((byte)doorSensor.K, finishStopPoint.sensor, doorSensor.StartBit, doorSensor.Length))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ����������� ������� �� �����.
    /// </summary>
    /// <param name="openedDoorCriterion">�������� "����� �������" - 0 ��� 1.</param>
    /// <param name="sensorValue">��������� ���� �������� (dataviewRow.sensor).</param>
    /// <param name="sensorStartBit">��������� ��� ������� �����.</param>
    /// <param name="sensorLength">����� ������ ������� �����.</param>
    /// <returns>True - ����� �������.</returns>
    private bool IsDoorOpened(byte openedDoorCriterion, ulong sensorValue,
      int sensorStartBit, int sensorLength)
    {
      // ������� ��������� ������� �����
      byte doorValue = (byte)calibrate.GetUserValue(
        BitConverter.GetBytes(sensorValue), sensorLength, sensorStartBit, 1, 0, false);

      return openedDoorCriterion == doorValue;
    }
  }
}
