using System.Collections.Generic;
using System.ComponentModel;

using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.Zones;
using TrackControl.Zones.Tuning;

namespace ReportsOnPassengerTraffic
{
    /// <summary>
    /// ��������� ������� �� ���������������. 
    /// </summary>
    public class ReportPassZonesModel
    {
        private IZonesManager _zonesManager;
        private IReportPassZonesProvider _provider;

        /// <summary>
        /// ������ �� ��� ��������� �������.
        /// </summary>
        private SortableBindingList<ReportPassCZ> cfgCZ;
        private IList<ReportPassZone> repPassZones;

        /// <summary>
        /// ��� ������� ReportPassZones (������� report_pass_zones).
        /// </summary>
        public IList<ReportPassZone> RepPassZones
        {
            get { return repPassZones; }
        }

        /// <summary>
        /// ������� ��� �����.
        /// </summary>
        public BindingList<ReportPassCZ> BindingData
        {
            get { return cfgCZ; }
        }

        /// <summary>
        /// ���� �� ���������. 
        /// </summary>
        public bool ChangesOccurred
        {
            get
            {
                foreach (ReportPassCZ cz in cfgCZ)
                {
                    if (cz.IsChanged)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private bool getStatePassZone(int idZone)
        {
            foreach( ReportPassCZ cz in cfgCZ )
            {
                if( cz.ZoneID == idZone )
                {
                    return  cz.Selected;
                }
            }

            return false;
        }

        private void SetZonesForPass(int idZones, bool state)
        {
            foreach (ReportPassCZ cz in cfgCZ)
            {
                if (cz.ZoneID == idZones)
                {
                    cz.IsChanged = true;
                    cz.Selected = state;
                    break;
                }
            }

            AcceptChanges();
        }

        public ReportPassZonesModel(IZonesManager zonesManager, IReportPassZonesProvider provider)
        {
            _zonesManager = zonesManager;
            _provider = provider;
            cfgCZ = new SortableBindingList<ReportPassCZ>();
            repPassZones = new List<ReportPassZone>();
            FillCfgCollection();
            _zonesManager.ZonesAdded += zonesModel_ZonesAdded;
            _zonesManager.ZonesRemoved += zonesModel_ZonesRemoved;
            ZonesTuningMode.SetingsZonesPassager += SetZonesForPass;
            TuningZoneView.statePassagerZone += getStatePassZone;
        }
        
        /// <summary>
        /// ���������� ��������� �������.
        /// </summary>
        private void FillCfgCollection()
        {
            ReportPassCZ passZone;
            int reportPassZonesId;

            Clear();

            repPassZones = _provider.GetAll();

            foreach (IZone zone in _zonesManager.Root.AllItems)
            {
                passZone = new ReportPassCZ(zone);
                passZone.Selected = ZoneInReportPassZones(zone.Id, out reportPassZonesId);
                passZone.ReportPassZonesId = reportPassZonesId;
                Add(passZone);
            }
        }

        /// <summary>
        /// �������� ���������� �� �� � ������� report_pass_zones ��� ���.
        /// </summary>
        /// <param name="zoneId">������������� ����������� ����.</param>
        /// <param name="reportPassZonesId">������������� ReportPassZones � ������ ���������
        /// ������ ��� -1 � ������ ���������� ������.</param>
        /// <returns>True - ���� ������� � ���������� � ������� report_pass_zones.</returns>
        private bool ZoneInReportPassZones(int zoneId, out int reportPassZonesId)
        {
            foreach (ReportPassZone r in repPassZones)
            {
                if (r.ZoneID == zoneId)
                {
                    reportPassZonesId = r.Id;
                    return true;
                }
            }
            reportPassZonesId = -1;
            return false;
        }

        /// <summary>
        /// �������� ������� � ���������.
        /// </summary>
        /// <param name="item">PassZone</param>
        public void Add(ReportPassCZ item)
        {
            cfgCZ.Add(item);
        }

        /// <summary>
        /// �������� ���.
        /// </summary>
        public void Clear()
        {
            cfgCZ.Clear();
            repPassZones.Clear();
        }

        /// <summary>
        /// ������ ���������.
        /// </summary>
        public void RejectChanges()
        {
            foreach (ReportPassCZ cz in cfgCZ)
            {
                if (cz.IsChanged)
                {
                    cz.RejectChanges();
                }
            }
        }

        /// <summary>
        /// ������������� ���������.
        /// </summary>
        public void AcceptChanges()
        {
            foreach (ReportPassCZ cz in cfgCZ)
            {
                if (!cz.IsChanged)
                {
                    continue;
                }

                if (cz.Selected)
                {
                    // ������� ������ � report_pass_zones
                    ReportPassZone rPassZone = new ReportPassZone(-1, cz.ZoneID);
                    if (_provider.Save(rPassZone))
                    {
                        cz.ReportPassZonesId = rPassZone.Id;
                        repPassZones.Add(rPassZone);
                    }
                }
                else
                {
                    // ������ ������ �� report_pass_zones
                    if (_provider.Delete(
                        new ReportPassZone(cz.ReportPassZonesId, cz.ZoneID)))
                    {
                        RemoveFromRepPassZonesById(cz.ReportPassZonesId);
                    }
                }
                cz.AcceptChanges();
            }
        }

        /// <summary>
        /// ������� ���������� ���������.
        /// </summary>
        /// <returns>������ ���������� ���������.</returns>
        public List<ReportPassCZ> GetChanges()
        {
            List<ReportPassCZ> result = new List<ReportPassCZ>();
            foreach (ReportPassCZ cz in cfgCZ)
            {
                if (cz.IsChanged)
                {
                    result.Add(cz);
                }
            }
            return result;
        }

        #region ��������� ������� ��������� ������ ����������� ���

        /// <summary>
        /// ���������� ������� ���������� ����� ���
        /// </summary>
        private void zonesModel_ZonesAdded(IEnumerable<IZone> zones)
        {
            foreach (IZone zone in zones)
            {
                ReportPassCZ passZone = new ReportPassCZ(zone);
                passZone.Selected = false;
                passZone.ReportPassZonesId = -1;
                Add(passZone);
            }
        }

        /// <summary>
        /// ���������� ������� �������� ���� ������.
        /// ������� ������ �� ���������, � �� �� �����,
        /// �.�. ���������� Foreign Key � ������� zones 
        /// � ��������� ���������.
        /// </summary>
        private void zonesModel_ZonesRemoved(IEnumerable<IZone> zones)
        {
            foreach (IZone zone in zones)
            {
                RemoveZones(zone);
            }
        }

        /// <summary>
        /// �������� ��� �� ���������.
        /// </summary>
        private void RemoveZones(IZone zone)
        {
            for (int i = cfgCZ.Count - 1; i >= 0; i--)
            {
                if (zone.Id == cfgCZ[i].ZoneID)
                {
                    cfgCZ.RemoveAt(i);
                    RemoveFromRepPassZonesByZoneId(zone.Id);
                    break;
                }
            }
        }

        #endregion

        #region �������� �� ��������� repPassZones

        /// <summary>
        /// �������� ������� ReportPassZones �� ��������� repPassZones
        /// ��������� ��� ������ ������������� ����.
        /// </summary>
        /// <param name="zoneId">������������� ��.</param>
        private void RemoveFromRepPassZonesByZoneId(int zoneId)
        {
            for (int i = 0; i < repPassZones.Count; i++)
            {
                if (repPassZones[i].ZoneID == zoneId)
                {
                    repPassZones.RemoveAt(i);
                    break;
                }
            }
        }

        /// <summary>
        /// �������� ������� ReportPassZones �� ��������� repPassZones
        /// ��������� ��� ������ ������������� �������.
        /// </summary>
        /// <param name="id">������������� ������� ReportPassZones.</param>
        private void RemoveFromRepPassZonesById(int id)
        {
            for (int i = 0; i < repPassZones.Count; i++)
            {
                if (repPassZones[i].Id == id)
                {
                    repPassZones.RemoveAt(i);
                    break;
                }
            }
        }

        #endregion
    }
}
