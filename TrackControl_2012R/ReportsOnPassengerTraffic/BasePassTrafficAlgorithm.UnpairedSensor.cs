using System.Collections.Generic;
using LocalCache;

namespace ReportsOnPassengerTraffic
{
  /// <summary>
  /// ������� ����� ������� ��������� � ����������������.
  /// <para>����� ������� ������ ��������� �������� ���������
  /// (���������� ������ �����, ��������� � ������ ��������)
  /// �������� "���������".</para>
  /// </summary>
  partial class BasePassTrafficAlgorithm
  {
    /// <summary>
    /// ������� ������������ �� ���������� ���������� ������� ���������������(��� �����).
    /// ��� ������� ����� ������� � ������� ��������� � �� �������� � ��������� ����.
    /// </summary>
    /// <param name="series">��� �������� �������.</param>
    /// <param name="firstSensor">���������� �������� ������ ������� ���� �������� -
    /// ������� ��� �������.</param>
      private void UnpairedSensorCounting(IDictionary<System.Int64, uint> series, bool firstSensor)
    {
        KeyValuePair<System.Int64, uint>[] kvpArray = new KeyValuePair<System.Int64, uint>[series.Count];
      series.CopyTo(kvpArray, 0);

      int startIndex = 0;
      int finishIndex = 0;
      int maxIndex = kvpArray.Length - 1;

      while (finishIndex < maxIndex)
      {
        if (LocateStoppingStart(kvpArray, finishIndex, out startIndex))//������� ����� � ������� ���������, ����� ����������� ��
        {
          if (startIndex == maxIndex)
          {
            return;
          }

          if (LocateStoppingFinish(kvpArray, startIndex, out finishIndex))
          {
            //stepCount = 0;
            //while ((stepCount < 3) && (finishIndex < maxIndex) &&
            //  (kvpArray[startIndex].Value == kvpArray[finishIndex].Value) &&
            //  (IsMoving(atlantaDataSet.dataview.FindByDataGps_ID(kvpArray[finishIndex].Key).speed)))
            //{
            //  stepCount++;
            //  finishIndex++;
            //}
          }
          else
          {
            finishIndex = maxIndex;
          }

          uint startValue = kvpArray[startIndex].Value;
          if (startIndex > 0)
          {
            startValue = kvpArray[startIndex - 1].Value;
          }

          if (startValue < kvpArray[finishIndex].Value)
          {
            SavePassCount(
              kvpArray[startIndex].Key,//finishIndex].Key,
              kvpArray[finishIndex].Value - startValue,
              GetTimeById(kvpArray[startIndex].Key),//kvpArray[finishIndex].Key),
              firstSensor);
          }
        }
        else
        {
          return;
        }
      }
    }

    /// <summary>
    /// ����� ������ ���������, ������� ������� ����������������.
    /// </summary>
    /// <param name="kvpArray">������ KeyValuePair, 
    /// ��������� �� ���� �������� DataGpsID - Value.</param>
    /// <param name="start">��������� ������ �������, 
    /// ������� � �������� ����� ������������� �����.</param>
    /// <param name="index">������ �������, ����������� �� ������ ���������.
    /// � ������ ���� ��������� �� �������, �� index ����� ����� kvpArray.Length.</param>
    /// <returns>True - ������� ���������.</returns>
      private bool LocateStoppingStart(KeyValuePair<System.Int64, uint>[] kvpArray,
      int start, out int index)
    {
      atlantaDataSet.dataviewRow startStopPoint;
      index = start;
      for (int i = start; i < kvpArray.Length; i++)
      {
        index = i;
        startStopPoint = AtlantaDataSet.dataview.FindByDataGps_ID(kvpArray[i].Key);
        if ((!IsMoving(startStopPoint.speed)) &&
          (!PointInExclusionZones(startStopPoint.Lat, startStopPoint.Lon)))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ����� �������� ����� ��������� - ������ ��������.
    /// </summary>
    /// <param name="kvpArray">������ KeyValuePair, 
    /// ��������� �� ���� �������� DataGpsID - Value.</param>
    /// <param name="start">��������� ������ �������, 
    /// ������� � �������� ����� ������������� �����.</param>
    /// <param name="index">������ �������, ����������� �� ����� ���������.
    /// � ������ ���� �������� �� �������, �� index ����� ����� kvpArray.Length.</param>
    /// <returns>True - ������� �������� ����� ���������.</returns>
      private bool LocateStoppingFinish(KeyValuePair<System.Int64, uint>[] kvpArray,
      int start, out int index)
    {
      atlantaDataSet.dataviewRow finishStopPoint;
      int dayStart = AtlantaDataSet.dataview.FindByDataGps_ID(kvpArray[start].Key).time.DayOfYear;
      index = start;
      for (int i = start; i < kvpArray.Length; i++)
      {
        index = i;
        finishStopPoint = AtlantaDataSet.dataview.FindByDataGps_ID(kvpArray[i].Key);
        if (IsMoving(finishStopPoint.speed)
          || IsNextDayAfterStart(dayStart, finishStopPoint.time.DayOfYear))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ������� �� ������� �������� �������� � ���,
    /// ��� ������������ �������� ��������� � ��������.
    /// </summary>
    /// <param name="speed">����������� ��������.</param>
    /// <returns>True - ��������.</returns>
    private bool IsMoving(double speed)
    {
      return speed > 0.1;
    }

    /// <summary>
    /// ����� �������� �� ������ � ��������� �����
    /// </summary>
    /// <returns>True - ������ � ������ �����.</returns>
    private bool IsNextDayAfterStart(int dateStart, int dateFinish)
    {
      return (dateFinish > dateStart);
    }
  }
}
