﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Diagnostics;
using TrackControl.Zones;

namespace TrackControl.SQLite
{
    public static class SQLiteProvider
    {
    const string sqlCreateZoneTable = @"CREATE TABLE IF NOT EXISTS zones (
      Zone_ID INTEGER NOT NULL PRIMARY KEY
    , Name CHAR(50) NOT NULL
    , ZonesGroupId int NOT NULL
    , Geometry multipolygon)";

        //const string sqlInsertZone = "INSERT INTO zones (Zone_ID,Name,ZonesGroupId, Geometry) VALUES ({0},'{1}',{2}, GeomFromText('{3}', 32632))";
    const string sqlInsertZone = "INSERT INTO zones (Zone_ID,Name,ZonesGroupId, Geometry) VALUES ({0},'{1}',{2}, GeomFromText('{3}',4326))";//,32635,32636
    const string sqlSetSpatialite = "SELECT load_extension('libspatialite-4.dll')";
    const string sqlGetZoneAreaDemensionCentriod = "SELECT ST_Area(Geometry) as Area, AsText(Centroid(Geometry)) as Centroid FROM zones Where Zone_ID = {0}";
    const string sqlGetZoneArea = "SELECT ST_Area(ST_Transform(Geometry, 900913)) as Area  FROM zones Where Zone_ID = {0}";

        //, PtDistWithin(Geometry) as Dimension

        public static bool CreateEmptyDB(string file)
        {
            bool ret = true;

            try
            {
                string dir = Path.GetDirectoryName(file);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                using (SQLiteConnection cn = new SQLiteConnection())
                {

                    cn.ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=False;", file);

                    cn.Open();
                    {
                        using (DbTransaction tr = cn.BeginTransaction())
                        {
                            try
                            {
                                using (DbCommand cmd = cn.CreateCommand())
                                {

                                    cmd.CommandText = sqlCreateZoneTable;

                                    cmd.ExecuteNonQuery();
                                }
                                tr.Commit();
                            }
                            catch
                            {
                                tr.Rollback();
                                ret = false;
                            }
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("CreateEmptyDB: {0}", ex));
                ret = false;
            }
            return ret;
        }

        public static bool LoadSpatiaLite(string file)
        {
            string sql = sqlSetSpatialite;
            return RunExecuteNonQueryCommand(file, sql);
        }

        public static bool InsertZone(string file, Zone zone, string poligon)
        {
            string sql = string.Format(sqlInsertZone, zone.Id, zone.Name, zone.Group.Id, poligon); ;
            return RunExecuteNonQueryCommand(file, sql);
        }

        private static bool RunExecuteNonQueryCommand(string file, string sql)
        {
            bool ret = true;

            try
            {

                using (SQLiteConnection cn = new SQLiteConnection())
                {

                    cn.ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=False;", file);

                    cn.Open();
                    {
                        using (DbTransaction tr = cn.BeginTransaction())
                        {
                            try
                            {
                                using (DbCommand cmd = cn.CreateCommand())
                                {

                                    cmd.CommandText = sqlSetSpatialite;
                                    cmd.ExecuteScalar();
                                    cmd.CommandText = sql;
                                    cmd.ExecuteNonQuery();
                                }
                                tr.Commit();
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(String.Format("RunExecuteNonQueryCommand: {0}", ex));
                                tr.Rollback();
                                ret = false;
                            }
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("RunExecuteNonQueryCommand: {0}", ex));
                ret = false;
            }
            return ret;
        }

        public static object GetPoligon(string file,int idZone)
        {
            object poligon = null ;
            try
            {
                
                using (SQLiteConnection cn = new SQLiteConnection())
                {

                    cn.ConnectionString = string.Format("Data Source=\"{0}\";FailIfMissing=False;", file);

                    cn.Open();
                    {
                        using (DbTransaction tr = cn.BeginTransaction())
                        {
                            try
                            {
                                using (DbCommand cmd = cn.CreateCommand())
                                {

                                    cmd.CommandText = sqlSetSpatialite;
                                    cmd.ExecuteScalar();
                                    cmd.CommandText = string.Format("SELECT Zone_ID, AsText(Geometry) as Ponts FROM zones WHERE Zone_ID ={0}",idZone);
                                    DbDataReader dreader = cmd.ExecuteReader();
                                    if (dreader.Read())
                                    {
                                        //int zone = dreader.GetInt32(dreader.GetOrdinal("Zone_ID"));
                                        //string ponts =dreader.GetString(dreader.GetOrdinal("Ponts"));
                                        if (!dreader.IsDBNull(dreader.GetOrdinal("Ponts")))
                                        {
                                            poligon = dreader.GetValue(dreader.GetOrdinal("Ponts"));
                                        }
                                        
                                    }
                                }
                                tr.Commit();
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(String.Format("GetPoligon: {0}", ex));
                                tr.Rollback();
                            }
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("GetPoligon: {0}", ex));
            }
            return poligon;
        }

        public static void GetZoneAreaDemensionCentriod(string file, int idZone, out double area, out double demension, out string centriod)
        {
            area=0;
            demension = 0;
            centriod = "";
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + file;
                connection.Open();
                string sql = string.Format(sqlGetZoneAreaDemensionCentriod, idZone);
                SQLiteDataReader reader = GetDataReader(sql,connection);
                if (reader.Read())
                {
                    area = reader.GetDouble(reader.GetOrdinal("Area"));
                    //demension = reader.GetDouble(reader.GetOrdinal("Dimension"));
                    centriod = reader.GetString(reader.GetOrdinal("Centroid"));
                }
                reader.Close(); 
                connection.Close();
            }

        }

        public static double GetZoneArea(string file, int idZone)
        {
            double area=0;
             SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + file;
                connection.Open();
                 //SELECT AsText(geom), Srid(geom)
                //string sql = string.Format("SELECT Srid(Geometry) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = string.Format("SELECT AsText(Geometry) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = string.Format("SELECT AsText(ST_Transform(Geometry,4326)) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = string.Format("SELECT AsText(Transform(Geometry,4326)) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = string.Format("SELECT ST_Area(Transform(Geometry,4326)) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = string.Format("SELECT ST_Area(SetSRID(Geometry,900913)) as Area  FROM zones Where Zone_ID = {0}", idZone);
                string sql = string.Format("SELECT ST_Area(Geometry) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = "SELECT ST_AREA(ST_Transform(ST_GeomFromText('POLYGON((871325.790874952 6105405.3261047,871418.748307692 6105359.72944624,871346.22022442 6105215.141258,871254.85408906 6105261.72007212,871325.790874952 6105405.3261047))',900913),31467)) As sqm";
                //string sql = string.Format("SELECT AsText(Geometry) as Area  FROM zones Where Zone_ID = {0}", idZone);
                //string sql = "SELECT ST_Length(ST_Transform(ST_GeomFromText('POLYGON(100.495995129 13.7117836894,100.485962221169 13.7117761471941,100.475962221169 13.7117761471941,100.495995129 13.7117836894)',4326),32647))";
                SQLiteDataReader reader = GetDataReader(sql,connection);
                if (reader.Read())
                {
                    object oarea = reader.GetValue(reader.GetOrdinal("Area"));
                }
                reader.Close(); 
                connection.Close();
            }
            return area;
        }

        public static bool IsGeomContainPoint(string file, string point)
        {
            //double area = 0;
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + file;
                connection.Open();
                string sql = string.Format("SELECT Zone_ID,Name  FROM zones Where Contains(Geometry,GeomFromText('{0}'))", point);

                SQLiteDataReader reader = GetDataReader(sql, connection);
                while (reader.Read())
                {
                    Debug.Print(string.Format("id {0} name {1}", reader.GetValue(reader.GetOrdinal("Zone_ID")), reader.GetValue(reader.GetOrdinal("Name"))));
                }
                reader.Close();
                connection.Close();
            }
            return true;
        }

        public static List<int> GetZonesListWithPoint(string file, string point)
        {
            List<int> zonesWithPoint = new List<int>(); 
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + file;
                connection.Open();
                string sql = string.Format("SELECT Zone_ID  FROM zones Where MBRContains(Geometry,GeomFromText('{0}'))", point);

                SQLiteDataReader reader = GetDataReader(sql, connection);
                while (reader.Read())
                {
                    zonesWithPoint.Add(reader.GetInt32(reader.GetOrdinal("Zone_ID")));
                    //Debug.Print(string.Format("id {0} name {1}", reader.GetValue(reader.GetOrdinal("Zone_ID")), reader.GetValue(reader.GetOrdinal("Name"))));
                }
                reader.Close();
                connection.Close();
            }
            return zonesWithPoint;
        }

        public static SQLiteDataReader GetDataReader(string sSQL, SQLiteConnection cnn)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sqlSetSpatialite;
            cmd.ExecuteScalar();
            cmd.CommandText = sSQL;
            return cmd.ExecuteReader();
        }

        static void CreateEmptyDBFactory()
        {
            string baseName = "CompanyWorkers.db3";

            SQLiteConnection.CreateFile(baseName);

            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = "Data Source = " + baseName;
                connection.Open();

                using (SQLiteCommand command = new SQLiteCommand(connection))
                {
                    command.CommandText = @"CREATE TABLE [workers] (
                    [id] integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    [name] char(100) NOT NULL,
                    [family] char(100) NOT NULL,
                    [age] int NOT NULL,
                    [profession] char(100) NOT NULL
                    );";
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
