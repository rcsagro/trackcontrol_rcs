﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TrackControl.Zones;
using TrackControl.General;
using System.Diagnostics; 


namespace TrackControl.SQLite
{
    public class ZonesController
    {
        
        readonly string _pathDb ;
        IZonesManager _zonesModel;
        public ZonesController()
        {
            //_pathDb =string.Format("{0}{1}Zones{1}Zones.sqllite", Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) 
            //    , Path.DirectorySeparatorChar);
            _pathDb = "D:\\!!RCS\\SpatialLite\\Zones.sqllite";
            //_pathDb = "D:\\!!RCS\\SpatialLite\\test-2.3\\test-2.3.sqlite";
        }

        public bool TestSpatialLite(IZonesManager zonesModel)
        {
            //SQLiteProvider.CreateEmptyDB(_pathDb); 
            _zonesModel = zonesModel;
            //CompareZonesParameters();
            //SQLiteProvider.SelectTown(_pathDb);
            //FillZoneTable();
            //GetZonePoints();
            TestContains();
            return true;
        }

        void FillZoneTable()
        {
            int cnt = 0;
            foreach (Zone z in _zonesModel.Zones )
            {
                string poligon ="";
                PointLatLng first = new PointLatLng(0,0);
                
                foreach (PointLatLng  pt in z.Points )
                {
                    string pLat = pt.Lat.ToString().Replace(",", ".");
                    string pLng = pt.Lng.ToString().Replace(",", ".");
                    if (first.Lat == 0 && first.Lng == 0)
                    {
                        first = pt;
                        poligon = string.Format("POLYGON(({0} {1}", pLat, pLng);
                    }
                    else
                    {
                        poligon = string.Format("{0},{1} {2}", poligon, pLat, pLng);
                    }
                }

                poligon = string.Format("{0},{1} {2}))", poligon, first.Lat.ToString().Replace(",", "."), first.Lng.ToString().Replace(",", "."));
                //poligon = "POLYGON((554000 4692000, 770000 4692000,770000 4925000, 554000 4925000, 554000 4692000))";
                //poligon = "POLYGON((49.846738 30.55101,49.846737 30.551012,49.846737 30.551012,49.84671 30.551025,49.84671 30.551027,49.846708 30.551027,49.846738 30.55101))";
                cnt++;
                if (SQLiteProvider.InsertZone(_pathDb, z, poligon))
                {
                    //object pol = SQLiteProvider.GetPoligon(_pathDb, z.Id);
                    //if (pol==null) mess
                }
                
            } 
        }

        void GetZonePoints()
        {
            foreach (Zone z in _zonesModel.Zones)
            {
                SQLiteProvider.GetPoligon(_pathDb, z.Id);
            }
        }

        void CompareZonesParameters()
        {
            
            foreach (Zone z in _zonesModel.Zones)
            {
                double area = SQLiteProvider.GetZoneArea(_pathDb, z.Id);
                //double demension = 0;
                //string centriod = "";
                //SQLiteProvider.GetZoneAreaDemensionCentriod(_pathDb, z.Id, out area, out demension,out centriod);
                //area = area * 1000;
                if (z.AreaKm != area)
                {
                    Debug.Print("NC {0} {1} {2}", z.AreaGa, area, z.Name); 
                }
                else
                {
                    Debug.Print("C {0}", z.Name ); 
                }
            }
        }

        void TestContains()
        {
            //string point = "POINT(49.9586 27.9694)";
            string point = "POINT(49.960 27.9695)";
            SQLiteProvider.IsGeomContainPoint(_pathDb, point);
        }

        public List<int> GetZonesWithPoint(PointLatLng point)
        {
            string pointSpatialLite = string.Format("POINT({0} {1})", point.Lat.ToString().Replace(",", "."), point.Lng.ToString().Replace(",", "."));
            return SQLiteProvider.GetZonesListWithPoint(_pathDb, pointSpatialLite);
        }
    }
}
