﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using BaseReports.Procedure;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;

namespace BaseReports
{
    public static class MobitelsRotateBandsProvider
    {
        public static bool UpdateRecord (int boundId,int boundValue)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                db.ConnectDb();
                {
                    string _sql = String.Format("UPDATE mobitels_rotate_bands SET Bound = {0} WHERE Id = {1}", boundValue, boundId);
                    //_cnn.ExecuteNonQueryCommand(_sql); 
                    db.ExecuteNonQueryCommand(_sql);
                }
                db.CloseDbConnection();
                return true;
            }
            catch
            {
                db.CloseDbConnection();
                return false;
            }
        }

        public static bool InsertRecords(int mobitelId,int[] boundValues)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                
                db.ConnectDb();
                {
                    Array.ForEach(boundValues, bound =>
                    {
                        string _sql = String.Format("INSERT INTO mobitels_rotate_bands (Mobitel_ID,Bound)) Values({0},{1})", mobitelId, bound);
                        //_cnn.ExecuteNonQueryCommand(_sql);
                        db.ExecuteNonQueryCommand(_sql);
                    });
                }
                db.CloseDbConnection();
                return true;
            }
            catch 
            {
                db.CloseDbConnection();
                return false;
            }
        }

        public static int InsertRecord(int mobitelId, int boundValue)
        {
            int id_inserted = -1;
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                
                db.ConnectDb();
                {
                    string _sql = String.Format("INSERT INTO mobitels_rotate_bands (Mobitel_ID,Bound) Values({0},{1})", mobitelId, boundValue);
                    id_inserted = db.ExecuteReturnLastInsert(_sql);
                }
                db.CloseDbConnection();
            }
            catch 
            {
                db.CloseDbConnection();
            }
            return id_inserted;
        }

        public static List<BoundRow> SelectRecords(int mobitelId)
        {

            List<BoundRow> brows = new List<BoundRow>();
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                
                db.ConnectDb();
                {
                     
                    string _sql = String.Format("SELECT Id,Bound FROM mobitels_rotate_bands WHERE Mobitel_ID = {0} ORDER BY Bound", mobitelId);
                    //MySqlDataReader dread = _cnn.GetDataReader(_sql);
                    db.GetDataReader(_sql);
                    while (db.Read())
                    {
                        //sr.Add(dread.GetInt32("Bound"));
                        BoundRow brow = new BoundRow(db.GetInt32("Id"), db.GetInt32("Bound"));
                        brows.Add(brow); 
                    }
                }
                db.CloseDbConnection();
                return brows;
            }
            catch 
            {
                db.CloseDbConnection();
                return null;
            }
        }

        public static bool BoundExist(int mobitelId, int boundValue)
        {
            int id_search = -1;
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                
                db.ConnectDb();
                {
                    string _sql = String.Format("SELECT Id FROM mobitels_rotate_bands WHERE Mobitel_ID = {0} and Bound = {1}", mobitelId, boundValue);
                    id_search = db.GetScalarValueNull<int>(_sql,0);
                }
                db.CloseDbConnection();
            }
            catch 
            {
                db.CloseDbConnection();
                return false;
            }
            if (id_search > 0)
                return true;
            else
            return false;
        }

        public static bool DeleteRecord(int id)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL _cnn = new ConnectMySQL())
                
                db.ConnectDb();
                {
                    string _sql = String.Format("DELETE FROM mobitels_rotate_bands WHERE Id = {0}", id);
                    db.ExecuteNonQueryCommand (_sql);
                    return true;
                }
                //db.CloseDbConnection();
            }
            catch 
            {
                db.CloseDbConnection();
                return false;
            }
        }
    }
}
