﻿using TrackControl.General;
using MySql.Data.MySqlClient;
using BaseReports.Procedure.Calibration;
using TrackControl.General.DatabaseDriver;

namespace BaseReports
{
    public static class CalibrationProvider
    {
        public static void SetCalibration(int id_sensor, ref Calibrate calibrate)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            {
                db.ConnectDb();
                string sSQL = string.Format("SELECT * FROM sensorcoefficient WHERE Sensor_id = {0}", id_sensor);
                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSQL);
                while ( /*dr.Read()*/ db.Read())
                {
                    Coefficient coefficient = new Coefficient();
                    coefficient.User = db.GetDouble("UserValue");
                    coefficient.K = db.GetDouble("K");
                    coefficient.b = db.GetDouble("b");
                    calibrate.Collection.Add(db.GetDouble("SensorValue"), coefficient);
                }
                db.CloseDataReader();
            }
            db.CloseDbConnection();
        }

        public static int InsertAgro(long agregatId,CalibrationRecord calibRecord)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(@"INSERT INTO agro_sensorkoefficients (UserValue, SensorValue, K, b, AgregatID)
                VALUES ({0}UserValue, {0}SensorValue, {0}K, {0}b, {0}AgregatID)",driverDb.ParamPrefics);
                driverDb.NewSqlParameterArray(5);
                CreateParameters(calibRecord, driverDb);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "AgregatID", agregatId);
                return driverDb.ExecuteReturnLastInsert(sql, driverDb.GetSqlParameterArray, "agro_sensorkoefficients");
            }
        }

        private static void CreateParameters(CalibrationRecord calibRecord, DriverDb driverDb)
        {
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "UserValue", calibRecord.UserValue);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "SensorValue", calibRecord.SensorValue);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "K", calibRecord.K);
            driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "b", calibRecord.b);
        }

        public static bool UpdateAgro(CalibrationRecord calibRecord)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(@"UPDATE agro_sensorkoefficients Set UserValue = {0}UserValue, SensorValue  ={0}SensorValue,
                K = {0}K, b = {0}b WHERE agro_sensorkoefficients.Id = {1}", driverDb.ParamPrefics, calibRecord.Id);
                driverDb.NewSqlParameterArray(4);
                CreateParameters(calibRecord, driverDb);
                driverDb.ExecuteNonQueryCommand(sql, driverDb.GetSqlParameterArray);
                return true;
            }
        }

        public static bool DeleteAgro(CalibrationRecord calibRecord)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format(@"DELETE FROM agro_sensorkoefficients WHERE agro_sensorkoefficients.Id = {0}", calibRecord.Id);
                driverDb.ExecuteNonQueryCommand(sql);
                return true;
            }
        }

    }

}
