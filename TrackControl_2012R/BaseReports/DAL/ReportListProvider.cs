﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Text;
using BaseReports.ReportListSetting;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace BaseReports.DAL
{
    public static class ReportListProvider
    {
        public static List<ReportSetting> GetReportsForRoleSettings()
        {
            List<ReportSetting> settings = new List<ReportSetting>();
           // using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = "SELECT * FROM users_reports_list";
                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSQL);
                while (db.Read())
                {
                    
                    ReportSetting setting = new ReportSetting(db.GetInt32("Id"), db.GetString("ReportType"), db.GetString("ReportCaption"));
                    if (!UserBaseCurrent.Instance.IsReportVisibleForRole(db.GetInt32("Id"))) setting.Visible = false;
                    settings.Add(setting); 
                }
            }

            db.CloseDbConnection();
            return settings;
        }

        public static void SaveReportList(List<ReportSetting> settings)
        {
            foreach (ReportSetting setting in settings)
            {
                if (setting.Id == 0)
                {
                    //using (ConnectMySQL cn = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQL = "INSERT INTO users_reports_list (ReportType,ReportCaption) VALUES (" + db.ParamPrefics + "ReportType," + db.ParamPrefics + "ReportCaption)";

                        //MySqlParameter[] parSql = new MySqlParameter[2]; 
                        db.NewSqlParameterArray(2);
                        //parSql[0] = new MySqlParameter("?ReportType", setting.TypeName);
                        db.NewSqlParameter( db.ParamPrefics + "ReportType", setting.TypeName, 0);
                        //parSql[1] = new MySqlParameter("?ReportCaption", setting.Caption);
                        db.NewSqlParameter( db.ParamPrefics + "ReportCaption", setting.Caption, 1);
                        setting.Id = db.ExecuteReturnLastInsert( sSQL, db.GetSqlParameterArray, "users_reports_list" ); 
                    }
                    db.CloseDbConnection();
                }
            }
        }
    }
}
