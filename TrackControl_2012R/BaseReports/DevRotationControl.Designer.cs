﻿namespace BaseReports
{
    partial class DevRotationControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevRotationControl ) );
            DevExpress.XtraCharts.SimpleDiagram simpleDiagram1 = new DevExpress.XtraCharts.SimpleDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel1 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions1 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView1 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            this.chartRotate = new DevExpress.XtraCharts.ChartControl();
            this.bmScale = new DevExpress.XtraBars.BarManager( this.components );
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.totalEngineOnLabel = new DevExpress.XtraBars.BarStaticItem();
            this.totalEngineOffLabel = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.ButtonDelete = new DevExpress.XtraBars.BarButtonItem();
            this.ButtonAdd = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.mnuAdd = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDelete = new DevExpress.XtraBars.BarButtonItem();
            this.mnuScale = new DevExpress.XtraBars.PopupMenu( this.components );
            this.rotateReportBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.imageCollection = new DevExpress.Utils.ImageCollection( this.components );
            this.reportGridPanel = new DevExpress.XtraEditors.PanelControl();
            this.gcRotation = new DevExpress.XtraGrid.GridControl();
            this.gvRotation = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_rot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.State = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Location = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InitialTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FinalTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Interval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BreakTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.motorTimePanel = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcScale = new DevExpress.XtraGrid.GridControl();
            this.gvScale = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Scale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRotate = new DevExpress.XtraGrid.GridControl();
            this.gvRotate = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeRot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceRot = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PercentRot = new DevExpress.XtraGrid.Columns.GridColumn();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.chartRotate ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( series1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesLabel1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesView1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesLabel2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.bmScale ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.mnuScale ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.rotateReportBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.imageCollection ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.reportGridPanel ) ).BeginInit();
            this.reportGridPanel.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcRotation ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvRotation ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.motorTimePanel ) ).BeginInit();
            this.motorTimePanel.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.splitContainerControl1 ) ).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.splitContainerControl2 ) ).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcScale ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvScale ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcRotate ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvRotate ) ).BeginInit();
            this.SuspendLayout();
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer ) ( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins( 10, 10, 70, 10 );
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins( 10, 10, 15, 10 );
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // chartRotate
            // 
            simpleDiagram1.LabelsResolveOverlappingMinIndent = 1;
            this.chartRotate.Diagram = simpleDiagram1;
            this.chartRotate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartRotate.Location = new System.Drawing.Point( 0, 0 );
            this.chartRotate.Name = "chartRotate";
            series1.ArgumentDataMember = "NumberTo";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            pieSeriesLabel1.LineVisible = true;
            pieSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            pieSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            series1.Label = pieSeriesLabel1;
            piePointOptions1.PercentOptions.ValueAsPercent = false;
            piePointOptions1.PointView = DevExpress.XtraCharts.PointView.Argument;
            series1.LegendPointOptions = piePointOptions1;
            series1.Name = "Percents";
            piePointOptions2.Pattern = "{V}%";
            piePointOptions2.PercentOptions.ValueAsPercent = false;
            series1.PointOptions = piePointOptions2;
            series1.SynchronizePointOptions = false;
            series1.ValueDataMembersSerializable = "PercentRotation";
            pieSeriesView1.RuntimeExploding = false;
            series1.View = pieSeriesView1;
            this.chartRotate.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pieSeriesLabel2.LineVisible = true;
            this.chartRotate.SeriesTemplate.Label = pieSeriesLabel2;
            pieSeriesView2.RuntimeExploding = false;
            this.chartRotate.SeriesTemplate.View = pieSeriesView2;
            this.chartRotate.Size = new System.Drawing.Size( 213, 215 );
            this.chartRotate.TabIndex = 0;
            // 
            // bmScale
            // 
            this.bmScale.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar1} );
            this.bmScale.DockControls.Add( this.barDockControl1 );
            this.bmScale.DockControls.Add( this.barDockControl2 );
            this.bmScale.DockControls.Add( this.barDockControl3 );
            this.bmScale.DockControls.Add( this.barDockControl4 );
            this.bmScale.DockControls.Add( this.standaloneBarDockControl1 );
            this.bmScale.Form = this;
            this.bmScale.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this.totalEngineOnLabel,
            this.totalEngineOffLabel,
            this.ButtonDelete,
            this.ButtonAdd,
            this.mnuAdd,
            this.mnuDelete} );
            this.bmScale.MaxItemId = 10;
            this.bmScale.StatusBar = this.bar2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Status bar";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.totalEngineOnLabel),
            new DevExpress.XtraBars.LinkPersistInfo(this.totalEngineOffLabel)} );
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Status bar";
            // 
            // totalEngineOnLabel
            // 
            this.totalEngineOnLabel.Caption = "\"--:--:--\"";
            this.totalEngineOnLabel.Id = 0;
            this.totalEngineOnLabel.Name = "totalEngineOnLabel";
            this.totalEngineOnLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // totalEngineOffLabel
            // 
            this.totalEngineOffLabel.Caption = "\"--:--:--\"";
            this.totalEngineOffLabel.Id = 1;
            this.totalEngineOffLabel.Name = "totalEngineOffLabel";
            this.totalEngineOffLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "Scale";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ButtonDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.ButtonAdd)} );
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Scale";
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Caption = "Delete";
            this.ButtonDelete.Id = 6;
            this.ButtonDelete.Name = "ButtonDelete";
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.Caption = "Add";
            this.ButtonAdd.Id = 7;
            this.ButtonAdd.Name = "ButtonAdd";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point( 0, 0 );
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size( 137, 23 );
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Caption = "Add";
            this.mnuAdd.Id = 8;
            this.mnuAdd.Name = "mnuAdd";
            // 
            // mnuDelete
            // 
            this.mnuDelete.Caption = "Delete";
            this.mnuDelete.Id = 9;
            this.mnuDelete.Name = "mnuDelete";
            // 
            // mnuScale
            // 
            this.mnuScale.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuDelete)} );
            this.mnuScale.Manager = this.bmScale;
            this.mnuScale.Name = "mnuScale";
            // 
            // rotateReportBindingSource
            // 
            this.rotateReportBindingSource.DataMember = "RotateReport";
            this.rotateReportBindingSource.DataSource = typeof( LocalCache.atlantaDataSet );
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer ) ( resources.GetObject( "imageCollection.ImageStream" ) ) );
            this.imageCollection.Images.SetKeyName( 0, "accept.png" );
            this.imageCollection.Images.SetKeyName( 1, "anchor.png" );
            // 
            // reportGridPanel
            // 
            this.reportGridPanel.AutoSize = true;
            this.reportGridPanel.Controls.Add( this.gcRotation );
            this.reportGridPanel.Location = new System.Drawing.Point( 0, 26 );
            this.reportGridPanel.Margin = new System.Windows.Forms.Padding( 1 );
            this.reportGridPanel.Name = "reportGridPanel";
            this.reportGridPanel.Size = new System.Drawing.Size( 791, 183 );
            this.reportGridPanel.TabIndex = 8;
            // 
            // gcRotation
            // 
            this.gcRotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRotation.Location = new System.Drawing.Point( 2, 2 );
            this.gcRotation.MainView = this.gvRotation;
            this.gcRotation.Name = "gcRotation";
            this.gcRotation.Size = new System.Drawing.Size( 787, 179 );
            this.gcRotation.TabIndex = 9;
            this.gcRotation.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRotation} );
            // 
            // gvRotation
            // 
            this.gvRotation.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb( ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ) );
            this.gvRotation.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gvRotation.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRotation.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_rot,
            this.State,
            this.Location,
            this.InitialTime,
            this.FinalTime,
            this.Interval,
            this.BreakTime} );
            this.gvRotation.GridControl = this.gcRotation;
            this.gvRotation.GroupSummary.AddRange( new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Interval", null, "{0:t}")} );
            this.gvRotation.Name = "gvRotation";
            this.gvRotation.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRotation.OptionsView.ShowFooter = true;
            // 
            // Id_rot
            // 
            this.Id_rot.Caption = "Id";
            this.Id_rot.FieldName = "Id";
            this.Id_rot.Name = "Id_rot";
            this.Id_rot.OptionsColumn.AllowEdit = false;
            this.Id_rot.OptionsColumn.ReadOnly = true;
            // 
            // State
            // 
            this.State.AppearanceHeader.Options.UseTextOptions = true;
            this.State.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.State.Caption = "Состояние двигателя";
            this.State.FieldName = "State";
            this.State.Name = "State";
            this.State.OptionsColumn.AllowEdit = false;
            this.State.OptionsColumn.ReadOnly = true;
            this.State.SummaryItem.FieldName = "Interval";
            this.State.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.State.Visible = true;
            this.State.VisibleIndex = 0;
            this.State.Width = 193;
            // 
            // Location
            // 
            this.Location.AppearanceHeader.Options.UseTextOptions = true;
            this.Location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Location.Caption = "Местоположение";
            this.Location.FieldName = "Location";
            this.Location.Name = "Location";
            this.Location.OptionsColumn.AllowEdit = false;
            this.Location.OptionsColumn.ReadOnly = true;
            this.Location.Visible = true;
            this.Location.VisibleIndex = 1;
            this.Location.Width = 351;
            // 
            // InitialTime
            // 
            this.InitialTime.AppearanceCell.Options.UseTextOptions = true;
            this.InitialTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InitialTime.AppearanceHeader.Options.UseTextOptions = true;
            this.InitialTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InitialTime.Caption = "Время начала";
            this.InitialTime.DisplayFormat.FormatString = "g";
            this.InitialTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.InitialTime.FieldName = "InitialTime";
            this.InitialTime.Name = "InitialTime";
            this.InitialTime.OptionsColumn.AllowEdit = false;
            this.InitialTime.OptionsColumn.ReadOnly = true;
            this.InitialTime.Visible = true;
            this.InitialTime.VisibleIndex = 2;
            this.InitialTime.Width = 165;
            // 
            // FinalTime
            // 
            this.FinalTime.AppearanceCell.Options.UseTextOptions = true;
            this.FinalTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FinalTime.AppearanceHeader.Options.UseTextOptions = true;
            this.FinalTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FinalTime.Caption = "Время окончания";
            this.FinalTime.DisplayFormat.FormatString = "g";
            this.FinalTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FinalTime.FieldName = "FinalTime";
            this.FinalTime.Name = "FinalTime";
            this.FinalTime.OptionsColumn.AllowEdit = false;
            this.FinalTime.OptionsColumn.ReadOnly = true;
            this.FinalTime.Visible = true;
            this.FinalTime.VisibleIndex = 3;
            this.FinalTime.Width = 165;
            // 
            // Interval
            // 
            this.Interval.AppearanceCell.Options.UseTextOptions = true;
            this.Interval.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.AppearanceHeader.Options.UseTextOptions = true;
            this.Interval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.Caption = "Интервал, дни.время";
            this.Interval.DisplayFormat.FormatString = "t";
            this.Interval.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.Interval.FieldName = "Interval";
            this.Interval.Name = "Interval";
            this.Interval.OptionsColumn.AllowEdit = false;
            this.Interval.OptionsColumn.ReadOnly = true;
            this.Interval.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.Interval.Visible = true;
            this.Interval.VisibleIndex = 4;
            this.Interval.Width = 161;
            // 
            // BreakTime
            // 
            this.BreakTime.AppearanceCell.Options.UseTextOptions = true;
            this.BreakTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BreakTime.AppearanceHeader.Options.UseTextOptions = true;
            this.BreakTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.BreakTime.Caption = "В т.ч. во время стоянок";
            this.BreakTime.DisplayFormat.FormatString = "t";
            this.BreakTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.BreakTime.FieldName = "BreakTime";
            this.BreakTime.Name = "BreakTime";
            this.BreakTime.OptionsColumn.AllowEdit = false;
            this.BreakTime.OptionsColumn.ReadOnly = true;
            this.BreakTime.Visible = true;
            this.BreakTime.VisibleIndex = 5;
            this.BreakTime.Width = 215;
            // 
            // motorTimePanel
            // 
            this.motorTimePanel.AutoSize = true;
            this.motorTimePanel.Controls.Add( this.splitContainerControl1 );
            this.motorTimePanel.Location = new System.Drawing.Point( 1, 213 );
            this.motorTimePanel.Margin = new System.Windows.Forms.Padding( 1 );
            this.motorTimePanel.Name = "motorTimePanel";
            this.motorTimePanel.Size = new System.Drawing.Size( 785, 219 );
            this.motorTimePanel.TabIndex = 9;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point( 2, 2 );
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add( this.splitContainerControl2 );
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add( this.chartRotate );
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size( 781, 215 );
            this.splitContainerControl1.SplitterPosition = 562;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point( 0, 0 );
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add( this.standaloneBarDockControl1 );
            this.splitContainerControl2.Panel1.Controls.Add( this.gcScale );
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add( this.gcRotate );
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size( 562, 215 );
            this.splitContainerControl2.SplitterPosition = 137;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gcScale
            // 
            this.gcScale.Location = new System.Drawing.Point( 0, 21 );
            this.gcScale.MainView = this.gvScale;
            this.gcScale.Margin = new System.Windows.Forms.Padding( 1 );
            this.gcScale.Name = "gcScale";
            this.gcScale.Size = new System.Drawing.Size( 137, 191 );
            this.gcScale.TabIndex = 1;
            this.gcScale.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvScale} );
            // 
            // gvScale
            // 
            this.gvScale.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb( ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ) );
            this.gvScale.Appearance.EvenRow.BackColor2 = System.Drawing.Color.FromArgb( ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ) );
            this.gvScale.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvScale.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.Scale} );
            this.gvScale.GridControl = this.gcScale;
            this.gvScale.Name = "gvScale";
            this.gvScale.OptionsSelection.MultiSelect = true;
            this.gvScale.OptionsView.EnableAppearanceEvenRow = true;
            this.gvScale.OptionsView.ShowGroupPanel = false;
            // 
            // Id
            // 
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.ReadOnly = true;
            // 
            // Scale
            // 
            this.Scale.AppearanceCell.Options.UseTextOptions = true;
            this.Scale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Scale.AppearanceHeader.Options.UseTextOptions = true;
            this.Scale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Scale.Caption = "Шкала";
            this.Scale.FieldName = "Bound";
            this.Scale.Name = "Scale";
            this.Scale.Visible = true;
            this.Scale.VisibleIndex = 0;
            // 
            // gcRotate
            // 
            this.gcRotate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRotate.Location = new System.Drawing.Point( 0, 0 );
            this.gcRotate.MainView = this.gvRotate;
            this.gcRotate.Name = "gcRotate";
            this.gcRotate.Size = new System.Drawing.Size( 419, 215 );
            this.gcRotate.TabIndex = 0;
            this.gcRotate.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRotate} );
            // 
            // gvRotate
            // 
            this.gvRotate.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb( ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ) );
            this.gvRotate.Appearance.EvenRow.BackColor2 = System.Drawing.Color.FromArgb( ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ), ( ( int ) ( ( ( byte ) ( 224 ) ) ) ) );
            this.gvRotate.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRotate.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gvRotate.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NF,
            this.NT,
            this.TimeRot,
            this.DistanceRot,
            this.PercentRot} );
            this.gvRotate.GridControl = this.gcRotate;
            this.gvRotate.Name = "gvRotate";
            this.gvRotate.OptionsBehavior.AutoPopulateColumns = false;
            this.gvRotate.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRotate.OptionsView.ShowFooter = true;
            this.gvRotate.OptionsView.ShowGroupPanel = false;
            // 
            // NF
            // 
            this.NF.AppearanceCell.Options.UseTextOptions = true;
            this.NF.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NF.AppearanceHeader.Options.UseTextOptions = true;
            this.NF.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NF.Caption = "Обороты от";
            this.NF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NF.FieldName = "NumberFrom";
            this.NF.Name = "NF";
            this.NF.OptionsColumn.AllowEdit = false;
            this.NF.OptionsColumn.ReadOnly = true;
            this.NF.Visible = true;
            this.NF.VisibleIndex = 0;
            this.NF.Width = 250;
            // 
            // NT
            // 
            this.NT.AppearanceCell.Options.UseTextOptions = true;
            this.NT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NT.AppearanceHeader.Options.UseTextOptions = true;
            this.NT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NT.Caption = "Обороты до";
            this.NT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.NT.FieldName = "NumberTo";
            this.NT.Name = "NT";
            this.NT.OptionsColumn.AllowEdit = false;
            this.NT.OptionsColumn.ReadOnly = true;
            this.NT.Visible = true;
            this.NT.VisibleIndex = 1;
            this.NT.Width = 237;
            // 
            // TimeRot
            // 
            this.TimeRot.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRot.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRot.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRot.Caption = "Время";
            this.TimeRot.FieldName = "TimeRotation";
            this.TimeRot.Name = "TimeRot";
            this.TimeRot.OptionsColumn.AllowEdit = false;
            this.TimeRot.OptionsColumn.ReadOnly = true;
            this.TimeRot.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.TimeRot.Visible = true;
            this.TimeRot.VisibleIndex = 2;
            this.TimeRot.Width = 266;
            // 
            // DistanceRot
            // 
            this.DistanceRot.AppearanceCell.Options.UseTextOptions = true;
            this.DistanceRot.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceRot.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceRot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceRot.Caption = "Пробег, км";
            this.DistanceRot.DisplayFormat.FormatString = "N2";
            this.DistanceRot.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DistanceRot.FieldName = "Distance";
            this.DistanceRot.Name = "DistanceRot";
            this.DistanceRot.OptionsColumn.AllowEdit = false;
            this.DistanceRot.OptionsColumn.ReadOnly = true;
            this.DistanceRot.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.DistanceRot.Visible = true;
            this.DistanceRot.VisibleIndex = 3;
            this.DistanceRot.Width = 248;
            // 
            // PercentRot
            // 
            this.PercentRot.AppearanceCell.Options.UseTextOptions = true;
            this.PercentRot.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PercentRot.AppearanceHeader.Options.UseTextOptions = true;
            this.PercentRot.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PercentRot.Caption = "Процент";
            this.PercentRot.DisplayFormat.FormatString = "N2";
            this.PercentRot.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentRot.FieldName = "PercentRotation";
            this.PercentRot.Name = "PercentRot";
            this.PercentRot.OptionsColumn.AllowEdit = false;
            this.PercentRot.OptionsColumn.ReadOnly = true;
            this.PercentRot.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.PercentRot.Visible = true;
            this.PercentRot.VisibleIndex = 4;
            this.PercentRot.Width = 253;
            // 
            // DevRotationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.motorTimePanel );
            this.Controls.Add( this.reportGridPanel );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "DevRotationControl";
            this.Size = new System.Drawing.Size( 791, 487 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.reportGridPanel, 0 );
            this.Controls.SetChildIndex( this.motorTimePanel, 0 );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesLabel1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesView1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( series1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesLabel2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( pieSeriesView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.chartRotate ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.bmScale ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.mnuScale ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.rotateReportBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.imageCollection ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.reportGridPanel ) ).EndInit();
            this.reportGridPanel.ResumeLayout( false );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcRotation ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvRotation ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.motorTimePanel ) ).EndInit();
            this.motorTimePanel.ResumeLayout( false );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.splitContainerControl1 ) ).EndInit();
            this.splitContainerControl1.ResumeLayout( false );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.splitContainerControl2 ) ).EndInit();
            this.splitContainerControl2.ResumeLayout( false );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcScale ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvScale ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcRotate ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvRotate ) ).EndInit();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraBars.BarManager bmScale;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.PopupMenu mnuScale;
        private System.Windows.Forms.BindingSource rotateReportBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraBars.BarStaticItem totalEngineOnLabel;
        private DevExpress.XtraBars.BarStaticItem totalEngineOffLabel;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraEditors.PanelControl reportGridPanel;
        private DevExpress.XtraGrid.GridControl gcRotation;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRotation;
        private DevExpress.XtraGrid.Columns.GridColumn Id_rot;
        private DevExpress.XtraGrid.Columns.GridColumn State;
        private DevExpress.XtraGrid.Columns.GridColumn Location;
        private DevExpress.XtraGrid.Columns.GridColumn InitialTime;
        private DevExpress.XtraGrid.Columns.GridColumn FinalTime;
        private DevExpress.XtraGrid.Columns.GridColumn Interval;
        private DevExpress.XtraGrid.Columns.GridColumn BreakTime;
        private DevExpress.XtraEditors.PanelControl motorTimePanel;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gcScale;
        private DevExpress.XtraGrid.Views.Grid.GridView gvScale;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn Scale;
        private DevExpress.XtraGrid.GridControl gcRotate;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRotate;
        private DevExpress.XtraGrid.Columns.GridColumn NF;
        private DevExpress.XtraGrid.Columns.GridColumn NT;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRot;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceRot;
        private DevExpress.XtraGrid.Columns.GridColumn PercentRot;
        private DevExpress.XtraCharts.ChartControl chartRotate;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem ButtonDelete;
        private DevExpress.XtraBars.BarButtonItem ButtonAdd;
        private DevExpress.XtraBars.BarButtonItem mnuAdd;
        private DevExpress.XtraBars.BarButtonItem mnuDelete;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;

    }
}
