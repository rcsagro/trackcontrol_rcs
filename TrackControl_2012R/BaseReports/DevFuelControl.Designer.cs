﻿namespace BaseReports
{
    partial class DevFuelControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevFuelControl ) );
            this.fuelDataGridControl = new DevExpress.XtraGrid.GridControl();
            this.fuelDataGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._location = new DevExpress.XtraGrid.Columns.GridColumn();
            this._time = new DevExpress.XtraGrid.Columns.GridColumn();
            this._quantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this._beginValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this._endValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGenerateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
            this.bar2 = new DevExpress.XtraBars.Bar();
            this._beforeLbl = new DevExpress.XtraBars.BarStaticItem();
            this._afterLbl = new DevExpress.XtraBars.BarStaticItem();
            this._fuelingLbl = new DevExpress.XtraBars.BarStaticItem();
            this._dischargeLbl = new DevExpress.XtraBars.BarStaticItem();
            this._totalLbl = new DevExpress.XtraBars.BarStaticItem();
            this._rateLbl = new DevExpress.XtraBars.BarStaticItem();
            this._avgSpeedLbl = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelDataGridControl ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelDataGridView ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.atlantaDataSetBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).BeginInit();
            this.SuspendLayout();
            // 
            // fuelDataGridControl
            // 
            this.fuelDataGridControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fuelDataGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelDataGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelDataGridControl.Location = new System.Drawing.Point( 0, 26 );
            this.fuelDataGridControl.MainView = this.fuelDataGridView;
            this.fuelDataGridControl.Name = "fuelDataGridControl";
            this.fuelDataGridControl.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1} );
            this.fuelDataGridControl.Size = new System.Drawing.Size( 912, 355 );
            this.fuelDataGridControl.TabIndex = 11;
            this.fuelDataGridControl.UseEmbeddedNavigator = true;
            this.fuelDataGridControl.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelDataGridView,
            this.gridView2} );
            // 
            // fuelDataGridView
            // 
            this.fuelDataGridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.fuelDataGridView.Appearance.Empty.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.fuelDataGridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.fuelDataGridView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.fuelDataGridView.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.fuelDataGridView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.fuelDataGridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.FilterPanel.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelDataGridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.fuelDataGridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.fuelDataGridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelDataGridView.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelDataGridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelDataGridView.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.GroupButton.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.GroupButton.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.GroupFooter.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.fuelDataGridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.fuelDataGridView.Appearance.GroupRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.GroupRow.Options.UseFont = true;
            this.fuelDataGridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelDataGridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.fuelDataGridView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelDataGridView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.fuelDataGridView.Appearance.HorzLine.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelDataGridView.Appearance.OddRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelDataGridView.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.Preview.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.Preview.Options.UseForeColor = true;
            this.fuelDataGridView.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.fuelDataGridView.Appearance.Row.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.RowSeparator.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.fuelDataGridView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.fuelDataGridView.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.fuelDataGridView.Appearance.VertLine.Options.UseBackColor = true;
            this.fuelDataGridView.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.fuelDataGridView.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelDataGridView.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelDataGridView.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelDataGridView.ColumnPanelRowHeight = 40;
            this.fuelDataGridView.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._location,
            this._time,
            this._quantity,
            this._beginValue,
            this._endValue,
            this.colGenerateType} );
            this.fuelDataGridView.GridControl = this.fuelDataGridControl;
            this.fuelDataGridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.fuelDataGridView.Name = "fuelDataGridView";
            this.fuelDataGridView.OptionsDetail.AllowZoomDetail = false;
            this.fuelDataGridView.OptionsDetail.EnableMasterViewMode = false;
            this.fuelDataGridView.OptionsDetail.ShowDetailTabs = false;
            this.fuelDataGridView.OptionsDetail.SmartDetailExpand = false;
            this.fuelDataGridView.OptionsNavigation.AutoFocusNewRow = true;
            this.fuelDataGridView.OptionsNavigation.EnterMoveNextColumn = true;
            this.fuelDataGridView.OptionsSelection.MultiSelect = true;
            this.fuelDataGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelDataGridView.OptionsView.EnableAppearanceOddRow = true;
            this.fuelDataGridView.OptionsView.ShowFooter = true;
            // 
            // _location
            // 
            this._location.AppearanceCell.Options.UseTextOptions = true;
            this._location.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.AppearanceHeader.Options.UseTextOptions = true;
            this._location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.Caption = "Местоположение";
            this._location.FieldName = "Location";
            this._location.Name = "_location";
            this._location.OptionsColumn.AllowEdit = false;
            this._location.OptionsColumn.ReadOnly = true;
            this._location.ToolTip = "Местоположение";
            this._location.Visible = true;
            this._location.VisibleIndex = 0;
            this._location.Width = 120;
            // 
            // _time
            // 
            this._time.AppearanceCell.Options.UseTextOptions = true;
            this._time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._time.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._time.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._time.AppearanceHeader.Options.UseTextOptions = true;
            this._time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._time.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._time.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._time.Caption = "Дата и время";
            this._time.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this._time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._time.FieldName = "time_";
            this._time.Name = "_time";
            this._time.OptionsColumn.AllowEdit = false;
            this._time.OptionsColumn.ReadOnly = true;
            this._time.ToolTip = "Дата и время заправки";
            this._time.Visible = true;
            this._time.VisibleIndex = 1;
            this._time.Width = 80;
            // 
            // _quantity
            // 
            this._quantity.AppearanceCell.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
            this._quantity.AppearanceCell.ForeColor = System.Drawing.Color.Green;
            this._quantity.AppearanceCell.Options.UseFont = true;
            this._quantity.AppearanceCell.Options.UseForeColor = true;
            this._quantity.AppearanceCell.Options.UseTextOptions = true;
            this._quantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._quantity.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._quantity.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._quantity.AppearanceHeader.Options.UseTextOptions = true;
            this._quantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._quantity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._quantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._quantity.Caption = "Заправлено, л";
            this._quantity.DisplayFormat.FormatString = "N2";
            this._quantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._quantity.FieldName = "dValue";
            this._quantity.Name = "_quantity";
            this._quantity.OptionsColumn.AllowEdit = false;
            this._quantity.OptionsColumn.ReadOnly = true;
            this._quantity.SummaryItem.DisplayFormat = "{0:f2}";
            this._quantity.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._quantity.ToolTip = "Количество заправленного топлива";
            this._quantity.Visible = true;
            this._quantity.VisibleIndex = 2;
            this._quantity.Width = 90;
            // 
            // _beginValue
            // 
            this._beginValue.AppearanceCell.Options.UseTextOptions = true;
            this._beginValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._beginValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._beginValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._beginValue.AppearanceHeader.Options.UseTextOptions = true;
            this._beginValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._beginValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._beginValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._beginValue.Caption = "До заправки, л";
            this._beginValue.DisplayFormat.FormatString = "N2";
            this._beginValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._beginValue.FieldName = "beginValue";
            this._beginValue.Name = "_beginValue";
            this._beginValue.SummaryItem.DisplayFormat = "{0:f2}";
            this._beginValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._beginValue.ToolTip = "Количество топлива до заправки";
            this._beginValue.Visible = true;
            this._beginValue.VisibleIndex = 3;
            this._beginValue.Width = 90;
            // 
            // _endValue
            // 
            this._endValue.AppearanceCell.Options.UseTextOptions = true;
            this._endValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._endValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._endValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._endValue.AppearanceHeader.Options.UseTextOptions = true;
            this._endValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._endValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._endValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._endValue.Caption = "После заправки, л";
            this._endValue.DisplayFormat.FormatString = "N2";
            this._endValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._endValue.FieldName = "endValue";
            this._endValue.Name = "_endValue";
            this._endValue.SummaryItem.DisplayFormat = "{0:f2}";
            this._endValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._endValue.ToolTip = "Количество топлива после заправки";
            this._endValue.Visible = true;
            this._endValue.VisibleIndex = 4;
            this._endValue.Width = 110;
            // 
            // colGenerateType
            // 
            this.colGenerateType.AppearanceCell.Options.UseTextOptions = true;
            this.colGenerateType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGenerateType.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGenerateType.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGenerateType.AppearanceHeader.Options.UseTextOptions = true;
            this.colGenerateType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGenerateType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGenerateType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGenerateType.Caption = "Способ генерации";
            this.colGenerateType.FieldName = "GenerateType";
            this.colGenerateType.Name = "colGenerateType";
            this.colGenerateType.ToolTip = "Способ генерации полученных данных";
            this.colGenerateType.Visible = true;
            this.colGenerateType.VisibleIndex = 5;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange( new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)} );
            this._imageComboRepo.Items.AddRange( new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)} );
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.fuelDataGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar2} );
            this.barManager1.DockControls.Add( this.barDockControl1 );
            this.barManager1.DockControls.Add( this.barDockControl2 );
            this.barManager1.DockControls.Add( this.barDockControl3 );
            this.barManager1.DockControls.Add( this.barDockControl4 );
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this._beforeLbl,
            this._afterLbl,
            this._fuelingLbl,
            this._dischargeLbl,
            this._totalLbl,
            this._rateLbl,
            this._avgSpeedLbl} );
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Status bar";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._beforeLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._afterLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._fuelingLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._dischargeLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._totalLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._rateLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._avgSpeedLbl)} );
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Status bar";
            // 
            // _beforeLbl
            // 
            this._beforeLbl.Caption = "В начале, л: ---";
            this._beforeLbl.Id = 0;
            this._beforeLbl.Name = "_beforeLbl";
            this._beforeLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _afterLbl
            // 
            this._afterLbl.Caption = "В конце, л: ---";
            this._afterLbl.Id = 1;
            this._afterLbl.Name = "_afterLbl";
            this._afterLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _fuelingLbl
            // 
            this._fuelingLbl.Appearance.ForeColor = System.Drawing.Color.Green;
            this._fuelingLbl.Appearance.Options.UseForeColor = true;
            this._fuelingLbl.Caption = "Заправлено, л: ---";
            this._fuelingLbl.Id = 2;
            this._fuelingLbl.Name = "_fuelingLbl";
            this._fuelingLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _dischargeLbl
            // 
            this._dischargeLbl.Appearance.ForeColor = System.Drawing.Color.Red;
            this._dischargeLbl.Appearance.Options.UseForeColor = true;
            this._dischargeLbl.Caption = "Слито, л: ---";
            this._dischargeLbl.Id = 3;
            this._dischargeLbl.Name = "_dischargeLbl";
            this._dischargeLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _totalLbl
            // 
            this._totalLbl.Caption = "Общий расход, л: ---";
            this._totalLbl.Id = 4;
            this._totalLbl.Name = "_totalLbl";
            this._totalLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _rateLbl
            // 
            this._rateLbl.Caption = "Расход, л/100км: ---";
            this._rateLbl.Id = 5;
            this._rateLbl.Name = "_rateLbl";
            this._rateLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _avgSpeedLbl
            // 
            this._avgSpeedLbl.Caption = "Ср. скорость, км/ч: ---";
            this._avgSpeedLbl.Id = 6;
            this._avgSpeedLbl.Name = "_avgSpeedLbl";
            this._avgSpeedLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins( 10, 10, 70, 10 );
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins( 10, 10, 15, 10 );
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // DevFuelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.fuelDataGridControl );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "DevFuelControl";
            this.Size = new System.Drawing.Size( 912, 430 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.fuelDataGridControl, 0 );
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelDataGridControl ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelDataGridView ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.atlantaDataSetBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraGrid.GridControl fuelDataGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelDataGridView;
        private DevExpress.XtraGrid.Columns.GridColumn _location;
        private DevExpress.XtraGrid.Columns.GridColumn _time;
        private DevExpress.XtraGrid.Columns.GridColumn _quantity;
        private DevExpress.XtraGrid.Columns.GridColumn _beginValue;
        private DevExpress.XtraGrid.Columns.GridColumn _endValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem _beforeLbl;
        private DevExpress.XtraBars.BarStaticItem _afterLbl;
        private DevExpress.XtraBars.BarStaticItem _fuelingLbl;
        private DevExpress.XtraBars.BarStaticItem _dischargeLbl;
        private DevExpress.XtraBars.BarStaticItem _totalLbl;
        private DevExpress.XtraBars.BarStaticItem _rateLbl;
        private DevExpress.XtraBars.BarStaticItem _avgSpeedLbl;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraGrid.Columns.GridColumn colGenerateType;
    }
}
