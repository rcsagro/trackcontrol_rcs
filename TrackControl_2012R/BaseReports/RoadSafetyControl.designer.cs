using LocalCache;
namespace BaseReports
{
  partial class RoadSafetyControl
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        this.trafficSafetyGridView = new System.Windows.Forms.DataGridView();
        this.Mark = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.Model = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.NamberPlate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.Driver = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.trafficSafetyReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.atlantaDataSet = new LocalCache.atlantaDataSet();
        this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        this.accelLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.breakLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.speedLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.reportPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyReportBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
        this.statusStrip1.SuspendLayout();
        this.SuspendLayout();
        // 
        // reportPanel
        // 
        this.reportPanel.Controls.Add(this.trafficSafetyGridView);
        this.reportPanel.Controls.Add(this.statusStrip1);
        this.reportPanel.Size = new System.Drawing.Size(761, 337);
        // 
        // atlantaDataSetBindingSource
        // 
        this.atlantaDataSetBindingSource.DataSource = this.trafficSafetyReportBindingSource;
        // 
        // trafficSafetyGridView
        // 
        this.trafficSafetyGridView.AllowUserToAddRows = false;
        this.trafficSafetyGridView.AutoGenerateColumns = false;
        this.trafficSafetyGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
        this.trafficSafetyGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.trafficSafetyGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        this.trafficSafetyGridView.ColumnHeadersHeight = 35;
        this.trafficSafetyGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mark,
            this.Model,
            this.NamberPlate,
            this.Driver,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71});
        this.trafficSafetyGridView.DataSource = this.trafficSafetyReportBindingSource;
        dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.trafficSafetyGridView.DefaultCellStyle = dataGridViewCellStyle12;
        this.trafficSafetyGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.trafficSafetyGridView.Location = new System.Drawing.Point(0, 0);
        this.trafficSafetyGridView.Name = "trafficSafetyGridView";
        dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.trafficSafetyGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
        this.trafficSafetyGridView.RowHeadersVisible = false;
        this.trafficSafetyGridView.Size = new System.Drawing.Size(761, 315);
        this.trafficSafetyGridView.TabIndex = 1;
        // 
        // Mark
        // 
        this.Mark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.Mark.DataPropertyName = "Mark";
        this.Mark.Frozen = true;
        this.Mark.HeaderText = "�����";
        this.Mark.Name = "Mark";
        this.Mark.ReadOnly = true;
        this.Mark.ToolTipText = "����� ������������� ��������";
        this.Mark.Width = 65;
        // 
        // Model
        // 
        this.Model.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.Model.DataPropertyName = "Model";
        this.Model.Frozen = true;
        this.Model.HeaderText = "������";
        this.Model.Name = "Model";
        this.Model.ReadOnly = true;
        this.Model.ToolTipText = "������ ������������� ��������";
        this.Model.Width = 71;
        // 
        // NamberPlate
        // 
        this.NamberPlate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.NamberPlate.DataPropertyName = "NamberPlate";
        this.NamberPlate.Frozen = true;
        this.NamberPlate.HeaderText = "�����";
        this.NamberPlate.Name = "NamberPlate";
        this.NamberPlate.ReadOnly = true;
        this.NamberPlate.ToolTipText = "��������������� ��� �������� �����";
        this.NamberPlate.Width = 66;
        // 
        // Driver
        // 
        this.Driver.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.Driver.DataPropertyName = "Driver";
        this.Driver.Frozen = true;
        this.Driver.HeaderText = "�.�.�";
        this.Driver.Name = "Driver";
        this.Driver.ReadOnly = true;
        this.Driver.ToolTipText = "�.�.� ��������";
        this.Driver.Width = 65;
        // 
        // dataGridViewTextBoxColumn62
        // 
        this.dataGridViewTextBoxColumn62.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn62.DataPropertyName = "Travel";
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle2.Format = "N1";
        dataGridViewCellStyle2.NullValue = null;
        this.dataGridViewTextBoxColumn62.DefaultCellStyle = dataGridViewCellStyle2;
        this.dataGridViewTextBoxColumn62.HeaderText = "���� ��.";
        this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
        this.dataGridViewTextBoxColumn62.ToolTipText = "���������� ����";
        this.dataGridViewTextBoxColumn62.Width = 70;
        // 
        // dataGridViewTextBoxColumn63
        // 
        this.dataGridViewTextBoxColumn63.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn63.DataPropertyName = "SpeedMax";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.Format = "N1";
        dataGridViewCellStyle3.NullValue = null;
        this.dataGridViewTextBoxColumn63.DefaultCellStyle = dataGridViewCellStyle3;
        this.dataGridViewTextBoxColumn63.HeaderText = "�������� ����. ��\\�";
        this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
        this.dataGridViewTextBoxColumn63.ToolTipText = "������������ ��������";
        this.dataGridViewTextBoxColumn63.Width = 106;
        // 
        // dataGridViewTextBoxColumn64
        // 
        this.dataGridViewTextBoxColumn64.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn64.DataPropertyName = "AccelMax";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle4.Format = "N1";
        this.dataGridViewTextBoxColumn64.DefaultCellStyle = dataGridViewCellStyle4;
        this.dataGridViewTextBoxColumn64.HeaderText = "��������� ����. �\\�2";
        this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
        this.dataGridViewTextBoxColumn64.ToolTipText = "������������ ���������";
        this.dataGridViewTextBoxColumn64.Width = 113;
        // 
        // dataGridViewTextBoxColumn65
        // 
        this.dataGridViewTextBoxColumn65.DataPropertyName = "BreakMax";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.Format = "N1";
        this.dataGridViewTextBoxColumn65.DefaultCellStyle = dataGridViewCellStyle5;
        this.dataGridViewTextBoxColumn65.HeaderText = "���������� ����. �\\�2";
        this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
        this.dataGridViewTextBoxColumn65.ToolTipText = "������������ ����������";
        this.dataGridViewTextBoxColumn65.Width = 121;
        // 
        // dataGridViewTextBoxColumn66
        // 
        this.dataGridViewTextBoxColumn66.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn66.DataPropertyName = "AccelCount";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle6.Format = "N0";
        this.dataGridViewTextBoxColumn66.DefaultCellStyle = dataGridViewCellStyle6;
        this.dataGridViewTextBoxColumn66.HeaderText = "���-�� ���������";
        this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
        this.dataGridViewTextBoxColumn66.ToolTipText = "���������� ���������� ��������� ���������";
        this.dataGridViewTextBoxColumn66.Width = 112;
        // 
        // dataGridViewTextBoxColumn67
        // 
        this.dataGridViewTextBoxColumn67.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn67.DataPropertyName = "BreakCount";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle7.Format = "N0";
        this.dataGridViewTextBoxColumn67.DefaultCellStyle = dataGridViewCellStyle7;
        this.dataGridViewTextBoxColumn67.HeaderText = "���-�� ����������";
        this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
        this.dataGridViewTextBoxColumn67.ToolTipText = "���������� ���������� ��������� ����������";
        this.dataGridViewTextBoxColumn67.Width = 121;
        // 
        // dataGridViewTextBoxColumn68
        // 
        this.dataGridViewTextBoxColumn68.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn68.DataPropertyName = "SpeedTreshold";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.dataGridViewTextBoxColumn68.DefaultCellStyle = dataGridViewCellStyle8;
        this.dataGridViewTextBoxColumn68.HeaderText = "����� ���������� ��������";
        this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
        this.dataGridViewTextBoxColumn68.ToolTipText = "�������� ����� �������� �� ��������� ����������� ��������";
        this.dataGridViewTextBoxColumn68.Width = 166;
        // 
        // dataGridViewTextBoxColumn69
        // 
        this.dataGridViewTextBoxColumn69.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn69.DataPropertyName = "Ka";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle9.Format = "N2";
        this.dataGridViewTextBoxColumn69.DefaultCellStyle = dataGridViewCellStyle9;
        this.dataGridViewTextBoxColumn69.HeaderText = "Ka";
        this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
        this.dataGridViewTextBoxColumn69.ToolTipText = "����������� ���������, ������������ ��� ��������� ���������� ��������� � ��������" +
            "��� ����";
        this.dataGridViewTextBoxColumn69.Width = 45;
        // 
        // dataGridViewTextBoxColumn70
        // 
        this.dataGridViewTextBoxColumn70.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn70.DataPropertyName = "Kb";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.Format = "N2";
        this.dataGridViewTextBoxColumn70.DefaultCellStyle = dataGridViewCellStyle10;
        this.dataGridViewTextBoxColumn70.HeaderText = "Kb";
        this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
        this.dataGridViewTextBoxColumn70.ToolTipText = "����������� ����������, ������������ ��� ��������� ���������� ���������� � ������" +
            "����� ���� ";
        this.dataGridViewTextBoxColumn70.Width = 45;
        // 
        // dataGridViewTextBoxColumn71
        // 
        this.dataGridViewTextBoxColumn71.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn71.DataPropertyName = "Kv";
        dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle11.Format = "N1";
        this.dataGridViewTextBoxColumn71.DefaultCellStyle = dataGridViewCellStyle11;
        this.dataGridViewTextBoxColumn71.HeaderText = "Kv";
        this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
        this.dataGridViewTextBoxColumn71.ToolTipText = "����������� ��������, ������������ ��� ��������� ������� ���������� �������� � ��" +
            "����� ��������";
        this.dataGridViewTextBoxColumn71.Width = 45;
        // 
        // trafficSafetyReportBindingSource
        // 
        this.trafficSafetyReportBindingSource.DataMember = "trafficSafetyReport";
        this.trafficSafetyReportBindingSource.DataSource = this.atlantaDataSet;
        // 
        // atlantaDataSet
        // 
        this.atlantaDataSet.DataSetName = "atlantaDataSet";
        this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // statusStrip1
        // 
        this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accelLabel,
            this.breakLabel,
            this.speedLabel});
        this.statusStrip1.Location = new System.Drawing.Point(0, 315);
        this.statusStrip1.Name = "statusStrip1";
        this.statusStrip1.Size = new System.Drawing.Size(761, 22);
        this.statusStrip1.TabIndex = 2;
        this.statusStrip1.Text = "statusStrip1";
        // 
        // accelLabel
        // 
        this.accelLabel.AutoToolTip = true;
        this.accelLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.accelLabel.Name = "accelLabel";
        this.accelLabel.Size = new System.Drawing.Size(127, 17);
        this.accelLabel.Text = "���������� ���������";
        this.accelLabel.ToolTipText = "����� ���������� ���������";
        // 
        // breakLabel
        // 
        this.breakLabel.AutoToolTip = true;
        this.breakLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.breakLabel.Name = "breakLabel";
        this.breakLabel.Size = new System.Drawing.Size(127, 17);
        this.breakLabel.Text = "���������� ���������";
        this.breakLabel.ToolTipText = "����� ���������� ����������";
        // 
        // speedLabel
        // 
        this.speedLabel.AutoToolTip = true;
        this.speedLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.speedLabel.Name = "speedLabel";
        this.speedLabel.Size = new System.Drawing.Size(157, 17);
        this.speedLabel.Text = "����� ���������� ��������";
        this.speedLabel.ToolTipText = "����� ����� ���������� ��������";
        // 
        // RoadSafetyControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScroll = true;
        this.Name = "RoadSafetyControl";
        this.Size = new System.Drawing.Size(763, 368);
        this.Controls.SetChildIndex(this.reportPanel, 0);
        this.reportPanel.ResumeLayout(false);
        this.reportPanel.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyReportBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
        this.statusStrip1.ResumeLayout(false);
        this.statusStrip1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.BindingSource trafficSafetyReportBindingSource;
    private System.Windows.Forms.DataGridView trafficSafetyGridView;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel accelLabel;
    private System.Windows.Forms.ToolStripStatusLabel breakLabel;
    private System.Windows.Forms.ToolStripStatusLabel speedLabel;
    private System.Windows.Forms.DataGridViewTextBoxColumn Mark;
    private System.Windows.Forms.DataGridViewTextBoxColumn Model;
    private System.Windows.Forms.DataGridViewTextBoxColumn NamberPlate;
    private System.Windows.Forms.DataGridViewTextBoxColumn Driver;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
  }
}
