﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;

namespace BaseReports
{
    [ToolboxItem(false)]
    public partial class GrainLevel : BaseReports.ReportsDE.BaseControl
    {
        public class reportGrain
        {
            string humanVeh;
            string numberVeh;
            string markingVeh;
            int identification;
            string location;
            string date;
            string time;
            double value;
            double begin;
            double end;
            string genertype;
            private int dataGpsId;

            public reportGrain(string human, string number, string marking, int ident, string location, string date,
                string time, double value, double begin, double end, string genertype, int dataGpsid)
            {
                this.humanVeh = human;
                this.numberVeh = number;
                this.markingVeh = marking;
                this.identification = ident;
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
            } // reportGrain

            public reportGrain(string location, string date, string time, double value, double begin, double end,
                string genertype, int dataGpsid)
            {
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
            } // reportGrain

            public int DataGPS_ID
            {
                get { return dataGpsId; }
            }

            [Developer(0)]
            public string HumanVehicle
            {
                get { return humanVeh; }
            }

            [Developer(1)]
            public string NumberVehicle
            {
                get { return numberVeh; }
            }

            [Developer(2)]
            public string MarkingVehicle
            {
                get { return markingVeh; }
            }

            [Developer(3)]
            public int Identification
            {
                get { return identification; }
            }

            [Developer(4)]
            public string Location
            {
                get { return location; }
            }

            [Developer(5)]
            public string date_
            {
                get { return date; }
            }

            [Developer(6)]
            public string Times
            {
                get { return time; }
            }

            [Developer(7)]
            public double dValue
            {
                get { return value; }
            }

            [Developer(8)]
            public double beginValue
            {
                get { return begin; }
            }

            [Developer(9)]
            public double endValue
            {
                get { return end; }
            }

            [Developer(10)]
            public string GenerateType
            {
                get { return genertype; }
            }
        } // reportGrain

        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, Grain.Summary> _summaries = new Dictionary<int, Grain.Summary>();

        private FuelAddDlg fuelAddDlg;
        private atlantaDataSet.FuelReportRow grain_row;
        private Grain grainAlg;
        private bool begin_end;
        private bool flagStart = true;

        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;

        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ControlNavigator connav;
        ReportBase<GrainLevel.reportGrain, TInfoDut> ReportingGrain;
        int numberChangedRow;
        private string NameReport = Resources.GrainCtrlCaption;

        /// <summary>
        /// Создает и инициализирует новый экземпляр класса BaseReports.GrainControl
        /// </summary>
        /// <param name="caption"></param>
        public GrainLevel()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            EnabledExportExtendMenu();
            VisionPanel(grainDataGridView, grainDataGridControl, bar2);
            buildGraph = new BuildGraphs();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            grainAlg = new Grain(AlgorithmType.GRAIN1);
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(grainAlg);

            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "Grain_Report"; 

            fuelAddDlg = new FuelAddDlg(graph);

            grainDataGridControl.UseEmbeddedNavigator = true;
            barButtonNavigator.Caption = Resources.ResButtonNavigatorHide;
            connav = grainDataGridControl.EmbeddedNavigator;
            connav.Buttons.BeginUpdate();
            connav.Buttons.Append.Visible = true;
            connav.Buttons.Append.Enabled = true;
            connav.Buttons.Remove.Visible = true;
            connav.Buttons.Remove.Enabled = true;
            connav.Buttons.EndUpdate();

            grainDataGridView.RowCellClick += new RowCellClickEventHandler(grainDataGridView_CellMouseDoubleClick);
            grainDataGridView.ValidatingEditor +=
                new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(
                    grainDataGridView_CellValidating);
            grainDataGridView.CellValueChanged += new CellValueChangedEventHandler(grainDataGridView_CellEndEdit);
            connav.ButtonClick += new NavigatorButtonClickEventHandler(connav_ButtonClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingGrain =
                new ReportBase<GrainLevel.reportGrain, TInfoDut>(Controls, compositeLink1, grainDataGridView,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is Grain)
            {
                if (e is GrainEventArgs)
                {
                    GrainEventArgs args = (GrainEventArgs) e;
                    if (_summaries.ContainsKey(args.Id))
                    {
                        _summaries.Remove(args.Id);
                    }

                    _summaries.Add(args.Id, args.Summary);
                } // if
            } // if
        } // Algorithm_Action

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.FuelReport.Clear();
            dataset.FuelValue.Clear();
            _summaries.Clear();
            ClearStatusLine();
        }

        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            List<Marker> markers = new List<Marker>();
            Int32[] index = grainDataGridView.GetSelectedRows();
            for (int i = 0; i < index.Length; i++)
            {
                atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index[i]]).Row;

                if (f_row["time_"] != DBNull.Value)
                {
                    double value = f_row.dValue;
                    string message = String.Format("{0}: {1:N2} {2}.",
                        value > 0 ? Resources.Loading : Resources.GrainLarceny,
                        value, Resources.MassAbbreviation);
                    PointLatLng location = new PointLatLng(f_row.Lat, f_row.Lon);
                    markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                } // if
            } // for

            ReportsControl.OnMarkersShowNeeded(markers);
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        }

        void UpdateZGraph()
        {
            // Сбрасываем все метки
            // Graph.ClearGraphZoom(false)
            Graph.ClearLabel();
            Int32[] index = grainDataGridView.GetSelectedRows();

            for (int i = 0; i < index.Length; i++)
            {
                if (index[i] >= 0)
                {
                    int indx = grainDataGridView.GetDataSourceRowIndex(index[i]);

                    atlantaDataSet.Grain_ReportRow f_row = (atlantaDataSet.Grain_ReportRow)
                        ((DataRowView) atlantaDataSetBindingSource.List[indx]).Row;

                    if (f_row["time_"] != DBNull.Value)
                    {
                        string val;
                        Color color;
                        ShowGraphToolStripButton_ClickExtracted(f_row, out val, out color);

                        if (f_row.pointValue <= 0.0)
                        {
                            Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.beginValue, val, color);
                            return;
                        }

                        Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.pointValue, val, color);
                    } // if
                }
            } // for
            // Graph.ClearGraphZoom(true); AddGraphInclinometrX
            ReportsControl.ShowGraph(curMrow);
            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
            Graph.SellectZoom();
        } // UpdateZGraph

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            _beforeLbl.Caption = Resources.BeforeLblm;
            _afterLbl.Caption = Resources.AfterLblm;
            _fuelingLbl.Caption = Resources.LoadingLbl;
            _dischargeLbl.Caption = Resources.DischargeLblm;
            _totalLbl.Caption = Resources.TotalLblm;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;
        }

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            ReportsControl.GraphClearSeries();
            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphGrain(Graph, dataset, mobitel_row);
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);

                try
                {
                    CreateGraph(mobitel, AlgorithmType.GRAIN1);
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in GrainControl: {0}", ex.Message);
                    throw new NotImplementedException(message);
                }
            } // if
        } // SelectGraphic

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                
                if (_summaries.ContainsKey(mobitel.Mobitel_ID))
                {
                    // if( atlantaDataSetBindingSource.Count > 0 )
                    {
                        grainDataGridControl.DataSource = atlantaDataSetBindingSource;
                        atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);
                        // UpdateStatusLine();
                        EnableButton();

                        if (CurrentActivateReport == NameReport)
                            SelectGraphic(curMrow);
                    }

                    //EnableEditButton();
                    UpdateStatusLine();
                    bbiPrintReport.Enabled = true;
                }
                else
                {
                    DisableButton();
                    //DisableEditButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        /// <summary>
        /// Построение отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                flagStart = false;
                SetStopButton();
                BeginReport();
                _stopRun = false;
                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();
                //atlantaDataSet.Grain_ReportDataTable

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;

                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                if (curMrow == null)
                {
                    return;
                }

                Select(curMrow);
                SetStartButton();
                //EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();

                SetStartButton();
            }

            flagStart = true;
        } // bbiStart_ItemClick

        /* функция для формирования верхней/средней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingGrain.GetInfoStructure;
            if (info.BreakMeedle)
            {
                ReportingGrain.SetRectangleBrckUP(450, 10, 200, 70);
                return (Resources.BeginLoading + ": " + info.totalFuel.ToString("N2") + "\n" +
                        Resources.GrainEnd + ": " + info.FuelStops.ToString("N2") + "\n" +
                        Resources.GrainLbl + " " + info.Fueling.ToString("N2") + "\n" +
                        Resources.DischargeLblm + " " + delsign(info.totalFuelSub).ToString("N2"));
            }
            else
            {
                return "";
            }
        } // GetStringBreackUp

        /* функция для формирования левой части заголовка отчета */

        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingGrain.GetInfoStructure;
            if (info.BreakLeft)
            {
                return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                        Resources.Driver + ": " + info.infoDriverName);
            }
            else
            {
                return "";
            }
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */

        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingGrain.GetInfoStructure;
            if (info.BreakRight)
            {
                ReportingGrain.SetRectangleBrckRight(900, 10, 200, 70);
                return (Resources.TotalLblm + " " + info.totalFuelAdd.ToString("N2") + "\n" +
                        Resources.AvgSpeedLbl + " " + info.MotionTreavelTime.ToString("N2"));
            }

            return "";
        }



        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportGrain, e);
            TInfoDut info = ReportingGrain.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        private static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.Grain_ReportRow f_row, out string val,
            out Color color)
        {
            val = String.Format("{0:#.#}{1}", f_row.dValue, Resources.MassAbbreviation);

            if (f_row.dValue > 0)
            {
                val = String.Format("{0} {1}", Resources.Loading, val);
                color = Color.Green;
            }
            else
            {
                val = String.Format("{0} {1}", Resources.GrainUnloading, val);
                color = Color.Red;
            }
        }

        private void grainDataGridView_CellFormatting(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "dValue")
            {
                if (e.Value is DBNull || e.Value == null)
                {
                    return;
                }

                if ((double) e.Value > 0.0)
                {
                    e.Column.AppearanceCell.ForeColor = Color.Green;
                }
                else
                {
                    e.Column.AppearanceCell.ForeColor = Color.Red;
                }
            } // if

        } // grainDataGridView_CellFormatting


        private void grainDataGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {

            if (e.RowHandle >= 0)
            {
                GridView view = sender as GridView;
                if (e.Column.FieldName == "dValue")
                {
                    double dbValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["dValue"]));
                    if (dbValue > 0.0)
                    {
                        e.Appearance.ForeColor = Color.Green;
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    Int64 idDataGps = Convert.ToInt64(view.GetRowCellValue(e.RowHandle, view.Columns["DataGPS_ID"]));
                    if (idDataGps > 0)
                    {
                        if (buildGraph.IsInclinometerDangered(dataset, curMrow, idDataGps))
                        {
                            e.Appearance.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        protected override void ExportAllDevToReportExtend()
        {
            colGenerateType.Visible = false;
            colHuman.Visible = true;
            colIdentification.Visible = true;
            colMarking.Visible = true;
            colNumberVeh.Visible = true;

            colHuman.VisibleIndex = 0;
            colNumberVeh.VisibleIndex = 1;
            colMarking.VisibleIndex = 2;
            colIdentification.VisibleIndex = 3;
            _location.VisibleIndex = 4;
            coldate.VisibleIndex = 5;
            coltime.VisibleIndex = 6;
            _quantity.VisibleIndex = 7;
            _beginValue.VisibleIndex = 8;
            _endValue.VisibleIndex = 9;

            ReportingGrain.CreateBindDataList();
            ReportingGrain.CreateElementReport();

            foreach (int mobitelId in _summaries.Keys)
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                //VehicleInfo info = new VehicleInfo(mobitel);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль

                if (info.DriverName == " ")
                {
                    tfinfo.infoDriverName = "------"; //+ Водитель
                }
                else
                {
                    tfinfo.infoDriverName = info.DriverFullName;
                }

                tfinfo.totalFuel = _summaries[mobitelId].Before; // +кг зерна в начале
                tfinfo.FuelStops = _summaries[mobitelId].After; // кг зерна в конце
                tfinfo.Fueling = _summaries[mobitelId].Graining; // Загружено кг зерна всего
                tfinfo.totalFuelSub = _summaries[mobitelId].Discharge; // +Разгружено кг зерна всего
                tfinfo.totalFuelAdd = _summaries[mobitelId].Total; // Всего кг зеорна прошло через ТС
                tfinfo.MotionTreavelTime = _summaries[mobitelId].AvgSpeed; // +Средняя скорость движения
                tfinfo.registerNumber = info.RegistrationNumber;
                tfinfo.markVehicle = info.CarMaker;
                tfinfo.Identification = info.Identificator;

                tfinfo.BreakLeft = false;
                tfinfo.BreakMeedle = false;
                tfinfo.BreakRight = false;

                ReportingGrain.AddInfoStructToList(tfinfo);

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId, "id ASC"))
                {
                    string humanVeh = tfinfo.infoDriverName;
                    string numberVehicle = tfinfo.registerNumber;
                    string markingVehicle = tfinfo.markVehicle;
                    int identificator = tfinfo.Identification;
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;

                    ReportingGrain.AddDataToBindList(new reportGrain(humanVeh, numberVehicle, markingVehicle,
                        identificator, location, date, time, value,
                        begin, end, genertype, 0));
                } // foreach 1
            } // foreach 2

            ReportingGrain.CreateAndShowReport();
            ReportingGrain.DeleteData();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;
            colGenerateType.Visible = true;
        }

        protected override void ExportAllDevToReport()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            foreach (int mobitelId in _summaries.Keys)
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль
                tfinfo.infoDriverName = info.DriverFullName; //+ Водитель
                tfinfo.totalFuel = _summaries[mobitelId].Before; // +кг зерна в начале
                tfinfo.FuelStops = _summaries[mobitelId].After; // кг зерна в конце
                tfinfo.Fueling = _summaries[mobitelId].Graining; // загружено кг зерна всего
                tfinfo.totalFuelSub = _summaries[mobitelId].Discharge; // +Разгружено кг зерна всего
                tfinfo.totalFuelAdd = _summaries[mobitelId].Total; // Всего кг зерна прошло через ТС
                tfinfo.MotionTreavelTime = _summaries[mobitelId].AvgSpeed; // +Средняя скорость движения

                tfinfo.BreakLeft = true;
                tfinfo.BreakMeedle = true;
                tfinfo.BreakRight = true;

                ReportingGrain.AddInfoStructToList(tfinfo);
                ReportingGrain.CreateBindDataList();

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId, "id ASC"))
                {
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;

                    ReportingGrain.AddDataToBindList(new reportGrain(location, date, time, value, begin, end, genertype, 0));
                } // foreach 1

                ReportingGrain.CreateElementReport();
            } // foreach 2

            ReportingGrain.CreateAndShowReport();
            ReportingGrain.DeleteData();
        }

        protected override void ExportToExcelDevExpress()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            TInfoDut tfinfo = new TInfoDut();

            tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
            tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(curMrow.Mobitel_ID);
            VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

            tfinfo.infoVehicle = info.Info; // Автомобиль
            tfinfo.infoDriverName = info.DriverFullName; // Водитель
            tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Before; // Кг зерна в начале
            tfinfo.FuelStops = _summaries[curMrow.Mobitel_ID].After; // Кг зерна в конце
            tfinfo.Fueling = _summaries[curMrow.Mobitel_ID].Graining; // Кг зерна всего
            tfinfo.totalFuelSub = _summaries[curMrow.Mobitel_ID].Discharge; // Разгружено кг зерна всего
            tfinfo.totalFuelAdd = _summaries[curMrow.Mobitel_ID].Total; // Всего кг разгружено автомобилем
            tfinfo.MotionTreavelTime = _summaries[curMrow.Mobitel_ID].AvgSpeed; // Средняя скорость движения

            tfinfo.BreakLeft = true;
            tfinfo.BreakMeedle = true;
            tfinfo.BreakRight = true;

            ReportingGrain.AddInfoStructToList(tfinfo); /* формируем заголовки таблиц отчета */
            ReportingGrain.CreateAndShowReport(grainDataGridControl);
        }

        private void GrainControl_Load(object sender, EventArgs e)
        {
            // to do
        }

        /// <summary>
        /// Обрабатывает выбор точек на графике при добавлении новой строки к отчету
        /// </summary>
        private void ZGraphControl_Action(object sender, ActionCancelEventArgs ev)
        {
            if (grain_row == null)
                return;

            if (!begin_end)
            {
                GpsData gpsData = DataSetManager.GetDataGpsArray(curMrow).First(gps => gps.Time == ev.Point.time);
                grain_row.beginValue = ev.Point.value;
                grain_row.time_ = ev.Point.time;
                grain_row.date_ = ev.Point.time.ToShortDateString();
                grain_row.Times = ev.Point.time.ToLongTimeString();
                grain_row.DataGPS_ID = gpsData.Id;
                grain_row.Location = Algorithm.FindLocation(gpsData.LatLng);
                grain_row.mobitel_id = gpsData.Mobitel;
                grain_row.sensor_id = 0;

                foreach (atlantaDataSet.sensorsRow s_row in curMrow.GetsensorsRows())
                {
                    if (s_row != null)
                    {
                        foreach (atlantaDataSet.relationalgorithmsRow ra_row in s_row.GetrelationalgorithmsRows())
                        {
                            if ((AlgorithmType) ra_row.AlgorithmID == AlgorithmType.GRAIN1)
                            {
                                grain_row.sensor_id = s_row.id;
                            } // if
                        } // foreach
                    } // if
                } // foreach
                begin_end = true;
                XtraMessageBox.Show(Resources.GrainCtrlAddLoadingFinishText, Resources.GrainCtrlAddLoadingCaption);
            } // if
            else
            {
                grain_row.endValue = ev.Point.value;
                grain_row.dValue = grain_row.endValue - grain_row.beginValue;
                Grain.Summary s = _summaries[curMrow.Mobitel_ID];
                if (grain_row.dValue > 0)
                {
                    s.Graining += grain_row.dValue;
                    s.Total += grain_row.dValue;
                } // if
                else
                {
                    s.Discharge += grain_row.dValue;
                }

                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);

                UpdateStatusLine();
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.ResetBindings(false);
                atlantaDataSetBindingSource.EndEdit();
                graph.RemoveMouseEventHandler();
                Graph.Action -= ZGraphControl_Action;
                begin_end = false;
                ReportsControl.OnGraphShowNeeded();
                UpdateZGraph();
            } // else
        } // ZGraphControl_Action

        /// <summary>
        /// Осуществляет пересчет в редактируемой строке отчета и итогов
        /// </summary>
        private void grainDataGridView_CellEndEdit(object sender, CellValueChangedEventArgs e)
        {
            string name = e.Column.Name; // Название редактируемой колонки

            if (name == "_time")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                string time = row.time_.ToString("dd.MM.yyyy HH:mm:ss");

                grainDataGridView.SetRowCellValue(e.RowHandle, "time_", time);
            }

            if (name == "_endValue" || name == "_beginValue")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                Grain.Summary s = _summaries[curMrow.Mobitel_ID];

                if (row.dValue > 0)
                {
                    s.Graining -= row.dValue;
                    s.Total -= row.dValue;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.dValue = row.endValue - row.beginValue;

                if (row.dValue > 0)
                {
                    s.Graining += row.dValue;
                    s.Total += row.dValue;
                }
                else
                {
                    s.Discharge += row.dValue;
                }

                s.Rate = s.Total*100/curMrow.path;
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
            } // if

            atlantaDataSetBindingSource.ResumeBinding();
            atlantaDataSetBindingSource.ResetBindings(false);
            atlantaDataSetBindingSource.EndEdit();
            graph.RemoveMouseEventHandler();
            UpdateStatusLine();
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        } // GrainDataGridView_CellEndEdit

        private void connav_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Append)
            {
                bindingNavigatorAddNewItem_Click();
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                bindingNavigatorDeleteItem_Click();
            }

            e.Handled = true;
        }

        /// <summary>
        /// Добавляет к отчету новую строку
        /// </summary>
        protected void bindingNavigatorAddNewItem_Click()
        {
            ReportsControl.OnGraphShowNeeded();

            grain_row = (atlantaDataSet.FuelReportRow)
                ((DataRowView) atlantaDataSetBindingSource.AddNew()).Row;

            grain_row["GenerateType"] = "manual";

            numberChangedRow = grainDataGridView.FocusedRowHandle;

            Graph.Action += new ActionEventHandler(ZGraphControl_Action);
            graph.AddMouseEventHandler();
            XtraMessageBox.Show(Resources.GrainCtrlAddLoadingStartText, Resources.GrainCtrlAddLoadingCaption);
        } // bindingNavigatorAddNewItem_Click

        /// <summary>
        /// Удаляет из отчета выделенную строку
        /// </summary>
        protected void bindingNavigatorDeleteItem_Click()
        {
            atlantaDataSet.FuelReportRow row =
                (atlantaDataSet.FuelReportRow) ((DataRowView) atlantaDataSetBindingSource.Current).Row;
            Grain.Summary s = _summaries[curMrow.Mobitel_ID];

            // Необходимо оставить последнюю строку в отчете, чтобы сводные отчеты
            // не строили данный отчет заново
            if (curMrow.GetFuelerReportRows().Length == 1)
            {
                row.beginValue = 0;
                row.endValue = 0;
                row.dValue = 0;
                s.Graining = 0;
                s.Discharge = 0;
                s.Total = s.Before - s.After;
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
                UpdateStatusLine();
                XtraMessageBox.Show(Resources.FuelCtrlDeleteError, Resources.Warning);
            }
            else
            {
                if (row.dValue > 0)
                {
                    s.Graining -= row.dValue;
                    s.Total -= row.dValue;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.Delete();
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
                UpdateStatusLine();
            } // else
        } // bindingNavigatorDeleteItem_Click

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine()
        {
            Grain.Summary s = _summaries[curMrow.Mobitel_ID];

            _beforeLbl.Caption = Resources.BeforeLblm + " " + s.Before.ToString("N0");
            _afterLbl.Caption = Resources.AfterLblm + " " + s.After.ToString("N0");
            _fuelingLbl.Caption = Resources.GrainLbl + " " + s.Graining.ToString("N0");
            _dischargeLbl.Caption = Resources.DischargeLblm + " " + delsign(s.Discharge).ToString("N0");
            _totalLbl.Caption = Resources.TotalLblm + " " + s.Total.ToString("N0");
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s.AvgSpeed.ToString("N2");
        }

        protected double delsign(double s)
        {
            if (s < 0.0)
                return -s;

            return s;
        }

        /// <summary>
        /// Осуществляет валидацию вводимых пользователем данных
        /// </summary>
        private void grainDataGridView_CellValidating(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if ((grainDataGridView.FocusedColumn.Name == "_endValue") ||
                (grainDataGridView.FocusedColumn.Name == "_beginValue"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.GrainCtrlLoadingValueError, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    double new_double;
                    if (!Double.TryParse(e.Value.ToString(), out new_double))
                    {
                        XtraMessageBox.Show(Resources.GrainCtrlLoadingValueError, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // if
            else if ((grainDataGridView.FocusedColumn.Name == "_time"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    DateTime new_time;
                    if (!DateTime.TryParse(e.Value.ToString(), out new_time))
                    {
                        XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // else if
        } // grainDataGridView_CellValidating

        private void grainDataGridView_CellMouseDoubleClick(object sender, RowCellClickEventArgs e)
        {
            try
            {
                ReportsControl.OnClearMapObjectsNeeded();

                if (e.RowHandle < 0)
                    return;

                DateTime time_;
                // Дата время
                object tm = grainDataGridView.GetRowCellValue(e.RowHandle, e.Column);
                if (!DateTime.TryParse(Convert.ToString(tm), out time_))
                    return;

                int index = grainDataGridView.GetDataSourceRowIndex(e.RowHandle);

                List<Marker> markers = new List<Marker>();

                atlantaDataSet.Grain_ReportRow f_row = (atlantaDataSet.Grain_ReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index]).Row;

                double value = f_row.dValue;
                string message = String.Format("{0}: {1:N2} {2}.",
                    value > 0 ? Resources.Loading : Resources.GrainLarceny,
                    value, Resources.MassAbbreviation);
                PointLatLng location = Algorithm.GpsDatas.First(gps => gps.Id == f_row.DataGPS_ID).LatLng;
                markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                ReportsControl.OnMarkersShowNeeded(markers);
                UpdateZGraph();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Grain Control", MessageBoxButtons.OK);
                return;
            }
        } // GrainDataGridView_CellMouseDoubleClick

        public override string Caption
        {
            get { return "Уровень зерна"; }
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(grainDataGridView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(grainDataGridView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(grainDataGridControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar2);
        }

        public void Localization()
        {
            _location.Caption = Resources.Location;
            _location.ToolTip = Resources.LocationTooltip;

            coldate.Caption = Resources.Date;
            coldate.ToolTip = Resources.LoadingDate;

            _quantity.Caption = Resources.LoadingValue;
            _quantity.ToolTip = Resources.LoadingValueHint;

            _beginValue.Caption = Resources.GrainLevelBeforeLoading;
            _beginValue.ToolTip = Resources.GrainLevelBeforeLoadingHint;

            _endValue.Caption = Resources.GrainLevelAfterLoading;
            _endValue.ToolTip = Resources.GrainLevelAfterLoadingHint;

            colHuman.Caption = Resources.ColHumanVehicle;
            colHuman.ToolTip = Resources.ColHumanVehicleTip;
            colNumberVeh.Caption = Resources.ColNumVehicle;
            colNumberVeh.ToolTip = Resources.ColNumVehicleTip;
            colMarking.Caption = Resources.ColMarking;
            colMarking.ToolTip = Resources.ColMarkingTip;
            colIdentification.Caption = Resources.ColIdent;
            colIdentification.ToolTip = Resources.ColIdentTip;
            coltime.Caption = Resources.ColTime;
            coltime.ToolTip = Resources.LoadingColTimeTip;

            _beforeLbl.Caption = Resources.BeforeLblm;
            _afterLbl.Caption = Resources.AfterLblm;
            _fuelingLbl.Caption = Resources.GrainLbl;
            _dischargeLbl.Caption = Resources.DischargeLblm;
            _totalLbl.Caption = Resources.TotalLblm;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;

            colGenerateType.Caption = Resources.GenerateType;
            colGenerateType.ToolTip = Resources.GenerateTypeTip;
        }

        // Класс разработан для исправления вывода названий в заголовки колонок таблицы
        // во время генерации отчета
        [AttributeUsage(AttributeTargets.All)]
        public class DeveloperAttribute : DisplayNameAttribute
        {
            //Private fields.
            private string colHuman;
            private string colNumberVeh;
            private string colMarking;
            private string colIdentification;
            private string _location;
            private string coldate;
            private string coltime;
            private string _quantity;
            private string _beginvalue;
            private string _endvalue;
            private string _colgener;
            private int val;

            //This constructor defines two required parameters: name and level.
            public DeveloperAttribute(int val)
            {
                this.val = val;
                this.colHuman = Resources.ColHumanVehicle;
                this.colNumberVeh = Resources.ColNumVehicle;
                this.colMarking = Resources.ColMarking;
                this.colIdentification = Resources.ColIdent;
                this._location = Resources.Location;
                this.coldate = Resources.ColDate;
                this.coltime = Resources.ColTime;
                this._quantity = Resources.LoadingValue;
                this._beginvalue = Resources.GrainLevelBeforeLoading;
                this._endvalue = Resources.GrainLevelAfterLoading;
                this._colgener = Resources.GenerateType;
            }

            //Define DisplayName property
            public override string DisplayName
            {
                get
                {
                    switch (val)
                    {
                        case 0:
                            return colHuman;
                        case 1:
                            return colNumberVeh;
                        case 2:
                            return colMarking;
                        case 3:
                            return colIdentification;
                        case 4:
                            return this._location;
                        case 5:
                            return coldate;
                        case 6:
                            return coltime;
                        case 7:
                            return this._quantity;
                        case 8:
                            return this._beginvalue;
                        case 9:
                            return this._endvalue;
                        case 10:
                            return this._colgener;
                        default:
                            throw new Exception("This unknown property!");
                    } // switch
                } // get
            } // DisplayName
        } // DeveloperAttribute
    } // GrainLevel
} // BaseReports
