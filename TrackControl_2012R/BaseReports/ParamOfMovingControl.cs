using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using LocalCache;
using BaseReports.Procedure;
using BaseReports.Properties;
using C1.C1Excel;
using TrackControl.General;

namespace BaseReports
{
  [ToolboxItem(false)]
  public partial class ParamOfMovingControl : ReportTabControl
  {
    #region -- .ctor --
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="caption"></param>
    public ParamOfMovingControl(string caption)
      : base(caption)
    {
      InitializeComponent();
      Init();

      excelPattern = "ParamsMoving.xls";
      AddAlgorithm(new ParamOfMovingAlgorithm());
    }

    /// <summary>
    /// ����������� ��� ���������� ��� �������� ������ "��������� ��������".
    /// </summary>
    public ParamOfMovingControl()
      : this(Resources.ParamsOfMoving)
    {
    }

    private void Init()
    {
      atlantaDataSetBindingSource.DataSource = dataset;
      atlantaDataSetBindingSource.DataMember = "TrafficDetailReport";
      trafficDetailReportDataGridView.DataSource = atlantaDataSetBindingSource;
      SelectFieldGrid(ref trafficDetailReportDataGridView);
      atlantaDataSet = null;

      hideExcelSplitBtn();
      base.showMapToolStripButton.Enabled = true;
      base.ShowGraphToolStripButton.Enabled = false;

      ColumnLocation.HeaderText = Resources.Location;
      ColumnDescribeEvent.HeaderText = Resources.Event;
      ColumnTime.HeaderText = Resources.DateTime;
      ColumnDuration.HeaderText = Resources.Duration;
      ColumnMaxValue.HeaderText = Resources.MaxValue;
      ColumnMiddleValue.HeaderText = Resources.AvgValue;
    }
    #endregion

    protected override void runToolStripButton_Click(object sender, EventArgs e)
    {
      dataset.TrafficDetailReport.Clear();

      base.runToolStripButton_Click(sender, e);

      Select(curMrow);
      printToolStripButton.Enabled = false;
    }

    public override void Select(atlantaDataSet.mobitelsRow m_row)
    {
      base.Select(m_row);
    }

    void trafficDetailReportDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      ParamOfMovingAlgorithm pm = (ParamOfMovingAlgorithm)base.algoritms[0];

      if (((string)trafficDetailReportDataGridView[trafficDetailReportDataGridView.Columns["ColumnDescribeEvent"].Index, e.RowIndex].Value)
        .IndexOf(Resources.Speeding) == 0)
      {
        e.CellStyle.BackColor = Color.LightCoral;
      }
      if (((string)trafficDetailReportDataGridView[trafficDetailReportDataGridView.Columns["ColumnDescribeEvent"].Index, e.RowIndex].Value)
        .IndexOf(Resources.CriticalAcceleration) == 0)
      {
        e.CellStyle.BackColor = Color.Khaki;
      }
      if (((string)trafficDetailReportDataGridView[trafficDetailReportDataGridView.Columns["ColumnDescribeEvent"].Index, e.RowIndex].Value)
        .IndexOf(Resources.CriticalBraking) == 0)
      {
        e.CellStyle.BackColor = Color.DarkSalmon;
      }
    }

    protected override void showMapToolStripButton_Click(object sender, EventArgs e)
    {
      if (trafficDetailReportDataGridView.CurrentRow != null)
      {
        ShowOnMap(trafficDetailReportDataGridView.CurrentRow.Index);
      }
    }

    protected override void ShowGraphToolStripButton_Click(object sender, EventArgs e)
    {
      /*
      Graph.ClearRegion();
      foreach (atlantaDataSet.StopReportRow s_row in curMrow.GetStopReportRows())
      {
        Graph.AddTimeRegion(s_row.beginTime, s_row.endTime);
      }
       * */
    }

    /// <summary>
    /// ������������ ������ � Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected override void ExcelToolStripButton_Click(object sender, EventArgs e)
    {
      LoadPatternExcel();
      XLSheet sheet = base.c1XLBook.Sheets[0];
      sheet[1, 2].Value = Algorithm.Period.Begin;
      sheet[1, 4].Value = Algorithm.Period.End;

      sheet[3, 2].Value = vehicleInfo.Info;
      sheet[4, 2].Value = vehicleInfo.DriverFullName;
      int i = 8;
      int numberAcc = 0, numberBreak = 0;
      TimeSpan timeOverSpeed = new TimeSpan(0);
        //Mobitel_id
      foreach (atlantaDataSet.TrafficDetailReportRow row in dataset.TrafficDetailReport.Select("Mobitel_id = " + curMrow.Mobitel_ID))
      {
        if (!row.IsLocationNull())
        {
          sheet[i, 1].Value = row.Location;
          sheet[i, 1].Style = sheet[8, 1].Style;
        }
        if (!row.IsDescribeEventNull())
        {
          sheet[i, 2].Value = row.DescribeEvent;
          sheet[i, 2].Style = sheet[8, 2].Style;

          if (row.DescribeEvent.IndexOf(Resources.Speeding) == 0)
          {
            timeOverSpeed += row.Duration;
          }
          if (row.DescribeEvent.IndexOf(Resources.CriticalAcceleration) == 0)
          {
            numberAcc++;
          }
          if (row.DescribeEvent.IndexOf(Resources.CriticalBraking) == 0)
          {
            numberBreak++;
          }
        }
        if (!row.IsTimeNull())
        {
          sheet[i, 3].Value = row.Time;
          sheet[i, 3].Style = sheet[8, 3].Style;
        }
        if (!row.IsDurationNull())
        {
          sheet[i, 4].Value = row.Duration;
          sheet[i, 4].Style = sheet[8, 4].Style;
        }
        if (!row.IsMaxValueNull())
        {
          sheet[i, 5].Value = row.MaxValue;
          sheet[i, 5].Style = sheet[8, 5].Style;
        }
        if (!row.IsMiddleValueNull())
        {
          sheet[i, 6].Value = row.MiddleValue;
          sheet[i, 6].Style = sheet[8, 6].Style;
        }
        sheet[3, 6].Value = timeOverSpeed;
        sheet[4, 6].Value = numberAcc;
        sheet[5, 6].Value = numberBreak;

        i++;
      }
      sheet.Name = vehicleInfo.RegistrationNumber;
      SaveExcel(Resources.ParamsOfMoving, true);
    }

    void ShowOnMap(int intex)
    {
      ReportsControl.OnClearMapObjectsNeeded();
      if (intex < 0)
        return;

      atlantaDataSet.TrafficDetailReportRow row = (atlantaDataSet.TrafficDetailReportRow)
        ((DataRowView)atlantaDataSetBindingSource.List[intex]).Row;
      atlantaDataSet.dataviewRow dv_row = dataset.dataview.FindByDataGps_ID(row.DataGps_ID);

      string message = String.Format(Resources.ParamOfMovingCtrlDuration,
        row.DescribeEvent, Environment.NewLine, row.Duration);
      PointLatLng location = new PointLatLng(dv_row.Lat + 0.0002, dv_row.Lon + 0.0002);
      List<Marker> markers = new List<Marker>();
      markers.Add(new Marker(MarkerType.Alarm, row.Mobitel_id, location, message, ""));
      ReportsControl.OnMarkersShowNeeded(markers);

      List<IGeoPoint> data = new List<IGeoPoint>();
      bool inside = false;
      foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select("Mobitel_ID=" + row.Mobitel_id, "time ASC"))
      {
        if (d_row.DataGps_ID == row.DataGps_ID)
          inside = true;
        if (inside)
          data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
        if (d_row.DataGps_ID == row.FinalPointId)
          break;
      }
      if (data.Count > 0)
      {
        List<Track> segments = new List<Track>();
        segments.Add(new Track(row.Mobitel_id, Color.Red, 2f, data));
        ReportsControl.OnTrackSegmentsShowNeeded(segments);
      }
    }

    void trafficDetailReportDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      ShowOnMap(e.RowIndex);
    }
  }
}

