using System;

namespace BaseReports.EventTrack
{
    /// <summary>
    /// �����, ��������������� ������ � �������� ������� �� ������ ��������� � ������.
    /// ����� ������� �������� ��������, ������� ������������� ��������.
    /// </summary>
    public class Transition
    {
        #region Fields
        /// <summary>
        /// ID ��������.
        /// </summary>
        private int _id;
        /// <summary>
        /// ��������� ��������� �������.
        /// </summary>
        private State _initialState;
        /// <summary>
        /// �������� ��������� ������� (���������
        /// ������� ����� ��������).
        /// </summary>
        private State _finalState;
        /// <summary>
        /// �������� ��������.
        /// </summary>
        private string _title;
        /// <summary>
        /// �������� ������, ��������������� � ������ ���������.
        /// </summary>
        private string _iconName;
        /// <summary>
        /// �������� ��������� �������, ���������������� � ������ ���������.
        /// </summary>
        private string _soundName;

        #endregion

        #region .ctor
        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="id">ID ��������</param>
        /// <param name="initialState">��������� ��������� �������</param>
        /// <param name="finalState">�������� ��������� �������</param>
        /// <param name="title">�������� ��������</param>
        /// <param name="iconName">�������� ������, ��������������� � ������ ���������</param>
        /// <param name="soundName">�������� ��������� �������, ���������������� � ������ ���������</param>
        public Transition(int id, State initialState, State finalState, string title, string iconName, string soundName)
        {
            _id = id;
            _initialState = initialState;
            _finalState = finalState;
            _title = title;
            _iconName = iconName;
            _soundName = soundName;
        }
        #endregion

        #region Properties
        /// <summary>
        /// ID ��������.
        /// </summary>
        public int Id
        {
            get { return _id; }
        }
        /// <summary>
        /// ��������� ��������� �������.
        /// </summary>
        public State InitialState
        {
            get { return _initialState; }
        }
        /// <summary>
        /// �������� ��������� �������.
        /// </summary>
        public State FinalState
        {
            get { return _finalState; }
        }
        /// <summary>
        /// �������� ��������.
        /// </summary>
        public string Title
        {
            get { return _title; }
        }
        /// <summary>
        /// �������� ������, ��������������� � ������ ���������.
        /// </summary>
        public string IconName
        {
            get { return _iconName; }
        }
        /// <summary>
        /// �������� ��������� �������, ���������������� � ������ ���������.
        /// </summary>
        public string SoundName
        {
            get { return _soundName; }
        }
        #endregion
    }
}
