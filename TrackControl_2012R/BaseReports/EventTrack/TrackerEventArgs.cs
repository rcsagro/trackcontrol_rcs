using System;
using TrackControl.General;

namespace BaseReports.EventTrack
{
    public class TrackerEventArgs : EventArgs
    {
        #region .ctor

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ BaseReports.EventTrack.TrackerEventArgs
        /// </summary>
        /// <param name="latLng">���������� ����� ������������� �������</param>
        /// <param name="time">����� ������������� �������</param>
        /// <param name="description">�������� �������</param>
        /// <param name="carInfo">���������� � ������������ ��������</param>
        /// <param name="driverInfo">��� �������� ������������� ��������</param>
        /// <param name="iconName">�������� ������, ��������������� � ��������.</param>
        /// <param name="soundName">�������� ��������� �������, ���������������� � ��������.</param>
        public TrackerEventArgs(PointLatLng latLng, DateTime time, string description, string carInfo, string driverInfo,
            string iconName, string soundName)
        {
            _latLng = latLng;
            _time = time;
            _description = description;
            _carInfo = carInfo;
            _driverInfo = driverInfo;
            _iconName = iconName;
            _soundName = soundName;
            _durMotion = new TimeSpan();
            _durStop = new TimeSpan();
        }

        #endregion

        #region Properties

        /// <summary>
        /// ���������� ����� ������������� �������.
        /// </summary>
        public PointLatLng LatLng
        {
            get { return _latLng; }
        }

        private PointLatLng _latLng;

        /// <summary>
        /// ����� ������������� �������.
        /// </summary>
        public DateTime Time
        {
            get { return _time; }
        }

        private DateTime _time;

        /// <summary>
        /// �������� �������.
        /// </summary>
        public string Description
        {
            get { return _description; }
        }

        private string _description;

        /// <summary>
        /// ���������� � ������������ ��������.
        /// </summary>
        public string CarInfo
        {
            get { return _carInfo; }
        }

        private string _carInfo;

        /// <summary>
        /// ��� �������� ������������� ��������
        /// </summary>
        public string DriverInfo
        {
            get { return _driverInfo; }
        }

        private string _driverInfo;

        /// <summary>
        /// �������� ������, ��������������� � ��������.
        /// </summary>
        public string IconName
        {
            get { return _iconName; }
        }

        private string _iconName;

        /// <summary>
        /// �������� ��������� �������, ���������������� � ��������.
        /// </summary>
        public string SoundName
        {
            get { return _soundName; }
        }

        private string _soundName;

        /// <summary>
        /// ������������ ���������� � ����� ���������
        /// </summary>
        public TimeSpan Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        private TimeSpan _duration;

        /// <summary>
        /// ������������ ���������� � ����� ��������� �� ����� ��������
        /// </summary>
        public TimeSpan _durationMotion
        {
            get { return _durMotion; }
            set { _durMotion = value; }
        }

        private TimeSpan _durMotion;

        /// <summary>
        /// ������������ ���������� � ����� ���������
        /// </summary>
        public TimeSpan _durationStop
        {
            get { return _durStop; }
            set { _durStop = value; }
        }

        private TimeSpan _durStop;


        /// <summary>
        /// ���������� ���� � ����� ���������
        /// </summary>
        public Double Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }

        private Double _distance;

        /// <summary>
        /// �������� �������, �� �������� ������������� ������ �������
        /// </summary>
        public string SensorName
        {
            get { return _sensorName; }
            set { _sensorName = value; }
        }

        private string _sensorName;

        /// <summary>
        /// �������� ������������� �������� �� ����� ������������� ������
        /// </summary>
        public double Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        private double _speed;

        public double SpeedAvg
        {
            get
            {
                double th = _duration.TotalHours;
                if (th == 0)
                    return 0;
                else
                    return Distance/th;
            }
        }

        /// <summary>
        /// ��������� ������������� �������� ����� �������
        /// </summary>
        internal State State = null;

        /// <summary>
        /// ID ������� �������
        /// </summary>
        internal int Id = 0;

        #endregion
    }
}
