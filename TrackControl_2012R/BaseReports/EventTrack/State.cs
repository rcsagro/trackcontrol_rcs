using System;
using LocalCache;

namespace BaseReports.EventTrack
{
    /// <summary>
    /// �����, ��������������� ��������� �������, � �������� ��������� ��������.
    /// ��������� ������������ ����������� ������������� �������.
    /// </summary>
    public class State
    {
        #region Fields

        /// <summary>
        /// ID ���������.
        /// </summary>
        private int _id;

        /// <summary>
        /// ������, ��������� �������� ���������� ��������� �������.
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        /// <summary>
        /// ����������� �������� ��������� ��������� ������� ��� ������� ���������.
        /// </summary>
        private double _minValue;

        /// <summary>
        /// ������������ �������� ��������� ��������� ������� ��� ������� ���������.
        /// </summary>
        private double _maxValue;

        /// <summary>
        /// �������� ��������� �������.
        /// </summary>
        private string _title;

        #endregion

        #region .ctor

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="id">ID ���������.</param>
        /// <param name="sensor">������, ��������� �������� ���������� ��������� �������.</param>
        /// <param name="minValue">����������� �������� ��������� ������� ��� ������� ���������.</param>
        /// <param name="maxValue">������������ �������� ��������� ��������� ������� ��� ������� ���������.</param>
        /// <param name="title">�������� ��������� �������.</param>
        public State(
            int id,
            atlantaDataSet.sensorsRow sensor,
            double minValue,
            double maxValue,
            string title)
        {
            _id = id;
            _sensor = sensor;
            _minValue = minValue;
            _maxValue = maxValue;
            _title = title;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ID ���������.
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// �������, ��������� �������� ���������� ��������� �������.
        /// </summary>
        public atlantaDataSet.sensorsRow Sensor
        {
            get { return _sensor; }
        }

        /// <summary>
        /// ����������� �������� ��������� ������� ��� ������� ���������.
        /// </summary>
        public double MinValue
        {
            get { return _minValue; }
        }

        /// <summary>
        /// ������������ �������� ��������� ��������� ������� ��� ������� ���������.
        /// </summary>
        public double MaxValue
        {
            get { return _maxValue; }
        }

        /// <summary>
        /// �������� ��������� �������.
        /// </summary>
        public string Title
        {
            get { return _title; }
        }

        /// <summary>
        /// ���������� �������������� ���������
        /// </summary>
        public static State Empty
        {
            get { return new State(-1, null, 0.0, 0.0, ""); }
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (((State)obj).Id == _id)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(State s1, State s2)
        {
            if (((object)s1) == null && ((object)s2) == null)
                return true;
            if (((object)s1) != null)
                return s1.Equals(s2);
            else
                return s2.Equals(s1);
        }

        public static bool operator !=(State s1, State s2)
        {
            return !(s1 == s2);
        }

        #endregion
    }
}