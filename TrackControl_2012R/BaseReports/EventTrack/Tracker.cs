using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraScheduler.UI;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.General;
using TrackControl.Reports;

namespace BaseReports.EventTrack
{
    /// <summary>
    /// ����� ��� ������������ ��������� ������������� ����.
    /// </summary>
    public class Tracker
    {
        #region Fields

        /// <summary>
        /// ������� ��������� �������
        /// </summary>
        private State _state;

        /// <summary>
        /// ���� ���������, ������� ����������� ������ �����
        /// </summary>
        private List<Transition> _transitions;

        /// <summary>
        /// ������, �� ���������� �������� ����������� �������� ���������
        /// � ����������� ������������ � ��������� ��������
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        /// <summary>
        /// �������� ������, �� ������� ���������� ��������
        /// </summary>
        private string _carInfo;

        /// <summary>
        /// ��� �������� ������������� ��������
        /// </summary>
        private string _driverInfo;

        /// <summary>
        /// ������ ��������� �������, ��� ������� ���������� ������������ ������ ���������
        /// </summary>
        private List<TrackerEventArgs> _firedEventsList = new List<TrackerEventArgs>();

        /// <summary>
        /// ������ ��������� �������, ������� ������� ������� �� _firedEventsList
        /// </summary>
        private List<TrackerEventArgs> _eventsForRemove = new List<TrackerEventArgs>();

        /// <summary>
        /// ����� ���������� �����.
        /// </summary>
        public DateTime TimePrev { get; set; }

        /// <summary>
        /// ����������� ����� ����� ����� �������������� (����������/transient)
        /// </summary>
        public int minTimeSwitch { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// �������, ����������� ��� ����������� �������� ����� �����������
        /// </summary>
        public event EventHandler<TrackerEventArgs> Checked;

        #endregion

        #region .ctor

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ BaseReports.EventTrack.Tracker
        /// </summary>
        /// <param name="sensor">������, �� ���������� �������� ������������� �������� (�������)</param>
        public Tracker(atlantaDataSet.sensorsRow sensor, Target target)
        {
            _sensor = sensor;

            atlantaDataSet.StateRow[] stateRows = sensor.GetStateRows();

            _transitions = new List<Transition>();

            foreach (atlantaDataSet.TransitionRow transitionRow in _sensor.GetTransitionRows())
            {
                if (target == Target.Alarm && !transitionRow.Enabled)
                    continue;
                if (target == Target.Report && !transitionRow.ViewInReport)
                    continue;

                State initialState = State.Empty;
                State finalState = State.Empty;

                foreach (atlantaDataSet.StateRow stateRow in stateRows)
                {
                    if (stateRow.Id == transitionRow.InitialStateId)
                    //if( stateRow.SensorId == transitionRow.SensorId )
                        initialState = new State(stateRow.Id, sensor, stateRow.MinValue, stateRow.MaxValue, stateRow.Title);

                    if (stateRow.Id == transitionRow.FinalStateId)
                    //if( stateRow.SensorId == transitionRow.SensorId )
                        finalState = new State(stateRow.Id, sensor, stateRow.MinValue, stateRow.MaxValue, stateRow.Title);
                }

                if (initialState == State.Empty || finalState == State.Empty)
                    continue;

                Transition transition = new Transition(
                    transitionRow.Id,
                    initialState,
                    finalState,
                    transitionRow.Title,
                    transitionRow.IconName,
                    transitionRow.SoundName);
                _transitions.Add(transition);
            }

            VehicleInfo info = new VehicleInfo(_sensor.mobitelsRow);
            _carInfo = info.Info;
            if (info.Group.Length > 0)
            {
                _carInfo += String.Format("  ({0} \"{1}\")", Resources.TrackerGroup, info.Group);
            }
            else
            {
                _carInfo += String.Format("  ({0})", Resources.TrackerGroupNotSet);
            }
            _driverInfo = info.DriverFullName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ����, �����������, ���� �� ��������������� � ������ ������������ ��������.
        /// ���� ��������������� ��������� ���, �� ��� ������ � ������� ������.
        /// </summary>
        public bool IsOK
        {
            get { return _transitions.Count > 0; }
        }

        /// <summary>
        /// ������, ��������� �������� ����������� ������ ������.
        /// </summary>
        public atlantaDataSet.sensorsRow Sensor
        {
            get { return _sensor; }
        }

        /// <summary>
        /// ��� ������� �������������� �������
        /// </summary>
        public string NameSensor { get; set; }

        /// <summary>
        /// ����� ������� ����������� �������� (transient) �������� �������
        /// </summary>
        public DateTime TimeTrans { get; set; }

        #endregion

        #region Public Methods

        List<State> states = new List<State>();

        /// <summary>
        /// ����������� ��������� ������ ������ ��������� �� ������� DataView
        /// </summary>
        /// <param name="row">DataGps</param>
        public void CheckEvent(GpsData row, bool lastPoint)
        {
            foreach (TrackerEventArgs arg in _firedEventsList)
            {
                arg.Distance = arg.Distance + row.Dist;
            }

            State state = getState(row);
            if (state == null) return; 

            //if (state == null || (state == _state && !lastPoint))
            //    return; // �������� �����

            _eventsForRemove.Clear();

            foreach (TrackerEventArgs arg in _firedEventsList)
            {
                var timeDelta = row.Time - TimePrev;

                if (row.Speed <= 0.0)
                    arg._durationStop = arg._durationStop + timeDelta;

                if (row.Speed > 0.0)
                    arg._durationMotion = arg._durationMotion + timeDelta;

                TimePrev = row.Time;

                if (arg.State != state || lastPoint)
                {
                    //arg.Duration = row.Time - arg.Time;

                    //if (row.Speed <= 0.0)
                    //    arg._durationStop = arg._durationStop + arg.Duration;

                    //if (row.Speed > 0.0)
                    //    arg._durationMotion = arg._durationMotion + arg.Duration;
                    arg.Duration = arg._durationMotion + arg._durationStop;
                    _eventsForRemove.Add(arg);
                    onChecked(arg);
                }
            }

            foreach (TrackerEventArgs a in _eventsForRemove)
            {
                _firedEventsList.Remove(a);
            }

            // ����� ��������� � ��������� �������� ��� �� ����������� ����������� � ������� ������� ��������
            foreach (Transition transition in _transitions)
            {
                if ((transition.InitialState == _state) && (transition.FinalState == state))
                {
                    TimeSpan Duration = new TimeSpan(0, 0, 0, 0, 0);

                    if (TimeTrans != null)
                    {
                        Duration = row.Time - TimeTrans;
                    }

                    if (Duration.TotalSeconds < minTimeSwitch)
                        break;

                    TrackerEventArgs arg = new TrackerEventArgs(row.LatLng, row.Time, transition.Title,
                        _carInfo, _driverInfo, transition.IconName, transition.SoundName);

                    arg.Speed = row.Speed;
                    arg.SensorName = _sensor.Name;
                    arg.Distance = row.Dist;
                    arg.State = state;
                    TimePrev = row.Time;
                    TimeTrans = row.Time;

                    _firedEventsList.Add(arg);
                }
            }

            _state = state;
        }

        /// <summary>
        /// ����������� ��������� ������ ������ ��������� �� ������� Online
        /// </summary>
        /// <param name="row"></param>
        public void CheckEvent(atlantaDataSet.onlineRow row)
        {
            if (row.MobitelId != _sensor.mobitel_id)
                return;

            State state = getState(row);
            if (state == null)
            {
                return;
            }

            foreach (Transition transition in _transitions)
            {
                if (transition.InitialState == _state && transition.FinalState == state)
                {
                    PointLatLng latLng = new PointLatLng(row.Lat, row.Lon);
                    onChecked(new TrackerEventArgs(latLng, row.time, transition.Title, _carInfo, _driverInfo,
                        transition.IconName, transition.SoundName));
                }
            }
            _state = state;
        }

        /// <summary>
        /// �������������� ��������� ��������� �������.
        /// ���������� �� �����, ��������������� ������������ �������.
        /// </summary>
        /// <param name="row"></param>
        public void InitState(atlantaDataSet.onlineRow row)
        {
            if (row.MobitelId != _sensor.mobitel_id)
                return;
            State state = getState(row);
            if (state != null)
                _state = state;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ���������� ������� Checked.
        /// </summary>
        /// <param name="args"></param>
        private void onChecked(TrackerEventArgs args)
        {
            if (Checked != null)
                Checked(this, args);
        }

        /// <summary>
        /// ���������� ��������� ������� �� ��������� ������ ������ ���������.
        /// </summary>
        /// <param name="row">��������� ������ ������ ���������.</param>
        /// <returns>��������� �������.</returns>
        private State getState(atlantaDataSet.onlineRow row)
        {
            return _sensor.Length == 1 ? getLogicSensorState(row) : getSensorState(row);
        }

        private State getState(GpsData row)
        {
            return _sensor.Length == 1 ? getLogicSensorState(row) : getSensorState(row);
        }

        /// <summary>
        /// ���������� ��������� ����������� �������.
        /// </summary>
        /// <param name="row">������ (�����) ������ ���������</param>
        /// <returns></returns>
        private State getLogicSensorState(atlantaDataSet.onlineRow row)
        {
            BitArray bits = new BitArray(BitConverter.GetBytes(row.sensor));
            double value = Convert.ToDouble(bits[_sensor.StartBit]);

            foreach (atlantaDataSet.StateRow state in _sensor.GetStateRows())
            {
                if (value >= state.MinValue && value < state.MaxValue)
                {
                    return new State(state.Id, _sensor, state.MinValue, state.MaxValue, state.Title);
                }
            }

            return null;
        }

        private State getLogicSensorState(GpsData row)
        {
            BitArray bits = new BitArray(row.Sensors);
            double value = Convert.ToDouble(bits[_sensor.StartBit]);

            foreach (atlantaDataSet.StateRow state in _sensor.GetStateRows())
            {
                if (value >= state.MinValue && value < state.MaxValue)
                {
                    return new State(state.Id, _sensor, state.MinValue, state.MaxValue, state.Title);
                }
            }

            return null;
        }

        /// <summary>
        /// ���������� ��������� ���������-��������� �������.
        /// </summary>
        /// <param name="row">������ (�����) ������ ���������</param>
        /// <returns></returns>
        private State getSensorState(atlantaDataSet.onlineRow row)
        {
            Calibrate calibrate = new Calibrate();
            foreach (atlantaDataSet.sensorcoefficientRow c_row in _sensor.GetsensorcoefficientRows())
            {
                Coefficient coefficient = new Coefficient();
                coefficient.User = c_row.UserValue;
                coefficient.K = c_row.K;
                coefficient.b = c_row.b;
                calibrate.Collection.Add(c_row.SensorValue, coefficient);
            }

            double value = calibrate.GetUserValue(BitConverter.GetBytes(row.sensor), _sensor.Length, _sensor.StartBit, _sensor.K, _sensor.B, _sensor.S);

            foreach (atlantaDataSet.StateRow state in _sensor.GetStateRows())
            {
                if (value >= state.MinValue && value < state.MaxValue)
                {
                    return new State(state.Id, _sensor, state.MinValue, state.MaxValue, state.Title);
                }
            }

            return null;
        }

        private State getSensorState(GpsData row)
        {
            Calibrate calibrate = new Calibrate();

            // ����� �������� ������� ���������
            for(int i = 0; i < _sensor.GetsensorcoefficientRows().Count(); i++)
            {
                atlantaDataSet.sensorcoefficientRow c_row = _sensor.GetsensorcoefficientRows()[i];
                Coefficient coefficient = new Coefficient();
                coefficient.User = c_row.UserValue;
                coefficient.K = c_row.K;
                coefficient.b = c_row.b;
                calibrate.Collection.Add(c_row.SensorValue, coefficient);
            }

            if (_sensor.AlgorithmID == (int)AlgorithmType.VOLTAGE_BAT) 
            {
                // ���������� ����� ������� ���������
                double svalue = row.sVoltage;

                // �������� ���������� ��� ������� (�� �����������)
                foreach (atlantaDataSet.StateRow state in _sensor.GetStateRows())
                {
                    if (svalue >= state.MinValue && svalue < state.MaxValue)
                    {
                        return new State(state.Id, _sensor, state.MinValue, state.MaxValue, state.Title);
                    }
                }

                return null;
            } // if

            // ���������� ����� ������� ���������
            double value = calibrate.GetUserValue(row.Sensors, _sensor.Length, _sensor.StartBit, _sensor.K, _sensor.B, _sensor.S);

            // �������� ���������� ��� ������� (�� �����������)
            foreach (atlantaDataSet.StateRow state in _sensor.GetStateRows())
            {
                if (value >= state.MinValue && value < state.MaxValue)
                {
                    return new State(state.Id, _sensor, state.MinValue, state.MaxValue, state.Title);
                }
            }

            return null;
        }

        #endregion
    }

    /// <summary>
    /// ����������, ��� ���� �������������� ������������ �������.
    /// </summary>
    public enum Target
    {
        Alarm = 1,
        Report = 2
    }
}
