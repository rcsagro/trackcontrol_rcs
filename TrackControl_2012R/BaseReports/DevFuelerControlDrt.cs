﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LocalCache;
using BaseReports;
using BaseReports.Procedure;
using BaseReports.Properties;
using TrackControl.General;
using TrackControl.Reports;
using BaseReports.ReportsDE;
using BaseReports.RFID;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports
{
    public partial class DevFuelerControlDrt : BaseReports.ReportsDE.BaseControl
    {
        public class reportFueler
        {
            string timestart;
            string timeend;
            string location;
            double changevalue;
            string fillup;
            string drivername;
            int tankfueladd; 
            double fulerrate;

            public reportFueler(string location, string fillup, string drivername, double changevalue, int tankfueladd, 
                    string timestart, string timeend)
            {
                this.timestart = timestart;
                this.timeend = timeend;
                this.location = location;
                this.changevalue = changevalue;
                this.fillup = fillup;
                this.drivername = drivername;
                this.tankfueladd = tankfueladd;
                this.fulerrate = fulerrate;
            } // reportFueler

            public string Location
            {
                get { return location; }
            }

            public string fillUp
            {
                get { return fillup; }
            }

            public string driverName
            {
                get { return drivername; }
            }

            public double changeValue
            {
                get { return changevalue; }
            }

            public int tankFuelAdd
            {
                get { return tankfueladd; }
            }

            public string timeStart
            {
                get { return timestart; }
            }

            public string timeEnd
            {
                get { return timeend; }
            }
        } // reportFuel
        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary <int, Fueler.Summary> _summaries = new Dictionary<int, Fueler.Summary>();

        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ReportBase<reportFueler, TInfo> ReportingFueler;
        /// <summary>
        /// Конструктор без параметров для контрола отчета "Заправщик".
        /// </summary>
        /// <param name="tabGrafMap"></param>
        public DevFuelerControlDrt()
        {
            InitializeComponent();
            Init();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel( fuelerDataVG, fuelerDataCG, bar3 );
            AddAlgorithm(new Fueler());

            fuelerDataVG.DoubleClick += new EventHandler(fuelerDataVG_DoubleClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler( composLink_CreateMarginalHeaderArea );

            ReportingFueler =
                new ReportBase<reportFueler, TInfo>( Controls, compositeLink1, fuelerDataVG,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp );
        } // DevFuelerControlDrt

        private void Init()
        {
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "FuelerReport";
            fuelerDataCG.DataSource = atlantaDataSetBindingSource;
        }
        
        public override string Caption 
        {
            get { return Resources.Refueler; }
        }

        protected override void Algorithm_Action( object sender, EventArgs e )
        {
            if ( sender is Fueler )
            {
                if ( e is FuelerEventArgs )
                {
                    FuelerEventArgs args = ( FuelerEventArgs ) e;
                    if ( _summaries.ContainsKey( args.Id ) )
                    {
                        _summaries.Remove( args.Id );
                    }
                    _summaries.Add( args.Id, args.Summary );

                    Select( curMrow );
                } // if
            } // if
        } // Algorithm_Action

        protected override void bbiStart_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if ( bbiStart.Caption == Resources.Start )
            {
                SetStopButton();
                BeginReport();
                _stopRun = false;

                ClearReport();
                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && m_row.GetdataviewRows().Length > 0 )
                    {
                        SelectItem( m_row );
                        noData = false;

                        if ( _stopRun )
                            break;
                    } // if
                } // foreach

                if ( noData )
                {
                    XtraMessageBox.Show( Resources.WarningNoData, Resources.Notification );
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";

                if ( curMrow == null )
                {
                    return;
                }

                Select( curMrow );
                SelectGraphics( curMrow );

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                _stopRun = true;
                StopReport();

                if ( !noData )
                    EnableButton();

                SetStartButton();
            } // else
        } // bbiStart_ItemClick

        public override void Select( atlantaDataSet.mobitelsRow mobitel )
        {
            if ( mobitel != null )
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo( mobitel );
                atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", mobitel.Mobitel_ID );

                if ( _summaries.ContainsKey( mobitel.Mobitel_ID ) )
                {
                    UpdateStatusLine();
                    EnableButton();
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                } // else
            } // if
        } // Select

        public void SelectGraphics( atlantaDataSet.mobitelsRow mobitel )
        {
            if ( mobitel != null )
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);
      
                try
                {
                    CreateGraph( mobitel );
                }
                catch ( Exception ex )
                {
                    throw new NotImplementedException( "Exception in FuelerControl: " + ex.Message );
                }
            } // if
        } // SelectGraphics

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem( atlantaDataSet.mobitelsRow m_row )
        {
            DisableButton();
            Application.DoEvents();
            foreach ( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if ( _stopRun )
                    return;
            }
            EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph( atlantaDataSet.mobitelsRow mobitel_row )
        {
            int length = mobitel_row.GetdataviewRows().Length;

            if ( length == 0 )
            {
                return;
            }

            ReportsControl.GraphClearSeries();

            Fueler fueler = new Fueler();
            fueler.SelectItem( mobitel_row );
            atlantaDataSet.sensorsRow sens_row = fueler.FindSensor( AlgorithmType.FUELER );
            double[] value = fueler.GettingValuesDRT();

            if ( value.Length > 0 )
            {
                graph.AddSeriesL( String.Format( "{0} ({1})", sens_row.Name, sens_row.NameUnit ), Color.DarkGreen,
                    value, dataset.dataview.SelectTimeByMobitelID( mobitel_row ), AlgorithmType.FUELER );
            }

            ReportsControl.ShowGraph( curMrow );
        } // CreateGraph

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.FuelerReport.Clear();
            _summaries.Clear();
            ClearStatusLine();
        }

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            barStaticItem1.Caption = Resources.TotalFuelRate;
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR;
        }

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine()
        {
            Fueler.Summary s = _summaries[curMrow.Mobitel_ID];

            barStaticItem1.Caption = Resources.TotalFuelRate + " " + s.FuelRate.ToString( "N2" );
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR + " " + s.UnrecordedFuelRate.ToString( "N2" );
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Показать на графике".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiShowOnGraf_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        } // bbiShowOnGraf_ItemClick

        private static void ShowGraphToolStripButton_ClickExtracted( atlantaDataSet.FuelerReportRow f_row, out string val, out Color color )
        {
            val = f_row.changeValue.ToString( "#.#" ) + Resources.LitreAbbreviation;
            color = Color.Black;
            if ( f_row.changeValue > 0 )
            {
                val = String.Format( "{0} {1}", Resources.Fuelling, val );
                color = Color.Green;
            }
        }

        protected void ShowOnGraph()
        {
            Graph.ClearRegion();
            Graph.ClearLabel();

            Int32[] numberRow = fuelerDataVG.GetSelectedRows();
            for(int i = 0; i < numberRow.Length; i++)
            {
                atlantaDataSet.FuelerReportRow f_row = ( atlantaDataSet.FuelerReportRow )
                  ( ( System.Data.DataRowView ) atlantaDataSetBindingSource.List[i] ).Row;
                if ( f_row != null )
                {
                    Graph.AddTimeRegion( f_row.timeStart, f_row.timeEnd );//выделяем цветом

                    string val;
                    Color color;
                    ShowGraphToolStripButton_ClickExtracted( f_row, out val, out color );
                    Graph.AddLabel( f_row.timeEnd, f_row.changeValue, f_row.pointValue, val, color );//Добавляем метку
                }
            }
            Graph.SellectZoom();
        }

        protected void ShowOnMap()
        {
            ReportsControl.OnClearMapObjectsNeeded();

            List<Marker> markers = new List<Marker>();
            Int32[] numberRow = fuelerDataVG.GetSelectedRows();
            for ( int i = 0; i < numberRow.Length; i++ )
            {
                atlantaDataSet.FuelerReportRow f_row = ( atlantaDataSet.FuelerReportRow )
                  ( ( System.Data.DataRowView ) atlantaDataSetBindingSource.List[i] ).Row;

                if ( f_row != null )
                {
                    double value = f_row.changeValue;
                    string message = String.Format( "{0}: {1:N2} {2}",
                      Resources.Fuelling, value, Resources.LitreAbbreviation );
                    PointLatLng location = new PointLatLng( f_row.dataviewRow.Lat, f_row.dataviewRow.Lon );
                    markers.Add( new Marker( MarkerType.Fueling, f_row.mobitel_id, location, message, "" ) );
                }
            }
            ReportsControl.OnMarkersShowNeeded( markers );
        }  // ShowOnMap

        private void fuelerDataVG_DoubleClick( object sender, EventArgs e )
        {
            ShowOnGraph();
            ShowOnMap();
        }

        protected override void bbiShowOnMap_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnMapShowNeeded();
            ShowOnMap();
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
            {
                if ( m_row.Check && m_row.GetdataviewRows().Length > 0 )
                {
                    TInfo tinfo = new TInfo();
                    tinfo.periodBeging = Algorithm.Period.Begin;
                    tinfo.periodEnd = Algorithm.Period.End;

                    atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID( m_row.Mobitel_ID );
                    VehicleInfo info = new VehicleInfo( m_row.Mobitel_ID );

                    tinfo.infoVehicle = info.Info; // Автомобиль
                    tinfo.infoDriverName = info.DriverFullName; // Водитель
                    tinfo.fuelerLiter = _summaries[m_row.Mobitel_ID].UnrecordedFuelRate; // Литров топлива непопавшее в отчет
                    tinfo.fuelerRate = _summaries[m_row.Mobitel_ID].FuelRate; // Заправлено литров всего

                    ReportingFueler.AddInfoStructToList( tinfo );
                    ReportingFueler.CreateBindDataList();

                    foreach ( atlantaDataSet.FuelerReportRow row in
                        dataset.FuelerReport.Select( "mobitel_id=" + m_row.Mobitel_ID, "timeStart ASC" ) )
                    {
                        string timestart = row.timeStart.ToString("dd.MM.yyyy hh:mm");
                        string timeend = row.timeEnd.ToString( "dd.MM.yyyy hh:mm" ); 
                        string location = row.Location;
                        double changevalue = Math.Round(row.changeValue, 2);
                        string fillup = row.fillUp;

                        string drivername;
                        if ( row.driverName != null && row.driverName != "" )
                            drivername = row.driverName;
                        else
                            drivername = "----";

                        int tankfueladd = row.tankFuelAdd;
                        double fulerrate = Math.Round(row.changeValue - row.tankFuelAdd, 2);

                        ReportingFueler.AddDataToBindList( new reportFueler( location, fillup, drivername, 
                            changevalue, tankfueladd, timestart, timeend ) );
                    } // foreach 2

                    ReportingFueler.CreateElementReport();
                } // if
            } // foreach 1

            ReportingFueler.CreateAndShowReport();
            ReportingFueler.DeleteData();
        } // ExportAllDevToReport

       // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {                                                                                                 
            TInfo tinfo = new TInfo();
            tinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
            tinfo.periodEnd = Algorithm.Period.End;   // Конец периода
            VehicleInfo info = new VehicleInfo(curMrow);

            tinfo.infoVehicle = info.Info; // Автомобиль
            tinfo.infoDriverName = info.DriverFullName; // Водитель
            tinfo.fuelerLiter = _summaries[curMrow.Mobitel_ID].UnrecordedFuelRate; // Литров топлива непопавшее в отчет
            tinfo.fuelerRate = _summaries[curMrow.Mobitel_ID].FuelRate; // Заправлено литров всего

            ReportingFueler.AddInfoStructToList( tinfo ); /* формируем заголовки таблиц отчета */
            ReportingFueler.CreateAndShowReport( fuelerDataCG );
        }

        protected override void barButtonGroupPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            GroupPanel( fuelerDataVG );
        }

        protected override void barButtonFooterPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            FooterPanel( fuelerDataVG );
        }

        protected override void barButtonNavigator_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            NavigatorPanel( fuelerDataCG );
        }

        protected override void barButtonStatusPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            StatusBar( bar3 );
        }

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea( object sender, CreateAreaEventArgs e )
        {
            DevExpressReportHeader( Resources.DetailReportTravel, e );
            TInfo info = ReportingFueler.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader( strPeriod, 22, e );
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ( "" );
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingFueler.GetInfoStructure;
            return ( Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName );
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingFueler.GetInfoStructure;
            return ( Resources.TotalFuelRate + " " + String.Format( "{0:f2}", info.fuelerRate ) + "\n" +
                     Resources.FuelerDrtCtrlUnrFR + " " + String.Format("{0:f2}", info.fuelerLiter ));
        }

        protected void Localization()
        {
            barStaticItem1.Caption = Resources.TotalFuelRate;
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR;

            colLocation.Caption = Resources.Location;
            colLocation.ToolTip = Resources.Location;

            colfillUp.Caption = Resources.FuelerDrtCtrlFilledVehicles;
            colfillUp.ToolTip = Resources.FuelerDrtCtrlFilledVehicles;

            colDriverName.Caption = Resources.recipient;
            colDriverName.ToolTip = Resources.recipient;

            colchangeValue.Caption = Resources.FuelerDrtCtrlDrtFillingValue;
            colchangeValue.ToolTip = Resources.FuelerDrtCtrlDrtFillingValue;

            coltankFuelAdd.Caption = Resources.FuelerDrtCtrlDutFillingValue;
            coltankFuelAdd.ToolTip = Resources.FuelerDrtCtrlDutFillingValue;

            coltimeStart.Caption = Resources.InitialTime;
            coltimeStart.ToolTip = Resources.InitialTime;

            coltimeEnd.Caption = Resources.EndTime;
            coltimeEnd.ToolTip = Resources.EndTime;
        } // Localization
    } // DevFuelerControlDrt
} // BaseReports
