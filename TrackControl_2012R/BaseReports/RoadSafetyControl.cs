using System;
using LocalCache;
using BaseReports.Procedure;
using BaseReports.Properties;
using C1.C1Excel;

namespace BaseReports
{
  /// <summary>
  /// ������������ ��������
  /// </summary>
  [System.ComponentModel.ToolboxItem(false)]
  public partial class RoadSafetyControl : ReportTabControl
  {
    #region Attribute

    int countAccel;
    int countBreak;
    TimeSpan timeSpeed;
    atlantaDataSet.mobitelsRow _mobitel;
    #endregion

    #region Constructor
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="caption"></param>
    public RoadSafetyControl(string caption)
      : base(caption)
    {
      InitializeComponent();
      Init();

      timeSpeed = new TimeSpan(0, 0, 0);
      excelPattern = "RoadSafety.xls";

      AddAlgorithm(new RoadSafety());
    }

    /// <summary>
    /// ����������� ��� ���������� ��� ������ "������������ ��������".
    /// </summary>
    public RoadSafetyControl()
      : this(Resources.RoadSafetyCtrlCaption)
    {
    }

    private void Init()
    {
      trafficSafetyReportBindingSource.DataSource = dataset;
      trafficSafetyReportBindingSource.DataMember = "TrafficSafetyReport";
      SelectFieldGrid(ref trafficSafetyGridView);
      ShowGraphToolStripButton.Visible = false;
      showMapToolStripButton.Visible = false;

      atlantaDataSet = null;
      hideExcelSplitBtn();

      accelLabel.Text = String.Format(Resources.RoadSafetyCtrlAccelCountText, "-");
      accelLabel.ToolTipText = Resources.RoadSafetyCtrlAccelCountHint;

      breakLabel.Text = String.Format(Resources.RoadSafetyCtrlBreaksCountText, "-");
      breakLabel.ToolTipText = Resources.RoadSafetyCtrlBreaksCountHint;

      speedLabel.Text = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, "--:--:--");
      speedLabel.ToolTipText = Resources.RoadSafetyCtrlSpeedingTimeHint;

      Mark.HeaderText = Resources.VehicleBrandName;
      Mark.ToolTipText = Resources.VehicleBrandNameHint;

      Model.HeaderText = Resources.VehicleModel;
      Model.ToolTipText = Resources.VehicleModelHint;

      NamberPlate.HeaderText = Resources.NumberPlate;
      NamberPlate.ToolTipText = Resources.NumberPlateHint;

      Driver.HeaderText = Resources.FullNameShortForm;
      Driver.ToolTipText = Resources.FullNameHint;

      dataGridViewTextBoxColumn62.HeaderText = Resources.DistanceShort;
      dataGridViewTextBoxColumn62.ToolTipText = Resources.DistanceHint;

      dataGridViewTextBoxColumn63.HeaderText = Resources.MaxSpeed;
      dataGridViewTextBoxColumn63.ToolTipText = Resources.MaxSpeedHint;

      dataGridViewTextBoxColumn64.HeaderText = Resources.MaxAcceleration;
      dataGridViewTextBoxColumn64.ToolTipText = Resources.MaxAccelerationHint;

      dataGridViewTextBoxColumn65.HeaderText = Resources.MaxBraking;
      dataGridViewTextBoxColumn65.ToolTipText = Resources.MaxBrakingHint;

      dataGridViewTextBoxColumn66.HeaderText = Resources.AccelerationsCount;
      dataGridViewTextBoxColumn66.ToolTipText = Resources.AccelerationsCountHint;

      dataGridViewTextBoxColumn67.HeaderText = Resources.BrakingsCount;
      dataGridViewTextBoxColumn67.ToolTipText = Resources.BrakingsCountHint;

      dataGridViewTextBoxColumn68.HeaderText = Resources.SpeedingTime;
      dataGridViewTextBoxColumn68.ToolTipText = Resources.SpeedingTimeHint;

      dataGridViewTextBoxColumn69.ToolTipText = Resources.KaHint;
      dataGridViewTextBoxColumn70.ToolTipText = Resources.KbHint;
      dataGridViewTextBoxColumn71.ToolTipText = Resources.KvHint;
    }
    #endregion

    #region Metods

    /// <summary>
    /// ���������� ����� ������
    /// </summary>
    private void Resume()
    {
      foreach (atlantaDataSet.TrafficSafetyReportRow row in dataset.TrafficSafetyReport)
      {
        countAccel += row.AccelCount;
        countBreak += row.BreakCount;
        timeSpeed += row.SpeedTreshold;
      }
      accelLabel.Text = String.Format(Resources.RoadSafetyCtrlAccelCountText, countAccel);
      breakLabel.Text = String.Format(Resources.RoadSafetyCtrlBreaksCountText, countBreak);
      speedLabel.Text = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, timeSpeed);
    }

    public override void Select(atlantaDataSet.mobitelsRow m_row)
    {
      _mobitel = m_row;
      return;
    }
    #endregion

    #region Events

    protected override void runToolStripButton_Click(object sender, EventArgs e)
    {
      dataset.TrafficSafetyReport.Clear();
      countAccel = 0;
      countBreak = 0;
      timeSpeed = new TimeSpan();
      base.runToolStripButton_Click(sender, e);
      Resume();
      printToolStripButton.Enabled = false;
    }

    protected override void ExcelToolStripButton_Click(object sender, EventArgs e)
    {
      LoadPatternExcel();
      XLSheet sheet = c1XLBook.Sheets[0];
      sheet[1, 2].Value = Algorithm.Period.Begin;
      sheet[1, 10].Value = Algorithm.Period.End;

      if (_mobitel != null)
      {
        VehicleInfo info = new VehicleInfo(_mobitel);
        double timeOverSpeed = 0;
        TimeSpan timeOverSpeedLimit = new TimeSpan();
        int numAcceleration = 0;
        int numBreak = 0;
        int i = 8;
        // ����� ����� �������� ������� �������
        XLStyle[] cellStiles = new XLStyle[12];
        for (int j = 0; j < 12; j++)
          cellStiles[j] = sheet[i, j].Style;

        foreach (atlantaDataSet.TrafficSafetyReportRow row in dataset.TrafficSafetyReport.Rows)
        {
          // ��������
          sheet[i, 0].Value = String.Format("{0} \"{1} {2}\"", row.NamberPlate, row.Mark, row.Model);
          sheet[i, 0].Style = cellStiles[0];
          // ���
          if (row["Driver"] != DBNull.Value)
            sheet[i, 1].Value = row.Driver;
          sheet[i, 1].Style = cellStiles[1];
          // ���������� ����, ��
          sheet[i, 2].Value = row.Travel;
          sheet[i, 2].Style = cellStiles[2];
          // ����� ����� ��������, �
          //sheet[i, 3].Value = row.S;
          //sheet[i, 3].Style = cellStiles[3];
          // ������� ��������
          //sheet[i, 4].Value = row.sp;
          //sheet[i, 4].Style = cellStiles[4];
          // ������������ ��������
          sheet[i, 3].Value = row.SpeedMax;
          sheet[i, 3].Style = cellStiles[3];
          // ������������ ���������
          sheet[i, 4].Value = row.AccelMax;
          sheet[i, 4].Style = cellStiles[4];
          // ������������ ����������
          sheet[i, 5].Value = row.BreakMax;
          sheet[i, 5].Style = cellStiles[5];
          // ����� ���������� ��������
          sheet[i, 6].Value = row.SpeedTreshold;
          sheet[i, 6].Style = cellStiles[6];
          // �����. ������. ���������
          sheet[i, 7].Value = row.AccelCount;
          sheet[i, 7].Style = cellStiles[7];
          // �����. ������. ����������
          sheet[i, 8].Value = row.BreakCount;
          sheet[i, 8].Style = cellStiles[8];
          // Kv
          sheet[i, 9].Value = row.Kv;
          sheet[i, 9].Style = cellStiles[9];
          // Ka
          sheet[i, 10].Value = row.Ka;
          sheet[i, 10].Style = cellStiles[10];
          // �b
          sheet[i, 11].Value = row.Kb;
          sheet[i, 11].Style = cellStiles[11];

          timeOverSpeed += row.SpeedTreshold.TotalHours;
          timeOverSpeedLimit += row.SpeedTreshold;
          numAcceleration += row.AccelCount;
          numBreak += row.BreakCount;
          i++;
        }

        //sheet[3, 10].Value = timeOverSpeed;//time over speed
        sheet[3, 10].Value = timeOverSpeedLimit;
        sheet[4, 10].Value = numAcceleration;//acceleration
        sheet[5, 10].Value = numBreak;//break

        sheet.Name = _mobitel.NumTT;// info.CarModel;  mobitel.NumTT
        SaveExcel(Resources.RoadSafetyCtrlCaption, true);
      }
    }

    #endregion
  }
}

