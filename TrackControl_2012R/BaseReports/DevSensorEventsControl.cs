﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports
{
    /// <summary>
    /// Класс контрола для отображения отчета по событиям датчиков
    /// </summary>
    [Serializable]
    [ToolboxItem( false )]
    public partial class DevSensorEventsControl : BaseReports.ReportsDE.BaseControl
    {
         public class reportSens
         {
             int id;
             string sensname;
             bool picture;
             string location;
             DateTime eventtime;
             TimeSpan duration;
             double distance;
             string descript;
             double speed;
             double avgspeed;
                 
             public reportSens(int id, string sensname, bool picture, string location, DateTime eventtime, TimeSpan duration,
                                double distance, string descript, double speed, double avgspeed)
             {
                 this.id = id;
                 this.sensname = sensname;
                 this.location = location;
                 this.eventtime = eventtime;
                 this.duration = duration;
                 this.distance = distance;
                 this.descript = descript;
                 this.speed = speed;
                 this.avgspeed = avgspeed; 
             } // reportSens

             public int Id
             {
                 get { return this.id; }
             }
             
             public string SensorName
             {
                 get { return this.sensname; }
             }

             public bool IsCheckZone
             {
                 get { return picture; }
             }
             
             public string Location
             {
                 get { return this.location; }
             }
             
             public DateTime EventTime
             {
                 get { return eventtime; }
             }
             
             public TimeSpan Duration
             {
                 get { return this.duration; }
             }
             
             public string DescriptionAttribute
             {
                 get { return this.descript; }
             }
             
             public double Speed
             {
                 get { return this.speed; }
             }
             
             public double Distance
             {
                 get { return this.distance; }
             }
             
             public double SpeedAvg
             {
                 get { return this.avgspeed; }
             }
         } // reportSens
         
        private List<int> _mobitelIdList = new List<int>();
        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;
        protected static atlantaDataSet dataset;
        VehicleInfo vehicleInfo;
        ReportBase<reportSens, TInfo> ReportingSens;

        public DevSensorEventsControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gViewSensorEvents, sensorControlEventsGrid, null);

            gViewSensorEvents.Images = icZone;
            icbZone.SmallImages = icZone;

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea); 
            
            gViewSensorEvents.DoubleClick += new EventHandler(gViewSensorEvents_DoubleClick);
            
            //ShowGraphToolStripButton.Visible = false;
            //sensorControlEventsGrid.AutoGenerateColumns = false;
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "SensorEvents";
            //SelectFieldGrid(ref sensorControlEventsGrid);
            eventsTotalBindingSource.DataSource = dataset;
            eventsTotalBindingSource.DataMember = "SensorEventsTotal";
            buildGraph = new BuildGraphs();
            AddAlgorithm( new SensorEvents() );
            
            ReportingSens =
                new ReportBase<reportSens, TInfo>(Controls, compositeLink1, gViewSensorEvents,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevSensorEventsControl
        
        public override string Caption {
            get { return Resources.SensorEvents; }
        }
          
        /// <summary>
        /// Обработчик события окончания работы алгоритма подготовки данных для отчета.
        /// Таблица отчета в датасете заполнена данными для всех телетреков.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if(sender is SensorEvents)
            { 
                SensorEventsEventArgs args = (SensorEventsEventArgs) e;
                _mobitelIdList.Add(args.Id);
            }
        }
          
        /// <summary>
        /// Выбирает данные из таблицы отчета в датасете только для конкретного телетрека.
        /// Связывает полученные данные с гридом.
        /// </summary>
        /// <param name="m_row"></param>
        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if( mobitel != null)
            {
                curMrow = mobitel;
                if(_mobitelIdList.Contains(curMrow.Mobitel_ID))
                {
                    EnableButton();
                    vehicleInfo = new VehicleInfo(mobitel);
                }
                else
                {
                    DisableButton();
                }
                
                atlantaDataSetBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                atlantaDataSetBindingSource.Sort = "EventTime ASC";
                
                eventsTotalBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                eventsTotalBindingSource.Sort = "Count ASC";
                
                //createGraphics();
            } // if
        } // Select
        
        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel)
        {
            if(mobitel != null)
            {
                if(mobitel.GetdataviewRows() == null || mobitel.GetdataviewRows().Length == 0)
                    return;
                
                curMrow = mobitel;
                CreateGraphics();
            }
        } // SelectGraphic
        
        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.SensorEvents.Clear();
            dataset.SensorEventsTotal.Clear();
            _mobitelIdList.Clear();
        }
        
        /// <summary>
        /// Запускает на выполнение алгоритм обработки данных и заполнения соответствующей таблицы
        /// в датасете. После чего связывает грид с данными отчета текущего телетрека.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                BeginReport();
                _stopRun = false;

                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && m_row.GetdataviewRows().Length > 0 )
                    {
                        SelectItem( m_row );
                        noData = false;
                        if (_stopRun) 
                                break;
                    }
                } // foreach

                if ( noData )
                {
                    XtraMessageBox.Show( Resources.WarningNoData, Resources.Notification );
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";
                Select(curMrow);
                SelectGraphic(curMrow);
                
                SetStartButton();
                EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            }
        } // bbiStart_ItemClick

        public void SelectItem( atlantaDataSet.mobitelsRow m_row )
        {
            DisableButton();
            Application.DoEvents();
            foreach ( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if ( _stopRun )
                    return;
            }
            EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem
        
        /// <summary>
        /// Отображает на карте маркер, соответствующий строке события, выбранной в гриде.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gViewSensorEvents_DoubleClick(object sender, EventArgs e)
        {
            if ( gViewSensorEvents.FocusedRowHandle < 0 ) 
                return;

            ReportsControl.OnClearMapObjectsNeeded();

            if ( gViewSensorEvents.IsValidRowHandle( gViewSensorEvents.FocusedRowHandle ) )
            {
                int id = ( int ) gViewSensorEvents.GetRowCellValue( gViewSensorEvents.FocusedRowHandle, "Id" );
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById( id );
                PointLatLng location = new PointLatLng( row.Latitude, row.Longitude );
                List<Marker> markers = new List<Marker>();
                markers.Add( new Marker( MarkerType.Alarm, row.MobitelId, location, row.Description, "" ) );
                ReportsControl.OnMarkersShowNeeded( markers );

                DateTime EventDate = row.EventTime;
                DateTime EventDateEnd = EventDate.Add( row.Duration );
                List<IGeoPoint> data = new List<IGeoPoint>();
                foreach ( atlantaDataSet.dataviewRow d_row in ReportTabControl.Dataset.dataview.Select(
                 String.Format( "Mobitel_ID= {0} ", row.MobitelId ), "time ASC" ) )
                {
                    if ( d_row.time >= EventDate && d_row.time <= EventDateEnd )
                    {
                        data.Add( DataSetManager.ConvertDataviewRowToDataGps( d_row ) );
                    }
                    if ( d_row.time > EventDateEnd ) break;

                }
                if ( data.Count > 0 )
                {
                    List<Track> segments = new List<Track>();
                    segments.Add( new Track( row.MobitelId, Color.Red, 2f, data ) );
                    ReportsControl.OnTrackSegmentsShowNeeded( segments );
                }

                //if (sender != null)
                //{
                //  ReportsControl.OnGraphShowNeeded();
                //}

                Graph.ClearRegion();
                Graph.ClearLabel();

                Graph.AddTimeRegion( row.EventTime, row.EventTime+row.Duration ); //выделяем цветом
                Graph.SellectZoom();
            } //if
        } // gViewSensorEvents_DoubleClick
        
        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(gViewSensorEvents.FocusedRowHandle < 0)
                return;
            
            if(gViewSensorEvents.IsValidRowHandle(gViewSensorEvents.FocusedRowHandle))
            {
                int id = (int)gViewSensorEvents.GetRowCellValue(gViewSensorEvents.FocusedRowHandle, "Id");
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById(id);
                
                if(sender != null)
                {
                    ReportsControl.OnGraphShowNeeded();
                }
                
                Graph.ClearRegion();
                Graph.ClearLabel();
                Graph.AddTimeRegion(row.EventTime, row.EventTime + row.Duration); 
                Graph.SellectZoom();
            } // if
        } // bbiShowOnGraf_ItemClick
        
        /// <summary>
        /// Отображает графики значений датчиков
        /// </summary>
        private void CreateGraphics()
        {
            vehicleInfo = new VehicleInfo(curMrow);
            ReportsControl.GraphClearSeries();
            buildGraph.AddGraphTemerature(graph, dataset, curMrow);
            ReportsControl.ShowGraph(curMrow);
        }  // CreateGraphics
        
        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.DetailReportTravel, e);
            TInfo info = ReportingSens.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }  // composLink_CreateMarginalHeaderArea
        
        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingSens.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            return ("");
        }
        
        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            if ( gViewSensorEvents.FocusedRowHandle < 0 ) 
                return;

            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            if ( gViewSensorEvents.IsValidRowHandle( gViewSensorEvents.FocusedRowHandle ) )
            {
                int id = ( int ) gViewSensorEvents.GetRowCellValue( gViewSensorEvents.FocusedRowHandle, "Id" );
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById( id );
                PointLatLng location = new PointLatLng( row.Latitude, row.Longitude );
                List<Marker> markers = new List<Marker>();
                markers.Add( new Marker( MarkerType.Alarm, row.MobitelId, location, row.Description, "" ) );
                ReportsControl.OnMarkersShowNeeded( markers );
                DateTime EventDate = row.EventTime;
                DateTime EventDateEnd = EventDate.Add( row.Duration );
                List<IGeoPoint> data = new List<IGeoPoint>();
                foreach ( atlantaDataSet.dataviewRow d_row in ReportTabControl.Dataset.dataview.Select(
                 String.Format( "Mobitel_ID= {0} ", row.MobitelId ), "time ASC" ) )
                {
                    if ( d_row.time >= EventDate && d_row.time <= EventDateEnd )
                    {
                        data.Add( DataSetManager.ConvertDataviewRowToDataGps( d_row ) );
                    }

                    if ( d_row.time > EventDateEnd ) 
                        break;
                } // foreach

                if ( data.Count > 0 )
                {
                    List<Track> segments = new List<Track>();
                    segments.Add( new Track( row.MobitelId, Color.Red, 2f, data ) );
                    ReportsControl.OnTrackSegmentsShowNeeded( segments );
                } // if
            }  // if
        } // bbiShowOnMap_ItemClick
        
        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gViewSensorEvents, true, true);
            
            TInfo tinfo = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);
            tinfo.infoVehicle = info.Info;
            tinfo.infoDriverName = info.DriverFullName;
            tinfo.periodBeging = Algorithm.Period.Begin;
            tinfo.periodEnd = Algorithm.Period.End;
            
            ReportingSens.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
            ReportingSens.CreateAndShowReport(sensorControlEventsGrid);
        } // ExportToExcelDevExpress
        
        protected void genReport(int mobitelId)
        {
            TInfo tinfo = new TInfo();
            tinfo.periodBeging = Algorithm.Period.Begin;
            tinfo.periodEnd = Algorithm.Period.End;
            
            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
            VehicleInfo info = new VehicleInfo(mobitelId);
            
            tinfo.infoVehicle = info.Info;
            tinfo.infoDriverName = info.DriverFullName;
            
            ReportingSens.AddInfoStructToList(tinfo);
            ReportingSens.CreateBindDataList();
            
            foreach(atlantaDataSet.SensorEventsRow row in dataset.SensorEvents.Select("MobitelId=" + mobitelId))
            {
                int id = row.Id;
                string sensname = row.SensorName;
                bool picture = row.IsCheckZone;
                string location = row.Location;
                DateTime eventtime = row.EventTime;
                TimeSpan duration = row.Duration;
                double distance = row.Distance;
                string descript = row.Description;
                double speed = row.Speed;
                double avgspeed = row.SpeedAvg;
                 
                ReportingSens.AddDataToBindList(new reportSens(id, sensname, picture, location, eventtime, duration,
                                                                distance, descript, speed, avgspeed));
            } // foreach

            ReportingSens.CreateElementReport();
        } // genReport

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint( gViewSensorEvents, true, true );
            
            _mobitelIdList.ForEach(delegate(int mobitelId) {genReport(mobitelId);});
            
            ReportingSens.CreateAndShowReport();
            ReportingSens.DeleteData();

        }  // ExportAllDevToReport
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel( gViewSensorEvents );
        }
        
        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gViewSensorEvents);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(sensorControlEventsGrid);
        }
        
        private void Localization()
        {
            _sensor.Caption = Resources.Sensor;
            _location.Caption = Resources.Location;
            _time.Caption = Resources.DateTime;
            _duration.Caption = Resources.Duration;
            _description.Caption = Resources.EventDescription;
            _speed.Caption = Resources.Speed;
            _speedAvg.Caption = Resources.SpeedAvg;
            _distance.Caption = Resources.DistanceText;
        } // Localization
        
    } // DevSensorEventsControl
} // BaseReports
