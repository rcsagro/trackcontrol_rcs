﻿namespace BaseReports
{
    partial class FuelerControlDrt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelerControlDrt));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.fuelerDataCG = new DevExpress.XtraGrid.GridControl();
            this.fuelerDataVG = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransportName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoginFueler = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransportCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfillUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGetterCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperatorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchangeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValueDRTFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDutos = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltankFuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelerDataCG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelerDataVG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageComboRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Общий расход: 0;";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Сумма расхода, меньше мин.: 0;";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1013, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 482);
            this.barDockControl2.Size = new System.Drawing.Size(1013, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 482);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1013, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 482);
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink1});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // fuelerDataCG
            // 
            this.fuelerDataCG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fuelerDataCG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelerDataCG.Location = new System.Drawing.Point(0, 24);
            this.fuelerDataCG.MainView = this.fuelerDataVG;
            this.fuelerDataCG.MenuManager = this.barManager1;
            this.fuelerDataCG.Name = "fuelerDataCG";
            this.fuelerDataCG.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1});
            this.fuelerDataCG.Size = new System.Drawing.Size(1013, 458);
            this.fuelerDataCG.TabIndex = 10;
            this.fuelerDataCG.UseEmbeddedNavigator = true;
            this.fuelerDataCG.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelerDataVG,
            this.gridView2});
            this.fuelerDataCG.Click += new System.EventHandler(this.fuelerDataCG_Click);
            // 
            // fuelerDataVG
            // 
            this.fuelerDataVG.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.fuelerDataVG.Appearance.Empty.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.EvenRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.fuelerDataVG.Appearance.FilterPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterPanel.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelerDataVG.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.FocusedRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FocusedRow.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelerDataVG.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupFooter.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.GroupPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupPanel.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.fuelerDataVG.Appearance.GroupRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupRow.Options.UseFont = true;
            this.fuelerDataVG.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelerDataVG.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.HorzLine.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.OddRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.Preview.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.Preview.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.Row.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.RowSeparator.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.SelectedRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.fuelerDataVG.Appearance.VertLine.Options.UseBackColor = true;
            this.fuelerDataVG.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelerDataVG.ColumnPanelRowHeight = 40;
            this.fuelerDataVG.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameSensor,
            this.colTransportName,
            this.colLoginFueler,
            this.colLocation,
            this.colTransportCode,
            this.colfillUp,
            this.colGetterCode,
            this.colDriverName,
            this.colOperatorCode,
            this.colOperator,
            this.colchangeValue,
            this.colValueDRTFact,
            this.colDutos,
            this.coltankFuelAdd,
            this.coltimeStart,
            this.coltimeEnd});
            this.fuelerDataVG.GridControl = this.fuelerDataCG;
            this.fuelerDataVG.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.fuelerDataVG.Name = "fuelerDataVG";
            this.fuelerDataVG.OptionsDetail.AllowZoomDetail = false;
            this.fuelerDataVG.OptionsDetail.EnableMasterViewMode = false;
            this.fuelerDataVG.OptionsDetail.ShowDetailTabs = false;
            this.fuelerDataVG.OptionsDetail.SmartDetailExpand = false;
            this.fuelerDataVG.OptionsSelection.MultiSelect = true;
            this.fuelerDataVG.OptionsView.ColumnAutoWidth = false;
            this.fuelerDataVG.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelerDataVG.OptionsView.EnableAppearanceOddRow = true;
            this.fuelerDataVG.OptionsView.ShowFooter = true;
            // 
            // colNameSensor
            // 
            this.colNameSensor.AppearanceCell.Options.UseTextOptions = true;
            this.colNameSensor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameSensor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameSensor.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameSensor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameSensor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameSensor.Caption = "Датчик";
            this.colNameSensor.FieldName = "NameSensor";
            this.colNameSensor.Name = "colNameSensor";
            this.colNameSensor.OptionsColumn.AllowEdit = false;
            this.colNameSensor.OptionsColumn.AllowFocus = false;
            this.colNameSensor.OptionsColumn.ReadOnly = true;
            this.colNameSensor.ToolTip = "Название проточного датчика";
            this.colNameSensor.Visible = true;
            this.colNameSensor.VisibleIndex = 0;
            // 
            // colTransportName
            // 
            this.colTransportName.AppearanceCell.Options.UseTextOptions = true;
            this.colTransportName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransportName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransportName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransportName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransportName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransportName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransportName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransportName.Caption = "Транспортное средство (заправщик)";
            this.colTransportName.FieldName = "TransportName";
            this.colTransportName.Name = "colTransportName";
            this.colTransportName.OptionsColumn.AllowEdit = false;
            this.colTransportName.OptionsColumn.AllowFocus = false;
            this.colTransportName.OptionsColumn.ReadOnly = true;
            // 
            // colLoginFueler
            // 
            this.colLoginFueler.AppearanceCell.Options.UseTextOptions = true;
            this.colLoginFueler.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoginFueler.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLoginFueler.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoginFueler.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoginFueler.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoginFueler.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLoginFueler.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLoginFueler.Caption = "Логин (заправщик)";
            this.colLoginFueler.FieldName = "LoginFueler";
            this.colLoginFueler.Name = "colLoginFueler";
            this.colLoginFueler.OptionsColumn.AllowEdit = false;
            this.colLoginFueler.OptionsColumn.AllowFocus = false;
            this.colLoginFueler.OptionsColumn.ReadOnly = true;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceCell.Options.UseTextOptions = true;
            this.colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.Caption = "Место положения";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.ToolTip = "Место положения";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 1;
            this.colLocation.Width = 200;
            // 
            // colTransportCode
            // 
            this.colTransportCode.AppearanceCell.Options.UseTextOptions = true;
            this.colTransportCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransportCode.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransportCode.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransportCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransportCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransportCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransportCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransportCode.Caption = "Заправленный транспорт. Код внешней базы.";
            this.colTransportCode.FieldName = "TransportCodeDB";
            this.colTransportCode.Name = "colTransportCode";
            this.colTransportCode.OptionsColumn.AllowEdit = false;
            this.colTransportCode.OptionsColumn.AllowFocus = false;
            this.colTransportCode.OptionsColumn.ReadOnly = true;
            this.colTransportCode.ToolTip = "Заправленный транспорт. Код внешней базы.";
            this.colTransportCode.Visible = true;
            this.colTransportCode.VisibleIndex = 2;
            // 
            // colfillUp
            // 
            this.colfillUp.AppearanceCell.Options.UseTextOptions = true;
            this.colfillUp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colfillUp.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colfillUp.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colfillUp.AppearanceHeader.Options.UseTextOptions = true;
            this.colfillUp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colfillUp.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colfillUp.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colfillUp.Caption = "Заправленный транспорт";
            this.colfillUp.FieldName = "fillUp";
            this.colfillUp.Name = "colfillUp";
            this.colfillUp.OptionsColumn.AllowEdit = false;
            this.colfillUp.OptionsColumn.AllowFocus = false;
            this.colfillUp.OptionsColumn.ReadOnly = true;
            this.colfillUp.ToolTip = "Заправленный транспорт";
            this.colfillUp.Visible = true;
            this.colfillUp.VisibleIndex = 3;
            this.colfillUp.Width = 200;
            // 
            // colGetterCode
            // 
            this.colGetterCode.AppearanceCell.Options.UseTextOptions = true;
            this.colGetterCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGetterCode.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGetterCode.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGetterCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colGetterCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colGetterCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGetterCode.Caption = "Получатель. Код внешней базы.";
            this.colGetterCode.FieldName = "GetterCodeDB";
            this.colGetterCode.Name = "colGetterCode";
            this.colGetterCode.OptionsColumn.AllowEdit = false;
            this.colGetterCode.OptionsColumn.AllowFocus = false;
            this.colGetterCode.OptionsColumn.ReadOnly = true;
            this.colGetterCode.ToolTip = "Получатель. Код внешней базы.";
            this.colGetterCode.Visible = true;
            this.colGetterCode.VisibleIndex = 4;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriverName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.Caption = "Получатель";
            this.colDriverName.FieldName = "driverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.AllowFocus = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.ToolTip = "Получатель";
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 5;
            this.colDriverName.Width = 90;
            // 
            // colOperatorCode
            // 
            this.colOperatorCode.AppearanceCell.Options.UseTextOptions = true;
            this.colOperatorCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperatorCode.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperatorCode.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOperatorCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colOperatorCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperatorCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperatorCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOperatorCode.Caption = "Оператор. Код внешней базы.";
            this.colOperatorCode.FieldName = "OperatorCodeDb";
            this.colOperatorCode.Name = "colOperatorCode";
            this.colOperatorCode.OptionsColumn.AllowEdit = false;
            this.colOperatorCode.OptionsColumn.AllowFocus = false;
            this.colOperatorCode.OptionsColumn.ReadOnly = true;
            this.colOperatorCode.ToolTip = "Оператор. Код внешней базы.";
            this.colOperatorCode.Visible = true;
            this.colOperatorCode.VisibleIndex = 6;
            // 
            // colOperator
            // 
            this.colOperator.AppearanceCell.Options.UseTextOptions = true;
            this.colOperator.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperator.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperator.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOperator.AppearanceHeader.Options.UseTextOptions = true;
            this.colOperator.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperator.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperator.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOperator.Caption = "Оператор";
            this.colOperator.FieldName = "Operator";
            this.colOperator.Name = "colOperator";
            this.colOperator.OptionsColumn.AllowEdit = false;
            this.colOperator.OptionsColumn.AllowFocus = false;
            this.colOperator.OptionsColumn.ReadOnly = true;
            this.colOperator.ToolTip = "Оператор выдавший топливо";
            this.colOperator.Visible = true;
            this.colOperator.VisibleIndex = 7;
            this.colOperator.Width = 90;
            // 
            // colchangeValue
            // 
            this.colchangeValue.AppearanceCell.Options.UseTextOptions = true;
            this.colchangeValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colchangeValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colchangeValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colchangeValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colchangeValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colchangeValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colchangeValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colchangeValue.Caption = "Величина зправки по ДРТ, л";
            this.colchangeValue.DisplayFormat.FormatString = "N2";
            this.colchangeValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colchangeValue.FieldName = "changeValue";
            this.colchangeValue.Name = "colchangeValue";
            this.colchangeValue.OptionsColumn.AllowEdit = false;
            this.colchangeValue.OptionsColumn.AllowFocus = false;
            this.colchangeValue.OptionsColumn.ReadOnly = true;
            this.colchangeValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "changeValue", "{0:f2}")});
            this.colchangeValue.ToolTip = "Величина зправки по ДРТ, л";
            this.colchangeValue.Visible = true;
            this.colchangeValue.VisibleIndex = 8;
            this.colchangeValue.Width = 120;
            // 
            // colValueDRTFact
            // 
            this.colValueDRTFact.AppearanceCell.Options.UseTextOptions = true;
            this.colValueDRTFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValueDRTFact.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colValueDRTFact.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colValueDRTFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colValueDRTFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValueDRTFact.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colValueDRTFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colValueDRTFact.Caption = "Величина зправки по ДРТ (факт), л";
            this.colValueDRTFact.FieldName = "changeValueFact";
            this.colValueDRTFact.Name = "colValueDRTFact";
            this.colValueDRTFact.OptionsColumn.AllowEdit = false;
            this.colValueDRTFact.OptionsColumn.AllowFocus = false;
            this.colValueDRTFact.OptionsColumn.ReadOnly = true;
            this.colValueDRTFact.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colValueDRTFact.ToolTip = "Величина зправки по ДРТ (факт), л";
            this.colValueDRTFact.Visible = true;
            this.colValueDRTFact.VisibleIndex = 9;
            // 
            // colDutos
            // 
            this.colDutos.AppearanceCell.Options.UseTextOptions = true;
            this.colDutos.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDutos.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDutos.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDutos.AppearanceHeader.Options.UseTextOptions = true;
            this.colDutos.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDutos.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDutos.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDutos.Caption = "Величина заправки по ДУТ, л";
            this.colDutos.FieldName = "valueDutos";
            this.colDutos.Name = "colDutos";
            this.colDutos.OptionsColumn.AllowEdit = false;
            this.colDutos.OptionsColumn.AllowFocus = false;
            this.colDutos.OptionsColumn.ReadOnly = true;
            this.colDutos.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDutos.ToolTip = "Величина заправки по ДУТ, л";
            this.colDutos.Visible = true;
            this.colDutos.VisibleIndex = 10;
            // 
            // coltankFuelAdd
            // 
            this.coltankFuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.coltankFuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltankFuelAdd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltankFuelAdd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltankFuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltankFuelAdd.Caption = "Заправлено в бак ТС по ДУТ, л";
            this.coltankFuelAdd.DisplayFormat.FormatString = "N2";
            this.coltankFuelAdd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.coltankFuelAdd.FieldName = "tankFuelAdd";
            this.coltankFuelAdd.Name = "coltankFuelAdd";
            this.coltankFuelAdd.OptionsColumn.AllowEdit = false;
            this.coltankFuelAdd.OptionsColumn.AllowFocus = false;
            this.coltankFuelAdd.OptionsColumn.ReadOnly = true;
            this.coltankFuelAdd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tankFuelAdd", "{0:f2}")});
            this.coltankFuelAdd.ToolTip = "Заправлено в бак ТС по ДУТ, л";
            this.coltankFuelAdd.Visible = true;
            this.coltankFuelAdd.VisibleIndex = 11;
            this.coltankFuelAdd.Width = 120;
            // 
            // coltimeStart
            // 
            this.coltimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.coltimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeStart.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeStart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.coltimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeStart.Caption = "Время начала";
            this.coltimeStart.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.coltimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.coltimeStart.FieldName = "timeStart";
            this.coltimeStart.Name = "coltimeStart";
            this.coltimeStart.OptionsColumn.AllowEdit = false;
            this.coltimeStart.OptionsColumn.AllowFocus = false;
            this.coltimeStart.OptionsColumn.ReadOnly = true;
            this.coltimeStart.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:f2}")});
            this.coltimeStart.ToolTip = "Время начала";
            this.coltimeStart.Visible = true;
            this.coltimeStart.VisibleIndex = 12;
            this.coltimeStart.Width = 200;
            // 
            // coltimeEnd
            // 
            this.coltimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.coltimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeEnd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeEnd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.coltimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeEnd.Caption = "Время окончания";
            this.coltimeEnd.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.coltimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.coltimeEnd.FieldName = "timeEnd";
            this.coltimeEnd.Name = "coltimeEnd";
            this.coltimeEnd.OptionsColumn.AllowEdit = false;
            this.coltimeEnd.OptionsColumn.AllowFocus = false;
            this.coltimeEnd.OptionsColumn.ReadOnly = true;
            this.coltimeEnd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "GeneralDistance", "{0:f2}")});
            this.coltimeEnd.ToolTip = "Время окончания";
            this.coltimeEnd.Visible = true;
            this.coltimeEnd.VisibleIndex = 13;
            this.coltimeEnd.Width = 200;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._imageComboRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)});
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.fuelerDataCG;
            this.gridView2.Name = "gridView2";
            // 
            // FuelerControlDrt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.fuelerDataCG);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "FuelerControlDrt";
            this.Size = new System.Drawing.Size(1013, 507);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.fuelerDataCG, 0);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelerDataCG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelerDataVG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageComboRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraGrid.GridControl fuelerDataCG;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelerDataVG;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraGrid.Columns.GridColumn colfillUp;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colchangeValue;
        private DevExpress.XtraGrid.Columns.GridColumn coltankFuelAdd;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn coltimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn coltimeEnd;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraGrid.Columns.GridColumn colOperator;
        private DevExpress.XtraGrid.Columns.GridColumn colDutos;
        private DevExpress.XtraGrid.Columns.GridColumn colTransportCode;
        private DevExpress.XtraGrid.Columns.GridColumn colGetterCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOperatorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colValueDRTFact;
        private DevExpress.XtraGrid.Columns.GridColumn colNameSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colTransportName;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginFueler;
    }
}
