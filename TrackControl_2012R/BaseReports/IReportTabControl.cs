using System;
using System.Windows.Forms;
using LocalCache;

namespace BaseReports
{
    public interface IReportTabControl
    {
        void EnableRun();
        void DisableRun();

        /// ��������� ������
        /// </summary>
        string Caption { get; }

        /// <summary>
        /// Control
        /// </summary>
        UserControl Control { get; }

        ///// <summary>
        ///// ���������� ����������� ����������
        ///// </summary>
        //void Run();
        void Select(atlantaDataSet.mobitelsRow m_row);
        void SelectItem(atlantaDataSet.mobitelsRow mRow);
        void SelectGraphic(atlantaDataSet.mobitelsRow m_row);

        /// <summary>
        /// ���������� ��������
        /// </summary>
        void SaveSetting();

        /// <summary>
        /// ������� ������ ������
        /// </summary>
        void ClearReport();

        string CurrentActivateReport { get; set; }
    }
}
