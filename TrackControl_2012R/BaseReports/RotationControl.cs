﻿using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using TrackControl.General;

namespace BaseReports
{
    /// <summary>
    /// Класс контрола для отображения детального отчета
    /// по оборотам двигателя транспортного средства
    /// </summary>
    [Serializable]
    [ToolboxItem( false )]
    public partial class RotationControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportRotate
        {
            int id;
            string state;
            string location;
            DateTime init;
            DateTime final;
            TimeSpan interval;
            TimeSpan break_time;

            public reportRotate(int id, string state, string location, DateTime init, DateTime final,
                TimeSpan interval, TimeSpan break_time)
            {
                this.id = id;
                this.state = state;
                this.location = location;
                this.init = init;
                this.final = final;
                this.interval = interval;
                this.break_time = break_time;
            }

            public int Id
            {
                get { return id; }
            }

            public string State
            {
                get { return state; }
            }

            public string Location
            {
                get { return location; }
            }

            public DateTime InitialTime
            {
                get { return init; }
            }

            public DateTime FinalTime
            {
                get { return final; }
            }

            public TimeSpan Interval
            {
                get { return interval; }
            }

            public TimeSpan BreakTime
            {
                get { return break_time; }
            }
        } // reportRotate

        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, Rotation.Summary> _summaries = new Dictionary<int, Rotation.Summary>();

        /// <summary>
        /// 
        ///  Id телетрека,данные которого отображаются в гриде и на диаграмме
        /// </summary>
        private int _teletrackId;

        /// <summary>
        /// телетрек использует диапазон по умолчанию
        /// </summary>
        private bool _IsBoundsDefault;
        /// <summary>
        /// Словарь, содержащий данные по моточасам для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, SortedDictionary<int, TimeSpan_Double>> _motoTimeBands =
        new Dictionary<int, SortedDictionary<int, TimeSpan_Double>>();

        /// <summary>
        /// признак детализации печатного отчета
        /// </summary>
        bool _expandAllGroups = false;

        protected static atlantaDataSet dataset;
        VehicleInfo vehicleInfo;
        ReportBase<reportRotate, TInfo> ReportingRotate;

        BarButtonItem bbiSwitchDiagram;
        Bar BarC;
        BarManager bManager;
        PrintableComponentLink printChart;
        DevExpress.XtraBars.BarButtonItem reportButton;

        public RotationControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel( gvRotation, gcRotation, bar2 );
            motorTimePanel.Visible = false;
            reportGridPanel.Visible = true;
            reportGridPanel.Dock = DockStyle.Fill;
            SizeChanged +=new EventHandler(DevRotationControl_SizeChanged);

            bbiSwitchDiagram = new BarButtonItem();
            bManager = GetBarManager();
            bManager.Items.Add( bbiSwitchDiagram );
            BarC = GetToolBar();
            BarC.LinksPersistInfo.Insert( 3, new LinkPersistInfo( BarLinkUserDefines.PaintStyle,
                bbiSwitchDiagram, BarItemPaintStyle.CaptionGlyph ) );
            bbiSwitchDiagram.Caption = "";
            // bbiSwitchDiagram.Glyph = imageCollection.Images[0];
            bbiSwitchDiagram.Id = 3;
            bbiSwitchDiagram.Name = "bbiShowDiagram";
            bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
            bbiSwitchDiagram.Glyph = Shared.Table; // imageCollection.Images[1];
            bbiSwitchDiagram.Enabled = false;
            bbiSwitchDiagram.ItemClick += new ItemClickEventHandler( bbiSwitchDiagram_ItemClick );
            gvScale.CellValueChanged +=new CellValueChangedEventHandler(gvScale_CellValueChanged);
            gvScale.ShowGridMenu += new GridMenuEventHandler(gvScale_ShowGridMenu);
            mnuAdd.ItemClick += new ItemClickEventHandler(mnuAdd_ItemClick);
            mnuDelete.ItemClick += new ItemClickEventHandler(mnuDelete_ItemClick);
            gvRotation.FocusedRowChanged += new FocusedRowChangedEventHandler(gvRotation_FocusedRowChanged);
            ButtonAdd.ItemClick += new ItemClickEventHandler(bbiAdd_ItemClick);
            ButtonDelete.ItemClick += new ItemClickEventHandler(bbiDelete_ItemClick);
            gvRotation.FocusedRowChanged += new FocusedRowChangedEventHandler(gvRotation_FocusedRowChanged);
            gcScale.Dock = DockStyle.Fill;
            printChart = new PrintableComponentLink();

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler( composLink_CreateMarginalHeaderArea );

            ReportingRotate =
                new ReportBase<reportRotate, TInfo>( Controls, compositeLink1, gvRotation,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp );

            ReportingRotate.AddLink( printChart );

            reportButton = getReportExport();

            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "RotateReport";
            gcRotation.DataSource = atlantaDataSetBindingSource;
            
            AddAlgorithm( new Rotation() );
        } // DevRotationControl

        public override string Caption
        {
            get
            {
                return Resources.RotationCtrlCaption;
            }
        }

        protected void DevRotationControl_SizeChanged(object sender, EventArgs e)
        {
            XtraUserControl Control = (XtraUserControl)sender;
            reportGridPanel.Width = Control.Width;
            reportGridPanel.Height = Control.Height;
            motorTimePanel.Width = Control.Width;
            motorTimePanel.Height = Control.Height;
        }

        public override void Select( atlantaDataSet.mobitelsRow mobitel )
        {
            //base.Select(mobitel);
            if ( mobitel != null )
            {
                curMrow = mobitel;
                /*Graph.ClearSeries();*/

                if ( mobitel == null ) // || dataset.TachometerValue.SelectByMobitel(mobitel).Length == 0)
                {
                    return;
                }

                vehicleInfo = new VehicleInfo( curMrow );
                if ( _summaries.ContainsKey( curMrow.Mobitel_ID ) )
                {
                    EnableButton();

                    bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                    bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
                    bbiSwitchDiagram.Enabled = true;

                    totalEngineOnLabel.Caption = String.Format(
                      Resources.TotalEngineOnTime, _summaries[curMrow.Mobitel_ID].EngineOnTime );
                    totalEngineOffLabel.Caption = String.Format(
                      Resources.TotalEngineOffTime, _summaries[curMrow.Mobitel_ID].EngineOffTime );
                    _IsBoundsDefault = _summaries[curMrow.Mobitel_ID].IsBoundsDefault;
                }
                else
                {
                    DisableButton();
                    bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                    bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
                    bbiSwitchDiagram.Enabled = false;
                    ClearStatusLine();
                }

                _teletrackId = mobitel.Mobitel_ID;

                PopulateMotoTimeReport();
                atlantaDataSetBindingSource.Filter = String.Format( "MobitelId={0}", curMrow.Mobitel_ID );

                // Строим график оборотов.  //График перенесен в правое меню и доступен тепер всем отчетам
                /*graph.AddSeriesL(
                    "Обороты (об./мин)",
                    Color.CornflowerBlue,
                    dataset.TachometerValue.SelectByMobitel(curMrow),
                    dataset.dataview.SelectTimeByMobitelID(curMrow),
                    SharedDLL.ALG_TYPE.ROTATE_E);*/
                graph.SimulateMenuItemRightCheck( Resources.RPM );
            } // if
        } // Select

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.RotateReport.Clear();
            dataset.TachometerValue.Clear();
            ClearData();
            EnableButton();
            bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
            bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
            bbiSwitchDiagram.Enabled = false;
        }

        protected override void Algorithm_Action( object sender, EventArgs e )
        {
            if ( sender is Rotation )
            {
                RotationEventArgs args = ( RotationEventArgs ) e;

                if ( _summaries.ContainsKey( args.Id ) )
                {
                    _summaries.Remove( args.Id );
                }

                _summaries.Add( args.Id, args.Summary );

                if ( _motoTimeBands.ContainsKey( args.Id ) )
                {
                    _motoTimeBands.Remove( args.Id );
                }

                _motoTimeBands.Add( args.Id, args.MotoTimeBand );
            }
        } // Algorithm_Action

        /// <summary>
        /// Меняет режим отображения отчета. Либо отображается детальный отчет по оборотам
        /// двигателя, либо отображается отчет по моточасам, иллюстрированный диаграммой.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bbiSwitchDiagram_ItemClick(object sender, EventArgs e)
        {
            if ( reportGridPanel.Visible )
            {
                reportGridPanel.Visible = false;
                motorTimePanel.Dock = DockStyle.Fill;
                motorTimePanel.Visible = true;
                bbiSwitchDiagram.Glyph = Shared.Table; // imageCollection.Images[1];
                bbiSwitchDiagram.Hint = "Показать индивидуальный отчет";
               // DisableButton();
               // DisableRun();
                SetEnableExportButton();
                reportButton.Enabled = false;
            }
            else
            {
                reportGridPanel.Dock = DockStyle.Fill;
                reportGridPanel.Visible = true;
                motorTimePanel.Visible = false;
                bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
               // EnableButton();
               // EnableRun();
                SetEnableExportButton();
                printChart.Component = null;
                reportButton.Enabled = true;
            }
        } // bbiSwitchDiagram_ItemClick

        /// <summary>
        /// Запускает на выполнение алгоритм обработки данных и заполнения соответствующей таблицы
        /// в датасете. После чего связывает грид с данными отчета текущего телетрека.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if ( bbiStart.Caption == Resources.Start )
            {
                SetStopButton();

                bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
                bbiSwitchDiagram.Enabled = false;

                BeginReport();
                _stopRun = false;

                ClearReport();

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();
                
                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && DataSetManager.IsGpsDataExist(m_row) )
                    {
                        SelectItem( m_row );
                        noData = false;
                        if ( _stopRun )
                            break;
                    } // if
                } // if

                if ( noData )
                {
                    XtraMessageBox.Show( Resources.WarningNoData, Resources.Notification );
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";
                Select( curMrow );

                // if (vehicleInfo != null) Graph.ShowSeries(vehicleInfo.Info);
                ReportsControl.ShowGraph( curMrow );

                graph.SimulateMenuItemRightCheck( Resources.RPM );

                SetStartButton();
                EnableButton();

                bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
                bbiSwitchDiagram.Enabled = true;
            } // if
            else
            {
                _stopRun = true;
                StopReport();
                if ( !noData )
                    EnableButton();
                SetStartButton();
                bbiSwitchDiagram.Glyph = Shared.ChartCircle; // imageCollection.Images[0];
                bbiSwitchDiagram.Hint = "Показать круговую диаграмму";
                bbiSwitchDiagram.Enabled = false;
            } // else
        } // bbiStart_ItemClick

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {

            DisableButton();
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if ( _stopRun )
                    break;
            }

            EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        protected override void bbiShowOnMap_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnMapShowNeeded();
            // эта функция в предыдущем отчете не реализована
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Показать на графике".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnGraphShowNeeded();
            Graph.ClearRegion();
            if ( gvRotation.FocusedRowHandle < 0 )
            {
                return;
            }

            if ( gvRotation.IsValidRowHandle( gvRotation.FocusedRowHandle ) )
            {
                int id = ( int ) gvRotation.GetRowCellValue( gvRotation.FocusedRowHandle, "Id" );
                atlantaDataSet.RotateReportRow row = dataset.RotateReport.FindById( id );
                Graph.AddTimeRegion( row.InitialTime, row.FinalTime );
            }
        } // bbiShowOnGraf_ItemClick

        private void gvScale_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            if (e.Column.FieldName.ToString() == "Bound")
            {
                int bound;
                if (!(Int32.TryParse(gvScale.GetRowCellValue(e.RowHandle, gvScale.Columns["Bound"]).ToString(), out bound))) return;
                if (bound == 0) return;
                if (MobitelsRotateBandsProvider.BoundExist(_teletrackId, bound))
                {
                    gvScale.SetRowCellValue(e.RowHandle, gvScale.Columns["Bound"], 0);
                    return;
                }
                if (bound <= 0) return;
                if (_IsBoundsDefault)
                {
                    MobitelsRotateBandsProvider.InsertRecord(_teletrackId, bound);
                    CopyTemplateToDB(bound);
                    _IsBoundsDefault = false;
                }
                else
                {
                    int id_bound;
                    if (!(Int32.TryParse(gvScale.GetRowCellValue(e.RowHandle, gvScale.Columns["Id"]).ToString(), out id_bound))) return;
                    if (id_bound == 0)
                    {
                        gvScale.SetRowCellValue(e.RowHandle, gvScale.Columns["Id"], MobitelsRotateBandsProvider.InsertRecord(_teletrackId, bound));
                    }
                    else
                    {
                        MobitelsRotateBandsProvider.UpdateRecord(id_bound, bound);
                    }
                }
                gcScale.DataSource = MobitelsRotateBandsProvider.SelectRecords(_teletrackId);
            }
        } // gvScale_CellValueChanged

        private void gvScale_ShowGridMenu( object sender, DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs e )
        {
            mnuScale.ShowPopup(System.Windows.Forms.Control.MousePosition);
        }

        private void mnuDelete_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            DeleteRows();
        }

        private void mnuAdd_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            AddRow();
        }

        private void bbiAdd_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            AddRow();
        }

        private void bbiDelete_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            DeleteRows();
        }

        private void gvRotation_FocusedRowChanged( object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e )
        {
            Graph.ClearRegion();

            if ( /*null == e.FocusedRowHandle ||*/ e.FocusedRowHandle < 0 )
            {
                return;
            }

            if ( gvRotation.IsValidRowHandle( e.FocusedRowHandle ) )
            {
                int id = ( int ) gvRotation.GetRowCellValue( e.FocusedRowHandle, "Id" );
                atlantaDataSet.RotateReportRow row = dataset.RotateReport.FindById( id );
                Graph.AddTimeRegion( row.InitialTime, row.FinalTime );
            }
        } // gvRotation_FocusedRowChange

        /// <summary>
        /// Очищает итоги и данные по моточасам
        /// </summary>
        private void ClearData()
        {
            _summaries.Clear();
            _motoTimeBands.Clear();

            ClearStatusLine();
        }

        /// <summary>
        /// Заполняет данными грид данными по моточасам и рисует диаграмму.
        /// </summary>
        /// <param name="teletrackId">ID текущего телетрека</param>
        private void PopulateMotoTimeReport()
        {

            if ( !_motoTimeBands.ContainsKey( _teletrackId ) || !_summaries.ContainsKey( _teletrackId ) )
            {
                return;
            }

            gcScale.DataSource = _summaries[_teletrackId].Bound;
            List<GridRow> grid_source = new List<GridRow>();
            int key_prev= 0;
            foreach ( KeyValuePair<int, TimeSpan_Double> kvp in _motoTimeBands[_teletrackId] )
            {
                GridRow gr = new GridRow( key_prev, kvp.Key, kvp.Value.ts, Math.Round( kvp.Value.db, 2 ), _summaries[_teletrackId].EngineOnTime.TotalSeconds == 0 ? 0 : Math.Round( kvp.Value.ts.TotalSeconds / _summaries[_teletrackId].EngineOnTime.TotalSeconds * 100, 2 ) );
                key_prev = kvp.Key;
                grid_source.Add( gr );
            }

            gcRotate.DataSource = grid_source;
            chartRotate.DataSource = grid_source;
        }

        /// <summary>
        /// Очищаем статусную строку
        /// </summary>
        private void ClearStatusLine()
        {
            totalEngineOnLabel.Caption = String.Format( Resources.TotalEngineOnTime, "--:--:--" );
            totalEngineOffLabel.Caption = String.Format( Resources.TotalEngineOffTime, "--:--:--" );
        }

        void AddRow()
        {
            {
                MobitelsRotateBandsProvider.InsertRecord( _teletrackId, 0 );
                gcScale.DataSource = MobitelsRotateBandsProvider.SelectRecords( _teletrackId );
            }
        }

        void DeleteRows()
        {
            if ( gvScale.SelectedRowsCount == 0 ) 
                return;

            if ( DialogResult.No == XtraMessageBox.Show( Resources.DeleteQuestion, Application.CompanyName, 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 ) ) 
                return;

            bool UpdateSource = false;

            //если удаляем часть шаблона - остаток присваиваем ТТ как его данные
            if ( _IsBoundsDefault )
            {
                for ( int i = 0; i < gvScale.RowCount; i++ )
                {
                    if ( !gvScale.IsRowSelected( i ) )
                    {
                        int bound;

                        if ( Int32.TryParse( gvScale.GetRowCellValue( i, gvScale.Columns["Bound"] ).ToString(), out bound ) 
                            && bound > 0)
                        {
                            if ( !MobitelsRotateBandsProvider.BoundExist( _teletrackId, bound ) )
                            {
                                MobitelsRotateBandsProvider.InsertRecord( _teletrackId, bound );
                            }
                        } // if
                    }  // if
                } // for

                if ( !UpdateSource )
                {
                    MobitelsRotateBandsProvider.InsertRecord( _teletrackId, 0 );
                }

                UpdateSource = true;
                _IsBoundsDefault = false;
            } // if
            else
            {
                for ( int i = 0; i < gvScale.SelectedRowsCount; i++ )
                {
                    int id_bound;

                    if ( ( Int32.TryParse( gvScale.GetRowCellValue( gvScale.GetSelectedRows()[i], gvScale.Columns["Id"] ).ToString(), 
                        out id_bound ) ) )
                    {
                        if ( id_bound > 0 )
                        {
                            if ( MobitelsRotateBandsProvider.DeleteRecord( id_bound ) ) UpdateSource = true;
                        }
                    } // if
                } // for
            } // else

            if (UpdateSource)
                gcScale.DataSource = MobitelsRotateBandsProvider.SelectRecords( _teletrackId );
        }  // DeleteRows

        void CopyTemplateToDB( int boundExcluding )
        {
            _summaries[_teletrackId].Bound.ForEach( delegate( BoundRow brow )
            {
                if ( brow.Bound != boundExcluding )
                    MobitelsRotateBandsProvider.InsertRecord( _teletrackId, brow.Bound );
            } );
        } // CopyTemplateToDB

        private void LinkGrid2Chart_CreateReportHeaderArea( object sender, CreateAreaEventArgs e )
        {
            VehicleInfo info = new VehicleInfo( curMrow.Mobitel_ID );
            DevExpressReportHeader( Resources.ReportSummaryOnTheEngineSpeed, e );
            DevExpressReportSubHeader( string.Format( "{4} {0} {5} {1}\n{6} {2}\n{7} {3}",
            Algorithm.Period.Begin, Algorithm.Period.End, info.Info, info.DriverFullName, Resources.PeriodFrom,
            Resources.PeriodTo, Resources.Vehicle, Resources.Driver ), 60, e );
        }

        private void LinkGrid1_CreateReportHeaderArea( object sender, CreateAreaEventArgs e )
        {
            VehicleInfo info = new VehicleInfo( curMrow.Mobitel_ID );
            string reportHeader = Resources.ReportDetailedOnTheEngineSpeed;

            if ( gvRotation.GroupCount > 0 && !_expandAllGroups ) 
                reportHeader = Resources.ReportSummaryOnTheEngineSpeed;

            DevExpressReportHeader( reportHeader, e );
            DevExpressReportSubHeader( string.Format( "{4} {0} {5} {1}\n{6}: {2}\n{7}: {3}",
                Algorithm.Period.Begin, Algorithm.Period.End, info.Info, info.DriverFullName,
                Resources.PeriodFrom, Resources.PeriodTo, Resources.Vehicle, Resources.Driver ), 60, e );
        }

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea( object sender, CreateAreaEventArgs e )
        {
            DevExpressReportHeader( Resources.DevRotationControlName, e );
            TInfo info = ReportingRotate.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader( strPeriod, 22, e );
        }  // composLink_CreateMarginalHeaderArea

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingRotate.GetInfoStructure;
            return ( Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName );
        }

        /* функция для формирования верхней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ( "" );
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingRotate.GetInfoStructure;
            return ( Resources.TotalTimeEngineOn + ": " + info.totalTimeWay + "\n" +
                Resources.TotalTimeEngineOff + ": " + info.totalTimerTour );
        }

        protected void SetupGidViewForPrint( GridView gv, bool ExpandAllGroups )
        {
            gv.OptionsPrint.ExpandAllDetails = true;
            gv.OptionsPrint.PrintDetails = true;
            gv.OptionsPrint.AutoWidth = false;
            gv.OptionsPrint.ExpandAllGroups = ExpandAllGroups;
            gv.BestFitColumns();
            gv.OptionsPrint.AutoWidth = true;
        }
        
        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            try
            {
                TInfo tinfo = new TInfo();
                VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);

                tinfo.infoVehicle = info.Info;
                tinfo.infoDriverName = info.DriverFullName;
                tinfo.periodBeging = Algorithm.Period.Begin;
                tinfo.periodEnd = Algorithm.Period.End;
                tinfo.totalTimeWay = _summaries[curMrow.Mobitel_ID].EngineOnTime; // Общее время с вкл. двигателем
                tinfo.totalTimerTour = _summaries[curMrow.Mobitel_ID].EngineOffTime; // Общее время с выкл. двигателем

                if (reportGridPanel.Visible)
                {
                    SetupGidViewForPrint(gvRotation, true);
                    ReportingRotate.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
                    ReportingRotate.CreateAndShowReport(gcRotation);
                }
                else
                {
                    SetupGidViewForPrint(gvRotate, true);
                    ReportingRotate.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
                    printChart.Component = chartRotate;
                    ReportingRotate.CreateAndShowReport(gcRotate);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(Resources.AbsentData, Resources.Notification);
            }
        } // ExportToExcelDevExpress
        
        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            if ( reportGridPanel.Visible )
            {
                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && DataSetManager.IsGpsDataExist(m_row) )
                    {
                        int mobitelId = m_row.Mobitel_ID;
                        TInfo tinfo = new TInfo();
                        VehicleInfo info = new VehicleInfo( mobitelId, m_row );

                        try
                        {
                            //atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID( m_row.Mobitel_ID );
                            tinfo.infoVehicle = info.Info;
                            tinfo.infoDriverName = info.DriverFullName;
                            tinfo.periodBeging = Algorithm.Period.Begin;
                            tinfo.periodEnd = Algorithm.Period.End;
                            tinfo.totalTimeWay = _summaries[mobitelId].EngineOnTime; // Общее время с вкл. двигателем
                            tinfo.totalTimerTour = _summaries[mobitelId].EngineOffTime;
                            // Общее время с выкл. двигателем

                            ReportingRotate.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
                            ReportingRotate.CreateBindDataList(); // создать новый список данных таблицы отчета
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }

                        foreach ( atlantaDataSet.RotateReportRow row in dataset.RotateReport.Select( "MobitelId=" + mobitelId, "Id ASC" ) )
                        {
                            int id = row.Id;
                            string state = row.State;
                            string location = ( row["Location"] != DBNull.Value ) ? row.Location : " - ";
                            DateTime init = row.InitialTime;
                            DateTime final = row.FinalTime;
                            TimeSpan interval = row.Interval;

                            TimeSpan break_time = new TimeSpan();
                            if ( row["BreakTime"] != DBNull.Value )
                            {
                                break_time = row.BreakTime;
                            }

                            ReportingRotate.AddDataToBindList( new reportRotate( id, state, location, init, final, interval,
                                break_time ) );
                        } // foreach 2

                        ReportingRotate.CreateElementReport();
                    } // if
                } // foreach 1

                ReportingRotate.CreateAndShowReport();
                ReportingRotate.DeleteData();
            } // if
        } // ExportAllDevToReport

        protected override void barButtonGroupPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            GroupPanel( gvRotation );
        }

        protected override void barButtonFooterPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            FooterPanel( gvRotation );
        }

        protected override void barButtonNavigator_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            NavigatorPanel( gcRotation );
        }

        protected override void barButtonStatusPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            StatusBar( bar2 );
        }

        /// <summary>
        /// источник данных для грида с оборотами
        /// </summary>
        private class GridRow
        {

            public int NumberFrom { get; set; }

            public int NumberTo { get; set; }

            public TimeSpan TimeRotation { get; set; }

            public double PercentRotation { get; set; }

            public double Distance { get; set; }

            public GridRow( int NF, int NT, TimeSpan TR, double D, double PR )
            {
                NumberFrom = NF;
                NumberTo = NT;
                TimeRotation = TR;
                Distance = D;
                PercentRotation = PR;
            }

            public GridRow() { }
        } // GridRow

        protected void Localization()
        {
              State.Caption = Resources.RotationCtrlEngineCondition;
              Location.Caption = Resources.Location;
              InitialTime.Caption = Resources.InitialTime;
              FinalTime.Caption = Resources.EndTime;
              Interval.Caption = Resources.DateTimeInterval;
              Interval.ToolTip = Resources.PeriodDuration;
              BreakTime.Caption = Resources.RotationCtrlIncludingParking;
              BreakTime.ToolTip = Resources.RotationCtrlIdleTime;
              NF.Caption = Resources.RotationFrom;
              NT.Caption = Resources.RotationTo;
              TimeRot.Caption = Resources.WorkingTime;
              DistanceRot.Caption = Resources.KilometrageCtrlColDistance;
              PercentRot.Caption = Resources.Percent;
              Scale.Caption = Resources.Scale;

              ButtonAdd.Hint = Resources.ReportTabCtrlAddText;
              ButtonAdd.Caption = Resources.ReportTabCtrlAddText;

              ButtonDelete.Hint = Resources.ReportTabCtrlDeleteText;
              ButtonDelete.Caption = Resources.ReportTabCtrlDeleteText;

              mnuAdd.Caption = Resources.ReportTabCtrlAddText;
              mnuDelete.Caption = Resources.ReportTabCtrlDeleteText;

              totalEngineOnLabel.Caption = String.Format(Resources.TotalEngineOnTime, "--:--:--");
              totalEngineOffLabel.Caption = String.Format(Resources.TotalEngineOffTime, "--:--:--");
        }  // Localization
    } // DevRotationControl
} // BaseReports
