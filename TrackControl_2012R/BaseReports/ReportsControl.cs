using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.ReportListSetting;
using BaseReports.ReportsDE;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraTab;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;

namespace BaseReports
{
    [ToolboxItem(false)]
    public partial class ReportsControl : UserControl, IReportCollection
    {
        private static bool isRotation;
        private static bool isSpeed = true;
        private static bool isAcc;
        private static bool isWay;

        public static IGraph Graph
        {
            get { return ReportsControl._graph; }
            set { ReportsControl._graph = value; }
        }

        private static IGraph _graph;

        /// <summary>
        /// ���������� ��������� ����� ��������� ������� �������
        /// </summary>
        private ReportListCtrl reportListCtrl;

        /// <summary>
        /// ������ TabPage, �������������� ����� ������.
        /// TabPage.Tag �������� ������ �� IReportTabControl.
        /// </summary>
        private List<XtraTabPage> reports;

        public XtraTabControl Tabs
        {
            get { return _xtraTabs; }
        }

        public static event MethodInvoker MapShowNeeded = delegate { };

        public static void OnMapShowNeeded()
        {
            MapShowNeeded();
        }

        public static event MethodInvoker GraphShowNeeded = delegate { };

        public static void OnGraphShowNeeded()
        {
            GraphShowNeeded();
        }

        // aketner - 09.08.2012
        public static event Action<IList<Track>> TrackSegmentsShowNeededWith = delegate { };

        public static void OnTrackSegmentsShowNeededWith(IList<Track> segments)
        {
            TrackSegmentsShowNeededWith(segments);
        }

        public static event Action<IList<Track>> TrackSegmetsShowNeeded = delegate { };

        public static void OnTrackSegmentsShowNeeded(IList<Track> segments)
        {
            TrackSegmetsShowNeeded(segments);
        }

        public delegate void TrackSegmetsShowWithMarkers(IList<Track> tracks, IList<Marker> markers);

        public static event TrackSegmetsShowWithMarkers TrackSegmetsShowWithMarkersEvent = delegate { };

        public static void OnTrackSegmetsShowWithMarkers(IList<Track> segments, IList<Marker> markers)
        {
            TrackSegmetsShowWithMarkersEvent(segments, markers);
        }

        public static event Action<IList<Marker>> MarkersShowNeeded = delegate { };

        public static void OnMarkersShowNeeded(IList<Marker> markers)
        {
            MarkersShowNeeded(markers);
        }

        // aketner - 09.08.2012
        public static event Action<IList<Marker>> MarkersShowNeededWith = delegate { };

        public static void OnMarkersShowNeededWith(IList<Marker> markers)
        {
            MarkersShowNeededWith(markers);
        }


        public static event MethodInvoker ClearMapObjectsNeeded = delegate { };

        public static void OnClearMapObjectsNeeded()
        {
            ClearMapObjectsNeeded();
        }

        public delegate void GraphDataUpdate();

        public GraphDataUpdate graphicsDataUpdate = null;
        
        public ReportsControl(atlantaDataSet dataset)
        {
            InitializeComponent();
            //btnReportList.Image = Shared.Settings;
            reports = new List<XtraTabPage>();
            Dock = DockStyle.Fill;
            Algorithm.AtlantaDataSet = dataset;
            if (ReportTabControl.Dataset == null)
            {
                ReportTabControl.Dataset = dataset;
            }
            if (BaseControl.Dataset == null)
            {
                BaseControl.Dataset = dataset;
                BaseControl.Graph = ReportTabControl.Graph;
            }
            reportListCtrl = new ReportListCtrl();
            ReportListCtrl.Reports_ctrl = this;

            _xtraTabs.SelectedPageChanged += _xtraTabs_SelectedPageChanged;
        }

        public ReportsControl(atlantaDataSet dataset, IReportCollection reports)
            : this(dataset)
        {
            AddReports(reports);
        }

        public ReportsControl(atlantaDataSet dataset, IGraph graph)
            : this(dataset)
        {
            Graph = graph;
            ReportTabControl.Graph = graph;
            BaseControl.Graph = graph;
            Algorithm.Graph = graph;
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void SaveSetting()
        {
            foreach (XtraTabPage page in reports)
            {
                ((IReportTabControl) page.Tag).SaveSetting();
            }
        }

        /// <summary>
        /// ���������� �������
        /// </summary>
        /// <param name="IReportCollection reports"></param>
        /// <returns>���������� ����������� ������� </returns>
        public int AddReports(IReportCollection reports)
        {
            _xtraTabs.TabPages.Clear();
            foreach (IReportTabControl report in reports)
            {
                AddReport(report);
            }

            return _xtraTabs.TabPages.Count;
        }

        public int AddReport(IReportTabControl report)
        {
            string reportTypeName = report.Control.GetType().Name;
            ReportSetting repSetting = reportListCtrl.FindReportSetting(reportTypeName);

            // ���� � ���������� ������ ������ ��� ��� - �������
            if (repSetting == null)
                repSetting = reportListCtrl.AddReportSetting(reportTypeName, report.Caption);

            AddTabPage(report, repSetting.Visible);

            return _xtraTabs.TabPages.Count;
        }

        public void EnableRunButtons()
        {
            foreach (XtraTabPage page in reports)
            {
                IReportTabControl rep = page.Tag as IReportTabControl;
                if (null != rep)
                    rep.EnableRun();
            }
        }

        public void DisableRunButtons()
        {
            reports.ForEach(delegate(XtraTabPage page)
            {
                IReportTabControl rep = page.Tag as IReportTabControl;
                if (null != rep)
                    rep.DisableRun();
            });
        }

        /// <summary>
        /// ���������� TabPage � ��������� reports � � ����������, ���� ����� �������
        /// </summary>
        /// <param name="report">IReportTabControl</param>
        /// <param name="visible">������������ ����� ��� ���</param>
        private void AddTabPage(IReportTabControl report, bool visible)
        {
            XtraTabPage page = new XtraTabPage();
            page.Text = report.Caption;
            page.Margin = new Padding(0);
            page.Tag = report;
            page.Controls.Add(report.Control);
            report.Control.Dock = DockStyle.Fill;
            reports.Add(page);

            if (visible)
            {
                _xtraTabs.TabPages.Add(page);
            }
        }

        void _xtraTabs_SelectedPageChanged( object sender, EventArgs e )
        {
            IReportTabControl report;

            // ������� �������������� �������
            for (int i = 0; i < ((XtraTabControl) sender).Controls.Count; i++)
            {
                report = ( IReportTabControl )reports[i].Tag;
                report.CurrentActivateReport = "";
            }
            
            XtraTabPage currentPage = ( ( XtraTabControl )sender ).SelectedTabPage;
            
            for (int i = 0; i < reports.Count; i++)
            {
                report = ( IReportTabControl )reports[i].Tag;

                if( report.Caption == currentPage.Text )
                {
                    report.CurrentActivateReport = report.Caption;
                    break;
                }
            }

            // ����������� ������� �������� ������������� ��������
            //redrawGraphObjects();
            if (graphicsDataUpdate != null)
                graphicsDataUpdate();

        } // page_TabIndexChanged

        /// <summary>
        /// ����� ������
        /// </summary>
        /// <param name="mobitelsRow m_row"></param>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            foreach (XtraTabPage tab in _xtraTabs.TabPages)
            {
                if (tab.Tag is IReportTabControl)
                {
                    ((IReportTabControl) tab.Tag).SelectItem(m_row);
                }
            }
        }

        /// <summary>
        /// ��������� ������� �� ��������� ������
        /// </summary>
        /// <param name="m_row"></param>
        public void SelectFiltr(atlantaDataSet.mobitelsRow m_row)
        {
            foreach (XtraTabPage page in reports)
            {
                ((IReportTabControl) page.Tag).Select(m_row);
                Algorithm.GpsDatas = DataSetManager.GetDataGpsArray(m_row);
            }
        }

        public static void GraphClearSeries()
        {
            int tmpShowGraph = _graph.SeriesIsShow(Resources.RPM);
            if (tmpShowGraph >= 0)
                isRotation = tmpShowGraph > 0 ? true : false;
            tmpShowGraph = _graph.SeriesIsShow(Resources.Speed);
            if (tmpShowGraph >= 0)
                isSpeed = tmpShowGraph > 0 ? true : false;
            tmpShowGraph = _graph.SeriesIsShow(Resources.Acceleration);
            if (tmpShowGraph >= 0)
                isAcc = tmpShowGraph > 0 ? true : false;
            tmpShowGraph = _graph.SeriesIsShow(Resources.DistanceShort);
            if (tmpShowGraph >= 0)
                isWay = tmpShowGraph > 0 ? true : false;

            Graph.ClearSeries();
        }

        public static void ShowGraph(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                Graph.ClearGraphZoom();
                Graph.ClearRegion();

                if (Algorithm.GpsDatas == null) 
                    return;

                DateTime[] gpsTimes = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                //DateTime[] time = ReportTabControl.Dataset.dataview.SelectTimeByMobitelID(m_row);
                
                // ������ �������� 
                Graph.AddSeriesR(Resources.Speed, Color.LightCoral,
                    Algorithm.GpsDatas.Select(gps => (double)gps.Speed).ToArray(),
                    gpsTimes, AlgorithmType.SPEED, isSpeed);

                // ������ ���������
                Graph.AddSeriesR(Resources.Acceleration, Color.DarkCyan,
                    Algorithm.GpsDatas.Select(gps => (double)gps.Accel).ToArray(),
                    gpsTimes, AlgorithmType.ACCEL, isAcc);

                //atlantaDataSet.dataviewRow[] d_rows = m_row.GetdataviewRows();
                double[] dist = new double[Algorithm.GpsDatas.Length];
                double sdist = 0;

                for (int i = 1; i < dist.Length; i++)
                {
                    sdist += Algorithm.GpsDatas[i].Dist;
                    dist[i] = sdist;
                }

                // ������ ����������� ����
                Graph.AddSeriesR(Resources.DistanceShort, Color.Blue, dist, gpsTimes, AlgorithmType.TRAVEL, isWay);

                // ������ ������ ��������.
                if (ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row).Length > 0)
                {
                    _graph.AddSeriesR(Resources.RPM, Color.CornflowerBlue,
                        ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row),
                        gpsTimes,AlgorithmType.ROTATE_E, isRotation);
                }

                VehicleInfo vehicleInfo = new VehicleInfo(m_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        }

        public static void ShowGraphFromMobi(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                Graph.ClearGraphZoom();
                //Graph.ClearGraphZoom();
                Graph.ClearRegion();

                if (Algorithm.GpsDatas == null)
                    return;

                //DateTime[] gpsTimes = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                DateTime[] time = ReportTabControl.Dataset.dataview.SelectTimeByMobitelID(m_row);
                double[] speed = ReportTabControl.Dataset.dataview.SelectSpeedByMoitelID(m_row);
                double[] accel = ReportTabControl.Dataset.dataview.SelectAccelByMoitelID(m_row);

                // ������ �������� 
                Graph.AddSeriesR(Resources.Speed, Color.LightCoral,
                    speed, time, AlgorithmType.SPEED, isSpeed);

                // ������ ���������
                Graph.AddSeriesR(Resources.Acceleration, Color.DarkCyan,
                    accel, time, AlgorithmType.ACCEL, isAcc);

                atlantaDataSet.dataviewRow[] d_rows = m_row.GetdataviewRows();
                double[] dist = new double[d_rows.Length];
                double sdist = 0;

                for (int i = 1; i < d_rows.Length; i++)
                {
                    sdist += d_rows[i].dist;
                    dist[i] = sdist;
                }

                // ������ ����������� ����
                Graph.AddSeriesR(Resources.DistanceShort, Color.Blue, dist, time, AlgorithmType.TRAVEL, isWay);

                // ������ ������ ��������.
                if (ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row).Length > 0)
                {
                    _graph.AddSeriesR(Resources.RPM, Color.CornflowerBlue,
                        ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row),
                        time, AlgorithmType.ROTATE_E, isRotation);
                }

                VehicleInfo vehicleInfo = new VehicleInfo(m_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        }

        /// <summary>
        /// ������� ������ ���� �������, ����������� � ���������
        /// </summary>
        public void ClearReportsData()
        {
            foreach (XtraTabPage page in reports)
            {
                ((IReportTabControl) page.Tag).ClearReport();
            }
        }

        #region IReportCollection Members

        public IEnumerator<IReportTabControl> GetEnumerator()
        {
            foreach (XtraTabPage page in reports)
            {
                yield return (IReportTabControl) page.Tag;
            }
        }

        /// <summary>
        /// ������� ������ �������
        /// </summary>
        public void RemoveAll()
        {
            reports.Clear();
        }

        #endregion

        public List<IReportTabControl> getReports
        {
            get
            {
                List<IReportTabControl> result = new List<IReportTabControl>();
                foreach (XtraTabPage page in reports)
                {
                    if (reportListCtrl.FindReportSetting( //��������� ������ �������� � ������ ������
                        ((IReportTabControl) page.Tag).Control.GetType().Name).Visible)
                    {
                        result.Add((IReportTabControl) page.Tag);
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// ���������� ������� ������� ������ ������ ������ ������
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void btnReportList_Click(object sender, EventArgs e)
        {
            ShowReportListForm();
        }

        /// <summary>
        /// ����������� ��������� ����� ������ �������
        /// </summary>
        private void ShowReportListForm()
        {
            if (reportListCtrl.ShowForm() == DialogResult.OK)
                HandleChangesInReportList();
        }

        /// <summary>
        /// ��������� ��������� � ������ �������
        /// </summary>
        public void HandleChangesInReportList()
        {
            Cursor originalCursor = null;
            if (ParentForm != null)
            {
                originalCursor = ParentForm.Cursor;
                ParentForm.Cursor = Cursors.WaitCursor;
            }
            try
            {
                // ������ ������� ����� � ��������
                _xtraTabs.TabPages.Clear();
                // ������� ������� ����� �������
                foreach (XtraTabPage page in reports)
                {
                    if (reportListCtrl.FindReportSetting(
                        ((IReportTabControl) page.Tag).Control.GetType().Name).Visible)
                    {
                        _xtraTabs.TabPages.Add(page);
                    }
                }
            }
            finally
            {
                if (ParentForm != null) ParentForm.Cursor = originalCursor;
            }
        }
    }
}
