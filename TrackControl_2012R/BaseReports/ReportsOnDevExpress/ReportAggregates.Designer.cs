﻿namespace BaseReports.ReportsOnDevExpress
{
    partial class ReportAggregates
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportAggregates));
            this.gcAggregate = new DevExpress.XtraGrid.GridControl();
            this.gvAggregate = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameForReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentRFIDname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStops = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink2 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAggregate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAggregate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink2.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcAggregate
            // 
            this.gcAggregate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcAggregate.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcAggregate.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcAggregate.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcAggregate.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcAggregate.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcAggregate.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcAggregate.Location = new System.Drawing.Point(0, 24);
            this.gcAggregate.MainView = this.gvAggregate;
            this.gcAggregate.Name = "gcAggregate";
            this.gcAggregate.Size = new System.Drawing.Size(958, 458);
            this.gcAggregate.TabIndex = 5;
            this.gcAggregate.UseEmbeddedNavigator = true;
            this.gcAggregate.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAggregate,
            this.gridView2});
            // 
            // gvAggregate
            // 
            this.gvAggregate.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvAggregate.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvAggregate.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvAggregate.Appearance.Empty.Options.UseBackColor = true;
            this.gvAggregate.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvAggregate.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvAggregate.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvAggregate.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvAggregate.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvAggregate.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAggregate.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvAggregate.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvAggregate.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvAggregate.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvAggregate.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvAggregate.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvAggregate.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvAggregate.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvAggregate.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvAggregate.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvAggregate.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvAggregate.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvAggregate.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvAggregate.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvAggregate.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvAggregate.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvAggregate.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvAggregate.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvAggregate.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvAggregate.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvAggregate.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvAggregate.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.GroupRow.Options.UseFont = true;
            this.gvAggregate.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvAggregate.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvAggregate.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvAggregate.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvAggregate.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvAggregate.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvAggregate.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvAggregate.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvAggregate.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.Preview.Options.UseBackColor = true;
            this.gvAggregate.Appearance.Preview.Options.UseForeColor = true;
            this.gvAggregate.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvAggregate.Appearance.Row.Options.UseBackColor = true;
            this.gvAggregate.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvAggregate.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvAggregate.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvAggregate.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvAggregate.Appearance.VertLine.Options.UseBackColor = true;
            this.gvAggregate.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvAggregate.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvAggregate.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvAggregate.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvAggregate.ColumnPanelRowHeight = 40;
            this.gvAggregate.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameForReport,
            this.colIdentRFIDname,
            this.colLocationStart,
            this.colTimeStart,
            this.colLocationEnd,
            this.colTimeEnd,
            this.colTimeDuration,
            this.colDistance,
            this.colTimeStops});
            this.gvAggregate.GridControl = this.gcAggregate;
            this.gvAggregate.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colDistance, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeDuration", this.colTimeDuration, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeStops", this.colTimeStops, "")});
            this.gvAggregate.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvAggregate.Name = "gvAggregate";
            this.gvAggregate.OptionsDetail.AllowZoomDetail = false;
            this.gvAggregate.OptionsDetail.EnableMasterViewMode = false;
            this.gvAggregate.OptionsDetail.ShowDetailTabs = false;
            this.gvAggregate.OptionsDetail.SmartDetailExpand = false;
            this.gvAggregate.OptionsView.ColumnAutoWidth = false;
            this.gvAggregate.OptionsView.EnableAppearanceEvenRow = true;
            this.gvAggregate.OptionsView.EnableAppearanceOddRow = true;
            this.gvAggregate.OptionsView.ShowFooter = true;
            // 
            // colNameForReport
            // 
            this.colNameForReport.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameForReport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameForReport.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameForReport.Caption = "Прицепной агрегат";
            this.colNameForReport.FieldName = "NameForReport";
            this.colNameForReport.Name = "colNameForReport";
            this.colNameForReport.OptionsColumn.AllowEdit = false;
            this.colNameForReport.OptionsColumn.AllowFocus = false;
            this.colNameForReport.OptionsColumn.ReadOnly = true;
            this.colNameForReport.Visible = true;
            this.colNameForReport.VisibleIndex = 0;
            this.colNameForReport.Width = 150;
            // 
            // colIdentRFIDname
            // 
            this.colIdentRFIDname.AppearanceCell.Options.UseTextOptions = true;
            this.colIdentRFIDname.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentRFIDname.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdentRFIDname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentRFIDname.Caption = "RFID";
            this.colIdentRFIDname.FieldName = "IdentRfidName";
            this.colIdentRFIDname.Name = "colIdentRFIDname";
            this.colIdentRFIDname.OptionsColumn.AllowEdit = false;
            this.colIdentRFIDname.OptionsColumn.AllowFocus = false;
            this.colIdentRFIDname.OptionsColumn.ReadOnly = true;
            this.colIdentRFIDname.Visible = true;
            this.colIdentRFIDname.VisibleIndex = 1;
            // 
            // colLocationStart
            // 
            this.colLocationStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocationStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocationStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocationStart.Caption = "Начало движения";
            this.colLocationStart.FieldName = "LocationStart";
            this.colLocationStart.Name = "colLocationStart";
            this.colLocationStart.OptionsColumn.AllowEdit = false;
            this.colLocationStart.OptionsColumn.AllowFocus = false;
            this.colLocationStart.OptionsColumn.ReadOnly = true;
            this.colLocationStart.Visible = true;
            this.colLocationStart.VisibleIndex = 2;
            this.colLocationStart.Width = 150;
            // 
            // colTimeStart
            // 
            this.colTimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStart.Caption = "Начало смены";
            this.colTimeStart.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStart.FieldName = "TimeStart";
            this.colTimeStart.Name = "colTimeStart";
            this.colTimeStart.OptionsColumn.AllowEdit = false;
            this.colTimeStart.OptionsColumn.AllowFocus = false;
            this.colTimeStart.OptionsColumn.ReadOnly = true;
            this.colTimeStart.Visible = true;
            this.colTimeStart.VisibleIndex = 3;
            this.colTimeStart.Width = 137;
            // 
            // colLocationEnd
            // 
            this.colLocationEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocationEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocationEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocationEnd.Caption = "Окончание движения";
            this.colLocationEnd.FieldName = "LocationEnd";
            this.colLocationEnd.Name = "colLocationEnd";
            this.colLocationEnd.OptionsColumn.AllowEdit = false;
            this.colLocationEnd.OptionsColumn.AllowFocus = false;
            this.colLocationEnd.OptionsColumn.ReadOnly = true;
            this.colLocationEnd.Visible = true;
            this.colLocationEnd.VisibleIndex = 4;
            this.colLocationEnd.Width = 150;
            // 
            // colTimeEnd
            // 
            this.colTimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEnd.Caption = "Окончание смены";
            this.colTimeEnd.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEnd.FieldName = "TimeEnd";
            this.colTimeEnd.Name = "colTimeEnd";
            this.colTimeEnd.OptionsColumn.AllowEdit = false;
            this.colTimeEnd.OptionsColumn.AllowFocus = false;
            this.colTimeEnd.OptionsColumn.ReadOnly = true;
            this.colTimeEnd.Visible = true;
            this.colTimeEnd.VisibleIndex = 5;
            this.colTimeEnd.Width = 125;
            // 
            // colTimeDuration
            // 
            this.colTimeDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeDuration.Caption = "Длительность смены";
            this.colTimeDuration.FieldName = "TimeDuration";
            this.colTimeDuration.Name = "colTimeDuration";
            this.colTimeDuration.OptionsColumn.AllowEdit = false;
            this.colTimeDuration.OptionsColumn.AllowFocus = false;
            this.colTimeDuration.OptionsColumn.ReadOnly = true;
            this.colTimeDuration.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeDuration.Visible = true;
            this.colTimeDuration.VisibleIndex = 6;
            this.colTimeDuration.Width = 109;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Пробег";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 7;
            this.colDistance.Width = 85;
            // 
            // colTimeStops
            // 
            this.colTimeStops.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStops.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStops.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStops.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStops.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStops.Caption = "Общее время стоянок";
            this.colTimeStops.FieldName = "TimeStops";
            this.colTimeStops.Name = "colTimeStops";
            this.colTimeStops.OptionsColumn.AllowEdit = false;
            this.colTimeStops.OptionsColumn.AllowFocus = false;
            this.colTimeStops.OptionsColumn.ReadOnly = true;
            this.colTimeStops.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeStops.Visible = true;
            this.colTimeStops.VisibleIndex = 8;
            this.colTimeStops.Width = 106;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcAggregate;
            this.gridView2.Name = "gridView2";
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 100, 100);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(5, 5, 5, 5);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = null;
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink2});
            // 
            // compositeLink2
            // 
            // 
            // 
            // 
            this.compositeLink2.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink2.ImageCollection.ImageStream")));
            this.compositeLink2.Landscape = true;
            this.compositeLink2.Margins = new System.Drawing.Printing.Margins(10, 10, 80, 10);
            this.compositeLink2.MinMargins = new System.Drawing.Printing.Margins(5, 5, 5, 5);
            this.compositeLink2.PrintingSystemBase = this.printingSystem1;
            // 
            // ReportAggregates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcAggregate);
            this.Name = "ReportAggregates";
            this.Size = new System.Drawing.Size(958, 482);
            this.Controls.SetChildIndex(this.gcAggregate, 0);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAggregate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAggregate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink2.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcAggregate;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAggregate;
        private DevExpress.XtraGrid.Columns.GridColumn colNameForReport;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentRFIDname;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationStart;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStops;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink2;

    }
}
