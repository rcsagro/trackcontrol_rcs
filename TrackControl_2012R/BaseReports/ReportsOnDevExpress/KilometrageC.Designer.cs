﻿namespace BaseReports.ReportsDE
{
    partial class KilometrageC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( KilometrageC ) );
            this.gcKlmtC = new DevExpress.XtraGrid.GridControl();
            this.gvKlmtC = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._images = new DevExpress.Utils.ImageCollection( this.components );
            this._colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colInitialTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colFinalTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colDistFromBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colAvgSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
            this.bar3 = new DevExpress.XtraBars.Bar();
            this._distanceStatus = new DevExpress.XtraBars.BarStaticItem();
            this._motionTimeStatus = new DevExpress.XtraBars.BarStaticItem();
            this._parkingTimeStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._source = new System.Windows.Forms.BindingSource( this.components );
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            this.imageCollection = new DevExpress.Utils.ImageCollection( this.components );
            ( ( System.ComponentModel.ISupportInitialize )( this.gcKlmtC ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gvKlmtC ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._images ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._source ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeReportLink.ImageCollection ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.imageCollection ) ).BeginInit();
            this.SuspendLayout();
            // 
            // gcKlmtC
            // 
            this.gcKlmtC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gcKlmtC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcKlmtC.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcKlmtC.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcKlmtC.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcKlmtC.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcKlmtC.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcKlmtC.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcKlmtC.Location = new System.Drawing.Point( 0, 26 );
            this.gcKlmtC.MainView = this.gvKlmtC;
            this.gcKlmtC.MenuManager = this.barManager1;
            this.gcKlmtC.Name = "gcKlmtC";
            this.gcKlmtC.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1} );
            this.gcKlmtC.Size = new System.Drawing.Size( 822, 440 );
            this.gcKlmtC.TabIndex = 9;
            this.gcKlmtC.UseEmbeddedNavigator = true;
            this.gcKlmtC.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvKlmtC,
            this.gridView2} );
            // 
            // gvKlmtC
            // 
            this.gvKlmtC.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvKlmtC.Appearance.Empty.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvKlmtC.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvKlmtC.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvKlmtC.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvKlmtC.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvKlmtC.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvKlmtC.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvKlmtC.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvKlmtC.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvKlmtC.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvKlmtC.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvKlmtC.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvKlmtC.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.gvKlmtC.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.GroupRow.Options.UseFont = true;
            this.gvKlmtC.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvKlmtC.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvKlmtC.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvKlmtC.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.gvKlmtC.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.gvKlmtC.Appearance.OddRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvKlmtC.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.Preview.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.Preview.Options.UseForeColor = true;
            this.gvKlmtC.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvKlmtC.Appearance.Row.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvKlmtC.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvKlmtC.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvKlmtC.Appearance.VertLine.Options.UseBackColor = true;
            this.gvKlmtC.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvKlmtC.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvKlmtC.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvKlmtC.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvKlmtC.ColumnPanelRowHeight = 40;
            this.gvKlmtC.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._colState,
            this._colLocation,
            this._colInitialTime,
            this._colFinalTime,
            this._colInterval,
            this._colDistance,
            this._colDistFromBegin,
            this._colAvgSpeed,
            this.colMaxSpeed} );
            this.gvKlmtC.GridControl = this.gcKlmtC;
            this.gvKlmtC.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvKlmtC.Name = "gvKlmtC";
            this.gvKlmtC.OptionsDetail.AllowZoomDetail = false;
            this.gvKlmtC.OptionsDetail.EnableMasterViewMode = false;
            this.gvKlmtC.OptionsDetail.ShowDetailTabs = false;
            this.gvKlmtC.OptionsDetail.SmartDetailExpand = false;
            this.gvKlmtC.OptionsSelection.MultiSelect = true;
            this.gvKlmtC.OptionsView.EnableAppearanceEvenRow = true;
            this.gvKlmtC.OptionsView.EnableAppearanceOddRow = true;
            this.gvKlmtC.OptionsView.ShowFooter = true;
            // 
            // _colState
            // 
            this._colState.AppearanceCell.Options.UseTextOptions = true;
            this._colState.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colState.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colState.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colState.AppearanceHeader.Options.UseTextOptions = true;
            this._colState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colState.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colState.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colState.Caption = "Состояние";
            this._colState.ColumnEdit = this._imageComboRepo;
            this._colState.FieldName = "State";
            this._colState.Name = "_colState";
            this._colState.OptionsColumn.AllowEdit = false;
            this._colState.OptionsColumn.AllowFocus = false;
            this._colState.OptionsColumn.ReadOnly = true;
            this._colState.ToolTip = "Состояние";
            this._colState.Visible = true;
            this._colState.VisibleIndex = 0;
            this._colState.Width = 70;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange( new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)} );
            this._imageComboRepo.Items.AddRange( new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)} );
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            this._imageComboRepo.SmallImages = this._images;
            // 
            // _images
            // 
            this._images.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "_images.ImageStream" ) ) );
            this._images.Images.SetKeyName( 0, "Parking.png" );
            this._images.Images.SetKeyName( 1, "Highway.png" );
            // 
            // _colLocation
            // 
            this._colLocation.AppearanceCell.Options.UseTextOptions = true;
            this._colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this._colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colLocation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colLocation.Caption = "Местоположение";
            this._colLocation.FieldName = "Location";
            this._colLocation.Name = "_colLocation";
            this._colLocation.OptionsColumn.AllowEdit = false;
            this._colLocation.OptionsColumn.AllowFocus = false;
            this._colLocation.OptionsColumn.ReadOnly = true;
            this._colLocation.ToolTip = "Местоположение";
            this._colLocation.Visible = true;
            this._colLocation.VisibleIndex = 1;
            this._colLocation.Width = 200;
            // 
            // _colInitialTime
            // 
            this._colInitialTime.AppearanceCell.Options.UseTextOptions = true;
            this._colInitialTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialTime.AppearanceHeader.Options.UseTextOptions = true;
            this._colInitialTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialTime.Caption = "Время начала";
            this._colInitialTime.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this._colInitialTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colInitialTime.FieldName = "InitialTime";
            this._colInitialTime.Name = "_colInitialTime";
            this._colInitialTime.OptionsColumn.AllowEdit = false;
            this._colInitialTime.OptionsColumn.AllowFocus = false;
            this._colInitialTime.OptionsColumn.ReadOnly = true;
            this._colInitialTime.ToolTip = "Время начала";
            this._colInitialTime.Visible = true;
            this._colInitialTime.VisibleIndex = 2;
            this._colInitialTime.Width = 90;
            // 
            // _colFinalTime
            // 
            this._colFinalTime.AppearanceCell.Options.UseTextOptions = true;
            this._colFinalTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colFinalTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colFinalTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colFinalTime.AppearanceHeader.Options.UseTextOptions = true;
            this._colFinalTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colFinalTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colFinalTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colFinalTime.Caption = "Время окончания";
            this._colFinalTime.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this._colFinalTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colFinalTime.FieldName = "FinalTime";
            this._colFinalTime.Name = "_colFinalTime";
            this._colFinalTime.OptionsColumn.AllowEdit = false;
            this._colFinalTime.OptionsColumn.AllowFocus = false;
            this._colFinalTime.OptionsColumn.ReadOnly = true;
            this._colFinalTime.ToolTip = "Время окончания";
            this._colFinalTime.Visible = true;
            this._colFinalTime.VisibleIndex = 3;
            this._colFinalTime.Width = 90;
            // 
            // _colInterval
            // 
            this._colInterval.AppearanceCell.Options.UseTextOptions = true;
            this._colInterval.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInterval.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInterval.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInterval.AppearanceHeader.Options.UseTextOptions = true;
            this._colInterval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInterval.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInterval.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInterval.Caption = "Интервал, дни.время";
            this._colInterval.ColumnEdit = this.repositoryItemTextEdit1;
            this._colInterval.DisplayFormat.FormatString = "dd.HH:mm";
            this._colInterval.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colInterval.FieldName = "Interval";
            this._colInterval.Name = "_colInterval";
            this._colInterval.OptionsColumn.AllowEdit = false;
            this._colInterval.OptionsColumn.AllowFocus = false;
            this._colInterval.OptionsColumn.ReadOnly = true;
            this._colInterval.SummaryItem.DisplayFormat = "{0:dd.HH:mm}";
            this._colInterval.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._colInterval.ToolTip = "Интервал дни.время";
            this._colInterval.Visible = true;
            this._colInterval.VisibleIndex = 4;
            this._colInterval.Width = 90;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // _colDistance
            // 
            this._colDistance.AppearanceCell.Options.UseTextOptions = true;
            this._colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this._colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistance.Caption = "Пробег, км";
            this._colDistance.DisplayFormat.FormatString = "N2";
            this._colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._colDistance.FieldName = "Distance";
            this._colDistance.Name = "_colDistance";
            this._colDistance.OptionsColumn.AllowEdit = false;
            this._colDistance.OptionsColumn.AllowFocus = false;
            this._colDistance.OptionsColumn.ReadOnly = true;
            this._colDistance.SummaryItem.DisplayFormat = "{0:f2}";
            this._colDistance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._colDistance.ToolTip = "Пробег, км";
            this._colDistance.Visible = true;
            this._colDistance.VisibleIndex = 5;
            this._colDistance.Width = 50;
            // 
            // _colDistFromBegin
            // 
            this._colDistFromBegin.AppearanceCell.Options.UseTextOptions = true;
            this._colDistFromBegin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistFromBegin.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistFromBegin.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistFromBegin.AppearanceHeader.Options.UseTextOptions = true;
            this._colDistFromBegin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistFromBegin.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistFromBegin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistFromBegin.Caption = "Пробег с начала периода, км";
            this._colDistFromBegin.DisplayFormat.FormatString = "N2";
            this._colDistFromBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._colDistFromBegin.FieldName = "GeneralDistance";
            this._colDistFromBegin.Name = "_colDistFromBegin";
            this._colDistFromBegin.OptionsColumn.AllowEdit = false;
            this._colDistFromBegin.OptionsColumn.AllowFocus = false;
            this._colDistFromBegin.OptionsColumn.ReadOnly = true;
            this._colDistFromBegin.SummaryItem.DisplayFormat = "{0:f2}";
            this._colDistFromBegin.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this._colDistFromBegin.ToolTip = "Пробег с начала периода, км";
            this._colDistFromBegin.Visible = true;
            this._colDistFromBegin.VisibleIndex = 6;
            this._colDistFromBegin.Width = 70;
            // 
            // _colAvgSpeed
            // 
            this._colAvgSpeed.AppearanceCell.Options.UseTextOptions = true;
            this._colAvgSpeed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colAvgSpeed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colAvgSpeed.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colAvgSpeed.AppearanceHeader.Options.UseTextOptions = true;
            this._colAvgSpeed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colAvgSpeed.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colAvgSpeed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colAvgSpeed.Caption = "Средняя скорость, км/ч";
            this._colAvgSpeed.DisplayFormat.FormatString = "N2";
            this._colAvgSpeed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._colAvgSpeed.FieldName = "AverageSpeed";
            this._colAvgSpeed.Name = "_colAvgSpeed";
            this._colAvgSpeed.OptionsColumn.AllowEdit = false;
            this._colAvgSpeed.OptionsColumn.AllowFocus = false;
            this._colAvgSpeed.OptionsColumn.ReadOnly = true;
            this._colAvgSpeed.ToolTip = "Средняя скорость, км/ч";
            this._colAvgSpeed.Visible = true;
            this._colAvgSpeed.VisibleIndex = 7;
            this._colAvgSpeed.Width = 90;
            // 
            // colMaxSpeed
            // 
            this.colMaxSpeed.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxSpeed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxSpeed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMaxSpeed.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxSpeed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxSpeed.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMaxSpeed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxSpeed.Caption = "Максимальная скорость, км/ч";
            this.colMaxSpeed.DisplayFormat.FormatString = "N2";
            this.colMaxSpeed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxSpeed.FieldName = "MaxSpeed";
            this.colMaxSpeed.Name = "colMaxSpeed";
            this.colMaxSpeed.OptionsColumn.AllowEdit = false;
            this.colMaxSpeed.OptionsColumn.AllowFocus = false;
            this.colMaxSpeed.OptionsColumn.ReadOnly = true;
            this.colMaxSpeed.ToolTip = "Максимальная скорость, км/ч";
            this.colMaxSpeed.Visible = true;
            this.colMaxSpeed.VisibleIndex = 8;
            this.colMaxSpeed.Width = 60;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar3} );
            this.barManager1.DockControls.Add( this.barDockControl1 );
            this.barManager1.DockControls.Add( this.barDockControl2 );
            this.barManager1.DockControls.Add( this.barDockControl3 );
            this.barManager1.DockControls.Add( this.barDockControl4 );
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this._distanceStatus,
            this._motionTimeStatus,
            this._parkingTimeStatus} );
            this.barManager1.MaxItemId = 3;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._distanceStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this._motionTimeStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this._parkingTimeStatus)} );
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // _distanceStatus
            // 
            this._distanceStatus.Caption = "Суммарный пробег: --- км";
            this._distanceStatus.Id = 0;
            this._distanceStatus.Name = "_distanceStatus";
            this._distanceStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _motionTimeStatus
            // 
            this._motionTimeStatus.Caption = "Время в пути: --:--:--";
            this._motionTimeStatus.Id = 1;
            this._motionTimeStatus.Name = "_motionTimeStatus";
            this._motionTimeStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _parkingTimeStatus
            // 
            this._parkingTimeStatus.Caption = "Время стоянок: --:--:--";
            this._parkingTimeStatus.Id = 2;
            this._parkingTimeStatus.Name = "_parkingTimeStatus";
            this._parkingTimeStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcKlmtC;
            this.gridView2.Name = "gridView2";
            // 
            // _source
            // 
            this._source.DataMember = "KilometrageReport";
            // 
            // printingSystem
            // 
            this.printingSystem.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Xls.ShowGridLines = true;
            this.printingSystem.ExportOptions.Xlsx.ShowGridLines = true;
            this.printingSystem.Links.AddRange( new object[] {
            this.compositeReportLink} );
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "compositeReportLink.ImageCollection.ImageStream" ) ) );
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins( 25, 25, 70, 25 );
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins( 25, 25, 15, 25 );
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystem = this.printingSystem;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem;
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "imageCollection.ImageStream" ) ) );
            this.imageCollection.Images.SetKeyName( 0, "ParkingMaps.png" );
            this.imageCollection.Images.SetKeyName( 1, "Parking.png" );
            // 
            // KilometrageC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.gcKlmtC );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "KilometrageC";
            this.Size = new System.Drawing.Size( 822, 515 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.gcKlmtC, 0 );
            ( ( System.ComponentModel.ISupportInitialize )( this.gcKlmtC ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gvKlmtC ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._images ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._source ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeReportLink.ImageCollection ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.imageCollection ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcKlmtC;
        private DevExpress.XtraGrid.Views.Grid.GridView gvKlmtC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn _colState;
        private DevExpress.XtraGrid.Columns.GridColumn _colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn _colInitialTime;
        private DevExpress.XtraGrid.Columns.GridColumn _colFinalTime;
        private DevExpress.XtraGrid.Columns.GridColumn _colInterval;
        private DevExpress.XtraGrid.Columns.GridColumn _colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn _colDistFromBegin;
        private DevExpress.XtraGrid.Columns.GridColumn _colAvgSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxSpeed;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem _distanceStatus;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem _motionTimeStatus;
        private DevExpress.XtraBars.BarStaticItem _parkingTimeStatus;
        private System.Windows.Forms.BindingSource _source;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private DevExpress.Utils.ImageCollection _images;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
