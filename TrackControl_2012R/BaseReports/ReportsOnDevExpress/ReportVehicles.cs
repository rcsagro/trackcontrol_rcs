﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.ReportsDE;
using BaseReports.RFID;
using DevExpress.XtraBars;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.General;

namespace BaseReports.ReportsOnDevExpress
{
    public partial class ReportVehicles : BaseReports.ReportsDE.BaseControl
    {
        struct GraphData
        {
            public double ValueY;
            public DateTime ValueX;

            public GraphData(int valueY, DateTime valueX)
            {
                ValueX = valueX;
                ValueY = valueY;
            }
        }

        public class TVehicleID
        {
            private string nameForReport;
            private ushort identID;
            private string identRfidName;
            private string locationStart;
            private DateTime timeStart;
            private string locationEnd;
            private DateTime timeEnd;
            private TimeSpan timeDuration;
            private double distance;
            private TimeSpan timeStops;
            private TimeSpan timeEngineOn;
            private TimeSpan timeEngineOnStops;
            private double fuelStart;
            private double fuelEnd;
            private double fuelAdd;
            private double fuelSub;
            private double fuelExpence;
            private double fuelExpenceAvgKlm;
            private double fuelExpenceAvgHr;

            public TVehicleID()
            {
                // to do
            }

            public string NameForReport
            {
                get { return nameForReport; }
                set { nameForReport = value; }
            }

            public ushort IdentID
            {
                get { return identID; }
                set { identID = value; }
            }

            public string IdentRfidName
            {
                get { return identRfidName; }
                set { identRfidName = value; }
            }

            public string LocationStart
            {
                get { return locationStart; }
                set { locationStart = value; }
            }

            public DateTime TimeStart
            {
                get { return timeStart; }
                set { timeStart = value; }
            }

            public string LocationEnd
            {
                get { return locationEnd; }
                set { locationEnd = value; }
            }

            public DateTime TimeEnd
            {
                get { return timeEnd; }
                set { timeEnd = value; }
            }

            public TimeSpan TimeDuration
            {
                get { return timeDuration; }
                set { timeDuration = value; }
            }

            public double Distance
            {
                get { return distance; }
                set { distance = value; }
            }

            public TimeSpan TimeStops
            {
                get { return timeStops; }
                set { timeStops = value; }
            }

            public TimeSpan TimeEngineOn
            {
                get { return timeEngineOn; }
                set { timeEngineOn = value; }
            }

            public TimeSpan TimeEngineOnStops
            {
                get { return timeEngineOnStops; }
                set { timeEngineOnStops = value; }
            }

            public double FuelStart
            {
                get { return fuelStart; }
                set { fuelStart = value; }
            }

            public double FuelEnd
            {
                get { return fuelEnd; }
                set { fuelEnd = value; }
            }

            public double FuelAdd
            {
                get { return fuelAdd; }
                set { fuelAdd = value; }
            }

            public double FuelSub
            {
                get { return fuelSub; }
                set { fuelSub = value; }
            }

            public double FuelExpence
            {
                get { return fuelExpence; }
                set { fuelExpence = value; }
            }

            public double FuelExpenceAvgKm
            {
                get { return fuelExpenceAvgKlm; }
                set { fuelExpenceAvgKlm = value; }
            }

            public double FuelExpenceAvgHour
            {
                get { return fuelExpenceAvgHr; }
                set { fuelExpenceAvgHr = value; }
            }
        } // TDriverRfid

        public ReportVehicles()
        {
            InitializeComponent();
            //DisableMenuSecondButton();
            HideStatusBar();
            VisionPanel(gvVehicle, gcVehicle, null);
            btnDrivers.Visibility = BarItemVisibility.Always;
            _algorithm = new VehicleID();
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new Rotation());
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1));
            AddAlgorithm(_algorithm);
            SetActiveToolBar(false);
            Localization();
            gvVehicle.DoubleClick += gvVehicle_DoubleClick;
            gvVehicle.FocusedRowChanged += gvVehicle_FocusedRowChanged;
            linkGrid.CreateReportHeaderArea += linkGrid_CreateReportHeaderArea;

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(linkGrid_CreateMarginalHeaderArea);

            ReportVehRfid =
                new ReportBase<TVehicleID, TInfo>(Controls, compositeLink1, gvVehicle,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        List<VehicleIDRecord> _records;
        GpsData[] _dRows;
        const string GRAPH_SERIES_NAME = "Identificatior TC";
        VehicleID _algorithm;

        /// <summary>
        /// признак детализации печатного отчета
        /// </summary>
        bool _expandAllGroups = true;
        ReportBase<TVehicleID, TInfo> ReportVehRfid;

        public override string Caption
        {
            get { return Resources.VehicleRFID; }
        }

        private void gvVehicle_DoubleClick(object sender, EventArgs e)
        {
            ShowOnMapAndGraphOne();
        }

        private void gvVehicle_FocusedRowChanged(object sender,
            DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                if (AddGraphRFID())
                    ShowOnMapAndGraphOne();
            }
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();

            if (AddGraphRFID())
                ShowOnMapAndGraphOne();
        }

        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowOnMapAndGraphOne();
        }

        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            base.bbiStart_ItemClick(sender, e);
        }

        protected override void btnDrivers_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_flagShow)
            {
                ShowOnMapAndGraphWithoutMarker();
                btnDrivers.Hint = "Показать все транспортные средства на карте";
                _flagShow = false;
                _allShow = false;
            }
            else
            {
                ShowOnMapAndGraph();
                btnDrivers.Hint = "Скрыть все транспортные средства с карты";
                _flagShow = true;
                _allShow = true;
            }
        }

        private void ShowOnMapAndGraph()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if (cm.Count == 0) 
                return;
            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();
            int i = 0;
            VehicleIDRecord recRfidCurrent = (VehicleIDRecord)cm.Current;

            foreach (VehicleIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
                Marker m = new Marker(MarkerType.RFID, recRfid.IdMobitel,
                    new PointLatLng(recRfid.StartGps.LatLng.Lat, recRfid.StartGps.LatLng.Lng), recRfid.NameForReport, "");
                if (recRfid == (VehicleIDRecord)cm.Current) m.IsActive = true;
                markers.Add(m);
            }

            //---------------------------------------------------
            Graph.ClearRegion();
            Graph.AddTimeRegion(recRfidCurrent.TimeStart, recRfidCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);
            //---------------------------------------------------

            segments.Add(GetTrackSegment(recRfidCurrent, Color.Red));
            if (segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        private void ShowOnMapAndGraphWithoutMarker()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];

            if (cm.Count == 0)
                return;

            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();

            int i = 0;
            VehicleIDRecord recRfidCurrent = (VehicleIDRecord)cm.Current;

            foreach (VehicleIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
            }

            //---------------------------------------------------
            Graph.ClearRegion();
            Graph.AddTimeRegion(recRfidCurrent.TimeStart, recRfidCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);

            //---------------------------------------------------

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            gcVehicle.DataSource = null;
            SetActiveToolBar(false);
        }

        bool _flagShow = false;
        bool _allShow = false;

        void SetActiveToolBar(bool active)
        {
            bbiPrintReport.Enabled = active;
            bbiShowOnMap.Enabled = active;
            bbiShowOnGraf.Enabled = active;
            btnDrivers.Enabled = active;
            btnDrivers.Hint = "Показать транспортные средства на карте";
            btnDrivers.Glyph = imageCollection1.Images[0];
            _flagShow = false;
            _allShow = false;

            if (active)
                SetStartButton();
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is BaseReports.Procedure.VehicleID)
                Select(curMrow);
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            base.Select(mobitel);

            if (mobitel == null || Algorithm.DicIDRecords == null ||
                !Algorithm.DicIDRecords.ContainsKey(mobitel.Mobitel_ID))
            {
                ClearReport();
                return;
            }

            _records = Algorithm.DicIDRecords[mobitel.Mobitel_ID];
            _dRows = DataSetManager.GetDataGpsArray(mobitel);
            gcVehicle.DataSource = _records;

            if (_records.Count > 0)
            {
                SetActiveToolBar(true);

                if (AddGraphRFID())
                    ShowOnMapAndGraphOne();
            }
        }

        private void ShowOnMapAndGraphOne()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if (cm.Count == 0) return;
            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();
            int i = 0;
            VehicleIDRecord recIDCurrent = (VehicleIDRecord)cm.Current;
            foreach (VehicleIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
                Marker m = new Marker(MarkerType.RFID, recRfid.IdMobitel,
                    new PointLatLng(recRfid.StartGps.LatLng.Lat, recRfid.StartGps.LatLng.Lng), recRfid.NameForReport, "");
                if (recRfid == (VehicleIDRecord)cm.Current)
                {
                    m.IsActive = true;
                    markers.Add(m);
                    continue;
                }

                if (_allShow)
                    markers.Add(m);
            }
            //---------------------------------------------------
            Graph.ClearRegion();
            Graph.AddTimeRegion(recIDCurrent.TimeStart, recIDCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);

            //---------------------------------------------------
            segments.Add(GetTrackSegment(recIDCurrent, Color.Red));
            if (segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        Track GetTrackSegment(VehicleIDRecord recRfid, Color color)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;
            foreach (GpsData dGps in _dRows)
            {
                if (dGps.Id == recRfid.StartGps.Id)
                    inside = true;
                if (inside)
                    data.Add(dGps);
                if (dGps.Id == recRfid.FinalGps.Id)
                    break;
            }
            if (data.Count > 0)
            {
                return new Track(recRfid.IdMobitel, color, 2f, data);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// для графика данные одной записи
        /// </summary>
        /// <param name="recRfid"></param>
        /// <returns></returns>
        List<GraphData> CreateImpuls(VehicleIDRecord recRfid)
        {
            List<GraphData> gds = new List<GraphData>();
            GraphData gd = new GraphData(recRfid.IdentID, recRfid.TimeStart);
            gds.Add(gd);
            gd = new GraphData(recRfid.IdentID, recRfid.TimeEnd);
            gds.Add(gd);
            return gds;
        }

        /// <summary>
        /// создает на общем графике график RFID
        /// </summary>
        bool AddGraphRFID()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if (cm.Count == 0) return false;
            List<GraphData> graphRFID = new List<GraphData>();
            foreach (VehicleIDRecord recRfid in cm.List)
            {
                List<GraphData> gds = CreateImpuls(recRfid);
                foreach (GraphData gd in gds)
                {
                    graphRFID.Add(gd);
                }
            }
            double[] Y = new double[graphRFID.Count];
            DateTime[] X = new DateTime[graphRFID.Count];
            int indexXY = 0;
            foreach (GraphData gd in graphRFID)
            {
                Y[indexXY] = gd.ValueY;
                X[indexXY] = gd.ValueX;
                indexXY++;
            }
            Graph.AddSeriesL(GRAPH_SERIES_NAME, Color.Brown, Y, X, AlgorithmType.VEHICLE_FUELER_IDENT, true);
            return true;
        }

        // created header of report area
        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);
            string reportHeader = Resources.VehicleRFID;
            if (gvVehicle.GroupCount > 0 && !_expandAllGroups) 
                reportHeader = Resources.ReportSummaryOnTheEngineSpeed;
            DevExpressReportHeader(reportHeader, e);
            DevExpressReportSubHeader(string.Format("{3} {0} {4} {1}\n{5}: {2}",
                Algorithm.Period.Begin, Algorithm.Period.End, info.Info,
                Resources.PeriodFrom, Resources.PeriodTo, Resources.Vehicle), 60, e);
        }

        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row)) // обходим выбранные машины
                {
                    base.Select(m_row);
                    if (m_row == null || Algorithm.DicIDRecords == null ||
                        !Algorithm.DicIDRecords.ContainsKey(m_row.Mobitel_ID))
                    {
                        break;
                    }

                    _records = Algorithm.DicIDRecords[m_row.Mobitel_ID];
                    VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);
                    TInfo t_info = new TInfo();

                    t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                    t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                    t_info.periodBeging = Algorithm.Period.Begin;
                    t_info.periodEnd = Algorithm.Period.End;

                    ReportVehRfid.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                    ReportVehRfid.CreateBindDataList(); // создать новый список данных таблицы отчета

                    for (int i = 0; i < _records.Count; i++)
                    {
                        TVehicleID tdrvRfid = new TVehicleID();
                        VehicleIDRecord drvRfid = _records[i];

                        tdrvRfid.NameForReport = drvRfid.NameForReport;
                        tdrvRfid.IdentID = drvRfid.IdentID;
                        tdrvRfid.IdentRfidName = drvRfid.IdentIDName;
                        tdrvRfid.LocationStart = drvRfid.LocationStart;
                        tdrvRfid.TimeStart = drvRfid.TimeStart;
                        tdrvRfid.LocationEnd = drvRfid.LocationEnd;
                        tdrvRfid.TimeEnd = drvRfid.TimeEnd;
                        tdrvRfid.TimeDuration = drvRfid.TimeDuration;
                        tdrvRfid.Distance = drvRfid.Distance;
                        tdrvRfid.TimeStops = drvRfid.TimeStops;
                        tdrvRfid.TimeEngineOn = drvRfid.TimeEngineOn;
                        tdrvRfid.TimeEngineOnStops = drvRfid.TimeEngineOnStops;
                        tdrvRfid.FuelStart = drvRfid.FuelStart;
                        tdrvRfid.FuelEnd = drvRfid.FuelStart;
                        tdrvRfid.FuelAdd = drvRfid.FuelAdd;
                        tdrvRfid.FuelSub = drvRfid.FuelSub;
                        tdrvRfid.FuelExpence = drvRfid.FuelExpense;
                        tdrvRfid.FuelExpenceAvgKm = drvRfid.FuelExpenseAvgKm;
                        tdrvRfid.FuelExpenceAvgHour = drvRfid.FuelExpenseAvgHour;

                        ReportVehRfid.AddDataToBindList(tdrvRfid);
                    } // for

                    ReportVehRfid.CreateElementReport();
                } // if
            } // foreach

            ReportVehRfid.CreateAndShowReport();
            ReportVehRfid.DeleteData();
        } // ExportAllDevToReport

        // Формирование верхнего колонтитула отчета
        private void linkGrid_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.DriverRfid, e);
            TInfo info = ReportVehRfid.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования верхней/средней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования левой части заголовка отчета */

        protected string GetStringBreackLeft()
        {
            TInfo info = ReportVehRfid.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                    Resources.Driver + ": " + info.infoDriverName);
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */

        protected string GetStringBreackRight()
        {
            TInfo info = ReportVehRfid.GetInfoStructure;
            return ("");
        }

        // created report
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvVehicle, true, true);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        void Localization()
        {
            btnDrivers.Caption = "Транспортные средства";
            btnDrivers.Hint = "Показать транспортные средства на карте";

            colNameForReport.Caption = Resources.VehicleRFID;
            colLocationStart.Caption = Resources.StartLocation;
            colLocationEnd.Caption = Resources.MovementEndPlace;
            colTimeStart.Caption = Resources.ShiftStart;
            colTimeEnd.Caption = Resources.ShiftEnd;
            colTimeDuration.Caption = Resources.ShiftDuration;
            colTimeStops.Caption = Resources.FuelExControDutDayTimeStops;
            colDistance.Caption = Resources.Kilometrage;
            colTimeEngineOn.Caption = Resources.TimeEngineOn;
            colTimeEngineOnStops.Caption = Resources.TimeStopsEngineOn;
            colFuelStart.Caption = Resources.FuelStart;
            colFuelEnd.Caption = Resources.FuelEnd;
            colFuelAdd.Caption = Resources.FuelCtrlCaption;
            colFuelSub.Caption = Resources.FuelLarceny;
            colFuelExpense.Caption = String.Format("{0},{1}", Resources.OutDataFormTotalFuelRate,
                Resources.LitreAbbreviation);
            colFuelExpenseAvgKm.Caption = Resources.FuelRate100km;
            colFuelExpenseAvgHour.Caption = String.Format("{0},{1}", Resources.FuelConsumptionShort,
                Resources.LphAbbreviation);
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvVehicle);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvVehicle);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcVehicle);
        }
    }
}
