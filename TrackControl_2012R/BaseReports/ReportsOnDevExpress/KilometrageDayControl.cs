﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure; 
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Utils;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles ;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports.ReportsDE
{
    // Класс контрола для отображения отчета о пробеге
    // и остановках транспортного средства
    [Serializable]
    public partial class KilometrageDayControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportKlmtDay
        {
            string initTime;
            string locatStart;
            string locatEnd;
            string initalTime;
            string finalTime;
            string intervWork;
            string intervMove;
            string timeStopsAll;
            double distance;
            double avrSpeed;
            private double maxSpeed;
            private double normaWay;
            private double normaMotor;
            private double summaNorma;
            private string motorIdle;

            public reportKlmtDay(string initTime, string locatStart, string locatEnd,
                string initalTime, string finalTime, string intervWork, string intervMove,
                string timeStopsAll, double distance, double avrSpeed, double maxSpeed, double normaWay, double normaMotor,
                double summaNorma, string motorIdle)
            {
                this.initTime = initTime;
                this.locatStart = locatStart;
                this.locatEnd = locatEnd;
                this.initalTime = initalTime;
                this.finalTime = finalTime;
                this.intervWork = intervWork;
                this.intervMove = intervMove;
                this.timeStopsAll = timeStopsAll;
                this.distance = distance;
                this.avrSpeed = avrSpeed;
                this.maxSpeed = maxSpeed;
                this.normaWay = normaWay;
                this.normaMotor = normaMotor;
                this.summaNorma = summaNorma;
                this.motorIdle = motorIdle;
            } // reportKlmtDay

            [Developer( 0 )]
            public string InitTime
            {
                get { return initTime; }
            }

            [Developer( 1 )]
            public string LocationStart
            {
                get { return locatStart; }
            }

            [Developer( 2 )]
            public string LocationEnd
            {
                get { return locatEnd; }
            }

            [Developer( 3 )]
            public string BeginTimeMove
            {
                get { return initalTime; }
            }

            [Developer( 4 )]
            public string EndTimeMove
            {
                get { return finalTime; }
            }

            [Developer( 5 )]
            public string IntervalWork
            {
                get { return intervWork; }
            }

            [Developer( 6 )]
            public string IntervalMove
            {
                get { return intervMove; }
            }

            [Developer( 9 )]
            public string TimeStopsTotal
            {
                get { return timeStopsAll; }
            }

            [Developer( 14 )]
            public string MotorHrIdle
            {
                get { return this.motorIdle; }
            }

            [Developer( 7 )]
            public double Distance
            {
                get { return distance; }
            }

            [Developer( 8 )]
            public double AverageSpeed
            {
                get { return avrSpeed; }
            }

            [Developer(15)]
            public double MaxSpeed
            {
                get { return maxSpeed; }
            }

            [Developer( 10 )]
            public double NormalWay
            {
                get { return normaWay; }
            }

            [Developer( 11 )]
            public double NormalMotor
            {
                get { return normaMotor; }
            }

            [Developer( 12 )]
            public double SummaNorma
            {
                get { return this.summaNorma; }
            }
        } // reportKlmtDay

        KilometrageDays algorithm;
        Rotation rotate_algoritm;
        atlantaDataSet.mobitelsRow mobitel = null;
        Dictionary<int, KilometrageDays.SummaryDay> summariesDay;
        ReportBase<reportKlmtDay, TInfo> ReportingKlmtDay;

        /* Конструктор */
        public KilometrageDayControl()
        {
            InitializeComponent();

            algorithm = new KilometrageDays();
            rotate_algoritm = new Rotation();

            AddAlgorithm( rotate_algoritm );
            AddAlgorithm(algorithm);
            
            _dataset = ReportTabControl.Dataset;
            summariesDay = new Dictionary<int, KilometrageDays.SummaryDay>();
            //kilometrageReportDayBindingSource.DataSource = _dataset;
            //kilometrageReportDayBindingSource.DataMember = "KilometrageReportDay";
            
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gvKlmtDay, gcKlmtDay, bar3);

            gvKlmtDay.CustomDrawFooterCell += GvKlmtDayOnCustomDrawFooterCell;
            gvKlmtDay.CustomDrawCell += gvKlmtDay_CustomDrawCell;


            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingKlmtDay =
                new ReportBase<reportKlmtDay, TInfo>(Controls, compositeReportLink, gvKlmtDay,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        void gvKlmtDay_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;

            if (e.RowHandle >= 0)
            {
                if (e.Column.FieldName == "MaxSpeed")
                {
                    string compareCol = view.GetRowCellDisplayText(e.RowHandle, view.Columns["Hunter"]);

                    if (compareCol != "0")
                    {
                        e.Appearance.BackColor = Color.LightPink;
                    }
                }
            };
        }

        private void GvKlmtDayOnCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
        }

// KilometrageDay

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is KilometrageDays)
            {
                if (e is KilometrageDaysEventArgs)
                {
                    KilometrageDaysEventArgs args = (KilometrageDaysEventArgs)e;
                    if (summariesDay.ContainsKey(args.Id))
                    {
                        summariesDay.Remove(args.Id);
                    }
                    summariesDay.Add(args.Id, args.SummaryDay);
                    Select(curMrow);
                } // if
            } // if
        } // Algorithm_Action

        public override string Caption
        {
            get { return Resources.KilometrageDay; }
        }

        public override void ClearReport()
        {
            gcKlmtDay.DataSource = null;
            _dataset.KilometrageReport.Clear();
            _dataset.KilometrageReportDay.Clear();
            summariesDay.Clear();
            ClearStatusLine();
        }

        private void ClearStatusLine()
        {
            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ---");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:--");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:--");
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel == null)
                return;

            curMrow = mobitel;

            if (summariesDay.ContainsKey(curMrow.Mobitel_ID))
            {
                barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalWay.ToString("F"));

                barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalTimeTour.ToString());

                barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalTimeWay.ToString());
            } // if
           
            kilometrageReportDayBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
            gcKlmtDay.DataSource = kilometrageReportDayBindingSource;

            //_dataset.KilometrageReport.Clear();
            //if (_dataset.KilometrageReport.Count > 0) // aketner - 18.09.2012
            //{
            //    string s = "Kilometrage Report is Initialized";
            //}

            if (kilometrageReportDayBindingSource.Count > 0)
            {
                EnableButton();
            }
            else
            {
                DisableButton();
                ClearStatusLine();
            }
        } // Select

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            foreach( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();
                if( _stopRun ) return;
            }
        }

        // --   Обработчики GUI   --
        // callback show data in report's table
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true;
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                // Установим связь с таблицами
                kilometrageReportDayBindingSource.DataSource = _dataset;
                kilometrageReportDayBindingSource.DataMember = "KilometrageReportDay";
                
                foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        Application.DoEvents();
                        SelectItem(m_row); // обслуживаем алгоритм обороты (Rotation)
                        noData = false;
                        if (_stopRun) 
                            break;
                    } // if
                } // foreach

                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                Select(mobitel);

                ReportsControl.OnClearMapObjectsNeeded();
                //ReportsControl.ShowGraph(curMrow);
                ReportsControl.ShowGraphFromMobi(curMrow);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnGraph();
        } // bbiShowOnMap_ItemClick

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportDayRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId ,  data);
            if( data.Count > 0 )
            {
                return new Track( row.MobitelId, Color.Red, 2f, data );
            }

            return null;
        } // GetTrackSegment

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowSpeedOnGraph();
        } // bbiShowOnGraf_ItemClick

        private void gvKlmtDay_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                ShowSpeedOnGraph();
                ShowTrackOnGraph();
            } // if
        } // gvDriver_FocusedRowChanged

        private void ShowSpeedOnGraph()
        {
            Graph.ClearRegion();
            int rowIndex = gvKlmtDay.GetFocusedDataSourceRowIndex();
            if (rowIndex >= 0)
            {
                atlantaDataSet.KilometrageReportDayRow row = (atlantaDataSet.KilometrageReportDayRow)
                    ((DataRowView)kilometrageReportDayBindingSource.List[rowIndex]).Row;

                Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
            }
        }

        private void ShowTrackOnGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();

            List<Track> segments = new List<Track>();
            Int32[] selected_row = gvKlmtDay.GetSelectedRows();

            for (int i = 0; i < selected_row.Length; i++)
            {
                atlantaDataSet.KilometrageReportDayRow k_row = (atlantaDataSet.KilometrageReportDayRow)
                    ((DataRowView)kilometrageReportDayBindingSource.List[selected_row[i]]).Row;

                Track trck = GetTrackSegment( k_row );
                if( trck != null)
                    segments.Add(trck);
            } // for

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // ShowTrackOnGraph

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (int mobitelId in summariesDay.Keys) // обходим выбранные машины
            {
                TInfo t_info = new TInfo();
                atlantaDataSet.mobitelsRow mobi = _dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitelId, mobi);

                // заголовок для таблиц отчета, для каждого отчета разный
                t_info.infoVehicle = info.Info; // Транспорт
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                t_info.totalWay = summariesDay[mobitelId].TotalWay; // Общий пройденный путь
                t_info.totalTimerTour = summariesDay[mobitelId].TotalTimeTour; // Общая продолжительность смены
                t_info.totalTimeWay = summariesDay[mobitelId].TotalTimeWay; // Общее время в пути
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmtDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmtDay.CreateBindDataList(); // создать новый список данных таблицы отчета

                // получить данные таблицы отчета по машине
                foreach (atlantaDataSet.KilometrageReportDayRow row in
                    _dataset.KilometrageReportDay.Select("MobitelId=" + mobitelId, "Id ASC"))
                {
                    string initTime = row.InitialTime.ToString("D");

                    string locatStart = "";

                    try
                    {
                        locatStart = (row["LocationStart"] != DBNull.Value) ? row.LocationStart : " - ";
                    }
                    catch (Exception ex)
                    {
                        locatStart = " - ";
                    }

                    string locatEnd = "";

                    try
                    {
                        locatEnd = (row["LocationEnd"] != DBNull.Value) ? row.LocationEnd : " - ";
                    }
                    catch (Exception ex)
                    {
                        locatEnd = " - ";
                    }

                    string initalTime = row.InitialTime.ToString("t");
                    string finalTime = row.FinalTime.ToString("t");
                    string intervWork = row.IntervalWork;
                    string intervMove = row.IntervalMove;
                    string timeStops = row.TimeStopsTotal.ToString();

                    double distance = 0.0;
                    double avrSpeed = 0.0;
                    double maxSpeed = 0.0;
                    double normaWay = 0.0;
                    double normaMotor = 0.0;
                    double summaNorma = 0.0;
                    TimeSpan spn = new TimeSpan();
                    string motorHours = "" + spn.Hours + ":" + spn.Minutes;

                    try
                    {
                        if (row["Distance"] != DBNull.Value)
                            distance = Math.Round(row.Distance, 2);
                    }
                    catch (Exception ex)
                    {
                        distance = 0.0;
                    }

                    try
                    {
                        if (row["AverageSpeed"] != DBNull.Value)
                            avrSpeed = Math.Round(row.AverageSpeed, 2);
                    }
                    catch (Exception ex)
                    {
                        avrSpeed = 0.0;
                    }

                    try
                    {
                        if (row["MaxSpeed"] != DBNull.Value)
                            maxSpeed = Math.Round(row.MaxSpeed, 2);
                    }
                    catch (Exception ex)
                    {
                        maxSpeed = 0.0;
                    }

                    try
                    {
                        if( row["NormaWay"] != DBNull.Value )
                            normaWay = Math.Round( row.NormaWay, 1 );
                    }
                    catch( Exception ex )
                    {
                        normaWay = 0.0;
                    }

                    try
                    {
                        if( row["NormaMotor"] != DBNull.Value )
                            normaMotor = Math.Round( row.NormaMotor, 1 );
                    }
                    catch( Exception ex )
                    {
                        normaMotor = 0.0;
                    }

                    try
                    {
                        if( row["SummaNormaFuel"] != DBNull.Value )
                            summaNorma = Math.Round( row.SummaNormaFuel, 1 );
                    }
                    catch( Exception ex )
                    {
                        summaNorma = 0.0;
                    }

                    try
                    {
                        if( row["MotorHrIdle"] != DBNull.Value )
                            motorHours =  row.MotorHrIdle;
                    }
                    catch( Exception ex )
                    {
                        motorHours = "00:00";
                    }

                    // сохранить данные таблицы отчета в списке таблиц
                    ReportingKlmtDay.AddDataToBindList(new reportKlmtDay(initTime, locatStart,
                        locatEnd, initalTime, finalTime, intervWork, intervMove, timeStops,
                                                 distance, avrSpeed, maxSpeed, normaWay, normaMotor, summaNorma, motorHours));
                } // foreach 1

                ReportingKlmtDay.CreateElementReport();
            } // foreach 2

            ReportingKlmtDay.CreateAndShowReport();
            ReportingKlmtDay.DeleteData();
        } // ExportAllDevToReport

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportKilometrageDay, e);
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            return (Resources.TotalDistTravel + ": " + String.Format("{0:f2}", info.totalWay) +
                Resources.KM + "\n" +
                Resources.TotalDurShift + ": " + info.totalTimerTour + "\n" +
                Resources.TotalTravelTime + ": " + info.totalTimeWay);
        }
      
        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvKlmtDay, true, true);

            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);

            // заголовок для таблиц отчета, для каждого отчета разный
            t_info.infoVehicle = info.Info; // Транспорт
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
            t_info.totalWay = summariesDay[curMrow.Mobitel_ID].TotalWay; // Общий пройденный путь
            t_info.totalTimerTour = summariesDay[curMrow.Mobitel_ID].TotalTimeTour; // Общая продолжительность смены
            t_info.totalTimeWay = summariesDay[curMrow.Mobitel_ID].TotalTimeWay; // Общее время в пути
            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            ReportingKlmtDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingKlmtDay.CreateAndShowReport(gcKlmtDay);          
        } // ExportToExcelDevExpress
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvKlmtDay);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvKlmtDay);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcKlmtDay);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        public void Localization()
        {
            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": --- ");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:-- ");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:-- ");

            colData.Caption = Resources.Date;
            colPlaceStartMove.Caption = Resources.StartLocation;
            colPlaceEndMove.Caption = Resources.MovementEndPlace;
            colTimeStartMove.Caption = Resources.InitialMovementTime;
            colTimeEndMove.Caption = Resources.EndMovementTime;
            colDurationPeriod.Caption = Resources.ShiftDurationHint;
            colTotalDurationPeriod.Caption = Resources.MovementTotalTime;
            colTotalDistance.Caption = Resources.DistanceText;
            colSpeedAverage.Caption = Resources.SpeedAverage;
            colMaxSpeed.Caption = Resources.MaxSpeed;
            colStopTime.Caption = Resources.StopTime;

            colData.ToolTip = Resources.Date;
            colPlaceStartMove.ToolTip = Resources.StartLocation;
            colPlaceEndMove.ToolTip = Resources.MovementEndPlace;
            colTimeStartMove.ToolTip = Resources.InitialMovementTime;
            colTimeEndMove.ToolTip = Resources.EndMovementTime;
            colDurationPeriod.ToolTip = Resources.ShiftDurationHint;
            colTotalDurationPeriod.ToolTip = Resources.MovementTotalTime;
            colTotalDistance.ToolTip = Resources.DistanceText;
            colSpeedAverage.ToolTip = Resources.SpeedAverage;
            colMaxSpeed.ToolTip = Resources.MaxSpeed;
            colStopTime.ToolTip = Resources.StopTimeHint;
            
            colNormalWay.ToolTip = Resources.NormalWayTip;
            colNormaMotor.ToolTip = Resources.NormalMotorTip;
            colSummaNormaFuel.ToolTip = Resources.SummaNormalTip;
            colMotorHrIdle.ToolTip = Resources.MotorHrStopTip;

            colNormalWay.Caption = Resources.NormalWay;
            colNormaMotor.Caption = Resources.NormalMotor;
            colSummaNormaFuel.Caption = Resources.SummaNormal;
            colMotorHrIdle.Caption = Resources.MotorHrStop;

            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ---");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:--");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:--");

            //colTotalDistance.SummaryItem.DisplayFormat = String.Concat(Resources.Total, ": {0:f2}");
        } // Localization

        // 03.06.2013 - Класс разработан для исправления вывода названий в заголовки колонок таблицы
        // во время генерации отчета
        [AttributeUsage( AttributeTargets.All )]
        public class DeveloperAttribute : DisplayNameAttribute
        {
            //Private fields.
           private string Data;
           private string PlaceStartMove;
           private string PlaceEndMove;
           private string TimeStartMove;
           private string TimeEndMove;
           private string DurationPeriod;
           private string TotalDurationPeriod;
           private string TotalDistance;
           private string SpeedAverage;
           private string MaxSpeed;
           private string StopTime;
           private string NormalWay;
           private string NormaMotor;
           private string SummaNormaFuel;
           private string MotorHrIdle;    
           private int val;

            //This constructor defines two required parameters: name and level.
            public DeveloperAttribute( int val )
            {
                this.val = val;

                Data = Resources.Date;
                PlaceStartMove = Resources.StartLocation;
                PlaceEndMove = Resources.MovementEndPlace;
                TimeStartMove = Resources.InitialMovementTime;
                TimeEndMove = Resources.EndMovementTime;
                DurationPeriod = Resources.ShiftDurationHint;
                TotalDurationPeriod = Resources.MovementTotalTime;
                TotalDistance = Resources.DistanceText;
                SpeedAverage = Resources.SpeedAverage;
                MaxSpeed = Resources.MaxSpeed;
                StopTime = Resources.StopTime;
                NormalWay = Resources.NormalWay;
                NormaMotor = Resources.NormalMotor;
                SummaNormaFuel = Resources.SummaNormal;
                MotorHrIdle = Resources.MotorHrStop;
            }

            //Define DisplayName property
            public override string DisplayName
            {
                get
                {
                    switch( val )
                    {
                        case 0:
                            return Data;
                        case 1:
                            return PlaceStartMove;
                        case 2:
                            return PlaceEndMove;
                        case 3:
                            return TimeStartMove;
                        case 4:
                            return TimeEndMove;
                        case 5:
                            return DurationPeriod;
                        case 6:
                            return TotalDurationPeriod;
                        case 7:
                            return TotalDistance;
                        case 8:
                            return SpeedAverage;
                        case 9:
                            return StopTime;
                        case 10:
                            return NormalWay;
                        case 11:
                            return NormaMotor;
                        case 12:
                            return SummaNormaFuel;
                        case 13:
                            return MotorHrIdle;
                        case 15:
                            return MaxSpeed;  
                        default:
                            throw new Exception( "This unknown property!" );
                    } // switch
                } // get
            } // DisplayName
        } // DeveloperAttribute
  } // KilometrageDay
} // BaseReports.ReportsDE
