﻿using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;

namespace Report
{
    /* тип Т1 - структура данных заголовка таблицы отчета */
    public class ReportClass<T1>
    {
        struct PlaceAndSize
        {
            public float X;
            public float Y;
            public float Width;
            public float Height;
        } // PlaceAndSize

        Link linkReport;
        Link linkg1Report;
        PrintableComponentLink printCompLink;
        PrintableComponentLink printLink;
        CompositeLink compositeReportLink;
        private CompositeLink compositeBreakLink;
        List<T1> infoList;
        int iCount;
        PlaceAndSize BrckUP; // верхний/средний блок
        PlaceAndSize BrckLeft; // левый блок
        PlaceAndSize BrckRight; // правый блок

        public ReportClass(CompositeLink compositeRepLink)
        {
            iCount = 0;
            infoList = new List<T1>();
            compositeReportLink = compositeRepLink;
            printLink = null;
            
            BrckUP.X = 200;
            BrckUP.Y = 0;
            BrckUP.Width = 515;
            BrckUP.Height = 22;

            BrckLeft.X = 0;
            BrckLeft.Y = 0;
            BrckLeft.Width = 400;
            BrckLeft.Height = 60;

            BrckRight.X = 770;
            BrckRight.Y = 0;
            BrckRight.Width = 300;
            BrckRight.Height = 60;
            compositeBreakLink = new CompositeLink();
        }

        // создает отчет по одной заранее подготовленной таблице
        public void CreateAndShowReport(GridControl gCtrl)
        {
            CreateNewLinks(gCtrl);
            SetComposeLink();
            CreateAndShowReport();
            ClearInfoList();
            ClearComposeLink();
        }

        // для формирования верхней/средней части заголовка таблицы отчета
        public delegate string GetStrBreackUp();
        public GetStrBreackUp getStrBrckUp;

        // для формирования левой части заголовка таблицы отчета
        public delegate string GetStrBreackLeft();
        public GetStrBreackLeft getStrBrckLeft;

        // для формирования правой части заголовка таблицы отчета
        public delegate string GetStrBreackRight();
        public GetStrBreackRight getStrBrckRight;

        // создать новые части страниц отчета
        public void CreateNewLinks(GridControl gCtrl)
        {
            PrintingSystem print = new PrintingSystem();
           
            linkReport = new Link();
            linkReport.CreateDetailArea +=
                new CreateAreaEventHandler(linkReport_CreateDetailArea);

            linkg1Report = new Link();
            linkg1Report.CreateDetailArea +=
                new CreateAreaEventHandler(linkg1Report_CreateDetailArea);

            printCompLink = new PrintableComponentLink(print);
            printCompLink.Component = gCtrl;
        } // CreateNewLinks

        // создать новые части страниц отчета
        public void CreateNewOtherLinks( GridControl gCtrl )
        {
            printCompLink = new PrintableComponentLink( new PrintingSystem() );
            printCompLink.Component = gCtrl;
        } // CreateNewLinks

        public void AddLink(PrintableComponentLink link)
        {
            if(link != null)
                printLink = link;
        }

        // Creates an interval between the grids and fills it with color
        protected void linkReport_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            TextBrick tb = new TextBrick();
            tb.Rect = new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 6);
            tb.BackColor = Color.Gray;
            e.Graph.DrawBrick(tb);
        }

        // Создать текстовый заголовок для каждой таблицы отчета
        protected void linkg1Report_CreateDetailArea(object sender, CreateAreaEventArgs e)
        {
            if (infoList == null)
                return;

            if (infoList.Count == 0)
                return;

            if (iCount >= infoList.Count)
                iCount = 0;

            TextBrick textBrck0 = new TextBrick();
            textBrck0.Text = getStrBrckUp();
            textBrck0.Font = new Font("Times New Roman", 9, FontStyle.Bold);
            textBrck0.Rect = new RectangleF(BrckUP.X, BrckUP.Y, BrckUP.Width, BrckUP.Height);
            textBrck0.BorderWidth = 0;
            textBrck0.BackColor = Color.Transparent;
            textBrck0.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textBrck0.VertAlignment = DevExpress.Utils.VertAlignment.Bottom;
            e.Graph.DrawBrick(textBrck0);

            string text = getStrBrckLeft();
            string[] textSplt = text.Split('\n');
            List<string> textBlock = new List<string>();

            for(int i = 0; i < textSplt.Length; i++)
            {
                textBlock.Add(textSplt[i]);
            }

            List<TextBrick> txtBrick = new List<TextBrick>();
            int sizeFont = 9;
            float iX = BrckLeft.X;
            float iY = BrckLeft.Y;
            List<float> blockWidth = new List<float>();
            float maxHeight = sizeFont * (float)1.65;
            float pixlength = 0;
            TextBrick textBrck;
            
            for (int i = 0; i < textBlock.Count; i++)
            {
                blockWidth.Add(textBlock[i].Length * sizeFont * (float)0.87);
            }

            for(int i = 0, k = 0; i < textBlock.Count; i++, k++)
            {
                textBrck = new TextBrick();
                textBrck.Text = textBlock[i];
                textBrck.Font = new Font("Times New Roman", sizeFont, FontStyle.Bold);
                textBrck.Rect = new RectangleF(iX, iY, blockWidth[k], maxHeight);
                textBrck.BorderWidth = 0;
                textBrck.BackColor = Color.Transparent;
                textBrck.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
                textBrck.VertAlignment = DevExpress.Utils.VertAlignment.Bottom;
                txtBrick.Add(textBrck);
                iY += maxHeight;
                iX = BrckLeft.X;
            } // for

            for (int i = 0; i < txtBrick.Count; i++)
            {
                e.Graph.DrawBrick(txtBrick[i]);
            }

            text = getStrBrckRight(); 
            textSplt = text.Split('\n');
            textBlock = new List<string>();

            for (int i = 0; i < textSplt.Length; i++)
            {
                textBlock.Add(textSplt[i]);
            }

            txtBrick = new List<TextBrick>();
            iX = BrckRight.X;
            iY = BrckRight.Y;
            blockWidth = new List<float>();
            maxHeight = sizeFont * (float)1.66;
            
            for (int i = 0; i < textBlock.Count; i++)
            {
                blockWidth.Add(textBlock[i].Length * sizeFont * (float)0.8);
            }

            for (int i = 0, k = 0; i < textBlock.Count; i++, k++)
            {
                textBrck = new TextBrick();
                textBrck.Text = textBlock[i];
                textBrck.Font = new Font("Times New Roman", sizeFont, FontStyle.Bold);
                textBrck.Rect = new RectangleF(iX, iY, blockWidth[k], maxHeight);
                textBrck.BorderWidth = 0;
                textBrck.BackColor = Color.Transparent;
                textBrck.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
                textBrck.VertAlignment = DevExpress.Utils.VertAlignment.Bottom;
                txtBrick.Add(textBrck);
                iY += maxHeight;
                iX = BrckRight.X;
            } // for

            for (int i = 0; i < txtBrick.Count; i++)
            {
                e.Graph.DrawBrick(txtBrick[i]);
            }

            iCount++;
        } // linkg1Report_CreateDetailArea

        // объединить элементы страницы в общий отчет
        public void SetComposeLink()
        {
            compositeReportLink.Links.Add(linkg1Report);
            compositeReportLink.Links.Add(printCompLink);
            compositeReportLink.Links.Add(linkReport);

            Link lnkEmpty = new Link();
            compositeBreakLink.Links.Add(lnkEmpty);
            compositeBreakLink.BreakSpace = 800;
            compositeReportLink.Links.Add(compositeBreakLink);
            
            if ( printLink != null )
                compositeReportLink.Links.Add( printLink );
        } // SetComposeLink

        // объединить элементы страницы в общий отчет
        public void SetComposeOtherLink()
        {
            compositeReportLink.Links.Add( printCompLink );
        } // SetComposeLink

        // создать и показать отчет
        public void CreateAndShowReport()
        {
            compositeReportLink.CreateDocument();
            compositeReportLink.ShowPreviewDialog();
        }

        public void ExportReportToExcel(string fileName, PrintingSystem printSys)
        {
            XlsExportOptions xlsOptions = printSys.ExportOptions.Xls;
            xlsOptions.SheetName = "Calibration";
            xlsOptions.ShowGridLines = true;
            xlsOptions.TextExportMode = TextExportMode.Value;
            printCompLink.PrintingSystem = printSys;
            compositeReportLink.PrintingSystem = printSys;
            compositeReportLink.CreateDocument();
            printSys.ExportToXls( fileName );
            StartProcess( fileName );
        }

        public void StartProcess( string path )
        {
            Process process = new Process();
            try
            {
                process.StartInfo.FileName = path;
                process.Start();
                process.WaitForInputIdle();
            }
            catch { }
        }

        // прибираем за собой, остальное сделает сборщик мусора
        public void ClearInfoList()
        {
            if (infoList != null)
                infoList.Clear();

            iCount = 0;
        }

        // удалить элементы страницы из общего отчета
        public void ClearComposeLink()
        {
            if (compositeReportLink != null)
                compositeReportLink.Links.Clear();  
        }

        /* добавить заголовок таблицы отчета в список */
        public void AddInfoStructToList(T1 inflist)
        {
            infoList.Add(inflist);
        }

        public void SetRectangleBrckUP(float x, float y, float w, float h)
        {
            BrckUP.X = x;
            BrckUP.Y = y;
            BrckUP.Width = w;
            BrckUP.Height = h;
        }

        public void SetRectangleBrckLetf(float x, float y, float w, float h)
        {
            BrckLeft.X = x;
            BrckLeft.Y = y;
            BrckLeft.Width = w;
            BrckLeft.Height = h;
        }

        public void SetRectangleBrckRight(float x, float y, float w, float h)
        {
            BrckRight.X = x;
            BrckRight.Y = y;
            BrckRight.Width = w;
            BrckRight.Height = h;
        }

        /* вернуть данные заголовка таблицы отчета */
        public T1 GetInfoStructure
        {
            get
            {
                T1 nullable = default(T1);
                if (infoList.Count > 0)
                    return infoList[iCount];
                else
                    return nullable;
            }
        }

        public void CreatePageBreak()
        {
            compositeReportLink.BreakSpace = 100;
        }
    } // ReportClass
} // Report
