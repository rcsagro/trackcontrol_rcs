﻿namespace BaseReports.ReportsDE
{
    partial class KilometrageDay
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( KilometrageDay ) );
        this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
        this.bar3 = new DevExpress.XtraBars.Bar();
        this.barDistanceStatus = new DevExpress.XtraBars.BarStaticItem();
        this.barDurationStatus = new DevExpress.XtraBars.BarStaticItem();
        this.barTimeTravel = new DevExpress.XtraBars.BarStaticItem();
        this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
        this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
        this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
        this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
        this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.gcKlmtDay = new DevExpress.XtraGrid.GridControl();
        this.kilometrageReportDayBindingSource = new System.Windows.Forms.BindingSource( this.components );
        this.gvKlmtDay = new DevExpress.XtraGrid.Views.Grid.GridView();
        this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colPlaceStartMove = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colPlaceEndMove = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colTimeStartMove = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colTimeEndMove = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colDurationPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colTotalDurationPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colStopTime = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colTotalDistance = new DevExpress.XtraGrid.Columns.GridColumn();
        this.colSpeedAverage = new DevExpress.XtraGrid.Columns.GridColumn();
        this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
        this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem( this.components );
        this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
        ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gcKlmtDay ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.kilometrageReportDayBindingSource ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gvKlmtDay ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.compositeReportLink.ImageCollection ) ).BeginInit();
        this.SuspendLayout();
        // 
        // barManager1
        // 
        this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar3} );
        this.barManager1.DockControls.Add( this.barDockControl1 );
        this.barManager1.DockControls.Add( this.barDockControl2 );
        this.barManager1.DockControls.Add( this.barDockControl3 );
        this.barManager1.DockControls.Add( this.barDockControl4 );
        this.barManager1.Form = this;
        this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this.barDistanceStatus,
            this.barDurationStatus,
            this.barTimeTravel} );
        this.barManager1.MaxItemId = 3;
        this.barManager1.StatusBar = this.bar3;
        // 
        // bar3
        // 
        this.bar3.BarName = "Status bar";
        this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
        this.bar3.DockCol = 0;
        this.bar3.DockRow = 0;
        this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
        this.bar3.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barDistanceStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.barDurationStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTimeTravel)} );
        this.bar3.OptionsBar.AllowQuickCustomization = false;
        this.bar3.OptionsBar.DrawDragBorder = false;
        this.bar3.OptionsBar.UseWholeRow = true;
        this.bar3.Text = "Status bar";
        // 
        // barDistanceStatus
        // 
        this.barDistanceStatus.Caption = "Пройденный путь, км: --- ";
        this.barDistanceStatus.Id = 0;
        this.barDistanceStatus.Name = "barDistanceStatus";
        this.barDistanceStatus.TextAlignment = System.Drawing.StringAlignment.Near;
        // 
        // barDurationStatus
        // 
        this.barDurationStatus.Caption = "Общая продолжительность смен: --:--:-- ";
        this.barDurationStatus.Id = 1;
        this.barDurationStatus.Name = "barDurationStatus";
        this.barDurationStatus.TextAlignment = System.Drawing.StringAlignment.Near;
        // 
        // barTimeTravel
        // 
        this.barTimeTravel.Caption = "Время в пути (без коротких остановок): --:--:-- ";
        this.barTimeTravel.Id = 2;
        this.barTimeTravel.Name = "barTimeTravel";
        this.barTimeTravel.TextAlignment = System.Drawing.StringAlignment.Near;
        // 
        // gridView2
        // 
        this.gridView2.GridControl = this.gcKlmtDay;
        this.gridView2.Name = "gridView2";
        // 
        // gcKlmtDay
        // 
        this.gcKlmtDay.DataSource = this.kilometrageReportDayBindingSource;
        this.gcKlmtDay.Dock = System.Windows.Forms.DockStyle.Fill;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.Append.Visible = false;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.Edit.Visible = false;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.Remove.Enabled = false;
        this.gcKlmtDay.EmbeddedNavigator.Buttons.Remove.Visible = false;
        this.gcKlmtDay.Location = new System.Drawing.Point( 0, 26 );
        this.gcKlmtDay.MainView = this.gvKlmtDay;
        this.gcKlmtDay.Name = "gcKlmtDay";
        this.gcKlmtDay.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1} );
        this.gcKlmtDay.Size = new System.Drawing.Size( 810, 361 );
        this.gcKlmtDay.TabIndex = 8;
        this.gcKlmtDay.UseEmbeddedNavigator = true;
        this.gcKlmtDay.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvKlmtDay,
            this.gridView2} );
        // 
        // gvKlmtDay
        // 
        this.gvKlmtDay.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.ColumnFilterButton.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.ColumnFilterButton.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
        this.gvKlmtDay.Appearance.Empty.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
        this.gvKlmtDay.Appearance.EvenRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
        this.gvKlmtDay.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
        this.gvKlmtDay.Appearance.FilterCloseButton.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.FilterCloseButton.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
        this.gvKlmtDay.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
        this.gvKlmtDay.Appearance.FilterPanel.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.FilterPanel.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
        this.gvKlmtDay.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
        this.gvKlmtDay.Appearance.FocusedRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.FocusedRow.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.FooterPanel.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.FooterPanel.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.FooterPanel.Options.UseTextOptions = true;
        this.gvKlmtDay.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gvKlmtDay.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gvKlmtDay.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
        this.gvKlmtDay.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
        this.gvKlmtDay.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
        this.gvKlmtDay.Appearance.GroupButton.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.GroupButton.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
        this.gvKlmtDay.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
        this.gvKlmtDay.Appearance.GroupFooter.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.GroupFooter.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
        this.gvKlmtDay.Appearance.GroupPanel.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.GroupPanel.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
        this.gvKlmtDay.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
        this.gvKlmtDay.Appearance.GroupRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.GroupRow.Options.UseFont = true;
        this.gvKlmtDay.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
        this.gvKlmtDay.Appearance.HeaderPanel.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.HeaderPanel.Options.UseBorderColor = true;
        this.gvKlmtDay.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
        this.gvKlmtDay.Appearance.HideSelectionRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
        this.gvKlmtDay.Appearance.HorzLine.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
        this.gvKlmtDay.Appearance.OddRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
        this.gvKlmtDay.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.Preview.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.Preview.Options.UseForeColor = true;
        this.gvKlmtDay.Appearance.Row.BackColor = System.Drawing.Color.White;
        this.gvKlmtDay.Appearance.Row.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.RowSeparator.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
        this.gvKlmtDay.Appearance.SelectedRow.Options.UseBackColor = true;
        this.gvKlmtDay.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
        this.gvKlmtDay.Appearance.VertLine.Options.UseBackColor = true;
        this.gvKlmtDay.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
        this.gvKlmtDay.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.gvKlmtDay.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.gvKlmtDay.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
        this.gvKlmtDay.ColumnPanelRowHeight = 40;
        this.gvKlmtDay.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colPlaceStartMove,
            this.colPlaceEndMove,
            this.colTimeStartMove,
            this.colTimeEndMove,
            this.colDurationPeriod,
            this.colTotalDurationPeriod,
            this.colStopTime,
            this.colTotalDistance,
            this.colSpeedAverage} );
        this.gvKlmtDay.GridControl = this.gcKlmtDay;
        this.gvKlmtDay.GroupSummary.AddRange( new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colTotalDistance, "")} );
        this.gvKlmtDay.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
        this.gvKlmtDay.Name = "gvKlmtDay";
        this.gvKlmtDay.OptionsDetail.AllowZoomDetail = false;
        this.gvKlmtDay.OptionsDetail.EnableMasterViewMode = false;
        this.gvKlmtDay.OptionsDetail.ShowDetailTabs = false;
        this.gvKlmtDay.OptionsDetail.SmartDetailExpand = false;
        this.gvKlmtDay.OptionsSelection.MultiSelect = true;
        this.gvKlmtDay.OptionsView.EnableAppearanceEvenRow = true;
        this.gvKlmtDay.OptionsView.EnableAppearanceOddRow = true;
        this.gvKlmtDay.OptionsView.ShowFooter = true;
        this.gvKlmtDay.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler( this.gvKlmtDay_ClickRow );
        // 
        // colData
        // 
        this.colData.AppearanceCell.Options.UseTextOptions = true;
        this.colData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colData.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colData.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colData.AppearanceHeader.Options.UseTextOptions = true;
        this.colData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colData.Caption = "Дата";
        this.colData.FieldName = "InitialTime";
        this.colData.ImageAlignment = System.Drawing.StringAlignment.Center;
        this.colData.Name = "colData";
        this.colData.OptionsColumn.AllowEdit = false;
        this.colData.OptionsColumn.AllowFocus = false;
        this.colData.OptionsColumn.ReadOnly = true;
        this.colData.ToolTip = "Дата";
        this.colData.Visible = true;
        this.colData.VisibleIndex = 0;
        this.colData.Width = 91;
        // 
        // colPlaceStartMove
        // 
        this.colPlaceStartMove.AppearanceCell.Options.UseTextOptions = true;
        this.colPlaceStartMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
        this.colPlaceStartMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colPlaceStartMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colPlaceStartMove.AppearanceHeader.Options.UseTextOptions = true;
        this.colPlaceStartMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colPlaceStartMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colPlaceStartMove.Caption = "Место начала движения";
        this.colPlaceStartMove.FieldName = "LocationStart";
        this.colPlaceStartMove.Name = "colPlaceStartMove";
        this.colPlaceStartMove.OptionsColumn.AllowEdit = false;
        this.colPlaceStartMove.OptionsColumn.AllowFocus = false;
        this.colPlaceStartMove.OptionsColumn.ReadOnly = true;
        this.colPlaceStartMove.ToolTip = "Место начала движения";
        this.colPlaceStartMove.Visible = true;
        this.colPlaceStartMove.VisibleIndex = 1;
        this.colPlaceStartMove.Width = 220;
        // 
        // colPlaceEndMove
        // 
        this.colPlaceEndMove.AppearanceCell.Options.UseTextOptions = true;
        this.colPlaceEndMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
        this.colPlaceEndMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colPlaceEndMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colPlaceEndMove.AppearanceHeader.Options.UseTextOptions = true;
        this.colPlaceEndMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colPlaceEndMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colPlaceEndMove.Caption = "Место окончания движения";
        this.colPlaceEndMove.FieldName = "LocationEnd";
        this.colPlaceEndMove.Name = "colPlaceEndMove";
        this.colPlaceEndMove.OptionsColumn.AllowEdit = false;
        this.colPlaceEndMove.OptionsColumn.AllowFocus = false;
        this.colPlaceEndMove.OptionsColumn.ReadOnly = true;
        this.colPlaceEndMove.ToolTip = "Место окончания движения";
        this.colPlaceEndMove.Visible = true;
        this.colPlaceEndMove.VisibleIndex = 2;
        this.colPlaceEndMove.Width = 220;
        // 
        // colTimeStartMove
        // 
        this.colTimeStartMove.AppearanceCell.Options.UseTextOptions = true;
        this.colTimeStartMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTimeStartMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colTimeStartMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTimeStartMove.AppearanceHeader.Options.UseTextOptions = true;
        this.colTimeStartMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTimeStartMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTimeStartMove.Caption = "Время начала движения";
        this.colTimeStartMove.DisplayFormat.FormatString = "HH:mm";
        this.colTimeStartMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
        this.colTimeStartMove.FieldName = "InitialTime";
        this.colTimeStartMove.Name = "colTimeStartMove";
        this.colTimeStartMove.OptionsColumn.AllowEdit = false;
        this.colTimeStartMove.OptionsColumn.AllowFocus = false;
        this.colTimeStartMove.OptionsColumn.ReadOnly = true;
        this.colTimeStartMove.ToolTip = "Время начала движения";
        this.colTimeStartMove.Visible = true;
        this.colTimeStartMove.VisibleIndex = 3;
        this.colTimeStartMove.Width = 100;
        // 
        // colTimeEndMove
        // 
        this.colTimeEndMove.AppearanceCell.Options.UseTextOptions = true;
        this.colTimeEndMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTimeEndMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colTimeEndMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTimeEndMove.AppearanceHeader.Options.UseTextOptions = true;
        this.colTimeEndMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTimeEndMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTimeEndMove.Caption = "Время окончания движения";
        this.colTimeEndMove.DisplayFormat.FormatString = "HH:mm";
        this.colTimeEndMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
        this.colTimeEndMove.FieldName = "FinalTime";
        this.colTimeEndMove.Name = "colTimeEndMove";
        this.colTimeEndMove.OptionsColumn.AllowEdit = false;
        this.colTimeEndMove.OptionsColumn.AllowFocus = false;
        this.colTimeEndMove.OptionsColumn.ReadOnly = true;
        this.colTimeEndMove.ToolTip = "Время окончания движения";
        this.colTimeEndMove.Visible = true;
        this.colTimeEndMove.VisibleIndex = 4;
        this.colTimeEndMove.Width = 100;
        // 
        // colDurationPeriod
        // 
        this.colDurationPeriod.AppearanceCell.Options.UseTextOptions = true;
        this.colDurationPeriod.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colDurationPeriod.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colDurationPeriod.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colDurationPeriod.AppearanceHeader.Options.UseTextOptions = true;
        this.colDurationPeriod.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colDurationPeriod.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colDurationPeriod.Caption = "Продолжительность смены";
        this.colDurationPeriod.FieldName = "IntervalWork";
        this.colDurationPeriod.Name = "colDurationPeriod";
        this.colDurationPeriod.OptionsColumn.AllowEdit = false;
        this.colDurationPeriod.OptionsColumn.AllowFocus = false;
        this.colDurationPeriod.OptionsColumn.ReadOnly = true;
        this.colDurationPeriod.ToolTip = "Продолжительность смены";
        this.colDurationPeriod.Visible = true;
        this.colDurationPeriod.VisibleIndex = 5;
        this.colDurationPeriod.Width = 125;
        // 
        // colTotalDurationPeriod
        // 
        this.colTotalDurationPeriod.AppearanceCell.Options.UseTextOptions = true;
        this.colTotalDurationPeriod.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTotalDurationPeriod.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colTotalDurationPeriod.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTotalDurationPeriod.AppearanceHeader.Options.UseTextOptions = true;
        this.colTotalDurationPeriod.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTotalDurationPeriod.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTotalDurationPeriod.Caption = "Общее время движения";
        this.colTotalDurationPeriod.FieldName = "IntervalMove";
        this.colTotalDurationPeriod.Name = "colTotalDurationPeriod";
        this.colTotalDurationPeriod.OptionsColumn.AllowEdit = false;
        this.colTotalDurationPeriod.OptionsColumn.AllowFocus = false;
        this.colTotalDurationPeriod.OptionsColumn.ReadOnly = true;
        this.colTotalDurationPeriod.ToolTip = "Общее время движения";
        this.colTotalDurationPeriod.Visible = true;
        this.colTotalDurationPeriod.VisibleIndex = 6;
        this.colTotalDurationPeriod.Width = 100;
        // 
        // colStopTime
        // 
        this.colStopTime.AppearanceCell.Options.UseTextOptions = true;
        this.colStopTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colStopTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colStopTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colStopTime.AppearanceHeader.Options.UseTextOptions = true;
        this.colStopTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colStopTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colStopTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colStopTime.Caption = "Общее время остановок";
        this.colStopTime.FieldName = "TimeStopsTotal";
        this.colStopTime.Name = "colStopTime";
        this.colStopTime.OptionsColumn.AllowEdit = false;
        this.colStopTime.OptionsColumn.AllowFocus = false;
        this.colStopTime.OptionsColumn.ReadOnly = true;
        this.colStopTime.ToolTip = "Общее время остановок во время смены";
        this.colStopTime.Visible = true;
        this.colStopTime.VisibleIndex = 7;
        // 
        // colTotalDistance
        // 
        this.colTotalDistance.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
        this.colTotalDistance.AppearanceCell.Options.UseTextOptions = true;
        this.colTotalDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTotalDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colTotalDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTotalDistance.AppearanceHeader.Options.UseTextOptions = true;
        this.colTotalDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colTotalDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colTotalDistance.Caption = "Пройденный путь, км";
        this.colTotalDistance.DisplayFormat.FormatString = "0.00";
        this.colTotalDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colTotalDistance.FieldName = "Distance";
        this.colTotalDistance.Name = "colTotalDistance";
        this.colTotalDistance.OptionsColumn.AllowEdit = false;
        this.colTotalDistance.OptionsColumn.AllowFocus = false;
        this.colTotalDistance.OptionsColumn.ReadOnly = true;
        this.colTotalDistance.SummaryItem.DisplayFormat = "{0:f2}";
        this.colTotalDistance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        this.colTotalDistance.ToolTip = "Пройденный путь, км";
        this.colTotalDistance.Visible = true;
        this.colTotalDistance.VisibleIndex = 8;
        this.colTotalDistance.Width = 100;
        // 
        // colSpeedAverage
        // 
        this.colSpeedAverage.AppearanceCell.Options.UseTextOptions = true;
        this.colSpeedAverage.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSpeedAverage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        this.colSpeedAverage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSpeedAverage.AppearanceHeader.Options.UseTextOptions = true;
        this.colSpeedAverage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.colSpeedAverage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this.colSpeedAverage.Caption = "Средняя скорость, км/ч";
        this.colSpeedAverage.DisplayFormat.FormatString = "0.00";
        this.colSpeedAverage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
        this.colSpeedAverage.FieldName = "AverageSpeed";
        this.colSpeedAverage.Name = "colSpeedAverage";
        this.colSpeedAverage.OptionsColumn.AllowEdit = false;
        this.colSpeedAverage.OptionsColumn.AllowFocus = false;
        this.colSpeedAverage.OptionsColumn.ReadOnly = true;
        this.colSpeedAverage.ToolTip = "Средняя скорость, км/ч";
        this.colSpeedAverage.Visible = true;
        this.colSpeedAverage.VisibleIndex = 9;
        this.colSpeedAverage.Width = 100;
        // 
        // repositoryItemTextEdit1
        // 
        this.repositoryItemTextEdit1.AutoHeight = false;
        this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
        this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
        // 
        // printingSystem
        // 
        this.printingSystem.ExportOptions.Email.Subject = "Report Document";
        this.printingSystem.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
        this.printingSystem.ExportOptions.Html.Title = "Report Document";
        this.printingSystem.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
        this.printingSystem.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
        this.printingSystem.ExportOptions.Mht.Title = "Report Document";
        this.printingSystem.ExportOptions.NativeFormat.ShowOptionsBeforeSave = true;
        this.printingSystem.ExportOptions.Pdf.DocumentOptions.Subject = "Report Document";
        this.printingSystem.ExportOptions.PrintPreview.DefaultFileName = "Report Document";
        this.printingSystem.ExportOptions.Xls.SheetName = "Report Sheet1";
        this.printingSystem.ExportOptions.Xls.ShowGridLines = true;
        this.printingSystem.ExportOptions.Xlsx.SheetName = "Report Sheet1";
        this.printingSystem.ExportOptions.Xlsx.ShowGridLines = true;
        this.printingSystem.Links.AddRange( new object[] {
            this.compositeReportLink} );
        // 
        // compositeReportLink
        // 
        // 
        // 
        // 
        this.compositeReportLink.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "compositeReportLink.ImageCollection.ImageStream" ) ) );
        this.compositeReportLink.Landscape = true;
        this.compositeReportLink.Margins = new System.Drawing.Printing.Margins( 25, 25, 70, 25 );
        this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins( 25, 25, 15, 25 );
        this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.compositeReportLink.PrintingSystem = this.printingSystem;
        this.compositeReportLink.PrintingSystemBase = this.printingSystem;
        // 
        // KilometrageDay
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
        this.Controls.Add( this.gcKlmtDay );
        this.Controls.Add( this.barDockControl3 );
        this.Controls.Add( this.barDockControl4 );
        this.Controls.Add( this.barDockControl2 );
        this.Controls.Add( this.barDockControl1 );
        this.Name = "KilometrageDay";
        this.Size = new System.Drawing.Size( 810, 436 );
        this.Controls.SetChildIndex( this.barDockControl1, 0 );
        this.Controls.SetChildIndex( this.barDockControl2, 0 );
        this.Controls.SetChildIndex( this.barDockControl4, 0 );
        this.Controls.SetChildIndex( this.barDockControl3, 0 );
        this.Controls.SetChildIndex( this.gcKlmtDay, 0 );
        ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gcKlmtDay ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.kilometrageReportDayBindingSource ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.gvKlmtDay ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.compositeReportLink.ImageCollection ) ).EndInit();
        this.ResumeLayout( false );

    }

    #endregion

    private DevExpress.XtraBars.BarManager barManager1;
    private DevExpress.XtraBars.Bar bar3;
    private DevExpress.XtraBars.BarStaticItem barDistanceStatus;
    private DevExpress.XtraBars.BarDockControl barDockControl1;
    private DevExpress.XtraBars.BarDockControl barDockControl2;
    private DevExpress.XtraBars.BarDockControl barDockControl3;
    private DevExpress.XtraBars.BarDockControl barDockControl4;
    private DevExpress.XtraBars.BarStaticItem barDurationStatus;
    private DevExpress.XtraBars.BarStaticItem barTimeTravel;
    private DevExpress.XtraGrid.GridControl gcKlmtDay;
    private DevExpress.XtraGrid.Views.Grid.GridView gvKlmtDay;
    private DevExpress.XtraGrid.Columns.GridColumn colData;
    private DevExpress.XtraGrid.Columns.GridColumn colPlaceStartMove;
    private DevExpress.XtraGrid.Columns.GridColumn colPlaceEndMove;
    private DevExpress.XtraGrid.Columns.GridColumn colTimeStartMove;
    private DevExpress.XtraGrid.Columns.GridColumn colTimeEndMove;
    private DevExpress.XtraGrid.Columns.GridColumn colDurationPeriod;
    private DevExpress.XtraGrid.Columns.GridColumn colTotalDurationPeriod;
    private DevExpress.XtraGrid.Columns.GridColumn colTotalDistance;
    private DevExpress.XtraGrid.Columns.GridColumn colSpeedAverage;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    private System.Windows.Forms.BindingSource kilometrageReportDayBindingSource;
    private DevExpress.XtraPrinting.PrintingSystem printingSystem;
    private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
    private DevExpress.XtraGrid.Columns.GridColumn colStopTime;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
  }
}
