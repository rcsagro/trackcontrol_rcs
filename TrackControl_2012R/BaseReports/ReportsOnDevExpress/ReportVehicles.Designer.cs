﻿namespace BaseReports.ReportsOnDevExpress
{
    partial class ReportVehicles
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportVehicles));
            this.gcVehicle = new DevExpress.XtraGrid.GridControl();
            this.gvVehicle = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameForReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentIDName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStops = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEngineOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEngineOnStops = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelSub = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpense = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseAvgKm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelExpenseAvgHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.psReprts = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReprts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcVehicle
            // 
            this.gcVehicle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcVehicle.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcVehicle.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcVehicle.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcVehicle.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcVehicle.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcVehicle.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcVehicle.Location = new System.Drawing.Point(0, 24);
            this.gcVehicle.MainView = this.gvVehicle;
            this.gcVehicle.Name = "gcVehicle";
            this.gcVehicle.Size = new System.Drawing.Size(798, 422);
            this.gcVehicle.TabIndex = 5;
            this.gcVehicle.UseEmbeddedNavigator = true;
            this.gcVehicle.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicle,
            this.gridView2});
            // 
            // gvVehicle
            // 
            this.gvVehicle.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvVehicle.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvVehicle.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvVehicle.Appearance.Empty.Options.UseBackColor = true;
            this.gvVehicle.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvVehicle.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvVehicle.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvVehicle.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvVehicle.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvVehicle.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvVehicle.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvVehicle.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvVehicle.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvVehicle.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvVehicle.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvVehicle.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvVehicle.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvVehicle.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvVehicle.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvVehicle.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvVehicle.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvVehicle.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvVehicle.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvVehicle.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvVehicle.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvVehicle.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvVehicle.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvVehicle.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvVehicle.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvVehicle.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvVehicle.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvVehicle.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.GroupRow.Options.UseFont = true;
            this.gvVehicle.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvVehicle.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvVehicle.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvVehicle.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvVehicle.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvVehicle.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvVehicle.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvVehicle.Appearance.OddRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvVehicle.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.Preview.Options.UseBackColor = true;
            this.gvVehicle.Appearance.Preview.Options.UseForeColor = true;
            this.gvVehicle.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvVehicle.Appearance.Row.Options.UseBackColor = true;
            this.gvVehicle.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvVehicle.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvVehicle.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvVehicle.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvVehicle.Appearance.VertLine.Options.UseBackColor = true;
            this.gvVehicle.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvVehicle.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvVehicle.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvVehicle.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvVehicle.ColumnPanelRowHeight = 40;
            this.gvVehicle.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameForReport,
            this.colIdentID,
            this.colIdentIDName,
            this.colLocationStart,
            this.colTimeStart,
            this.colLocationEnd,
            this.colTimeEnd,
            this.colTimeDuration,
            this.colDistance,
            this.colTimeStops,
            this.colTimeEngineOn,
            this.colTimeEngineOnStops,
            this.colFuelStart,
            this.colFuelEnd,
            this.colFuelAdd,
            this.colFuelSub,
            this.colFuelExpense,
            this.colFuelExpenseAvgKm,
            this.colFuelExpenseAvgHour});
            this.gvVehicle.GridControl = this.gcVehicle;
            this.gvVehicle.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colDistance, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeDuration", this.colTimeDuration, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeEngineOn", this.colTimeEngineOn, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeEngineOnStops", this.colTimeEngineOnStops, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelExpense", this.colFuelExpense, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelAdd", this.colFuelAdd, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelSub", this.colFuelSub, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeStops", this.colTimeStops, "")});
            this.gvVehicle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvVehicle.Name = "gvVehicle";
            this.gvVehicle.OptionsDetail.AllowZoomDetail = false;
            this.gvVehicle.OptionsDetail.EnableMasterViewMode = false;
            this.gvVehicle.OptionsDetail.ShowDetailTabs = false;
            this.gvVehicle.OptionsDetail.SmartDetailExpand = false;
            this.gvVehicle.OptionsView.ColumnAutoWidth = false;
            this.gvVehicle.OptionsView.EnableAppearanceEvenRow = true;
            this.gvVehicle.OptionsView.EnableAppearanceOddRow = true;
            this.gvVehicle.OptionsView.ShowFooter = true;
            // 
            // colNameForReport
            // 
            this.colNameForReport.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameForReport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameForReport.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameForReport.Caption = "Транспортное средство";
            this.colNameForReport.FieldName = "NameForReport";
            this.colNameForReport.Name = "colNameForReport";
            this.colNameForReport.OptionsColumn.AllowEdit = false;
            this.colNameForReport.OptionsColumn.AllowFocus = false;
            this.colNameForReport.OptionsColumn.ReadOnly = true;
            this.colNameForReport.ToolTip = "Анализируемое транспортное средство";
            this.colNameForReport.Visible = true;
            this.colNameForReport.VisibleIndex = 0;
            this.colNameForReport.Width = 150;
            // 
            // colIdentID
            // 
            this.colIdentID.AppearanceCell.Options.UseTextOptions = true;
            this.colIdentID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentID.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdentID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentID.Caption = "Идентификатор";
            this.colIdentID.FieldName = "IdentID";
            this.colIdentID.Name = "colIdentID";
            this.colIdentID.OptionsColumn.AllowEdit = false;
            this.colIdentID.OptionsColumn.AllowFocus = false;
            this.colIdentID.OptionsColumn.ReadOnly = true;
            this.colIdentID.ToolTip = "Идентификатор транспортного средства";
            this.colIdentID.Width = 57;
            // 
            // colIdentIDName
            // 
            this.colIdentIDName.AppearanceCell.Options.UseTextOptions = true;
            this.colIdentIDName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentIDName.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdentIDName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentIDName.Caption = "Идентификатор ТС";
            this.colIdentIDName.FieldName = "IdentIDName";
            this.colIdentIDName.Name = "colIdentIDName";
            this.colIdentIDName.OptionsColumn.AllowEdit = false;
            this.colIdentIDName.OptionsColumn.AllowFocus = false;
            this.colIdentIDName.OptionsColumn.ReadOnly = true;
            this.colIdentIDName.ToolTip = "Идентификатор ТС";
            this.colIdentIDName.Visible = true;
            this.colIdentIDName.VisibleIndex = 1;
            // 
            // colLocationStart
            // 
            this.colLocationStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocationStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocationStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocationStart.Caption = "Начало движения";
            this.colLocationStart.FieldName = "LocationStart";
            this.colLocationStart.Name = "colLocationStart";
            this.colLocationStart.OptionsColumn.AllowEdit = false;
            this.colLocationStart.OptionsColumn.AllowFocus = false;
            this.colLocationStart.OptionsColumn.ReadOnly = true;
            this.colLocationStart.Visible = true;
            this.colLocationStart.VisibleIndex = 2;
            this.colLocationStart.Width = 150;
            // 
            // colTimeStart
            // 
            this.colTimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStart.Caption = "Начало смены";
            this.colTimeStart.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeStart.FieldName = "TimeStart";
            this.colTimeStart.Name = "colTimeStart";
            this.colTimeStart.OptionsColumn.AllowEdit = false;
            this.colTimeStart.OptionsColumn.AllowFocus = false;
            this.colTimeStart.OptionsColumn.ReadOnly = true;
            this.colTimeStart.Visible = true;
            this.colTimeStart.VisibleIndex = 3;
            this.colTimeStart.Width = 137;
            // 
            // colLocationEnd
            // 
            this.colLocationEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocationEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocationEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocationEnd.Caption = "Окончание движения";
            this.colLocationEnd.FieldName = "LocationEnd";
            this.colLocationEnd.Name = "colLocationEnd";
            this.colLocationEnd.OptionsColumn.AllowEdit = false;
            this.colLocationEnd.OptionsColumn.AllowFocus = false;
            this.colLocationEnd.OptionsColumn.ReadOnly = true;
            this.colLocationEnd.Visible = true;
            this.colLocationEnd.VisibleIndex = 4;
            this.colLocationEnd.Width = 150;
            // 
            // colTimeEnd
            // 
            this.colTimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEnd.Caption = "Окончание смены";
            this.colTimeEnd.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEnd.FieldName = "TimeEnd";
            this.colTimeEnd.Name = "colTimeEnd";
            this.colTimeEnd.OptionsColumn.AllowEdit = false;
            this.colTimeEnd.OptionsColumn.AllowFocus = false;
            this.colTimeEnd.OptionsColumn.ReadOnly = true;
            this.colTimeEnd.Visible = true;
            this.colTimeEnd.VisibleIndex = 5;
            this.colTimeEnd.Width = 125;
            // 
            // colTimeDuration
            // 
            this.colTimeDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDuration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeDuration.Caption = "Длительность смены";
            this.colTimeDuration.FieldName = "TimeDuration";
            this.colTimeDuration.Name = "colTimeDuration";
            this.colTimeDuration.OptionsColumn.AllowEdit = false;
            this.colTimeDuration.OptionsColumn.AllowFocus = false;
            this.colTimeDuration.OptionsColumn.ReadOnly = true;
            this.colTimeDuration.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeDuration.Visible = true;
            this.colTimeDuration.VisibleIndex = 6;
            this.colTimeDuration.Width = 109;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Пробег";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 7;
            this.colDistance.Width = 85;
            // 
            // colTimeStops
            // 
            this.colTimeStops.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStops.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStops.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStops.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStops.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStops.Caption = "Общее время стоянок";
            this.colTimeStops.FieldName = "TimeStops";
            this.colTimeStops.Name = "colTimeStops";
            this.colTimeStops.OptionsColumn.AllowEdit = false;
            this.colTimeStops.OptionsColumn.AllowFocus = false;
            this.colTimeStops.OptionsColumn.ReadOnly = true;
            this.colTimeStops.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeStops.Visible = true;
            this.colTimeStops.VisibleIndex = 8;
            this.colTimeStops.Width = 106;
            // 
            // colTimeEngineOn
            // 
            this.colTimeEngineOn.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEngineOn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEngineOn.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEngineOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEngineOn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEngineOn.Caption = "Время работы со вкл. двигателем";
            this.colTimeEngineOn.FieldName = "TimeEngineOn";
            this.colTimeEngineOn.Name = "colTimeEngineOn";
            this.colTimeEngineOn.OptionsColumn.AllowEdit = false;
            this.colTimeEngineOn.OptionsColumn.AllowFocus = false;
            this.colTimeEngineOn.OptionsColumn.ReadOnly = true;
            this.colTimeEngineOn.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeEngineOn.Visible = true;
            this.colTimeEngineOn.VisibleIndex = 9;
            this.colTimeEngineOn.Width = 107;
            // 
            // colTimeEngineOnStops
            // 
            this.colTimeEngineOnStops.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEngineOnStops.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEngineOnStops.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEngineOnStops.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEngineOnStops.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEngineOnStops.Caption = "Время стоянок со вкл. двигателем";
            this.colTimeEngineOnStops.FieldName = "TimeEngineOnStops";
            this.colTimeEngineOnStops.Name = "colTimeEngineOnStops";
            this.colTimeEngineOnStops.OptionsColumn.AllowEdit = false;
            this.colTimeEngineOnStops.OptionsColumn.AllowFocus = false;
            this.colTimeEngineOnStops.OptionsColumn.ReadOnly = true;
            this.colTimeEngineOnStops.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colTimeEngineOnStops.Visible = true;
            this.colTimeEngineOnStops.VisibleIndex = 10;
            this.colTimeEngineOnStops.Width = 121;
            // 
            // colFuelStart
            // 
            this.colFuelStart.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelStart.Caption = "Топлива в начале";
            this.colFuelStart.FieldName = "FuelStart";
            this.colFuelStart.Name = "colFuelStart";
            this.colFuelStart.OptionsColumn.AllowEdit = false;
            this.colFuelStart.OptionsColumn.AllowFocus = false;
            this.colFuelStart.OptionsColumn.ReadOnly = true;
            this.colFuelStart.Visible = true;
            this.colFuelStart.VisibleIndex = 11;
            this.colFuelStart.Width = 83;
            // 
            // colFuelEnd
            // 
            this.colFuelEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.Caption = "Топлива в конце";
            this.colFuelEnd.FieldName = "FuelEnd";
            this.colFuelEnd.Name = "colFuelEnd";
            this.colFuelEnd.OptionsColumn.AllowEdit = false;
            this.colFuelEnd.OptionsColumn.AllowFocus = false;
            this.colFuelEnd.OptionsColumn.ReadOnly = true;
            this.colFuelEnd.Visible = true;
            this.colFuelEnd.VisibleIndex = 12;
            this.colFuelEnd.Width = 64;
            // 
            // colFuelAdd
            // 
            this.colFuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelAdd.Caption = "Заправки";
            this.colFuelAdd.FieldName = "FuelAdd";
            this.colFuelAdd.Name = "colFuelAdd";
            this.colFuelAdd.OptionsColumn.AllowEdit = false;
            this.colFuelAdd.OptionsColumn.AllowFocus = false;
            this.colFuelAdd.OptionsColumn.ReadOnly = true;
            this.colFuelAdd.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelAdd.Visible = true;
            this.colFuelAdd.VisibleIndex = 13;
            this.colFuelAdd.Width = 69;
            // 
            // colFuelSub
            // 
            this.colFuelSub.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelSub.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelSub.Caption = "Сливы";
            this.colFuelSub.FieldName = "FuelSub";
            this.colFuelSub.Name = "colFuelSub";
            this.colFuelSub.OptionsColumn.AllowEdit = false;
            this.colFuelSub.OptionsColumn.AllowFocus = false;
            this.colFuelSub.OptionsColumn.ReadOnly = true;
            this.colFuelSub.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelSub.Visible = true;
            this.colFuelSub.VisibleIndex = 14;
            this.colFuelSub.Width = 74;
            // 
            // colFuelExpense
            // 
            this.colFuelExpense.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpense.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpense.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpense.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpense.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpense.Caption = "Общий расход, л";
            this.colFuelExpense.FieldName = "FuelExpense";
            this.colFuelExpense.Name = "colFuelExpense";
            this.colFuelExpense.OptionsColumn.AllowEdit = false;
            this.colFuelExpense.OptionsColumn.AllowFocus = false;
            this.colFuelExpense.OptionsColumn.ReadOnly = true;
            this.colFuelExpense.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colFuelExpense.Visible = true;
            this.colFuelExpense.VisibleIndex = 15;
            this.colFuelExpense.Width = 74;
            // 
            // colFuelExpenseAvgKm
            // 
            this.colFuelExpenseAvgKm.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseAvgKm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseAvgKm.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseAvgKm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseAvgKm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseAvgKm.Caption = "Ср. расход л/100 км";
            this.colFuelExpenseAvgKm.FieldName = "FuelExpenseAvgKm";
            this.colFuelExpenseAvgKm.Name = "colFuelExpenseAvgKm";
            this.colFuelExpenseAvgKm.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseAvgKm.OptionsColumn.AllowFocus = false;
            this.colFuelExpenseAvgKm.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseAvgKm.Visible = true;
            this.colFuelExpenseAvgKm.VisibleIndex = 16;
            this.colFuelExpenseAvgKm.Width = 84;
            // 
            // colFuelExpenseAvgHour
            // 
            this.colFuelExpenseAvgHour.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelExpenseAvgHour.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseAvgHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelExpenseAvgHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelExpenseAvgHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelExpenseAvgHour.Caption = "Ср. расход л/ч";
            this.colFuelExpenseAvgHour.FieldName = "FuelExpenseAvgHour";
            this.colFuelExpenseAvgHour.Name = "colFuelExpenseAvgHour";
            this.colFuelExpenseAvgHour.OptionsColumn.AllowEdit = false;
            this.colFuelExpenseAvgHour.OptionsColumn.AllowFocus = false;
            this.colFuelExpenseAvgHour.OptionsColumn.ReadOnly = true;
            this.colFuelExpenseAvgHour.Visible = true;
            this.colFuelExpenseAvgHour.VisibleIndex = 17;
            this.colFuelExpenseAvgHour.Width = 80;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcVehicle;
            this.gridView2.Name = "gridView2";
            // 
            // psReprts
            // 
            this.psReprts.Links.AddRange(new object[] {
            this.compositeLink1,
            this.linkGrid});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.psReprts;
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcVehicle;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.Margins = new System.Drawing.Printing.Margins(20, 20, 100, 100);
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystemBase = this.psReprts;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "car.png");
            // 
            // ReportVehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcVehicle);
            this.Name = "ReportVehicles";
            this.Size = new System.Drawing.Size(798, 446);
            this.Controls.SetChildIndex(this.gcVehicle, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReprts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkGrid.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcVehicle;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colNameForReport;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentID;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentIDName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationStart;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStops;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEngineOn;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEngineOnStops;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelStart;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelSub;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpense;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseAvgKm;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelExpenseAvgHour;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraPrinting.PrintingSystem psReprts;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}
