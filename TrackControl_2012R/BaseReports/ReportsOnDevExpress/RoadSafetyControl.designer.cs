﻿namespace BaseReports.ReportsDE
{
    partial class RoadSafetyControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoadSafetyControl));
            this.atlantaDataSet = new LocalCache.atlantaDataSet();
            this.trafficSafetyReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeReportLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.gcRoadSafety = new DevExpress.XtraGrid.GridControl();
            this.gvRoadSafety = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccelMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccelCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedTreshold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colKa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barAccel = new DevExpress.XtraBars.BarStaticItem();
            this.barBreaks = new DevExpress.XtraBars.BarStaticItem();
            this.barTreshold = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barBreak = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRoadSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoadSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // atlantaDataSet
            // 
            this.atlantaDataSet.DataSetName = "atlantaDataSet";
            this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // trafficSafetyReportBindingSource
            // 
            this.trafficSafetyReportBindingSource.DataMember = "trafficSafetyReport";
            this.trafficSafetyReportBindingSource.DataSource = this.atlantaDataSet;
            // 
            // atlantaDataSetBindingSource
            // 
            this.atlantaDataSetBindingSource.DataSource = this.trafficSafetyReportBindingSource;
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink1});
            // 
            // compositeReportLink1
            // 
            // 
            // 
            // 
            this.compositeReportLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink1.ImageCollection.ImageStream")));
            this.compositeReportLink1.Landscape = true;
            this.compositeReportLink1.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink1.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // gcRoadSafety
            // 
            this.gcRoadSafety.DataSource = this.trafficSafetyReportBindingSource;
            this.gcRoadSafety.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcRoadSafety.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcRoadSafety.Location = new System.Drawing.Point(0, 24);
            this.gcRoadSafety.MainView = this.gvRoadSafety;
            this.gcRoadSafety.Name = "gcRoadSafety";
            this.gcRoadSafety.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gcRoadSafety.Size = new System.Drawing.Size(779, 357);
            this.gcRoadSafety.TabIndex = 9;
            this.gcRoadSafety.UseEmbeddedNavigator = true;
            this.gcRoadSafety.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRoadSafety,
            this.gridView2});
            // 
            // gvRoadSafety
            // 
            this.gvRoadSafety.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvRoadSafety.Appearance.Empty.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvRoadSafety.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvRoadSafety.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvRoadSafety.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvRoadSafety.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoadSafety.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvRoadSafety.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvRoadSafety.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvRoadSafety.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvRoadSafety.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvRoadSafety.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvRoadSafety.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvRoadSafety.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvRoadSafety.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvRoadSafety.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvRoadSafety.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvRoadSafety.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvRoadSafety.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvRoadSafety.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.GroupRow.Options.UseFont = true;
            this.gvRoadSafety.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvRoadSafety.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRoadSafety.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvRoadSafety.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvRoadSafety.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvRoadSafety.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvRoadSafety.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.Preview.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.Preview.Options.UseForeColor = true;
            this.gvRoadSafety.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvRoadSafety.Appearance.Row.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvRoadSafety.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRoadSafety.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvRoadSafety.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRoadSafety.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvRoadSafety.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvRoadSafety.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvRoadSafety.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvRoadSafety.ColumnPanelRowHeight = 40;
            this.gvRoadSafety.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMark,
            this.colModel,
            this.colNumberPlate,
            this.colDriver,
            this.colTravel,
            this.colSpeedMax,
            this.colAccelMax,
            this.colBreakMax,
            this.colAccelCount,
            this.colBreakCount,
            this.colSpeedTreshold,
            this.colKa,
            this.colKb,
            this.colKv});
            this.gvRoadSafety.GridControl = this.gcRoadSafety;
            this.gvRoadSafety.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colAccelCount, "")});
            this.gvRoadSafety.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvRoadSafety.Name = "gvRoadSafety";
            this.gvRoadSafety.OptionsDetail.AllowZoomDetail = false;
            this.gvRoadSafety.OptionsDetail.EnableMasterViewMode = false;
            this.gvRoadSafety.OptionsDetail.ShowDetailTabs = false;
            this.gvRoadSafety.OptionsDetail.SmartDetailExpand = false;
            this.gvRoadSafety.OptionsSelection.MultiSelect = true;
            this.gvRoadSafety.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRoadSafety.OptionsView.EnableAppearanceOddRow = true;
            this.gvRoadSafety.OptionsView.ShowFooter = true;
            // 
            // colMark
            // 
            this.colMark.AppearanceCell.Options.UseTextOptions = true;
            this.colMark.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMark.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMark.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMark.AppearanceHeader.Options.UseTextOptions = true;
            this.colMark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMark.Caption = "Марка";
            this.colMark.FieldName = "Mark";
            this.colMark.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colMark.Name = "colMark";
            this.colMark.OptionsColumn.AllowEdit = false;
            this.colMark.OptionsColumn.AllowFocus = false;
            this.colMark.OptionsColumn.ReadOnly = true;
            this.colMark.ToolTip = "Марка транспортного средства";
            this.colMark.Visible = true;
            this.colMark.VisibleIndex = 0;
            this.colMark.Width = 40;
            // 
            // colModel
            // 
            this.colModel.AppearanceCell.Options.UseTextOptions = true;
            this.colModel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colModel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colModel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colModel.AppearanceHeader.Options.UseTextOptions = true;
            this.colModel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colModel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colModel.Caption = "Модель";
            this.colModel.FieldName = "Model";
            this.colModel.Name = "colModel";
            this.colModel.OptionsColumn.AllowEdit = false;
            this.colModel.OptionsColumn.AllowFocus = false;
            this.colModel.OptionsColumn.ReadOnly = true;
            this.colModel.ToolTip = "Модель транспортного средства";
            this.colModel.Visible = true;
            this.colModel.VisibleIndex = 1;
            this.colModel.Width = 40;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNumberPlate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberPlate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberPlate.Caption = "Номер";
            this.colNumberPlate.FieldName = "NamberPlate";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.AllowFocus = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.ToolTip = "Государственный или гаражный номер";
            this.colNumberPlate.Visible = true;
            this.colNumberPlate.VisibleIndex = 2;
            this.colNumberPlate.Width = 40;
            // 
            // colDriver
            // 
            this.colDriver.AppearanceCell.Options.UseTextOptions = true;
            this.colDriver.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriver.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriver.Caption = "Ф.И.О. водителя";
            this.colDriver.FieldName = "Driver";
            this.colDriver.Name = "colDriver";
            this.colDriver.OptionsColumn.AllowEdit = false;
            this.colDriver.OptionsColumn.AllowFocus = false;
            this.colDriver.OptionsColumn.ReadOnly = true;
            this.colDriver.ToolTip = "Ф.И.О. водителя";
            this.colDriver.Visible = true;
            this.colDriver.VisibleIndex = 3;
            this.colDriver.Width = 40;
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.Caption = "Путь, км";
            this.colTravel.DisplayFormat.FormatString = "{0:f1}";
            this.colTravel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravel.FieldName = "Travel";
            this.colTravel.Name = "colTravel";
            this.colTravel.OptionsColumn.AllowEdit = false;
            this.colTravel.OptionsColumn.AllowFocus = false;
            this.colTravel.OptionsColumn.ReadOnly = true;
            this.colTravel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Travel", "{0:f2}")});
            this.colTravel.ToolTip = "Пройденный путь, км";
            this.colTravel.Visible = true;
            this.colTravel.VisibleIndex = 4;
            this.colTravel.Width = 47;
            // 
            // colSpeedMax
            // 
            this.colSpeedMax.AppearanceCell.Options.UseTextOptions = true;
            this.colSpeedMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedMax.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSpeedMax.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedMax.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedMax.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedMax.Caption = "Скорость макс. км/ч";
            this.colSpeedMax.DisplayFormat.FormatString = "{0:f1}";
            this.colSpeedMax.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSpeedMax.FieldName = "SpeedMax";
            this.colSpeedMax.Name = "colSpeedMax";
            this.colSpeedMax.OptionsColumn.AllowEdit = false;
            this.colSpeedMax.OptionsColumn.AllowFocus = false;
            this.colSpeedMax.OptionsColumn.ReadOnly = true;
            this.colSpeedMax.ToolTip = "Максимальная скорость, км/ч";
            this.colSpeedMax.Visible = true;
            this.colSpeedMax.VisibleIndex = 5;
            this.colSpeedMax.Width = 47;
            // 
            // colAccelMax
            // 
            this.colAccelMax.AppearanceCell.Options.UseTextOptions = true;
            this.colAccelMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccelMax.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAccelMax.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAccelMax.AppearanceHeader.Options.UseTextOptions = true;
            this.colAccelMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccelMax.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAccelMax.Caption = "Ускорение макс. м/с2";
            this.colAccelMax.DisplayFormat.FormatString = "{0:f1}";
            this.colAccelMax.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAccelMax.FieldName = "AccelMax";
            this.colAccelMax.Name = "colAccelMax";
            this.colAccelMax.OptionsColumn.AllowEdit = false;
            this.colAccelMax.OptionsColumn.AllowFocus = false;
            this.colAccelMax.OptionsColumn.ReadOnly = true;
            this.colAccelMax.ToolTip = "Максимальное ускорение, м/с2";
            this.colAccelMax.Visible = true;
            this.colAccelMax.VisibleIndex = 6;
            this.colAccelMax.Width = 47;
            // 
            // colBreakMax
            // 
            this.colBreakMax.AppearanceCell.Options.UseTextOptions = true;
            this.colBreakMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBreakMax.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBreakMax.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBreakMax.AppearanceHeader.Options.UseTextOptions = true;
            this.colBreakMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBreakMax.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBreakMax.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBreakMax.Caption = "Торможение макс. м/с2";
            this.colBreakMax.DisplayFormat.FormatString = "{0:f1}";
            this.colBreakMax.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBreakMax.FieldName = "BreakMax";
            this.colBreakMax.Name = "colBreakMax";
            this.colBreakMax.OptionsColumn.AllowEdit = false;
            this.colBreakMax.OptionsColumn.AllowFocus = false;
            this.colBreakMax.OptionsColumn.ReadOnly = true;
            this.colBreakMax.ToolTip = "Максимальное торможение, м/с2";
            this.colBreakMax.Visible = true;
            this.colBreakMax.VisibleIndex = 7;
            this.colBreakMax.Width = 47;
            // 
            // colAccelCount
            // 
            this.colAccelCount.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colAccelCount.AppearanceCell.Options.UseTextOptions = true;
            this.colAccelCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccelCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAccelCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAccelCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colAccelCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccelCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAccelCount.Caption = "Кол-во ускорений";
            this.colAccelCount.DisplayFormat.FormatString = "{0:f1}";
            this.colAccelCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAccelCount.FieldName = "AccelCount";
            this.colAccelCount.Name = "colAccelCount";
            this.colAccelCount.OptionsColumn.AllowEdit = false;
            this.colAccelCount.OptionsColumn.AllowFocus = false;
            this.colAccelCount.OptionsColumn.ReadOnly = true;
            this.colAccelCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AccelCount", "{0:f2}")});
            this.colAccelCount.ToolTip = "Количество превышений заданного ускорения";
            this.colAccelCount.Visible = true;
            this.colAccelCount.VisibleIndex = 8;
            this.colAccelCount.Width = 47;
            // 
            // colBreakCount
            // 
            this.colBreakCount.AppearanceCell.Options.UseTextOptions = true;
            this.colBreakCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBreakCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBreakCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBreakCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colBreakCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBreakCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBreakCount.Caption = "Кол-во торможений";
            this.colBreakCount.DisplayFormat.FormatString = "{0:f1}";
            this.colBreakCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBreakCount.FieldName = "BreakCount";
            this.colBreakCount.Name = "colBreakCount";
            this.colBreakCount.OptionsColumn.AllowEdit = false;
            this.colBreakCount.OptionsColumn.AllowFocus = false;
            this.colBreakCount.OptionsColumn.ReadOnly = true;
            this.colBreakCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BreakCount", "{0:f2}")});
            this.colBreakCount.ToolTip = "Количество превышений заданного торможения";
            this.colBreakCount.Visible = true;
            this.colBreakCount.VisibleIndex = 9;
            this.colBreakCount.Width = 100;
            // 
            // colSpeedTreshold
            // 
            this.colSpeedTreshold.AppearanceCell.Options.UseTextOptions = true;
            this.colSpeedTreshold.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedTreshold.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSpeedTreshold.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedTreshold.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedTreshold.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedTreshold.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSpeedTreshold.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSpeedTreshold.Caption = "Время превышения скорости";
            this.colSpeedTreshold.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSpeedTreshold.FieldName = "SpeedTreshold";
            this.colSpeedTreshold.Name = "colSpeedTreshold";
            this.colSpeedTreshold.OptionsColumn.AllowEdit = false;
            this.colSpeedTreshold.OptionsColumn.AllowFocus = false;
            this.colSpeedTreshold.OptionsColumn.ReadOnly = true;
            this.colSpeedTreshold.ToolTip = "Суммарное время движения со скоростью превышающей заданную";
            this.colSpeedTreshold.Visible = true;
            this.colSpeedTreshold.VisibleIndex = 10;
            this.colSpeedTreshold.Width = 55;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colKa
            // 
            this.colKa.AppearanceCell.Options.UseTextOptions = true;
            this.colKa.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKa.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKa.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKa.AppearanceHeader.Options.UseTextOptions = true;
            this.colKa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKa.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKa.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKa.Caption = "Ка";
            this.colKa.DisplayFormat.FormatString = "{0:f1}";
            this.colKa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKa.FieldName = "Ka";
            this.colKa.Name = "colKa";
            this.colKa.OptionsColumn.AllowEdit = false;
            this.colKa.OptionsColumn.AllowFocus = false;
            this.colKa.OptionsColumn.ReadOnly = true;
            this.colKa.ToolTip = "Коэффициент ускорения, определяется как отношение количества ускорения к пройденн" +
    "ому пути";
            this.colKa.Visible = true;
            this.colKa.VisibleIndex = 11;
            this.colKa.Width = 61;
            // 
            // colKb
            // 
            this.colKb.AppearanceCell.Options.UseTextOptions = true;
            this.colKb.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKb.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKb.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKb.AppearanceHeader.Options.UseTextOptions = true;
            this.colKb.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKb.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKb.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKb.Caption = "Кв";
            this.colKb.DisplayFormat.FormatString = "{0:f2}";
            this.colKb.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKb.FieldName = "Kb";
            this.colKb.Name = "colKb";
            this.colKb.OptionsColumn.AllowEdit = false;
            this.colKb.OptionsColumn.AllowFocus = false;
            this.colKb.OptionsColumn.ReadOnly = true;
            this.colKb.ToolTip = "Коэффициент торможения, определяется как отношение количества торможения к пройде" +
    "нному пути";
            this.colKb.Visible = true;
            this.colKb.VisibleIndex = 12;
            this.colKb.Width = 66;
            // 
            // colKv
            // 
            this.colKv.AppearanceCell.Options.UseTextOptions = true;
            this.colKv.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKv.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKv.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKv.AppearanceHeader.Options.UseTextOptions = true;
            this.colKv.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKv.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKv.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKv.Caption = "Kv";
            this.colKv.DisplayFormat.FormatString = "{0:f2}";
            this.colKv.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colKv.FieldName = "Kv";
            this.colKv.Name = "colKv";
            this.colKv.OptionsColumn.AllowEdit = false;
            this.colKv.OptionsColumn.AllowFocus = false;
            this.colKv.OptionsColumn.ReadOnly = true;
            this.colKv.ToolTip = "Коэффициент скорости, определяется как отношение времени превышения скорости к вр" +
    "емени движения";
            this.colKv.Visible = true;
            this.colKv.VisibleIndex = 13;
            this.colKv.Width = 81;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcRoadSafety;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barAccel,
            this.barBreak,
            this.barBreaks,
            this.barTreshold});
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barAccel),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBreaks),
            new DevExpress.XtraBars.LinkPersistInfo(this.barTreshold)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // barAccel
            // 
            this.barAccel.Caption = "Количество ускорений: ---";
            this.barAccel.Id = 0;
            this.barAccel.Name = "barAccel";
            this.barAccel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barBreaks
            // 
            this.barBreaks.Caption = "Количество торможений: ---";
            this.barBreaks.Id = 2;
            this.barBreaks.Name = "barBreaks";
            this.barBreaks.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barTreshold
            // 
            this.barTreshold.Caption = "Время превышения скорости: --:--:--";
            this.barTreshold.Id = 3;
            this.barTreshold.Name = "barTreshold";
            this.barTreshold.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(779, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 381);
            this.barDockControl2.Size = new System.Drawing.Size(779, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 381);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(779, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 381);
            // 
            // barBreak
            // 
            this.barBreak.Caption = "Количество торможений: ---";
            this.barBreak.Id = 1;
            this.barBreak.Name = "barBreak";
            this.barBreak.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // RoadSafetyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gcRoadSafety);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "RoadSafetyControl";
            this.Size = new System.Drawing.Size(779, 406);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gcRoadSafety, 0);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficSafetyReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcRoadSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoadSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LocalCache.atlantaDataSet atlantaDataSet;
        private System.Windows.Forms.BindingSource trafficSafetyReportBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink1;
        private DevExpress.XtraGrid.GridControl gcRoadSafety;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRoadSafety;
        private DevExpress.XtraGrid.Columns.GridColumn colMark;
        private DevExpress.XtraGrid.Columns.GridColumn colModel;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colDriver;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMax;
        private DevExpress.XtraGrid.Columns.GridColumn colAccelMax;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakMax;
        private DevExpress.XtraGrid.Columns.GridColumn colAccelCount;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedTreshold;
        private DevExpress.XtraGrid.Columns.GridColumn colKa;
        private DevExpress.XtraGrid.Columns.GridColumn colKb;
        private DevExpress.XtraGrid.Columns.GridColumn colKv;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barAccel;
        private DevExpress.XtraBars.BarStaticItem barBreak;
        private DevExpress.XtraBars.BarStaticItem barBreaks;
        private DevExpress.XtraBars.BarStaticItem barTreshold;
    }
}
