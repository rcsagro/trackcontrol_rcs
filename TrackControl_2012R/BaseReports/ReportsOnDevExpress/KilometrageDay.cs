﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure; 
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles ;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports.ReportsDE
{
    // Класс контрола для отображения отчета о пробеге
    // и остановках транспортного средства
    [Serializable]
    public partial class KilometrageDay : BaseReports.ReportsDE.BaseControl
    {
        // Класс хранитель данных по каждой машине для формирование отчета
        // для каждого отчета разный
        public class reportKlmtDay
        {
            string initTime;
            string locatStart;
            string locatEnd;
            string initalTime;
            string finalTime;
            string intervWork;
            string intervMove;
            string timeStopsAll;
            double distance;
            double avrSpeed;

            public reportKlmtDay(string initTime, string locatStart, string locatEnd,
                string initalTime, string finalTime, string intervWork, string intervMove,
                string timeStopsAll, double distance, double avrSpeed)
            {
                this.initTime = initTime;
                this.locatStart = locatStart;
                this.locatEnd = locatEnd;
                this.initalTime = initalTime;
                this.finalTime = finalTime;
                this.intervWork = intervWork;
                this.intervMove = intervMove;
                this.timeStopsAll = timeStopsAll;
                this.distance = distance;
                this.avrSpeed = avrSpeed;
            } // reportKlmtDay

            public string InitTime
            {
                get { return initTime; }
            }

            public string LocationStart
            {
                get { return locatStart; }
            }

            public string LocationEnd
            {
                get { return locatEnd; }
            }

            public string BeginTimeMove
            {
                get { return initalTime; }
            }
            public string EndTimeMove
            {
                get { return finalTime; }
            }
            public string IntervalWork
            {
                get { return intervWork; }
            }

            public string IntervalMove
            {
                get { return intervMove; }
            }

            public string TimeStopsTotal
            {
                get { return timeStopsAll; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double AverageSpeed
            {
                get { return avrSpeed; }
            }
        } // reportKlmtDay

        KilometrageDays algorithm;
        atlantaDataSet.mobitelsRow mobitel;
        Dictionary<int, KilometrageDays.SummaryDay> summariesDay;
        ReportBase<reportKlmtDay, TInfo> ReportingKlmtDay;

        /* Конструктор */
        public KilometrageDay()
        {
            InitializeComponent();

            algorithm = new KilometrageDays();
            AddAlgorithm(algorithm);
            _dataset = ReportTabControl.Dataset;
            summariesDay = new Dictionary<int, KilometrageDays.SummaryDay>();
            //kilometrageReportDayBindingSource.DataSource = _dataset;
            //kilometrageReportDayBindingSource.DataMember = "KilometrageReportDay";
            
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gvKlmtDay, gcKlmtDay, bar3);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingKlmtDay =
                new ReportBase<reportKlmtDay, TInfo>(Controls, compositeReportLink, gvKlmtDay,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // KilometrageDay

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is KilometrageDays)
            {
                if (e is KilometrageDaysEventArgs)
                {
                    KilometrageDaysEventArgs args = (KilometrageDaysEventArgs)e;
                    if (summariesDay.ContainsKey(args.Id))
                    {
                        summariesDay.Remove(args.Id);
                    }
                    summariesDay.Add(args.Id, args.SummaryDay);
                    Select(curMrow);
                } // if
            } // if
        } // Algorithm_Action

        public override string Caption
        {
            get { return Resources.KilometrageDay; }
        }

        public override void ClearReport()
        {
            gcKlmtDay.DataSource = null;
            _dataset.KilometrageReport.Clear();
            _dataset.KilometrageReportDay.Clear();
            summariesDay.Clear();
            ClearStatusLine();
        }

        private void ClearStatusLine()
        {
            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ---");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:--");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:--");
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel == null)
                return;

            curMrow = mobitel;

            if (summariesDay.ContainsKey(curMrow.Mobitel_ID))
            {
                barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalWay.ToString("F"));

                barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalTimeTour.ToString());

                barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": ",
                    summariesDay[curMrow.Mobitel_ID].TotalTimeWay.ToString());
            } // if
           
            kilometrageReportDayBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
            gcKlmtDay.DataSource = kilometrageReportDayBindingSource;

            //_dataset.KilometrageReport.Clear();
            //if (_dataset.KilometrageReport.Count > 0) // aketner - 18.09.2012
            //{
            //    string s = "Kilometrage Report is Initialized";
            //}

            if (kilometrageReportDayBindingSource.Count > 0)
            {
                EnableButton();
            }
            else
            {
                DisableButton();
                ClearStatusLine();
            }
        } // Select

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            algorithm.SelectItem(m_row);
            algorithm.Run();
        }

        // --   Обработчики GUI   --
        // callback show data in report's table
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true;
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                // Установим связь с таблицами
                kilometrageReportDayBindingSource.DataSource = _dataset;
                kilometrageReportDayBindingSource.DataMember = "KilometrageReportDay";
                
                foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        Application.DoEvents();
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun) 
                            break;
                    } // if
                } // foreach

                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                Select(mobitel);

                ReportsControl.OnClearMapObjectsNeeded();
                ReportsControl.ShowGraph(curMrow);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnGraph();
        } // bbiShowOnMap_ItemClick

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportDayRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow d_row in _dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;
                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach
            return new Track(row.MobitelId, Color.Red, 2f, data);
        } // GetTrackSegment

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowSpeedOnGraph();
        } // bbiShowOnGraf_ItemClick

        private void gvKlmtDay_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                ShowSpeedOnGraph();
                ShowTrackOnGraph();
            } // if
        } // gvDriver_FocusedRowChanged

        private void ShowSpeedOnGraph()
        {
            Graph.ClearRegion();
            int rowIndex = gvKlmtDay.GetFocusedDataSourceRowIndex();
            atlantaDataSet.KilometrageReportDayRow row = (atlantaDataSet.KilometrageReportDayRow)
                ((DataRowView)kilometrageReportDayBindingSource.List[rowIndex]).Row;

            Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
        }

        private void ShowTrackOnGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();

            List<Track> segments = new List<Track>();
            Int32[] selected_row = gvKlmtDay.GetSelectedRows();

            for (int i = 0; i < selected_row.Length; i++)
            {
                atlantaDataSet.KilometrageReportDayRow k_row = (atlantaDataSet.KilometrageReportDayRow)
                    ((DataRowView)kilometrageReportDayBindingSource.List[selected_row[i]]).Row;

                segments.Add(GetTrackSegment(k_row));
            } // for

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // ShowTrackOnGraph

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (int mobitelId in summariesDay.Keys) // обходим выбранные машины
            {
                TInfo t_info = new TInfo();
                VehicleInfo info = new VehicleInfo(mobitelId);

                // заголовок для таблиц отчета, для каждого отчета разный
                t_info.infoVehicle = info.Info; // Транспорт
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                t_info.totalWay = summariesDay[mobitelId].TotalWay; // Общий пройденный путь
                t_info.totalTimerTour = summariesDay[mobitelId].TotalTimeTour; // Общая продолжительность смены
                t_info.totalTimeWay = summariesDay[mobitelId].TotalTimeWay; // Общее время в пути
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmtDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmtDay.CreateBindDataList(); // создать новый список данных таблицы отчета

                // получить данные таблицы отчета по машине
                foreach (atlantaDataSet.KilometrageReportDayRow row in
                    _dataset.KilometrageReportDay.Select("MobitelId=" + mobitelId, "Id ASC"))
                {
                    string initTime = row.InitialTime.ToString("D");
                    string locatStart = (row["LocationStart"] != DBNull.Value) ? row.LocationStart : " - ";
                    string locatEnd = (row["LocationEnd"] != DBNull.Value) ? row.LocationEnd : " - ";
                    string initalTime = row.InitialTime.ToString("t");
                    string finalTime = row.FinalTime.ToString("t");
                    string intervWork = row.IntervalWork;
                    string intervMove = row.IntervalMove;
                    string timeStops = row.TimeStopsTotal.ToString();

                    double distance = 0.0;
                    double avrSpeed = 0.0;

                    if (row["Distance"] != DBNull.Value)
                        distance = Math.Round(row.Distance, 2);

                    if (row["AverageSpeed"] != DBNull.Value)
                        avrSpeed = Math.Round(row.AverageSpeed, 2);

                    // сохранить данные таблицы отчета в списке таблиц
                    ReportingKlmtDay.AddDataToBindList(new reportKlmtDay(initTime, locatStart,
                        locatEnd, initalTime, finalTime, intervWork, intervMove, timeStops,
                                                 distance, avrSpeed));
                } // foreach 1

                ReportingKlmtDay.CreateElementReport();
            } // foreach 2

            ReportingKlmtDay.CreateAndShowReport();
            ReportingKlmtDay.DeleteData();
        } // ExportAllDevToReport

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportKilometrageDay, e);
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingKlmtDay.GetInfoStructure;
            return (Resources.TotalDistTravel + ": " + String.Format("{0:f2}", info.totalWay) +
                Resources.KM + "\n" +
                Resources.TotalDurShift + ": " + info.totalTimerTour + "\n" +
                Resources.TotalTravelTime + ": " + info.totalTimeWay);
        }
      
        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvKlmtDay, true, true);

            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);

            // заголовок для таблиц отчета, для каждого отчета разный
            t_info.infoVehicle = info.Info; // Транспорт
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
            t_info.totalWay = summariesDay[curMrow.Mobitel_ID].TotalWay; // Общий пройденный путь
            t_info.totalTimerTour = summariesDay[curMrow.Mobitel_ID].TotalTimeTour; // Общая продолжительность смены
            t_info.totalTimeWay = summariesDay[curMrow.Mobitel_ID].TotalTimeWay; // Общее время в пути
            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            ReportingKlmtDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingKlmtDay.CreateAndShowReport(gcKlmtDay);          
        } // ExportToExcelDevExpress
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvKlmtDay);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvKlmtDay);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcKlmtDay);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        public void Localization()
        {
            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": --- ");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:-- ");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:-- ");

            colData.Caption = Resources.Date;
            colPlaceStartMove.Caption = Resources.StartLocation;
            colPlaceEndMove.Caption = Resources.MovementEndPlace;
            colTimeStartMove.Caption = Resources.InitialMovementTime;
            colTimeEndMove.Caption = Resources.EndMovementTime;
            colDurationPeriod.Caption = Resources.ShiftDurationHint;
            colTotalDurationPeriod.Caption = Resources.MovementTotalTime;
            colTotalDistance.Caption = Resources.DistanceText;
            colSpeedAverage.Caption = Resources.SpeedAverage;
            colStopTime.Caption = Resources.StopTime;

            colData.ToolTip = Resources.Date;
            colPlaceStartMove.ToolTip = Resources.StartLocation;
            colPlaceEndMove.ToolTip = Resources.MovementEndPlace;
            colTimeStartMove.ToolTip = Resources.InitialMovementTime;
            colTimeEndMove.ToolTip = Resources.EndMovementTime;
            colDurationPeriod.ToolTip = Resources.ShiftDurationHint;
            colTotalDurationPeriod.ToolTip = Resources.MovementTotalTime;
            colTotalDistance.ToolTip = Resources.DistanceText;
            colSpeedAverage.ToolTip = Resources.SpeedAverage;
            colStopTime.ToolTip = Resources.StopTimeHint;

            barDistanceStatus.Caption = String.Concat(Resources.DistanceText.ToString(), ": ---");
            barDurationStatus.Caption = String.Concat(Resources.TotalShiftTimeText.ToString(), ": --:--:--");
            barTimeTravel.Caption = String.Concat(Resources.TotalTravelTimeText.ToString(), ": --:--:--");

            //colTotalDistance.SummaryItem.DisplayFormat = String.Concat(Resources.Total, ": {0:f2}");
        } // Localization
  } // KilometrageDay
} // BaseReports.ReportsDE
