﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Utils;
using LocalCache;
using ReportsOnFuelExpenses;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports.ReportsDE
{
    public partial class KilometrageTotal : BaseReports.ReportsDE.BaseControl
    {   
       public class reportKlmtAll
       {
           string transportName;
           string driverName;
           string placeStart;
           string placeEnd;
           DateTime timeStart;
           DateTime timeEnd;
           TimeSpan duration;
           double distanceTotal;
           TimeSpan timeMoveTotal;
           TimeSpan timeStopsTotal;
           private TimeSpan MotoHrStop;
           private double normalWay;
           private double normalMotor;
           private double summaNormal;
           
           public reportKlmtAll(string transportName, string driverName, string placeStart,
                                string placeEnd, DateTime timeStart, DateTime timeEnd,
                                TimeSpan duration, double distanceTotal, TimeSpan timeMoveTotal,
                                TimeSpan timeStopsTotal, TimeSpan MotoHrStop, double normWay, double normMotor, double summaNormal)
           {
               this.transportName = transportName;
               this.driverName = driverName;
               this.placeStart = placeStart;
               this.placeEnd = placeEnd;
               this.timeStart = timeStart;
               this.timeEnd = timeEnd;
               this.duration = duration;
               this.distanceTotal = distanceTotal;
               this.timeMoveTotal = timeMoveTotal;
               this.timeStopsTotal = timeStopsTotal;
               this.MotoHrStop = MotoHrStop;
               this.normalWay = normWay;
               this.normalMotor = normMotor;
               this.summaNormal = summaNormal;
           } // reportKlmAll
           
           public string TransportName
           {
               get { return transportName; }
           }
           
           public string DraverName
           {
               get { return driverName; }
           }
           
           public string PlaceStart
           {
               get { return placeStart; }
           }
           
           public string PlaceEnd
           {
               get { return placeEnd; }
           }
           
           public DateTime TimeStart
           {
               get { return timeStart; }
           }
           
           public DateTime TimeEnd
           {
               get { return timeEnd; }
           }
           
           public TimeSpan Duration
           {
               get { return duration; }
           }
           
           public double DistanceTotal
           {
               get { return distanceTotal; }
           }
           
           public TimeSpan TimeMoveTotal
           {
               get { return timeMoveTotal; }
           }
           
           public TimeSpan TimeStopsTotal
           {
               get { return timeStopsTotal; }
           }

           public TimeSpan MotorHoursStop
           {
               get { return this.MotoHrStop; }
           }

           public double NormalWayFuel
           {
               get { return this.normalWay; }
           }

           public double NormalMotorFuel
           {
               get { return this.normalMotor; }
           }

           public double SummaNormal
           {
               get { return this.summaNormal; }
           }
       } // reportKlmtAll

       KilometrageDays algorithm;
       atlantaDataSet.mobitelsRow mobitel = null;
       Dictionary<int, KilometrageDays.SummaryDay> summariesDay;
       BindingList<reportKlmtAll> gridDataList;
       ReportBase<reportKlmtAll, TInfo> ReportingKlmtTotal;
       bool flagTableShow = false;
       List <atlantaDataSet.mobitelsRow> listCurMrow;
       Rotation rotate_algoritm;
       //GpsData[] d_rows;
       
        public KilometrageTotal()
        {
            InitializeComponent();
            HideStatusBar();
            Localization();
            DisableButton();
            DisableRun();
            DisableMenuFirstButton();
            VisionPanel(gViewKlmtTotal, gControlKlmtTotal, null);
            Dock = DockStyle.Fill;
            
            gViewKlmtTotal.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            gViewKlmtTotal.Appearance.OddRow.Options.UseBackColor = true;
            gViewKlmtTotal.OptionsView.EnableAppearanceOddRow = true;
            gViewKlmtTotal.OptionsSelection.MultiSelect = true;
            rotate_algoritm = new Rotation();
            
            listCurMrow = new List<atlantaDataSet.mobitelsRow>();           
            algorithm = new KilometrageDays();
            AddAlgorithm( rotate_algoritm );
            AddAlgorithm(algorithm);
            _dataset = ReportTabControl.Dataset;
            summariesDay = new Dictionary<int, KilometrageDays.SummaryDay>();
            klmtReportDayBindingSource.DataSource = _dataset;
            klmtReportDayBindingSource.DataMember = "KilometrageReportDay";
            gridDataList = new BindingList<reportKlmtAll>();
            gViewKlmtTotal.Columns.View.OptionsBehavior.AutoPopulateColumns = false;
            gViewKlmtTotal.RowClick += new RowClickEventHandler(gViewKlmtTotal_ClickRow);
            gViewKlmtTotal.CustomDrawFooterCell += GViewKlmtTotalOnCustomDrawFooterCell;

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);
            
            ReportingKlmtTotal =
                new ReportBase<reportKlmtAll, TInfo>(Controls, compositeReportLink, gViewKlmtTotal,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        private void GViewKlmtTotalOnCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
        }
        
        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is KilometrageDays)
            {
                if (e is KilometrageDaysEventArgs)
                {
                    KilometrageDaysEventArgs args = (KilometrageDaysEventArgs)e;
                    if (summariesDay.ContainsKey(args.Id))
                    {
                        summariesDay.Remove(args.Id);
                    }
                    summariesDay.Add(args.Id, args.SummaryDay);
                    Select(curMrow);
                } // if
            } // if
        } // Algorithm_Action
        
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            //algorithm.SelectItem(m_row);
            //algorithm.Run();

            foreach( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if( _stopRun )
                    return;
            }
        }
        
        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;

                if (mobitel.Check)
                {
                    if (gridDataList.Count > 0)
                    {
                        gControlKlmtTotal.DataSource = gridDataList;
                        EnableButton();
                    }
                    else
                    {
                        if (!flagTableShow)
                        {
                            DisableButton();
                            gControlKlmtTotal.DataSource = null;
                        }
                    } // else
                } // if
                else
                {
                    DisableButton();
                    gControlKlmtTotal.DataSource = null;
                }

                klmtReportDayBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
            } // if
            else
            {
                DisableButton();
                ReportsControl.GraphClearSeries();
                Graph.ClearLabel();
            }
        } // Select

        TimeSpan ConvertToSpan(string format)
        {
            TimeSpan dmtime = new TimeSpan(0, 0, 0, 0);
            bool result = TimeSpan.TryParse(format, out dmtime); //****
            return dmtime;
        }
        
        protected void CreateKlmtDistanceTotal()
        {
            if( gridDataList == null)
                return;

            int iRow = -1;
            //обходим выбранные машины
            foreach( int mobitelId in summariesDay.Keys)
            {
                iRow++;
                //VehicleInfo info = new VehicleInfo(mobitelId);
             
                atlantaDataSet.mobitelsDataTable mobitable = _dataset.mobitels;
                atlantaDataSet.mobitelsRow mobitel = null;
                foreach (var mobi in mobitable)
                {
                    if (mobitelId == mobi.Mobitel_ID)
                    {
                        mobitel = mobi;
                        break;
                    }
                }

                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);
                
                string transportName = info.Info; // Транспорт
                string driverName = info.DriverFullName; // Водитель транспорта
                double totalWay = summariesDay[mobitelId].TotalWay; // Пройденный путь
                TimeSpan totalTimerTour = summariesDay[mobitelId].TotalTimeTour; // Продолжительность смен

                DataRow[] dRow = _dataset.KilometrageReportDay.Select("MobitelId=" + mobitelId, "Id ASC");
                if (dRow.Length == 0) 
                    return;
                atlantaDataSet.KilometrageReportDayRow nRow = (atlantaDataSet.KilometrageReportDayRow)
                    ((DataRow)dRow[0]);
                
                string placeStartMove = nRow.LocationStart; // Место начала движения
                DateTime timeStartMove = nRow.InitialTime;  // Время начала движения //****

                int EndRow = dRow.Length - 1;

                nRow = (atlantaDataSet.KilometrageReportDayRow)((DataRow)dRow[EndRow]);

                string placeEndMove = nRow.LocationEnd; // Место конца движения
                DateTime timeEndMove = nRow.FinalTime;  // Время конца движения //*****

                TimeSpan timeMoveAll = new TimeSpan(0, 0, 0, 0);
                TimeSpan timeAllMotorStop = new TimeSpan(0, 0, 0, 0);
               
                // получить данные таблицы отчета по машине
                for( int i = 0; i < dRow.Length; i++ )
                {
                    nRow = (atlantaDataSet.KilometrageReportDayRow)((DataRow)dRow[i]);
                    TimeSpan dmtime = new TimeSpan(0, 0, 0, 0);
                    dmtime = ConvertToSpan(nRow.IntervalMove); //****
                    TimeSpan duration1 = new TimeSpan(dmtime.Days, dmtime.Hours, dmtime.Minutes, dmtime.Seconds);
                    timeMoveAll = timeMoveAll + duration1;

                    TimeSpan drTime = new TimeSpan(0, 0, 0, 0);
                    if(nRow["MotorHrIdle"] != DBNull.Value)
                        drTime = ConvertToSpan(nRow.MotorHrIdle); //****
                    else
                        drTime = ConvertToSpan("0.00:00:00");

                    TimeSpan duration2 = new TimeSpan(drTime.Days, drTime.Hours, drTime.Minutes, drTime.Seconds);
                    timeAllMotorStop = timeAllMotorStop + duration2;
                } // foreach

                double NormalMotor = 0.0;
                double NormalWay = 0.0;
                double SummaNormal = 0.0;
                TimeSpan MotoHoursStop = new TimeSpan();

                //// aketner 24.05.2013
                atlantaDataSet.vehicleRow[] mrow = listCurMrow[iRow].GetvehicleRows();
                if (mrow.Length > 0)
                {
                    if (mrow[0].Mobitel_id == mobitelId)
                    {
                        double normWays = 0.0;
                        double normMotorHr = 0.0;

                        if (mrow[0]["Team_id"] != DBNull.Value)
                        {
                            if (mrow[0].FuelWayLiter == 0.0 && mrow[0].FuelMotorLiter == 0.0)
                            {
                                foreach (atlantaDataSet.teamRow teamRow in Algorithm.AtlantaDataSet.team)
                                {
                                    if (teamRow.id == mrow[0].Team_id)
                                    {
                                        normWays = teamRow.FuelWayKmGrp;
                                        normMotorHr = teamRow.FuelMotoHrGrp;
                                        break;
                                    } // if
                                } // foreach
                            } // if
                            else
                            {
                                normWays = mrow[0].FuelWayLiter;
                                normMotorHr = mrow[0].FuelMotorLiter;
                            }
                        } // if
                        else
                        {
                            normWays = mrow[0].FuelWayLiter;
                            normMotorHr = mrow[0].FuelMotorLiter;
                        }

                        //  FuelExpens fuelExpenses = new FuelExpens();
                        // TimeSpan TimeStopWithEngineOn = fuelExpenses.CalcTimeEngineOn(mobitelId, listCurMrow[iRow]);

                        double seconds = timeAllMotorStop.Days*86400 + timeAllMotorStop.Seconds + timeAllMotorStop.Minutes*60 +
                                         timeAllMotorStop.Hours*3600;
                        double hours = seconds/3600.0;
                        NormalMotor = Math.Round(hours*normMotorHr, 1);

                        MotoHoursStop = timeAllMotorStop;

                        NormalWay = totalWay*normWays/100;

                        SummaNormal = NormalWay + NormalMotor;
                    } // if
               } // if

                TimeSpan time_Stops_All = totalTimerTour - timeMoveAll;

                gridDataList.Add(new reportKlmtAll(transportName, driverName, placeStartMove,
                                                    placeEndMove, timeStartMove, timeEndMove,
                                                    totalTimerTour, totalWay, timeMoveAll, time_Stops_All,
                                                    MotoHoursStop, NormalWay, NormalMotor, SummaNormal ) );
                } // foreach

                gControlKlmtTotal.DataSource = gridDataList;
            flagTableShow = true;
        } // CreateKlmtDistanceTotal
        
        public override string Caption
        {
            get
            {
                return Resources.KilometrageTotalCaption;
            }
        }
        
        public override void ClearReport()
        {
            listCurMrow.Clear();
            flagTableShow = false;
            gControlKlmtTotal.DataSource = null;
            gridDataList.Clear();
            _dataset.KilometrageReport.Clear();
            _dataset.KilometrageReportDay.Clear();
            summariesDay.Clear();
        }
        
        // --   Обработчики GUI   --
        // callback show data in report's table
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true;
            if(bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;
                
                foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
                {
                    if (m_row. Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        Application.DoEvents();
                        listCurMrow.Add(m_row);
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun) 
                            break;
                    } // if
                } // foreach
                
                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                Select(mobitel);

                ReportsControl.OnClearMapObjectsNeeded();
                //ReportsControl.ShowGraph(curMrow);
                ReportsControl.ShowGraphFromMobi(curMrow);

                SetStartButton();
                CreateKlmtDistanceTotal();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                StopReport();
                _stopRun = true;
                
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick
        
        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.TotalReportTravel, e);
            TInfo info = ReportingKlmtTotal.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }
        
         /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            return ("");
        }

        /* функция для формирования верхней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            return ("");
        }
        
       // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            try
            {
                XtraGridService.SetupGidViewForPrint(gViewKlmtTotal, true, true);

                TInfo t_info = new TInfo();

                //if (curMrow == null)
                //    return;
                
                //VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);

                //// заголовок для таблиц отчета
                //t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                //t_info.infoDriverName = info.DriverFullName; // Водитель транспорта

                t_info.totalWay = 0; // Пройденный путь
                t_info.totalTimerTour = new TimeSpan(0, 0, 0); // Время в пути
                t_info.totalTimeWay = new TimeSpan(0, 0, 0); // Время стоянки/стоянок
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmtTotal.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmtTotal.CreateAndShowReport(gControlKlmtTotal);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.StackTrace, e.Message, MessageBoxButtons.OK);
            }
        } // ExportAllDevToReport

        protected void ShowSpeedOnGraph(int iRow)
        {
            if (listCurMrow == null)
                return;

            if (listCurMrow.Count <= 0)
                return;

            ReportsControl.ShowGraph(listCurMrow[iRow]);
        } // ShowSpeedOnGraph

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportDayRow row, List<GpsData> data)
        {
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId, data); 
            return new Track(row.MobitelId, Color.DarkBlue, 2f, data);
        } // GetTrackSegment

        static void showMark(PointLatLng latlng, int mobitelId, string message)
        {
            PointLatLng location = new PointLatLng(latlng.Lat, latlng.Lng);
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Report, mobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeededWith(markers);
        }

        // строим трек на карте на основе отчета посуточного пробега
        protected void ShowTrackOnMap(int iRow)
        {
            atlantaDataSet.KilometrageReportDayRow nRow;
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();
            atlantaDataSet.mobitelsRow mobRow = listCurMrow[iRow];
            DataRow[] dRow = _dataset.KilometrageReportDay.Select("MobitelId=" + mobRow.Mobitel_ID, "Id ASC");
            algorithm.SelectItem(mobRow);
            
            for (int i = 0; i < dRow.Length; i++)
            {
                nRow = (atlantaDataSet.KilometrageReportDayRow)((DataRow)dRow[i]);
                List<GpsData> data = new List<GpsData>(); 
                segments.Add(GetTrackSegment(nRow, data));
            } // for

            if (segments.Count > 0)
            {
                ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                if(segments[0].GeoPoints == null)
                    return;

                showMark(segments[0].GeoPoints[segments[0].GeoPoints.Count - 1].LatLng, mobRow.Mobitel_ID, mobRow.Name);
            }
        } // ShowTrackOnMap
        
         // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            int indexRow = gViewKlmtTotal.GetFocusedDataSourceRowIndex();
            ShowTrackOnMap(indexRow);  
        } // bbiShowOnMap_ItemClick
        
        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            int indexRow = gViewKlmtTotal.GetFocusedDataSourceRowIndex();
            ShowSpeedOnGraph(indexRow);
        } // bbiShowOnGraf_ItemClick
        
        //клик по строке таблицы
        private void gViewKlmtTotal_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                int indexRow = gViewKlmtTotal.GetFocusedDataSourceRowIndex();
                ShowSpeedOnGraph(indexRow); // перерисовать график
                ShowTrackOnMap(indexRow); // перерисовать карту
            } // if
        } // gvDriver_FocusedRowChanged
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gViewKlmtTotal);
        }
        
        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gViewKlmtTotal);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gControlKlmtTotal);
        }
        
        public void Localization()
        {
            colTransport.Caption = Resources.Transport;
            colDriver.Caption = Resources.Driver;
            colPlaceBeginMove.Caption = Resources.StartLocation; 
            colPlaceEndMove.Caption = Resources.MovementEndPlace;
            colTimeBeginMove.Caption = Resources.TimeBeginMove;
            colTimeEndMove.Caption = Resources.TimeEndMove;
            colDurationPeriods.Caption = Resources.DurationPeriod;
            colDistance.Caption = Resources.Distance;
            colTimeMoving.Caption = Resources.TimeMoving;
            colTimeParkingAll.Caption = Resources.TimeParkingAll;

            colTransport.ToolTip = Resources.Transport;
            colDriver.ToolTip = Resources.Driver;
            colPlaceBeginMove.ToolTip = Resources.StartLocation;
            colPlaceEndMove.ToolTip = Resources.MovementEndPlace;
            colTimeBeginMove.ToolTip = Resources.TimeBeginMove;
            colTimeEndMove.ToolTip = Resources.TimeEndMove;
            colDurationPeriods.ToolTip = Resources.DurationPeriod;
            colDistance.ToolTip = Resources.Distance;
            colTimeMoving.ToolTip = Resources.TimeMoving;
            colTimeParkingAll.ToolTip = Resources.TimeParkingAllHint;

            colMotorHrStop.Caption = Resources.MotorHrStop;
            colMotorHrStop.ToolTip = Resources.MotorHrStopTip;
            colNormalMotor.Caption = Resources.NormalMotor;
            colNormalMotor.ToolTip = Resources.NormalMotorTip;
            colNormalWay.Caption = Resources.NormalWay;
            colNormalWay.ToolTip = Resources.NormalWayTip;
            colSummaNormal.Caption = Resources.SummaNormal;
            colSummaNormal.ToolTip = Resources.SummaNormalTip;
        } // Localization
    } // KilometrageTotal
} // BaseReports.ReportsDE
