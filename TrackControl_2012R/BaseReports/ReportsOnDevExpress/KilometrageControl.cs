﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraScheduler.UI;
using LocalCache;
using Report;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports.Graph;

namespace BaseReports.ReportsDE
{
    public partial class KilometrageControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportKlmt
        {
            string state;
            string location;
            string datetime;
            string initial_time;
            string final_time;
            string interval;
            double distance;
            double general_distance;
            double average_speed;
            double max_speed;

            public reportKlmt(string state, string location, string datetime, string initial_time,
                    string final_time, string interval, double distance, 
                double general_distance, double average_speed, double max_speed)
            {
                this.state = state;
                this.location = location;
                this.datetime = datetime;
                this.initial_time = initial_time;
                this.final_time = final_time;
                this.interval = interval;
                this.distance = distance;
                this.general_distance = general_distance;
                this.average_speed = average_speed;
                this.max_speed = max_speed;
            } // reportKlmt

            public string State
            {
                get { return state; }
            }

            public string Location
            {
                get { return location; }
            }

            public string DateTime
            {
                get { return datetime; }
            }

            public string Initial_Time
            {
                get { return initial_time; }
            }

            public string Final_Time
            {
                get { return final_time; }
            }

            public string Interval
            {
                get { return interval; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double General_Distance
            {
                get { return general_distance; }
            }

            public double Average_Speed
            {
                get { return average_speed; }
            }

            public double Max_Speed
            {
                get { return max_speed; }
            }
        } // reportKlmt

        public static int MobitelId = 0; // статическая переменная, используется в GoogleMapControl.cs
        Kilometrage algoritm;
        atlantaDataSet _dataset;
        atlantaDataSet.mobitelsRow _mobitel;
        Dictionary<int, Kilometrage.Summary> _summaries;
        IGraph _graph;
        bool _allStops;
        BarButtonItem _bbiShowParking;
        Bar _barC;
        BarManager _bManager;
        bool _flagShowParking = true;
        ReportBase<reportKlmt, TInfo> ReportingKlmt;
        string NameThisReport = Resources.Kilometrage;
        //Rotation rotate_algoritm;

        public KilometrageControl()
        {
            InitializeComponent();
            Init();
            HideStatusBar();
            Localization();
            DisableButton();
            VisionPanel(gvKlmtC, gcKlmtC, bar3);
            Dock = DockStyle.Fill;
            gvKlmtC.Columns.View.OptionsBehavior.AutoPopulateColumns = false;
            gvKlmtC.KeyDown += GvKlmtCOnKeyDown;
            gvKlmtC.RowClick += gvKlmtC_RowClick;
            algoritm = new Kilometrage();
            //rotate_algoritm = new Rotation();
            //AddAlgorithm( rotate_algoritm );
            AddAlgorithm(algoritm);
            algoritm.ReportCompleted += _algorithm_ReportCompleted;
            _dataset = ReportTabControl.Dataset;
            _source.DataSource = _dataset;
            _graph = ReportTabControl.Graph;
            _summaries = new Dictionary<int, Kilometrage.Summary>();
            bbiShowOnGraf.Enabled = false;
            DisableRun();
            isButtonStartPush = false;
            gvKlmtC.CustomDrawFooterCell += GvKlmtCOnCustomDrawFooterCell;
            gvKlmtC.CustomColumnGroup += gvKlmtC_CustomColumnGroup;
            gvKlmtC.CustomDrawGroupRow += gvKlmtC_CustomDrawGroupRow;
            gvKlmtC.BeforePrintRow += gvKlmtC_BeforePrintRow;

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingKlmt =
                new ReportBase<reportKlmt, TInfo>(Controls, compositeReportLink, gvKlmtC,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
            EnableRun();
        }

        private void GvKlmtCOnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            if (keyEventArgs.KeyCode == Keys.Down || keyEventArgs.KeyCode == Keys.Up)
            {
                redrawMapAndGraph();                
            }
        }

        void gvKlmtC_RowClick(object sender, RowClickEventArgs e)
        {
            redrawMapAndGraph();
        }

        void gvKlmtC_BeforePrintRow(object sender, DevExpress.XtraGrid.Views.Printing.CancelPrintRowEventArgs e)
        {
            GridView view = sender as GridView;
            int hndl = e.RowHandle;
            int countRow = 0;
            TimeSpan duration = new TimeSpan();
            string GroupText = "";

            if(hndl < 0 && hndl > -2147483648)
            {
                e.Cancel = true;
                GroupText = view.GetGroupRowDisplayText(hndl);
                int count = view.GetChildRowCount(hndl);
                for (int i = 0; i < count; i++)
                {
                    int handle = view.GetChildRowHandle(hndl, i);
                    if (string.Equals(view.GetRowCellValue(handle, "State"), Resources.Parking))
                    {
                        countRow++;
                        string period = view.GetRowCellValue(handle, "Interval").ToString();
                        TimeSpan speriod = TimeSpan.Parse(period);
                        duration += speriod;
                    }
                } // for

                GroupText += ". " + Resources.KilometrageTotalNumber + countRow + ". ";
                GroupText += Resources.KilometrageNumberStops + duration;

                TextBrick tb = e.PS.CreateTextBrick() as TextBrick;
                tb.Text = GroupText;
                tb.Font = new Font(tb.Font, FontStyle.Bold);
                tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
                tb.Padding = new PaddingInfo(5, 0, 0, 0);
                tb.BackColor = Color.LightGray;
                tb.ForeColor = Color.Black; 
                SizeF clientPageSize = (e.BrickGraphics as BrickGraphics).ClientPageSize;
                float textBrickHeight = ((GridViewInfo)view.GetViewInfo()).CalcRowHeight(e.Graphics, e.RowHandle, e.Level);
                RectangleF textBrickRect = new RectangleF(0, e.Y, (int)clientPageSize.Width, textBrickHeight);
                e.BrickGraphics.DrawBrick(tb, textBrickRect);
                e.Y += (int)textBrickHeight;
            } // if
        }

        void gvKlmtC_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            int countRow = 0;
            TimeSpan duration = new TimeSpan();

            if (info.Column.Caption == Resources.KilometrageDateTimeCaption)
            {
                int count = view.GetChildRowCount(info.RowHandle);
                for (int i = 0; i < count; i++)
                {
                    int handle = view.GetChildRowHandle(info.RowHandle, i);

                    if (string.Equals(view.GetRowCellValue(handle, "State"), Resources.Parking))
                    {
                        countRow++;
                        string period = view.GetRowCellValue(handle, "Interval").ToString();
                        TimeSpan speriod = TimeSpan.Parse(period);
                        duration += speriod;
                    }
                }

                info.GroupText = Resources.KilometrageDateTimeCaption + ": " + info.GroupValueText + ". ";
                info.GroupText += ". " + Resources.KilometrageTotalNumber + countRow + ". ";
                info.GroupText += Resources.KilometrageNumberStops + duration;
            }
        }

        void gvKlmtC_CustomColumnGroup(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "DateTime")
            {
                e.Handled = true;
            }
        }

        private void GvKlmtCOnCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
        }
        
        public override string Caption
        {
            get { return Resources.Kilometrage; }
        }

        public override void ClearReport()
        {
            ReportTabControl.Dataset.KilometrageReport.Clear();
            _summaries.Clear();
            gcKlmtC.DataSource = null;
            ClearStatusLine();
            //_dataset.KilometrageReport.Clear();
            //ClearStatusLine();
        }

        public override void DisableButton()
        {
            bbiShowOnMap.Enabled = false;
            bbiShowOnGraf.Enabled = false;
            bbiPrintReport.Enabled = false;
            _bbiShowParking.Enabled = false;
        }

        public override void EnableButton()
        {
            bbiShowOnMap.Enabled = true;
            bbiShowOnGraf.Enabled = true;
            bbiPrintReport.Enabled = true;
            _bbiShowParking.Enabled = true;
        }

        // ============ для статистики================
        private void func_test()
        {
            DriverDb db = new DriverDb();
            string sql = @"SELECT IDENT_CURRENT('datagps')";
            db.ConnectDb();

            int time_sleep = (int) 0.5 * 60 * 1000;
            int cycles = 15;
            int numberId;
            List<int> list_result = new List<int>();
            List<int> list_time = new List<int>();

            while (true)
            {
                DataTable tbl = db.GetDataTable( sql );
                numberId = Convert.ToInt32( tbl.Rows[0][0].ToString() );
                list_result.Add(numberId);
                list_time.Add(System.DateTime.Now.Hour * 3600 + System.DateTime.Now.Minute * 60 + System.DateTime.Now.Second);
                cycles--;

                if(cycles <= 0)
                    break;

                Thread.Sleep( time_sleep );
            }

            List<double> statist = new List<double>();

            for (int i = 0; i < list_result.Count - 1; i++)
            {
                double delta_result = list_result[i + 1] - list_result[i];
                double delta_time = list_time[i + 1] - list_time[i];
                statist.Add(delta_result / delta_time);
            }

            double stat_result = 0.0;

            for (int i = 0; i < statist.Count; i++)
            {
                stat_result += statist[i];
            }

            double result = stat_result / statist.Count;
        } // test
        // ======================================
        
        // --   Обработчики GUI   --
        // callback show data in report's table
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            curMrow = _mobitel;

            if (bchSafetyMode.Checked)
            {
                RunInSafeMode(Caption);
                return;
            }

            ReportsControl.OnClearMapObjectsNeeded();
            bool noData = true;
            isButtonStartPush = false;

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;
               
                foreach (atlantaDataSet.mobitelsRow mRow in _dataset.mobitels)
                {
                    if (mRow.Check && DataSetManager.IsGpsDataExist(mRow))
                    {
                        Application.DoEvents();
                        SelectItem(mRow);
                        noData = false;
                        if (_stopRun) 
                            break;
                    }
                } // foreach

                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                isButtonStartPush = true;
                Select(curMrow);
                SetStartButton();
                EnableButton();
                //ReportsControl.ShowGraphFromMobi(curMrow);
                ReportsControl.ShowGraph(curMrow);
            } // if
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnGraph();
        }

        private void ShowTrackOnGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();
            Int32[] selected_row = gvKlmtC.GetSelectedRows();

            for (int i = 0; i < selected_row.Length; i++)
            {
                atlantaDataSet.KilometrageReportRow nrow = (atlantaDataSet.KilometrageReportRow)
                    ((DataRowView)_source.List[selected_row[i]]).Row;

                segments.Add(GetTrackSegment(nrow));
            } // for

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // ShowTrackOnGraph

        private Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId , data);

            if (data.Count <= 0)
                return null;

            return new Track(row.MobitelId, Color.Red, 2f, data);
        } // GetTrackSegment

        private void ShowSpeedOnGraph()
        {
            Graph.ClearRegion();
            int rowIndex = gvKlmtC.GetFocusedDataSourceRowIndex();
            if (rowIndex >= 0)
            {
                atlantaDataSet.KilometrageReportRow row = (atlantaDataSet.KilometrageReportRow)
                    ((DataRowView)_source.List[rowIndex]).Row;

                Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
            }
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowSpeedOnGraph();
        }
        
        protected void bbiShowParking_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _allStops = !_allStops;
            ReportsControl.OnMapShowNeeded();
            redrawMapAndGraph();

            if (_flagShowParking)
            {
                _bbiShowParking.Hint = Resources.ShowParkingOnMap;
                _flagShowParking = false;
            }
            else
            {
                _bbiShowParking.Hint = Resources.RemoveParkingFromMap;
                _flagShowParking = true;
            }
        } // bbiShowParking_ItemClick
        
        public void SelectItem( atlantaDataSet.mobitelsRow m_row )
        {
            foreach( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if( _stopRun ) 
                    return;
            }
        }

        private void ClearStatusLine()
        {
            _distanceStatus.Caption = Resources.KilometrageCtrlSumDist.ToString() + ": ----";
            _motionTimeStatus.Caption = Resources.KilometrageTravelTime.ToString() + ": --:--:--";
            _parkingTimeStatus.Caption = Resources.KilometrageParkingTime.ToString() + ": --:--:--";
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            ReportsControl.OnClearMapObjectsNeeded();

            gcKlmtC.DataSource = null;

            if (mobitel == null)
                return;

            _mobitel = mobitel;

            MobitelId = _mobitel.Mobitel_ID;

            if (_summaries.ContainsKey(mobitel.Mobitel_ID))
            {
                _distanceStatus.Caption = Resources.KilometrageCtrlSumDist + ": " +
                    String.Format("{0:f2}", _summaries[mobitel.Mobitel_ID].TotalTravel);

                string time = String.Format( @"{0:dd\.hh\:mm\:ss}", _summaries[mobitel.Mobitel_ID].TravelTime );
                if(time.Contains( "00" ) )
                {
                    time = time.Remove( 0, 3 );
                }

                _motionTimeStatus.Caption = Resources.KilometrageTravelTime + ": " + time;

                time = String.Format(@"{0:dd\.hh\:mm\:ss}", _summaries[mobitel.Mobitel_ID].ParkingTime);

                if (time.Contains("00"))
                {
                    time = time.Remove(0, 3);
                }

                _parkingTimeStatus.Caption = Resources.KilometrageParkingTime + ": " + time;
            } // if

            if (isButtonStartPush)
            {
                _source.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                gcKlmtC.DataSource = _source;

                if (_source.Count > 0)
                {
                    if (CurrentActivateReport == NameThisReport)
                    {
                        EnableButton();
                        redrawMapAndGraph();
                    }
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        void _algorithm_ReportCompleted(KilometrageEventArgs args)
        {
            if (_summaries.ContainsKey(args.Id))
                _summaries.Remove(args.Id);

            _summaries.Add(args.Id, args.Summary);
        } // _algorithm_ReportCompleted

        void redrawMapAndGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            _graph.ClearRegion();
            DataRow dR = gvKlmtC.GetFocusedDataRow();

            if (dR != null)
            {
                atlantaDataSet.KilometrageReportRow row = (atlantaDataSet.KilometrageReportRow)dR;
                _graph.AddTimeRegion(row.InitialTime, row.FinalTime);

                if (Resources.Movement == row.State)
                    showTrackSegment(row);
                else if (!_allStops)
                    showStop(row);
            } // if

            if (_allStops)
                showAllStops();
        } // redrawMapAndGraph

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            try
            {
                string format;
                string culture = CultureInfo.CurrentCulture.Name;

                if ("ru-RU" == culture)
                    format = "HH:mm";
                else
                    format = "HH:mm";

                foreach (int mobitelId in _summaries.Keys)
                {
                    atlantaDataSet.mobitelsRow mobi = _dataset.mobitels.FindByMobitel_ID(mobitelId);
                    VehicleInfo info = new VehicleInfo(mobitelId, mobi);
                    TInfo t_info = new TInfo();

                    // заголовок для таблиц отчета, для каждого отчета разный
                    t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                    t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                    t_info.totalWay = _summaries[mobitelId].TotalTravel; // Пройденный путь
                    t_info.totalTimerTour = _summaries[mobitelId].TravelTime; // Время в пути
                    t_info.totalTimeWay = _summaries[mobitelId].ParkingTime; // Время стоянки/стоянок
                    t_info.periodBeging = Algorithm.Period.Begin;
                    t_info.periodEnd = Algorithm.Period.End;

                    ReportingKlmt.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                    ReportingKlmt.CreateBindDataList(); // создать новый список данных таблицы отчета

                    foreach (atlantaDataSet.KilometrageReportRow row in _dataset.KilometrageReport.Select(
                        "MobitelId=" + mobitelId, "Id ASC"))
                    {
                        string state = row.State;
                        string location = (row["Location"] != DBNull.Value) ? row.Location : " - ";

                        string initial_time = row.InitialTime.ToString(format);
                        string final_time = row.FinalTime.ToString(format);
                        string interval = row.Interval.ToString();
                        string datetime = row.DateTime.ToString("dd.MM.yyyy");

                        double distance = 0.0;
                        double general_distance = 0.0;
                        double average_speed = 0.0;
                        double max_speed = 0.0;

                        if (row["Distance"] != DBNull.Value)
                            distance = Math.Round(row.Distance, 2);

                        if (row["GeneralDistance"] != DBNull.Value)
                            general_distance = Math.Round(row.GeneralDistance, 2);

                        if (row["AverageSpeed"] != DBNull.Value)
                            average_speed = Math.Round(row.AverageSpeed, 2);

                        if (row["MaxSpeed"] != DBNull.Value)
                            max_speed = Math.Round(row.MaxSpeed, 2);

                        ReportingKlmt.AddDataToBindList(new reportKlmt(state, location, datetime, initial_time,
                            final_time, interval, distance, general_distance, average_speed, max_speed));
                    } // foreach 1

                    ReportingKlmt.CreateElementReport();
                } // foreach 2

                ReportingKlmt.CreateAndShowReport();
                ReportingKlmt.DeleteData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error import report! Try again.",
                    MessageBoxButtons.OK);
            }
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            try
            {
                XtraGridService.SetupGidViewForPrint(gvKlmtC, true, true);

                TInfo t_info = new TInfo();
                VehicleInfo info = new VehicleInfo(_mobitel.Mobitel_ID, _mobitel); // сначала работаем по RFID

                // заголовок для таблиц отчета, для каждого отчета разный
                t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                t_info.totalWay = _summaries[_mobitel.Mobitel_ID].TotalTravel; // Пройденный путь
                t_info.totalTimerTour = _summaries[_mobitel.Mobitel_ID].TravelTime; // Время в пути
                t_info.totalTimeWay = _summaries[_mobitel.Mobitel_ID].ParkingTime; // Время стоянки
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmt.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmt.CreateAndShowReport(gcKlmtC);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error importing. Try again.",
                    MessageBoxButtons.OK);
            }
        } // ExportToExcelDevExpress

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.DetailReportTravel, e);
            TInfo info = ReportingKlmt.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        private string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования левой части заголовка отчета */
        private string GetStringBreackLeft()
        {
            TInfo info = ReportingKlmt.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */
        private string GetStringBreackRight()
        {
            TInfo info = ReportingKlmt.GetInfoStructure;
            return (Resources.TotalDistTravel + ": " + String.Format("{0:f2}", info.totalWay) +
                Resources.KM + "\n" +
                Resources.TimeInTraveled + ": " + info.totalTimerTour + "\n" +
                Resources.ParkingTime + ": " + info.totalTimeWay);
        }

        static void showStop(atlantaDataSet.KilometrageReportRow row)
        {
            PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
            string message = row.Interval.ToString();
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeeded(markers);
        }

        void showAllStops()
        {
            List<Marker> markers = new List<Marker>();
            string filterExpression = String.Format("MobitelId={0}", _mobitel.Mobitel_ID);

            foreach (atlantaDataSet.KilometrageReportRow row in _dataset.KilometrageReport.Select(filterExpression))
            {
                if (Resources.Parking == row.State)
                {
                    PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
                    string message = row.Interval.ToString();
                    markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
                } // if
            } // foreach

            ReportsControl.OnMarkersShowNeeded(markers);
        } // showAllStops

        static void showTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId, data);
            if (data.Count > 0)
            {
                List<Track> segments = new List<Track>();
                segments.Add(new Track(row.MobitelId, Color.Red, 2f, data));
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // showTrackSegment

        //void gvKlmtC_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        //{
           // redrawMapAndGraph();
        //}

        private void Localization()
        {
            ClearStatusLine();

            _colState.Caption = Resources.KilometrageCtrlColState;
            _colLocation.Caption = Resources.Location;
            _colInitialTime.Caption = Resources.InitialTime;
            _colFinalTime.Caption = Resources.EndTime;
            _colInterval.Caption = Resources.DateTimeInterval;
            _colDistance.Caption = Resources.KilometrageCtrlColDistance;
            _colDistFromBegin.Caption = Resources.KilometrageCtrlColDistFromBegin;
            _colAvgSpeed.Caption = Resources.KilometrageCtrlColAvgSpeed;
            colMaxSpeed.Caption = Resources.KilometrageCtrlColMaxSpeed;

            _colState.ToolTip = Resources.KilometrageCtrlColState;
            _colLocation.ToolTip = Resources.Location;
            _colInitialTime.ToolTip = Resources.InitialTime;
            _colFinalTime.ToolTip = Resources.EndTime;
            _colInterval.ToolTip = Resources.DateTimeInterval;
            _colDistance.ToolTip = Resources.KilometrageCtrlColDistance;
            _colDistFromBegin.ToolTip = Resources.KilometrageCtrlColDistFromBegin;
            _colAvgSpeed.ToolTip = Resources.KilometrageCtrlColAvgSpeed;
            colMaxSpeed.ToolTip = Resources.KilometrageCtrlColMaxSpeed;
            _colInitialDateTime.ToolTip = Resources.KilometrageDateTimeToolTip;
            _colInitialDateTime.Caption = Resources.KilometrageDateTimeCaption;

            //_colDistance.SummaryItem.DisplayFormat = String.Concat(Resources.Total, ": {0:f2}");

            _imageComboRepo.Items[0].Description = global::BaseReports.Properties.Resources.Parking;
            _imageComboRepo.Items[0].Value = global::BaseReports.Properties.Resources.Parking;
            _imageComboRepo.Items[0].ImageIndex = 0;

            _imageComboRepo.Items[1].Description = global::BaseReports.Properties.Resources.Movement;
            _imageComboRepo.Items[1].Value = global::BaseReports.Properties.Resources.Movement;
            _imageComboRepo.Items[1].ImageIndex = 1;

            _bbiShowParking.Hint = Resources.ShowParkingOnMap;
        } // Localization
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvKlmtC);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvKlmtC);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcKlmtC);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        private void Init()
        {
            bchSafetyMode.Visibility = BarItemVisibility.Always; 
            _bbiShowParking = new BarButtonItem();
            _bManager = GetBarManager();
            _bManager.Items.Add(_bbiShowParking);
            _barC = GetToolBar();
            _barC.LinksPersistInfo.Insert(3, new LinkPersistInfo(BarLinkUserDefines.PaintStyle,
                _bbiShowParking, BarItemPaintStyle.CaptionGlyph));
            _bbiShowParking.Caption = "";
            _bbiShowParking.Glyph = imageCollection.Images[1];
            _bbiShowParking.Id = 3;
            _bbiShowParking.Name = "bbiShowParking";
            _bbiShowParking.Hint = Resources.ShowParkingOnMap;
            _bbiShowParking.ItemClick += new ItemClickEventHandler(bbiShowParking_ItemClick);
        }
    } // KilometrageC
} // BaseReports.ReportsDE
