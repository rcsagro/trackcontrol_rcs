﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports.ReportsDE
{
    public partial class KilometrageC : BaseReports.ReportsDE.BaseControl
    {
        // Класс хранитель данных по каждой машине для формирование отчета
        // для каждого отчета разный
        public class reportKlmt
        {
            string state;
            string location;
            DateTime initial_time;
            DateTime final_time;
            string interval;
            double distance;
            double general_distance;
            double average_speed;
            double max_speed;

            public reportKlmt(string state, string location, DateTime initial_time,
                    DateTime final_time, string interval, double distance, 
                double general_distance, double average_speed, double max_speed)
            {
                this.state = state;
                this.location = location;
                this.initial_time = initial_time;
                this.final_time = final_time;
                this.interval = interval;
                this.distance = distance;
                this.general_distance = general_distance;
                this.average_speed = average_speed;
                this.max_speed = max_speed;
            } // reportKlmt

            public string State
            {
                get { return state; }
            }

            public string Location
            {
                get { return location; }
            }

            public DateTime Initial_Time
            {
                get { return initial_time; }
            }

            public DateTime Final_Time
            {
                get { return final_time; }
            }

            public string Interval
            {
                get { return interval; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double General_Distance
            {
                get { return general_distance; }
            }

            public double Average_Speed
            {
                get { return average_speed; }
            }

            public double Max_Speed
            {
                get { return max_speed; }
            }
        } // reportKlmt

        public static int mobitelID = 0; // статическая переменная, используется в GoogleMapControl.cs
        Kilometrage _algorithm;
        atlantaDataSet _dataset;
        atlantaDataSet.mobitelsRow _mobitel;
        Dictionary<int, Kilometrage.Summary> _summaries;
        IGraph _graph;
        bool _allStops;
        BarButtonItem bbiShowParking;
        Bar BarC;
        BarManager bManager;
        bool flagShowParking = true;
        ReportBase<reportKlmt, TInfo> ReportingKlmt;

        public KilometrageC()
        {
            InitializeComponent();
            Init();
            HideStatusBar();
            Localization();
            DisableButton();
            VisionPanel(gvKlmtC, gcKlmtC, bar3);
            Dock = DockStyle.Fill;
            gvKlmtC.Columns.View.OptionsBehavior.AutoPopulateColumns = false;
            gvKlmtC.FocusedRowChanged += gvKlmtC_FocusedRowChanged;
            _algorithm = new Kilometrage();
            AddAlgorithm(_algorithm);
            _algorithm.ReportCompleted += _algorithm_ReportCompleted;
            _dataset = ReportTabControl.Dataset;
            _source.DataSource = _dataset;
            _graph = ReportTabControl.Graph;
            _summaries = new Dictionary<int, Kilometrage.Summary>();
            bbiShowOnGraf.Enabled = false;
            DisableRun();
            isButtonStartPush = false;

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingKlmt =
                new ReportBase<reportKlmt, TInfo>(Controls, compositeReportLink, gvKlmtC,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // KilometrageC
        
        public override string Caption
        {
            get { return Resources.Kilometrage; }
        }

        public override void ClearReport()
        {
            ReportTabControl.Dataset.KilometrageReport.Clear();
            _summaries.Clear();
            gcKlmtC.DataSource = null;
            ClearStatusLine();
            _dataset.KilometrageReport.Clear();
        
            ClearStatusLine();
        }

        public override void DisableButton()
        {
            bbiShowOnMap.Enabled = false;
            bbiShowOnGraf.Enabled = false;
            bbiPrintReport.Enabled = false;
            bbiShowParking.Enabled = false;
        }

        public override void EnableButton()
        {
            bbiShowOnMap.Enabled = true;
            bbiShowOnGraf.Enabled = true;
            bbiPrintReport.Enabled = true;
            bbiShowParking.Enabled = true;
        }
        
        // --   Обработчики GUI   --
        // callback show data in report's table
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnClearMapObjectsNeeded();
            
            bool noData = true;
            isButtonStartPush = false;
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();

                BeginReport();
                _stopRun = false;

                foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        Application.DoEvents();
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun) 
                            break;
                    }
                } // foreach

                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                isButtonStartPush = true;
                Select(_mobitel);
                ReportsControl.ShowGraph(_mobitel);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnGraph();
        }

        private void ShowTrackOnGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();
            Int32[] selected_row = gvKlmtC.GetSelectedRows();

            for (int i = 0; i < selected_row.Length; i++)
            {
                atlantaDataSet.KilometrageReportRow nrow = (atlantaDataSet.KilometrageReportRow)
                    ((DataRowView)_source.List[selected_row[i]]).Row;
                segments.Add(GetTrackSegment(nrow));
            } // for

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // ShowTrackOnGraph

        public Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow d_row in _dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;
                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach
            return new Track(row.MobitelId, Color.Red, 2f, data);
        } // GetTrackSegment

        private void ShowSpeedOnGraph()
        {
            Graph.ClearRegion();
            int rowIndex = gvKlmtC.GetFocusedDataSourceRowIndex();

            atlantaDataSet.KilometrageReportRow row = (atlantaDataSet.KilometrageReportRow)
                ((DataRowView)_source.List[rowIndex]).Row;

            Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowSpeedOnGraph();
        }
        
        protected void bbiShowParking_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _allStops = !_allStops;
            ReportsControl.OnMapShowNeeded();
            redrawMapAndGraph();

            if (flagShowParking)
            {
                bbiShowParking.Hint = Resources.ShowParkingOnMap;
                flagShowParking = false;
            }
            else
            {
                bbiShowParking.Hint = Resources.RemoveParkingFromMap;
                flagShowParking = true;
            }
        } // bbiShowParking_ItemClick

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            _algorithm.SelectItem(m_row);
            _algorithm.Run();
        }

        private void ClearStatusLine()
        {
            _distanceStatus.Caption = Resources.KilometrageCtrlSumDist.ToString() + ": ----";
            _motionTimeStatus.Caption = Resources.KilometrageTravelTime.ToString() + ": --:--:--";
            _parkingTimeStatus.Caption = Resources.KilometrageParkingTime.ToString() + ": --:--:--";
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            ReportsControl.OnClearMapObjectsNeeded();

            gcKlmtC.DataSource = null;

            if (mobitel == null)
                return;

            _mobitel = mobitel;
            mobitelID = _mobitel.Mobitel_ID;

            if (_summaries.ContainsKey(mobitel.Mobitel_ID))
            {
                _distanceStatus.Caption = Resources.KilometrageCtrlSumDist.ToString() + ": " +
                    String.Format("{0:f2}", _summaries[mobitel.Mobitel_ID].TotalTravel);
                    _motionTimeStatus.Caption = Resources.KilometrageTravelTime.ToString() + ": " +
                        String.Format(@"{0:dd\.hh\:mm}", _summaries[mobitel.Mobitel_ID].TravelTime);
                    _parkingTimeStatus.Caption = Resources.KilometrageParkingTime.ToString() + ": " +
                    String.Format(@"{0:dd\.hh\:mm}", _summaries[mobitel.Mobitel_ID].ParkingTime);
            } // if

            if (isButtonStartPush)
            {
                _source.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                gcKlmtC.DataSource = _source;

                if (_source.Count > 0)
                {
                    EnableButton();
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        void _algorithm_ReportCompleted(KilometrageEventArgs args)
        {
            if (_summaries.ContainsKey(args.Id))
                _summaries.Remove(args.Id);

            _summaries.Add(args.Id, args.Summary);
        } // _algorithm_ReportCompleted

        void redrawMapAndGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            _graph.ClearRegion();
            DataRow dR = gvKlmtC.GetFocusedDataRow();

            if (dR != null)
            {
                atlantaDataSet.KilometrageReportRow row = (atlantaDataSet.KilometrageReportRow)dR;
                _graph.AddTimeRegion(row.InitialTime, row.FinalTime);

                if (Resources.Movement == row.State)
                    showTrackSegment(row);
                else if (!_allStops)
                    showStop(row);
            } // if

            if (_allStops)
                showAllStops();
        } // redrawMapAndGraph

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (int mobitelId in _summaries.Keys)
            {
                VehicleInfo info = new VehicleInfo(mobitelId);
                TInfo t_info = new TInfo();
                
                // заголовок для таблиц отчета, для каждого отчета разный
                t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                t_info.totalWay = _summaries[mobitelId].TotalTravel; // Пройденный путь
                t_info.totalTimerTour = _summaries[mobitelId].TravelTime; // Время в пути
                t_info.totalTimeWay = _summaries[mobitelId].ParkingTime; // Время стоянки/стоянок
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmt.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmt.CreateBindDataList(); // создать новый список данных таблицы отчета

                foreach (atlantaDataSet.KilometrageReportRow row in _dataset.KilometrageReport.Select(
                    "MobitelId=" + mobitelId, "Id ASC"))
                {
                    string state = row.State;
                    string location = (row["Location"] != DBNull.Value) ? row.Location : " - ";
                    DateTime initial_time = row.InitialTime;
                    DateTime final_time = row.FinalTime;
                    string interval = row.Interval.ToString();

                    double distance = 0.0;
                    double general_distance = 0.0;
                    double average_speed = 0.0;
                    double max_speed = 0.0;
                    
                    if (row["Distance"] != DBNull.Value)
                         distance = Math.Round(row.Distance, 2);
                    
                    if (row["GeneralDistance"] != DBNull.Value)
                         general_distance = Math.Round(row.GeneralDistance, 2);
                    
                    if (row["AverageSpeed"] != DBNull.Value)
                         average_speed = Math.Round(row.AverageSpeed, 2);

                    if(row["MaxSpeed"] != DBNull.Value)
                        max_speed = Math.Round(row.MaxSpeed, 2);

                    ReportingKlmt.AddDataToBindList(new reportKlmt(state, location, initial_time,
                        final_time, interval, distance, general_distance, average_speed, max_speed));
                } // foreach 1

                ReportingKlmt.CreateElementReport();
            } // foreach 2

            ReportingKlmt.CreateAndShowReport();
            ReportingKlmt.DeleteData();
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvKlmtC, true, true);

            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(_mobitel.Mobitel_ID);

            // заголовок для таблиц отчета, для каждого отчета разный
            t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
            t_info.totalWay = _summaries[_mobitel.Mobitel_ID].TotalTravel; // Пройденный путь
            t_info.totalTimerTour = _summaries[_mobitel.Mobitel_ID].TravelTime; // Время в пути
            t_info.totalTimeWay = _summaries[_mobitel.Mobitel_ID].ParkingTime; // Время стоянки
            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            ReportingKlmt.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingKlmt.CreateAndShowReport(gcKlmtC);
        } // ExportToExcelDevExpress

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.DetailReportTravel, e);
            TInfo info = ReportingKlmt.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingKlmt.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingKlmt.GetInfoStructure;
            return (Resources.TotalDistTravel + ": " + String.Format("{0:f2}", info.totalWay) +
                Resources.KM + "\n" +
                Resources.TimeInTraveled + ": " + info.totalTimerTour + "\n" +
                Resources.ParkingTime + ": " + info.totalTimeWay);
        }

        static void showStop(atlantaDataSet.KilometrageReportRow row)
        {
            PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
            string message = row.Interval.ToString();
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeeded(markers);
        }

        void showAllStops()
        {
            List<Marker> markers = new List<Marker>();
            string filterExpression = String.Format("MobitelId={0}", _mobitel.Mobitel_ID);

            foreach (atlantaDataSet.KilometrageReportRow row in _dataset.KilometrageReport.Select(filterExpression))
            {
                if (Resources.Parking == row.State)
                {
                    PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
                    string message = row.Interval.ToString();
                    markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
                } // if
            } // foreach

            ReportsControl.OnMarkersShowNeeded(markers);
        } // showAllStops

        static void showTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow drow in ReportTabControl.Dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (drow.DataGps_ID == row.InitialPointId)
                    inside = true;

                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(drow));

                if (drow.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach

            if (data.Count > 0)
            {
                List<Track> segments = new List<Track>();
                segments.Add(new Track(row.MobitelId, Color.Red, 2f, data));
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // showTrackSegment

        void gvKlmtC_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            redrawMapAndGraph();
        }

        public void Localization()
        {
            ClearStatusLine();

            _colState.Caption = Resources.KilometrageCtrlColState;
            _colLocation.Caption = Resources.Location;
            _colInitialTime.Caption = Resources.InitialTime;
            _colFinalTime.Caption = Resources.EndTime;
            _colInterval.Caption = Resources.DateTimeInterval;
            _colDistance.Caption = Resources.KilometrageCtrlColDistance;
            _colDistFromBegin.Caption = Resources.KilometrageCtrlColDistFromBegin;
            _colAvgSpeed.Caption = Resources.KilometrageCtrlColAvgSpeed;
            colMaxSpeed.Caption = Resources.KilometrageCtrlColMaxSpeed;

            _colState.ToolTip = Resources.KilometrageCtrlColState;
            _colLocation.ToolTip = Resources.Location;
            _colInitialTime.ToolTip = Resources.InitialTime;
            _colFinalTime.ToolTip = Resources.EndTime;
            _colInterval.ToolTip = Resources.DateTimeInterval;
            _colDistance.ToolTip = Resources.KilometrageCtrlColDistance;
            _colDistFromBegin.ToolTip = Resources.KilometrageCtrlColDistFromBegin;
            _colAvgSpeed.ToolTip = Resources.KilometrageCtrlColAvgSpeed;
            colMaxSpeed.ToolTip = Resources.KilometrageCtrlColMaxSpeed;

            //_colDistance.SummaryItem.DisplayFormat = String.Concat(Resources.Total, ": {0:f2}");

            _imageComboRepo.Items[0].Description = global::BaseReports.Properties.Resources.Parking;
            _imageComboRepo.Items[0].Value = global::BaseReports.Properties.Resources.Parking;
            _imageComboRepo.Items[0].ImageIndex = 0;

            _imageComboRepo.Items[1].Description = global::BaseReports.Properties.Resources.Movement;
            _imageComboRepo.Items[1].Value = global::BaseReports.Properties.Resources.Movement;
            _imageComboRepo.Items[1].ImageIndex = 1;

            bbiShowParking.Hint = Resources.ShowParkingOnMap;
        } // Localization
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvKlmtC);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvKlmtC);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcKlmtC);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }
        
        protected void Init()
        {
            bbiShowParking = new BarButtonItem();
            bManager = GetBarManager();
            bManager.Items.Add(bbiShowParking);
            BarC = GetToolBar();
            BarC.LinksPersistInfo.Insert(3, new LinkPersistInfo(BarLinkUserDefines.PaintStyle,
                bbiShowParking, BarItemPaintStyle.CaptionGlyph));
            bbiShowParking.Caption = "";
            bbiShowParking.Glyph = imageCollection.Images[1];
            bbiShowParking.Id = 3;
            bbiShowParking.Name = "bbiShowParking";
            bbiShowParking.Hint = "Показать стоянки на карте";
            bbiShowParking.ItemClick += new ItemClickEventHandler(bbiShowParking_ItemClick);
        } // Init
    } // KilometrageC
} // BaseReports.ReportsDE
