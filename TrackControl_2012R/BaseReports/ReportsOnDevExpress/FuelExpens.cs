#region Using directives
using System;
using System.Linq; 
using BaseReports;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Reports;
using TrackControl.Vehicles;
//using ReportsOnFuelExpenses.Properties;
#endregion

namespace ReportsOnFuelExpenses
{
    /// <summary>
    /// ������������ ������ ������ �� ������� �� ��������� ��������
    /// <para>��� ������� ������� ������� ����������� ������ ������ �������</para>
    /// </summary>
    public class FuelExpens : Fuel //Algorithm, IAlgorithm
    {
        atlantaDataSet.mobitelsRow m_row;

        public FuelExpens()
        {
            m_row = null;
        }

        public override void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            this.m_row = m_row;

            base.SelectItem(m_row);

            if( AtlantaDataSet.TachometerValue.Select( String.Format( "MobitelId={0}", m_row.Mobitel_ID ) ).Length != 0 )
            {
                return;
            }

            atlantaDataSet.sensorsRow sensor = FindSensor( AlgorithmType.ROTATE_E );

            if( sensor == null )
                return;

            Calibrate calibrate = Calibration();

            foreach( GpsData d in GpsDatas )
            {
                atlantaDataSet.TachometerValueRow rotateRow = AtlantaDataSet.TachometerValue.NewTachometerValueRow();
                rotateRow.Id = d.Id;
                rotateRow.MobitelId = m_row.Mobitel_ID;
                rotateRow.SensorId = sensor.id;
                //������������� ��� �������� ���������� c CAN //����� � ���, ��� ������ ������ �� ���������� �� sensor.K ���
                //������� ������� ��������� (��� �� �������������� ��� ��������, ���� � �����), � ��� ����� ������ ���������.
                rotateRow.Value = calibrate.GetUserValue( d.Sensors, sensor.Length, sensor.StartBit, 1 , sensor.B, sensor.S);//sensor.K);
                if( sensor.Length != 1 && rotateRow.Value == ( ( 1 << sensor.Length ) - 1 ) )
                    rotateRow.Value = 0;
                else
                    rotateRow.Value *= sensor.K;
                //
                AtlantaDataSet.TachometerValue.AddTachometerValueRow( rotateRow );
            }
        }

        public TimeSpan CalcTimeEngineOn( int mobitelId, atlantaDataSet.mobitelsRow mobRow )
        {
            if (mobitelId == mobRow.Mobitel_ID)
            {
                SelectItem(mobRow);

                // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
                bool rotateValuesExist =
                    AtlantaDataSet.TachometerValue.Select(String.Format("MobitelId={0}", mobRow.Mobitel_ID)).Length > 0;

                // ������� ������
                //GpsData[] lDataRows1 = DataSetManager.ConvertDataviewRowsToDataGpsArray(
                //    (atlantaDataSet.dataviewRow[])atlantaDataSet.dataview.Select(
                //        String.Format("Mobitel_Id={0}", mobitelId), "time"));
                GpsData[] lDataRows = DataSetManager.GetDataGpsArray(mobRow).OrderBy(gps => gps.Time).ToArray(); 

                TimeSpan TimeStopWithEngineOn = new TimeSpan();

                if (rotateValuesExist)
                {
                    // ����� ������� � ����. ����������, �
                    TimeSpan TimeStopWithEngineOff = GetTimeStopEnginOff(lDataRows, 0, 0);
                    // ����� ������� � ���. ����������, �
                    TimeStopWithEngineOn = BasicMethods.GetTotalStopsTime(lDataRows, 0) - TimeStopWithEngineOff;
                }

                return TimeStopWithEngineOn;
            }

            return new TimeSpan();
        }
    }
}
