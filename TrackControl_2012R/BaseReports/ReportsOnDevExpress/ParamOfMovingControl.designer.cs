﻿namespace BaseReports.ReportsDE
{
    partial class ParamOfMovingControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParamOfMovingControl));
            this.gcParamMoving = new DevExpress.XtraGrid.GridControl();
            this.trafficDetailReportBindingSource = new System.Windows.Forms.BindingSource();
            this.atlantaDataSet = new LocalCache.atlantaDataSet();
            this.gvParamMoving = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDataGpsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescribeEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMaxValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem();
            this.compositeReportLink1 = new DevExpress.XtraPrintingLinks.CompositeLink();
            ((System.ComponentModel.ISupportInitialize)(this.gcParamMoving)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvParamMoving)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink1.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcParamMoving
            // 
            this.gcParamMoving.DataSource = this.trafficDetailReportBindingSource;
            this.gcParamMoving.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcParamMoving.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcParamMoving.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcParamMoving.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcParamMoving.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcParamMoving.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcParamMoving.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcParamMoving.Location = new System.Drawing.Point(0, 26);
            this.gcParamMoving.MainView = this.gvParamMoving;
            this.gcParamMoving.Name = "gcParamMoving";
            this.gcParamMoving.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gcParamMoving.Size = new System.Drawing.Size(780, 399);
            this.gcParamMoving.TabIndex = 9;
            this.gcParamMoving.UseEmbeddedNavigator = true;
            this.gcParamMoving.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvParamMoving,
            this.gridView2});
            // 
            // trafficDetailReportBindingSource
            // 
            this.trafficDetailReportBindingSource.DataMember = "TrafficDetailReport";
            this.trafficDetailReportBindingSource.DataSource = this.atlantaDataSet;
            // 
            // atlantaDataSet
            // 
            this.atlantaDataSet.DataSetName = "atlantaDataSet";
            this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvParamMoving
            // 
            this.gvParamMoving.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvParamMoving.Appearance.Empty.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvParamMoving.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvParamMoving.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvParamMoving.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvParamMoving.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvParamMoving.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvParamMoving.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvParamMoving.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvParamMoving.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvParamMoving.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvParamMoving.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvParamMoving.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvParamMoving.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvParamMoving.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvParamMoving.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvParamMoving.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvParamMoving.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvParamMoving.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvParamMoving.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.GroupRow.Options.UseFont = true;
            this.gvParamMoving.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvParamMoving.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvParamMoving.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvParamMoving.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvParamMoving.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvParamMoving.Appearance.OddRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvParamMoving.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.Preview.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.Preview.Options.UseForeColor = true;
            this.gvParamMoving.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvParamMoving.Appearance.Row.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvParamMoving.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvParamMoving.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvParamMoving.Appearance.VertLine.Options.UseBackColor = true;
            this.gvParamMoving.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvParamMoving.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvParamMoving.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvParamMoving.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvParamMoving.ColumnPanelRowHeight = 40;
            this.gvParamMoving.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDataGpsID,
            this.colLocation,
            this.colDescribeEvent,
            this.colTime,
            this.colDuration,
            this.colMaxValue,
            this.colMiddleValue});
            this.gvParamMoving.GridControl = this.gcParamMoving;
            this.gvParamMoving.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvParamMoving.Name = "gvParamMoving";
            this.gvParamMoving.OptionsDetail.AllowZoomDetail = false;
            this.gvParamMoving.OptionsDetail.EnableMasterViewMode = false;
            this.gvParamMoving.OptionsDetail.ShowDetailTabs = false;
            this.gvParamMoving.OptionsDetail.SmartDetailExpand = false;
            this.gvParamMoving.OptionsSelection.MultiSelect = true;
            this.gvParamMoving.OptionsView.EnableAppearanceEvenRow = true;
            this.gvParamMoving.OptionsView.EnableAppearanceOddRow = true;
            this.gvParamMoving.OptionsView.ShowFooter = true;
            // 
            // colDataGpsID
            // 
            this.colDataGpsID.Caption = "DataGpsID";
            this.colDataGpsID.FieldName = "DataGps_ID";
            this.colDataGpsID.Name = "colDataGpsID";
            // 
            // colLocation
            // 
            this.colLocation.AppearanceCell.Options.UseTextOptions = true;
            this.colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.Caption = "Местоположение";
            this.colLocation.FieldName = "Location";
            this.colLocation.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.ToolTip = "Местоположение";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 0;
            this.colLocation.Width = 100;
            // 
            // colDescribeEvent
            // 
            this.colDescribeEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colDescribeEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescribeEvent.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDescribeEvent.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescribeEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescribeEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescribeEvent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescribeEvent.Caption = "Событие";
            this.colDescribeEvent.FieldName = "DescribeEvent";
            this.colDescribeEvent.Name = "colDescribeEvent";
            this.colDescribeEvent.OptionsColumn.AllowEdit = false;
            this.colDescribeEvent.OptionsColumn.AllowFocus = false;
            this.colDescribeEvent.OptionsColumn.ReadOnly = true;
            this.colDescribeEvent.ToolTip = "Событие";
            this.colDescribeEvent.Visible = true;
            this.colDescribeEvent.VisibleIndex = 1;
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.Caption = "Дата, время";
            this.colTime.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTime.FieldName = "Time";
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.AllowEdit = false;
            this.colTime.OptionsColumn.AllowFocus = false;
            this.colTime.OptionsColumn.ReadOnly = true;
            this.colTime.ToolTip = "Дата, время";
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 2;
            this.colTime.Width = 95;
            // 
            // colDuration
            // 
            this.colDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.Caption = "Длительность";
            this.colDuration.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDuration.FieldName = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.ToolTip = "Длительность";
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 3;
            this.colDuration.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // colMaxValue
            // 
            this.colMaxValue.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMaxValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxValue.Caption = "Максимальное значение";
            this.colMaxValue.DisplayFormat.FormatString = "N2";
            this.colMaxValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxValue.FieldName = "MaxValue";
            this.colMaxValue.Name = "colMaxValue";
            this.colMaxValue.OptionsColumn.AllowEdit = false;
            this.colMaxValue.OptionsColumn.AllowFocus = false;
            this.colMaxValue.OptionsColumn.ReadOnly = true;
            this.colMaxValue.ToolTip = "Максимальное значение";
            this.colMaxValue.Visible = true;
            this.colMaxValue.VisibleIndex = 4;
            this.colMaxValue.Width = 125;
            // 
            // colMiddleValue
            // 
            this.colMiddleValue.AppearanceCell.Options.UseTextOptions = true;
            this.colMiddleValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMiddleValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMiddleValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMiddleValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colMiddleValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMiddleValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMiddleValue.Caption = "Среднее значение";
            this.colMiddleValue.DisplayFormat.FormatString = "N2";
            this.colMiddleValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMiddleValue.FieldName = "MiddleValue";
            this.colMiddleValue.Name = "colMiddleValue";
            this.colMiddleValue.OptionsColumn.AllowEdit = false;
            this.colMiddleValue.OptionsColumn.AllowFocus = false;
            this.colMiddleValue.OptionsColumn.ReadOnly = true;
            this.colMiddleValue.ToolTip = "Среднее значение";
            this.colMiddleValue.Visible = true;
            this.colMiddleValue.VisibleIndex = 5;
            this.colMiddleValue.Width = 125;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcParamMoving;
            this.gridView2.Name = "gridView2";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink1});
            // 
            // compositeReportLink1
            // 
            // 
            // 
            // 
            this.compositeReportLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink1.ImageCollection.ImageStream")));
            this.compositeReportLink1.Landscape = true;
            this.compositeReportLink1.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink1.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink1.PrintingSystem = this.printingSystem1;
            this.compositeReportLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // ParamOfMovingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gcParamMoving);
            this.Name = "ParamOfMovingControl";
            this.Size = new System.Drawing.Size(780, 449);
            this.Controls.SetChildIndex(this.gcParamMoving, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcParamMoving)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvParamMoving)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink1.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcParamMoving;
        private DevExpress.XtraGrid.Views.Grid.GridView gvParamMoving;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colDescribeEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private LocalCache.atlantaDataSet atlantaDataSet;
        private System.Windows.Forms.BindingSource trafficDetailReportBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink1;
        private DevExpress.XtraGrid.Columns.GridColumn colDataGpsID;
    }
}
