﻿using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;

namespace Report
{
    public struct TEventInfo
    {
        // описать поля журнала
        //private DateTime periodBeging;
        //private DateTime periodEnd;
    }

    // структура данных для заголовка таблицы по машине, для отчетов Пробеги
    public struct TInfo
    {
        public string nameTitle;

        public string TypeFuel; // вид используемого топлива
        public string OutLinkIdFuel; // Код внешней базы

        public DateTime periodBeging;
        // период времени по каждой машине может отличаться
        public DateTime periodEnd;
        public string infoVehicle;
        // Транспорт
        public string infoDriverName;
        // Водитель транспорта
        public double totalWay;
        // Общий пройденный путь
        public TimeSpan totalTimerTour;
        // Общая продолжительность смены
        public TimeSpan totalTimeWay;
        // Общее время в пути
        public TimeSpan totalTimeStops;
        public TimeSpan timeOverSpeedLimit;
        // time over speed
        public int numAcceleration;
        // acceleration
        public int numBreak;
        // break
        public TimeSpan totalParking;
        public float passCountAvg;
        public uint totalPassCount;
        public double fuelerRate;
        public double fuelerLiter;
        // Общий пробег внутри контрольной зоны
        public double totalWayInKZ;
        // Общий пробег вне контрольной зоны
        public double totalWayWithoutKZ;
        // Код внешней базы
        public string OutLinkId;

        public double distance;
        public double totalfuel; // Общий расход топлива
        public double fuelavg; // Расход в л/100км
        public double fuelavgnorma; // Норма расхода в л/100км
        public double deviation;
        public string infoAggregateName; // название агрегата транспортного средства
    }

    // TInfo
    // структура данных для заголовка таблицы по машине, для отчетов ДУТ
    public struct TInfoDut
    {
        public DateTime periodBeging;
        // период времени по каждой машине может отличаться
        public DateTime periodEnd;
        public string infoVehicle;
        // Транспорт
        public string infoDriverName;
        // Водитель транспорта
        public string registerNumber;
        // Регистрационный номер транспорта
        public double totalWay;
        // Общий пройденный путь
        public TimeSpan totalTimerTour;
        // Общая продолжительность смены
        public TimeSpan totalTimeWay;
        // // Общее время в пути
        public TimeSpan totalStops;
        public TimeSpan totalEngineOn;
        // Общее время работы со включенным двигателем, ч
        public double totalFuel;
        // Общий расход топлива, л
        public double totalFuelAdd;
        // Всего заправлено, л
        public double totalFuelSub;
        // Всего слито, л
        public double MiddleFuel;
        // Средний расход топлива, л/100 км
        public double MotoHour;
        public double MiddleFuelStops;
        public double FuelStops;
        public double MotionTreavelTime;
        public double Fueling;
        public string markVehicle { get; set; }
        public int Identification { get; set; }
        public bool BreakLeft { get; set; }
        public bool BreakMeedle { get; set; }
        public bool BreakRight { get; set; }
    } // TInfoDut

    internal struct PlaceAndSize
    {
        public float X;
        public float Y;
        public float Width;
        public float Height;
    }

    // PlaceAndSize
    // Базовый клас построения отчетов, для всех отчетов одинаковый
    /* тип Т0 - структура данных таблицы отчета */
    /* тип Т1 - структура данных заголовка таблицы отчета */

    public class ReportBase<T0, T1>
    {
        private ReportClass<T1> _reportClass;
        private List<GridControl> listControl;
        private BindingList<T0> gridDataList;
        private List<BindingList<T0>> ListGrids;
        private GridControl gControl;
        private GridView gView;
        private GridView baseGridView;
        private Control.ControlCollection Controls;
        private PrintingSystem printSys;
        private GridView dataGridView;
        private List<string> listNumberCol;
        private int numControl;

        // конструктор
        public ReportBase(Control.ControlCollection AppCtrl, CompositeLink compositeRepLink, PrintingSystem prn,
            GridView dgv, List<string> listNumCol)
        {
            _reportClass = new ReportClass<T1>(compositeRepLink);
            listControl = new List<GridControl>();
            ListGrids = new List<BindingList<T0>>();
            Controls = AppCtrl;
            printSys = prn;
            dataGridView = dgv;
            listNumberCol = listNumCol;
        }

        // конструктор
        public ReportBase(Control.ControlCollection AppCtrl, CompositeLink compositeRepLink,
            GridView appGView, ReportClass<T1>.GetStrBreackLeft getStrBrckLeft,
            ReportClass<T1>.GetStrBreackRight getStrBrckRight,
            ReportClass<T1>.GetStrBreackUp getStrBrckUp)
        {
            _reportClass = new ReportClass<T1>(compositeRepLink);
            listControl = new List<GridControl>();
            ListGrids = new List<BindingList<T0>>();
            baseGridView = appGView;
            Controls = AppCtrl;
            numControl = 0;

            _reportClass.getStrBrckLeft =
                new ReportClass<T1>.GetStrBreackLeft(getStrBrckLeft);

            _reportClass.getStrBrckRight =
                new ReportClass<T1>.GetStrBreackRight(getStrBrckRight);

            _reportClass.getStrBrckUp =
                new ReportClass<T1>.GetStrBreackUp(getStrBrckUp);
        }

        // ReportBase
        public void CreateBindDataList()
        {
            gridDataList = new BindingList<T0>();
            ListGrids.Add(gridDataList);
        }

        public void AddLink(PrintableComponentLink link)
        {
            _reportClass.AddLink(link);
        }

        public void ClearLink()
        {
            _reportClass.ClearComposeLink();
        }

        // установить и настроить колонки таблиц отчета
        protected void SetColumnsOtherTables()
        {
            if (gView == null)
                return;

            if (dataGridView == null)
                return;

            if (gView.Columns.Count != listNumberCol.Count)
                return;

            for (int j = 0; j < listNumberCol.Count; j++)
            {
                gView.Columns[j].Caption = dataGridView.Columns[listNumberCol[j]].Caption;
            }
        }

        // SetColumntOtherTable
        protected void SetColumnsForTables()
        {
            if (gView == null)
                return;

            if (baseGridView == null)
                return;

            try
            {
                for (int j = 0; j < baseGridView.Columns.Count; j++)
                {
                    gView.Columns[j].AbsoluteIndex = baseGridView.Columns[j].AbsoluteIndex;
                    gView.Columns[j].AppearanceCell.TextOptions.HAlignment =
                        baseGridView.Columns[j].AppearanceCell.TextOptions.HAlignment;
                    gView.Columns[j].AppearanceCell.TextOptions.VAlignment =
                        baseGridView.Columns[j].AppearanceCell.TextOptions.VAlignment;
                    gView.Columns[j].AppearanceCell.TextOptions.WordWrap =
                        baseGridView.Columns[j].AppearanceCell.TextOptions.WordWrap;
                    gView.Columns[j].AppearanceHeader.TextOptions.HAlignment =
                        baseGridView.Columns[j].AppearanceHeader.TextOptions.HAlignment;
                    gView.Columns[j].AppearanceHeader.TextOptions.VAlignment =
                        baseGridView.Columns[j].AppearanceHeader.TextOptions.VAlignment;
                    gView.Columns[j].AppearanceHeader.TextOptions.WordWrap =
                        baseGridView.Columns[j].AppearanceHeader.TextOptions.WordWrap;
                    gView.Columns[j].Visible = baseGridView.Columns[j].Visible;
                    gView.Columns[j].Caption = baseGridView.Columns[j].Caption;
                    gView.Columns[j].DisplayFormat.FormatString = baseGridView.Columns[j].DisplayFormat.FormatString;
                    gView.Columns[j].DisplayFormat.FormatType = baseGridView.Columns[j].DisplayFormat.FormatType;
                    gView.Columns[j].ColumnEdit = baseGridView.Columns[j].ColumnEdit;
                    gView.Columns[j].VisibleIndex = baseGridView.Columns[j].VisibleIndex;
                    gView.Columns[j].GroupIndex = baseGridView.Columns[j].GroupIndex;
                    gView.Columns[j].GroupInterval = baseGridView.Columns[j].GroupInterval;
                    gView.Columns[j].SortIndex = baseGridView.Columns[j].SortIndex;
                    gView.Columns[j].SortMode = baseGridView.Columns[j].SortMode;
                    gView.Columns[j].SortOrder = baseGridView.Columns[j].SortOrder;
                    gView.Columns[j].FilterMode = baseGridView.Columns[j].FilterMode;
                    gView.Columns[j].FilterInfo = baseGridView.Columns[j].FilterInfo;
                } // for
            }
            catch(Exception e)
            {
                // to do
            }

            // additionals options for view control
            gView.Appearance.HeaderPanel.BackColor = baseGridView.Appearance.HeaderPanel.BackColor;
            gView.Appearance.HeaderPanel.ForeColor = baseGridView.Appearance.HeaderPanel.ForeColor;
            gView.Appearance.HeaderPanel.BackColor2 = baseGridView.Appearance.HeaderPanel.BackColor2;
            gView.Appearance.HeaderPanel.TextOptions.HAlignment =
                baseGridView.Appearance.HeaderPanel.TextOptions.HAlignment;
            gView.Appearance.HeaderPanel.TextOptions.VAlignment =
                baseGridView.Appearance.HeaderPanel.TextOptions.VAlignment;
            gView.Appearance.HeaderPanel.TextOptions.WordWrap = baseGridView.Appearance.HeaderPanel.TextOptions.WordWrap;
            gView.ColumnPanelRowHeight = baseGridView.ColumnPanelRowHeight;

            gView.AppearancePrint.HeaderPanel.TextOptions.HAlignment = HorzAlignment.Center;
            gView.AppearancePrint.HeaderPanel.TextOptions.VAlignment = VertAlignment.Center;
            gView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = WordWrap.Wrap;
            gView.OptionsView.ShowGroupPanel = baseGridView.OptionsView.ShowGroupPanel;
            gView.GroupCount = baseGridView.GroupCount;
            gView.ActiveFilterString = baseGridView.ActiveFilterString;
            gView.BeforePrintRow += gView_BeforePrintRow;
        }

        void gView_BeforePrintRow(object sender, DevExpress.XtraGrid.Views.Printing.CancelPrintRowEventArgs e)
        {
            GridView view = sender as GridView;
            int hndl = e.RowHandle;
            int countRow = 0;
            TimeSpan duration = new TimeSpan();
            string GroupText = "";

            if (hndl < 0 && hndl > -2147483648)
            {
                e.Cancel = true;
                GroupText = view.GetGroupRowDisplayText(hndl);
                int count = view.GetChildRowCount(hndl);
                for (int i = 0; i < count; i++)
                {
                    int handle = view.GetChildRowHandle(hndl, i);
                    if (string.Equals(view.GetRowCellValue(handle, "State"), Resources.Parking))
                    {
                        countRow++;
                        string period = view.GetRowCellValue(handle, "Interval").ToString();
                        TimeSpan speriod = TimeSpan.Parse(period);
                        duration += speriod;
                    }
                } // for

                GroupText += ". " + Resources.KilometrageTotalNumber + countRow + ". ";
                GroupText += Resources.KilometrageNumberStops + duration;

                TextBrick tb = e.PS.CreateTextBrick() as TextBrick;
                tb.Text = GroupText;
                tb.Font = new Font(tb.Font, FontStyle.Bold);
                tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
                tb.Padding = new PaddingInfo(5, 0, 0, 0);
                tb.BackColor = Color.LightGray;
                tb.ForeColor = Color.Black;
                SizeF clientPageSize = (e.BrickGraphics as BrickGraphics).ClientPageSize;
                float textBrickHeight = ((GridViewInfo) view.GetViewInfo()).CalcRowHeight(e.Graphics, e.RowHandle,
                    e.Level);
                RectangleF textBrickRect = new RectangleF(0, e.Y, (int) clientPageSize.Width, textBrickHeight);
                e.BrickGraphics.DrawBrick(tb, textBrickRect);
                e.Y += (int) textBrickHeight;
            }
        }

        // SetColumnsForTables
        // прибираем за собой, остальное сделает сборщик мусора
        protected void ClearGridDataList()
        {
            if (ListGrids != null)
            {
                for (int i = 0; i < ListGrids.Count; i++)
                {
                    ListGrids[i].Clear();
                }
            }
        }

        // ClearVariable
        // удалить контролы таблиц
        // отсоединить от контрола приложения
        protected void DeleteGridControls()
        {
            if (listControl == null)
                return;

            for (int i = 0; i < listControl.Count; i++)
                Controls.Remove(listControl[i]);

            listControl.Clear();

            numControl = 0;
        }

        // DeleteGridControls
        public void DeleteData()
        {
            DeleteGridControls();
            ClearGridDataList();
            ClearComposeLink();
            _reportClass.ClearInfoList();
        }

        // экспортировать в excel - файл
        public void ExportDataToExcel(string fileName)
        {
            _reportClass.ExportReportToExcel(fileName, printSys);
        }

        // создать и показать отчет
        public void CreateAndShowReport()
        {
            _reportClass.CreateAndShowReport();
        }

        // объединить элементы страницы в общий отчет
        protected void SetComposeLink()
        {
            _reportClass.SetComposeLink();
        }

        // объединить элементы страницы в общий отчет
        protected void SetComposeOtherLink()
        {
            _reportClass.SetComposeOtherLink();
        }

        // удалить элементы страницы из общего отчета
        protected void ClearComposeLink()
        {
            _reportClass.ClearComposeLink();
        }

        /* создать новый GridControl и связать с источником данных */

        protected void CreateNewGridControl()
        {
            gControl = new GridControl();
            gView = new GridView();

            gView.OptionsView.ColumnAutoWidth = true;
            gView.ColumnPanelRowHeight = 40;
            gView.GridControl = gControl;
            gView.Appearance.OddRow.BackColor = Color.WhiteSmoke;
            gView.Appearance.OddRow.Options.UseBackColor = true;
            gView.OptionsView.EnableAppearanceOddRow = true;
            gView.Appearance.VertLine.BackColor = Color.LightGray;
            gView.Appearance.VertLine.Options.UseBackColor = true;
            gView.Appearance.HorzLine.BackColor = Color.LightGray;
            gView.Appearance.HorzLine.Options.UseBackColor = true;
            gView.Appearance.Row.BackColor = Color.White;
            gView.Appearance.Row.Options.UseBackColor = true;
            gView.Appearance.RowSeparator.BackColor = Color.Gray;
            gView.Appearance.RowSeparator.Options.UseBackColor = true;
            gView.Appearance.HeaderPanel.BackColor = Color.Silver;
            gView.Appearance.HeaderPanel.BackColor2 = Color.LightGray;
            gView.Name = "gView" + numControl;

            gControl.MainView = gView;
            gControl.Visible = false;
            gControl.Name = "gControl" + numControl;
            numControl++;

            gControl.ViewCollection.AddRange(new BaseView[] {gView});
            Controls.Add(gControl); // соединить с контролом приложения
            listControl.Add(gControl); // сохранить в списке

            XtraGridService.SetupGidViewForPrint(gView, true, true);

            // подставить структуры данных для отчета
            gControl.DataSource = gridDataList;
        }

        // CreateNewGridControl
        // создать элементы отчета
        public void CreateElementOtherReport()
        {
            CreateNewGridControl();
            InitialColumns();
            SetColumnsOtherTables();
            CreateNewOtherLinks(gControl);
            SetComposeOtherLink();
        }

        // CreateElementReport
        // создать элементы отчета
        public void CreateElementReport()
        {
            CreateNewGridControl();
            InitialColumns();
            SetColumnsForTables();
            CreateNewLinks(gControl);
            SetComposeLink();
        }

        // CreateElementReport
        // настройка колонок GridControl
        protected void InitialColumns()
        {
            for (int i = 0; i < gView.Columns.Count; i++)
            {
                gView.Columns[i].AppearanceCell.TextOptions.HAlignment =
                    DevExpress.Utils.HorzAlignment.Center;
                gView.Columns[i].AppearanceCell.TextOptions.VAlignment =
                    DevExpress.Utils.VertAlignment.Center;
                gView.Columns[i].AppearanceCell.TextOptions.WordWrap =
                    DevExpress.Utils.WordWrap.Wrap;

                gView.Columns[i].AppearanceHeader.TextOptions.HAlignment =
                    DevExpress.Utils.HorzAlignment.Center;
                gView.Columns[i].AppearanceHeader.TextOptions.WordWrap =
                    DevExpress.Utils.WordWrap.Wrap;
            } // for
        }

        // InitialColumns
        public void SetRectangleBrckUP(float x, float y, float w, float h)
        {
            _reportClass.SetRectangleBrckUP(x, y, w, h);
        }

        public void SetRectangleBrckLetf(float x, float y, float w, float h)
        {
            _reportClass.SetRectangleBrckLetf(x, y, w, h);
        }

        public void SetRectangleBrckRight(float x, float y, float w, float h)
        {
            _reportClass.SetRectangleBrckRight(x, y, w, h);
        }

        // создает отчет по одной заранее подготовленной таблице
        public void CreateAndShowReport(GridControl gCtrl)
        {
            _reportClass.CreateNewLinks(gCtrl);
            _reportClass.SetComposeLink();
            _reportClass.CreateAndShowReport();
            DeleteData();
        }

        // CreateAndShowReport
        // создать новые части страниц отчета
        private void CreateNewOtherLinks(GridControl gCtrl)
        {
            _reportClass.CreateNewOtherLinks(gCtrl);
        }

        // CreateNewLinks
        // создать новые части страниц отчета
        private void CreateNewLinks(GridControl gCtrl)
        {
            _reportClass.CreateNewLinks(gCtrl);
        }

        // CreateNewLinks
        /* добавить заголовок таблицы отчета в список */

        public void AddInfoStructToList(T1 inflist)
        {
            _reportClass.AddInfoStructToList(inflist);
        }

        // добавить таблицу в список таблиц отчета
        public void AddDataToBindList(T0 data)
        {
            gridDataList.Add(data);
        }

        /* вернуть данные заголовка таблицы отчета */
        public T1 GetInfoStructure
        {
            get
            {
                return _reportClass.GetInfoStructure;
            }
        }
    } // ReportBase
} // Report