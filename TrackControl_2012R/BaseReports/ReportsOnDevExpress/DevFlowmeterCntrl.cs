﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.Reports.Graph;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid.Views;
using Report;

namespace BaseReports.ReportsDE
{
    [ToolboxItem( false )]
    public partial class DevFlowmeterCntrl : BaseReports.ReportsDE.BaseControl
    {
        public class reportFlowMeter
        {
            string location;
            DateTime begin;
            DateTime end; 
            TimeSpan _duration;
            double _travel;
            double fuel_rate;
            double idle_fuel;
            double move_fuel;

            public reportFlowMeter(string location, DateTime begin, DateTime end,
                TimeSpan _duration, double _travel, double fuel_rate, double idle_fuel,
               double move_fuel)
            {
                this.location = location;
                this.begin = begin;
                this.end = end;
                this._duration = _duration;
                this._travel = _travel;
                this.fuel_rate = fuel_rate;
                this.idle_fuel = idle_fuel;
                this.move_fuel = move_fuel;
            } // reportFlowMeter

            public string Location
            {
                get { return location; }
            }

            public DateTime beginTime
            {
                get { return begin; } 
            }

            public DateTime endTime
            {
                get { return end; }
            }

            public TimeSpan duration
            {
                get { return _duration; }
            }

            public double travel
            {
                get { return _travel; }
            }

            public double fuelRate
            {
                get { return fuel_rate; }
            }

            public double idleFuelRate
            {
                get { return idle_fuel; }
            }

            public double moveFuelRate
            {
                get { return move_fuel; }
            }
        } // reportFlowMeter

        private FlowMeter fuelRateAlg;
        // Итоги с группировкой по телетрекам
        private IDictionary<int, FuelRateTotal> totals;
        // Интерфейс для построение графиков по SeriesL
        private static IBuildGraphs buildGraph;
        protected VehicleInfo vehicleInfo;
        protected static atlantaDataSet dataset;
        ReportBase<reportFlowMeter, TInfoDut> ReportingFlowMeter;
        TimeSpan timeTravel;
        TimeSpan timeStoping;

        public DevFlowmeterCntrl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gvFlowMeter, gcFlowMeter, bar3);

            fuelRateAlg = FlowMeter.GetFlowMeter();
            totals = new Dictionary<int, FuelRateTotal>();

            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new Rotation());
            AddAlgorithm(fuelRateAlg);
            //AddAlgorithm(new FlowMeter(AlgorithmType.FUEL2));

            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "flowmeterReport";
            gcFlowMeter.DataSource = atlantaDataSetBindingSource;
            atlantaDataSet = null;
            buildGraph = new BuildGraphs();
            timeTravel = new TimeSpan(0);
            timeStoping = new TimeSpan(0);

            gvFlowMeter.RowClick += new RowClickEventHandler(gvFlowMeter_RowClick);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFlowMeter =
                new ReportBase<reportFlowMeter, TInfoDut>(Controls, compositeReportLink, gvFlowMeter,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevFlowmeterCntrl

        public override void  Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                ClearStatusLine();

                curMrow = m_row;
                vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);

                if ((m_row.GetdataviewRows() == null) || (m_row.GetdataviewRows().Length == 0) ||
                    (m_row.GetflowmeterReportRows().Length == 0))
                {
                    DisableButton();
                    ClearStatusLine();
                    gcFlowMeter.DataSource = null;

                    return;
                }

                FuelRateTotal frTotal = GetTotals(m_row);

                if (frTotal != null)
                {
                    _travelLabel.Caption = Resources.DistanceText + ": " + frTotal.TotalDistance.ToString("0.##"); // Пройденный путь
                    _timeTravelLabel.Caption = Resources.TotalTravelTimeText + ": " + String.Format("{0} ({1})", 
                        frTotal.TotalTimeMotion, frTotal.TotalTimeMotionWithoutShortStops); // Время в пути
                    _stopTimeLabel.Caption = Resources.FlowmeterCtrlParkingTime + " " + String.Format("{0} ({1})", 
                        frTotal.TotalTimeStops, frTotal.TotalTimeStopsWithShortStops); // Время стоянки 
                    //_idleFuelRateLabel.Caption = frTotal.TotalFuelRateOnHour.ToString("0.##"); // Расход средний 
                    //_moveFuelRateLabel.Caption = frTotal.TotalFuelRateOnHundred.ToString("0.##"); // Расход
                    _flowmeterLabel.Caption = Resources.TotalLiters + ": " + frTotal.TotalFuelRate.ToString("0.##"); // Всего топлива
                    timeTravel = frTotal.TotalTimeMotion;
                    timeStoping = frTotal.TotalTimeStops;
                } // if

                gcFlowMeter.DataSource = atlantaDataSetBindingSource;

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    EnableButton();
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                    gcFlowMeter.DataSource = null;
                }
            } // if
            else
            {
                DisableButton();
                ReportsControl.GraphClearSeries();
                Graph.ClearLabel();
            } // else
        } // Select

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is FlowMeter)
            {
                Select(curMrow);
            }
        }

        // итоги по данному телетреку
        public FuelRateTotal GetTotals(atlantaDataSet.mobitelsRow m_row)
        {
            if (totals.ContainsKey(m_row.Mobitel_ID))
            {
                return totals[m_row.Mobitel_ID];
            }

            FuelRateTotal fuelRateTotal = fuelRateAlg.GetFuelRateTotal(m_row);

            if (fuelRateTotal.FuelRateSensor1 != null) //Проверяет был ли уже построен отчет по данному ТТ
            {
                PartialAlgorithms partialAlg = new PartialAlgorithms();
                partialAlg.SelectItem(m_row);
                GpsData[] d_rows = DataSetManager.ConvertDataviewRowsToDataGpsArray(m_row.GetdataviewRows());
                fuelRateTotal.TotalDistance = Math.Round(m_row.path, 2);
                fuelRateTotal.TotalTimeMotionWithoutShortStops = BasicMethods.GetTotalMotionTime(d_rows);
                fuelRateTotal.TotalTimeStopsWithShortStops = BasicMethods.GetTotalStopsTime(d_rows, 0);

                fuelRateTotal.TotalFuelRateOnHour = ((partialAlg.GetFuelUseInMotionDrt(d_rows, 0) +
                    partialAlg.GetFuelUseInStopDrt(d_rows, 0, 0)) / (fuelRateTotal.TotalTimeMotion +
                    (BasicMethods.GetTotalStopsTime(d_rows, 0) - 
                    partialAlg.GetTimeStopEnginOff(d_rows, 0, 0))).TotalHours);
                fuelRateTotal.TotalFuelRateOnHour = Math.Round(fuelRateTotal.TotalFuelRateOnHour, 2);

                fuelRateTotal.TotalFuelRateOnHundred = Math.Round(m_row.motionFuelRate, 2);
                fuelRateTotal.TotalFuelRate = Math.Round(m_row.summFuelRate, 2);

                foreach (DataRowView dr in atlantaDataSetBindingSource)
                {
                    atlantaDataSet.flowmeterReportRow fRow = (atlantaDataSet.flowmeterReportRow) dr.Row;

                    if (fRow.Location == Resources.Movement)
                    {
                        fuelRateTotal.TotalTimeMotion += fRow.duration;
                    }
                    else
                    {
                        fuelRateTotal.TotalTimeStops += fRow.duration;
                    }
                } // foreach

                totals.Add(m_row.Mobitel_ID, fuelRateTotal);
            } // if
            else
            {
                fuelRateTotal = null;
            }
            return fuelRateTotal;
        } // GetTotals

        public void SelectGraphic(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);

                try
                {
                    if (m_row.GetdataviewRows() == null || m_row.GetdataviewRows().Length == 0)
                    {
                        return;
                    }

                    ReportsControl.GraphClearSeries();
                    Graph. ClearRegion();

                    buildGraph.AddGraphFlowmeter(graph, dataset, m_row);

                    // Graph.ShowSeries(vehicleInfo.Info);
                    ReportsControl.ShowGraph(curMrow);
                } // try
                catch (Exception ex)
                {

                    throw new NotImplementedException("Exception in FlowmeterControl: " + ex.Message);
                }
            } // if
        } // SelectGraphic

        // Очищает данные отчета
        public override void ClearReport()
        {
            dataset.flowmeterReport.Clear();
            totals.Clear();
        }

        // кнопка построение отчета
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;
                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in Dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    }
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";

                Select(curMrow);
                SelectGraphic(curMrow);
                SetStartButton();
            } // if
            else
            {
                SetStartButton();
                StopReport();
                _stopRun = true;

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun) 
                    return;
            }
            Application.DoEvents();
        } // SelctItem

        // Обрабатывает клик на кнопке "Показать на графике"
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (sender == null)
                return;

            ReportsControl.OnGraphShowNeeded();
            ShowDataOnGraph();
        } // bbiShowOnGraf_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnMap();
        }

        private void ShowTrackOnMap()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();
            Int32[] selected_row = gvFlowMeter.GetSelectedRows();

            DataRow[] datRow = dataset.KilometrageReport.Select("MobitelId=" + curMrow.Mobitel_ID, "Id ASC");

            if (datRow.Length > 0)
            {

                for (int i = 0; i < selected_row.Length; i++)
                {
                    atlantaDataSet.KilometrageReportRow nRow = (atlantaDataSet.KilometrageReportRow)datRow[selected_row[i]];
                    segments.Add(GetTrackSegment(nRow));
                }

                if (segments.Count > 0)
                    ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // ShowTrackOnGraph

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;
                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach
            return new Track(row.MobitelId, Color.Red, 2f, data);
        } // GetTrackSegment
        
        protected void gvFlowMeter_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                ShowTrackOnMap();
                ShowDataOnGraph();
            }
        }

        protected void ShowDataOnGraph()
        {
            Graph.ClearRegion();
            Graph.ClearLabel();

            Int32[] selectRow = gvFlowMeter.GetSelectedRows();

            for (int i = 0; i < selectRow.Length; i++)
            {
                atlantaDataSet.flowmeterReportRow f_row = (atlantaDataSet.flowmeterReportRow)
                  ((DataRowView)atlantaDataSetBindingSource.List[selectRow[i]]).Row;

                if (f_row != null)
                {
                    Graph.AddTimeRegion(f_row.beginTime, f_row.endTime); // выделяем цветом
                }
            } // for

            Graph.SellectZoom();
        } // ShowDataOnGraph

        // формирование отчета одностраничного по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvFlowMeter, true, true);

            TInfoDut info = new TInfoDut();

            info.periodBeging = Algorithm.Period.Begin;
            info.periodEnd = Algorithm.Period.End;

            info.infoVehicle = vehicleInfo.Info; // автомобиль
            info.infoDriverName = vehicleInfo.DriverFullName; // водитель

            info.totalWay = Math.Round(curMrow.path, 2); // пройденный путь
            info.totalTimeWay = timeTravel; // время в пути
            info.totalStops = timeStoping; // время стоянок

            if (double.IsNaN(curMrow.idleFuelRate))
            {
                info.MotoHour = 0.0; // моточасы
            }
            else
            {
                info.MotoHour = Math.Round(curMrow.idleFuelRate, 2);
            }

            if (double.IsNaN(curMrow.motionFuelRate))
            {
                info.totalFuelSub = 0.0; // расход топлива
            }
            else
            {
                info.totalFuelSub = Math.Round(curMrow.motionFuelRate, 2); // расход топлива
            }

            info.totalFuel = Math.Round(curMrow.summFuelRate, 2); // всего топлива

            ReportingFlowMeter.AddInfoStructToList(info);
            ReportingFlowMeter.CreateAndShowReport(gcFlowMeter);
        } // ExportToExcelDevExpress

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_row in Dataset.mobitels)
            {
                if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                {
                    atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", 
                        m_row.Mobitel_ID);

                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        TInfoDut info = new TInfoDut();
                        info.totalTimeWay = new TimeSpan(0);
                        info.totalStops = new TimeSpan(0);

                        VehicleInfo v_info = new VehicleInfo(m_row.Mobitel_ID);

                        FuelRateTotal frTotal = GetTotals(m_row);

                        if (frTotal != null)
                        {
                            info.totalTimeWay = frTotal.TotalTimeMotion;
                            info.totalStops = frTotal.TotalTimeStops;
                        } // if

                        info.periodBeging = Algorithm.Period.Begin;
                        info.periodEnd = Algorithm.Period.End;

                        info.infoVehicle = v_info.Info; // автомобиль
                        info.infoDriverName = v_info.DriverFullName; // водитель

                        info.totalWay = Math.Round(m_row.path, 2); // пройденный путь

                        if (double.IsNaN(m_row.idleFuelRate))
                        {
                            info.MotoHour = 0.0; // моточасы
                        }
                        else
                        {
                            info.MotoHour = Math.Round(m_row.idleFuelRate, 2);
                        }

                        if (double.IsNaN(m_row.motionFuelRate))
                        {
                            info.totalFuelSub = 0.0; // расход топлива
                        }
                        else
                        {
                            info.totalFuelSub = Math.Round(m_row.motionFuelRate, 2);
                        }

                        info.totalFuel = Math.Round(m_row.summFuelRate, 2); // всего топлива

                        ReportingFlowMeter.AddInfoStructToList(info); /* формируем заголовки таблиц отчета */
                        ReportingFlowMeter.CreateBindDataList(); // создать новый список

                        foreach (DataRowView dr in atlantaDataSetBindingSource)
                        {
                            atlantaDataSet.flowmeterReportRow fRow =
                                    (atlantaDataSet.flowmeterReportRow)dr.Row;

                            string location = fRow.Location;
                            DateTime begin = fRow.beginTime;
                            DateTime end = fRow.endTime;
                            TimeSpan duration = fRow.duration;
                            double travel = 0.0;
                            double fuel_rate = 0.0;
                            double idle_fuel = 0.0;
                            double move_fuel = 0.0;

                            if (fRow["travel"] != DBNull.Value)
                                travel = Math.Round(fRow.travel, 2);

                            if (fRow["fuelRate"] != DBNull.Value)
                                fuel_rate = Math.Round(fRow.fuelRate, 2);

                            if (fRow["idleFuelRate"] != DBNull.Value)
                                idle_fuel = Math.Round(fRow.idleFuelRate, 2);

                            if (fRow["moveFuelRate"] != DBNull.Value)
                                move_fuel = Math.Round(fRow.moveFuelRate, 2);

                            ReportingFlowMeter.AddDataToBindList(new reportFlowMeter(location, begin,
                                end, duration, travel, fuel_rate, idle_fuel, move_fuel));
                        } // foreach 1

                        ReportingFlowMeter.CreateElementReport();
                    } // if
                } // if
            } // foreach 2

            ReportingFlowMeter.CreateAndShowReport();
            ReportingFlowMeter.DeleteData();
        } // ExportAllDevToReport

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.FlowMetersFuel, e);
            TInfoDut info = ReportingFlowMeter.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFlowMeter.GetInfoStructure;
            ReportingFlowMeter.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней/средней  части заголовка отчета */
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFlowMeter.GetInfoStructure;
            ReportingFlowMeter.SetRectangleBrckUP(380, 0, 320, 85);
            return (Resources.TotalDistTravel + ": " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.TotalTravelTime + ": " + info.totalTimeWay + "\n" +
                Resources.FuelExControDutDayTimeStops + ": " + info.totalStops);
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFlowMeter.GetInfoStructure;
            ReportingFlowMeter.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.FuelExControlCommonFuelrate + ": " + String.Format("{0:f2}", info.totalFuel) + "\n" +
                Resources.TotalFuelSub + ": " + String.Format("{0:f2}", info.totalFuelSub) + "\n" +
                Resources.FuelExControDutDayFuelrateOnTime + ": " + String.Format("{0:f2}", info.MotoHour));
        }
        
        public override string Caption
        {
            get
            {
                return Resources.FuelConsumptionShort;
            }
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvFlowMeter);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvFlowMeter);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcFlowMeter);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        private void ClearStatusLine()
        {
            _travelLabel.Caption = Resources.DistanceText + ": --,--";
            _timeTravelLabel.Caption = Resources.TotalTravelTimeText + ": --:--:--";
            _stopTimeLabel.Caption = Resources.FlowmeterCtrlParkingTime + " --:--:--";
            _flowmeterLabel.Caption = Resources.TotalLiters + ": --,--";
            //_idleFuelRateLabelCaption = Resources.HourmeterFooter + ": ---";
            //_moveFuelRateLabel.Caption = Resources.FuelRate100kmFooter + ": ---";
            timeTravel = new TimeSpan(0);
            timeStoping = new TimeSpan(0);
        } // ClearStatusLine

        private void Localization()
        {
            _travelLabel.Caption = Resources.DistanceText + ": --,--";
            _timeTravelLabel.Caption = Resources.TotalTravelTimeText + ": --:--:--";
            _stopTimeLabel.Caption = Resources.FlowmeterCtrlParkingTime + " --:--:--";
            _flowmeterLabel.Caption = Resources.TotalLiters + ": --,--";
            //_idleFuelRateLabelCaption = Resources.HourmeterFooter + ": ---";
            //_moveFuelRateLabel.Caption = Resources.FuelRate100kmFooter + ": ---";

            colLocation.Caption = Resources.FlowmeterCtrlLocation;
            colBeginTime.Caption = Resources.InitialTime;
            colEndTime.Caption = Resources.EndTime;
            colDuration.Caption = Resources.FlowmeterPeriod;
            colTravel.Caption = Resources.Distance;
            colFuelRate.Caption = Resources.TotalLiters;
            colIdleFuelRate.Caption = Resources.FuelConsumptionParkingHint;
            colMoveFuelRate.Caption = Resources.FuelConsumptionMovementHint;

            colLocation.ToolTip = Resources.FlowmeterCtrlLocation;
            colBeginTime.ToolTip = Resources.InitialTime;
            colEndTime.ToolTip = Resources.EndTime;
            colDuration.ToolTip = Resources.FlowmeterPeriod;
            colTravel.ToolTip = Resources.Distance;
            colFuelRate.ToolTip = Resources.TotalLiters;
            colIdleFuelRate.ToolTip = Resources.FuelConsumptionParkingHint;
            colMoveFuelRate.ToolTip = Resources.FuelConsumptionMovementHint;
        } // Localization
    } // DevFlowmeterCntrl
} // BaseReports.ReportsOnDevExpress
