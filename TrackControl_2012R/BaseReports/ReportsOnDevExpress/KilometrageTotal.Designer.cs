﻿namespace BaseReports.ReportsDE
{
    partial class KilometrageTotal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KilometrageTotal));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.klmtReportDayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gControlKlmtTotal = new DevExpress.XtraGrid.GridControl();
            this.gViewKlmtTotal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceBeginMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceEndMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeBeginMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEndMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDurationPeriods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMoving = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTimeParkingAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMotorHrStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNormalWay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNormalMotor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSummaNormal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.klmtReportDayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gControlKlmtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewKlmtTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1});
            this.barManager1.MaxItemId = 4;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1057, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 506);
            this.barDockControl2.Size = new System.Drawing.Size(1057, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 506);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1057, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 506);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Продолжительность смен: ----";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.NativeFormat.ShowOptionsBeforeSave = true;
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink});
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink.ImageCollection.ImageStream")));
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // klmtReportDayBindingSource
            // 
            this.klmtReportDayBindingSource.AllowNew = true;
            this.klmtReportDayBindingSource.DataMember = "KilometrageReportDay";
            this.klmtReportDayBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // gControlKlmtTotal
            // 
            this.gControlKlmtTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gControlKlmtTotal.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gControlKlmtTotal.Location = new System.Drawing.Point(0, 24);
            this.gControlKlmtTotal.MainView = this.gViewKlmtTotal;
            this.gControlKlmtTotal.Name = "gControlKlmtTotal";
            this.gControlKlmtTotal.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            this.gControlKlmtTotal.Size = new System.Drawing.Size(1057, 482);
            this.gControlKlmtTotal.TabIndex = 9;
            this.gControlKlmtTotal.UseEmbeddedNavigator = true;
            this.gControlKlmtTotal.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gViewKlmtTotal,
            this.gridView2});
            // 
            // gViewKlmtTotal
            // 
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gViewKlmtTotal.Appearance.Empty.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gViewKlmtTotal.Appearance.EvenRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gViewKlmtTotal.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gViewKlmtTotal.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gViewKlmtTotal.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gViewKlmtTotal.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewKlmtTotal.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gViewKlmtTotal.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gViewKlmtTotal.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewKlmtTotal.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewKlmtTotal.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewKlmtTotal.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gViewKlmtTotal.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gViewKlmtTotal.Appearance.GroupButton.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gViewKlmtTotal.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gViewKlmtTotal.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gViewKlmtTotal.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gViewKlmtTotal.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gViewKlmtTotal.Appearance.GroupRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.GroupRow.Options.UseFont = true;
            this.gViewKlmtTotal.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewKlmtTotal.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gViewKlmtTotal.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewKlmtTotal.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewKlmtTotal.Appearance.HorzLine.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gViewKlmtTotal.Appearance.OddRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gViewKlmtTotal.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.Preview.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.Preview.Options.UseForeColor = true;
            this.gViewKlmtTotal.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gViewKlmtTotal.Appearance.Row.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gViewKlmtTotal.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gViewKlmtTotal.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewKlmtTotal.Appearance.VertLine.Options.UseBackColor = true;
            this.gViewKlmtTotal.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gViewKlmtTotal.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewKlmtTotal.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewKlmtTotal.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewKlmtTotal.ColumnPanelRowHeight = 40;
            this.gViewKlmtTotal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransport,
            this.colDriver,
            this.colPlaceBeginMove,
            this.colPlaceEndMove,
            this.colTimeBeginMove,
            this.colTimeEndMove,
            this.colDurationPeriods,
            this.colDistance,
            this.colTimeMoving,
            this.colTimeParkingAll,
            this.colMotorHrStop,
            this.colNormalWay,
            this.colNormalMotor,
            this.colSummaNormal});
            this.gViewKlmtTotal.GridControl = this.gControlKlmtTotal;
            this.gViewKlmtTotal.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colTimeMoving, "")});
            this.gViewKlmtTotal.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gViewKlmtTotal.Name = "gViewKlmtTotal";
            this.gViewKlmtTotal.OptionsBehavior.Editable = false;
            this.gViewKlmtTotal.OptionsDetail.AllowZoomDetail = false;
            this.gViewKlmtTotal.OptionsDetail.EnableMasterViewMode = false;
            this.gViewKlmtTotal.OptionsDetail.ShowDetailTabs = false;
            this.gViewKlmtTotal.OptionsDetail.SmartDetailExpand = false;
            this.gViewKlmtTotal.OptionsView.ColumnAutoWidth = false;
            this.gViewKlmtTotal.OptionsView.EnableAppearanceEvenRow = true;
            this.gViewKlmtTotal.OptionsView.EnableAppearanceOddRow = true;
            this.gViewKlmtTotal.OptionsView.ShowFooter = true;
            // 
            // colTransport
            // 
            this.colTransport.AppearanceCell.Options.UseTextOptions = true;
            this.colTransport.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransport.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransport.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransport.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransport.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransport.Caption = "Транспорт";
            this.colTransport.FieldName = "TransportName";
            this.colTransport.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colTransport.Name = "colTransport";
            this.colTransport.OptionsColumn.AllowEdit = false;
            this.colTransport.OptionsColumn.AllowFocus = false;
            this.colTransport.OptionsColumn.ReadOnly = true;
            this.colTransport.ToolTip = "Транспорт";
            this.colTransport.Visible = true;
            this.colTransport.VisibleIndex = 0;
            this.colTransport.Width = 44;
            // 
            // colDriver
            // 
            this.colDriver.AppearanceCell.Options.UseTextOptions = true;
            this.colDriver.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDriver.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriver.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriver.Caption = "Водитель";
            this.colDriver.FieldName = "DraverName";
            this.colDriver.Name = "colDriver";
            this.colDriver.OptionsColumn.AllowEdit = false;
            this.colDriver.OptionsColumn.AllowFocus = false;
            this.colDriver.OptionsColumn.ReadOnly = true;
            this.colDriver.ToolTip = "Водитель";
            this.colDriver.Visible = true;
            this.colDriver.VisibleIndex = 1;
            this.colDriver.Width = 33;
            // 
            // colPlaceBeginMove
            // 
            this.colPlaceBeginMove.AppearanceCell.Options.UseTextOptions = true;
            this.colPlaceBeginMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colPlaceBeginMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPlaceBeginMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlaceBeginMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlaceBeginMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlaceBeginMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlaceBeginMove.Caption = "Место начала движения";
            this.colPlaceBeginMove.FieldName = "PlaceStart";
            this.colPlaceBeginMove.Name = "colPlaceBeginMove";
            this.colPlaceBeginMove.OptionsColumn.AllowEdit = false;
            this.colPlaceBeginMove.OptionsColumn.AllowFocus = false;
            this.colPlaceBeginMove.OptionsColumn.ReadOnly = true;
            this.colPlaceBeginMove.ToolTip = "Место начала движения";
            this.colPlaceBeginMove.Visible = true;
            this.colPlaceBeginMove.VisibleIndex = 2;
            this.colPlaceBeginMove.Width = 67;
            // 
            // colPlaceEndMove
            // 
            this.colPlaceEndMove.AppearanceCell.Options.UseTextOptions = true;
            this.colPlaceEndMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlaceEndMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPlaceEndMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlaceEndMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colPlaceEndMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPlaceEndMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPlaceEndMove.Caption = "Место окончания движения";
            this.colPlaceEndMove.FieldName = "PlaceEnd";
            this.colPlaceEndMove.Name = "colPlaceEndMove";
            this.colPlaceEndMove.OptionsColumn.AllowEdit = false;
            this.colPlaceEndMove.OptionsColumn.AllowFocus = false;
            this.colPlaceEndMove.OptionsColumn.ReadOnly = true;
            this.colPlaceEndMove.ToolTip = "Место окончания движения";
            this.colPlaceEndMove.Visible = true;
            this.colPlaceEndMove.VisibleIndex = 3;
            this.colPlaceEndMove.Width = 63;
            // 
            // colTimeBeginMove
            // 
            this.colTimeBeginMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeBeginMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeBeginMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeBeginMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeBeginMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeBeginMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeBeginMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeBeginMove.Caption = "Время начала движения";
            this.colTimeBeginMove.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.colTimeBeginMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeBeginMove.FieldName = "TimeStart";
            this.colTimeBeginMove.Name = "colTimeBeginMove";
            this.colTimeBeginMove.OptionsColumn.AllowEdit = false;
            this.colTimeBeginMove.OptionsColumn.AllowFocus = false;
            this.colTimeBeginMove.OptionsColumn.ReadOnly = true;
            this.colTimeBeginMove.ToolTip = "Время начала движения";
            this.colTimeBeginMove.Visible = true;
            this.colTimeBeginMove.VisibleIndex = 4;
            this.colTimeBeginMove.Width = 71;
            // 
            // colTimeEndMove
            // 
            this.colTimeEndMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEndMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEndMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeEndMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEndMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEndMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEndMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEndMove.Caption = "Время окончания движения";
            this.colTimeEndMove.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.colTimeEndMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEndMove.FieldName = "TimeEnd";
            this.colTimeEndMove.Name = "colTimeEndMove";
            this.colTimeEndMove.OptionsColumn.AllowEdit = false;
            this.colTimeEndMove.OptionsColumn.AllowFocus = false;
            this.colTimeEndMove.OptionsColumn.ReadOnly = true;
            this.colTimeEndMove.ToolTip = "Время окончания движения";
            this.colTimeEndMove.Visible = true;
            this.colTimeEndMove.VisibleIndex = 5;
            this.colTimeEndMove.Width = 125;
            // 
            // colDurationPeriods
            // 
            this.colDurationPeriods.AppearanceCell.Options.UseTextOptions = true;
            this.colDurationPeriods.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDurationPeriods.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDurationPeriods.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDurationPeriods.AppearanceHeader.Options.UseTextOptions = true;
            this.colDurationPeriods.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDurationPeriods.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDurationPeriods.Caption = "Продолжительность смен";
            this.colDurationPeriods.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDurationPeriods.FieldName = "Duration";
            this.colDurationPeriods.Name = "colDurationPeriods";
            this.colDurationPeriods.OptionsColumn.AllowEdit = false;
            this.colDurationPeriods.OptionsColumn.AllowFocus = false;
            this.colDurationPeriods.OptionsColumn.ReadOnly = true;
            this.colDurationPeriods.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Duration", "{0:dd.HH:mm}")});
            this.colDurationPeriods.ToolTip = "Продолжительность смен";
            this.colDurationPeriods.Visible = true;
            this.colDurationPeriods.VisibleIndex = 6;
            this.colDurationPeriods.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Пройденный путь, км";
            this.colDistance.DisplayFormat.FormatString = "{0:f2}";
            this.colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistance.FieldName = "DistanceTotal";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DistanceTotal", "{0:f2}")});
            this.colDistance.ToolTip = "Пройденный путь, км";
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 7;
            // 
            // colTimeMoving
            // 
            this.colTimeMoving.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colTimeMoving.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMoving.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoving.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMoving.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoving.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMoving.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoving.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoving.Caption = "Суммарное время движения";
            this.colTimeMoving.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTimeMoving.FieldName = "TimeMoveTotal";
            this.colTimeMoving.Name = "colTimeMoving";
            this.colTimeMoving.OptionsColumn.AllowEdit = false;
            this.colTimeMoving.OptionsColumn.AllowFocus = false;
            this.colTimeMoving.OptionsColumn.ReadOnly = true;
            this.colTimeMoving.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeMoveTotal", "{0:dd.HH:mm}")});
            this.colTimeMoving.ToolTip = "Суммарное время движения";
            this.colTimeMoving.Visible = true;
            this.colTimeMoving.VisibleIndex = 8;
            this.colTimeMoving.Width = 100;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colTimeParkingAll
            // 
            this.colTimeParkingAll.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeParkingAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeParkingAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeParkingAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeParkingAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeParkingAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeParkingAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeParkingAll.Caption = "Суммарное время остановок";
            this.colTimeParkingAll.ColumnEdit = this.repositoryItemTextEdit3;
            this.colTimeParkingAll.FieldName = "TimeStopsTotal";
            this.colTimeParkingAll.Name = "colTimeParkingAll";
            this.colTimeParkingAll.OptionsColumn.AllowEdit = false;
            this.colTimeParkingAll.OptionsColumn.AllowFocus = false;
            this.colTimeParkingAll.OptionsColumn.ReadOnly = true;
            this.colTimeParkingAll.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeStopsTotal", "{0:dd.HH:mm}")});
            this.colTimeParkingAll.ToolTip = "Суммарное время остановок";
            this.colTimeParkingAll.Visible = true;
            this.colTimeParkingAll.VisibleIndex = 9;
            this.colTimeParkingAll.Width = 100;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colMotorHrStop
            // 
            this.colMotorHrStop.AppearanceCell.Options.UseTextOptions = true;
            this.colMotorHrStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMotorHrStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMotorHrStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colMotorHrStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colMotorHrStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMotorHrStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMotorHrStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMotorHrStop.Caption = "Общее время работы на холостом ходу";
            this.colMotorHrStop.FieldName = "MotorHoursStop";
            this.colMotorHrStop.Name = "colMotorHrStop";
            this.colMotorHrStop.OptionsColumn.AllowEdit = false;
            this.colMotorHrStop.OptionsColumn.AllowFocus = false;
            this.colMotorHrStop.OptionsColumn.ReadOnly = true;
            this.colMotorHrStop.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colMotorHrStop.ToolTip = "Общее время работы на холостом ходу";
            this.colMotorHrStop.Visible = true;
            this.colMotorHrStop.VisibleIndex = 10;
            // 
            // colNormalWay
            // 
            this.colNormalWay.AppearanceCell.Options.UseTextOptions = true;
            this.colNormalWay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormalWay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormalWay.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colNormalWay.AppearanceHeader.Options.UseTextOptions = true;
            this.colNormalWay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormalWay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormalWay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNormalWay.Caption = "Нормированный расход топлива по пробегу, л";
            this.colNormalWay.DisplayFormat.FormatString = "N1";
            this.colNormalWay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNormalWay.FieldName = "NormalWayFuel";
            this.colNormalWay.Name = "colNormalWay";
            this.colNormalWay.OptionsColumn.AllowEdit = false;
            this.colNormalWay.OptionsColumn.AllowFocus = false;
            this.colNormalWay.OptionsColumn.ReadOnly = true;
            this.colNormalWay.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NormalWayFuel", "{0:f1}")});
            this.colNormalWay.ToolTip = "Нормированный расход топлива по пробегу, л";
            this.colNormalWay.Visible = true;
            this.colNormalWay.VisibleIndex = 11;
            // 
            // colNormalMotor
            // 
            this.colNormalMotor.AppearanceCell.Options.UseTextOptions = true;
            this.colNormalMotor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormalMotor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormalMotor.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colNormalMotor.AppearanceHeader.Options.UseTextOptions = true;
            this.colNormalMotor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormalMotor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormalMotor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNormalMotor.Caption = "Нормированный расход топлива на холостом ходу, л";
            this.colNormalMotor.DisplayFormat.FormatString = "N1";
            this.colNormalMotor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNormalMotor.FieldName = "NormalMotorFuel";
            this.colNormalMotor.Name = "colNormalMotor";
            this.colNormalMotor.OptionsColumn.AllowEdit = false;
            this.colNormalMotor.OptionsColumn.AllowFocus = false;
            this.colNormalMotor.OptionsColumn.ReadOnly = true;
            this.colNormalMotor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NormalMotorFuel", "{0:f1}")});
            this.colNormalMotor.ToolTip = "Нормированный расход топлива на холостом ходу, л";
            this.colNormalMotor.Visible = true;
            this.colNormalMotor.VisibleIndex = 12;
            // 
            // colSummaNormal
            // 
            this.colSummaNormal.AppearanceCell.Options.UseTextOptions = true;
            this.colSummaNormal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSummaNormal.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSummaNormal.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSummaNormal.AppearanceHeader.Options.UseTextOptions = true;
            this.colSummaNormal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSummaNormal.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSummaNormal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSummaNormal.Caption = "Сумма нормированных расходов, л";
            this.colSummaNormal.DisplayFormat.FormatString = "N1";
            this.colSummaNormal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSummaNormal.FieldName = "SummaNormal";
            this.colSummaNormal.Name = "colSummaNormal";
            this.colSummaNormal.OptionsColumn.AllowEdit = false;
            this.colSummaNormal.OptionsColumn.AllowFocus = false;
            this.colSummaNormal.OptionsColumn.ReadOnly = true;
            this.colSummaNormal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SummaNormal", "{0:f1}")});
            this.colSummaNormal.ToolTip = "Сумма нормированных расходов, л";
            this.colSummaNormal.Visible = true;
            this.colSummaNormal.VisibleIndex = 13;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gControlKlmtTotal;
            this.gridView2.Name = "gridView2";
            // 
            // KilometrageTotal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gControlKlmtTotal);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "KilometrageTotal";
            this.Size = new System.Drawing.Size(1057, 506);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gControlKlmtTotal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.klmtReportDayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gControlKlmtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewKlmtTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private System.Windows.Forms.BindingSource klmtReportDayBindingSource;
        private DevExpress.XtraGrid.GridControl gControlKlmtTotal;
        private DevExpress.XtraGrid.Views.Grid.GridView gViewKlmtTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colTransport;
        private DevExpress.XtraGrid.Columns.GridColumn colDriver;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceBeginMove;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceEndMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeBeginMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEndMove;
        private DevExpress.XtraGrid.Columns.GridColumn colDurationPeriods;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMoving;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeParkingAll;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colNormalWay;
        private DevExpress.XtraGrid.Columns.GridColumn colNormalMotor;
        private DevExpress.XtraGrid.Columns.GridColumn colMotorHrStop;
        private DevExpress.XtraGrid.Columns.GridColumn colSummaNormal;
    }
}
