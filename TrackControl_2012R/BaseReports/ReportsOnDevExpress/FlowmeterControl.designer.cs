﻿namespace BaseReports.ReportsDE
{
    partial class FlowmeterControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlowmeterControl));
            this.gcFlowMeter = new DevExpress.XtraGrid.GridControl();
            this.flowmeterReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atlantaDataSet = new LocalCache.atlantaDataSet();
            this.gvFlowMeter = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeginTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdleFuelRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMoveFuelRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this._travelLabel = new DevExpress.XtraBars.BarStaticItem();
            this._timeTravelLabel = new DevExpress.XtraBars.BarStaticItem();
            this._stopTimeLabel = new DevExpress.XtraBars.BarStaticItem();
            this._flowmeterLabel = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcFlowMeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFlowMeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gcFlowMeter
            // 
            this.gcFlowMeter.DataSource = this.flowmeterReportBindingSource;
            this.gcFlowMeter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcFlowMeter.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcFlowMeter.Location = new System.Drawing.Point(0, 24);
            this.gcFlowMeter.MainView = this.gvFlowMeter;
            this.gcFlowMeter.Name = "gcFlowMeter";
            this.gcFlowMeter.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2});
            this.gcFlowMeter.Size = new System.Drawing.Size(833, 455);
            this.gcFlowMeter.TabIndex = 9;
            this.gcFlowMeter.UseEmbeddedNavigator = true;
            this.gcFlowMeter.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFlowMeter,
            this.gridView2});
            // 
            // flowmeterReportBindingSource
            // 
            this.flowmeterReportBindingSource.DataMember = "flowmeterReport";
            this.flowmeterReportBindingSource.DataSource = this.atlantaDataSet;
            // 
            // atlantaDataSet
            // 
            this.atlantaDataSet.DataSetName = "atlantaDataSet";
            this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvFlowMeter
            // 
            this.gvFlowMeter.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvFlowMeter.Appearance.Empty.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvFlowMeter.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvFlowMeter.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvFlowMeter.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvFlowMeter.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFlowMeter.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFlowMeter.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvFlowMeter.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvFlowMeter.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFlowMeter.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFlowMeter.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFlowMeter.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvFlowMeter.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvFlowMeter.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvFlowMeter.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvFlowMeter.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvFlowMeter.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvFlowMeter.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvFlowMeter.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.GroupRow.Options.UseFont = true;
            this.gvFlowMeter.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFlowMeter.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFlowMeter.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFlowMeter.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFlowMeter.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvFlowMeter.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvFlowMeter.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.Preview.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.Preview.Options.UseForeColor = true;
            this.gvFlowMeter.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvFlowMeter.Appearance.Row.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvFlowMeter.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFlowMeter.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFlowMeter.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFlowMeter.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvFlowMeter.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFlowMeter.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFlowMeter.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFlowMeter.ColumnPanelRowHeight = 40;
            this.gvFlowMeter.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLocation,
            this.colBeginTime,
            this.colEndTime,
            this.colDuration,
            this.colTravel,
            this.colFuelRate,
            this.colIdleFuelRate,
            this.colMoveFuelRate});
            this.gvFlowMeter.GridControl = this.gcFlowMeter;
            this.gvFlowMeter.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvFlowMeter.Name = "gvFlowMeter";
            this.gvFlowMeter.OptionsDetail.AllowZoomDetail = false;
            this.gvFlowMeter.OptionsDetail.EnableMasterViewMode = false;
            this.gvFlowMeter.OptionsDetail.ShowDetailTabs = false;
            this.gvFlowMeter.OptionsDetail.SmartDetailExpand = false;
            this.gvFlowMeter.OptionsSelection.MultiSelect = true;
            this.gvFlowMeter.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFlowMeter.OptionsView.EnableAppearanceOddRow = true;
            this.gvFlowMeter.OptionsView.ShowFooter = true;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceCell.Options.UseTextOptions = true;
            this.colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.Caption = "Состояние/Местоположение";
            this.colLocation.FieldName = "Location";
            this.colLocation.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.ToolTip = "Состояние/Местоположение";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 0;
            this.colLocation.Width = 170;
            // 
            // colBeginTime
            // 
            this.colBeginTime.AppearanceCell.Options.UseTextOptions = true;
            this.colBeginTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colBeginTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginTime.Caption = "Время начала";
            this.colBeginTime.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.colBeginTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBeginTime.FieldName = "beginTime";
            this.colBeginTime.Name = "colBeginTime";
            this.colBeginTime.OptionsColumn.AllowEdit = false;
            this.colBeginTime.OptionsColumn.AllowFocus = false;
            this.colBeginTime.OptionsColumn.ReadOnly = true;
            this.colBeginTime.ToolTip = "Время начала";
            this.colBeginTime.Visible = true;
            this.colBeginTime.VisibleIndex = 1;
            this.colBeginTime.Width = 95;
            // 
            // colEndTime
            // 
            this.colEndTime.AppearanceCell.Options.UseTextOptions = true;
            this.colEndTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colEndTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndTime.Caption = "Время окончания";
            this.colEndTime.DisplayFormat.FormatString = "dd:MM:yyyy HH:mm";
            this.colEndTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEndTime.FieldName = "endTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.ReadOnly = true;
            this.colEndTime.ToolTip = "Время окончания";
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 2;
            this.colEndTime.Width = 95;
            // 
            // colDuration
            // 
            this.colDuration.AppearanceCell.Options.UseTextOptions = true;
            this.colDuration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDuration.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.AppearanceHeader.Options.UseTextOptions = true;
            this.colDuration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDuration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDuration.Caption = "Период, д.время";
            this.colDuration.ColumnEdit = this.repositoryItemTextEdit2;
            this.colDuration.FieldName = "duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.ToolTip = "Период, д.время";
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 3;
            this.colDuration.Width = 105;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.ReadOnly = true;
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.Caption = "Пройденный путь, км";
            this.colTravel.DisplayFormat.FormatString = "N1";
            this.colTravel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravel.FieldName = "travel";
            this.colTravel.Name = "colTravel";
            this.colTravel.OptionsColumn.AllowEdit = false;
            this.colTravel.OptionsColumn.AllowFocus = false;
            this.colTravel.OptionsColumn.ReadOnly = true;
            this.colTravel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "travel", "{0:f1}")});
            this.colTravel.ToolTip = "Пройденный путь, км";
            this.colTravel.Visible = true;
            this.colTravel.VisibleIndex = 4;
            this.colTravel.Width = 70;
            // 
            // colFuelRate
            // 
            this.colFuelRate.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelRate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelRate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelRate.Caption = "Топлива всего, л";
            this.colFuelRate.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelRate.FieldName = "fuelRate";
            this.colFuelRate.Name = "colFuelRate";
            this.colFuelRate.OptionsColumn.AllowEdit = false;
            this.colFuelRate.OptionsColumn.AllowFocus = false;
            this.colFuelRate.OptionsColumn.ReadOnly = true;
            this.colFuelRate.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "fuelRate", "{0:f1}")});
            this.colFuelRate.ToolTip = "Топлива всего, л";
            this.colFuelRate.Visible = true;
            this.colFuelRate.VisibleIndex = 5;
            this.colFuelRate.Width = 100;
            // 
            // colIdleFuelRate
            // 
            this.colIdleFuelRate.AppearanceCell.Options.UseTextOptions = true;
            this.colIdleFuelRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdleFuelRate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdleFuelRate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdleFuelRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdleFuelRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdleFuelRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdleFuelRate.Caption = "Расход топлива во время стоянки средний, л";
            this.colIdleFuelRate.DisplayFormat.FormatString = "N1";
            this.colIdleFuelRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIdleFuelRate.FieldName = "idleFuelRate";
            this.colIdleFuelRate.Name = "colIdleFuelRate";
            this.colIdleFuelRate.OptionsColumn.AllowEdit = false;
            this.colIdleFuelRate.OptionsColumn.AllowFocus = false;
            this.colIdleFuelRate.OptionsColumn.ReadOnly = true;
            this.colIdleFuelRate.ToolTip = "Расход топлива во время стоянки средний, л";
            this.colIdleFuelRate.Visible = true;
            this.colIdleFuelRate.VisibleIndex = 6;
            this.colIdleFuelRate.Width = 80;
            // 
            // colMoveFuelRate
            // 
            this.colMoveFuelRate.AppearanceCell.Options.UseTextOptions = true;
            this.colMoveFuelRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMoveFuelRate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMoveFuelRate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMoveFuelRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colMoveFuelRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMoveFuelRate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMoveFuelRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMoveFuelRate.Caption = "Расход топлива при движении, л";
            this.colMoveFuelRate.DisplayFormat.FormatString = "N1";
            this.colMoveFuelRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMoveFuelRate.FieldName = "moveFuelRate";
            this.colMoveFuelRate.Name = "colMoveFuelRate";
            this.colMoveFuelRate.OptionsColumn.AllowEdit = false;
            this.colMoveFuelRate.OptionsColumn.AllowFocus = false;
            this.colMoveFuelRate.OptionsColumn.ReadOnly = true;
            this.colMoveFuelRate.ToolTip = "Расход топлива при движении, л";
            this.colMoveFuelRate.Visible = true;
            this.colMoveFuelRate.VisibleIndex = 7;
            this.colMoveFuelRate.Width = 110;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcFlowMeter;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._travelLabel,
            this._timeTravelLabel,
            this._stopTimeLabel,
            this._flowmeterLabel});
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._travelLabel),
            new DevExpress.XtraBars.LinkPersistInfo(this._timeTravelLabel),
            new DevExpress.XtraBars.LinkPersistInfo(this._stopTimeLabel),
            new DevExpress.XtraBars.LinkPersistInfo(this._flowmeterLabel)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // _travelLabel
            // 
            this._travelLabel.Caption = "Пройденный путь, --,-- км";
            this._travelLabel.Id = 0;
            this._travelLabel.Name = "_travelLabel";
            this._travelLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _timeTravelLabel
            // 
            this._timeTravelLabel.Caption = "Время в пути (без коротких остановок): --:--:--";
            this._timeTravelLabel.Id = 1;
            this._timeTravelLabel.Name = "_timeTravelLabel";
            this._timeTravelLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _stopTimeLabel
            // 
            this._stopTimeLabel.Caption = "Время стоянки (с короткими остановками): --:--:--";
            this._stopTimeLabel.Id = 2;
            this._stopTimeLabel.Name = "_stopTimeLabel";
            this._stopTimeLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _flowmeterLabel
            // 
            this._flowmeterLabel.Caption = "Всего топлива: --,-- л";
            this._flowmeterLabel.Id = 3;
            this._flowmeterLabel.Name = "_flowmeterLabel";
            this._flowmeterLabel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(833, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 479);
            this.barDockControl2.Size = new System.Drawing.Size(833, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 479);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(833, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 479);
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink});
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink.ImageCollection.ImageStream")));
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // FlowmeterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gcFlowMeter);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "FlowmeterControl";
            this.Size = new System.Drawing.Size(833, 504);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gcFlowMeter, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcFlowMeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFlowMeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcFlowMeter;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFlowMeter;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colBeginTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIdleFuelRate;
        private DevExpress.XtraGrid.Columns.GridColumn colMoveFuelRate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraBars.BarStaticItem _travelLabel;
        private DevExpress.XtraBars.BarStaticItem _timeTravelLabel;
        private DevExpress.XtraBars.BarStaticItem _stopTimeLabel;
        private DevExpress.XtraBars.BarStaticItem _flowmeterLabel;
        private System.Windows.Forms.BindingSource flowmeterReportBindingSource;
        private LocalCache.atlantaDataSet atlantaDataSet;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
    }
}
