using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Native;
using LocalCache;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;

namespace BaseReports.ReportsDE
{
    public partial class BaseControl : DevExpress.XtraEditors.XtraUserControl, IReportTabControl
    {
        protected bool _stopRun;
        protected static IGraph graph;
        protected bool isButtonStartPush = false;

        public static IGraph Graph
        {
            get { return graph; }
            set { graph = value; }
        }
        /// <summary>
        /// ��������� ������
        /// </summary>
        public static atlantaDataSet Dataset
        {
            set { _dataset = value; }
            get { return _dataset; }
        }

        protected static atlantaDataSet _dataset;
        /// <summary>
        /// ��������� ������� ������ mobitelsRow 
        /// (�� ������� ������ ��������� ������)
        /// </summary>
        protected atlantaDataSet.mobitelsRow curMrow;

        /// <summary>
        /// �������� ������� ������
        /// </summary>
        protected VehicleInfo _vehicleInfo;

        /// <summary>
        /// ������ ����������
        /// </summary>
        protected List<IAlgorithm> _algoritms;

        private string nameReport = "";

        public BaseControl()
        {
            InitializeComponent();
            Init();
            _algoritms = new List<IAlgorithm>();
            Algorithm.Action += Algorithm_Action;
            Localization();
            comboAlgoChoiser.Visibility = BarItemVisibility.Never;
            InitComboAlgoChoiser();
            repAlgoChoiser.SelectedIndexChanged += comboAlgoChoiser_ItemClick;
        }

        protected virtual void comboAlgoChoiser_ItemClick(object sender, EventArgs e)
        {
            // to do this
        }

        private void InitComboAlgoChoiser()
        {
            repAlgoChoiser.Items.Add(Resources.LevelFuelTrack);
            repAlgoChoiser.Items.Add(Resources.LevelFuelAgregat1);
            repAlgoChoiser.Items.Add(Resources.LevelFuelAgregat2);
            repAlgoChoiser.Items.Add(Resources.LevelFuelAgregat3);
            repAlgoChoiser.Items.Add(Resources.LevelFuelAgregatTotal);

            comboAlgoChoiser.EditValue = Resources.LevelFuelTrack;
        }

        protected void SetVisibilityAlgoChoiser(bool visible)
        {
            if(visible)
                comboAlgoChoiser.Visibility = BarItemVisibility.Always;
            else
                comboAlgoChoiser.Visibility = BarItemVisibility.Never;
        }

        protected void DisableMenuButton()
        {
            barButtonItem2.Enabled = false;
        }

        protected void EnableMenuButton()
        {
            barButtonItem2.Enabled = true;
        }

        void Localization()
        {
            SetStartButton();
            bbiPrintReport.Caption = Resources.PrintExport;
        }

        public virtual void EnabledExportExtendMenu()
        {
            barReportExtend.Visibility = BarItemVisibility.Always;
        }


        #region IReportTabControl Members
        public virtual void DisableButton()
        {
            bbiShowOnMap.Enabled = false;
            bbiShowOnGraf.Enabled = false;
            bbiPrintReport.Enabled = false;
            checkAddingInfo.Enabled = false;
        }

        public virtual void EnableButton()
        {
            bbiShowOnMap.Enabled = true;
            bbiShowOnGraf.Enabled = true;
            bbiPrintReport.Enabled = true;
            checkAddingInfo.Enabled = true;
        }

        public virtual void SetEnableExportButton()
        {
            bbiPrintReport.Enabled = true;
        }

        public void EnableRun()
        {
            SetStartButton();
            bbiStart.Enabled = true;
        }

        public void DisableRun()
        {
            bbiStart.Enabled = false;
            DisableButton();
        }

        public virtual string Caption
        {
            get { throw new NotImplementedException(); }
        }

        public UserControl Control
        {
            get { return (UserControl)this; ; }
        }

        public virtual void Select(LocalCache.atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                _vehicleInfo = new VehicleInfo(mobitel);
            }
        }
        /// <summary>
        /// ������� ������
        /// </summary>
        public void SelectItem(LocalCache.atlantaDataSet.mobitelsRow mRow)
        {
            //ClearReport();
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(mRow);
                alg.Run();
                if (_stopRun) return; 
            }
        }

        public void SelectGraphic(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            return;
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void SaveSetting()
        {
            return;
        }

        /// <summary>
        /// ������� ������ ������
        /// </summary>
        public virtual void ClearReport()
        {
            return;
        }

        public string CurrentActivateReport
        {
            get { return nameReport; } 
            set { nameReport = value; }
        }

        #endregion

        protected virtual void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                _stopRun = false;
                StartReport();
                SetStartButton();
            }
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
            }
        }

        protected void SetStopButton()
        {
            bbiStart.Caption = Resources.Stop;
            bbiStart.Glyph = Shared.StopMain;
            bbiStart.Hint = Resources.Stop;
        }

        protected void SetStartButton()
        {
            bbiStart.Caption = Resources.Start;
            bbiStart.Glyph = Shared.StartMain;
            bbiStart.Hint = Resources.Start;
            Algorithm.IsSuppressReportsIndication = false;
        }

        private void StartReport()
        {
            bool noData = true; // �� ��������� ������ ��� ������ �� �� ����� ������.
            foreach (IAlgorithm alg in _algoritms)
            {
                if (alg is Algorithm)
                    Algorithm.StopRun = false;
            }
            _stopRun = false;
            foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    Application.DoEvents();  
                    SelectItem(m_row);
                    noData = false;
                    if (_stopRun) break;
                }
            }
            if (noData)
            {
                MessageBox.Show(Resources.WarningNoData, Resources.Notification);
            }
        }

        public void StopReport()
        {
            foreach (IAlgorithm alg in _algoritms)
            {
                if (alg is Algorithm) 
                Algorithm.StopRun  = true;
            }
        }

        public void BeginReport()
        {
            foreach (IAlgorithm alg in _algoritms)
            {
                if (alg is Algorithm)
                    Algorithm.StopRun = false;
            }
        }

        /// <summary>
        /// ���������� ������ ���������
        /// </summary>
        /// <param name="algorithm"></param>
        public void AddAlgorithm(IAlgorithm algorithm)
        {
            _algoritms.Add(algorithm);
        }

        protected virtual void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void bbiShowOnGraf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void Algorithm_Action(object sender, EventArgs e)
        {
            return;
        }

        // ������������ ������ ��������������� �� ������� ������
        protected virtual void ExportToExcelDevExpress()
        {
            return;
        }

        // ������������ ������ ���������������� �� ������ �������
        protected virtual void ExportAllDevToReport()
        {
            return;
        }

        protected virtual void ExportAllDevToReportExtend()
        {
            return;
        }

        protected void DevExpressReportHeader(string Header, CreateAreaEventArgs e)
        {
            TextBrick brick = e.Graph.DrawString(Header, Color.Black, new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25),
            DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Arial", 12, FontStyle.Bold);
            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }

        protected void DevExpressReportSubHeader(string SubHeader, int height, CreateAreaEventArgs e)
        {

            TextBrick brick = e.Graph.DrawString(SubHeader, Color.Black, new RectangleF(0, 30, e.Graph.ClientPageSize.Width, height),
            DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Times New Roman", 10, FontStyle.Bold);
            brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            brick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
            brick.BorderWidth = 0;
            brick.BackColor = Color.Transparent;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }

        public void HideStatusBar()
        {
            bar3.Visible = false;
        }

        public void DisableMenuFirstButton()
        {
            barCurrentReport.Enabled = false;
        }

        public void DisableMenuSecondButton()
        {
            barAllReport.Enabled = false;
        }

        public void DisableMenuReportExtendButton()
        {
            barReportExtend.Enabled = false;
        }

        public void DisableMenuItem2Button()
        {
            barButtonItem2.Enabled = false;
        }

        public DevExpress.XtraBars.BarManager GetBarManager()
        {
            return barManager1;
        }

        public DevExpress.XtraBars.Bar GetToolBar()
        {
            return bar2;
        }

        public DevExpress.XtraBars.BarButtonItem getReportExport()
        {
            return barAllReport;
        }

        protected virtual void barCurrentReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportToExcelDevExpress();
        }

        protected virtual void barAllReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportAllDevToReport();
        }
        
        protected void SetNewNameToPrintBar(string nameOne, string hintOne, string nameTwo, string hintTwo)
        {
			barCurrentReport.Caption = nameOne;
			barCurrentReport.Hint = hintOne;
			barAllReport.Caption = nameTwo;
			barAllReport.Hint = hintTwo;
        }

        protected void Init()
        {
            bbiShowOnGraf.Hint = Resources.ShowOnGraph;
            bbiShowOnMap.Hint = Resources.ShowOnMap;
            bbiPrintReport.Hint = Resources.PrintExport;
            barCurrentReport.Caption = Resources.ReportCurrentTransport;
            barAllReport.Caption = Resources.ReportAllTransport;
            barButtonShowing.Caption = Resources.ButtonShowHidePanel;

            barButtonGroupPanel.Caption = Resources.PanelGrouping;
            barButtonFooterPanel.Caption = Resources.ShowAllPanel;
            barButtonNavigator.Caption = Resources.ShowNavigator;
            barButtonStatusPanel.Caption = Resources.HideStatusLine;
            barButtonItem1.Caption = Resources.ChangeSizePanel;

            bchSafetyMode.Caption = Resources.SafetyMode;
            bchSafetyMode.Hint = Resources.SafetyModeHint;

            checkAddingInfo.Caption = Resources.AddingInfo;
            checkAddingInfo.Hint = Resources.AddingInfoHint;
        }

        protected void GroupPanel(GridView gV)
        {
            if (gV.OptionsView.ShowGroupPanel == false)
            {
                gV.OptionsView.ShowGroupPanel = true;
                barButtonGroupPanel.Caption = Resources.ResButtonGroupPanelHide;
            }
            else
            {
                gV.OptionsView.ShowGroupPanel = false;
                barButtonGroupPanel.Caption = Resources.ResButtonGroupPanelShow;
            }
        }

        protected void FooterPanel(GridView gV)
        {
            if (gV.OptionsView.ShowFooter == false)
            {
                gV.OptionsView.ShowFooter = true;
                barButtonFooterPanel.Caption = Resources.ResButtonFooterPanelHide;
            }
            else
            {
                gV.OptionsView.ShowFooter = false;
                barButtonFooterPanel.Caption = Resources.ResButtonFooterPanelShow;
            }
        }

        protected void NavigatorPanel(GridControl gC)
        {
            if (gC.UseEmbeddedNavigator == false)
            {
                gC.UseEmbeddedNavigator = true;
                barButtonNavigator.Caption = Resources.ResButtonNavigatorHide;
            }
            else
            {
                gC.UseEmbeddedNavigator = false;
                barButtonNavigator.Caption = Resources.ResButtonNavigatorShow;
            }
        }

        protected void StatusBar(Bar _bar)
        {
            if (_bar.Visible == false)
            {
                _bar.Visible = true;
                barButtonStatusPanel.Caption = Resources.ResButtonStatusPanelHide;
            }
            else
            {
                _bar.Visible = false;
                barButtonStatusPanel.Caption = Resources.ResButtonStatusPanelShow;
            }
        }

        protected void VisionPanel(GridView gView, GridControl gControl, Bar _bar)
        {
            if (gView != null)
            {
                gView.OptionsView.ShowFooter = false;
                barButtonGroupPanel.Enabled = true;
            }
            else
            {
                barButtonGroupPanel.Enabled = false;
                barButtonGroupPanel.Caption = Resources.ResButtonGroupPanelShow;
            }

            if (gView != null)
            {
                gView.OptionsView.ShowGroupPanel = false;
                barButtonFooterPanel.Enabled = true;
            }
            else
            {
                barButtonFooterPanel.Enabled = false;
                barButtonFooterPanel.Caption = Resources.ResButtonFooterPanelShow;
            }

            if (gControl != null)
            {
                gControl.UseEmbeddedNavigator = false;
                barButtonNavigator.Enabled = true;
            }
            else
            {
                barButtonNavigator.Enabled = false;
                barButtonNavigator.Caption = Resources.ResButtonNavigatorShow;
            }

            if (_bar != null)
            {
                _bar.Visible = true;
                barButtonStatusPanel.Enabled = true;
            }
            else
            {
                barButtonStatusPanel.Enabled = false;
                barButtonStatusPanel.Caption = Resources.ResButtonStatusPanelShow;
            }
        } // VisionPanel

        protected virtual void ButtonPanelVision()
        {
            return;
        }

        private void barButtonShowing_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            return;
        }

        private delegate GpsData[] GetDataGps(int mobitelId, DateTime begin, DateTime end);

        protected void RunInSafeMode(string reportName)
        {
            if (ReportTabControl.CheckedVehicles == null || ReportTabControl.CheckedVehicles.Count == 0) 
                return;

            ReportsControl.OnClearMapObjectsNeeded();
            bool noData = true;
            isButtonStartPush = false;
            Algorithm.IsSuppressReportsIndication = true;

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                var getDg = new GetDataGps(EnvokenGetData /* ��� ������� ������ ������� ������ */);
                IAsyncResult iftAr = null;
                bool first = true;
                _dataset.mobitels.ForEach(row => row.Check = false);
                ReportTabControl.CheckedVehicles.ForEach(repVeh =>
                    {
                        atlantaDataSet.mobitelsRow row = _dataset.mobitels.FindByMobitel_ID(repVeh.Mobitel.Id);
                        if (row != null) row.Check = true;
                    });
                
                Algorithm.RunFromModule = true;
                atlantaDataSet.mobitelsRow mRowPrev = null;
                int counter = 1;

                foreach (atlantaDataSet.mobitelsRow mRow in _dataset.mobitels)
                {
                    if (mRow.Check)
                    {
                        if (first)
                        {
                            Algorithm.SetEventAlgoritmStarted(String.Format(Resources.BuildingReportMessage, reportName), "", ReportTabControl.CheckedVehicles.Count);
                            iftAr = getDg.BeginInvoke(mRow.Mobitel_ID, Algorithm.Period.Begin, Algorithm.Period.End,
                                                      null, null);
                            first = false;
                        }
                        else
                        {
                            while (!iftAr.IsCompleted)
                            {
                                Application.DoEvents();
                            }
                            Algorithm.GpsDatas = getDg.EndInvoke(iftAr);
                            Algorithm.SetEventAlgoritmProgressChanged(++counter);
                            iftAr = getDg.BeginInvoke(mRow.Mobitel_ID, Algorithm.Period.Begin, Algorithm.Period.End, null,
                                                      null);
                            if (Algorithm.GpsDatas.Length > 0)
                            {
                                Application.DoEvents();

                                SelectItem(mRowPrev);

                                noData = false;
                                if (_stopRun) break;
                            }
                        }
                        mRowPrev = mRow;
                    }
                } // foreach

                while (!iftAr.IsCompleted)
                {
                    Application.DoEvents();
                }
                Algorithm.GpsDatas = getDg.EndInvoke(iftAr);
                if (Algorithm.GpsDatas.Length > 0)
                {
                    Application.DoEvents();

                    SelectItem(mRowPrev);
                    noData = false;
                }
                Algorithm.SetEventAlgoritmFinished();
                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                isButtonStartPush = true;
                Select(curMrow);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            } // else
            Algorithm.IsSuppressReportsIndication = false;
            Algorithm.RunFromModule = false;
        }
        /// <summary>
        /// ��������� ������ � ������ ������
        /// </summary>
        private GpsData[] EnvokenGetData(int mobitelId, DateTime begin, DateTime end)
        {
            var ds = new atlantaDataSet();
            var data = DataSetManager.GetGpsDataFromDb(ds, mobitelId, begin, end);
            if (data == null) return null;
            ds.Dispose();
            GC.Collect();
            return data.ToArray();
            //return DataSetManager.GetGpsDataFromDb(ds, mobitelId, begin, end).ToArray();
        }

        protected virtual void checkAddingInfo_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            return;
        }

        protected virtual void btnDrivers_ItemClick(object sender, ItemClickEventArgs e)
        {
            return;
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportAllDevToReportExtend();
        }

        protected virtual void ExportAllDevToReportMod()
        {
            return;
        }

        private void barButtonItem2_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            ExportAllDevToReportMod();
        }

        protected virtual void comboAlgoChoiser_ItemClick(object sender, ItemClickEventArgs e)
        {
            // to do this
        }

        protected virtual void btnAgregat_ItemClick(object sender, ItemClickEventArgs e)
        {
            // to do this
            return;
        }
    } // BaseControl
} // BaseReports.ReportsDE
