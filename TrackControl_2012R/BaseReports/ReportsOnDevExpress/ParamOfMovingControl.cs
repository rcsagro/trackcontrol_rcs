﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Charts.Native;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace BaseReports.ReportsDE
{
    [ToolboxItem( false )]
    public partial class ParamOfMovingControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportParamMove
        {
            string location = "";
            string describe_event = "";
            string time;
            TimeSpan duration = new TimeSpan(0);
            double maxvalue = 0.0;
            double middle_value = 0.0;

            public reportParamMove(string location, string describe_event, string time,
                TimeSpan duration, double maxvalue, double middle_value)
            {
                this.location = location;
                this.describe_event = describe_event;
                this.time = time;
                this.duration = duration;
                this.maxvalue = maxvalue;
                this.middle_value = middle_value;
            } // reportParamMove

            //[DisplayName( "Местоположение" )]
            [Developer(0)]
            public string LocationAuto
            {
                get { return location; }
            }

            //[DisplayName( "Событие" )]
            [Developer(1)]
            public string DescribeEvent
            {
                get { return describe_event; }
            }

            //[DisplayName( "Дата и время" )]
            [Developer(2)]
            public string Data
            {
                get { return time; }
            }

            //[DisplayName( "Длительность" )]
            [Developer(3)]
            public TimeSpan Duration
            {
                get { return duration; }
            }

            //[DisplayName( "Максимальное значение" )]
            [Developer(4)]
            public double MaxValue
            {
                get { return maxvalue; }
            }

            //[DisplayName( "Среднее значение" )]
            [Developer(5)]
            public double MiddleValue
            {
                get { return middle_value; }
            }
        } // reportParamMove

        protected static atlantaDataSet dataset;
        ReportBase<reportParamMove, TInfo> ReportParamMoving;

        public ParamOfMovingControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gvParamMoving, gcParamMoving, null);
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "TrafficDetailReport";
            gcParamMoving.DataSource = atlantaDataSetBindingSource;
            atlantaDataSet = null;

            gvParamMoving.RowClick += new RowClickEventHandler(gvParamMoving_DoubleClick);
            gvParamMoving.CustomDrawCell +=new RowCellCustomDrawEventHandler(gvParamMoving_CustomDrawCell);

            AddAlgorithm(new ParamOfMovingAlgorithm());

            compositeReportLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportParamMoving =
                new ReportBase<reportParamMove, TInfo>(Controls, compositeReportLink1, gvParamMoving,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevParamOfMovingCtrl

        protected void gvParamMoving_DoubleClick(object sender, RowClickEventArgs e)
        {
            ShowOnMap(e.RowHandle);
            ShowOnGraph();
        }

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                _vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    EnableButton();
                }
                else
                {
                    DisableButton();
                }
            } // if
            else
            {
                DisableButton();
            }
        } // Select

        public override void ClearReport()
        {
            dataset.TrafficDetailReport.Clear();
        }

        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true;

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;

                        if (_stopRun)
                            break;
                    } // if
                } // foreach
                
                if (noData)
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                Select(curMrow);
                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                StopReport();
                _stopRun = true;

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowOnMap(gvParamMoving.GetFocusedDataSourceRowIndex());
        }

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        }

        void ShowOnGraph()
        {
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            Graph.ClearLabel();
        }

        void ShowOnMap(int index)
        {
            ReportsControl.OnClearMapObjectsNeeded();

            if (index < 0)
                return;

            //int dataGpsID = (int)gvParamMoving.GetRowCellValue( index, "DataGps_ID" );
            System.Data.DataRowView prow = (System.Data.DataRowView)gvParamMoving.GetRow( index );
            object dataGpsID = prow.Row["DataGps_ID"];

            DataRow[] rows = dataset.TrafficDetailReport.Select(string.Format("DataGps_ID = {0}", dataGpsID));
            if (rows.Length == 0) return;
            atlantaDataSet.TrafficDetailReportRow row = (atlantaDataSet.TrafficDetailReportRow)rows[0];

            //atlantaDataSet.dataviewRow dv_row = dataset.dataview.FindByDataGps_ID(row.DataGps_ID);
            GpsData gpsData = Algorithm.GpsDatas.First(gps => gps.Id == row.DataGps_ID);
            string message = String.Format(Resources.ParamOfMovingCtrlDuration,
              row.DescribeEvent, Environment.NewLine, row.Duration);

            PointLatLng location = new PointLatLng(gpsData.LatLng.Lat + 0.0002, gpsData.LatLng.Lng + 0.0002);

            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Alarm, row.Mobitel_id, location, message, ""));

            ReportsControl.OnMarkersShowNeeded(markers);

            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.DataGps_ID, row.FinalPointId, data); 
            if (data.Count > 0)
            {
                List<Track> segments = new List<Track>();
                segments.Add(new Track(row.Mobitel_id, Color.Red, 2f, data));
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // ShowOnMap

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        void gvParamMoving_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView currentView = sender as GridView;

            string descrEvent = (string)currentView.GetRowCellValue(e.RowHandle, 
                currentView.Columns["DescribeEvent"]);

            if (descrEvent.IndexOf(Resources.CriticalAcceleration) == 0)
            {
                Brush brush = Brushes.Khaki;
                e.Graphics.FillRectangle(brush, e.Bounds);
                return;
            }

            if (descrEvent.IndexOf(Resources.CriticalBraking) == 0)
            {
                Brush brush = Brushes.DarkSalmon;
                e.Graphics.FillRectangle(brush, e.Bounds);
                return;
            }

            if (descrEvent.IndexOf(Resources.Speeding) == 0)
            {
                Brush brush = Brushes.LightCoral;
                e.Graphics.FillRectangle(brush, e.Bounds);
            }
        } // gvParamMoving_CustomDrawCell

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    TInfo t_info = new TInfo();
                    VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);

                    t_info.periodBeging = Algorithm.Period.Begin;
                    t_info.periodEnd = Algorithm.Period.End;
                    t_info.infoVehicle = info.Info;
                    t_info.infoDriverName = info.DriverFullName;

                    int numberAcc = 0;
                    int numberBreak = 0;
                    TimeSpan timeOverSpeed = new TimeSpan(0);

                    foreach (atlantaDataSet.TrafficDetailReportRow row in
                        dataset.TrafficDetailReport.Select("Mobitel_id = " + m_row.Mobitel_ID))
                    {
                        if (!row.IsDescribeEventNull())
                        {
                            if (row.DescribeEvent.IndexOf(Resources.Speeding) == 0)
                            {
                                timeOverSpeed += row.Duration;
                            }

                            if (row.DescribeEvent.IndexOf(Resources.CriticalAcceleration) == 0)
                            {
                                numberAcc++;
                            }

                            if (row.DescribeEvent.IndexOf(Resources.CriticalBraking) == 0)
                            {
                                numberBreak++;
                            }
                        } // if
                    } // foreach 1

                    t_info.timeOverSpeedLimit = timeOverSpeed;
                    t_info.numAcceleration = numberAcc;
                    t_info.numBreak = numberBreak;

                    ReportParamMoving.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                    ReportParamMoving.CreateBindDataList(); // создать новый список данных таблицы отчета

                    foreach (atlantaDataSet.TrafficDetailReportRow row in
                        dataset.TrafficDetailReport.Select("Mobitel_id = " + m_row.Mobitel_ID))
                    {
                        string location = "";
                        string describe_event = "";
                        string time = ""; // = new DateTime(); // aketner 02.11.2012
                        TimeSpan duration = new TimeSpan(0);
                        double maxvalue = 0.0;
                        double middle_value = 0.0;

                        if (!row.IsLocationNull())
                        {
                            location = row.Location;
                        }

                        if (!row.IsDescribeEventNull())
                        {
                            describe_event = row.DescribeEvent;
                        }

                        if (!row.IsTimeNull())
                        {
                            time = row.Time.ToString("dd.MM.yyyy HH:mm:ss");
                        }

                        if (!row.IsDurationNull())
                        {
                            duration = row.Duration;
                        }

                        if (!row.IsMaxValueNull())
                        {
                            maxvalue = Math.Round(row.MaxValue, 2);
                        }

                        if (!row.IsMiddleValueNull())
                        {
                            middle_value = Math.Round(row.MiddleValue, 2);
                        }

                        ReportParamMoving.AddDataToBindList(new reportParamMove(location, describe_event,
                            time, duration, maxvalue, middle_value));
                    } // foreach 1

                    ReportParamMoving.CreateElementReport();
                } // if
            } // foreach 2

            ReportParamMoving.CreateAndShowReport();
            ReportParamMoving.DeleteData();
        } // ExportAllDevToReport

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportParamMoving, e);
            TInfo info = ReportParamMoving.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportParamMoving.GetInfoStructure;
            ReportParamMoving.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней части заголовка отчета*/
        protected string GetStringBreackUp()
        {
            ReportParamMoving.SetRectangleBrckUP(380, 0, 290, 85);
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportParamMoving.GetInfoStructure;
            ReportParamMoving.SetRectangleBrckRight(670, 0, 380, 85);
            return (Resources.TotalTimeParamdSpeed + ": " + info.timeOverSpeedLimit + "\n" +
                Resources.AccelerationCriticalParam + ": " + info.numAcceleration + "\n" +
                Resources.BrakingsCriticalParam + ": " + info.numBreak);
        }

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvParamMoving, true, true);
            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);
  
            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;
            t_info.infoVehicle = info.Info;
            t_info.infoDriverName = info.DriverFullName;

            int numberAcc = 0;
            int numberBreak = 0;
            TimeSpan timeOverSpeed = new TimeSpan(0);

            foreach (atlantaDataSet.TrafficDetailReportRow row in 
                dataset.TrafficDetailReport.Select("Mobitel_id = " + curMrow.Mobitel_ID))
            {
                if (!row.IsDescribeEventNull())
                {
                    if (row.DescribeEvent.IndexOf(Resources.Speeding) == 0)
                    {
                        timeOverSpeed += row.Duration;
                    }

                    if (row.DescribeEvent.IndexOf(Resources.CriticalAcceleration) == 0)
                    {
                        numberAcc++;
                    }

                    if (row.DescribeEvent.IndexOf(Resources.CriticalBraking) == 0)
                    {
                        numberBreak++;
                    }
                } // if
            } // foreach

            t_info.timeOverSpeedLimit = timeOverSpeed;
            t_info.numAcceleration = numberAcc;
            t_info.numBreak = numberBreak;

            ReportParamMoving.AddInfoStructToList(t_info);
            ReportParamMoving.CreateAndShowReport(gcParamMoving);
        } // ExportToExcelDevExpress

        public override string Caption
        {
            get { return Resources.ParamsOfMoving; }
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvParamMoving);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvParamMoving);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcParamMoving);
        }

        private void Localization()
        {
            colLocation.Caption = Resources.Location;
            colDescribeEvent.Caption = Resources.Event;
            colTime.Caption = Resources.DateTime;
            colDuration.Caption = Resources.Duration;
            colMaxValue.Caption = Resources.MaxValue;
            colMiddleValue.Caption = Resources.AvgValue;

            colLocation.ToolTip = Resources.Location;
            colDescribeEvent.ToolTip = Resources.Event;
            colTime.ToolTip = Resources.DateTime;
            colDuration.ToolTip = Resources.Duration;
            colMaxValue.ToolTip = Resources.MaxValue;
            colMiddleValue.ToolTip = Resources.AvgValue;
        }
    } // DevParamOfMovingCtrl

    // Класс разработан для исправления вывода названий в заголовки колонок таблицы
    // во время генерации отчета
    [AttributeUsage( AttributeTargets.All )]
    public class DeveloperAttribute : DisplayNameAttribute
    {
        //Private fields.
        private string localized;
        private string devent;
        private string data;
        private string duration;
        private string maxvalue;
        private string middlevalue;
        private int val;

        //This constructor defines two required parameters: name and level.
        public DeveloperAttribute(int val)
        {
            this.val = val;
            this.localized = Resources.Location;
            this.devent = Resources.Event;
            this.data = Resources.DateTime;
            this.duration = Resources.Duration;
            this.maxvalue = Resources.MaxValue;
            this.middlevalue = Resources.AvgValue;
        }

        //Define DisplayName property
        public override string DisplayName
        {
            get
            {
                switch (val)
                {
                    case 0:
                        return this.localized;
                    case 1:
                        return this.devent;
                    case 2:
                        return this.data;
                    case 3:
                        return this.duration;
                    case 4:
                        return this.maxvalue;
                    case 5:
                        return this.middlevalue;
                    default:
                        throw new Exception("This unknown property!");
                } // switch
            } // get
        } // DisplayName
    } // DeveloperAttribute
} // BaseReports.ReportsDE
