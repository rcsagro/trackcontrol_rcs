﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.Reports.Graph;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid.Views;
using Report;

namespace BaseReports.ReportsDE
{
    [System.ComponentModel.ToolboxItem( false )]
    public partial class DevRoadSafetyCtrl : BaseReports.ReportsDE.BaseControl
    {
        int countAccel;
        int countBreak;
        TimeSpan timeSpeed;
        atlantaDataSet.mobitelsRow _mobitel;
        protected static atlantaDataSet dataset;
        ReportClass<TInfo> ReportSafetyRoad;
        List<atlantaDataSet.mobitelsRow> listCurMrow;
        private static IBuildGraphs buildGraph; // Интерфейс для построение графиков по SeriesL

        public DevRoadSafetyCtrl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableMenuFirstButton();
            VisionPanel(gvRoadSafety, gcRoadSafety, bar1);
            timeSpeed = new TimeSpan(0, 0, 0);
            dataset = ReportTabControl.Dataset;
            trafficSafetyReportBindingSource.DataSource = dataset;
            trafficSafetyReportBindingSource.DataMember = "TrafficSafetyReport";
            atlantaDataSet = null;
            buildGraph = new BuildGraphs();
            listCurMrow = new List<atlantaDataSet.mobitelsRow>();

            gvRoadSafety.RowClick += new RowClickEventHandler(gvRoadSafety_RowClick);

            compositeReportLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportSafetyRoad =
                new ReportClass<TInfo>(compositeReportLink1);

            ReportSafetyRoad.getStrBrckLeft =
                new ReportClass<TInfo>.GetStrBreackLeft(GetStringBreackLeft);

            ReportSafetyRoad.getStrBrckRight =
                new ReportClass<TInfo>.GetStrBreackRight(GetStringBreackRight);

            ReportSafetyRoad.getStrBrckUp =
                new ReportClass<TInfo>.GetStrBreackUp(GetStringBreackUp);

            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new RoadSafety());
        } // DevRoadSafetyCtrl

        public override string Caption
        {
            get
            {
                return Resources.RoadSafetyCtrlCaption;
            }
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvRoadSafety);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvRoadSafety);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcRoadSafety);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }

        // Вычисление общих данных
        private void Resume()
        {
            foreach (atlantaDataSet.TrafficSafetyReportRow row in dataset.TrafficSafetyReport)
            {
                countAccel += row.AccelCount;
                countBreak += row.BreakCount;
                timeSpeed += row.SpeedTreshold;
            }

            barAccel.Caption = String.Format(Resources.RoadSafetyCtrlAccelCountText, countAccel);
            barBreaks.Caption = String.Format(Resources.RoadSafetyCtrlBreaksCountText, countBreak);
            barTreshold.Caption = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, timeSpeed);
        } // Resume

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            _mobitel = m_row;

            if (m_row.Check)
            {
                if (trafficSafetyReportBindingSource.Count > 0)
                {
                    gcRoadSafety.DataSource = trafficSafetyReportBindingSource;
                    EnableButton();
                    SetStatusLine();
                }
            }
            else
            {
                DisableButton();
                ClearStatusLine();
                gcRoadSafety.DataSource = null;
	    }
        } // Select;

        protected void SetStatusLine()
        {
            barAccel.Caption = String.Format(Resources.RoadSafetyCtrlAccelCountText, countAccel);
            barBreaks.Caption = String.Format(Resources.RoadSafetyCtrlBreaksCountText, countBreak);
            barTreshold.Caption = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, timeSpeed);
        }

        protected void ClearStatusLine()
        {
            barAccel.Caption = String.Format(Resources.RoadSafetyCtrlAccelCountText, "---");
            barBreaks.Caption = String.Format(Resources.RoadSafetyCtrlBreaksCountText, ": ---");
            barTreshold.Caption = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, "--:--:--");
        }

        // Нажатие кнопки формирования отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                timeSpeed = new TimeSpan();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        listCurMrow.Add(m_row);
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                } // if

                gcRoadSafety.DataSource = trafficSafetyReportBindingSource;

                Resume();

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            }
        } // bbiStart_ItemClick

        // очистим структуры данных отчета
        public override void ClearReport()
        {
            dataset.TrafficSafetyReport.Clear();
            listCurMrow.Clear();
            countAccel = 0;
            countBreak = 0;

            barAccel.Caption = String.Format(Resources.RoadSafetyCtrlAccelCountText, "---");
            barBreaks.Caption = String.Format(Resources.RoadSafetyCtrlBreaksCountText, ": ---");
            barTreshold.Caption = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, "--:--:--");
        }

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
	        int indexRow = gvRoadSafety.GetFocusedDataSourceRowIndex();
            ShowTrackOnMap(indexRow); /* построить трек на карте */
        }

        // показать график
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
	        ReportsControl.OnGraphShowNeeded();
            int indexRow = gvRoadSafety.GetFocusedDataSourceRowIndex();
            SelectGraphic(listCurMrow[indexRow]);
        }

        // клик по строке таблицы
        protected void gvRoadSafety_RowClick(object sender, RowClickEventArgs e)
        {
	        if (e.RowHandle >= 0)
            {
		        int indexRow = gvRoadSafety.GetFocusedDataSourceRowIndex();
                SelectGraphic(listCurMrow[indexRow]); // перерисовать график
		        ShowTrackOnMap(indexRow); // перерисовать карту
	        }
        }

        static void showMark(PointLatLng latlng, int mobitelId, string message)
        {
            PointLatLng location = new PointLatLng(latlng.Lat, latlng.Lng);
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Report, mobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeededWith(markers);
        }
	
	    public void ShowTrackOnMap(int iRow)
	    {
            // aketner - 19.09.2012
            ReportsControl.OnClearMapObjectsNeeded();
            atlantaDataSet.mobitelsRow mobRow = listCurMrow[iRow];
            GpsData[] d_rows = DataSetManager.ConvertDataviewRowsToDataGpsArray(mobRow.GetdataviewRows());
            List<IGeoPoint> dataGeoPoint = new List<IGeoPoint>();
            List<Track> segments = new List<Track>();

            if(d_rows.Length > 0)
                dataGeoPoint.AddRange(d_rows);

            if(dataGeoPoint.Count > 0)
                segments.Add(new Track(mobRow.Mobitel_ID, Color.DarkBlue, 2f, dataGeoPoint));

            if (segments.Count > 0)
            {
                ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                showMark(segments[0].GeoPoints[segments[0].GeoPoints.Count - 1].LatLng, mobRow.Mobitel_ID, mobRow.Name);
            } // if
            
            // алгоритм устарел - aketner 19.09.2012
            /*atlantaDataSet.KilometrageReportRow klmtRow;
            DataRow[] dRow = dataset.KilometrageReport.Select("MobitelId=" + mobRow.Mobitel_ID, "Id ASC");

            for (int i = 0; i < dRow.Length; i++)
            {
                klmtRow = (atlantaDataSet.KilometrageReportRow)((DataRow)dRow[i]);
                segments.Add(GetTrackSegment(klmtRow));
            } // for

            if (segments.Count > 0)
            {
                ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                showMark(segments[segments.Count - 1].GeoPoints[0].LatLng, mobRow.Mobitel_ID, mobRow.Name);
            } // if */
        } // ShowTrackOnMap

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;

                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));

                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach

            return new Track(row.MobitelId, Color.DarkBlue, 2f, data);
        } // GetTrackSegment
	
	    public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if (mobitel_row == null)
                return;

            if (mobitel_row.GetdataviewRows() == null || mobitel_row.GetdataviewRows().Length == 0)
                return;

            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            ReportsControl.ShowGraph(mobitel_row);
        } // SelectGraphic

        // формирование отчета по текущим машинам
        protected override void ExportAllDevToReport()
        {
            if (_mobitel != null)
            {
                XtraGridService.SetupGidViewForPrint(gvRoadSafety, true, true);
                TInfo t_info = new TInfo();
                VehicleInfo info = new VehicleInfo(_mobitel);

                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                double timeOverSpeed = 0;
                TimeSpan timeOverSpeedLimit = new TimeSpan();
                int numAcceleration = 0;
                int numBreak = 0;

                foreach (atlantaDataSet.TrafficSafetyReportRow row in dataset.TrafficSafetyReport.Rows)
                {
                    timeOverSpeed += row.SpeedTreshold.TotalHours;
                    timeOverSpeedLimit += row.SpeedTreshold;
                    numAcceleration += row.AccelCount;
                    numBreak += row.BreakCount;
                } // ferach

                t_info.timeOverSpeedLimit = timeOverSpeedLimit; // time over speed
                t_info.numAcceleration = numAcceleration; // acceleration
                t_info.numBreak = numBreak; // break
                t_info.infoVehicle = info.CarModel;

                ReportSafetyRoad.AddInfoStructToList(t_info); 
                ReportSafetyRoad.CreateAndShowReport(gcRoadSafety);
            } // if
        } // ExportAllDevToReport

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportSafetyRoadTitle, e);
            TInfo info = ReportSafetyRoad.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
	        ReportSafetyRoad.SetRectangleBrckLetf(0, 0, 300, 85);
            return ("");
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
	        ReportSafetyRoad.SetRectangleBrckUP(380, 0, 320, 85);
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportSafetyRoad.GetInfoStructure;
	        ReportSafetyRoad.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.TotalTimeTresholdSpeed + ": " + info.timeOverSpeedLimit + "\n" +
                Resources.AccelerationCriticalAll + ": " + info.numAcceleration + "\n" +
                Resources.BrakingsCriticalAll + ": " + info.numBreak);
        }

        protected void Localization()
        {
            barAccel.Caption = String.Format(Resources.RoadSafetyCtrlAccelCountText, "---");
            barBreaks.Caption = String.Format(Resources.RoadSafetyCtrlBreaksCountText, ": ---");
            barTreshold.Caption = String.Format(Resources.RoadSafetyCtrlSpeedingTimeText, "--:--:--");

            colMark.Caption = Resources.VehicleBrandName;
            colMark.ToolTip = Resources.VehicleBrandNameHint;

            colModel.Caption = Resources.VehicleModel;
            colModel.ToolTip = Resources.VehicleModelHint;

            colNumberPlate.Caption = Resources.NumberPlate;
            colNumberPlate.ToolTip = Resources.NumberPlateHint;

            colDriver.Caption = Resources.FullNameShortForm;
            colDriver.ToolTip = Resources.FullNameHint;

            colTravel.Caption = Resources.DistanceShort;
            colTravel.ToolTip = Resources.DistanceHint;

            colSpeedMax.Caption = Resources.MaxSpeed;
            colSpeedMax.ToolTip = Resources.MaxSpeedHint;

            colAccelMax.Caption = Resources.MaxAcceleration;
            colAccelMax.ToolTip = Resources.MaxAccelerationHint;

            colBreakMax.Caption = Resources.MaxBraking;
            colBreakMax.ToolTip = Resources.MaxBrakingHint;

            colAccelCount.Caption = Resources.AccelerationsCount;
            colAccelCount.ToolTip = Resources.AccelerationsCountHint;

            colBreakCount.Caption = Resources.BrakingsCount;
            colBreakCount.ToolTip = Resources.BrakingsCountHint;

            colSpeedTreshold.Caption = Resources.SpeedingTime;
            colSpeedTreshold.ToolTip = Resources.SpeedingTimeHint;

            colKa.Caption = Resources.KaCaption;
            colKa.ToolTip = Resources.KaHint;

            colKb.Caption = Resources.KbCaption;
            colKb.ToolTip = Resources.KbHint;

            colKv.Caption = Resources.KvCaption;
            colKv.ToolTip = Resources.KvHint;
        } // Localization
    } // DevRoadSafetyCtrl
} // BaseReports.ReportsDE
