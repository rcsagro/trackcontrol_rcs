namespace BaseReports.ReportsDE
{
    partial class BaseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseControl));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.bchSafetyMode = new DevExpress.XtraBars.BarCheckItem();
            this.bbiShowOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowOnGraf = new DevExpress.XtraBars.BarButtonItem();
            this.btnDrivers = new DevExpress.XtraBars.BarButtonItem();
            this.btnAgregat = new DevExpress.XtraBars.BarButtonItem();
            this.checkAddingInfo = new DevExpress.XtraBars.BarCheckItem();
            this.comboAlgoChoiser = new DevExpress.XtraBars.BarEditItem();
            this.repAlgoChoiser = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.bbiPrintReport = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barCurrentReport = new DevExpress.XtraBars.BarButtonItem();
            this.barAllReport = new DevExpress.XtraBars.BarButtonItem();
            this.barReportExtend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonShowing = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu2 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonGroupPanel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonFooterPanel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonNavigator = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonStatusPanel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiReportDetal = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReportTotal = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiStart,
            this.bbiShowOnMap,
            this.bbiShowOnGraf,
            this.bbiReportDetal,
            this.bbiReportTotal,
            this.bbiPrintReport,
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barCurrentReport,
            this.barAllReport,
            this.barButtonGroupPanel,
            this.barButtonFooterPanel,
            this.barButtonNavigator,
            this.barButtonStatusPanel,
            this.barButtonShowing,
            this.barButtonItem1,
            this.bchSafetyMode,
            this.checkAddingInfo,
            this.btnDrivers,
            this.barReportExtend,
            this.barButtonItem2,
            this.comboAlgoChoiser,
            this.btnAgregat});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repAlgoChoiser});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchSafetyMode),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiShowOnMap, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiShowOnGraf, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDrivers),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAgregat, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.checkAddingInfo, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.comboAlgoChoiser),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiPrintReport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonShowing)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "����";
            this.bbiStart.Enabled = false;
            this.bbiStart.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStart.Glyph")));
            this.bbiStart.Id = 0;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // bchSafetyMode
            // 
            this.bchSafetyMode.Caption = "Safety mode";
            this.bchSafetyMode.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.bchSafetyMode.Id = 21;
            this.bchSafetyMode.Name = "bchSafetyMode";
            this.bchSafetyMode.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiShowOnMap
            // 
            this.bbiShowOnMap.Caption = "�������� �� �����";
            this.bbiShowOnMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowOnMap.Glyph")));
            this.bbiShowOnMap.Id = 1;
            this.bbiShowOnMap.Name = "bbiShowOnMap";
            this.bbiShowOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowOnMap_ItemClick);
            // 
            // bbiShowOnGraf
            // 
            this.bbiShowOnGraf.Caption = "�������� �� �������";
            this.bbiShowOnGraf.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowOnGraf.Glyph")));
            this.bbiShowOnGraf.Id = 2;
            this.bbiShowOnGraf.Name = "bbiShowOnGraf";
            this.bbiShowOnGraf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowOnGraf_ItemClick);
            // 
            // btnDrivers
            // 
            this.btnDrivers.Caption = "��������";
            this.btnDrivers.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDrivers.Glyph")));
            this.btnDrivers.GlyphDisabled = ((System.Drawing.Image)(resources.GetObject("btnDrivers.GlyphDisabled")));
            this.btnDrivers.Hint = "�������� ���� ���������";
            this.btnDrivers.Id = 23;
            this.btnDrivers.Name = "btnDrivers";
            this.btnDrivers.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnDrivers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDrivers_ItemClick);
            // 
            // btnAgregat
            // 
            this.btnAgregat.Caption = "��������";
            this.btnAgregat.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAgregat.Glyph")));
            this.btnAgregat.GlyphDisabled = ((System.Drawing.Image)(resources.GetObject("btnAgregat.GlyphDisabled")));
            this.btnAgregat.Id = 30;
            this.btnAgregat.Name = "btnAgregat";
            toolTipTitleItem1.Text = "��������";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "�������� �������� �� �����";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnAgregat.SuperTip = superToolTip1;
            this.btnAgregat.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnAgregat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAgregat_ItemClick);
            // 
            // checkAddingInfo
            // 
            this.checkAddingInfo.Caption = "For 1C";
            this.checkAddingInfo.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.checkAddingInfo.Hint = "Adding info for 1C";
            this.checkAddingInfo.Id = 22;
            this.checkAddingInfo.Name = "checkAddingInfo";
            this.checkAddingInfo.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.checkAddingInfo.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.checkAddingInfo_CheckedChanged);
            // 
            // comboAlgoChoiser
            // 
            this.comboAlgoChoiser.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.comboAlgoChoiser.Caption = "����� ���������";
            this.comboAlgoChoiser.Edit = this.repAlgoChoiser;
            this.comboAlgoChoiser.Id = 26;
            this.comboAlgoChoiser.Name = "comboAlgoChoiser";
            this.comboAlgoChoiser.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.comboAlgoChoiser.Width = 175;
            // 
            // repAlgoChoiser
            // 
            this.repAlgoChoiser.AutoHeight = false;
            this.repAlgoChoiser.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repAlgoChoiser.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repAlgoChoiser.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repAlgoChoiser.Name = "repAlgoChoiser";
            this.repAlgoChoiser.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // bbiPrintReport
            // 
            this.bbiPrintReport.ActAsDropDown = true;
            this.bbiPrintReport.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiPrintReport.Caption = "������/�������";
            this.bbiPrintReport.DropDownControl = this.popupMenu1;
            this.bbiPrintReport.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrintReport.Glyph")));
            this.bbiPrintReport.Id = 9;
            this.bbiPrintReport.Name = "bbiPrintReport";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCurrentReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.barAllReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.barReportExtend),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barCurrentReport
            // 
            this.barCurrentReport.Caption = "������� ������";
            this.barCurrentReport.Id = 13;
            this.barCurrentReport.Name = "barCurrentReport";
            this.barCurrentReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCurrentReport_ItemClick);
            // 
            // barAllReport
            // 
            this.barAllReport.Caption = "�� ���� �������";
            this.barAllReport.Id = 14;
            this.barAllReport.Name = "barAllReport";
            this.barAllReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barAllReport_ItemClick);
            // 
            // barReportExtend
            // 
            this.barReportExtend.Caption = "�� ���� ������� �����������";
            this.barReportExtend.Hint = "�� ���� ������� � ����������� ��������";
            this.barReportExtend.Id = 24;
            this.barReportExtend.Name = "barReportExtend";
            this.barReportExtend.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barReportExtend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "�� ���� �������, ���";
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick_1);
            // 
            // barButtonShowing
            // 
            this.barButtonShowing.ActAsDropDown = true;
            this.barButtonShowing.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonShowing.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonShowing.Caption = "���������� ��������";
            this.barButtonShowing.DropDownControl = this.popupMenu2;
            this.barButtonShowing.Id = 19;
            this.barButtonShowing.Name = "barButtonShowing";
            this.barButtonShowing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonShowing_ItemClick);
            // 
            // popupMenu2
            // 
            this.popupMenu2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonGroupPanel),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonFooterPanel),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonNavigator),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonStatusPanel)});
            this.popupMenu2.Manager = this.barManager1;
            this.popupMenu2.Name = "popupMenu2";
            // 
            // barButtonGroupPanel
            // 
            this.barButtonGroupPanel.Caption = "�������� ������ �����������";
            this.barButtonGroupPanel.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonGroupPanel.Glyph")));
            this.barButtonGroupPanel.Id = 15;
            this.barButtonGroupPanel.Name = "barButtonGroupPanel";
            this.barButtonGroupPanel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonGroupPanel_ItemClick);
            // 
            // barButtonFooterPanel
            // 
            this.barButtonFooterPanel.Caption = "�������� �������� ������";
            this.barButtonFooterPanel.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonFooterPanel.Glyph")));
            this.barButtonFooterPanel.Id = 16;
            this.barButtonFooterPanel.Name = "barButtonFooterPanel";
            this.barButtonFooterPanel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonFooterPanel_ItemClick);
            // 
            // barButtonNavigator
            // 
            this.barButtonNavigator.Caption = "�������� ���������";
            this.barButtonNavigator.Id = 17;
            this.barButtonNavigator.Name = "barButtonNavigator";
            this.barButtonNavigator.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonNavigator_ItemClick);
            // 
            // barButtonStatusPanel
            // 
            this.barButtonStatusPanel.Caption = "������ ��������� ������";
            this.barButtonStatusPanel.Id = 18;
            this.barButtonStatusPanel.Name = "barButtonStatusPanel";
            this.barButtonStatusPanel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonStatusPanel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1097, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 380);
            this.barDockControlBottom.Size = new System.Drawing.Size(1097, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 356);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1097, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 356);
            // 
            // bbiReportDetal
            // 
            this.bbiReportDetal.Caption = "���������";
            this.bbiReportDetal.Id = 5;
            this.bbiReportDetal.Name = "bbiReportDetal";
            // 
            // bbiReportTotal
            // 
            this.bbiReportTotal.Caption = "�������";
            this.bbiReportTotal.Id = 6;
            this.bbiReportTotal.Name = "bbiReportTotal";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "������� ������";
            this.barSubItem1.Id = 10;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "barSubItem2";
            this.barSubItem2.Id = 11;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "barSubItem3";
            this.barSubItem3.Id = 12;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "�������� ������ ������";
            this.barButtonItem1.Id = 20;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // BaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "BaseControl";
            this.Size = new System.Drawing.Size(1097, 403);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.XtraBars.BarButtonItem bbiStart;
        protected DevExpress.XtraBars.BarButtonItem bbiShowOnMap;
        protected DevExpress.XtraBars.BarButtonItem bbiShowOnGraf;
        private DevExpress.XtraBars.BarButtonItem bbiReportDetal;
        private DevExpress.XtraBars.BarButtonItem bbiReportTotal;
        protected DevExpress.XtraBars.BarButtonItem bbiPrintReport;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem barCurrentReport;
        private DevExpress.XtraBars.BarButtonItem barAllReport;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        protected DevExpress.XtraBars.BarButtonItem barButtonGroupPanel;
        protected DevExpress.XtraBars.BarButtonItem barButtonFooterPanel;
        protected DevExpress.XtraBars.BarButtonItem barButtonNavigator;
        protected DevExpress.XtraBars.BarButtonItem barButtonStatusPanel;
        private DevExpress.XtraBars.PopupMenu popupMenu2;
        protected DevExpress.XtraBars.BarButtonItem barButtonShowing;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraBars.BarCheckItem bchSafetyMode;
        public DevExpress.XtraBars.BarCheckItem checkAddingInfo;
        protected DevExpress.XtraBars.BarButtonItem btnDrivers;
        private DevExpress.XtraBars.BarButtonItem barReportExtend;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        protected DevExpress.XtraBars.BarEditItem comboAlgoChoiser;
        protected DevExpress.XtraEditors.Repository.RepositoryItemComboBox repAlgoChoiser;
        protected DevExpress.XtraBars.BarButtonItem btnAgregat;
    }
}
