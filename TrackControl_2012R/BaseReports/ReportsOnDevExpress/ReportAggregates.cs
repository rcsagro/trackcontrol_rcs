﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Utils;
using DevExpress.XtraBars;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using DevExpress.XtraPrinting;
using Report;
using TrackControl.GMap;

namespace BaseReports.ReportsOnDevExpress
{
    public partial class ReportAggregates : BaseReports.ReportsDE.BaseControl
    {
        struct GraphData
        {
            public double ValueY;
            public DateTime ValueX;

            public GraphData(int valueY, DateTime valueX)
            {
                ValueX = valueX;
                ValueY = valueY;
            }
        }

        public class TAggregateRfid
        {
            private string nameForReport;
            private string identRfidName;
            private string locationStart;
            private DateTime timeStart;
            private string locationEnd;
            private DateTime timeEnd;
            private TimeSpan timeDuration;
            private double distance;
            private TimeSpan timeStops;

            public string NameForReport
            {
                get { return nameForReport; }
                set { nameForReport = value; }
            }

            public string IdentRfidName
            {
                get { return identRfidName; }
                set { identRfidName = value; }
            }

            public string LocationStart
            {
                get { return locationStart; }
                set { locationStart = value; }
            }

            public DateTime TimeStart
            {
                get { return timeStart; }
                set { timeStart = value; }
            }

            public string LocationEnd
            {
                get { return locationEnd; }
                set { locationEnd = value; }
            }

            public DateTime TimeEnd
            {
                get { return timeEnd; }
                set { timeEnd = value; }
            }

            public TimeSpan TimeDuration
            {
                get { return timeDuration; }
                set { timeDuration = value; }
            }

            public double Distance
            {
                get { return distance; }
                set { distance = value; }
            }

            public TimeSpan TimeStops
            {
                get { return timeStops; }
                set { timeStops = value; }
            }
        }

        private const string GRAPH_SERIES_NAME = "Aggregate RFID";
        private AggregateRfid _algrithm;
        private List<AggregateRFIDRecord> _records;
        private GpsData[] _dRows;

        private bool _expandAllGroups = true;

        private ReportBase<TAggregateRfid, TInfo> ReportAggregateRfid;

        private bool _flagShow = false;
        private bool _allShow = false;

        public ReportAggregates()
        {
            InitializeComponent();
            HideStatusBar();
            VisionPanel(gvAggregate, gcAggregate, null);
            btnAgregat.Visibility = BarItemVisibility.Always;
            _algrithm = new AggregateRfid();
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(_algrithm);
            SetActiveToolBar(false);
            Localization();
            gvAggregate.DoubleClick += gvAggregate_DoubleClick;
            gvAggregate.FocusedRowChanged += gvAggregate_FocusedRowChanged;
            compositeLink2.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(linkGrid_CreateMarginalHeaderArea);
            ReportAggregateRfid = new ReportBase<TAggregateRfid, TInfo>(Controls, compositeLink2, gvAggregate,
                GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        public override string Caption
        {
            get { return Resources.AggregateRFID; }
        }

        public override void ClearReport()
        {
            gcAggregate.DataSource = null;
            SetActiveToolBar(false);
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if(sender is BaseReports.Procedure.AggregateRfid)
                Select(curMrow);
        }

        protected override void btnAgregat_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_flagShow)
            {
                ShowOnMapAndGraphWithoutMarker();
                btnAgregat.Hint = "Показать все агрегаты на карте";
                _flagShow = false;
                _allShow = false;
            }
            else
            {
                ShowOnMapAndGraph();
                btnAgregat.Hint = "Скрыть все агрегаты с карты";
            }
        }

        void SetActiveToolBar(bool active)
        {
            bbiPrintReport.Enabled = active;
            bbiShowOnMap.Enabled = active;
            bbiShowOnGraf.Enabled = active;
            btnAgregat.Enabled = active;
            btnAgregat.Hint = "Показать все агрегаты на карте";
            _flagShow = false;
            _allShow = false;
            if(active)
                SetStartButton();
        }

        bool AddGraphRFID()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if (cm.Count == 0) return false;
            List<GraphData> graphRFID = new List<GraphData>();
            foreach (AggregateRFIDRecord recRfid in cm.List)
            {
                List<GraphData> gds = CreateImpuls(recRfid);
                foreach (GraphData gd in gds)
                {
                    graphRFID.Add(gd);
                }
            }

            double[] Y = new double[graphRFID.Count];
            DateTime[] X = new DateTime[graphRFID.Count];
            int indexXY = 0;
            foreach (GraphData gd in graphRFID)
            {
                Y[indexXY] = gd.ValueY;
                X[indexXY] = gd.ValueX;
                indexXY++;
            }

            Graph.AddSeriesL(GRAPH_SERIES_NAME, Color.Brown, Y, X, AlgorithmType.AGGREGAT_WORK, true);
            return true;
        }

        private void ShowOnMapAndGraphOne()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if (cm.Count == 0) return;
            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();
            int i = 0;
            AggregateRFIDRecord recRfidCurrent = (AggregateRFIDRecord)cm.Current;
            foreach (AggregateRFIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
                Marker m = new Marker(MarkerType.RFID, recRfid.IdMobitel,
                    new PointLatLng(recRfid.StartGps.LatLng.Lat, recRfid.StartGps.LatLng.Lng),
                    recRfid.NameForReport, "");
                if (recRfid == (AggregateRFIDRecord)cm.Current)
                {
                    m.IsActive = true;
                    markers.Add(m);
                    continue;
                }
                if(_allShow)
                    markers.Add(m);
            } // foreach
            Graph.ClearRegion();
            Graph.AddTimeRegion(recRfidCurrent.TimeStart, recRfidCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);
            segments.Add(GetTrackSegment(recRfidCurrent, Color.Red));
            if(segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        private void ShowOnMapAndGraph()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if(cm.Count == 0) return;
            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();
            int i = 0;
            AggregateRFIDRecord recRfidCurrent = (AggregateRFIDRecord)cm.Current;
            foreach (AggregateRFIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
                Marker m = new Marker(MarkerType.RFID, recRfid.IdMobitel,
                    new PointLatLng(recRfid.StartGps.LatLng.Lat, recRfid.StartGps.LatLng.Lng),
                    recRfid.NameForReport, "");
                if (recRfid == (AggregateRFIDRecord)cm.Current)
                    m.IsActive = true;
                markers.Add(m);
            } // foreach
            Graph.ClearRegion();
            Graph.AddTimeRegion(recRfidCurrent.TimeStart, recRfidCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);
            segments.Add(GetTrackSegment(recRfidCurrent, Color.Red));
            if(segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        private void ShowOnMapAndGraphWithoutMarker()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[_records];
            if(cm.Count == 0) return;
            List<Track> segments = new List<Track>();
            List<Marker> markers = new List<Marker>();
            int i = 0;
            AggregateRFIDRecord recRfidCurrent = (AggregateRFIDRecord)cm.Current;
            foreach (AggregateRFIDRecord recRfid in cm.List)
            {
                ++i;
                segments.Add(GetTrackSegment(recRfid, i % 2 == 0 ? Color.RoyalBlue : Color.Green));
            }
            
            Graph.ClearRegion();
            Graph.AddTimeRegion(recRfidCurrent.TimeStart, recRfidCurrent.TimeEnd);
            Graph.ShowSeries(GRAPH_SERIES_NAME);
            if(segments.Count > 0)
                ReportsControl.OnTrackSegmetsShowWithMarkers(segments, markers);
        }

        Track GetTrackSegment(AggregateRFIDRecord recRfid, Color color)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;
            foreach (GpsData dGps in _dRows)
            {
                if (dGps.Id == recRfid.StartGps.Id)
                    inside = true;
                if(inside) data.Add(dGps);
                if (dGps.Id == recRfid.FinalGps.Id)
                    break;
            }

            if (data.Count > 0)
            {
                return new Track(recRfid.IdMobitel, color, 2f, data);
            }

            return null;
        }

        #region GUI

        private void gvAggregate_DoubleClick(object sender, EventArgs e)
        {
            ShowOnMapAndGraphOne();
        }

        private void gvAggregate_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                if(AddGraphRFID())
                    ShowOnMapAndGraphOne();
            }
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            if(AddGraphRFID())
                ShowOnMapAndGraphOne();
        }

        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowOnMapAndGraphOne();
        }

        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            base.bbiStart_ItemClick(sender, e);
        }

        #endregion

        List<GraphData> CreateImpuls(AggregateRFIDRecord recRfid)
        {
            List<GraphData> gds = new List<GraphData>();
            GraphData gd = new GraphData(recRfid.IdentRfid, recRfid.TimeStart);
            gds.Add(gd);
            gd = new GraphData(recRfid.IdentRfid, recRfid.TimeEnd);
            gds.Add(gd);
            return gds;
        }
        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            base.Select(mobitel);
            if (mobitel == null || Algorithm.AggregateRfidRecords == null || Algorithm.AggregateRfidRecords.Count == 0 ||
                !Algorithm.AggregateRfidRecords.ContainsKey(mobitel.Mobitel_ID))
            {
                ClearReport(); 
                return;
            } // if
            _records = Algorithm.AggregateRfidRecords[mobitel.Mobitel_ID];
            _dRows = DataSetManager.GetDataGpsArray(mobitel);
            gcAggregate.DataSource = _records;

            if (_records.Count > 0)
            {
                SetActiveToolBar(true);
                if (AddGraphRFID())
                    ShowOnMapAndGraphOne();
            } // if
        } // Select

        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_row in _dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    base.Select(m_row);
                    if (m_row == null || Algorithm.AggregateRfidRecords == null ||
                        Algorithm.AggregateRfidRecords.Count == 0 ||
                        !Algorithm.AggregateRfidRecords.ContainsKey(m_row.Mobitel_ID))
                    {
                        break;
                    }

                    _records = Algorithm.AggregateRfidRecords[m_row.Mobitel_ID];
                    VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);
                    TInfo t_info = new TInfo();
                    t_info.infoVehicle = info.Info;
                    t_info.infoAggregateName = info.AggregateName;
                    t_info.periodBeging = Algorithm.Period.Begin;
                    t_info.periodEnd = Algorithm.Period.End;
                    t_info.totalWay = 0.0;
                    t_info.totalParking = new TimeSpan();
                    t_info.totalTimerTour = new TimeSpan();
                    TimeSpan durationAll = new TimeSpan();

                    ReportAggregateRfid.CreateBindDataList();
                    for (int i = 0; i < _records.Count; i++)
                    {
                        TAggregateRfid taggregateRfid = new TAggregateRfid();
                        AggregateRFIDRecord aggregateRfid = _records[i];
                        taggregateRfid.NameForReport = aggregateRfid.NameForReport;
                        taggregateRfid.IdentRfidName = aggregateRfid.IdentRfidName;
                        taggregateRfid.LocationStart = aggregateRfid.LocationStart;
                        taggregateRfid.TimeStart = aggregateRfid.TimeStart;
                        taggregateRfid.LocationEnd = aggregateRfid.LocationEnd;
                        taggregateRfid.TimeEnd = aggregateRfid.TimeEnd;
                        taggregateRfid.TimeDuration = aggregateRfid.TimeDuration;
                        taggregateRfid.Distance = aggregateRfid.Distance;
                        taggregateRfid.TimeStops = aggregateRfid.TimeStops;
                        
                        t_info.totalWay += aggregateRfid.Distance;
                        t_info.totalParking += aggregateRfid.TimeStops;
                        durationAll += aggregateRfid.TimeDuration;
                        t_info.totalTimerTour = new TimeSpan();

                        ReportAggregateRfid.AddDataToBindList(taggregateRfid);
                    } // for

                    t_info.totalTimerTour = durationAll - t_info.totalParking;
                    ReportAggregateRfid.AddInfoStructToList(t_info);
                    
                    ReportAggregateRfid.CreateElementReport();
                } //if
            } // foreach
            ReportAggregateRfid.CreateAndShowReport();
            ReportAggregateRfid.DeleteData();
        } // ExportAllDevToReport

        private void linkGrid_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.AggregateRfides, e);
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);;
            string strPeriod = string.Format("{3} {1} {4} {0}\n{5}: {2}", Algorithm.Period.End, Algorithm.Period.Begin,
                info.Info, Resources.PeriodFrom, Resources.PeriodTo, Resources.Vehicle);
            DevExpressReportSubHeader(strPeriod, 30, e);
        }

        protected string GetStringBreackUp()
        {
            return "";
        }

        protected string GetStringBreackLeft()
        {
            TInfo t_info = ReportAggregateRfid.GetInfoStructure;
            return Resources.AggregateTotalWay + " " + Math.Round(t_info.totalWay, 2) + "\n" + Resources.AggregateTotalMove + " " + t_info.totalTimerTour + "\n" + 
                   Resources.AggregateTotalStay + " " + t_info.totalParking;
        }

        protected string GetStringBreackRight()
        {
            return "";
        }

        protected override void ExportToExcelDevExpress()
        {
            atlantaDataSet.mobitelsRow m_row = curMrow;
            if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
            {
                base.Select(m_row);
                if (m_row == null || Algorithm.AggregateRfidRecords == null ||
                    Algorithm.AggregateRfidRecords.Count == 0 ||
                    !Algorithm.AggregateRfidRecords.ContainsKey(m_row.Mobitel_ID))
                {
                    return;
                }

                TimeSpan time_Duration = new TimeSpan();

                _records = Algorithm.AggregateRfidRecords[m_row.Mobitel_ID];
                VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);
                TInfo t_info = new TInfo();
                t_info.totalWay = 0.0;
                t_info.totalTimerTour = new TimeSpan();
                t_info.totalParking = new TimeSpan();
                t_info.infoVehicle = info.Info;
                t_info.infoAggregateName = info.AggregateName;
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportAggregateRfid.CreateBindDataList();
                
                for (int i = 0; i < _records.Count; i++)
                {
                    TAggregateRfid taggregateRfid = new TAggregateRfid();
                    AggregateRFIDRecord aggregateRfid = _records[i];
                    taggregateRfid.NameForReport = aggregateRfid.NameForReport;
                    taggregateRfid.IdentRfidName = aggregateRfid.IdentRfidName;
                    taggregateRfid.LocationStart = aggregateRfid.LocationStart;
                    taggregateRfid.TimeStart = aggregateRfid.TimeStart;
                    taggregateRfid.LocationEnd = aggregateRfid.LocationEnd;
                    taggregateRfid.TimeEnd = aggregateRfid.TimeEnd;
                    taggregateRfid.TimeDuration = aggregateRfid.TimeDuration;
                    time_Duration += aggregateRfid.TimeDuration;
                    taggregateRfid.Distance = aggregateRfid.Distance;
                    t_info.totalWay += aggregateRfid.Distance;
                    taggregateRfid.TimeStops = aggregateRfid.TimeStops;
                    t_info.totalParking += aggregateRfid.TimeStops;

                    ReportAggregateRfid.AddDataToBindList(taggregateRfid);
                } // for

                t_info.totalTimerTour = (time_Duration - t_info.totalParking);
                ReportAggregateRfid.AddInfoStructToList(t_info);
                
                ReportAggregateRfid.CreateElementReport();
            } //if
            ReportAggregateRfid.CreateAndShowReport();
            ReportAggregateRfid.DeleteData();
        }

        void Localization()
        {
            colNameForReport.Caption = Resources.Aggregate;
            colLocationStart.Caption = Resources.StartLocation;
            colLocationEnd.Caption = Resources.MovementEndPlace;
            colTimeStart.Caption = Resources.ShiftStart;
            colTimeEnd.Caption = Resources.ShiftEnd;
            colTimeDuration.Caption = Resources.ShiftDuration;
            colTimeStops.Caption = Resources.TimeStops;
            colDistance.Caption = Resources.Kilometrage;
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gvAggregate);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gvAggregate);
        }

        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gcAggregate);
        }
    }
}
