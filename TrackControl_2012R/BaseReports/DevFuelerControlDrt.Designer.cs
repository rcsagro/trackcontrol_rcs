﻿namespace BaseReports
{
    partial class DevFuelerControlDrt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevFuelerControlDrt ) );
            this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            this.fuelerDataCG = new DevExpress.XtraGrid.GridControl();
            this.fuelerDataVG = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfillUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchangeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltankFuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltimeStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltimeEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource( this.components );
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelerDataCG ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelerDataVG ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.atlantaDataSetBindingSource ) ).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar3} );
            this.barManager1.DockControls.Add( this.barDockControl1 );
            this.barManager1.DockControls.Add( this.barDockControl2 );
            this.barManager1.DockControls.Add( this.barDockControl3 );
            this.barManager1.DockControls.Add( this.barDockControl4 );
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2} );
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2)} );
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Общий расход: 0;";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Сумма расхода, меньше мин.: 0;";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins( 10, 10, 70, 10 );
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins( 10, 10, 15, 10 );
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // fuelerDataCG
            // 
            this.fuelerDataCG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fuelerDataCG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelerDataCG.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelerDataCG.Location = new System.Drawing.Point( 0, 26 );
            this.fuelerDataCG.MainView = this.fuelerDataVG;
            this.fuelerDataCG.MenuManager = this.barManager1;
            this.fuelerDataCG.Name = "fuelerDataCG";
            this.fuelerDataCG.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1} );
            this.fuelerDataCG.Size = new System.Drawing.Size( 924, 432 );
            this.fuelerDataCG.TabIndex = 10;
            this.fuelerDataCG.UseEmbeddedNavigator = true;
            this.fuelerDataCG.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelerDataVG,
            this.gridView2} );
            // 
            // fuelerDataVG
            // 
            this.fuelerDataVG.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.fuelerDataVG.Appearance.Empty.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.EvenRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.fuelerDataVG.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.fuelerDataVG.Appearance.FilterPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FilterPanel.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelerDataVG.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.FocusedRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FocusedRow.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelerDataVG.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelerDataVG.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupButton.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupButton.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupFooter.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.GroupPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupPanel.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.fuelerDataVG.Appearance.GroupRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.GroupRow.Options.UseFont = true;
            this.fuelerDataVG.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.fuelerDataVG.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.fuelerDataVG.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.fuelerDataVG.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.fuelerDataVG.Appearance.HorzLine.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.OddRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.fuelerDataVG.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.Preview.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.Preview.Options.UseForeColor = true;
            this.fuelerDataVG.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.fuelerDataVG.Appearance.Row.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.RowSeparator.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.fuelerDataVG.Appearance.SelectedRow.Options.UseBackColor = true;
            this.fuelerDataVG.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.fuelerDataVG.Appearance.VertLine.Options.UseBackColor = true;
            this.fuelerDataVG.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.fuelerDataVG.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.fuelerDataVG.ColumnPanelRowHeight = 40;
            this.fuelerDataVG.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLocation,
            this.colfillUp,
            this.colDriverName,
            this.colchangeValue,
            this.coltankFuelAdd,
            this.coltimeStart,
            this.coltimeEnd} );
            this.fuelerDataVG.GridControl = this.fuelerDataCG;
            this.fuelerDataVG.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.fuelerDataVG.Name = "fuelerDataVG";
            this.fuelerDataVG.OptionsDetail.AllowZoomDetail = false;
            this.fuelerDataVG.OptionsDetail.EnableMasterViewMode = false;
            this.fuelerDataVG.OptionsDetail.ShowDetailTabs = false;
            this.fuelerDataVG.OptionsDetail.SmartDetailExpand = false;
            this.fuelerDataVG.OptionsSelection.MultiSelect = true;
            this.fuelerDataVG.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelerDataVG.OptionsView.EnableAppearanceOddRow = true;
            this.fuelerDataVG.OptionsView.ShowFooter = true;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceCell.Options.UseTextOptions = true;
            this.colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLocation.Caption = "Место положения";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.ToolTip = "Место положения";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 0;
            this.colLocation.Width = 70;
            // 
            // colfillUp
            // 
            this.colfillUp.AppearanceCell.Options.UseTextOptions = true;
            this.colfillUp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colfillUp.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colfillUp.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colfillUp.AppearanceHeader.Options.UseTextOptions = true;
            this.colfillUp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colfillUp.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colfillUp.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colfillUp.Caption = "Заправленный транспорт";
            this.colfillUp.FieldName = "fillUp";
            this.colfillUp.Name = "colfillUp";
            this.colfillUp.OptionsColumn.AllowEdit = false;
            this.colfillUp.OptionsColumn.AllowFocus = false;
            this.colfillUp.OptionsColumn.ReadOnly = true;
            this.colfillUp.ToolTip = "Заправленный транспорт";
            this.colfillUp.Visible = true;
            this.colfillUp.VisibleIndex = 1;
            this.colfillUp.Width = 200;
            // 
            // colDriverName
            // 
            this.colDriverName.AppearanceCell.Options.UseTextOptions = true;
            this.colDriverName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriverName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriverName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDriverName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDriverName.Caption = "Получатель";
            this.colDriverName.FieldName = "driverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.AllowFocus = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.ToolTip = "Получатель";
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 2;
            this.colDriverName.Width = 90;
            // 
            // colchangeValue
            // 
            this.colchangeValue.AppearanceCell.Options.UseTextOptions = true;
            this.colchangeValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colchangeValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colchangeValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colchangeValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colchangeValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colchangeValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colchangeValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colchangeValue.Caption = "Величина зправки по ДРТ, л";
            this.colchangeValue.DisplayFormat.FormatString = "N2";
            this.colchangeValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colchangeValue.FieldName = "changeValue";
            this.colchangeValue.Name = "colchangeValue";
            this.colchangeValue.OptionsColumn.AllowEdit = false;
            this.colchangeValue.OptionsColumn.AllowFocus = false;
            this.colchangeValue.OptionsColumn.ReadOnly = true;
            this.colchangeValue.SummaryItem.DisplayFormat = "{0:f2}";
            this.colchangeValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colchangeValue.ToolTip = "Величина зправки по ДРТ, л";
            this.colchangeValue.Visible = true;
            this.colchangeValue.VisibleIndex = 3;
            this.colchangeValue.Width = 90;
            // 
            // coltankFuelAdd
            // 
            this.coltankFuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.coltankFuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltankFuelAdd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltankFuelAdd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltankFuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltankFuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltankFuelAdd.Caption = "Заправлено в бак ТС по ДУТ, л";
            this.coltankFuelAdd.DisplayFormat.FormatString = "N2";
            this.coltankFuelAdd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.coltankFuelAdd.FieldName = "tankFuelAdd";
            this.coltankFuelAdd.Name = "coltankFuelAdd";
            this.coltankFuelAdd.OptionsColumn.AllowEdit = false;
            this.coltankFuelAdd.OptionsColumn.AllowFocus = false;
            this.coltankFuelAdd.OptionsColumn.ReadOnly = true;
            this.coltankFuelAdd.SummaryItem.DisplayFormat = "{0:f2}";
            this.coltankFuelAdd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.coltankFuelAdd.ToolTip = "Заправлено в бак ТС по ДУТ, л";
            this.coltankFuelAdd.Visible = true;
            this.coltankFuelAdd.VisibleIndex = 4;
            this.coltankFuelAdd.Width = 90;
            // 
            // coltimeStart
            // 
            this.coltimeStart.AppearanceCell.Options.UseTextOptions = true;
            this.coltimeStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeStart.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeStart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeStart.AppearanceHeader.Options.UseTextOptions = true;
            this.coltimeStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeStart.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeStart.Caption = "Время начала";
            this.coltimeStart.DisplayFormat.FormatString = "dd.MM.yyyy hh:mm:ss";
            this.coltimeStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.coltimeStart.FieldName = "timeStart";
            this.coltimeStart.Name = "coltimeStart";
            this.coltimeStart.OptionsColumn.AllowEdit = false;
            this.coltimeStart.OptionsColumn.AllowFocus = false;
            this.coltimeStart.OptionsColumn.ReadOnly = true;
            this.coltimeStart.SummaryItem.DisplayFormat = "{0:f2}";
            this.coltimeStart.SummaryItem.FieldName = "Distance";
            this.coltimeStart.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.coltimeStart.ToolTip = "Время начала";
            this.coltimeStart.Visible = true;
            this.coltimeStart.VisibleIndex = 5;
            this.coltimeStart.Width = 50;
            // 
            // coltimeEnd
            // 
            this.coltimeEnd.AppearanceCell.Options.UseTextOptions = true;
            this.coltimeEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeEnd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeEnd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.coltimeEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltimeEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltimeEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltimeEnd.Caption = "Время окончания";
            this.coltimeEnd.DisplayFormat.FormatString = "dd.MM.yyyy hh:mm:ss";
            this.coltimeEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.coltimeEnd.FieldName = "timeEnd";
            this.coltimeEnd.Name = "coltimeEnd";
            this.coltimeEnd.OptionsColumn.AllowEdit = false;
            this.coltimeEnd.OptionsColumn.AllowFocus = false;
            this.coltimeEnd.OptionsColumn.ReadOnly = true;
            this.coltimeEnd.SummaryItem.DisplayFormat = "{0:f2}";
            this.coltimeEnd.SummaryItem.FieldName = "GeneralDistance";
            this.coltimeEnd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.coltimeEnd.ToolTip = "Время окончания";
            this.coltimeEnd.Visible = true;
            this.coltimeEnd.VisibleIndex = 6;
            this.coltimeEnd.Width = 70;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange( new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)} );
            this._imageComboRepo.Items.AddRange( new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)} );
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.fuelerDataCG;
            this.gridView2.Name = "gridView2";
            // 
            // DevFuelerControlDrt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.fuelerDataCG );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "DevFuelerControlDrt";
            this.Size = new System.Drawing.Size( 924, 507 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.fuelerDataCG, 0 );
            ( ( System.ComponentModel.ISupportInitialize )( this.barManager1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelerDataCG ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.fuelerDataVG ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this._imageComboRepo ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.atlantaDataSetBindingSource ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraGrid.GridControl fuelerDataCG;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelerDataVG;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraGrid.Columns.GridColumn colfillUp;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colchangeValue;
        private DevExpress.XtraGrid.Columns.GridColumn coltankFuelAdd;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn coltimeStart;
        private DevExpress.XtraGrid.Columns.GridColumn coltimeEnd;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
    }
}
