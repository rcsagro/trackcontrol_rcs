using System;
using System.Collections.Generic;
using System.Text;
using GPSControl.Report.Algorithm;

namespace GPSControl.Report
{
  class Stop:GPSControl.Report.Algorithm.Algorithm,IAlgorithm
  {
    public Stop():base()
    {

    }
    #region IAlgorithm Members

    void IAlgorithm.Run()
    {
      Spoint[] points =
			base.FindMin( d_rows, atlantaDataSet.dataview.speedColumn, atlantaDataSet.dataview.DataGps_IDColumn,0);
      // Проверка наличия остановок
      if (points.Length == 0)
        return;
      TimeSpan stopTime = new TimeSpan();
      TimeSpan travelTime = new TimeSpan();
      foreach ( Spoint p in points )
			{
				atlantaDataSet.StopReportRow sr_row = ( atlantaDataSet.StopReportRow )
																	atlantaDataSet.StopReport.NewRow();
				sr_row.mobitel_id = m_row.Mobitel_ID;
				sr_row.begin_id = p.X;
				sr_row.end_id = p.Y;
        //sr_row.Travel = CalcDist(ref sr_row,p.X);
        //lastId = p.Y;
				atlantaDataSet.dataviewRow row = atlantaDataSet.dataview.FindByDataGps_ID( p.X );
				sr_row.beginTime = row.time;
				sr_row.Location = row.Lat.ToString() + ";" + row.Lon.ToString();
				row = atlantaDataSet.dataview.FindByDataGps_ID( p.Y );
				sr_row.endTime = row.time;
				sr_row.Duration = sr_row.endTime - sr_row.beginTime;
        stopTime += sr_row.Duration;
				sr_row.duration_s = sr_row.Duration.TotalSeconds;
				atlantaDataSet.StopReport.AddStopReportRow( sr_row );
        
			}
      travelTime = end - begin - stopTime;
      m_row.timeTravel = travelTime;
      m_row.timeStop = stopTime;

    }

    //void IAlgorithm.SelectItem(atlantaDataSet.mobitelsRow m_row)
    //{
    //  base.SelectItem(m_row);
    //}

    #endregion
  }
}
