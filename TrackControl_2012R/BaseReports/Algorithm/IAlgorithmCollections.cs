using System;
using System.Collections.Generic;
using System.Text;

namespace GPSControl.Report.Algorithm
{
  interface IAlgorithmCollections
  {
    void AddAlgorithm(IAlgorithm algorithm);
    System.Collections.Generic.IEnumerator<IAlgorithm> GetEnumerator();
  }
}
