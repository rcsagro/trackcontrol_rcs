using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using GPSControl.Report;

namespace GPSControl.Report.Algorithm
{
    
  public class Algorithm : IAlgorithm
  {
#region Attribute


    /// <summary>
    /// ����� ������
    /// </summary>
    string mark;
    /// <summary>
    /// ������ ������� 
    /// </summary>
    string model;
    /// <summary>
    /// ���. ����� �������
    /// </summary>
    string numberPlate;
    /// <summary>
    /// ��� ��������
    /// </summary>
    string driver;
    
    static protected atlantaDataSet atlantaDataSet;
    public static atlantaDataSet AtlantaDataSet
    {
      set { atlantaDataSet = value; }
    }
    protected atlantaDataSet.mobitelsRow m_row;
    protected atlantaDataSet.dataviewRow[] d_rows;
    protected List<atlantaDataSet.dataviewRow> selRow;
		protected atlantaDataSet.sensorsRow s_row;
    internal static RCS.PlanRoutes.CheckZones.CheckZoneCollection zones;
    static protected DateTime begin, end;
    protected DateTime next;//,beginMotion, endMotion;

#endregion
    public Algorithm()
    {
      Init();
      
    }

#region Metods
    /// <summary>
    /// �������� ����������� ���
    /// </summary>
    protected void Init()
    {
      try
      {
        if (zones == null)
        {
          zones = new RCS.PlanRoutes.CheckZones.CheckZoneCollection();
          foreach (atlantaDataSet.zonesRow z_row in atlantaDataSet.zones.Rows)
            zones.AddCheckZone(new RCS.PlanRoutes.CheckZones.CheckZone(z_row));
        }
      }
      catch (Exception ex)
      {
        RW_Log.ExecuteLogging.Log("App",ex.Message+"\n�������� �� ��������������� DataSet");
      }
    }
    /// <summary>
    /// ����� ������� � ����������� ����������
    /// </summary>
    /// <param name="ALG_TYPE ��������"></param>
    /// <returns>atlantaDataSet.sensorsRow </returns>
    protected internal atlantaDataSet.sensorsRow FindSensor(ALG_TYPE alg)
    {
      atlantaDataSet.sensorsRow[] s_rows = m_row.GetsensorsRows();
      // ����� ������� � ����������� ����������
      foreach (atlantaDataSet.sensorsRow s_row in s_rows)
      {
        atlantaDataSet.relationalgorithmsRow[] r_rows = s_row.GetrelationalgorithmsRows();
        foreach (atlantaDataSet.relationalgorithmsRow r_row in r_rows)
        {
          if (r_row.AlgorithmID == (int)alg)
          {
            this.s_row = s_row;
            return s_row;
          } // if (r_row.AlgorithmID)
        } // foreach (r_row)
      } // foreach (s_row)
      atlantaDataSet.sensoralgorithmsRow sa_row = atlantaDataSet.sensoralgorithms.FindByID((int)alg);
      throw new Exception("�� ���� ������ " +m_row.Name + 
        " �� ����������� � ��������� ���������� \""+ sa_row.Name+"\""); 
    } 
    /// <summary>
    /// ����������� ���������� ���������
    /// </summary>
    /// <returns>RCS.SensorControl.Calibrate ���������� ��������� </returns>
    protected internal RCS.SensorControl.Calibrate Calibration()
    {
      atlantaDataSet.sensorcoefficientRow[] c_rows = s_row.GetsensorcoefficientRows();
      RCS.SensorControl.Calibrate calibrate = new RCS.SensorControl.Calibrate();
      foreach (atlantaDataSet.sensorcoefficientRow c_row in c_rows)
      {
        RCS.SensorControl.Coefficient coefficient = new RCS.SensorControl.Coefficient();
        coefficient.User = c_row.UserValue;
        coefficient.K = c_row.K;
        coefficient.b = c_row.b;
        calibrate.Collection.Add(c_row.SensorValue, coefficient);
      } 
      return calibrate;
    } 

    /// <summary>
    /// ����������� ������� �������
    /// </summary>
    protected void InitPeriod()
    {
      begin = d_rows[0].time;
      end = d_rows[d_rows.Length - 1].time;
      next = begin.Date;
    }
    /// <summary>
    /// ����������� ������� �������
    /// </summary>
    static public void InitPeriod(DateTime _begin, DateTime _end)
    {
      begin = _begin;
      end = _end;
    }

    /// <summary>
    /// ����� ������ �� ������
    /// ������������ ��� ��������� ��������� ������
    /// </summary>
    /// <param name="begin"></param>
    /// <param name="end"></param>
    /// <returns>�-�� ������� </returns>
    protected int SelectTime(DateTime begin, DateTime end)
    {
      selRow = new List<atlantaDataSet.dataviewRow>();
      //rotRow = new List<atlantaDataSet.RotateValueRow>();
      foreach (atlantaDataSet.dataviewRow row in d_rows)
      {
        if (row.time >= next & row.time < next.AddDays(1))
        {
          selRow.Add(row);
          //atlantaDataSet.RotateValueRow[] rows = row.GetRotateValueRows();
          //if (rows.Length > 0)
            //rotRow.Add(rows[0]);
        }
      } 
      return selRow.Count;
    }
    public double GetMax(DataTable table, string colName)
    {

      double maxValue;
      string expr = "MAX(" + colName + ")";
      string filtr = "Mobitel_id = " + m_row.Mobitel_ID.ToString();
      maxValue = (double)table.Compute(expr, filtr);
      //Trace.WriteLine("Max = " + maxValue.ToString());
      return maxValue;
    }

    public double GetMin(DataTable table, string colName,double Pmin)
    {
      double minValue;
      string expr = "MIN(" + colName + ")";
      string filtr = "Mobitel_id = " + m_row.Mobitel_ID.ToString() + "AND " + colName + " > " + Pmin.ToString();
      minValue = (double)table.Compute(expr, filtr);
      //Trace.WriteLine("Min = " + minValue.ToString());
      return minValue;

    }
    
    protected static DateTime GetTime(int id)
    {
      return atlantaDataSet.dataview.FindByDataGps_ID(id).time;
    }

    protected Spoint[] FindMinMax(DataRow[] rows, DataColumn valcol, DataColumn keycol, double pmin, double pmax)
    {
      ArrayList points = new ArrayList();
      int begin_id = 0;
      bool start = false;
      //int cur_id=0;
      foreach (DataRow row in rows)
      {
        if (!start && (((double)row[valcol] >= pmin) && ((double)row[valcol] < pmax)))
        {
          start = true;
          begin_id = (int)row[keycol];
        } // if
        else if (start && (((double)row[valcol] < pmin) || ((double)row[valcol] >= pmax)))
        {
          SPoint p = new SPoint(begin_id, (int)row[keycol]);
          points.Add(p);
          start = false;
        } // if
        //cur_id = (int)row[keycol];
      } // foreach  (dvRow)
      if (start)
      {
        DataRow row = rows[rows.Length - 1];
        SPoint p = new SPoint(begin_id, (int)row[keycol]);
        points.Add(p);
        start = false;
      } // if (start)
      //return Summ;
      return (Spoint[])points.ToArray(typeof(SPoint));
    }

    protected Spoint[] FindMin(DataRow[] rows, DataColumn valcol, DataColumn keycol, double pmin)
    {
      ArrayList points = new ArrayList();
      int begin_id = 0;
      bool start = false;
      //int cur_id=0;
      foreach (DataRow row in rows)
      {
        if (!start & (double)row[valcol] <= pmin)
        {
          start = true;
          begin_id = (int)row[keycol];
        } // if
        else if (start & (double)row[valcol] > pmin)
        {
          Spoint p = new Spoint(begin_id, (int)row[keycol]);
          points.Add(p);
          start = false;
        } // if
        //cur_id = (int)row[keycol];
      } // foreach  (dvRow)
      if (start)
      {
        DataRow row = rows[rows.Length - 1];
        Spoint p = new Spoint(begin_id, (int)row[keycol]);
        points.Add(p);
        start = false;
      } // if (start)
      //return Summ;
      return (Spoint[])points.ToArray(typeof(Spoint));
    }
    /// <summary>
    /// Find maximum
    /// </summary>
    /// <param name="rows">Rows</param>
    /// <param name="valcol">Valcol</param>
    /// <param name="keycol">Keycol</param>
    /// <returns> Point[] </returns>
    protected Spoint[] FindMax(DataRow[] rows, DataColumn valcol, DataColumn keycol,double pmax)
    {
      ArrayList points = new ArrayList();
      int begin_id = 0;
      bool start = false;
      foreach (DataRow row in rows)
      {
        if (!start & (double)row[valcol] > pmax)
        {
          start = true;
          begin_id = (int)row[keycol];
        } // if
        else if (start & (double)row[valcol] <= pmax)
        {
          SPoint p = new SPoint(begin_id, (int)row[keycol]);
          points.Add(p);
          start = false;
        } // if
      } // foreach  (dvRow)
      if (start)
      {
        DataRow row = rows[rows.Length - 1];
        SPoint p = new SPoint(begin_id, (int)row[keycol]);
        points.Add(p);
        start = false;
      } // if (start)
      //return Summ;
      return (Spoint[])points.ToArray(typeof(SPoint));

    } // FindMax(rows, valcol, keycol)   
#endregion


    #region IAlgorithm Members

    /// <summary>
    /// ����� ������
    /// </summary>
    public string Mark
    {
      get { return mark; }
    }
    /// <summary>
    /// ������ ������� 
    /// </summary>
    public string Model
    {
      get { return model; }
    }
    /// <summary>
    /// ���. ����� �������
    /// </summary>
    public string NumberPlate
    {
      get { return numberPlate; }
    }
    /// <summary>
    /// ��� ��������
    /// </summary>
    public string Driver
    {
      get { return driver; }
    }

     public void Run()
    {
      throw new Exception("The method or operation is not implemented.");
    }

    public void SelectItem(atlantaDataSet.mobitelsRow m_row)
    {
      this.m_row = m_row;
      atlantaDataSet.vehicleRow v_row = m_row.GetvehicleRows()[0];
      model = v_row.CarModel;
      mark = v_row.MakeCar;
      numberPlate = v_row.NumberPlate;
      //driver = v_row.driverRow.Family;
      d_rows = m_row.GetdataviewRows();
      if (d_rows.Length == 0)
      {
        throw new Exception("�� ��������� ������ ��� ������\n" +mark+model+" "+numberPlate); 
      }
      //InitPeriod();
    }


    #endregion
  }
}
