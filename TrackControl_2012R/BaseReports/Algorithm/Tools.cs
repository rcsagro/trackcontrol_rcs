
namespace GPSControl.Report.Algorithm
{  
  /// <summary>
  /// �������� ����������
  /// </summary>
  public enum ALG_TYPE:int
  {
    /// <summary>
    /// '������ ������������', '��� ������� ������������ �������� ���. ����.'
    /// </summary>
    WORK_M = 1,
    /// <summary>
    /// '������ ��������� ���������', '������ ������������ ��� ����������� � ���.'
    /// </summary>
    WORK_E = 2,
    /// <summary>
    /// '������� ������������', '��� ������� ������������ �-�� ��������'
    /// </summary>
    ROTATE_M = 3,
    /// <summary>
    /// '����������', '������ �������������� ���� ����'
    /// </summary>
    VOLT = 4,
    /// <summary>
    /// '�������� ��������� ���������', '��� ������� ������������ �-�� ��������'
    /// </summary>
    ROTATE_E = 5,
    /// <summary>
    /// '�����������', '��� ������������� ��������'
    /// </summary>
    TEMP1 = 6,
    /// <summary>
    /// '�������1', '������� ������� � ����1'
    /// </summary>
    FUEL1 = 7,
    /// <summary>
    /// '�������2', '������� ������� � ����2'
    /// </summary>
    FUEL2=  8,
    /// <summary>
    /// '���������1', '��� �������� ���������� ����������1'
    /// </summary>
    PASS1 = 9,
    /// <summary>
    /// '���������2', '��� �������� ���������� ����������2'
    /// </summary>
    PASS2 = 10,
    /// <summary>
    /// '�����1', '��� ����������1'
    /// </summary>
    DOOR1 = 11,
    /// <summary>
    /// '�����2', '��� ����������2'
    /// </summary>
    DOOR2 = 12
  }

  public struct Spoint
	{
		public Spoint( int x, int y )
		{
			this.x = x;
			this.y = y;
		}
		private int x;
    public int X
    {
      get
      {
        return x;
      }
      set
      {
        x = value;
      }
    }
    private int y;
    public int Y
    {
      get
      {
        return y;
      }
      set
      {
        y = value;
      }
    }
  } 
}