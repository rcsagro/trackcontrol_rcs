﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LocalCache;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using Report;
using TrackControl.General;
using TrackControl.Reports;

namespace BaseReports
{
    public partial class FuelerControlDrt : BaseReports.ReportsDE.BaseControl
    {
        public class reportFueler
        {
            private string namesensor;
            private string timestart;
            private string timeend;
            private string location;
            private double changevalue;
            private double valuedutos;
            private string transportcode;
            private string gettercode;
            private string operatorcode;
            private int changevaluefact;
            private string fillup;
            private string drivername;
            private int tankfueladd;
            private string operat;
            private string transportname;
            private string loginfueler;

            public reportFueler(string namesensor, string location, string transportcode, string fillup,
                string gettercode, string drivername, string operatorcode, string operat, double changevalue,
                int changevaluefact, double valuedutos, int tankfueladd, string timestart, string timeend,
                string transportname, string loginfueler)
            {
                this.namesensor = namesensor;
                this.timestart = timestart;
                this.timeend = timeend;
                this.location = location;
                this.changevalue = changevalue;
                this.valuedutos = valuedutos;
                this.transportcode = transportcode;
                this.gettercode = gettercode;
                this.operatorcode = operatorcode;
                this.changevaluefact = changevaluefact;
                this.fillup = fillup;
                this.drivername = drivername;
                this.tankfueladd = tankfueladd;
                this.operat = operat;
                this.transportname = transportname;
                this.loginfueler = loginfueler;
            } // reportFueler

            public string NameSensor
            {
                get { return namesensor; }
            }

            public string TransportName
            {
                get { return transportname; }
            }

            public string LoginFueler
            {
                get { return loginfueler; }
            }

            public string Location
            {
                get { return location; }
            }

            public string TransportCodeDB
            {
                get { return transportcode; }
            }

            public string fillUp
            {
                get { return fillup; }
            }

            public string GetterCodeDB
            {
                get { return gettercode; }
            }

            public string driverName
            {
                get { return drivername; }
            }

            public string OperatorCodeDB
            {
                get { return operatorcode; }
            }

            public string Operator
            {
                get { return operat; }
            }

            public double changeValue
            {
                get { return changevalue; }
            }

            public int changeValueFact
            {
                get { return changevaluefact; }
            }

            public double valueDutos
            {
                get { return valuedutos; }
            }

            public int tankFuelAdd
            {
                get { return tankfueladd; }
            }

            public string timeStart
            {
                get { return timestart; }
            }

            public string timeEnd
            {
                get { return timeend; }
            }
        } // reportFuel

        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, Fueler.Summary> _summaries = new Dictionary<int, Fueler.Summary>();

        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        private ReportBase<reportFueler, TInfo> ReportingFueler;
        Color[] initColors = new Color[13];
        private DateTime periodBeging;
        private DateTime periodEnd;

        /// <summary>
        /// Конструктор без параметров для контрола отчета "Заправщик".
        /// </summary>
        /// <param name="tabGrafMap"></param>
        public FuelerControlDrt()
        {
            InitializeComponent();
            Init();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(fuelerDataVG, fuelerDataCG, bar3);

            AddAlgorithm(new Fueler());

            fuelerDataVG.DoubleClick += new EventHandler(fuelerDataVG_DoubleClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFueler =
                new ReportBase<reportFueler, TInfo>(Controls, compositeLink1, fuelerDataVG,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);

            fuelerDataVG.CustomDrawGroupRow += new RowObjectCustomDrawEventHandler(fuelerDataVG_CustomDrawGroupRow);
        }

        private void fuelerDataVG_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            if (info.Column.Caption == Resources.recipient)
            {
                if (view.IsGroupRow(e.RowHandle))
                {
                    int count = view.GetChildRowCount(e.RowHandle);
                    double summa1 = 0;
                    double summa2 = 0;

                    for (int i = 0; i < count; i++)
                    {
                        int childHandle = view.GetChildRowHandle(e.RowHandle, i);
                        string value1 = view.GetRowCellValue(childHandle, "changeValue").ToString();
                        string value2 = view.GetRowCellValue(childHandle, "tankFuelAdd").ToString();
                        summa1 = summa1 + Convert.ToDouble(value1);
                        summa2 = summa2 + Convert.ToDouble(value2);
                    }

                    string s = view.GetGroupRowDisplayText(e.RowHandle);
                    s = s + " (Величина заправки по ДРТ: " +
                        Convert.ToString(Math.Round(summa1, 2)) + "л)" + " (Заправлено в бак ТС по ДУТ: " +
                        Convert.ToString(Math.Round(summa2, 2)) + "л)";
                    info.GroupText = s;
                } // if
            } // if
        } //  fuelerDataVG_CustomDrawGroupRow

        private void Init()
        {
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "FuelerReport";
            fuelerDataCG.DataSource = atlantaDataSetBindingSource;
            checkAddingInfo.Visibility = BarItemVisibility.Always;
            checkAddingInfo.Enabled = false;
            colTransportCode.Visible = false;
            colGetterCode.Visible = false;
            colOperatorCode.Visible = false;
            colValueDRTFact.Visible = false;
            initColors[0] = Color.DarkGreen;
            initColors[1] = Color.Aqua;
            initColors[2] = Color.Blue;
            initColors[3] = Color.BlueViolet;
            initColors[4] = Color.Brown;
            initColors[5] = Color.CadetBlue;
            initColors[6] = Color.DarkOrange;
            initColors[7] = Color.DodgerBlue;
            initColors[8] = Color.LimeGreen;
            initColors[9] = Color.Black;
            initColors[10] = Color.Crimson;
            initColors[11] = Color.DarkGray;
            initColors[12] = Color.YellowGreen;
        }

        public override string Caption
        {
            get { return Resources.Refueler; }
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is Fueler)
            {
                if (e is FuelerEventArgs)
                {
                    FuelerEventArgs args = (FuelerEventArgs) e;
                    if (_summaries.ContainsKey(args.Id))
                    {
                        _summaries.Remove(args.Id);
                    }
                    _summaries.Add(args.Id, args.Summary);

                    Select(curMrow);
                } // if
            } // if
        } // Algorithm_Action

        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                checkAddingInfo.Enabled = true;
                SetStopButton();
                BeginReport();
                _stopRun = false;

                ClearReport();
                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        curMrow = m_row;
                        SelectItem(m_row);
                        noData = false;

                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";

                SetStartButton();

                if (atlantaDataSetBindingSource.Count > 0)
                    EnableButton();
            } // if
            else
            {
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();

                SetStartButton();
            } // else
        } // bbiStart_ItemClick

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);

                if (atlantaDataSetBindingSource.Count <= 0)
                {
                    return;
                }

                if (_summaries.ContainsKey(mobitel.Mobitel_ID))
                {
                    UpdateStatusLine();
                    SelectGraphics(curMrow);

                    if (atlantaDataSetBindingSource.Count > 0)
                        EnableButton();
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                } // else
            } // if
        } // Select

        public void SelectGraphics(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);

                if (atlantaDataSetBindingSource.Count <= 0)
                {
                    return;
                }

                try
                {
                    CreateGraph(mobitel);
                }
                catch (Exception ex)
                {
                    throw new NotImplementedException("Exception in FuelerControl: " + ex.Message);
                }
            } // if
        } // SelectGraphics

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            DisableButton();
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            //EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row)
        {
            //int length = mobitel_row.GetdataviewRows().Length;
            int length = DataSetManager.GetDataGpsArray(mobitel_row).Length;

            if (length == 0)
            {
                return;
            }

            ReportsControl.GraphClearSeries();

            Fueler fueler = new Fueler();
            fueler.SelectItem(mobitel_row);
            //atlantaDataSet.sensorsRow sens_row = fueler.FindSensor(AlgorithmType.FUELER);
            int idSeries = 0;
            List<atlantaDataSet.sensorsRow > sens_row = fueler.FindDrtSensors(AlgorithmType.FUELER);
            if (sens_row.Count > 0)
            {
                idSeries = (int)AlgorithmType.FUELER;
            }
            else
            {
                sens_row = fueler.FindDrtSensors(AlgorithmType.FUELER_DIFFER);
                if(sens_row.Count > 0)
                {
                    idSeries = (int)AlgorithmType.FUELER_DIFFER;
                }
            }
            double[] value = null;
            for (int indx = 0; indx < sens_row.Count; indx++)
            {
                if(idSeries == (int)AlgorithmType.FUELER)
                    value = fueler.GettingValuesFromDRT(sens_row[indx]);
                else if(idSeries == (int)AlgorithmType.FUELER_DIFFER)
                    value = fueler.GettingValuesFromDRTDiff(sens_row[indx]);

                if (value.Length > 0)
                {
                    //graph.AddSeriesL(String.Format("{0} ({1})", sens_row.Name, sens_row.NameUnit), Color.DarkGreen,
                    //    value, dataset.dataview.SelectTimeByMobitelID(mobitel_row), AlgorithmType.FUELER);

                    graph.AddSeriesL(String.Format("{0} ({1}){2}", sens_row[indx].Name, sens_row[indx].NameUnit, "\n"),
                        initColors[indx],
                        value, DataSetManager.GetDataGpsArray(mobitel_row).Select(gps => gps.Time).ToArray(),
                        (AlgorithmType) idSeries, true);

                    idSeries += 1;
                } // if
            } // for
            ReportsControl.ShowGraph(curMrow);
        } // CreateGraph

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.FuelerReport.Clear();
            _summaries.Clear();
            ClearStatusLine();
        }

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            barStaticItem1.Caption = Resources.TotalFuelRate;
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR;
        }

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine()
        {
            Fueler.Summary s = _summaries[curMrow.Mobitel_ID];

            barStaticItem1.Caption = Resources.TotalFuelRate + " " + s.FuelRate.ToString("N2");
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR + " " + s.UnrecordedFuelRate.ToString("N2");
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Показать на графике".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        } // bbiShowOnGraf_ItemClick

        private static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.FuelerReportRow f_row, out string val,
            out Color color)
        {
            val = f_row.changeValue.ToString("#.#") + Resources.LitreAbbreviation;
            color = Color.Black;
            if (f_row.changeValue > 0)
            {
                val = String.Format("{0} {1}", Resources.Fuelling, val);
                color = Color.Green;
            }
        }

        protected void ShowOnGraph()
        {
            Int32[] numberRow = fuelerDataVG.GetSelectedRows();

            for (int i = 0; i < numberRow.Length; i++)
            {
                if (numberRow[i] < 0)
                {
                    return;
                }
            }

            Graph.ClearRegion();
            Graph.ClearLabel();

            for (int i = 0; i < numberRow.Length; i++)
            {
                //int number = fuelerDataVG
                atlantaDataSet.FuelerReportRow f_row = (atlantaDataSet.FuelerReportRow)
                    ((System.Data.DataRowView)
                        atlantaDataSetBindingSource.List[numberRow[i]]).Row;

                if (f_row != null)
                {
                    Graph.AddTimeRegion(f_row.timeStart, f_row.timeEnd); //выделяем цветом

                    string val;
                    Color color;
                    ShowGraphToolStripButton_ClickExtracted(f_row, out val, out color);
                    Graph.AddLabel(f_row.timeEnd, f_row.changeValue, f_row.pointValue, val, color); //Добавляем метку
                }
            }

            Graph.SellectZoom();
        }

        protected void ShowOnMap()
        {
            try
            {
                ReportsControl.OnClearMapObjectsNeeded();
                List<Marker> markers = new List<Marker>();
                Int32[] numberRow = fuelerDataVG.GetSelectedRows();

                for (int i = 0; i < numberRow.Length; i++)
                {
                    atlantaDataSet.FuelerReportRow f_row = (atlantaDataSet.FuelerReportRow)
                        ((System.Data.DataRowView) atlantaDataSetBindingSource.List[numberRow[i]]).Row;

                    if (f_row != null)
                    {
                        double value = f_row.changeValue;
                        string message = String.Format("{0}; {1}: {2:N2} {3}", f_row.NameSensor,
                            Resources.Fuelling, value, Resources.LitreAbbreviation);
                        //PointLatLng location = new PointLatLng(f_row.dataviewRow.Lat, f_row.dataviewRow.Lon);
                        PointLatLng location = Algorithm.GpsDatas.First(gps => gps.Id == f_row.DataGPS_ID).LatLng;
                        markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                    }
                }

                ReportsControl.OnMarkersShowNeeded(markers);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error map", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        } // ShowOnMap

        private void fuelerDataVG_DoubleClick(object sender, EventArgs e)
        {
            ShowOnGraph();
            ShowOnMap();
        }

        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowOnMap();
        }

        protected override void ExportAllDevToReportMod()
        {
            colNameSensor.Visible = false;
            colGetterCode.Visible = false;
            colOperatorCode.Visible = false;
            colValueDRTFact.Visible = false;
            colTransportCode.Visible = false;
            colTransportName.Visible = true;
            colLoginFueler.Visible = true;
            colNameSensor.VisibleIndex = -1;
            colTransportName.VisibleIndex = 0;
            colLoginFueler.VisibleIndex = 1;
            colLocation.VisibleIndex = 2;
            colTransportCode.VisibleIndex = -1;
            colfillUp.VisibleIndex = 3;
            colGetterCode.VisibleIndex = -1;
            colDriverName.VisibleIndex = 4;
            colOperatorCode.VisibleIndex = -1;
            colOperator.VisibleIndex = 5;
            colchangeValue.VisibleIndex = 6;
            colValueDRTFact.VisibleIndex = -1;
            colDutos.VisibleIndex = 7;
            coltankFuelAdd.VisibleIndex = 8;
            coltimeStart.VisibleIndex = 9;
            coltimeEnd.VisibleIndex = 10;

            periodBeging = Algorithm.Period.Begin;
            periodEnd = Algorithm.Period.End;

            ReportingFueler.CreateBindDataList();
            ReportingFueler.CreateElementReport();

            foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
            {
                string carmodel = "";
                string vehiclemodel = "";

                TInfo tinfo = new TInfo();
                VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);

                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    try
                    {
                        tinfo.periodBeging = Algorithm.Period.Begin;
                        tinfo.periodEnd = Algorithm.Period.End;

                        atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(m_row.Mobitel_ID);

                        carmodel = info.CarMaker + " " + info.CarModel + " " + info.RegistrationNumber + " (" + info.Identificator + ")";

                        //tinfo.infoVehicle = info.Info; // Автомобиль
                        //tinfo.infoDriverName = info.DriverFullName; // Водитель
                        //tinfo.OutLinkId = info.OutLinkId; // код внешней базы
                        //tinfo.fuelerLiter = _summaries[m_row.Mobitel_ID].UnrecordedFuelRate;
                        // Литров топлива непопавшее в отчет
                        //tinfo.fuelerRate = _summaries[m_row.Mobitel_ID].FuelRate; // Заправлено литров всего
                    } // try
                    catch (Exception ex)
                    {
                        continue;
                    }

                    foreach (atlantaDataSet.FuelerReportRow row in
                        dataset.FuelerReport.Select("mobitel_id=" + m_row.Mobitel_ID, "timeStart ASC"))
                    {
                        string transportname = carmodel;
                        string loginfueler = info.DevIdShort;
                        string timestart = row.timeStart.ToString("dd.MM.yyyy HH:mm");
                        string timeend = row.timeEnd.ToString("dd.MM.yyyy HH:mm");
                        string location = row.Location;
                        double changevalue = Math.Round(row.changeValue, 2);
                        double valuedutos = Math.Round(row.valueDutos, 2);
                        string transportcode = "";
                        try
                        {
                            transportcode = row.TransportCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }
                        string gettercode = "";
                        try
                        {
                            gettercode = row.GetterCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }
                        string operatorcode = "";
                        try
                        {
                            operatorcode = row.OperatorCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }

                        int changevaluefact = row.changeValueFact;
                        string fillup = row.fillUp;
                        string idOperat = row.Operator;

                        string drivername;
                        if (row.driverName != null && row.driverName != "")
                            drivername = row.driverName;
                        else
                            drivername = "----";

                        int tankfueladd = row.tankFuelAdd;
                        double fulerrate = Math.Round(row.changeValue - row.tankFuelAdd, 2);

                        ReportingFueler.AddDataToBindList(new reportFueler("", location, transportcode, fillup,
                            gettercode, drivername, operatorcode, idOperat,
                            changevalue, changevaluefact, valuedutos, tankfueladd, timestart, timeend, transportname, loginfueler));
                    } // foreach 2
                } // if
            } // foreach 1

            ReportingFueler.CreateAndShowReport();
            ReportingFueler.DeleteData();
            colNameSensor.Visible = true;
            colTransportName.Visible = false;
            colLoginFueler.Visible = false;
            colNameSensor.Visible = false;
            colGetterCode.Visible = false;
            colOperatorCode.Visible = false;
            colValueDRTFact.Visible = false;
            colTransportCode.Visible = false;
            colTransportName.Visible = true;
            colLoginFueler.Visible = true;
            colNameSensor.VisibleIndex = 0;
            colTransportName.VisibleIndex = -1;
            colLoginFueler.VisibleIndex = -1;
            colLocation.VisibleIndex = 1;
            colTransportCode.VisibleIndex = -1;
            colfillUp.VisibleIndex = 2;
            colGetterCode.VisibleIndex = -1;
            colDriverName.VisibleIndex = 3;
            colOperatorCode.VisibleIndex = -1;
            colOperator.VisibleIndex = 4;
            colchangeValue.VisibleIndex = 5;
            colValueDRTFact.VisibleIndex = -1;
            colDutos.VisibleIndex = 6;
            coltankFuelAdd.VisibleIndex = 7;
            coltimeStart.VisibleIndex = 8;
            coltimeEnd.VisibleIndex = 9;
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    try
                    {
                        TInfo tinfo = new TInfo();
                        periodBeging = Algorithm.Period.Begin;
                        periodEnd = Algorithm.Period.End;

                        atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(m_row.Mobitel_ID);
                        VehicleInfo info = new VehicleInfo(m_row.Mobitel_ID, m_row);

                        tinfo.infoVehicle = info.Info; // Автомобиль
                        tinfo.infoDriverName = info.DriverFullName; // Водитель
                        tinfo.OutLinkId = info.OutLinkId; // код внешней базы
                        tinfo.fuelerLiter = _summaries[m_row.Mobitel_ID].UnrecordedFuelRate;
                        // Литров топлива непопавшее в отчет
                        tinfo.fuelerRate = _summaries[m_row.Mobitel_ID].FuelRate; // Заправлено литров всего

                        ReportingFueler.AddInfoStructToList(tinfo);
                        ReportingFueler.CreateBindDataList();
                    } // try
                    catch (Exception ex)
                    {
                        continue;
                    }

                    foreach (atlantaDataSet.FuelerReportRow row in
                        dataset.FuelerReport.Select("mobitel_id=" + m_row.Mobitel_ID, "timeStart ASC"))
                    {
                        string namesensor = "";
                        try
                        {
                            namesensor = row.NameSensor;
                        }
                        catch (Exception e)
                        {
                            namesensor = "--";
                        }

                        string timestart = row.timeStart.ToString("dd.MM.yyyy HH:mm");
                        string timeend = row.timeEnd.ToString("dd.MM.yyyy HH:mm");
                        string location = row.Location;
                        double changevalue = Math.Round(row.changeValue, 2);
                        double valuedutos = Math.Round(row.valueDutos, 2);
                        string transportcode = "";
                        try
                        {
                            transportcode = row.TransportCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }
                        string gettercode = "";
                        try
                        {
                            gettercode = row.GetterCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }
                        string operatorcode = "";
                        try
                        {
                            operatorcode = row.OperatorCodeDB;
                        }
                        catch (Exception e)
                        {
                            // to do this
                        }

                        int changevaluefact = row.changeValueFact;
                        string fillup = row.fillUp;
                        string idOperat = row.Operator;

                        string drivername;
                        if (row.driverName != null && row.driverName != "")
                            drivername = row.driverName;
                        else
                            drivername = "----";

                        int tankfueladd = row.tankFuelAdd;
                        double fulerrate = Math.Round(row.changeValue - row.tankFuelAdd, 2);

                        ReportingFueler.AddDataToBindList(new reportFueler(namesensor, location, transportcode, fillup,
                            gettercode, drivername, operatorcode, idOperat,
                            changevalue, changevaluefact, valuedutos, tankfueladd, timestart, timeend, "", ""));
                    } // foreach 2

                    ReportingFueler.CreateElementReport();
                } // if
            } // foreach 1

            ReportingFueler.CreateAndShowReport();
            ReportingFueler.DeleteData();
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            try
            {
                TInfo tinfo = new TInfo();
                periodBeging = Algorithm.Period.Begin; // Начало периода
                periodEnd = Algorithm.Period.End; // Конец периода
                VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);

                tinfo.infoVehicle = info.Info; // Автомобиль
                tinfo.infoDriverName = info.DriverFullName; // Водитель
                tinfo.OutLinkId = info.OutLinkId; // Код внешней базы
                tinfo.TypeFuel = info.TypeFuel; // вид топлива
                tinfo.OutLinkIdFuel = info.OutLinkIdFuel; // код внешней базы
                tinfo.fuelerLiter = _summaries[curMrow.Mobitel_ID].UnrecordedFuelRate;
                // Литров топлива непопавшее в отчет
                tinfo.fuelerRate = _summaries[curMrow.Mobitel_ID].FuelRate; // Заправлено литров всего

                ReportingFueler.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
                ReportingFueler.CreateAndShowReport(fuelerDataCG);
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(Resources.AbsentData, Resources.Notification);
            }
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(fuelerDataVG);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(fuelerDataVG);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(fuelerDataCG);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            try
            {
                DevExpressReportHeader(Resources.FuelerControlDrt, e);

                TInfo info = ReportingFueler.GetInfoStructure;

                string strPeriod = Resources.PeriodFrom + " " + periodBeging + " " +
                                   Resources.PeriodTo + " " + periodEnd;
                DevExpressReportSubHeader(strPeriod, 22, e);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        /* функция для формирования верхней/средней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования левой части заголовка отчета */

        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingFueler.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                    Resources.OutLinkId + ": " + info.OutLinkId + "\n" +
                    Resources.Driver + ": " + info.infoDriverName);
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */

        protected string GetStringBreackRight()
        {
            TInfo info = ReportingFueler.GetInfoStructure;
            return (Resources.TotalFuelRate + " " + String.Format("{0:f2}", info.fuelerRate) + "\n" +
                    Resources.FuelerDrtCtrlUnrFR + " " + String.Format("{0:f2}", info.fuelerLiter));
        }

        protected void Localization()
        {
            barStaticItem1.Caption = Resources.TotalFuelRate;
            barStaticItem2.Caption = Resources.FuelerDrtCtrlUnrFR;

            colLocation.Caption = Resources.Location;
            colLocation.ToolTip = Resources.Location;

            colfillUp.Caption = Resources.FuelerDrtCtrlFilledVehicles;
            colfillUp.ToolTip = Resources.FuelerDrtCtrlFilledVehicles;

            colDriverName.Caption = Resources.recipient;
            colDriverName.ToolTip = Resources.recipient;

            colchangeValue.Caption = Resources.FuelerDrtCtrlDrtFillingValue;
            colchangeValue.ToolTip = Resources.FuelerDrtCtrlDrtFillingValue;

            colDutos.Caption = Resources.FuelerDutCtrl;
            colDutos.ToolTip = Resources.FuelerDutCtrlTip;

            coltankFuelAdd.Caption = Resources.FuelerDrtCtrlDutFillingValue;
            coltankFuelAdd.ToolTip = Resources.FuelerDrtCtrlDutFillingValue;

            coltimeStart.Caption = Resources.InitialTime;
            coltimeStart.ToolTip = Resources.InitialTime;

            coltimeEnd.Caption = Resources.EndTime;
            coltimeEnd.ToolTip = Resources.EndTime;

            colOperator.Caption = Resources.Operator;
            colOperator.ToolTip = Resources.TipOperator;

            colTransportCode.Caption = Resources.TransportCode;
            colTransportCode.ToolTip = Resources.TransportCodeToolTip;

            colGetterCode.Caption = Resources.GetterCode;
            colGetterCode.ToolTip = Resources.GetterCodeToolTip;

            colOperatorCode.Caption = Resources.OperatorCode;
            colOperatorCode.ToolTip = Resources.OperatorCodeToolTip;

            colValueDRTFact.Caption = Resources.ValueDRTFact;
            colValueDRTFact.ToolTip = Resources.ValueDRTFactToolTip;

            colNameSensor.Caption = Resources.NameSensor;
            colNameSensor.ToolTip = Resources.NameSensorToolTip;

            colTransportName.Caption = Resources.TransportName;
            colTransportName.ToolTip = Resources.TransportName;

            colLoginFueler.Caption = Resources.LoginFueler;
            colLoginFueler.ToolTip = Resources.LoginFueler;
        }

        private void fuelerDataCG_Click(object sender, EventArgs e)
        {
            // to do this
        }

        protected override void checkAddingInfo_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            colTransportCode.Visible = !colTransportCode.Visible;
            colGetterCode.Visible = !colGetterCode.Visible;
            colOperatorCode.Visible = !colGetterCode.Visible;
            colValueDRTFact.Visible = !colValueDRTFact.Visible;

            if (colTransportCode.Visible)
            {
                colNameSensor.VisibleIndex = 0;
                colLocation.VisibleIndex = 1;
                colTransportCode.VisibleIndex = 2;
                colfillUp.VisibleIndex = 3;
                colGetterCode.VisibleIndex = 4;
                colDriverName.VisibleIndex = 5;
                colOperatorCode.VisibleIndex = 6;
                colOperator.VisibleIndex = 7;
                colchangeValue.VisibleIndex = 8;
                colValueDRTFact.VisibleIndex = 9;
                colDutos.VisibleIndex = 10;
                coltankFuelAdd.VisibleIndex = 11;
                coltimeStart.VisibleIndex = 12;
                coltimeEnd.VisibleIndex = 13;
            }
            else
            {
                colNameSensor.VisibleIndex = 0;
                colLocation.VisibleIndex = 1;
                colTransportCode.VisibleIndex = -1;
                colfillUp.VisibleIndex = 2;
                colGetterCode.VisibleIndex = -1;
                colDriverName.VisibleIndex = 3;
                colOperatorCode.VisibleIndex = -1;
                colOperator.VisibleIndex = 4;
                colchangeValue.VisibleIndex = 5;
                colValueDRTFact.VisibleIndex = -1;
                colDutos.VisibleIndex = 6;
                coltankFuelAdd.VisibleIndex = 7;
                coltimeStart.VisibleIndex = 8;
                coltimeEnd.VisibleIndex = 9;
            }
        } // Localization
    } // DevFuelerControlDrt
} // BaseReports
