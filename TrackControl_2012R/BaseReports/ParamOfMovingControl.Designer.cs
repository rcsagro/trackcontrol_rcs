namespace BaseReports
{
  partial class ParamOfMovingControl
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        this.trafficDetailReportDataGridView = new System.Windows.Forms.DataGridView();
        this.ColumnLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnDescribeEvent = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnMaxValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnMiddleValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.trafficDetailReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.atlantaDataSet = new LocalCache.atlantaDataSet();
        this.reportPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportDataGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
        this.SuspendLayout();
        // 
        // reportPanel
        // 
        this.reportPanel.Controls.Add(this.trafficDetailReportDataGridView);
        this.reportPanel.Size = new System.Drawing.Size(761, 502);
        // 
        // trafficDetailReportDataGridView
        // 
        this.trafficDetailReportDataGridView.AllowUserToAddRows = false;
        this.trafficDetailReportDataGridView.AutoGenerateColumns = false;
        this.trafficDetailReportDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnLocation,
            this.ColumnDescribeEvent,
            this.ColumnTime,
            this.ColumnDuration,
            this.ColumnMaxValue,
            this.ColumnMiddleValue});
        this.trafficDetailReportDataGridView.DataSource = this.trafficDetailReportBindingSource;
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.trafficDetailReportDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
        this.trafficDetailReportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.trafficDetailReportDataGridView.Location = new System.Drawing.Point(0, 0);
        this.trafficDetailReportDataGridView.Name = "trafficDetailReportDataGridView";
        this.trafficDetailReportDataGridView.ReadOnly = true;
        this.trafficDetailReportDataGridView.RowHeadersVisible = false;
        this.trafficDetailReportDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.trafficDetailReportDataGridView.Size = new System.Drawing.Size(761, 502);
        this.trafficDetailReportDataGridView.TabIndex = 0;
        this.trafficDetailReportDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.trafficDetailReportDataGridView_CellFormatting);
        this.trafficDetailReportDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.trafficDetailReportDataGridView_CellMouseDoubleClick);
        // 
        // ColumnLocation
        // 
        this.ColumnLocation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.ColumnLocation.DataPropertyName = "Location";
        this.ColumnLocation.HeaderText = "��������������";
        this.ColumnLocation.Name = "ColumnLocation";
        this.ColumnLocation.ReadOnly = true;
        // 
        // ColumnDescribeEvent
        // 
        this.ColumnDescribeEvent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
        this.ColumnDescribeEvent.DataPropertyName = "DescribeEvent";
        this.ColumnDescribeEvent.HeaderText = "�������";
        this.ColumnDescribeEvent.Name = "ColumnDescribeEvent";
        this.ColumnDescribeEvent.ReadOnly = true;
        this.ColumnDescribeEvent.Width = 76;
        // 
        // ColumnTime
        // 
        this.ColumnTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.ColumnTime.DataPropertyName = "Time";
        dataGridViewCellStyle1.Format = "dd.MM.yyyy HH:mm:ss";
        dataGridViewCellStyle1.NullValue = null;
        this.ColumnTime.DefaultCellStyle = dataGridViewCellStyle1;
        this.ColumnTime.HeaderText = "����, �����";
        this.ColumnTime.Name = "ColumnTime";
        this.ColumnTime.ReadOnly = true;
        this.ColumnTime.Width = 96;
        // 
        // ColumnDuration
        // 
        this.ColumnDuration.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.ColumnDuration.DataPropertyName = "Duration";
        dataGridViewCellStyle2.Format = "dd.HH:mm:ss";
        dataGridViewCellStyle2.NullValue = null;
        this.ColumnDuration.DefaultCellStyle = dataGridViewCellStyle2;
        this.ColumnDuration.HeaderText = "������������";
        this.ColumnDuration.Name = "ColumnDuration";
        this.ColumnDuration.ReadOnly = true;
        this.ColumnDuration.Width = 105;
        // 
        // ColumnMaxValue
        // 
        this.ColumnMaxValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.ColumnMaxValue.DataPropertyName = "MaxValue";
        dataGridViewCellStyle3.Format = "N2";
        dataGridViewCellStyle3.NullValue = null;
        this.ColumnMaxValue.DefaultCellStyle = dataGridViewCellStyle3;
        this.ColumnMaxValue.HeaderText = "������������ ��������";
        this.ColumnMaxValue.Name = "ColumnMaxValue";
        this.ColumnMaxValue.ReadOnly = true;
        this.ColumnMaxValue.Width = 159;
        // 
        // ColumnMiddleValue
        // 
        this.ColumnMiddleValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.ColumnMiddleValue.DataPropertyName = "MiddleValue";
        dataGridViewCellStyle4.Format = "N2";
        dataGridViewCellStyle4.NullValue = null;
        this.ColumnMiddleValue.DefaultCellStyle = dataGridViewCellStyle4;
        this.ColumnMiddleValue.HeaderText = "������� ��������";
        this.ColumnMiddleValue.Name = "ColumnMiddleValue";
        this.ColumnMiddleValue.ReadOnly = true;
        this.ColumnMiddleValue.Width = 125;
        // 
        // trafficDetailReportBindingSource
        // 
        this.trafficDetailReportBindingSource.DataMember = "TrafficDetailReport";
        this.trafficDetailReportBindingSource.DataSource = this.atlantaDataSet;
        // 
        // atlantaDataSet
        // 
        this.atlantaDataSet.DataSetName = "atlantaDataSet";
        this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // ParamOfMovingControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.Name = "ParamOfMovingControl";
        this.Size = new System.Drawing.Size(763, 533);
        this.Controls.SetChildIndex(this.reportPanel, 0);
        this.reportPanel.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportDataGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.trafficDetailReportBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView trafficDetailReportDataGridView;
    private System.Windows.Forms.BindingSource trafficDetailReportBindingSource;
    private LocalCache.atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLocation;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescribeEvent;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDuration;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMaxValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMiddleValue;
  }
}
