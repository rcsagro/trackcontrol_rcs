﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;

namespace BaseReports
{
    [ToolboxItem(false)]
    public partial class FuelControlOnes : BaseReports.ReportsDE.BaseControl
    {
        public class reportFuel
        {
            string humanVeh;
            string numberVeh;
            string markingVeh;
            int identification;
            string location;
            string date;
            string time;
            double value;
            double begin;
            double end;
            string genertype;
            private int dataGpsId;
            private string nameDut;

            public reportFuel(string human, string number, string marking, int ident, string location, string date,
                string time, double value, double begin, double end, string genertype, int dataGpsid, string namedut)
            {
                this.humanVeh = human;
                this.numberVeh = number;
                this.markingVeh = marking;
                this.identification = ident;
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
                nameDut = namedut;
            } // reportFuel

            public reportFuel(string location, string date, string time, double value, double begin, double end,
                string genertype, int dataGpsid, string namedut)
            {
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
                nameDut = namedut;
            } // reportFuel

            public int DataGPS_ID
            {
                get { return dataGpsId; }
            }

            [Developer(0)]
            public string HumanVehicle
            {
                get { return humanVeh; }
            }

            [Developer(1)]
            public string NumberVehicle
            {
                get { return numberVeh; }
            }

            [Developer(2)]
            public string MarkingVehicle
            {
                get { return markingVeh; }
            }

            [Developer(3)]
            public string NamesDut
            {
                get { return nameDut; }
            }

            [Developer(4)]
            public int Identification
            {
                get { return identification; }
            }

            [Developer(5)]
            public string Location
            {
                get { return location; }
            }

            [Developer(6)]
            public string date_
            {
                get { return date; }
            }

            [Developer(7)]
            public string Times
            {
                get { return time; }
            }

            [Developer(8)]
            public double dValue
            {
                get { return value; }
            }

            [Developer(9)]
            public double beginValue
            {
                get { return begin; }
            }

            [Developer(10)]
            public double endValue
            {
                get { return end; }
            }

            [Developer(11)]
            public string GenerateType
            {
                get { return genertype; }
            }
        } // reportFuel

        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, FuelOne.Summary> _summaries = new Dictionary<int, FuelOne.Summary>();

        private FuelAddDlg fuelAddDlg;
        private atlantaDataSet.FuelReportRow f_row;
        private FuelOne fuelAlg1;
        private FuelOne fuelAlg2;
        private FuelOne fuelAlg3;
        private FuelOne fuelAlg4;
        private FuelOne fuelAlg5;
        private bool begin_end;
        private bool flagStart = true;

        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;

        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ControlNavigator connav;
        ReportBase<reportFuel, TInfoDut> ReportingFuel;
        int numberChangedRow;
        private string NameReport = Resources.FuelCtrlCaptionOnes;

        /// <summary>
        /// Создает и инициализирует новый экземпляр класса BaseReports.FuelControl
        /// </summary>
        /// <param name="caption"></param>
        public FuelControlOnes()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            EnabledExportExtendMenu();
            SetVisibilityAlgoChoiser(true);
            VisionPanel(fuelDataGridView2, fuelDataGridControl2, bar2);
            buildGraph = new BuildGraphs();
            DisableMenuButton();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            fuelAlg1 = new FuelOne(AlgorithmType.FUEL1);
            fuelAlg2 = new FuelOne(AlgorithmType.FUEL1_AGREGAT);
            fuelAlg3 = new FuelOne(AlgorithmType.FUEL2_AGREGAT);
            fuelAlg4 = new FuelOne(AlgorithmType.FUEL3_AGREGAT);
            fuelAlg5 = new FuelOne(AlgorithmType.FUEL_AGREGAT_ALL);
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(fuelAlg1);
            AddAlgorithm(fuelAlg2);
            AddAlgorithm(fuelAlg3);
            AddAlgorithm(fuelAlg4);
            AddAlgorithm(fuelAlg5);

            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "FuelReport";

            fuelAddDlg = new FuelAddDlg(graph);

            fuelDataGridControl2.UseEmbeddedNavigator = true;
            barButtonNavigator.Caption = Resources.ResButtonNavigatorHide;
            connav = fuelDataGridControl2.EmbeddedNavigator;
            connav.Buttons.BeginUpdate();
            connav.Buttons.Append.Visible = true;
            connav.Buttons.Append.Enabled = true;
            connav.Buttons.Remove.Visible = true;
            connav.Buttons.Remove.Enabled = true;
            connav.Buttons.EndUpdate();

            fuelDataGridView2.RowCellClick += new RowCellClickEventHandler(fuelDataGridView_CellMouseDoubleClick);
            fuelDataGridView2.ValidatingEditor +=
                new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(
                    fuelDataGridView_CellValidating);
            fuelDataGridView2.CellValueChanged += new CellValueChangedEventHandler(fuelDataGridView_CellEndEdit);
            connav.ButtonClick += new NavigatorButtonClickEventHandler(connav_ButtonClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuel =
                new ReportBase<reportFuel, TInfoDut>(Controls, compositeLink1, fuelDataGridView2,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevFuelControl

        public override string Caption
        {
            get { return Resources.FuelCtrlCaptionOnes; }
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is FuelOne)
            {
                if (e is Fuel_EventArgs)
                {
                    Fuel_EventArgs args = (Fuel_EventArgs) e;
                    if (_summaries.ContainsKey(args.Id))
                    {
                        _summaries.Remove(args.Id);
                    }

                    _summaries.Add(args.Id, args.Summary);
                } // if
            } // if
        } // Algorithm_Action

        /// <summary>
        /// Построение отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                flagStart = false;
                SetStopButton();
                BeginReport();
                _stopRun = false;
                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;

                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                if (curMrow == null)
                {
                    return;
                }

                Select(curMrow);
                SetStartButton();
                //EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();

                SetStartButton();
            }

            flagStart = true;
        } // bbiStart_ItemClick

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            DisableButton();
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    break;
            } // foreach

            //EnableButton();
            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        protected override void comboAlgoChoiser_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarEditItem item = (BarEditItem)e.Item;
            string value = item.EditValue.ToString();
            int index = repAlgoChoiser.Items.IndexOf(value);
            int code = -1;

            if (index == 0)
            {
                code = (int)AlgorithmType.FUEL1;
            }
            else if (index == 1)
            {
                code = (int)AlgorithmType.FUEL1_AGREGAT;
            }
            else if (index == 2)
            {
                code = (int)AlgorithmType.FUEL2_AGREGAT;
            }
            else if (index == 3)
            {
                code = (int)AlgorithmType.FUEL3_AGREGAT;
            }
            else if (index == 4)
            {
                code = (int)AlgorithmType.FUEL_AGREGAT_ALL;
            }
            else
            {
                XtraMessageBox.Show("Неизвестный алгоритм", "Выбор алгоритма", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }  
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);

                if (CurrentActivateReport == NameReport)
                    SelectGraphic(curMrow);
              
                if (_summaries.ContainsKey(mobitel.Mobitel_ID))
                {
                    // if( atlantaDataSetBindingSource.Count > 0 )
                    {
                        fuelDataGridControl2.DataSource = atlantaDataSetBindingSource;
                        atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + "TypeAlgorythm=" + (int)AlgorithmType.FUEL1;
                        // UpdateStatusLine();
                        EnableButton();

                        if (CurrentActivateReport == NameReport)
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL1);
                    }

                    //EnableEditButton();
                    UpdateStatusLine();
                    bbiPrintReport.Enabled = true;
                }
                else
                {
                    DisableButton();
                    //DisableEditButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel, int code)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = "Mobitel_id={0}" + mobitel.Mobitel_ID + "TypeAlgorytm=" + code;

                try
                {
                    CreateGraph(mobitel, AlgorithmType.FUEL1);
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in FuelControl: {0}", ex.Message);
                    throw new NotImplementedException(message);
                }
            } // if
        } // SelectGraphic

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            ReportsControl.GraphClearSeries();
            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphFuel(algoParam, Graph, dataset, mobitel_row);
            buildGraph.AddGraphVoltage(Graph, dataset, mobitel_row); // add 04.07.2017 aketner
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            List<Marker> markers = new List<Marker>();
            Int32[] index = fuelDataGridView2.GetSelectedRows();
            for (int i = 0; i < index.Length; i++)
            {
                atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index[i]]).Row;

                if (f_row["time_"] != DBNull.Value)
                {
                    double value = f_row.dValue;
                    string message = String.Format("{0}: {1:N2} {2}.",
                        value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                        value, Resources.LitreAbbreviation);
                    PointLatLng location = new PointLatLng(f_row.Lat, f_row.Lon);
                    markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                } // if
            } // for

            ReportsControl.OnMarkersShowNeeded(markers);
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        }

        private static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.FuelReportRow f_row, out string val,
            out Color color)
        {
            val = String.Format("{0:#.#}{1}", f_row.dValue, Resources.LitreAbbreviation);

            if (f_row.dValue > 0)
            {
                val = String.Format("{0} {1}", Resources.Fuelling, val);
                color = Color.Green;
            }
            else
            {
                val = String.Format("{0} {1}", Resources.FuelLarceny, val);
                color = Color.Red;
            }
        }

        void UpdateZGraph()
        {
            // Сбрасываем все метки
            // Graph.ClearGraphZoom(false)
            Graph.ClearLabel();
            Int32[] index = fuelDataGridView2.GetSelectedRows();

            for (int i = 0; i < index.Length; i++)
            {
                if (index[i] >= 0)
                {
                    int indx = fuelDataGridView2.GetDataSourceRowIndex(index[i]);
                    atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                        ((DataRowView) atlantaDataSetBindingSource.List[indx]).Row;

                    if (f_row["time_"] != DBNull.Value)
                    {
                        string val;
                        Color color;
                        ShowGraphToolStripButton_ClickExtracted(f_row, out val, out color);

                        if (f_row.pointValue <= 0.0)
                        {
                            Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.beginValue, val, color);
                            return;
                        }

                        Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.pointValue, val, color);
                    } // if
                }
            } // for

            // Graph.ClearGraphZoom(true); AddGraphInclinometrX
            ReportsControl.ShowGraph(curMrow);
            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
            Graph.SellectZoom();
        } // UpdateZGraph

        private void fuelDataGridView_CellFormatting(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "dValue")
            {
                if (e.Value is DBNull || e.Value == null)
                {
                    return;
                }

                if ((double) e.Value > 0.0)
                {
                    e.Column.AppearanceCell.ForeColor = Color.Green;
                }
                else
                {
                    e.Column.AppearanceCell.ForeColor = Color.Red;
                }
            } // if
        } // fuelDataGridView_CellFormatting

        private void fuelDataGridView_CellMouseDoubleClick(object sender, RowCellClickEventArgs e)
        {
            try
            {
                ReportsControl.OnClearMapObjectsNeeded();

                if (e.RowHandle < 0)
                    return;

                DateTime time_;
                // Дата время
                object tm = fuelDataGridView2.GetRowCellValue(e.RowHandle, e.Column);
                if (!DateTime.TryParse(Convert.ToString(tm), out time_))
                    // fuelDataGridView.CurrentRow.ed
                    return;

                int index = fuelDataGridView2.GetDataSourceRowIndex(e.RowHandle);

                List<Marker> markers = new List<Marker>();
                atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index]).Row;
                double value = f_row.dValue;
                string message = String.Format("{0}: {1:N2} {2}.",
                    value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                    value, Resources.LitreAbbreviation);
                PointLatLng location = Algorithm.GpsDatas.First(gps => gps.Id == f_row.DataGPS_ID).LatLng;
                markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                ReportsControl.OnMarkersShowNeeded(markers);
                UpdateZGraph();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Fuel Control", MessageBoxButtons.OK);
                return;
            }
        } // fuelDataGridView_CellMouseDoubleClick

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.FuelReport.Clear();
            dataset.FuelValue.Clear();
            _summaries.Clear();
            ClearStatusLine();
        }

        /// <summary>
        /// Осуществляет валидацию вводимых пользователем данных
        /// </summary>
        private void fuelDataGridView_CellValidating(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if ((fuelDataGridView2.FocusedColumn.Name == "_endValue") ||
                (fuelDataGridView2.FocusedColumn.Name == "_beginValue"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.FuelCtrlFuellingValueError, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    double new_double;
                    if (!Double.TryParse(e.Value.ToString(), out new_double))
                    {
                        XtraMessageBox.Show(Resources.FuelCtrlFuellingValueError, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // if
            else if ((fuelDataGridView2.FocusedColumn.Name == "_time"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    DateTime new_time;
                    if (!DateTime.TryParse(e.Value.ToString(), out new_time))
                    {
                        XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // else if
        } // fuelDataGridView_CellValidating

        /// <summary>
        /// Осуществляет пересчет в редактируемой строке отчета и итогов
        /// </summary>
        private void fuelDataGridView_CellEndEdit(object sender, CellValueChangedEventArgs e)
        {
            string name = e.Column.Name; // Название редактируемой колонки

            if (name == "_time")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                string time = row.time_.ToString("dd.MM.yyyy HH:mm:ss");

                fuelDataGridView2.SetRowCellValue(e.RowHandle, "time_", time);
            }

            if (name == "_endValue" || name == "_beginValue")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                FuelOne.Summary s = _summaries[curMrow.Mobitel_ID];

                if (row.dValue > 0)
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.dValue = row.endValue - row.beginValue;

                if (row.dValue > 0)
                {
                    s.Fueling += row.dValue;
                    s.Total += row.dValue;
                }
                else
                {
                    s.Discharge += row.dValue;
                }

                s.Rate = s.Total*100/curMrow.path;
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
            } // if

            atlantaDataSetBindingSource.ResumeBinding();
            atlantaDataSetBindingSource.ResetBindings(false);
            atlantaDataSetBindingSource.EndEdit();
            graph.RemoveMouseEventHandler();
            UpdateStatusLine();
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        } // fuelDataGridView_CellEndEdit

        private void fuelDataGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {

            if (e.RowHandle >= 0)
            {
                GridView view = sender as GridView;
                if (e.Column.FieldName == "dValue")
                {
                    double dbValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["dValue"]));
                    if (dbValue > 0.0)
                    {
                        e.Appearance.ForeColor = Color.Green;
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }
                    Int64 idDataGps = Convert.ToInt64(view.GetRowCellValue(e.RowHandle, view.Columns["DataGPS_ID"]));
                    if (idDataGps > 0)
                    {
                        if (buildGraph.IsInclinometerDangered(dataset, curMrow, idDataGps))
                        {
                            e.Appearance.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Добавляет к отчету новую строку
        /// </summary>
        protected void bindingNavigatorAddNewItem_Click()
        {
            ReportsControl.OnGraphShowNeeded();

            f_row = (atlantaDataSet.FuelReportRow)
                ((DataRowView) atlantaDataSetBindingSource.AddNew()).Row;

            f_row["GenerateType"] = "manual";

            numberChangedRow = fuelDataGridView2.FocusedRowHandle;

            Graph.Action += new ActionEventHandler(ZGraphControl_Action);
            graph.AddMouseEventHandler();
            XtraMessageBox.Show(Resources.FuelCtrlAddFuellingStartText, Resources.FuelCtrlAddFuellingCaption);
        } // bindingNavigatorAddNewItem_Click

        /// <summary>
        /// Удаляет из отчета выделенную строку
        /// </summary>
        protected void bindingNavigatorDeleteItem_Click()
        {
            atlantaDataSet.FuelReportRow row =
                (atlantaDataSet.FuelReportRow) ((DataRowView) atlantaDataSetBindingSource.Current).Row;
            FuelOne.Summary s = _summaries[curMrow.Mobitel_ID];

            // Необходимо оставить последнюю строку в отчете, чтобы сводные отчеты
            // не строили данный отчет заново
            if (curMrow.GetFuelerReportRows().Length == 1)
            {
                row.beginValue = 0;
                row.endValue = 0;
                row.dValue = 0;
                s.Fueling = 0;
                s.Discharge = 0;
                s.Total = s.Before - s.After;
                s.Rate = s.Total*100/curMrow.path;
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
                UpdateStatusLine();
                XtraMessageBox.Show(Resources.FuelCtrlDeleteError, Resources.Warning);
            }
            else
            {
                if (row.dValue > 0)
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                    s.Rate = s.Total*100/curMrow.path;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.Delete();
                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);
                UpdateStatusLine();
            } // else
        } // bindingNavigatorDeleteItem_Click

        private void connav_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Append)
            {
                bindingNavigatorAddNewItem_Click();
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                bindingNavigatorDeleteItem_Click();
            }

            e.Handled = true;
        }

        /// <summary>
        /// Обрабатывает выбор точек на графике при добавлении новой строки к отчету
        /// </summary>
        private void ZGraphControl_Action(object sender, ActionCancelEventArgs ev)
        {
            if (f_row == null)
                return;

            if (!begin_end)
            {
                //atlantaDataSet.dataviewRow d_row = dataset.dataview.FindByTime(curMrow, ev.Point.time);
                GpsData gpsData = DataSetManager.GetDataGpsArray(curMrow).First(gps => gps.Time == ev.Point.time);
                f_row.beginValue = ev.Point.value;
                f_row.time_ = ev.Point.time;
                f_row.date_ = ev.Point.time.ToShortDateString();
                f_row.Times = ev.Point.time.ToLongTimeString();
                f_row.DataGPS_ID = gpsData.Id;
                f_row.Location = Algorithm.FindLocation(gpsData.LatLng);
                f_row.mobitel_id = gpsData.Mobitel;
                f_row.sensor_id = 0;

                foreach (atlantaDataSet.sensorsRow s_row in curMrow.GetsensorsRows())
                {
                    if (s_row != null)
                    {
                        foreach (atlantaDataSet.relationalgorithmsRow ra_row in s_row.GetrelationalgorithmsRows())
                        {
                            if ((AlgorithmType) ra_row.AlgorithmID == AlgorithmType.FUEL1)
                            {
                                f_row.sensor_id = s_row.id;
                            } // if
                        } // foreach
                    } // if
                } // foreach
                begin_end = true;
                XtraMessageBox.Show(Resources.FuelCtrlAddFuellingFinishText, Resources.FuelCtrlAddFuellingCaption);
            } // if
            else
            {
                f_row.endValue = ev.Point.value;
                f_row.dValue = f_row.endValue - f_row.beginValue;
                FuelOne.Summary s = _summaries[curMrow.Mobitel_ID];
                if (f_row.dValue > 0)
                {
                    s.Fueling += f_row.dValue;
                    s.Total += f_row.dValue;
                    s.Rate = s.Total*100/curMrow.path;
                } // if
                else
                {
                    s.Discharge += f_row.dValue;
                }

                _summaries.Remove(curMrow.Mobitel_ID);
                _summaries.Add(curMrow.Mobitel_ID, s);

                UpdateStatusLine();
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.ResetBindings(false);
                atlantaDataSetBindingSource.EndEdit();
                graph.RemoveMouseEventHandler();
                Graph.Action -= ZGraphControl_Action;
                begin_end = false;
                ReportsControl.OnGraphShowNeeded();
                UpdateZGraph();
            } // else
        } // ZGraphControl_Action

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;
        }

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine()
        {
            FuelOne.Summary s = _summaries[curMrow.Mobitel_ID];

            _beforeLbl.Caption = Resources.BeforeLbl + " " + s.Before.ToString("N0");
            _afterLbl.Caption = Resources.AfterLbl + " " + s.After.ToString("N0");
            _fuelingLbl.Caption = Resources.FuelLbl + " " + s.Fueling.ToString("N0");
            _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s.Discharge).ToString("N0");
            _totalLbl.Caption = Resources.TotalLbl + " " + s.Total.ToString("N0");
            _rateLbl.Caption = Resources.RateLbl + " " + s.Rate.ToString("N2");
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s.AvgSpeed.ToString("N2");
        }

        // формирование расширенного отчета по всем выбранным машинам
        protected override void ExportAllDevToReportExtend()
        {
            colGenerateType.Visible = false;
            colHuman.Visible = true;
            colIdentification.Visible = true;
            colMarking.Visible = true;
            colNumberVeh.Visible = true;

            colHuman.VisibleIndex = 0;
            colNumberVeh.VisibleIndex = 1;
            colMarking.VisibleIndex = 2;
            colIdentification.VisibleIndex = 3;
            _location.VisibleIndex = 4;
            coldate.VisibleIndex = 5;
            coltime.VisibleIndex = 6;
            _quantity.VisibleIndex = 7;
            _beginValue.VisibleIndex = 8;
            _endValue.VisibleIndex = 9;

            ReportingFuel.CreateBindDataList();
            ReportingFuel.CreateElementReport();

            foreach (int mobitelId in _summaries.Keys)
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                //VehicleInfo info = new VehicleInfo(mobitel);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль

                if (info.DriverName == " ")
                {
                    tfinfo.infoDriverName = "------"; //+ Водитель
                }
                else
                {
                    tfinfo.infoDriverName = info.DriverFullName;
                }

                tfinfo.totalFuel = _summaries[mobitelId].Before; // +Литров топлива в начале
                tfinfo.FuelStops = _summaries[mobitelId].After; // Литров топлива в конце
                tfinfo.Fueling = _summaries[mobitelId].Fueling; // Заправлено литров всего
                tfinfo.totalFuelSub = _summaries[mobitelId].Discharge; // +Слито литров всего
                tfinfo.totalFuelAdd = _summaries[mobitelId].Total; // Всего литров израсходовано автомобилем
                tfinfo.MiddleFuel = _summaries[mobitelId].Rate; //+ Расход топлива на 100 км пробега
                tfinfo.MotionTreavelTime = _summaries[mobitelId].AvgSpeed; // +Средняя скорость движения
                tfinfo.registerNumber = info.RegistrationNumber;
                tfinfo.markVehicle = info.CarMaker;
                tfinfo.Identification = info.Identificator;

                tfinfo.BreakLeft = false;
                tfinfo.BreakMeedle = false;
                tfinfo.BreakRight = false;

                ReportingFuel.AddInfoStructToList(tfinfo);

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId, "id ASC"))
                {
                    string humanVeh = tfinfo.infoDriverName;
                    string numberVehicle = tfinfo.registerNumber;
                    string markingVehicle = tfinfo.markVehicle;
                    int identificator = tfinfo.Identification;
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;
                    string nameDut = row.NamesDut;

                    ReportingFuel.AddDataToBindList(new reportFuel(humanVeh, numberVehicle, markingVehicle,
                        identificator, location, date, time, value,
                        begin, end, genertype, 0, nameDut));
                } // foreach 1
            } // foreach 2

            ReportingFuel.CreateAndShowReport();
            ReportingFuel.DeleteData();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;
            colGenerateType.Visible = true;
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            foreach (int mobitelId in _summaries.Keys)
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль
                tfinfo.infoDriverName = info.DriverFullName; //+ Водитель
                tfinfo.totalFuel = _summaries[mobitelId].Before; // +Литров топлива в начале
                tfinfo.FuelStops = _summaries[mobitelId].After; // Литров топлива в конце
                tfinfo.Fueling = _summaries[mobitelId].Fueling; // Заправлено литров всего
                tfinfo.totalFuelSub = _summaries[mobitelId].Discharge; // +Слито литров всего
                tfinfo.totalFuelAdd = _summaries[mobitelId].Total; // Всего литров израсходовано автомобилем
                tfinfo.MiddleFuel = _summaries[mobitelId].Rate; //+ Расход топлива на 100 км пробега
                tfinfo.MotionTreavelTime = _summaries[mobitelId].AvgSpeed; // +Средняя скорость движения

                tfinfo.BreakLeft = true;
                tfinfo.BreakMeedle = true;
                tfinfo.BreakRight = true;

                ReportingFuel.AddInfoStructToList(tfinfo);
                ReportingFuel.CreateBindDataList();

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId, "id ASC"))
                {
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;
                    string typeDut = row.NamesDut;

                    ReportingFuel.AddDataToBindList(new reportFuel(location, date, time, value, begin, end, genertype, 0, typeDut));
                } // foreach 1

                ReportingFuel.CreateElementReport();
            } // foreach 2

            ReportingFuel.CreateAndShowReport();
            ReportingFuel.DeleteData();
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            TInfoDut tfinfo = new TInfoDut();

            tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
            tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(curMrow.Mobitel_ID);
            VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

            tfinfo.infoVehicle = info.Info; // Автомобиль
            tfinfo.infoDriverName = info.DriverFullName; // Водитель
            tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Before; // Литров топлива в начале
            tfinfo.FuelStops = _summaries[curMrow.Mobitel_ID].After; // Литров топлива в конце
            tfinfo.Fueling = _summaries[curMrow.Mobitel_ID].Fueling; // Заправлено литров всего
            tfinfo.totalFuelSub = _summaries[curMrow.Mobitel_ID].Discharge; // Слито литров всего
            tfinfo.totalFuelAdd = _summaries[curMrow.Mobitel_ID].Total; // Всего литров израсходовано автомобилем
            tfinfo.MiddleFuel = _summaries[curMrow.Mobitel_ID].Rate; // Расход топлива на 100 км пробега
            tfinfo.MotionTreavelTime = _summaries[curMrow.Mobitel_ID].AvgSpeed; // Средняя скорость движения

            tfinfo.BreakLeft = true;
            tfinfo.BreakMeedle = true;
            tfinfo.BreakRight = true;

            ReportingFuel.AddInfoStructToList(tfinfo); /* формируем заголовки таблиц отчета */
            ReportingFuel.CreateAndShowReport(fuelDataGridControl2);
        } // ExportToExcelDevExpress

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportFueler, e);
            TInfoDut info = ReportingFuel.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        protected double delsign(double s)
        {
            if (s < 0.0)
                return -s;

            return s;
        }

        /* функция для формирования верхней/средней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakMeedle)
            {
                ReportingFuel.SetRectangleBrckUP(450, 10, 200, 70);
                return (Resources.BeginFueling + ": " + info.totalFuel.ToString("N2") + "\n" +
                        Resources.FuelEnd + ": " + info.FuelStops.ToString("N2") + "\n" +
                        Resources.FuelLbl + " " + info.Fueling.ToString("N2") + "\n" +
                        Resources.DischargeLbl + " " + delsign(info.totalFuelSub).ToString("N2"));
            }
            else
            {
                return "";
            }
        }

        /* функция для формирования левой части заголовка отчета */

        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakLeft)
            {
                return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                        Resources.Driver + ": " + info.infoDriverName);
            }
            else
            {
                return "";
            }
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */

        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakRight)
            {
                ReportingFuel.SetRectangleBrckRight(900, 10, 200, 70);
                return (Resources.TotalLbl + " " + info.totalFuelAdd.ToString("N2") + "\n" +
                        Resources.RateLbl + " " + info.MiddleFuel.ToString("N2") + "\n" +
                        Resources.AvgSpeedLbl + " " + info.MotionTreavelTime.ToString("N2"));
            }

            return "";
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(fuelDataGridView2);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(fuelDataGridView2);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(fuelDataGridControl2);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar2);
        }

        public void Localization()
        {
            _location.Caption = Resources.Location;
            _location.ToolTip = Resources.Location;

            coldate.Caption = Resources.DateTime;
            coldate.ToolTip = Resources.FuellingDateTime;

            _quantity.Caption = Resources.FuellingValue;
            _quantity.ToolTip = Resources.FuellingValueHint;

            _beginValue.Caption = Resources.FuellLevelBeforeFuelling;
            _beginValue.ToolTip = Resources.FuellLevelBeforeFuellingHint;

            _endValue.Caption = Resources.FuellLevelAfterFuelling;
            _endValue.ToolTip = Resources.FuellLevelAfterFuellingHint;

            colHuman.Caption = Resources.ColHumanVehicle;
            colHuman.ToolTip = Resources.ColHumanVehicleTip;
            colNumberVeh.Caption = Resources.ColNumVehicle;
            colNumberVeh.ToolTip = Resources.ColNumVehicleTip;
            colMarking.Caption = Resources.ColMarking;
            colMarking.ToolTip = Resources.ColMarkingTip;
            colIdentification.Caption = Resources.ColIdent;
            colIdentification.ToolTip = Resources.ColIdentTip;
            coldate.Caption = Resources.ColDate;
            coldate.ToolTip = Resources.ColDateTip;
            coltime.Caption = Resources.ColTime;
            coltime.ToolTip = Resources.ColTimeTip;

            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;

            colGenerateType.Caption = Resources.GenerateType;
            colGenerateType.ToolTip = Resources.GenerateTypeTip;
        }

        private void FuelControl_Load(object sender, EventArgs e)
        {
            // to do
        }

        // Класс разработан для исправления вывода названий в заголовки колонок таблицы
        // во время генерации отчета
        [AttributeUsage(AttributeTargets.All)]
        public class DeveloperAttribute : DisplayNameAttribute
        {
            //Private fields.
            private string colHuman;
            private string colNumberVeh;
            private string colMarking;
            private string colIdentification;
            private string _location;
            private string coldate;
            private string coltime;
            private string _quantity;
            private string _beginvalue;
            private string _endvalue;
            private string _colgener;
            private int val;

            //This constructor defines two required parameters: name and level.
            public DeveloperAttribute(int val)
            {
                this.val = val;
                this.colHuman = Resources.ColHumanVehicle;
                this.colNumberVeh = Resources.ColNumVehicle;
                this.colMarking = Resources.ColMarking;
                this.colIdentification = Resources.ColIdent;
                this._location = Resources.Location;
                this.coldate = Resources.ColDate;
                this.coltime = Resources.ColTime;
                this._quantity = Resources.FuellingValue;
                this._beginvalue = Resources.FuellLevelBeforeFuelling;
                this._endvalue = Resources.FuellLevelAfterFuelling;
                this._colgener = Resources.GenerateType;
            }

            //Define DisplayName property
            public override string DisplayName
            {
                get
                {
                    switch (val)
                    {
                        case 0:
                            return colHuman;
                        case 1:
                            return colNumberVeh;
                        case 2:
                            return colMarking;
                        case 3:
                            return colIdentification;
                        case 4:
                            return this._location;
                        case 5:
                            return coldate;
                        case 6:
                            return coltime;
                        case 7:
                            return this._quantity;
                        case 8:
                            return this._beginvalue;
                        case 9:
                            return this._endvalue;
                        case 10:
                            return this._colgener;
                        default:
                            throw new Exception("This unknown property!");
                    } // switch
                } // get
            } // DisplayName
        } // DeveloperAttribute
    } // DevFuelControl
} // BaseReports