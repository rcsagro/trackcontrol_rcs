using System;
using System.Collections.Generic;
using BaseReports.Properties;

namespace BaseReports.CrossingCZ
{
    /// <summary>
    /// ����������� ���������� � ��.
    /// </summary>
    public class CrossZones : TrackControl.General.Core.Singleton<CrossZones>
    {
        private IDictionary<int, List<CrossingInfo>> crossZones;

        /// <summary>
        /// ���������� ���������� � ����������� � ������������ � ��.
        /// </summary>
        public int Count
        {
            get { return crossZones.Count; }
        }

        public CrossZones()
        {
            if (Instance != null)
            {
                throw new Exception(Resources.CrossZonesInstanceError);
            }
            crossZones = new Dictionary<int, List<CrossingInfo>>();
        }

        /// <summary>
        /// ������� ������ � ������������. 
        /// </summary>
        public void Clear()
        {
            crossZones.Clear();
        }

        /// <summary>
        /// ���������� ������ ����������� ��������� ���������.
        /// ���� ��� ������������ ������ � ������� ���������, ��
        /// �� ����� ��������.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="crossingList">������ ����������� � ��.</param>
        public void Add(int mobitelId, List<CrossingInfo> crossingList)
        {
            if (crossZones.ContainsKey(mobitelId))
            {
                crossZones.Remove(mobitelId);
            }
            crossZones.Add(mobitelId, crossingList);
        }

        /// <summary>
        /// ������ ����������� ��������� ��������� � ��.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <returns>������ �����������.</returns>
        public IEnumerable<CrossingInfo> GetCrossInfoCollection(int mobitelId)
        {
            return crossZones[mobitelId];
        }

        /// <summary>
        /// ������ ����������� ��������� ��������� � ��.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <returns>������ �����������.</returns>
        public CrossingInfo[] GetCrossInfoArray(int mobitelId)
        {
            return crossZones[mobitelId].ToArray();
        }

        /// <summary>
        /// ���������� ����������� � �� � ��������� ���������.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <returns>���������� �����������.</returns>
        public int GetCrossingCount(int mobitelId)
        {
            if (crossZones.ContainsKey(mobitelId))
            {
                return crossZones[mobitelId].Count;
            }
            return 0;
        }
    }
}
