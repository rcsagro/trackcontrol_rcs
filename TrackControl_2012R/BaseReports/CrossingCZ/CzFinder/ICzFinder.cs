using System.Collections.Generic;
using TrackControl.General;

namespace BaseReports.CrossingCZ.CzFinder
{
  /// <summary>
  /// ����� ��.
  /// </summary>
  interface ICzFinder
  {
    /// <summary>
    /// ����� �� ������� ����������� �����.
    /// </summary>
    /// <param name="latLng">����� � �������������� �����������</param>
    /// <returns>������ ��������������� ��, 
    /// ������� ����������� �����.</returns>
    List<int> GetZonesWithPoint(PointLatLng latLng);
  }
}
