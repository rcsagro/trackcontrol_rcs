using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Zones;

namespace BaseReports.CrossingCZ.CzFinder
{
  /// <summary>
  /// ����� �� � ���������������� ����������.
  /// </summary>
  class CustomCzFinder : ICzFinder
  {
    IEntityFinder<IZone> _zoneFinder;
    private List<int> zonesIdArray;

    private CustomCzFinder() { }
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="model">����������� ����������� ���</param>
    /// <param name="list">������ ��������������� ��.</param>
    public CustomCzFinder(IEntityFinder<IZone> zoneFinder, List<int> list)
    {
      _zoneFinder = zoneFinder;
      zonesIdArray = list;
    }

    /// <summary>
    /// ����� �� ������� ����������� �����.
    /// </summary>
    /// <param name="latLng">����� � �������������� �����������</param>
    /// <returns>������ ��������������� ��, 
    /// ������� ����������� �����.</returns>
    public List<int> GetZonesWithPoint(PointLatLng latLng)
    {
      List<int> result = new List<int>();
      foreach (int id in zonesIdArray)
      {
        IZone z = _zoneFinder.GetById(id);
        if ((z != null) && (z.Contains(latLng)))
        {
          result.Add(z.Id);
        }
      }
      return result;
    }
  }
}
