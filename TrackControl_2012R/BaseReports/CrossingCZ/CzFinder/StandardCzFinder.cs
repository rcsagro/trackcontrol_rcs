using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Zones;

namespace BaseReports.CrossingCZ.CzFinder
{
    /// <summary>
    /// ������� ��������� ��.
    /// </summary>
    class StandardCzFinder : ICzFinder
    {
        private IZonesLocator zonesLocator;

        private StandardCzFinder()
        {
        }

        public IZonesLocator GetLocator
        {
            get { return zonesLocator; }
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="locator">IZonesLocator.</param>
        public StandardCzFinder(IZonesLocator locator)
        {
            zonesLocator = locator;
        }

        /// <summary>
        /// ����� �� ������� ����������� �����.
        /// </summary>
        /// <param name="latLng">����� � �������������� �����������</param>
        /// <returns>������ ��������������� ��, 
        /// ������� ����������� �����.</returns>
        public List<int> GetZonesWithPoint(PointLatLng latLng)
        {
            List<int> result = new List<int>();
            foreach (IZone z in zonesLocator.GetCheckedZonesWithPoint(latLng))
            {
                result.Add(z.Id);
            }
            return result;
        }
    }
}
