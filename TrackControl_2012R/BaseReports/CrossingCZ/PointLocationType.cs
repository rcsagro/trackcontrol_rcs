
namespace BaseReports.CrossingCZ
{
  /// <summary>
  /// ��� ������������ ����� ������������ ����������� ���.
  /// </summary>
  public enum PointLocationType
  {
    /// <summary>
    /// ����� � �� - �����������.
    /// </summary>
    CrossIn,
    /// <summary>
    /// ����� �� �� - �����������.
    /// </summary>
    CrossOut,
    /// <summary>
    /// ������ ����� � �� ��� ������ � ���������� �� ��� 
    /// ������������� �������� ���������� ��������       
    /// �������� ����� � ��.
    /// </summary>
    BeginIn,
    /// <summary>
    /// ��������� ����� � �� ��� ��������� � �� ��� ������������� 
    /// �������� ���������� �������� �������� ����� �� ��.
    /// </summary>
    EndIn,
    /// <summary>
    /// ������ ����� �� ��������� ��.
    /// </summary>
    BeginOut,
    /// <summary>
    /// ��������� ����� �� ��������� ��.
    /// </summary>
    EndOut
  }
}
