using System;

namespace BaseReports.CrossingCZ
{
    /// <summary>
    /// �������� ���������� � ����� ����������� ��.
    /// </summary>
    public class CrossingInfo
    {
        private int mobitelId;

        /// <summary>
        /// ������������� ���������.
        /// </summary>
        public int MobitelId
        {
            get { return mobitelId; }
        }

        private int zoneID;

        /// <summary>
        /// ������������� ��.
        /// </summary>
        public int ZoneID
        {
            get { return zoneID; }
        }

        private string zoneName;

        /// <summary>
        /// ������������ ��.
        /// </summary>
        public string ZoneName
        {
            get { return zoneName; }
        }

        private System.Int64 pointId;

        /// <summary>
        /// ������������� �����.
        /// </summary>
        public System.Int64 PointId
        {
            get { return pointId; }
        }

        private PointLocationType pointLocationType;

        /// <summary>
        /// ��� ������������ ����� ������������ ��.
        /// </summary>
        public PointLocationType PointLocationType
        {
            get { return pointLocationType; }
        }

        private DateTime crossingTime;

        /// <summary>
        /// ���� � ����� �����������.
        /// </summary>
        public DateTime CrossingTime
        {
            get { return crossingTime; }
        }

        private float kilometrage;

        /// <summary>
        /// ����������, ���������� � ������� ����������� �����������.
        /// </summary>
        public float Kilometrage
        {
            get { return kilometrage; }
            set { kilometrage = value; }
        }

        private CrossingInfo()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="zoneID">������������� ��.</param>
        /// <param name="pointId">������������� �����.</param>
        /// <param name="zoneName">������������ ��.</param>
        /// <param name="pointLocationType">��� ������������ ����� ������������ ��.</param>
        /// <param name="crossingTime">���� � ����� �����������.</param>
        /// <param name="kilometrage">����������, ���������� � ������� ����������� �����������.</param>
        public CrossingInfo(int mobitelId, int zoneID, System.Int64 pointId,
            string zoneName, PointLocationType pointLocationType, DateTime crossingTime,
            float kilometrage)
        {
            this.mobitelId = mobitelId;
            this.zoneID = zoneID;
            this.zoneName = zoneName;
            this.pointId = pointId;
            this.pointLocationType = pointLocationType;
            this.crossingTime = crossingTime;
            this.kilometrage = kilometrage;
        }
    }
}
