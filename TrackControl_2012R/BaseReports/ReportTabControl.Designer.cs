using System.Drawing;
namespace BaseReports
{
  partial class ReportTabControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportTabControl));
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.reportBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.runToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._stopBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.showMapToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.ShowGraphToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._excelDropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.excelSplitItemCurrent = new System.Windows.Forms.ToolStripMenuItem();
            this.excelSplitItemAll = new System.Windows.Forms.ToolStripMenuItem();
            this.ExcelToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.tsbPrintExport = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiDetal = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTotal = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVehicalOne = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVehicalAll = new System.Windows.Forms.ToolStripMenuItem();
            this.fieldGridToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.btXML = new System.Windows.Forms.ToolStripButton();
            this.reportPanel = new System.Windows.Forms.Panel();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingNavigator)).BeginInit();
            this.reportBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printToolStripMenuItem,
            this.excelToolStripMenuItem,
            this.toolStripSeparator4,
            this.mapToolStripMenuItem,
            this.graphToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip, "contextMenuStrip");
            // 
            // printToolStripMenuItem
            // 
            resources.ApplyResources(this.printToolStripMenuItem, "printToolStripMenuItem");
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // excelToolStripMenuItem
            // 
            resources.ApplyResources(this.excelToolStripMenuItem, "excelToolStripMenuItem");
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.ExcelToolStripButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // mapToolStripMenuItem
            // 
            resources.ApplyResources(this.mapToolStripMenuItem, "mapToolStripMenuItem");
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Click += new System.EventHandler(this.showMapToolStripButton_Click);
            // 
            // graphToolStripMenuItem
            // 
            resources.ApplyResources(this.graphToolStripMenuItem, "graphToolStripMenuItem");
            this.graphToolStripMenuItem.Name = "graphToolStripMenuItem";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            resources.ApplyResources(this.bindingNavigatorSeparator, "bindingNavigatorSeparator");
            // 
            // bindingNavigatorPositionItem
            // 
            resources.ApplyResources(this.bindingNavigatorPositionItem, "bindingNavigatorPositionItem");
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            resources.ApplyResources(this.bindingNavigatorCountItem, "bindingNavigatorCountItem");
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            resources.ApplyResources(this.bindingNavigatorSeparator1, "bindingNavigatorSeparator1");
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            resources.ApplyResources(this.bindingNavigatorSeparator2, "bindingNavigatorSeparator2");
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // reportBindingNavigator
            // 
            this.reportBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.reportBindingNavigator.BindingSource = this.atlantaDataSetBindingSource;
            this.reportBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.reportBindingNavigator.DeleteItem = null;
            this.reportBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.reportBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripButton,
            this._stopBtn,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSeparator5,
            this.showMapToolStripButton,
            this.ShowGraphToolStripButton,
            this.toolStripSeparator2,
            this.printToolStripButton,
            this._excelDropDown,
            this.ExcelToolStripButton,
            this.tsbPrintExport,
            this.toolStripSeparator3,
            this.fieldGridToolStripSplitButton,
            this.btXML});
            resources.ApplyResources(this.reportBindingNavigator, "reportBindingNavigator");
            this.reportBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.reportBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.reportBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.reportBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.reportBindingNavigator.Name = "reportBindingNavigator";
            this.reportBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorAddNewItem, "bindingNavigatorAddNewItem");
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // runToolStripButton
            // 
            this.runToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.runToolStripButton, "runToolStripButton");
            this.runToolStripButton.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.runToolStripButton.Name = "runToolStripButton";
            this.runToolStripButton.Click += new System.EventHandler(this.runToolStripButton_Click);
            // 
            // _stopBtn
            // 
            this._stopBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._stopBtn, "_stopBtn");
            this._stopBtn.Margin = new System.Windows.Forms.Padding(0);
            this._stopBtn.Name = "_stopBtn";
            this._stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorMoveFirstItem, "bindingNavigatorMoveFirstItem");
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorMovePreviousItem, "bindingNavigatorMovePreviousItem");
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorMoveNextItem, "bindingNavigatorMoveNextItem");
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorMoveLastItem, "bindingNavigatorMoveLastItem");
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.bindingNavigatorDeleteItem, "bindingNavigatorDeleteItem");
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // showMapToolStripButton
            // 
            this.showMapToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.showMapToolStripButton, "showMapToolStripButton");
            this.showMapToolStripButton.Name = "showMapToolStripButton";
            this.showMapToolStripButton.Click += new System.EventHandler(this.showMapToolStripButton_Click);
            // 
            // ShowGraphToolStripButton
            // 
            this.ShowGraphToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ShowGraphToolStripButton, "ShowGraphToolStripButton");
            this.ShowGraphToolStripButton.Name = "ShowGraphToolStripButton";
            this.ShowGraphToolStripButton.Click += new System.EventHandler(this.ShowGraphToolStripButton_Click);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.printToolStripButton, "printToolStripButton");
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            this.printToolStripButton.EnabledChanged += new System.EventHandler(this.printToolStripButton_EnabledChanged);
            // 
            // _excelDropDown
            // 
            this._excelDropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelSplitItemCurrent,
            this.excelSplitItemAll});
            resources.ApplyResources(this._excelDropDown, "_excelDropDown");
            this._excelDropDown.Name = "_excelDropDown";
            this._excelDropDown.EnabledChanged += new System.EventHandler(this.ExcelToolStripButton_EnabledChanged);
            // 
            // excelSplitItemCurrent
            // 
            this.excelSplitItemCurrent.Name = "excelSplitItemCurrent";
            resources.ApplyResources(this.excelSplitItemCurrent, "excelSplitItemCurrent");
            this.excelSplitItemCurrent.Click += new System.EventHandler(this.excelSplitItemCurrent_Click);
            // 
            // excelSplitItemAll
            // 
            this.excelSplitItemAll.Name = "excelSplitItemAll";
            resources.ApplyResources(this.excelSplitItemAll, "excelSplitItemAll");
            this.excelSplitItemAll.Click += new System.EventHandler(this.excelSplitItemAll_Click);
            // 
            // ExcelToolStripButton
            // 
            this.ExcelToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ExcelToolStripButton, "ExcelToolStripButton");
            this.ExcelToolStripButton.Name = "ExcelToolStripButton";
            this.ExcelToolStripButton.Click += new System.EventHandler(this.ExcelToolStripButton_Click);
            this.ExcelToolStripButton.EnabledChanged += new System.EventHandler(this.ExcelToolStripButton_EnabledChanged);
            // 
            // tsbPrintExport
            // 
            this.tsbPrintExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDetal,
            this.tsmiTotal,
            this.tsmiVehicalOne,
            this.tsmiVehicalAll});
            resources.ApplyResources(this.tsbPrintExport, "tsbPrintExport");
            this.tsbPrintExport.Name = "tsbPrintExport";
            // 
            // tsmiDetal
            // 
            this.tsmiDetal.Name = "tsmiDetal";
            resources.ApplyResources(this.tsmiDetal, "tsmiDetal");
            this.tsmiDetal.Click += new System.EventHandler(this.tsmiDetal_Click);
            // 
            // tsmiTotal
            // 
            resources.ApplyResources(this.tsmiTotal, "tsmiTotal");
            this.tsmiTotal.Name = "tsmiTotal";
            this.tsmiTotal.Click += new System.EventHandler(this.tsmiTotal_Click);
            // 
            // tsmiVehicalOne
            // 
            this.tsmiVehicalOne.Name = "tsmiVehicalOne";
            resources.ApplyResources(this.tsmiVehicalOne, "tsmiVehicalOne");
            this.tsmiVehicalOne.Click += new System.EventHandler(this.tsmiVehicalOne_Click);
            // 
            // tsmiVehicalAll
            // 
            this.tsmiVehicalAll.Name = "tsmiVehicalAll";
            resources.ApplyResources(this.tsmiVehicalAll, "tsmiVehicalAll");
            this.tsmiVehicalAll.Click += new System.EventHandler(this.tsmiVehicalAll_Click);
            // 
            // fieldGridToolStripSplitButton
            // 
            this.fieldGridToolStripSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.fieldGridToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.fieldGridToolStripSplitButton, "fieldGridToolStripSplitButton");
            this.fieldGridToolStripSplitButton.Name = "fieldGridToolStripSplitButton";
            // 
            // btXML
            // 
            this.btXML.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btXML, "btXML");
            this.btXML.Name = "btXML";
            this.btXML.Click += new System.EventHandler(this.btXML_Click);
            // 
            // reportPanel
            // 
            resources.ApplyResources(this.reportPanel, "reportPanel");
            this.reportPanel.Name = "reportPanel";
            // 
            // ReportTabControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.reportPanel);
            this.Controls.Add(this.reportBindingNavigator);
            this.Name = "ReportTabControl";
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingNavigator)).EndInit();
            this.reportBindingNavigator.ResumeLayout(false);
            this.reportBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

   }

    #endregion

   private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem graphToolStripMenuItem;
    protected System.Windows.Forms.ToolStripButton runToolStripButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    protected System.Windows.Forms.BindingNavigator reportBindingNavigator;
    protected System.Windows.Forms.Panel reportPanel;
    protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
    protected System.Windows.Forms.ToolStripButton ExcelToolStripButton;
    protected System.Windows.Forms.ToolStripButton showMapToolStripButton;
    protected System.Windows.Forms.ToolStripButton ShowGraphToolStripButton;
    protected System.Windows.Forms.ToolStripButton printToolStripButton;
    //protected C1.Win.C1Report.C1Report c1Report;
    protected System.Windows.Forms.ToolStripSplitButton fieldGridToolStripSplitButton;
    //protected C1.C1Excel.C1XLBook c1XLBook;
    protected System.Windows.Forms.ContextMenuStrip contextMenuStrip;
    protected System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
    protected System.Windows.Forms.ToolStripButton _stopBtn;
    private System.Windows.Forms.ToolStripButton btXML;
    protected System.Windows.Forms.ToolStripDropDownButton _excelDropDown;
    protected System.Windows.Forms.ToolStripMenuItem excelSplitItemCurrent;
    protected System.Windows.Forms.ToolStripMenuItem excelSplitItemAll;
    protected System.Windows.Forms.ToolStripDropDownButton tsbPrintExport;
    protected System.Windows.Forms.ToolStripMenuItem tsmiDetal;
    protected System.Windows.Forms.ToolStripMenuItem tsmiTotal;
    protected System.Windows.Forms.ToolStripMenuItem tsmiVehicalOne;
    protected System.Windows.Forms.ToolStripMenuItem tsmiVehicalAll;
  }
}
