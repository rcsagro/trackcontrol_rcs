using System;
using System.Windows.Forms;
using BaseReports.Properties;
using TrackControl.Reports.Graph;

namespace BaseReports
{
  public partial class FuelAddDlg : Form
  {
    private IGraph graph;
    private SelPoint beginSelPoint;
    private SelPoint endSelPoint;
    private bool state;
    public Label Label
    {
      set { label = value; }
    }

    public FuelAddDlg(IGraph graph)
    {
      InitializeComponent();
      this.graph = graph;
      label.Text = Resources.FuelAddDlgFuellingStartingPoint;
      Text = Resources.FuelAddDlgCaption;
    }

    public new void Show()
    {
      graph.AddMouseEventHandler();
      base.Show();
    }

    private void okButton_Click(object sender, EventArgs e)
    {
      DateTime time;
      double value;
      if (graph.GetSelectPoint(out time, out value))
      {
        if (!state)
        {
          beginSelPoint.time = time;
          beginSelPoint.value = value;
          state = true;
          label.Text = Resources.FuelAddDlgFuellingEndPoint;
        }
        else
        {
          endSelPoint.time = time;
          endSelPoint.value = value;
          state = false;
          Hide();
          label.Text = Resources.FuelAddDlgFuellingStartingPoint;
        }
      }
    }
  }
}