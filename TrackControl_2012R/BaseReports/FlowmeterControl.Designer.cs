using LocalCache;
namespace BaseReports
{
  partial class FlowmeterControl
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlowmeterControl));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        this.flowmeterReportDataGridView = new System.Windows.Forms.DataGridView();
        this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.idleFuelRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.moveFuelRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.flowmeterReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.atlantaDataSet = new LocalCache.atlantaDataSet();
        this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        this._travelLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.travelLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this._timeTravelLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.timeTravelLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this._stopTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.stopTimeLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this._flowmeterLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.flowmeterLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this._idleFuelRateLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.idleFuelRateLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this._moveFuelRateLabel = new System.Windows.Forms.ToolStripStatusLabel();
        this.moveFuelRateLabelData = new System.Windows.Forms.ToolStripStatusLabel();
        this.reportPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportDataGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
        this.statusStrip1.SuspendLayout();
        this.SuspendLayout();
        // 
        // reportPanel
        // 
        this.reportPanel.Controls.Add(this.flowmeterReportDataGridView);
        this.reportPanel.Controls.Add(this.statusStrip1);
        this.reportPanel.Size = new System.Drawing.Size(1428, 443);
        // 
        // c1Report
        // 
        this.c1Report.ReportDefinition = resources.GetString("c1Report.ReportDefinition");
        this.c1Report.ReportName = "Flowmeter";
        // 
        // flowmeterReportDataGridView
        // 
        this.flowmeterReportDataGridView.AllowUserToAddRows = false;
        dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
        this.flowmeterReportDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.flowmeterReportDataGridView.AutoGenerateColumns = false;
        this.flowmeterReportDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
        this.flowmeterReportDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.flowmeterReportDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        this.flowmeterReportDataGridView.ColumnHeadersHeight = 36;
        this.flowmeterReportDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.idleFuelRate,
            this.moveFuelRate});
        this.flowmeterReportDataGridView.DataSource = this.flowmeterReportBindingSource;
        this.flowmeterReportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.flowmeterReportDataGridView.Location = new System.Drawing.Point(0, 0);
        this.flowmeterReportDataGridView.Name = "flowmeterReportDataGridView";
        this.flowmeterReportDataGridView.ReadOnly = true;
        this.flowmeterReportDataGridView.RowHeadersVisible = false;
        this.flowmeterReportDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.flowmeterReportDataGridView.Size = new System.Drawing.Size(1428, 421);
        this.flowmeterReportDataGridView.TabIndex = 0;
        this.flowmeterReportDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.flowmeterReportDataGridView_CellMouseDoubleClick);
        // 
        // dataGridViewTextBoxColumn2
        // 
        this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn2.DataPropertyName = "Location";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
        this.dataGridViewTextBoxColumn2.HeaderText = "��������� / �������������� ";
        this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
        this.dataGridViewTextBoxColumn2.ReadOnly = true;
        this.dataGridViewTextBoxColumn2.Width = 171;
        // 
        // dataGridViewTextBoxColumn3
        // 
        this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn3.DataPropertyName = "beginTime";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle4;
        this.dataGridViewTextBoxColumn3.HeaderText = "����� ������";
        this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
        this.dataGridViewTextBoxColumn3.ReadOnly = true;
        this.dataGridViewTextBoxColumn3.Width = 95;
        // 
        // dataGridViewTextBoxColumn4
        // 
        this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn4.DataPropertyName = "endTime";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle5;
        this.dataGridViewTextBoxColumn4.HeaderText = "����� ���������";
        this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
        this.dataGridViewTextBoxColumn4.ReadOnly = true;
        this.dataGridViewTextBoxColumn4.Width = 111;
        // 
        // dataGridViewTextBoxColumn7
        // 
        this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn7.DataPropertyName = "duration";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
        this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle6;
        this.dataGridViewTextBoxColumn7.HeaderText = "������ �.�����";
        this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
        this.dataGridViewTextBoxColumn7.ReadOnly = true;
        this.dataGridViewTextBoxColumn7.Width = 105;
        // 
        // dataGridViewTextBoxColumn8
        // 
        this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn8.DataPropertyName = "travel";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
        dataGridViewCellStyle7.Format = "N1";
        this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle7;
        this.dataGridViewTextBoxColumn8.HeaderText = "���� (��)";
        this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
        this.dataGridViewTextBoxColumn8.ReadOnly = true;
        this.dataGridViewTextBoxColumn8.ToolTipText = "���������� ����";
        this.dataGridViewTextBoxColumn8.Width = 73;
        // 
        // dataGridViewTextBoxColumn9
        // 
        this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn9.DataPropertyName = "fuelRate";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
        dataGridViewCellStyle8.Format = "N1";
        this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle8;
        this.dataGridViewTextBoxColumn9.HeaderText = "������� ����� (�)";
        this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
        this.dataGridViewTextBoxColumn9.ReadOnly = true;
        this.dataGridViewTextBoxColumn9.ToolTipText = "����� ���������� ���������������� �������";
        this.dataGridViewTextBoxColumn9.Width = 101;
        // 
        // idleFuelRate
        // 
        this.idleFuelRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.idleFuelRate.DataPropertyName = "idleFuelRate";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
        dataGridViewCellStyle9.Format = "N1";
        this.idleFuelRate.DefaultCellStyle = dataGridViewCellStyle9;
        this.idleFuelRate.HeaderText = "������ ��. (�/�)";
        this.idleFuelRate.Name = "idleFuelRate";
        this.idleFuelRate.ReadOnly = true;
        this.idleFuelRate.ToolTipText = "������ ������� �� ����� �������";
        this.idleFuelRate.Width = 82;
        // 
        // moveFuelRate
        // 
        this.moveFuelRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.moveFuelRate.DataPropertyName = "moveFuelRate";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
        dataGridViewCellStyle10.Format = "N1";
        this.moveFuelRate.DefaultCellStyle = dataGridViewCellStyle10;
        this.moveFuelRate.HeaderText = "������ (�/100��)";
        this.moveFuelRate.Name = "moveFuelRate";
        this.moveFuelRate.ReadOnly = true;
        this.moveFuelRate.ToolTipText = "������ ������� ��� ��������";
        this.moveFuelRate.Width = 110;
        // 
        // flowmeterReportBindingSource
        // 
        this.flowmeterReportBindingSource.DataMember = "flowmeterReport";
        this.flowmeterReportBindingSource.DataSource = this.atlantaDataSet;
        // 
        // atlantaDataSet
        // 
        this.atlantaDataSet.DataSetName = "atlantaDataSet";
        this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // statusStrip1
        // 
        this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._travelLabel,
            this.travelLabelData,
            this._timeTravelLabel,
            this.timeTravelLabelData,
            this._stopTimeLabel,
            this.stopTimeLabelData,
            this._flowmeterLabel,
            this.flowmeterLabelData,
            this._idleFuelRateLabel,
            this.idleFuelRateLabelData,
            this._moveFuelRateLabel,
            this.moveFuelRateLabelData});
        this.statusStrip1.Location = new System.Drawing.Point(0, 421);
        this.statusStrip1.Name = "statusStrip1";
        this.statusStrip1.Size = new System.Drawing.Size(1428, 22);
        this.statusStrip1.TabIndex = 1;
        this.statusStrip1.Text = "statusStrip1";
        // 
        // _travelLabel
        // 
        this._travelLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._travelLabel.Name = "_travelLabel";
        this._travelLabel.Size = new System.Drawing.Size(105, 17);
        this._travelLabel.Text = "���������� ����: ";
        // 
        // travelLabelData
        // 
        this.travelLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.travelLabelData.Name = "travelLabelData";
        this.travelLabelData.Size = new System.Drawing.Size(45, 17);
        this.travelLabelData.Text = " -,-- ��";
        // 
        // _timeTravelLabel
        // 
        this._timeTravelLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._timeTravelLabel.Name = "_timeTravelLabel";
        this._timeTravelLabel.Size = new System.Drawing.Size(215, 17);
        this._timeTravelLabel.Text = "����� � ���� (��� �������� ���������): ";
        // 
        // timeTravelLabelData
        // 
        this.timeTravelLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.timeTravelLabelData.Name = "timeTravelLabelData";
        this.timeTravelLabelData.Size = new System.Drawing.Size(46, 17);
        this.timeTravelLabelData.Text = " --:--:--";
        // 
        // _stopTimeLabel
        // 
        this._stopTimeLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._stopTimeLabel.Name = "_stopTimeLabel";
        this._stopTimeLabel.Size = new System.Drawing.Size(229, 17);
        this._stopTimeLabel.Text = "����� ������� (� ��������� �����������): ";
        // 
        // stopTimeLabelData
        // 
        this.stopTimeLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.stopTimeLabelData.Name = "stopTimeLabelData";
        this.stopTimeLabelData.Size = new System.Drawing.Size(43, 17);
        this.stopTimeLabelData.Text = "--:--:--";
        // 
        // _flowmeterLabel
        // 
        this._flowmeterLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._flowmeterLabel.Name = "_flowmeterLabel";
        this._flowmeterLabel.Size = new System.Drawing.Size(87, 17);
        this._flowmeterLabel.Text = "����� �������: ";
        // 
        // flowmeterLabelData
        // 
        this.flowmeterLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.flowmeterLabelData.Name = "flowmeterLabelData";
        this.flowmeterLabelData.Size = new System.Drawing.Size(36, 17);
        this.flowmeterLabelData.Text = "-,-- �";
        // 
        // _idleFuelRateLabel
        // 
        this._idleFuelRateLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._idleFuelRateLabel.Name = "_idleFuelRateLabel";
        this._idleFuelRateLabel.Size = new System.Drawing.Size(65, 17);
        this._idleFuelRateLabel.Text = "��������: ";
        // 
        // idleFuelRateLabelData
        // 
        this.idleFuelRateLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.idleFuelRateLabelData.Name = "idleFuelRateLabelData";
        this.idleFuelRateLabelData.Size = new System.Drawing.Size(38, 17);
        this.idleFuelRateLabelData.Text = "-- �/�";
        // 
        // _moveFuelRateLabel
        // 
        this._moveFuelRateLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        this._moveFuelRateLabel.Name = "_moveFuelRateLabel";
        this._moveFuelRateLabel.Size = new System.Drawing.Size(50, 17);
        this._moveFuelRateLabel.Text = "������: ";
        // 
        // moveFuelRateLabelData
        // 
        this.moveFuelRateLabelData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.moveFuelRateLabelData.Name = "moveFuelRateLabelData";
        this.moveFuelRateLabelData.Size = new System.Drawing.Size(70, 17);
        this.moveFuelRateLabelData.Text = "-,-- �/100��";
        // 
        // FlowmeterControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.Name = "FlowmeterControl";
        this.Size = new System.Drawing.Size(1430, 474);
        this.Controls.SetChildIndex(this.reportPanel, 0);
        this.reportPanel.ResumeLayout(false);
        this.reportPanel.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportDataGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.flowmeterReportBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
        this.statusStrip1.ResumeLayout(false);
        this.statusStrip1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.DataGridView flowmeterReportDataGridView;
    private System.Windows.Forms.BindingSource flowmeterReportBindingSource;
    private atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.ToolStripStatusLabel _travelLabel;
    private System.Windows.Forms.ToolStripStatusLabel _flowmeterLabel;
    private System.Windows.Forms.ToolStripStatusLabel _timeTravelLabel;
    private System.Windows.Forms.ToolStripStatusLabel _stopTimeLabel;
    private System.Windows.Forms.ToolStripStatusLabel _idleFuelRateLabel;
    private System.Windows.Forms.ToolStripStatusLabel _moveFuelRateLabel;
    private System.Windows.Forms.ToolStripStatusLabel travelLabelData;
    private System.Windows.Forms.ToolStripStatusLabel timeTravelLabelData;
    private System.Windows.Forms.ToolStripStatusLabel stopTimeLabelData;
    private System.Windows.Forms.ToolStripStatusLabel flowmeterLabelData;
    private System.Windows.Forms.ToolStripStatusLabel idleFuelRateLabelData;
    private System.Windows.Forms.ToolStripStatusLabel moveFuelRateLabelData;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn idleFuelRate;
    private System.Windows.Forms.DataGridViewTextBoxColumn moveFuelRate;
  }
}
