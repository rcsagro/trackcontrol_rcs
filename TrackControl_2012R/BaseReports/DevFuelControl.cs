﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using DevExpress.XtraPrinting;
using DevExpress.XtraEditors;
using BaseReports.ReportsDE;
using BaseReports.RFID;
using TrackControl.Vehicles;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Report;

using TrackControl.General;

namespace BaseReports
{
    [System.ComponentModel.ToolboxItem( false )]
    public partial class DevFuelControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportFuel
        {
            string location;
            DateTime time;
            double value;
            double begin;
            double end;
            string genertype;

            public reportFuel( string location, DateTime time, double value, double begin, double end, string genertype )
            {
                this.location = location;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
            } // reportFuel

            public string Location
            {
                get { return location; }
            }

            public DateTime time_
            {
                get { return time; }
            }

            public double dValue
            {
                get { return value; }
            }

            public double beginValue
            {
                get { return begin; }
            }

            public double endValue
            {
                get { return end; }
            }

            public string GenerateType
            {
                get { return genertype; }
            }
        } // reportFuel

        /// <summary>
        /// Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства). Ключом является ID телетрека.
        /// </summary>
        private Dictionary<int, Fuel.Summary> _summaries = new Dictionary<int, Fuel.Summary>();
        private FuelAddDlg fuelAddDlg;
        private atlantaDataSet.FuelReportRow f_row;
        private Fuel fuelAlg;
        private bool begin_end;

        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;
        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ControlNavigator connav;
        ReportBase<reportFuel, TInfoDut> ReportingFuel;
        int numberChangedRow;

        /// <summary>
        /// Создает и инициализирует новый экземпляр класса BaseReports.FuelControl
        /// </summary>
        /// <param name="caption"></param>
        public DevFuelControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel( fuelDataGridView, fuelDataGridControl, bar2 );
            buildGraph = new BuildGraphs();
            fuelAlg = new Fuel( AlgorithmType.FUEL1 );
            AddAlgorithm( new Kilometrage() );
            AddAlgorithm( fuelAlg );
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "FuelReport";
            fuelDataGridControl.DataSource = atlantaDataSetBindingSource;
            fuelAddDlg = new FuelAddDlg( graph );
            fuelDataGridControl.UseEmbeddedNavigator = true;
            barButtonNavigator.Caption = Resources.ResButtonNavigatorHide;
            connav = fuelDataGridControl.EmbeddedNavigator;
            connav.Buttons.BeginUpdate();
            try
            {
                connav.Buttons.Append.Visible = true;
                connav.Buttons.Remove.Visible = true;
                connav.Buttons.Edit.Visible = true;
                connav.Buttons.EndEdit.Visible = true;
            }
            finally
            {
                connav.Buttons.EndUpdate();
            }

            //fuelDataGridView.CustomColumnDisplayText += new CustomColumnDisplayTextEventHandler( fuelDataGridView_CellFormatting );
            fuelDataGridView.RowCellClick += new RowCellClickEventHandler(fuelDataGridView_CellMouseDoubleClick);
            fuelDataGridView.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler( fuelDataGridView_CellValidating );
            fuelDataGridView.CellValueChanged += new CellValueChangedEventHandler( fuelDataGridView_CellEndEdit );
            connav.ButtonClick += new NavigatorButtonClickEventHandler(connav_ButtonClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler( composLink_CreateMarginalHeaderArea );

            ReportingFuel =
                new ReportBase<reportFuel, TInfoDut>( Controls, compositeLink1, fuelDataGridView,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp );
        } // DevFuelControl

        public override string Caption
        {
            get { return Resources.FuelCtrlCaption; }
        }

        protected override void Algorithm_Action( object sender, EventArgs e )
        {
            if ( sender is Fuel )
            {
                if ( e is FuelEventArgs )
                {
                    FuelEventArgs args = ( FuelEventArgs )e;
                    if ( _summaries.ContainsKey( args.Id ) )
                    {
                        _summaries.Remove( args.Id );
                    }

                    _summaries.Add( args.Id, args.Summary );
                } // if
            } // if
        } // Algorithm_Action

        /// <summary>
        /// Построение отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if ( bbiStart.Caption == Resources.Start )
            {
                SetStopButton();
                BeginReport();
                _stopRun = false;
                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();

                foreach ( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                {
                    if ( m_row.Check && m_row.GetdataviewRows().Length > 0 )
                    {
                        SelectItem( m_row );
                        noData = false;

                        if ( _stopRun )
                            break;
                    } // if
                } // foreach

                if ( noData )
                {
                    XtraMessageBox.Show( Resources.WarningNoData, Resources.Notification );
                }

                if ( curMrow == null )
                {
                    return;
                }

                Select( curMrow );
                SelectGraphic( curMrow );

                SetStartButton();
                EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();
                if ( !noData )
                    EnableButton();
                SetStartButton();
            }
        } // bbiStart_ItemClick

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem( atlantaDataSet.mobitelsRow m_row )
        {
            DisableButton();
            Application.DoEvents();
            foreach ( IAlgorithm alg in _algoritms )
            {
                alg.SelectItem( m_row );
                alg.Run();

                if ( _stopRun )
                    break;
            } // foreach

            EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        public override void Select( atlantaDataSet.mobitelsRow mobitel )
        {
            if ( mobitel != null )
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo( mobitel );
                atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", mobitel.Mobitel_ID );

                if ( _summaries.ContainsKey( mobitel.Mobitel_ID ) )
                {
                    UpdateStatusLine();
                    EnableButton();
                    //EnableEditButton();
                }
                else
                {
                    DisableButton();
                    //DisableEditButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        public void SelectGraphic( atlantaDataSet.mobitelsRow mobitel )
        {
            if ( mobitel != null )
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo( mobitel );
                atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", mobitel.Mobitel_ID );

                try
                {
                    CreateGraph( mobitel );
                }
                catch ( Exception ex )
                {
                    string message = String.Format( "Exception in FuelControl: {0}", ex.Message );
                    throw new NotImplementedException( message );
                }
            } // if
        } // SelectGraphic

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph( atlantaDataSet.mobitelsRow mobitel_row )
        {
            int length = mobitel_row.GetdataviewRows().Length;

            if ( length == 0 )
            {
                return;
            }

            ReportsControl.GraphClearSeries();
            buildGraph.AddGraphFuel( Graph, dataset, mobitel_row );
            ReportsControl.ShowGraph( curMrow );
        } // CreateGraph

        protected override void bbiShowOnMap_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            List<Marker> markers = new List<Marker>();
            Int32[] index = fuelDataGridView.GetSelectedRows();
            for ( int i = 0; i < index.Length; i++ )
            {
                atlantaDataSet.FuelReportRow f_row = ( atlantaDataSet.FuelReportRow )
                    ( ( DataRowView )atlantaDataSetBindingSource.List[index[i]]).Row;

                if ( f_row["time_"] != DBNull.Value )
                {
                    double value = f_row.dValue;
                    string message = String.Format( "{0}: {1:N2} {2}.",
                        value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                        value, Resources.LitreAbbreviation );
                    PointLatLng location = new PointLatLng( f_row.dataviewRow.Lat, f_row.dataviewRow.Lon );
                    markers.Add( new Marker( MarkerType.Fueling, f_row.mobitel_id, location, message, "" ) );
                } // if
            } // for

            ReportsControl.OnMarkersShowNeeded( markers );
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick( object sender, ItemClickEventArgs e )
        {
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        }

        private static void ShowGraphToolStripButton_ClickExtracted( atlantaDataSet.FuelReportRow f_row, out string val, out Color color )
        {
            val = String.Format( "{0:#.#}{1}", f_row.dValue, Resources.LitreAbbreviation );
            if ( f_row.dValue > 0 )
            {
                val = String.Format( "{0} {1}", Resources.Fuelling, val );
                color = Color.Green;
            }
            else
            {
                val = String.Format( "{0} {1}", Resources.FuelLarceny, val );
                color = Color.Red;
            }
        }

        void UpdateZGraph()
        {
            // Сбрасываем все метки
            // Graph.ClearGraphZoom(false)
            Graph.ClearLabel();
            Int32[] index = fuelDataGridView.GetSelectedRows();
            for ( int i = 0; i < index.Length; i++ )
            {
                atlantaDataSet.FuelReportRow f_row = ( atlantaDataSet.FuelReportRow )
                    ( ( DataRowView )atlantaDataSetBindingSource.List[index[i]] ).Row;

                if ( f_row["time_"] != DBNull.Value )
                {
                    string val;
                    Color color;
                    ShowGraphToolStripButton_ClickExtracted( f_row, out val, out color );
                    Graph.AddLabel( f_row.time_, f_row.beginValue, f_row.pointValue, val, color );
                } // if
            } // for
            ReportsControl.ShowGraph( curMrow );
            // Graph.ClearGraphZoom(true);
            Graph.SellectZoom();
        } // UpdateZGraph

        private void fuelDataGridView_CellFormatting( object sender, CustomColumnDisplayTextEventArgs e )
        {
            if ( e.Column.FieldName == "dValue" )
            {
                if ( e.Value is DBNull || e.Value == null )
                {
                    return;
                }

                if ( (double)e.Value > 0.0 )
                {
                    e.Column.AppearanceCell.ForeColor = Color.Green;
                }
                else
                {
                    e.Column.AppearanceCell.ForeColor = Color.Red;
                }
            } // if
        } // fuelDataGridView_CellFormatting

        private void fuelDataGridView_CellMouseDoubleClick( object sender, RowCellClickEventArgs e )
        {
            ReportsControl.OnClearMapObjectsNeeded();

            if ( e.RowHandle < 0 )
                return;

            DateTime time_;
            // Дата время
            object tm = fuelDataGridView.GetRowCellValue(e.RowHandle, e.Column);
            if ( !DateTime.TryParse( Convert.ToString( tm ), out time_ ) )
                // fuelDataGridView.CurrentRow.ed
                return;

            List<Marker> markers = new List<Marker>();
            atlantaDataSet.FuelReportRow f_row = ( atlantaDataSet.FuelReportRow )
                ( ( DataRowView )atlantaDataSetBindingSource.List[e.RowHandle] ).Row;
            double value = f_row.dValue;
            string message = String.Format( "{0}: {1:N2} {2}.", value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                value, Resources.LitreAbbreviation );
            PointLatLng location = new PointLatLng( f_row.dataviewRow.Lat, f_row.dataviewRow.Lon );
            markers.Add( new Marker( MarkerType.Fueling, f_row.mobitel_id, location, message, "" ) );
            ReportsControl.OnMarkersShowNeeded( markers );
            UpdateZGraph();
        } // fuelDataGridView_CellMouseDoubleClick

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.FuelReport.Clear();
            dataset.FuelValue.Clear();
            _summaries.Clear();
            ClearStatusLine();
        }

        /// <summary>
        /// Осуществляет валидацию вводимых пользователем данных
        /// </summary>
        private void fuelDataGridView_CellValidating( object sender, BaseContainerValidateEditorEventArgs  e )
        {
            if ( ( fuelDataGridView.FocusedColumn.Name == "_endValue" ) ||
                ( fuelDataGridView.FocusedColumn.Name == "_beginValue" ) )
            {
                if ( e.Value == null ||
                    String.IsNullOrEmpty( e.Value.ToString() ) )
                {
                    XtraMessageBox.Show( Resources.FuelCtrlFuellingValueError, Resources.DataError );
                    e.Valid = false;
                }
                else
                {
                    double new_double;
                    if ( !Double.TryParse( e.Value.ToString(), out new_double ) )
                    {
                        XtraMessageBox.Show( Resources.FuelCtrlFuellingValueError, Resources.DataError );
                        e.Valid = false;
                    }
                } // else
            } // if
            else if ( ( fuelDataGridView.FocusedColumn.Name == "_time" ) )
            {
                if ( e.Value == null ||
                    String.IsNullOrEmpty( e.Value.ToString() ) )
                {
                    XtraMessageBox.Show( Resources.IncorrectDateFormat, Resources.DataError );
                    e.Valid = false;
                }
                else
                {
                    DateTime new_time;
                    if ( !DateTime.TryParse( e.Value.ToString(), out new_time ) )
                    {
                        XtraMessageBox.Show( Resources.IncorrectDateFormat, Resources.DataError );
                        e.Valid = false;
                    }
                } // else
            } // else if
        } // fuelDataGridView_CellValidating

        /// <summary>
        /// Осуществляет пересчет в редактируемой строке отчета и итогов
        /// </summary>
        private void fuelDataGridView_CellEndEdit( object sender, CellValueChangedEventArgs e )
        {
            string name = e.Column.Name; // Название редактируемой колонки

            if ( name == "_endValue" || name == "_beginValue" )
            {
                atlantaDataSet.FuelReportRow row = ( atlantaDataSet.FuelReportRow )
                    ( ( DataRowView )atlantaDataSetBindingSource.List[e.RowHandle] ).Row;
                Fuel.Summary s = _summaries[curMrow.Mobitel_ID];

                if ( row.dValue > 0 )
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.dValue = row.endValue - row.beginValue;

                if ( row.dValue > 0 )
                {
                    s.Fueling += row.dValue;
                    s.Total += row.dValue;
                }
                else
                {
                    s.Discharge += row.dValue;
                }

                s.Rate = s.Total * 100 / curMrow.path;
                _summaries.Remove( curMrow.Mobitel_ID );
                _summaries.Add( curMrow.Mobitel_ID, s );
            } // if

            atlantaDataSetBindingSource.ResumeBinding();
            atlantaDataSetBindingSource.ResetBindings( false );
            atlantaDataSetBindingSource.EndEdit();
            graph.RemoveMouseEventHandler();
            UpdateStatusLine();
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        } // fuelDataGridView_CellEndEdit

        /// <summary>
        /// Добавляет к отчету новую строку
        /// </summary>
        protected void bindingNavigatorAddNewItem_Click()
        {
            ReportsControl.OnGraphShowNeeded();

            f_row = ( atlantaDataSet.FuelReportRow )
                ( ( DataRowView )atlantaDataSetBindingSource.AddNew() ).Row;

            f_row["GenerateType"] = "manual";

            numberChangedRow = fuelDataGridView.FocusedRowHandle;

            Graph.Action += new ActionEventHandler( ZGraphControl_Action );
            graph.AddMouseEventHandler();
            XtraMessageBox.Show( Resources.FuelCtrlAddFuellingStartText, Resources.FuelCtrlAddFuellingCaption );
        } // bindingNavigatorAddNewItem_Click

        /// <summary>
        /// Удаляет из отчета выделенную строку
        /// </summary>
        protected void bindingNavigatorDeleteItem_Click()
        {
            atlantaDataSet.FuelReportRow row = ( atlantaDataSet.FuelReportRow )( ( DataRowView )atlantaDataSetBindingSource.Current ).Row;
            Fuel.Summary s = _summaries[curMrow.Mobitel_ID];

            // Необходимо оставить последнюю строку в отчете, чтобы сводные отчеты
            // не строили данный отчет заново
            if ( curMrow.GetFuelerReportRows().Length == 1 )
            {
                row.beginValue = 0;
                row.endValue = 0;
                row.dValue = 0;
                s.Fueling = 0;
                s.Discharge = 0;
                s.Total = s.Before - s.After;
                s.Rate = s.Total * 100 / curMrow.path;
                _summaries.Remove( curMrow.Mobitel_ID );
                _summaries.Add( curMrow.Mobitel_ID, s );
                UpdateStatusLine();
                XtraMessageBox.Show( Resources.FuelCtrlDeleteError, Resources.Warning );
            }
            else
            {
                if ( row.dValue > 0 )
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                    s.Rate = s.Total * 100 / curMrow.path;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.Delete();
                _summaries.Remove( curMrow.Mobitel_ID );
                _summaries.Add( curMrow.Mobitel_ID, s );
                UpdateStatusLine();
            } // else
        } // bindingNavigatorDeleteItem_Click

        private void connav_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            if ( e.Button.ButtonType == NavigatorButtonType.Append )
            {
                bindingNavigatorAddNewItem_Click();
            }
            else if ( e.Button.ButtonType == NavigatorButtonType.Remove )
            {
                bindingNavigatorDeleteItem_Click();
            }
        }

        /// <summary>
        /// Обрабатывает выбор точек на графике при добавлении новой строки к отчету
        /// </summary>
        private void ZGraphControl_Action( object sender, ActionCancelEventArgs ev )
        {
            System.Diagnostics.Trace.WriteLine( ev.Point.time.ToString() );
            System.Diagnostics.Trace.WriteLine( ev.Point.value.ToString() );

            if ( f_row == null )
                return;

            if ( !begin_end )
            {
                atlantaDataSet.dataviewRow d_row = dataset.dataview.FindByTime( curMrow, ev.Point.time );
                f_row.beginValue = ev.Point.value;
                f_row.time_ = ev.Point.time;
                f_row.DataGPS_ID = d_row.DataGps_ID;
                f_row.Location = Algorithm.FindLocation( new PointLatLng( d_row.Lat, d_row.Lon ) );
                f_row.mobitel_id = d_row.Mobitel_ID;
                f_row.sensor_id = 0;

                foreach ( atlantaDataSet.sensorsRow s_row in curMrow.GetsensorsRows() )
                {
                    if ( s_row != null )
                    {
                        foreach ( atlantaDataSet.relationalgorithmsRow ra_row in s_row.GetrelationalgorithmsRows() )
                        {
                            if ( ( AlgorithmType )ra_row.AlgorithmID == AlgorithmType.FUEL1 )
                            {
                                f_row.sensor_id = s_row.id;
                            } // if
                        } // foreach
                    } // if
                } // foreach
                begin_end = true;
                XtraMessageBox.Show( Resources.FuelCtrlAddFuellingFinishText, Resources.FuelCtrlAddFuellingCaption );
            } // if
            else
            {
                f_row.endValue = ev.Point.value;
                f_row.dValue = f_row.endValue - f_row.beginValue;
                Fuel.Summary s = _summaries[curMrow.Mobitel_ID];
                if ( f_row.dValue > 0 )
                {
                    s.Fueling += f_row.dValue;
                    s.Total += f_row.dValue;
                    s.Rate = s.Total * 100 / curMrow.path;
                } // if
                else
                {
                    s.Discharge += f_row.dValue;
                }
                _summaries.Remove( curMrow.Mobitel_ID );
                _summaries.Add( curMrow.Mobitel_ID, s );

                UpdateStatusLine();
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.ResetBindings( false );
                atlantaDataSetBindingSource.EndEdit();
                graph.RemoveMouseEventHandler();
                Graph.Action -= ZGraphControl_Action;
                begin_end = false;
                ReportsControl.OnGraphShowNeeded();
                UpdateZGraph();
            } // else
        } // ZGraphControl_Action

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;
        }

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine()
        {
            Fuel.Summary s = _summaries[curMrow.Mobitel_ID];

            _beforeLbl.Caption = Resources.BeforeLbl + " " + s.Before.ToString( "N0" );
            _afterLbl.Caption = Resources.AfterLbl + " " + s.After.ToString( "N0" );
            _fuelingLbl.Caption = Resources.FuelLbl + " " + s.Fueling.ToString( "N0" );
            _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s.Discharge).ToString( "N0" );
            _totalLbl.Caption = Resources.TotalLbl + " " + s.Total.ToString( "N0" );
            _rateLbl.Caption = Resources.RateLbl + " " + s.Rate.ToString( "N2" );
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s.AvgSpeed.ToString( "N2" );
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach ( int mobitelId in _summaries.Keys )
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End;   // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID( mobitelId );
                VehicleInfo info = new VehicleInfo( mobitelId );

                tfinfo.infoVehicle = info.Info; // Автомобиль
                tfinfo.infoDriverName = info.DriverFullName; // Водитель
                tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Before; // Литров топлива в начале
                tfinfo.FuelStops = _summaries[curMrow.Mobitel_ID].After; // Литров топлива в конце
                tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Fueling; // Заправлено литров всего
                tfinfo.totalFuelSub = _summaries[curMrow.Mobitel_ID].Discharge; // Слито литров всего
                tfinfo.totalFuelAdd = _summaries[curMrow.Mobitel_ID].Total; // Всего литров израсходовано автомобилем
                tfinfo.MiddleFuel = _summaries[curMrow.Mobitel_ID].Rate; // Расход топлива на 100 км пробега
                tfinfo.MotionTreavelTime = _summaries[curMrow.Mobitel_ID].AvgSpeed; // Средняя скорость движения

                ReportingFuel.AddInfoStructToList( tfinfo );
                ReportingFuel.CreateBindDataList();

                foreach ( atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select( "mobitel_id=" + mobitelId, "id ASC" ) )
                {
                    string location = row.Location;
                    DateTime time = row.time_;
                    double value = row.dValue;                 
                    double begin = row.beginValue;                
                    double end = row.endValue;
                    string genertype = row.GenerateType;

                    ReportingFuel.AddDataToBindList( new reportFuel( location, time, value,
                            begin, end, genertype ) );
                } // foreach 1
                ReportingFuel.CreateElementReport();
            } // foreach 2

            ReportingFuel.CreateAndShowReport();
            ReportingFuel.DeleteData();
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            TInfoDut tfinfo = new TInfoDut();

            tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
            tfinfo.periodEnd = Algorithm.Period.End;   // Конец периода

            VehicleInfo info = new VehicleInfo( curMrow.Mobitel_ID );

            tfinfo.infoVehicle = info.Info; // Автомобиль
            tfinfo.infoDriverName = info.DriverFullName; // Водитель
            tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Before; // Литров топлива в начале
            tfinfo.FuelStops = _summaries[curMrow.Mobitel_ID].After; // Литров топлива в конце
            tfinfo.totalFuel = _summaries[curMrow.Mobitel_ID].Fueling; // Заправлено литров всего
            tfinfo.totalFuelSub = _summaries[curMrow.Mobitel_ID].Discharge; // Слито литров всего
            tfinfo.totalFuelAdd = _summaries[curMrow.Mobitel_ID].Total; // Всего литров израсходовано автомобилем
            tfinfo.MiddleFuel = _summaries[curMrow.Mobitel_ID].Rate; // Расход топлива на 100 км пробега
            tfinfo.MotionTreavelTime = _summaries[curMrow.Mobitel_ID].AvgSpeed; // Средняя скорость движения

            ReportingFuel.AddInfoStructToList( tfinfo ); /* формируем заголовки таблиц отчета */
            ReportingFuel.CreateAndShowReport( fuelDataGridControl );
        } // ExportToExcelDevExpress

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea( object sender, CreateAreaEventArgs e )
        {
            DevExpressReportHeader( Resources.DetailReportTravel, e );
            TInfoDut info = ReportingFuel.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader( strPeriod, 22, e );
        }

        protected double delsign( double s )
        {
            if ( s < 0.0 )
                return -s;

            return s;
        }

        /* функция для формирования верхней/средней части заголовка отчета */
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            ReportingFuel.SetRectangleBrckUP( 450, 10, 200, 50 );
            return ( Resources.BeginFueling + ": " + info.totalFuel.ToString( "N2" ) + "\n" +
                Resources.FuelLbl + " " + info.totalFuel.ToString( "N2" ) + "\n" +
                Resources.DischargeLbl + " " + delsign(info.totalFuelSub).ToString( "N2" ) );
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            return ( Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName );
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            ReportingFuel.SetRectangleBrckRight( 900, 10, 200, 70 );
            return ( Resources.TotalLbl + " " + info.totalFuelAdd.ToString( "N2" ) + "\n" +
                Resources.RateLbl + " " + info.MiddleFuel.ToString( "N2" ) + "\n" +
                Resources.FuelEnd + ", л: " + info.FuelStops.ToString( "N2" ) + "\n" +
                Resources.AvgSpeedLbl + " " + info.MotionTreavelTime.ToString( "N2" ) );
        }

        protected override void barButtonGroupPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            GroupPanel( fuelDataGridView );
        }

        protected override void barButtonFooterPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            FooterPanel( fuelDataGridView );
        }

        protected override void barButtonNavigator_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            NavigatorPanel( fuelDataGridControl );
        }

        protected override void barButtonStatusPanel_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            StatusBar( bar2 );
        }

        public void Localization()
        {
            _location.Caption = Resources.Location;
            _location.ToolTip = Resources.Location;

            _time.Caption = Resources.DateTime;
            _time.ToolTip = Resources.FuellingDateTime;

            _quantity.Caption = Resources.FuellingValue;
            _quantity.ToolTip = Resources.FuellingValueHint;

            _beginValue.Caption = Resources.FuellLevelBeforeFuelling;
            _beginValue.ToolTip = Resources.FuellLevelBeforeFuellingHint;

            _endValue.Caption = Resources.FuellLevelAfterFuelling;
            _endValue.ToolTip = Resources.FuellLevelAfterFuellingHint;

            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;
        }
    } // DevFuelControl
} // BaseReports