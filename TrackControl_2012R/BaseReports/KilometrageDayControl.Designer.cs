namespace BaseReports
{
  partial class KilometrageDayControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KilometrageDayControl));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        this.kilometrageDayDataGridView = new System.Windows.Forms.DataGridView();
        this.dayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.locationStartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.locationEndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.initialTimeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.finalTimeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.intervalWorkDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.intervalMoveDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.distanceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.averageSpeedDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.kilometrageDayReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        this.lblTotalWayText = new System.Windows.Forms.ToolStripStatusLabel();
        this.lblTotalWay = new System.Windows.Forms.ToolStripStatusLabel();
        this.lblTotalTimeTourText = new System.Windows.Forms.ToolStripStatusLabel();
        this.lblTotalTimeTour = new System.Windows.Forms.ToolStripStatusLabel();
        this.lblTotalTimeWayText = new System.Windows.Forms.ToolStripStatusLabel();
        this.lblTotalTimeWay = new System.Windows.Forms.ToolStripStatusLabel();
        this.atlantaDataSet = new LocalCache.atlantaDataSet();
        this.reportPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.kilometrageDayDataGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.kilometrageDayReportBindingSource)).BeginInit();
        this.statusStrip1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
        this.SuspendLayout();
        // 
        // reportPanel
        // 
        this.reportPanel.Controls.Add(this.statusStrip1);
        this.reportPanel.Controls.Add(this.kilometrageDayDataGridView);
        this.reportPanel.Size = new System.Drawing.Size(761, 502);
        // 
        // c1Report
        // 
        this.c1Report.ReportDefinition = resources.GetString("c1Report.ReportDefinition");
        this.c1Report.ReportName = "KilometrageDayReport";
        // 
        // kilometrageDayDataGridView
        // 
        this.kilometrageDayDataGridView.AllowUserToAddRows = false;
        this.kilometrageDayDataGridView.AllowUserToDeleteRows = false;
        dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
        this.kilometrageDayDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.kilometrageDayDataGridView.AutoGenerateColumns = false;
        this.kilometrageDayDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
        this.kilometrageDayDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
        this.kilometrageDayDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
        this.kilometrageDayDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.kilometrageDayDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        this.kilometrageDayDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.kilometrageDayDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dayDataGridViewTextBoxColumn,
            this.locationStartDataGridViewTextBoxColumn,
            this.locationEndDataGridViewTextBoxColumn,
            this.initialTimeDataGridViewTextBoxColumn1,
            this.finalTimeDataGridViewTextBoxColumn1,
            this.intervalWorkDataGridViewTextBoxColumn1,
            this.intervalMoveDataGridViewTextBoxColumn1,
            this.distanceDataGridViewTextBoxColumn1,
            this.averageSpeedDataGridViewTextBoxColumn1});
        this.kilometrageDayDataGridView.DataSource = this.kilometrageDayReportBindingSource;
        dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.kilometrageDayDataGridView.DefaultCellStyle = dataGridViewCellStyle12;
        this.kilometrageDayDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.kilometrageDayDataGridView.Location = new System.Drawing.Point(0, 0);
        this.kilometrageDayDataGridView.Name = "kilometrageDayDataGridView";
        this.kilometrageDayDataGridView.ReadOnly = true;
        dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.kilometrageDayDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
        this.kilometrageDayDataGridView.RowHeadersVisible = false;
        dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        this.kilometrageDayDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle14;
        this.kilometrageDayDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.kilometrageDayDataGridView.Size = new System.Drawing.Size(761, 502);
        this.kilometrageDayDataGridView.TabIndex = 1;
        this.kilometrageDayDataGridView.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_RowEnter);
        this.kilometrageDayDataGridView.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_RowLeave);
        this.kilometrageDayDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grid_CellMouseDoubleClick);
        // 
        // dayDataGridViewTextBoxColumn
        // 
        this.dayDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dayDataGridViewTextBoxColumn.DataPropertyName = "InitialTime";
        dataGridViewCellStyle3.Format = "d";
        dataGridViewCellStyle3.NullValue = null;
        this.dayDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
        this.dayDataGridViewTextBoxColumn.HeaderText = "����";
        this.dayDataGridViewTextBoxColumn.Name = "dayDataGridViewTextBoxColumn";
        this.dayDataGridViewTextBoxColumn.ReadOnly = true;
        this.dayDataGridViewTextBoxColumn.ToolTipText = "����";
        this.dayDataGridViewTextBoxColumn.Width = 70;
        // 
        // locationStartDataGridViewTextBoxColumn
        // 
        this.locationStartDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.locationStartDataGridViewTextBoxColumn.DataPropertyName = "LocationStart";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        this.locationStartDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
        this.locationStartDataGridViewTextBoxColumn.HeaderText = "����� ������ ��������";
        this.locationStartDataGridViewTextBoxColumn.Name = "locationStartDataGridViewTextBoxColumn";
        this.locationStartDataGridViewTextBoxColumn.ReadOnly = true;
        this.locationStartDataGridViewTextBoxColumn.ToolTipText = "����� ������ ��������";
        this.locationStartDataGridViewTextBoxColumn.Width = 220;
        // 
        // locationEndDataGridViewTextBoxColumn
        // 
        this.locationEndDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.locationEndDataGridViewTextBoxColumn.DataPropertyName = "LocationEnd";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        this.locationEndDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
        this.locationEndDataGridViewTextBoxColumn.HeaderText = "����� ��������� ��������";
        this.locationEndDataGridViewTextBoxColumn.Name = "locationEndDataGridViewTextBoxColumn";
        this.locationEndDataGridViewTextBoxColumn.ReadOnly = true;
        this.locationEndDataGridViewTextBoxColumn.ToolTipText = "����� ��������� ��������";
        this.locationEndDataGridViewTextBoxColumn.Width = 220;
        // 
        // initialTimeDataGridViewTextBoxColumn1
        // 
        this.initialTimeDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.initialTimeDataGridViewTextBoxColumn1.DataPropertyName = "InitialTime";
        dataGridViewCellStyle6.Format = "t";
        dataGridViewCellStyle6.NullValue = null;
        this.initialTimeDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
        this.initialTimeDataGridViewTextBoxColumn1.HeaderText = "����� ������ ��������";
        this.initialTimeDataGridViewTextBoxColumn1.Name = "initialTimeDataGridViewTextBoxColumn1";
        this.initialTimeDataGridViewTextBoxColumn1.ReadOnly = true;
        this.initialTimeDataGridViewTextBoxColumn1.ToolTipText = "����� ������ ��������";
        this.initialTimeDataGridViewTextBoxColumn1.Width = 80;
        // 
        // finalTimeDataGridViewTextBoxColumn1
        // 
        this.finalTimeDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.finalTimeDataGridViewTextBoxColumn1.DataPropertyName = "FinalTime";
        dataGridViewCellStyle7.Format = "t";
        dataGridViewCellStyle7.NullValue = null;
        this.finalTimeDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
        this.finalTimeDataGridViewTextBoxColumn1.HeaderText = "����� ��������� ��������";
        this.finalTimeDataGridViewTextBoxColumn1.Name = "finalTimeDataGridViewTextBoxColumn1";
        this.finalTimeDataGridViewTextBoxColumn1.ReadOnly = true;
        this.finalTimeDataGridViewTextBoxColumn1.ToolTipText = "����� ��������� ��������";
        this.finalTimeDataGridViewTextBoxColumn1.Width = 80;
        // 
        // intervalWorkDataGridViewTextBoxColumn1
        // 
        this.intervalWorkDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.intervalWorkDataGridViewTextBoxColumn1.DataPropertyName = "IntervalWork";
        dataGridViewCellStyle8.Format = "t";
        dataGridViewCellStyle8.NullValue = null;
        this.intervalWorkDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
        this.intervalWorkDataGridViewTextBoxColumn1.HeaderText = "��������- ��������� �����, �";
        this.intervalWorkDataGridViewTextBoxColumn1.Name = "intervalWorkDataGridViewTextBoxColumn1";
        this.intervalWorkDataGridViewTextBoxColumn1.ReadOnly = true;
        this.intervalWorkDataGridViewTextBoxColumn1.ToolTipText = "����������������� �����, �";
        this.intervalWorkDataGridViewTextBoxColumn1.Width = 80;
        // 
        // intervalMoveDataGridViewTextBoxColumn1
        // 
        this.intervalMoveDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.intervalMoveDataGridViewTextBoxColumn1.DataPropertyName = "IntervalMove";
        dataGridViewCellStyle9.Format = "t";
        dataGridViewCellStyle9.NullValue = null;
        this.intervalMoveDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
        this.intervalMoveDataGridViewTextBoxColumn1.HeaderText = "����� ����� ��������";
        this.intervalMoveDataGridViewTextBoxColumn1.Name = "intervalMoveDataGridViewTextBoxColumn1";
        this.intervalMoveDataGridViewTextBoxColumn1.ReadOnly = true;
        this.intervalMoveDataGridViewTextBoxColumn1.ToolTipText = "����� �������� � ���������";
        this.intervalMoveDataGridViewTextBoxColumn1.Width = 80;
        // 
        // distanceDataGridViewTextBoxColumn1
        // 
        this.distanceDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.distanceDataGridViewTextBoxColumn1.DataPropertyName = "Distance";
        dataGridViewCellStyle10.Format = "N2";
        dataGridViewCellStyle10.NullValue = null;
        this.distanceDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
        this.distanceDataGridViewTextBoxColumn1.HeaderText = "���������� ����, ��";
        this.distanceDataGridViewTextBoxColumn1.Name = "distanceDataGridViewTextBoxColumn1";
        this.distanceDataGridViewTextBoxColumn1.ReadOnly = true;
        this.distanceDataGridViewTextBoxColumn1.ToolTipText = "���������� ����, ��";
        this.distanceDataGridViewTextBoxColumn1.Width = 80;
        // 
        // averageSpeedDataGridViewTextBoxColumn1
        // 
        this.averageSpeedDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.averageSpeedDataGridViewTextBoxColumn1.DataPropertyName = "AverageSpeed";
        dataGridViewCellStyle11.Format = "N2";
        dataGridViewCellStyle11.NullValue = null;
        this.averageSpeedDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle11;
        this.averageSpeedDataGridViewTextBoxColumn1.HeaderText = "������� ��������, ��/�";
        this.averageSpeedDataGridViewTextBoxColumn1.Name = "averageSpeedDataGridViewTextBoxColumn1";
        this.averageSpeedDataGridViewTextBoxColumn1.ReadOnly = true;
        this.averageSpeedDataGridViewTextBoxColumn1.ToolTipText = "������� ��������, ��/�";
        this.averageSpeedDataGridViewTextBoxColumn1.Width = 80;
        // 
        // kilometrageDayReportBindingSource
        // 
        this.kilometrageDayReportBindingSource.DataMember = "KilometrageReportDay";
        this.kilometrageDayReportBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
        // 
        // statusStrip1
        // 
        this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTotalWayText,
            this.lblTotalWay,
            this.lblTotalTimeTourText,
            this.lblTotalTimeTour,
            this.lblTotalTimeWayText,
            this.lblTotalTimeWay});
        this.statusStrip1.Location = new System.Drawing.Point(0, 480);
        this.statusStrip1.Name = "statusStrip1";
        this.statusStrip1.ShowItemToolTips = true;
        this.statusStrip1.Size = new System.Drawing.Size(761, 22);
        this.statusStrip1.TabIndex = 2;
        this.statusStrip1.Text = "statusStrip1";
        // 
        // lblTotalWayText
        // 
        this.lblTotalWayText.Name = "lblTotalWayText";
        this.lblTotalWayText.Size = new System.Drawing.Size(121, 17);
        this.lblTotalWayText.Text = "���������� ����, ��:";
        this.lblTotalWayText.ToolTipText = "���������� ����";
        // 
        // lblTotalWay
        // 
        this.lblTotalWay.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.lblTotalWay.Name = "lblTotalWay";
        this.lblTotalWay.Size = new System.Drawing.Size(23, 17);
        this.lblTotalWay.Text = "---";
        this.lblTotalWay.ToolTipText = "���������� ����";
        // 
        // lblTotalTimeTourText
        // 
        this.lblTotalTimeTourText.Name = "lblTotalTimeTourText";
        this.lblTotalTimeTourText.Size = new System.Drawing.Size(187, 17);
        this.lblTotalTimeTourText.Text = "����� ����������������� �����:";
        this.lblTotalTimeTourText.ToolTipText = "����� ����������������� �����";
        // 
        // lblTotalTimeTour
        // 
        this.lblTotalTimeTour.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.lblTotalTimeTour.Name = "lblTotalTimeTour";
        this.lblTotalTimeTour.Size = new System.Drawing.Size(43, 17);
        this.lblTotalTimeTour.Text = "--:--:--";
        this.lblTotalTimeTour.ToolTipText = "����� ����������������� �����";
        // 
        // lblTotalTimeWayText
        // 
        this.lblTotalTimeWayText.Name = "lblTotalTimeWayText";
        this.lblTotalTimeWayText.Size = new System.Drawing.Size(77, 17);
        this.lblTotalTimeWayText.Text = "����� � ����:";
        this.lblTotalTimeWayText.ToolTipText = "����� �������� � ���������";
        // 
        // lblTotalTimeWay
        // 
        this.lblTotalTimeWay.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
        this.lblTotalTimeWay.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.lblTotalTimeWay.Name = "lblTotalTimeWay";
        this.lblTotalTimeWay.Size = new System.Drawing.Size(43, 17);
        this.lblTotalTimeWay.Text = "--:--:--";
        this.lblTotalTimeWay.ToolTipText = "����� �������� � ���������";
        // 
        // atlantaDataSet
        // 
        this.atlantaDataSet.DataSetName = "atlantaDataSet";
        this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // KilometrageDayControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Name = "KilometrageDayControl";
        this.Size = new System.Drawing.Size(763, 533);
        this.Controls.SetChildIndex(this.reportPanel, 0);
        this.reportPanel.ResumeLayout(false);
        this.reportPanel.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.c1Report)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.kilometrageDayDataGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.kilometrageDayReportBindingSource)).EndInit();
        this.statusStrip1.ResumeLayout(false);
        this.statusStrip1.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private LocalCache.atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.DataGridView kilometrageDayDataGridView;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalWay;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalTimeTour;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalTimeWay;
    private System.Windows.Forms.BindingSource kilometrageDayReportBindingSource;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalWayText;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalTimeTourText;
    private System.Windows.Forms.ToolStripStatusLabel lblTotalTimeWayText;
    private System.Windows.Forms.DataGridViewTextBoxColumn dayDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn locationStartDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn locationEndDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn initialTimeDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn finalTimeDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn intervalWorkDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn intervalMoveDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn distanceDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn averageSpeedDataGridViewTextBoxColumn1;
  }
}
