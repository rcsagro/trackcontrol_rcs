﻿namespace BaseReports
{
    partial class SensorEventsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorEventsControl));
            this.sensorControlEventsGrid = new DevExpress.XtraGrid.GridControl();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gViewSensorEvents = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this._sensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this._ischeckzone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbZone = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._description = new DevExpress.XtraGrid.Columns.GridColumn();
            this._location = new DevExpress.XtraGrid.Columns.GridColumn();
            this._time = new DevExpress.XtraGrid.Columns.GridColumn();
            this._duration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.DurationMotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DurationStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this._distance = new DevExpress.XtraGrid.Columns.GridColumn();
            this._speed = new DevExpress.XtraGrid.Columns.GridColumn();
            this._speedAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.icZone = new DevExpress.Utils.ImageCollection(this.components);
            this.eventsTotalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.sensorControlEventsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewSensorEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsTotalBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // sensorControlEventsGrid
            // 
            this.sensorControlEventsGrid.DataSource = this.atlantaDataSetBindingSource;
            this.sensorControlEventsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorControlEventsGrid.Location = new System.Drawing.Point(0, 24);
            this.sensorControlEventsGrid.MainView = this.gViewSensorEvents;
            this.sensorControlEventsGrid.Name = "sensorControlEventsGrid";
            this.sensorControlEventsGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.icbZone,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEdit1});
            this.sensorControlEventsGrid.Size = new System.Drawing.Size(988, 613);
            this.sensorControlEventsGrid.TabIndex = 4;
            this.sensorControlEventsGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gViewSensorEvents});
            // 
            // gViewSensorEvents
            // 
            this.gViewSensorEvents.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gViewSensorEvents.Appearance.EvenRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.gViewSensorEvents.Appearance.EvenRow.Options.UseBackColor = true;
            this.gViewSensorEvents.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gViewSensorEvents.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewSensorEvents.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewSensorEvents.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gViewSensorEvents.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.gViewSensorEvents.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.gViewSensorEvents.Appearance.OddRow.Options.UseBackColor = true;
            this.gViewSensorEvents.AppearancePrint.EvenRow.Options.UseTextOptions = true;
            this.gViewSensorEvents.AppearancePrint.EvenRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.EvenRow.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.EvenRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gViewSensorEvents.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gViewSensorEvents.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gViewSensorEvents.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gViewSensorEvents.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewSensorEvents.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gViewSensorEvents.ColumnPanelRowHeight = 40;
            this.gViewSensorEvents.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this._sensor,
            this._ischeckzone,
            this._description,
            this._location,
            this._time,
            this._duration,
            this.DurationMotion,
            this.DurationStop,
            this._distance,
            this._speed,
            this._speedAvg});
            this.gViewSensorEvents.GridControl = this.sensorControlEventsGrid;
            this.gViewSensorEvents.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Duration", null, "Длительность:{0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Duration", null, "Событий:{0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", null, "Пробег:{0:N2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "_durationMotion", this.DurationMotion, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "_durationStop", null, "")});
            this.gViewSensorEvents.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gViewSensorEvents.Name = "gViewSensorEvents";
            this.gViewSensorEvents.OptionsDetail.AllowZoomDetail = false;
            this.gViewSensorEvents.OptionsDetail.EnableMasterViewMode = false;
            this.gViewSensorEvents.OptionsDetail.ShowDetailTabs = false;
            this.gViewSensorEvents.OptionsDetail.SmartDetailExpand = false;
            this.gViewSensorEvents.OptionsView.ColumnAutoWidth = false;
            this.gViewSensorEvents.OptionsView.EnableAppearanceEvenRow = true;
            this.gViewSensorEvents.OptionsView.EnableAppearanceOddRow = true;
            this.gViewSensorEvents.OptionsView.ShowFooter = true;
            // 
            // Id
            // 
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.ReadOnly = true;
            // 
            // _sensor
            // 
            this._sensor.AppearanceCell.Options.UseTextOptions = true;
            this._sensor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._sensor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._sensor.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._sensor.AppearanceHeader.Options.UseTextOptions = true;
            this._sensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._sensor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._sensor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._sensor.Caption = "Датчик";
            this._sensor.FieldName = "SensorName";
            this._sensor.Name = "_sensor";
            this._sensor.OptionsColumn.AllowEdit = false;
            this._sensor.OptionsColumn.ReadOnly = true;
            this._sensor.ToolTip = "Датчик";
            this._sensor.Visible = true;
            this._sensor.VisibleIndex = 0;
            this._sensor.Width = 187;
            // 
            // _ischeckzone
            // 
            this._ischeckzone.AppearanceCell.Options.UseTextOptions = true;
            this._ischeckzone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._ischeckzone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._ischeckzone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._ischeckzone.AppearanceHeader.Options.UseImage = true;
            this._ischeckzone.AppearanceHeader.Options.UseTextOptions = true;
            this._ischeckzone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._ischeckzone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._ischeckzone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._ischeckzone.Caption = " ";
            this._ischeckzone.ColumnEdit = this.icbZone;
            this._ischeckzone.FieldName = "IsCheckZone";
            this._ischeckzone.ImageIndex = 0;
            this._ischeckzone.Name = "_ischeckzone";
            this._ischeckzone.OptionsColumn.AllowEdit = false;
            this._ischeckzone.OptionsColumn.ReadOnly = true;
            this._ischeckzone.Visible = true;
            this._ischeckzone.VisibleIndex = 1;
            this._ischeckzone.Width = 45;
            // 
            // icbZone
            // 
            this.icbZone.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.icbZone.AutoHeight = false;
            this.icbZone.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbZone.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F12);
            this.icbZone.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Контрольная зона", true, 0)});
            this.icbZone.Name = "icbZone";
            // 
            // _description
            // 
            this._description.AppearanceCell.Options.UseTextOptions = true;
            this._description.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._description.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._description.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._description.AppearanceHeader.Options.UseTextOptions = true;
            this._description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._description.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._description.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._description.Caption = "Описание события";
            this._description.FieldName = "Description";
            this._description.Name = "_description";
            this._description.OptionsColumn.AllowEdit = false;
            this._description.OptionsColumn.ReadOnly = true;
            this._description.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Duration", "")});
            this._description.ToolTip = "Описание события";
            this._description.Visible = true;
            this._description.VisibleIndex = 2;
            this._description.Width = 172;
            // 
            // _location
            // 
            this._location.AppearanceCell.Options.UseTextOptions = true;
            this._location.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._location.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.AppearanceHeader.Options.UseTextOptions = true;
            this._location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.Caption = "Местоположение";
            this._location.FieldName = "Location";
            this._location.Name = "_location";
            this._location.OptionsColumn.AllowEdit = false;
            this._location.OptionsColumn.ReadOnly = true;
            this._location.ToolTip = "Местоположение";
            this._location.Visible = true;
            this._location.VisibleIndex = 3;
            this._location.Width = 294;
            // 
            // _time
            // 
            this._time.AppearanceCell.Options.UseTextOptions = true;
            this._time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._time.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._time.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._time.AppearanceHeader.Options.UseTextOptions = true;
            this._time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._time.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._time.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._time.Caption = "Время события";
            this._time.DisplayFormat.FormatString = "dd/MM/yy HH:mm:ss";
            this._time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._time.FieldName = "EventTime";
            this._time.Name = "_time";
            this._time.OptionsColumn.AllowEdit = false;
            this._time.OptionsColumn.ReadOnly = true;
            this._time.ToolTip = "Время события";
            this._time.Visible = true;
            this._time.VisibleIndex = 4;
            this._time.Width = 122;
            // 
            // _duration
            // 
            this._duration.AppearanceCell.Options.UseTextOptions = true;
            this._duration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._duration.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._duration.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._duration.AppearanceHeader.Options.UseTextOptions = true;
            this._duration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._duration.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._duration.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._duration.Caption = "Длительность общая";
            this._duration.ColumnEdit = this.repositoryItemTextEdit1;
            this._duration.DisplayFormat.FormatString = "HH:mm:ss";
            this._duration.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._duration.FieldName = "Duration";
            this._duration.Name = "_duration";
            this._duration.OptionsColumn.AllowEdit = false;
            this._duration.OptionsColumn.AllowFocus = false;
            this._duration.OptionsColumn.ReadOnly = true;
            this._duration.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this._duration.ToolTip = "Длительность общая";
            this._duration.Visible = true;
            this._duration.VisibleIndex = 5;
            this._duration.Width = 103;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // DurationMotion
            // 
            this.DurationMotion.AppearanceCell.Options.UseTextOptions = true;
            this.DurationMotion.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DurationMotion.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DurationMotion.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DurationMotion.AppearanceHeader.Options.UseTextOptions = true;
            this.DurationMotion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DurationMotion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DurationMotion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DurationMotion.Caption = "Длительность в движении";
            this.DurationMotion.DisplayFormat.FormatString = "HH:mm:ss";
            this.DurationMotion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DurationMotion.FieldName = "_durationMotion";
            this.DurationMotion.Name = "DurationMotion";
            this.DurationMotion.OptionsColumn.AllowEdit = false;
            this.DurationMotion.OptionsColumn.AllowFocus = false;
            this.DurationMotion.OptionsColumn.ReadOnly = true;
            this.DurationMotion.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.DurationMotion.ToolTip = "Длительность в движении";
            this.DurationMotion.Visible = true;
            this.DurationMotion.VisibleIndex = 6;
            this.DurationMotion.Width = 90;
            // 
            // DurationStop
            // 
            this.DurationStop.AppearanceCell.Options.UseTextOptions = true;
            this.DurationStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DurationStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DurationStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DurationStop.AppearanceHeader.Options.UseTextOptions = true;
            this.DurationStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DurationStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DurationStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DurationStop.Caption = "Длительность на стоянках";
            this.DurationStop.DisplayFormat.FormatString = "HH:mm:ss";
            this.DurationStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DurationStop.FieldName = "_durationStop";
            this.DurationStop.Name = "DurationStop";
            this.DurationStop.OptionsColumn.AllowEdit = false;
            this.DurationStop.OptionsColumn.AllowFocus = false;
            this.DurationStop.OptionsColumn.ReadOnly = true;
            this.DurationStop.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.DurationStop.ToolTip = "Длительность на стоянках";
            this.DurationStop.Visible = true;
            this.DurationStop.VisibleIndex = 7;
            this.DurationStop.Width = 90;
            // 
            // _distance
            // 
            this._distance.AppearanceCell.Options.UseTextOptions = true;
            this._distance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._distance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._distance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._distance.AppearanceHeader.Options.UseTextOptions = true;
            this._distance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._distance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._distance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._distance.Caption = "Пробег";
            this._distance.DisplayFormat.FormatString = "N2";
            this._distance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._distance.FieldName = "Distance";
            this._distance.Name = "_distance";
            this._distance.OptionsColumn.AllowEdit = false;
            this._distance.OptionsColumn.ReadOnly = true;
            this._distance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:N2}")});
            this._distance.ToolTip = "Пробег";
            this._distance.Visible = true;
            this._distance.VisibleIndex = 8;
            this._distance.Width = 85;
            // 
            // _speed
            // 
            this._speed.AppearanceCell.Options.UseTextOptions = true;
            this._speed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._speed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._speed.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._speed.AppearanceHeader.Options.UseTextOptions = true;
            this._speed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._speed.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._speed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._speed.Caption = "Максимальная скорость";
            this._speed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._speed.FieldName = "Speed";
            this._speed.Name = "_speed";
            this._speed.OptionsColumn.AllowEdit = false;
            this._speed.OptionsColumn.ReadOnly = true;
            this._speed.ToolTip = "Максимальная скорость км/ч";
            this._speed.Visible = true;
            this._speed.VisibleIndex = 9;
            this._speed.Width = 109;
            // 
            // _speedAvg
            // 
            this._speedAvg.AppearanceCell.Options.UseTextOptions = true;
            this._speedAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._speedAvg.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._speedAvg.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._speedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this._speedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._speedAvg.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._speedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._speedAvg.Caption = "Средняя скорость км/ч";
            this._speedAvg.DisplayFormat.FormatString = "N3";
            this._speedAvg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._speedAvg.FieldName = "SpeedAvg";
            this._speedAvg.Name = "_speedAvg";
            this._speedAvg.OptionsColumn.AllowEdit = false;
            this._speedAvg.OptionsColumn.ReadOnly = true;
            this._speedAvg.ToolTip = "Ср. скорость км/ч";
            this._speedAvg.Visible = true;
            this._speedAvg.VisibleIndex = 10;
            this._speedAvg.Width = 133;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink1});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // icZone
            // 
            this.icZone.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icZone.ImageStream")));
            this.icZone.Images.SetKeyName(0, "CZ_Y.png");
            // 
            // eventsTotalBindingSource
            // 
            this.eventsTotalBindingSource.DataSource = this.atlantaDataSetBindingSource;
            // 
            // SensorEventsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.sensorControlEventsGrid);
            this.Name = "SensorEventsControl";
            this.Size = new System.Drawing.Size(988, 637);
            this.Controls.SetChildIndex(this.sensorControlEventsGrid, 0);
            ((System.ComponentModel.ISupportInitialize)(this.sensorControlEventsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewSensorEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsTotalBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl sensorControlEventsGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gViewSensorEvents;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn _sensor;
        private DevExpress.XtraGrid.Columns.GridColumn _ischeckzone;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbZone;
        private DevExpress.XtraGrid.Columns.GridColumn _location;
        private DevExpress.XtraGrid.Columns.GridColumn _time;
        private DevExpress.XtraGrid.Columns.GridColumn _duration;
        private DevExpress.XtraGrid.Columns.GridColumn _description;
        private DevExpress.XtraGrid.Columns.GridColumn _speed;
        private DevExpress.XtraGrid.Columns.GridColumn _distance;
        private DevExpress.XtraGrid.Columns.GridColumn _speedAvg;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.Utils.ImageCollection icZone;
        private System.Windows.Forms.BindingSource eventsTotalBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn DurationMotion;
        private DevExpress.XtraGrid.Columns.GridColumn DurationStop;
    }
}
