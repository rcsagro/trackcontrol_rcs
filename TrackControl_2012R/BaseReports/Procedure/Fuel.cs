using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    public class ValuePair
    {
        public double value;
        public double averageValue;

        public ValuePair()
        {
        }

        public ValuePair(double val, double avgVal)
        {
            value = val;
            averageValue = avgVal;
        }

        public static double[] GetValue(ValuePair[] vPair)
        {
            double[] data = new double[vPair.Length];
            for (int i = 0; i < data.Length; i++)
                data[i] = vPair[i].value;
            return data;
        }

        public static double[] GetAvgValue(ValuePair[] vPair)
        {
            double[] data = new double[vPair.Length];
            for (int i = 0; i < data.Length; i++)
                data[i] = vPair[i].averageValue;
            return data;
        }
    }
    
    public class FuelDictionarys
    {
        private IDictionary<System.Int64, ValuePair> valueFuelSum;
        public List<IDictionary<long, ValuePair>> valueFuelArrayDict = new List<IDictionary<long, ValuePair>>();
        public List<atlantaDataSet.sensorsRow> fuelSensors = new List<atlantaDataSet.sensorsRow>();

        public bool FuelSensorExist(int iNumber)
        {
            return fuelSensors[iNumber] != null;
        }

        //private IDictionary<Int64, ValuePair> valueFuelSensor1;

        public IDictionary<Int64, ValuePair> GetValueFuelSensor(int iCount)
        {
            if( valueFuelArrayDict.Count > 0)
                return valueFuelArrayDict[iCount];

            return null;
        }

        public void SetValueFuelSensor(int iCount, IDictionary<Int64, ValuePair> value)
        {
            valueFuelArrayDict[iCount] = value;
        }

        //private IDictionary<System.Int64, ValuePair> valueFuelSensor2;

        //public IDictionary<System.Int64, ValuePair> ValueFuelSensor2
        //{
        //    get { return valueFuelSensor2; }
        //    set { valueFuelSensor2 = value; }
        //}

        public IDictionary<System.Int64, ValuePair> ValueFuelSum
        {
            get { return valueFuelSum; }
            set { valueFuelSum = value; }
        }

        public FuelDictionarys()
        {
            for (int i = 0; i < valueFuelArrayDict.Count; i++)
            {
                valueFuelArrayDict.Add(new Dictionary<Int64, ValuePair>());
            }

            valueFuelSum = new Dictionary<Int64, ValuePair>();
        }

        public void Clear()
        {
            if( valueFuelArrayDict != null )
            {
                for( int i = 0; i < valueFuelArrayDict.Count; i++ )
                {
                    if( valueFuelArrayDict[i] != null )
                        valueFuelArrayDict[i].Clear();
                }

                valueFuelArrayDict.Clear();
            }

            if( fuelSensors != null )
                if( fuelSensors.Count > 0 )
                    fuelSensors.Clear();

            FreeSensorReferences();
            valueFuelSum.Clear();
        }

        /// <summary>
        /// ������������ ������ �� �������� ��������.
        /// </summary>
        public void FreeSensorReferences()
        {
            for (int i = 0; i < valueFuelArrayDict.Count; i++)
            {
                valueFuelArrayDict[i] = null;
            }
        }

        public void ValueFuelSensorsClear()
        {
            for( int i = 0; i < valueFuelArrayDict.Count; i++ )
            {
                valueFuelArrayDict[i].Clear();
            }
        }

        public void GenerFuelDictonary(int count)
        {
            for( int i = 0; i < count; i++ )
            {
                valueFuelArrayDict.Add(new Dictionary<long, ValuePair>());
            }
        }
    }

    public class Fuel : PartialAlgorithms, IAlgorithm
    {
        #region Attribute

        /// <summary>
        /// ������ ����������
        /// </summary>
        private int width;

        private atlantaDataSet.KilometrageReportRow[] kilometrage_rows;

        public atlantaDataSet.KilometrageReportRow[] Kilometrage_rows
        {
            get { return kilometrage_rows; }
            set { kilometrage_rows = value; }
        }

        private AlgorithmType algType;

        public FuelDictionarys fuelDictionary;

        /// <summary>
        /// ����� ��������
        /// </summary>
        private double fuelingEdge;

        /// <summary>
        /// ����� �����
        /// </summary>
        private double fuelingDischarge;

        /// <summary>
        /// ����� ������� ������� � ���, ��� ������������� ��������
        /// </summary>
        private double flowmeterNorm;

        private int useMinMaxAlgoritm;

        /// <summary>
        /// ����� ����� ��������� ����������/�������, ������ �������� ��������/����� ���� ������������ ��� ������ ��������� 2
        /// </summary>
        private TimeSpan timeForCombine = new TimeSpan(0, 2, 0);

        /// <summary>
        /// �������� ������ ������
        /// </summary>
        private Summary _summarise;

        public Summary summary
        {
            get
            {
                return _summarise;
            }
        }

        private const int MIN_MAX_ALL_TIME_IS_STOP = 5;//���� ������ ���� ��������� 
        private const int MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT = 3;//����� ��� MIN_MAX_AGGREGATE_ON_STOP, �������� ��� MIN_MAX_ONE
        private const int MIN_MAX_AGGREGATE_ON_STOP = 2;
        private const int MIN_MAX_ONE = 1;
        private const int MIN_MAX_DISABLE = 0;

        #endregion 

        public Fuel()
            : base()
        {
            // to do there
        }

        public Fuel(object param)
            : base()
        {
            if (param is AlgorithmType)
            {
                algType = (AlgorithmType) param;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }

            width = 1;
            fuelDictionary = new FuelDictionarys();
            AtlantaDataSet.FuelReport.Clear();
        }

        #region Metods

        /// <summary>
        /// ���������� �������� ��������
        /// � �������� ���������
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lower"></param>
        /// <param name="upper"></param>
        /// <param name="ind"></param>
        /// <returns></returns>
        protected double average(double[] data, int lower, int upper, int ind)
        {
            // ����������� ��������� y���������
            // ������� ��������
            if (data.Length == 0)
            {
                return 0;
            }
            int begin, end;
            if (ind - width < lower)
            {
                begin = lower;
            }
            else
            {
                begin = ind - width;
            }
            // ������ ��������
            if (ind + width > upper)
            {
                end = upper;
            }
            else
            {
                end = ind + width;
            }

            if (begin == end)
            {
                return data[begin];
            }
            if (end > data.Length)
            {
                end = data.Length;
            }
            double avr = 0;
            double min = data[Min(data, begin, end)];
            double max = data[Max(data, begin, end)];
            for (int i = begin; i < end; i++)
            {
                avr += data[i];
            }

            if (end - begin - 2 > 0)
            {
                avr -= min + max;
                return avr/(end - begin - 2);
            }
            else
            {
                return avr/(end - begin);
            }
        }

        /// <summary>
        /// ����� ������������ ��������
        /// </summary>
        /// <param name="data"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected int Min(double[] data, int begin, int end)
        {
            int ind = begin;
            if (data.Length > 0)
            {
                double min = data[begin];
                for (int i = begin + 1; i < end; i++)
                {
                    if (min > data[i])
                    {
                        min = data[i];
                        ind = i;
                    }
                }
            }
            return ind;
        }

        /// <summary>
        /// ����� ������������� ��������
        /// </summary>
        /// <param name="data"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected System.Int64 Max(double[] data, System.Int64 begin, System.Int64 end)
        {
            System.Int64 ind = begin;
            if (data.Length > 0)
            {
                double max = data[begin];
                for (System.Int64 i = begin; i < end; i++)
                {
                    if (max < data[i])
                    {
                        max = data[i];
                        ind = i;
                    }
                }
            }
            return ind;
        }

        /// <summary>
        /// ���������� ���������
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private System.Int64 disp(System.Int64 begin, System.Int64 end)
        {
            if (begin < 0)
            {
                begin = 0;
            }
            System.Int64 size = end - begin;
            if (size < 0) return 0;
            double[] dis = new double[size];
            ValuePair[] fuelValue = new ValuePair[fuelDictionary.ValueFuelSum.Count];
            fuelDictionary.ValueFuelSum.Values.CopyTo(fuelValue, 0);
            for (int i = 1; i < size; i++)
            {
                double d = fuelValue[i + begin].averageValue - fuelValue[i + begin - 1].averageValue;
                dis[i] = (d*d)/(size - 1);
            }
            return begin + Max(dis, 0, size);
        }

        /// <summary>
        /// ����������� ����� ������� ���������� ���������
        /// ����������� �������� 1-����� ������ ��� ��������� ������,
        /// 2-����� ������������ ������� � ������� ����� �������� ��������
        /// </summary>
        /// <param name="points"></param>
        /// <param name="need"></param>
        /// <param name="go_top"></param>
        /// <returns></returns>
        private double GetValueMetodomLine(double[] points, int need, ref bool go_top)
        {
            go_top = false;
            double ret_value = 0; //x-����� �������� ������; �-�������� ������ �������
            double Zx = 0, Zxx = 0, Zxy = 0, Zy = 0;
            for (int i = 0; i < points.Length; i++)
            {
                Zx += i;
                Zxx += (i*i);
                Zy += points[i];
                Zxy += (i*points[i]);
            }
            double coefA = ((Zy/Zx - points.Length*Zxy/(Zx*Zx))/(1 - points.Length*Zxx/(Zx*Zx)));
            double coefB = (Zxy - coefA*Zxx)/Zx;
            ret_value = coefA*need + coefB;
            //find delta to line
            double[] delta_line = new double[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                delta_line[i] = Math.Abs((i*coefA + coefB) - points[i]);
            }
            //������� � ������� ����� �������� ������������� �� ������, ���� � ��� ������ 2-� �����.
            if (delta_line.Length > 2)
            {
                System.Int64 max = Max(delta_line, 0, delta_line.Length);
                //correctind line
                Zx -= max;
                Zxx -= (max*max);
                Zy -= points[max];
                Zxy -= (max*points[max]);

                coefA = ((Zy/Zx - (points.Length - 1)*Zxy/(Zx*Zx))/(1 - (points.Length - 1)*Zxx/(Zx*Zx)));
                coefB = (Zxy - coefA*Zxx)/Zx;
            }
            if (coefA > 0)
            {
                go_top = true;
            }
            if (!double.IsInfinity(coefA) && !double.IsNaN(coefA))
            {
                ret_value = coefA*need + coefB;
            }
            return ret_value;
        }

        #endregion

        #region IAlgorithm Members

        void IAlgorithm.SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
            this.width = 1;
            if (set_row != null)
            {
                this.width = set_row.band;
            }
        }

        public void ClearAtlantaFuelReport()
        {
            if (AtlantaDataSet.FuelReport.Count > 0)
                AtlantaDataSet.FuelReport.Clear();
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        public override void Run()
        {
            try
            {
                if (m_row == null)
                {
                    return;
                }

                if (m_row.GetFuelReportRows() != null)
                {
                    if (GpsDatas.Length == 0)
                    {
                        return;
                    }
                }

                if (m_row.GetKilometrageReportRows() == null || m_row.GetKilometrageReportRows().Length == 0)
                {
                    return;
                }

                double summFuel = 0;
                double diffFuel = 0;

                int iSummFuelCount = 0;
                int iDiffFuelCount = 0;

                GettingValuesDUT(fuelDictionary);

                //����� ������ ������� �������� 
                fuelDictionary.ValueFuelSensorsClear();

                if (fuelDictionary.ValueFuelSum.Count == 0) //���� ������ �� ������� - ������ ������
                    return;

                _summarise = new Summary();

                for (int i = 0; i < GpsDatas.Length; i++)
                {
                    if (fuelDictionary.ValueFuelSum.ContainsKey(GpsDatas[i].Id))
                    {
                        _summarise.Before = fuelDictionary.ValueFuelSum[GpsDatas[i].Id].averageValue;  //������� ������� � ������
                        break;
                    }
                }

                int lenArray = GpsDatas.Length;
                for (int k = 1; k < GpsDatas.Length; k++)
                {
                    if (fuelDictionary.ValueFuelSum.ContainsKey(GpsDatas[lenArray - k].Id))
                    {
                        _summarise.After = fuelDictionary.ValueFuelSum[GpsDatas[lenArray - k].Id].averageValue; // �������� ������� �������
                        break;
                    }
                }

                // ������� � �����
                #region GetSettings

                fuelingEdge = 10;
                fuelingDischarge = 10;
                flowmeterNorm = 33;
                int UseDischargeInFuelrate = 0;
                useMinMaxAlgoritm = 0;
                bool isMinMax3 = false;
                if (set_row != null) // ���� ���� ������ � ���������� �� ����� ������ ������
                {
                    fuelingEdge = set_row.FuelingEdge;
                    fuelingDischarge = set_row.FuelingDischarge;
                    flowmeterNorm = set_row.AvgFuelRatePerHour;
                    UseDischargeInFuelrate = set_row.FuelrateWithDischarge > 0 ? 1 : 0;
                    useMinMaxAlgoritm = set_row.FuelingMinMaxAlgorithm;
                    if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT) isMinMax3 = true;
                    timeForCombine = set_row.FuelerMaxTimeStop;
                }

                #endregion

                int maximum = m_row.GetKilometrageReportRows().Length * 2;
                BeforeReportFilling(Resources.Fuel, maximum);

                atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows;
                if (useMinMaxAlgoritm == MIN_MAX_ALL_TIME_IS_STOP)
                {
                    useMinMaxAlgoritm = MIN_MAX_AGGREGATE_ON_STOP;
                    tmpKilimetrage_rows = GetOneTotalKilometrageReportRow(m_row.GetKilometrageReportRows());
                }
                else
                {
                    atlantaDataSet.KilometrageReportRow[] tmpKmRows =
                        CombineAdjacentStopsOrMoves(m_row.GetKilometrageReportRows());
                    tmpKilimetrage_rows = CorrectingKilometrage(tmpKmRows);
                }


                byte j = 0;
                //AtlantaDataSet.FuelReport.Clear();
                for (int s = 0; s < tmpKilimetrage_rows.Length; s++)
                {
                    atlantaDataSet.KilometrageReportRow k_row = tmpKilimetrage_rows[s];
                    double beginValue = 0;
                    double endValue = 0;
                    System.Int64 beginIndex = -1;
                    System.Int64 endIndex = -1;
                    bool newSearch = true;
                    // ������ �������� �������� ����� ��� MIN_MAX_AGGREGATE_ON_STOP, �������� ��� MIN_MAX_ONE
                    if (isMinMax3)
                    {
                        useMinMaxAlgoritm = MIN_MAX_DISABLE;
                        CountBeginAndEndFuelValue(s, tmpKilimetrage_rows,
                            ref beginValue, ref endValue, ref beginIndex, ref endIndex, newSearch);
                        if (endValue > beginValue)
                        {
                            useMinMaxAlgoritm = MIN_MAX_ONE;
                        }
                        else if (endValue < beginValue)
                        {
                            useMinMaxAlgoritm = MIN_MAX_AGGREGATE_ON_STOP;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    beginValue = 0;
                    endValue = 0;
                    beginIndex = -1;
                    endIndex = -1;
                    newSearch = true;
                    //--------------------------------------------------------------------------------------------------
                    while (CountBeginAndEndFuelValue(s, tmpKilimetrage_rows,
                        ref beginValue, ref endValue, ref beginIndex, ref endIndex, newSearch))
                    {
                        newSearch = false;

                        // ��������� �� ������ �� ��������/���� 
                        // ���� ���� ���������� �� ��������/����
                        if (((endValue - beginValue > fuelingEdge))
                            || beginValue - endValue > fuelingDischarge)
                        {
                            if (TestTrueFueling(beginValue, endValue, tmpKilimetrage_rows, s))
                            {
                                if (TestTrueDischarge(tmpKilimetrage_rows[s], beginValue, endValue))
                                {
                                    atlantaDataSet.FuelReportRow f_row = AtlantaDataSet.FuelReport.NewFuelReportRow();
                                    f_row.mobitel_id = this.m_row.Mobitel_ID;
                                    f_row.sensor_id = base.s_row.id;
                                    f_row.Location = k_row.Location;
                                    f_row.beginValue = beginValue;
                                    f_row.endValue = endValue;
                                    f_row.dValue = endValue - beginValue;
                                    f_row.beginTime = GpsDatas[beginIndex].Time;
                                    f_row.endTime = GpsDatas[endIndex].Time;
                                    System.Int64 indFuel = disp(beginIndex, endIndex);
                                    f_row.pointValue = fuelDictionary.ValueFuelSum[GpsDatas[indFuel].Id].averageValue;
                                    //���������� ������� ������� � ������ ��������
                                    f_row.DataGPS_ID = GpsDatas[indFuel].Id;
                                    f_row.date_ = GpsDatas[indFuel].Time.ToShortDateString();
                                    f_row.Times = GpsDatas[indFuel].Time.ToLongTimeString();
                                    f_row.time_ = GpsDatas[indFuel].Time;
                                    f_row.Lat = GpsDatas[indFuel].LatLng.Lat;
                                    f_row.Lon = GpsDatas[indFuel].LatLng.Lng;
                                    f_row.GenerateType = "system";
                                    f_row.TypeAlgorythm = (int) algType;
                                    AtlantaDataSet.FuelReport.AddFuelReportRow(f_row);
                                }
                            }
                        }
                    }

                    if (j > 64)
                    {
                        ReportProgressChanged(s);
                        j = 0;
                    }
                    else
                    {
                        j++;
                    }
                }

                // ������ �������� ���/����� - ��������� �������� �����/��������
                if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP)
                    CombineSimilarActions();

                #region -- ���������� ������ --

                if (m_row.GetFuelReportRows() != null)
                {
                    for (int index = 0; index < m_row.GetFuelReportRows().Length; index++)
                    {
                        var fRow = m_row.GetFuelReportRows()[index];
                        if (fRow.TypeAlgorythm == TypeAlgorithm)
                        {
                            if (fRow.dValue > 0)
                            {
                                summFuel += fRow.dValue;
                                iSummFuelCount++;
                            }
                            else
                            {
                                diffFuel += fRow.dValue;
                                iDiffFuelCount++;
                            }
                        }
                    }
                }

                _summarise.Fueling = summFuel; // ����� ����������
                _summarise.FuelingCount = iSummFuelCount; // ���������� ��������
                _summarise.Discharge = diffFuel; // ����� �����
                _summarise.DischargeCount = iDiffFuelCount; // ���������� ������
                _summarise.Total = _summarise.Before + _summarise.Fueling - _summarise.After +
                                   UseDischargeInFuelrate * _summarise.Discharge; // ����� ������

                //_summary.Rate = m_row.path < 50 ? 0 : _summary.Total * 100.0  / m_row.path; // ������ �� 100 ��
                _summarise.Rate = _summarise.Total * 100.0 / m_row.path; // ������ �� 100 ��

                #endregion

                #region --   ������� �������� ��������  --

                TimeSpan travelTime = //���������� �� �������� �������?????
                    //sumFuelRows[sumFuelRows.Length - 1].dataviewRow.time - sumFuelRows[0].dataviewRow.time;
                    GpsDatas[GpsDatas.Length - 1].Time - GpsDatas[0].Time;

                foreach (atlantaDataSet.KilometrageReportRow row in m_row.GetKilometrageReportRows())
                {
                    if (row.Distance == 0)
                    {
                        travelTime = travelTime.Subtract(row.Interval);
                    }
                }

                if (travelTime.TotalHours > 0)
                {
                    _summarise.AvgSpeed = m_row.path / travelTime.TotalHours;
                }

                _summarise.id = m_row.Mobitel_ID;
                _summarise.typeAlgo = (int) algType;

                #endregion


                fuelDictionary.Clear();

                AfterReportFilling();
                Algorithm.OnAction(this, new FuelEventArgs(m_row.Mobitel_ID, _summarise, (int) algType));
                System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Fuel Exception \n" + ex.Message + "\n" + ex.Source);
                Log(ex.Message + "\n" + ex.StackTrace + "\n");
            }
        } // Run

        public static void Log(string message)
        {
            File.AppendAllText("Base_Reports_Procedure_Fuel_log.log", message);
        }

        #endregion

        #region DUT Value

        /// <summary>
        /// ��������� ���������� ����� DataGps[], ��� ������ � ����������
        /// </summary>
        public void SetD_rows(GpsData[] rows, atlantaDataSet.mobitelsRow mRow)
        {
            GpsDatas = rows;
            m_row = mRow;
        }

        public int TypeAlgorithm
        {
            get { return (int) algType; }
        }

        public void SettingAlgoritm(object param)
        {
            if (param is AlgorithmType)
            {
                algType = (AlgorithmType)param;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }
        }

        /// <summary>
        /// �������� ������ �� atlantaDataSet.dataview. ������������� �������� ��� �������� ����������
        /// � ��� ������ ��������
        /// </summary>
        public void GettingValuesDUT(FuelDictionarys fuelDict)
        {
            try
            {
                fuelDict.Clear();

                //FindSensors(ref fuelDict.fuelSensor1, ref fuelDict.fuelSensor2, AlgorithmType.FUEL1);

                FindSensorsDUT(fuelDict.fuelSensors, algType /*AlgorithmType.FUEL1*/);

                if (fuelDict.fuelSensors.Count == 0)
                    return;

                fuelDict.GenerFuelDictonary(fuelDict.fuelSensors.Count);


                this.width = 1;

                if (set_row != null)
                {
                    this.width = set_row.band;
                }

                SelectFuelValues(fuelDict, GpsDatas);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error in GettingValuesDUT", MessageBoxButtons.OK);
            }

            try
            {
                CorrectFuelForTwoSensors(fuelDict);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Getting Values DUT", MessageBoxButtons.OK);
            }
        }

        public void GettingValuesDUT(FuelDictionarys fuelDict, GpsData[] gpsDatas)
        {
            GpsDatas = gpsDatas;
            GettingValuesDUT(fuelDict);
        }

        public void GettingValuesDUTAlgorithm(FuelDictionarys fuelDict, GpsData[] gpsDatas, object param)
        {
            GpsDatas = gpsDatas;
            SettingAlgoritm(param);
            GettingValuesDUT(fuelDict);
        }

        /// <summary>
        /// �������� ����������� ������ � ��������� ��������� �������� LogID 
        /// </summary>
        /// <param name="prevLogId">��������� �������� LogId</param>
        /// <param name="curLogId">�������� �������� LogId</param>
        private List<GpsData> getAdditionsData(atlantaDataSet.sensorsRow sensor, int prevLogId, int curLogId, int mobitelId,
            bool Is64Packet)
        {
            DataTable gpsDoData = null;

            using (var db = new DriverDb())
            {
                db.NewSqlConnection(db.CS);
                db.NewSqlCommand();
                db.CommandSqlConnection();
                db.CommandTimeout(800);
                db.CommandType(CommandType.StoredProcedure);

                if (Is64Packet)
                    db.CommandText("getDiapasone64");
                else
                    db.CommandText("getDiapasone");

                db.CommandParametersAdd(db.ParamPrefics + "mobile", db.GettingInt32(), mobitelId);
                db.CommandParametersAdd(db.ParamPrefics + "prevLogId", db.GettingInt32(), prevLogId);
                db.CommandParametersAdd(db.ParamPrefics + "currLogId", db.GettingInt32(), curLogId);
                db.SqlConnectionOpen();
                db.SqlDataReader = db.CommandExecuteReader();
                gpsDoData = db.GetDataTable(db.GetCommand);
                db.SqlConnectionClose();
            }

            List<GpsData> gpsDiaData = new List<GpsData>();

            if (gpsDoData != null)
            {
                Calibrate calibrateSensor = Calibration();

                for (int k = 0; k < gpsDoData.Rows.Count; k++)
                {
                    GpsData gps = new GpsData();
                    gps.Time = Convert.ToDateTime(gpsDoData.Rows[k]["time"].ToString());
                    long gpsDataId = (long)Convert.ToUInt32(gpsDoData.Rows[k]["DataGps_ID"].ToString());
                    gps.Id = gpsDataId;
                    gps.SrvPacketID = Convert.ToInt64(gpsDoData.Rows[k]["srvPack"].ToString());
                    gps.Events = (uint)Convert.ToUInt32(gpsDoData.Rows[k]["Events"].ToString());

                    if (Is64Packet)
                    {
                        byte[] initSensors = new byte[25];
                        if (gpsDoData.Rows[k]["sensor"] != null)
                        {
                            byte[] sensors = (byte[])gpsDoData.Rows[k]["sensor"];
                            sensors.CopyTo(initSensors, 25 - sensors.Length);
                            Array.Reverse(initSensors);
                            gps.Sensors = initSensors;
                        }
                    }
                    else
                    {
                        ulong snsrs = Convert.ToUInt64(gpsDoData.Rows[k]["sensor"].ToString());
                        gps.Sensors = BitConverter.GetBytes(snsrs);
                    }

                    gps.SensorValue = calibrateSensor.GetUserValue(gps.Sensors, sensor.Length, sensor.StartBit, sensor.K, sensor.B, sensor.S);
                    gpsDiaData.Add(gps);
                } // for
            } // if

            return gpsDiaData;
        }

        /// <summary>
        /// ������� ������. ���������� ����� ValueSeries1 � ValueSeries2
        /// ���������� �������� ������
        /// </summary>
        protected void SelectFuelValues(FuelDictionarys fuelDict, GpsData[] d_rows_sv)
        {
            //Calibrate calibrateSensor1 = null;
            //Calibrate calibrateSensor2 = null;

            List<Calibrate> calibrateSensors = new List<Calibrate>();

            for (int i = 0; i < fuelDict.fuelSensors.Count; i++)
            {
                if (fuelDict.FuelSensorExist(i))
                {
                    //��������, ��� ���������������� ������� fuelSensor, ������� ���������
                    s_row = fuelDict.fuelSensors[i];
                    calibrateSensors.Add(Calibration());
                }
            }

            //if (fuelDict.FuelSensor1Exist)
            //{
            //    //��������, ��� ������� fuelSensor1, ������� ���������
            //    s_row = fuelDict.fuelSensor1;
            //    calibrateSensor1 = Calibration();
            //}
            //if (fuelDict.FuelSensor2Exist)
            //{
            //    //��������, ��� ������� fuelSensor2, ������� ���������
            //    s_row = fuelDict.fuelSensor2;
            //    calibrateSensor2 = Calibration();
            //}

            if (d_rows_sv != null)
            {
                //foreach (GpsData d in d_rows_sv)
                for(int k = 0; k < d_rows_sv.Length; k++)
                {
                    GpsData d = d_rows_sv[k];
                    //if (calibrateSensor1 != null)
                    //{
                    //    double tmp = calibrateSensor1.GetUserValue(d.Sensors, fuelDict.fuelSensor1.Length,
                    //        fuelDict.fuelSensor1.StartBit, fuelDict.fuelSensor1.K);

                    //    fuelDict.ValueFuelSensor1.Add(d.Id, new ValuePair(tmp, tmp));
                    //}

                    //if (calibrateSensor2 != null)
                    //{
                    //    double tmp = calibrateSensor2.GetUserValue(d.Sensors, fuelDict.fuelSensor2.Length,
                    //        fuelDict.fuelSensor2.StartBit, fuelDict.fuelSensor2.K);
                    //    fuelDict.ValueFuelSensor2.Add(d.Id, new ValuePair(tmp, tmp));
                    //}

                    for (int i = 0; i < calibrateSensors.Count; i++)
                    {
                        if (calibrateSensors[i] != null)
                        {
                            double tmp = calibrateSensors[i].GetUserValue(d.Sensors, fuelDict.fuelSensors[i].Length,
                                fuelDict.fuelSensors[i].StartBit, fuelDict.fuelSensors[i].K, fuelDict.fuelSensors[i].B, fuelDict.fuelSensors[i].S);

                            if (fuelDict.valueFuelArrayDict[i].ContainsKey(d.Id))
                            {
                                double tmp1 = fuelDict.valueFuelArrayDict[i][d.Id].averageValue;
                                double tmp2 = fuelDict.valueFuelArrayDict[i][d.Id].value;
                                continue;
                            }

                            fuelDict.valueFuelArrayDict[i].Add(d.Id, new ValuePair(tmp, tmp));

                            if(tmp > 1)
                            {
                                double x = tmp;
                            }
                            else if(tmp != 0)
                            {
                                double y = tmp;
                            }
                            else
                            {
                                double z = tmp;
                            }
                            //AverageFuel(fuelDict.valueFuelArrayDict[i]);
                        } // if
                    } // for
                } // foreach

                //List<GpsData[]> gpsDiapasonData = new List<GpsData[]>();
                //for (int i = 0; i < calibrateSensors.Count; i++)
                //{
                //    if (calibrateSensors[i] != null)
                //    {
                //        GpsData[] gpsDt = new GpsData[calibrateSensors.Count];
                //        gpsDiapasonData.Add(gpsDt);
                //        int count = d_rows_sv.Length;
                //        GpsData lastPoint = d_rows_sv[count - 1];
                //        int prevlogId = lastPoint.LogId + 1;
                //        int postlogId = lastPoint.LogId + 1;
                //        gpsDiapasonData[i] =
                //            getAdditionsData(fuelDict.fuelSensors[i], prevlogId, postlogId, lastPoint.Mobitel,
                //                true).ToArray();
                //    }
                //}

                //for (int i = 0; i < calibrateSensors.Count; i++)
                //{
                //    if (calibrateSensors[i] != null)
                //    {
                //        GpsData d = gpsDiapasonData[i][0];
                //        double tmp = calibrateSensors[i].GetUserValue(d.Sensors, fuelDict.fuelSensors[i].Length,
                //            fuelDict.fuelSensors[i].StartBit, fuelDict.fuelSensors[i].K);
                //        int length = fuelDict.valueFuelArrayDict[i].Count;
                //        long id = d_rows_sv[d_rows_sv.Length - 1].Id;
                //        fuelDict.valueFuelArrayDict[i].Add(id, new ValuePair(tmp, tmp));

                //        //AverageFuel(fuelDict.valueFuelArrayDict[i]);
                //    } // if
                //} // for
            }

            //AverageFuel(fuelDict.ValueFuelSensor1);
            //AverageFuel(fuelDict.ValueFuelSensor2);

            for (int i = 0; i < fuelDict.valueFuelArrayDict.Count; i++)
            {
                 AverageFuel(fuelDict.valueFuelArrayDict[i]);
            } // for

            // ������������� �������� �� �������� � ����
            for (int i = 0; i < fuelDict.valueFuelArrayDict.Count; i++)
            {
                CorrectingAverageFuel(fuelDict.valueFuelArrayDict[i]);
            } // for
        }

        private void CorrectingAverageFuel(IDictionary<long, ValuePair> valuePairs)
        {
            try
            {
                if (valuePairs.Count > 0)
                {
                    //�������� ��� ����������� �������� ���� ��������� ���������������
                    System.Int64[] keysValueSeries = new System.Int64[valuePairs.Count];
                    valuePairs.Keys.CopyTo(keysValueSeries, 0);
                    ValuePair[] average_vPair = new ValuePair[valuePairs.Count];
                    valuePairs.Values.CopyTo(average_vPair, 0);
                    ValuePair[] source_vPair = new ValuePair[valuePairs.Count];
                    valuePairs.Values.CopyTo(source_vPair, 0);

                    for (int i = 0; i < average_vPair.Length; i++)
                    {
                        if (average_vPair[i].averageValue == 0)
                        {
                            if (source_vPair[i].value == 0)
                                continue;

                            valuePairs[keysValueSeries[i]].averageValue = valuePairs[keysValueSeries[i]].value;

                        } // if
                    } // for
                } // if
            } // try
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        /// ����������� �� �������� ������ ������ ������� � ��������� ��������������� ���� ValuePair.averageValue 
        /// </summary>
        public void AverageFuel(IDictionary<System.Int64, ValuePair> valueSeries)
        {
            if (valueSeries.Count > 0)
            {
                atlantaDataSet.KilometrageReportRow[] kilometrage_rows = m_row.GetKilometrageReportRows();
                double[] data = new double[valueSeries.Count];
                //�������� ��� ����������� �������� ���� ��������� ���������������
                System.Int64[] keysValueSeries = new System.Int64[valueSeries.Count];
                valueSeries.Keys.CopyTo(keysValueSeries, 0);
                ValuePair[] tmp_vPair = new ValuePair[valueSeries.Count];
                valueSeries.Values.CopyTo(tmp_vPair, 0);

                #region __filter_downfall_valueData_test_

                int start = -1; // test zero_jump
                for (int i = 0; i < tmp_vPair.Length; i++)
                {
                    if (tmp_vPair[i].value > 0) 
                    {
                        if (start > 0) // ����������� � ������� (�����)
                        {
                            for (int j = start; j < i; j++) // ��������� ���� ���������� ��������� � ������
                            {
                                valueSeries[keysValueSeries[j]].value = tmp_vPair[start - 1].value;
                            }
                        }
                        else if(start == 0)
                        {
                            for (int j = i; j >= start; j--) // ��������� ���� ���������� ��������� � ������
                            {
                                valueSeries[keysValueSeries[j]].value = tmp_vPair[i].value;
                            }
                        }

                        start = -1;
                    }
                    else if (start < 0 && i >= 0) // ������ ������ (����)
                    {
                        start = i; // ������ ������� �����
                    }
                } // for

                if (tmp_vPair[tmp_vPair.Length - 1].value == 0)
                {
                    int k = tmp_vPair.Length - 1;

                    while (k >= 0 && tmp_vPair[k].value == 0)
                    {
                        k--;
                    }

                    int j = k;

                    while (j >= 0 && j < tmp_vPair.Length)
                    {
                        valueSeries[keysValueSeries[j]].value = tmp_vPair[k].value;

                        j++;
                    }
                }

                #endregion //__filter_downfall_valueData_test_

                valueSeries.Values.CopyTo(tmp_vPair, 0);
                data = ValuePair.GetValue(tmp_vPair);

                int indexBegin = 0;
                int indexEnd = 200;

                if(kilometrage_rows.Length == 0)
                {
                    while (indexEnd < valueSeries.Count)
                    {
                        for (int j = indexBegin; j < indexEnd && j < valueSeries.Count; j++)
                        {
                            valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                        }

                        indexBegin = indexEnd;
                        indexEnd += indexEnd;
                    } // while

                    for (int j = indexBegin; j < valueSeries.Count; j++)
                    {
                        valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                    }
                } // if

                //�������� ����������� ������� ��� �������� ������� � �������� ��������

                foreach (atlantaDataSet.KilometrageReportRow k_row in kilometrage_rows)
                {
                    //Find indexes
                    indexBegin = Array.IndexOf<System.Int64>(keysValueSeries, k_row.InitialPointId);
                    indexEnd = Array.IndexOf<System.Int64>(keysValueSeries, k_row.FinalPointId);

                    if (indexBegin < 0)
                    {
                        indexBegin = 0;
                    }

                    if (indexEnd < 0)
                    {
                        indexEnd = 0;
                    }

                    for (int j = indexBegin; j < indexEnd; j++)
                    {
                        valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                    }
                }

                for (int j = indexEnd; j < valueSeries.Count; j++)
                {
                    valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                }
            }
        }

        /// <summary>
        /// ��������� ����� �� n �������� ������, ���� ���� ������ ���� �� �� ���� �������� � �������.
        /// ���� ������ ���� - �������� ��� ������ � ������� �����. ��� ���������� ������ ������������ ������ ������� �����.
        /// </summary>
        /// <param name="fuelDict"></param>
        protected void CorrectFuelForTwoSensors(FuelDictionarys fuelDict)
        {
            //if (fuelDict.ValueFuelSensor1.Count > 0 && fuelDict.ValueFuelSensor2.Count > 0)
            ////���� ���� ������ ���� �� �� ���� �������� - ������ �� �����
            //{
            //    System.Int64[] key = new System.Int64[fuelDict.ValueFuelSensor1.Count];
            //    fuelDict.ValueFuelSensor1.Keys.CopyTo(key, 0);

            //    for (int i = 0; i < fuelDict.ValueFuelSensor1.Count; i++)
            //    {
            //        double valueFuelSum = fuelDict.ValueFuelSensor1[key[i]].value +
            //                              fuelDict.ValueFuelSensor2[key[i]].value;
            //        double valueFuelSumAverage = fuelDict.ValueFuelSensor1[key[i]].averageValue +
            //                                     fuelDict.ValueFuelSensor2[key[i]].averageValue;
            //        fuelDict.ValueFuelSum.Add(key[i], new ValuePair(valueFuelSum, valueFuelSumAverage));
            //    }
            //}
            //else
            //{
            //    //�������� ��������, ������ ��� ����� ����� �������� �� ������ �����
            //    fuelDict.ValueFuelSum = new Dictionary<System.Int64, ValuePair>(fuelDict.ValueFuelSensor1);
            //}

            try
            {
                //���� ���� ������ ���� �� �� ���� �������� - ������ �� �����
                int countMax = 0;
                int indexMax = 0;
                for (int k = 0; k < fuelDict.valueFuelArrayDict.Count; k++) // ���� ������ �� ������� ������ �������
                {
                    if (fuelDict.valueFuelArrayDict[k].Count > countMax)
                    {
                        countMax = fuelDict.valueFuelArrayDict[k].Count;
                        indexMax = k;
                    }
                }

                Int64[] key = null;

                for (int k = 0; k < fuelDict.valueFuelArrayDict.Count; k++)
                {
                    key = new Int64[fuelDict.valueFuelArrayDict[k].Count];
                    fuelDict.valueFuelArrayDict[k].Keys.CopyTo(key, 0);
                }

                if (fuelDict.valueFuelArrayDict.Count > 0)
                {
                    for (int j = 0; j < fuelDict.valueFuelArrayDict[indexMax].Count; j++) // ����� ������ ����� ��������� �� ������� ��������
                    {
                        double valueFuelSum = 0;
                        double valueFuelSumAverage = 0;

                        for (int l = 0; l < fuelDict.valueFuelArrayDict.Count; l++)
                        {
                            if (fuelDict.valueFuelArrayDict[l].Count > j)
                            {
                                valueFuelSum += fuelDict.valueFuelArrayDict[l][key[j]].value;
                                valueFuelSumAverage += fuelDict.valueFuelArrayDict[l][key[j]].averageValue;
                            }
                        } // for

                        if (!fuelDict.ValueFuelSum.ContainsKey(key[j]))
                            fuelDict.ValueFuelSum.Add(key[j], new ValuePair(valueFuelSum, valueFuelSumAverage));
                    } // for
                }

               //�������� ��������, ������ ��� ����� ����� �������� �� ������ �����
               //fuelDict.ValueFuelSum =
               //  new Dictionary<System.Int64, ValuePair>(fuelDict.valueFuelArrayDict[k]);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error CorrectFuelForTwoSensors",
                    MessageBoxButtons.OK);
            }
        }

        #endregion //DUT Value

        #region private_methods

        /// <summary>
        /// ������� ������ �������� � �������, �� ���������� ���������� �������, � �������� DataGps_ID
        /// </summary>
        /// <param name="dgs_id"></param>
        /// <returns></returns>
        private System.Int64 FindIndexInSumValue(System.Int64 dgs_id)
        {
            System.Int64[] keys = new System.Int64[fuelDictionary.ValueFuelSum.Count];
            fuelDictionary.ValueFuelSum.Keys.CopyTo(keys, 0);
            return Array.IndexOf<System.Int64>(keys, dgs_id);
        }

        /// <summary>
        /// �������� �������� �������, ���� � ���������� ����� ��������� �����
        /// </summary>
        /// <param name="dValue">������, ��� �������� ������ ��������</param>
        /// <param name="kilometrage_row">KilometrageReportRow</param>
        /// <param name="isBeginStop">������� ������ �������� � ������ ���������/������, 
        /// ����� ������� ��������</param>
        private void MoveFuelValueOnTime(ref double dValue,
            atlantaDataSet.KilometrageReportRow kilometrage_row, bool isBeginStop)
        {
            try
            {
                if (set_row != null)
                {
                    if (kilometrage_row.Interval >= (set_row.TimeGetFuelAfterStop + set_row.TimeGetFuelBeforeMotion))
                    {
                        //���� ������������ ������� ������, �������� �� ���e� ���������
                        TimeSpan timeMoving = set_row.TimeGetFuelBeforeMotion;
                        if (isBeginStop)
                        {
                            timeMoving = set_row.TimeGetFuelAfterStop;
                        }
                        if (timeMoving.TotalSeconds > 0) //���� ����� �������� ������
                        {
                            //GpsData[] tmpD_view = DataSetManager.GetGpsDataExceptLastPoint(
                            //    m_row,
                            //    kilometrage_row.InitialTime + set_row.TimeGetFuelAfterStop,
                            //    kilometrage_row.FinalTime - set_row.TimeGetFuelBeforeMotion);
                            GpsData[] tmpD_view =
                                GpsDatas.Where(
                                    gps => gps.Time >= kilometrage_row.InitialTime + set_row.TimeGetFuelAfterStop
                                           && gps.Time <= kilometrage_row.FinalTime - set_row.TimeGetFuelBeforeMotion
                                           && gps.Mobitel == m_row.Mobitel_ID).ToArray();

                            if (tmpD_view.Length > 0)
                            {
                                if (isBeginStop)
                                {
                                    //sumFuelRows[atlantaDataSet.FuelValue.SelectByDataGPSID(
                                    //sumFuelRows, tmpD_view[0].DataGps_ID)].Value;//.Average;
                                    if (fuelDictionary.ValueFuelSum.ContainsKey(tmpD_view[0].Id))
                                        dValue = fuelDictionary.ValueFuelSum[tmpD_view[0].Id].value;
                                }
                                else
                                {
                                    //sumFuelRows[atlantaDataSet.FuelValue.SelectByDataGPSID(
                                    //sumFuelRows, tmpD_view[tmpD_view.Length - 1].DataGps_ID)].Value;//.Average;
                                    if (fuelDictionary.ValueFuelSum.ContainsKey(tmpD_view[tmpD_view.Length - 1].Id))
                                        dValue = fuelDictionary.ValueFuelSum[tmpD_view[tmpD_view.Length - 1].Id].value;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error MoveFuelValueOnTime");
            }
        }

        private bool TestIgnition(GpsData[] gpsDatas)
        {
            bool ret = false;
            atlantaDataSet.sensorsRow engineSensor = FindSensor(AlgorithmType.WORK_E);
            if (engineSensor != null)
            {
                Calibrate calibr = Calibration();
                foreach (GpsData gpsData in gpsDatas)
                {
                    //if(calibr.GetUserValue(tmpDv_row, engineSensor.Length, engineSensor.StartBit, engineSensor.K) > 0)
                    //if (((gpsData.Sensors >> engineSensor.StartBit) & 0x01) > 0)//engine on
                    if (
                        calibr.GetUserValue(gpsData.Sensors, engineSensor.Length, engineSensor.StartBit, engineSensor.K, engineSensor.B, engineSensor.S) >
                        0)

                    {
                        ret = true;
                        break;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// ���������� ����������������� ����� ����� ������ � ������� 
        /// (��������� �������� �������� � ��������� ����������� � ���� ������)
        /// </summary>
        /// <param name="kr_row">������ ������ � �������</param>
        /// <returns></returns>
        private atlantaDataSet.KilometrageReportRow[] CorrectingKilometrage(atlantaDataSet.KilometrageReportRow[] kr_row)
        {
            List<atlantaDataSet.KilometrageReportRow> kilometrage_corrected =
                new List<atlantaDataSet.KilometrageReportRow>();
            bool last_correct = false;
            TimeSpan timeBreak = new TimeSpan(0, 5, 0);
            if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
                timeBreak = set_row.TimeBreak;

            for (int n = 0; n < kr_row.Length - 2; n++)
            {
                System.Windows.Forms.Application.DoEvents();
                if (kr_row[n].Distance == 0 && kr_row[n].State != Resources.Movement)
                {
                    last_correct = false;
                    TimeSpan timeMoving = (kr_row[n + 1].Interval);
                        //����� ��������� ����� � ������ ������ ���� ��������
                    if (timeMoving.TotalSeconds < timeBreak.TotalSeconds*1.2)
                        //��� �������� ���� ������, ��� ���. ����� ��������� +20% �� ��������� ������ �� ��� ������
                    {
                        //List<atlantaDataSet.dataviewRow> list_dataview = new List<atlantaDataSet.dataviewRow>();
                        List<GpsData> list_dataGps = new List<GpsData>();
                        GpsData[] atlDv_row = null;
                        foreach (GpsData  tmpGpsData in GpsDatas) //�������� ������ ��������� � �������� ��������
                        {
                            if (tmpGpsData.Time >= kr_row[n + 1].InitialTime &&
                                tmpGpsData.Time <= kr_row[n + 1].FinalTime)
                            {
                                list_dataGps.Add(tmpGpsData);
                            }
                        }

                        atlDv_row = list_dataGps.ToArray();
                        //��������� ���� �� �� ���� ��������� ������� �������� ������� ����� ��������� //���� ������ �� ���� ��������
                        var gpsDataSpeed = new Dictionary<long, double>();
                        list_dataGps.ForEach(gpsData => gpsDataSpeed.Add(gpsData.Id, gpsData.Speed));
                        Spoint[] points = Algorithm.FindMin(gpsDataSpeed, 0); //�������� ����� ��� �������� �������

                        //��������� ���������� �� ��������� �� ������ ��������� ��� ��������
                        bool engineOff = TestIgnition(atlDv_row);

                        if (points.Length != 0 || atlDv_row.Length == 1 || engineOff)
                            // �������� ������� ��������� ��� ���� ����� �� ��������� ���� ������ ����
                        {
                            TimeSpan duration = new TimeSpan();
                            foreach (Spoint point in points)
                            {
                                DateTime timeY = GpsDatas.First(gps => gps.Id == point.Y).Time;
                                DateTime timeX = GpsDatas.First(gps => gps.Id == point.X).Time;
                                //duration += atlantaDataSet.dataview.FindByDataGps_ID(point.Y).time - atlantaDataSet.dataview.FindByDataGps_ID(point.X).time;
                                duration += timeY - timeX;
                            }
                            if (duration.TotalSeconds >= 2*timeMoving.TotalSeconds/3)
                                //���� ����� �������� ������ 2/3 �� ����� ��������, �� ��������� ��� �������� ���������
                            {
                                //����� ��������� ��� ���������
                                atlantaDataSet.KilometrageReportRow krr_row =
                                    AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();

                                krr_row.MobitelId = kr_row[n].MobitelId;
                                krr_row.InitialPointId = kr_row[n].InitialPointId;
                                krr_row.FinalPointId = kr_row[n + 2].FinalPointId;
                                krr_row.Distance = 0;
                                krr_row.InitialTime = kr_row[n].InitialTime;
                                krr_row.Location = kr_row[n].Location;
                                krr_row.FinalTime = kr_row[n + 2].FinalTime;
                                krr_row.Interval = krr_row.FinalTime - krr_row.InitialTime;
                                kilometrage_corrected.Add(krr_row);
                                n += 2; // ������������� ��������� �������� � ���������
                                last_correct = true;
                                continue;
                            }
                        }
                    }
                    kilometrage_corrected.Add(kr_row[n]);
                }
            }

            //TODO: ��� ������ ���� ����������� ������ � ��������� if ???
            if (!last_correct)
                //���� ��������� ������ � ���������� � ������ ������� �� ���� ������������ ��� �������������, ������� � ��
                if (kr_row[kr_row.Length - 1].Distance == 0) //���� ��������� ������ ���������
                    kilometrage_corrected.Add(kr_row[kr_row.Length - 1]);
                else //���� ������������� ������ ���������
                    if (kr_row.Length >= 2)
                        kilometrage_corrected.Add(kr_row[kr_row.Length - 2]);
            return kilometrage_corrected.ToArray();
        }

        private atlantaDataSet.KilometrageReportRow[] CombineAdjacentStopsOrMoves(atlantaDataSet.KilometrageReportRow[] krRows)
        {
            var kmCombinedStopsAndMoves = new List<atlantaDataSet.KilometrageReportRow>();
            var stopsRows = new List<atlantaDataSet.KilometrageReportRow>();
            var moveRows = new List<atlantaDataSet.KilometrageReportRow>();
            for (int i = 0; i < krRows.Length; i++)
            {
                if (krRows[i].Distance == 0 && krRows[i].State != Resources.Movement)
                {
                    if (moveRows.Count() > 0)
                    {
                        GetCombinedMoves(moveRows, kmCombinedStopsAndMoves);
                    }
                    moveRows.Clear();
                    stopsRows.Add(krRows[i]);
                }
                else
                {
                    if (stopsRows.Count() > 0)
                    {
                        GetCombinedStops(stopsRows, kmCombinedStopsAndMoves);
                    }
                    stopsRows.Clear();
                    moveRows.Add(krRows[i]);
                }
            }
            if (stopsRows.Count() > 0)
            {
                GetCombinedStops(stopsRows, kmCombinedStopsAndMoves);
            }
            else if (moveRows.Count() > 0)
            {
                GetCombinedMoves(moveRows, kmCombinedStopsAndMoves);
            }
            return kmCombinedStopsAndMoves.ToArray();
        }

        private atlantaDataSet.KilometrageReportRow[] GetOneTotalKilometrageReportRow(atlantaDataSet.KilometrageReportRow[] krRows)
        {
            var totalKilometrageReportRows = new List<atlantaDataSet.KilometrageReportRow>();
            int indexStopRow = 0;
            for (int i = 0; i < krRows.Length; i++)
            {
                if (krRows[i].Distance == 0)
                {
                    indexStopRow = i;
                    break;
                }
            }
            krRows[indexStopRow].InitialPointId = krRows[0].InitialPointId;
            krRows[indexStopRow].FinalPointId = krRows[krRows.Count() -1].FinalPointId;
            krRows[indexStopRow].InitialTime = krRows[0].InitialTime;
            krRows[indexStopRow].FinalTime = krRows[krRows.Count() - 1].FinalTime;
            totalKilometrageReportRows.Add(krRows[indexStopRow]);
            return totalKilometrageReportRows.ToArray();
        }

        private void GetCombinedStops(List<atlantaDataSet.KilometrageReportRow> stopsRows, List<atlantaDataSet.KilometrageReportRow> krRowsCombinedStops)
        {
            var krRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            krRow.MobitelId = stopsRows[0].MobitelId;
            krRow.InitialPointId = stopsRows[0].InitialPointId;
            krRow.FinalPointId = stopsRows[stopsRows.Count - 1].FinalPointId;
            krRow.Distance = 0;
            krRow.InitialTime = stopsRows[0].InitialTime;
            krRow.Location = stopsRows[0].Location;
            krRow.FinalTime = stopsRows[stopsRows.Count - 1].FinalTime;
            krRow.Interval = krRow.FinalTime - krRow.InitialTime;
            krRow.State = stopsRows[0].State; 
            krRowsCombinedStops.Add(krRow);
        }

        private void GetCombinedMoves(List<atlantaDataSet.KilometrageReportRow> movesRows, List<atlantaDataSet.KilometrageReportRow> krRowsCombinedStops)
        {
            var krRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            krRow.MobitelId = movesRows[0].MobitelId;
            krRow.InitialPointId = movesRows[0].InitialPointId;
            krRow.FinalPointId = movesRows[movesRows.Count - 1].FinalPointId;
            krRow.Distance = 0;
            foreach (var movesRow in movesRows)
            {
                krRow.Distance += movesRow.Distance;
            }
            krRow.InitialTime = movesRows[0].InitialTime;
            krRow.FinalTime = movesRows[movesRows.Count - 1].FinalTime;
            krRow.Interval = krRow.FinalTime - krRow.InitialTime;
            krRow.State = movesRows[0].State;
            krRowsCombinedStops.Add(krRow);
        }

        /// <summary>
        /// ���������� �����, ��� ������ ��������� ������ ������ ������ ������� � ������ ���������
        /// </summary>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="current_stop"></param>
        /// <param name="inStop"></param>
        /// <param name="begin"></param>
        /// <returns></returns>
        private System.Int64 FindStartIndexLine(atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            int current_stop, ref bool inStop, System.Int64 beginIndex)
        {
            System.Int64 startIndex = FindIndexInSumValue(tmpKilimetrage_rows[current_stop - 1].FinalPointId);
            TimeSpan delta = (tmpKilimetrage_rows[current_stop].InitialTime -
                              tmpKilimetrage_rows[current_stop - 1].FinalTime);
            TimeSpan minutes = GetAproximationTime();

            if (delta < minutes)
            {
                delta = (tmpKilimetrage_rows[current_stop - 1].FinalTime -
                         tmpKilimetrage_rows[current_stop - 1].InitialTime); //����� �� ������ ���������� ���������
                double parcent_time = (delta.TotalMinutes/minutes.TotalMinutes);
                if (parcent_time <= 0)
                {
                    parcent_time = 1;
                }
                startIndex =
                    (int)
                        (startIndex -
                         (startIndex - FindIndexInSumValue(tmpKilimetrage_rows[current_stop - 1].InitialPointId))
                         /parcent_time);
                inStop = true;
            }
            else
            {
                if (minutes.TotalMinutes > 0 && delta.TotalMinutes >0)
                    startIndex =
                        (int) (beginIndex - 5 - (beginIndex - startIndex)/(delta.TotalMinutes/minutes.TotalMinutes));
                        //�������� ��������� ������ ������ �� ����� 10 �����
                else
                {
                    startIndex = beginIndex;
                }
            }
            //�������� ������ � ������
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            return startIndex;
        }

        /// <summary>
        /// ���������� �����, ��� ��������� ��������� ������ ������ ������ ������� ����� ������� ��������
        /// </summary>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="current_stop"></param>
        /// <param name="inStop"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private System.Int64 FindFinishIndexLine(atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            int current_stop, ref bool inStop, System.Int64 end)
        {
            System.Int64 finish =
                //atlantaDataSet.FuelValue.SelectByDataGPSID(sumFuelRows, tmpKilimetrage_rows[current_stop + 1].InitialPointId);
                FindIndexInSumValue(tmpKilimetrage_rows[current_stop + 1].InitialPointId);
            TimeSpan delta = tmpKilimetrage_rows[current_stop + 1].InitialTime -
                             tmpKilimetrage_rows[current_stop].FinalTime; //������������ �� ��������� ���������
            TimeSpan aproximTime = GetAproximationTime();
            if (delta < aproximTime)
            {
                aproximTime -= delta;
                delta = (tmpKilimetrage_rows[current_stop + 1].FinalTime -
                         tmpKilimetrage_rows[current_stop + 1].InitialTime); //����� �� ����� ��������� ���������
                double parcent_time = (delta.TotalMinutes/aproximTime.TotalMinutes);
                if (parcent_time <= 0)
                {
                    parcent_time = 1;
                }
                if (delta.TotalMinutes > 0)
                {
                    finish =
                        (int)
                            (finish +
                             ( //atlantaDataSet.FuelValue.SelectByDataGPSID(sumFuelRows, tmpKilimetrage_rows[current_stop + 1].FinalPointId)
                                 FindIndexInSumValue(tmpKilimetrage_rows[current_stop + 1].FinalPointId)
                                 - finish)/parcent_time);
                }
                inStop = true;
            }
            else
            {
                if (aproximTime.TotalMinutes > 0)
                {
                    int partTime = (int) (delta.TotalMinutes/aproximTime.TotalMinutes);
                    if (partTime > 0)
                        finish = end + (finish - end)/partTime; //�������� �������� ������ ������ �� ����� 10 �����
                }
                else
                {
                    finish = end;
                }
            }
            return finish;
        }

        private TimeSpan GetAproximationTime()
        {
            if (set_row != null)
            {
                return set_row.FuelApproximationTime;
            }
            else
            {
                return new TimeSpan(0, 10, 0);
            }
        }

        /// <summary>
        /// �������� ����� �� ��������, ��� ���������� �����
        /// </summary>
        /// <param name="len"></param>
        /// <param name="beginIndex"></param>
        /// <returns></returns>
        private double[] SelectLinePoints(int len, System.Int64 beginIndex)
        {
            if (len < 0)
            {
                len = 0;
            }
            double[] line_point = new double[len];
            ValuePair[] fuelValue = new ValuePair[fuelDictionary.ValueFuelSum.Count];
            fuelDictionary.ValueFuelSum.Values.CopyTo(fuelValue, 0);
            for (int l = 0; l < line_point.Length; l++)
            {
                System.Int64 num = beginIndex + l;
                if (num >= fuelValue.Length)
                    num = fuelValue.Length - 1;

                line_point[l] = fuelValue[num].averageValue;
            }

            return line_point;
        }

        /// <summary>
        /// ��������� ��������� � �������� ������� ������� ��� ���������. ���������� ������ ��������� ������, ����� ���������
        /// ������ ������� ���������� ���������, ������� ������ ������� ������� ��������� ������.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="beginValue"></param>
        /// <param name="endValue"></param>
        private bool CountBeginAndEndFuelValue(int numStop, atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            ref double beginValue, ref double endValue, ref System.Int64 beginIndex, ref System.Int64 endIndex,
            bool newSearch)
        {
            bool ret = true;

            if (newSearch) //Math.Round(beginValue, 3) == 0 && Math.Round(endValue, 3) == 0)
                beginIndex = FindIndexInSumValue(tmpKilimetrage_rows[numStop].InitialPointId);
            else
                beginIndex = endIndex;

            endIndex = FindIndexInSumValue(tmpKilimetrage_rows[numStop].FinalPointId);

            if (fuelDictionary.ValueFuelSum.ContainsKey(tmpKilimetrage_rows[numStop].InitialPointId))
                beginValue = fuelDictionary.ValueFuelSum[tmpKilimetrage_rows[numStop].InitialPointId].averageValue;
                ///sumFuelRows[begin].Value;
            if (fuelDictionary.ValueFuelSum.ContainsKey(tmpKilimetrage_rows[numStop].FinalPointId))
                endValue = fuelDictionary.ValueFuelSum[tmpKilimetrage_rows[numStop].FinalPointId].averageValue;
                //sumFuelRows[end - 1].Value;
            if (!newSearch) //������ ���� ��� ��������� ��� ���������� useMinMaxAlgoritm
                ret = false;
            // ��������� ��������� � �������� �������� ������� ���������� ���������
            if (useMinMaxAlgoritm == MIN_MAX_DISABLE)
            {
                #region defaultAlgorithm

                if (0 != numStop)
                {
                    //��������� ������������ �������� ��
                    bool inStop = false;
                    System.Int64 startIndex = FindStartIndexLine(tmpKilimetrage_rows, numStop, ref inStop, beginIndex);
                    double[] line_point = SelectLinePoints((int) (beginIndex - startIndex - 3), startIndex + 1);
                        //3-��� ���������� ������ ������� ������ ���� �����
                    if (line_point.Length >= 2)
                    {
                        bool isLineIncrease = false;
                        double tmpBeginValue = GetValueMetodomLine(line_point, line_point.Length - 1, ref isLineIncrease);
                        if (isLineIncrease && (beginValue - tmpBeginValue) < 0)
                        {
                            beginValue = (tmpBeginValue + beginValue)/2;
                        }
                        else if (!inStop) //|| Math.Abs(beginValue - tmpBeginValue) < fuelingEdge)
                        {
                            beginValue = tmpBeginValue;
                        }
                    }
                    MoveFuelValueOnTime(ref beginValue, tmpKilimetrage_rows[numStop], true);
                }

                if (numStop + 1 < tmpKilimetrage_rows.Length)
                {
                    //��������� ������������ �������� �����
                    bool inStop = false;
                    System.Int64 finish = FindFinishIndexLine(tmpKilimetrage_rows, numStop, ref inStop, endIndex);
                    double[] line_point = SelectLinePoints((int) (finish - endIndex), endIndex);
                    if (line_point.Length >= 2)
                    {
                        bool isLineIncrease = false;
                        double tmpEndValue = GetValueMetodomLine(line_point, 0, ref isLineIncrease);
                        if (isLineIncrease && (endValue - tmpEndValue) > 0)
                        {
                            endValue = (tmpEndValue + endValue)/2;
                        }
                        else if (!inStop || Math.Abs(endValue - tmpEndValue) < fuelingEdge)
                        {
                            endValue = tmpEndValue;
                        }
                    }
                    MoveFuelValueOnTime(ref endValue, tmpKilimetrage_rows[numStop], false);
                }

                #endregion //defaultAlgorithm
            }
            else
            {
                //count begin/endValue on Min/Max algorithm
                ValuePair[] fuelValue = new ValuePair[fuelDictionary.ValueFuelSum.Count];
                fuelDictionary.ValueFuelSum.Values.CopyTo(fuelValue, 0);
                double maxValue = 0;
                double minValue = 0;
                System.Int64 maxIndex = -1;
                System.Int64 minIndex = beginIndex;
                if (useMinMaxAlgoritm == MIN_MAX_ONE  && newSearch)
                {
                    #region AlgorithmMinMax_1

                    for (System.Int64 i = beginIndex; i < fuelValue.Length && i < endIndex; i++) //find maximum
                    {
                        if (fuelValue[i].averageValue > maxValue)
                        {
                            maxValue = fuelValue[i].averageValue;
                            maxIndex = i;
                        }
                    }
                    minValue = maxValue;
                    minIndex = maxIndex;
                    for (System.Int64 i = beginIndex; i < fuelValue.Length && i < maxIndex; i++) //find minimum
                    {
                        if (fuelValue[i].averageValue < minValue)
                        {
                            minValue = fuelValue[i].averageValue;
                            minIndex = i;
                        }
                    }
                    if (minIndex < maxIndex && (maxValue - minValue) > fuelingEdge) //���� ������ ��������
                    {
                        endValue = maxValue;
                        beginValue = minValue;
                        endIndex = maxIndex;
                        beginIndex = minIndex;
                    }

                    #endregion
                }
                else
                {
                    if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP || useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT)
                    {
//use MIN_MAX=2 or MIN_MAX=3

                        #region AlgorithmMinMax_2

                        double fuelrateOnSecond = flowmeterNorm/3600;
                        bool isBeginChange = false;
                        maxIndex = endIndex;

                        for (System.Int64 i = beginIndex; i < fuelValue.Length - 1 && i < endIndex - 1; i++)
                        {
                            double fuelrate = Math.Abs((fuelValue[i].averageValue - fuelValue[i + 1].averageValue)/
                                                       (GpsDatas[i + 1].Time - GpsDatas[i].Time).TotalSeconds);
                            if (fuelrate*2 > fuelrateOnSecond)
                            {
                                //begin changeFuel
                                if (!isBeginChange)
                                    minIndex = i;
                                isBeginChange = true;
                            }
                            else
                            {
                                if (isBeginChange)
                                {
                                    //save change
                                    isBeginChange = false;
                                    maxIndex = i;
                                    break;
                                }
                            }
                        }

                        if (isBeginChange)
                            maxIndex = endIndex;

                        maxValue = fuelValue[maxIndex].averageValue;
                        minValue = fuelValue[minIndex].averageValue;
                        if (minIndex < maxIndex && maxIndex != endIndex && minIndex != endIndex - 1)
                            //��� �� ��������� �� �����
                        {
                            ret = true;
                            endValue = maxValue;
                            beginValue = minValue;
                            beginIndex = minIndex;
                            endIndex = maxIndex;
                        }

                        #endregion
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// ���������� ������ ������� �� �������� ���������� � ������� �� ����, ��� ������ ��������.
        /// </summary>
        /// <param name="beginVal"></param>
        /// <param name="endVal"></param>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="numStop"></param>
        /// <returns>true - ��������/����, false - ������</returns>
        private bool TestTrueFueling(double beginVal, double endVal,
            atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows, int numStop)
        {
            int equal = 0;
            double before_begin = 0; // beginVal;
            double after_end = 0; // endVal;
            TimeSpan timeMoveBefo = new TimeSpan(0, 0, 0), timeMoveAfter = new TimeSpan(0, 0, 0);
            if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP)
                return true;

            if (0 != numStop)
            {
                timeMoveBefo = (tmpKilimetrage_rows[numStop].InitialTime - tmpKilimetrage_rows[numStop - 1].FinalTime);
                before_begin = fuelDictionary.ValueFuelSum[tmpKilimetrage_rows[numStop - 1].FinalPointId].averageValue;
            }
            if (numStop + 1 < tmpKilimetrage_rows.Length)
            {
                timeMoveAfter = tmpKilimetrage_rows[numStop + 1].InitialTime - tmpKilimetrage_rows[numStop].FinalTime;
                after_end = fuelDictionary.ValueFuelSum[tmpKilimetrage_rows[numStop + 1].InitialPointId].averageValue;
                
            }

            double deltaFuelValue = beginVal - endVal; //�� ������� - ����
            double fuelStep = fuelingDischarge;
            if (endVal - beginVal > fuelingEdge) //��������
            {
                fuelStep = fuelingEdge;
                deltaFuelValue = endVal - beginVal;
            }

            if (timeMoveBefo.TotalHours > 0 && before_begin > 0) //���� ��������� �������� �� � ���� �������� ��������
            {
                //���� ����� ��������� ��� ����������/���������� ������ �� �������� � ��� ������ � �������� ��������/����� - ������� �� ������
                double flowmeterValue = (before_begin - beginVal);
                double deltaValue = Math.Abs(flowmeterValue - flowmeterNorm * timeMoveBefo.TotalHours);

                if (Math.Abs(deltaValue - deltaFuelValue) < fuelStep/2)
                {
                    equal++;
                }
            }

            if (timeMoveAfter.TotalHours > 0 && after_end > 0)
            {
                double flowmeterValue = (endVal - after_end);
                double deltaValue = Math.Abs(flowmeterValue - flowmeterNorm*timeMoveAfter.TotalHours);

                if (Math.Abs(deltaValue - deltaFuelValue) < fuelStep/2)
                {
                    equal++;
                }
            }
            return ((equal%2) == 0);
        }

        private bool TestTrueDischarge(atlantaDataSet.KilometrageReportRow km_rows, double fuelStart, double fuelEnd)
        {
            //double[] tachometerVal = ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row);
            bool ret = true;
            if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP)
                return true;

            atlantaDataSet.TachometerValueRow[] rotVal_row = m_row.GetTachometerValueRows();
            if (rotVal_row.Length > 0 && (fuelStart - fuelEnd > fuelingDischarge) &&
                rotVal_row.Length == fuelDictionary.ValueFuelSum.Count) //������ ���� ���� �������! � ����
            {
                int minRotate = 10;
                double normFuelrate = 30;

                if (set_row != null) //���� ���� ������ � ���������� �� ����� ������ ������
                {
                    minRotate = set_row.RotationMain;
                    normFuelrate = set_row.AvgFuelRatePerHour;
                }

                System.Int64 beginIndex = FindIndexInSumValue(km_rows.InitialPointId);
                System.Int64 endIndex = FindIndexInSumValue(km_rows.FinalPointId);
                ValuePair[] fuelValue = new ValuePair[fuelDictionary.ValueFuelSum.Count];
                fuelDictionary.ValueFuelSum.Values.CopyTo(fuelValue, 0);

                TimeSpan timeRotate = new TimeSpan(0);
                System.Int64 startRotate = -1;
                System.Int64 endRotate = -1;

                for (System.Int64 i = beginIndex; i < endIndex; i++)
                {
                    if (rotVal_row[i].Value >= minRotate && endRotate >= startRotate)
                    {
                        startRotate = i;
                    }
                    if (endRotate < startRotate && rotVal_row[i].Value < minRotate)
                    {
                        endRotate = i;
                        //timeRotate += (rotVal_row[endRotate].dataviewRow.time - rotVal_row[startRotate].dataviewRow.time);
                        timeRotate += (GpsDatas.First(gps => gps.Id == rotVal_row[endRotate].Id).Time
                                       - GpsDatas.First(gps => gps.Id == rotVal_row[startRotate].Id).Time);
                    }
                }

                if (endRotate < startRotate)
                    timeRotate += (GpsDatas.First(gps => gps.Id == rotVal_row[endIndex].Id).Time
                                   - GpsDatas.First(gps => gps.Id == rotVal_row[startRotate].Id).Time);
                
                //timeRotate += (rotVal_row[endIndex].dataviewRow.time - rotVal_row[startRotate].dataviewRow.time);

                double maxAdmitFuelrate = timeRotate.TotalHours*normFuelrate;
                if (maxAdmitFuelrate > 0 && maxAdmitFuelrate > (fuelStart - fuelEnd))
                    ret = false;
            }
            //atlantaDataSet.RotateReportRow[] rot_rows = (atlantaDataSet.RotateReportRow[])atlantaDataSet.RotateReport.Select("State = '��������' AND MobitelId=" + m_row.Mobitel_ID.ToString()
            //  + " AND ((InitialTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and InitialTime  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#" +
            //  ") OR (FinalTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and FinalTime  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#  and InitialTime  < #" + dtSeek.ToString("MM.dd.yyyy") + "#))", "InitialTime");
            return ret;
        }

        void CombineSimilarActions()
        {
            if (m_row.GetFuelReportRows() != null)
            {
                atlantaDataSet.FuelReportRow[] fRows = m_row.GetFuelReportRows();
                if (fRows.Length > 1)
                {
                    atlantaDataSet.FuelReportRow prevFrow = fRows[0];
                    for (int i = 1; i < fRows.Length; i++)
                    {
                        if (Math.Sign(prevFrow.dValue) == Math.Sign(fRows[i].dValue))
                        {
                            if (fRows[i].beginTime.Subtract(prevFrow.endTime) <= timeForCombine)
                            {
                                prevFrow.endValue = fRows[i].endValue;
                                prevFrow.dValue = prevFrow.endValue - prevFrow.beginValue;
                                prevFrow.endTime = fRows[i].endTime;
                                AtlantaDataSet.FuelReport.RemoveFuelReportRow(fRows[i]);
                            }
                            else
                                prevFrow = fRows[i];
                        }
                        else
                            prevFrow = fRows[i];
                    }
                    //atlantaDataSet.FuelReport.RemoveFuelReportRow();
                    //atlantaDataSet.FuelReportRow fRow = null;
                    //AtlantaDataSet.FuelReport.RemoveFuelReportRow(fRow);
                }

                //if (Math.Sign(fRows[fRows.Length - 1].dValue) == Math.Sign(dValue))
                //{
                    
                //}


                //atlantaDataSet.FuelReportRow f_row = AtlantaDataSet.FuelReport.NewFuelReportRow();
                //f_row.mobitel_id = this.m_row.Mobitel_ID;
                //f_row.sensor_id = base.s_row.id;
                //f_row.Location = k_row.Location;
                //f_row.beginValue = beginValue;
                //f_row.endValue = endValue;
                //f_row.dValue = endValue - beginValue;

                //if (f_row.dValue > 0)
                //{
                //    summFuel += f_row.dValue;
                //    iSummFuelCount++;
                //}
                //else
                //{
                //    diffFuel += f_row.dValue;
                //    iDiffFuelCount++;
                //}

                //System.Int64 indFuel = disp(beginIndex, endIndex);
                //f_row.pointValue = fuelDictionary.ValueFuelSum[GpsDatas[indFuel].Id].averageValue;
                ////���������� ������� ������� � ������ ��������
                //f_row.DataGPS_ID = GpsDatas[indFuel].Id;
                //f_row.time_ = GpsDatas[indFuel].Time;
                //f_row.Lat = GpsDatas[indFuel].LatLng.Lat;
                //f_row.Lon = GpsDatas[indFuel].LatLng.Lng;
                //f_row.GenerateType = "system";
            }

        }

        #endregion

        #region --   Nested struct Fuel.Summary   --

        /// <summary>
        /// ������������ �������� ������ ������ � ��������� � ������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        public struct Summary
        {
            /// <summary>
            /// ������ ������� � ������ ��������� �������
            /// </summary>
            public double Before;

            /// <summary>
            /// ������ ������� � ����� ��������� �������
            /// </summary>
            public double After;

            /// <summary>
            /// ������ �������, ������������� �� �������� ������
            /// </summary>
            public double Fueling;

            /// <summary>
            /// ���������� �������� �� �������� ������
            /// </summary>
            public int FuelingCount;

            /// <summary>
            /// ������ �������, ������� �� �������� ������
            /// </summary>
            public double Discharge;

            /// <summary>
            /// ���������� ������ �� �������� ������
            /// </summary>
            public int DischargeCount;

            /// <summary>
            /// ����� ������ �������, ��������������� ������������ ���������
            /// (�.�. ��� ����� ������� �������)
            /// </summary>
            public double Total;

            /// <summary>
            /// ������� ������ ������� �� 100 �� �������
            /// </summary>
            public double Rate;

            /// <summary>
            /// ������� �������� ��������
            /// </summary>
            public double AvgSpeed;

            /// <summary>
            /// �������������� mobitel
            /// </summary>
            public int id;

            /// <summary>
            /// type of algorythm
            /// </summary>
            public int typeAlgo;
        }

        #endregion
    }

    /// <summary>
    /// ������������� ��������� ��� ����������� ������� ���������
    /// ������ ��������� ���������� ������ ��� ������ � ��������� 
    /// � ������ �� ����������� ������������� ��������
    /// </summary>
    public class FuelEventArgs : EventArgs
    {
        /// <summary>
        /// ������� � �������������� ����� ��������� ������ BaseReports.Procedure.FuelEventArgs
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public FuelEventArgs(int id, Fuel.Summary summary, int typeAlg)
        {
            _id = id;
            _summary = summary;
            _typeAlg = typeAlg;
        }

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// ID ���������
        /// </summary>
        public int IdAlgo
        {
            get { return _typeAlg; }
        }

        private int _id;
        private int _typeAlg;

        /// <summary>
        /// �������� ������ ������ � ��������� � ������ �� �����������
        /// ��������� (������������� ��������).
        /// </summary>
        public Fuel.Summary Summary
        {
            get
            {
                return _summary;
            }
        }

        private Fuel.Summary _summary;
    }
}
