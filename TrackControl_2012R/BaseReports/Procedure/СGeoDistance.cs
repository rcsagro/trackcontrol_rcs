﻿// Project: Fuel, File: СGeoDistance.cs
// Namespace: Fuel, Class: СGeoDistance
// Path: D:\MyProjects\Fuel\Fuel, Author: Ivanov
// Code lines: 31, Size of file: 1,13 KB
// Creation date: 30.01.2007 11:44
// Last modified: 05.04.2007 17:52
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
using SharedDLL;
//using BaseReports.GIS;
#endregion

namespace BaseReports.Procedure
{
  class Coord
  {
    public double Latitude;
    public double Longitude;
    public double path;
    public Coord(int Lat, int Lon)
    {
      Latitude = Lat / 6000000;
      Longitude = Lon / 6000000;
    }
    public Coord()
    {
      Latitude = 0;
      Longitude = 0;
      path = 0;
    }
    //public double Distance(Coord point)
    //{
    //  return RCS.Data.СGeoDistance.CalculateFromGrad(Latitude, Longitude, point.Latitude, point.Longitude);
    //}
    /// <summary>
    /// Add path
    /// </summary>
    /// <param name="point">Point</param>
    /// <returns>Double</returns>
    public double AddPath(double lat, double lon)
    {
      if (Latitude == 0 && Longitude == 0)
      {
        Latitude = lat;
        Longitude = lon;
        return 0;
      } // if
      double dPath = СGeoDistance.CalculateFromGrad(Latitude, Longitude, lat, lon);
      if (dPath < 150)
      {
        path += dPath;//СGeoDistance.CalculateFromGrad(Latitude, Longitude, lat, lon);
        Latitude = lat;
        Longitude = lon;
      } // if
      return path;
    } // AddPath(point)
  }



  class СGeoDistance
  {
    //Equatorial radius of the earth from WGS 84 
    //in meters, semi major axis = a
    private	static int a = 6378137;
    //flattening = 1/298.257223563 = 0.0033528106647474805
    //first eccentricity squared = e = (2-flattening)*flattening
    private	static double e = 0.0066943799901413165;
    //Miles to Meters conversion factor (take inverse for opposite)
    //private	static double milesToMeters = 1609.347;
    //Degrees to Radians converstion factor (take inverse for opposite)
    private	static double degreesToRadians = System.Math.PI/180;
    //расстояние в километрах
    /// <summary>
    /// Calculate from grad
    /// </summary>
    /// <param name="lat0">Lat 0</param>
    /// <param name="lon0">Lon 0</param>
    /// <param name="lat">Lat</param>
    /// <param name="lon">Lon</param>
    /// <returns>Double</returns>
    public static double CalculateFromGrad(double lat0, double lon0, double lat, double lon)
    {
      lat0 = lat0 * degreesToRadians;
      lon0 = lon0 * degreesToRadians;
      lat = lat * degreesToRadians;
      lon = lon * degreesToRadians;

      return CalculateFromRadian(lat0, lon0, lat, lon);

    } // CalculateFromGrad(lat0, lon0, lat)

    public static double dLat(RealPoint p0,RealPoint p1)
    {
    	double Rm = calcMeridionalRadiusOfCurvature(p0.Lat*degreesToRadians);
      return Rm * (p0.Lat*degreesToRadians - p1.Lat*degreesToRadians);
    }
    public static double dLon(RealPoint p0, RealPoint p1)
    {
      double Rpv = calcRoCinPrimeVertical(p0.Lat*degreesToRadians);
      return Rpv * Math.Cos(p0.Lat*degreesToRadians)*(p1.Lon*degreesToRadians-p0.Lon*degreesToRadians);
    }
    /// <summary>
    /// Calculate from radian
    /// </summary>
    /// <param name="lat0">Lat 0</param>
    /// <param name="lon0">Lon 0</param>
    /// <param name="lat">Lat</param>
    /// <param name="lon">Lon</param>
    /// <returns>Double</returns>
    public static double CalculateFromRadian(double lat0, double lon0, double lat, double lon)
    {
      	double Rm = calcMeridionalRadiusOfCurvature(lat0);
		
	      double Rpv = calcRoCinPrimeVertical(lat0); 
      	
	      double distance = Math.Sqrt(Math.Pow(Rm,2) * Math.Pow(lat-lat0,2) +
		      Math.Pow(Rpv,2) * Math.Pow(Math.Cos(lat0),2) * Math.Pow(lon-lon0,2));
          
	      return distance/1000;
    } // CalculateFromRadian(lat0, lon0, lat)
     /// <summary>
     /// Calc ro cin prime vertical
     /// </summary>
     /// <param name="lat0">Lat 0</param>
     /// <returns>Double</returns>
     private	static double calcRoCinPrimeVertical(double lat0)
     {
       	return a/Math.Sqrt(1 - e * Math.Pow(Math.Sin(lat0),2));
     } // calcRoCinPrimeVertical(lat0)
     
     /// <summary>
     /// Calc meridional radius of curvature
     /// </summary>
     /// <param name="lat0">Lat 0</param>
     /// <returns>Double</returns>
     private	static double calcMeridionalRadiusOfCurvature(double lat0)
     {
       	return	a*(1 - e)/Math.Pow(1 - e * (Math.Pow(Math.Sin(lat0),2)),1.5);
     } // calcMeridionalRadiusOfCurvature(lat0)
  }
}
