using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Charts.Native;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.GMap.MapProviders;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace BaseReports.Procedure
{
    public class Algorithm : IAlgorithm
    {
        public const int RFID_MISSING = 1023;
        public const int RFID_NOT_WORKING = 0;

        /// <summary>
        /// �������� ������� DRT
        /// </summary>
        protected string _namedrtsens;

        /// <summary>
        /// ����������� ����� �������� � ������ �����
        /// </summary>
        protected static double _minTimeMove;

        private static bool isProviderExcaption = false;

        /// <summary>
        /// ����������� ����� ����� ����� �������������� (����������/transition)
        /// </summary>
        protected static int _minimTimeSwitch;

        public static bool StopRun { get; set; }

        public static IGeoLocator GeoLocator { get; set; }

        public static IZonesLocator ZonesLocator { get; set; }

        public static bool IsSuppressReportsIndication;

        public string NameDrtSensor
        {
            get { return _namedrtsens; }
            set { _namedrtsens = value; }
        }

        public static void PlacemarkStatus()
        {
            try
            {
                if (GMapProviders.isActiveProvider())
                {
                    GMapProviders.GenerateProvider();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Placemark status", MessageBoxButtons.OK);
            }
        }

        public static string FindAddressLocation(PointLatLng point)
        {
            try
            {
                // ���� ��� �������
                if (isProviderExcaption)
                {
                    return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);
                }
                // ���� ������� ���������
                if (!GMapProviders.isActiveProvider() & null == GeoLocator)
                {
                    return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);
                }

                #region --   ��������� ��������� � ����   --

                if (null != ZonesLocator)
                {
                    StringBuilder builder = new StringBuilder();
                    if (RunFromModule)
                    {
                        foreach (IZone zone in ZonesLocator.GetZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    else
                    {
                        foreach (IZone zone in ZonesLocator.GetCheckedZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    if (builder.Length > 0)
                    {
                        builder.Length -= 2;
                        return builder.ToString();
                    }
                }

                #endregion

                #region --   ���������� �������� ���� GMapProvider   --
                GeoCoderStatusCode status = 0;
                Placemark place = GMapProviders.getActiveProvider(point, out status);

                if (place != null)
                {
                    if (status == GeoCoderStatusCode.G_GEO_SUCCESS)
                    {
                        if (place.Address.Length > 0)
                            return place.Address;
                    }
                }

                #endregion

                #region --   ����� ���������� ���-��������   --

                if (null != GeoLocator)
                {
                    return GeoLocator.GetLocationInfo(point);
                }

                #endregion
                #region --   � ������� ������, ������ ���������� ����������   --

                return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);

                #endregion
            }
            catch (Exception exception)
            {
                // � ������� ������, ������ ���������� ����������   --
                XtraMessageBox.Show(exception.Message + "\n" + exception.StackTrace, "Error find location", MessageBoxButtons.OK);
                isProviderExcaption = true;
                return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);
            }
        }

        public static string FindLocation(PointLatLng point)
        {
            try
            {
                #region --   ��������� ��������� � ����   --

                if (null != ZonesLocator)
                {
                    StringBuilder builder = new StringBuilder();
                    if (RunFromModule)
                    {
                        foreach (IZone zone in ZonesLocator.GetZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    else
                    {
                        foreach (IZone zone in ZonesLocator.GetCheckedZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    if (builder.Length > 0)
                    {
                        builder.Length -= 2;
                        return builder.ToString();
                    }
                }

                #endregion

                #region --   ���������� �������� ���� GMapProvider   --

                GeoCoderStatusCode status = 0;
                Placemark place = null;
                place = GMapProviders.getActiveProvider(point, out status);

                if (place != null)
                {
                    if (status == GeoCoderStatusCode.G_GEO_SUCCESS)
                    {
                        if (place.Address.Length > 0)
                            return place.Address;
                    }
                }

                #endregion
                    
                #region --   ����� ���������� ���-��������   --

                    if (null != GeoLocator)
                    {
                        return GeoLocator.GetLocationInfo(point);
                    }

                #endregion
                #region --   � ������� ������, ������ ���������� ����������   --

                return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);

                #endregion
            }
            catch (Exception exception)
            {
                // � ������� ������, ������ ���������� ����������   --
                XtraMessageBox.Show(exception.Message + "\n" + exception.StackTrace, "Error find location", MessageBoxButtons.OK);
                return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);
            }
        } // FindLocation

        public static string FindLocationWithCheckZoneIndication(PointLatLng point, out bool IsCheckZone)
        {
            try
            {
                #region --   ��������� ��������� � ����   --

                if (null != ZonesLocator)
                {
                    StringBuilder builder = new StringBuilder();
                    if (RunFromModule)
                    {
                        foreach (IZone zone in ZonesLocator.GetZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    else
                    {
                        foreach (IZone zone in ZonesLocator.GetCheckedZonesWithPoint(point))
                        {
                            builder.AppendFormat("{0}, ", zone.Name);
                        }
                    }
                    if (builder.Length > 0)
                    {
                        builder.Length -= 2;
                        IsCheckZone = true;
                        return builder.ToString();
                    }
                }

                #endregion

                IsCheckZone = false;

                #region --   ���������� �������� ���� GMapProvider   --

                GeoCoderStatusCode status = 0;
                Placemark place = null;
                place = GMapProviders.getActiveProvider(point, out status);

                if (place != null)
                {
                    if (status == GeoCoderStatusCode.G_GEO_SUCCESS)
                    {
                        if (place.Address.Length > 0)
                            return place.Address;
                    }
                }

                #endregion

                #region --   ����� ���������� ���-��������   --

                if (null != GeoLocator)
                {
                    return GeoLocator.GetLocationInfo(point);
                }

                #endregion

                #region --   � ������� ������, ������ ���������� ����������   --

                return String.Format("Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng);

                #endregion
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error find location", MessageBoxButtons.OK);
                IsCheckZone = false;
                return String.Format( "Lat={0:N5} Lng={1:N5}", point.Lat, point.Lng );
            }
        }

        public static ITreeModel<IZone> ZonesModel
        {
            get { return _zonesModel; }
            set { _zonesModel = value; }
        }

        private static ITreeModel<IZone> _zonesModel;

        public static IGraph Graph
        {
            get { return Algorithm.graph; }
            set { Algorithm.graph = value; }
        }

        private static IGraph graph;

        public static atlantaDataSet AtlantaDataSet
        {
            get; 
            set;
        }

        protected atlantaDataSet.mobitelsRow m_row;

        public static GpsData[] GpsDatas;

        private atlantaDataSet.sensorsRow[] s_rows;

        protected atlantaDataSet.sensorsRow[] S_rows
        {
            get { return s_rows; }
            set { s_rows = value; }
        }

        protected atlantaDataSet.sensorsRow s_row;
        protected List<atlantaDataSet.sensorsRow> s_rowListDut = new List<atlantaDataSet.sensorsRow>();

        public atlantaDataSet.sensorsRow S_row
        {
            get { return s_row; }
            set { s_row = value; }
        }

        protected atlantaDataSet.settingRow set_row;

        public static ReportPeriod Period
        {
            get { return _period; }
            set { _period = value; }
        }

        private static ReportPeriod _period;
        // ������ ������ RFID ��������
        public static Dictionary<int, List<DriverRFIDRecord>> DicRfidRecords;
        // ������ ������ RFID ��������
        public static Dictionary<int, List<AggregateRFIDRecord>> AggregateRfidRecords;

        // ������ ������ �������������� ��
        public static Dictionary<int, List<VehicleIDRecord>> DicIDRecords;

        public static event EventHandler Action;
        public static event ProgressChanged AlgorithmStarted = delegate { };
        public static event Action<Int32> ProgressChanged = delegate { };
        public static event MethodInvoker AlgorithmFinished = delegate { };

        //���������� ������ � ���������� ��� ������ � ����������� �� ������� �������
        public static bool RunFromModule;

        protected static void OnAction(object sender, EventArgs e)
        {
            if ((Action != null) && (!RunFromModule))
            {
                try
                {
                    Action(sender, e);
                }
                catch (Exception exception)
                {
                    // to do this
                }   
            }
        }

        /// <summary>
        /// ����� �������(��) � ����������� ����������
        /// </summary>
        /// <param name="ALG_TYPE ��������"></param>
        /// <returns>atlantaDataSet.sensorsRow </returns>
        public List<atlantaDataSet.sensorsRow> FindDrtSensors(AlgorithmType alg)
        {
            if (m_row == null)
                return null;
            atlantaDataSet.sensorsRow[] s_rows = m_row.GetsensorsRows();
            if (s_rows == null)
                return null;
            // ����� �������(��) � ����������� ����������
            List<atlantaDataSet.sensorsRow> lstDrtSens = new List<atlantaDataSet.sensorsRow>();
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                atlantaDataSet.relationalgorithmsRow[] r_rows = s_row.GetrelationalgorithmsRows();
                foreach (atlantaDataSet.relationalgorithmsRow r_row in r_rows)
                {
                    if (r_row.AlgorithmID == (int)alg)
                    {
                        lstDrtSens.Add(s_row);
                    }
                }
            }

            return lstDrtSens;
        }

        /// <summary>
        /// ����� ������� � ����������� ����������
        /// </summary>
        /// <param name="ALG_TYPE ��������"></param>
        /// <returns>atlantaDataSet.sensorsRow </returns>
        public atlantaDataSet.sensorsRow FindSensor(AlgorithmType alg)
        {
            if (m_row == null) 
                return null;
            atlantaDataSet.sensorsRow[] s_rows = m_row.GetsensorsRows();
            if (s_rows == null) 
                return null;
            // ����� ������� � ����������� ����������
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                atlantaDataSet.relationalgorithmsRow[] r_rows = s_row.GetrelationalgorithmsRows();
                foreach (atlantaDataSet.relationalgorithmsRow r_row in r_rows)
                {
                    if (r_row.AlgorithmID == (int) alg)
                    {
                        this.s_row = s_row;
                        return s_row;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// ���������� ������ ��������
        /// </summary>
        /// <param name="ALG_TYPE ��������"></param>
        /// <returns>atlantaDataSet.sensorsRow </returns>
        public void SetterSensor(atlantaDataSet.sensorsRow sensorsRow)
        {
            this.s_row = sensorsRow;
        }

        /// <summary>
        /// ����� �������� ��� �����������������
        /// </summary>
        /// <param name="ALG_TYPE ��������"></param>
        /// <returns>atlantaDataSet.sensorsRow </returns>
        public List<atlantaDataSet.sensorsRow> FindSensorsDutos(AlgorithmType alg)
        {
            if (m_row == null) 
                return null;

            List<atlantaDataSet.sensorsRow> lsiDut = new List<atlantaDataSet.sensorsRow>();

            FindSensorsDUT(lsiDut, alg);

            return lsiDut;
        }

        protected internal atlantaDataSet.sensorsRow[] FindSensors(AlgorithmType alg)
        {
            List<atlantaDataSet.sensorsRow> s_rows = new List<atlantaDataSet.sensorsRow>();

            if( AlgorithmType.FUEL_AGREGAT_ALL == alg)
            {
                if (m_row != null)
                {
                    foreach (atlantaDataSet.sensorsRow s_row in m_row.GetsensorsRows())
                    {
                        //atlantaDataSet.relationalgorithmsRow[] r_rows = s_row.GetrelationalgorithmsRows();
                        foreach (atlantaDataSet.relationalgorithmsRow r_row in s_row.GetrelationalgorithmsRows())
                        {
                            if (r_row.AlgorithmID == (int)AlgorithmType.FUEL1_AGREGAT)
                            {
                                s_row.AlgorithmID = r_row.AlgorithmID;
                                s_rows.Add(s_row);
                            }

                            if (r_row.AlgorithmID == (int)AlgorithmType.FUEL2_AGREGAT)
                            {
                                s_row.AlgorithmID = r_row.AlgorithmID;
                                s_rows.Add(s_row);
                            }

                            if (r_row.AlgorithmID == (int)AlgorithmType.FUEL3_AGREGAT)
                            {
                                s_row.AlgorithmID = r_row.AlgorithmID;
                                s_rows.Add(s_row);
                            }
                        }
                    }
                }

                return s_rows.ToArray();
            }

            if (m_row != null)
            {
                foreach (atlantaDataSet.sensorsRow s_row in m_row.GetsensorsRows())
                {
                    //atlantaDataSet.relationalgorithmsRow[] r_rows = s_row.GetrelationalgorithmsRows();
                    foreach (atlantaDataSet.relationalgorithmsRow r_row in s_row.GetrelationalgorithmsRows())
                    {
                        if (r_row.AlgorithmID == (int) alg)
                        {
                            s_row.AlgorithmID = r_row.AlgorithmID;
                            s_rows.Add(s_row);
                        }
                    }
                }
            }

            return s_rows.ToArray();
        
        } // FindSensors

        /// <summary>
        /// ����� ���� �������� ��� �������� ��������� (DRT).
        /// </summary>
        protected internal void FindSensorsDRT( FuelRateDictionarys fuelDict, AlgorithmType alg )
        {
            foreach( atlantaDataSet.sensorsRow s_row in FindSensors( alg ) )
            {
                if( fuelDict.fuelRateSensor1 == null )
                {
                    fuelDict.fuelRateSensor1 = s_row;
                    fuelDict.TypeAlgorithm1 = (int) alg;
                }
                else if( fuelDict.fuelRateSensor2 == null )
                {
                    fuelDict.fuelRateSensor2 = s_row;
                    fuelDict.TypeAlgorithm2 = (int) alg;
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// ����� �������� �� ������ ���� (1-�� � 2-�� ... 10-��, ��� �������� ���������).
        /// </summary>
        protected internal void FindSensorsDUT( List<atlantaDataSet.sensorsRow> snsList, AlgorithmType alg )
        {
            foreach( atlantaDataSet.sensorsRow s_row in FindSensors( alg ) )
            {
                snsList.Add(s_row);
            }
        }

        /// <summary>
        /// ����������� ���������� ���������
        /// </summary>
        /// <returns>Calibrate ���������� ���������.</returns>
        protected internal Calibrate Calibration()
        {
            Calibrate calibrate = new Calibrate();
            atlantaDataSet.sensorcoefficientRow[] c_rows = s_row.GetsensorcoefficientRows();
            calibrate = new Calibrate();

            foreach (atlantaDataSet.sensorcoefficientRow c_row in c_rows)
            {
                Coefficient coefficient = new Coefficient();
                coefficient.User = c_row.UserValue;
                coefficient.K = c_row.K;
                coefficient.b = c_row.b;
                calibrate.Collection.Add(c_row.SensorValue, coefficient);
            }

            return calibrate;
        }

        protected static DateTime GetTime(System.Int64 id)
        {
            //return atlantaDataSet.dataview.FindByDataGps_ID(id).time;
            return GpsDatas.First(gps => gps.Id == id).Time;
        }

        protected static Spoint[] FindMin(DataRow[] rows, DataColumn valcol, DataColumn keycol, double pmin)
        {
            ArrayList points = new ArrayList();
            System.Int64 beginId = 0;
            bool start = false;
            //int cur_id=0;
            for (int i = 1; i < rows.Length; i++)
            {
                if (!start & (double) rows[i][valcol] <= pmin)
                {
                    start = true;
                    beginId = (System.Int64) rows[i - 1][keycol];
                }
                else if (start & (double) rows[i][valcol] > pmin)
                {
                    Spoint p = new Spoint(beginId, (System.Int64) rows[i][keycol]);
                    points.Add(p);
                    start = false;
                }
            }

            if (start)
            {
                DataRow row = rows[rows.Length - 1];
                Spoint p = new Spoint(beginId, (System.Int64) row[keycol]);
                points.Add(p);
                start = false;
            }

            return (Spoint[]) points.ToArray(typeof (Spoint));
        }

        protected static Spoint[] FindMin(Dictionary<Int64, double> gpsDataParam, double pmin)
        {

            ArrayList points = new ArrayList();
            System.Int64 beginId = 0;
            bool start = false;
            bool first = true;
            Int64 prevValue = 0;
            foreach (KeyValuePair<Int64, double> kvp in gpsDataParam)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    if (!start & kvp.Value <= pmin)
                    {
                        start = true;
                        beginId = prevValue;
                    }
                    else if (start & kvp.Value > pmin)
                    {
                        Spoint p = new Spoint(beginId, kvp.Key);
                        points.Add(p);
                        start = false;
                    }
                }
                prevValue = kvp.Key;
            }

            if (start)
            {
                Spoint p = new Spoint(beginId, prevValue);
                points.Add(p);
                start = false;
            }

            return (Spoint[])points.ToArray(typeof(Spoint));
            
        }

        /// <summary>
        /// Find maximum
        /// </summary>
        /// <param name="rows">Rows</param>
        /// <param name="valcol">Valcol</param>
        /// <param name="keycol">Keycol</param>
        /// <returns> Point[] </returns>
        protected static Spoint[] FindMax(DataRow[] rows, DataColumn valcol, DataColumn keycol, double pmax)
        {
            ArrayList points = new ArrayList();
            System.Int64 begin_id = 0;
            bool start = false;
            foreach (DataRow row in rows)
            {
                if (!start & (double) row[valcol] > pmax)
                {
                    start = true;
                    begin_id = (System.Int64) row[keycol];
                } // if
                else if (start & (double) row[valcol] <= pmax)
                {
                    Spoint p = new Spoint(begin_id, (System.Int64) row[keycol]);
                    points.Add(p);
                    start = false;
                }
            }
            if (start)
            {
                DataRow row = rows[rows.Length - 1];
                Spoint p = new Spoint(begin_id, (System.Int64) row[keycol]);
                points.Add(p);
                start = false;
            }

            return (Spoint[]) points.ToArray(typeof (Spoint));
        }

        protected static Spoint[] FindMax(Dictionary<Int64,double > gpsDataParam , double pmax)
        {
            ArrayList points = new ArrayList();
            Int64 beginId = 0;
            bool start = false;
            Int64 prevValue = 0;

            foreach (KeyValuePair<Int64, double> kvp in gpsDataParam)
            {
                if (!start & kvp.Value > pmax)
                {
                    start = true;
                    beginId = kvp.Key;
                } // if
                else if (start & kvp.Value <= pmax)
                {
                    Spoint p = new Spoint(beginId, kvp.Key);
                    points.Add(p);
                    start = false;
                }
                prevValue = kvp.Key;
            }
            if (start)
            {
                Spoint p = new Spoint(beginId, prevValue);
                points.Add(p);
                start = false; 
            }

            return (Spoint[])points.ToArray(typeof(Spoint));
        }

        /// <summary>
        /// �������� ����������� ����� ����������� ������
        /// </summary>
        /// <param name="reportName">������������ ������</param>
        protected virtual void BeforeReportFilling(string reportName)
        {
            BeforeReportFilling(reportName, 0);
        }

        /// <summary>
        /// �������� ����������� ����� ����������� ������
        /// </summary>
        /// <param name="name">������������ ������</param>
        /// <param name="maximum">������������ �������� ��������-����</param>
        protected virtual void BeforeReportFilling(string name, int maximum)
        {
            if (!IsSuppressReportsIndication) 
                AlgorithmStarted(String.Format(Resources.BuildingReportMessage, name), vehicle.Info, maximum);

            Application.DoEvents();
        }

        /// <summary>
        /// ��������� ��������-������
        /// </summary>
        /// <param name="value">�������� ��� ProgressBar.EditValue</param>
        protected virtual void ReportProgressChanged(int value)
        {
            if (!IsSuppressReportsIndication) ProgressChanged(value);
            Application.DoEvents();
        }

        /// <summary>
        /// �������� ����������� ����� ���������� ������
        /// </summary>
        protected virtual void AfterReportFilling()
        {
           if(!IsSuppressReportsIndication) 
               AlgorithmFinished();
        }

        #region IAlgorithm Members

        private VehicleInfo vehicle;

        /// <summary>
        /// �������� ������������� ��������
        /// </summary>
        public VehicleInfo Vehicle
        {
            get { return vehicle; }
        }

        public static ushort Rfid_New_Missing { get; set; }

        public virtual void Run()
        {
            return;
        }

        public virtual void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row == null) 
                return;

            this.m_row = m_row;
            vehicle = new VehicleInfo(m_row);
            atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();

            set_row = (vehicles == null || vehicles.Length == 0)
                          ? null
                          : AtlantaDataSet.setting.FindByid(vehicles[0].setting_id);

            GMapProviders.SetStartParams();

            if(!RunFromModule) 
                GpsDatas = DataSetManager.GetDataGpsArray(m_row);
        }

        public virtual void SelectItem(int mobitelId, List<GpsData> gpsData)
        {
            this.m_row = (atlantaDataSet.mobitelsRow)AtlantaDataSet.mobitels.FindByMobitel_ID(mobitelId);
            if (m_row == null) return;
            vehicle = new VehicleInfo(m_row);
            atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();
            set_row = (vehicles == null || vehicles.Length == 0)
                          ? null
                          : AtlantaDataSet.setting.FindByid(vehicles[0].setting_id);
            GMapProviders.SetStartParams();
            GpsDatas = gpsData.ToArray();
        }

        #endregion

        public static void GetPointsInsided(long initialPointId, long finalPointId, List<GpsData> data)
        {
            if (Algorithm.GpsDatas == null) return;
            bool inside = false;
            List<GpsData> gpsDatasOrdByTime = Algorithm.GpsDatas.OrderBy(gps => gps.Time).ToList();

            foreach (GpsData gpsData in gpsDatasOrdByTime)
            {
                if (gpsData.Id == initialPointId)
                {
                    inside = true;
                }

                if (inside)
                {
                    data.Add(gpsData);
                }

                if (gpsData.Id == finalPointId)
                {
                    break;
                }
            } // foreach
        }

        public static void GetPointsInsided(long initialPointId,long finalPointId, List<IGeoPoint> data)
        {
            if (Algorithm.GpsDatas == null) return;
            bool inside = false;
            //foreach (atlantaDataSet.dataviewRow drow in ReportTabControl.Dataset.dataview.Select(
            //    "Mobitel_ID=" + row.MobitelId, "time ASC"))
            

            List<GpsData> gpsDatasOrdByTime = Algorithm.GpsDatas.OrderBy(gps => gps.Time).ToList();

            foreach (GpsData gpsData in gpsDatasOrdByTime)
            {
                if (gpsData.Id == initialPointId)
                {
                    inside = true;
                }

                if (inside)
                {
                    data.Add(gpsData);
                }

                if (gpsData.Id == finalPointId)
                {
                    break;
                }
            } // foreach
        }

        public static void GetPointsExtractedBetwDates(DateTime EventDate, DateTime EventDateEnd, List<IGeoPoint> data)
        {
            List<GpsData> gpsDatasOrdByTime = Algorithm.GpsDatas.OrderBy(gps => gps.Time).ToList();

            foreach (GpsData gpsData in gpsDatasOrdByTime)
            {
                if (gpsData.Time >= EventDate && gpsData.Time <= EventDateEnd)
                {
                    data.Add(gpsData);
                }
                if (gpsData.Time > EventDateEnd) break;
            } // foreach
        }

        public static void SetEventAlgoritmStarted(string before, string after, int value)
        {
            AlgorithmStarted(before, after, value);
        }
        public static void SetEventAlgoritmFinished()
        {
            AlgorithmFinished();
        }
        public static void SetEventAlgoritmProgressChanged(int value)
        {
            ProgressChanged(value);
        }
    }
}
