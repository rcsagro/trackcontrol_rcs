using System;
using System.Linq; 
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    /// <summary>
    /// ����� ��� ���������� ������ ��� ������ �� �������� ���������
    /// ������������� ��������.
    /// </summary>
    public class Rotation : Algorithm, IAlgorithm
    {
        /// <summary>
        /// ��������� ����� ��������� ���������� ������ ���������
        /// </summary>
        private int[] ROTATION_BAND =
        {
            200, 400, 600, 800, 1000,
            1200, 1400, 1600, 1800, 2000,
            2200, 2400, 2600, 2800, 3000,
            3200, 3400, 3600, 3800, 4000,
            4200, 4400, 4600, 4800, 5000,
            5200, 5400, 5600, 5800, 6000,
            6200, 6400, 6600, 6800, 7000, 100000
        };

        #region Fields

        /// <summary>
        /// �������� ������ ������ �� �������� ���������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        private Summary _summary;

        /// <summary>
        /// �������� ������ �� ��������� ��������� �������������
        /// �������� (����������������� ������ ��������� � ������������
        /// ��������� ������).
        /// </summary>
        private SortedDictionary<int, TimeSpan_Double> _motoTimeBand;

        #endregion

        #region IAlgorithm Members

        /// <summary>
        /// ����������� ��������� ��������� ����������� ���������, ��������� �����
        /// � ��������� ������� ��������������� ������� � ��������.
        /// </summary>
        public override void Run()
        {
            try
            {
                atlantaDataSet.RotateReportRow[] rt_rows = (atlantaDataSet.RotateReportRow[])
                    AtlantaDataSet.RotateReport.Select("MobitelId=" + m_row.Mobitel_ID);

                if (rt_rows.Length != 0)
                {
                    return;
                }

                atlantaDataSet.TachometerValueRow[] rows = (atlantaDataSet.TachometerValueRow[])
                    AtlantaDataSet.TachometerValue.Select("MobitelId=" + m_row.Mobitel_ID);

                // ��������� ������� ������� ������ ������� ��������
                if (rows.Length == 0)
                {
                    return;
                }

                BeforeReportFilling(Resources.EngineRevolutions, rows.Length);

                _summary = new Summary();
                _motoTimeBand = new SortedDictionary<int, TimeSpan_Double>();
                GetBound();
                // �����, ���������������� ������� ������ � ������� ���������
                // ������������� ��������
                atlantaDataSet.TachometerValueRow tempPoint = rows[0];

                // ����� ������� ������������� �������� � ���������� ����������
                // �� ������� ���������
                TimeSpan breakTime = new TimeSpan();

                // ����� ������ ������� ������������� �������� � ���������� ����������
                DateTime breakBegin = new DateTime();

                // ����� ������ ����������� ��������� (��� ������������ ���������)
                DateTime lastTime = new DateTime();

                if (GpsDatas.First(gps => gps.Id == rows[0].Id) != null) // (rows[0].dataviewRow != null)
                    lastTime = GpsDatas.First(gps => gps.Id == rows[0].Id).Time; // rows[0].dataviewRow.time;

                // ���� ��������� ������ ���������
                bool isEngineON = rows[0].Value > 0;
                bool isEngineONprev = isEngineON;

                // ���� ��������� �������� ������������� ��������
                bool isMoving = false;

                if (GpsDatas.First(gps => gps.Id == rows[0].Id) != null) // (rows[0].dataviewRow != null)
                    isMoving = GpsDatas.First(gps => gps.Id == rows[0].Id).Speed > 0;

                if (isEngineON && !isMoving)
                    breakBegin = GpsDatas.First(gps => gps.Id == rows[0].Id).Time;

                int counter = 0;
                int k = 0;
                foreach (var rws in rows) // ���������� ����������� ������ �������
                {
                    //for (int i = 0; i < GpsDatas.Length; i++)
                    {
                        //if (rws.Id == GpsDatas[i].Id)
                        {
                            rws.Distance = GpsDatas[k].Dist;
                            rws.Speed = GpsDatas[k].Speed;
                            rws.Time = GpsDatas[k].Time;
                            rws.LatLng = GpsDatas[k].LatLng;
                            //break;
                        }
                    }
                    k++;
                } // foreach

                for (int i = 1; i < rows.Length; i++) // optimization
                {
                    try
                    {
                        // ����������� ����� ������� ������������� �������� � ����������
                        // ����������.
                        if (isEngineON && isMoving && rows[i].Speed == 0 /*GpsDatas.First(gps => gps.Id == rows[i].Id).Speed == 0*/) // optimization
                        {
                            breakBegin = rows[i].Time /*GpsDatas.First(gps => gps.Id == rows[i].Id).Time*/; // optimization
                            isMoving = false;
                        }
                        else if (isEngineON && !isMoving && rows[i].Speed > 0 /*GpsDatas.First(gps => gps.Id == rows[i].Id).Speed > 0*/) // optimization
                        {
                            breakTime += rows[i].Time /*GpsDatas.First(gps => gps.Id == rows[i].Id).Time*/ - breakBegin; // optimization

                            isMoving = true;
                        }

                        // ����������� ��������� ��������� (��� ������� �������� �������, 
                        // ��� ��������� �� ��������).
                        if (!isEngineON && rows[i].Value > 0)
                        {
                            addEngineOffToReport(tempPoint, rows[i]); // optimization

                            isEngineON = true;

                            tempPoint = rows[i];

                            if (!isMoving)
                            {
                                breakBegin = rows[i].Time /*GpsDatas.First(gps => gps.Id == rows[i].Id).Time*/; // optimization
                            }
                        }
                        else if (isEngineON && rows[i].Value == 0)
                        {
                            if (!isMoving)
                            {
                                breakTime += rows[i].Time /*GpsDatas.First(gps => gps.Id == rows[i].Id).Time*/ - breakBegin; // optimization
                            }

                            addEngineOnToReport(tempPoint, rows[i], breakTime); // optimization

                            isEngineON = false;

                            breakTime = new TimeSpan();
                            tempPoint = rows[i];
                        }

                        // ����������� ��������, � ����� ����� ����� ������ � ������� ���������
                        if (isEngineONprev)
                            updateMotoTimeBand(rows[i], lastTime); // optimization

                        lastTime = rows[i].Time /*GpsDatas.First(gps => gps.Id == rows[i].Id).Time*/; // optimization

                        isEngineONprev = isEngineON;

                        if (counter++ > (rows.Length / 100)) 
                        {
                            counter = 0;
                            ReportProgressChanged(i);
                        }
                    }
                    catch (Exception ex)
                    {
                        break;
                    }
                } // for

                // ������������ ������� ����� ������� ��������� ������� ��������
                // �� ��������� ������ �������
                if (isEngineON)
                {
                    if (!isMoving)
                    {
                        breakTime += GpsDatas.First(gps => gps.Id == rows[rows.Length - 1].Id).Time - breakBegin;
                        //rows[rows.Length - 1].dataviewRow.time - breakBegin
                    }
                    addEngineOnToReport(tempPoint, rows[rows.Length - 1], breakTime);
                }
                else
                {
                    addEngineOffToReport(tempPoint, rows[rows.Length - 1]);
                }

                AfterReportFilling();
            }
            catch (Exception ex0)
            {
                XtraMessageBox.Show(ex0.Message + "\n" + ex0.StackTrace, "Error Rotation", MessageBoxButtons.OK);
            }
        } // Run()

        /// <summary>
        /// ����������� ������ ��������� � ��������� ��������������� ������� �
        /// �������� ������������� ����������� ������� �������� ���������.
        /// </summary>
        /// <param name="m_row">�� ����, ��������</param>
        public override void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);

            if (AtlantaDataSet.TachometerValue.Select(String.Format("MobitelId={0}", m_row.Mobitel_ID)).Length != 0)
            {
                return;
            }

            // ���������� ����� �������.
            atlantaDataSet.sensorsRow sensor = FindSensor(AlgorithmType.ROTATE_E);

            if (sensor == null)
                return;

            Calibrate calibrate = Calibration();
            foreach (GpsData d in GpsDatas)
            {
                atlantaDataSet.TachometerValueRow rotateRow = AtlantaDataSet.TachometerValue.NewTachometerValueRow();
                rotateRow.Id = d.Id;
                rotateRow.MobitelId = m_row.Mobitel_ID;
                rotateRow.SensorId = sensor.id;
                //������������� ��� �������� ���������� c CAN //����� � ���, ��� ������ ������ �� ���������� �� sensor.K ���
                //������� ������� ��������� (��� �� �������������� ��� ��������, ���� � �����), � ��� ����� ������ ���������.
                rotateRow.Value = calibrate.GetUserValue(d.Sensors, sensor.Length, sensor.StartBit, 1, sensor.B, sensor.S); //sensor.K);

                if (sensor.Length != 1 && rotateRow.Value == ((1 << sensor.Length) - 1))
                    rotateRow.Value = 0;
                else
                    rotateRow.Value *= sensor.K;
                //
                AtlantaDataSet.TachometerValue.AddTachometerValueRow(rotateRow);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// �������� ����������� ����� ��������� ������������ ������
        /// � ���������� ������� ��������� ������ ���������.
        /// </summary>
        protected override void AfterReportFilling()
        {
            base.AfterReportFilling();

            System.Windows.Forms.Application.DoEvents();
            Algorithm.OnAction(this, new RotationEventArgs(m_row.Mobitel_ID, _summary, _motoTimeBand));
        }

        /// <summary>
        /// ��������� ������ � ������� �������� � ������� � ������� �������������
        /// �������� � ���������� ����������.
        /// </summary>
        /// <param name="initialPoint">��������� ����� �������, ����� ��������� �������</param>
        /// <param name="finalPoint">�������� ����� �������, ����� ��������� �������</param>
        /// <param name="breakTime">����������������� ������� � �������� ������� �������, �����
        /// ������������ �������� �� ���������</param>
        private void addEngineOnToReport(
            atlantaDataSet.TachometerValueRow initialPoint,
            atlantaDataSet.TachometerValueRow finalPoint,
            TimeSpan breakTime)
        {
            atlantaDataSet.RotateReportRow row = AtlantaDataSet.RotateReport.NewRotateReportRow();
            row.MobitelId = m_row.Mobitel_ID;
            row.State = Resources.Working;

            row.InitialTime = initialPoint.Time /*GpsDatas.First(gps => gps.Id == initialPoint.Id).Time*/; // optimization

            row.InitialPointId = initialPoint.Id;

            row.FinalTime = finalPoint.Time /*GpsDatas.First(gps => gps.Id == finalPoint.Id).Time*/; // optimization

            row.FinalPointId = finalPoint.Id;
            row.Interval = row.FinalTime - row.InitialTime;
            row.BreakTime = breakTime;
            _summary.EngineOnTime += row.Interval;

            AtlantaDataSet.RotateReport.AddRotateReportRow(row);
        }

        /// <summary>
        /// ��������� ������ � ������� �������� � ������� � ������� �������������
        /// �������� � ����������� ����������.
        /// </summary>
        /// <param name="initialPoint">��������� ����� �������, ����� ��������� ��������</param>
        /// <param name="finalPoint">�������� ����� �������, ����� ��������� ��������</param>
        private void addEngineOffToReport(atlantaDataSet.TachometerValueRow initialPoint,
            atlantaDataSet.TachometerValueRow finalPoint)
        {
            atlantaDataSet.RotateReportRow row = AtlantaDataSet.RotateReport.NewRotateReportRow();
            row.MobitelId = m_row.Mobitel_ID;
            row.State = Resources.IsntWorking;

            //GpsData initialData = GpsDatas.First(gps => gps.Id == initialPoint.Id); // optimization

            row.Location = FindLocation((PointLatLng)initialPoint.LatLng) /*FindLocation(initialData.LatLng)*/; // optimization ?????

            row.InitialTime = initialPoint.Time /*initialData.Time*/;
            row.InitialPointId = initialPoint.Id /*initialData.Id*/;

            row.FinalTime = finalPoint.Time /*GpsDatas.First(gps => gps.Id == finalPoint.Id).Time*/; // optimization

            row.FinalPointId = finalPoint.Id;
            row.Interval = row.FinalTime - row.InitialTime;

            _summary.EngineOffTime += row.Interval;

            AtlantaDataSet.RotateReport.AddRotateReportRow(row);
        }

        /// <summary>
        /// ��������� ���������� � ����������� �� ���������
        /// </summary>
        /// <param name="row">������� ��������� ������� �������� ���������</param>
        /// <param name="prewTime">����� ������ ����������� ���������</param>
        private void updateMotoTimeBand(atlantaDataSet.TachometerValueRow row, DateTime prewTime)
        {
            TimeSpan duration = row.Time /*GpsDatas.First(gps => gps.Id == row.Id).Time*/ - prewTime;
          
            foreach (BoundRow brow in _summary.Bound)
            {
                if (row.Value < brow.Bound)
                {
                    if (_motoTimeBand.ContainsKey(brow.Bound))
                    {
                        _motoTimeBand[brow.Bound].ts += duration;

                        _motoTimeBand[brow.Bound].db += row.Distance /*GpsDatas.First(gps => gps.Id == row.Id).Dist*/; // optimization
                    }
                    else
                    {
                        TimeSpan_Double tsd = new TimeSpan_Double(duration, row.Distance /*GpsDatas.First(gps => gps.Id == row.Id).Dist*/); // optimization

                        _motoTimeBand.Add(brow.Bound, tsd);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// �������� ��������  - ��� ��������� ��� 
        /// ����������� ��������� ��� ������ �� ���������
        /// </summary>
        private void GetBound()
        {
            //_summary.Bound = new List<BoundRow>();
            _summary.Bound = MobitelsRotateBandsProvider.SelectRecords(m_row.Mobitel_ID);
            if (_summary.Bound.Count == 0)
            {
                for (int i = 0; i < ROTATION_BAND.Length; i++)
                {
                    BoundRow sc = new BoundRow(0, ROTATION_BAND[i]);
                    _summary.Bound.Add(sc);
                }
                _summary.IsBoundsDefault = true;
            }
            else
                _summary.IsBoundsDefault = false;
        }

        #endregion

        #region Nested Types

        /// <summary>
        /// ������������ �������� ������ ������ �� �������� ���������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        public struct Summary
        {
            /// <summary>
            /// ����� ����� � ���������� ����������
            /// </summary>
            public TimeSpan EngineOnTime;

            /// <summary>
            /// ����� ����� � ����������� ����������
            /// </summary>
            public TimeSpan EngineOffTime;

            /// <summary>
            /// ����� ��������
            /// </summary>
            public List<BoundRow> Bound;

            /// <summary>
            /// ������������ �������� �� ���������
            /// </summary>
            /// <returns></returns>
            public bool IsBoundsDefault;
        }

        #endregion
    }

    public class BoundRow
    {
        public int Bound { get; set; }
        public int Id { get; set; }

        public BoundRow(int id, int bound)
        {
            Bound = bound;
            Id = id;
        }

        public BoundRow()
        {

        }
    }

    /// <summary>
    /// �����, ���������� ���������, ������� ����� �������� � ����������
    /// ������� �� ��������� ������ ��������� ���������� ������ ��� ������
    /// �� �������� ��������� ����������� ������������� ��������.
    /// </summary>
    public class RotationEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// ID ���������
        /// </summary>
        private int _id;

        /// <summary>
        /// �������� ������ ������ �� �������� ��������� �� �����������
        /// ��������� (������������� ��������). 
        /// </summary>
        private Rotation.Summary _summary;

        /// <summary>
        /// �������� ������ �� ��������� ��������� �������������
        /// �������� (����������������� ������ ��������� � ������������
        /// ��������� ������).
        /// </summary>
        private SortedDictionary<int, TimeSpan_Double> _motoTimeBand = new SortedDictionary<int, TimeSpan_Double>();

        #endregion

        #region .ctor

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public RotationEventArgs(int id, Rotation.Summary summary, SortedDictionary<int, TimeSpan_Double> motoTimeBand)
        {
            _id = id;
            _summary = summary;
            _motoTimeBand = motoTimeBand;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// �������� ������ ������ �� �������� ��������� �� �����������
        /// ��������� (������������� ��������).
        /// </summary>
        public Rotation.Summary Summary
        {
            get { return _summary; }
        }

        /// <summary>
        /// �������� ������ �� ��������� ��������� �������������
        /// �������� (����������������� ������ ��������� � ������������
        /// ��������� ������).
        /// </summary>
        public SortedDictionary<int, TimeSpan_Double> MotoTimeBand
        {
            get { return _motoTimeBand; }
        }

        #endregion
    }
}
