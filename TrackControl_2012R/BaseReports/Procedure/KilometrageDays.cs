using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LocalCache;                                                                               
using TrackControl.General;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    public class KilometrageDays : Kilometrage
    {
        /// <summary>
        /// �������� ������ ������ � ������� � ����������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        private SummaryDay _summaryDay;

        //������ �� ��������:
        //1 ���� ������ ������ �� �� ���� ���� �� ����������� ��������� ������:
        //����� �� ������� (���� ���� ��� ���� ������) �������� ����� �� ������� ���� ������� �������������
        //������ ��� ���������� ��������� ����� ��������� � ��������� ��������� ���
        private DateTime dtMarkerReport = new DateTime();
        private DateTime dtEndReport = new DateTime();
        private System.Int64 FinalPointId = 0;
        //����� ����� �������� �� ����
        protected TimeSpan tsDayMove;
        //protected int row_start_day = -1;
        protected int ikm_rows = 0;
        private double normWays = 0.0;
        private double normMotorHr = 0.0;

        public override void Run()
        {
            try
            {
                atlantaDataSet _dataset = ReportTabControl.Dataset;
                //_dataset.KilometrageReport.Clear();
                //if (_dataset.KilometrageReport.Count > 0) // aketner-18.09.2012
                //{
                //    string s = "Kilometrage Report is Initialized";
                //    //return;
                //}

                atlantaDataSet.KilometrageReportDayRow[] km_Day_rows = (atlantaDataSet.KilometrageReportDayRow[])
                                                                       AtlantaDataSet.KilometrageReportDay.Select(
                                                                           "MobitelId = " + m_row.Mobitel_ID);

                if (km_Day_rows.Length > 0)
                {
                    return;
                }

                _summaryDay = new SummaryDay();
                //atlantaDataSet.KilometrageReportDay.Clear();
                //atlantaDataSet.KilometrageReport.Clear();

                base.Run();

                //----------aketner 30.05.2013 -------------------------
                // �������� ������� ��������� ����������

                atlantaDataSet.vehicleRow[] mrow = m_row.GetvehicleRows();
                if (mrow.Length > 0)
                {
                    if (mrow[0]["Team_id"] != DBNull.Value)
                    {
                        if (mrow[0].FuelWayLiter == 0.0 && mrow[0].FuelMotorLiter == 0.0)
                        {
                            foreach (atlantaDataSet.teamRow teamRow in Algorithm.AtlantaDataSet.team)
                            {
                                if (teamRow.id == mrow[0].Team_id)
                                {
                                    normWays = teamRow.FuelWayKmGrp;
                                    normMotorHr = teamRow.FuelMotoHrGrp;
                                    break;
                                } // if
                            } // foreach
                        } // if
                        else
                        {
                            normWays = mrow[0].FuelWayLiter;
                            normMotorHr = mrow[0].FuelMotorLiter;
                        }
                    } // if
                    else
                    {
                        normWays = mrow[0].FuelWayLiter;
                        normMotorHr = mrow[0].FuelMotorLiter;
                    }
                } // if
                //-----------------------------------------------------------------

                //DataRow[] as_rows = atlantaDataSet.KilometrageReport.Select();
                atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])
                                                                AtlantaDataSet.KilometrageReport.Select("MobitelId = " +
                                                                                                        m_row.Mobitel_ID);

                if (0 == km_rows.Length)
                {
                    return;
                }

                ikm_rows = km_rows.Length;
                BeforeReportFilling(Resources.KilometrageDay, ikm_rows);

                // aketner - 18.06.2013===========================================
                if( FormInterval.isActiveMonth() || FormInterval.isActiveWeek() || FormInterval.isActiveTime() || FormInterval.isActivateNewAlgorythm )
                {
                    List<int> move_list = new List<int>();
                    List<List<int>> mvList = new List<List<int>>();
                    List<int> park_list = new List<int>();
                    List<List<int>> prkList = new List<List<int>>();

                    // �������� ���������� � �������� � ��������� �� ������� �������
                    DateTime currentTime = km_rows[0].InitialTime;
                    if (km_rows[0].State == Resources.Movement)
                    {
                        move_list.Add(0);
                    }
                    else if (km_rows[0].State == Resources.Parking)
                    {
                        park_list.Add(0);
                    }

                    for (int k = 1; k < km_rows.Length; k++)
                    {
                        if (km_rows[k].InitialTime.Day == currentTime.Day)
                        {
                            if (km_rows[k].State == Resources.Movement)
                            {
                                move_list.Add(k);
                            }
                            else if (km_rows[k].State == Resources.Parking)
                            {
                                park_list.Add(k);
                            }
                        } // if
                        else
                        {
                            mvList.Add(move_list);
                            prkList.Add(park_list);

                            currentTime = km_rows[k].InitialTime;

                            park_list = new List<int>();
                            move_list = new List<int>();

                            if (km_rows[k].State == Resources.Movement)
                            {
                                move_list.Add(k);
                            }
                            else if (km_rows[k].State == Resources.Parking)
                            {
                                park_list.Add(k);
                            }
                        } // else
                    } // for

                    mvList.Add(move_list);
                    prkList.Add(park_list);

                    // ������������ ���������� � �������� � ��������
                    if (mvList.Count != prkList.Count)
                        return;

                    for (int i = 0; i < mvList.Count; i++)
                    {
                        DateTime initMove = new DateTime();
                        DateTime finalMove = new DateTime();
                        Int64 initMoveId = 0;
                        Int64 finalMoveId = 0;
                        string localStart = "";
                        string localEnd = "";
                        TimeSpan summaMove = new TimeSpan(0, 0, 0);
                        double distance_move = 0.0;
                        double avrgSpeed = 0.0;
                        double maxiSpeed = 0.0;
                        TimeSpan parkingTime = new TimeSpan(0, 0, 0);
                        DateTime initPark = new DateTime();
                        Int64 initParkId = 0;
                        DateTime finalPark = new DateTime();
                        Int64 finalParkId = 0;
                        DateTime initialDatePoint = new DateTime();
                        DateTime finaliseDatePoint = new DateTime();
                        TimeSpan intervalWorking = new TimeSpan(0, 0, 0);
                        DateTime finalTimePoint = new DateTime();
                        long initialPointID = 0;
                        long finalizePointID = 0;

                        List<int> move = mvList[i];

                        if (move.Count > 0)
                        {
                            // � ������� ���������� ������� �������� ����
                            initMove = km_rows[move[0]].InitialTime; // ������ ��������
                            initialDatePoint = initMove;
                            initMoveId = km_rows[move[0]].InitialPointId;
                            initialPointID = initMoveId;
                            finalMove = km_rows[move[move.Count - 1]].FinalTime; // ����� ��������
                            finaliseDatePoint = finalMove;
                            finalTimePoint = finalMove;
                            finalMoveId = km_rows[move[move.Count - 1]].FinalPointId;
                            finalizePointID = finalMoveId;
                            intervalWorking = finalMove - initMove;

                            // ==============
                            int count = 0;
                            for (int k = 0; k < km_rows.Length; k++)
                            {
                                if (km_rows[k].FinalPointId == -1)
                                {
                                    count++;
                                }
                            }
                            //================

                            //localStart =
                            //    FindLocation(new PointLatLng(atlantaDataSet.dataview.FindByDataGps_ID(initMoveId).Lat,
                            //                                 atlantaDataSet.dataview.FindByDataGps_ID(initMoveId).Lon));
                            localStart = FindLocation(GpsDatas.First(gps => gps.Id == initMoveId).LatLng);
                            //localEnd =
                            //    FindLocation(new PointLatLng(atlantaDataSet.dataview.FindByDataGps_ID(finalMoveId).Lat,
                            //                                 atlantaDataSet.dataview.FindByDataGps_ID(finalMoveId).Lon));
                            localEnd = FindLocation(GpsDatas.First(gps => gps.Id == finalMoveId).LatLng);

                            summaMove = new TimeSpan(0, 0, 0);

                            // ��������� ����� ����� �������� �� ��������� ��������
                            for (int j = 0; j < move.Count; j++)
                            {
                                summaMove += (km_rows[move[j]].FinalTime - km_rows[move[j]].InitialTime);
                            }

                            distance_move = 0.0;

                            // ��������� ���������� ���� �� ����� �������� �� ��������� ��������� � ��
                            for (int j = 0; j < move.Count; j++)
                            {
                                distance_move += (km_rows[move[j]].Distance);
                            }

                            // ��������� ������� �������� �� ����� �������� � ��������� ��������� ���������
                            avrgSpeed = distance_move/summaMove.TotalSeconds*3600; // ��/�

                            maxiSpeed = 0;
                            // ������ ������������ �������� �� ����� �������� �� ��������� ��������� � ��/�
                            for (int j = 0; j < move.Count; j++)
                            {
                                if (maxiSpeed < km_rows[move[j]].MaxSpeed)
                                    maxiSpeed = km_rows[move[j]].MaxSpeed;
                                }

                                // ��������� ����� ������� � ������ ���������� ���������
                            List<int> parkin = prkList[i];

                            parkingTime = new TimeSpan(0, 0, 0);

                            for (int j = 0; j < parkin.Count; j++)
                            {
                                parkingTime += (km_rows[parkin[j]].FinalTime - km_rows[parkin[j]].InitialTime);
                            }
                        } // if
                        else
                        {
                            // � ������� ���������� ������� �������� �� ����
                            List<int> parki = prkList[i];
                            if (parki.Count > 0)
                            {
                                initPark = km_rows[parki[0]].InitialTime;
                                initialDatePoint = initPark;
                                initParkId = km_rows[parki[0]].InitialPointId;
                                initialPointID = initParkId;
                                finalPark = km_rows[parki[parki.Count - 1]].FinalTime;
                                finaliseDatePoint = initPark;
                                finalTimePoint = finalPark;
                                finalParkId = km_rows[parki[parki.Count - 1]].FinalPointId;
                                finalizePointID = finalParkId;

                                //localStart =
                                //    FindLocation(
                                //        new PointLatLng(atlantaDataSet.dataview.FindByDataGps_ID(initParkId).Lat,
                                //                        atlantaDataSet.dataview.FindByDataGps_ID(initParkId).Lon));
                                localStart = FindLocation(GpsDatas.First(gps => gps.Id == initParkId).LatLng);
                                //localEnd =
                                //    FindLocation(
                                //        new PointLatLng(atlantaDataSet.dataview.FindByDataGps_ID(finalParkId).Lat,
                                //                        atlantaDataSet.dataview.FindByDataGps_ID(finalParkId).Lon));
                                localEnd = FindLocation(GpsDatas.First(gps => gps.Id == finalParkId).LatLng);
                                parkingTime = finalPark - initPark;
                            } // if
                        } // else

                        atlantaDataSet.KilometrageReportDayRow parkingRow;
                        parkingRow = AtlantaDataSet.KilometrageReportDay.NewKilometrageReportDayRow();
                        parkingRow.MobitelId = m_row.Mobitel_ID;
                        parkingRow.InitialTime = initialDatePoint;
                        parkingRow.FinalTime = finaliseDatePoint;
                        parkingRow.InitialPointId = initialPointID;
                        parkingRow.FinalPointId = finalizePointID;
                        parkingRow.LocationStart = localStart;
                        parkingRow.LocationEnd = localEnd;
                        parkingRow.IntervalMove = Convert.ToString(summaMove);
                        parkingRow.TimeStopsTotal = Convert.ToString(parkingTime);
                        parkingRow.Distance = Math.Round(distance_move, 2);
                        parkingRow.AverageSpeed = Math.Round(avrgSpeed, 2);
                        if (avrgSpeed > maxiSpeed)
                        {
                            parkingRow.MaxSpeed = Math.Round(avrgSpeed, 2);
                            parkingRow.Hunter = 1;
                        }
                        else
                        {
                            parkingRow.MaxSpeed = Math.Round(maxiSpeed, 2);
                            parkingRow.Hunter = 0;
                        }

                        parkingRow.IntervalWork = Convert.ToString(intervalWorking);
                        parkingRow.NormaWay = Math.Round(parkingRow.Distance*this.normWays/100, 1);

                        //����� ���������� � ����������� ������� ���������
                        double hours = 0.0;

                        TimeSpan tsEngineOnTime = GetRotateOnInterval(initialDatePoint, finalTimePoint);

                        hours = (tsEngineOnTime.Seconds + tsEngineOnTime.Minutes*60 + tsEngineOnTime.Hours*3600)/3600.0;

                        if (tsEngineOnTime.Days < 1)
                        {
                            DateTime dT = new DateTime(2000, 1, 1, tsEngineOnTime.Hours, tsEngineOnTime.Minutes,
                                                       tsEngineOnTime.Seconds);
                            parkingRow.MotorHrIdle = dT.ToString("HH:mm");
                        }
                        else
                        {
                            DateTime dT = new DateTime(2000, 1, tsEngineOnTime.Days, tsEngineOnTime.Hours,
                                                       tsEngineOnTime.Minutes, tsEngineOnTime.Seconds);
                            parkingRow.MotorHrIdle = dT.ToString("dd.HH:mm");
                        }

                        parkingRow.NormaMotor = Math.Round(hours*this.normMotorHr, 1);
                        parkingRow.SummaNormaFuel = Math.Round(parkingRow.Distance*this.normWays/100, 1) +
                                                    Math.Round(hours*this.normMotorHr, 1);

                        AtlantaDataSet.KilometrageReportDay.AddKilometrageReportDayRow(parkingRow);

                        _summaryDay.TotalWay += distance_move;
                        _summaryDay.TotalTimeTour += intervalWorking;
                        _summaryDay.TotalTimeWay += summaMove;

                        ReportProgressChanged(i);
                    } // for

                    AfterReportFilling();

                    return;
                } // if
                //===================================================================

                dtMarkerReport = km_rows[0].InitialTime;
                dtEndReport = km_rows[km_rows.Length - 1].FinalTime;
                km_rows = (atlantaDataSet.KilometrageReportRow[]) AtlantaDataSet.KilometrageReport.Select(
                    String.Format("State = '{0}' AND MobitelId = {1}", Resources.Movement, m_row.Mobitel_ID),
                    "InitialTime");

                //�������� ������� �������
                if (0 == km_rows.Length)
                {
                    int iDays = ReportDaysBetweenDates(dtMarkerReport, dtEndReport);
                    for (int i = 0; i <= iDays; i++)
                    {
                        FillStopsOneDay(dtMarkerReport.AddDays(i));
                    }
                    return;
                }
                ikm_rows = km_rows.Length;

                BeforeReportFilling(Resources.KilometrageDay, ikm_rows);

                // ��������� ����� ���
                atlantaDataSet.KilometrageReportRow beginDayPoint = km_rows[0];
                atlantaDataSet.KilometrageReportRow endDayPoint = km_rows[0];
                // ������ �� ����
                //double distance = beginDayPoint.Distance;
                //TimeSpan tsDayMove = beginDayPoint.Interval;
                //DateTime dtBegin = beginDayPoint.InitialTime;
                //DateTime dtEnd = beginDayPoint.FinalTime;
                double distance = 0;
                TimeSpan tsDayMove = new TimeSpan();
                DateTime dtBegin = beginDayPoint.InitialTime;
                DateTime dtEnd = beginDayPoint.FinalTime;
                for (int i = 0; i < km_rows.Length; i++)
                {
                    if (dtBegin.Day != km_rows[i].InitialTime.Day)
                    {
                        addDayToReport(beginDayPoint, endDayPoint, dtBegin, dtEnd, tsDayMove, distance); //!
                        beginDayPoint = km_rows[i];
                        endDayPoint = km_rows[i];
                        dtBegin = beginDayPoint.InitialTime;
                        dtEnd = beginDayPoint.FinalTime;
                        if (dtBegin.Day != km_rows[i].FinalTime.Day)
                        {
                            distance = 0;
                            Split(ref km_rows[i], ref endDayPoint, ref beginDayPoint, ref distance, ref dtBegin,
                                  ref dtEnd,
                                  ref tsDayMove);
                        }
                        else
                        {
                            tsDayMove = endDayPoint.Interval;
                            distance = endDayPoint.Distance;
                        }
                    }
                    else
                        //����������� �������� ������������ ����� ������� ��� �� ��� ���
                        if (dtBegin.Day != km_rows[i].FinalTime.Day)
                        {
                            Split(ref km_rows[i], ref endDayPoint, ref beginDayPoint, ref distance, ref dtBegin,
                                  ref dtEnd,
                                  ref tsDayMove);
                        }
                        else
                        {
                            endDayPoint = km_rows[i];
                            dtEnd = endDayPoint.FinalTime;
                            distance += endDayPoint.Distance;
                            tsDayMove = tsDayMove.Add(endDayPoint.Interval);
                        }
                    ReportProgressChanged(i);
                }

                //��������� ����
                if ((distance > 0))
                {
                    addDayToReport(beginDayPoint, endDayPoint, dtBegin, dtEnd, tsDayMove, distance); // !!
                }
                //���������� ����������� ���� ���� ����� ��������
                //FillStops(dtMarkerReport, dtEndReport);
                int iDaysF = ReportDaysBetweenDates(dtMarkerReport, dtEndReport);
                for (int i = 1; i <= iDaysF; i++)
                {
                    FillStopsOneDay(dtMarkerReport.AddDays(i));
                }

                AfterReportFilling();
            }
            catch (Exception ex) // aketner 23.07.2013
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                AfterReportFilling();
                return;
            }
        } // run

        protected void addDayToReport( // ��������� ����� � ����� ����� ���� ��������
            atlantaDataSet.KilometrageReportRow initialDayPoint,
            atlantaDataSet.KilometrageReportRow finalDayPoint,
            DateTime InitialTime,
            DateTime FinalTime,
            TimeSpan intervalMove,
            double distance)
        {
            //���������� ����������� ���� ���� ����� ����������
            FillStops(dtMarkerReport, InitialTime);
            dtMarkerReport = InitialTime;
            //----------------------------------------------------
            string locationStart = "";
            string locationEnd = "";
            //if (atlantaDataSet.dataview.FindByDataGps_ID(initialDayPoint.InitialPointId) != null)
            if (GpsDatas.First(gps => gps.Id == initialDayPoint.InitialPointId) != null)
            {
                //LocationStart = FindLocation(new PointLatLng(
                //                                 atlantaDataSet.dataview.FindByDataGps_ID(initialDayPoint.InitialPointId)
                //                                               .Lat,
                //                                 atlantaDataSet.dataview.FindByDataGps_ID(initialDayPoint.InitialPointId)
                //                                               .Lon));
                //locationEnd = FindLocation(new PointLatLng(
                //                               atlantaDataSet.dataview.FindByDataGps_ID(finalDayPoint.FinalPointId).Lat,
                //                               atlantaDataSet.dataview.FindByDataGps_ID(finalDayPoint.FinalPointId).Lon));
                locationStart = FindLocation(GpsDatas.First(gps => gps.Id == initialDayPoint.InitialPointId).LatLng);
                locationEnd = FindLocation(GpsDatas.First(gps => gps.Id == finalDayPoint.FinalPointId).LatLng);
            }

            atlantaDataSet.KilometrageReportDayRow parkingRow =
                AtlantaDataSet.KilometrageReportDay.NewKilometrageReportDayRow();

            parkingRow.MobitelId = m_row.Mobitel_ID;
            parkingRow.LocationStart = locationStart;
            parkingRow.LocationEnd = locationEnd;
            //parkingRow.Day = initialDayPoint.InitialTime.ToString("d");
            parkingRow.InitialTime = InitialTime;
            parkingRow.FinalTime = FinalTime;
            // aketner - 06.08.2012
            TimeSpan Interval_Work = FinalTime.Subtract(InitialTime);
            //=============      
            parkingRow.IntervalWork = Convert.ToString(FinalTime.Subtract(InitialTime));

            if( parkingRow.IntervalWork.Length > 5 ) // ����� ����������� �� ������
            {
                int length = parkingRow.IntervalWork.Length;
                int indx = length - 2 - 1;

                parkingRow.IntervalWork = parkingRow.IntervalWork.Substring( 0, indx );
            }

            //parkingRow.IntervalWork = String.Format("{0:t}", parkingRow.IntervalWork);
            //parkingRow.IntervalWork = parkingRow.IntervalWork.Substring(0, 5); 
            // aketner - 06.08.2012
            TimeSpan Time_Stops_Total = Interval_Work - intervalMove;
            parkingRow.TimeStopsTotal = Time_Stops_Total.ToString();
            if (parkingRow.TimeStopsTotal.Length > 5)
            {
                parkingRow.TimeStopsTotal = parkingRow.TimeStopsTotal.Substring(0, 5);
            }

            //========== aketner 13.08.2012 ==========================================
            parkingRow.IntervalMove = intervalMove.ToString();

            if (parkingRow.IntervalMove.Length > 5)
            {
                parkingRow.IntervalMove = parkingRow.IntervalMove.Substring(0, 5);
            }

            parkingRow.Distance = distance;

            // aketner - 15.08.2012
            double speed_max = 0.0;

            try // aketner - �������� ��� ������ �� DBNull ����� ���� 28.12.2012
            {
                // ���������� ������� �������� �� GPS-��������� �� �������� ���������
                if (initialDayPoint["AverageSpeed"] != System.Convert.DBNull &&
                    finalDayPoint["AverageSpeed"] != System.Convert.DBNull)
                {
                    if (initialDayPoint.AverageSpeed > finalDayPoint.AverageSpeed)
                    {
                        speed_max = initialDayPoint.AverageSpeed;
                    }
                    else
                    {
                        speed_max = finalDayPoint.AverageSpeed;
                    }
                } // if
            } // try
            catch (Exception ex)
            {
                speed_max = 0.0; // ���� DBNull, �� ������� �������� ����� ����� 0
            }

            const double limitSpd = 1.25753;
            TimeSpan limitTime = new TimeSpan(0, 0, 5, 0, 0);

            //��������, ��/�. ����� ���������� ���� �� ����������� �����.
            if (speed_max > 0.0)
            {
                if (intervalMove.TotalHours > 0)
                {
                    double averageSpd = distance/intervalMove.TotalHours;

                    if (speed_max < averageSpd)
                    {
                        double limit = averageSpd/speed_max;

                        if (limit < limitSpd)
                        {
                            parkingRow.AverageSpeed = averageSpd;
                        }
                        else
                        {
                            if (limitTime < intervalMove)
                            {
                                parkingRow.AverageSpeed = averageSpd;
                            }
                            else
                            {
                                parkingRow.AverageSpeed = speed_max;
                            }
                        }
                    }
                    else
                    {
                        parkingRow.AverageSpeed = averageSpd;
                    }
                } // if
            } // if
            else
            {
                parkingRow.AverageSpeed = speed_max;
            }
            //========================================================================

            parkingRow.InitialPointId = initialDayPoint.InitialPointId;
            parkingRow.FinalPointId = finalDayPoint.FinalPointId;

            //----aketner 30.05.2013 -----------------
            parkingRow.NormaWay = Math.Round(parkingRow.Distance*this.normWays/100, 1);

            //����� ���������� � ����������� ������� ���������
            double hours = 0;

            TimeSpan tsEngineOnTime = GetRotateOnInterval( InitialTime, FinalTime );

            hours = ( tsEngineOnTime.Seconds + tsEngineOnTime.Minutes * 60 + tsEngineOnTime.Hours * 3600 ) / 3600.0;

            if (tsEngineOnTime.Days < 1)
            {
                DateTime dT = new DateTime(2000, 1, 1, tsEngineOnTime.Hours, tsEngineOnTime.Minutes,
                                           tsEngineOnTime.Seconds);
                parkingRow.MotorHrIdle = dT.ToString("HH:mm");
            }
            else
            {
                DateTime dT = new DateTime( 2000, 1, tsEngineOnTime.Days, tsEngineOnTime.Hours, tsEngineOnTime.Minutes, tsEngineOnTime.Seconds );
                parkingRow.MotorHrIdle = dT.ToString("dd.HH:mm");
            }

            parkingRow.NormaMotor = Math.Round( hours * this.normMotorHr, 1 );
            parkingRow.SummaNormaFuel = Math.Round( parkingRow.Distance * this.normWays / 100, 1 ) +
                                        Math.Round( hours * this.normMotorHr, 1 );
            //---------------------------------------------------

            AtlantaDataSet.KilometrageReportDay.AddKilometrageReportDayRow(parkingRow);
            _summaryDay.TotalWay += distance;
            _summaryDay.TotalTimeTour += FinalTime.Subtract(InitialTime);
            _summaryDay.TotalTimeWay += intervalMove;
        } // addDayToReport

        protected void addEmptyDayToReport(DateTime dtDay) // ��������� ����� � ����� ����� ���� ������� � ��� ��������
        {
            atlantaDataSet.KilometrageReportDayRow parkingRow =
                AtlantaDataSet.KilometrageReportDay.NewKilometrageReportDayRow();
            parkingRow.MobitelId = m_row.Mobitel_ID;
            parkingRow.LocationStart = Resources.MissedData;
            parkingRow.LocationEnd = "";
            parkingRow.InitialTime = new DateTime(dtDay.Year, dtDay.Month, dtDay.Day, 0, 0, 0);
            parkingRow.FinalTime = new DateTime(dtDay.Year, dtDay.Month, dtDay.Day, 23, 59, 59);
            parkingRow.IntervalWork = "00:00";
            parkingRow.IntervalMove = "00:00";
            // aketner-06.08.2012
            parkingRow.TimeStopsTotal = "00:00";
            //=====
            parkingRow.Distance = 0;
            parkingRow.AverageSpeed = 0;
            parkingRow.MaxSpeed = 0;
            parkingRow.InitialPointId = 0;
            parkingRow.FinalPointId = 0;
            // aketner - 20.06.2013
            parkingRow.MotorHrIdle = "00:00";
            parkingRow.SummaNormaFuel = 0;
            parkingRow.NormaMotor = 0;
            parkingRow.NormaWay = 0;
            //=========================
            AtlantaDataSet.KilometrageReportDay.AddKilometrageReportDayRow(parkingRow);
        }

        /// <summary>
        /// �������� ����������� ����� ��������� ������������ ������
        /// � ���������� ������� ��������� ������ ���������.
        /// </summary>
        protected override void AfterReportFilling()
        {
            base.AfterReportFilling();
            Algorithm.OnAction(this, new KilometrageDaysEventArgs(m_row.Mobitel_ID, _summaryDay));
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// ��� �������� ����� ����� ���������� �� ��� ���
        /// </summary>
        public void Split(ref atlantaDataSet.KilometrageReportRow km_row,
                          ref atlantaDataSet.KilometrageReportRow endDayPoint,
                          ref atlantaDataSet.KilometrageReportRow beginDayPoint,
                          ref double distance, ref DateTime dtBegin, ref DateTime dtEnd, ref TimeSpan tsDayMove)
        {
            //����������� �������� ������������ ����� ������� ��� �� ��� ���
            dtEnd = new DateTime(km_row.InitialTime.Year, km_row.InitialTime.Month,
                                 km_row.InitialTime.Day).AddDays(1).AddSeconds(-1); //, 23, 59, 59);
            DateTime dtBeginLastDay = new DateTime(km_row.FinalTime.Year, km_row.FinalTime.Month,
                                                   km_row.FinalTime.Day, 00, 00, 00);
            double minutesToEndDay = dtEnd.Subtract(km_row.InitialTime).TotalMinutes;
            double minutesMoveInNextDay = (km_row.FinalTime.Subtract(dtBeginLastDay).TotalMinutes);
            if (minutesMoveInNextDay < 0)
            {
                minutesMoveInNextDay = 0;
            }
            endDayPoint = km_row;
            if ((minutesMoveInNextDay + minutesToEndDay) > 0)
            {
                if (endDayPoint.Distance > 0)
                {
                    distance = distance + endDayPoint.Distance*minutesToEndDay/(minutesMoveInNextDay + minutesToEndDay);
                }
            }
            //DataGPS_ID ������� -------------------------------
            FinalPointId = km_row.FinalPointId;
            DateTime dtSeek = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day);
            //LocalCache.atlantaDataSet.dataviewRow[] dataRows = (LocalCache.atlantaDataSet.dataviewRow[])
            //                                                   atlantaDataSet.dataview.Select(
            //                                                       "Mobitel_Id=" + m_row.Mobitel_ID.ToString() +
            //                                                       " AND time >= #" + dtSeek.ToString("MM.dd.yyyy") +
            //                                                       "# and time  < #" +
            //                                                       dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#",
            //                                                       "time");
            GpsData[] gpsDatas =
                GpsDatas.Where(
                    gps => gps.Time >= dtSeek && gps.Time < dtSeek.AddDays(1))
                        .ToArray();

            if (gpsDatas.Length > 1)
            {
                km_row.FinalPointId = gpsDatas[gpsDatas.Length - 1].Id;
            }
            tsDayMove += dtEnd.Subtract(endDayPoint.InitialTime);
            addDayToReport(beginDayPoint, endDayPoint, dtBegin, dtEnd, tsDayMove, distance);
            km_row.InitialPointId = km_row.FinalPointId;
            km_row.FinalPointId = FinalPointId;
            beginDayPoint = km_row;
            dtBegin = dtBeginLastDay;
            dtEnd = km_row.FinalTime;
            if (dtEnd.Subtract(dtBegin).TotalMinutes < 0)
            {
                dtEnd = dtBegin;
            }
            endDayPoint = beginDayPoint;
            if (km_row.Interval.TotalMinutes > 0)
            {
                tsDayMove = dtEnd.Subtract(dtBegin);
            }
            if (km_row.Distance > 0)
            {
                distance = km_row.Distance*minutesMoveInNextDay/(minutesMoveInNextDay + minutesToEndDay);
            }
        }

        /// <summary>
        /// ��������� ��� ��� �������� ���������� �� ����������
        /// </summary>
        public void FillStops(DateTime dtFrom, DateTime dtTo)
        {
            int iDays = ReportDaysBetweenDates(dtFrom, dtTo);
            int iStart = 1;
            atlantaDataSet.KilometrageReportDayRow[] km_Day_rows = (atlantaDataSet.KilometrageReportDayRow[])
                                                                   AtlantaDataSet.KilometrageReportDay.Select(
                                                                       "MobitelId = " + m_row.Mobitel_ID +
                                                                       " AND InitialTime >= #" +
                                                                       dtFrom.ToString("MM.dd.yyyy") + "#");
            if (km_Day_rows.Length == 0)
            {
                iStart = 0;
            }
            for (int i = iStart; i < iDays; i++)
            {
                FillStopsOneDay(dtFrom.AddDays(i));
            }
        }

        public void FillStopsOneDay(DateTime dtStop)
        {
            //atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])atlantaDataSet.KilometrageReport.Select("State = '�������' AND MobitelId = " + m_row.Mobitel_ID + " AND InitialTime >= #" + dtStop.ToString("MM.dd.yyyy") + "# and FinalTime  < #" + dtStop.AddDays(1).ToString("MM.dd.yyyy") + "#", "InitialTime");
            //LocalCache.atlantaDataSet.dataviewRow[] dataRows = (LocalCache.atlantaDataSet.dataviewRow[])
            //                                                   atlantaDataSet.dataview.Select(
            //                                                       "Mobitel_Id=" + m_row.Mobitel_ID.ToString() +
            //                                                       " AND time >= #" + dtStop.ToString("MM.dd.yyyy") +
            //                                                       "# and time  < #" +
            //                                                       dtStop.AddDays(1).ToString("MM.dd.yyyy") + "#",
            //                                                       "time");
            GpsData[] gpsDatas =
                GpsDatas.Where(
                    gps => gps.Time >= dtStop && gps.Time < dtStop.AddDays(1))
                        .ToArray();
            if (gpsDatas.Length > 1)
            {
                System.Int64 initialDayPoint = gpsDatas[0].Id;
                System.Int64 finalPointId = gpsDatas[gpsDatas.Length - 1].Id;
                //string LocationStart = FindLocation(new PointLatLng(
                //                                        atlantaDataSet.dataview.FindByDataGps_ID(initialDayPoint).Lat,
                //                                        atlantaDataSet.dataview.FindByDataGps_ID(initialDayPoint).Lon));
                //string locationEnd = FindLocation(new PointLatLng(
                //                                      atlantaDataSet.dataview.FindByDataGps_ID(FinalPointId).Lat,
                //                                      atlantaDataSet.dataview.FindByDataGps_ID(FinalPointId).Lon));
                string locationStart = FindLocation(GpsDatas.First(gps => gps.Id == initialDayPoint).LatLng);
                string locationEnd = FindLocation(GpsDatas.First(gps => gps.Id == finalPointId).LatLng);
                atlantaDataSet.KilometrageReportDayRow parkingRow =
                    AtlantaDataSet.KilometrageReportDay.NewKilometrageReportDayRow();

                // ������������� ����� ������� ������
                parkingRow.MobitelId = m_row.Mobitel_ID;
                parkingRow.LocationStart = locationStart;
                parkingRow.LocationEnd = locationEnd;
                //parkingRow.Day = initialDayPoint.InitialTime.ToString("d");
                parkingRow.InitialTime = new DateTime(dtStop.Year, dtStop.Month, dtStop.Day, dtStop.Hour, dtStop.Minute,
                                                      dtStop.Second);
                parkingRow.FinalTime = new DateTime(dtStop.Year, dtStop.Month, dtStop.Day, 23, 59, 59);
                parkingRow.IntervalWork = "00:00";
                parkingRow.IntervalMove = "00:00";
                // aketner-06.08.2012
                parkingRow.TimeStopsTotal = "00:00"; // ����� ����� ��������� �� ����� �����
                //====
                parkingRow.Distance = 0;
                parkingRow.AverageSpeed = 0;
                parkingRow.MaxSpeed = 0;
                parkingRow.InitialPointId = initialDayPoint;
                parkingRow.FinalPointId = finalPointId;
                AtlantaDataSet.KilometrageReportDay.AddKilometrageReportDayRow(parkingRow);
            }
        }

        TimeSpan GetRotateOnInterval(DateTime initialDatePoint, DateTime finalTimePoint)
        {
            DateTime dtSeek = initialDatePoint;
            DateTime dtEndSeek = finalTimePoint;
            dtSeek = new DateTime( dtSeek.Year, dtSeek.Month, dtSeek.Day, dtSeek.Hour, dtSeek.Minute,
                                  dtSeek.Second );

            atlantaDataSet.RotateReportRow[] rot_rows =
                (atlantaDataSet.RotateReportRow[])AtlantaDataSet.RotateReport.Select(
                    "State = '" + Resources.Working + "' AND MobitelId=" + m_row.Mobitel_ID.ToString() +
                    " AND ((InitialTime >= #" + dtSeek.ToString( "MM.dd.yyyy" ) + "# and InitialTime  < #" +
                    dtSeek.AddDays( 1 ).ToString( "MM.dd.yyyy" ) + "#" +
                    ") OR (FinalTime >= #" + dtSeek.ToString( "MM.dd.yyyy" ) + "# and FinalTime  < #" +
                    dtSeek.AddDays( 1 ).ToString( "MM.dd.yyyy" ) + "#  and InitialTime  < #" +
                    dtSeek.ToString( "MM.dd.yyyy" ) +
                    "#)" + " OR (InitialTime < #" + dtSeek.ToString( "MM.dd.yyyy" ) + "# and FinalTime > #" +
                    dtSeek.AddDays( 1 ).ToString( "MM.dd.yyyy" ) + "#))", "InitialTime" );

            TimeSpan tsEngineOnTime = new TimeSpan();
            DateTime dtBgn = new DateTime( dtSeek.Year, dtSeek.Month, dtSeek.Day, dtSeek.Hour, dtSeek.Minute,
                                          dtSeek.Second );
            DateTime dtNd = new DateTime( dtEndSeek.Year, dtEndSeek.Month, dtEndSeek.Day, dtEndSeek.Hour,
                                         dtEndSeek.Minute, dtEndSeek.Second );
            
            if( rot_rows.Length > 0 )
            {
                foreach( atlantaDataSet.RotateReportRow tmp_rot_row in rot_rows )
                {
                    //������� ����� ���� - ������ - ���������
                    DateTime dtBWork = new DateTime();
                    DateTime dtEWork = new DateTime();

                    if( tmp_rot_row.InitialTime >= dtBgn )
                    {
                        if( tmp_rot_row.InitialTime <= dtNd )
                        {
                            dtBWork = tmp_rot_row.InitialTime;
                        }
                    }
                    else
                    {
                        if( tmp_rot_row.FinalTime >= dtBgn )
                        {
                            dtBWork = dtBgn;
                        }
                    }

                    if( tmp_rot_row.FinalTime >= dtBgn )
                    {
                        if( tmp_rot_row.FinalTime < dtNd )
                        {
                            dtEWork = tmp_rot_row.FinalTime;
                        }
                        else
                        {
                            if( tmp_rot_row.InitialTime <= dtNd )
                            {
                                dtEWork = dtNd;
                            }
                        }
                    }

                    tsEngineOnTime = tsEngineOnTime.Add( dtEWork - dtBWork );
                } // foreach 
            } // if

            return tsEngineOnTime;
        } // GetRotateOnInterval

        /// <summary>
        /// ���������� ��������� ����� ����� ����� ����� ������.
        /// </summary>
        public int ReportDaysBetweenDates(DateTime dtFrom, DateTime dtTo)
        {
            dtFrom = new DateTime(dtFrom.Year, dtFrom.Month, dtFrom.Day, dtFrom.Hour, dtFrom.Minute, dtFrom.Second);
            dtTo = new DateTime(dtTo.Year, dtTo.Month, dtTo.Day, dtTo.Hour, dtTo.Minute, dtTo.Second);
            TimeSpan tsDiff = dtTo.Subtract(dtFrom);
            return tsDiff.Days;
        }

        #region Nested Types

        /// <summary>
        /// ������������ �������� ������ ������ � ������� � ����������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        public struct SummaryDay
        {
            /// <summary>
            /// ����� ���������� ����
            /// </summary>
            public double TotalWay;

            /// <summary>
            /// ����� ����������������� �����
            /// </summary>
            public TimeSpan TotalTimeTour;

            /// <summary>
            /// ����� � ����
            /// </summary>
            public TimeSpan TotalTimeWay;
        }

        #endregion
    }

    /// <summary>
    /// �����, ���������� ���������, ������� ����� �������� � ����������
    /// ������� �� ��������� ������ ��������� ���������� ������ ��� ������
    /// � ������� � ���������� ����������� ������������� ��������.
    /// </summary>
    public class KilometrageDaysEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// ID ���������
        /// </summary>
        private int _id;

        /// <summary>
        /// �������� ������ ������ � ������� � ���������� �� �����������
        /// ��������� (������������� ��������). 
        /// </summary>
        private KilometrageDays.SummaryDay _summaryDay;

        #endregion

        #region .ctor

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public KilometrageDaysEventArgs(int id, KilometrageDays.SummaryDay SummaryDay)
        {
            _id = id;
            _summaryDay = SummaryDay;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        /// <summary>
        /// �������� ������ ������ � ������� � ���������� �� �����������
        /// ��������� (������������� ��������).
        /// </summary>
        public KilometrageDays.SummaryDay SummaryDay
        {
            get { return _summaryDay; }
        }

        #endregion
    }
}