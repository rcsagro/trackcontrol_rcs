using LocalCache;
using TrackControl.Vehicles;
using System.Collections.Generic ;


namespace BaseReports.Procedure
{
    public interface IAlgorithm
    {
        /// <summary>
        /// ��������� �������� ���������� ������
        /// </summary>
        void Run();

        void SelectItem(atlantaDataSet.mobitelsRow m_row);
        void SelectItem(int mobitelId, List<GpsData> gpsData);
        atlantaDataSet.sensorsRow S_row { get; set; }

        /// <summary>
        /// �������� ������������� ��������
        /// </summary>
        VehicleInfo Vehicle { get; }

        /// <summary>
        /// �������� ������� DRT
        /// </summary>
        //string NameDrtSensor { get; set; }
    }
}
