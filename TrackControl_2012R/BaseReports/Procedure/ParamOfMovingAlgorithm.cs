using System;
using BaseReports.Properties;
using LocalCache;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
  public class ParamOfMovingAlgorithm : Algorithm, IAlgorithm
  {
    public TrafficProperty traffPar;

    /// <summary>
    /// ������� ����� ������ ������ � ��������� ���������� �������
    /// </summary>
    /// <param name="dataGps"></param>
    /// <returns></returns>
    private atlantaDataSet.TrafficDetailReportRow GetNewRowWithBaseSetting(GpsData dataGps)
    {
      atlantaDataSet.TrafficDetailReportRow ret_row =
        AtlantaDataSet.TrafficDetailReport.NewTrafficDetailReportRow();
      ret_row.Location = "";
      ret_row.Time = dataGps.Time;
      ret_row.DataGps_ID = dataGps.Id;
      ret_row.Mobitel_id = base.m_row.Mobitel_ID;
      ret_row.Name = base.m_row.Name;
      ret_row.Speed = dataGps.Speed;
      ret_row.Location = FindLocation(dataGps.LatLng);
      ret_row.Duration = new TimeSpan(0, 0, 0);
      return ret_row;
    }

    #region IAlgorithm Members

    void IAlgorithm.Run()
    {
      traffPar = new TrafficProperty(m_row);

      BeforeReportFilling(Resources.ParamsOfMoving, GpsDatas.Length);

      int points_acc = 0, points_speed = 0, points_break = 0;
      atlantaDataSet.TrafficDetailReportRow row_speed = null;
      atlantaDataSet.TrafficDetailReportRow row_acc = null;
      atlantaDataSet.TrafficDetailReportRow row_break = null;
      //double mid_speed

      int progressCounter = 0;
      foreach (GpsData d in GpsDatas)
      {
        if (d.Speed > this.traffPar.speedAlow)
        {
          if (0 == points_speed)
          {
            points_speed = 1;
            row_speed = GetNewRowWithBaseSetting(d);

            row_speed.DescribeEvent = string.Format(Resources.Speeding + " (> {0} {1})",
              this.traffPar.speedAlow, Resources.SpeedAbbreviation);//"���������� �������� (> {0} ��/���)",this.traffPar.speedAlow);
            row_speed.MiddleValue = d.Speed;
            row_speed.MaxValue = d.Speed; ;//������ ������������ �������� �� ��������

            AtlantaDataSet.TrafficDetailReport.AddTrafficDetailReportRow(row_speed);
          }
          else
          {
            if (row_speed.MaxValue < d.Speed)
            {
              row_speed.MaxValue = d.Speed;//������ ������������ �������� �� ��������
            }
            points_speed++;
            row_speed.MiddleValue += d.Speed;
          }
        }
        else if (points_speed > 0)
        {//save and close
          if (d.Time > row_speed.Time)
          {
            row_speed.Duration = d.Time - row_speed.Time;
          }
          row_speed.MiddleValue = row_speed.MiddleValue / points_speed;
          row_speed.FinalPointId = d.Id;
          row_speed = null;
          points_speed = 0;
        }
        if (d.Accel > 0 && d.Accel > this.traffPar.accelAlow)
        {
          if (0 == points_acc)
          {
            points_acc = 1;
            row_acc = GetNewRowWithBaseSetting(d);

            row_acc.DescribeEvent = string.Format(Resources.CriticalAcceleration + " (> {0} {1})",
              this.traffPar.accelAlow, Resources.AccelerationAbbreviation);
            row_acc.MiddleValue = d.Accel;
            row_acc.MaxValue = d.Accel; ;//������ ������������ �������� �� ��������

            AtlantaDataSet.TrafficDetailReport.AddTrafficDetailReportRow(row_acc);
          }
          else
          {
            if (row_acc.MaxValue < d.Accel)
            {
              row_acc.MaxValue = d.Accel;//������ ������������ �������� �� ��������
            }
            points_acc++;
            row_acc.MiddleValue += d.Accel;
          }
        }
        else if (points_acc > 0)
        {//save and close
          if (d.Time > row_acc.Time)
          {
            row_acc.Duration = d.Time - row_acc.Time;
          }
          row_acc.MiddleValue = row_acc.MiddleValue / points_acc;
          row_acc.FinalPointId = d.Id;
          row_acc = null;
          points_acc = 0;
        }

        if (d.Accel < 0 && Math.Abs(d.Accel) > Math.Abs(this.traffPar.breakAlow))
        {
          if (0 == points_break)
          {
            points_break = 1;
            row_break = GetNewRowWithBaseSetting(d);

            row_break.DescribeEvent = string.Format(Resources.CriticalBraking + " (> {0} {1})",
              this.traffPar.breakAlow, Resources.AccelerationAbbreviation);//"���������� �������� (> {0} ��/���)",this.traffPar.speedAlow);
            row_break.MiddleValue = d.Accel;
            row_break.MaxValue = d.Accel;//������ ������������ �������� �� ��������

            AtlantaDataSet.TrafficDetailReport.AddTrafficDetailReportRow(row_break);
          }
          else
          {
            if (row_break.MaxValue > d.Accel)
            {
              row_break.MaxValue = d.Accel;//������ ������������ �������� �� ��������
            }
            points_break++;
            row_break.MiddleValue += d.Accel;
          }
        }
        else if (points_break > 0)
        {//save and close
          if (d.Time > row_break.Time)
          {
            row_break.Duration = d.Time - row_break.Time;
          }
          row_break.MiddleValue = row_break.MiddleValue / points_break;
          row_break.FinalPointId = d.Id;
          row_break = null;
          points_break = 0;
        }
        if ((++progressCounter % 64) == 0)
        {
          ReportProgressChanged(progressCounter);
        }
      }
      AfterReportFilling();
    }

    #endregion
  }
}
