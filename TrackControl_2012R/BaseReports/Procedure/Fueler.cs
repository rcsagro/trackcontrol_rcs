using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    public class Fueler : PartialAlgorithms, IAlgorithm
    {
        /// <summary>
        /// �������� ������ ������
        /// </summary>
        private Summary _summary;

        /// <summary>
        /// ��������� - ����������� ������ ��� �������� ��������, ������
        /// </summary>
        private double minDeltaValue;

        /// <summary>
        /// ��������� - ����������� ����� ����� ��������� ����������
        /// </summary>
        private TimeSpan minDeltaTime;

        /// <summary>
        /// ������������ �������� ����� ��������� � �������� ������ ����������. �������� ���������� �������.����� �������� ������ � ����������� �������� 
        /// </summary>
        private int _maxIntervalBetweenPointsInHour;
        /// <summary>
        /// ��������� ������ ������ ������������� ����������.
        /// </summary> //���� � ������� ��������� - ����� ��������� � � ������ �� ��������.
        private int radius;

        private const string starterDriverName = "";

        private List<VehicleInfo> cntFuelingVehicleInfo = new List<VehicleInfo>();

        private FuelerPart fuelerPart;

        private KeyValuePair<System.Int64, double>[] kvpFueler = null;

        private atlantaDataSet fPartDataSet = null;

        private string OutLinkIdDriver = "";

        private string OutLinkIdOperator = "";

        private string OutLinkIdTransport = "--";

        public Fueler() : base()
        {
        }

        void IAlgorithm.SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
            //this.minTimeDifFueler = new TimeSpan(0, 1, 0);
            //if (set_row != null)
            //{
            //  this.minTimeDifFueler = set_row.band; //???
            //}
        }

        private string RemoveSpaces(string inputString)
        {
            inputString = inputString.Replace("  ", string.Empty);
            inputString = inputString.Trim().Replace(" ", string.Empty);

            return inputString;
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        public override void Run()
        {
            try
            {
                if (m_row == null)
                {
                    return;
                }

                if (m_row.GetFuelerReportRows() != null)
                {
                    if (m_row.GetFuelerReportRows().Length != 0 || GpsDatas.Length == 0)
                    {
                        return;
                    }
                }

                //BeforeReportFilling(Resources.FuelerCaption);
                //System.Windows.Forms.Application.DoEvents();

                minDeltaValue = 0.05; // 1
                minDeltaTime = new TimeSpan(0, 2, 0);
                _maxIntervalBetweenPointsInHour = 4;
                radius = 500; // ???
                int countInMoving = 0;

                if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
                {
                    minDeltaValue = set_row.FuelerMinFuelrate;
                    minDeltaTime = set_row.FuelerMaxTimeStop;
                    radius = set_row.FuelerRadiusFinds;
                    countInMoving = set_row.FuelerCountInMotion;
                }

                int mobitelID = -1;
                _summary = new Summary();
                _summary.UnrecordedFuelRate = 0;
                _summary.FuelRate = 0;
                List<atlantaDataSet.sensorsRow> lstDrtSensors = FindDrtSensors(AlgorithmType.FUELER);
                if (lstDrtSensors.Count > 0)
                {
                    TypeAlgorithm = (int) AlgorithmType.FUELER;
                }
                else
                {
                    lstDrtSensors = FindDrtSensors(AlgorithmType.FUELER_DIFFER);
                    if(lstDrtSensors.Count > 0)
                        TypeAlgorithm = (int)AlgorithmType.FUELER_DIFFER;
                }
                // �������� ������ �������� ��� (����)
                for (int indx = 0; indx < lstDrtSensors.Count; indx++)
                {
                    BeforeReportFilling(Resources.FuelerCaption);
                    System.Windows.Forms.Application.DoEvents();
                    NameDrtSensor = lstDrtSensors[indx].Name;
                    FuelRateDictionarys frDict = new FuelRateDictionarys();
                    SetterSensor(lstDrtSensors[indx]);
                    frDict.fuelRateSensor1 = lstDrtSensors[indx]; // ������� �� ��������� ���� ������ ��� �� ���������
                    frDict.fuelDutSensors = FindSensorsDutos(AlgorithmType.FUEL1); // �������� ������ �������� ���
                    if (TypeAlgorithm == (int)AlgorithmType.FUELER)
                        GettingValuesFuelerDRT(frDict, countInMoving); // ��������� ����� ������ ��������� �������� DRT
                    else if (TypeAlgorithm == (int)AlgorithmType.FUELER_DIFFER)
                    {
                        frDict.TypeAlgorithm1 = (int)AlgorithmType.FUELDIFFDRT;
                        GettingValuesFuelerDRTDiff(frDict, countInMoving);
                    }
                    GettingValuesFuelerDUTs(frDict, countInMoving); // ��������� ����� ������ ��������� �������� �����

                    // ��������� ��������������� �������� DRT and DUTs

                    if (frDict.ValSeries1.Count == 0) // ���� ������ �� ������� - �� �� ��������� ����
                    {
                        //AfterReportFilling();
                        //return;
                        continue;
                    }

                    //_summary = new Summary();
                    //_summary.UnrecordedFuelRate = 0;
                    //_summary.FuelRate = 0;

                    List<atlantaDataSet.FuelerReportRow> tmpFuelerReportRow;
                    fuelerPart = new FuelerPart(AtlantaDataSet);
                    fuelerPart.NameSensorDrt = NameDrtSensor;

                    //REPORT BUILD
                    mobitelID = m_row.Mobitel_ID;

                    // ����� ���� ��������� � ���������� ��� DRT � DUTs
                    tmpFuelerReportRow = fuelerPart.GetFuelerReportRows(frDict, minDeltaValue, minDeltaTime, mobitelID, _maxIntervalBetweenPointsInHour);

                    // ����� ����������� ������ ��� ����������� �������������
                    fPartDataSet = fuelerPart.getfPartDataSet;
                    kvpFueler = fuelerPart.getFromFuelerPart(frDict);

                    //������� ������ & �������� �������� ������ ����������� (�� ��������)
                    int i = 0;

                    while (i < tmpFuelerReportRow.Count)
                    {
                        _summary.FuelRate += tmpFuelerReportRow[i].changeValue;

                        if (tmpFuelerReportRow[i].changeValue < minDeltaValue)
                        {
                            _summary.UnrecordedFuelRate += tmpFuelerReportRow[i].changeValue;

                            // �����, ������� ����� �������� DUT � ��� ��� �������� ����
                            if (i < (tmpFuelerReportRow.Count - 1))
                            {
                                if ((i + 1) < tmpFuelerReportRow.Count)
                                    tmpFuelerReportRow[i + 1].valueDutos += tmpFuelerReportRow[i].valueDutos;
                            }
                            else
                            {
                                if ((i - 1) >= 0)
                                    tmpFuelerReportRow[i - 1].valueDutos += tmpFuelerReportRow[i].valueDutos;
                            }

                            tmpFuelerReportRow.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                    }

                    BeforeReportFilling(Resources.Refueler, tmpFuelerReportRow.Count);
                    int progressCounter = 0;

                    // ����� ��������� ������������� ���������
                    atlantaDataSet.sensorsRow sens_row = FindSensor(AlgorithmType.OPERATOR_IDENT);
                    // ����� ��������� ������������� ������������� ��������
                    atlantaDataSet.sensorsRow ident_row = FindSensor(AlgorithmType.VEHICLE_FUELER_IDENT);

                    //����� ����� ������� �������� ���������
                    for (int p = 0; p < tmpFuelerReportRow.Count; p++)
                    {
                        atlantaDataSet.FuelerReportRow fuelRep_row = tmpFuelerReportRow[p];
                        bool startOldAlgorithm = true;

                        // �������� ������������� ������������� ��������
                        fuelRep_row.fillUp = "";
                        OutLinkIdTransport = "--";
                        if (ident_row != null) //���� ������ �� ������� - ������ ������
                        {
                            ulong idVehicle =
                                Procedure.Calibration.Calibrate.ulongSector(
                                    GpsDatas.First(gps => gps.Id == fuelRep_row.DataGPS_ID).Sensors,
                                    ident_row.Length, ident_row.StartBit);

                            // �� ����������� ���� �������� ������������ �������� �� ������� Vehicle
                            if (idVehicle > 0)
                            {
                                string vhclName = FindVehicleName(idVehicle);
                                string ident = vhclName + "(" + idVehicle + ")";
                                fuelRep_row.fillUp = RemoveSpaces(ident);
                                // ��������� ��� ������� ���� ��� ������������� ��������
                                if (OutLinkIdTransport != "--")
                                    fuelRep_row.TransportCodeDB = OutLinkIdTransport;
                                else
                                    fuelRep_row.TransportCodeDB = "--";

                                // ��������� ��������� ��������� ������������ �������
                                VehicleInfo[] vehicInf = new VehicleInfo[1];
                                vehicInf[0] = new VehicleInfo(FindMobitelId(idVehicle));

                                FillDriverName(fuelRep_row, vehicInf);

                                startOldAlgorithm = false;
                            } // if
                        } // if

                        if (startOldAlgorithm)
                        {
                            // ��� ��������� ����� ������ ������ + 
                            // ���� ������������� ������ ����� ����� ���������
                            FuelingTC[] fuelingTC = fuelerPart.GetFuelingTC(fuelRep_row, radius);
                            FillNameFueleredCar(ref fuelingTC, fuelRep_row);
                        }

                        // �������� ������������� ��������� 
                        fuelRep_row.Operator = "";
                        fuelRep_row.OperatorCodeDB = "";
                        if (sens_row != null) //���� ������ �� ������� - ������ ������
                        {
                            //ulong idOperator = Procedure.Calibration.Calibrate.ulongSector(
                            //    BitConverter.GetBytes(atlantaDataSet.dataview.FindByDataGps_ID(fuelRep_row.DataGPS_ID).sensor),
                            //    sens_row.Length, sens_row.StartBit);
                            ulong idOperator = Procedure.Calibration.Calibrate.ulongSector(
                                GpsDatas.First(gps => gps.Id == fuelRep_row.DataGPS_ID).Sensors,
                                sens_row.Length, sens_row.StartBit);

                            // �� ����������� ���� �������� �������� �� ������� Driver
                            if (idOperator > 0)
                            {
                                string drvName = FindDriverName(idOperator);
                                string oper = drvName + "(" + idOperator + ")";
                                fuelRep_row.Operator = RemoveSpaces(oper);
                                fuelRep_row.OperatorCodeDB = OutLinkIdDriver;
                            } // if
                        } // if

                        ReportProgressChanged(++progressCounter);
                    } // foreach

                    //XtraMessageBox.Show( "�����, �����: " + result.ToString() + ". ���������� ����� �����:" + tmpFuelerReportRow.Count.ToString(), "����� ������" );
                    //myStopwatch.Reset();

                    //�������� ���������� � �������
                    foreach (atlantaDataSet.FuelerReportRow fuelerR_row in tmpFuelerReportRow)
                    {
                        if (fuelerR_row.fillUp == "") //���� �� ����� ������������ �� - ��� � �������
                        {
                            fuelerR_row.fillUp = Resources.NotFound;
                            fuelerR_row.TransportCodeDB = "--";
                        }

                        //atlantaDataSet.FuelerReport.AddFuelerReportRow(fuelerR_row);
                        AtlantaDataSet.FuelerReport.AddFuelerReportRow(fuelerR_row);
                    }

                    SelectItem(AtlantaDataSet.mobitels.FindByMobitel_ID(mobitelID));
                    //�������� ��������, ��� ��� ��� ����� ������ �� ��� ����������
                    //AfterReportFilling();
                    //Algorithm.OnAction(this, new FuelerEventArgs(mobitelID, _summary));
                    //System.Windows.Forms.Application.DoEvents();
                } // for earch N-s DRT's
                AfterReportFilling();
                Algorithm.OnAction(this, new FuelerEventArgs(mobitelID, _summary));
                System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Fueler Run", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public int TypeAlgorithm { get; set; }

        public DateTime getTimeFromFuelerPart( long index )
        {
            DateTime dtm = new DateTime();

            if (fPartDataSet == null)
                return dtm;

            //return fPartDataSet.dataview.FindByDataGps_ID( kvpFueler[index].Key ).time;

            if (index >= kvpFueler.Length)
            {
                return dtm;
            }

            try
            {
                dtm = GpsDatas.First(gps => gps.Id == kvpFueler[index].Key).Time;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "gettimefromfuelerpart", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            return dtm;
        }

        public long getIndexFromFuelerPart(long DataGPS_ID)
        {
            long i = -1;

            for (i = 0; i < kvpFueler.Count(); i++)
            {
                if (kvpFueler[i].Key == DataGPS_ID)
                    return i;
            }

            return i;
        }

        public long getIndexFromFuelerPart( DateTime dtmTime )
        {
            long i = -1;
            long k = kvpFueler.Length - 1;

            for( i = 0; i < kvpFueler.Length; i++ )
            {
                //if( fPartDataSet.dataview.FindByDataGps_ID(kvpFueler[i].Key).time.Equals(dtmTime) )
                //    return i;

                if (GpsDatas.First(gps => gps.Id == kvpFueler[i].Key).Time.Equals(dtmTime))
                    return i;
            }

            return k;
        }

        private void FillDriverName( atlantaDataSet.FuelerReportRow fr_row, VehicleInfo[] vehInfo )
        {
            bool flagPresent = false;
            bool flagPresent2 = false;
            OutLinkIdDriver = "";
            ulong idCard = 0;
            DateTime timeStart = fr_row.timeStart;
            DateTime timeEnd = fr_row.timeEnd;

            atlantaDataSet.sensorsRow driverIdSensRow = FindSensor(AlgorithmType.DRIVER);
            fr_row.driverName = starterDriverName + "(" + Resources.handbook + ")"; //����������
            fr_row.GetterCodeDB = OutLinkIdDriver;
            
            if (driverIdSensRow != null && fr_row.timeEnd.Subtract(fr_row.timeStart).TotalSeconds > 0)
            {
                //��������� �� ID ��������
                //ulong idCard = Procedure.Calibration.Calibrate.ulongSector(BitConverter.GetBytes(atlantaDataSet.dataview.FindByDataGps_ID(fr_row.DataGPS_ID).sensor),
                //        driverIdSensRow.Length, driverIdSensRow.StartBit);

                idCard = Procedure.Calibration.Calibrate.ulongSector(GpsDatas.First(gps => gps.Id == fr_row.DataGPS_ID).Sensors,
                        driverIdSensRow.Length, driverIdSensRow.StartBit);

                if ((idCard == 0) || (idCard == 1023))
                {
                    // ��������� ���������
                    long indexStart = getIndexFromFuelerPart(timeStart);
                    long indexEnd = getIndexFromFuelerPart(timeEnd);

                    // ���� �������� idCard, �� ����� ����� ���������� ��������� �����
                    while (indexStart < indexEnd)
                    {
                        try
                        {
                            //idCard = Procedure.Calibration.Calibrate.ulongSector(
                            //    BitConverter.GetBytes(atlantaDataSet.dataview.FindByTime(fr_row.mobitelsRow, tmStart).sensor),
                            //    driverIdSensRow.Length, driverIdSensRow.StartBit);
                            idCard = Procedure.Calibration.Calibrate.ulongSector(GpsDatas.First(gps => gps.Time == timeStart).Sensors,
                                driverIdSensRow.Length, driverIdSensRow.StartBit);

                            indexStart++;
                        }
                        catch (Exception e)
                        { 
                            // to do this 
                        }

                        if ((idCard <= 0) || (idCard == 1023))
                        {
                            timeStart = getTimeFromFuelerPart(indexStart);
                        }
                        else
                        {
                            break;
                        }
                    } // while
                } // if

                // ���� idCard = 1023, �� �������� ����� ��������, � ��������� ������ �������, 
                // ����� ������ ��������
                if (idCard == (ulong) ((1 << driverIdSensRow.Length) - 1))
                {
                    //List<atlantaDataSet.dataviewRow> dv_fueller = DataSetManager.GetDataViewRowsFromDb(atlantaDataSet,
                    //    fr_row.mobitel_id, timeStart, timeEnd);
                    List<GpsData> gpsDatas = DataSetManager.GetGpsDataFromDb(AtlantaDataSet, fr_row.mobitel_id, timeStart, timeEnd);
                    if (gpsDatas != null)
                    {
                        foreach (GpsData gpsData in gpsDatas)
                        {
                            idCard = BaseReports.Procedure.Calibration.Calibrate.ulongSector(gpsData.Sensors,
                                driverIdSensRow.Length, driverIdSensRow.StartBit);

                            if (idCard != (ulong)((1 << driverIdSensRow.Length) - 1))
                                break;
                        }
                    } // if

                    flagPresent = true;
                    flagPresent2 = true;
                } // if

                //���� idCard = 0, �� �������� ����� ��������, � ��������� ������ �������, ����� ������ ��������
                if( idCard == 0 && !flagPresent)
                {
                    //List<atlantaDataSet.dataviewRow> dv_fueller = DataSetManager.GetDataViewRowsFromDb( atlantaDataSet,
                    //    fr_row.mobitel_id, timeStart, timeEnd );
                    List<GpsData> gpsDatas = DataSetManager.GetGpsDataFromDb(AtlantaDataSet, fr_row.mobitel_id, timeStart, timeEnd);
                    if (gpsDatas != null)
                    {
                        foreach (GpsData gpsData in gpsDatas)
                        {
                            idCard = BaseReports.Procedure.Calibration.Calibrate.ulongSector(gpsData.Sensors,
                                                                                             driverIdSensRow.Length,
                                                                                             driverIdSensRow.StartBit);

                            if (idCard != 0)
                                break;
                        }
                    }

                    flagPresent2 = true;
                }

                //���� idCard = 4095, �� �������� ����� ��������, � ��������� ������ �������, ����� ������ ��������
                if( idCard == 4095 && !flagPresent2 )
                {
                    //List<atlantaDataSet.dataviewRow> dv_fueller = DataSetManager.GetDataViewRowsFromDb( atlantaDataSet,
                    //    fr_row.mobitel_id, timeStart, timeEnd );
                    List<GpsData> gpsDatas = DataSetManager.GetGpsDataFromDb(AtlantaDataSet, fr_row.mobitel_id, timeStart, timeEnd);
                    if (gpsDatas != null)
                    {
                        foreach (GpsData gpsData in gpsDatas)
                        {
                            idCard = BaseReports.Procedure.Calibration.Calibrate.ulongSector(gpsData.Sensors,
                                                                                             driverIdSensRow.Length,
                                                                                             driverIdSensRow.StartBit);

                            if (idCard != 4095)
                                break;
                        }
                    }
                }

                if (idCard == (ulong) ((1 << driverIdSensRow.Length) - 1))
                {
                    fr_row.driverName = String.Format(starterDriverName + "(" + Resources.NotRfidCard + ")", idCard);
                    fr_row.GetterCodeDB = OutLinkIdDriver;
                }
                //��� ��������
                else
                {
                    if (idCard == 0)
                    {
                        fr_row.driverName = starterDriverName + "(" + Resources.RfidDisable + ")"; //RFID ��������
                        fr_row.GetterCodeDB = OutLinkIdDriver;
                    }
                    else
                    {
                        string strName = FindDriverName(idCard);
                        fr_row.driverName = String.Format(strName + "({0})", idCard);
                        fr_row.GetterCodeDB = OutLinkIdDriver;
                    }
                }
            }

            //��������� �� ������� �������� ��, ���� ��� �� ����� ������� � ����� �� ����
            if (vehInfo.Length == 1)
            {
                if (fr_row.driverName.Contains(starterDriverName) && vehInfo[0].DriverName != "" && starterDriverName.Length > 0)
                {
                    fr_row.driverName = fr_row.driverName.Replace(starterDriverName, vehInfo[0].DriverName);
                    fr_row.GetterCodeDB = OutLinkIdDriver;
                }
            }
        } // FillDriverName

        private string FindVehicleName(ulong idVehicle)
        {
            OutLinkIdTransport = "--";
            string ret = starterDriverName;
            foreach (atlantaDataSet.vehicleRow vehicle_row in AtlantaDataSet.vehicle)
            {
                try
                {
                    if (idVehicle == (ulong) vehicle_row.Identifier &&
                        vehicle_row.NumberPlate != "" &&
                        vehicle_row.MakeCar != "")
                    {
                        ret = vehicle_row.MakeCar + "\"" + vehicle_row.NumberPlate + "\"";
                        if(vehicle_row.OutLinkId != "")
                            OutLinkIdTransport = vehicle_row.OutLinkId;
                        break;
                    }
                }
                catch // ����� ��� ������ ����������
                {
                }
            }
            return ret;
        } // FindVehicleName

        private int FindMobitelId(ulong idVehicle)
        {
            int ret = 0;

            foreach (atlantaDataSet.vehicleRow vehicle_row in AtlantaDataSet.vehicle)
            {
                try
                {
                    if (idVehicle == (ulong) vehicle_row.Identifier &&
                        vehicle_row.Mobitel_id != 0)
                    {
                        ret = vehicle_row.Mobitel_id;
                        break;
                    }
                }
                catch // ����� ��� ������ ����������
                { }
            }
            return ret;
        } // FindVehicleName

        private string FindDriverName(ulong idCard)
        {
            OutLinkIdDriver = "";
            string ret = starterDriverName;
            foreach (atlantaDataSet.driverRow driver_row in AtlantaDataSet.driver)
            {
                try
                {
                    ulong ident = 0;

                    if (driver_row.Identifier != null) 
                        ident = Convert.ToUInt64(driver_row.Identifier);

                    if (idCard == ident && driver_row.Family != "")
                    {
                        ret = driver_row.Family;
                        OutLinkIdDriver = driver_row.OutLinkId;
                        break;
                    }
                }
                catch //����� ��� ������ ����������
                { }
            }
            return ret;
        }

        private void FillNameFueleredCar( ref FuelingTC[] fuelingTC, atlantaDataSet.FuelerReportRow fr_row)
        {
            try
            {
                cntFuelingVehicleInfo.Clear();
                fr_row.fillUp = "";
                fr_row.TransportCodeDB = "";
                fr_row.tankFuelAdd = 0;


                foreach (FuelingTC fuel_TC in fuelingTC)
                {
                    atlantaDataSet.mobitelsRow tmpM_row = AtlantaDataSet.mobitels.FindByMobitel_ID(fuel_TC.mobitelID);
                    SelectItem(tmpM_row);
                    VehicleInfo info = new VehicleInfo(tmpM_row.Mobitel_ID, tmpM_row);
                    List<int> candidatMobitelID = new List<int>();

                    if (FindSensor(AlgorithmType.FUEL1) != null)
                        // ���� � ������� �� ���� ������ ������ ���� �������� � ���� ������.
                    {
                        // ����� ������ � ������ � ���������
                        atlantaDataSet.FuelReportRow[] fuelRep_row =
                            GetFuelReportRow(fuel_TC.mobitelID, fuel_TC.beginFueling - minDeltaTime,
                                fuel_TC.endFueling + minDeltaTime);

                        foreach (atlantaDataSet.FuelReportRow fuel_row in fuelRep_row)
                        {
                            if (fuel_row.dValue > 0) // ���� ������ �������� - ��������� ��
                            {
                                // ���������
                                AddFillUpInfo(fr_row, info);
                                fr_row.tankFuelAdd += (int) fuel_row.dValue;
                                break;
                            }
                        }

                        // ����� ������ �������� � ��������
                        if (fuelRep_row.Length == 0)
                        {
                            // ������ ���� ��������� � ������ �������� ������� ������� � ������ ��
                            CountFuelValueInStop(ref fr_row, fuel_TC, tmpM_row, info);
                        }
                    }
                    else
                    // ���� � �� ��� ������� ������ �� ���������� ��� ��� �������� � ������ ������������ �����������
                    {
                        if (fr_row.fillUp == "")
                        {
                            fr_row.fillUp = info.CarMaker + " " + info.CarModel + " " + info.RegistrationNumber + " (" +
                                            info.Identificator + ")";
                            if (info.OutLinkId != "")
                            {
                                fr_row.TransportCodeDB = info.OutLinkId;
                            }
                            else
                            {
                                fr_row.TransportCodeDB = "--";
                            }

                            cntFuelingVehicleInfo.Clear();
                            cntFuelingVehicleInfo.Add(info);
                        }
                        else if (fr_row.tankFuelAdd == 0)
                        {
                            fr_row.fillUp += "; " + info.CarMaker + " " + info.CarModel + " " + 
                                info.RegistrationNumber + " (" + info.Identificator + ")";

                            if (info.OutLinkId != "")
                            {
                                fr_row.TransportCodeDB += (", " + info.OutLinkId);
                            }
                            else
                            {
                                fr_row.TransportCodeDB += (", " + "--");
                            }

                            cntFuelingVehicleInfo.Add(info);
                        }
                    }
                } // foreach

                //if (cntFuelingVehicleInfo.Count == 1)
                SelectItem(fr_row.mobitelsRow);
                FillDriverName(fr_row, cntFuelingVehicleInfo.ToArray());
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error FillNameFueleredCar", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        } // FillNameFueleredCar

        private void CountFuelValueInStop(ref atlantaDataSet.FuelerReportRow fueler_row, FuelingTC fuel_TC,
            atlantaDataSet.mobitelsRow mob_row, VehicleInfo vehicle_info)
        {
            try
            {
                fuelerPart.LoadDataToRepository(fueler_row, fuel_TC.mobitelID);

                // ����� �������� � ������ ������ ��� ������� ��������
                GpsData[] gpsDatas = DataSetManager.GetDataGpsForPeriod(
                    mob_row, fueler_row.timeStart - minDeltaTime, fueler_row.timeEnd + minDeltaTime);

                List<int[]> listStops = new List<int[]>();
                int[] pairBeginEndStop = new int[2] { 0, 0 };
                bool startStop = false;

                for (int i = 0; i < gpsDatas.Length; i++)
                {
                    if (!startStop && gpsDatas[i].Speed == 0)
                    {
                        startStop = true;
                        pairBeginEndStop[0] = i;
                    } // if

                    if (startStop && gpsDatas[i].Speed != 0)
                    {
                        startStop = false;
                        pairBeginEndStop[1] = i - 1;
                        listStops.Add(pairBeginEndStop);
                        pairBeginEndStop = new int[2] { 0, 0 };
                    }
                } // for

                if (startStop)
                {
                    pairBeginEndStop[1] = gpsDatas.Length - 1;
                    listStops.Add(pairBeginEndStop);
                }

                //����� ����������� ������� ��� ��������� �� ��� ������� �������
                int maxTime = 0;
                TimeSpan maxTimeSpan = new TimeSpan(0, 0, 0);

                for (int i = 0; i < listStops.Count; i++)
                {
                    TimeSpan tmpTimeSpan = (gpsDatas[listStops[i][1]].Time - gpsDatas[listStops[i][0]].Time);
                    if (tmpTimeSpan > maxTimeSpan)
                    {
                        maxTimeSpan = tmpTimeSpan;
                        maxTime = i;
                    }
                }

                //���� ���� ���������, ��������� �������� ������� �� ������ ��������� 
                if (maxTimeSpan.TotalSeconds != 0)
                {
                    FuelDictionarys fuelDict = new FuelDictionarys();
                    Fuel fuel = new Fuel();
                    fuel.SetD_rows(gpsDatas, mob_row);
                    fuel.SettingAlgoritm(AlgorithmType.FUEL1);
                 
                    fuel.GettingValuesDUT(fuelDict);
                    double changeFuelValue = 0;

                    if (fuelDict.ValueFuelSum.Count > 0 && fuelDict.ValueFuelSum.Count > listStops[maxTime][1]
                        && fuelDict.ValueFuelSum.Count > listStops[maxTime][0])
                        changeFuelValue = fuelDict.ValueFuelSum[gpsDatas[listStops[maxTime][1]].Id].value -
                                          fuelDict.ValueFuelSum[gpsDatas[listStops[maxTime][0]].Id].value;

                    int fuelingEdge = 10;

                    if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
                    {
                        fuelingEdge = set_row.FuelingEdge;
                    }

                    if (changeFuelValue >= fuelingEdge)
                    {
                        AddFillUpInfo(fueler_row, vehicle_info);

                        if (mob_row.GetFuelReportRows().Length == 0)
                        {
                            fueler_row.tankFuelAdd += (int)changeFuelValue;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in CountFuelValueInStop", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// ��������� ������ � ����������� � ����������� ��, ���� ��� � ������ ���� �������� ������ ��� �������,
        /// �� ��� ����������. ���� ���� � �������� �� �������� �����������.
        /// </summary>
        /// <param name="frRow">������ ������</param>
        /// <param name="info">���������� � ������������ ��</param>
        private void AddFillUpInfo(atlantaDataSet.FuelerReportRow frRow, VehicleInfo vehicle_info)
        {
            if (frRow.fillUp != "")
            {
                frRow.fillUp += ",";
                frRow.TransportCodeDB += ",";
                if (frRow.tankFuelAdd == 0)
                {
                    frRow.fillUp = "";
                    frRow.TransportCodeDB = "";
                    cntFuelingVehicleInfo.Clear();
                }
            }

            frRow.fillUp += vehicle_info.Info;
            if (vehicle_info.OutLinkId != "")
                frRow.TransportCodeDB += vehicle_info.OutLinkId;
            else
                frRow.TransportCodeDB += "--";
            cntFuelingVehicleInfo.Add(vehicle_info);
        }

        public double[] GettingValuesDRT()
        {
            double[] ret = new double[0];
            //���������� ����� ��������� ������ � ������ �� �������, ��� ��� �� ����������������� ������� ������ �������
            //�� �����, ��� �������� ������ ��. �������������� ��� ������ ����.
            FuelRateDictionarys frDict = new FuelRateDictionarys();
            frDict.fuelRateSensor1 = FindSensor(AlgorithmType.FUELER);
            GettingValuesDRT(frDict);

            if (frDict.ValSeries1.Count > 0)
            {
                ret = new double[frDict.ValSeries1.Count];
                int k = 0;
                foreach (var val in frDict.ValSeries1)
                {
                    ret[k++] = val.Value.value;
                }
            }

            frDict.Clear();
            return ret;
        }

        public double[] GettingValuesFromDRTDiff(atlantaDataSet.sensorsRow sRow)
        {
            double[] ret = new double[0];
            //���������� ����� ��������� ������ � ������ �� �������, ��� ��� �� ����������������� ������� ������ �������
            //�� �����, ��� �������� ������ ��. �������������� ��� ������ ����.
            FuelRateDictionarys frDict = new FuelRateDictionarys();
            frDict.fuelRateSensor1 = sRow;
            frDict.TypeAlgorithm1 = (int)AlgorithmType.FUELDIFFDRT;
            GettingValuesDRTDiff(frDict);

            if (frDict.ValSeries1.Count > 0)
            {
                ret = new double[frDict.ValSeries1.Count];
                int k = 0;
                foreach (var val in frDict.ValSeries1)
                {
                    ret[k++] = val.Value.value;
                }
            }

            frDict.Clear();
            return ret;
        }

        public double[] GettingValuesFromDRT(atlantaDataSet.sensorsRow sRow)
        {
            double[] ret = new double[0];
            //���������� ����� ��������� ������ � ������ �� �������, ��� ��� �� ����������������� ������� ������ �������
            //�� �����, ��� �������� ������ ��. �������������� ��� ������ ����.
            FuelRateDictionarys frDict = new FuelRateDictionarys();
            frDict.fuelRateSensor1 = sRow;
            GettingValuesDRT(frDict);

            if (frDict.ValSeries1.Count > 0)
            {
                ret = new double[frDict.ValSeries1.Count];
                int k = 0;
                foreach (var val in frDict.ValSeries1)
                {
                    ret[k++] = val.Value.value;
                }
            }

            frDict.Clear();
            return ret;
        }

        #region --   Nested struct Fuel.Summary   --

        /// <summary>
        /// ������������ �������� ������ ������ � ��������� � ������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        public struct Summary
        {
            /// <summary>
            /// ������ ������� � ������ ��������� �������
            /// </summary>
            //public double Begin;
            /// <summary>
            /// ������ ������� � ����� ��������� �������
            /// </summary>
            //public double End;
            /// <summary>
            /// ���������� ������� ��������������� �� �������� ������ ��
            /// </summary>
            public double FuelRate;

            /// <summary>
            /// ���������� ������� �� �������� � �����, �� �������� ������������ �������
            /// </summary>
            public double UnrecordedFuelRate;
        }

        #endregion
    }

    public class FuelingTC
    {
        public int mobitelID;
        public DateTime beginFueling;
        public DateTime endFueling;
    }

    /// <summary>
    /// ������������� ��������� ��� ����������� ������� ���������
    /// ������ ��������� ���������� ������ ��� ������ � ���������
    /// �� ����������� ������������� ��������
    /// </summary>
    public class FuelerEventArgs : EventArgs
    {
        /// <summary>
        /// ������� � �������������� ����� ��������� ������ BaseReports.Procedure.FuelerEventArgs
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public FuelerEventArgs(int id, Fueler.Summary summary)
        {
            _id = id;
            _summary = summary;
        }

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        private int _id;

        /// <summary>
        /// �������� ������ ������ � ��������� �� ����������.
        /// </summary>
        public Fueler.Summary Summary
        {
            get { return _summary; }
        }

        private Fueler.Summary _summary;
    }
}