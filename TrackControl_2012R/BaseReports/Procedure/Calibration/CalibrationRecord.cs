﻿using System;
using TrackControl.General;

namespace BaseReports.Procedure.Calibration
{
    public class CalibrationRecord 
    {
        public CalibrationRecord()
        {

        }

        public CalibrationRecord(long id, double userValueInp, double sensorValueInp, double kInp, double bInp)
        {
            Id = id;
            UserValue = userValueInp;
            SensorValue = sensorValueInp;
            K = kInp;
            b = bInp;
        }

        public CalibrationRecord(int id)
        {
            //VehicleCategory vc = VehicleCategoryProvader.GetCategory(id);
            //if (vc != null)
            //{
            //    Id = vc.Id;
            //    Name = vc.Name;
            //    Tonnage = vc.Tonnage;
            //    FuelNormMoving = vc.FuelNormMoving;
            //    FuelNormParking = vc.FuelNormParking;
            //}
        }


        public double UserValue { get; set; }

        public double SensorValue { get; set; }

        public double K { get; set; }

        public double b { get; set; }

        public long Id { get; set; }

        public bool Save(long agregatId = 0)
        {
            if (Id == 0 && agregatId >0)
            {
                Id = CalibrationProvider.InsertAgro(agregatId,this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            if (Id > 0)
            {
                return CalibrationProvider.UpdateAgro(this);
            }
            return false;
        }

        public bool Delete()
        {
            return CalibrationProvider.DeleteAgro(this);
            return true;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", UserValue, SensorValue);
        }
    }
}
