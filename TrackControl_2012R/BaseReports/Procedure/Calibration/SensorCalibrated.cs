﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseReports;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraEditors;

namespace TrackControl.Vehicles
{
    public class SensorCalibrated : Sensor 
    {
        Calibrate _calibrate;
        public SensorCalibrated(string name, string description, int startBit, int bitLength, double k, double b, bool s)
        : base(name, description, startBit, bitLength, k, b, s)
        {
                
        }

        public SensorCalibrated(int idSensor)
            : base(idSensor)
        {
            if (idSensor > 0) SetCalibration();
        }

        public SensorCalibrated(int idAlgoritm, int idMobitel)
            : base(idAlgoritm, idMobitel)
        {
            if (this.Id >0) SetCalibration();
        }

        public SensorCalibrated (int idAlgoritm, int idMobitel,IEnumerable<CalibrationRecord> records)
            : base(idAlgoritm, idMobitel)
        {
            if (this.Id > 0) SetCalibrationExternalRecords(records);
        }

        private void SetCalibration()
        {
            _calibrate = new Calibrate();
            CalibrationProvider.SetCalibration(this.Id, ref _calibrate);
        }

        private void SetCalibrationExternalRecords(IEnumerable<CalibrationRecord> records)
        {
            _calibrate = new Calibrate();
            foreach (var record in records)
            {
                var coefficient = new Coefficient {User = record.UserValue, K = record.K, b = record.b};
                _calibrate.Collection.Add(record.SensorValue, coefficient);
            }

        }

        public SensorCalibrated(Sensor sensor)
             : base(sensor)
        {
            SetCalibration();
        }

        public SensorCalibrated(Sensor sensor, IEnumerable<CalibrationRecord> records)
            : base(sensor)
        {
            SetCalibrationExternalRecords(records);
        }

        public override double GetValue(byte[] sensorDataGps)
        {
            if (Valid)
            {
                if (_calibrate == null) SetCalibration();
                return Math.Round(_calibrate.GetUserValue(sensorDataGps, BitLength, StartBit, K, B, S), 2); 
            }
            else
                return 0;
        }
    }
}
