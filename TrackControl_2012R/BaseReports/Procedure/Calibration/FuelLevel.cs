﻿using System;
using System.Collections;
using System.Collections.Generic;
using BaseReports.Properties;
using TrackControl.Vehicles ;
using TrackControl.Reports ;
using TrackControl.General.DatabaseDriver;


namespace BaseReports.Procedure.Calibration
{

    public static class FuelLevel
    {

        public static double GetFuelValueOnPoint(Vehicle vh, GpsData gpsData)
        {
            return GetFuelValueOnPoint(vh.MobitelId, gpsData);
        }

        public static double GetFuelValueOnPoint(int MobitelId, GpsData gpsData)
        {
            double fuelLevel = 0;
            Sensor sensor1 = new Sensor((int) AlgorithmType.FUEL1, MobitelId);
            if (sensor1 != null)
            {
                Sensor sensor2 = new Sensor((int) AlgorithmType.FUEL1, MobitelId, sensor1.Id);
                fuelLevel = GetValueFromSensor(sensor1, gpsData);
                if (sensor2 != null) fuelLevel += GetValueFromSensor(sensor2, gpsData);
            }
            return Math.Round(fuelLevel, 2);
        }

        public static double GetValueFromSensor(Sensor calibrSensor, GpsData gpsData)
        {
            Calibrate calibrate = new Calibrate();
            CalibrationProvider.SetCalibration(calibrSensor.Id, ref calibrate);
            return
                Math.Round(
                    calibrate.GetUserValue(gpsData.Sensors, calibrSensor.BitLength, calibrSensor.StartBit,
                        calibrSensor.K, calibrSensor.B, calibrSensor.S), 2);
        }
    }
}
