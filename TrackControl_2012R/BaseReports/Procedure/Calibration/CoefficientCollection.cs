using System;
using System.Collections.Generic;

namespace BaseReports.Procedure.Calibration
{
  public class CoefficientCollection : SortedUniqueDictionary<double, Coefficient>
  {
  }

  #region SortedUniqueDictionary
  /// <summary>
  /// ����������� ���������� ��������� ��������� ��������.
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  [SerializableAttribute]
  public class SortedUniqueDictionary<TKey, TValue> : SortedDictionary<TKey, TValue>
  {
    public new TValue this[TKey key]
    {
      get { return base[key]; }
      set
      {
        if (base.ContainsKey(key))
        {
          base.Remove(key);
          base.Add(key, value);
        }
        else
        {
          base[key] = value;
        }
      }
    }

    public new void Add(TKey key, TValue value)
    {
      if (base.ContainsKey(key))
      {
        base.Remove(key);
      }
      base.Add(key, value);
    }

    /// <summary>
    /// This override method support value equality.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
      SortedUniqueDictionary<TKey, TValue> sud = obj as SortedUniqueDictionary<TKey, TValue>;
      if (sud == null) return false;
      if (sud.Count != this.Count) return false;
      foreach (TKey key in this.Keys)
      {
        if (!sud.ContainsKey(key)) return false;
        if (!sud[key].Equals(this[key])) return false;
      }
      return true;
    }

    public override int GetHashCode()
    {
      /*
       * ����� ����������.
       */
      return base.GetHashCode();
    }
  }

  #endregion
}
