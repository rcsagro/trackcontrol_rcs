
namespace BaseReports.Procedure.Calibration
{
  public struct Coefficient
  {
    public double User;
    public double K;
    public double b;

    public Coefficient(double user, double k, double b)
    {
      User = user;
      K = k;
      this.b = b;
    }

    public Coefficient(Coefficient coefficient)
    {
      User = coefficient.User;
      K = coefficient.K;
      b = coefficient.b;
    }
  }
}
