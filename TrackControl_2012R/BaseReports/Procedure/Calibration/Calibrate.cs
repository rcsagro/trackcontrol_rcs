using BaseReports.Properties;
//using DevExpress.XtraEditors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using DevExpress.XtraEditors;

namespace BaseReports.Procedure.Calibration
{
    /// <summary>
    /// ����� ��� ������� ������������� ������������� �������.
    /// </summary>
    public class Calibrate
    {
        public CoefficientCollection Collection = new CoefficientCollection();

        public double GetUserValue(byte[] datagpsSensor, long length, long position, double K, double B, bool S)
        {
            double sensorValue = 0;
            if(S)
                sensorValue = Convert.ToDouble(ulongSector(datagpsSensor, length, position, S));
            else
                sensorValue = Convert.ToDouble(ulongSector(datagpsSensor, length, position));

            return CalcUserValue(sensorValue, K, B);
        }

        //public double GetUserValue(byte[] datagpsSensor, long length, long position, double K)
        //{
            //double sensorValue = Convert.ToDouble(ulongSector(datagpsSensor, length, position));

            //return CalcUserValue(sensorValue, K);
        //}

        public double GetUserValueSuppressHighZero(byte[] datagpsSensor, int length, int position, double K, double B, bool S)
        {
            double sensorValue = 0;
            if (S)
            {
                sensorValue = Convert.ToDouble(ulongSector(datagpsSensor, length, position, S));
            }
            else
            {
                sensorValue = Convert.ToDouble(ulongSector(datagpsSensor, length, position));
            }
            if (sensorValue == Math.Pow(2, length) - 1) sensorValue = 0;
            return CalcUserValue(sensorValue, K, B);
        }

        /// <summary>
        /// �������� ������������� ���������.
        /// </summary>
        /// <returns>�������, ���������� �� ����� ���������
        /// ������ � ���������.</returns>
        public bool Recalculation()
        {
            bool changed = false;
            if (Collection.Count == 1)
            {
                SortedDictionary<double, Coefficient>.Enumerator enumerator = Collection.GetEnumerator();
                enumerator.MoveNext();
                KeyValuePair<double, Coefficient> A = enumerator.Current;

                Coefficient coefficient = new Coefficient(A.Value.User, (A.Value.User/A.Key), 0);
                changed = !coefficient.Equals(Collection[A.Key]);
                Collection[A.Key] = coefficient;
            }
            else if (Collection.Count > 0)
            {
                CoefficientCollection newCollection = new CoefficientCollection();
                KeyValuePair<double, Coefficient> A, B;
                double K = 0;
                double b = 0;

                SortedDictionary<double, Coefficient>.Enumerator enumerator = Collection.GetEnumerator();
                enumerator.MoveNext();
                B = enumerator.Current;

                while (enumerator.MoveNext())
                {
                    A = B;
                    B = enumerator.Current;

                    K = (B.Value.User - A.Value.User)/(B.Key - A.Key);
                    b = A.Value.User - K*A.Key;

                    newCollection.Add(A.Key, new Coefficient(A.Value.User, K, b));
                }
                newCollection.Add(B.Key, new Coefficient(B.Value.User, K, b));
                changed = !newCollection.Equals(Collection);
                Collection = newCollection;
            }
            return changed;
        }

        public static Int64 ulongSector(byte[] sensors, long length, long position, bool S)
        {
            try
            {
                if (length < 0 || sensors == null)
                {
                    return 0;
                }
                if (position < 0)
                {
                    return 0;
                }
                ulong res = 0;
                BitArray bits = new BitArray(sensors);
                for (int i = 0; i < length; i++)
                {
                    ulong d = Convert.ToUInt64(bits[i + (int)position]);
                    res += d << i;
                }

                Int64 val = 0;
                if (S)
                {
                    double valbits = Math.Pow(2.0, (double)length);
                    ulong mask = 1;
                    mask <<= ((int)length - 1);
                    if ((res & mask) > 0)
                    {
                        val = ((Int64)res - (Int64)valbits);
                    }
                    else
                    {
                        val = (Int64)res;
                    }
                }

                return val;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error calibrate", MessageBoxButtons.OK);
                return 0;
            }
        } // ulongSector(byte[] sensors, long length, long position, byte S)

        public static ulong ulongSector(byte[] sensors, long length, long position)
        {
            try
            {
                if (length < 0 || sensors == null)
                {
                    return 0;
                }
                if (position < 0)
                {
                    return 0;
                }
                ulong res = 0;
                BitArray bits = new BitArray(sensors);
                for (int i = 0; i < length; i++)
                {
                    int index = i + (int)position;
                    ulong d = 0;
                    if (index < bits.Count)
                    {
                        d = Convert.ToUInt64(bits[index]);
                        res += d << i;
                    }
                }

                return res;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error calibrate", MessageBoxButtons.OK);
                return 0;
            }
        } // ulongSector(byte[] sensors, long length, long position)

        private double CalcUserValue(double sensorValue, double K, double B)
        {
            if (Collection.Count == 0)
            {
                double value = K * sensorValue + B;
                return value;
            }

            if (Collection.ContainsKey(sensorValue))
                return Collection[sensorValue].User;

            KeyValuePair<double, Coefficient> firstPair, leftPair;
            Coefficient C;

            SortedDictionary<double, Coefficient>.Enumerator enumerator = Collection.GetEnumerator();
            enumerator.MoveNext();
            firstPair = enumerator.Current;

            if (sensorValue < firstPair.Key)
                return firstPair.Value.User;

            leftPair = firstPair;

            while (enumerator.MoveNext())
            {
                if (sensorValue < enumerator.Current.Key)
                {
                    C = leftPair.Value;
                    return (C.K * sensorValue + C.b);
                }

                leftPair = enumerator.Current;
            }

            return leftPair.Value.User;
        } // CalcUserValue(double sensorValue, double K, double B)

        //private double CalcUserValue(double sensorValue, double K)
        //{
        //    if (Collection.Count == 0)
        //        return K * sensorValue;

        //    if (Collection.ContainsKey(sensorValue))
        //        return Collection[sensorValue].User;

        //    KeyValuePair<double, Coefficient> firstPair, leftPair;
        //    Coefficient C;

        //    SortedDictionary<double, Coefficient>.Enumerator enumerator = Collection.GetEnumerator();
        //    enumerator.MoveNext();
        //    firstPair = enumerator.Current;

        //    if (sensorValue < firstPair.Key)
        //        return firstPair.Value.User;

        //    leftPair = firstPair;

        //    while (enumerator.MoveNext())
        //    {
        //        if (sensorValue < enumerator.Current.Key)
        //        {
        //            C = leftPair.Value;
        //            return (C.K * sensorValue + C.b);
        //        }

        //        leftPair = enumerator.Current;
        //    }

        //    return leftPair.Value.User;
        //}
    }
}
