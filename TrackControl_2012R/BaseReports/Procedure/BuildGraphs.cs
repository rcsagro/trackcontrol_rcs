using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    public class Diapasone
    {
        public int begin = 0;
        public int end = 0;
    }

    public class BuildGraphs : IBuildGraphs
    {
        private Color _colorInclinometerX = Color.DarkBlue;
        private Color _colorInclinometerY = Color.DarkCyan;
        private Color _colorInclinometerDanger = Color.Aquamarine;
        static private bool GraphFuelShow = false;
        private IDictionary<Int32, string> sensorsNameDict1 = new Dictionary<int, string>();
        private IDictionary<Int32, string> sensorsUnitDict1 = new Dictionary<int, string>();
        private IDictionary<Int32, string> sensorsNameDict2 = new Dictionary<int, string>();
        private IDictionary<Int32, string> sensorsUnitDict2 = new Dictionary<int, string>();

        private AlgorithmType algType;

        int greene = 40; // ��������� ����������� ����
        int SandyBrown = 150;
        int Yallow = 10;
        int Brown = 80;

        public void SetAlgorithm(object algorithm)
        {
            if (algorithm is AlgorithmType)
            {
                algType = (AlgorithmType) algorithm;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }
        }

        // ����� ������������ ������� �� �����
        public void AddGraphFuel(object alg_type, IGraph graph, atlantaDataSet atlDataSet,
            atlantaDataSet.mobitelsRow mob_row)
        {
            FuelDictionarys fuelDict = new FuelDictionarys();
            Fuel fuel = new Fuel();
            fuel.SettingAlgoritm(alg_type);
            fuel.SelectItem(mob_row);
            fuel.GettingValuesDUT(fuelDict);

            //bool twoSensnors = fuelDict.ValueFuelSensor2.Count > 0;

            int i = 0;
            int number = 0;

            bool ShowGraph = BuildGraphs.IsGraphFuelShow; // �������� � ����������

            // ���������� ������� ��� ����� ������� � ��������
            for (i = 0; i < fuelDict.valueFuelArrayDict.Count; i++)
            {
                if (fuelDict.fuelSensors[i] != null)
                {
                    if (fuelDict.fuelSensors[i].Length > 0)
                    {
                        //�������� ����� ������ �� ������� �������
                        ValuePair[] value = new ValuePair[fuelDict.valueFuelArrayDict[i].Count];
                        fuelDict.valueFuelArrayDict[i].Values.CopyTo(value, 0);

                        greene = greene + 50;

                        if (greene > 255)
                            greene = greene - 255;

                        bool ShowGraphOne = false;
                        if (fuelDict.valueFuelArrayDict.Count > 1)
                            ShowGraphOne = ShowGraph;
                        else
                            ShowGraphOne = true;

                        timeMeanFuel = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                        valueMeanFuel = ValuePair.GetAvgValue(value);

                        // ������ ������ ��������
                        graph.AddSeriesL(
                            String.Format(Resources.BuildGraphsAvg, fuelDict.fuelSensors[i].Name,
                                fuelDict.fuelSensors[i].NameUnit),
                            Color.FromArgb(255, 0, greene, 0), valueMeanFuel, timeMeanFuel,
                            (AlgorithmType) alg_type + number++, ShowGraphOne);

                        SandyBrown = SandyBrown + 100;

                        if (SandyBrown > 255)
                            SandyBrown = SandyBrown - 255;

                        timeRealFuel = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                        valueRealFuel = ValuePair.GetValue(value);

                        // ������ ������ ����� ������
                        graph.AddSeriesL(
                            String.Format("{0} ({1})", fuelDict.fuelSensors[i].Name,
                                fuelDict.fuelSensors[i].NameUnit),
                            Color.FromArgb(255, 39, SandyBrown, 129), valueRealFuel,
                            timeRealFuel, (AlgorithmType) alg_type + number++, ShowGraphOne);

                    } // if
                } // if
            } // for

            if (fuelDict.valueFuelArrayDict.Count > 1)
            {
                //������ ������ ��� ����� �������� �� ������� ����
                if (fuelDict.ValueFuelSum.Count > 0)
                {
                    ValuePair[] value = new ValuePair[fuelDict.ValueFuelSum.Count];
                    fuelDict.ValueFuelSum.Values.CopyTo(value, 0);

                    Brown = Brown + 50;
                    if (Brown > 255)
                        Brown = Brown - 255;

                    // ������ ������ ��������
                    string title = Resources.BuildGraphsTotalFuelAvg;
                    if ((int)alg_type == (int) AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        title = Resources.BuildGraphsTotalFuelAvgAgregat;
                    }

                    graph.AddSeriesL(
                        String.Format(title, fuelDict.fuelSensors[0].NameUnit),
                        Color.FromArgb(255, 39, Brown, 129), ValuePair.GetAvgValue(value),
                        Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        (AlgorithmType) alg_type + number++, true);

                    Yallow = Yallow + 100;
                    if (Yallow > 255)
                        Yallow = Yallow - 255;

                    // ������ ������ ��� ��������� (����� ������)
                    string stitle = Resources.BuildGraphsTotalFuel;
                    if ((int)alg_type == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        stitle = Resources.BuildGraphsTotalFuelAgregat;
                    }

                    graph.AddSeriesL(
                        String.Format(stitle, fuelDict.fuelSensors[0].NameUnit),
                        Color.FromArgb(255, 39, Yallow, 129), ValuePair.GetValue(value), Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        (AlgorithmType) alg_type + number++, true);
                } // if
            } // if

            fuelDict.Clear();
        } // AddGraphFuel

        // ����� ������������ ������� �� ��������� ����������� �����
        public void AddGraphGrain(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            GrainDictionarys grainDict = new GrainDictionarys();
            Grain grain = new Grain();

            grain.SettingAlgoritm(algType);
            grain.SelectItem(mob_row);
            grain.GettingValuesDUT(grainDict);

            //bool twoSensnors = grainDict.ValuegrainSensor2.Count > 0;

            int greene = 40; // ��������� ����������� ����
            int SandyBrown = 150;

            int i = 0;
            int number = 0;

            bool ShowGraph = BuildGraphs.IsGraphFuelShow; // �������� � ����������

            // ���������� ������� ��� ����� ������� � ��������
            for (i = 0; i < grainDict.valueGrainArrayDict.Count; i++)
            {
                if (grainDict.GrainSensors[i] != null)
                {
                    if (grainDict.GrainSensors[i].Length > 0)
                    {
                        //�������� ����� ������ �� ������� �������
                        ValuePairs[] value = new ValuePairs[grainDict.valueGrainArrayDict[i].Count];
                        grainDict.valueGrainArrayDict[i].Values.CopyTo(value, 0);

                        greene = greene + 50;

                        if (greene > 255)
                            greene = greene - 255;

                        bool ShowGraphOne = false;
                        if (grainDict.valueGrainArrayDict.Count > 1)
                            ShowGraphOne = ShowGraph;
                        else
                            ShowGraphOne = true;

                        // ������ ������ ��������
                        graph.AddSeriesL(
                            String.Format(Resources.BuildGraphsAvg, grainDict.GrainSensors[i].Name,
                                grainDict.GrainSensors[i].NameUnit),
                            Color.FromArgb(255, 0, greene, 0), ValuePairs.GetAvgValue(value),
                            Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                            AlgorithmType.GRAIN1 + number++, ShowGraphOne);

                        SandyBrown = SandyBrown + 50;

                        if (SandyBrown > 255)
                            SandyBrown = SandyBrown - 255;

                        // ������ ������ ����� ������
                        graph.AddSeriesL(
                            String.Format("{0} ({1})", grainDict.GrainSensors[i].Name,
                                grainDict.GrainSensors[i].NameUnit),
                            Color.FromArgb(255, 39, SandyBrown, 129), ValuePairs.GetValue(value),
                            Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                            AlgorithmType.GRAIN1 + number++, ShowGraphOne);

                    } // if
                } // if
            } // for

            if (grainDict.valueGrainArrayDict.Count > 1)
            {
                //������ ������ ��� ����� �������� �� ������� ����
                if (grainDict.ValueGrainSum.Count > 0)
                {
                    ValuePairs[] value = new ValuePairs[grainDict.ValueGrainSum.Count];
                    grainDict.ValueGrainSum.Values.CopyTo(value, 0);

                    // ������ ������ ��������
                    // ������ ������ ��� ��������� (����� ������)
                    string dtitle = Resources.BuildGraphsTotalFuelAvg;
                    if ((int)algType == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        dtitle = Resources.BuildGraphsTotalFuelAvgAgregat;
                    }

                    graph.AddSeriesL(
                        String.Format(dtitle, grainDict.GrainSensors[0].NameUnit),
                        Color.BlueViolet, ValuePairs.GetAvgValue(value),
                        Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        AlgorithmType.GRAIN1 + number++, true);

                    // ������ ������ ��� ��������� (����� ������)
                    string rtitle = Resources.BuildGraphsTotalFuel;
                    if ((int)algType == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        rtitle = Resources.BuildGraphsTotalFuelAgregat;
                    }

                    graph.AddSeriesL(
                        String.Format(rtitle, grainDict.GrainSensors[0].NameUnit),
                        Color.Maroon, ValuePairs.GetValue(value), Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        AlgorithmType.FUEL1 + number++, true);
                } // if
            } // if

            grainDict.Clear();
        } // AddGraphFuel

        static public bool IsGraphFuelShow
        {
            get { return GraphFuelShow; }
            set { GraphFuelShow = value; }
        }

        // ����� ������������ ������� �� �����
        public void AddGraphFlowmeter(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            try
            {
                FlowMeter fuelRate = FlowMeter.GetFlowMeter();
                FuelRateDictionarys frDict = fuelRate.GetFuelRateDictionarys;
                fuelRate.SelectItem(mob_row);

                if (frDict.ValSeriesData1[mob_row.Mobitel_ID].Count > 0)
                {
                    int i = 0;
                    double[] value = new double[frDict.ValSeriesData1[mob_row.Mobitel_ID].Count];
                    foreach (var val in frDict.ValSeriesData1[mob_row.Mobitel_ID])
                    {
                        value[i++] = val.Value.value;
                    }

                    DateTime[] arrayTime = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                    string nameSensor = "";
                    string nameUnit = "";
                    try
                    {
                        nameSensor = frDict.SensorsDict1[mob_row.Mobitel_ID].Name;
                        nameUnit = frDict.SensorsDict1[mob_row.Mobitel_ID].NameUnit;

                        if (sensorsNameDict1.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsNameDict1.Remove(mob_row.Mobitel_ID);
                            sensorsNameDict1.Add(mob_row.Mobitel_ID, nameSensor);
                        }
                        else
                        {
                            sensorsNameDict1.Add(mob_row.Mobitel_ID, nameSensor);
                        }

                        if (sensorsUnitDict1.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsUnitDict1.Remove(mob_row.Mobitel_ID);
                            sensorsUnitDict1.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                        else
                        {
                            sensorsUnitDict1.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                    }
                    catch (Exception e)
                    {
                        nameSensor = sensorsNameDict1[mob_row.Mobitel_ID];
                        nameUnit = sensorsUnitDict1[mob_row.Mobitel_ID];
                    }

                    graph.AddSeriesL(String.Format("{0} ({1})", nameSensor, nameUnit), Color.DarkGreen, value, arrayTime,
                        AlgorithmType.FUEL2);
                }

                if (frDict.ValSeriesData2[mob_row.Mobitel_ID].Count > 0)
                {
                    int i = 0;
                    double[] value = new double[frDict.ValSeriesData2[mob_row.Mobitel_ID].Count];
                    foreach (var val in frDict.ValSeriesData2[mob_row.Mobitel_ID])
                    {
                        value[i++] = val.Value.value;
                    }

                    DateTime[] arrayTime = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                    string nameSensor = "";
                    string nameUnit = "";
                    try
                    {
                        nameSensor = frDict.SensorsDict2[mob_row.Mobitel_ID].Name;
                        nameUnit = frDict.SensorsDict2[mob_row.Mobitel_ID].NameUnit;

                        if (sensorsNameDict2.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsNameDict2.Remove(mob_row.Mobitel_ID);
                            sensorsNameDict2.Add(mob_row.Mobitel_ID, nameSensor);
                        }
                        else
                        {
                            sensorsNameDict2.Add(mob_row.Mobitel_ID, nameSensor);
                        }

                        if (sensorsUnitDict2.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsUnitDict2.Remove(mob_row.Mobitel_ID);
                            sensorsUnitDict2.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                        else
                        {
                            sensorsUnitDict2.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                    }
                    catch (Exception e)
                    {
                        nameSensor = sensorsNameDict2[mob_row.Mobitel_ID];
                        nameUnit = sensorsUnitDict2[mob_row.Mobitel_ID];
                    }

                    graph.AddSeriesL(String.Format("{0} ({1})", nameSensor, nameUnit), Color.LawnGreen, value, arrayTime,
                        AlgorithmType.FUEL2 + 1);
                }

                if (frDict.ValueSeriesDataSum[mob_row.Mobitel_ID].Count > 0)
                {
                    int i = 0;
                    double[] sumValue = new double[frDict.ValueSeriesDataSum[mob_row.Mobitel_ID].Count];
                    foreach (var val in frDict.ValueSeriesDataSum[mob_row.Mobitel_ID])
                    {
                        sumValue[i++] = val.Value;
                    }

                    DateTime[] arrayTime = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                    string nameSensor = "";
                    string nameUnit = "";
                    try
                    {
                        nameSensor = frDict.SensorsDict1[mob_row.Mobitel_ID].Name;
                        nameUnit = frDict.SensorsDict1[mob_row.Mobitel_ID].NameUnit;

                        if (sensorsNameDict1.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsNameDict1.Remove(mob_row.Mobitel_ID);
                            sensorsNameDict1.Add(mob_row.Mobitel_ID, nameSensor);
                        }
                        else
                        {
                            sensorsNameDict1.Add(mob_row.Mobitel_ID, nameSensor);
                        }

                        if (sensorsUnitDict1.ContainsKey(mob_row.Mobitel_ID))
                        {
                            sensorsUnitDict1.Remove(mob_row.Mobitel_ID);
                            sensorsUnitDict1.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                        else
                        {
                            sensorsUnitDict1.Add(mob_row.Mobitel_ID, nameUnit);
                        }
                    }
                    catch (Exception e)
                    {
                        nameSensor = sensorsNameDict1[mob_row.Mobitel_ID];
                        nameUnit = sensorsUnitDict1[mob_row.Mobitel_ID];
                    }

                    graph.AddSeriesL(
                        String.Format(Resources.BuildGraphsTotal, nameUnit), Color.Blue,
                        sumValue, arrayTime, AlgorithmType.FUEL2 + 2);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in BuildGraphs", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        // ����� ��������� � ������ ������ ���������� ��� ����������
        private double[] DiffCorrectAverage(double[] sdata)
        {
            int numPoint = 1;
            double[] sdiff = new double[sdata.Length];

            for (int k = 0; k < numPoint; k++)
            {
                sdiff[k] = 0;
            }

            for (int i = numPoint; i < sdata.Length; i++)
            {
                double vn = (sdata[i - numPoint] - sdata[i])/numPoint;
                sdiff[i] = vn*vn;
            }

            double limit = 150; // ��������� ��������� ���������
            double width = 25; // ��������� ��������� ���������

            List<Diapasone> lstPoints = new List<Diapasone>();
            Diapasone dps = new Diapasone();
            dps.begin = 0;
            dps.end = sdiff.Length - 1;
            int l;
            bool isFlagEnd = false;

            for (int j = 0; j < sdiff.Length; j++)
            {
                isFlagEnd = true;

                for (int g = j; g < sdiff.Length; g++) // ���� �������� ����� �������� ���������
                {
                    if (sdiff[g] >= limit)
                    {
                        isFlagEnd = false;
                        dps.end = g;

                        if (((dps.end - 1) - (dps.begin + 1)) >= width)
                            lstPoints.Add(dps);

                        for (l = g; l < sdiff.Length; l++)
                        {
                            if (sdiff[l] < limit)
                            {
                                dps = new Diapasone(); // ���� ��������� ����� ���������� ���������
                                dps.begin = l;
                                dps.end = sdiff.Length - 1;
                                j = l;
                                break;
                            } // if
                        } // for

                        j = l;
                        break;
                    } // if
                } // for

                if (isFlagEnd)
                    break;
            } // for

            double[] pond = new double[sdata.Length];
            for (int i = 0; i < sdata.Length; i++)
            {
                pond[i] = sdata[i];
            }

            for (int p = 0; p < lstPoints.Count; p++)
            {
                double mm = 0.0;
                for (int b = lstPoints[p].begin; b < lstPoints[p].end; b++)
                {
                    mm += pond[b];
                }
                mm = mm/(lstPoints[p].end - lstPoints[p].begin);
                for (int b = lstPoints[p].begin; b < lstPoints[p].end; b++)
                {
                    pond[b] = mm;
                }
            }

            return pond;
        }

        int globColor = 0;
        private DateTime[] timeRealFuel;
        private DateTime[] timeMeanFuel;
        private double[] valueMeanFuel;
        private double[] valueRealFuel;

        public double[] ValueMeanFuel
        {
            get { return valueMeanFuel; }
        }

        public double[] ValueRealFuel
        {
            get { return valueRealFuel; }
        }

        public DateTime[] TimeMeanFuel
        {
            get { return timeMeanFuel; }
        }

        public DateTime[] TimeRealFuel
        {
            get { return timeRealFuel; }
        }


        public void AddGraphDalnomer(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            Color[] color =
            {
                Color.Green, Color.Red, Color.Brown, Color.DodgerBlue, Color.Fuchsia, Color.Blue,
                Color.BlueViolet, Color.Crimson, Color.Chartreuse, Color.Aqua
            };
            Algorithm tmpAlgorithm = new Algorithm();
            tmpAlgorithm.SelectItem(mob_row);
            atlantaDataSet.sensorsRow[] s_rows = tmpAlgorithm.FindSensors(AlgorithmType.RANGEFINDER);

            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                //atlantaDataSet.
                //Row[] dv_row = mob_row.GetdataviewRows();
                double[] data = new double[Algorithm.GpsDatas.Length];
                tmpAlgorithm.S_row = s_row;
                Calibrate calibrate = tmpAlgorithm.Calibration();

                for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
                {
                    data[j] = calibrate.GetUserValue(Algorithm.GpsDatas[j].Sensors, (int) s_row.Length,
                        (int) s_row.StartBit, s_row.K, s_row.B, s_row.S);
                }

                // ===== aketner 06.02.2017 ====================
                // ����� ������������ ������ ������������������� �� ���������
                double[] arrpond = null;
                arrpond = DiffCorrectAverage(data);
                // ===============================================

                Random rd = new Random();


                graph.AddSeriesL(
                    String.Format("{0} ({1})", s_row.Name, s_row.NameUnit),
                    color[rd.Next(0, color.Length)],
                    data,
                    Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    AlgorithmType.RANGEFINDER + globColor*20); //+20*i - ��� ������������ � ������ ������ ��������

                // ==== aketner 06.02.2017 ========
                if (arrpond != null)
                {
                    graph.AddSeriesL(
                        String.Format("{0} ({1})", s_row.Name, "Average"),
                        color[rd.Next(0, color.Length)],
                        arrpond,
                        Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        AlgorithmType.RANGEFINDER + globColor*20 + 1);
                        //+20*i - ��� ������������ � ������ ������ ��������
                }
                //======================================================

                globColor++;
            }
        }

        public void AddGraphAngle(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            Color[] color = {Color.Green, Color.Red, Color.Brown, Color.DodgerBlue, Color.Fuchsia};
            Algorithm tmpAlgorithm = new Algorithm();
            tmpAlgorithm.SelectItem(mob_row);
            atlantaDataSet.sensorsRow[] s_rows = tmpAlgorithm.FindSensors(AlgorithmType.ANGLE);
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                //atlantaDataSet.
                //Row[] dv_row = mob_row.GetdataviewRows();
                double[] data = new double[Algorithm.GpsDatas.Length];
                tmpAlgorithm.S_row = s_row;
                Calibrate calibrate = tmpAlgorithm.Calibration();
                for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
                {
                    data[j] = calibrate.GetUserValue(Algorithm.GpsDatas[j].Sensors, (int) s_row.Length,
                        (int) s_row.StartBit, s_row.K, s_row.B, s_row.S);
                }

                // ===== aketner 11.09.2017 ====================
                // ����� ������������ ������ ����������� �������� �� ��������
                double[] arrpond = null;
                arrpond = DiffCorrectAverage(data);
                // ===============================================

                graph.AddSeriesL(
                    String.Format("{0} ({1})", s_row.Name, s_row.NameUnit),
                    color[globColor%5],
                    data,
                    Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    AlgorithmType.ANGLE + globColor*20); //+20*i - ��� ������������ � ������ ������ ��������

                // ==== aketner 11.09.2017 ========
                if (arrpond != null)
                {
                    graph.AddSeriesL(
                        String.Format("{0} ({1})", s_row.Name, "Average"),
                        color[globColor%5],
                        arrpond,
                        Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                        AlgorithmType.ANGLE + globColor*20 + 1); //+20*i - ��� ������������ � ������ ������ ��������
                }
                //======================================================

                globColor++;
            }
        }

        public void AddGraphTemperature(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            Color[] color = {Color.Green, Color.Red, Color.Brown, Color.DodgerBlue, Color.Fuchsia};
            int[] arrpond = null;
            Algorithm tmpAlgorithm = new Algorithm();
            tmpAlgorithm.SelectItem(mob_row);
            atlantaDataSet.sensorsRow[] s_rows = tmpAlgorithm.FindSensors(AlgorithmType.TEMP1);
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                //atlantaDataSet.
                //Row[] dv_row = mob_row.GetdataviewRows();
                double[] data = new double[Algorithm.GpsDatas.Length];
                tmpAlgorithm.S_row = s_row;
                Calibrate calibrate = tmpAlgorithm.Calibration();
                for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
                {
                    data[j] = calibrate.GetUserValue(Algorithm.GpsDatas[j].Sensors, (int) s_row.Length,
                        (int) s_row.StartBit, s_row.K, s_row.B, s_row.S);
                }

                CorrectDipTemperature(ref data);

                graph.AddSeriesL(
                    String.Format("{0} ({1})", s_row.Name, s_row.NameUnit),
                    color[globColor%5],
                    data, //dataset.TemperatureValue.SelectBysensorID(s_row),//
                    Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    AlgorithmType.TEMP1 + globColor*20); //+20*i - ��� ������������ � ������ ������ ��������
                globColor++;
            }
        }

        public void AddGraphVoltage(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            Color[] color = {Color.Green, Color.Red, Color.Brown, Color.DodgerBlue, Color.Fuchsia};
            Algorithm voltAlgorithm = new Algorithm();
            voltAlgorithm.SelectItem(mob_row);
            // ��� ���������� ����������� �� �������/������� �������
            atlantaDataSet.sensorsRow[] s_rows = voltAlgorithm.FindSensors(AlgorithmType.VOLT);
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                //atlantaDataSet.
                //Row[] dv_row = mob_row.GetdataviewRows();
                double[] data = new double[Algorithm.GpsDatas.Length];
                voltAlgorithm.S_row = s_row;
                Calibrate calibrate = voltAlgorithm.Calibration();
                for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
                {
                    data[j] = calibrate.GetUserValue(Algorithm.GpsDatas[j].Sensors, (int)s_row.Length,
                        (int)s_row.StartBit, s_row.K, s_row.B, s_row.S);
                }

                CorrectDipVoltage(ref data);

                graph.AddSeriesL(
                    String.Format("{0} ({1})", s_row.Name, s_row.NameUnit),
                    color[globColor % 5],
                    data, Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    AlgorithmType.VOLT + globColor * 20); //+20*i - ��� ������������ � ������ ������ ��������
                
                globColor++;
            } // foreach
            
            // ��� ���������� ����������� ����� �������� ��������64, ������ ��� �������� ���� ��� � ���
            s_rows = voltAlgorithm.FindSensors(AlgorithmType.VOLTAGE_BAT);
            foreach (atlantaDataSet.sensorsRow s_row in s_rows)
            {
                double[] data = new double[Algorithm.GpsDatas.Length];
                for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
                {
                    data[j] = Algorithm.GpsDatas[j].sVoltage;
                }

                graph.AddSeriesL(
                    String.Format("{0} ({1})", s_row.Name, s_row.NameUnit),
                    color[globColor % 5],
                    data, Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    AlgorithmType.VOLTAGE_BAT + globColor * 20); // + 20 * i -> ��� ������������ � ������ ������ ��������

            } // foreach
        } // AddGraphVoltage

        public void AddGraphSensorAngle(IGraph graph, int idMobitel)
        {
            var sensorAngle = new SensorCalibrated((int) AlgorithmType.ANGLE, idMobitel);
            if (sensorAngle.Id > 0)
            {
                DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                double[] sensorValues = Algorithm.GpsDatas.Select(gps => sensorAngle.GetValue(gps.Sensors)).ToArray();
                graph.AddSeriesL(
                    String.Format("{0} ({1})", sensorAngle.Name, sensorAngle.UnitName),
                    Color.DimGray, sensorValues, times, AlgorithmType.ANGLE);
            }
        }

        public void AddGraphSensorRangeFinder(IGraph graph, int idMobitel)
        {
            var sensorRf = new SensorCalibrated((int) AlgorithmType.RANGEFINDER, idMobitel);
            if (sensorRf.Id > 0)
            {
                DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                double[] sensorValues = Algorithm.GpsDatas.Select(gps => sensorRf.GetValue(gps.Sensors)).ToArray();
                graph.AddSeriesL(
                    String.Format("{0} ({1})", sensorRf.Name, sensorRf.UnitName),
                    Color.DimGray, sensorValues, times, AlgorithmType.RANGEFINDER);
            }
        }


        public void AddGraphSensorAgregat(IGraph graph, Sensor sensor, double[] sensorValues)
        {

            DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
            graph.AddSeriesL(
                String.Format("{0} ({1})", sensor.Name, sensor.UnitName),
                Color.DimGray, sensorValues, times, AlgorithmType.RANGEFINDER);
        }

        public void AddGraphSensorAgregatLogic(IGraph graph, int idMobitel)
        {
            var sensorAgregatLogic = new LogicSensor((int) AlgorithmType.AGREGAT_LOGIC, idMobitel);
            if (sensorAgregatLogic.Id > 0)
            {
                DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                double[] sensorValues =
                    Algorithm.GpsDatas.Select(gps => Convert.ToDouble(sensorAgregatLogic.GetValue(gps.Sensors) ? 1 : 0))
                        .ToArray();
                graph.AddSeriesL(
                    String.Format("{0} ({1})", sensorAgregatLogic.Name, ""),
                    Color.CornflowerBlue, sensorValues, times, AlgorithmType.ANGLE);
            }
        }

        public void AddGraphSensorUnloading(IGraph graph, int idMobitel)
        {
            var sensorUnload = new LogicSensor((int) AlgorithmType.VEHICLE_UNLOAD, idMobitel);
            if (sensorUnload.Id > 0)
            {
                DateTime[] times = Algorithm.GpsDatas.Select(gps => gps.Time).ToArray();
                double[] sensorValues =
                    Algorithm.GpsDatas.Select(gps => Convert.ToDouble(sensorUnload.GetValue(gps.Sensors) ? 1 : 0))
                        .ToArray();
                graph.AddSeriesL(
                    String.Format("{0} ({1})", sensorUnload.Name, ""),
                    Color.DarkSlateBlue, sensorValues, times, AlgorithmType.VEHICLE_UNLOAD);
            }
        }

        /// <summary>
        /// //��������� �������� ������� � ������ (�� 3-� �����)
        /// </summary>
        /// <param name="data">������ ��� ��������</param>
        private void CorrectDipTemperature(ref double[] data)
        {
            for (int j = 0; j < data.Length - 2; j++)
            {
                if (System.Math.Abs(data[j] - data[j + 1]) > 2)
                    //���� ������� ����� ������� ������ 2-� ��������, ��������� �� ������ �� ���
                {
                    for (int fallLen = 1; fallLen < 4 && ((j + fallLen + 1) < data.Length); fallLen++)
                    {
                        if (System.Math.Abs(data[j] - data[j + fallLen + 1]) <= 2)
                        {
                            data[j + 1] = (data[j] + data[j + fallLen + 1])/2;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// //��������� �������� ������� � ������ (�� 2-� �����)
        /// </summary>
        /// <param name="data">������ ��� ��������</param>
        private void CorrectDipVoltage(ref double[] data)
        {
            for (int j = 0; j < data.Length - 2; j++)
            {
                if (System.Math.Abs(data[j] - data[j + 1]) > 2)
                    //���� ������� ����� ������� ������ 2-� �����, ��������� �� ������ �� ���
                {
                    for (int fallLen = 1; fallLen < 3 && ((j + fallLen + 1) < data.Length); fallLen++)
                    {
                        if (System.Math.Abs(data[j] - data[j + fallLen + 1]) <= 2)
                        {
                            data[j + 1] = (data[j] + data[j + fallLen + 1])/2;
                            break;
                        }
                    }
                }
            }
        }

        #region Inclinometer

        public bool AddGraphInclinometers(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row)
        {
            Algorithm tmpAlgorithm = new Algorithm();
            atlantaDataSet.settingRow setRow = GetSettingsRow(atlDataSet, mob_row, tmpAlgorithm);
            if (setRow == null) return false;
            SensorInclinometer inclX = new SensorInclinometer(tmpAlgorithm, AlgorithmType.INCLINOMETER_X,
                setRow.InclinometerMaxAngleAxisX);
            SensorInclinometer inclY = new SensorInclinometer(tmpAlgorithm, AlgorithmType.INCLINOMETER_Y,
                setRow.InclinometerMaxAngleAxisY);
            if (!inclX.IsSensorExist && !inclY.IsSensorExist) return false;
            //atlantaDataSet.dataviewRow[] dv_rows = mob_row.GetdataviewRows();
            if (Algorithm.GpsDatas == null || Algorithm.GpsDatas.Length == 0) return false;
            //if (dv_rows.Length == 0) return false;
            inclX.Init(Algorithm.GpsDatas.Length);
            inclY.Init(Algorithm.GpsDatas.Length);
            List<DateTime> regionUnLimit = new List<DateTime>();
            for (int j = 0; j < Algorithm.GpsDatas.Length; j++)
            {
                inclX.SetSensorValue(j, Algorithm.GpsDatas[j]);
                inclY.SetSensorValue(j, Algorithm.GpsDatas[j]);

                if (inclX.IsValueDangered || inclY.IsValueDangered)
                {
                    regionUnLimit.Add(Algorithm.GpsDatas[j].Time);
                }
                else
                {
                    AddDangerRegion(graph, regionUnLimit);
                }
            }
            AddDangerRegion(graph, regionUnLimit);
            string seriesNameX = String.Format("{0},{1}", Resources.InclinometerX, Resources.Gradus);
            if (inclX.IsSensorExist)
            {
                graph.AddSeriesR(
                    seriesNameX,
                    _colorInclinometerX,
                    inclX.SensorData,
                    Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    inclX.TypeAlgoritm);
            }
            string seriesNameY = String.Format("{0},{1}", Resources.InclinometerY, Resources.Gradus);
            if (inclY.IsSensorExist)
            {
                graph.AddSeriesR(
                    seriesNameY,
                    _colorInclinometerY,
                    inclY.SensorData,
                    Algorithm.GpsDatas.Select(gps => gps.Time).ToArray(),
                    inclY.TypeAlgoritm);
            }
            if (inclX.IsSensorExist || inclY.IsSensorExist)
            {
                return true;
            }
            return false;
        }

        private static atlantaDataSet.settingRow GetSettingsRow(atlantaDataSet atlDataSet,
            atlantaDataSet.mobitelsRow mob_row, Algorithm tmpAlgorithm)
        {
            if (mob_row == null) return null;
            tmpAlgorithm.SelectItem(mob_row);
            atlantaDataSet.vehicleRow[] vehRows = mob_row.GetvehicleRows();
            if (vehRows == null || vehRows.Length == 0 || vehRows[0].setting_id < 0) return null;
            atlantaDataSet.settingRow setRow = atlDataSet.setting.FindByid(vehRows[0].setting_id);
            return setRow;
        }

        private void AddDangerRegion(IGraph graph, List<DateTime> region)
        {
            if (region.Count > 1)
            {
                graph.AddDangerRegion(region[0], region[region.Count - 1], _colorInclinometerDanger);
            }
            region.Clear();
        }

        public bool IsInclinometerDangered(atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row,
            Int64 dataGpsId)
        {
            if (atlDataSet == null || mob_row == null) return false;

            //atlantaDataSet.dataviewRow[] dv_rows =
            //    (LocalCache.atlantaDataSet.dataviewRow[])atlDataSet.dataview.Select(string.Format("DataGps_ID <= {0} and DataGps_ID >= {1}", dataGpsId, dataGpsId ));
            
            List<GpsData> gpsDataList = new List<GpsData>();
            GpsData[] gpsDatas = new GpsData[0];
            try
            {
                //GpsData[] gpsDatas = Algorithm.GpsDatas.Where(gps => gps.Id == dataGpsId).ToArray();

                for (int i = 0; i < Algorithm.GpsDatas.Length; i++)
                {
                    if (Algorithm.GpsDatas[i].Id == dataGpsId)
                    {
                        gpsDataList.Add(Algorithm.GpsDatas[i]);
                    }
                }

                gpsDatas = gpsDataList.ToArray();
            }
            catch (Exception ex)
            {
                // just to do
            }

            if (gpsDatas.Length == 0) 
                return false;

            Algorithm tmpAlgorithm = new Algorithm();
            atlantaDataSet.settingRow setRow = GetSettingsRow(atlDataSet, mob_row, tmpAlgorithm);

            if (setRow == null) return false;

            SensorInclinometer inclX = new SensorInclinometer(tmpAlgorithm, AlgorithmType.INCLINOMETER_X,
                setRow.InclinometerMaxAngleAxisX);
            SensorInclinometer inclY = new SensorInclinometer(tmpAlgorithm, AlgorithmType.INCLINOMETER_Y,
                setRow.InclinometerMaxAngleAxisY);

            if (!inclX.IsSensorExist && !inclY.IsSensorExist) return false;

            inclX.Init(gpsDatas.Length);
            inclY.Init(gpsDatas.Length);
            for (int j = 0; j < gpsDatas.Length; j++)
            {
                inclX.SetSensorValue(j, gpsDatas[j]);
                inclY.SetSensorValue(j, gpsDatas[j]);

                if (inclX.IsValueDangered || inclY.IsValueDangered)
                {
                    return true;
                }
            }
            return false;
        }

        private class SensorInclinometer
        {
            private AlgorithmType _typeAlgoritm;

            public AlgorithmType TypeAlgoritm
            {
                get { return _typeAlgoritm; }
                set { _typeAlgoritm = value; }
            }

            private Algorithm _algoritml;
            private double _dangerValue;
            private atlantaDataSet.sensorsRow _sensRowIncl;
            private Calibrate _calibrate;
            private double[] data;
            private bool _isValueDangered;

            public SensorInclinometer(Algorithm algoritml, AlgorithmType typeAlgoritm, double dangerValue)
            {
                _typeAlgoritm = typeAlgoritm;
                _algoritml = algoritml;
                _sensRowIncl = _algoritml.FindSensor(_typeAlgoritm);
                _dangerValue = dangerValue;
            }

            public bool IsSensorExist
            {
                get { return !(_sensRowIncl == null); }
            }

            public bool IsValueDangered
            {
                get { return _isValueDangered; }
            }

            public double[] SensorData
            {
                get { return data; }
            }

            public void Init(int lenght)
            {
                if (IsSensorExist)
                {
                    data = new double[lenght];
                    _algoritml.S_row = _sensRowIncl;
                    _calibrate = _algoritml.Calibration();
                    ;
                }
            }

            public void SetSensorValue(int indexDvRow, GpsData gpsData)
            {
                if (IsSensorExist)
                {
                    data[indexDvRow] = _calibrate.GetUserValue(gpsData.Sensors, (int) _sensRowIncl.Length,
                        (int) _sensRowIncl.StartBit, _sensRowIncl.K, _sensRowIncl.B, _sensRowIncl.S);
                    if (Math.Abs(data[indexDvRow]) > _dangerValue)
                    {
                        _isValueDangered = true;
                    }
                    else
                    {
                        _isValueDangered = false;
                    }
                }
            }
        }

        #endregion
    }
}
