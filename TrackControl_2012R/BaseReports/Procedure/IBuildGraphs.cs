using LocalCache;
using TrackControl.Reports.Graph;
using System;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    public interface IBuildGraphs
    {
        double[] ValueRealFuel { get; }
        double[] ValueMeanFuel { get; }
        DateTime[] TimeRealFuel { get; }
        DateTime[] TimeMeanFuel { get; }
        void AddGraphFuel(object alg_type, IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        void AddGraphFlowmeter(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        void AddGraphTemperature(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        void AddGraphAngle(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        void AddGraphDalnomer(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        bool AddGraphInclinometers(IGraph graph, atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row);
        bool IsInclinometerDangered(atlantaDataSet atlDataSet, atlantaDataSet.mobitelsRow mob_row, Int64 dataGpsId);
        void SetAlgorithm(object algorithm);
        void AddGraphVoltage(IGraph graph, atlantaDataSet dataset, atlantaDataSet.mobitelsRow curMrow);
        void AddGraphGrain(IGraph graph, atlantaDataSet dataset, atlantaDataSet.mobitelsRow mobitelRow);
    }
}
