using System;
using System.Collections.Generic;
using System.Linq;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    /// <summary>
    /// Traffic property
    /// </summary>
    public class TrafficProperty
    {
        /// <summary>
        /// Create traffic property
        /// </summary>
        /// <param name="ac">Ac</param>
        /// <param name="br">Br</param>
        /// <param name="sp">Sp</param>
        public TrafficProperty(double ac, double br, double sp)
        {
            accelAlow = ac;
            breakAlow = br;
            speedAlow = sp;
        }

        public TrafficProperty(atlantaDataSet.mobitelsRow m_row)
        {
            atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();
            if (vehicles != null && vehicles.Length > 0)
            {
                atlantaDataSet.settingRow s_row = vehicles[0].settingRow;
                if (s_row != null)
                {
                    accelAlow = s_row.accelMax;
                    breakAlow = s_row.breakMax;
                    speedAlow = s_row.speedMax;
                }
            }
        }
        /// <summary>
        /// Accel alow
        /// </summary>
        public double accelAlow;
        /// <summary>
        /// Break alow
        /// </summary>
        public double breakAlow;
        /// <summary>
        /// Speed alow
        /// </summary>
        public double speedAlow;
    } // struct TrafficProperty

    public class RoadSafety : Algorithm, IAlgorithm
    {
        TrafficProperty traffPar;

        public RoadSafety()
            : base()
        {
        }

        #region IAlgorithm Members

        void IAlgorithm.Run()
        {
            if (GpsDatas.Length == 0)
            {
                return;
            }

            traffPar = new TrafficProperty(m_row);
            atlantaDataSet.TrafficSafetyReportRow trow =
              AtlantaDataSet.TrafficSafetyReport.NewTrafficSafetyReportRow();

            trow.Mark = Vehicle.CarMaker ?? String.Empty;
            trow.Model = Vehicle.CarModel ?? String.Empty;
            trow.NamberPlate = Vehicle.RegistrationNumber ?? String.Empty;
            trow.Mobitel_id = m_row.Mobitel_ID;
            trow.Driver = Vehicle.DriverFullName;
            // ���������� ����
            trow.Travel = BasicMethods.GetKilometrage(GpsDatas);
            // �������� ���������� �������
            double Pmax = traffPar.speedAlow;
            //Spoint[] speedPoints1 = FindMax(
            //  m_row.GetdataviewRows(), atlantaDataSet.dataview.speedColumn,
            //  atlantaDataSet.dataview.DataGps_IDColumn, Pmax);
            var gpsDataSpeed = new Dictionary<long, double>();
            GpsDatas.ToList().ForEach(gpsData => gpsDataSpeed.Add(gpsData.Id, gpsData.Speed));
            Spoint[] speedPoints = FindMax(gpsDataSpeed, Pmax);
            TimeSpan speed_dT = new TimeSpan();
            foreach (Spoint p in speedPoints)
            {
                speed_dT += GetTime(p.Y) - GetTime(p.X);
            }
            trow.SpeedTreshold = speed_dT;
            TimeSpan fullTime = BasicMethods.GetMaxTime(GpsDatas) - BasicMethods.GetMinTime(GpsDatas);
            TimeSpan TimeMotion = fullTime - BasicMethods.GetTotalStopsTime(GpsDatas, 0);

            if (speed_dT.TotalMinutes == 0.0 || TimeMotion.TotalMinutes == 0.0)
                trow.Kv = 0.0;
            else
                trow.Kv = speed_dT.TotalMinutes * 100 / TimeMotion.TotalMinutes;

            // �������� ���������
            Pmax = traffPar.accelAlow;
            //Spoint[] accelPoints1 = FindMax(
            //  m_row.GetdataviewRows(), atlantaDataSet.dataview.accelColumn,
            //  atlantaDataSet.dataview.DataGps_IDColumn, Pmax);
            var gpsDataAccell = new Dictionary<long, double>();
            GpsDatas.ToList().ForEach(gpsData => gpsDataAccell.Add(gpsData.Id, gpsData.Accel));
            Spoint[] accelPoints = FindMax(gpsDataAccell, Pmax);
            trow.AccelCount = accelPoints.Length;

            if (accelPoints.Length == 0.0 || trow.Travel == 0.0)
                trow.Ka = 0.0;
            else
                trow.Ka = accelPoints.Length * 100 / trow.Travel;

            // �������� ����������
            double Pmin = traffPar.breakAlow * (-1);
            //Spoint[] breakPoints1 = FindMin(
            //  m_row.GetdataviewRows(), atlantaDataSet.dataview.accelColumn,
            //  atlantaDataSet.dataview.DataGps_IDColumn, Pmin);
            Spoint[] breakPoints = FindMin(gpsDataAccell, Pmin);
            trow.BreakCount = breakPoints.Length;

            if (breakPoints.Length == 0.0 || trow.Travel == 0.0)
                trow.Kb = 0.0;
            else
                trow.Kb = breakPoints.Length * 100 / trow.Travel;

            trow.SpeedMax = BasicMethods.GetMaxSpeed(GpsDatas);
            trow.AccelMax = GetMaxAccel();
            trow.BreakMax = GetMinAccel();

            AtlantaDataSet.TrafficSafetyReport.AddTrafficSafetyReportRow(trow);
        }

        #endregion

        private double GetMaxAccel()
        {
            double result = GpsDatas[0].Accel;
            foreach (GpsData d in GpsDatas)
            {
                if (d.Accel > result)
                {
                    result = d.Accel;
                }
            }
            return result;
        }

        private double GetMinAccel()
        {
            double result = GpsDatas[0].Accel;
            foreach (GpsData d in GpsDatas)
            {
                if (d.Accel < result)
                {
                    result = d.Accel;
                }
            }
            return result;
        }
    }
}
