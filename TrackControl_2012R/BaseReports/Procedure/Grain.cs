﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    public class ValuePairs
    {
        public double value;
        public double averageValue;

        public ValuePairs()
        {
        }

        public ValuePairs(double val, double avgVal)
        {
            value = val;
            averageValue = avgVal;
        }

        public static double[] GetValue(ValuePairs[] vPair)
        {
            double[] data = new double[vPair.Length];
            for (int i = 0; i < data.Length; i++)
                data[i] = vPair[i].value;
            return data;
        }

        public static double[] GetAvgValue(ValuePairs[] vPair)
        {
            double[] data = new double[vPair.Length];
            for (int i = 0; i < data.Length; i++)
                data[i] = vPair[i].averageValue;
            return data;
        }
    }

    public class GrainDictionarys
    {
        private IDictionary<System.Int64, ValuePairs> valueGrainSum;
        public List<IDictionary<long, ValuePairs>> valueGrainArrayDict = new List<IDictionary<long, ValuePairs>>();
        public List<atlantaDataSet.sensorsRow> GrainSensors = new List<atlantaDataSet.sensorsRow>();

        public bool GrainSensorExist(int iNumber)
        {
            return GrainSensors[iNumber] != null;
        }

        //private IDictionary<Int64, ValuePairs> valueGrainSensor1;

        public IDictionary<Int64, ValuePairs> GetValueGrainSensor(int iCount)
        {
            if (valueGrainArrayDict.Count > 0)
                return valueGrainArrayDict[iCount];

            return null;
        }

        public void SetValueGrainSensor(int iCount, IDictionary<Int64, ValuePairs> value)
        {
            valueGrainArrayDict[iCount] = value;
        }

        //private IDictionary<System.Int64, ValuePairs> valueGrainSensor2;

        //public IDictionary<System.Int64, ValuePairs> ValueGrainSensor2
        //{
        //    get { return valueGrainSensor2; }
        //    set { valueGrainSensor2 = value; }
        //}

        public IDictionary<System.Int64, ValuePairs> ValueGrainSum
        {
            get { return valueGrainSum; }
            set { valueGrainSum = value; }
        }

        public GrainDictionarys()
        {
            for (int i = 0; i < valueGrainArrayDict.Count; i++)
            {
                valueGrainArrayDict.Add(new Dictionary<Int64, ValuePairs>());
            }

            valueGrainSum = new Dictionary<Int64, ValuePairs>();
        }

        public void Clear()
        {
            if (valueGrainArrayDict != null)
            {
                for (int i = 0; i < valueGrainArrayDict.Count; i++)
                {
                    if (valueGrainArrayDict[i] != null)
                        valueGrainArrayDict[i].Clear();
                }

                valueGrainArrayDict.Clear();
            }

            if (GrainSensors != null)
                if (GrainSensors.Count > 0)
                    GrainSensors.Clear();

            FreeSensorReferences();
            valueGrainSum.Clear();
        }

        /// <summary>
        /// Освобождение ссылок на описания датчиков.
        /// </summary>
        public void FreeSensorReferences()
        {
            for (int i = 0; i < valueGrainArrayDict.Count; i++)
            {
                valueGrainArrayDict[i] = null;
            }
        }

        public void ValueGrainSensorsClear()
        {
            for (int i = 0; i < valueGrainArrayDict.Count; i++)
            {
                valueGrainArrayDict[i].Clear();
            }
        }

        public void GenerGrainDictonary(int count)
        {
            for (int i = 0; i < count; i++)
            {
                valueGrainArrayDict.Add(new Dictionary<long, ValuePairs>());
            }
        }
    }

    public class Grain : PartialAlgorithms, IAlgorithm
    {
          #region Attribute

        /// <summary>
        /// Полоса осреднения
        /// </summary>
        private int width;

        private atlantaDataSet.KilometrageReportRow[] kilometrage_rows;

        public atlantaDataSet.KilometrageReportRow[] Kilometrage_rows
        {
            get { return kilometrage_rows; }
            set { kilometrage_rows = value; }
        }

        private AlgorithmType algType;

        public GrainDictionarys GrainDictionary;

        /// <summary>
        /// порог заправки
        /// </summary>
        private double GrainingEdge;

        /// <summary>
        /// порог слива
        /// </summary>
        private double GrainingDischarge;

        /// <summary>
        /// норма расхода топлива в час, для транспортного средства
        /// </summary>
        private double flowmeterNorm;

        private int useMinMaxAlgoritm;

        /// <summary>
        /// время между соседними заправками/сливами, меньше которого заправки/сливы буду объединяться для МинМах алгоритма 2
        /// </summary>
        private TimeSpan timeForCombine = new TimeSpan(0, 2, 0);

        /// <summary>
        /// Итоговые данные отчета
        /// </summary>
        private Grain.Summary _summary;

        public Grain.Summary summary
        {
            get
            {
                return _summary;
            }
        }

        private const int MIN_MAX_ALL_TIME_IS_STOP = 5;//весь период одна остановка 
        private const int MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT = 3;//сливы как MIN_MAX_AGGREGATE_ON_STOP, заправки как MIN_MAX_ONE
        private const int MIN_MAX_AGGREGATE_ON_STOP = 2;
        private const int MIN_MAX_ONE = 1;
        private const int MIN_MAX_DISABLE = 0;

        #endregion 

        public Grain()
            : base()
        {
            // to do there
        }

        public Grain(object param)
            : base()
        {
            if (param is AlgorithmType)
            {
                algType = (AlgorithmType) param;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }

            width = 1;
            GrainDictionary = new GrainDictionarys();
        }

        #region Metods

        /// <summary>
        /// Вычисление бегущего среднего
        /// в заданном диапазоне
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lower"></param>
        /// <param name="upper"></param>
        /// <param name="ind"></param>
        /// <returns></returns>
        protected double average(double[] data, int lower, int upper, int ind)
        {
            // Определение диапазона yсреднения
            // верхний диапазон
            if (data.Length == 0)
            {
                return 0;
            }
            int begin, end;
            if (ind - width < lower)
            {
                begin = lower;
            }
            else
            {
                begin = ind - width;
            }
            // нижний диапазон
            if (ind + width > upper)
            {
                end = upper;
            }
            else
            {
                end = ind + width;
            }

            if (begin == end)
            {
                return data[begin];
            }
            if (end > data.Length)
            {
                end = data.Length;
            }
            double avr = 0;
            double min = data[Min(data, begin, end)];
            double max = data[Max(data, begin, end)];
            for (int i = begin; i < end; i++)
            {
                avr += data[i];
            }

            if (end - begin - 2 > 0)
            {
                avr -= min + max;
                return avr/(end - begin - 2);
            }
            else
            {
                return avr/(end - begin);
            }
        }

        /// <summary>
        /// Поиск минимального значения
        /// </summary>
        /// <param name="data"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected int Min(double[] data, int begin, int end)
        {
            int ind = begin;
            if (data.Length > 0)
            {
                double min = data[begin];
                for (int i = begin + 1; i < end; i++)
                {
                    if (min > data[i])
                    {
                        min = data[i];
                        ind = i;
                    }
                }
            }
            return ind;
        }

        /// <summary>
        /// Поиск максимального значения
        /// </summary>
        /// <param name="data"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected System.Int64 Max(double[] data, System.Int64 begin, System.Int64 end)
        {
            System.Int64 ind = begin;
            if (data.Length > 0)
            {
                double max = data[begin];
                for (System.Int64 i = begin; i < end; i++)
                {
                    if (max < data[i])
                    {
                        max = data[i];
                        ind = i;
                    }
                }
            }
            return ind;
        }

        /// <summary>
        /// Вычисление дисперсии
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private System.Int64 disp(System.Int64 begin, System.Int64 end)
        {
            if (begin < 0)
            {
                begin = 0;
            }
            System.Int64 size = end - begin;
            if (size < 0) return 0;
            double[] dis = new double[size];
            ValuePairs[] GrainValue = new ValuePairs[GrainDictionary.ValueGrainSum.Count];
            GrainDictionary.ValueGrainSum.Values.CopyTo(GrainValue, 0);
            for (int i = 1; i < size; i++)
            {
                double d = GrainValue[i + begin].averageValue - GrainValue[i + begin - 1].averageValue;
                dis[i] = (d*d)/(size - 1);
            }
            return begin + Max(dis, 0, size);
        }

        /// <summary>
        /// Определение точки методом наимельших квадратов
        /// принимаемые значения 1-буфер данных для постройки прямой,
        /// 2-точка относительно буффера в которой нужно получить значение
        /// </summary>
        /// <param name="points"></param>
        /// <param name="need"></param>
        /// <param name="go_top"></param>
        /// <returns></returns>
        private double GetValueMetodomLine(double[] points, int need, ref bool go_top)
        {
            go_top = false;
            double ret_value = 0; //x-номер елемента масива; у-значение уровня топлива
            double Zx = 0, Zxx = 0, Zxy = 0, Zy = 0;
            for (int i = 0; i < points.Length; i++)
            {
                Zx += i;
                Zxx += (i*i);
                Zy += points[i];
                Zxy += (i*points[i]);
            }
            double coefA = ((Zy/Zx - points.Length*Zxy/(Zx*Zx))/(1 - points.Length*Zxx/(Zx*Zx)));
            double coefB = (Zxy - coefA*Zxx)/Zx;
            ret_value = coefA*need + coefB;
            //find delta to line
            double[] delta_line = new double[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                delta_line[i] = Math.Abs((i*coefA + coefB) - points[i]);
            }
            //Находим и удаляем точку наиболее отклоняющуюся от прямой, если у нас больше 2-х точек.
            if (delta_line.Length > 2)
            {
                System.Int64 max = Max(delta_line, 0, delta_line.Length);
                //correctind line
                Zx -= max;
                Zxx -= (max*max);
                Zy -= points[max];
                Zxy -= (max*points[max]);

                coefA = ((Zy/Zx - (points.Length - 1)*Zxy/(Zx*Zx))/(1 - (points.Length - 1)*Zxx/(Zx*Zx)));
                coefB = (Zxy - coefA*Zxx)/Zx;
            }
            if (coefA > 0)
            {
                go_top = true;
            }
            if (!double.IsInfinity(coefA) && !double.IsNaN(coefA))
            {
                ret_value = coefA*need + coefB;
            }
            return ret_value;
        }

        #endregion

        #region IAlgorithm Members

        void IAlgorithm.SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
            this.width = 1;
            if (set_row != null)
            {
                this.width = set_row.band;
            }
        }

        /// <summary>
        /// Построение отчета
        /// </summary>
        public override void Run()
        {
            //try {
            if (m_row == null)
            {
                return;
            }

            if (m_row.GetGrainReportRows() != null)
            {
                int length = m_row.GetGrainReportRows().Length;

                if ( length != 0 || GpsDatas.Length == 0)
                {
                    return;
                }
            }

            if (m_row.GetKilometrageReportRows() == null || m_row.GetKilometrageReportRows().Length == 0)
            {
                return;
            }

            double summGrain = 0;
            double diffGrain = 0;

            int iSummGrainCount = 0;
            int iDiffGrainCount = 0;

            AtlantaDataSet.Grain_Report.Clear(); // очистим структуру перед использованием

            GettingValuesDUT(GrainDictionary);

            //можем теперь удалить ненужные 
            GrainDictionary.ValueGrainSensorsClear();
            
            if (GrainDictionary.ValueGrainSum.Count == 0) //Если данных не нашлось - нечего делать
                return;

            _summary = new Grain.Summary();
            _summary.Before = GrainDictionary.ValueGrainSum[GpsDatas[0].Id].averageValue; // Топлива в начале
            _summary.After = GrainDictionary.ValueGrainSum[GpsDatas[GpsDatas.Length - 1].Id].averageValue;
            // Топлива в конце

            #region GetSettings

            GrainingEdge = 10;
            GrainingDischarge = 10;
            flowmeterNorm = 33;
            int UseDischargeInGrainrate = 0;
            useMinMaxAlgoritm = 0;
            bool isMinMax3 = false;
            if (set_row != null) // Если есть записи с насройками то берем данные оттуда
            {
                GrainingEdge = set_row.FuelingEdge;
                GrainingDischarge = set_row.FuelingDischarge;
                flowmeterNorm = set_row.AvgFuelRatePerHour;
                UseDischargeInGrainrate = set_row.FuelrateWithDischarge > 0 ? 1 : 0;
                useMinMaxAlgoritm = set_row.FuelingMinMaxAlgorithm;
                if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT) isMinMax3 = true;
                timeForCombine = set_row.FuelerMaxTimeStop;
            }

            #endregion

            int maximum = m_row.GetKilometrageReportRows().Length*2;
            BeforeReportFilling(Resources.Fuel, maximum);

            atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows;
            if (useMinMaxAlgoritm == MIN_MAX_ALL_TIME_IS_STOP)
            {
                useMinMaxAlgoritm = MIN_MAX_AGGREGATE_ON_STOP;
                tmpKilimetrage_rows = GetOneTotalKilometrageReportRow(m_row.GetKilometrageReportRows());
            }
            else
            {
                atlantaDataSet.KilometrageReportRow[] tmpKmRows =
                    CombineAdjacentStopsOrMoves(m_row.GetKilometrageReportRows());
                tmpKilimetrage_rows = CorrectingKilometrage(tmpKmRows);
            }

            
            byte j = 0;
            List<double> listResult = new List<double>();
            for (int s = 0; s < tmpKilimetrage_rows.Length; s++)
            {
                atlantaDataSet.KilometrageReportRow k_row = tmpKilimetrage_rows[s];
                double beginValue = 0;
                double endValue = 0;
                System.Int64 beginIndex = -1;
                System.Int64 endIndex = -1;
                bool newSearch = true;
                // третий алгоритм минмакса сливы как MIN_MAX_AGGREGATE_ON_STOP, заправки как MIN_MAX_ONE
                if (isMinMax3)
                {
                    useMinMaxAlgoritm = MIN_MAX_DISABLE;
                    CountBeginAndEndGrainValue(s, tmpKilimetrage_rows,
                                              ref beginValue, ref endValue, ref beginIndex, ref endIndex, newSearch);
                    if (endValue > beginValue)
                    {
                        useMinMaxAlgoritm = MIN_MAX_ONE;
                    }
                    else if (endValue < beginValue)
                    {
                        useMinMaxAlgoritm = MIN_MAX_AGGREGATE_ON_STOP;
                    }
                    else
                    {
                        continue;
                    }
                }
                beginValue = 0;
                endValue = 0;
                beginIndex = -1;
                endIndex = -1;
                newSearch = true;
                //--------------------------------------------------------------------------------------------------
                while (CountBeginAndEndGrainValue(s, tmpKilimetrage_rows,
                    ref beginValue, ref endValue, ref beginIndex, ref endIndex, newSearch))
                {
                    newSearch = false;
                    
                    // Проверяем не ложная ли заправка/слив 
                    // если есть подозрение на заправку/слив
                    if (((endValue - beginValue > GrainingEdge) ) 
                        || beginValue - endValue > GrainingDischarge) 
                    {
                        if (TestTrueGraining(beginValue, endValue, tmpKilimetrage_rows, s))
                        {
                            if (TestTrueDischarge(tmpKilimetrage_rows[s], beginValue, endValue))
                            {
                                atlantaDataSet.Grain_ReportRow f_row = AtlantaDataSet.Grain_Report.NewGrain_ReportRow();
                                f_row.mobitel_id = this.m_row.Mobitel_ID;
                                f_row.sensor_id = base.s_row.id;
                                f_row.Location = k_row.Location;
                                f_row.beginValue = beginValue;
                                f_row.endValue = endValue;
                                double result = endValue - beginValue;
                                listResult.Add(result);
                                f_row.dValue = result;
                                f_row.beginTime = GpsDatas[beginIndex].Time;
                                f_row.endTime = GpsDatas[endIndex].Time;
                                System.Int64 indGrain = disp(beginIndex, endIndex);
                                f_row.pointValue = GrainDictionary.ValueGrainSum[GpsDatas[indGrain].Id].averageValue;
                                //Мгновенный уровень топлива в момент заправки
                                f_row.DataGPS_ID = GpsDatas[indGrain].Id;
                                f_row.date_ = GpsDatas[indGrain].Time.ToShortDateString();
                                f_row.Times = GpsDatas[indGrain].Time.ToLongTimeString();
                                f_row.time_ = GpsDatas[indGrain].Time;
                                f_row.Lat = GpsDatas[indGrain].LatLng.Lat;
                                f_row.Lon = GpsDatas[indGrain].LatLng.Lng;
                                f_row.GenerateType = "system";
                                AtlantaDataSet.Grain_Report.AddGrain_ReportRow(f_row);
                            }
                        }
                    }
                }

                if (j > 64)
                {
                    ReportProgressChanged(s);
                    j = 0;
                }
                else
                {
                    j++;
                }
            }

            // второй алгоритм мин/макса - соединяем соседние сливы/заправки
            if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP) 
                CombineSimilarActions();

            #region -- Заполнение итогов --

            int numberRow = listResult.Count;

            for (int i = 0; i < numberRow; i++)
            {
                double dValue = listResult[i];
                if (dValue > 0)
                {
                    summGrain += dValue;
                    iSummGrainCount++;
                }
                else
                {
                    diffGrain += dValue;
                    iDiffGrainCount++;
                }
            }
   
            _summary.Graining = summGrain; // Всего заправлено
            _summary.GrainingCount = iSummGrainCount; // Количество заправок
            _summary.Discharge = diffGrain; // Всего слито
            _summary.DischargeCount = iDiffGrainCount; // Количество сливов
            _summary.Total = _summary.Before + _summary.Graining - _summary.After +
                             UseDischargeInGrainrate*_summary.Discharge; // Общий расход

            //_summary.Rate = m_row.path < 50 ? 0 : _summary.Total * 100.0  / m_row.path; // Расход на 100 км
            _summary.Rate =  _summary.Total * 100.0 / m_row.path; // Расход на 100 км

            #endregion

            #region --   Средняя скорость движения  --

            TimeSpan travelTime = //одинаковое ли значение получим?????
                //sumGrainRows[sumGrainRows.Length - 1].dataviewRow.time - sumGrainRows[0].dataviewRow.time;
                GpsDatas[GpsDatas.Length - 1].Time - GpsDatas[0].Time;

            foreach (atlantaDataSet.KilometrageReportRow row in m_row.GetKilometrageReportRows())
            {
                if (row.Distance == 0)
                {
                    travelTime = travelTime.Subtract(row.Interval);
                }
            }

            if (travelTime.TotalHours > 0)
            {
                _summary.AvgSpeed = m_row.path / travelTime.TotalHours;
            }

            #endregion


            GrainDictionary.Clear();

            AfterReportFilling();
            Algorithm.OnAction(this, new GrainEventArgs(m_row.Mobitel_ID, _summary));
            System.Windows.Forms.Application.DoEvents();
            //}
            //catch (Exception ex)
            //{ MessageBox.Show("Grain Exception \n" + ex.Message + "\n" + ex.Source); }
        }

        #endregion

        #region DUT Value

        /// <summary>
        /// Позволяет установить масив DataGps[], для работы с алгоритмом
        /// </summary>
        public void SetD_rows(GpsData[] rows, atlantaDataSet.mobitelsRow mRow)
        {
            GpsDatas = rows;
            m_row = mRow;
        }

        public void SettingAlgoritm(object param)
        {
            if (param is AlgorithmType)
            {
                algType = (AlgorithmType)param;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }
        }

        /// <summary>
        /// Выбирает данные из atlantaDataSet.dataview. Пересчитывает значения для заданого усреднения
        /// и для десяти датчиков
        /// </summary>
        public void GettingValuesDUT(GrainDictionarys GrainDict)
        {
            GrainDict.Clear();

            //FindSensors(ref GrainDict.GrainSensor1, ref GrainDict.GrainSensor2, AlgorithmType.Grain1);
            FindSensorsDUT( GrainDict.GrainSensors, algType /*AlgorithmType.Grain1*/ );

            if (GrainDict.GrainSensors.Count == 0)
                return;

            GrainDict.GenerGrainDictonary( GrainDict.GrainSensors.Count );

            
            this.width = 1;

            if (set_row != null)
            {
                this.width = set_row.band;
            }

            SelectGrainValues(GrainDict, GpsDatas);

            try
            {
                CorrectGrainForTwoSensors(GrainDict);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Getting Values DUT", MessageBoxButtons.OK);
            }
        }

        public void GettingValuesDUT(GrainDictionarys GrainDict, GpsData[] gpsDatas)
        {
            GpsDatas = gpsDatas;
            GettingValuesDUT(GrainDict);
        }

        /// <summary>
        /// Дозапрос необходимых данных в указанном диапазоне значений LogID 
        /// </summary>
        /// <param name="prevLogId">Начальное значение LogId</param>
        /// <param name="curLogId">Конечное значение LogId</param>
        private List<GpsData> getAdditionsData(atlantaDataSet.sensorsRow sensor, int prevLogId, int curLogId, int mobitelId,
            bool Is64Packet)
        {
            DataTable gpsDoData = null;

            using (var db = new DriverDb())
            {
                db.NewSqlConnection(db.CS);
                db.NewSqlCommand();
                db.CommandSqlConnection();
                db.CommandTimeout(800);
                db.CommandType(CommandType.StoredProcedure);

                if (Is64Packet)
                    db.CommandText("getDiapasone64");
                else
                    db.CommandText("getDiapasone");

                db.CommandParametersAdd(db.ParamPrefics + "mobile", db.GettingInt32(), mobitelId);
                db.CommandParametersAdd(db.ParamPrefics + "prevLogId", db.GettingInt32(), prevLogId);
                db.CommandParametersAdd(db.ParamPrefics + "currLogId", db.GettingInt32(), curLogId);
                db.SqlConnectionOpen();
                db.SqlDataReader = db.CommandExecuteReader();
                gpsDoData = db.GetDataTable(db.GetCommand);
                db.SqlConnectionClose();
            }

            List<GpsData> gpsDiaData = new List<GpsData>();

            if (gpsDoData != null)
            {
                Calibrate calibrateSensor = Calibration();

                for (int k = 0; k < gpsDoData.Rows.Count; k++)
                {
                    GpsData gps = new GpsData();
                    gps.Time = Convert.ToDateTime(gpsDoData.Rows[k]["time"].ToString());
                    long gpsDataId = (long)Convert.ToUInt32(gpsDoData.Rows[k]["DataGps_ID"].ToString());
                    gps.Id = gpsDataId;
                    gps.SrvPacketID = Convert.ToInt64(gpsDoData.Rows[k]["srvPack"].ToString());
                    gps.Events = (uint)Convert.ToUInt32(gpsDoData.Rows[k]["Events"].ToString());

                    if (Is64Packet)
                    {
                        byte[] initSensors = new byte[25];
                        if (gpsDoData.Rows[k]["sensor"] != null)
                        {
                            byte[] sensors = (byte[])gpsDoData.Rows[k]["sensor"];
                            sensors.CopyTo(initSensors, 25 - sensors.Length);
                            Array.Reverse(initSensors);
                            gps.Sensors = initSensors;
                        }
                    }
                    else
                    {
                        ulong snsrs = Convert.ToUInt64(gpsDoData.Rows[k]["sensor"].ToString());
                        gps.Sensors = BitConverter.GetBytes(snsrs);
                    }

                    gps.SensorValue = calibrateSensor.GetUserValue(gps.Sensors, sensor.Length, sensor.StartBit, sensor.K, sensor.B, sensor.S);
                    gpsDiaData.Add(gps);
                } // for
            } // if

            return gpsDiaData;
        }

        /// <summary>
        /// Выборка данных. Заполнение рядов ValueSeries1 и ValueSeries2
        /// значениями датчиков уровня
        /// </summary>
        protected void SelectGrainValues(GrainDictionarys GrainDict, GpsData[] d_rows_sv)
        {
            //Calibrate calibrateSensor1 = null;
            //Calibrate calibrateSensor2 = null;

            List<Calibrate> calibrateSensors = new List<Calibrate>();

            for (int i = 0; i < GrainDict.GrainSensors.Count; i++)
            {
                if (GrainDict.GrainSensorExist(i))
                {
                    //Получаем, для соответствующего датчика GrainSensor, таблицу тарировки
                    s_row = GrainDict.GrainSensors[i];
                    calibrateSensors.Add(Calibration());
                }
            }

            //if (GrainDict.GrainSensor1Exist)
            //{
            //    //Получаем, для датчика GrainSensor1, таблицу тарировки
            //    s_row = GrainDict.GrainSensor1;
            //    calibrateSensor1 = Calibration();
            //}
            //if (GrainDict.GrainSensor2Exist)
            //{
            //    //Получаем, для датчика GrainSensor2, таблицу тарировки
            //    s_row = GrainDict.GrainSensor2;
            //    calibrateSensor2 = Calibration();
            //}

            if (d_rows_sv != null)
            {
                //foreach (GpsData d in d_rows_sv)
                for(int k = 0; k < d_rows_sv.Length; k++)
                {
                    GpsData d = d_rows_sv[k];
                    //if (calibrateSensor1 != null)
                    //{
                    //    double tmp = calibrateSensor1.GetUserValue(d.Sensors, GrainDict.GrainSensor1.Length,
                    //        GrainDict.GrainSensor1.StartBit, GrainDict.GrainSensor1.K);

                    //    GrainDict.ValueGrainSensor1.Add(d.Id, new ValuePairs(tmp, tmp));
                    //}

                    //if (calibrateSensor2 != null)
                    //{
                    //    double tmp = calibrateSensor2.GetUserValue(d.Sensors, GrainDict.GrainSensor2.Length,
                    //        GrainDict.GrainSensor2.StartBit, GrainDict.GrainSensor2.K);
                    //    GrainDict.ValueGrainSensor2.Add(d.Id, new ValuePairs(tmp, tmp));
                    //}

                    for (int i = 0; i < calibrateSensors.Count; i++)
                    {
                        if (calibrateSensors[i] != null)
                        {
                            double tmp = calibrateSensors[i].GetUserValue(d.Sensors, GrainDict.GrainSensors[i].Length,
                                GrainDict.GrainSensors[i].StartBit, GrainDict.GrainSensors[i].K, GrainDict.GrainSensors[i].B, GrainDict.GrainSensors[i].S);

                            GrainDict.valueGrainArrayDict[i].Add(d.Id, new ValuePairs(tmp, tmp));

                            //AverageGrain(GrainDict.valueGrainArrayDict[i]);
                        } // if
                    } // for
                } // foreach

                //List<GpsData[]> gpsDiapasonData = new List<GpsData[]>();
                //for (int i = 0; i < calibrateSensors.Count; i++)
                //{
                //    if (calibrateSensors[i] != null)
                //    {
                //        GpsData[] gpsDt = new GpsData[calibrateSensors.Count];
                //        gpsDiapasonData.Add(gpsDt);
                //        int count = d_rows_sv.Length;
                //        GpsData lastPoint = d_rows_sv[count - 1];
                //        int prevlogId = lastPoint.LogId + 1;
                //        int postlogId = lastPoint.LogId + 1;
                //        gpsDiapasonData[i] =
                //            getAdditionsData(GrainDict.GrainSensors[i], prevlogId, postlogId, lastPoint.Mobitel,
                //                true).ToArray();
                //    }
                //}

                //for (int i = 0; i < calibrateSensors.Count; i++)
                //{
                //    if (calibrateSensors[i] != null)
                //    {
                //        GpsData d = gpsDiapasonData[i][0];
                //        double tmp = calibrateSensors[i].GetUserValue(d.Sensors, GrainDict.GrainSensors[i].Length,
                //            GrainDict.GrainSensors[i].StartBit, GrainDict.GrainSensors[i].K);
                //        int length = GrainDict.valueGrainArrayDict[i].Count;
                //        long id = d_rows_sv[d_rows_sv.Length - 1].Id;
                //        GrainDict.valueGrainArrayDict[i].Add(id, new ValuePairs(tmp, tmp));

                //        //AverageGrain(GrainDict.valueGrainArrayDict[i]);
                //    } // if
                //} // for
            }

            //AverageGrain(GrainDict.ValueGrainSensor1);
            //AverageGrain(GrainDict.ValueGrainSensor2);

            for (int i = 0; i < GrainDict.valueGrainArrayDict.Count; i++)
            {
                 AverageGrain(GrainDict.valueGrainArrayDict[i]);
            } // for
        }

        /// <summary>
        /// Высчитывает по принятых данных бегуще среднее и заполняет соотведствующее поле ValuePairs.averageValue 
        /// </summary>
        public void AverageGrain(IDictionary<System.Int64, ValuePairs> valueSeries)
        {
            if (valueSeries.Count > 0)
            {
                atlantaDataSet.KilometrageReportRow[] kilometrage_rows = m_row.GetKilometrageReportRows();
                double[] data = new double[valueSeries.Count];
                //Копируем для возможности перебора всех элементов последовательно
                System.Int64[] keysValueSeries = new System.Int64[valueSeries.Count];
                valueSeries.Keys.CopyTo(keysValueSeries, 0);
                ValuePairs[] tmp_vPair = new ValuePairs[valueSeries.Count];
                valueSeries.Values.CopyTo(tmp_vPair, 0);

                #region __filter_downfall_valueData_test_

                int start = -1; // test zero_jump
                for (int i = 0; i < tmp_vPair.Length; i++)
                {
                    if (tmp_vPair[i].value > 0) 
                    {
                        if (start > 0) // возвращение с провала (фронт)
                        {
                            for (int j = start; j < i; j++) // заполняем весь промежуток значением с начала
                            {
                                valueSeries[keysValueSeries[j]].value = tmp_vPair[start - 1].value;
                            }
                        }
                        else if(start == 0)
                        {
                            for (int j = i; j >= start; j--) // заполняем весь промежуток значением с начала
                            {
                                valueSeries[keysValueSeries[j]].value = tmp_vPair[i].value;
                            }
                        }

                        start = -1;
                    }
                    else if (start < 0 && i >= 0) // старта небыло (спад)
                    {
                        start = i; // первая нулевая точка
                    }
                } // for

                if (tmp_vPair[tmp_vPair.Length - 1].value == 0)
                {
                    int k = tmp_vPair.Length - 1;

                    while (k >= 0 && tmp_vPair[k].value == 0)
                    {
                        k--;
                    }

                    int j = k;

                    while (j >= 0 && j < tmp_vPair.Length)
                    {
                        valueSeries[keysValueSeries[j]].value = tmp_vPair[k].value;

                        j++;
                    }
                }

                #endregion //__filter_downfall_valueData_test_

                valueSeries.Values.CopyTo(tmp_vPair, 0);
                data = ValuePairs.GetValue(tmp_vPair);

                int indexBegin = 0;
                int indexEnd = 200;

                if(kilometrage_rows.Length == 0)
                {
                    while (indexEnd < valueSeries.Count)
                    {
                        for (int j = indexBegin; j < indexEnd && j < valueSeries.Count; j++)
                        {
                            valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                        }

                        indexBegin = indexEnd;
                        indexEnd += indexEnd;
                    } // while

                    for (int j = indexBegin; j < valueSeries.Count; j++)
                    {
                        valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                    }
                } // if

                //Получаем усреднееное топливо для участков стоянок и движений отдельно

                foreach (atlantaDataSet.KilometrageReportRow k_row in kilometrage_rows)
                {
                    //Find indexes
                    indexBegin = Array.IndexOf<System.Int64>(keysValueSeries, k_row.InitialPointId);
                    indexEnd = Array.IndexOf<System.Int64>(keysValueSeries, k_row.FinalPointId);

                    if (indexBegin < 0)
                    {
                        indexBegin = 0;
                    }

                    if (indexEnd < 0)
                    {
                        indexEnd = 0;
                    }

                    for (int j = indexBegin; j < indexEnd; j++)
                    {
                        valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                    }
                }

                for (int j = indexEnd; j < valueSeries.Count; j++)
                {
                    valueSeries[keysValueSeries[j]].averageValue = average(data, indexBegin, indexEnd, j);
                }
            }
        }

        /// <summary>
        /// Вычисляет сумму от n датчиков уровня, если есть данные хотя бы от двух датчиков в словаре.
        /// Если датчик один - копирует его данные в словарь суммы. Для построение отчета используется только словарь суммы.
        /// </summary>
        /// <param name="GrainDict"></param>
        protected void CorrectGrainForTwoSensors(GrainDictionarys GrainDict)
        {
            //if (GrainDict.ValueGrainSensor1.Count > 0 && GrainDict.ValueGrainSensor2.Count > 0)
            ////если есть данные хотя бы от двух датчиков - строим их сумму
            //{
            //    System.Int64[] key = new System.Int64[GrainDict.ValueGrainSensor1.Count];
            //    GrainDict.ValueGrainSensor1.Keys.CopyTo(key, 0);

            //    for (int i = 0; i < GrainDict.ValueGrainSensor1.Count; i++)
            //    {
            //        double valueGrainSum = GrainDict.ValueGrainSensor1[key[i]].value +
            //                              GrainDict.ValueGrainSensor2[key[i]].value;
            //        double valueGrainSumAverage = GrainDict.ValueGrainSensor1[key[i]].averageValue +
            //                                     GrainDict.ValueGrainSensor2[key[i]].averageValue;
            //        GrainDict.ValueGrainSum.Add(key[i], new ValuePairs(valueGrainSum, valueGrainSumAverage));
            //    }
            //}
            //else
            //{
            //    //Копируем значение, потому что потом отчет строится по данным суммы
            //    GrainDict.ValueGrainSum = new Dictionary<System.Int64, ValuePairs>(GrainDict.ValueGrainSensor1);
            //}

            try
            {
                //если есть данные хотя бы от двух датчиков - строим их сумму
                int countMax = 0;
                int indexMax = 0;
                for (int k = 0; k < GrainDict.valueGrainArrayDict.Count; k++) // ищем первый не нулевой датчик топлива
                {
                    if (GrainDict.valueGrainArrayDict[k].Count > countMax)
                    {
                        countMax = GrainDict.valueGrainArrayDict[k].Count;
                        indexMax = k;
                    }
                }

                Int64[] key = null;

                for (int k = 0; k < GrainDict.valueGrainArrayDict.Count; k++)
                {
                    key = new Int64[GrainDict.valueGrainArrayDict[k].Count];
                    GrainDict.valueGrainArrayDict[k].Keys.CopyTo(key, 0);
                }

                if (GrainDict.valueGrainArrayDict.Count > 0)
                {
                    for (int j = 0; j < GrainDict.valueGrainArrayDict[indexMax].Count; j++) // тогда строим сумму остальных не нулевых датчиков
                    {
                        double valueGrainSum = 0;
                        double valueGrainSumAverage = 0;

                        for (int l = 0; l < GrainDict.valueGrainArrayDict.Count; l++)
                        {
                            if (GrainDict.valueGrainArrayDict[l].Count > j)
                            {
                                valueGrainSum += GrainDict.valueGrainArrayDict[l][key[j]].value;
                                valueGrainSumAverage += GrainDict.valueGrainArrayDict[l][key[j]].averageValue;
                            }
                        } // for

                        if (!GrainDict.ValueGrainSum.ContainsKey(key[j]))
                            GrainDict.ValueGrainSum.Add(key[j], new ValuePairs(valueGrainSum, valueGrainSumAverage));
                    } // for
                }

               //Копируем значение, потому что потом отчет строится по данным суммы
               //GrainDict.ValueGrainSum =
               //  new Dictionary<System.Int64, ValuePairs>(GrainDict.valueGrainArrayDict[k]);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error CorrectGrainForTwoSensors",
                    MessageBoxButtons.OK);
            }
        }

        #endregion //DUT Value

        #region private_methods

        /// <summary>
        /// Находит индекс элемента в словаре, со значениями суммарного топлива, с заданным DataGps_ID
        /// </summary>
        /// <param name="dgs_id"></param>
        /// <returns></returns>
        private System.Int64 FindIndexInSumValue(System.Int64 dgs_id)
        {
            System.Int64[] keys = new System.Int64[GrainDictionary.ValueGrainSum.Count];
            GrainDictionary.ValueGrainSum.Keys.CopyTo(keys, 0);
            return Array.IndexOf<System.Int64>(keys, dgs_id);
        }

        /// <summary>
        /// Изменяет значение топлива, если в настройках задан временной здвиг
        /// </summary>
        /// <param name="dValue">Ссылка, для хранения нового значения</param>
        /// <param name="kilometrage_row">KilometrageReportRow</param>
        /// <param name="isBeginStop">Признак поиска значения с начала остановки/вконце, 
        /// перед началом движения</param>
        private void MoveGrainValueOnTime(ref double dValue,
            atlantaDataSet.KilometrageReportRow kilometrage_row, bool isBeginStop)
        {
            try
            {
                if (set_row != null)
                {
                    if (kilometrage_row.Interval >= (set_row.TimeGetFuelAfterStop + set_row.TimeGetFuelBeforeMotion))
                    {
                        //если длительность стоянки больше, отступов от краeв остановки
                        TimeSpan timeMoving = set_row.TimeGetFuelBeforeMotion;
                        if (isBeginStop)
                        {
                            timeMoving = set_row.TimeGetFuelAfterStop;
                        }
                        if (timeMoving.TotalSeconds > 0) //если задан интервал сдвига
                        {
                            //GpsData[] tmpD_view = DataSetManager.GetGpsDataExceptLastPoint(
                            //    m_row,
                            //    kilometrage_row.InitialTime + set_row.TimeGetGrainAfterStop,
                            //    kilometrage_row.FinalTime - set_row.TimeGetGrainBeforeMotion);
                            GpsData[] tmpD_view =
                                GpsDatas.Where(
                                    gps => gps.Time >= kilometrage_row.InitialTime + set_row.TimeGetFuelAfterStop
                                           && gps.Time <= kilometrage_row.FinalTime - set_row.TimeGetFuelBeforeMotion
                                           && gps.Mobitel == m_row.Mobitel_ID).ToArray();

                            if (tmpD_view.Length > 0)
                            {
                                if (isBeginStop)
                                {
                                    //sumGrainRows[atlantaDataSet.GrainValue.SelectByDataGPSID(
                                    //sumGrainRows, tmpD_view[0].DataGps_ID)].Value;//.Average;
                                    if (GrainDictionary.ValueGrainSum.ContainsKey(tmpD_view[0].Id))
                                        dValue = GrainDictionary.ValueGrainSum[tmpD_view[0].Id].value;
                                }
                                else
                                {
                                    //sumGrainRows[atlantaDataSet.GrainValue.SelectByDataGPSID(
                                    //sumGrainRows, tmpD_view[tmpD_view.Length - 1].DataGps_ID)].Value;//.Average;
                                    if (GrainDictionary.ValueGrainSum.ContainsKey(tmpD_view[tmpD_view.Length - 1].Id))
                                        dValue = GrainDictionary.ValueGrainSum[tmpD_view[tmpD_view.Length - 1].Id].value;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error MoveGrainValueOnTime");
            }
        }

        private bool TestIgnition(GpsData[] gpsDatas)
        {
            bool ret = false;
            atlantaDataSet.sensorsRow engineSensor = FindSensor(AlgorithmType.WORK_E);
            if (engineSensor != null)
            {
                Calibrate calibr = Calibration();
                foreach (GpsData gpsData in gpsDatas)
                {
                    //if(calibr.GetUserValue(tmpDv_row, engineSensor.Length, engineSensor.StartBit, engineSensor.K) > 0)
                    //if (((gpsData.Sensors >> engineSensor.StartBit) & 0x01) > 0)//engine on
                    if (
                        calibr.GetUserValue(gpsData.Sensors, engineSensor.Length, engineSensor.StartBit, engineSensor.K, engineSensor.B, engineSensor.S) >
                        0)

                    {
                        ret = true;
                        break;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Возвращает откорректированые масив строк отчета о пробеге 
        /// (обединяет короткие движения с соседными остановками в одну запись)
        /// </summary>
        /// <param name="kr_row">Строки отчета о пробеге</param>
        /// <returns></returns>
        private atlantaDataSet.KilometrageReportRow[] CorrectingKilometrage(atlantaDataSet.KilometrageReportRow[] kr_row)
        {
            List<atlantaDataSet.KilometrageReportRow> kilometrage_corrected =
                new List<atlantaDataSet.KilometrageReportRow>();
            bool last_correct = false;
            TimeSpan timeBreak = new TimeSpan(0, 5, 0);
            if (set_row != null) //Если есть записи с настройками то берем данные оттуда
                timeBreak = set_row.TimeBreak;

            for (int n = 0; n < kr_row.Length - 2; n++)
            {
                System.Windows.Forms.Application.DoEvents();
                if (kr_row[n].Distance == 0 && kr_row[n].State != Resources.Movement)
                {
                    last_correct = false;
                    TimeSpan timeMoving = (kr_row[n + 1].Interval);
                        //после остановки сразу в отчете должно быть движение
                    if (timeMoving.TotalSeconds < timeBreak.TotalSeconds*1.2)
                        //ели движение было меньше, чем мин. время остановки +20% то проверяем небыло ли оно ложным
                    {
                        //List<atlantaDataSet.dataviewRow> list_dataview = new List<atlantaDataSet.dataviewRow>();
                        List<GpsData> list_dataGps = new List<GpsData>();
                        GpsData[] atlDv_row = null;
                        foreach (GpsData  tmpGpsData in GpsDatas) //Выбираем данные попадающе в интервал движения
                        {
                            if (tmpGpsData.Time >= kr_row[n + 1].InitialTime &&
                                tmpGpsData.Time <= kr_row[n + 1].FinalTime)
                            {
                                list_dataGps.Add(tmpGpsData);
                            }
                        }

                        atlDv_row = list_dataGps.ToArray();
                        //проверяем была ли на этом интервале нулевая скорость большую часть интервала //если небыло то таже операция
                        var gpsDataSpeed = new Dictionary<long, double>();
                        list_dataGps.ForEach(gpsData => gpsDataSpeed.Add(gpsData.Id, gpsData.Speed));
                        Spoint[] points = Algorithm.FindMin(gpsDataSpeed, 0); //получаем точки где скорость нулевая

                        //проверяем включалось ли зажигание на данном интервале при движении
                        bool engineOff = TestIgnition(atlDv_row);

                        if (points.Length != 0 || atlDv_row.Length == 1 || engineOff)
                            // Проверка наличия остановок ИЛИ если точка со скоростью была только одна
                        {
                            TimeSpan duration = new TimeSpan();
                            foreach (Spoint point in points)
                            {
                                DateTime timeY = GpsDatas.First(gps => gps.Id == point.Y).Time;
                                DateTime timeX = GpsDatas.First(gps => gps.Id == point.X).Time;
                                //duration += atlantaDataSet.dataview.FindByDataGps_ID(point.Y).time - atlantaDataSet.dataview.FindByDataGps_ID(point.X).time;
                                duration += timeY - timeX;
                            }
                            if (duration.TotalSeconds >= 2*timeMoving.TotalSeconds/3)
                                //Если время движения меньше 2/3 от всего движения, то обединяем две соседние остановки
                            {
                                //иначе обеденяем обе остановки
                                atlantaDataSet.KilometrageReportRow krr_row =
                                    AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();

                                krr_row.MobitelId = kr_row[n].MobitelId;
                                krr_row.InitialPointId = kr_row[n].InitialPointId;
                                krr_row.FinalPointId = kr_row[n + 2].FinalPointId;
                                krr_row.Distance = 0;
                                krr_row.InitialTime = kr_row[n].InitialTime;
                                krr_row.Location = kr_row[n].Location;
                                krr_row.FinalTime = kr_row[n + 2].FinalTime;
                                krr_row.Interval = krr_row.FinalTime - krr_row.InitialTime;
                                kilometrage_corrected.Add(krr_row);
                                n += 2; // перепрыгиваем следующее движение и остановку
                                last_correct = true;
                                continue;
                            }
                        }
                    }
                    kilometrage_corrected.Add(kr_row[n]);
                }
            }

            //TODO: Как должны быть расставлены скобки в следующих if ???
            if (!last_correct)
                //Если последняя запись с остановкой в отчете пробега не была использована при корректировке, добавим и ее
                if (kr_row[kr_row.Length - 1].Distance == 0) //если последняя запись остановка
                    kilometrage_corrected.Add(kr_row[kr_row.Length - 1]);
                else //если предпоследняя запись остановка
                    if (kr_row.Length >= 2)
                        kilometrage_corrected.Add(kr_row[kr_row.Length - 2]);
            return kilometrage_corrected.ToArray();
        }

        private atlantaDataSet.KilometrageReportRow[] CombineAdjacentStopsOrMoves(atlantaDataSet.KilometrageReportRow[] krRows)
        {
            var kmCombinedStopsAndMoves = new List<atlantaDataSet.KilometrageReportRow>();
            var stopsRows = new List<atlantaDataSet.KilometrageReportRow>();
            var moveRows = new List<atlantaDataSet.KilometrageReportRow>();
            for (int i = 0; i < krRows.Length; i++)
            {
                if (krRows[i].Distance == 0 && krRows[i].State != Resources.Movement)
                {
                    if (moveRows.Count() > 0)
                    {
                        GetCombinedMoves(moveRows, kmCombinedStopsAndMoves);
                    }
                    moveRows.Clear();
                    stopsRows.Add(krRows[i]);
                }
                else
                {
                    if (stopsRows.Count() > 0)
                    {
                        GetCombinedStops(stopsRows, kmCombinedStopsAndMoves);
                    }
                    stopsRows.Clear();
                    moveRows.Add(krRows[i]);
                }
            }
            if (stopsRows.Count() > 0)
            {
                GetCombinedStops(stopsRows, kmCombinedStopsAndMoves);
            }
            else if (moveRows.Count() > 0)
            {
                GetCombinedMoves(moveRows, kmCombinedStopsAndMoves);
            }
            return kmCombinedStopsAndMoves.ToArray();
        }

        private atlantaDataSet.KilometrageReportRow[] GetOneTotalKilometrageReportRow(atlantaDataSet.KilometrageReportRow[] krRows)
        {
            var totalKilometrageReportRows = new List<atlantaDataSet.KilometrageReportRow>();
            int indexStopRow = 0;
            for (int i = 0; i < krRows.Length; i++)
            {
                if (krRows[i].Distance == 0)
                {
                    indexStopRow = i;
                    break;
                }
            }
            krRows[indexStopRow].InitialPointId = krRows[0].InitialPointId;
            krRows[indexStopRow].FinalPointId = krRows[krRows.Count() -1].FinalPointId;
            krRows[indexStopRow].InitialTime = krRows[0].InitialTime;
            krRows[indexStopRow].FinalTime = krRows[krRows.Count() - 1].FinalTime;
            totalKilometrageReportRows.Add(krRows[indexStopRow]);
            return totalKilometrageReportRows.ToArray();
        }

        private void GetCombinedStops(List<atlantaDataSet.KilometrageReportRow> stopsRows, List<atlantaDataSet.KilometrageReportRow> krRowsCombinedStops)
        {
            var krRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            krRow.MobitelId = stopsRows[0].MobitelId;
            krRow.InitialPointId = stopsRows[0].InitialPointId;
            krRow.FinalPointId = stopsRows[stopsRows.Count - 1].FinalPointId;
            krRow.Distance = 0;
            krRow.InitialTime = stopsRows[0].InitialTime;
            krRow.Location = stopsRows[0].Location;
            krRow.FinalTime = stopsRows[stopsRows.Count - 1].FinalTime;
            krRow.Interval = krRow.FinalTime - krRow.InitialTime;
            krRow.State = stopsRows[0].State; 
            krRowsCombinedStops.Add(krRow);
        }

        private void GetCombinedMoves(List<atlantaDataSet.KilometrageReportRow> movesRows, List<atlantaDataSet.KilometrageReportRow> krRowsCombinedStops)
        {
            var krRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            krRow.MobitelId = movesRows[0].MobitelId;
            krRow.InitialPointId = movesRows[0].InitialPointId;
            krRow.FinalPointId = movesRows[movesRows.Count - 1].FinalPointId;
            krRow.Distance = 0;
            foreach (var movesRow in movesRows)
            {
                krRow.Distance += movesRow.Distance;
            }
            krRow.InitialTime = movesRows[0].InitialTime;
            krRow.FinalTime = movesRows[movesRows.Count - 1].FinalTime;
            krRow.Interval = krRow.FinalTime - krRow.InitialTime;
            krRow.State = movesRows[0].State;
            krRowsCombinedStops.Add(krRow);
        }

        /// <summary>
        /// Возвращает точку, для начала постройки прямой поиска уровня топлива в момент остановки
        /// </summary>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="current_stop"></param>
        /// <param name="inStop"></param>
        /// <param name="begin"></param>
        /// <returns></returns>
        private System.Int64 FindStartIndexLine(atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            int current_stop, ref bool inStop, System.Int64 beginIndex)
        {
            System.Int64 startIndex = FindIndexInSumValue(tmpKilimetrage_rows[current_stop - 1].FinalPointId);
            TimeSpan delta = (tmpKilimetrage_rows[current_stop].InitialTime -
                              tmpKilimetrage_rows[current_stop - 1].FinalTime);
            TimeSpan minutes = GetAproximationTime();

            if (delta < minutes)
            {
                delta = (tmpKilimetrage_rows[current_stop - 1].FinalTime -
                         tmpKilimetrage_rows[current_stop - 1].InitialTime); //время до начала предыдущей остановки
                double parcent_time = (delta.TotalMinutes/minutes.TotalMinutes);
                if (parcent_time <= 0)
                {
                    parcent_time = 1;
                }
                startIndex =
                    (int)
                        (startIndex -
                         (startIndex - FindIndexInSumValue(tmpKilimetrage_rows[current_stop - 1].InitialPointId))
                         /parcent_time);
                inStop = true;
            }
            else
            {
                if (minutes.TotalMinutes > 0 && delta.TotalMinutes >0)
                    startIndex =
                        (int) (beginIndex - 5 - (beginIndex - startIndex)/(delta.TotalMinutes/minutes.TotalMinutes));
                        //получаем начальный индекс данных за около 10 минут
                else
                {
                    startIndex = beginIndex;
                }
            }
            //выбираем данные в буффер
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            return startIndex;
        }

        /// <summary>
        /// Возвращает точку, для окончания постройки прямой поиска уровня топлива перед началом движения
        /// </summary>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="current_stop"></param>
        /// <param name="inStop"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private System.Int64 FindFinishIndexLine(atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            int current_stop, ref bool inStop, System.Int64 end)
        {
            System.Int64 finish =
                //atlantaDataSet.GrainValue.SelectByDataGPSID(sumGrainRows, tmpKilimetrage_rows[current_stop + 1].InitialPointId);
                FindIndexInSumValue(tmpKilimetrage_rows[current_stop + 1].InitialPointId);
            TimeSpan delta = tmpKilimetrage_rows[current_stop + 1].InitialTime -
                             tmpKilimetrage_rows[current_stop].FinalTime; //Длительность до следующей остановки
            TimeSpan aproximTime = GetAproximationTime();
            if (delta < aproximTime)
            {
                aproximTime -= delta;
                delta = (tmpKilimetrage_rows[current_stop + 1].FinalTime -
                         tmpKilimetrage_rows[current_stop + 1].InitialTime); //время до конца следующей остановки
                double parcent_time = (delta.TotalMinutes/aproximTime.TotalMinutes);
                if (parcent_time <= 0)
                {
                    parcent_time = 1;
                }
                if (delta.TotalMinutes > 0)
                {
                    finish =
                        (int)
                            (finish +
                             ( //atlantaDataSet.GrainValue.SelectByDataGPSID(sumGrainRows, tmpKilimetrage_rows[current_stop + 1].FinalPointId)
                                 FindIndexInSumValue(tmpKilimetrage_rows[current_stop + 1].FinalPointId)
                                 - finish)/parcent_time);
                }
                inStop = true;
            }
            else
            {
                if (aproximTime.TotalMinutes > 0)
                {
                    int partTime = (int) (delta.TotalMinutes/aproximTime.TotalMinutes);
                    if (partTime > 0)
                        finish = end + (finish - end)/partTime; //получаем конечный индекс данных за около 10 минут
                }
                else
                {
                    finish = end;
                }
            }
            return finish;
        }

        private TimeSpan GetAproximationTime()
        {
            if (set_row != null)
            {
                return set_row.FuelApproximationTime;
            }
            else
            {
                return new TimeSpan(0, 10, 0);
            }
        }

        /// <summary>
        /// Выбираем точки за интервал, для построения линии
        /// </summary>
        /// <param name="len"></param>
        /// <param name="beginIndex"></param>
        /// <returns></returns>
        private double[] SelectLinePoints(int len, System.Int64 beginIndex)
        {
            if (len < 0)
            {
                len = 0;
            }
            double[] line_point = new double[len];
            ValuePairs[] GrainValue = new ValuePairs[GrainDictionary.ValueGrainSum.Count];
            GrainDictionary.ValueGrainSum.Values.CopyTo(GrainValue, 0);
            for (int l = 0; l < line_point.Length; l++)
            {
                System.Int64 num = beginIndex + l;
                if (num >= GrainValue.Length)
                    num = GrainValue.Length - 1;

                line_point[l] = GrainValue[num].averageValue;
            }

            return line_point;
        }

        /// <summary>
        /// Вычисляет начальный и конечный уровень топлива для остановки. Использует прямое получения данных, метод постройки
        /// прямой методом наименьших квадратов, методом сдвига момента времени получения данных.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="beginValue"></param>
        /// <param name="endValue"></param>
        private bool CountBeginAndEndGrainValue(int numStop, atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows,
            ref double beginValue, ref double endValue, ref System.Int64 beginIndex, ref System.Int64 endIndex,
            bool newSearch)
        {
            bool ret = true;

            if (newSearch) //Math.Round(beginValue, 3) == 0 && Math.Round(endValue, 3) == 0)
                beginIndex = FindIndexInSumValue(tmpKilimetrage_rows[numStop].InitialPointId);
            else
                beginIndex = endIndex;

            endIndex = FindIndexInSumValue(tmpKilimetrage_rows[numStop].FinalPointId);

            if (GrainDictionary.ValueGrainSum.ContainsKey(tmpKilimetrage_rows[numStop].InitialPointId))
                beginValue = GrainDictionary.ValueGrainSum[tmpKilimetrage_rows[numStop].InitialPointId].averageValue;
                ///sumGrainRows[begin].Value;
            if (GrainDictionary.ValueGrainSum.ContainsKey(tmpKilimetrage_rows[numStop].FinalPointId))
                endValue = GrainDictionary.ValueGrainSum[tmpKilimetrage_rows[numStop].FinalPointId].averageValue;
                //sumGrainRows[end - 1].Value;
            if (!newSearch) //Только один раз выполняем при отключеном useMinMaxAlgoritm
                ret = false;
            // Вычисляем начальные и конечные значения методом наимельших квадратов
            if (useMinMaxAlgoritm == MIN_MAX_DISABLE)
            {
                #region defaultAlgorithm

                if (0 != numStop)
                {
                    //проверяем длительность движения до
                    bool inStop = false;
                    System.Int64 startIndex = FindStartIndexLine(tmpKilimetrage_rows, numStop, ref inStop, beginIndex);
                    double[] line_point = SelectLinePoints((int) (beginIndex - startIndex - 3), startIndex + 1);
                        //3-Для построения прямой берется больше двух точек
                    if (line_point.Length >= 2)
                    {
                        bool isLineIncrease = false;
                        double tmpBeginValue = GetValueMetodomLine(line_point, line_point.Length - 1, ref isLineIncrease);
                        if (isLineIncrease && (beginValue - tmpBeginValue) < 0)
                        {
                            beginValue = (tmpBeginValue + beginValue)/2;
                        }
                        else if (!inStop) //|| Math.Abs(beginValue - tmpBeginValue) < GrainingEdge)
                        {
                            beginValue = tmpBeginValue;
                        }
                    }
                    MoveGrainValueOnTime(ref beginValue, tmpKilimetrage_rows[numStop], true);
                }

                if (numStop + 1 < tmpKilimetrage_rows.Length)
                {
                    //проверяем длительность движения после
                    bool inStop = false;
                    System.Int64 finish = FindFinishIndexLine(tmpKilimetrage_rows, numStop, ref inStop, endIndex);
                    double[] line_point = SelectLinePoints((int) (finish - endIndex), endIndex);
                    if (line_point.Length >= 2)
                    {
                        bool isLineIncrease = false;
                        double tmpEndValue = GetValueMetodomLine(line_point, 0, ref isLineIncrease);
                        if (isLineIncrease && (endValue - tmpEndValue) > 0)
                        {
                            endValue = (tmpEndValue + endValue)/2;
                        }
                        else if (!inStop || Math.Abs(endValue - tmpEndValue) < GrainingEdge)
                        {
                            endValue = tmpEndValue;
                        }
                    }
                    MoveGrainValueOnTime(ref endValue, tmpKilimetrage_rows[numStop], false);
                }

                #endregion //defaultAlgorithm
            }
            else
            {
                //count begin/endValue on Min/Max algorithm
                ValuePairs[] GrainValue = new ValuePairs[GrainDictionary.ValueGrainSum.Count];
                GrainDictionary.ValueGrainSum.Values.CopyTo(GrainValue, 0);
                double maxValue = 0;
                double minValue = 0;
                System.Int64 maxIndex = -1;
                System.Int64 minIndex = beginIndex;
                if (useMinMaxAlgoritm == MIN_MAX_ONE  && newSearch)
                {
                    #region AlgorithmMinMax_1

                    for (System.Int64 i = beginIndex; i < GrainValue.Length && i < endIndex; i++) //find maximum
                    {
                        if (GrainValue[i].averageValue > maxValue)
                        {
                            maxValue = GrainValue[i].averageValue;
                            maxIndex = i;
                        }
                    }
                    minValue = maxValue;
                    minIndex = maxIndex;
                    for (System.Int64 i = beginIndex; i < GrainValue.Length && i < maxIndex; i++) //find minimum
                    {
                        if (GrainValue[i].averageValue < minValue)
                        {
                            minValue = GrainValue[i].averageValue;
                            minIndex = i;
                        }
                    }
                    if (minIndex < maxIndex && (maxValue - minValue) > GrainingEdge) //Если найшли заправку
                    {
                        endValue = maxValue;
                        beginValue = minValue;
                        endIndex = maxIndex;
                        beginIndex = minIndex;
                    }

                    #endregion
                }
                else
                {
                    if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP || useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP_ONLY_OUT)
                    {
//use MIN_MAX=2 or MIN_MAX=3

                        #region AlgorithmMinMax_2

                        double GrainrateOnSecond = flowmeterNorm/3600;
                        bool isBeginChange = false;
                        maxIndex = endIndex;

                        for (System.Int64 i = beginIndex; i < GrainValue.Length - 1 && i < endIndex - 1; i++)
                        {
                            double Grainrate = Math.Abs((GrainValue[i].averageValue - GrainValue[i + 1].averageValue)/
                                                       (GpsDatas[i + 1].Time - GpsDatas[i].Time).TotalSeconds);
                            if (Grainrate*2 > GrainrateOnSecond)
                            {
                                //begin changeGrain
                                if (!isBeginChange)
                                    minIndex = i;
                                isBeginChange = true;
                            }
                            else
                            {
                                if (isBeginChange)
                                {
                                    //save change
                                    isBeginChange = false;
                                    maxIndex = i;
                                    break;
                                }
                            }
                        }

                        if (isBeginChange)
                            maxIndex = endIndex;

                        maxValue = GrainValue[maxIndex].averageValue;
                        minValue = GrainValue[minIndex].averageValue;
                        if (minIndex < maxIndex && maxIndex != endIndex && minIndex != endIndex - 1)
                            //еще не добрались до конца
                        {
                            ret = true;
                            endValue = maxValue;
                            beginValue = minValue;
                            beginIndex = minIndex;
                            endIndex = maxIndex;
                        }

                        #endregion
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Сравнивает уровни топлива на соседних остановках с уровнем на этой, для поиска выбросов.
        /// </summary>
        /// <param name="beginVal"></param>
        /// <param name="endVal"></param>
        /// <param name="tmpKilimetrage_rows"></param>
        /// <param name="numStop"></param>
        /// <returns>true - заправка/слив, false - выброс</returns>
        private bool TestTrueGraining(double beginVal, double endVal,
            atlantaDataSet.KilometrageReportRow[] tmpKilimetrage_rows, int numStop)
        {
            int equal = 0;
            double before_begin = 0; // beginVal;
            double after_end = 0; // endVal;
            TimeSpan timeMoveBefo = new TimeSpan(0, 0, 0), timeMoveAfter = new TimeSpan(0, 0, 0);
            if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP)
                return true;

            if (0 != numStop)
            {
                timeMoveBefo = (tmpKilimetrage_rows[numStop].InitialTime - tmpKilimetrage_rows[numStop - 1].FinalTime);
                before_begin = GrainDictionary.ValueGrainSum[tmpKilimetrage_rows[numStop - 1].FinalPointId].averageValue;
            }
            if (numStop + 1 < tmpKilimetrage_rows.Length)
            {
                timeMoveAfter = tmpKilimetrage_rows[numStop + 1].InitialTime - tmpKilimetrage_rows[numStop].FinalTime;
                after_end = GrainDictionary.ValueGrainSum[tmpKilimetrage_rows[numStop + 1].InitialPointId].averageValue;
                
            }

            double deltaGrainValue = beginVal - endVal; //по дефолту - слив
            double GrainStep = GrainingDischarge;
            if (endVal - beginVal > GrainingEdge) //заправка
            {
                GrainStep = GrainingEdge;
                deltaGrainValue = endVal - beginVal;
            }

            if (timeMoveBefo.TotalHours > 0 && before_begin > 0) //было небольшое движение до и было получено значение
            {
                //Если перед заправкой был повышенный/пониженный расход на величину в пол порога к величине заправки/слива - считаем ее ложной
                double flowmeterValue = (before_begin - beginVal);
                double deltaValue = Math.Abs(flowmeterValue - flowmeterNorm * timeMoveBefo.TotalHours);

                if (Math.Abs(deltaValue - deltaGrainValue) < GrainStep/2)
                {
                    equal++;
                }
            }

            if (timeMoveAfter.TotalHours > 0 && after_end > 0)
            {
                double flowmeterValue = (endVal - after_end);
                double deltaValue = Math.Abs(flowmeterValue - flowmeterNorm*timeMoveAfter.TotalHours);

                if (Math.Abs(deltaValue - deltaGrainValue) < GrainStep/2)
                {
                    equal++;
                }
            }
            return ((equal%2) == 0);
        }

        private bool TestTrueDischarge(atlantaDataSet.KilometrageReportRow km_rows, double GrainStart, double GrainEnd)
        {
            //double[] tachometerVal = ReportTabControl.Dataset.TachometerValue.SelectByMobitel(m_row);
            bool ret = true;
            if (useMinMaxAlgoritm == MIN_MAX_AGGREGATE_ON_STOP)
                return true;

            atlantaDataSet.TachometerValueRow[] rotVal_row = m_row.GetTachometerValueRows();
            if (rotVal_row.Length > 0 && (GrainStart - GrainEnd > GrainingDischarge) &&
                rotVal_row.Length == GrainDictionary.ValueGrainSum.Count) //только если есть обороты! и слив
            {
                int minRotate = 10;
                double normGrainrate = 30;

                if (set_row != null) //Если есть записи с насройками то берем данные оттуда
                {
                    minRotate = set_row.RotationMain;
                    normGrainrate = set_row.AvgFuelRatePerHour;
                }

                System.Int64 beginIndex = FindIndexInSumValue(km_rows.InitialPointId);
                System.Int64 endIndex = FindIndexInSumValue(km_rows.FinalPointId);
                ValuePairs[] GrainValue = new ValuePairs[GrainDictionary.ValueGrainSum.Count];
                GrainDictionary.ValueGrainSum.Values.CopyTo(GrainValue, 0);

                TimeSpan timeRotate = new TimeSpan(0);
                System.Int64 startRotate = -1;
                System.Int64 endRotate = -1;

                for (System.Int64 i = beginIndex; i < endIndex; i++)
                {
                    if (rotVal_row[i].Value >= minRotate && endRotate >= startRotate)
                    {
                        startRotate = i;
                    }
                    if (endRotate < startRotate && rotVal_row[i].Value < minRotate)
                    {
                        endRotate = i;
                        //timeRotate += (rotVal_row[endRotate].dataviewRow.time - rotVal_row[startRotate].dataviewRow.time);
                        timeRotate += (GpsDatas.First(gps => gps.Id == rotVal_row[endRotate].Id).Time
                                       - GpsDatas.First(gps => gps.Id == rotVal_row[startRotate].Id).Time);
                    }
                }

                if (endRotate < startRotate)
                    timeRotate += (GpsDatas.First(gps => gps.Id == rotVal_row[endIndex].Id).Time
                                   - GpsDatas.First(gps => gps.Id == rotVal_row[startRotate].Id).Time);
                
                //timeRotate += (rotVal_row[endIndex].dataviewRow.time - rotVal_row[startRotate].dataviewRow.time);

                double maxAdmitGrainrate = timeRotate.TotalHours*normGrainrate;
                if (maxAdmitGrainrate > 0 && maxAdmitGrainrate > (GrainStart - GrainEnd))
                    ret = false;
            }
            //atlantaDataSet.RotateReportRow[] rot_rows = (atlantaDataSet.RotateReportRow[])atlantaDataSet.RotateReport.Select("State = 'Работает' AND MobitelId=" + m_row.Mobitel_ID.ToString()
            //  + " AND ((InitialTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and InitialTime  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#" +
            //  ") OR (FinalTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and FinalTime  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#  and InitialTime  < #" + dtSeek.ToString("MM.dd.yyyy") + "#))", "InitialTime");
            return ret;
        }

        void CombineSimilarActions()
        {
            if (m_row.GetGrain_ReportRows() != null)
            {
                atlantaDataSet.Grain_ReportRow[] fRows = m_row.GetGrain_ReportRows();
                if (fRows.Length > 1)
                {
                    atlantaDataSet.Grain_ReportRow prevFrow = fRows[0];
                    for (int i = 1; i < fRows.Length; i++)
                    {
                        if (Math.Sign(prevFrow.dValue) == Math.Sign(fRows[i].dValue))
                        {
                            if (fRows[i].beginTime.Subtract(prevFrow.endTime) <= timeForCombine)
                            {
                                prevFrow.endValue = fRows[i].endValue;
                                prevFrow.dValue = prevFrow.endValue - prevFrow.beginValue;
                                prevFrow.endTime = fRows[i].endTime;
                                AtlantaDataSet.Grain_Report.RemoveGrain_ReportRow(fRows[i]);
                            }
                            else
                                prevFrow = fRows[i];
                        }
                        else
                            prevFrow = fRows[i];
                    }
                    //atlantaDataSet.GrainReport.RemoveGrainReportRow();
                    //atlantaDataSet.GrainReportRow fRow = null;
                    //AtlantaDataSet.GrainReport.RemoveGrainReportRow(fRow);
                }

                //if (Math.Sign(fRows[fRows.Length - 1].dValue) == Math.Sign(dValue))
                //{
                    
                //}


                //atlantaDataSet.GrainReportRow f_row = AtlantaDataSet.GrainReport.NewGrainReportRow();
                //f_row.mobitel_id = this.m_row.Mobitel_ID;
                //f_row.sensor_id = base.s_row.id;
                //f_row.Location = k_row.Location;
                //f_row.beginValue = beginValue;
                //f_row.endValue = endValue;
                //f_row.dValue = endValue - beginValue;

                //if (f_row.dValue > 0)
                //{
                //    summGrain += f_row.dValue;
                //    iSummGrainCount++;
                //}
                //else
                //{
                //    diffGrain += f_row.dValue;
                //    iDiffGrainCount++;
                //}

                //System.Int64 indGrain = disp(beginIndex, endIndex);
                //f_row.pointValue = GrainDictionary.ValueGrainSum[GpsDatas[indGrain].Id].averageValue;
                ////Мгновенный уровень топлива в момент заправки
                //f_row.DataGPS_ID = GpsDatas[indGrain].Id;
                //f_row.time_ = GpsDatas[indGrain].Time;
                //f_row.Lat = GpsDatas[indGrain].LatLng.Lat;
                //f_row.Lon = GpsDatas[indGrain].LatLng.Lng;
                //f_row.GenerateType = "system";
            }

        }

        #endregion

        #region --   Nested struct Grain.Summary   --

        /// <summary>
        /// Представляет итоговые данные отчета о заправках и сливах
        /// по конкретному телетреку (транспортному средству).
        /// </summary>
        public struct Summary
        {
            /// <summary>
            /// Литров топлива в начале отчетного периода
            /// </summary>
            public double Before;

            /// <summary>
            /// Литров топлива в конце отчетного периода
            /// </summary>
            public double After;

            /// <summary>
            /// Литров топлива, заправленного за отчетный период
            /// </summary>
            public double Graining;

            /// <summary>
            /// Количество заправок за отчетный период
            /// </summary>
            public int GrainingCount;

            /// <summary>
            /// Литров топлива, слитого за отчетный период
            /// </summary>
            public double Discharge;

            /// <summary>
            /// Количество сливов за отчетный период
            /// </summary>
            public int DischargeCount;

            /// <summary>
            /// Всего литров топлива, израсходованных транспортным средством
            /// (т.е. без учета слитого топлива)
            /// </summary>
            public double Total;

            /// <summary>
            /// Средний расход топлива на 100 км пробега
            /// </summary>
            public double Rate;

            /// <summary>
            /// Средняя скорость движения
            /// </summary>
            public double AvgSpeed;
        }

        #endregion
    }

    /// <summary>
    /// Инкапсулирует параметры для обработчика события окончания
    /// работы алгоритма подготовки данных для отчета о заправках 
    /// и сливах по конкретному транспортному средству
    /// </summary>
    public class GrainEventArgs : EventArgs
    {
        /// <summary>
        /// Создает и инициализирует новый экземпляр класса BaseReports.Procedure.GrainEventArgs
        /// </summary>
        /// <param name="id">ID телетрека</param>
        /// <param name="summary">Итоговые данные отчета</param>
        public GrainEventArgs(int id, Grain.Summary summary)
        {
            _id = id;
            _summary = summary;
        }

        /// <summary>
        /// ID телетрека
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        private int _id;

        /// <summary>
        /// Итоговые данные отчета о заправках и сливах по конкретному
        /// телетреку (транспортному средству).
        /// </summary>
        public Grain.Summary Summary
        {
            get
            {
                return _summary;
            }
        }

        private Grain.Summary _summary;
    }
}
