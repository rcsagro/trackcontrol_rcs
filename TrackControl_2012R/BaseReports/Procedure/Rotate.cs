using System;
using BaseReports.Procedure.Calibration;
using BaseReports.Properties;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    public class Rotate : Algorithm, IAlgorithm
    {
        #region Attribute

        AlgorithmType algType;

        #endregion

        public Rotate(object param)
            : base()
        {
            if (param is AlgorithmType)
            {
                algType = (AlgorithmType) param;
            }
            else
            {
                throw new Exception(Resources.AlgorithmTypeError);
            }
        }

        #region IAlgorithm Members

        void IAlgorithm.Run()
        {
            AfterReportFilling();
            Algorithm.OnAction(this, null);
        }

        void IAlgorithm.SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
            this.S_rows = FindSensors(algType);
            if (S_rows == null)
            {
                return;
            }
            if (m_row.GetRotateValueRows().Length != 0)
            {
                return;
            }
            foreach (atlantaDataSet.sensorsRow sensor in S_rows)
            {
                if (sensor.GetRotateValueRows().Length == 0)
                    //���� ��� ����� ������� ��� ��������� �������, �� ���������� ���
                {
                    s_row = sensor;
                    Calibrate calibrate = Calibration();

                    foreach (GpsData d in GpsDatas)
                    {
                        atlantaDataSet.RotateValueRow rotateRow = AtlantaDataSet.RotateValue.NewRotateValueRow();
                        rotateRow.DataGPS_ID = d.Id;
                        rotateRow.Mobitel_ID = m_row.Mobitel_ID;
                        rotateRow.Sensor_ID = sensor.id;
                        rotateRow.Value = calibrate.GetUserValue(d.Sensors, sensor.Length, sensor.StartBit, sensor.K,
                            sensor.B, sensor.S);
                        AtlantaDataSet.RotateValue.AddRotateValueRow(rotateRow);
                    }
                }
            }
        }

        #endregion
    }
}
