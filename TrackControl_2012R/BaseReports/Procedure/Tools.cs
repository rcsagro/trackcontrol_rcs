
namespace BaseReports.Procedure
{
  public struct Spoint
  {
      public Spoint(System.Int64 x, System.Int64 y)
    {
      this.x = x;
      this.y = y;
    }
      private System.Int64 x;
      public System.Int64 X
    {
      get { return x; }
      set { x = value; }
    }
      private System.Int64 y;
      public System.Int64 Y
    {
      get { return y; }
      set { y = value; }
    }
  }
}