﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BaseReports.Properties;
using BaseReports.RFID;
using DevExpress.Office.Utils;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    /// <summary>
    /// отчет по агрегатам согласно их RFID идентификаторам
    /// </summary>
    class AggregateRfid : Algorithm
    {
        public AggregateRfid()
        {
            // to do this
        }

        public override void Run()
        {
            if(GpsDatas == null || GpsDatas.Length == 0 || m_row == null)
                return;

            int idMobitel = m_row.Mobitel_ID;
            using (AggregateRFIDRecordsCreator creator = new AggregateRFIDRecordsCreator())
            {
                creator.ProgressChanged += ReportProgressChanged;
                creator.IdMobitel = idMobitel;
                creator.m_row = m_row;
                if (AggregateRfidRecords.ContainsKey(idMobitel))
                {
                    SelectItem(m_row);
                    Algorithm.OnAction(this, new EventArgs());
                }
                else
                {
                    AggregateRfidRecords.Add(idMobitel, new List<AggregateRFIDRecord>());
                }
                BeforeReportFilling(Resources.AggregateRFID, GpsDatas.Length);
                creator.SetDefaultObservers();
                
                if (!creator.CuttingDataGpsByRfid(GpsDatas))
                {
                    AfterReportFilling();
                    return;
                }

                creator.WriteNoRecordedGps();
                creator.ProgressChanged -= ReportProgressChanged;
            } // using
            AfterReportFilling();
            Algorithm.OnAction(this, new EventArgs());
        } // Run
    }
}
