using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars.ViewInfo;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    /// <summary>
    /// ����� ��� ���������� ������ ��� ������ � ������� ������������
    /// �������� � ���������� ��������������� ������� � ��������.
    /// ����� � ������� �������� ������ � ���������� �������� � ���������
    /// </summary>
    public class Kilometrage : Algorithm, IAlgorithm
    {
        private bool _notBegin;

        /// <summary>
        /// �������� ������ ������ � ������� � ����������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        private Summary _summary;

        private int value = 0;

        /// <summary>
        /// ����� �������, ���������� �������� ����������� � ������, ��� ���������.
        /// �������� �� ��������� ���������� ���� �����.
        /// </summary>
        private TimeSpan _legalTimeBreak = new TimeSpan(0, 5, 0);

        public event Action<KilometrageEventArgs> ReportCompleted = delegate { };

        class Interval
        {
            private DateTime time;
            //private double state;

            public Interval()
            {
                this.time = new DateTime();
                //this.state = 0;
            }

            public DateTime Time
            {
                get { return this.time; }
                set { this.time = value; }
            }
        } // Interval

        List<Interval> list_beginer = new List<Interval>();
        List<Interval> list_ender = new List<Interval>();

        // ��������� ������� ��� �������� ������������� ������
        atlantaDataSet.KilometrageReportDataTable tempReport = new atlantaDataSet.KilometrageReportDataTable();

        public static double MinTimeMove
        {
            get { return _minTimeMove; }
            set { _minTimeMove = value; }
        }

        #region IAlgorithm Members

        private GpsData copyObject(GpsData gpsdata)
        {
            GpsData gpsdatacop = new GpsData();
            gpsdatacop.Time = new DateTime(gpsdata.Time.Year, gpsdata.Time.Month, gpsdata.Time.Day, gpsdata.Time.Hour,
                gpsdata.Time.Minute, gpsdata.Time.Second);
            gpsdatacop.Dist = gpsdata.Dist;
            gpsdatacop.Id = gpsdata.Id;
            gpsdatacop.Speed = gpsdata.Speed;
            gpsdatacop.Accel = gpsdata.Accel;
            gpsdatacop.Altitude = gpsdata.Altitude;
            gpsdatacop.Direction = gpsdata.Direction;
            gpsdatacop.Events = gpsdata.Events;
            gpsdatacop.LatLng = gpsdata.LatLng;
            gpsdatacop.LatLngDgps = gpsdata.LatLngDgps;
            gpsdatacop.LogId = gpsdata.LogId;
            gpsdatacop.Mobitel = gpsdata.Mobitel;
            gpsdatacop.RssiGsm = gpsdata.RssiGsm;
            gpsdatacop.Satellites = gpsdata.Satellites;
            gpsdatacop.SensorValue = gpsdata.SensorValue;
            gpsdatacop.Sensors = gpsdata.Sensors;
            gpsdatacop.Speed = gpsdata.Speed;
            gpsdatacop.SrvPacketID = gpsdata.SrvPacketID;
            gpsdatacop.SensorsSet = gpsdata.SensorsSet;
            gpsdatacop.UnixTime = gpsdata.UnixTime;
            gpsdatacop.Valid = gpsdata.Valid;
            gpsdatacop.Voltage = gpsdata.Voltage;
            gpsdata.sEvents = gpsdata.sEvents;
            gpsdatacop.sVoltage = gpsdata.sVoltage;
            return gpsdatacop;
        }

        /// <summary>
        /// ����������� ������ ����������� ���������, ��������� �����
        /// � ��������� ������� ��������������� ������� � ��������.
        /// </summary>
        public override void Run()
        {
            try
            {
                if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
                {
                    _minTimeMove = set_row.TimeMinimMovingSize.TotalSeconds;
                }

                // ������� ��������� �������
                for (; tempReport.Rows.Count > 0;)
                {
                    atlantaDataSet.KilometrageReportRow rW = tempReport[0];
                    tempReport.RemoveKilometrageReportRow(rW);
                } // for

                // �������� ������� �������
                if (GpsDatas == null || 0 == GpsDatas.Length || m_row == null)
                    return;

                if (m_row.GetKilometrageReportRows() != null)
                {
                    if (m_row.GetKilometrageReportRows().Length != 0)
                    {
                        return;
                    }
                }

                BeforeReportFilling(Resources.Kilometrage, GpsDatas.Length);

                // ������� ����� � ��������� ��� �������. ����� ��� ����������� �������������
                // ����� ��� ����������� �� �����.
                GpsData breakPoint = GpsDatas[0];

                // ��������� ����� ��������� ��� �������� � ���������
                GpsData beginPoint = GpsDatas[0];

                // �������� ����� ��������� ��� �������� � ���������
                GpsData endPoint = GpsDatas[0];

                // ������ � ������� ��������� �������
                double distance = GpsDatas[0].Dist;

                // ������������ �������� �� DataGps
                double MaxSpeed = GpsDatas[0].Speed;

                // ���� �������� ������������� ��������
                bool isMoving = GpsDatas[0].Speed > 0;

                if (set_row != null)
                {
                    _legalTimeBreak = set_row.TimeBreak;
                }
                _notBegin = false;
                _summary = new Summary();
                byte j = 0;

                // ��������� ������� ����������� �� ���� �� ���������� ������
                int i = 0;
                int indexEndPoint = -1;
                double tempSpeed = 0.0;
                int countLimitParking = 0;
                List<GpsData> limitPointsParking = new List<GpsData>();
                int countLimitMove = 0;
                List<GpsData> limitPointsMove = new List<GpsData>();
                TimeSpan timeBreak = new TimeSpan(0, 0, 0, 0);

                //=============================

                for (i = 1; i < GpsDatas.Length; i++) // for optimization, 28 second
                {
                    if (GpsDatas[i].Speed > MaxSpeed)
                        MaxSpeed = GpsDatas[i].Speed;

                    DateTime dt = GpsDatas[i].Time;

                    if (!isMoving && GpsDatas[i].Speed > 0) // ���� �������, ����� ��������
                    {
                        timeBreak = GpsDatas[i].Time - endPoint.Time;

                        if (timeBreak >= _legalTimeBreak)
                        {
                            if (endPoint != beginPoint)
                            {
                                addMotionToReport(breakPoint, beginPoint, endPoint, distance, ref MaxSpeed); // add Movement
                                distance = 0;
                            }

                            TimeSpan tmBreak = new TimeSpan(timeBreak.Days, timeBreak.Hours, timeBreak.Minutes, timeBreak.Seconds);
                            addBreakToReport(endPoint, GpsDatas[i], tmBreak); // add Parking

                            beginPoint = GpsDatas[i];
                            breakPoint = endPoint;
                            endPoint = null;

                            //if (_notBegin)
                            //    distance = 0;
                        } // if

                        isMoving = true;
                    } // if

                    if (isMoving && GpsDatas[i].Speed == 0) // ���� ��������, ����� �������
                    {
                        isMoving = false;
                        endPoint = GpsDatas[i];
                        indexEndPoint = i;
                    }

                    if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() &&
                        !FormInterval.isActiveTime() &&
                        FormInterval.isActivateNewAlgorythm)
                    {
                        // �������� ������ ����������� ������ ������� � �������� ������� ���
                        if ((i + 1 < GpsDatas.Length) && GpsDatas[i].Time.Day != GpsDatas[i + 1].Time.Day)
                        {
                            //GpsData pointTemp = endPoint;

                            if (GpsDatas[i].Speed == 0)
                            {
                                if (beginPoint != endPoint)
                                {
                                    if ((beginPoint.Speed > 0) && (endPoint.Speed == 0))
                                    {
                                        //if ((GpsDatas[indexEndPoint + 1].Time.Day < GpsDatas[i].Time.Day))
                                        //{
                                        //    endPoint = copyObject(beginPoint);
                                        //}

                                        endPoint.Time = endPoint.Time.AddSeconds(3);
                                        endPoint.Speed = GpsDatas[i].Speed;
                                        endPoint.Dist = GpsDatas[i].Dist;
                                        addMotionToReport(breakPoint, beginPoint, endPoint, distance, ref MaxSpeed); // add Movement
                                        beginPoint = endPoint;
                                        distance = 0;
                                    }
                                }

                                //endPoint = pointTemp;
                                //if (!(GpsDatas[indexEndPoint + 1].Time.Day < GpsDatas[i].Time.Day))
                                {
                                    endPoint = GpsDatas[i];
                                }

                                timeBreak = GpsDatas[i].Time - beginPoint.Time;
                                addBreakToReport(beginPoint, endPoint, timeBreak); // add Parkin
                                beginPoint = GpsDatas[i + 1];
                                breakPoint = endPoint;
                                endPoint = GpsDatas[i + 1]; 
                            }
                            else if ((beginPoint.Speed > 0) && (GpsDatas[i].Speed > 0))
                            {
                                addMotionToReport(breakPoint, beginPoint, GpsDatas[i], distance, ref MaxSpeed); // add Movement
                                distance = 0;
                                beginPoint = GpsDatas[i];
                                breakPoint = GpsDatas[i];
                                endPoint = GpsDatas[i + 1];
                            }
                        } // if
                    }
                    //===============================================================================================

                    distance += GpsDatas[i].Dist;

                    if (j > 100)
                    {
                        ReportProgressChanged(i);
                        j = 0;
                    }
                    else
                    {
                        j++;
                    }
                } // for

                value = i;

                // ������������ ������� ����� �����
                if (isMoving)
                {
                    if (GpsDatas.Length > 0)
                    {
                        addMotionToReport(breakPoint, beginPoint, GpsDatas[GpsDatas.Length - 1], distance, ref MaxSpeed);
                    }
                }
                else
                {
                    if (endPoint != beginPoint)
                    {
                        addMotionToReport(breakPoint, beginPoint, endPoint, distance, ref MaxSpeed);
                    }

                    addBreakToReport(endPoint, GpsDatas[GpsDatas.Length - 1],
                        GpsDatas[GpsDatas.Length - 1].Time - endPoint.Time);
                }

                // ���������� ��������� ����������� ���������� ����
                if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                    !FormInterval.isActivateNewAlgorythm)
                {
                    FormatingIntervals();
                }
                else
                {
                    foreach (var rowed in tempReport)
                    {
                        atlantaDataSet.KilometrageReportRow mvRow =
                            AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
                        mvRow.State = rowed.State;
                        mvRow.MobitelId = rowed.MobitelId;

                        if (mvRow.State == Resources.Parking)
                        {
                            PointLatLng latlng = new PointLatLng(rowed.Latitude, rowed.Longitude);
                            mvRow.Location = FindAddressLocation(latlng);
                            mvRow.LatLng = latlng;
                        }

                        mvRow.DateTime = rowed.DateTime;
                        mvRow.InitialTime = rowed.InitialTime;
                        mvRow.FinalTime = rowed.FinalTime;
                        mvRow.Interval = rowed.Interval;
                        mvRow.Distance = rowed.Distance;
                        mvRow.GeneralDistance = rowed.GeneralDistance;
                        mvRow.MaxSpeed = rowed.MaxSpeed;
                        mvRow.AverageSpeed = rowed.AverageSpeed;
                        mvRow.InitialPointId = rowed.InitialPointId;
                        mvRow.FinalPointId = rowed.FinalPointId;
                        mvRow.Latitude = rowed.Latitude;
                        mvRow.Longitude = rowed.Longitude;
                        AtlantaDataSet.KilometrageReport.AddKilometrageReportRow(mvRow);
                    }
                }

                analizingFirstInterval();
                AfterReportFilling();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error kilometrage", MessageBoxButtons.OK);
            }
        } // Run

        public void FormatingIntervals()
        {
            int last;

            // �������� ������� ��������� ����������
            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
                return;

            var beging = new DateTime();
            DateTime end = new DateTime();

            // ���������� ���������� ���������
            if (FormInterval.isActiveTime())
            {
                beging = Convert.ToDateTime(FormInterval.getTimePeriodBegin());
                end = Convert.ToDateTime(FormInterval.getTimePeriodEnd());
            }

            if (FormInterval.isActiveWeek() || FormInterval.isActiveMonth() || FormInterval.isActivateNewAlgorythm)
            {
                beging = Convert.ToDateTime("00:00:00");
                end = Convert.ToDateTime("23:59:59");
            }

            //for (int iC = 0; iC < list_mobitels.Count; iC++)
            {
                _summary.ParkingTime = new TimeSpan(0, 0, 0, 0);
                _summary.TotalTravel = 0;
                _summary.TravelTime = new TimeSpan(0, 0, 0, 0);

                // ��������� ������� ����������� �� ���� �� ���������� ������
                list_beginer.Clear();
                list_ender.Clear();
                TimeSpan currentTime = new TimeSpan(GpsDatas[0].Time.Day, 0, 0, 0);
                TimeSpan temp = new TimeSpan(0, 0, 0);
                TimeSpan pointUp = new TimeSpan(23, 59, 59);
                Interval intervs;
                int kk = 0;

                //d_rows = DataSetManager.ConvertDataviewRowsToDataGpsArray(m_row.GetdataviewRows());

                if (GpsDatas.Length <= 0)
                    return;

                for (int i = 0; i < GpsDatas.Length; i++)
                {
                    if (currentTime.Days == GpsDatas[i].Time.Day)
                    {
                        temp = GpsDatas[i].Time.TimeOfDay - beging.TimeOfDay;

                        if (pointUp > temp)
                        {
                            pointUp = temp;
                            kk = i;
                        }
                    } // if
                    else
                    {
                        currentTime = new TimeSpan(GpsDatas[i].Time.Day, 0, 0, 0);
                        pointUp = new TimeSpan(23, 59, 59);
                        intervs = new Interval();
                        intervs.Time = GpsDatas[kk].Time;
                        list_beginer.Add(intervs);
                    } // else
                } // for

                intervs = new Interval();
                intervs.Time = GpsDatas[kk].Time;
                list_beginer.Add(intervs);

                currentTime = new TimeSpan(GpsDatas[0].Time.Day, 0, 0, 0);
                temp = new TimeSpan(0, 0, 0);
                pointUp = new TimeSpan(23, 59, 59);
                kk = 0;

                for (int i = 0; i < GpsDatas.Length; i++)
                {
                    if (currentTime.Days == GpsDatas[i].Time.Day)
                    {
                        temp = end.TimeOfDay - GpsDatas[i].Time.TimeOfDay;

                        if (pointUp > temp)
                        {
                            pointUp = temp;
                            kk = i;
                        }
                    }
                    else
                    {
                        currentTime = new TimeSpan(GpsDatas[i].Time.Day, 0, 0, 0);
                        pointUp = new TimeSpan(23, 59, 59);
                        intervs = new Interval();
                        intervs.Time = GpsDatas[kk].Time;
                        list_ender.Add(intervs);
                    }
                } // for

                intervs = new Interval {Time = GpsDatas[kk].Time};
                list_ender.Add(intervs);

                // ����� ������ ������������ ��������� ��������
                // �������� ������������� ���������� �������
                // ���������� �������� ������� � �������� ������ ��� ��������� ��������� ���������
                List<Interval> arrayIntervals = new List<Interval>();
                Interval eInterval = new Interval();

                atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])
                    tempReport.Select("MobitelId = " + m_row.Mobitel_ID);

                if (km_rows.Length <= 0)
                    return;

                eInterval.Time = km_rows[0].InitialTime;
                arrayIntervals.Add(eInterval);

                for (int k = 0; k < km_rows.Length; k++)
                {
                    eInterval = new Interval();
                    eInterval.Time = km_rows[k].FinalTime;
                    arrayIntervals.Add(eInterval);
                }

                // ���������� �������� ������ ��� � ������ � ������ ����������� ������� ������ ��� � ������ �����
                for (int m = 0; m < list_beginer.Count; m++)
                {
                    for (int k = 0; k < arrayIntervals.Count - 1; k++)
                    {
                        if (arrayIntervals[k].Time < list_beginer[m].Time &&
                            arrayIntervals[k + 1].Time > list_beginer[m].Time)
                        {
                            Interval interv = new Interval();
                            interv.Time = list_beginer[m].Time;
                            arrayIntervals.Insert(k + 1, interv);
                            break;
                        } // if
                    } // for
                } // for

                for (int m = 0; m < list_ender.Count; m++)
                {
                    for (int k = 0; k < arrayIntervals.Count - 1; k++)
                    {
                        if (arrayIntervals[k].Time < list_ender[m].Time &&
                            arrayIntervals[k + 1].Time > list_ender[m].Time)
                        {
                            Interval interv = new Interval();
                            interv.Time = list_ender[m].Time;
                            arrayIntervals.Insert(k + 1, interv);
                            break;
                        } // if
                    } // for
                } // for

                // ���������� �������� ������ ��� ����� � �������
                atlantaDataSet.KilometrageReportDataTable kiloReport = new atlantaDataSet.KilometrageReportDataTable();
                atlantaDataSet.KilometrageReportRow reportRow = kiloReport.NewKilometrageReportRow();
                reportRow.DateTime = arrayIntervals[0].Time;
                reportRow.InitialTime = arrayIntervals[0].Time;
                reportRow.FinalTime = arrayIntervals[1].Time;
                kiloReport.AddKilometrageReportRow(reportRow);

                for (int k = 2; k < arrayIntervals.Count; k++)
                {
                    reportRow = kiloReport.NewKilometrageReportRow();
                    reportRow.DateTime = arrayIntervals[k - 1].Time;
                    reportRow.InitialTime = arrayIntervals[k - 1].Time;
                    reportRow.FinalTime = arrayIntervals[k].Time;
                    kiloReport.AddKilometrageReportRow(reportRow);
                } // for

                // ������������� ������� ������� ��������� � �������
                // ������ �������� � ��������� ����� ���� 
                // ���������� ��� ���������� ���������
                last = -1;
                for (int k = 0; k < kiloReport.Rows.Count; k++)
                {
                    atlantaDataSet.KilometrageReportRow row = kiloReport[k];

                    // ����� ���������� ��������� � �������� ������� ������
                    int iLimit1 = 0;
                    int iLimit2 = 0;

                    for (int p = 0; p < GpsDatas.Length; p++)
                    {
                        if (row.InitialTime == GpsDatas[p].Time)
                        {
                            iLimit1 = p;
                            break;
                        }
                    }

                    for (int p = iLimit1; p < GpsDatas.Length; p++)
                    {
                        if (row.FinalTime == GpsDatas[p].Time)
                        {
                            iLimit2 = p;
                            break;
                        }
                    }

                    // ����������� ��������� �������� � ���� �� ��������� �� ���������
                    // ���������� �� ���������� ���
                    if (GpsDatas[iLimit1].Time.Day != GpsDatas[iLimit2].Time.Day)
                    {
                        continue;
                    }

                    atlantaDataSet.KilometrageReportRow repRow =
                        AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
                    repRow.DateTime = row.DateTime;
                    repRow.InitialTime = row.InitialTime;
                    repRow.FinalTime = row.FinalTime;
                    repRow.MobitelId = m_row.Mobitel_ID;

                    // ���� ��������� ���������� (������� ��� ��������)
                    bool Parking = true;
                    bool prevParking = true;
                    for (int iCount = iLimit1; iCount <= iLimit2; iCount++)
                    {
                        if (GpsDatas[iCount].Speed > 0)
                        {
                            prevParking = Parking;
                            Parking = false;
                        }
                    }

                    if (prevParking)
                        Parking = true;

                    if (Parking)
                    {
                        // ���� ������� (��������) - ������ ��� �������
                        repRow.State = Resources.Parking;
                        repRow.LatLng = GpsDatas[iLimit1].LatLng;
                        repRow.DateTime = GpsDatas[iLimit1].Time;
                        repRow.InitialTime = GpsDatas[iLimit1].Time;
                        repRow.FinalTime = GpsDatas[iLimit2].Time;
                        repRow.Interval = GpsDatas[iLimit2].Time - GpsDatas[iLimit1].Time;
                        repRow.Latitude = GpsDatas[iLimit1].LatLng.Lat;
                        repRow.Longitude = GpsDatas[iLimit1].LatLng.Lng;
                        repRow.InitialPointId = GpsDatas[iLimit1].Id;
                        repRow.FinalPointId = GpsDatas[iLimit2].Id;
                        repRow.Distance = 0;
                        repRow.GeneralDistance = 0;

                        _summary.ParkingTime += repRow.Interval;
                    }
                    else
                    {
                        // ���� �������� - ������ ��� ��������
                        repRow.State = Resources.Movement;
                        repRow.DateTime = GpsDatas[iLimit1].Time;
                        repRow.InitialTime = GpsDatas[iLimit1].Time;
                        repRow.FinalTime = GpsDatas[iLimit2].Time;
                        repRow.Interval = GpsDatas[iLimit2].Time - GpsDatas[iLimit1].Time;

                        // ���������� ���������� ���� � ������ ���������
                        double Distance = 0.0;
                        for (int iCount = iLimit1; iCount <= iLimit2; iCount++)
                        {
                            Distance += GpsDatas[iCount].Dist;
                        }

                        repRow.Distance = Distance;
                        repRow.GeneralDistance = _summary.TotalTravel + Distance;
                        repRow.AverageSpeed = Distance/repRow.Interval.TotalSeconds*3600;

                        // ������ ������������ �������� � ������ ���������
                        double Speed_Max = 0.0;
                        for (int iCount = iLimit1; iCount <= iLimit2; iCount++)
                        {
                            if (GpsDatas[iCount].Speed > Speed_Max)
                            {
                                Speed_Max = GpsDatas[iCount].Speed;
                            }
                        }

                        if (repRow.AverageSpeed > Speed_Max)
                            repRow.AverageSpeed = Speed_Max;

                        repRow.MaxSpeed = Speed_Max;
                        repRow.InitialPointId = GpsDatas[iLimit1].Id;
                        repRow.FinalPointId = GpsDatas[iLimit2].Id;
                        repRow.Latitude = GpsDatas[iLimit1].LatLng.Lat;
                        repRow.Longitude = GpsDatas[iLimit1].LatLng.Lng;

                        _summary.TravelTime += repRow.Interval;
                        _summary.TotalTravel += repRow.Distance;
                    } // else

                    if (last >= 0)
                    {
                        last = 1;

                        atlantaDataSet.KilometrageReportRow rowLast =
                            AtlantaDataSet.KilometrageReport[AtlantaDataSet.KilometrageReport.Rows.Count - 1];

                        if (rowLast.State == repRow.State)
                        {
                            if (rowLast.InitialTime.Day == repRow.InitialTime.Day)
                            {
                                if (rowLast.State == Resources.Parking)
                                {
                                    rowLast.FinalTime = repRow.FinalTime;
                                    rowLast.Interval = rowLast.Interval + repRow.Interval;
                                    rowLast.FinalPointId = repRow.FinalPointId;
                                } // if
                                else if (rowLast.State == Resources.Movement)
                                {

                                    rowLast.FinalTime = repRow.FinalTime;
                                    rowLast.Interval = rowLast.Interval + repRow.Interval;
                                    rowLast.Distance = rowLast.Distance + repRow.Distance;
                                    rowLast.GeneralDistance = repRow.GeneralDistance;
                                    rowLast.AverageSpeed = rowLast.Distance/rowLast.Interval.TotalSeconds*3600;

                                    if (repRow.MaxSpeed > rowLast.MaxSpeed)
                                        rowLast.MaxSpeed = repRow.MaxSpeed;

                                    if (rowLast.AverageSpeed > rowLast.MaxSpeed)
                                        rowLast.AverageSpeed = rowLast.MaxSpeed;

                                    rowLast.FinalPointId = repRow.FinalPointId;
                                } // else if

                                continue;
                            } // if
                        } // if
                    } // if
                    else
                    {
                        last = 0;
                    }

                    AtlantaDataSet.KilometrageReport.AddKilometrageReportRow(repRow);
                } // for

                int indx = 0;
                int j = 0;
                BeforeReportFilling("Getting adress locations", AtlantaDataSet.KilometrageReport.Count);
                PlacemarkStatus(); // ������ ������ ���������� ����� FindAddressLocation
                foreach (var row in AtlantaDataSet.KilometrageReport)
                {
                    if (row.LatLng != DBNull.Value)
                        row.Location = FindAddressLocation((PointLatLng) row.LatLng);

                    if (j > 100)
                    {
                        ReportProgressChanged(indx);
                        j = 0;
                    }

                    indx++;
                    j++;
                }

                // ������� ���� ��������� �������
                for (; kiloReport.Rows.Count > 0;)
                {
                    atlantaDataSet.KilometrageReportRow row = kiloReport[0];
                    kiloReport.RemoveKilometrageReportRow(row);
                }
            }

            // ������� ��������� �������
            for (; tempReport.Rows.Count > 0;)
            {
                atlantaDataSet.KilometrageReportRow rW = tempReport[0];
                tempReport.RemoveKilometrageReportRow(rW);
            } // for
        } // FormatingIntervals

        public atlantaDataSet.KilometrageReportRow[] GetKilometrageReportRows()
        {
            return m_row.GetKilometrageReportRows();
        }

        public void ClearKilometrageReportRows()
        {
            if (m_row != null)
            {
                foreach (var row in m_row.GetKilometrageReportRows())
                {
                    row.Delete();
                }
            }
        }

        private void analizingFirstInterval()
        {
            atlantaDataSet.KilometrageReportRow rowPoint, rowGlow, rowTemp;
            DateTime beginTime;
            int number = 0;

            for (int i = 0; i < AtlantaDataSet.KilometrageReport.Count; i++)
            {
                rowPoint = AtlantaDataSet.KilometrageReport[i];
                if (rowPoint.State == Resources.Movement)
                {
                    if (_minTimeMove > rowPoint.Interval.TotalSeconds)
                    {
                        for (int k = i; k < AtlantaDataSet.KilometrageReport.Count; k++)
                        {
                            rowGlow = AtlantaDataSet.KilometrageReport[k];
                            if (rowGlow.State == Resources.Parking)
                            {
                                int j = k - 1;
                                DateTime datePoint = AtlantaDataSet.KilometrageReport[k].InitialTime;

                                while (j >= 0)
                                {
                                    rowTemp = AtlantaDataSet.KilometrageReport[j];
                                    if (datePoint.Date.Day == rowTemp.InitialTime.Date.Day)
                                    {
                                        beginTime = rowTemp.InitialTime;
                                        AtlantaDataSet.KilometrageReport[k].Interval += rowTemp.Interval;
                                        AtlantaDataSet.KilometrageReport[k].DateTime = beginTime;
                                        AtlantaDataSet.KilometrageReport[k].InitialTime = beginTime;
                                        rowTemp.StateForDelete = "del";
                                    }
                                    else
                                    {
                                        k = j;
                                    }

                                    j--;
                                } // while

                                number = k;
                                break;
                            } // if
                        } // for

                        for (int p = 0; p < AtlantaDataSet.KilometrageReport.Count; p++)
                        {
                            rowTemp = AtlantaDataSet.KilometrageReport[0];
                            if (rowTemp.StateForDelete == "del")
                                AtlantaDataSet.KilometrageReport.RemoveKilometrageReportRow(rowTemp);
                        } // for

                        i = 0;
                    } // if
                    else
                    {
                        break;
                    }
                } // if
            } // for  

            TimeSpan ParkingInterval = new TimeSpan(0, 0, 0, 0);
            TimeSpan TravelInterval = new TimeSpan(0, 0, 0, 0);
            double Distance = 0;

            // ������������� ������
            for (int k = 0; k < AtlantaDataSet.KilometrageReport.Count; k++)
            {
                rowPoint = AtlantaDataSet.KilometrageReport[k];

                if (rowPoint.State == Resources.Movement)
                {
                    TravelInterval += rowPoint.Interval;
                    Distance += rowPoint.Distance;
                }
                else if (rowPoint.State == Resources.Parking)
                {
                    ParkingInterval += rowPoint.Interval;
                }
            }

            // _summary.ParkingTime = ParkingInterval;
            // _summary.TravelTime = TravelInterval;
            // _summary.TotalTravel = Distance;

        } // analizingFirstInterval

        #endregion

        #region Private Methods

        /// <summary>
        /// �������� ����������� ����� ��������� ������������ ������
        /// � ���������� ������� ��������� ������ ���������.
        /// </summary>
        protected override void AfterReportFilling()
        {
            base.AfterReportFilling();

            KilometrageEventArgs args = new KilometrageEventArgs(m_row.Mobitel_ID, _summary);
            OnAction(this, args);
            ReportCompleted(args);
            System.Windows.Forms.Application.DoEvents();
        }

        protected void ReportFilling(int mobitelos, Summary _summ)
        {
            base.AfterReportFilling();

            KilometrageEventArgs args = new KilometrageEventArgs(mobitelos, _summ);
            OnAction(this, args);
            ReportCompleted(args);
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// ������ ������ ������ � ������� � ����������
        /// ������� ������������� ��������
        /// </summary>
        /// <param name="initialPoint">������ ��������� � ��������� ����� �������</param>
        /// <param name="finalPoint">������ ��������� � �������� ����� �������</param>
        /// <param name="interval">������������ �������</param>
        private void addBreakToReport(GpsData initialPoint, GpsData finalPoint, TimeSpan interval)
        {
            string location = ""; //FindLocation(initialPoint.LatLng);
            GpsData lastPoint = null;

            atlantaDataSet.KilometrageReportRow parkingRow = null;
            atlantaDataSet.KilometrageReportRow tempRow = null;

            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
            {
                parkingRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            }
            else
            {
                tempRow = tempReport.NewKilometrageReportRow();
            }

            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
            {
                parkingRow.State = Resources.Parking;
                parkingRow.MobitelId = m_row.Mobitel_ID;
                //parkingRow.Location = location;
                parkingRow.DateTime = initialPoint.Time;
                parkingRow.InitialTime = initialPoint.Time;
                parkingRow.FinalTime = finalPoint.Time;
                parkingRow.Interval = interval;
                parkingRow.Latitude = initialPoint.LatLng.Lat;
                parkingRow.Longitude = initialPoint.LatLng.Lng;
                parkingRow.InitialPointId = initialPoint.Id;
                parkingRow.FinalPointId = finalPoint.Id;
                parkingRow.Distance = 0;
                parkingRow.MaxSpeed = 0;
                parkingRow.AverageSpeed = 0;
                parkingRow.GeneralDistance = 0;
            }
            else
            {
                tempRow.State = Resources.Parking;
                tempRow.MobitelId = m_row.Mobitel_ID;
                //tempRow.Location = location;
                tempRow.DateTime = initialPoint.Time;
                tempRow.InitialTime = initialPoint.Time;
                tempRow.FinalTime = finalPoint.Time;
                tempRow.Interval = interval;
                tempRow.Latitude = initialPoint.LatLng.Lat;
                tempRow.Longitude = initialPoint.LatLng.Lng;
                tempRow.InitialPointId = initialPoint.Id;
                tempRow.FinalPointId = finalPoint.Id;
                tempRow.Distance = 0;
                tempRow.MaxSpeed = 0;
                tempRow.AverageSpeed = 0;
                tempRow.GeneralDistance = 0;
            }

            // �������� ������� ��������� ����������
            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
                AtlantaDataSet.KilometrageReport.AddKilometrageReportRow(parkingRow);
            else
            {
                tempReport.AddKilometrageReportRow(tempRow);
            }

            _summary.ParkingTime += interval;
        }

        /// <summary>
        /// ������ ������ ������ � ������� � ����������
        /// �������� ������������� ��������
        /// </summary>
        /// <param name="initialPoint">������ ��������� � ��������� ����� �������</param>
        /// <param name="finalPoint">������ ��������� � �������� ����� �������</param>
        /// <param name="distance">����� �������</param>
        private void addMotionToReport(GpsData breakPoint, GpsData initialPoint, GpsData finalPoint, double distance,
            ref double speed_max)
        {
            atlantaDataSet.KilometrageReportRow movementRow = null;
            atlantaDataSet.KilometrageReportRow tempRow = null;

            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
                movementRow = AtlantaDataSet.KilometrageReport.NewKilometrageReportRow();
            else
                tempRow = tempReport.NewKilometrageReportRow();

            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
            {
                movementRow.State = Resources.Movement;
                movementRow.MobitelId = m_row.Mobitel_ID;
                movementRow.DateTime = initialPoint.Time;
                movementRow.InitialTime = initialPoint.Time;
                movementRow.FinalTime = finalPoint.Time;
                movementRow.Interval = finalPoint.Time - initialPoint.Time;
                movementRow.Distance = distance;
                movementRow.GeneralDistance = _summary.TotalTravel + distance;
                movementRow.MaxSpeed = speed_max;
                movementRow.AverageSpeed = 0;
                // aketner - 15.08.2012
                //��������, ��/�. ����� ���������� ���� �� ����������� �����.
                if (speed_max >= 0)
                {
                    if (movementRow.Interval.TotalHours > 0)
                    {
                        double averageSpd = distance/movementRow.Interval.TotalHours;

                        if (speed_max < averageSpd)
                        {
                            movementRow.AverageSpeed = speed_max;
                        }
                        else
                        {
                            movementRow.AverageSpeed = averageSpd;
                        }
                    } // if
                } // if

                //if (speed_max > 0 && movementRow.Interval.TotalHours > 0 && speed_max < distance / movementRow.Interval.TotalHours)
                //    movementRow.AverageSpeed = speed_max;
                //else
                //    movementRow.AverageSpeed = distance / movementRow.Interval.TotalHours;

                movementRow.InitialPointId = breakPoint.Id;
                movementRow.FinalPointId = finalPoint.Id;
                movementRow.Latitude = 0.0f;
                movementRow.Longitude = 0.0f;
                _summary.TravelTime += movementRow.Interval;
                _summary.TotalTravel += distance;
            }
            else
            {
                tempRow.State = Resources.Movement;
                tempRow.MobitelId = m_row.Mobitel_ID;
                //tempRow.Location = location;
                tempRow.DateTime = initialPoint.Time;
                tempRow.InitialTime = initialPoint.Time;
                tempRow.FinalTime = finalPoint.Time;
                tempRow.Interval = finalPoint.Time - initialPoint.Time;
                tempRow.Distance = distance;
                tempRow.GeneralDistance = _summary.TotalTravel + distance;
                tempRow.MaxSpeed = speed_max;
                tempRow.AverageSpeed = 0;
                // aketner - 15.08.2012
                //��������, ��/�. ����� ���������� ���� �� ����������� �����.
                if (speed_max >= 0)
                {
                    if (tempRow.Interval.TotalHours > 0)
                    {
                        double averageSpd = distance/tempRow.Interval.TotalHours;

                        if (speed_max < averageSpd)
                        {
                            tempRow.AverageSpeed = speed_max;
                        }
                        else
                        {
                            tempRow.AverageSpeed = averageSpd;
                        }
                    } // if
                } // if

                //if (speed_max > 0 && tempRow.Interval.TotalHours > 0 && speed_max < distance / tempRow.Interval.TotalHours)
                //    tempRow.AverageSpeed = speed_max;
                //else
                //    tempRow.AverageSpeed = distance / tempRow.Interval.TotalHours;

                tempRow.InitialPointId = breakPoint.Id;
                tempRow.FinalPointId = finalPoint.Id;
                tempRow.Latitude = 0.0f;
                tempRow.Longitude = 0.0f;
                _summary.TravelTime += tempRow.Interval;
                _summary.TotalTravel += distance;
            }

            // �������� ������� ��������� ����������
            if (!FormInterval.isActiveMonth() && !FormInterval.isActiveWeek() && !FormInterval.isActiveTime() &&
                !FormInterval.isActivateNewAlgorythm)
                AtlantaDataSet.KilometrageReport.AddKilometrageReportRow(movementRow);
            else
                tempReport.AddKilometrageReportRow(tempRow);

            _notBegin = true;
            speed_max = 0;
        } // addMotionReport

        #endregion

        public string MovementNameFromResource()
        {
            return Resources.Movement;
        }

        #region Nested Types

        /// <summary>
        /// ������������ �������� ������ ������ � ������� � ����������
        /// �� ����������� ��������� (������������� ��������)
        /// </summary>
        public struct Summary
        {
            /// <summary>
            /// ����� ���������� ����
            /// </summary>
            public double TotalTravel;

            /// <summary>
            /// ����� ����� � ����
            /// </summary>
            public TimeSpan TravelTime;

            /// <summary>
            /// ����� ����� �������
            /// </summary>
            public TimeSpan ParkingTime;
        }

        #endregion
    }

    /// <summary>
    /// �����, ���������� ���������, ������� ����� �������� � ����������
    /// ������� �� ��������� ������ ��������� ���������� ������ ��� ������
    /// � ������� � ���������� ����������� ������������� ��������
    /// </summary>
    public class KilometrageEventArgs : EventArgs
    {
        /// <summary>
        /// ������� � �������������� ����� ��������� ������ BaseReports.Procedure.KilometrageEventArgs
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public KilometrageEventArgs(int id, Kilometrage.Summary summary)
        {
            _id = id;
            _summary = summary;
        }

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        private int _id;

        /// <summary>
        /// �������� ������ ������ � ������� � ���������� �� �����������
        /// ��������� (������������� ��������)
        /// </summary>
        public Kilometrage.Summary Summary
        {
            get { return _summary; }
        }

        private Kilometrage.Summary _summary;
    }
}


