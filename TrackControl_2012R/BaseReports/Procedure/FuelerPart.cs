using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    public class FuelerPart : Algorithm
    {
        private atlantaDataSet fPartDataSet;

        public string NameSensorDrt
        {
            get { return NameDrtSensor; }
            set { NameDrtSensor = value; }
        }

        public FuelerPart(atlantaDataSet ds)
        {
            fPartDataSet = ds;
        }

        /// <summary>
        /// ���������� ����, �������� ��������, ������������� ��������� �� ������� �����.
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public static double GetAngle(double lat, double lon, int radius)
        {
            return ((radius/�GeoDistance.CalculateFromGrad(lat, lon, lat + 0.00100, lon + 0.00100)/1000.0)*0.00100);
                //(0.00100*1000) = 1 - ������ ��� ��������� � ���������� 
        }

        public KeyValuePair<System.Int64, double>[] getFromFuelerPart( FuelRateDictionarys fuelRateDict )
        {
            KeyValuePair<System.Int64, double>[] kvpFueler = new KeyValuePair<System.Int64, double>[fuelRateDict.ValSeries1.Count];
            int k = 0;
            foreach (var vl in fuelRateDict.ValSeries1)
            {
               kvpFueler[k++] = new KeyValuePair<long, double>(vl.Key, vl.Value.value);
            }

            return kvpFueler;
        }

        public atlantaDataSet getfPartDataSet
        {
            get
            {
                return fPartDataSet;
            }
        }

        /// <summary>
        /// ������� ��� ������� �������/�������� � ���� ��� ������������� ���������� �� ������� ����� ���������� � ���. ������� 
        /// </summary>
        /// <param name="fuelRateDict"></param>
        /// <param name="minFuelRate"></param>
        /// <param name="mobitelID"></param>
        /// <returns></returns>
        public List<atlantaDataSet.FuelerReportRow> GetFuelerReportRows(FuelRateDictionarys fuelRateDict,
            double minFuelRate, TimeSpan minDeltaTime, int mobitelID,int maxIntervalBetweenPointsInHour)
        {
            bool isStartFueler = false; //������� ��� ���� ������ ��������
            double startFuelrateValue = 0;
            KeyValuePair<System.Int64, double>[] kvpFueler = new KeyValuePair<System.Int64, double>[fuelRateDict.ValSeries1.Count];
            atlantaDataSet.FuelerReportRow fr_row = fPartDataSet.FuelerReport.NewFuelerReportRow();
            int m = 0;
            foreach (var vl in fuelRateDict.ValSeries1)
            {
                kvpFueler[m++] = new KeyValuePair<long, double>(vl.Key, vl.Value.value);
            }

            var fuelerReportRows = new List<atlantaDataSet.FuelerReportRow>();
            List<double> localValueBeg = null;

            for (int i = 0; i < fuelRateDict.ValSeries1.Count - 2; i++) //����� �������� �� �������� ������� � �������
            {
                if (Math.Round(Math.Abs(kvpFueler[i].Value - kvpFueler[i + 1].Value), 5) >=
                    fuelRateDict.fuelRateSensor1.K && !IsExceedMaxInterval(kvpFueler[i], kvpFueler[i + 1], maxIntervalBetweenPointsInHour)) //���� ������ �������
                {
                    if (!isStartFueler) //������ ��������
                    {
                        fr_row.DataGPS_ID = kvpFueler[i].Key;
                        fr_row.mobitel_id = mobitelID;
                        //fr_row.Location = FindLocation(new PointLatLng(
                        //fPartDataSet.dataview.FindByDataGps_ID(kvpFueler[i].Key).Lat,
                        //fPartDataSet.dataview.FindByDataGps_ID(kvpFueler[i].Key).Lon));
                        fr_row.Location =
                            FindLocation(Algorithm.GpsDatas.First(gps => gps.Id == kvpFueler[i].Key).LatLng);
                        fr_row.sensor_id = fuelRateDict.fuelRateSensor1.id;
                        //fr_row.timeStart = fPartDataSet.dataview.FindByDataGps_ID(kvpFueler[i].Key).time;
                        fr_row.timeStart = GpsDatas.First(gps => gps.Id == kvpFueler[i].Key).Time;
                        fr_row.timeEnd = GpsDatas.First(gps => gps.Id == kvpFueler[i + 1].Key).Time;
                        isStartFueler = true;
                        startFuelrateValue = kvpFueler[i].Value;
                    }
                }
                else
                {
                    if (isStartFueler) //����� ��������
                    {
                        //fr_row.timeEnd = fPartDataSet.dataview.FindByDataGps_ID(kvpFueler[i].Key).time;
                        fr_row.DataGPS_End = kvpFueler[i].Key;
                        fr_row.timeEnd = GpsDatas.First(gps => gps.Id == kvpFueler[i].Key).Time;
                        fr_row.pointValue = kvpFueler[i].Value;
                        fr_row.changeValue = kvpFueler[i].Value - startFuelrateValue;
                        isStartFueler = false;
                        fr_row.driverName = "-----";
                        fuelerReportRows.Add(fr_row);
                        fr_row = fPartDataSet.FuelerReport.NewFuelerReportRow();
                    }
                }

                // ������������ �����
                if(fuelerReportRows.Count == 0)
                    continue;

                List<double> lststart = new List<double>();
                List<double> lstend = new List<double>();

                int last = fuelerReportRows.Count - 1;
                long startid = fuelerReportRows[last].DataGPS_ID;
                long endid = fuelerReportRows[last].DataGPS_End;

                for (int k = 0; k < fuelRateDict.ValueDutSeries.Count; k++)
                {
                    IDictionary<long, double> series = fuelRateDict.ValueDutSeries[k];
                    lststart.Add(series[startid]);
                    lstend.Add(series[endid]);
                }

                double valuedut = 0.0;

                for (int j = 0; j < fuelRateDict.ValueDutSeries.Count; j++)
                {
                    valuedut += lststart[j] - lstend[j];
                }

                fuelerReportRows[last].valueDutos = valuedut;
                lststart.Clear();
                lstend.Clear();
            }

            // ������������ �����
            if (fuelRateDict.ValueDutSeries.Count > 0)
            {
                for (int g = 0; g < fuelerReportRows.Count; g++)
                {
                    if (fuelerReportRows[g].valueDutos > 0 && fuelerReportRows[g].valueDutos < 1)
                        fuelerReportRows[g].valueDutos = 1;
                }
            }

            //���������� �������� �� ������� ��������� ������������ ����� ���� � ��������, ������ ��������� //minFuelRate
            for (int i = fuelerReportRows.Count - 1; i > 0; i--)
            {
                if ((fuelerReportRows[i].timeStart - fuelerReportRows[i - 1].timeEnd) < minDeltaTime)
                {
                    fuelerReportRows[i - 1].timeEnd = fuelerReportRows[i].timeEnd;
                    fuelerReportRows[i - 1].changeValue += fuelerReportRows[i].changeValue;
                    fuelerReportRows[i - 1].valueDutos += fuelerReportRows[i].valueDutos;
                    fuelerReportRows[i - 1].pointValue = fuelerReportRows[i].pointValue;
                    fuelerReportRows.RemoveAt(i);
                }
            }

            for (int i = 0; i < fuelerReportRows.Count; i++)
            {
                fuelerReportRows[i].changeValueFact = (int)Math.Round(fuelerReportRows[i].changeValue);
                fuelerReportRows[i].NameSensor = NameDrtSensor;
            }

            return fuelerReportRows;
        }

        /// <summary>
        /// ����� ����� � ������� �� ������� � �����������
        /// </summary>
        /// <param name="fr_row"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public FuelingTC[] GetFuelingTC(atlantaDataSet.FuelerReportRow fr_row, int radius)
        {
            FuelingTC[] tmpFuelint = new FuelingTC[0];
            Dictionary<int, FuelingTC> dicFuelingTC = new Dictionary<int, FuelingTC>();

            try
            {
                //List<FuelingTC> fuelingTC = new List<FuelingTC>();
                //atlantaDataSet.dataviewRow dv_row = fr_row.dataviewRow;
                
                if (Algorithm.GpsDatas.Length <= 0)
                    return tmpFuelint;

                GpsData gpsData = Algorithm.GpsDatas.First(gps => gps.Id == fr_row.DataGPS_ID); // ????

                //double angle = FuelerPart.GetAngle(dv_row.Lat, dv_row.Lon, radius);
                if (radius > 0)
                {
                    //List<LocalCache.atlantaDataSet.dataviewRow> listDv_rows =
                    //    new List<LocalCache.atlantaDataSet.dataviewRow>();
                    List<GpsData> gpsDatas = new List<GpsData>();
                    string mobSql = "";
                    int mobCounter = 0;
                    const int countToRecord = 200;
                    foreach (atlantaDataSet.mobitelsRow mob_row in AtlantaDataSet.mobitels)
                    {
                        if (mob_row.Mobitel_ID == gpsData.Mobitel)
                            base.SelectItem(mob_row);
                        if (((mob_row.Mobitel_ID != gpsData.Mobitel) ||
                             //��� ���������� ����� �� ����, ���� � ���� ��� ������� ������???
                             (mob_row.Mobitel_ID == gpsData.Mobitel && FindSensor(AlgorithmType.FUEL1) != null)) &&
                            fr_row.timeEnd.Subtract(fr_row.timeStart).TotalSeconds > 0)
                        {
                            //gpsDatas.AddRange(DataSetManager.GetGpsDataFromDb(
                            //    AtlantaDataSet, mob_row.Mobitel_ID, fr_row.timeStart, fr_row.timeEnd));
                            var gpsDatasDb = DataSetManager.GetGpsDataFromDb(
                                AtlantaDataSet, mob_row.Mobitel_ID, fr_row.timeStart, fr_row.timeEnd);
                            if (gpsDatasDb != null) gpsDatas.AddRange(gpsDatasDb);
                            if (mobSql.Length == 0)
                            {
                                mobSql = mobSql + " AND ( Mobitel_ID = " + mob_row.Mobitel_ID;
                            }
                            else
                            {
                                mobSql = mobSql + " OR Mobitel_ID = " + mob_row.Mobitel_ID;
                            }
                            mobCounter++;
                        }
                        if (mobCounter >= countToRecord)
                        {
                            if (mobSql.Length > 0) mobSql = mobSql + ")";
                            AddMobitelsToDictionary(fr_row, radius, dicFuelingTC, gpsData, mobSql);
                            AddMobitelsToDictionary64(fr_row, radius, dicFuelingTC, gpsData, mobSql);
                            mobSql = "";
                            mobCounter = 0;
                        }
                    } // foreach
                    if (mobCounter > 0)
                    {
                        if (mobSql.Length > 0) mobSql = mobSql + ")";
                        AddMobitelsToDictionary(fr_row, radius, dicFuelingTC, gpsData, mobSql);
                        AddMobitelsToDictionary64(fr_row, radius, dicFuelingTC, gpsData, mobSql);
                    }

                    //foreach (LocalCache.atlantaDataSet.dataviewRow row in listDv_rows)
                    //{
                    //    //if ((row.Lat > (dv_row.Lat - angle)) && (row.Lat < (dv_row.Lat + angle)) &&//�������� �� �������� �� � ����������
                    //    //  (row.Lon > (dv_row.Lon - angle)) && (row.Lon < (dv_row.Lon + angle)))
                    //    //{
                    //    double tmpDist = �GeoDistance.CalculateFromGrad(row.Lat, row.Lon, dv_row.Lat, dv_row.Lon);
                    //    if (tmpDist <= radius / 1000.0)//���� ����� ���������� � ���������� �������� ������� ������ ����������.
                    //    {
                    //      if (dicFuelingTC.ContainsKey(row.Mobitel_ID))
                    //      {
                    //        if (dicFuelingTC[row.Mobitel_ID].beginFueling > row.time)
                    //          dicFuelingTC[row.Mobitel_ID].beginFueling = row.time;
                    //        if (dicFuelingTC[row.Mobitel_ID].endFueling < row.time)
                    //          dicFuelingTC[row.Mobitel_ID].endFueling = row.time;
                    //      }
                    //      else
                    //      {//new key
                    //        FuelingTC tmpFueling = new FuelingTC();
                    //        tmpFueling.mobitelID = row.Mobitel_ID;
                    //        tmpFueling.beginFueling = row.time;
                    //        tmpFueling.endFueling = row.time;
                    //        dicFuelingTC.Add(row.Mobitel_ID, tmpFueling);
                    //      }
                    //    }
                    //  //}
                    //}
                }

                tmpFuelint = new FuelingTC[dicFuelingTC.Count];
                if (dicFuelingTC.Count > 0)
                    dicFuelingTC.Values.CopyTo(tmpFuelint, 0);
                return tmpFuelint;
            }
            catch (Exception ex)
            {
                tmpFuelint = new FuelingTC[dicFuelingTC.Count];
                if (dicFuelingTC.Count > 0)
                    dicFuelingTC.Values.CopyTo(tmpFuelint, 0);
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error  GetFuelingTC", MessageBoxButtons.OK);
                return tmpFuelint;
            }
        }

        private static void AddMobitelsToDictionary(atlantaDataSet.FuelerReportRow fr_row, int radius,
            Dictionary<int, FuelingTC> dicFuelingTC, GpsData gpsData, string mobSql)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sSQL = string.Format(TrackControlQuery.DataviewTableAdapter.DatagpsSelectDinParameters,
                            mobSql, driverDb.ParamPrefics);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Begin", fr_row.timeStart);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "End", fr_row.timeEnd);

                driverDb.GetDataReader(sSQL, driverDb.GetSqlParameterArray);


                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while (driverDb.Read())
                    {
                        double lat = driverDb.GetDouble("Lat");
                        double lon = driverDb.GetDouble("Lon");
                        DateTime time = driverDb.GetDateTime("time");
                        int idMobitel = driverDb.GetInt32("Mobitel_ID");

                        double tmpDist = �GeoDistance.CalculateFromGrad(lat, lon, gpsData.LatLng.Lat, gpsData.LatLng.Lng);
                        if (tmpDist <= radius/1000.0)
                            //���� ����� ���������� � ���������� �������� ������� ������ ����������.
                        {
                            if (dicFuelingTC.ContainsKey(idMobitel))
                            {
                                if (dicFuelingTC[idMobitel].beginFueling > time)
                                    dicFuelingTC[idMobitel].beginFueling = time;
                                if (dicFuelingTC[idMobitel].endFueling < time)
                                    dicFuelingTC[idMobitel].endFueling = time;
                            }
                            else
                            {
                                //new key
                                FuelingTC tmpFueling = new FuelingTC();
                                tmpFueling.mobitelID = idMobitel;
                                tmpFueling.beginFueling = time;
                                tmpFueling.endFueling = time;
                                dicFuelingTC.Add(idMobitel, tmpFueling);
                            }
                        }
                    }
                }
                else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while (driverDb.Read())
                    {
                        int k = driverDb.GetOrdinal("Lat");
                        double lat = Convert.ToDouble(driverDb.GetValue(k).ToString());

                        k = driverDb.GetOrdinal("Lon");
                        double lon = Convert.ToDouble(driverDb.GetValue(k).ToString());

                        k = driverDb.GetOrdinal("time");
                        DateTime time = driverDb.GetDateTime(k);
                        k = driverDb.GetOrdinal("Mobitel_ID");
                        int idMobitel = driverDb.GetInt32(k);
                        double tmpDist = �GeoDistance.CalculateFromGrad(lat, lon, gpsData.LatLng.Lat, gpsData.LatLng.Lng);
                        if (tmpDist <= radius/1000.0)
                            //���� ����� ���������� � ���������� �������� ������� ������ ����������.
                        {
                            if (dicFuelingTC.ContainsKey(idMobitel))
                            {
                                if (dicFuelingTC[idMobitel].beginFueling > time)
                                    dicFuelingTC[idMobitel].beginFueling = time;
                                if (dicFuelingTC[idMobitel].endFueling < time)
                                    dicFuelingTC[idMobitel].endFueling = time;
                            }
                            else
                            {
                                //new key
                                FuelingTC tmpFueling = new FuelingTC();
                                tmpFueling.mobitelID = idMobitel;
                                tmpFueling.beginFueling = time;
                                tmpFueling.endFueling = time;
                                dicFuelingTC.Add(idMobitel, tmpFueling);
                            }
                        }
                    }
                }
                driverDb.CloseDataReader();
            }
        }

        private static void AddMobitelsToDictionary64(atlantaDataSet.FuelerReportRow fr_row, int radius,
            Dictionary<int, FuelingTC> dicFuelingTC, GpsData gpsData, string mobSql)
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();

                string sSQL64 = string.Format(TrackControlQuery.DataviewTableAdapter.DatagpsSelectDinParameters64,
                            mobSql, driverDb.ParamPrefics);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Begin", fr_row.timeStart);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "End", fr_row.timeEnd);

                driverDb.GetDataReader(sSQL64, driverDb.GetSqlParameterArray);


                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while (driverDb.Read())
                    {
                        double lat = driverDb.GetDouble("Lat");
                        double lon = driverDb.GetDouble("Lon");
                        DateTime time = driverDb.GetDateTime("time");
                        int idMobitel = driverDb.GetInt32("Mobitel_ID");

                        double tmpDist = �GeoDistance.CalculateFromGrad(lat, lon, gpsData.LatLng.Lat, gpsData.LatLng.Lng);
                        if (tmpDist <= radius / 1000.0)
                        //���� ����� ���������� � ���������� �������� ������� ������ ����������.
                        {
                            if (dicFuelingTC.ContainsKey(idMobitel))
                            {
                                if (dicFuelingTC[idMobitel].beginFueling > time)
                                    dicFuelingTC[idMobitel].beginFueling = time;
                                if (dicFuelingTC[idMobitel].endFueling < time)
                                    dicFuelingTC[idMobitel].endFueling = time;
                            }
                            else
                            {
                                //new key
                                FuelingTC tmpFueling = new FuelingTC();
                                tmpFueling.mobitelID = idMobitel;
                                tmpFueling.beginFueling = time;
                                tmpFueling.endFueling = time;
                                dicFuelingTC.Add(idMobitel, tmpFueling);
                            }
                        }
                    }
                }
                else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    while (driverDb.Read())
                    {
                        int k = driverDb.GetOrdinal("Lat");
                        double lat = Convert.ToDouble(driverDb.GetValue(k).ToString());

                        k = driverDb.GetOrdinal("Lon");
                        double lon = Convert.ToDouble(driverDb.GetValue(k).ToString());

                        k = driverDb.GetOrdinal("time");
                        DateTime time = driverDb.GetDateTime(k);
                        k = driverDb.GetOrdinal("Mobitel_ID");
                        int idMobitel = driverDb.GetInt32(k);
                        double tmpDist = �GeoDistance.CalculateFromGrad(lat, lon, gpsData.LatLng.Lat, gpsData.LatLng.Lng);
                        if (tmpDist <= radius / 1000.0)
                        //���� ����� ���������� � ���������� �������� ������� ������ ����������.
                        {
                            if (dicFuelingTC.ContainsKey(idMobitel))
                            {
                                if (dicFuelingTC[idMobitel].beginFueling > time)
                                    dicFuelingTC[idMobitel].beginFueling = time;
                                if (dicFuelingTC[idMobitel].endFueling < time)
                                    dicFuelingTC[idMobitel].endFueling = time;
                            }
                            else
                            {
                                //new key
                                FuelingTC tmpFueling = new FuelingTC();
                                tmpFueling.mobitelID = idMobitel;
                                tmpFueling.beginFueling = time;
                                tmpFueling.endFueling = time;
                                dicFuelingTC.Add(idMobitel, tmpFueling);
                            }
                        }
                    }
                }
                driverDb.CloseDataReader();
            }
        }

        /// <summary>
        /// find add_fuel
        /// </summary>
        /// <param name="fr_row"></param>
        /// <param name="idMobitel"></param>
        public void LoadDataToRepository(atlantaDataSet.FuelerReportRow fr_row, int idMobitel)
        {
            //int n = 0;
            //prevrow = (atlantaDataSet.dataviewRow)dataset.dataview.NewRow();
            //atlantaDataSet.mobitelsRow mRow = AtlantaDataSet.mobitels.FindByMobitel_ID(mob_id);
            var mobitel = VehicleProvider2.GetMobitel(idMobitel);
            if (mobitel.Is64BitPackets)
            {
                List<GpsData> gpsDatas = DataSetManager.GetGpsData64FromDbMobitelRow(mobitel, fr_row.timeStart, fr_row.timeEnd);
                if (DataSetManager.GpsDatas64.ContainsKey(idMobitel))
                {
                    gpsDatas.ForEach(gpsData =>
                    {
                        if (!DataSetManager.GpsDatas64[idMobitel].Contains(gpsData))
                            DataSetManager.GpsDatas64[idMobitel].Add(gpsData);
                    });
                }
                else
                {
                    DataSetManager.GpsDatas64.Add(idMobitel, gpsDatas);
                }
            }
            else
            {
                //����� ������ �� ���� � dataview
                foreach (atlantaDataSet.dataviewRow row in DataSetManager.GetDataViewRowsFromDb(
                    AtlantaDataSet, idMobitel, fr_row.timeStart, fr_row.timeEnd))
                {
                    if (AtlantaDataSet.dataview.FindByDataGps_ID(row.DataGps_ID) == null)
                    {
                        row.accel = 0;
                        row.dist = 0;
                        AtlantaDataSet.dataview.Rows.Add(row);
                    }
                }
            }
        }

        private bool IsExceedMaxInterval(KeyValuePair<System.Int64, double> kvStart, KeyValuePair<System.Int64, double> kvEnd,int maxIntervalBetweenPointsInHour)
        {
            DateTime timeStart = GpsDatas.First(gps => gps.Id == kvStart.Key).Time;
            DateTime timeEnd = GpsDatas.First(gps => gps.Id == kvEnd.Key).Time;
            if (timeEnd.Subtract(timeStart).TotalHours >= maxIntervalBetweenPointsInHour)
                return true;
            return false;
        }
    }
}