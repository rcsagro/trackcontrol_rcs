﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BaseReports.Properties;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    /// <summary>
    /// отчет по водителям согласно их RFID идентификаторам
    /// </summary>
    public class DriversRfid : Algorithm
    {

        public DriversRfid()
        {
            // to do
        }
        
        public override void Run()
        {
            if (GpsDatas==null || GpsDatas.Length == 0 || m_row ==null) 
                return;

            int idMobitel = m_row.Mobitel_ID;
            using (DriverRFIDRecordsCreator creator = new DriverRFIDRecordsCreator())
            {
                creator.ProgressChanged += ReportProgressChanged;
                creator.IdMobitel = idMobitel;
                creator.m_row = m_row;
                if (DicRfidRecords.ContainsKey(idMobitel))
                {
                    SelectItem(m_row);
                    Algorithm.OnAction(this, new EventArgs());
                    return;
                }
                else
                {
                    DicRfidRecords.Add(idMobitel, new List<DriverRFIDRecord>());
                }
                BeforeReportFilling(Resources.DriversRFID, GpsDatas.Length);
                creator.SetDefaultObservers();
                creator.DefineSensors();
                if (!creator.CuttingDataGpsByRfid(GpsDatas))
                {
                    AfterReportFilling();
                    return;
                }
                creator.WriteNoRecordedGps();
                creator.ProgressChanged -= ReportProgressChanged;
            }
            AfterReportFilling();
            Algorithm.OnAction(this, new EventArgs());
        }

    }
}
