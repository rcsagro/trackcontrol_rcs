using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace BaseReports.Procedure
{
    public class PartialAlgorithms : Algorithm
    {
        public PartialAlgorithms()
        {
        }

        public PartialAlgorithms(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
        }

        #region Metods

        /// <summary>
        /// �������� ����� ������ �� ��������� � ������, 
        /// ��������������� ��������� ������ d_rows
        /// </summary>
        /// <param name="d_rows">������ DataGps</param>
        /// <returns>������ FuelReportRow</returns>
        public atlantaDataSet.FuelReportRow[] GetFuelReportRow(GpsData[] d_rows)
        {
            List<atlantaDataSet.FuelReportRow> fuel_report_rows = new List<atlantaDataSet.FuelReportRow>();
            if (d_rows != null)
            {
                int i, k = 0;
                foreach (GpsData d in d_rows)
                {
                    //atlantaDataSet.FuelReportRow[] fuelRows = AtlantaDataSet.FuelReport.Where(fr => fr.DataGPS_ID == d.Id).ToArray();  
                    atlantaDataSet.FuelReportRow fuelRows = null; // = new List<atlantaDataSet.FuelReportRow>();
                    for (i = k; i < AtlantaDataSet.FuelReport.Count; i++)
                    {
                        if (AtlantaDataSet.FuelReport[i].DataGPS_ID == d.Id)
                        {
                            k = i;
                            break;
                        }
                    }

                    if (i < AtlantaDataSet.FuelReport.Count)
                    {
                        fuel_report_rows.Add(AtlantaDataSet.FuelReport[i]);
                    }
                }
            }
            return fuel_report_rows.ToArray();
        }

        /// <summary>
        /// �������� ����� ������ �� ��������� � ������, 
        /// ��������������� ��������� �������
        /// </summary>
        /// <param>�������� ������� � ������� ������ ��������</param>
        /// <returns>������ FuelReportRow</returns>
        public atlantaDataSet.FuelReportRow[] GetFuelReportRow(int mobitelId, DateTime beginTime, DateTime endTime)
        {
            List<atlantaDataSet.FuelReportRow> fuel_report_rows = new List<atlantaDataSet.FuelReportRow>();
            atlantaDataSet.FuelReportRow[] fuel_rows =
                AtlantaDataSet.mobitels.FindByMobitel_ID(mobitelId).GetFuelReportRows();

            foreach (atlantaDataSet.FuelReportRow fuel_row in fuel_rows)
            {
                if (fuel_row.time_ >= beginTime && fuel_row.time_ <= endTime)
                {
                    fuel_report_rows.Add(fuel_row);
                }
            }
            return fuel_report_rows.ToArray();
        }

        /// <summary>
        /// ����������� ������ ������� ������� � ����������� ����������.
        /// </summary>
        /// <param name="d_rows">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <param name="rot_min"></param>
        /// <returns>����� ����� ������� � ����������� ����������, TimeSpan</returns>
        public TimeSpan GetTimeStopEnginOff(GpsData[] d_rows, double vmin, double rot_min)
        {
            TimeSpan summ = new TimeSpan(0);
            if (d_rows.Length > 0)
            {
                //���� ���� ������ � ��������� ���������, �� ��� �������� ���������, 
                //���� ��� �� ������ �� ������ � ������� 
                List<atlantaDataSet.TachometerValueRow> rotval_rows = new List<atlantaDataSet.TachometerValueRow>();

                int i, k = 0;
                foreach (GpsData d in d_rows) //�������� ������ � ��������
                {
                    //atlantaDataSet.TachometerValueRow[] rotRows = AtlantaDataSet.TachometerValue.Where(tr => tr.Id == d.Id).ToArray();
                    //atlantaDataSet.TachometerValueRow rotRows = new List<atlantaDataSet.TachometerValueRow>();
                    for (i = k; i < AtlantaDataSet.TachometerValue.Count; i++)
                    {
                        if (AtlantaDataSet.TachometerValue[i].Id == d.Id)
                        {
                            k = i;
                            break;
                        }
                    }

                    if (i < AtlantaDataSet.TachometerValue.Count) //���� ���� ������ � ��������
                    {
                        rotval_rows.Add(AtlantaDataSet.TachometerValue[i]);
                    }
                }

                FuelRateDictionarys frDict = new FuelRateDictionarys();

                if (rotval_rows.Count == 0) //�������� � ��������, ��������� ��� ��������
                {
                    frDict.fuelRateSensor1 = FindSensor(AlgorithmType.FUEL2);
                    if (frDict.fuelRateSensor1 != null)
                    {
                        frDict.TypeAlgorithm1 = (int) AlgorithmType.FUEL2;
                        SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                        CorrectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                    }
                    else
                    {
                        frDict.fuelRateSensor1 = FindSensor(AlgorithmType.FUELDIFFDRT);
                        if (frDict.fuelRateSensor1 != null)
                        {
                            frDict.TypeAlgorithm1 = (int) AlgorithmType.FUELDIFFDRT;
                            SelectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                            CorrectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                        }
                    }
                }

                DateTime beginTime = d_rows[0].Time;
                bool bstart = false;
                int[] local_rotate_value = new int[0];

                if (rotval_rows.Count > 0) //������ �� ��������
                {
                    local_rotate_value = new int[rotval_rows.Count];
                    for (i = 0; i < local_rotate_value.Length; i++)
                    {
                        local_rotate_value[i] = (int) rotval_rows[i].Value;
                    }
                }
                else if (frDict.ValSeries1.Count > 0) //������ �� ������� �������
                {
                    local_rotate_value = new int[frDict.ValSeries1.Count];
                    rot_min = 0; //��������� ����� �� 0, ��� ��� � ��� ����� ������ ��� �������� "? 100 : 0";
                    for (i = 1; i < d_rows.Length && i < local_rotate_value.Length; i++)
                    {
                        local_rotate_value[i] =
                            frDict.ValSeries1[d_rows[i].Id].value != frDict.ValSeries1[d_rows[i - 1].Id].value ? 100 : 0;
                    }
                    local_rotate_value[0] = 0;
                }

                for (i = 0; i < local_rotate_value.Length; i++)
                {
                    if (!bstart && local_rotate_value[i] <= rot_min && d_rows[i].Speed <= vmin)
                    {
                        bstart = true;
                        beginTime = d_rows[i].Time;
                    }
                    else if (bstart && (local_rotate_value[i] > rot_min || d_rows[i].Speed > vmin))
                    {
                        summ += (d_rows[i].Time - beginTime);
                        bstart = false;
                    }
                }

                if (bstart)
                {
                    summ += (d_rows[d_rows.Length - 1].Time - beginTime);
                }
            }
            return summ;
        }

        /// <summary>
        /// ����������� ������ ������� ������� � ����������� ����������.
        /// </summary>
        /// <param name="d_rows">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <param name="rot_min"></param>
        /// <returns>����� ����� ������� � ����������� ����������, TimeSpan</returns>
        public TimeSpan GetTimeStopEnginOff(FuelRateDictionarys frDict, GpsData[] d_rows, double vmin, double rot_min)
        {
            TimeSpan summ = new TimeSpan(0);
            if (d_rows.Length > 0)
            {
                //���� ���� ������ � ��������� ���������, �� ��� �������� ���������, 
                //���� ��� �� ������ �� ������ � ������� 
                List<atlantaDataSet.TachometerValueRow> rotval_rows = new List<atlantaDataSet.TachometerValueRow>();

                int i, k = 0;
                foreach (GpsData d in d_rows) //�������� ������ � ��������
                {
                    //atlantaDataSet.TachometerValueRow[] rotRows = AtlantaDataSet.TachometerValue.Where(tr => tr.Id == d.Id).ToArray();
                    //List<atlantaDataSet.TachometerValueRow> rotRows = new List<atlantaDataSet.TachometerValueRow>();

                    for (i = k; i < AtlantaDataSet.TachometerValue.Count; i++)
                    {
                        if (d.Id == AtlantaDataSet.TachometerValue[i].Id)
                        {
                            k = i;
                            break;
                        }
                    }

                    if (AtlantaDataSet.TachometerValue.Count > i)
                    {
                        rotval_rows.Add(AtlantaDataSet.TachometerValue[i]);
                    }
                }

                DateTime beginTime = d_rows[0].Time;
                DateTime endTime = d_rows[d_rows.Length - 1].Time;
                IDictionary<System.Int64, valueFuelSeries> valSeries = new Dictionary<long, valueFuelSeries>();

                foreach (var vel in frDict.ValSeries1)
                {
                    if (beginTime <= vel.Value.Time)
                    {
                        if (endTime >= vel.Value.Time)
                        {
                            valSeries.Add(vel.Key, vel.Value);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                bool bstart = false;
                int[] local_rotate_value = new int[0];
                if (rotval_rows.Count > 0) //������ �� ��������
                {
                    local_rotate_value = new int[rotval_rows.Count];
                    for (i = 0; i < local_rotate_value.Length; i++)
                    {
                        local_rotate_value[i] = (int) rotval_rows[i].Value;
                    }
                }
                else if (valSeries.Count > 0) //������ �� ������� �������
                {
                    local_rotate_value = new int[valSeries.Count];
                    rot_min = 0; //��������� ����� �� 0, ��� ��� � ��� ����� ������ ��� �������� "? 100 : 0";
                    for (i = 1; i < d_rows.Length && i < local_rotate_value.Length; i++)
                    {
                        local_rotate_value[i] = valSeries[d_rows[i].Id].value != valSeries[d_rows[i - 1].Id].value
                            ? 100
                            : 0;
                    }

                    local_rotate_value[0] = 0;
                }

                for (i = 0; i < local_rotate_value.Length; i++)
                {
                    if (!bstart && local_rotate_value[i] <= rot_min && d_rows[i].Speed <= vmin)
                    {
                        bstart = true;
                        beginTime = d_rows[i].Time;
                    }
                    else if (bstart && (local_rotate_value[i] > rot_min || d_rows[i].Speed > vmin))
                    {
                        summ += (d_rows[i].Time - beginTime);
                        bstart = false;
                    }
                }

                if (bstart)
                {
                    summ += (d_rows[d_rows.Length - 1].Time - beginTime);
                }
            }
            return summ;
        }

        /// <summary>
        /// ������ ������� � �������� �� ���
        /// </summary>
        /// <param name="gpsData">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <returns>������ �������, � �.</returns>
        public double GetFuelUseInMotionDrt(FuelRateDictionarys frDict, GpsData[] gpsData, double vmin)
        {
            double flow_fuel = 0;
            if (gpsData != null && gpsData.Length > 0)
            {
                if (frDict.ValSeries1.Count > 0) //���� ���� ������ �� ��������
                {
                    float[] data;
                    IDictionary<System.Int64, double> pValueSeries = new Dictionary<long, double>();
                    if (frDict.ValueSeriesSum.Count > 0) //������ ������ ����� ���� ���� ��� ��� �������
                    {
                        pValueSeries = frDict.ValueSeriesSum;
                    }
                    else
                    {
                        foreach (var el in frDict.ValSeries1)
                        {
                            pValueSeries.Add(el.Key, el.Value.value);
                        }
                    }

                    data = new float[pValueSeries.Count];
                    //data - �������� ��� ���������� ������� �� ������. ������ �������� ������ ����� �������
                    for (int i = 1; i < data.Length && i < gpsData.Length; i++)
                    {
                        data[i - 1] = (float) (pValueSeries[gpsData[i].Id] - pValueSeries[gpsData[i - 1].Id]);
                        //data[i] - data[i - 1];
                    }
                    data[data.Length - 1] = 0;

                    for (int i = 0; i < data.Length; i++)
                    {
                        if (gpsData[i].Speed > vmin)
                        {
                            flow_fuel += data[i];
                        }
                    }
                }
            }
            return flow_fuel;
        }

        /// <summary>
        /// ������ ������� � �������� �� ���
        /// </summary>
        /// <param name="gpsData">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <returns>������ �������, � �.</returns>
        public double GetFuelUseInMotionDrt(GpsData[] gpsData, double vmin)
        {
            double flow_fuel = 0;
            FuelRateDictionarys frDict = new FuelRateDictionarys();
            if (gpsData != null && gpsData.Length > 0)
            {
                bool isNotFuelDrtAdd = true;
                frDict.FuelRateSensor1Exist = false;
                frDict.FuelRateSensor2Exist = false;
                frDict.Clear();
                FindSensorsDRT(frDict, AlgorithmType.FUEL2);

                if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                {
                    FindSensorsDRT(frDict, AlgorithmType.FUEL_DIRECT);
                    FindSensorsDRT(frDict, AlgorithmType.FUEL_INVERSE);

                    if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                    {
                        FindSensorsDRT(frDict, AlgorithmType.FUELDRTADD);
                        if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                        {
                            FindSensorsDRT(frDict, AlgorithmType.FUELDIFFDRT);
                            if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                            {
                                return 0;
                            }
                            else
                            {
                                isNotFuelDrtAdd = false;
                            }
                        }
                        else
                        {
                            SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, gpsData);
                            CorrectValuesDrtAdd(frDict.ValSeries1, frDict.ValSeries2, frDict.fuelRateSensor1,
                                frDict.fuelRateSensor2, gpsData);
                            CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
                            isNotFuelDrtAdd = false; 
                        }
                    } // if
                } // if

                if (isNotFuelDrtAdd)
                {
                    SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, gpsData);
                    CorrectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, gpsData);
                    CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
                }
                else
                {
                    if (frDict.TypeAlgorithm1 == (int) AlgorithmType.FUELDIFFDRT ||
                        frDict.TypeAlgorithm2 == (int) AlgorithmType.FUELDIFFDRT)
                    {
                        SelectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, gpsData);
                        CorrectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, gpsData);
                        CorrectForTwoSensorsDiff(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum, frDict);
                    }
                }

                if (frDict.ValSeries1.Count > 0) //���� ���� ������ �� ��������
                {
                    float[] data;
                    IDictionary<System.Int64, double> pValueSeries = new Dictionary<long, double>();

                    if (frDict.ValueSeriesSum.Count > 0) //������ ������ ����� ���� ���� ��� ��� �������
                    {
                        pValueSeries = frDict.ValueSeriesSum;
                    }
                    else
                    {
                        foreach (var el in frDict.ValSeries1)
                        {
                            pValueSeries.Add(el.Key, el.Value.value);
                        }
                    }

                    data = new float[pValueSeries.Count];
                    //data - �������� ��� ���������� ������� �� ������. ������ �������� ������ ����� �������
                    for (int i = 1; i < data.Length && i < gpsData.Length; i++)
                    {
                        data[i - 1] = (float) (pValueSeries[gpsData[i].Id] - pValueSeries[gpsData[i - 1].Id]);
                    }
                    data[data.Length - 1] = 0;

                    for (int i = 0; i < data.Length; i++)
                    {
                        if (gpsData[i].Speed > vmin)
                        {
                            flow_fuel += data[i];
                        }
                    }
                }
            }

            return flow_fuel;
        }

        /// <summary>
        /// ������ ������� �� �������� �� ���, ����� ����� �����������
        /// </summary>
        /// <param name="d_rows">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <param name="rot_min"></param>
        /// <returns>������ �������, � �.</returns>
        public double GetFuelUseInStopDrt(FuelRateDictionarys frDict, GpsData[] d_rows, double vmin, double rot_min)
        {
            double flow_fuel = 0;

            if (d_rows != null && d_rows.Length > 0)
            {
                List<atlantaDataSet.TachometerValueRow> rotate_rows = new List<atlantaDataSet.TachometerValueRow>();
                if (m_row.GetTachometerValueRows().Length > 0)
                {
                    int i, k = 0;
                    foreach (GpsData row in d_rows)
                    {
                        //atlantaDataSet.TachometerValueRow[] rotRows = AtlantaDataSet.TachometerValue.Where(tr => tr.Id == row.Id).ToArray(); 
                        //List<atlantaDataSet.TachometerValueRow> rotRows = new List<atlantaDataSet.TachometerValueRow>();
                        for (i = k; i < AtlantaDataSet.TachometerValue.Count; i++)
                        {
                            if (row.Id == AtlantaDataSet.TachometerValue[i].Id)
                            {
                                k = i;
                                break;
                            }
                        }

                        //if (rotRows.Count != 0)
                        {
                            rotate_rows.Add(AtlantaDataSet.TachometerValue[i]);
                        }
                    }
                }

                if (frDict.ValSeries1.Count > 0) //���� ���� ������ �� ��������
                {
                    float[] data;
                    IDictionary<System.Int64, double> pValueSeries = new Dictionary<long, double>();
                    if (frDict.ValueSeriesSum.Count > 0) //������ ������ ����� ���� ���� ��� ��� �������
                    {
                        pValueSeries = frDict.ValueSeriesSum;
                    }
                    else
                    {
                        foreach (var el in frDict.ValSeries1)
                        {
                            pValueSeries.Add(el.Key, el.Value.value);
                        }
                    }

                    data = new float[pValueSeries.Count];
                    //data - �������� ��� ���������� ������� �� ������. ������ �������� ������ ����� �������
                    for (int i = 1; i < data.Length && i < d_rows.Length; i++)
                    {
                        data[i - 1] = (float) (pValueSeries[d_rows[i].Id] - pValueSeries[d_rows[i - 1].Id]);
                    }
                    data[data.Length - 1] = 0;

                    if (rotate_rows.Count > 0)
                    {
                        for (int i = 0; i < rotate_rows.Count; i++)
                        {
                            if (d_rows[i].Speed <= vmin && rotate_rows[i].Value >= rot_min && i < data.Length)
                            {
                                flow_fuel += data[i];
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (d_rows[i].Speed <= vmin)
                            {
                                flow_fuel += data[i];
                            }
                        }
                    }
                }
            }
            return flow_fuel;
        }

        /// <summary>
        /// ������ ������� �� �������� �� ���
        /// </summary>
        /// <param name="d_rows">������ DataGps</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������</param>
        /// <param name="rot_min"></param>
        /// <returns>������ �������, � �.</returns>
        public double GetFuelUseInStopDrt(GpsData[] d_rows, double vmin, double rot_min)
        {
            double flow_fuel = 0;
            FuelRateDictionarys frDict = new FuelRateDictionarys();

            if (d_rows != null && d_rows.Length > 0)
            {
                List<atlantaDataSet.TachometerValueRow> rotate_rows = new List<atlantaDataSet.TachometerValueRow>();
                if (m_row.GetTachometerValueRows().Length > 0)
                {
                    int k = 0;
                    foreach (GpsData row in d_rows)
                    {
                        //atlantaDataSet.TachometerValueRow[] rotRows = AtlantaDataSet.TachometerValue.Where(tr => tr.Id == row.Id).ToArray();
                        for (int i = k; i < AtlantaDataSet.TachometerValue.Count; i++)
                        {
                            if (row.Id == AtlantaDataSet.TachometerValue[i].Id)
                            {
                                rotate_rows.Add(AtlantaDataSet.TachometerValue[i]);
                                k = i;
                                break;
                            }
                        }
                    }
                }

                bool isNotFuelDrtAdd = true;
                frDict.FuelRateSensor1Exist = false;
                frDict.FuelRateSensor2Exist = false;
                frDict.Clear();
                FindSensorsDRT(frDict, AlgorithmType.FUEL2);

                if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                {
                    FindSensorsDRT(frDict, AlgorithmType.FUEL_DIRECT);
                    FindSensorsDRT(frDict, AlgorithmType.FUEL_INVERSE);

                    if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                    {
                        FindSensorsDRT(frDict, AlgorithmType.FUELDRTADD);
                        if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                        {
                            FindSensorsDRT(frDict, AlgorithmType.FUELDIFFDRT);
                            if (!frDict.FuelRateSensor1Exist && !frDict.FuelRateSensor2Exist)
                            {
                                return 0;
                            }
                            else
                            {
                                isNotFuelDrtAdd = false;
                            }
                        }
                        else
                        {
                            SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                            CorrectValuesDrtAdd(frDict.ValSeries1, frDict.ValSeries2, frDict.fuelRateSensor1,
                                frDict.fuelRateSensor2, d_rows);
                            CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
                            isNotFuelDrtAdd = false;   
                        }
                    }
                }

                if (isNotFuelDrtAdd)
                {
                    SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                    CorrectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                    CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
                }
                else
                {
                    if (frDict.TypeAlgorithm1 == (int)AlgorithmType.FUELDIFFDRT || 
                        frDict.TypeAlgorithm2 == (int)AlgorithmType.FUELDIFFDRT)
                    SelectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                    CorrectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, d_rows);
                    CorrectForTwoSensorsDiff(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum, frDict);
                }

                if (frDict.ValSeries1.Count > 0) //���� ���� ������ �� ��������
                {
                    float[] data;
                    IDictionary<System.Int64, double> pValueSeries = new Dictionary<long, double>();

                    if (frDict.ValueSeriesSum.Count > 0) //������ ������ ����� ���� ���� ��� ��� �������
                    {
                        pValueSeries = frDict.ValueSeriesSum;
                    }
                    else
                    {
                        foreach (var el in frDict.ValSeries1)
                        {
                            pValueSeries.Add(el.Key, el.Value.value);
                        }
                    }
                    data = new float[pValueSeries.Count];
                    //data - �������� ��� ���������� ������� �� ������. ������ �������� ������ ����� �������
                    for (int i = 1; i < data.Length && i < d_rows.Length; i++)
                    {
                        data[i - 1] = (float) (pValueSeries[d_rows[i].Id] - pValueSeries[d_rows[i - 1].Id]);
                    }
                    data[data.Length - 1] = 0;

                    if (rotate_rows.Count > 0)
                    {
                        for (int i = 0; i < rotate_rows.Count; i++)
                        {
                            if (d_rows[i].Speed <= vmin && rotate_rows[i].Value >= rot_min && i < data.Length)
                            {
                                flow_fuel += data[i];
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (d_rows[i].Speed <= vmin)
                            {
                                flow_fuel += data[i];
                            }
                        }
                    }
                }
            }
            return flow_fuel;
        }

        #endregion

        public double TotalExpenseDUT(GpsData[] d_rows, out double FuelStart, out double FuelEnd, out double FuelAdd,
            out double FuelSub)
        {
            FuelStart = 0;
            FuelEnd = 0;
            FuelAdd = 0;
            FuelSub = 0;
            if (d_rows.Length == 0) return 0;
            FuelDictionarys fuelDict = TotalExpenseDUTPrepareData();
            if (fuelDict.ValueFuelSum.Count == 0) return 0;
            if (!fuelDict.ValueFuelSum.ContainsKey(d_rows[0].Id) ||
                !fuelDict.ValueFuelSum.ContainsKey(d_rows[d_rows.Length - 1].Id)) return 0;
            FuelStart = Math.Round(fuelDict.ValueFuelSum[d_rows[0].Id].value, 2);
            FuelEnd = Math.Round(fuelDict.ValueFuelSum[d_rows[d_rows.Length - 1].Id].value, 2);
            LocalCache.atlantaDataSet.FuelReportRow[] FRRows = GetFuelReportRow(d_rows);

            if (FRRows.Length > 0)
            {
                foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                {
                    if (tmp_FR_row.dValue > 0)
                        FuelAdd += tmp_FR_row.dValue;
                    else if (tmp_FR_row.dValue < 0)
                        FuelSub += tmp_FR_row.dValue;
                }
            }
            //����������, �
            FuelAdd = Math.Round(FuelAdd, 2);
            //�����, �
            FuelSub = Math.Round(-FuelSub, 2);
            //����� ������, �
            return Math.Round((FuelStart + FuelAdd - FuelEnd), 2);

        }

        private FuelDictionarys TotalExpenseDUTPrepareData()
        {
            //���������� ������ ��� ���������� ������� �������
            IAlgorithm brKlm = (IAlgorithm) new Kilometrage();
            brKlm.SelectItem(m_row);
            brKlm.Run();
            IAlgorithm brFuel1 = (IAlgorithm) new Fuel(AlgorithmType.FUEL1);
            brFuel1.SelectItem(m_row);
            brFuel1.Run();
            Fuel fuelAlg = new Fuel();
            fuelAlg.SelectItem(m_row);
            FuelDictionarys fuelDict = new FuelDictionarys();
            fuelAlg.GettingValuesDUT(fuelDict);
            return fuelDict;
        }

        #region DRT Value

        /// <summary>
        /// ���������� ������ ������ ������ � �� ��������� ����������: FUEL2, FUEL_DIRECT, FUEL_INVERSE
        /// </summary>
        public void GettingValuesDRT(FuelRateDictionarys frDict)
        {
            SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            CorrectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            if(frDict.TypeAlgorithm1 != (int)AlgorithmType.FUELDIFFDRT && frDict.TypeAlgorithm2 != (int)AlgorithmType.FUELDIFFDRT)
                CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
        }

        public void GettingValuesDRTDiff(FuelRateDictionarys frDict)
        {
            SelectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            CorrectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            if (frDict.TypeAlgorithm1 != (int)AlgorithmType.FUELDIFFDRT && frDict.TypeAlgorithm2 != (int)AlgorithmType.FUELDIFFDRT)
                CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
        }

        /// <summary>
        /// �������� �� ������ �������� (differential) ��� ������������ �����
        /// </summary>
        protected void GettingValuesDRTDiffer(FuelRateDictionarys frDict)
        {
            SelectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            CorrectValuesDiff(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
        }

        /// <summary>
        /// ���������� ������ ������ ������ � �� ��������� ��� ��������� FUELDRTADD
        /// </summary>
        public void GettingValuesDRTAdd(FuelRateDictionarys frDict)
        {
            SelectValues(frDict.ValSeries1, frDict.ValSeries2, frDict, GpsDatas);
            CorrectValuesDrtAdd(frDict.ValSeries1, frDict.ValSeries2, frDict.fuelRateSensor1, frDict.fuelRateSensor2, GpsDatas);
            if (frDict.TypeAlgorithm1 != (int)AlgorithmType.FUELDIFFDRT && frDict.TypeAlgorithm2 != (int)AlgorithmType.FUELDIFFDRT)
                CorrectForTwoSensors(frDict.ValSeries1, frDict.ValSeries2, frDict.ValueSeriesSum);
        }

        public void GettingValuesFuelerDRTDiff(FuelRateDictionarys frDict, int IsMovingUse)
        {
            frDict.Clear();

            frDict.ValSeries2 = null;
            frDict.fuelRateSensor2 = null;
            SelectValuesDiff(frDict.ValSeries1, null, frDict, GpsDatas);
            CorrectFuelerValueDiff(frDict.ValSeries1, frDict.fuelRateSensor1, IsMovingUse);
        }


        public void GettingValuesFuelerDRT(FuelRateDictionarys frDict, int IsMovingUse)
        {
            frDict.Clear();

            frDict.ValSeries2 = null;
            frDict.fuelRateSensor2 = null;
            SelectValues(frDict.ValSeries1, null, frDict, GpsDatas);
            CorrectFuelerValue(frDict.ValSeries1, frDict.fuelRateSensor1, IsMovingUse);
        }

        public void GettingValuesFuelerDUTs(FuelRateDictionarys frDict, int IsMovingUse)
        {
            frDict.ClearDut();

            SelectValuesDUT(frDict.ValueDutSeries, frDict.fuelDutSensors, GpsDatas);
            //CorrectFuelerValueDUT(frDict.ValueDutSeries, frDict.fuelDutSensors, IsMovingUse);
        }

        /// <summary>
        /// ������� ������. ���������� ����� CorrectValueSeries �������� ���������� ���,
        /// ���������� �������� ��� ��� ���� - � ���������� ��������� �������������
        /// ��������.
        /// </summary>
        protected void SelectValuesDUT(List<IDictionary<System.Int64, double>> valueSeriesDut,
            List<atlantaDataSet.sensorsRow> fuelDutSensor, GpsData[] d_rows_sv)
        {
            foreach (var dutFuel in fuelDutSensor)
            {
                Calibrate calibrSens = null;

                if (dutFuel != null)
                {
                    //��������, ��� ������� dutFuel, ������� ���������
                    atlantaDataSet.sensorsRow s_rowDut = dutFuel;
                    s_rowListDut.Add(s_rowDut);
                    calibrSens = Calibration();
                }

                if (calibrSens == null)
                    return;

                IDictionary<System.Int64, double> valueSeries = new Dictionary<long, double>();

                foreach (GpsData d in d_rows_sv)
                {
                    if (!valueSeries.ContainsKey(d.Id))
                    {
                        valueSeries.Add(d.Id, calibrSens.GetUserValue(d.Sensors, dutFuel.Length, 
                            dutFuel.StartBit, dutFuel.K, dutFuel.B, dutFuel.S));
                    }
                }

                valueSeriesDut.Add(valueSeries);
            }
        } // SelectValuesDUT

        /// <summary>
        /// ������� ������. ���������� ����� CorrectValueSeries1 � CorrectValueSeries2
        /// ���������� �������� ���� DIFF ������� ��� ���� - � ���������� ��������� �������������
        /// ��������.
        /// </summary>
        protected void SelectValuesDiff(IDictionary<System.Int64, valueFuelSeries> valueSeries1,
            IDictionary<System.Int64, valueFuelSeries> valueSeries2, FuelRateDictionarys frd, GpsData[] d_rows_sv)
        {
            Calibrate calibrateSensor1 = null;
            Calibrate calibrateSensor2 = null;

            if (frd.fuelRateSensor1 != null)
            {
                //��������, ��� ������� fuelRateSensor1, ������� ���������
                if (frd.TypeAlgorithm1 == (int) AlgorithmType.FUELDIFFDRT)
                {
                    s_row = frd.fuelRateSensor1;
                    calibrateSensor1 = Calibration();
                    if (calibrateSensor1 != null)
                    {
                        for (int i = 0; i < d_rows_sv.Length; i++)
                        {
                            valueFuelSeries vfs = new valueFuelSeries();
                            vfs.value = calibrateSensor1.GetUserValue(d_rows_sv[i].Sensors, frd.fuelRateSensor1.Length,
                                frd.fuelRateSensor1.StartBit, frd.fuelRateSensor1.K, frd.fuelRateSensor1.B, frd.fuelRateSensor1.S);

                            vfs.Time = d_rows_sv[i].Time;
                            valueSeries1.Add(d_rows_sv[i].Id, vfs);
                        } // for
                    } // if
                } // if
            } // if

            if (frd.fuelRateSensor2 != null)
            {
                if (frd.TypeAlgorithm2 == (int) AlgorithmType.FUELDIFFDRT)
                {
                    //��������, ��� ������� fuelRateSensor2, ������� ���������
                    s_row = frd.fuelRateSensor2;
                    calibrateSensor2 = Calibration();
                    if (calibrateSensor2 != null)
                    {
                        for (int i = 0; i < d_rows_sv.Length; i++)
                        {
                            valueFuelSeries vfs = new valueFuelSeries();
                            vfs.value = calibrateSensor2.GetUserValue(d_rows_sv[i].Sensors, frd.fuelRateSensor2.Length,
                                frd.fuelRateSensor2.StartBit, frd.fuelRateSensor2.K, frd.fuelRateSensor2.B, frd.fuelRateSensor2.S);
                            vfs.Time = d_rows_sv[i].Time;
                            valueSeries2.Add(d_rows_sv[i].Id, vfs);
                        } // for
                    } // if
                } // if
            } // if
        } // SelctValues

        /// <summary>
        /// ������� ������. ���������� ����� CorrectValueSeries1 � CorrectValueSeries2
        /// ���������� �������� ������� ��� ���� - � ���������� ��������� �������������
        /// ��������.
        /// </summary>
        protected void SelectValues(IDictionary<System.Int64, valueFuelSeries> valueSeries1,
            IDictionary<System.Int64, valueFuelSeries> valueSeries2, FuelRateDictionarys frd, GpsData[] d_rows_sv)
        {
            Calibrate calibrateSensor1 = null;
            Calibrate calibrateSensor2 = null;

            if (frd.fuelRateSensor1 != null)
            {
                if (frd.TypeAlgorithm1 != (int) AlgorithmType.FUELDIFFDRT)
                {
                    //��������, ��� ������� fuelRateSensor1, ������� ���������
                    s_row = frd.fuelRateSensor1;
                    calibrateSensor1 = Calibration();
                    if (calibrateSensor1 != null)
                    {
                        for (int i = 0; i < d_rows_sv.Length; i++)
                        {
                            valueFuelSeries vfs = new valueFuelSeries();
                            vfs.value = calibrateSensor1.GetUserValue(d_rows_sv[i].Sensors, frd.fuelRateSensor1.Length,
                                frd.fuelRateSensor1.StartBit, frd.fuelRateSensor1.K, frd.fuelRateSensor1.B, frd.fuelRateSensor1.S);

                            vfs.Time = d_rows_sv[i].Time;
                            try
                            {
                                if (!valueSeries1.ContainsKey(d_rows_sv[i].Id))
                                {
                                    valueSeries1.Add(d_rows_sv[i].Id, vfs);
                                }
                            }
                            catch(Exception e)
                            {
                                int k = 10;
                            }
                        } // for
                    } // if
                } // if
            } // if

            if (frd.fuelRateSensor2 != null)
            {
                if (frd.TypeAlgorithm2 != (int) AlgorithmType.FUELDIFFDRT)
                {
                    //��������, ��� ������� fuelRateSensor2, ������� ���������
                    s_row = frd.fuelRateSensor2;
                    calibrateSensor2 = Calibration();
                    if (calibrateSensor2 != null)
                    {
                        for (int i = 0; i < d_rows_sv.Length; i++)
                        {
                            valueFuelSeries vfs = new valueFuelSeries();
                            vfs.value = calibrateSensor2.GetUserValue(d_rows_sv[i].Sensors, frd.fuelRateSensor2.Length,
                                frd.fuelRateSensor2.StartBit, frd.fuelRateSensor2.K, frd.fuelRateSensor2.B, frd.fuelRateSensor2.S);
                            vfs.Time = d_rows_sv[i].Time;
                            valueSeries2.Add(d_rows_sv[i].Id, vfs);
                        }
                    }
                }
            }
        } // SelctValues

        /// <summary>
        /// ������������� �������� ��������� �������� ��� ��������� FUELDRTADD - ��������� ������ � ������������. 
        /// � ���������� �������� ���� � ������������ ����������.
        /// </summary>
        protected void CorrectValuesDrtAdd(IDictionary<System.Int64, valueFuelSeries> valueSeries1,
            IDictionary<System.Int64, valueFuelSeries> valueSeries2,
            atlantaDataSet.sensorsRow fuelRateSensor1, atlantaDataSet.sensorsRow fuelRateSensor2, GpsData[] gpsData)
        {
            if (fuelRateSensor1 != null)
            {
                CorrectSeriesValuesDrtAdd(valueSeries1, fuelRateSensor1, gpsData);
            }

            if (fuelRateSensor2 != null)
            {
                CorrectSeriesValuesDrtAdd(valueSeries2, fuelRateSensor2, gpsData);
            }
        }

        /// <summary>
        /// ������������� �������� ��������� �������� (��� differential) - ��������� ������ � ������������. 
        /// � ���������� �������� ���� � ������������ ����������.
        /// </summary>
        protected void CorrectValuesDiff(IDictionary<System.Int64, valueFuelSeries> valueSeries1,
            IDictionary<System.Int64, valueFuelSeries> valueSeries2, FuelRateDictionarys frd, GpsData[] gpsData)
        {
            if (frd.fuelRateSensor1 != null)
            {
                if (frd.TypeAlgorithm1 == (int)AlgorithmType.FUELDIFFDRT)
                {
                    CorrectSeriesValuesDiff(valueSeries1, frd.fuelRateSensor1, gpsData);
                }
            }

            if (frd.fuelRateSensor2 != null)
            {
                if (frd.TypeAlgorithm2 == (int)AlgorithmType.FUELDIFFDRT)
                {
                    CorrectSeriesValuesDiff(valueSeries2, frd.fuelRateSensor2, gpsData);
                }
            }
        }

        /// <summary>
        /// ������������� �������� ��������� �������� - ��������� ������ � ������������. 
        /// � ���������� �������� ���� � ������������ ����������.
        /// </summary>
        protected void CorrectValues(IDictionary<System.Int64, valueFuelSeries> valueSeries1,
            IDictionary<System.Int64, valueFuelSeries> valueSeries2, FuelRateDictionarys frd, GpsData[] gpsData)
        {
            if (frd.fuelRateSensor1 != null)
            {
                if (frd.TypeAlgorithm1 != (int) AlgorithmType.FUELDIFFDRT)
                {
                    CorrectSeriesValues(valueSeries1, frd.fuelRateSensor1, gpsData);
                }
            }

            if (frd.fuelRateSensor2 != null)
            {
                if (frd.TypeAlgorithm2 != (int) AlgorithmType.FUELDIFFDRT)
                {
                    CorrectSeriesValues(valueSeries2, frd.fuelRateSensor2, gpsData);
                }
            }
        }

        /// <summary>
        /// ����������� ��������� �������� �� �������
        /// </summary>
        private int MAX_COUNT_PER_SEC = 8;

        /// <summary>
        /// ������������ �������� ����� �������� = 1016 
        /// </summary>
        private const ushort MAX_SENSOR_BOARD_VALUE = 1016;

        /// <summary>
        /// ������������� �������� ��������� ������� FUELDRTADD
        /// � ���������� �������� ��� � ������������ ����������.
        /// </summary>
        /// <param name="series">��� �������� � �����������.</param>
        /// <param name="sensor">������.</param>
        private void CorrectSeriesValuesDrtAdd(IDictionary<System.Int64, valueFuelSeries> series,
            atlantaDataSet.sensorsRow sensor, GpsData[] gpsData)
        {
            try
            {
                if (series.Count == 0)
                    return; // ���� ������ ������

                // ������ ������ �����������
                double[] buffer = new double[series.Count];
                buffer[0] = 0;

                // �������� ������� � ������ ��� �������� �������(���������� ����� �������)
                KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                    new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
                series.CopyTo(kvpArray, 0);

                // ��������� ������ ����������� ��� �������� �������� ����� "����" ������������
                // 1. ������ ������� �����������
                for (int i = 1; i < series.Count; i++)
                {
                    buffer[i - 1] = kvpArray[i - 1].Value.value - kvpArray[i].Value.value;
                } // for

                buffer[series.Count - 1] =
                    Math.Abs(kvpArray[series.Count - 2].Value.value - kvpArray[series.Count - 1].Value.value);

                // 2. ��������� ������� ������� ��������
                for (int i = 0; i < series.Count; i++)
                {
                    if (Math.Abs(buffer[i]) >= 2)
                    {
                        buffer[i] = 0;
                    }
                } // for

                if (Math.Abs(buffer[0]) >= 2)
                {
                    buffer[0] = 0;
                }

                // 3. ����������� ������, �������� �� ���������� �������� � ������� �������
                for (int i = 0; i < series.Count; i++)
                {
                    if (buffer[i] == 0)
                        kvpArray[i].Value.value = 0;
                }

                // 4. ���� ������ ��������
                double firstValue = 0;
                int k = 0;
                for (int i = 0; i < series.Count; i++)
                {
                    if (kvpArray[i].Value.value != 0)
                    {
                        firstValue = kvpArray[i].Value.value;
                        k = i;
                        break;
                    }
                } // for

                // 5. ��������� ������ ��������, ������ �����
                for (int i = 0; i < k; i++)
                {
                    kvpArray[i].Value.value = firstValue;
                }

                // 6. ��������� ������ ��������, ������ �����
                firstValue = kvpArray[0].Value.value;
                for (int i = 0; i < series.Count; i++)
                {
                    if (kvpArray[i].Value.value != 0)
                    {
                        kvpArray[i].Value.value = kvpArray[i].Value.value - firstValue;
                    }
                    else
                    {
                        if (i - 1 >= 0)
                            kvpArray[i].Value.value = kvpArray[i - 1].Value.value;
                        else
                            kvpArray[i].Value.value = 0;
                    }
                } // for

                // 7. �������� ������ ��� ���������� ������
                for (int i = 0; i < series.Count; i++)
                {
                    buffer[i] = kvpArray[i].Value.value;
                }

                // 8. ��������� �� ������������ ������ � ������������ ��, ���� ������������ ����������
                double bigNum = 0;
                double minNum = 0;
                bool isFlag = false;
                for (int i = 0; i < series.Count - 1; i++)
                {
                    if (Math.Abs(buffer[i] - buffer[i + 1]) > 10 && !isFlag)
                    {
                        bigNum = buffer[i + 1];
                        minNum = buffer[i];
                        buffer[i + 1] = buffer[i];
                        isFlag = true;
                    }
                    else if (Math.Abs(buffer[i] - buffer[i + 1]) > 10 && isFlag)
                    {
                        buffer[i + 1] = minNum + (buffer[i + 1] - bigNum);
                    }
                    else
                    {
                        isFlag = false;
                    }
                }

                if (Math.Abs(buffer[series.Count - 1] - buffer[series.Count - 2]) > 10)
                {
                    buffer[series.Count - 1] = buffer[series.Count - 2];
                }

                // 9. ��������� ������� ��� ������ ������
                for (int i = 0; i < series.Count; i++)
                {
                    series[kvpArray[i].Key].value = buffer[i];
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error CorrectSeriesValuesDrtAdd",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } // CorrectSeriesValuesDrtAdd

        /// <summary>
        /// �������� ����������� ������ � ��������� ��������� �������� LogID 
        /// </summary>
        /// <param name="prevLogId">��������� �������� LogId</param>
        /// <param name="curLogId">�������� �������� LogId</param>
        List<GpsData> getAdditionsData(atlantaDataSet.sensorsRow sensor, int prevLogId, int curLogId, int mobitelId,
            bool Is64Packet)
        {
            DataTable gpsDoData = null;

            using (var db = new DriverDb())
            {
                db.NewSqlConnection(db.CS);
                db.NewSqlCommand();
                db.CommandSqlConnection();
                db.CommandTimeout(800);
                db.CommandType(CommandType.StoredProcedure);

                if (Is64Packet)
                    db.CommandText("getDiapasone64");
                else
                    db.CommandText("getDiapasone");

                db.CommandParametersAdd(db.ParamPrefics + "mobile", db.GettingInt32(), mobitelId);
                db.CommandParametersAdd(db.ParamPrefics + "prevLogId", db.GettingInt32(), prevLogId);
                db.CommandParametersAdd(db.ParamPrefics + "currLogId", db.GettingInt32(), curLogId);
                db.SqlConnectionOpen();
                db.SqlDataReader = db.CommandExecuteReader();
                gpsDoData = db.GetDataTable(db.GetCommand);
                db.SqlConnectionClose();
            }

            List<GpsData> gpsDiaData = new List<GpsData>();

            if (gpsDoData != null)
            {
                Calibrate calibrateSensor = Calibration();

                for (int k = 0; k < gpsDoData.Rows.Count; k++)
                {
                    DateTime tmt = new DateTime();

                    try
                    {
                        string date_str = gpsDoData.Rows[k]["time"].ToString();
                        if (date_str == "")
                            date_str = "01.01.2001 00:00:00";
                        tmt = Convert.ToDateTime(date_str);
                    }
                    catch(Exception e)
                    {
                        continue;
                    }

                    GpsData gps = new GpsData();

                    gps.Time = tmt;
                    ulong gpsDataId = (ulong) Convert.ToUInt32(gpsDoData.Rows[k]["DataGps_ID"].ToString());
                    gps.SrvPacketID = Convert.ToInt64(gpsDoData.Rows[k]["srvPack"].ToString());
                    gps.Events = (uint) Convert.ToUInt32(gpsDoData.Rows[k]["Events"].ToString());

                    if (Is64Packet)
                    {
                        byte[] sensors = null;

                        byte[] initSensors = new byte[25];
                        try
                        {
                            sensors = Convert.FromBase64String(gpsDoData.Rows[k]["sensor"].ToString());
                        }
                        catch(Exception ex)
                        {
                            try
                            {
                                sensors = System.Text.Encoding.ASCII.GetBytes(gpsDoData.Rows[k]["sensor"].ToString());
                            }
                            catch(Exception e)
                            {
                                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Exception of convert string to byte");
                                return gpsDiaData;
                            }
                        }
                        
                        sensors.CopyTo(initSensors, 25 - sensors.Length);
                        Array.Reverse(initSensors);
                        gps.Sensors = initSensors;
                    }
                    else
                    {
                        ulong snsrs = Convert.ToUInt64(gpsDoData.Rows[k]["sensor"].ToString());
                        gps.Sensors = BitConverter.GetBytes(snsrs);
                    }

                    gps.SensorValue = calibrateSensor.GetUserValue(gps.Sensors, sensor.Length, sensor.StartBit, sensor.K, sensor.B, sensor.S);
                    gpsDiaData.Add(gps);
                } // for
            } // if

            return gpsDiaData;
        }

        /// <summary>
        /// ������������� �������� ��������� ������� differential ��������� ���� - 
        /// ��������� ������ � ������������. 
        /// � ���������� �������� ��� � ������������ ����������.
        /// </summary>
        /// <param name="series">��� �������� � ����������� ������� differential.</param>
        /// <param name="sensor">������ DIFF.</param>
        private void CorrectSeriesValuesDiff(IDictionary<System.Int64, valueFuelSeries> series,
            atlantaDataSet.sensorsRow sensor, GpsData[] gpsData)
        {
            if (series.Count == 0)
                return;

            if (sensor == null)
                return;

            // �������� ������� � ������ ��� �������� �������(���������� ����� �������)
            KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
            series.CopyTo(kvpArray, 0);

            double countResult = 0.0;
            for (int i = 0; i < kvpArray.Length; i++)
            {
                if ((TrackControl.TtEvents.TtEventsDescr.SENSOR_BOARD & gpsData[i].Events) ==
                    TrackControl.TtEvents.TtEventsDescr.SENSOR_BOARD)
                {
                    countResult = countResult + series[kvpArray[i].Key].value;
                }

                series[kvpArray[i].Key].value = countResult;
            }
        }

        /// <summary>
        /// ������������� �������� ��������� ������� ��������������� ��������� ���� - 
        /// ��������� ������ � ������������. 
        /// � ���������� �������� ��� � ������������ ����������.
        /// </summary>
        /// <param name="series">��� �������� � ����������� ������� DRT.</param>
        /// <param name="sensor">������.</param>
        private void CorrectSeriesValues(IDictionary<System.Int64, valueFuelSeries> series,
            atlantaDataSet.sensorsRow sensor, GpsData[] gpsData)
        {if (series.Count == 0)
                return;

            // ������ ������������ ��������� ����� ��������� ����������
            double[] buffer = new double[series.Count];
            buffer[0] = 0;

            // �������� ������� � ������ ��� �������� �������(���������� ����� �������)
            KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
            series.CopyTo(kvpArray, 0);

            GpsData[] gpsDiapasonData = new GpsData[0]; // ������ ��� ����������

            for (int i = 1; i < series.Count; i++)
            {
                if (kvpArray[i].Value.value >= kvpArray[i - 1].Value.value)
                {
                    buffer[i] = kvpArray[i].Value.value - kvpArray[i - 1].Value.value;
                }
                else
                {
                    // ��������� ������� ����� ��� � ����������������
                    buffer[i] = kvpArray[i].Value.value;
                    // ������� ��������� ������ ������� ���������� ��� ������������ ����� �������� 
                    // (���������� ����� �������� ��������� �����)
                    if ((TrackControl.TtEvents.TtEventsDescr.MAIN_POWER_ON & gpsData[i - 1].Events) == 0 &&
                        (TrackControl.TtEvents.TtEventsDescr.PROGRAM_RESTART & gpsData[i].Events) == 0 &&
                        (TrackControl.TtEvents.TtEventsDescr.PROGRAM_START & gpsData[i].Events) == 0)
                    {
                        DateTime prevTime = kvpArray[i - 1].Value.Time;
                        int prevLogId = gpsData[i - 1].LogId;
                        DateTime curTime = kvpArray[i].Value.Time;
                        int curLogId = gpsData[i].LogId;
                        if (curLogId - prevLogId > 1) // ����� ������ �������� ���������������� ������������
                        {
                            gpsDiapasonData =
                                getAdditionsData(sensor, prevLogId, curLogId, m_row.Mobitel_ID, m_row.Is64bitPackets)
                                    .ToArray();
                        }

                        // ��������������� ����� � ���������� ��������, ��� � ��������� ������ ��� ����� ������� ��� � �����������
                        List<GpsData> tempData = new List<GpsData>();
                        if (gpsDiapasonData != null)
                        {
                            for (int h = 0; h < gpsDiapasonData.Length; h++)
                            {
                                if (gpsDiapasonData[h].Valid && gpsDiapasonData[h].Time > prevTime && gpsDiapasonData[h].Time < curTime)
                                {
                                    tempData.Add(gpsDiapasonData[h]);
                                } // if
                            } // for
                        } // if

                        GpsData[] gpsDiapason = tempData.ToArray();
                        double setAvgFuelRatePerHour = -1;
                        double prevFuelValue = kvpArray[i - 1].Value.value;
                        double currFuelValue = kvpArray[i].Value.value;

                        if (set_row != null)
                        {
                            setAvgFuelRatePerHour = set_row.AvgFuelRatePerHour;
                        }

                        double prevValue = 0.0;
                        double dtime = 0.0;
                        double avarageDiapasoneFuel = 0.0;
                        bool isChange = false;

                        if (gpsDiapason != null)
                        {
                            for (int p = gpsDiapason.Length - 1; p >= 0; p--)
                            {
                                if (gpsDiapason[p].SensorValue == 0.0)
                                {
                                    prevTime = gpsDiapason[p].Time;
                                    isChange = true;
                                    break;
                                }
                            } // for

                            TimeSpan deltatime = curTime - prevTime;
                            dtime = deltatime.TotalMilliseconds / 1000 / 3600;

                            if (dtime > 0 && isChange)
                            {
                                avarageDiapasoneFuel = (prevFuelValue - currFuelValue) / dtime;

                                if (avarageDiapasoneFuel > 0)
                                {
                                    if (avarageDiapasoneFuel < setAvgFuelRatePerHour)
                                        setAvgFuelRatePerHour = avarageDiapasoneFuel;
                                }
                            }   
                        } // if

                        double avarage = GetCalculatedValue(prevFuelValue, prevTime, currFuelValue, curTime, sensor.K, setAvgFuelRatePerHour);
                        buffer[i] += avarage;
                    } // if
                } // if
            } // for

            // ���������� �������������� ����
            series[kvpArray[0].Key].value = 0;

            for (int i = 1; i < kvpArray.Length; i++)
            {
                series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value + buffer[i];
            }

            List<int> arrayIndex = new List<int>();
            bool flagUse = true;
            double tmp;

            // ����� ���� � ���������� ������� ��������� ���������� ���������������� ���������� ���������
            for (int j = 0; j < series.Count; j++)
            {
                tmp = series[kvpArray[j].Key].value;

                if (tmp == 0 && flagUse)
                {
                    arrayIndex.Add(j);
                    flagUse = false;
                }
                else if (tmp != 0 && !flagUse)
                {
                    flagUse = true;
                    arrayIndex.Add(j);
                }
            }

            if (!flagUse)
                arrayIndex.Add(series.Count - 1);

            double tmp1;
            double tmp2;

            // ����� ������� ������� ��������� ���������� ���������������� ���������� ���������
            for (int k = 0; k < arrayIndex.Count; k += 2)
            {
                if (arrayIndex[k] == 0)
                    continue;

                tmp1 = series[kvpArray[arrayIndex[k] - 1].Key].value;

                if (arrayIndex[k + 1] == (series.Count - 1))
                    continue;

                tmp2 = series[kvpArray[arrayIndex[k + 1]].Key].value;

                tmp = (tmp1 + tmp2) / 2;

                for (int i = arrayIndex[k]; i < arrayIndex[k + 1]; i++)
                {
                    series[kvpArray[i].Key].value = tmp;
                }
            } // for
        } // CorrectSeriesValues

        public double GetCalculatedValue(double prevValue, DateTime prevTime, double curValue, DateTime curTime,
            double sensorK, double setAvgFuelRatePerHour)
        {
            if (OverflowOccurred(prevValue, prevTime, curValue, curTime, sensorK, setAvgFuelRatePerHour))
            {
                double tmp = (MAX_SENSOR_BOARD_VALUE*sensorK - prevValue);
                if (tmp >= 0)
                    return tmp;
            }

            return 0;
        }

        private void CorrectFuelerValueDUT(List<IDictionary<System.Int64, valueFuelSeries>> seriesList,
            List<atlantaDataSet.sensorsRow> sensorList, int IsMovingUse)
        {
            if (seriesList.Count == 0)
                return;

            // ������������� �������� ��������� �������� - ��������� ������ � ������������. 
            // � ���������� �������� ���� � ������������ ����������.
            int indx = 0;
            foreach (var series in seriesList)
            {
                atlantaDataSet.sensorsRow sensor = sensorList[indx];
                CorrectSeriesValues(series, sensor, GpsDatas);

                if (IsMovingUse == 0)
                {
                    // �� ������� - ������������ ��������� �������� �� ����� ��������
                    KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                        new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
                    series.CopyTo(kvpArray, 0);

                    double old = kvpArray[0].Value.value;

                    for (int i = 1; i < kvpArray.Length; i++)
                    {
                        double delta = series[kvpArray[i].Key].value - old;
                        old = series[kvpArray[i].Key].value;

                        if (GpsDatas[i].Speed > 0)
                        {
                            series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value;
                        }
                        else
                        {
                            series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value + delta;
                        }
                    }
                }
                indx++;
            } // foreach
        }

        private void CorrectFuelerValue(IDictionary<System.Int64, valueFuelSeries> series,
            atlantaDataSet.sensorsRow sensor,
            int IsMovingUse)
        {
            if (series.Count == 0)
                return;

            // ������������� �������� ��������� �������� - ��������� ������ � ������������. 
            // � ���������� �������� ���� � ������������ ����������.
            CorrectSeriesValues(series, sensor, GpsDatas);

            if (IsMovingUse == 0)
            {
                // �� ������� - ������������ ��������� �������� �� ����� ��������
                KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                    new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
                series.CopyTo(kvpArray, 0);

                double old = kvpArray[0].Value.value;

                for (int i = 1; i < kvpArray.Length; i++)
                {
                    double delta = series[kvpArray[i].Key].value - old;
                    old = series[kvpArray[i].Key].value;

                    if (GpsDatas[i].Speed > 0)
                    {
                        series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value;
                    }
                    else
                    {
                        series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value + delta;
                    }
                }
            }
        }

        private void CorrectFuelerValueDiff(IDictionary<System.Int64, valueFuelSeries> series,
           atlantaDataSet.sensorsRow sensor,
           int IsMovingUse)
        {
            if (series.Count == 0)
                return;

            // ������������� �������� ��������� �������� - ��������� ������ � ������������. 
            // � ���������� �������� ���� � ������������ ����������.
            CorrectSeriesValuesDiff(series, sensor, GpsDatas);

            if (IsMovingUse == 0)
            {
                // �� ������� - ������������ ��������� �������� �� ����� ��������
                KeyValuePair<System.Int64, valueFuelSeries>[] kvpArray =
                    new KeyValuePair<System.Int64, valueFuelSeries>[series.Count];
                series.CopyTo(kvpArray, 0);

                double old = kvpArray[0].Value.value;

                for (int i = 1; i < kvpArray.Length; i++)
                {
                    double delta = series[kvpArray[i].Key].value - old;
                    old = series[kvpArray[i].Key].value;

                    if (GpsDatas[i].Speed > 0)
                    {
                        series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value;
                    }
                    else
                    {
                        series[kvpArray[i].Key].value = series[kvpArray[i - 1].Key].value + delta;
                    }
                }
            }
        }

        /// <summary>
        /// ����������� ��������� �� ������������ ����� ��������
        /// </summary>
        /// <param name="previous">���������� �������� �������.</param>
        /// <param name="current">������� �������� �������.</param>
        /// <param name="sensorK">����������� ������� ���������������.</param>
        /// <returns>True - ��������� ������������.</returns>
        private bool OverflowOccurredOld(KeyValuePair<System.Int64, double> previous,
            KeyValuePair<System.Int64, double> current, double sensorK)
        {
            if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
            {
                MAX_COUNT_PER_SEC = (int) (set_row.AvgFuelRatePerHour*2/3600/sensorK);
            }

            // ������������ ��������� ���-�� ������������ ������� �� �������� ��������� ��������
            //double maxValue = (atlantaDataSet.dataview.FindByDataGps_ID(current.Key).time -
            //                   atlantaDataSet.dataview.FindByDataGps_ID(previous.Key).time).TotalSeconds*sensorK*
            //                  MAX_COUNT_PER_SEC;
            double maxValue = (GpsDatas.First(gps => gps.Id == current.Key).Time -
                               GpsDatas.First(gps => gps.Id == previous.Key).Time).TotalSeconds*sensorK*
                              MAX_COUNT_PER_SEC;
            // ��������� ��������� ��������
            double assessedValue = MAX_SENSOR_BOARD_VALUE*sensorK - previous.Value + current.Value;
            return maxValue >= assessedValue;
        }

        /// <summary>
        /// ����������� ��������� �� ������������ ����� ��������
        /// </summary>
        /// <param name="previous">���������� �������� �������.</param>
        /// <param name="current">������� �������� �������.</param>
        /// <param name="sensorK">����������� ������� ���������������.</param>
        /// <returns>True - ��������� ������������.</returns>
        private bool OverflowOccurred(double prevValue, DateTime prevTime, double curValue, DateTime curTime,
            double sensorK, double setAvgFuelRatePerHour)
        {
            if (setAvgFuelRatePerHour >= 0) //���� ���� ������ � ����������� �� ����� ������ ������
            {
                MAX_COUNT_PER_SEC = (int) (setAvgFuelRatePerHour * 2 / 3600 / sensorK);
            }

            // ������������ ��������� ���-�� ������������ ������� �� �������� ��������� ��������
            TimeSpan dTime = curTime - prevTime;
            double seconds = dTime.TotalMilliseconds / 1000;
            double maxValue = seconds * sensorK * MAX_COUNT_PER_SEC;
            //double maxValue = (curTime - prevTime).TotalSeconds * sensorK * MAX_COUNT_PER_SEC;
            // ��������� ��������� ��������
            double assessedValue = MAX_SENSOR_BOARD_VALUE * sensorK - prevValue + curValue;
            return maxValue >= assessedValue;
        }

        /// <summary>
        /// ���� ���� ��� ������� �������, �� �������� ���� � ������� � ���������� ���������
        /// � ������� �������. �� �������� �������� ����� �� �������� �������� (1-� � 2-�� ��� 2-� � 1-��). 
        /// </summary>
        protected void CorrectForTwoSensors(IDictionary<System.Int64, valueFuelSeries> valueSeris1,
            IDictionary<System.Int64, valueFuelSeries> valueSeris2,
            IDictionary<System.Int64, double> valueSerisSum)
        {
            if (valueSeris1.Count > 0 && valueSeris2.Count > 0)
                //���� ���� ������ ��� ������� ������� - ������� � ��� ������� � ����� ���������
                //� �� ������� �������������
            {
                foreach (KeyValuePair<System.Int64, valueFuelSeries> value in valueSeris1)
                {
                    try
                    {
                        // ��� �� ���� ��������?
                        // if (Properties.Settings.Default.FlowmeterOneSubTwo)
                        {
                            // ���1 - ���2
                            if (!valueSerisSum.ContainsKey(value.Key))
                                valueSerisSum.Add(value.Key,
                                    (valueSeris1[value.Key].value - valueSeris2[value.Key].value));
                        }
                        //   else
                        //   {
                        // ���2 - ���1
                        //      valueSerisSum.Add(value.Key, (valueSeris2[value.Key] - valueSeris1[value.Key]));
                        //   }
                    }
                    catch (Exception e)
                    {
                        throw new NotImplementedException(e.Message);
                    }

                    // -- aketner �� 01.09.2014 -- �������� ����� �������, ��������� � ��� ������, ���� ��������� ������� �� ����� ���� �������������
                    //double valueKey = valueSeris1[value.Key] - valueSeris2[value.Key];

                    //if( valueKey >= 0 )
                    //{
                    //    valueSerisSum.Add( value.Key, valueKey );
                    //}
                    //else
                    //{
                    //    valueSerisSum.Add( value.Key, -valueKey );
                    //}
                }
            }
        }

        /// <summary>
        /// ���� ���� ��� ������� �������, �� �������� ���� � ������� � ���������� ���������
        /// � ������� �������. �� �������� �������� ����� �� �������� �������� (1-� � 2-�� ��� 2-� � 1-��). 
        /// </summary>
        protected void CorrectForTwoSensorsDiff(IDictionary<System.Int64, valueFuelSeries> valueSeris1,
            IDictionary<System.Int64, valueFuelSeries> valueSeris2,
            IDictionary<System.Int64, double> valueSerisSum, FuelRateDictionarys frDct)
        {
            if (frDct.TypeAlgorithm1 != (int)AlgorithmType.FUELDIFFDRT || frDct.TypeAlgorithm2 != (int)AlgorithmType.FUELDIFFDRT)
                return;

            if (valueSeris1.Count > 0 && valueSeris2.Count > 0)
            //���� ���� ������ ��� ������� ������� - ������� � ��� ������� � ����� ���������
            //� �� ������� �������������
            {
                foreach (KeyValuePair<System.Int64, valueFuelSeries> value in valueSeris1)
                {
                    try
                    {
                        // ��� �� ���� ��������?
                        // if (Properties.Settings.Default.FlowmeterOneSubTwo)
                        {
                            // ���1 - ���2
                            if (!valueSerisSum.ContainsKey(value.Key))
                                valueSerisSum.Add(value.Key,
                                    (valueSeris1[value.Key].value - valueSeris2[value.Key].value));
                        }
                        //   else
                        //   {
                        // ���2 - ���1
                        //      valueSerisSum.Add(value.Key, (valueSeris2[value.Key] - valueSeris1[value.Key]));
                        //   }
                    }
                    catch (Exception e)
                    {
                        throw new NotImplementedException(e.Message);
                    }

                    // -- aketner �� 01.09.2014 -- �������� ����� �������, ��������� � ��� ������, ���� ��������� ������� �� ����� ���� �������������
                    //double valueKey = valueSeris1[value.Key] - valueSeris2[value.Key];

                    //if( valueKey >= 0 )
                    //{
                    //    valueSerisSum.Add( value.Key, valueKey );
                    //}
                    //else
                    //{
                    //    valueSerisSum.Add( value.Key, -valueKey );
                    //}
                }
            }
        }

        #endregion
    }
}
