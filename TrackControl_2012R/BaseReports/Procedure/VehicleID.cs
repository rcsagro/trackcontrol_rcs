﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BaseReports.Properties;
using BaseReports.RFID;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.Procedure
{
    public class VehicleID : Algorithm
    {
        public VehicleID()
        {
            // to do
        }

        public override void Run()
        {
            if (GpsDatas == null || 
                GpsDatas.Length == 0 || 
                m_row == null)
                return;

            int idMobitel = m_row.Mobitel_ID;

            using (VehicklesIDRecordsCreator creator = new VehicklesIDRecordsCreator())
            {
                creator.ProgressChanged += ReportProgressChanged;
                creator.IdMobitel = idMobitel;
                creator.m_row = m_row;
                if (DicIDRecords.ContainsKey(idMobitel))
                {
                    SelectItem(m_row);
                    Algorithm.OnAction(this, new EventArgs());
                    return;
                }
                else
                {
                    DicIDRecords.Add(idMobitel, new List<VehicleIDRecord>());
                }

                BeforeReportFilling(Resources.VehicleRFID, GpsDatas.Length);

                creator.SetDefaultObservers();
                creator.DefineSensors();

                if (!creator.CuttingDataGpsByRfid(GpsDatas))
                {
                    AfterReportFilling();
                    return;
                }

                creator.WriteNoRecordedGps();
                creator.ProgressChanged -= ReportProgressChanged;
            }

            AfterReportFilling();
            Algorithm.OnAction(this, new EventArgs());
        }  
    }
}