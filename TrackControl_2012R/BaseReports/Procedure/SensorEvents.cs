using System;
using System.Collections.Generic;
using BaseReports.EventTrack;
using BaseReports.Properties;
using LocalCache;
using TrackControl.General;

namespace BaseReports.Procedure
{
    /// <summary>
    /// ����� ��� ���������� ������ ��� ������ �� �������� ��������, ������������
    /// � ��������� ������������� ��������. ������� ��� ������ ����������� ���������������
    /// ������� � ��������.
    /// </summary>
    public class SensorEvents : Algorithm, IAlgorithm
    {
        #region Fields

        /// <summary>
        /// ��������� �������� �������. ������ ������ ����������� �������
        /// ������������� �������.
        /// </summary>
        private List<Tracker> _trackers = new List<Tracker>();

        /// <summary>
        /// �������, � ������� �������� ����� ����������� ������� ��� �������
        /// �������, ������������� � ���������.
        /// </summary>
        private Dictionary<int, int> _triggeringCount = new Dictionary<int, int>();

        #endregion

        #region IAlgorithm Members

        /// <summary>
        /// ����������� ������ ����������� ���������, ��������� �����
        /// � ��������� ������� ��������������� ������� � ��������.
        /// </summary>
        public override void Run()
        {
            // �������� ������� ������� � ������������ ��������
            if (0 == GpsDatas.Length || 0 == S_rows.Length)
            {
                return;
            }

            BeforeReportFilling(Resources.SensorEvents, GpsDatas.Length);

            bool lastPoint = false;

            for (int i = 1; i < GpsDatas.Length; i++)
            {
                lastPoint = i == (GpsDatas.Length - 1);

                foreach (Tracker tracker in _trackers)
                {
                    tracker.CheckEvent(GpsDatas[i], lastPoint);
                }

                if ((i%64) == 0)
                {
                    ReportProgressChanged(i);
                }
            }

            AfterReportFilling();
        }

        /// <summary>
        /// ������� �������, ������������ � �������� ���������, � �������������� �������
        /// ������� ������ ��������.
        /// </summary>
        /// <param name="m_row">�� ����, ������� ��������</param>
        public override void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);

            this.S_rows = (atlantaDataSet.sensorsRow[]) AtlantaDataSet.sensors.Select(String.Format("mobitel_id= {0}", m_row.Mobitel_ID));

            if (set_row != null) _minimTimeSwitch = set_row.MinTimeSwitch;

            _trackers.Clear();

            foreach (atlantaDataSet.sensorsRow sensor in this.S_rows)
            {
                Tracker tracker = new Tracker(sensor, Target.Report);
                if (set_row != null) tracker.minTimeSwitch = set_row.MinTimeSwitch; 
                tracker.NameSensor = sensor.Name;
                tracker.TimeTrans = new DateTime();

               // if (tracker.IsOK)
                {
                    tracker.Checked += new EventHandler<TrackerEventArgs>(tracker_Checked);
                    _trackers.Add(tracker);
                }
            }

            _triggeringCount.Clear();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// �������� ����������� ����� ��������� ������������ ������
        /// � ���������� ������� ��������� ������ ���������.
        /// </summary>
        protected override void AfterReportFilling()
        {
            base.AfterReportFilling();

            foreach (int key in _triggeringCount.Keys)
            {
                atlantaDataSet.SensorEventsTotalRow row = AtlantaDataSet.SensorEventsTotal.NewSensorEventsTotalRow();
                row.Name = AtlantaDataSet.sensors.FindByid(key).Name;
                row.Count = _triggeringCount[key];
                row.MobitelId = m_row.Mobitel_ID;
                AtlantaDataSet.SensorEventsTotal.AddSensorEventsTotalRow(row);
            }

            Algorithm.OnAction(this, new SensorEventsEventArgs(m_row.Mobitel_ID));
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// ������������ �� ����� ����������� �������, �.�. �������� �� ������ ������������
        /// ��������� � ������ ����������� ���������. ��������� ���������� ���������� � ���������������
        /// ������� � ��������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tracker_Checked(object sender, TrackerEventArgs e)
        {
            atlantaDataSet.SensorEventsRow row = AtlantaDataSet.SensorEvents.NewSensorEventsRow();
            //row.Id  = e.Id;
            row.Latitude = e.LatLng.Lat;
            row.Longitude = e.LatLng.Lng;
            row.SensorName = e.SensorName;
            row.EventTime = e.Time;
            row.Duration = e.Duration;

            row._durationMotion = e._durationMotion;
            row._durationStop = e._durationStop;

            row.Description = e.Description;
            bool IsCheckZone = false;
            row.Location = FindLocationWithCheckZoneIndication(new PointLatLng(row.Latitude, row.Longitude), out IsCheckZone);
            row.IsCheckZone = IsCheckZone;
            row.Speed = Math.Round(e.Speed, 5);
            row.Distance = e.Distance;
            row.MobitelId = m_row.Mobitel_ID;
            row.SpeedAvg = e.SpeedAvg;
            AtlantaDataSet.SensorEvents.AddSensorEventsRow(row);

            Tracker tracker = (Tracker) sender;
            tracker.minTimeSwitch = _minimTimeSwitch;
            if (_triggeringCount.ContainsKey(tracker.Sensor.id))
            {
                _triggeringCount[tracker.Sensor.id]++;
            }
            else
            {
                _triggeringCount.Add(tracker.Sensor.id, 1);
            }
        }

        #endregion
    }

    /// <summary>
    /// �����, ���������� ���������, ������� ����� �������� � ����������
    /// ������� �� ��������� ������ ��������� ���������� ������ ��� ������.
    /// </summary>
    public class SensorEventsEventArgs : EventArgs
    {
        /// <summary>
        /// ID ���������.
        /// </summary>
        private int _id;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="id">ID ���������</param>
        public SensorEventsEventArgs(int id)
        {
            _id = id;
        }

        /// <summary>
        /// ID ���������.
        /// </summary>
        public int Id
        {
            get { return _id; }
        }
    }
}
