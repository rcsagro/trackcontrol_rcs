using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using BaseReports.CrossingCZ;
using BaseReports.CrossingCZ.CzFinder;
using BaseReports.Procedure;
using BaseReports.Properties;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;
using TrackControl.UkrGIS.SearchEngine;
using TrackControl.Zones;

namespace BaseReports.Procedure
{
    /// <summary>
    /// ���������� ������� CrossZones �� ��������� ������
    /// �� ������ DataView, zones � points.
    /// <para>��������� ���� �� �������, ������������ �������, 
    /// ������� � ������� �� ���, �������� ������.</para>
    /// <para>"������" ����������� ���������.</para>
    /// </summary>
    public class CrossZonesAlgorithm : Algorithm
    {
        /// <summary>
        /// ����������� �������������� �������� ���� �����������.
        /// </summary>
        /// <param name="pointLocationType">PointLocationType.</param>
        /// <returns>��� ������������ ����� ������������ ��.</returns>
        public static string GetCrossTypeAsString(PointLocationType pointLocationType)
        {
            switch (pointLocationType)
            {
                case PointLocationType.CrossIn:
                    return Resources.CrossZonesAlgorithmCrossIn;
                case PointLocationType.CrossOut:
                    return Resources.CrossZonesAlgorithmCrossOut;
                case PointLocationType.BeginIn:
                    return Resources.CrossZonesAlgorithmBeginIn;
                case PointLocationType.EndIn:
                    return Resources.CrossZonesAlgorithmEndIn;
                case PointLocationType.BeginOut:
                    return Resources.CrossZonesAlgorithmBeginOut;
                case PointLocationType.EndOut:
                    return Resources.CrossZonesAlgorithmEndOut;
                default:
                    return "";
            }
        }

        /// <summary>
        /// ����������� �������������� �������� ���� ����������� 
        /// �� �������������� ��������������.
        /// </summary>
        /// <param name="pointLocationType">������������� �������������.</param>
        /// <returns>��� ������������ ����� ������������ ��.</returns>
        public static string GetCrossTypeAsString(byte pointLocationType)
        {
            return GetCrossTypeAsString((PointLocationType) pointLocationType);
        }

        /// <summary>
        /// ������ ��������������� ���, ����� ������� �������� ����.
        /// ������������� �� �����������.
        /// </summary>
        private ICollection<int> zones;

        /// <summary>
        /// ������ ����������� � ������ ����� �������� ���������.
        /// </summary>
        private LinkedList<CrossingInfo> listCrossings;

        /// <summary>
        /// ����������� ��������� �������� ����� ����� 
        /// ����������������� ������������� ����� ��. 
        /// <para>��������������� � ���������� - ������� setting</para>
        /// </summary>
        private TimeSpan minTime;

        /// <summary>
        /// �������� �� ��������� ������������ ���������� ��������� ����� ����� 
        /// ����������������� ������������� ����� �� = 1 ������.
        /// </summary>
        private static readonly TimeSpan defaultMinTime = new TimeSpan(0, 1, 0);

        /// <summary>
        /// ���� �� ���������� ����������� ����.
        /// True - �� ������� ���� ���� ���� ��������.
        /// </summary>
        public static bool AtLeastOneZoneIsChecked
        {
            get
            {
                return ZonesModel == null ? false : ZonesModel.Checked.Count > 0;
            }
        }

        /// <summary>
        /// ���������� �� ����������� ����.
        /// True - �� ������� ���� ���� ���� ����������.
        /// </summary>
        public static bool AtLeastOneZoneExists
        {
            get
            {
                return ZonesModel == null ? false : ZonesModel.GetAll().Count > 0;
            }
        }

        public override void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            base.SelectItem(m_row);
            minTime = (set_row == null)
                ? defaultMinTime
                : set_row.CzMinCrossingPairTime;
        }

        /// <summary>
        /// ������ ���������� ������ � �������������� ZonesLocator.
        /// </summary>
        public override void Run()
        {
            InnerRun(new StandardCzFinder(ZonesLocator));
        }

        /// <summary>
        /// ������ ���������� ������ � �������������� 
        /// ����������� ��������� ����������� ���.
        /// </summary>
        /// <param name="list">������ ��������������� ��.</param>
        public void Run(List<int> list)
        {
            InnerRun(new CustomCzFinder(ZonesModel, list));
        }

        /// <summary>
        /// ����� �����������.
        /// </summary>
        /// <param name="czFinder">ICzFinder.</param>
        private void InnerRun(ICzFinder czFinder)
        {
            if ((czFinder is CustomCzFinder) || (AtLeastOneZoneIsChecked))
            {
                zones = new List<int>();
                listCrossings = new LinkedList<CrossingInfo>();
                try
                {
                    FindCrossings(czFinder);
                    RemoveMarginalCrossings();
                    CalcKilometrage();
                    ExportToCrossZones();
                }
                finally
                {
                    zones = null;
                    listCrossings = null;
                }
            }
            else
            {
                MessageBox.Show(Resources.CrossZonesAlgorithmChooseZone,
                    "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// ����� ����� ����������� ����������� ��� (��) � ���������� ������� CrossZones.
        /// ����������� ������ ���������� ����.
        /// �������� ��������� ����������� �������������� ���������� ����� �
        /// ���������� ��, �.�. � �������������� ��.
        /// <para>������ ��������� ���������� ������� ��������� ���� �������� �� - 
        /// ����, � ������� ������� ���������� �����, � ����, � ������� ������ ������� �����.</para>
        /// <para>���� ��������� ��, ���������� �� ���������� ����, �� �������� 
        /// �������������� ��, ����������� �� ������� ����, ������ ��������� ����� � ��.</para>
        /// <para>���� ��������� ��, ���������� �� ���������� ����, �������� ������������� ����, 
        /// � � ����� ��������� ���� ������������� ���������� - ��������� ����� �� ��.</para>
        /// <para>���� ������������� ������������ � ����� ���������� - �������� ��� ���������
        /// � �������� ������ ��.</para>
        /// <para>� ������ ���� ������ ����� ������� ����������� � ��, �� ��� ����� ��������
        /// ��� ��������� � ��(PointLocationType.BEGIN_IN).</para>
        /// <para>� ������ ���� ��������� ����� ������� ����������� � �� � ��� ������� �� �����
        /// ������, �� ��� ����� �������� ��� ��������� � ��(PointLocationType.END_IN).</para>
        /// <para>���� ������ �(���) ��������� ����� ����� �� ��������� ��, �� ��� �����
        /// �������� ��� ������(���������) �� ��������� ��</para>
        /// 
        /// <para>�����-����� ��� �����-����� �/�� �� ����������� ������ ���� ����� �����
        /// ����� ������������ � ���� ����� ������ ���� ����� �������� �������������� 
        /// ��������� CZMinDoubleCrossTime, ��. app.config.</para>
        /// </summary>
        /// <param name="czFinder">ICzFinder.</param>
        private void FindCrossings(ICzFinder czFinder)
        {
            // ��������� ���, ������� ����������� ������� �����
            List<int> currentZonesSet;
            // ��������� ���, ������� ������������ ���������� �����
            List<int> prevZonesSet = new List<int>();

            // �������� �� ���� ������ ����� � ����������� ��������� � ����,
            // �� ��������� ���� ��������� ������� CrossZones
            // P.S. � d_rows ��������� ������ ������ ������ ���������
            foreach (GpsData d in GpsDatas)
            {
                currentZonesSet = czFinder.GetZonesWithPoint(d.LatLng);

                if ((prevZonesSet.Count == 0) && (currentZonesSet.Count == 0))
                {
                    if (d == GpsDatas[0])
                    {
                        // ������ �����(������) �� ��������� ��
                        AddCrossingInfo(m_row.Mobitel_ID, -1, d.Id, "", PointLocationType.BeginOut, d.Time);
                        continue;
                    }
                    if (d == GpsDatas[GpsDatas.Length - 1])
                    {
                        // ��������� �����(�����) �� ��������� ��
                        AddCrossingInfo(m_row.Mobitel_ID, -1, d.Id, "", PointLocationType.EndOut, d.Time);
                        break;
                    }
                }

                // �������������� ������ ��� ���������� ����� ��� - ������ � ��
                LinkedList<int> tmpZones = new LinkedList<int>(currentZonesSet);

                // ���������� ��� ��������� ��� ������ ������ �� ����
                foreach (int zoneId in prevZonesSet)
                {
                    // ���� � ������� ��������� ���������� ������������� ����, 
                    // ������ ��������� �����, ����� ��� ��������� � ��
                    if (!tmpZones.Contains(zoneId))
                    {
                        // ����� �� ����
                        AddCrossingInfo(m_row.Mobitel_ID, zoneId, d.Id,
                            ZonesModel.GetById(zoneId).Name,
                            PointLocationType.CrossOut, d.Time);
                    }
                    else
                    {
                        if (d == GpsDatas[GpsDatas.Length - 1])
                        {
                            // ��������� �����(�����) � ����
                            AddCrossingInfo(m_row.Mobitel_ID, zoneId, d.Id,
                                ZonesModel.GetById(zoneId).Name,
                                PointLocationType.EndIn, d.Time);
                        }
                        // �������������� �������� �������, ����� �������� ������ �����
                        tmpZones.Remove(zoneId);
                    }
                }

                // � tmpZones �������� ��������� ���� ��� � ������� ��������� �����
                foreach (int zoneId in tmpZones)
                {
                    AddToListZones(zoneId);
                    // ���� ������������� ������ ������ ������� � GPS ������� DataView,
                    // �� �� ����� ���� � �������(�������) � ����(��), ����� - ��� �����������-����� � ����.
                    AddCrossingInfo(m_row.Mobitel_ID, zoneId, d.Id,
                        ZonesModel.GetById(zoneId).Name,
                        d == GpsDatas[0] ? PointLocationType.BeginIn : PointLocationType.CrossIn,
                        d.Time);
                }

                // �������� ������� �������� �� ��� ������� ��������� �����
                prevZonesSet.Clear();
                prevZonesSet = currentZonesSet.GetRange(0, currentZonesSet.Count);
            }
        }

        /// <summary>
        /// ������� ����� ����������� �� ������ listCrossings � ������� CrossZones.
        /// </summary>
        private void ExportToCrossZones()
        {
            CrossZones.Instance.Add(m_row.Mobitel_ID, new List<CrossingInfo>(listCrossings));
        }

        /// <summary>
        /// ���������� ����� ����������� �� ��������� ������ listCrossings.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="zoneID">������������� ��.</param>
        /// <param name="pointId">������������� �����.</param>
        /// <param name="zoneName">������������ ��.</param> 
        /// <param name="pointLocationType">��� ������������ ����� ������������ ��.</param>
        /// <param name="crossingTime">����� �����������.</param>
        private void AddCrossingInfo(int mobitelId, int zoneID, System.Int64 pointId,
            string zoneName, PointLocationType pointLocationType, DateTime crossingTime)
        {
            listCrossings.AddLast(new CrossingInfo(
                mobitelId, zoneID, pointId, zoneName, pointLocationType, crossingTime, 0));
        }

        /// <summary>
        /// �������� � ������ ��������������� ��� ��� ����, 
        /// ���� ������ ��� � ������.
        /// </summary>
        /// <param name="zoneId">������������� ����.</param>
        private void AddToListZones(int zoneId)
        {
            if (!zones.Contains(zoneId))
            {
                zones.Add(zoneId);
            }
        }

        /// <summary>
        /// ������� ���������� � �������������� ������������ �����-����� ��� 
        /// �����-����� �/�� ��, ���� ����� ������ �������� �������������� 
        /// ��������� CZMinCrossDeltaTime (��. app.config).
        /// ��������� ������� � ��������� CrossingCZ.doc.
        /// <para>� ����� ������ �������� ������� ��� ����� - 
        /// ������ � ����� ��������.</para>
        /// <para>���� ���-�� ����� ������ ��� ����� ����, �� ������������� � 
        /// ������� ������ ������. ��� ����� - ��� ������, ���� ����������� � �����.</para>
        /// </summary>
        private void RemoveMarginalCrossings()
        {
            // ��� ����� - ��� ������, ���� ����������� � �����
            if ((zones.Count == 1) && (listCrossings.Count <= 3))
            {
                return;
            }

            // �����������, ������� �������� ��������
            List<CrossingInfo> delRowsBuffer = new List<CrossingInfo>();
            // ������������ ����������� ������� ��
            List<CrossingInfo> crossingSubset = new List<CrossingInfo>();
            // ������
            int i;

            foreach (int zoneId in zones)
            {
                FillCrossingSubsetByZoneId(zoneId, crossingSubset);

                // ���� ����������� ��������� ������
                if (crossingSubset.Count == 1)
                {
                    crossingSubset.Clear();
                    continue;
                }

                i = 0;
                // ����������� ���� 
                while (i < crossingSubset.Count - 1)
                {
                    // ��������� ������� ��� ���� ��������� �����������,  
                    // �.� ���������� ���� ���� �������� ��� ��������
                    if (i == crossingSubset.Count - 1)
                    {
                        break;
                    }

                    if ((crossingSubset[i + 1].CrossingTime - crossingSubset[i].CrossingTime) < minTime)
                    {
                        delRowsBuffer.Add(crossingSubset[i]);
                        delRowsBuffer.Add(crossingSubset[i + 1]);
                        i += 2;
                    }
                    else
                    {
                        i++;
                    }
                }

                crossingSubset.Clear();

                // �������� �������������� �����������
                if (delRowsBuffer.Count > 0)
                {
                    foreach (CrossingInfo ci in delRowsBuffer)
                    {
                        listCrossings.Remove(ci);
                    }
                    delRowsBuffer.Clear();
                }
            }
        }

        /// <summary>
        /// ���������� ������������ �����������, ������������ � 
        /// �������� ��.
        /// </summary>
        /// <param name="zoneId">������������� ��.</param>
        /// <param name="subset">������� ������������.</param>
        private void FillCrossingSubsetByZoneId(int zoneId, IList<CrossingInfo> subset)
        {
            foreach (CrossingInfo ci in listCrossings)
            {
                if ((ci.ZoneID == zoneId) && (ci != listCrossings.First.Value) &&
                    (ci != listCrossings.Last.Value))
                {
                    subset.Add(ci);
                }
            }
        }

        /// <summary>
        /// ������� ����������� ����� ��������� �������������. 
        /// </summary>
        private void CalcKilometrage()
        {
            if (listCrossings.Count == 0)
            {
                return;
            }

            IEnumerator<CrossingInfo> enumerator = listCrossings.GetEnumerator();
            enumerator.MoveNext();
            CrossingInfo current = enumerator.Current;
            CrossingInfo next;

            while (enumerator.MoveNext())
            {
                next = enumerator.Current;
                if (current.PointId != next.PointId)
                {
                    // ������� �����, ���� ��� �������� ����������� �� ������������ �����
                    // ���� � �� �� ����� (��� �������������� ��)
                    //next.Kilometrage = (float)BasicMethods.GetKilometrage(
                    //  DataSetManager.GetDataGpsForPeriod(m_row, current.CrossingTime, next.CrossingTime));
                    next.Kilometrage =
                        (float)
                            BasicMethods.GetKilometrage(
                                GpsDatas.Where(gps => gps.Time >= current.CrossingTime && gps.Time <= next.CrossingTime)
                                    .ToArray());
                }
                current = next;
            }
        }
    }
}
