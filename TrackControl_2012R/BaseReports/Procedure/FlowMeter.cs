using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;
using BaseReports.Properties;
using DevExpress.XtraEditors;

namespace BaseReports.Procedure
{
    public class Summaries
    {
        public string travelLabel;
        public string timeTravelLabel;
        public string stopTimeLabel;
        public string idleFuelRateLabel;
        public string moveFuelRateLabel;
        public string flowmeterLabel;
        public TimeSpan timeTravel;
        public TimeSpan timeStoping;
    }

    public class valueFuelSeries
    {
        public DateTime Time;
        public double value;
    }

    public class FuelRateDictionarys
    {
        public atlantaDataSet.sensorsRow fuelRateSensor1;

        public atlantaDataSet.sensorsRow fuelRateSensor2;

        public List<atlantaDataSet.sensorsRow> fuelDutSensors;

        // ��� ��������� ��������� fuelRateSensor1
        public int TypeAlgorithm1 { get; set; }

        // ��� ��������� ��������� fuelRateSensor2
        public int TypeAlgorithm2 { get; set; }

        public bool FuelRateSensor1Exist
        {
            get { return fuelRateSensor1 != null; }
            set
            {
                if(value == false)
                    fuelRateSensor1 = null;
            }
        }

        public bool FuelRateSensor2Exist
        {
            get { return fuelRateSensor2 != null; }
            set
            {
                if(value == false)
                    fuelRateSensor2 = null;
            }
        }

        private List<IDictionary<System.Int64, double>> valueDictDutos;

        public List<IDictionary<System.Int64, double>> ValueDutSeries
        {
            get { return valueDictDutos; }
            set { valueDictDutos = value; }
        }

        private IDictionary<System.Int64, valueFuelSeries> valSeries1 = new Dictionary<System.Int64, valueFuelSeries>();
        public IDictionary<System.Int64, valueFuelSeries> ValSeries1
        {
            get { return valSeries1; }
            set { valSeries1 = value; }
        }

        private IDictionary<System.Int64, valueFuelSeries> valSeries2 = new Dictionary<System.Int64, valueFuelSeries>();
        public IDictionary<System.Int64, valueFuelSeries> ValSeries2
        {
            get { return valSeries2; }
            set { valSeries2 = value; }
        }

        private IDictionary<System.Int64, double> valueSeriesSum = new Dictionary<System.Int64, double>();

        public IDictionary<System.Int64, double> ValueSeriesSum
        {
            get { return valueSeriesSum; }
            set { valueSeriesSum = value; }
        }

        private IDictionary<System.Int32, Summaries> summarieses = new Dictionary<int, Summaries>();

        public IDictionary<System.Int32, Summaries> Summarieses
        {
            get { return summarieses; }
            set { summarieses = value; }
        }

        IDictionary<System.Int32, IDictionary<System.Int64, valueFuelSeries>> valSeriesData1 = new Dictionary<int, IDictionary<long, valueFuelSeries>>();
        IDictionary<System.Int32, IDictionary<System.Int64, valueFuelSeries>> valSeriesData2 = new Dictionary<int, IDictionary<long, valueFuelSeries>>();
        private IDictionary<int, IDictionary<long, double>> valueSeriesDataSum = new Dictionary<int, IDictionary<long, double>>();

        public IDictionary<System.Int32, IDictionary<System.Int64, valueFuelSeries>> ValSeriesData1
        {
            get { return valSeriesData1; } 
            set { valSeriesData1 = value; }
        }

        public IDictionary<System.Int32, IDictionary<System.Int64, valueFuelSeries>> ValSeriesData2
        {
            get { return valSeriesData2; }
            set { valSeriesData2 = value; }
        }

        public IDictionary<System.Int32, IDictionary<System.Int64, double>> ValueSeriesDataSum
        {
            get { return valueSeriesDataSum; }
            set { valueSeriesDataSum = value; }
        }

        protected IDictionary<System.Int32, atlantaDataSet.sensorsRow> sensorDict1 = new Dictionary<int, atlantaDataSet.sensorsRow>();
        protected IDictionary<System.Int32, atlantaDataSet.sensorsRow> sensorDict2 = new Dictionary<int, atlantaDataSet.sensorsRow>();

        public IDictionary<System.Int32, atlantaDataSet.sensorsRow> SensorsDict1
        {
            get { return sensorDict1; } 
            set { sensorDict1 = value; }
        }
        public IDictionary<System.Int32, atlantaDataSet.sensorsRow> SensorsDict2
        {
            get { return sensorDict2; }
            set { sensorDict2 = value; }
        }

        public FuelRateDictionarys()
        {
            // for DUTs values
            valueDictDutos = new List<IDictionary<long, double>>();
        }

        public void Clear()
        {
            valSeries1 = new Dictionary<System.Int64, valueFuelSeries>();
            valSeries2 = new Dictionary<System.Int64, valueFuelSeries>();
            valueSeriesSum = new Dictionary<System.Int64, double>();
        }

        public void ClearDut()
        {
            foreach (var series in valueDictDutos)
            {
                series.Clear();
            }
            valueDictDutos = new List<IDictionary<long, double>>(); 
            valueSeriesSum = new Dictionary<System.Int64, double>();
        }

        /// <summary>
        /// ������������ ������ �� �������� ��������.
        /// </summary>
        public void FreeSensorReferences()
        {
            fuelRateSensor1 = null;
            fuelRateSensor2 = null;
        }
    } // FuelRateDictionarys

    public class FlowMeter : PartialAlgorithms
    {
        private static FlowMeter pFlowMeter;
        public FuelRateDictionarys frDictionarys;

        private BindingSource atlantaData;

        public FuelRateDictionarys GetFuelRateDictionarys
        {
            get { return frDictionarys; }
            set { frDictionarys = value; }
        }

        public BindingSource SetDataBindingSource
        {
            get { return atlantaData; } 
            set { atlantaData = value; }
        }

        #region __c.tor__

        public static FlowMeter GetFlowMeter()
        {
            if (pFlowMeter == null)
            {
                pFlowMeter = new FlowMeter();
            }
            return pFlowMeter;
        }

        private FlowMeter()
        {
            frDictionarys = new FuelRateDictionarys();
        }

        #endregion

        #region IAlgorithm Members

        /// <summary>
        /// ����������� ������ ����������� ���������, ��������� �����
        /// � ��������� ������� ��������������� ������� � ��������.
        /// ������ �������������� ������� ������� �������. ���� �� ���� - ������ �� ���� �������.
        /// ���� ��� �� ���� - ���������� ������� ������������ ���������, ������ - ������� �� ���������.
        /// </summary>
        public override void Run()
        {
            // �������� ������� �������
            if (0 == GpsDatas.Length)
            {
                return;
            }

            if (m_row == null)
            {
                return;
            }

            if (m_row.GetflowmeterReportRows() != null)
            {
                if (m_row.GetflowmeterReportRows().Length != 0)
                {
                    return;
                }
            }

            frDictionarys.FreeSensorReferences();

            bool is_fuel2_direct_inverse = true;
            bool is_fuel2_differance = false;

            // ��� ��� ���������� � ��������� DRT ����� ���� ������ (DRT, DRT1, DRT2, DRTDIFF)
            // �� ���������� ������� ������ ���������� FUEL_DIRECT, FUEL_INVERSE, FUEL_DIFF
            // ������� � ������ �������� �������� FUEL2
            // ������� ������� ���� �������� FUEL2

            // ����, ���� FUEL2
            FindSensorsDRT(frDictionarys, AlgorithmType.FUEL2);
            frDictionarys.Clear();

            if ((GpsDatas.Length == 0) ||
                ((frDictionarys.FuelRateSensor1Exist) && (!frDictionarys.FuelRateSensor2Exist)))
            {
                FindSensorsDRT(frDictionarys, AlgorithmType.FUELDIFFDRT);

                if (frDictionarys.FuelRateSensor2Exist)
                {
                    GettingValuesDRTDiffer(frDictionarys);
                }
            }

            if ((GpsDatas.Length == 0) || ((!frDictionarys.FuelRateSensor1Exist) &&  
                (!frDictionarys.FuelRateSensor2Exist)))
            {
                frDictionarys.FreeSensorReferences();

                // ����� ���� �������� FUEL_DIRECT � FUEL_INVERSE
                FindSensorsDRT(frDictionarys, AlgorithmType.FUEL_DIRECT); // ��� ���1
                FindSensorsDRT(frDictionarys, AlgorithmType.FUEL_INVERSE); // ��� ���2

                if ((GpsDatas.Length == 0) ||
                    ((!frDictionarys.FuelRateSensor1Exist) &&
                     (!frDictionarys.FuelRateSensor2Exist)))
                {
                    frDictionarys.FreeSensorReferences();
                    // �� ��� ����� ����, ����� ������ �������� ������� ��������� ��� ��������������
                    FindSensorsDRT(frDictionarys, AlgorithmType.FUELDRTADD);
                    is_fuel2_direct_inverse = false;

                    if ((GpsDatas.Length == 0) ||
                        ((!frDictionarys.FuelRateSensor1Exist) &&
                         (!frDictionarys.FuelRateSensor2Exist)))
                    {
                        frDictionarys.FreeSensorReferences();
                        // ���������� ����� �������� FUEL_DIFF
                        FindSensorsDRT(frDictionarys, AlgorithmType.FUELDIFFDRT);

                        if (frDictionarys.FuelRateSensor1Exist)
                        {
                            is_fuel2_differance = true;
                        }
                    }

                    if ((GpsDatas.Length == 0) ||
                        ((!frDictionarys.FuelRateSensor1Exist) &&
                         (!frDictionarys.FuelRateSensor2Exist)))
                        {
                            frDictionarys.FreeSensorReferences();
                            return; // �� ����, ������� ���������� �� �����, �� ����� ������ �� �����
                        } // if
                } // if
            } // if

            if (is_fuel2_differance)
            {
                GettingValuesDRTDiffer(frDictionarys);
            }

            // ���-�� �����, ����� �� ��������� ����������: FUEL2, FUEL_DIRECT, FUEL_INVERSE
            if (is_fuel2_direct_inverse)
            {
                GettingValuesDRT(frDictionarys);
            }
            else if (!is_fuel2_differance) // ����� �� ��������� ��������� FUELDRTADD � FUEL_DIFF
            {
                FindSensorsDRT(frDictionarys, AlgorithmType.FUELDIFFDRT);

                if (frDictionarys.FuelRateSensor2Exist)
                {
                    GettingValuesDRTDiffer(frDictionarys);
                }

                GettingValuesDRTAdd(frDictionarys);
            }
            // ======= ����� ��������������� ��������� ==============================

            try
            {
                FillReport();

                FillSummary(frDictionarys, m_row);

                if (!frDictionarys.ValSeriesData1.ContainsKey(m_row.Mobitel_ID))
                {
                    frDictionarys.ValSeriesData1.Add(m_row.Mobitel_ID, frDictionarys.ValSeries1);
                }

                if (!frDictionarys.ValSeriesData2.ContainsKey(m_row.Mobitel_ID))
                {
                    frDictionarys.ValSeriesData2.Add(m_row.Mobitel_ID, frDictionarys.ValSeries2);
                }

                if (frDictionarys.ValueSeriesDataSum.ContainsKey(m_row.Mobitel_ID))
                {
                    frDictionarys.ValueSeriesDataSum.Remove(m_row.Mobitel_ID);
                    frDictionarys.ValueSeriesDataSum.Add(m_row.Mobitel_ID, frDictionarys.ValueSeriesSum);
                }
                else
                {
                    frDictionarys.ValueSeriesDataSum.Add(m_row.Mobitel_ID, frDictionarys.ValueSeriesSum);
                }

                if (frDictionarys.SensorsDict1.ContainsKey(m_row.Mobitel_ID))
                {
                    frDictionarys.SensorsDict1.Remove(m_row.Mobitel_ID);
                    frDictionarys.SensorsDict1.Add(m_row.Mobitel_ID, frDictionarys.fuelRateSensor1);
                }
                else
                {
                    frDictionarys.SensorsDict1.Add(m_row.Mobitel_ID, frDictionarys.fuelRateSensor1);                    
                }

                if (frDictionarys.SensorsDict2.ContainsKey(m_row.Mobitel_ID))
                {
                    frDictionarys.SensorsDict2.Remove(m_row.Mobitel_ID);
                    frDictionarys.SensorsDict2.Add(m_row.Mobitel_ID, frDictionarys.fuelRateSensor2);
                }
                else
                {
                    frDictionarys.SensorsDict2.Add(m_row.Mobitel_ID, frDictionarys.fuelRateSensor2);
                }
            }
            finally
            {
                AfterReportFilling();
                OnAction(this, null);
            }
        }

        private void FillSummary(FuelRateDictionarys dictionarys, atlantaDataSet.mobitelsRow mRow)
        {
            FuelRateTotal frTotal = GetTotals(mRow);
            Summaries summar = new Summaries();

            if (frTotal != null)
            {
                summar.travelLabel = Resources.DistanceText + ": " + frTotal.TotalDistance.ToString("0.##"); // ���������� ����
                summar.timeTravelLabel = Resources.TotalTravelTimeText + ": " + String.Format("{0} ({1})", frTotal.TotalTimeMotion, frTotal.TotalTimeMotionWithoutShortStops); // ����� � ����
                summar.stopTimeLabel = Resources.FlowmeterCtrlParkingTime + " " + String.Format("{0} ({1})", frTotal.TotalTimeStops, frTotal.TotalTimeStopsWithShortStops); // ����� ������� 
                //summar.idleFuelRateLabel = frTotal.TotalFuelRateOnHour.ToString("0.##"); // ������ ������� 
                //summar.moveFuelRateLabel = frTotal.TotalFuelRateOnHundred.ToString("0.##"); // ������
                summar.flowmeterLabel = Resources.TotalLiters + ": " + frTotal.TotalFuelRate.ToString("0.##"); // ����� �������
                summar.timeTravel = frTotal.TotalTimeMotion;
                summar.timeStoping = frTotal.TotalTimeStops;
            } // if

            dictionarys.Summarieses.Add(mRow.Mobitel_ID, summar);
        } // FillSummary

        #endregion

        #region General Methods

        // ����� �� ������� ���������
        public FuelRateTotal GetTotals(atlantaDataSet.mobitelsRow m_row)
        {
            FuelRateTotal fuelRateTotal = GetFuelRateTotal(m_row);
            FuelRateDictionarys frDict = GetFuelRateDictionarys;

            if (fuelRateTotal.FuelRateSensor1 != null) //��������� ��� �� ��� �������� ����� �� ������� ��
            {
                PartialAlgorithms partialAlg = new PartialAlgorithms();
                partialAlg.SelectItem(m_row);
                GpsData[] d_rows = DataSetManager.GetDataGpsArray(m_row);
                fuelRateTotal.TotalDistance = Math.Round(m_row.path, 2);
                fuelRateTotal.TotalTimeMotionWithoutShortStops = BasicMethods.GetTotalMotionTime(d_rows);
                fuelRateTotal.TotalTimeStopsWithShortStops = BasicMethods.GetTotalStopsTime(d_rows, 0);

                double inmotion = partialAlg.GetFuelUseInMotionDrt(frDict, d_rows, 0);

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                double instop = partialAlg.GetFuelUseInStopDrt(frDict, d_rows, 0, 0); // ���� 41 ���
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                TimeSpan tmspan = BasicMethods.GetTotalStopsTime(d_rows, 0);

                stopWatch = new Stopwatch();
                stopWatch.Start();
                TimeSpan stopengin = partialAlg.GetTimeStopEnginOff(frDict, d_rows, 0, 0); // ���� 41 ���
                stopWatch.Stop();
                ts = stopWatch.Elapsed;

                fuelRateTotal.TotalFuelRateOnHour = ((inmotion + instop) / (fuelRateTotal.TotalTimeMotion + (tmspan - stopengin)).TotalHours);

                fuelRateTotal.TotalFuelRateOnHour = Math.Round(fuelRateTotal.TotalFuelRateOnHour, 2);

                fuelRateTotal.TotalFuelRateOnHundred = Math.Round(m_row.motionFuelRate, 2);
                fuelRateTotal.TotalFuelRate = Math.Round(m_row.summFuelRate, 2);

                foreach (DataRowView dr in atlantaData)
                {
                    atlantaDataSet.flowmeterReportRow fRow = (atlantaDataSet.flowmeterReportRow)dr.Row;

                    if (fRow.Location == Resources.Movement)
                    {
                        fuelRateTotal.TotalTimeMotion += fRow.duration;
                    }
                    else
                    {
                        fuelRateTotal.TotalTimeStops += fRow.duration;
                    }
                } // foreach
            } // if
            else
            {
                fuelRateTotal = null;
            }
            return fuelRateTotal;
        } // GetTotals

        /// <summary>
        /// ������� ��������� ������ � �������� ����������
        /// </summary>
        public FuelRateTotal GetFuelRateTotal(atlantaDataSet.mobitelsRow loc_m_row)
        {
            FuelRateTotal frTotal = new FuelRateTotal();
            m_row = loc_m_row;

            FindSensorsDRT(frDictionarys, AlgorithmType.FUEL2);

            if (frDictionarys.FuelRateSensor1Exist && frDictionarys.FuelRateSensor2Exist)
            {
                frTotal.FuelRateSensor1 = frDictionarys.fuelRateSensor1;
                frTotal.FuelRateSensor2 = frDictionarys.fuelRateSensor2;
            }
            else
            {
                FindSensorsDRT(frDictionarys, AlgorithmType.FUEL_DIRECT);
                FindSensorsDRT(frDictionarys, AlgorithmType.FUEL_INVERSE);

                if (frDictionarys.FuelRateSensor1Exist && frDictionarys.FuelRateSensor2Exist)
                {
                    frTotal.FuelRateSensor1 = frDictionarys.fuelRateSensor1;
                    frTotal.FuelRateSensor2 = frDictionarys.fuelRateSensor2;
                }
                else
                {
                    FindSensorsDRT(frDictionarys, AlgorithmType.FUELDRTADD);
                    if (frDictionarys.FuelRateSensor1Exist && frDictionarys.FuelRateSensor2Exist)
                    {
                        frTotal.FuelRateSensor1 = frDictionarys.fuelRateSensor1;
                        frTotal.FuelRateSensor2 = frDictionarys.fuelRateSensor2;
                    }

                    FindSensorsDRT(frDictionarys, AlgorithmType.FUELDIFFDRT);
                    if (frDictionarys.FuelRateSensor1Exist && frDictionarys.FuelRateSensor2Exist)
                    {
                        frTotal.FuelRateSensor1 = frDictionarys.fuelRateSensor1;
                        frTotal.FuelRateSensor2 = frDictionarys.fuelRateSensor2;
                    }
                }
            }

            return frTotal;
        }

        private void FillReport()
        {
            try
            {
                double timeEngineWorkOnStops = 0;
                KeyValuePair<System.Int64, double>[] kvpArrayValueFuelRate = null;
                IDictionary<System.Int64, double> pCorrectValueSeries = new Dictionary<long, double>();

                if (frDictionarys.ValSeries1.Count > 0)
                {
                    kvpArrayValueFuelRate = new KeyValuePair<System.Int64, double>[frDictionarys.ValSeries1.Count];
                }

                if (frDictionarys.ValueSeriesSum.Count == 0)
                {
                    if (kvpArrayValueFuelRate == null)
                        return;

                    if (kvpArrayValueFuelRate.Length <= 0)
                        return;

                    int k = 0;
                    foreach (var lem in frDictionarys.ValSeries1)
                    {
                        KeyValuePair<System.Int64, double> kvPair = new KeyValuePair<long, double>(lem.Key, lem.Value.value);
                        kvpArrayValueFuelRate[k++] = kvPair;
                        pCorrectValueSeries.Add(lem.Key, lem.Value.value);
                    }
                }
                else
                {
                    if (kvpArrayValueFuelRate == null)
                        return;
                    if (kvpArrayValueFuelRate.Length <= 0)
                        return;

                    frDictionarys.ValueSeriesSum.CopyTo(kvpArrayValueFuelRate, 0);
                    pCorrectValueSeries = frDictionarys.ValueSeriesSum;
                }

                // ������������ ������
                double idleFuelRate = 0;
                double motionFuelRate = 0;
                double summFuelRate = 0;
                int mobitelID = m_row.Mobitel_ID;

                atlantaDataSet.KilometrageReportRow[] km_rows = m_row.GetKilometrageReportRows();
                if (km_rows.Length == 0)
                {
                    return;
                }

                BeforeReportFilling(Resources.FuelConsumption, km_rows.Length);

                for (int i = 0; i < km_rows.Length; i++)
                {
                    atlantaDataSet.flowmeterReportRow fr_row = AtlantaDataSet.flowmeterReport.NewflowmeterReportRow();
                    fr_row.Mobitel_id = mobitelID; //m_row.Mobitel_ID;
                    fr_row.beginTime = km_rows[i].InitialTime;
                    fr_row.endTime = km_rows[i].FinalTime;
                    fr_row.end_id = km_rows[i].FinalPointId;
                    fr_row.duration = km_rows[i].Interval;

                    if (km_rows[i].Distance == 0 && km_rows[i].State != Resources.Movement) //row with stops
                    {
                        fr_row.begin_id = km_rows[i].InitialPointId; 
                        fr_row.Location = String.Format(Resources.StoppingFormat, km_rows[i].Location);
                        if (pCorrectValueSeries.ContainsKey(fr_row.end_id) & pCorrectValueSeries.ContainsKey(fr_row.begin_id))
                        {
                            fr_row.fuelRate = pCorrectValueSeries[fr_row.end_id] - pCorrectValueSeries[fr_row.begin_id];
                        }
                        // �������� = ������ ������� �� ����� ������� / ����� ����� ������� � ���������� ����������
                        fr_row.idleFuelRate = 0;

                        // ������� ������ ��� ����������� ������� ������ ��������� �� �������
                        GpsData[] dataRows = DataSetManager.GetDataGpsForPeriod(m_row, fr_row.beginTime, fr_row.endTime);

                        if (dataRows.Length > 0)
                        {
                            // ����������� ������� ������� � ����. ����������
                            TimeSpan timeStopEnginOff = new TimeSpan(0, 0, 0);
                            timeStopEnginOff = GetTimeStopEnginOff(frDictionarys, dataRows, 0, 0);
                            // � � ���. ����������
                            TimeSpan delta = fr_row.duration - timeStopEnginOff;
                            double timeStopEnginOn = (delta).TotalHours;
                            timeEngineWorkOnStops += timeStopEnginOn;
                            if (fr_row.fuelRate >= 0.05 && timeStopEnginOn >= 0.0083334)
                            {
                                fr_row.idleFuelRate = fr_row.fuelRate / timeStopEnginOn;
                            }
                        }

                        idleFuelRate += fr_row.fuelRate;
                    }
                    else
                    {
                        fr_row.begin_id = (i > 0) ? km_rows[i - 1].FinalPointId : kvpArrayValueFuelRate[0].Key;
                        fr_row.Location = Resources.Movement;

                        if (pCorrectValueSeries.ContainsKey(fr_row.end_id) &
                            pCorrectValueSeries.ContainsKey(fr_row.begin_id))
                        {
                            double avarMax = pCorrectValueSeries[fr_row.end_id];
                            double avarMin = pCorrectValueSeries[fr_row.begin_id];
                            fr_row.fuelRate = avarMax - avarMin;
                        } 

                        fr_row.travel = km_rows[i].Distance;
                        fr_row.moveFuelRate = fr_row.fuelRate * 100 / fr_row.travel;
                        motionFuelRate += fr_row.fuelRate;
                    }

                    summFuelRate += fr_row.fuelRate;
                    AtlantaDataSet.flowmeterReport.AddflowmeterReportRow(fr_row);
                    ReportProgressChanged(i);
                } // for

                m_row.summFuelRate = summFuelRate;
                m_row.idleFuelRate = idleFuelRate / timeEngineWorkOnStops;
                m_row.motionFuelRate = motionFuelRate * 100 / m_row.path;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Fill Report", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            #endregion
        }
    }

    /// <summary>
    /// ����� �� ������ "������"
    /// </summary>
    public class FuelRateTotal
    {
        private double totalDistance;

        public double TotalDistance
        {
            get { return totalDistance; }
            set { totalDistance = value; }
        }

        private TimeSpan totalTimeMotionWithoutShortStops;

        public TimeSpan TotalTimeMotionWithoutShortStops
        {
            get { return totalTimeMotionWithoutShortStops; }
            set { totalTimeMotionWithoutShortStops = value; }
        }

        private TimeSpan totalTimeStopsWithShortStops;

        public TimeSpan TotalTimeStopsWithShortStops
        {
            get { return totalTimeStopsWithShortStops; }
            set { totalTimeStopsWithShortStops = value; }
        }

        private TimeSpan totalTimeMotion;

        public TimeSpan TotalTimeMotion
        {
            get { return totalTimeMotion; }
            set { totalTimeMotion = value; }
        }

        private TimeSpan totalTimeStops;

        public TimeSpan TotalTimeStops
        {
            get { return totalTimeStops; }
            set { totalTimeStops = value; }
        }

        private double totalFuelRateOnHour;

        public double TotalFuelRateOnHour
        {
            get { return totalFuelRateOnHour; }
            set { totalFuelRateOnHour = value; }
        }

        private double totalFuelRateOnHundred;

        public double TotalFuelRateOnHundred
        {
            get { return totalFuelRateOnHundred; }
            set { totalFuelRateOnHundred = value; }
        }

        private double totalFuelRate;

        public double TotalFuelRate
        {
            get { return totalFuelRate; }
            set { totalFuelRate = value; }
        }

        private atlantaDataSet.sensorsRow fuelRateSensor1;

        public atlantaDataSet.sensorsRow FuelRateSensor1
        {
            get { return fuelRateSensor1; }
            set { fuelRateSensor1 = value; }
        }

        private atlantaDataSet.sensorsRow fuelRateSensor2;

        public atlantaDataSet.sensorsRow FuelRateSensor2
        {
            get { return fuelRateSensor2; }
            set { fuelRateSensor2 = value; }
        }
    }
}
