using System;
using System.Collections.Generic;

namespace BaseReports
{
  public interface IReportCollection
  {    
    /// <summary>
    /// ���������� ������
    /// </summary>
    /// <param name="report"></param>
    int AddReport(IReportTabControl report);
    /// <summary>
    /// �������������
    /// </summary>
    /// <returns></returns>
    IEnumerator<IReportTabControl> GetEnumerator();
    /// <summary>
    /// ������� ������ �������
    /// </summary>
    void RemoveAll();
  }
}
