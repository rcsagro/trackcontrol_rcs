namespace BaseReports
{
  partial class ReportsControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._xtraTabs = new DevExpress.XtraTab.XtraTabControl();
        ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).BeginInit();
        this.SuspendLayout();
        // 
        // _xtraTabs
        // 
        this._xtraTabs.Dock = System.Windows.Forms.DockStyle.Fill;
        this._xtraTabs.Location = new System.Drawing.Point(0, 0);
        this._xtraTabs.Name = "_xtraTabs";
        this._xtraTabs.Size = new System.Drawing.Size(578, 267);
        this._xtraTabs.TabIndex = 2;
        // 
        // ReportsControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._xtraTabs);
        this.Name = "ReportsControl";
        this.Size = new System.Drawing.Size(578, 267);
        ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraTab.XtraTabControl _xtraTabs;
  }
}
