using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BaseReports
{
    public partial class BaseControl : DevExpress.XtraEditors.XtraUserControl
    {
        public BaseControl()
        {
            InitializeComponent();
        }
    }
}
