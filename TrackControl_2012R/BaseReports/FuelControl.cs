﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.Vehicles;

namespace BaseReports
{
    [ToolboxItem(false)]
    public partial class FuelControl : BaseReports.ReportsDE.BaseControl
    {
        public class reportFuel
        {
            string humanVeh;
            string numberVeh;
            string markingVeh;
            int identification;
            string location;
            string date;
            string time;
            double value;
            double begin;
            double end;
            string genertype;
            private int dataGpsId;

            public reportFuel(string human, string number, string marking, int ident, string location, string date,
                string time, double value, double begin, double end, string genertype, int dataGpsid)
            {
                this.humanVeh = human;
                this.numberVeh = number;
                this.markingVeh = marking;
                this.identification = ident;
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
            } // reportFuel

            public reportFuel(string location, string date, string time, double value, double begin, double end,
                string genertype, int dataGpsid)
            {
                this.location = location;
                this.date = date;
                this.time = time;
                this.value = Math.Round(value, 2);
                this.begin = Math.Round(begin, 2);
                this.end = Math.Round(end, 2);
                this.genertype = genertype;
                this.dataGpsId = dataGpsid;
            } // reportFuel

            public int DataGPS_ID
            {
                get { return dataGpsId; }
            }

            [Developer(0)]
            public string HumanVehicle
            {
                get { return humanVeh; }
            }

            [Developer(1)]
            public string NumberVehicle
            {
                get { return numberVeh; }
            }

            [Developer(2)]
            public string MarkingVehicle
            {
                get { return markingVeh; }
            }

            [Developer(3)]
            public int Identification
            {
                get { return identification; }
            }

            [Developer(4)]
            public string Location
            {
                get { return location; }
            }

            [Developer(5)]
            public string date_
            {
                get { return date; }
            }

            [Developer(6)]
            public string Times
            {
                get { return time; }
            }

            [Developer(7)]
            public double dValue
            {
                get { return value; }
            }

            [Developer(8)]
            public double beginValue
            {
                get { return begin; }
            }

            [Developer(9)]
            public double endValue
            {
                get { return end; }
            }

            [Developer(10)]
            public string GenerateType
            {
                get { return genertype; }
            }
        } // reportFuel

        /// <summary>
        /// Список, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства).
        /// </summary>
        private List<Fuel.Summary> _summariesData = new List<Fuel.Summary>();

        private FuelAddDlg fuelAddDlg;
        private atlantaDataSet.FuelReportRow f_row;
        private Fuel fuelAlg1;
        private Fuel fuelAlg2;
        private Fuel fuelAlg3;
        private Fuel fuelAlg4;
        private Fuel fuelAlg5;
        private bool begin_end;
        private bool flagStart = true;

        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;

        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ControlNavigator connav;
        ReportBase<reportFuel, TInfoDut> ReportingFuel;
        int numberChangedRow;
        private string NameReport = Resources.FuelCtrlCaption;
        private int globalCode = (int)AlgorithmType.FUEL1;
        bool isStartReport;
        ComboBoxEdit comboAlgorithm = null;

        /// <summary>
        /// Создает и инициализирует новый экземпляр класса BaseReports.FuelControl
        /// </summary>
        /// <param name="caption"></param>
        public FuelControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            SetVisibilityAlgoChoiser(true);
            EnabledExportExtendMenu();
            VisionPanel(fuelDataGridView, fuelDataGridControl, bar2);
            buildGraph = new BuildGraphs();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;
            isStartReport = true;

            fuelAlg1 = new Fuel(AlgorithmType.FUEL1);
            fuelAlg2 = new Fuel(AlgorithmType.FUEL1_AGREGAT);
            fuelAlg3 = new Fuel(AlgorithmType.FUEL2_AGREGAT);
            fuelAlg4 = new Fuel(AlgorithmType.FUEL3_AGREGAT);
            fuelAlg5 = new Fuel(AlgorithmType.FUEL_AGREGAT_ALL);
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(fuelAlg1);
            AddAlgorithm(fuelAlg2);
            AddAlgorithm(fuelAlg3);
            AddAlgorithm(fuelAlg4);
            AddAlgorithm(fuelAlg5);

            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "FuelReport";

            fuelAddDlg = new FuelAddDlg(graph);

            fuelDataGridControl.UseEmbeddedNavigator = true;
            barButtonNavigator.Caption = Resources.ResButtonNavigatorHide;
            connav = fuelDataGridControl.EmbeddedNavigator;
            connav.Buttons.BeginUpdate();
            //try
            //{
            connav.Buttons.Append.Visible = true;
            connav.Buttons.Append.Enabled = true;
            connav.Buttons.Remove.Visible = true;
            connav.Buttons.Remove.Enabled = true;
            //connav.Buttons.Edit.Visible = true;
            //connav.Buttons.Edit.Enabled = true;
            //connav.Buttons.EndEdit.Visible = true;
            //connav.Buttons.EndEdit.Enabled = true;
            //}
            //finally
            //{
            connav.Buttons.EndUpdate();
            //}

            //fuelDataGridView.CustomColumnDisplayText += new CustomColumnDisplayTextEventHandler( fuelDataGridView_CellFormatting );
            fuelDataGridView.RowCellClick += new RowCellClickEventHandler(fuelDataGridView_CellMouseDoubleClick);
            fuelDataGridView.ValidatingEditor +=
                new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(
                    fuelDataGridView_CellValidating);
            fuelDataGridView.CellValueChanged += new CellValueChangedEventHandler(fuelDataGridView_CellEndEdit);
            connav.ButtonClick += new NavigatorButtonClickEventHandler(connav_ButtonClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuel =
                new ReportBase<reportFuel, TInfoDut>(Controls, compositeLink1, fuelDataGridView,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevFuelControl

        public override string Caption
        {
            get { return Resources.FuelCtrlCaption; }
        }

        private int ContainsData(int id, int algo)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == id && _summariesData[i].typeAlgo == algo)
                {
                        return i;
                } // if
            } // for

            return -1;
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is Fuel)
            {
                if (e is FuelEventArgs)
                {
                    FuelEventArgs args = (FuelEventArgs) e;
                    int index = ContainsData(args.Id, args.IdAlgo);

                    if (index != -1)
                    {
                        _summariesData.RemoveAt(index);
                    }

                    _summariesData.Add(args.Summary);
                } // if
            } // if
        } // Algorithm_Action

        /// <summary>
        /// Построение отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                flagStart = false;
                SetStopButton();
                BeginReport();
                _stopRun = false;
                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;

                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                if (curMrow == null)
                {
                    return;
                }

                Select(curMrow);
                SetStartButton();
                //EnableButton();
                isStartReport = true;
            }
            else
            {
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();

                SetStartButton();
            }

            flagStart = true;
        } // bbiStart_ItemClick

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            DisableButton();
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    break;
            } // foreach

            //EnableButton();
            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        private bool ContainsID(int id)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == id)
                {
                    return true;
                }
            }

            return false;
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                int index = 0;
                if (comboAlgorithm != null)
                {
                    index = comboAlgorithm.SelectedIndex;
                }
              
                if (ContainsID(mobitel.Mobitel_ID))
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        fuelDataGridControl.DataSource = atlantaDataSetBindingSource;

                        if (index == 0)
                        {
                            atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " +
                                                                 "TypeAlgorythm=" + (int) AlgorithmType.FUEL1;
                        }
                        else if (index == 1)
                        {
                            atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " +
                                                                 "TypeAlgorythm=" + (int) AlgorithmType.FUEL1_AGREGAT;
                        }
                        else if (index == 2)
                        {
                            atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " +
                                                                 "TypeAlgorythm=" + (int)AlgorithmType.FUEL2_AGREGAT;
                        }
                        else if (index == 3)
                        {
                            atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " +
                                                                 "TypeAlgorythm=" + (int)AlgorithmType.FUEL3_AGREGAT;
                        }
                        else if (index == 4)
                        {
                            atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " +
                                                                 "TypeAlgorythm=" + (int)AlgorithmType.FUEL_AGREGAT_ALL;
                        }
                    }
                    // UpdateStatusLine();
                        EnableButton();

                        if (CurrentActivateReport == NameReport)
                        {
                            if (index == 0)
                            {
                                SelectGraphic(curMrow, (int)AlgorithmType.FUEL1);
                            }
                            else if (index == 1)
                            {
                                SelectGraphic(curMrow, (int)AlgorithmType.FUEL1_AGREGAT);
                            }
                            else if (index == 2)
                            {
                                SelectGraphic(curMrow, (int)AlgorithmType.FUEL2_AGREGAT);
                            }
                            else if (index == 3)
                            {
                                SelectGraphic(curMrow, (int)AlgorithmType.FUEL3_AGREGAT);
                            }
                            else if (index == 4)
                            {
                                SelectGraphic(curMrow, (int)AlgorithmType.FUEL_AGREGAT_ALL);
                            }
                        }

                    //EnableEditButton();
                    UpdateStatusLine(globalCode);
                    bbiPrintReport.Enabled = true;
                }
                else
                {
                    DisableButton();
                    //DisableEditButton();
                    ClearStatusLine();
                }
            } // if
        } // Select

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel, int type)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);

                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " + "TypeAlgorythm=" + type;

                try
                {
                    ReportsControl.GraphClearSeries();

                    if (type == (int)AlgorithmType.FUEL1)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1);
                    }
                    else if (type == (int)AlgorithmType.FUEL1_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL2_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL2_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL3_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL3_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL_AGREGAT_ALL);
                    }
                    else
                    {
                        throw new NotImplementedException("Unknown type algorythm!");
                    }
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in FuelControl: {0}", ex.Message);
                    XtraMessageBox.Show(message, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // if
        } // SelectGraphic

        /// <summary>
        /// Построение графика.
        /// </summary>
        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphFuel(algoParam, Graph, dataset, mobitel_row);
            buildGraph.AddGraphVoltage(Graph, dataset, mobitel_row); // add 04.07.2017 aketner
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            List<Marker> markers = new List<Marker>();
            Int32[] index = fuelDataGridView.GetSelectedRows();
            for (int i = 0; i < index.Length; i++)
            {
                atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index[i]]).Row;

                if (f_row["time_"] != DBNull.Value)
                {
                    double value = f_row.dValue;
                    string message = String.Format("{0}: {1:N2} {2}.",
                        value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                        value, Resources.LitreAbbreviation);
                    PointLatLng location = new PointLatLng(f_row.Lat, f_row.Lon);
                    markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                } // if
            } // for

            ReportsControl.OnMarkersShowNeeded(markers);
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        }

        private static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.FuelReportRow f_row, out string val,
            out Color color)
        {
            val = String.Format("{0:#.#}{1}", f_row.dValue, Resources.LitreAbbreviation);

            if (f_row.dValue > 0)
            {
                val = String.Format("{0} {1}", Resources.Fuelling, val);
                color = Color.Green;
            }
            else
            {
                val = String.Format("{0} {1}", Resources.FuelLarceny, val);
                color = Color.Red;
            }
        }

        void UpdateZGraph()
        {
            // Сбрасываем все метки
            // Graph.ClearGraphZoom(false)
            Graph.ClearLabel();
            Int32[] index = fuelDataGridView.GetSelectedRows();

            for (int i = 0; i < index.Length; i++)
            {
                if (index[i] >= 0)
                {
                    int indx = fuelDataGridView.GetDataSourceRowIndex(index[i]);
                    atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                        ((DataRowView) atlantaDataSetBindingSource.List[indx]).Row;

                    if (f_row["time_"] != DBNull.Value)
                    {
                        string val;
                        Color color;
                        ShowGraphToolStripButton_ClickExtracted(f_row, out val, out color);

                        if (f_row.pointValue <= 0.0)
                        {
                            Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.beginValue, val, color);
                            return;
                        }

                        Graph.AddLabel(f_row.time_, f_row.beginValue, f_row.pointValue, val, color);
                    } // if
                }
            } // for

            // Graph.ClearGraphZoom(true); AddGraphInclinometrX
            ReportsControl.ShowGraph(curMrow);
            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
            Graph.SellectZoom();
        } // UpdateZGraph

        private void fuelDataGridView_CellFormatting(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "dValue")
            {
                if (e.Value is DBNull || e.Value == null)
                {
                    return;
                }

                if ((double) e.Value > 0.0)
                {
                    e.Column.AppearanceCell.ForeColor = Color.Green;
                }
                else
                {
                    e.Column.AppearanceCell.ForeColor = Color.Red;
                }
            } // if

        } // fuelDataGridView_CellFormatting

        private void fuelDataGridView_CellMouseDoubleClick(object sender, RowCellClickEventArgs e)
        {
            try
            {
                ReportsControl.OnClearMapObjectsNeeded();

                if (e.RowHandle < 0)
                    return;

                DateTime time_;
                // Дата время
                object tm = fuelDataGridView.GetRowCellValue(e.RowHandle, e.Column);
                if (!DateTime.TryParse(Convert.ToString(tm), out time_))
                    // fuelDataGridView.CurrentRow.ed
                    return;

                int index = fuelDataGridView.GetDataSourceRowIndex(e.RowHandle);

                List<Marker> markers = new List<Marker>();
                atlantaDataSet.FuelReportRow f_row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[index]).Row;
                double value = f_row.dValue;
                string message = String.Format("{0}: {1:N2} {2}.",
                    value > 0 ? Resources.Fuelling : Resources.FuelLarceny,
                    value, Resources.LitreAbbreviation);
                //PointLatLng location = new PointLatLng( f_row.dataviewRow.Lat, f_row.dataviewRow.Lon );
                PointLatLng location = Algorithm.GpsDatas.First(gps => gps.Id == f_row.DataGPS_ID).LatLng;
                markers.Add(new Marker(MarkerType.Fueling, f_row.mobitel_id, location, message, ""));
                ReportsControl.OnMarkersShowNeeded(markers);
                UpdateZGraph();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Fuel Control", MessageBoxButtons.OK);
                return;
            }
        } // fuelDataGridView_CellMouseDoubleClick

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            fuelAlg1.ClearAtlantaFuelReport();
            fuelAlg2.ClearAtlantaFuelReport();
            fuelAlg3.ClearAtlantaFuelReport();
            fuelAlg4.ClearAtlantaFuelReport();
            
            dataset.FuelReport.Clear();
            dataset.FuelValue.Clear();
            _summariesData.Clear();
            ClearStatusLine();
        }

        /// <summary>
        /// Осуществляет валидацию вводимых пользователем данных
        /// </summary>
        private void fuelDataGridView_CellValidating(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if ((fuelDataGridView.FocusedColumn.Name == "_endValue") ||
                (fuelDataGridView.FocusedColumn.Name == "_beginValue"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.FuelCtrlFuellingValueError, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    double new_double;
                    if (!Double.TryParse(e.Value.ToString(), out new_double))
                    {
                        XtraMessageBox.Show(Resources.FuelCtrlFuellingValueError, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // if
            else if ((fuelDataGridView.FocusedColumn.Name == "_time"))
            {
                if (e.Value == null ||
                    String.IsNullOrEmpty(e.Value.ToString()))
                {
                    XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                    e.Valid = false;
                }
                else
                {
                    DateTime new_time;
                    if (!DateTime.TryParse(e.Value.ToString(), out new_time))
                    {
                        XtraMessageBox.Show(Resources.IncorrectDateFormat, Resources.DataError);
                        e.Valid = false;
                    }
                } // else
            } // else if
        } // fuelDataGridView_CellValidating

        /// <summary>
        /// Осуществляет пересчет в редактируемой строке отчета и итогов
        /// </summary>
        private void fuelDataGridView_CellEndEdit(object sender, CellValueChangedEventArgs e)
        {
            string name = e.Column.Name; // Название редактируемой колонки

            if (name == "_time")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                string time = row.time_.ToString("dd.MM.yyyy HH:mm:ss");

                fuelDataGridView.SetRowCellValue(e.RowHandle, "time_", time);
            }

            if (name == "_endValue" || name == "_beginValue")
            {
                atlantaDataSet.FuelReportRow row = (atlantaDataSet.FuelReportRow)
                    ((DataRowView) atlantaDataSetBindingSource.List[e.RowHandle]).Row;

                Fuel.Summary s = getSummary(curMrow.Mobitel_ID, row.TypeAlgorythm);

                if (row.dValue > 0)
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                row.dValue = row.endValue - row.beginValue;

                if (row.dValue > 0)
                {
                    s.Fueling += row.dValue;
                    s.Total += row.dValue;
                }
                else
                {
                    s.Discharge += row.dValue;
                }

                s.Rate = s.Total * 100 / curMrow.path;

                RemoveSummary(curMrow.Mobitel_ID, row.TypeAlgorythm);
                AddSummary(s);
            } // if

            atlantaDataSetBindingSource.ResumeBinding();
            atlantaDataSetBindingSource.ResetBindings(false);
            atlantaDataSetBindingSource.EndEdit();
            graph.RemoveMouseEventHandler();
            UpdateStatusLine(globalCode);
            ReportsControl.OnGraphShowNeeded();
            UpdateZGraph();
        }

        private void AddSummary(Fuel.Summary summary)
        {
            _summariesData.Add(summary);
        }

        private void RemoveSummary(int mobitelId, int type)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == mobitelId && _summariesData[i].typeAlgo == type)
                {
                    _summariesData.RemoveAt(i);
                }
            }
        }

        private Fuel.Summary getSummary(int mobitelId, int type)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == mobitelId && _summariesData[i].typeAlgo == type)
                {
                    return _summariesData[i];
                }
            }

            return new Fuel.Summary();
        }

// fuelDataGridView_CellEndEdit

        private void fuelDataGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {

            if (e.RowHandle >= 0)
            {
                GridView view = sender as GridView;
                if (e.Column.FieldName == "dValue")
                {
                    double dbValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["dValue"]));
                    if (dbValue > 0.0)
                    {
                        e.Appearance.ForeColor = Color.Green;
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }

                    Int64 idDataGps = 0;
                    try
                    {
                        idDataGps = Convert.ToInt64(view.GetRowCellValue(e.RowHandle, view.Columns["DataGPS_ID"]));
                    }
                    catch (Exception ex)
                    {
                        idDataGps = 0;
                    }

                    if (idDataGps > 0)
                    {
                        if (buildGraph.IsInclinometerDangered(dataset, curMrow, idDataGps))
                        {
                            e.Appearance.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Добавляет к отчету новую строку
        /// </summary>
        protected void bindingNavigatorAddNewItem_Click()
        {
            ReportsControl.OnGraphShowNeeded();

            f_row = (atlantaDataSet.FuelReportRow)
                ((DataRowView) atlantaDataSetBindingSource.AddNew()).Row;

            f_row["GenerateType"] = "manual";

            if(globalCode == 7)
            {
                f_row["TypeAlgorythm"] = (int)AlgorithmType.FUEL1;
                f_row["mobitel_id"] = curMrow.Mobitel_ID;
            }
            else if (globalCode == 31)
            {
                f_row["TypeAlgorythm"] = (int)AlgorithmType.FUEL1_AGREGAT;
                f_row["mobitel_id"] = curMrow.Mobitel_ID;
            }
            else if (globalCode == 32)
            {
                f_row["TypeAlgorythm"] = (int)AlgorithmType.FUEL2_AGREGAT;
                f_row["mobitel_id"] = curMrow.Mobitel_ID;
            }
            else if (globalCode == 33)
            {
                f_row["TypeAlgorythm"] = (int)AlgorithmType.FUEL3_AGREGAT;
                f_row["mobitel_id"] = curMrow.Mobitel_ID;
            }
            else if (globalCode == 34)
            {
                f_row["TypeAlgorythm"] = (int)AlgorithmType.FUEL_AGREGAT_ALL;
                f_row["mobitel_id"] = curMrow.Mobitel_ID;
            }

            numberChangedRow = fuelDataGridView.FocusedRowHandle;

            Graph.Action += new ActionEventHandler(ZGraphControl_Action_Fuel1);

            graph.AddMouseEventHandler();
            XtraMessageBox.Show(Resources.FuelCtrlAddFuellingStartText, Resources.FuelCtrlAddFuellingCaption);
        } // bindingNavigatorAddNewItem_Click

        /// <summary>
        /// Удаляет из отчета выделенную строку
        /// </summary>
        protected void bindingNavigatorDeleteItem_Click()
        {
            atlantaDataSet.FuelReportRow row =
                (atlantaDataSet.FuelReportRow) ((DataRowView) atlantaDataSetBindingSource.Current).Row;
            Fuel.Summary s = getSummary(curMrow.Mobitel_ID, row.TypeAlgorythm);

            // Необходимо оставить последнюю строку в отчете, чтобы сводные отчеты
            // не строили данный отчет заново
            if (curMrow.GetFuelerReportRows().Length == 1)
            {
                row.beginValue = 0;
                row.endValue = 0;
                row.dValue = 0;
                s.Fueling = 0;
                s.Discharge = 0;
                s.Total = s.Before - s.After;
                s.Rate = s.Total*100/curMrow.path;
                RemoveSummary(curMrow.Mobitel_ID, row.TypeAlgorythm);
                AddSummary(s);
                UpdateStatusLine(globalCode);
                XtraMessageBox.Show(Resources.FuelCtrlDeleteError, Resources.Warning);
            }
            else
            {
                if (row.dValue > 0)
                {
                    s.Fueling -= row.dValue;
                    s.Total -= row.dValue;
                    s.Rate = s.Total*100/curMrow.path;
                }
                else
                {
                    s.Discharge -= row.dValue;
                }

                RemoveSummary(curMrow.Mobitel_ID, row.TypeAlgorythm);
                AddSummary(s);
                UpdateStatusLine(globalCode);
                row.Delete();
            } // else
        } // bindingNavigatorDeleteItem_Click

        private void connav_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Append)
            {
                bindingNavigatorAddNewItem_Click();
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                bindingNavigatorDeleteItem_Click();
            }

            e.Handled = true;
        }

        /// <summary>
        /// Обрабатывает выбор точек на графике топлива при добавлении новой строки к отчету
        /// </summary>
        private void ZGraphControl_Action_Fuel1(object sender, ActionCancelEventArgs ev)
        {
            if (f_row == null)
                return;

            int typeAlgorythm = ev.Point.Type;

            if (!begin_end)
            {
                GpsData gpsData = DataSetManager.GetDataGpsArray(curMrow).First(gps => gps.Time == ev.Point.time);
                f_row.beginValue = ev.Point.value;
                f_row.time_ = ev.Point.time;
                f_row.date_ = ev.Point.time.ToShortDateString();
                f_row.Times = ev.Point.time.ToLongTimeString();
                f_row.DataGPS_ID = gpsData.Id;
                f_row.Location = Algorithm.FindLocation(gpsData.LatLng);
                f_row.mobitel_id = gpsData.Mobitel;
                f_row.sensor_id = 0;
                f_row.TypeAlgorythm = globalCode;

                foreach (atlantaDataSet.sensorsRow s_row in curMrow.GetsensorsRows())
                {
                    if (s_row != null)
                    {
                        foreach (atlantaDataSet.relationalgorithmsRow ra_row in s_row.GetrelationalgorithmsRows())
                        {
                            if ((AlgorithmType) ra_row.AlgorithmID == (AlgorithmType)typeAlgorythm)
                            {
                                f_row.sensor_id = s_row.id;
                                f_row.TypeAlgorythm = typeAlgorythm;
                            } // if
                        } // foreach
                    } // if
                } // foreach

                begin_end = true;
                XtraMessageBox.Show(Resources.FuelCtrlAddFuellingFinishText, Resources.FuelCtrlAddFuellingCaption);
            } // if
            else
            {
                f_row.endValue = ev.Point.value;
                f_row.dValue = f_row.endValue - f_row.beginValue;
                Fuel.Summary s = getSummary(curMrow.Mobitel_ID, f_row.TypeAlgorythm);
                if (f_row.dValue > 0)
                {
                    s.Fueling += f_row.dValue;
                    s.Total += f_row.dValue;
                    s.Rate = s.Total * 100 / curMrow.path;
                } // if
                else
                {
                    s.Discharge += f_row.dValue;
                }

                RemoveSummary(curMrow.Mobitel_ID, f_row.TypeAlgorythm);
                AddSummary(s);

                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.ResetBindings(false);
                atlantaDataSetBindingSource.EndEdit();
                graph.RemoveMouseEventHandler();
                Graph.Action -= ZGraphControl_Action_Fuel1;
                begin_end = false;
                ReportsControl.OnGraphShowNeeded();
                UpdateZGraph();
            } // else
        } // ZGraphControl_Action_Fuel1

        /// <summary>
        /// Обнуляет итоговые показатели статусной строки
        /// </summary>
        private void ClearStatusLine()
        {
            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;
        }

        Fuel.Summary s0;
        Fuel.Summary s1;
        Fuel.Summary s2;
        Fuel.Summary s3;
        Fuel.Summary s4;

        /// <summary>
        /// Обновляет итоговые показатели в статусной строке
        /// </summary>
        private void UpdateStatusLine(int typecode)
        {
            //if (isStartReport)
            {
                if (curMrow == null)
                    return;

                s0 = getSummary(curMrow.Mobitel_ID, (int)AlgorithmType.FUEL1);
                s1 = getSummary(curMrow.Mobitel_ID, (int)AlgorithmType.FUEL1_AGREGAT);
                s2 = getSummary(curMrow.Mobitel_ID, (int)AlgorithmType.FUEL2_AGREGAT);
                s3 = getSummary(curMrow.Mobitel_ID, (int)AlgorithmType.FUEL3_AGREGAT);
                s4 = getSummary(curMrow.Mobitel_ID, (int)AlgorithmType.FUEL_AGREGAT_ALL);
                //isStartReport = false;
            }

            if (typecode == (int) AlgorithmType.FUEL1)
            {
                _beforeLbl.Caption = Resources.BeforeLbl + " " + s0.Before.ToString("N0");
                _afterLbl.Caption = Resources.AfterLbl + " " + s0.After.ToString("N0");
                _fuelingLbl.Caption = Resources.FuelLbl + " " + s0.Fueling.ToString("N0");
                _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s0.Discharge).ToString("N0");
                _totalLbl.Caption = Resources.TotalLbl + " " + s0.Total.ToString("N0");
                _rateLbl.Caption = Resources.RateLbl + " " + s0.Rate.ToString("N2");
                _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s0.AvgSpeed.ToString("N2");
            }
            else if (typecode == (int)AlgorithmType.FUEL1_AGREGAT)
            {
                _beforeLbl.Caption = Resources.BeforeLbl + " " + s1.Before.ToString("N0");
                _afterLbl.Caption = Resources.AfterLbl + " " + s1.After.ToString("N0");
                _fuelingLbl.Caption = Resources.FuelLbl + " " + s1.Fueling.ToString("N0");
                _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s1.Discharge).ToString("N0");
                _totalLbl.Caption = Resources.TotalLbl + " " + s1.Total.ToString("N0");
                _rateLbl.Caption = Resources.RateLbl + " " + s1.Rate.ToString("N2");
                _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s1.AvgSpeed.ToString("N2");
            }
            else if (typecode == (int)AlgorithmType.FUEL2_AGREGAT)
            {
                _beforeLbl.Caption = Resources.BeforeLbl + " " + s2.Before.ToString("N0");
                _afterLbl.Caption = Resources.AfterLbl + " " + s2.After.ToString("N0");
                _fuelingLbl.Caption = Resources.FuelLbl + " " + s2.Fueling.ToString("N0");
                _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s2.Discharge).ToString("N0");
                _totalLbl.Caption = Resources.TotalLbl + " " + s2.Total.ToString("N0");
                _rateLbl.Caption = Resources.RateLbl + " " + s2.Rate.ToString("N2");
                _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s2.AvgSpeed.ToString("N2");
            }
            else if (typecode == (int)AlgorithmType.FUEL3_AGREGAT)
            {
                _beforeLbl.Caption = Resources.BeforeLbl + " " + s3.Before.ToString("N0");
                _afterLbl.Caption = Resources.AfterLbl + " " + s3.After.ToString("N0");
                _fuelingLbl.Caption = Resources.FuelLbl + " " + s3.Fueling.ToString("N0");
                _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s3.Discharge).ToString("N0");
                _totalLbl.Caption = Resources.TotalLbl + " " + s3.Total.ToString("N0");
                _rateLbl.Caption = Resources.RateLbl + " " + s3.Rate.ToString("N2");
                _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s3.AvgSpeed.ToString("N2");
            }
            else if (typecode == (int) AlgorithmType.FUEL_AGREGAT_ALL)
            {
                _beforeLbl.Caption = Resources.BeforeLbl + " " + s4.Before.ToString("N0");
                _afterLbl.Caption = Resources.AfterLbl + " " + s4.After.ToString("N0");
                _fuelingLbl.Caption = Resources.FuelLbl + " " + s4.Fueling.ToString("N0");
                _dischargeLbl.Caption = Resources.DischargeLbl + " " + delsign(s4.Discharge).ToString("N0");
                _totalLbl.Caption = Resources.TotalLbl + " " + s4.Total.ToString("N0");
                _rateLbl.Caption = Resources.RateLbl + " " + s4.Rate.ToString("N2");
                _avgSpeedLbl.Caption = Resources.AvgSpeedLbl + " " + s4.AvgSpeed.ToString("N2");
            }
        }

        // формирование расширенного отчета по всем выбранным машинам
        protected override void ExportAllDevToReportExtend()
        {
            colGenerateType.Visible = false;
            colHuman.Visible = true;
            colIdentification.Visible = true;
            colMarking.Visible = true;
            colNumberVeh.Visible = true;

            colHuman.VisibleIndex = 0;
            colNumberVeh.VisibleIndex = 1;
            colMarking.VisibleIndex = 2;
            colIdentification.VisibleIndex = 3;
            _location.VisibleIndex = 4;
            coldate.VisibleIndex = 5;
            coltime.VisibleIndex = 6;
            _quantity.VisibleIndex = 7;
            _beginValue.VisibleIndex = 8;
            _endValue.VisibleIndex = 9;

            ReportingFuel.CreateBindDataList();
            ReportingFuel.CreateElementReport();

            foreach (int mobitelId in getSummaryKeys(globalCode))
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                //VehicleInfo info = new VehicleInfo(mobitel);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль

                if (info.DriverName == " ")
                {
                    tfinfo.infoDriverName = "------"; //+ Водитель
                }
                else
                {
                    tfinfo.infoDriverName = info.DriverFullName;
                }

                Fuel.Summary _ss = getSummary(mobitelId, globalCode);

                tfinfo.totalFuel = _ss.Before; // +Литров топлива в начале
                tfinfo.FuelStops = _ss.After; // Литров топлива в конце
                tfinfo.Fueling = _ss.Fueling; // Заправлено литров всего
                tfinfo.totalFuelSub = _ss.Discharge; // +Слито литров всего
                tfinfo.totalFuelAdd = _ss.Total; // Всего литров израсходовано автомобилем
                tfinfo.MiddleFuel = _ss.Rate; //+ Расход топлива на 100 км пробега
                tfinfo.MotionTreavelTime = _ss.AvgSpeed; // +Средняя скорость движения
                tfinfo.registerNumber = info.RegistrationNumber;
                tfinfo.markVehicle = info.CarMaker;
                tfinfo.Identification = info.Identificator;

                tfinfo.BreakLeft = false;
                tfinfo.BreakMeedle = false;
                tfinfo.BreakRight = false;

                ReportingFuel.AddInfoStructToList(tfinfo);

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId + " and " + "TypeAlgorythm=" + globalCode, "id ASC"))
                {
                    string humanVeh = tfinfo.infoDriverName;
                    string numberVehicle = tfinfo.registerNumber;
                    string markingVehicle = tfinfo.markVehicle;
                    int identificator = tfinfo.Identification;
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;

                    ReportingFuel.AddDataToBindList(new reportFuel(humanVeh, numberVehicle, markingVehicle,
                        identificator, location, date, time, value,
                        begin, end, genertype, 0));
                } // foreach 1
            } // foreach 2

            ReportingFuel.CreateAndShowReport();
            ReportingFuel.DeleteData();

            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;
            colGenerateType.Visible = true;
        }

        private IEnumerable<int> getSummaryKeys(int type)
        {
            List<int> keys = new List<int>();
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if(type == _summariesData[i].typeAlgo)
                    keys.Add(_summariesData[i].id); 
            }

            return keys;
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            IEnumerable<int> keys = getSummaryKeys(globalCode);

            if (keys.Count() == 0)
                return;

            foreach (int mobitelId in keys)
            {
                TInfoDut tfinfo = new TInfoDut();

                tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
                tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

                Fuel.Summary _ss = getSummary(mobitelId, globalCode);

                tfinfo.infoVehicle = info.Info; //+ Автомобиль
                tfinfo.infoDriverName = info.DriverFullName; //+ Водитель
                tfinfo.totalFuel = _ss.Before; // +Литров топлива в начале
                tfinfo.FuelStops = _ss.After; // Литров топлива в конце
                tfinfo.Fueling = _ss.Fueling; // Заправлено литров всего
                tfinfo.totalFuelSub = _ss.Discharge; // +Слито литров всего
                tfinfo.totalFuelAdd = _ss.Total; // Всего литров израсходовано автомобилем
                tfinfo.MiddleFuel = _ss.Rate; //+ Расход топлива на 100 км пробега
                tfinfo.MotionTreavelTime = _ss.AvgSpeed; // +Средняя скорость движения

                tfinfo.BreakLeft = true;
                tfinfo.BreakMeedle = true;
                tfinfo.BreakRight = true;

                ReportingFuel.AddInfoStructToList(tfinfo);
                ReportingFuel.CreateBindDataList();

                foreach (
                    atlantaDataSet.FuelReportRow row in dataset.FuelReport.Select("mobitel_id=" + mobitelId + " and " + "TypeAlgorythm=" + globalCode, "id ASC"))
                {
                    string location = row.Location;
                    string date = row.date_;
                    string time = row.Times;
                    double value = row.dValue;
                    double begin = row.beginValue;
                    double end = row.endValue;
                    string genertype = row.GenerateType;

                    ReportingFuel.AddDataToBindList(new reportFuel(location, date, time, value, begin, end, genertype, 0));
                } // foreach 1

                ReportingFuel.CreateElementReport();
            } // foreach 2

            ReportingFuel.CreateAndShowReport();
            ReportingFuel.DeleteData();
        } // ExportAllDevToReport

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            colHuman.Visible = false;
            colIdentification.Visible = false;
            colMarking.Visible = false;
            colNumberVeh.Visible = false;

            TInfoDut tfinfo = new TInfoDut();

            tfinfo.periodBeging = Algorithm.Period.Begin; // Начало периода
            tfinfo.periodEnd = Algorithm.Period.End; // Конец периода

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(curMrow.Mobitel_ID);
            VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);

            Fuel.Summary _ss = getSummary(curMrow.Mobitel_ID, globalCode);

            tfinfo.infoVehicle = info.Info; // Автомобиль
            tfinfo.infoDriverName = info.DriverFullName; // Водитель
            tfinfo.totalFuel = _ss.Before; // Литров топлива в начале
            tfinfo.FuelStops = _ss.After; // Литров топлива в конце
            tfinfo.Fueling = _ss.Fueling; // Заправлено литров всего
            tfinfo.totalFuelSub = _ss.Discharge; // Слито литров всего
            tfinfo.totalFuelAdd = _ss.Total; // Всего литров израсходовано автомобилем
            tfinfo.MiddleFuel = _ss.Rate; // Расход топлива на 100 км пробега
            tfinfo.MotionTreavelTime = _ss.AvgSpeed; // Средняя скорость движения

            tfinfo.BreakLeft = true;
            tfinfo.BreakMeedle = true;
            tfinfo.BreakRight = true;

            ReportingFuel.AddInfoStructToList(tfinfo); /* формируем заголовки таблиц отчета */
            ReportingFuel.CreateAndShowReport(fuelDataGridControl);
        } // ExportToExcelDevExpress

        // Формирование верхнего колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportFueler, e);
            TInfoDut info = ReportingFuel.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        protected double delsign(double s)
        {
            if (s < 0.0)
                return -s;

            return s;
        }

        /* функция для формирования верхней/средней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakMeedle)
            {
                ReportingFuel.SetRectangleBrckUP(450, 10, 200, 70);
                return (Resources.BeginFueling + ": " + info.totalFuel.ToString("N2") + "\n" +
                        Resources.FuelEnd + ": " + info.FuelStops.ToString("N2") + "\n" +
                        Resources.FuelLbl + " " + info.Fueling.ToString("N2") + "\n" +
                        Resources.DischargeLbl + " " + delsign(info.totalFuelSub).ToString("N2"));
            }
            else
            {
                return "";
            }
        }

        /* функция для формирования левой части заголовка отчета */

        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakLeft)
            {
                return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                        Resources.Driver + ": " + info.infoDriverName);
            }
            else
            {
                return "";
            }
        } // GetStringBreackLeft

        /* функция для формирования правой части заголовка отчета */

        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuel.GetInfoStructure;
            if (info.BreakRight)
            {
                ReportingFuel.SetRectangleBrckRight(900, 10, 200, 70);
                return (Resources.TotalLbl + " " + info.totalFuelAdd.ToString("N2") + "\n" +
                        Resources.RateLbl + " " + info.MiddleFuel.ToString("N2") + "\n" +
                        Resources.AvgSpeedLbl + " " + info.MotionTreavelTime.ToString("N2"));
            }

            return "";
        }

        protected void comboChoiserData(ComboBoxEdit combo)
        {
            if (curMrow == null)
                return;

            int index = combo.SelectedIndex;
            int code = -1;

            if (index == 0)
            {
                code = (int)AlgorithmType.FUEL1;
                globalCode = code;
                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 1)
            {
                code = (int)AlgorithmType.FUEL1_AGREGAT;
                globalCode = code;
                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 2)
            {
                code = (int)AlgorithmType.FUEL2_AGREGAT;
                globalCode = code;
                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 3)
            {
                code = (int)AlgorithmType.FUEL3_AGREGAT;
                globalCode = code;
                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 4)
            {
                code = (int)AlgorithmType.FUEL_AGREGAT_ALL;
                globalCode = code;
                UpdateStatusLine(globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + (int)AlgorithmType.FUEL_AGREGAT_ALL;
                SelectGraphic(curMrow, code);
            }
            else
            {
                XtraMessageBox.Show("Неизвестный алгоритм", "Выбор алгоритма", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        protected override void comboAlgoChoiser_ItemClick(object sender, EventArgs e)
        {
            ComboBoxEdit combo = (ComboBoxEdit) sender;
            comboAlgorithm = combo;
            comboChoiserData(combo);
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(fuelDataGridView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(fuelDataGridView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(fuelDataGridControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar2);
        }

        public void Localization()
        {
            _location.Caption = Resources.Location;
            _location.ToolTip = Resources.Location;

            coldate.Caption = Resources.DateTime;
            coldate.ToolTip = Resources.FuellingDateTime;

            _quantity.Caption = Resources.FuellingValue;
            _quantity.ToolTip = Resources.FuellingValueHint;

            _beginValue.Caption = Resources.FuellLevelBeforeFuelling;
            _beginValue.ToolTip = Resources.FuellLevelBeforeFuellingHint;

            _endValue.Caption = Resources.FuellLevelAfterFuelling;
            _endValue.ToolTip = Resources.FuellLevelAfterFuellingHint;

            colHuman.Caption = Resources.ColHumanVehicle;
            colHuman.ToolTip = Resources.ColHumanVehicleTip;
            colNumberVeh.Caption = Resources.ColNumVehicle;
            colNumberVeh.ToolTip = Resources.ColNumVehicleTip;
            colMarking.Caption = Resources.ColMarking;
            colMarking.ToolTip = Resources.ColMarkingTip;
            colIdentification.Caption = Resources.ColIdent;
            colIdentification.ToolTip = Resources.ColIdentTip;
            coldate.Caption = Resources.ColDate;
            coldate.ToolTip = Resources.ColDateTip;
            coltime.Caption = Resources.ColTime;
            coltime.ToolTip = Resources.ColTimeTip;

            _beforeLbl.Caption = Resources.BeforeLbl;
            _afterLbl.Caption = Resources.AfterLbl;
            _fuelingLbl.Caption = Resources.FuelLbl;
            _dischargeLbl.Caption = Resources.DischargeLbl;
            _totalLbl.Caption = Resources.TotalLbl;
            _rateLbl.Caption = Resources.RateLbl;
            _avgSpeedLbl.Caption = Resources.AvgSpeedLbl;

            colGenerateType.Caption = Resources.GenerateType;
            colGenerateType.ToolTip = Resources.GenerateTypeTip;
            //_time.Caption = Resources.Times;
            //_quantity.Caption = Resources.Quantity;
            //_beginValue.Caption = Resources.BeginValue;
            //_endValue.Caption = Resources.EndValue;
        }

        private void FuelControl_Load(object sender, EventArgs e)
        {
            // to do
        }

        // Класс разработан для исправления вывода названий в заголовки колонок таблицы
        // во время генерации отчета
        [AttributeUsage(AttributeTargets.All)]
        public class DeveloperAttribute : DisplayNameAttribute
        {
            //Private fields.
            private string colHuman;
            private string colNumberVeh;
            private string colMarking;
            private string colIdentification;
            private string _location;
            private string coldate;
            private string coltime;
            private string _quantity;
            private string _beginvalue;
            private string _endvalue;
            private string _colgener;
            private int val;

            //This constructor defines two required parameters: name and level.
            public DeveloperAttribute(int val)
            {
                this.val = val;
                this.colHuman = Resources.ColHumanVehicle;
                this.colNumberVeh = Resources.ColNumVehicle;
                this.colMarking = Resources.ColMarking;
                this.colIdentification = Resources.ColIdent;
                this._location = Resources.Location;
                this.coldate = Resources.ColDate;
                this.coltime = Resources.ColTime;
                this._quantity = Resources.FuellingValue;
                this._beginvalue = Resources.FuellLevelBeforeFuelling;
                this._endvalue = Resources.FuellLevelAfterFuelling;
                this._colgener = Resources.GenerateType;
            }

            //Define DisplayName property
            public override string DisplayName
            {
                get
                {
                    switch (val)
                    {
                        case 0:
                            return colHuman;
                        case 1:
                            return colNumberVeh;
                        case 2:
                            return colMarking;
                        case 3:
                            return colIdentification;
                        case 4:
                            return this._location;
                        case 5:
                            return coldate;
                        case 6:
                            return coltime;
                        case 7:
                            return this._quantity;
                        case 8:
                            return this._beginvalue;
                        case 9:
                            return this._endvalue;
                        case 10:
                            return this._colgener;
                        default:
                            throw new Exception("This unknown property!");
                    } // switch
                } // get
            } // DisplayName
        }

        private void fuelDataGridControl_Click(object sender, EventArgs e)
        {

        } // DeveloperAttribute
    } // DevFuelControl
} // BaseReports