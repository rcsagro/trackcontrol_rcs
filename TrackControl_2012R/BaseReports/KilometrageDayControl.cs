using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using C1.C1Excel;
using C1.Win.C1Report;
using LocalCache;
using BaseReports.Procedure;
using BaseReports.Properties;
using TrackControl.General;

namespace BaseReports
{
  /// <summary>
  /// ����� �������� ��� ����������� ������ � �������
  /// � ���������� ������������� ��������
  /// </summary>
  [Serializable]
  public partial class KilometrageDayControl : ReportTabControl
  {
    /// <summary>
    /// �������, ���������� �������� ������ ��� ������� ��������� (�������������
    /// ��������). ������ �������� ID ���������.
    /// </summary>
    private Dictionary<int, KilometrageDays.SummaryDay> _summariesDay =
      new Dictionary<int, KilometrageDays.SummaryDay>();

    #region .ctor

    /// <summary>
    /// �����������
    /// </summary>
    /// <param name="caption">�������� ������</param>
    public KilometrageDayControl(string caption)
      : base(caption)
    {
      InitializeComponent();
      Init();

      printPattern = "KilometrageDayReport.xml";
      excelPattern = "KilometrageDayReport.xls";

      AddAlgorithm(new KilometrageDays());
    }

    /// <summary>
    /// ����������� ��� ���������� ��� ��������,
    /// ������� ���������� ����� � ������� � ����������
    /// ������������� ��������.
    /// </summary>
    public KilometrageDayControl()
      : this(Resources.KilometrageDay)
    {
    }
    #endregion

    #region Event Handlers

    protected override void Algorithm_Action(object sender, EventArgs e)
    {
      if (sender is KilometrageDays)
      {
        if (e is KilometrageDaysEventArgs)
        {
          KilometrageDaysEventArgs args = (KilometrageDaysEventArgs)e;
          if (_summariesDay.ContainsKey(args.Id))
          {
            _summariesDay.Remove(args.Id);
          }
          _summariesDay.Add(args.Id, args.SummaryDay);
          Select(curMrow);
        }
      }
    }

    #endregion

    #region Public Members

    public override void Select(atlantaDataSet.mobitelsRow mobitel)
    {
      if (mobitel != null)
      {
        curMrow = mobitel;
        if (_summariesDay.ContainsKey(curMrow.Mobitel_ID))
        {
          // ��������� ������� �������� ��������� ������
          lblTotalWay.Text = _summariesDay[curMrow.Mobitel_ID].TotalWay.ToString("F");
          lblTotalTimeTour.Text = _summariesDay[curMrow.Mobitel_ID].TotalTimeTour.ToString();
          lblTotalTimeWay.Text = _summariesDay[curMrow.Mobitel_ID].TotalTimeWay.ToString();

          vehicleInfo = new VehicleInfo(curMrow);
          EnableButton(true);
        }
        else
        {
          EnableButton(false);
          ClearStatusLine();
        }

        atlantaDataSetBindingSource.Filter =
          String.Format("MobitelId={0}", mobitel.Mobitel_ID);
      }
    }

    /// <summary>
    /// ������� ������ ������
    /// </summary>
    public override void ClearReport()
    {
      dataset.KilometrageReport.Clear();
      dataset.KilometrageReportDay.Clear();
      ClearStatusLine();
      _summariesDay.Clear();
    }

    #endregion

    #region Protected Members
    /// <summary>
    /// ������������ ����� �� ������� ������ � .xls-��������, 
    /// ��������� � ��������� ��� � Excel.
    /// </summary>
    protected override void exportToExcelCurrent()
    {
      LoadPatternExcel();
      XLSheet template = c1XLBook.Sheets[0];
      AddXlsSheet(template, curMrow.Mobitel_ID);
      c1XLBook.Sheets.Remove(template);

      SaveExcel(Resources.KilometrageDayCtrlFileName, true);
    }

    /// <summary>
    /// ������������ ����� �� ���� �������� ������� � .xls-��������, 
    /// ��������� ��� � ��������� � Excel.
    /// ����� �� ������ ������ ������������� �� ��������� �����.
    /// </summary>
    protected override void exportToExcelActive()
    {
      LoadPatternExcel();
      XLSheet template = c1XLBook.Sheets[0];
      foreach (int mobitelId in _summariesDay.Keys)
      {
        AddXlsSheet(template, mobitelId);
      }
      c1XLBook.Sheets.Remove(template);

      SaveExcel(Resources.KilometrageDayCtrlAggregateFileName, true);
    }

    #endregion

    #region GUI Event Handlers

    protected override void runToolStripButton_Click(object sender, EventArgs e)
    {
      ClearReport();
      _summariesDay = new Dictionary<int, KilometrageDays.SummaryDay>();
      atlantaDataSetBindingSource.RaiseListChangedEvents = false;
      atlantaDataSetBindingSource.SuspendBinding();
      base.runToolStripButton_Click(sender, e);
      atlantaDataSetBindingSource.RaiseListChangedEvents = true;
      atlantaDataSetBindingSource.ResumeBinding();
      atlantaDataSetBindingSource.Filter = "";
      Select(curMrow);

      ReportsControl.OnClearMapObjectsNeeded();
      ReportsControl.ShowGraph(curMrow);
    }

    void grid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      ReportsControl.OnClearMapObjectsNeeded();
      if (e.RowIndex >= 0)
      {
        atlantaDataSet.KilometrageReportDayRow row = (atlantaDataSet.KilometrageReportDayRow)
          ((DataRowView)atlantaDataSetBindingSource.List[e.RowIndex]).Row;
        List<Track> segments = new List<Track>();
        segments.Add(GetTrackSegment(row));
        ReportsControl.OnTrackSegmentsShowNeeded(segments);
      }
    }

    void grid_RowEnter(object sender, DataGridViewCellEventArgs e)
    {
      atlantaDataSet.KilometrageReportDayRow row = (atlantaDataSet.KilometrageReportDayRow)
        ((DataRowView)atlantaDataSetBindingSource.List[e.RowIndex]).Row;
      Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
    }

    void grid_RowLeave(object sender, DataGridViewCellEventArgs e)
    {
      Graph.ClearRegion();
    }

    protected override void printToolStripButton_Click(object sender, EventArgs e)
    {
      LoadPatternPrint("KilometrageDayReport");

      c1Report.DataSource.Recordset = atlantaDataSetBindingSource;

      Section section = c1Report.Sections[SectionTypeEnum.Header];
      section.Fields["ReportFrom"].Text = Algorithm.Period.Begin.ToString();
      section.Fields["ReportTo"].Text = Algorithm.Period.End.ToString();

      // ���������� � ��������
      section.Fields["CarName"].Text = vehicleInfo.Info;
      section.Fields["DriverName"].Text = vehicleInfo.DriverFullName;

      section.Fields["TotalWay"].Text =
        _summariesDay[curMrow.Mobitel_ID].TotalWay.ToString("F");
      section.Fields["TotalTimeTour"].Text =
        _summariesDay[curMrow.Mobitel_ID].TotalTimeTour.ToString();
      section.Fields["TotalTimeWay"].Text =
        _summariesDay[curMrow.Mobitel_ID].TotalTimeWay.ToString();

      using (PrintPreviewDialog dlg = new PrintPreviewDialog())
      {
        dlg.Document = c1Report.Document;
        ((Form)dlg).WindowState = FormWindowState.Maximized;
        dlg.ShowDialog();
      }
    }

    /* ���������� ������ - ���������� �� ������� */
    protected override void ShowGraphToolStripButton_Click(object sender, EventArgs e)
    {
      ReportsControl.OnGraphShowNeeded();
      Graph.ClearRegion();
      int rowIndex = kilometrageDayDataGridView.CurrentRow.Index;
      atlantaDataSet.KilometrageReportDayRow row = (atlantaDataSet.KilometrageReportDayRow)
        ((DataRowView)atlantaDataSetBindingSource.List[rowIndex]).Row;

      Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
    }

      // ����� ���������� ������ - ���������� �� �����
    protected override void showMapToolStripButton_Click(object sender, EventArgs e)
    {
      ReportsControl.OnClearMapObjectsNeeded();
      ReportsControl.OnMapShowNeeded();

      List<Track> segments = new List<Track>();
      foreach (DataGridViewRow row in kilometrageDayDataGridView.SelectedRows)
      {
        if (row.Index >= 0)
        {
          atlantaDataSet.KilometrageReportDayRow k_row = (atlantaDataSet.KilometrageReportDayRow)
            ((DataRowView)atlantaDataSetBindingSource.List[row.Index]).Row;

          segments.Add(GetTrackSegment(k_row));
        }

      }
      if (segments.Count > 0)
        ReportsControl.OnTrackSegmentsShowNeeded(segments);
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    void ClearStatusLine()
    {
      lblTotalWay.Text = NULL_VALUE_FOR_TOTAL;
      lblTotalTimeTour.Text = TIME_NULL_VALUE_FOR_TOTAL;
      lblTotalTimeWay.Text = TIME_NULL_VALUE_FOR_TOTAL;
    }

    /// <summary>
    /// ������������� ����������� ��� ��������� VS.
    /// </summary>
    void Init()
    {
      atlantaDataSetBindingSource.DataSource = dataset;
      atlantaDataSetBindingSource.DataMember = "KilometrageReportDay";
      kilometrageDayDataGridView.DataSource = atlantaDataSetBindingSource;
      SelectFieldGrid(ref kilometrageDayDataGridView);
      ExcelToolStripButton.Visible = false;
      atlantaDataSet = null;

      lblTotalWayText.Text = Resources.DistanceText;
      lblTotalWayText.ToolTipText = Resources.DistanceHint;
      lblTotalWay.ToolTipText = Resources.DistanceHint;

      lblTotalTimeTourText.Text = Resources.TotalShiftTimeText;
      lblTotalTimeTourText.ToolTipText = Resources.TotalShiftTimeHint;
      lblTotalTimeTour.ToolTipText = Resources.TotalShiftTimeHint;

      lblTotalTimeWayText.Text = Resources.TotalTravelTimeText;
      lblTotalTimeWayText.ToolTipText = Resources.TotalTravelTimeHint;
      lblTotalTimeWay.ToolTipText = Resources.TotalTravelTimeHint;

      dayDataGridViewTextBoxColumn.HeaderText = Resources.Date;
      dayDataGridViewTextBoxColumn.ToolTipText = Resources.Date;

      locationStartDataGridViewTextBoxColumn.HeaderText = Resources.StartLocation;
      locationStartDataGridViewTextBoxColumn.ToolTipText = Resources.StartLocation;

      locationEndDataGridViewTextBoxColumn.HeaderText = Resources.MovementEndPlace;
      locationEndDataGridViewTextBoxColumn.ToolTipText = Resources.MovementEndPlace;

      initialTimeDataGridViewTextBoxColumn1.HeaderText = Resources.InitialMovementTime;
      initialTimeDataGridViewTextBoxColumn1.ToolTipText = Resources.InitialMovementTime;

      finalTimeDataGridViewTextBoxColumn1.HeaderText = Resources.EndMovementTime;
      finalTimeDataGridViewTextBoxColumn1.ToolTipText = Resources.EndMovementTime;

      intervalWorkDataGridViewTextBoxColumn1.HeaderText = Resources.ShiftDurationText;
      intervalWorkDataGridViewTextBoxColumn1.ToolTipText = Resources.ShiftDurationHint;

      intervalMoveDataGridViewTextBoxColumn1.HeaderText = Resources.MovementTotalTime;
      intervalMoveDataGridViewTextBoxColumn1.ToolTipText = Resources.TotalTravelTimeHint;

      distanceDataGridViewTextBoxColumn1.HeaderText = Resources.DistanceHint;
      distanceDataGridViewTextBoxColumn1.ToolTipText = Resources.DistanceHint;

      averageSpeedDataGridViewTextBoxColumn1.HeaderText = Resources.AvgSpeed;
      averageSpeedDataGridViewTextBoxColumn1.ToolTipText = Resources.AvgSpeed;
    }

    /// <summary>
    /// ��������� xls-���� � ������� �� ��������� ������.
    /// </summary>
    /// <param name="template">������ ������</param>
    /// <param name="mobitelId">ID ��������� ��������� ������</param>
    void AddXlsSheet(XLSheet template, int mobitelId)
    {
      XLSheet sheet = template.Clone();

      sheet[1, 3].Value = Algorithm.Period.Begin;
      sheet[1, 5].Value = Algorithm.Period.End;

      atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
      VehicleInfo info = new VehicleInfo(mobitelId);

      sheet[3, 2].Value = info.Info; // ����������
      sheet[4, 2].Value = info.DriverFullName; // ��������
      sheet[3, 7].Value = _summariesDay[mobitelId].TotalWay; // ���������� ����
      sheet[4, 7].Value = _summariesDay[mobitelId].TotalTimeTour; // ����� ����������������� �����
      sheet[5, 7].Value = _summariesDay[mobitelId].TotalTimeWay; // ����� � ����

      int i = 8; // ����� ������ xls-���������, � ������� ���������� ������
      foreach (atlantaDataSet.KilometrageReportDayRow row in 
          dataset.KilometrageReportDay.Select("MobitelId=" + mobitelId, "Id ASC"))
      {
        sheet[i, 1].Value = row.InitialTime.ToString("D");
        sheet[i, 1].Style = sheet[8, 1].Style;
        sheet[i, 2].Value = (row["LocationStart"] != DBNull.Value) ? row.LocationStart : " - ";
        sheet[i, 2].Style = sheet[8, 2].Style;
        sheet[i, 3].Value = (row["LocationEnd"] != DBNull.Value) ? row.LocationEnd : " - ";
        sheet[i, 3].Style = sheet[8, 3].Style;
        sheet[i, 4].Value = row.InitialTime.ToString("t");
        sheet[i, 4].Style = sheet[8, 4].Style;
        sheet[i, 5].Value = row.FinalTime.ToString("t");
        sheet[i, 5].Style = sheet[8, 5].Style;
        sheet[i, 6].Value = row.IntervalWork;
        sheet[i, 6].Style = sheet[8, 6].Style;
        sheet[i, 7].Value = row.IntervalMove;
        sheet[i, 7].Style = sheet[8, 7].Style;

        if (row["Distance"] != DBNull.Value)
        {
          sheet[i, 8].Value = row.Distance;
        }

        sheet[i, 8].Style = sheet[8, 8].Style;

        if (row["AverageSpeed"] != DBNull.Value)
        {
          sheet[i, 9].Value = row.AverageSpeed;
        }

        sheet[i, 9].Style = sheet[8, 9].Style;

        i++;
      }

      sheet.Name = mobitel.NumTT; //info.CarModel;
      c1XLBook.Sheets.Add(sheet);
    }

    static Track GetTrackSegment(atlantaDataSet.KilometrageReportDayRow row)
    {
      List<IGeoPoint> data = new List<IGeoPoint>();
      bool inside = false;
      foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select("Mobitel_ID=" + row.MobitelId, "time ASC"))
      {
        if (d_row.DataGps_ID == row.InitialPointId)
          inside = true;
        if (inside)
          data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
        if (d_row.DataGps_ID == row.FinalPointId)
          break;
      }
      return new Track(row.MobitelId, Color.Red, 2f, data);
    }
    #endregion
  }
}
