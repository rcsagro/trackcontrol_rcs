using BaseReports.Procedure;
using BaseReports.Properties;
using C1.C1Excel;
using C1.Win.C1Report;
using LocalCache;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports
{
  [ToolboxItem(false)]
  public partial class FlowmeterControl : ReportTabControl
  {
    private FlowMeter fuelRateAlg;

    /// <summary>
    /// ����� � ������������ �� ����������.
    /// </summary>
    private IDictionary<int, FuelRateTotal> totals;
    /// <summary>
    /// ��������� ��� ���������� �������� �� SeriesL
    /// </summary>
    private static IBuildGraphs buildGraph;

    #region .ctor

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="caption">�������� ������.</param>
    public FlowmeterControl(string caption)
      : base(caption)
    {
      InitializeComponent();
      Init();
      excelPattern = "Flowmeter.xls";
      printPattern = "Flowmeter.xml";

      fuelRateAlg = FlowMeter.GetFlowMeter();//new FlowMeter();
      totals = new Dictionary<int, FuelRateTotal>();

      AddAlgorithm(new Kilometrage());
      AddAlgorithm(new Rotation());
      AddAlgorithm(fuelRateAlg);
      //AddAlgorithm(new FlowMeter(AlgorithmType.FUEL2));
    }

    /// <summary>
    /// ����������� ��� ���������� ��� �������� ������ "������".
    /// </summary>
    public FlowmeterControl()
      : this(Resources.FuelConsumptionShort)
    {
    }

    private void Init()
    {
      atlantaDataSetBindingSource.DataSource = dataset;
      atlantaDataSetBindingSource.DataMember = "flowmeterReport";
      flowmeterReportDataGridView.DataSource = atlantaDataSetBindingSource;

      buildGraph = new BuildGraphs();

      SelectFieldGrid(ref flowmeterReportDataGridView);
      this.showMapToolStripButton.Visible = false;
      //this.ShowGraphToolStripButton.Visible = false;
      atlantaDataSet = null;
      hideExcelSplitBtn();

      _travelLabel.Text = Resources.DistanceText;
      travelLabelData.Text = " -,--";
      _timeTravelLabel.Text = Resources.TotalTravelTimeText;
      _stopTimeLabel.Text = Resources.FlowmeterCtrlParkingTime;
      _flowmeterLabel.Text = Resources.TotalLiters;
      flowmeterLabelData.Text = "-,--";
      _idleFuelRateLabel.Text = Resources.HourmeterFooter;
      idleFuelRateLabelData.Text = "--";
      _moveFuelRateLabel.Text = Resources.FuelRate100kmFooter;
      moveFuelRateLabelData.Text = "-,--";

      dataGridViewTextBoxColumn2.HeaderText = Resources.FlowmeterCtrlLocation;
      dataGridViewTextBoxColumn3.HeaderText = Resources.InitialTime;
      dataGridViewTextBoxColumn4.HeaderText = Resources.EndTime;
      dataGridViewTextBoxColumn7.HeaderText = Resources.FlowmeterPeriod;

      dataGridViewTextBoxColumn8.HeaderText = Resources.DistanceShort;
      dataGridViewTextBoxColumn8.ToolTipText = Resources.DistanceHint;

      dataGridViewTextBoxColumn9.HeaderText = Resources.TotalLiters;
      dataGridViewTextBoxColumn9.ToolTipText = Resources.TotalFuelConsumptionHint;

      idleFuelRate.HeaderText = Resources.Hourmeter;
      idleFuelRate.ToolTipText = Resources.FuelConsumptionParkingHint;

      moveFuelRate.HeaderText = Resources.FuelRate100km;
      moveFuelRate.ToolTipText = Resources.FuelConsumptionMovementHint;
    }

    #endregion

    #region Public Methods

    public override void Select(atlantaDataSet.mobitelsRow m_row)
    {
      base.Select(m_row);
      if (m_row != null)
      {
        ClearStatusLine();
        if (m_row.GetdataviewRows() == null || m_row.GetdataviewRows().Length == 0 ||
          m_row.GetflowmeterReportRows().Length == 0)
        {
          return;
        }

        FuelRateTotal frTotal = GetTotals(m_row);
        if (frTotal != null)
        {
          travelLabelData.Text = frTotal.TotalDistance.ToString("#.##");//"���������� ����: " + 
          timeTravelLabelData.Text = String.Format("{0}({1})", frTotal.TotalTimeMotion, frTotal.TotalTimeMotionWithoutShortStops);//"����� � ����: " + 
          stopTimeLabelData.Text = String.Format("{0}({1})", frTotal.TotalTimeStops, frTotal.TotalTimeStopsWithShortStops);//"����� �������: " + 
          idleFuelRateLabelData.Text = frTotal.TotalFuelRateOnHour.ToString("0.##");//"������ ��.: " + 
          moveFuelRateLabelData.Text = frTotal.TotalFuelRateOnHundred.ToString("0.##");//"������: " + 
          flowmeterLabelData.Text = frTotal.TotalFuelRate.ToString("0.##");//"����� �������: " + 
        }
        else
          return;
      }
    }

    public override void SelectGraphic(atlantaDataSet.mobitelsRow m_row)
    {
      base.Select(m_row);
      if (m_row != null)
      {// ���������� �������
        try
        {
          if (m_row.GetdataviewRows() == null || m_row.GetdataviewRows().Length == 0)
          {
            return;
          }
          ReportsControl.GraphClearSeries();

          buildGraph.AddGraphFlowmeter(graph, dataset, m_row);

          //Graph.ShowSeries(vehicleInfo.Info);
          ReportsControl.ShowGraph(curMrow);
        }
        catch (Exception ex)
        {
          throw new NotImplementedException("Exception in FlowmeterControl: " + ex.Message);
        }
      }
    }

    /// <summary>
    /// ������� ������ ������
    /// </summary>
    public override void ClearReport()
    {
      dataset.flowmeterReport.Clear();
      totals.Clear();
    }

    #endregion

    #region GUI Event Handlers
    /// <summary>
    /// ���������� ������
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected override void runToolStripButton_Click(object sender, EventArgs e)
    {
      ClearReport();

      atlantaDataSetBindingSource.RaiseListChangedEvents = false;
      atlantaDataSetBindingSource.SuspendBinding();
      base.runToolStripButton_Click(sender, e);
      atlantaDataSetBindingSource.RaiseListChangedEvents = true;
      atlantaDataSetBindingSource.ResumeBinding();
      atlantaDataSetBindingSource.Filter = "";

      Select(curMrow);
      SelectGraphic(curMrow);
    }

    /// <summary>
    /// ������������ ���� �� ������ "�������� �� �������".
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected override void ShowGraphToolStripButton_Click(object sender, EventArgs e)
    {
      if (sender != null)
        ReportsControl.OnGraphShowNeeded();
      Graph.ClearRegion();
      Graph.ClearLabel();
      if (null == flowmeterReportDataGridView.CurrentRow)
        return;

      foreach (System.Windows.Forms.DataGridViewRow row in flowmeterReportDataGridView.SelectedRows)
      {
        atlantaDataSet.flowmeterReportRow f_row = (atlantaDataSet.flowmeterReportRow)
          ((System.Data.DataRowView)atlantaDataSetBindingSource.List[row.Index]).Row;
        if (f_row != null)
        {
          Graph.AddTimeRegion(f_row.beginTime, f_row.endTime);//�������� ������
        }
      }
      Graph.SellectZoom();
    }

    private void flowmeterReportDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      ShowGraphToolStripButton_Click(null, new EventArgs());
    }

    protected override void printToolStripButton_Click(object sender, EventArgs e)
    {
      LoadPatternPrint("Flowmeter");

      c1Report.DataSource.Recordset = atlantaDataSetBindingSource;
      Section section = c1Report.Sections[SectionTypeEnum.Header];

      section.Fields["fldBeginPeriod"].Text = Algorithm.Period.Begin.ToString("dd.MM.yyyy HH:mm:ss");
      section.Fields["fldEndPeriod"].Text = Algorithm.Period.End.ToString("dd.MM.yyyy HH:mm:ss");
      section.Fields["fldCarName"].Text = vehicleInfo.Info;
      section.Fields["fldDriver"].Text = vehicleInfo.DriverFullName;

      section.Fields["fldSumTravel"].Text = curMrow.path.ToString("#,##0.00");
      section.Fields["fldSumTravelTime"].Text = curMrow.timeTravel.ToString();
      section.Fields["fldSumParkingTime"].Text = curMrow.timeStop.ToString();
      section.Fields["fldMotoHours"].Text = curMrow.idleFuelRate.ToString("#,##0.00");
      section.Fields["fldSumFuelRate"].Text = curMrow.motionFuelRate.ToString("#,##0.00");
      section.Fields["fldSumFuel"].Text = curMrow.summFuelRate.ToString("#,##0.00");

      PrintPreviewDialog dlg = new PrintPreviewDialog();
      dlg.Document = c1Report.Document;
      ((Form)dlg).WindowState = FormWindowState.Maximized;
      dlg.ShowDialog();
    }

    protected override void ExcelToolStripButton_Click(object sender, EventArgs e)
    {
      LoadPatternExcel();
      XLSheet sheet = c1XLBook.Sheets[0];
      sheet[1, 1].Value = Algorithm.Period.Begin;
      sheet[1, 4].Value = Algorithm.Period.End;

      sheet[3, 1].Value = vehicleInfo.Info; // ����������
      sheet[4, 1].Value = vehicleInfo.DriverFullName; // ��������
      sheet[5, 1].Value = curMrow.path; // ���������� ����
      sheet[6, 1].Value = curMrow.timeTravel.ToString(); // ����� � ����
      sheet[7, 1].Value = curMrow.timeStop.ToString(); // ����� �������
      sheet[5, 4].Value = curMrow.idleFuelRate; // ��������
      sheet[6, 4].Value = curMrow.motionFuelRate; // ������
      sheet[7, 4].Value = curMrow.summFuelRate; // ����� �������

      int i = 10;
      foreach (DataRowView dr in atlantaDataSetBindingSource)
      {
        atlantaDataSet.flowmeterReportRow fRow =
          (atlantaDataSet.flowmeterReportRow)dr.Row;
        sheet[i, 0].Value = fRow.Location;
        sheet[i, 0].Style = sheet[10, 0].Style;
        sheet[i, 1].Value = fRow.beginTime;
        sheet[i, 1].Style = sheet[10, 1].Style;
        sheet[i, 2].Value = fRow.endTime;
        sheet[i, 2].Style = sheet[10, 2].Style;
        sheet[i, 3].Value = fRow.duration;
        sheet[i, 3].Style = sheet[10, 3].Style;

        if (fRow["travel"] != DBNull.Value)
          sheet[i, 4].Value = fRow.travel;
        sheet[i, 4].Style = sheet[10, 4].Style;

        if (fRow["fuelRate"] != DBNull.Value)
          sheet[i, 5].Value = fRow.fuelRate;
        sheet[i, 5].Style = sheet[10, 4].Style;

        if (fRow["idleFuelRate"] != DBNull.Value)
          sheet[i, 6].Value = fRow.idleFuelRate;
        sheet[i, 6].Style = sheet[10, 4].Style;

        if (fRow["moveFuelRate"] != DBNull.Value)
          sheet[i, 7].Value = fRow.moveFuelRate;
        sheet[i, 7].Style = sheet[10, 4].Style;

        i++;
      }
      sheet.Name = vehicleInfo.RegistrationNumber;
      SaveExcel(Resources.FuelConsumptionShort, true);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// ������ ������.
    /// </summary>
    /// <param name="mobitelId">������������� ���������.</param>
    /// <returns>PassTrafficDailyTotals - ����� �� ������� ���������.</returns>
    public FuelRateTotal GetTotals(atlantaDataSet.mobitelsRow m_row)
    {
      if (totals.ContainsKey(m_row.Mobitel_ID))
      {
        return totals[m_row.Mobitel_ID];
      }

      FuelRateTotal fuelRateTotal = fuelRateAlg.GetFuelRateTotal(m_row);
      if (fuelRateTotal.FuelRateSensor1 != null)//��������� ��� �� ��� �������� ����� �� ������� ��
      {
        PartialAlgorithms partialAlg = new PartialAlgorithms();
        partialAlg.SelectItem(m_row);
        GpsData[] d_rows = DataSetManager.ConvertDataviewRowsToDataGpsArray(m_row.GetdataviewRows());
        fuelRateTotal.TotalDistance = Math.Round(m_row.path,2);
        fuelRateTotal.TotalTimeMotionWithoutShortStops  = BasicMethods.GetTotalMotionTime(d_rows);
        fuelRateTotal.TotalTimeStopsWithShortStops  = BasicMethods.GetTotalStopsTime(d_rows, 0);
        fuelRateTotal.TotalFuelRateOnHour = ((partialAlg.GetFuelUseInMotionDrt(d_rows, 0) + partialAlg.GetFuelUseInStopDrt(d_rows, 0, 0))
                  / (fuelRateTotal.TotalTimeMotion + (BasicMethods.GetTotalStopsTime(d_rows, 0) - partialAlg.GetTimeStopEnginOff(d_rows, 0, 0))).TotalHours);
        fuelRateTotal.TotalFuelRateOnHundred = m_row.motionFuelRate;
        fuelRateTotal.TotalFuelRate = m_row.summFuelRate;

        foreach (DataRowView dr in atlantaDataSetBindingSource)
        {
            atlantaDataSet.flowmeterReportRow fRow =
              (atlantaDataSet.flowmeterReportRow)dr.Row;
            if (fRow.Location == Resources.Movement)
            {
                fuelRateTotal.TotalTimeMotion += fRow.duration;
            }
            else
            {
                fuelRateTotal.TotalTimeStops += fRow.duration;
            }

        }


        totals.Add(m_row.Mobitel_ID, fuelRateTotal);
      }
      else
      {
        fuelRateTotal = null;
      }
      return fuelRateTotal;
    }

    protected override void Algorithm_Action(object sender, EventArgs e)
    {
      if (sender is Procedure.FlowMeter)
      {
        EnableButton(true);
        Select(curMrow);
      }
    }

    /// <summary>
    /// ������� ������.
    /// </summary>
    private void ClearStatusLine()
    {
      travelLabelData.Text = "-,--";//���������� ����:  
      timeTravelLabelData.Text = "--:--:--";//����� � ����:
      stopTimeLabelData.Text = "--:--:--";//����� �������:
      moveFuelRateLabelData.Text = "-,--";//"������: " + 
      idleFuelRateLabelData.Text = "--";//"��������: " + 
      flowmeterLabelData.Text = "-,--";//"����� �������: " + 
    }

    #endregion
  }
}

