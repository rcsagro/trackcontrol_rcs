using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;
using BaseReports.Procedure;
using BaseReports.Properties;
using LocalCache;
using TrackControl.General.Log;
using DevExpress.XtraEditors;
using TrackControl.Reports;
using TrackControl.Reports.Graph;
using TrackControl.General;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

namespace BaseReports
{
    [Serializable]
    [ToolboxItem(false)]
    public partial class ReportTabControl : UserControl, IReportTabControl
    {
        private string nameReport = "";

        /// <summary>
        /// ������� "��� ������"
        /// </summary>
        public readonly string NA = Resources.ReportTabCtrlNA;

        /// <summary>
        /// ������������ ������ �������� � ������: ---
        /// </summary>
        protected const string NULL_VALUE_FOR_TOTAL = "---";

        /// <summary>
        /// ������������ ������ �������� ��� ������� � ������: --:--:--
        /// </summary>
        protected const string TIME_NULL_VALUE_FOR_TOTAL = "--:--:--";

        /// <summary>
        /// �������������� ����� �� ������� ������ ��� �� ���� ���������.
        /// </summary>
        //private ExcelExport _excelExport = ExcelExport.Current;
        /// <summary>
        /// ������ ������� �������
        /// </summary>
        protected List<StateColumn> columns;

        /// <summary>
        /// ���� � ����� ��������
        /// </summary>
        protected string _settingFilePath;

        /// <summary>
        /// ������ ����������
        /// </summary>
        protected List<IAlgorithm> algoritms;

        /// <summary>
        /// �������� ������� ������
        /// </summary>
        protected VehicleInfo vehicleInfo;

        /// <summary>
        /// ��� ����� ������� ��� �������� � Excel
        /// </summary>
        protected string excelPattern;

        /// <summary>
        /// ��� ����� ������� ��� �������� � Excel
        /// </summary>
        public string ExcelPattern
        {
            set { excelPattern = value; }
        }

        protected string printPattern;

        /// <summary>
        /// ��� ����� ������� ��� ������
        /// </summary>
        public string PrintPatten
        {
            set { printPattern = value; }
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        public static atlantaDataSet Dataset { get; set; }

        protected static IGraph graph;

        public static IGraph Graph
        {
            get { return graph; }
            set { graph = value; }
        }

        //static private atlantaDataSet atlantaDataSet; 
        /// <summary>
        /// ��������
        /// </summary>
        private string caption;

        /// <summary>
        /// ��������
        /// </summary>
        public string Caption
        {
            get { return caption; }
        }

        public BindingNavigator bindingNavigator
        {
            get { return null; }
        }

        public void EnableRun()
        {
            runToolStripButton.Enabled = true;
        }

        public void DisableRun()
        {
            runToolStripButton.Enabled = false;
        }


        public static IList<ReportVehicle> CheckedVehicles { get; set; }

        /// <summary>
        /// ��������� ������� ������ mobitelsRow 
        /// (�� ������� ������ ��������� ������)
        /// </summary>
        protected atlantaDataSet.mobitelsRow curMrow;

        protected StateColumn FindColumn(string name)
        {
            foreach (StateColumn col in columns)
            {
                if (col.name == name)
                {
                    return col;
                }
            }
            return new StateColumn(name, true);
        }

        protected void SelectFieldGrid(ref DataGridView grid)
        {
            columns = LoadSetting();
            if (columns.Count == 0)
            {
                foreach (DataGridViewColumn col in grid.Columns)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(col.HeaderText);
                    StateColumn scol = FindColumn(col.HeaderText);
                    //StateColumn scol = new StateColumn(col.HeaderText,true);
                    item.Checked = true;
                    col.Visible = scol.visible;
                    if (scol.visible)
                        item.CheckState = CheckState.Checked;
                    else
                        item.CheckState = CheckState.Unchecked;
                    item.CheckOnClick = true;
                    item.Tag = col;
                    item.CheckStateChanged += fieldGrid_CheckStateChanged;
                    fieldGridToolStripSplitButton.DropDownItems.Add(item);
                    columns.Add(scol);
                }
            }
            else
            {
                foreach (DataGridViewColumn col in grid.Columns)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(col.HeaderText);
                    StateColumn scol = FindColumn(col.HeaderText);
                    //StateColumn scol = new StateColumn(col.HeaderText,true);
                    item.Checked = true;
                    col.Visible = scol.visible;
                    if (scol.visible)
                        item.CheckState = CheckState.Checked;
                    else
                        item.CheckState = CheckState.Unchecked;
                    item.CheckOnClick = true;
                    item.Tag = col;
                    item.CheckStateChanged += fieldGrid_CheckStateChanged;
                    fieldGridToolStripSplitButton.DropDownItems.Add(item);
                    //columns.Add(scol);
                }
            }
        }

        /// <summary>
        /// �������� ��������
        /// </summary>
        protected List<StateColumn> LoadSetting()
        {
            if (File.Exists(_settingFilePath))
            {
                try
                {
                    using (StreamReader xmlFile = new StreamReader(_settingFilePath))
                    {
                        columns = (List<StateColumn>)
                            (new XmlSerializer(typeof (List<StateColumn>))).Deserialize(xmlFile);
                    }
                } // try
                catch (Exception ex)
                {
                    ExecuteLogging.Log("App", String.Format(
                        Resources.ReportTabCtrlErrorXmlLoading, _settingFilePath, ex));
                }
            }
            return columns;
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void SaveSetting()
        {
            try
            {
                using (StreamWriter xmlFile = new StreamWriter(_settingFilePath, false))
                {
                    (new XmlSerializer(typeof (List<StateColumn>))).Serialize(xmlFile, columns);
                }
            }
            catch (Exception ex)
            {
                ExecuteLogging.Log("App", String.Format(
                    Resources.ReportTabCtrlErrorXmlSaving, _settingFilePath, ex));
            }
        }

        public ReportTabControl()
        {
            InitializeComponent();
            initComponents();

            Algorithm.Action += Algorithm_Action;

            columns = new List<StateColumn>();
            algoritms = new List<IAlgorithm>();
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="caption">�������� ������.</param>
        public ReportTabControl(string caption)
            : this()
        {
            this.caption = caption;
            string repositoryPath = Path.Combine(
                TrackControl.General.Globals.APP_DATA, "ReportGrids");

            if (!Directory.Exists(repositoryPath))
                Directory.CreateDirectory(repositoryPath);

            _settingFilePath = String.Format("{0}.xml", Path.Combine(repositoryPath, caption));
        }

        #region Protected Methods

        /// <summary>
        /// ������������ � Excel ����� �� ������� ������.
        /// </summary>
        protected virtual void exportToExcelCurrent()
        {
            MessageBox.Show(Resources.ReportTabCtrlErrorExportImplementation);
        }

        /// <summary>
        /// ������������ � Excel ����� �� ��������� �������.
        /// </summary>
        protected virtual void exportToExcelActive()
        {
            MessageBox.Show(Resources.ReportTabCtrlErrorBatchExport);
        }

        /// <summary>
        /// ������������ � Excel ���� DevExpress.
        /// </summary>
        protected virtual void exportToExcelDevExpress()
        {
            ExportToExcelDevExpress(false);
        }

        /// <summary>
        /// ������������ � Excel ���� DevExpress ��������� ��� ��������� � ������ ������� �����������
        /// </summary>
        protected virtual void exportToExcelDevExpressDetal()
        {
            ExportToExcelDevExpress(true);
        }

        protected virtual void ExportToExcelDevExpress(bool ExpandAllGroups)
        {

        }

        protected void SetupGidViewForPrint(GridView gv, bool ExpandAllGroups)
        {
            gv.OptionsPrint.ExpandAllDetails = true;
            gv.OptionsPrint.PrintDetails = true;
            gv.OptionsPrint.AutoWidth = false;
            gv.OptionsPrint.ExpandAllGroups = ExpandAllGroups;
            gv.BestFitColumns();
            gv.OptionsPrint.AutoWidth = true;
        }

        protected void DevExpressReportHeader(string Header, CreateAreaEventArgs e)
        {
            DevExpress.XtraPrinting.TextBrick brick = e.Graph.DrawString(Header, Color.Black,
                new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 20),
                DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Arial", 14, FontStyle.Bold);
            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);
        }

        protected void DevExpressReportSubHeader(string SubHeader, int height, CreateAreaEventArgs e)
        {

            DevExpress.XtraPrinting.TextBrick brick = e.Graph.DrawString(SubHeader, Color.Black,
                new RectangleF(0, 30, e.Graph.ClientPageSize.Width, height),
                DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Arial", 10);
            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Near);
        }

        /// <summary>
        /// ������ ������ ������. ������������ ��� ��� �������, � ������� �������������
        /// ������ ������ ���� ��� �� �������������.
        /// </summary>
        protected void hideExcelSplitBtn()
        {
            _excelDropDown.Visible = false;
        }

        #endregion

        #region GUI Event Handlers

        /// <summary>
        /// ������������ ���� �� ����� "���� ������".
        /// ������� � Excel ����� ���������� �� ������� ������.
        /// </summary>
        private void excelSplitItemCurrent_Click(object sender, EventArgs e)
        {
            exportToExcelCurrent();
        }

        /// <summary>
        /// ������������ ���� �� ����� "��� ������".
        /// ������� � Excel ����� ���������� �� ���� �������� �������.
        /// </summary>
        private void excelSplitItemAll_Click(object sender, EventArgs e)
        {
            exportToExcelActive();
        }

        private void tsmiVehicalOne_Click(object sender, EventArgs e)
        {
            exportToExcelCurrent();
        }

        private void tsmiVehicalAll_Click(object sender, EventArgs e)
        {
            exportToExcelActive();
        }

        /// <summary>
        /// ������ ����� DevExpress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiDetal_Click(object sender, EventArgs e)
        {
            exportToExcelDevExpressDetal();
        }

        private void tsmiTotal_Click(object sender, EventArgs e)
        {
            exportToExcelDevExpress();
        }

        #endregion

        /// <summary>
        /// ������������� ����������� ��� ������� ��������� VS.
        /// </summary>
        private void initComponents()
        {
            runToolStripButton.Image = Shared.Start;
            _stopBtn.Image = Shared.Stop;
            mapToolStripMenuItem.Image = Shared.Map;
            showMapToolStripButton.Image = Shared.Map;
            printToolStripMenuItem.Image = Shared.Printer;
            tsbPrintExport.Image = Shared.Printer;
            tsmiDetal.Image = Shared.Printer;
            tsmiTotal.Image = Shared.Printer;
            tsmiVehicalOne.Image = Shared.ToXls;
            tsmiVehicalAll.Image = Shared.ToXls;
            printToolStripButton.Image = Shared.Printer;
            fieldGridToolStripSplitButton.Image = Shared.Table;
            graphToolStripMenuItem.Image = Shared.Chart;
            ShowGraphToolStripButton.Image = Shared.Chart;
            _excelDropDown.Image = Shared.ToXls;
            excelToolStripMenuItem.Image = Shared.ToXls;
            ExcelToolStripButton.Image = Shared.ToXls;
            atlantaDataSetBindingSource.DataSource = Dataset;
            Dock = DockStyle.Fill;

            runToolStripButton.Text = Resources.BuildReportText;
            runToolStripButton.ToolTipText = Resources.BuildReportText;
            _stopBtn.Text = Resources.ReportTabCtrlBreakReportBuildingText;
            _stopBtn.ToolTipText = Resources.ReportTabCtrlBreakReportBuildingText;

            bindingNavigatorMoveFirstItem.Text = Resources.ReportTabCtrlMoveFirstItemText;
            bindingNavigatorMoveFirstItem.ToolTipText = Resources.ReportTabCtrlMoveFirstItemText;
            bindingNavigatorMovePreviousItem.Text = Resources.ReportTabCtrlMovePreviousItemText;
            bindingNavigatorMovePreviousItem.ToolTipText = Resources.ReportTabCtrlMovePreviousItemText;
            bindingNavigatorPositionItem.Text = Resources.ReportTabCtrlCurrentPositionText;
            bindingNavigatorPositionItem.ToolTipText = Resources.ReportTabCtrlCurrentPositionText;
            bindingNavigatorCountItem.Text = Resources.ReportTabCtrlRowCountHint;
            bindingNavigatorCountItem.ToolTipText = Resources.ReportTabCtrlRowCountHint;
            bindingNavigatorMoveNextItem.Text = Resources.ReportTabCtrlMoveNextItemText;
            bindingNavigatorMoveNextItem.ToolTipText = Resources.ReportTabCtrlMoveNextItemText;
            bindingNavigatorMoveLastItem.Text = Resources.ReportTabCtrlMoveLastItemText;
            bindingNavigatorMoveLastItem.ToolTipText = Resources.ReportTabCtrlMoveLastItemText;
            bindingNavigatorAddNewItem.Text = Resources.ReportTabCtrlAddText;
            bindingNavigatorAddNewItem.ToolTipText = Resources.ReportTabCtrlAddText;
            bindingNavigatorDeleteItem.Text = Resources.ReportTabCtrlDeleteText;
            bindingNavigatorDeleteItem.ToolTipText = Resources.ReportTabCtrlDeleteText;

            showMapToolStripButton.Text = Resources.ReportTabCtrlShowMapText;
            showMapToolStripButton.ToolTipText = Resources.ReportTabCtrlShowMapText;
            ShowGraphToolStripButton.Text = Resources.ReportTabCtrlShowGraphText;
            ShowGraphToolStripButton.ToolTipText = Resources.ReportTabCtrlShowGraphText;

            printToolStripButton.Text = Resources.PrintText;
            printToolStripButton.ToolTipText = Resources.PrintText;

            _excelDropDown.Text = Resources.ExportText;
            _excelDropDown.ToolTipText = Resources.ExportText;
            excelSplitItemCurrent.Text = Resources.ReportTabCtrlCurrentVehicleText;
            excelSplitItemAll.Text = Resources.ReportTabCtrlAllVehiclesText;

            tsmiVehicalOne.Text = Resources.ReportTabCtrlCurrentVehicleText;
            tsmiVehicalAll.Text = Resources.ReportTabCtrlAllVehiclesText;
            tsmiDetal.Text = Resources.Detailed;
            tsmiTotal.Text = Resources.Summary;

            ExcelToolStripButton.Text = Resources.ReportTabCtrlExcelExportText;
            ExcelToolStripButton.ToolTipText = Resources.ReportTabCtrlExcelExportText;

            btXML.Text = Resources.ReportTabCtrlXmlText;
            btXML.ToolTipText = Resources.ReportTabCtrlXmlText;
            fieldGridToolStripSplitButton.Text = Resources.ReportTabCtrlFieldGridText;
            fieldGridToolStripSplitButton.ToolTipText = Resources.ReportTabCtrlFieldGridHint;

            printToolStripMenuItem.Text = Resources.PrintText;
            excelToolStripMenuItem.Text = Resources.ReportTabCtrlExcelExportText;
            mapToolStripMenuItem.Text = Resources.ReportTabCtrlShowMapText;
            graphToolStripMenuItem.Text = Resources.ReportTabCtrlShowGraphText;
        }

        protected virtual void Algorithm_Action(object sender, EventArgs e)
        {
            return;
        }

        protected void EnableButton(bool enable)
        {
            printToolStripButton.Enabled = enable;
            ExcelToolStripButton.Enabled = enable;
            _excelDropDown.Enabled = enable;
            ShowGraphToolStripButton.Enabled = enable;
            showMapToolStripButton.Enabled = enable;
            printToolStripMenuItem.Enabled = enable;
            excelToolStripMenuItem.Enabled = enable;
            graphToolStripMenuItem.Enabled = enable;
            mapToolStripMenuItem.Enabled = enable;
            btXML.Enabled = enable;
            tsbPrintExport.Enabled = enable;
            if (enable) EnableButtonIndividualReport();
        }

        protected virtual void EnableButtonIndividualReport()
        {

        }

        protected void EnableEditButton(bool enable)
        {
            bindingNavigatorAddNewItem.Visible = enable;
            bindingNavigatorDeleteItem.Visible = enable;
        }


        #region IAlgoritmCollections Members

        /// <summary>
        /// ���������� ������ ���������
        /// </summary>
        /// <param name="algorithm"></param>
        public void AddAlgorithm(IAlgorithm algorithm)
        {
            algoritms.Add(algorithm);
        }

        /// <summary>
        /// ������������� ��� ������ List<algoritm>
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IAlgorithm> GetEnumerator()
        {
            return algoritms.GetEnumerator();
        }

        public virtual void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", mobitel.Mobitel_ID);
            }
        }

        public virtual void SelectGraphic(atlantaDataSet.mobitelsRow mobitel)
        {
            return;
        }

        #endregion

        #region ButtonEvent

        protected virtual void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void showMapToolStripButton_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void runToolStripButton_Click(object sender, EventArgs e)
        {
            bool noData = true; // �� ��������� ������ ��� ������ �� �� ����� ������.
            foreach (atlantaDataSet.mobitelsRow m_row in Dataset.mobitels)
            {
                if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                {
                    SelectItem(m_row);
                    noData = false;
                }
            }
            if (noData)
            {
                MessageBox.Show(Resources.WarningNoData, Resources.Notification);
            }
        }

        protected virtual void ExcelToolStripButton_Click(object sender, EventArgs e)
        {
            return;
        }

        protected virtual void printToolStripButton_Click(object sender, EventArgs e)
        {
            return;
        }

        #endregion

        #region IRepotrTabControl Members

        /// <summary>
        /// Control
        /// </summary>
        public UserControl Control
        {
            get { return (UserControl) this; }
        }

        /// <summary>
        /// ������� ������
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow mRow)
        {
            runToolStripButton.Enabled = false;
            printToolStripButton.Enabled = false;
            ExcelToolStripButton.Enabled = false;
            Application.DoEvents();
            foreach (IAlgorithm alg in algoritms)
            {
                alg.SelectItem(mRow);
                alg.Run();
            }
            runToolStripButton.Enabled = true;
            printToolStripButton.Enabled = true;
            ExcelToolStripButton.Enabled = true;
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        }


        /// <summary>
        /// ������������ ���������� � ��������� �������
        /// </summary>
        /// <returns>String</returns>
        public static readonly string TemplReportDirectoryName = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Reports");


        /// <summary>
        /// ������������ ���������� � �������� Excel ��������
        /// </summary>
        /// <returns>String</returns>
        public static readonly string XslReportDirectoryName = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "UserReport");

        /// <summary>
        /// �������� ������� ����� ���
        /// ������������ ������
        /// </summary>
        protected void LoadPatternPrint(string name)
        {
            //c1Report.Load(Path.Combine(TemplReportDirectoryName, printPattern), name);
        }

        /// <summary>
        /// �������� ������� ����� ��� 
        /// ������������ ������ � Excel
        /// </summary>
        protected void LoadPatternExcel()
        {
            //c1XLBook.Load(Path.Combine(TemplReportDirectoryName, excelPattern));
        }

        /// <summary>
        /// ���������� ����� ������ � Excel
        /// </summary>
        protected void SaveExcel(string file, bool open)
        {
            string xslFile = GetExcelFileName(file);

            try
            {
                // c1XLBook.Save(xslFile);
                if (open)
                    System.Diagnostics.Process.Start(xslFile);
            }
            catch (Exception)
            {
                XtraMessageBox.Show(xslFile.ToString(),
                    String.Format(Resources.FileCreationError, xslFile),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string GetExcelFileName(string file)
        {
            if (!Directory.Exists(XslReportDirectoryName))
                Directory.CreateDirectory(XslReportDirectoryName);

            string xslFile = Path.Combine(XslReportDirectoryName,
                String.Format("{0}{1:ddMMyyHHmm}.xls", file, DateTime.Now));
            return xslFile;
        }

        /// <summary>
        /// ������� ������ ������
        /// </summary>
        public virtual void ClearReport()
        {
            return;
        }

        #endregion


        protected virtual void fieldGrid_CheckStateChanged(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in fieldGridToolStripSplitButton.DropDownItems)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    ((DataGridViewColumn) item.Tag).Visible = true;
                    FindColumn(item.Text).visible = true;
                }
                else
                {
                    ((DataGridViewColumn) item.Tag).Visible = false;
                    FindColumn(item.Text).visible = false;
                }
            }
        }

        protected virtual void toolStripMenuItem1_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected virtual void Grid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        protected virtual void ShowGraphToolStripButton_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// ���������� ��������� ������� Enabled � ������ ������ 
        /// </summary>
        private void printToolStripButton_EnabledChanged(object sender, EventArgs e)
        {
            if (sender is ToolStripButton)
                ValidateReportButtonEnabled((ToolStripButton) sender, printPattern);
        }

        /// <summary>
        /// ���������� ��������� ������� Enabled � ������ �������� � ������
        /// </summary>
        private void ExcelToolStripButton_EnabledChanged(object sender, EventArgs e)
        {
            if (sender is ToolStripItem)
                ValidateReportButtonEnabled((ToolStripItem) sender, excelPattern);
        }

        /// <summary>
        /// �������� ������������ ��������� ������� Enabled � true � ������ ������.
        /// ������ ������ ��������� ������������� � �������� ��������� ���� ���� 
        /// ������� �������.
        /// </summary>
        /// <param name="button">������</param>
        /// <param name="reportName">��� ����� ������� ������</param>
        protected void ValidateReportButtonEnabled(ToolStripItem button, string reportName)
        {
            if (((String.IsNullOrEmpty(reportName))) || (
                (button.Enabled) && (!File.Exists(Path.Combine(TemplReportDirectoryName, reportName)))))
            {
                button.Enabled = false;
            }
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {

        }

        protected void CancelReport()
        {
        }

        protected virtual void btXML_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Resources.ReportTabCtrlExportXmlWarning, caption);
        }

        protected void WriteXML(DataTable dtXML, string FileName)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Title = Resources.ReportTabCtrlSaveXmlReport;
                dlg.FileName = FileName + ".xml";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    dtXML.Columns.AddRange(new DataColumn[]
                    {
                        new DataColumn("DateStart", typeof (DateTime)),
                        new DataColumn("DateEnd", typeof (DateTime))
                    });
                    foreach (DataRow drow in dtXML.Rows)
                    {
                        drow["DateStart"] = Algorithm.Period.Begin;
                        drow["DateEnd"] = Algorithm.Period.End;
                    }
                    dtXML.WriteXml(FileName + ".xml", XmlWriteMode.WriteSchema);
                }
            }
        }

        public string CurrentActivateReport
        {
            get { return nameReport; }
            set { nameReport = value; }
        }

        #region Nested classes

        /// <summary>
        /// ����� ��� �������� ������� � Excel.
        /// </summary>
        public enum ExcelExport
        {
            Current = 1, // ������� ������ �� ������� ������
            All = 2 // ������� ������ �� ���� ��������� �������
        }

        /// <summary>
        /// ������� ��������� ������� �����
        /// </summary>
        [Serializable]
        public class StateColumn : ISerializable
        {
            /// <summary>
            /// �������� �������
            /// </summary>
            public string name;

            /// <summary>
            /// ��������� �������
            /// </summary>
            public bool visible;

            public StateColumn()
            {
            }

            public StateColumn(string name, bool visible)
            {
                this.name = name;
                this.visible = visible;
            }

            public StateColumn(SerializationInfo info, StreamingContext context)
            {
                name = info.GetString("Name");
                visible = info.GetBoolean("Visible");
            }

            #region ISerializable Members

            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("Name", name);
                info.AddValue("Visible", visible);
            }

            #endregion
        }

        #endregion
    }
}
