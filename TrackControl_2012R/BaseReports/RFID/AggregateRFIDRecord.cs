﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseReports.Procedure;
using BaseReports.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.RFID
{
    public class AggregateRFIDRecord : Algorithm
    {
        public int IdMobitel { get; set; }

        public GpsData PrevDataGps { get; set; }

        public Aggregat Aggregate { get; set; }

        public ushort IdentRfid { get; set; }

        public string IdentRfidName { get; set; }

        public string NameForReport
        {
            get
            {
                if (Aggregate == null || (Aggregate.Identifier == null &&
                                          IdentRfid != RFID_MISSING &&
                                          IdentRfid != RFID_NOT_WORKING))
                    return IdentRfid.ToString();
                else
                    return Aggregate.AggregateName;
            }
        }

        List<GpsData> _dataGps;

        public List<GpsData> DataGps
        {
            get { return _dataGps; }
            set
            {
                _dataGps = value;
                if (_dataGps.Count <= 0) return;
                _startGps = _dataGps[0];
                _finalGps = _dataGps[_dataGps.Count - 1];
            }
        }

        GpsData _startGps;

        public GpsData StartGps
        {
            get { return _startGps; }
            set { _startGps = value; }
        }

        GpsData _finalGps;

        public GpsData FinalGps
        {
            get { return _finalGps; }
            set { _finalGps = value; }
        }


        public string LocationStart { get; set; }

        public string LocationEnd { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public TimeSpan TimeDuration { get; set; }

        public double Distance { get; set; }

        public TimeSpan TimeStops { get; set; }

        public AggregateRFIDRecord(int idMobitel)
        {
            _dataGps = new List<GpsData>();
            PrevDataGps = null;
            IdMobitel = idMobitel;
        }

        public void SetRecordParameters(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            PartialAlgorithms PA = new PartialAlgorithms(m_row);
            Distance = Math.Round(BasicMethods.GetKilometrage(_dataGps.ToArray()), 2);
            TimeDuration = TimeEnd.Subtract(TimeStart);
            TimeStops = BasicMethods.GetTotalStopsTime(_dataGps.ToArray(), 0);
        }

        public void SetIdentRfidName(UInt16 identRfid, long sensorLenght)
        {
            IdentRfid = identRfid;

            if (IdentRfid == (UInt16)((1 << (int)sensorLenght) - 1))
            {
                IdentRfidName = Resources.RfidNoCard;
            }
            else if (IdentRfid == RFID_NOT_WORKING)
            {
                IdentRfidName = Resources.RfidNoReader;
            }
            else
            {
                IdentRfidName = IdentRfid.ToString();
            }
        }
    } // AggregateRFIDRecord
}
