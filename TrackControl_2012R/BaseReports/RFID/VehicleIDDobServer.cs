﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.Vehicles;
using TrackControl.General;

namespace BaseReports.RFID
{
    /// <summary>
    /// управление одним RFID идентификатором
    /// </summary>
    public class VehicleIDDobServer : IDisposable 
    {
        /// <summary>
        /// количество антидребезговых точек
        /// </summary>
       public readonly int maxBouncePoints = 1;

        int countBouncePoints;

        /// <summary>
        /// пропуски в рамках дребезга при активном наблюдателе
        /// </summary>
        List<GpsData> _gapGPSs = new List<GpsData>();

        /// <summary>
        /// набор геоточек обнаруженный наблюдателем идентификатора 
        /// </summary>
        List<GpsData> _dataGPSs = new List<GpsData>() ;

        public List<GpsData> DataGPSs
        {
            get { return _dataGPSs; }
            set {_dataGPSs = value;}
        }

        public event Action<UInt16> CreateRecord;
        public event Action<UInt16> StartRecord;

        Vehicle _vehicle;
        public Vehicle vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        UInt16? _identifier;

        bool _isStateActive = false;

        public bool IsStateActive
        {
            get { return _isStateActive; }
            set { _isStateActive = value; }
        }

        public VehicleIDDobServer(UInt16? identifier, Vehicle vehicle)
        {
            _vehicle = vehicle;
            _identifier = identifier;
        }

        public void OnReceiveRFID(UInt16 tagRFID, GpsData dataGPS, bool firstPoint, bool lastPoint)
        {
            SetFirstPoint(tagRFID, dataGPS, firstPoint);

            if (tagRFID == _identifier)
            {
                if (_isStateActive && _gapGPSs.Count > 0)
                {
                    foreach (GpsData dgps in _gapGPSs)
                    {
                        if (!_dataGPSs.Contains(dgps)) _dataGPSs.Add(dgps); 
                    }
                    _gapGPSs.Clear(); 
                }
                if (_isStateActive && !_dataGPSs.Contains(dataGPS)) 
                    _dataGPSs.Add(dataGPS);

                SetStateConsideringBounce(false, dataGPS);
            }
            else
            {
                SetStateConsideringBounce(true, dataGPS);
            }

            SetLastPoint(dataGPS, lastPoint);
        }

        /// <summary>
        /// смена состояния с учетом дребезга 
        /// </summary>
        /// <param name="stateForTest"> текущее состояние</param>
        /// <param name="dataGPS">время поступления новой метки</param>
        private void SetStateConsideringBounce(bool stateForTest, GpsData dataGPS)
        {
            if (_isStateActive == stateForTest)
            {
                if (countBouncePoints < maxBouncePoints)
                {
                    if (!_isStateActive && (!_dataGPSs.Contains(dataGPS))) _dataGPSs.Add(dataGPS);
                    if (_isStateActive && (!_gapGPSs.Contains(dataGPS))) _gapGPSs.Add(dataGPS);
                    countBouncePoints++;
                }
                else
                {
                    countBouncePoints = 0;
                    _isStateActive = !stateForTest;

                    if (_isStateActive)
                    {
                        if (!stateForTest && !_dataGPSs.Contains(dataGPS))
                        {
                            _dataGPSs.Add(dataGPS);
                            _gapGPSs.Clear(); 
                        }
                        //if (stateForTest && !_gapGPSs.Contains(dataGPS)) _gapGPSs.Add(dataGPS);
                        StartRecord((UInt16)_identifier);
                        }
                    else
                    {
                        CreateRecord((UInt16)_identifier);
                    }
                }
            }
            else
            {
                countBouncePoints = 0;
                if (!_isStateActive ) 
                {
                    _dataGPSs.Clear() ;
                    _gapGPSs.Clear();
                }
            }
        }

        private void SetFirstPoint(UInt16 tagRFID, GpsData dataGPS, bool firstPoint)
        {
            if (firstPoint && tagRFID == _identifier)
            {
                _isStateActive = true;
                if (!_dataGPSs.Contains(dataGPS)) _dataGPSs.Add(dataGPS);
            }
        }

        private void SetLastPoint(GpsData dataGPS, bool lastPoint)
        {
            if (lastPoint && _isStateActive)
            {
                if (!_dataGPSs.Contains(dataGPS)) _dataGPSs.Add(dataGPS);
                CreateRecord((UInt16)_identifier);
                return;
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            _dataGPSs.Clear();
            _gapGPSs.Clear();  
        }

        #endregion
    }
}
