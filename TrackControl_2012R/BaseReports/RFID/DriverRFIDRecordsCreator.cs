﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.RFID
{
    public class DriverRFIDRecordsCreator : IDisposable
    {
        public delegate void SendRfidTime(UInt16 rfid, GpsData dataGps, bool firstPoint, bool lastPoint);
        public event SendRfidTime SendRfid;
        public event Action<int> ProgressChanged;
        bool _IsFuelSensorPresent;
        bool _IsRotateSensorPresent;

        List<GpsData> _notRecordedGps;

        Sensor _sensorRFID;

        int _idMobitel;

        public int IdMobitel
        {
            get { return _idMobitel; }
            set { 
                _idMobitel = value; 
                Vehicle vh = new Vehicle(_idMobitel);
                _defaultDriver = vh.Driver;
                _sensorRFID = new Sensor((int)AlgorithmType.DRIVER, _idMobitel);
                }
        }

        GpsData[] _dRows;
        // наблюдатели за идентификаторами
        Dictionary<UInt16, DriverRFIDobserver> _driver_observers;

        Driver _defaultDriver;

        public atlantaDataSet.mobitelsRow m_row;
        
        public DriverRFIDRecordsCreator()
        {
            _driver_observers = new Dictionary<ushort, DriverRFIDobserver>();
            if (Algorithm.DicRfidRecords == null) 
                Algorithm.DicRfidRecords = new Dictionary<int, List<DriverRFIDRecord>>();
            _notRecordedGps = new List<GpsData>();
        }

        public void WriteNoRecordedGps()
        {
            if (_notRecordedGps.Count > 0)
            {
                WriteRecord(_notRecordedGps, _defaultDriver, Algorithm.Rfid_New_Missing, false);
                _notRecordedGps.Clear();
            }
            
        }

        List<UInt16> rfids = new List<ushort>(); 

        public bool CuttingDataGpsByRfid(GpsData[] d_rows)
        {
            _dRows=d_rows;
            int cnt = 0;

            foreach (GpsData d in _dRows)
            {
                ++cnt;

                if (d.Valid)
                {
                    if (_sensorRFID != null && _sensorRFID.Valid)
                    {
                        UInt16 tagRfid = (UInt16) _sensorRFID.GetValue(d.Sensors);

                        if (tagRfid < Algorithm.Rfid_New_Missing && tagRfid > Algorithm.RFID_NOT_WORKING)
                        {
                            rfids.Add(tagRfid);
                        }

                        if (tagRfid != 0)
                        {
                            if (!_driver_observers.ContainsKey(tagRfid))
                            {
                                Driver driver = new Driver(0, tagRfid);

                                if (driver.Identifier == tagRfid || driver.Identifier == null)
                                {
                                    SubscribeObserver(tagRfid, driver);
                                }
                            }
                        }

                        SetEvent(d, tagRfid);
                    }
                    else
                    {
                        SetEvent(d, Algorithm.RFID_NOT_WORKING);
                    }

                    if (cnt % 64 == 0)
                    {
                        if (ProgressChanged != null) 
                            ProgressChanged(cnt);
                    }
                } // if

                if (Algorithm.StopRun)
                {
                    return false;
                }
            } // foreach

            return true;
        } // CuttingDataGpsByRfid

        private void UnSubscribeObservers()
        {
            foreach (var kvp in _driver_observers)
            {
                UnSubscribeObserver(kvp.Key);
            }
            _driver_observers.Clear();
        }

        private void SetEvent(GpsData d, UInt16 tagRfid)
        {
            if (SendRfid != null)
            {
                if (d.Id == _dRows[0].Id)
                {
                    SendRfid(tagRfid, d, true, false);
                    _notRecordedGps.Add(d);
                }
                else if (d.Id == _dRows[_dRows.Length - 1].Id)
                    SendRfid(tagRfid, d, false, true);
                else
                {
                    SendRfid(tagRfid, d, false, false);
                    _notRecordedGps.Add(d);
                }
            }
        }

        private void SubscribeObserver(UInt16 tagRfid, Driver driver)
        {
            if (_driver_observers.ContainsKey((tagRfid)))
                _driver_observers.Remove(tagRfid);

            var rfidObserver = new DriverRFIDobserver(tagRfid, driver);
            SendRfid += rfidObserver.OnReceiveRFID;
            rfidObserver.CreateRecord += OnCreateRecord;
            rfidObserver.StartRecord += OnStartRecord;
            _driver_observers.Add(tagRfid, rfidObserver);
        }
        
        public void SetDefaultObservers()
        {
            SubscribeObserver(Algorithm.Rfid_New_Missing, _defaultDriver);
            SubscribeObserver(Algorithm.RFID_NOT_WORKING, _defaultDriver);
        }

        void OnCreateRecord(UInt16 trackRfid)
        {
            if (!_driver_observers.ContainsKey(trackRfid)) 
                return;

            WriteRecord(_driver_observers[trackRfid].DataGPSs, _driver_observers[trackRfid].Driver, trackRfid, true);
            ClearWritedPoints(trackRfid);
        }

        private void ClearWritedPoints(UInt16 trackRfid)
        {
            foreach (KeyValuePair<UInt16, DriverRFIDobserver> kvp in _driver_observers)
            {
                if (kvp.Key != trackRfid)
                {
                    if ((_driver_observers[trackRfid].DataGPSs.Count - _driver_observers[trackRfid].maxBouncePoints) > 0)
                    {
                        for (int i = _driver_observers[trackRfid].DataGPSs.Count - _driver_observers[trackRfid].maxBouncePoints; i < _driver_observers[trackRfid].DataGPSs.Count; i++)
                        {
                            if (_driver_observers[kvp.Key].DataGPSs.Contains(_driver_observers[trackRfid].DataGPSs[i]))
                                _driver_observers[kvp.Key].DataGPSs.Remove(_driver_observers[trackRfid].DataGPSs[i]);
                        }
                    }
                }
            }
            _driver_observers[trackRfid].DataGPSs.Clear();
        }

        void WriteRecord(List<GpsData> dataGPS, Driver driver, UInt16 trackRfid, bool clearNotRecordedGps)
        {
            if (dataGPS.Count == 0) 
                return;
            GpsData dataGPSstart = dataGPS[0];
            GpsData dataGPSend = dataGPS[dataGPS.Count - 1];
            DriverRFIDRecord record = new DriverRFIDRecord(_idMobitel, _IsFuelSensorPresent, _IsRotateSensorPresent);

            record.Driver = driver;
            record.DataGps = dataGPS;
            record.SetIdentRfidName(trackRfid,_sensorRFID.BitLength);
            record.TimeStart = dataGPS[0].Time ;
            record.TimeEnd = dataGPS[dataGPS.Count - 1].Time;
            int firstIndex = Array.IndexOf(_dRows, dataGPS[0]);

            if (firstIndex > 0)
            {
                record.PrevDataGps = _dRows[firstIndex - 1];
            }
            if (m_row != null)
            {
                record.LocationStart = Algorithm.FindLocation(dataGPSstart.LatLng);
                record.LocationEnd = Algorithm.FindLocation(dataGPSend.LatLng);
                record.SetRecordParameters(m_row);
            }
            Algorithm.DicRfidRecords[_idMobitel].Add(record);
#if DEBUG
            //string gps_id ="";
            //foreach (GpsData dgps in dataGPS)
            //{
            //    gps_id = string.Format("{0} {1}", gps_id, dgps.Id);
            //}
            //Console.WriteLine("Водитель {0}  старт {1} стоп {2} gps {3} ", driver.FullName,
            //    dataGPSstart.Id, dataGPSend.Id, gps_id);
#endif
            if (clearNotRecordedGps && _notRecordedGps.Count > 0)
            {
                foreach (GpsData dgps in dataGPS)
                {
                    if (_notRecordedGps.Contains(dgps)) _notRecordedGps.Remove(dgps);
                }
            }
        }

        void OnStartRecord(UInt16 trackRfid)
        {
            if (!_driver_observers.ContainsKey(trackRfid)) return;
            if (_notRecordedGps.Count > 0)
            {

                foreach (GpsData dgps in _driver_observers[trackRfid].DataGPSs)
                {
                    if (_notRecordedGps.Contains(dgps)) _notRecordedGps.Remove(dgps);
                }
                //если между сбросом одного наблюдателя и стартом следующего осталось нераспределенных точек меньше дребезга
                //передаем их активному наблюдателю
                if (_driver_observers[trackRfid].maxBouncePoints >= _notRecordedGps.Count)
                {
                    foreach (GpsData dgps in _driver_observers[trackRfid].DataGPSs)
                    {
                        if (!_notRecordedGps.Contains(dgps)) _notRecordedGps.Add(dgps);
                    }
                    _driver_observers[trackRfid].DataGPSs.Clear();
                    foreach (GpsData dgps in _notRecordedGps)
                    {
                        _driver_observers[trackRfid].DataGPSs.Add(dgps);
                    }
                    //_driver_observers[trackRfid].DataGPSs = _notRecordedGps;
                    _notRecordedGps.Clear();
                }
                if (CountActiveObserverPresent() == 1 && _notRecordedGps.Count > 0)
                {
                    WriteNoRecordedGps();

                }
            }
        }

        private void UnSubscribeObserver(UInt16 trackRfid)
        {
            SendRfid -= _driver_observers[trackRfid].OnReceiveRFID;
            _driver_observers[trackRfid].Dispose();
        }

        int CountActiveObserverPresent()
        {
            int cnt = 0;
            foreach (var kvp in _driver_observers)
            {
                if (kvp.Value.IsStateActive) cnt++;
            }
            return cnt;
        }

        private void DefineFuelSensor()
        {
            Sensor fuelSensor = new Sensor((int)AlgorithmType.FUEL1, _idMobitel);
            if (fuelSensor.Id >0) _IsFuelSensorPresent = true;
        }

        private void DefineRotateSensor()
        {
            Sensor rotateSensor = new Sensor((int)AlgorithmType.ROTATE_E, _idMobitel);
            if (rotateSensor.Id >0) _IsRotateSensorPresent = true;
        }

        public void DefineSensors()
        {
            DefineRotateSensor();
            DefineFuelSensor();
        }

        #region Testing
        double _lat = 29882160;
        double _lon = 18453960;
        int _id_mobitel_test = 174;
        ulong _sensor1 = 3458765403952517120;
        ulong _sensor2 = 3458765042101522432;
        int _id_gps = 0;

        [Conditional("DEBUG")]
        public void RunTest()
        {
            _idMobitel = _id_mobitel_test;
            TestSequence();
            Console.WriteLine("START");
            //Run();
        }

        [Conditional("DEBUG")]
        void TestSequence()
        {
            TestSequence1();
            //TestSequence2();
        }

        void TestSequence1()
        {
            if (_dRows != null && _dRows.Length != 0) Array.Clear(_dRows, 0, _dRows.Length);
            List<GpsData> gds = new List<GpsData>();
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            _dRows = gds.ToArray();
        }

        void TestSequence2()
        {
            if (_dRows != null && _dRows.Length != 0) Array.Clear(_dRows, 0, _dRows.Length);
            List<GpsData> gds = new List<GpsData>();
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            _dRows = gds.ToArray();
        }

        private void TestInsertRFID(ref List<GpsData> gds, ulong sensor)
        {
            GpsData gd = new GpsData();
            gd.Id = ++_id_gps;
            gd.Valid = true;
            gd.Sensors = BitConverter.GetBytes(sensor);
            gd.LatLng = new PointLatLng(_lat, _lon);
            gd.Mobitel = _id_mobitel_test;
            gds.Add(gd);
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            UnSubscribeObservers();
        }

        #endregion
    }
}
