﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BaseReports.Procedure;
using DevExpress.XtraRichEdit;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.RFID
{
    public class AggregateRFIDRecordsCreator : IDisposable
    {
        public delegate void SendRfidTime(UInt16 rfid, GpsData dataGps, bool firstPoint, bool lastPoint);

        public event SendRfidTime SendRfid;
        public event Action<int> ProgressChanged;
        private List<GpsData> _notRecordedGps;
        private Sensor _sensorRFID;
        private int _idMobitel;
        Aggregat _defaultAggregate;

        public int IdMobitel
        {
            get { return _idMobitel; }
            set
            {
                _idMobitel = value;
                Vehicle vh = new Vehicle(_idMobitel);
                vh.VehicleInfoAggregate(_idMobitel);
                _defaultAggregate = vh.Aggregate;
                _sensorRFID = new Sensor((int) AlgorithmType.AGREGAT, _idMobitel);
            }
        }

        private GpsData[] _dRows;
        private Dictionary<UInt16, AggregateRFIDObserver> _aggregate_observers;
        public atlantaDataSet.mobitelsRow m_row;

        public AggregateRFIDRecordsCreator()
        {
            _aggregate_observers = new Dictionary<ushort, AggregateRFIDObserver>();
            if (Algorithm.AggregateRfidRecords == null)
                Algorithm.AggregateRfidRecords = new Dictionary<int, List<AggregateRFIDRecord>>();
            _notRecordedGps = new List<GpsData>();
        }

        public void WriteNoRecordedGps()
        {
            if (_notRecordedGps.Count > 0)
            {
                WriteRecord(_notRecordedGps, _defaultAggregate, Algorithm.Rfid_New_Missing, false);
                _notRecordedGps.Clear();
            }
        }

        private List<UInt16> rfids = new List<ushort>();

        public bool CuttingDataGpsByRfid(GpsData[] d_rows)
        {
            _dRows = d_rows;
            int cnt = 0;

            foreach (GpsData d in _dRows)
            {
                ++cnt;
                if (d.Valid)
                {
                    if (_sensorRFID != null && _sensorRFID.Valid)
                    {
                        UInt16 tagRfid = (UInt16) _sensorRFID.GetValue(d.Sensors);
                        if (tagRfid < Algorithm.Rfid_New_Missing && tagRfid > Algorithm.RFID_NOT_WORKING)
                        {
                            rfids.Add(tagRfid);
                        }

                        if (tagRfid != 0)
                        {
                            if (!_aggregate_observers.ContainsKey(tagRfid))
                            {
                                Aggregat aggregate = new Aggregat(m_row.Mobitel_ID, tagRfid);
                                if (aggregate.Identifier == tagRfid || aggregate.Identifier == 0)
                                {
                                    SubscribeObserver(tagRfid, aggregate);
                                }
                            }
                        }

                        SetEvent(d, tagRfid);
                    }
                    else
                    {
                        SetEvent(d, Algorithm.RFID_NOT_WORKING);
                    }

                    if (cnt % 64 == 0)
                    {
                        if (ProgressChanged != null)
                            ProgressChanged(cnt);
                    }
                }

                if (Algorithm.StopRun)
                {
                    return false;
                }
            }

            return true;
        } // CuttingDataGpsByRfid

        private void UnSubscribeObservers()
        {
            foreach (var kvp in _aggregate_observers)
            {
                UnSubscribeObserver(kvp.Key);
            }

            _aggregate_observers.Clear();
        }

        private void SetEvent(GpsData d, UInt16 tagRfid)
        {
            if (SendRfid != null)
            {
                if (d.Id == _dRows[0].Id)
                {
                    SendRfid(tagRfid, d, true, false);
                    _notRecordedGps.Add(d);
                }
                else if (d.Id == _dRows[_dRows.Length - 1].Id)
                    SendRfid(tagRfid, d, false, true);
                else
                {
                    SendRfid(tagRfid, d, false, false);
                    _notRecordedGps.Add(d);
                }
            }
        }

        private void SubscribeObserver(UInt16 tagRfid, Aggregat aggregate)
        {
            if (_aggregate_observers.ContainsKey(tagRfid))
                _aggregate_observers.Remove(tagRfid);

            var rfidObserver = new AggregateRFIDObserver(tagRfid, aggregate);
            SendRfid += rfidObserver.OnReceiveRFID;
            rfidObserver.CreateRecord += OnCreateRecord;
            rfidObserver.StartRecord += OnStartRecord;
            _aggregate_observers.Add(tagRfid, rfidObserver);
        }

        public void SetDefaultObservers()
        {
            SubscribeObserver(Algorithm.Rfid_New_Missing, _defaultAggregate);
            SubscribeObserver(Algorithm.RFID_NOT_WORKING, _defaultAggregate);
        }

        void OnCreateRecord(UInt16 trackRfid)
        {
            if (!_aggregate_observers.ContainsKey(trackRfid))
                return;

            WriteRecord(_aggregate_observers[trackRfid].DataGPSs, _aggregate_observers[trackRfid].Aggregate, trackRfid,
                true);
            ClearWritedPoints(trackRfid);
        }

        private void ClearWritedPoints(UInt16 trackRfid)
        {
            foreach (KeyValuePair<UInt16, AggregateRFIDObserver> kvp in _aggregate_observers)
            {
                if (kvp.Key != trackRfid)
                {
                    if ((_aggregate_observers[trackRfid].DataGPSs.Count -
                        _aggregate_observers[trackRfid].maxBouncePoints) > 0)
                    {
                        for (int i = _aggregate_observers[trackRfid].DataGPSs.Count -
                                     _aggregate_observers[trackRfid].maxBouncePoints;
                            i < _aggregate_observers[trackRfid].DataGPSs.Count;
                            i++)
                        {
                            if (_aggregate_observers[kvp.Key].DataGPSs
                                .Contains(_aggregate_observers[trackRfid].DataGPSs[i]))
                                _aggregate_observers[kvp.Key].DataGPSs
                                    .Remove(_aggregate_observers[trackRfid].DataGPSs[i]);
                        }
                    }
                }
            }

            _aggregate_observers[trackRfid].DataGPSs.Clear();
        }

        void WriteRecord(List<GpsData> dataGPS, Aggregat aggregate, UInt16 trackRfid, bool clearNotRecordedGps)
        {
            if (dataGPS.Count == 0)
                return;
            GpsData dataGPSstart = dataGPS[0];
            GpsData dataGPSend = dataGPS[dataGPS.Count - 1];
            AggregateRFIDRecord record = new AggregateRFIDRecord(_idMobitel);

            record.Aggregate = aggregate;
            record.DataGps = dataGPS;
            record.SetIdentRfidName(trackRfid, _sensorRFID.BitLength);
            record.TimeStart = dataGPS[0].Time;
            record.TimeEnd = dataGPS[dataGPS.Count - 1].Time;
            int firstIndex = Array.IndexOf(_dRows, dataGPS[0]);

            if (firstIndex > 0)
            {
                record.PrevDataGps = _dRows[firstIndex - 1];
            }

            if (m_row != null)
            {
                record.LocationStart = Algorithm.FindLocation(dataGPSstart.LatLng);
                record.LocationEnd = Algorithm.FindLocation(dataGPSend.LatLng);
                record.SetRecordParameters(m_row);
            }

            Algorithm.AggregateRfidRecords[_idMobitel].Add(record);

            if (clearNotRecordedGps && _notRecordedGps.Count > 0)
            {
                foreach (GpsData dgps in dataGPS)
                {
                    if (_notRecordedGps.Contains(dgps))
                        _notRecordedGps.Remove(dgps);
                }
            }
        }

        void OnStartRecord(UInt16 trackRfid)
        {
            if (!_aggregate_observers.ContainsKey(trackRfid)) return;
            if (_notRecordedGps.Count > 0)
            {
                foreach (GpsData dgps in _aggregate_observers[trackRfid].DataGPSs)
                {
                    if (_notRecordedGps.Contains(dgps)) _notRecordedGps.Remove(dgps);
                }

                if (_aggregate_observers[trackRfid].maxBouncePoints >= _notRecordedGps.Count)
                {
                    foreach (GpsData dgps in _aggregate_observers[trackRfid].DataGPSs)
                    {
                        if (!_notRecordedGps.Contains(dgps)) _notRecordedGps.Add(dgps);
                    }
                    _aggregate_observers[trackRfid].DataGPSs.Clear();
                    foreach (GpsData dgps in _notRecordedGps)
                    {
                        _aggregate_observers[trackRfid].DataGPSs.Add(dgps);
                    }
                    //_driver_observers[trackRfid].DataGPSs = _notRecordedGps;
                    _notRecordedGps.Clear();
                }
                if (CountActiveObserverPresent() == 1 && _notRecordedGps.Count > 0)
                {
                    WriteNoRecordedGps();

                }
            }
        }

        private void UnSubscribeObserver(UInt16 trackRfid)
        {
            SendRfid -= _aggregate_observers[trackRfid].OnReceiveRFID;
            _aggregate_observers[trackRfid].Dispose();
        }

        int CountActiveObserverPresent()
        {
            int cnt = 0;
            foreach (var kvp in _aggregate_observers)
            {
                if (kvp.Value.IsStateActive) cnt++;
            }
            return cnt;
        }
        
        #region Testing
        double _lat = 29882160;
        double _lon = 18453960;
        int _id_mobitel_test = 174;
        ulong _sensor1 = 3458765403952517120;
        ulong _sensor2 = 3458765042101522432;
        int _id_gps = 0;

        [Conditional("DEBUG")]
        public void RunTest()
        {
            _idMobitel = _id_mobitel_test;
            TestSequence();
            Console.WriteLine("START");
            //Run();
        }

        [Conditional("DEBUG")]
        void TestSequence()
        {
            TestSequence1();
            //TestSequence2();
        }

        void TestSequence1()
        {
            if (_dRows != null && _dRows.Length != 0) Array.Clear(_dRows, 0, _dRows.Length);
            List<GpsData> gds = new List<GpsData>();
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            _dRows = gds.ToArray();
        }

        void TestSequence2()
        {
            if (_dRows != null && _dRows.Length != 0) Array.Clear(_dRows, 0, _dRows.Length);
            List<GpsData> gds = new List<GpsData>();
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 0);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, 1023);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor2);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            TestInsertRFID(ref gds, _sensor1);
            _dRows = gds.ToArray();
        }

        private void TestInsertRFID(ref List<GpsData> gds, ulong sensor)
        {
            GpsData gd = new GpsData();
            gd.Id = ++_id_gps;
            gd.Valid = true;
            gd.Sensors = BitConverter.GetBytes(sensor);
            gd.LatLng = new PointLatLng(_lat, _lon);
            gd.Mobitel = _id_mobitel_test;
            gds.Add(gd);
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            UnSubscribeObservers();
        }

        #endregion
    }
}