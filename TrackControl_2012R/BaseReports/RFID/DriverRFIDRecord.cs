﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using TrackControl.Vehicles;
using TrackControl.Reports;
using BaseReports.Procedure;
using BaseReports.Properties;

namespace BaseReports.RFID
{
    public class DriverRFIDRecord : Algorithm
    {
        bool _IsFuelSensorPresent;

        bool _IsRotateSensorPresent;

        public int IdMobitel { get; set; }

        public GpsData PrevDataGps { get; set; }

        public Driver Driver { get; set; }

        public ushort IdentRfid { get; set; }

        public string IdentRfidName { get; set; }

        public string NameForReport
        {
            get
            {
                if (Driver == null || (Driver.Identifier == null && IdentRfid != RFID_MISSING &&
                                       IdentRfid != RFID_NOT_WORKING))
                    return IdentRfid.ToString();
                else
                    return Driver.FullName;
            }
        }

        List<GpsData> _dataGps;

        public List<GpsData> DataGps
        {
            get { return _dataGps; }
            set
            {
                _dataGps = value;
                if (_dataGps.Count <= 0) return;
                _startGps = _dataGps[0];
                _finalGps = _dataGps[_dataGps.Count - 1];
            }
        }

        GpsData _startGps;

        public GpsData StartGps
        {
            get { return _startGps; }
            set { _startGps = value; }
        }

        GpsData _finalGps;

        public GpsData FinalGps
        {
            get { return _finalGps; }
            set { _finalGps = value; }
        }


        public string LocationStart { get; set; }

        public string LocationEnd { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public TimeSpan TimeDuration { get; set; }

        public double Distance { get; set; }

        public TimeSpan TimeEngineOn { get; set; }

        public TimeSpan TimeStops { get; set; }

        public TimeSpan TimeEngineOnStops { get; set; }

        double _fuelStart;

        public double FuelStart
        {
            get { return _fuelStart; }
            set { _fuelStart = value; }
        }

        double _fuelEnd;

        public double FuelEnd
        {
            get { return _fuelEnd; }
            set { _fuelEnd = value; }
        }

        double _fuelAdd;

        public double FuelAdd
        {
            get { return _fuelAdd; }
            set { _fuelAdd = value; }
        }

        double _fuelSub;

        public double FuelSub
        {
            get { return _fuelSub; }
            set { _fuelSub = value; }
        }

        double _fuelExpense;

        public double FuelExpense
        {
            get { return _fuelExpense; }
            set { _fuelExpense = value; }
        }

        double _fuelExpenseAvgKm;

        public double FuelExpenseAvgKm
        {
            get { return _fuelExpenseAvgKm; }
            set { _fuelExpenseAvgKm = value; }
        }

        double _fuelExpenseAvgHour;

        public double FuelExpenseAvgHour
        {
            get { return _fuelExpenseAvgHour; }
            set { _fuelExpenseAvgHour = value; }
        }

        public DriverRFIDRecord(int idMobitel, bool isFuelSensorPresent, bool isRotateSensorPresent)
        {
            _dataGps = new List<GpsData>();
            PrevDataGps = null;
            IdMobitel = idMobitel;
            _IsFuelSensorPresent = isFuelSensorPresent;
            _IsRotateSensorPresent = isRotateSensorPresent;
        }

        public void SetRecordParameters(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            PartialAlgorithms PA = new PartialAlgorithms(m_row);
            Distance = Math.Round(BasicMethods.GetKilometrage(_dataGps.ToArray()), 2);
            TimeDuration = TimeEnd.Subtract(TimeStart);
            TimeStops = BasicMethods.GetTotalStopsTime(_dataGps.ToArray(), 0);
            if (_IsRotateSensorPresent)
            {
                TimeEngineOnStops = TimeStops.Subtract(PA.GetTimeStopEnginOff(_dataGps.ToArray(), 0, 0));
                TimeEngineOn = BasicMethods.GetTotalMotionTime(_dataGps.ToArray()).Add(TimeEngineOnStops);
            }

            if (_IsFuelSensorPresent)
            {
                _fuelExpense = PA.TotalExpenseDUT(_dataGps.ToArray(), out _fuelStart, out _fuelEnd, out _fuelAdd,
                    out _fuelSub);
                if (_fuelExpense < 0) _fuelExpense = 0;
                //Ср. расход л/100 км
                if (Distance > 0) _fuelExpenseAvgKm = Math.Round(100 * _fuelExpense / Distance, 2);
                //Ср. расход л/ч
                if (TimeEngineOn.TotalHours > 0)
                    _fuelExpenseAvgHour = Math.Round(_fuelExpense / TimeEngineOn.TotalHours, 2);
            }
        }

        public void SetIdentRfidName(UInt16 identRfid, long sensorLenght)
        {
            IdentRfid = identRfid;

            if (IdentRfid == (UInt16) ((1 << (int) sensorLenght) - 1))
            {
                IdentRfidName = Resources.RfidNoCard;
            }
            else if (IdentRfid == RFID_NOT_WORKING)
            {
                IdentRfidName = Resources.RfidNoReader;
            }
            else
            {
                IdentRfidName = IdentRfid.ToString();
            }
        } // DriverRFIDRecord
    }
}
