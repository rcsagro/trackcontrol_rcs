﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseReports.Procedure;
using BaseReports.Properties;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace BaseReports.RFID
{
    public class VehicleIDRecord : Algorithm 
    {
        bool _IsFuelSensorPresent;
        bool _IsRotateSensorPresent;

        public int IdMobitel { get; set; }
        public GpsData PrevDataGps { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }

        public Vehicle Vehicle { get; set; }

        GpsData _startGps;
        List<GpsData> _dataGps;

        public GpsData StartGps
        {
            get { return _startGps; }
            set { _startGps = value; }
        }

        GpsData _finalGps;

        public GpsData FinalGps
        {
            get { return _finalGps; }
            set { _finalGps = value; }
        }

        double _fuelStart;

        double _fuelEnd;

        public double FuelEnd
        {
            get { return _fuelEnd; }
            set { _fuelEnd = value; }
        }

        double _fuelAdd;

        double _fuelSub;

        double _fuelExpense;

        double _fuelExpenseAvgKm;

        double _fuelExpenseAvgHour;

        public ushort IdentID { get; set; }

        public string NameForReport
        {
            get
            {
                if (Vehicle == null || (Vehicle.CarMaker == null &&
                                        IdentID != RFID_MISSING && IdentID != RFID_NOT_WORKING))
                {
                    return IdentID.ToString();
                }
                else
                {
                    return Vehicle.Info;
                }
            }
        }

        public VehicleIDRecord(int idMobitel, bool isFuelSensorPresent, bool isRotateSensorPresent)
        {
            _dataGps = new List<GpsData>();
            PrevDataGps = null;
            IdMobitel = idMobitel;
            _IsFuelSensorPresent = isFuelSensorPresent;
            _IsRotateSensorPresent = isRotateSensorPresent;
        }

        public List<GpsData> DataGps
        {
            get { return _dataGps; }
            set
            {
                _dataGps = value;
                if (_dataGps.Count <= 0) return;
                _startGps = _dataGps[0];
                _finalGps = _dataGps[_dataGps.Count - 1];
            }
        }

        public void SetRecordParameters(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            PartialAlgorithms PA = new PartialAlgorithms(m_row);
            Distance = Math.Round(BasicMethods.GetKilometrage(_dataGps.ToArray()), 2);
            TimeDuration = TimeEnd.Subtract(TimeStart);
            TimeStops = BasicMethods.GetTotalStopsTime(_dataGps.ToArray(), 0);
            if (_IsRotateSensorPresent)
            {
                TimeEngineOnStops = TimeStops.Subtract(PA.GetTimeStopEnginOff(_dataGps.ToArray(), 0, 0));
                TimeEngineOn = BasicMethods.GetTotalMotionTime(_dataGps.ToArray()).Add(TimeEngineOnStops);
            }
            if (_IsFuelSensorPresent)
            {
                _fuelExpense = PA.TotalExpenseDUT(_dataGps.ToArray(), out _fuelStart, out _fuelEnd, out _fuelAdd, out _fuelSub);
                if (_fuelExpense < 0) _fuelExpense = 0;
                //Ср. расход л/100 км
                if (Distance > 0) _fuelExpenseAvgKm = Math.Round(100 * _fuelExpense / Distance, 2);
                //Ср. расход л/ч
                if (TimeEngineOn.TotalHours > 0) _fuelExpenseAvgHour = Math.Round(_fuelExpense / TimeEngineOn.TotalHours, 2);
            }
        }

        public void SetIdentRfidName(UInt16 identRfid, long sensorLenght)
        {
            IdentID = identRfid;

            if (IdentID == (UInt16)((1 << (int)sensorLenght) - 1))
            {
                IdentIDName = Resources.RfidNoCard;
            }
            else if (IdentID == RFID_NOT_WORKING)
            {
                IdentIDName = Resources.RfidNoReader;
            }
            else
            {
                IdentIDName = IdentID.ToString();
            }
        }

        public string IdentIDName { get; set; }
        public string LocationStart { get; set; }
        public string LocationEnd { get; set; }
        public TimeSpan TimeDuration { get; set; }
        public double Distance { get; set; }
        public TimeSpan TimeStops { get; set; }
        public TimeSpan TimeEngineOn { get; set; }
        public TimeSpan TimeEngineOnStops { get; set; }
        public double FuelStart { get; set; }
        public double FuelAdd { get; set; }
        public double FuelSub { get; set; }
        public double FuelExpense { get; set; }
        public double FuelExpenseAvgKm { get; set; }
        public double FuelExpenseAvgHour { get; set; }
    }
}
