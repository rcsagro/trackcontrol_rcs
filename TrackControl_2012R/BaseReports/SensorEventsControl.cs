﻿using BaseReports.Procedure;
using BaseReports.Properties;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace BaseReports
{
    /// <summary>
    /// Класс контрола для отображения отчета по событиям датчиков
    /// </summary>
    // [Serializable]
    //  [ToolboxItem(false)]
    public partial class SensorEventsControl : ReportsDE.BaseControl
    {
        public class reportSens
        {
            private int id;
            private string sensname;
            private bool picture;
            private string location;
            private DateTime eventtime;
            private TimeSpan duration;
            private TimeSpan _durMotion;
            private TimeSpan _durStop;
            private double distance;
            private string descript;
            private double speed;
            private double avgspeed;

            public reportSens(int id, string sensname, bool picture, string location, DateTime eventtime,
                TimeSpan duration, TimeSpan _durMotion, TimeSpan _durStop,
                double distance, string descript, double speed, double avgspeed)
            {
                this.id = id;
                this.picture = picture;
                this.sensname = sensname;
                this.location = location;
                this.eventtime = eventtime;
                this.duration = duration;
                this._durMotion = _durMotion;
                this._durStop = _durStop;
                this.distance = distance;
                this.descript = descript;
                this.speed = speed;
                this.avgspeed = avgspeed;
            } // reportSens

            public int Id
            {
                get { return this.id; }
            }

            public string SensorName
            {
                get { return this.sensname; }
            }

            public bool IsCheckZone
            {
                get { return picture; }
            }

            public string Description
            {
                get { return this.descript; }
            }

            public string Location
            {
                get { return this.location; }
            }

            public DateTime EventTime
            {
                get { return this.eventtime; }
            }

            public TimeSpan Duration
            {
                get { return this.duration; }
            }

            public TimeSpan _durationMotion
            {
                get { return this._durMotion; }
            }

            public TimeSpan _durationStop
            {
                get { return this._durStop; }
            }

            public double Distance
            {
                get { return this.distance; }
            }

            public double Speed
            {
                get { return this.speed; }
            }

            public double SpeedAvg
            {
                get { return this.avgspeed; }
            }
        } // reportSens

        private List<int> _mobitelIdList = new List<int>();

        /// <summary>
        /// Интерфейс для построение графиков по SeriesL
        /// </summary>
        private static IBuildGraphs buildGraph;

        protected static atlantaDataSet dataset;
        private VehicleInfo vehicleInfo;
        private ReportBase<reportSens, TInfo> ReportingSens;
        private List<string> listSens = new List<string>();
        private string NameThisReport = Resources.SensorEvents;

        public SensorEventsControl()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gViewSensorEvents, sensorControlEventsGrid, null);

            gViewSensorEvents.Images = icZone;
            icbZone.SmallImages = icZone;

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            gViewSensorEvents.DoubleClick += new EventHandler(gViewSensorEvents_DoubleClick);

            //ShowGraphToolStripButton.Visible = false;
            //sensorControlEventsGrid.AutoGenerateColumns = false;
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = dataset;
            atlantaDataSetBindingSource.DataMember = "SensorEvents";

            //SelectFieldGrid(ref sensorControlEventsGrid);
            eventsTotalBindingSource.DataSource = dataset;
            eventsTotalBindingSource.DataMember = "SensorEventsTotal";
            buildGraph = new BuildGraphs();
            AddAlgorithm(new SensorEvents());
            gViewSensorEvents.CustomDrawGroupRow += gViewSensorEvents_CustomDrawGroupRow;

            ReportingSens =
                new ReportBase<reportSens, TInfo>(Controls, compositeLink1, gViewSensorEvents,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);

            gViewSensorEvents.CustomDrawFooterCell += gViewSensorEvents_CustomDrawFooterCell;
            gViewSensorEvents.CustomSummaryCalculate += GViewSensorEventsOnCustomSummaryCalculate;
        }

        private TimeSpan totalSum;
        private TimeSpan totalMove;
        private TimeSpan totalStop;

        private void GViewSensorEventsOnCustomSummaryCalculate( object sender, CustomSummaryEventArgs e )
        {
            // Initialization.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
            {
                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("Duration") == 0)
                {
                    totalSum = new TimeSpan(0, 0, 0, 0);
                }

                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("_durationMotion") == 0)
                {
                    totalMove = new TimeSpan(0, 0, 0, 0);
                }

                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("_durationStop") == 0)
                {
                    totalStop = new TimeSpan(0, 0, 0, 0);
                }
            }

            // Calculation.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
            {
                if( ( ( DevExpress.XtraGrid.GridSummaryItem )e.Item ).FieldName.CompareTo( "Duration" ) == 0 )
                {
                    string value = e.GetValue("Duration").ToString();
                    TimeSpan sp;
                    TimeSpan.TryParse(value, out sp);
                    totalSum += sp;
                }

                if( ( ( DevExpress.XtraGrid.GridSummaryItem )e.Item ).FieldName.CompareTo( "_durationMotion" ) == 0 )
                {
                    string value = e.GetValue( "_durationMotion" ).ToString();
                    TimeSpan sp;
                    TimeSpan.TryParse( value, out sp );
                    totalMove += sp;
                }

                if( ( ( DevExpress.XtraGrid.GridSummaryItem )e.Item ).FieldName.CompareTo( "_durationStop" ) == 0 )
                {
                    string value = e.GetValue( "_durationStop" ).ToString();
                    TimeSpan sp;
                    TimeSpan.TryParse( value, out sp );
                    totalStop += sp;
                }
            }

            // Finalization.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("Duration") == 0)
                {
                    e.TotalValue = totalSum.ToString();
                }

                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("_durationMotion") == 0)
                {
                    e.TotalValue = totalMove.ToString();
                }

                if (((DevExpress.XtraGrid.GridSummaryItem) e.Item).FieldName.CompareTo("_durationStop") == 0)
                {
                    e.TotalValue = totalStop.ToString();
                }
            }
        }

        private void gViewSensorEvents_CustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
        }

        private void gViewSensorEvents_CustomDrawGroupRow( object sender, RowObjectCustomDrawEventArgs e )
        {
            GridView view = sender as GridView;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;

            if( info.Column.Caption == Resources.EventDescription )
            {
                if (view.IsGroupRow(e.RowHandle))
                {
                    int count = view.GetChildRowCount( e.RowHandle );
                    TimeSpan summa1 = new TimeSpan();
                    TimeSpan summa2 = new TimeSpan();
                    TimeSpan interval;
                    
                    for( int i = 0; i < count; i++ )
                    {
                        int childHandle = view.GetChildRowHandle( e.RowHandle, i );
                        string value1 = view.GetRowCellValue( childHandle, "_durationMotion" ).ToString();
                        string value2 = view.GetRowCellValue( childHandle, "_durationStop" ).ToString();
                        
                        TimeSpan.TryParse(value1, out interval);
                        summa1 = summa1 + interval;
                        TimeSpan.TryParse( value2, out interval );
                        summa2 = summa2 + interval;
                    }

                    string str = view.GetGroupRowDisplayText(e.RowHandle);
                    int indx = str.IndexOf(",");
                    string st = str.Substring(0, indx + 1);
                    string stp = str.Substring( indx + 2, str.Length - indx - 4 );
                    string strt = " " + Resources.InMotion + summa1 + ", " + Resources.InStops + summa2 + ", ";

                    info.GroupText = st + strt + stp + ";";
                } // if
            } // if
        } // DevSensorEventsControl

        public override string Caption
        {
            get { return Resources.SensorEvents; }
        }

        /// <summary>
        /// Обработчик события окончания работы алгоритма подготовки данных для отчета.
        /// Таблица отчета в датасете заполнена данными для всех телетреков.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is SensorEvents)
            {
                SensorEventsEventArgs args = (SensorEventsEventArgs) e;
                _mobitelIdList.Add(args.Id);
            }
        }

        /// <summary>
        /// Выбирает данные из таблицы отчета в датасете только для конкретного телетрека.
        /// Связывает полученные данные с гридом.
        /// </summary>
        /// <param name="m_row"></param>
        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                if (_mobitelIdList.Contains(curMrow.Mobitel_ID))
                {
                    EnableButton();
                    vehicleInfo = new VehicleInfo(mobitel);
                }
                else
                {
                    DisableButton();
                }

                atlantaDataSetBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                atlantaDataSetBindingSource.Sort = "EventTime ASC";

                eventsTotalBindingSource.Filter = String.Format("MobitelId={0}", mobitel.Mobitel_ID);
                eventsTotalBindingSource.Sort = "Count ASC";

                //createGraphics();
               // if (atlantaDataSetBindingSource.Count > 0 || eventsTotalBindingSource.Count > 0)
                if( CurrentActivateReport == NameThisReport )
                {
                    SelectGraphic(mobitel);
                }
            } // if
        } // Select

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel != null)
            {
                if (!DataSetManager.IsGpsDataExist(mobitel))
                    return;

                curMrow = mobitel;
                CreateGraphics();
            }
        } // SelectGraphic

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            dataset.SensorEvents.Clear();
            dataset.SensorEventsTotal.Clear();
            _mobitelIdList.Clear();
        }

        /// <summary>
        /// Запускает на выполнение алгоритм обработки данных и заполнения соответствующей таблицы
        /// в датасете. После чего связывает грид с данными отчета текущего телетрека.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                BeginReport();
                _stopRun = false;

                ClearReport();
                ReportsControl.OnClearMapObjectsNeeded();

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    }
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";
                Select(curMrow);
                //SelectGraphic(curMrow);

                SetStartButton();
                EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            }
        } // bbiStart_ItemClick

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            DisableButton();
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }
            EnableButton();
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        /// <summary>
        /// Отображает на карте маркер, соответствующий строке события, выбранной в гриде.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gViewSensorEvents_DoubleClick(object sender, EventArgs e)
        {
            if (gViewSensorEvents.FocusedRowHandle < 0)
                return;

            ReportsControl.OnClearMapObjectsNeeded();

            if (gViewSensorEvents.IsValidRowHandle(gViewSensorEvents.FocusedRowHandle))
            {
                int id = (int) gViewSensorEvents.GetRowCellValue(gViewSensorEvents.FocusedRowHandle, "Id");
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById(id);
                PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
                List<Marker> markers = new List<Marker>();
                markers.Add(new Marker(MarkerType.Alarm, row.MobitelId, location, row.Description, ""));
                ReportsControl.OnMarkersShowNeeded(markers);

                DateTime EventDate = row.EventTime;
                DateTime EventDateEnd = EventDate.Add(row.Duration);
                List<IGeoPoint> data = new List<IGeoPoint>();
                Algorithm.GetPointsExtractedBetwDates(EventDate, EventDateEnd, data);
                if (data.Count > 0)
                {
                    List<Track> segments = new List<Track>();
                    segments.Add(new Track(row.MobitelId, Color.Red, 2f, data));
                    ReportsControl.OnTrackSegmentsShowNeeded(segments);
                }

                //if (sender != null)
                //{
                //  ReportsControl.OnGraphShowNeeded();
                //}

                Graph.ClearRegion();
                Graph.ClearLabel();

                Graph.AddTimeRegion(row.EventTime, row.EventTime + row.Duration); //выделяем цветом
                Graph.SellectZoom();
            } //if
        } // gViewSensorEvents_DoubleClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gViewSensorEvents.FocusedRowHandle < 0)
                return;

            if (gViewSensorEvents.IsValidRowHandle(gViewSensorEvents.FocusedRowHandle))
            {
                int id = (int) gViewSensorEvents.GetRowCellValue(gViewSensorEvents.FocusedRowHandle, "Id");
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById(id);

                if (sender != null)
                {
                    ReportsControl.OnGraphShowNeeded();
                }

                Graph.ClearRegion();
                Graph.ClearLabel();
                Graph.AddTimeRegion(row.EventTime, row.EventTime + row.Duration);
                Graph.SellectZoom();
            } // if
        } // bbiShowOnGraf_ItemClick

        /// <summary>
        /// Отображает графики значений датчиков
        /// </summary>
        private void CreateGraphics()
        {
            vehicleInfo = new VehicleInfo(curMrow);
            ReportsControl.GraphClearSeries();
            buildGraph.AddGraphAngle(graph, dataset, curMrow);
            buildGraph.AddGraphDalnomer(graph, dataset, curMrow);
            buildGraph.AddGraphTemperature(graph, dataset, curMrow);
            buildGraph.AddGraphVoltage(graph, dataset, curMrow);
            ReportsControl.ShowGraph(curMrow);
        } // CreateGraphics

        private void calcNameSens(string name)
        {
            if (Search(name))
            {
                listSens.Add(name);
            }
        }

        private bool Search(object getRowCellValue)
        {
            for (int i = 0; i < listSens.Count; i++)
            {
                if (listSens[i] == (string) getRowCellValue)
                {
                    return false;
                }
            }

            return true;
        }

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            TInfo info = ReportingSens.GetInfoStructure;

            DevExpressReportHeader(Resources.Detilede, e);
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        } // composLink_CreateMarginalHeaderArea

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingSens.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                    Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней части заголовка отчета */

        protected string GetStringBreackUp()
        {
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingSens.GetInfoStructure;

            return ( Resources.MoveWay + ": " + info.totalWay + "\n" +
                Resources.TimeWayTotal + ": " + info.totalTimeWay + "\n" +
                Resources.AllPeriod + ": " + info.totalParking );
        }

        
        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gViewSensorEvents.FocusedRowHandle < 0)
                return;

            ReportsControl.OnMapShowNeeded();
            ReportsControl.OnClearMapObjectsNeeded();

            if (gViewSensorEvents.IsValidRowHandle(gViewSensorEvents.FocusedRowHandle))
            {
                int id = (int) gViewSensorEvents.GetRowCellValue(gViewSensorEvents.FocusedRowHandle, "Id");
                atlantaDataSet.SensorEventsRow row = dataset.SensorEvents.FindById(id);
                PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
                List<Marker> markers = new List<Marker>();
                markers.Add(new Marker(MarkerType.Alarm, row.MobitelId, location, row.Description, ""));
                ReportsControl.OnMarkersShowNeeded(markers);
                DateTime EventDate = row.EventTime;
                DateTime EventDateEnd = EventDate.Add(row.Duration);
                List<IGeoPoint> data = new List<IGeoPoint>();
                Algorithm.GetPointsExtractedBetwDates(EventDate, EventDateEnd, data);
                if (data.Count > 0)
                {
                    List<Track> segments = new List<Track>();
                    segments.Add(new Track(row.MobitelId, Color.Red, 2f, data));
                    ReportsControl.OnTrackSegmentsShowNeeded(segments);
                } // if
            } // if
        } // bbiShowOnMap_ItemClick

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gViewSensorEvents, true, true);

            TInfo tinfo = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);
            tinfo.infoVehicle = info.Info;
            tinfo.infoDriverName = info.DriverFullName;
            tinfo.periodBeging = Algorithm.Period.Begin;
            tinfo.periodEnd = Algorithm.Period.End;

            TimeSpan durMotion = new TimeSpan();
            TimeSpan durStop = new TimeSpan();
            double _distance = 0;
            listSens.Clear();
         
            foreach( atlantaDataSet.SensorEventsRow row in dataset.SensorEvents.Select( "MobitelId=" + curMrow.Mobitel_ID ) )
            {
                durMotion = durMotion + row._durationMotion;
                durStop = durStop + row._durationStop;
                _distance = _distance + row.Distance;
                calcNameSens( row.SensorName );
            }

            tinfo.nameTitle = "";
            if( listSens.Count > 0 )
            {
                tinfo.nameTitle = " (";
                for( int i = 0; i < listSens.Count - 1; i++ )
                    tinfo.nameTitle = tinfo.nameTitle + listSens[i] + ", ";
                tinfo.nameTitle = tinfo.nameTitle + listSens[listSens.Count - 1];
                tinfo.nameTitle = tinfo.nameTitle + ")";
            }
          
            tinfo.totalParking = durStop; // Общее время стоянок
            tinfo.totalWay = Math.Round(_distance, 2); // Пройденный путь
            tinfo.totalTimeWay = durMotion; // Общее время движения

            ReportingSens.AddInfoStructToList(tinfo); /* формируем заголовки таблиц отчета */
            ReportingSens.CreateAndShowReport(sensorControlEventsGrid);
        } // ExportToExcelDevExpress

        protected void genReport(int mobitelId)
        {
            TInfo tinfo = new TInfo();
            tinfo.periodBeging = Algorithm.Period.Begin;
            tinfo.periodEnd = Algorithm.Period.End;

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
            VehicleInfo info = new VehicleInfo(mobitelId);

            tinfo.infoVehicle = info.Info;
            tinfo.infoDriverName = info.DriverFullName;

            TimeSpan durMotion = new TimeSpan();
            TimeSpan durStop = new TimeSpan();
            double _distance = 0;
            listSens.Clear();
            
            foreach (atlantaDataSet.SensorEventsRow row in dataset.SensorEvents.Select("MobitelId=" + mobitelId))
            {
                durMotion = durMotion + row._durationMotion;
                durStop = durStop + row._durationStop;
                _distance = _distance + row.Distance;
                calcNameSens(row.SensorName);
            }

            tinfo.totalParking = durStop; // Общее время стоянок
            tinfo.totalWay = Math.Round(_distance, 2); // Пройденный путь
            tinfo.totalTimeWay = durMotion; // Общее время движения

            tinfo.nameTitle = "";
            if( listSens.Count > 0 )
            {
                tinfo.nameTitle = " (";
                for( int i = 0; i < listSens.Count - 1; i++ )
                    tinfo.nameTitle = tinfo.nameTitle + listSens[i] + ", ";
                tinfo.nameTitle = tinfo.nameTitle + listSens[listSens.Count - 1];
                tinfo.nameTitle = tinfo.nameTitle + ")";
            }
           
            ReportingSens.AddInfoStructToList(tinfo);
            ReportingSens.CreateBindDataList();

            foreach (atlantaDataSet.SensorEventsRow row in dataset.SensorEvents.Select("MobitelId=" + mobitelId))
            {
                int id = row.Id;
                string sensname = row.SensorName;
                bool picture = row.IsCheckZone;
                string location = row.Location;
                DateTime eventtime = row.EventTime;
                TimeSpan duration = row.Duration;
                TimeSpan _durMotion = row._durationMotion;
                TimeSpan _durStop = row._durationStop;
                double distance = row.Distance;
                string descript = row.Description;
                double speed = row.Speed;
                double avgspeed = row.SpeedAvg;

                ReportingSens.AddDataToBindList(new reportSens(id, sensname, picture, location, eventtime, duration, _durMotion, _durStop,
                    Math.Round(distance, 2), descript, Math.Round(speed, 2), Math.Round(avgspeed, 2)));
            } // foreach

            ReportingSens.CreateElementReport();
        } // genReport

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint(gViewSensorEvents, true, true);

            _mobitelIdList.ForEach(delegate(int mobitelId) { genReport(mobitelId); });

            ReportingSens.CreateAndShowReport();
            ReportingSens.DeleteData();
        } // ExportAllDevToReport

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gViewSensorEvents);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gViewSensorEvents);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(sensorControlEventsGrid);
        }

        private void Localization()
        {
            _sensor.Caption = Resources.Sensor;
            _location.Caption = Resources.Location;
            _time.Caption = Resources.DateTime;
            _duration.Caption = Resources.Duration;
            DurationMotion.Caption = Resources.DurationMotion;
            DurationStop.Caption = Resources.DurationStop;
            _description.Caption = Resources.EventDescription;
            _speed.Caption = Resources.SpeedEvent;
            _speedAvg.Caption = Resources.SpeedAvg;
            _distance.Caption = Resources.DistanceText;

            _sensor.ToolTip = Resources.Sensor;
            _location.ToolTip = Resources.Location;
            _time.ToolTip = Resources.DateTime;
            _duration.ToolTip = Resources.Duration;
            DurationMotion.ToolTip = Resources.DurationMotion;
            DurationStop.ToolTip = Resources.DurationStop;
            _description.ToolTip = Resources.EventDescription;
            _speed.ToolTip = Resources.SpeedEvent;
            _speedAvg.ToolTip = Resources.SpeedAvg;
            _distance.ToolTip = Resources.DistanceText;
        } // Localization
    } // DevSensorEventsControl
} // BaseReports
