using System;
using System.Xml.Serialization;
using TrackControl.General;

namespace BaseReports.ReportListSetting
{
    /// <summary>
    /// ��������� ������
    /// </summary>
    [Serializable(), XmlType("Report")]
    public class ReportSetting : IEntity
    {

        private string typeName;

        /// <summary>
        /// ������������ ���� ������ ������
        /// </summary>
        [XmlAttribute()]
        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        private string caption;

        /// <summary>
        /// ������������(���������) ������
        /// </summary>
        [XmlAttribute()]
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }

        private bool visible;

        /// <summary>
        /// ��������� ������ � �����������
        /// </summary>
        [XmlAttribute()]
        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public ReportSetting()
        {
        }

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="typeName">������������ ���� ������ ������</param>
        /// <param name="caption">������������(���������) ������</param>
        public ReportSetting(string typeName, string caption)
            : this()
        {
            this.typeName = typeName;
            this.caption = caption;
            visible = true;
        }

        public ReportSetting(int id, string typeName, string caption)
            : this()
        {
            this.id = id;
            this.typeName = typeName;
            this.caption = caption;
            visible = true;
        }

        #region IEntity Members

        private int id;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public bool IsNew
        {
            get { return id == 0; }
        }

        #endregion
    }
}
