using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using BaseReports.Properties;
using TrackControl.General.Log;
using BaseReports.DAL;

namespace BaseReports.ReportListSetting
{
  /// <summary>
  /// ���������� ��������� ����� ��������� ������� �������,
  /// ����������� ��� ������.
  /// </summary>
  public class ReportListCtrl
  {

   public static ReportsControl Reports_ctrl { get; set; }
    /// <summary>
    /// ���� � ����� � ���������� ������ �������
    /// </summary>
    private static readonly string _settingFilePath = Path.Combine(
      TrackControl.General.Globals.APP_DATA, "ReportList.xml");

    /// <summary>
    /// ������ �������
    /// </summary>
    private static List<ReportSetting> reports;

    private XmlAttributeOverrides _overrideAttributes;
    /// <summary>
    /// ����������� ������� ������� ��� ������� ������������ XML - 
    /// ������ ���������� ������������ ����.
    /// </summary>
    private XmlAttributeOverrides OverrideAttributes
    {
      get
      {
        if (_overrideAttributes == null)
        {
          XmlAttributes attributes = new XmlAttributes();
          attributes.XmlRoot = new XmlRootAttribute("Reports");
          XmlAttributeOverrides overrides = new XmlAttributeOverrides();
          overrides.Add(typeof(List<ReportSetting>), attributes);
          _overrideAttributes = overrides;
        }
        return _overrideAttributes;
      }
    }

    public ReportListCtrl()
    {
      reports = new List<ReportSetting>();
      //LoadSetting();
      LoadSettingFromDB();
    }

    /// <summary>
    /// �������� ��������� �� �����
    /// </summary>
    private void LoadSetting()
    {
      if (File.Exists(_settingFilePath))
      {
        try
        {
          using (StreamReader xmlFile = new StreamReader(_settingFilePath))
          {
            reports = (List<ReportSetting>)(new XmlSerializer(
              typeof(List<ReportSetting>), OverrideAttributes)).Deserialize(xmlFile);
          }
        }
        catch (Exception ex)
        {
          ExecuteLogging.Log("App", String.Format(
            Resources.ReportListCtrlErrorXmlLoading, _settingFilePath, ex));
        }
      }
    }

    private void LoadSettingFromDB()
    {
        reports = ReportListProvider.GetReportsForRoleSettings();  
    }
    /// <summary>
    /// ���������� ��������� � ����
    /// </summary>
    private void SaveSetting()
    {
      try
      {
        using (StreamWriter xmlFile = new StreamWriter(_settingFilePath, false))
        {
          (new XmlSerializer(
            typeof(List<ReportSetting>), OverrideAttributes)).Serialize(xmlFile, reports);
        }
      }
      catch (Exception ex)
      {
        ExecuteLogging.Log("App", String.Format(
          Resources.ReportListCtrlErrorXmlSaving, _settingFilePath, ex));
      }
    }

    private void SaveSettingToDB()
    {
        ReportListProvider.SaveReportList(reports);
    }

    /// <summary>
    /// ����� ��������� ��������� ���� ������
    /// </summary>
    /// <param name="typeName">������������ ���� ������</param>
    /// <returns>ReportCfg - ���� �������, null - � ��������� ������</returns>
    internal ReportSetting FindReportSetting(string typeName)
    {
      return reports.Find(
        delegate(ReportSetting r) { return r.TypeName == typeName; });
    }

    public ReportSetting GetById(int id)
    {
        return reports.Find(
          delegate(ReportSetting r) { return r.Id == id; });
    }
    /// <summary>
    /// ���������� ��������� ��� ������ ������
    /// </summary>
    /// <param name="typeName">������������ ���� ������ ������</param>
    /// <param name="caption">������������ ������</param>
    /// <returns>������ ��� ��������� � ����������� ���������</returns>
    internal ReportSetting AddReportSetting(string typeName, string caption)
    {
      ReportSetting result = new ReportSetting(typeName, caption);
      reports.Add(result);
      //SaveSetting();
      SaveSettingToDB();
      return result;
    }

    /// <summary>
    /// ���������� �����
    /// </summary>
    /// <returns>DialogResult: OK - ������ ������ ����������</returns>
    internal DialogResult ShowForm()
    {
      List<ReportSetting> tmpReports = DeepClone(reports);

      DialogResult result = new ReportListForm(tmpReports).ShowDialog();

      if (result == DialogResult.OK)
      {
        reports = DeepClone(tmpReports);
        SaveSetting();
      }
      return result;
    }
    /// <summary>
    /// ������ ������������ �������. 
    /// ����� ������ �� �������� ������ �� �������� � ��� �������� �������.
    /// </summary>
    /// <param name="T">����������� ������</param>
    /// <returns>����</returns>
    private T DeepClone<T>(T obj)
    {
      T result;
      using (MemoryStream stream = new MemoryStream())
      {
        IFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, obj);

        stream.Position = 0;
        result = (T)formatter.Deserialize(stream);
      }
      return result;
    }


    public List<ReportSetting> GetReportsList()
    {
        if (reports != null)
            return DeepClone(reports);
        else
            return null;
    }

    public void SetReportsList(List<ReportSetting> tmpReports)
    {
        reports = DeepClone(tmpReports);
        SaveSetting();
        Reports_ctrl.HandleChangesInReportList(); 
    }
  }
}
