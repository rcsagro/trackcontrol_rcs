namespace BaseReports.ReportListSetting
{
  partial class ReportListForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportListForm));
        this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
        this.layoutPanel = new System.Windows.Forms.TableLayoutPanel();
        this.btnOk = new System.Windows.Forms.Button();
        this.btnCancel = new System.Windows.Forms.Button();
        this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.navigator = new System.Windows.Forms.BindingNavigator(this.components);
        this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
        this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
        this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.grid = new System.Windows.Forms.DataGridView();
        this.columnVisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.columnCaption = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tableLayoutPanel1.SuspendLayout();
        this.layoutPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.navigator)).BeginInit();
        this.navigator.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
        this.SuspendLayout();
        // 
        // tableLayoutPanel1
        // 
        this.tableLayoutPanel1.ColumnCount = 1;
        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableLayoutPanel1.Controls.Add(this.layoutPanel, 0, 0);
        this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
        this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 356);
        this.tableLayoutPanel1.Name = "tableLayoutPanel1";
        this.tableLayoutPanel1.RowCount = 1;
        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
        this.tableLayoutPanel1.Size = new System.Drawing.Size(507, 45);
        this.tableLayoutPanel1.TabIndex = 0;
        // 
        // layoutPanel
        // 
        this.layoutPanel.ColumnCount = 3;
        this.layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
        this.layoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
        this.layoutPanel.Controls.Add(this.btnOk, 1, 1);
        this.layoutPanel.Controls.Add(this.btnCancel, 2, 1);
        this.layoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.layoutPanel.Location = new System.Drawing.Point(3, 3);
        this.layoutPanel.Name = "layoutPanel";
        this.layoutPanel.RowCount = 2;
        this.layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
        this.layoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.layoutPanel.Size = new System.Drawing.Size(501, 39);
        this.layoutPanel.TabIndex = 0;
        // 
        // btnOk
        // 
        this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
        this.btnOk.Enabled = false;
        this.btnOk.Location = new System.Drawing.Point(317, 11);
        this.btnOk.Name = "btnOk";
        this.btnOk.Size = new System.Drawing.Size(75, 23);
        this.btnOk.TabIndex = 2;
        this.btnOk.Text = "���������";
        this.btnOk.UseVisualStyleBackColor = true;
        // 
        // btnCancel
        // 
        this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.btnCancel.Location = new System.Drawing.Point(409, 11);
        this.btnCancel.Name = "btnCancel";
        this.btnCancel.Size = new System.Drawing.Size(75, 23);
        this.btnCancel.TabIndex = 3;
        this.btnCancel.Text = "������";
        this.btnCancel.UseVisualStyleBackColor = true;
        // 
        // navigator
        // 
        this.navigator.AddNewItem = null;
        this.navigator.BindingSource = this.bindingSource;
        this.navigator.CountItem = this.bindingNavigatorCountItem;
        this.navigator.DeleteItem = null;
        this.navigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
        this.navigator.Location = new System.Drawing.Point(0, 0);
        this.navigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
        this.navigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
        this.navigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
        this.navigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
        this.navigator.Name = "navigator";
        this.navigator.PositionItem = this.bindingNavigatorPositionItem;
        this.navigator.Size = new System.Drawing.Size(507, 25);
        this.navigator.TabIndex = 1;
        this.navigator.Text = "bindingNavigator1";
        // 
        // bindingNavigatorCountItem
        // 
        this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
        this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
        this.bindingNavigatorCountItem.Text = "of {0}";
        this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
        // 
        // bindingNavigatorMoveFirstItem
        // 
        this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
        this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
        this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveFirstItem.Text = "Move first";
        // 
        // bindingNavigatorMovePreviousItem
        // 
        this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
        this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
        this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMovePreviousItem.Text = "Move previous";
        // 
        // bindingNavigatorSeparator
        // 
        this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
        this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorPositionItem
        // 
        this.bindingNavigatorPositionItem.AccessibleName = "Position";
        this.bindingNavigatorPositionItem.AutoSize = false;
        this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
        this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
        this.bindingNavigatorPositionItem.Text = "0";
        this.bindingNavigatorPositionItem.ToolTipText = "Current position";
        // 
        // bindingNavigatorSeparator1
        // 
        this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
        this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorMoveNextItem
        // 
        this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
        this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
        this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveNextItem.Text = "Move next";
        // 
        // bindingNavigatorMoveLastItem
        // 
        this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
        this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
        this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveLastItem.Text = "Move last";
        // 
        // bindingNavigatorSeparator2
        // 
        this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
        this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
        // 
        // grid
        // 
        this.grid.AllowUserToAddRows = false;
        this.grid.AllowUserToDeleteRows = false;
        this.grid.AllowUserToResizeRows = false;
        this.grid.AutoGenerateColumns = false;
        this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnVisible,
            this.columnCaption});
        this.grid.DataSource = this.bindingSource;
        this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
        this.grid.Location = new System.Drawing.Point(0, 25);
        this.grid.MultiSelect = false;
        this.grid.Name = "grid";
        this.grid.RowHeadersVisible = false;
        this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
        this.grid.Size = new System.Drawing.Size(507, 331);
        this.grid.TabIndex = 2;
        this.grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellContentClick);
        // 
        // columnVisible
        // 
        this.columnVisible.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.columnVisible.DataPropertyName = "Visible";
        this.columnVisible.HeaderText = "���������";
        this.columnVisible.Name = "columnVisible";
        this.columnVisible.ToolTipText = "��������� ������";
        this.columnVisible.Width = 70;
        // 
        // columnCaption
        // 
        this.columnCaption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.columnCaption.DataPropertyName = "Caption";
        this.columnCaption.HeaderText = "������������";
        this.columnCaption.Name = "columnCaption";
        this.columnCaption.ReadOnly = true;
        this.columnCaption.ToolTipText = "������������ ������";
        this.columnCaption.Width = 300;
        // 
        // ReportListForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.CancelButton = this.btnCancel;
        this.ClientSize = new System.Drawing.Size(507, 401);
        this.Controls.Add(this.grid);
        this.Controls.Add(this.navigator);
        this.Controls.Add(this.tableLayoutPanel1);
        this.MaximizeBox = false;
        this.MinimizeBox = false;
        this.MinimumSize = new System.Drawing.Size(300, 200);
        this.Name = "ReportListForm";
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "��������� ��������� �������";
        this.tableLayoutPanel1.ResumeLayout(false);
        this.layoutPanel.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.navigator)).EndInit();
        this.navigator.ResumeLayout(false);
        this.navigator.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TableLayoutPanel layoutPanel;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.BindingSource bindingSource;
    private System.Windows.Forms.BindingNavigator navigator;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.DataGridView grid;
    private System.Windows.Forms.DataGridViewCheckBoxColumn columnVisible;
    private System.Windows.Forms.DataGridViewTextBoxColumn columnCaption;

  }
}