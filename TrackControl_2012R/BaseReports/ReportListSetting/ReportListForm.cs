using System.Collections.Generic;
using System.Windows.Forms;
using BaseReports.Properties;

namespace BaseReports.ReportListSetting
{
  /// <summary>
  /// ����� ������ ������������� ������� �������
  /// </summary>
  partial class ReportListForm : Form
  {
    private ReportListForm()
    {
      InitializeComponent();
      Init();
    }

    private void Init()
    {
      btnOk.Text = Resources.ApplyText;
      btnCancel.Text = Resources.CancelText;

      columnVisible.HeaderText = Resources.ReportListFormColVisibilityText;
      columnVisible.ToolTipText = Resources.ReportListFormColVisibilityHint;

      columnCaption.HeaderText = Resources.ReportListFormColTitleText;
      columnCaption.ToolTipText = Resources.ReportListFormColTitleHint;

      Text = Resources.ReportListFormCaption;
    }

    /// <summary>
    /// Create report selector form
    /// </summary>
    /// <param name="reports">������ �������� �������(List)</param>
    public ReportListForm(List<ReportSetting> reports)
      : this()
    {
      bindingSource.DataSource = reports;
    }

    private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      if ((e.ColumnIndex == 0) && (!btnOk.Enabled))
      {
        btnOk.Enabled = true;
      }
    }
  }
}