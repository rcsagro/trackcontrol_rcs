﻿namespace BaseReports
{
    partial class GrainLevel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrainLevel));
            this.grainDataGridControl = new DevExpress.XtraGrid.GridControl();
            this.grainDataGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DataGPS_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHuman = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberVeh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdentification = new DevExpress.XtraGrid.Columns.GridColumn();
            this._location = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.coltime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._quantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this._beginValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this._endValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGenerateType = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this._beforeLbl = new DevExpress.XtraBars.BarStaticItem();
            this._afterLbl = new DevExpress.XtraBars.BarStaticItem();
            this._fuelingLbl = new DevExpress.XtraBars.BarStaticItem();
            this._dischargeLbl = new DevExpress.XtraBars.BarStaticItem();
            this._totalLbl = new DevExpress.XtraBars.BarStaticItem();
            this._avgSpeedLbl = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainDataGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageComboRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // grainDataGridControl
            // 
            this.grainDataGridControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grainDataGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grainDataGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grainDataGridControl.Location = new System.Drawing.Point(0, 24);
            this.grainDataGridControl.MainView = this.grainDataGridView;
            this.grainDataGridControl.Name = "grainDataGridControl";
            this.grainDataGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._imageComboRepo,
            this.repositoryItemTextEdit1,
            this.textEdit});
            this.grainDataGridControl.Size = new System.Drawing.Size(799, 354);
            this.grainDataGridControl.TabIndex = 12;
            this.grainDataGridControl.UseEmbeddedNavigator = true;
            this.grainDataGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grainDataGridView,
            this.gridView2});
            // 
            // grainDataGridView
            // 
            this.grainDataGridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.grainDataGridView.Appearance.Empty.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.grainDataGridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.grainDataGridView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.grainDataGridView.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.grainDataGridView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.grainDataGridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.FilterPanel.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.grainDataGridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grainDataGridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grainDataGridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grainDataGridView.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grainDataGridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.grainDataGridView.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.GroupButton.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.GroupButton.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.GroupFooter.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.grainDataGridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.grainDataGridView.Appearance.GroupRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.GroupRow.Options.UseFont = true;
            this.grainDataGridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.grainDataGridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.grainDataGridView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.grainDataGridView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.grainDataGridView.Appearance.HorzLine.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.OddRow.BackColor = System.Drawing.Color.Gainsboro;
            this.grainDataGridView.Appearance.OddRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.grainDataGridView.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.Preview.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.Preview.Options.UseForeColor = true;
            this.grainDataGridView.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.grainDataGridView.Appearance.Row.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.RowSeparator.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.grainDataGridView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grainDataGridView.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.grainDataGridView.Appearance.VertLine.Options.UseBackColor = true;
            this.grainDataGridView.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.grainDataGridView.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grainDataGridView.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grainDataGridView.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.grainDataGridView.ColumnPanelRowHeight = 40;
            this.grainDataGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DataGPS_ID,
            this.colHuman,
            this.colNumberVeh,
            this.colMarking,
            this.colIdentification,
            this._location,
            this.coldate,
            this.coltime,
            this._quantity,
            this._beginValue,
            this._endValue,
            this.colGenerateType});
            this.grainDataGridView.GridControl = this.grainDataGridControl;
            this.grainDataGridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grainDataGridView.Name = "grainDataGridView";
            this.grainDataGridView.OptionsDetail.AllowZoomDetail = false;
            this.grainDataGridView.OptionsDetail.EnableMasterViewMode = false;
            this.grainDataGridView.OptionsDetail.ShowDetailTabs = false;
            this.grainDataGridView.OptionsDetail.SmartDetailExpand = false;
            this.grainDataGridView.OptionsNavigation.AutoFocusNewRow = true;
            this.grainDataGridView.OptionsNavigation.EnterMoveNextColumn = true;
            this.grainDataGridView.OptionsSelection.MultiSelect = true;
            this.grainDataGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.grainDataGridView.OptionsView.EnableAppearanceOddRow = true;
            this.grainDataGridView.OptionsView.ShowFooter = true;
            // 
            // DataGPS_ID
            // 
            this.DataGPS_ID.Caption = "DataGPS_ID";
            this.DataGPS_ID.FieldName = "DataGPS_ID";
            this.DataGPS_ID.Name = "DataGPS_ID";
            // 
            // colHuman
            // 
            this.colHuman.AppearanceCell.Options.UseTextOptions = true;
            this.colHuman.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHuman.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colHuman.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHuman.AppearanceHeader.Options.UseTextOptions = true;
            this.colHuman.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHuman.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colHuman.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colHuman.Caption = "Водитель ТС";
            this.colHuman.FieldName = "HumanVehicle";
            this.colHuman.Name = "colHuman";
            this.colHuman.OptionsColumn.AllowEdit = false;
            this.colHuman.OptionsColumn.AllowFocus = false;
            this.colHuman.OptionsColumn.ReadOnly = true;
            this.colHuman.ToolTip = "Водитель транспортного средства";
            this.colHuman.Visible = true;
            this.colHuman.VisibleIndex = 0;
            // 
            // colNumberVeh
            // 
            this.colNumberVeh.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberVeh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberVeh.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberVeh.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberVeh.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberVeh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberVeh.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberVeh.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberVeh.Caption = "ГосНомер ТС";
            this.colNumberVeh.FieldName = "NumberVehicle";
            this.colNumberVeh.Name = "colNumberVeh";
            this.colNumberVeh.OptionsColumn.AllowEdit = false;
            this.colNumberVeh.OptionsColumn.AllowFocus = false;
            this.colNumberVeh.OptionsColumn.ReadOnly = true;
            this.colNumberVeh.ToolTip = "Госномер транспортного средства";
            this.colNumberVeh.Visible = true;
            this.colNumberVeh.VisibleIndex = 1;
            // 
            // colMarking
            // 
            this.colMarking.AppearanceCell.Options.UseTextOptions = true;
            this.colMarking.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMarking.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMarking.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMarking.AppearanceHeader.Options.UseTextOptions = true;
            this.colMarking.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMarking.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMarking.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMarking.Caption = "Марка ТС";
            this.colMarking.FieldName = "MarkingVehicle";
            this.colMarking.Name = "colMarking";
            this.colMarking.OptionsColumn.AllowEdit = false;
            this.colMarking.OptionsColumn.AllowFocus = false;
            this.colMarking.OptionsColumn.ReadOnly = true;
            this.colMarking.ToolTip = "Марка транспортного средства";
            this.colMarking.Visible = true;
            this.colMarking.VisibleIndex = 2;
            // 
            // colIdentification
            // 
            this.colIdentification.AppearanceCell.Options.UseTextOptions = true;
            this.colIdentification.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentification.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdentification.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdentification.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdentification.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentification.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdentification.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdentification.Caption = "Идентификатор";
            this.colIdentification.FieldName = "Identification";
            this.colIdentification.Name = "colIdentification";
            this.colIdentification.OptionsColumn.AllowEdit = false;
            this.colIdentification.OptionsColumn.AllowFocus = false;
            this.colIdentification.OptionsColumn.ReadOnly = true;
            this.colIdentification.ToolTip = "Идентификатор";
            this.colIdentification.Visible = true;
            this.colIdentification.VisibleIndex = 3;
            // 
            // _location
            // 
            this._location.AppearanceCell.Options.UseTextOptions = true;
            this._location.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.AppearanceHeader.Options.UseTextOptions = true;
            this._location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._location.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._location.Caption = "Местоположение";
            this._location.FieldName = "Location";
            this._location.Name = "_location";
            this._location.OptionsColumn.AllowEdit = false;
            this._location.OptionsColumn.ReadOnly = true;
            this._location.ToolTip = "Местоположение события";
            this._location.Visible = true;
            this._location.VisibleIndex = 4;
            this._location.Width = 120;
            // 
            // coldate
            // 
            this.coldate.AppearanceCell.Options.UseTextOptions = true;
            this.coldate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coldate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coldate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coldate.AppearanceHeader.Options.UseTextOptions = true;
            this.coldate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coldate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coldate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coldate.Caption = "Дата";
            this.coldate.ColumnEdit = this.textEdit;
            this.coldate.FieldName = "date_";
            this.coldate.Name = "coldate";
            this.coldate.OptionsColumn.AllowEdit = false;
            this.coldate.OptionsColumn.AllowFocus = false;
            this.coldate.OptionsColumn.ReadOnly = true;
            this.coldate.ToolTip = "Дата загрузки";
            this.coldate.Visible = true;
            this.coldate.VisibleIndex = 5;
            this.coldate.Width = 80;
            // 
            // textEdit
            // 
            this.textEdit.AutoHeight = false;
            this.textEdit.Name = "textEdit";
            this.textEdit.ReadOnly = true;
            // 
            // coltime
            // 
            this.coltime.AppearanceCell.Options.UseTextOptions = true;
            this.coltime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltime.AppearanceHeader.Options.UseTextOptions = true;
            this.coltime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltime.Caption = "Время";
            this.coltime.FieldName = "Times";
            this.coltime.Name = "coltime";
            this.coltime.OptionsColumn.AllowEdit = false;
            this.coltime.OptionsColumn.AllowFocus = false;
            this.coltime.OptionsColumn.ReadOnly = true;
            this.coltime.ToolTip = "Время загрузки";
            this.coltime.Visible = true;
            this.coltime.VisibleIndex = 6;
            // 
            // _quantity
            // 
            this._quantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._quantity.AppearanceCell.ForeColor = System.Drawing.Color.Green;
            this._quantity.AppearanceCell.Options.UseFont = true;
            this._quantity.AppearanceCell.Options.UseForeColor = true;
            this._quantity.AppearanceCell.Options.UseTextOptions = true;
            this._quantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._quantity.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._quantity.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._quantity.AppearanceHeader.Options.UseTextOptions = true;
            this._quantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._quantity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._quantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._quantity.Caption = "Загружено, кг";
            this._quantity.DisplayFormat.FormatString = "N2";
            this._quantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._quantity.FieldName = "dValue";
            this._quantity.Name = "_quantity";
            this._quantity.OptionsColumn.AllowEdit = false;
            this._quantity.OptionsColumn.ReadOnly = true;
            this._quantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dValue", "{0:f2}")});
            this._quantity.ToolTip = "Количество загруженного зерна";
            this._quantity.Visible = true;
            this._quantity.VisibleIndex = 7;
            this._quantity.Width = 90;
            // 
            // _beginValue
            // 
            this._beginValue.AppearanceCell.Options.UseTextOptions = true;
            this._beginValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._beginValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._beginValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._beginValue.AppearanceHeader.Options.UseTextOptions = true;
            this._beginValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._beginValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._beginValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._beginValue.Caption = "До загрузки, кг";
            this._beginValue.DisplayFormat.FormatString = "N2";
            this._beginValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._beginValue.FieldName = "beginValue";
            this._beginValue.Name = "_beginValue";
            this._beginValue.ToolTip = "Количество зерна до загрузки";
            this._beginValue.Visible = true;
            this._beginValue.VisibleIndex = 8;
            this._beginValue.Width = 90;
            // 
            // _endValue
            // 
            this._endValue.AppearanceCell.Options.UseTextOptions = true;
            this._endValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._endValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._endValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._endValue.AppearanceHeader.Options.UseTextOptions = true;
            this._endValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._endValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._endValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._endValue.Caption = "После загрузки, кг";
            this._endValue.DisplayFormat.FormatString = "N2";
            this._endValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._endValue.FieldName = "endValue";
            this._endValue.Name = "_endValue";
            this._endValue.ToolTip = "Количество зерна после загрузки";
            this._endValue.Visible = true;
            this._endValue.VisibleIndex = 9;
            this._endValue.Width = 110;
            // 
            // colGenerateType
            // 
            this.colGenerateType.AppearanceCell.Options.UseTextOptions = true;
            this.colGenerateType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGenerateType.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGenerateType.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGenerateType.AppearanceHeader.Options.UseTextOptions = true;
            this.colGenerateType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGenerateType.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colGenerateType.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colGenerateType.Caption = "Способ генерации";
            this.colGenerateType.FieldName = "GenerateType";
            this.colGenerateType.Name = "colGenerateType";
            this.colGenerateType.ToolTip = "Способ генерации полученных данных";
            this.colGenerateType.Visible = true;
            this.colGenerateType.VisibleIndex = 10;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._imageComboRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Stop", "Stop", 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Movement", "Movement", 1)});
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.grainDataGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink1});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._beforeLbl,
            this._afterLbl,
            this._fuelingLbl,
            this._dischargeLbl,
            this._totalLbl,
            this._avgSpeedLbl});
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Status bar";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._beforeLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._afterLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._fuelingLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._dischargeLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._totalLbl),
            new DevExpress.XtraBars.LinkPersistInfo(this._avgSpeedLbl)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Status bar";
            // 
            // _beforeLbl
            // 
            this._beforeLbl.Caption = "В начале, кг: ---";
            this._beforeLbl.Id = 0;
            this._beforeLbl.Name = "_beforeLbl";
            this._beforeLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _afterLbl
            // 
            this._afterLbl.Caption = "В конце, кг: ---";
            this._afterLbl.Id = 1;
            this._afterLbl.Name = "_afterLbl";
            this._afterLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _fuelingLbl
            // 
            this._fuelingLbl.Caption = "Загружено, кг: ---";
            this._fuelingLbl.Id = 2;
            this._fuelingLbl.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Green;
            this._fuelingLbl.ItemAppearance.Normal.Options.UseForeColor = true;
            this._fuelingLbl.Name = "_fuelingLbl";
            this._fuelingLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _dischargeLbl
            // 
            this._dischargeLbl.Caption = "Разгружено, кг: ---";
            this._dischargeLbl.Id = 3;
            this._dischargeLbl.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
            this._dischargeLbl.ItemAppearance.Normal.Options.UseForeColor = true;
            this._dischargeLbl.Name = "_dischargeLbl";
            this._dischargeLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _totalLbl
            // 
            this._totalLbl.Caption = "Общий расход, кг: ---";
            this._totalLbl.Id = 4;
            this._totalLbl.Name = "_totalLbl";
            this._totalLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _avgSpeedLbl
            // 
            this._avgSpeedLbl.Caption = "Ср. скорость, км/ч: ---";
            this._avgSpeedLbl.Id = 6;
            this._avgSpeedLbl.Name = "_avgSpeedLbl";
            this._avgSpeedLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(799, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 378);
            this.barDockControl2.Size = new System.Drawing.Size(799, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 378);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(799, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 378);
            // 
            // GrainLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grainDataGridControl);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "GrainLevel";
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.grainDataGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainDataGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grainDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageComboRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grainDataGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView grainDataGridView;
        private DevExpress.XtraGrid.Columns.GridColumn DataGPS_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colHuman;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberVeh;
        private DevExpress.XtraGrid.Columns.GridColumn colMarking;
        private DevExpress.XtraGrid.Columns.GridColumn colIdentification;
        private DevExpress.XtraGrid.Columns.GridColumn _location;
        private DevExpress.XtraGrid.Columns.GridColumn coldate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEdit;
        private DevExpress.XtraGrid.Columns.GridColumn coltime;
        private DevExpress.XtraGrid.Columns.GridColumn _quantity;
        private DevExpress.XtraGrid.Columns.GridColumn _beginValue;
        private DevExpress.XtraGrid.Columns.GridColumn _endValue;
        private DevExpress.XtraGrid.Columns.GridColumn colGenerateType;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem _beforeLbl;
        private DevExpress.XtraBars.BarStaticItem _afterLbl;
        private DevExpress.XtraBars.BarStaticItem _fuelingLbl;
        private DevExpress.XtraBars.BarStaticItem _dischargeLbl;
        private DevExpress.XtraBars.BarStaticItem _totalLbl;
        private DevExpress.XtraBars.BarStaticItem _avgSpeedLbl;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
    }
}
