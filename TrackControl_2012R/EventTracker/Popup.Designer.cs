namespace EventTracker
{
    partial class Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          this.mainTable = new System.Windows.Forms.TableLayoutPanel();
          this.imageLbl = new System.Windows.Forms.Label();
          this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
          this.carLbl = new System.Windows.Forms.Label();
          this.carName = new System.Windows.Forms.Label();
          this.locationTable = new System.Windows.Forms.TableLayoutPanel();
          this.locationLbl = new System.Windows.Forms.Label();
          this.locationText = new System.Windows.Forms.Label();
          this.eventName = new System.Windows.Forms.Label();
          this.buttonTable = new System.Windows.Forms.TableLayoutPanel();
          this.mapBtn = new System.Windows.Forms.Button();
          this.timer = new System.Windows.Forms.Timer(this.components);
          this.mainTable.SuspendLayout();
          this.tableLayoutPanel1.SuspendLayout();
          this.locationTable.SuspendLayout();
          this.buttonTable.SuspendLayout();
          this.SuspendLayout();
          // 
          // mainTable
          // 
          this.mainTable.ColumnCount = 2;
          this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
          this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.mainTable.Controls.Add(this.imageLbl, 0, 1);
          this.mainTable.Controls.Add(this.tableLayoutPanel1, 0, 0);
          this.mainTable.Controls.Add(this.locationTable, 0, 2);
          this.mainTable.Controls.Add(this.eventName, 1, 1);
          this.mainTable.Controls.Add(this.buttonTable, 0, 3);
          this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
          this.mainTable.Location = new System.Drawing.Point(5, 3);
          this.mainTable.Margin = new System.Windows.Forms.Padding(0);
          this.mainTable.Name = "mainTable";
          this.mainTable.RowCount = 4;
          this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
          this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
          this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
          this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
          this.mainTable.Size = new System.Drawing.Size(382, 158);
          this.mainTable.TabIndex = 0;
          // 
          // imageLbl
          // 
          this.imageLbl.AutoSize = true;
          this.imageLbl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.imageLbl.Location = new System.Drawing.Point(0, 20);
          this.imageLbl.Margin = new System.Windows.Forms.Padding(0);
          this.imageLbl.Name = "imageLbl";
          this.imageLbl.Size = new System.Drawing.Size(81, 70);
          this.imageLbl.TabIndex = 0;
          // 
          // tableLayoutPanel1
          // 
          this.tableLayoutPanel1.ColumnCount = 2;
          this.mainTable.SetColumnSpan(this.tableLayoutPanel1, 2);
          this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
          this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.tableLayoutPanel1.Controls.Add(this.carLbl, 0, 0);
          this.tableLayoutPanel1.Controls.Add(this.carName, 1, 0);
          this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
          this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
          this.tableLayoutPanel1.Name = "tableLayoutPanel1";
          this.tableLayoutPanel1.RowCount = 1;
          this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.tableLayoutPanel1.Size = new System.Drawing.Size(382, 20);
          this.tableLayoutPanel1.TabIndex = 3;
          // 
          // carLbl
          // 
          this.carLbl.AutoSize = true;
          this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.carLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.carLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
          this.carLbl.Location = new System.Drawing.Point(0, 0);
          this.carLbl.Margin = new System.Windows.Forms.Padding(0);
          this.carLbl.Name = "carLbl";
          this.carLbl.Size = new System.Drawing.Size(66, 20);
          this.carLbl.TabIndex = 2;
          this.carLbl.Text = "������ :";
          this.carLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // carName
          // 
          this.carName.AutoSize = true;
          this.carName.Dock = System.Windows.Forms.DockStyle.Fill;
          this.carName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.carName.ForeColor = System.Drawing.Color.Black;
          this.carName.Location = new System.Drawing.Point(66, 0);
          this.carName.Margin = new System.Windows.Forms.Padding(0);
          this.carName.Name = "carName";
          this.carName.Size = new System.Drawing.Size(316, 20);
          this.carName.TabIndex = 3;
          this.carName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // locationTable
          // 
          this.locationTable.ColumnCount = 2;
          this.mainTable.SetColumnSpan(this.locationTable, 2);
          this.locationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
          this.locationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.locationTable.Controls.Add(this.locationLbl, 0, 0);
          this.locationTable.Controls.Add(this.locationText, 1, 0);
          this.locationTable.Dock = System.Windows.Forms.DockStyle.Fill;
          this.locationTable.Location = new System.Drawing.Point(0, 90);
          this.locationTable.Margin = new System.Windows.Forms.Padding(0);
          this.locationTable.Name = "locationTable";
          this.locationTable.RowCount = 1;
          this.locationTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
          this.locationTable.Size = new System.Drawing.Size(382, 39);
          this.locationTable.TabIndex = 5;
          // 
          // locationLbl
          // 
          this.locationLbl.AutoSize = true;
          this.locationLbl.Dock = System.Windows.Forms.DockStyle.Fill;
          this.locationLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.locationLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
          this.locationLbl.Location = new System.Drawing.Point(0, 0);
          this.locationLbl.Margin = new System.Windows.Forms.Padding(0);
          this.locationLbl.Name = "locationLbl";
          this.locationLbl.Size = new System.Drawing.Size(55, 39);
          this.locationLbl.TabIndex = 2;
          this.locationLbl.Text = "����� : ";
          // 
          // locationText
          // 
          this.locationText.AutoSize = true;
          this.locationText.Dock = System.Windows.Forms.DockStyle.Fill;
          this.locationText.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.locationText.ForeColor = System.Drawing.Color.Black;
          this.locationText.Location = new System.Drawing.Point(55, 0);
          this.locationText.Margin = new System.Windows.Forms.Padding(0);
          this.locationText.Name = "locationText";
          this.locationText.Size = new System.Drawing.Size(327, 39);
          this.locationText.TabIndex = 3;
          // 
          // eventName
          // 
          this.eventName.AutoSize = true;
          this.eventName.Dock = System.Windows.Forms.DockStyle.Fill;
          this.eventName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.eventName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(43)))), ((int)(((byte)(33)))));
          this.eventName.Location = new System.Drawing.Point(94, 20);
          this.eventName.Margin = new System.Windows.Forms.Padding(13, 0, 0, 0);
          this.eventName.Name = "eventName";
          this.eventName.Size = new System.Drawing.Size(288, 70);
          this.eventName.TabIndex = 1;
          this.eventName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // buttonTable
          // 
          this.buttonTable.ColumnCount = 2;
          this.mainTable.SetColumnSpan(this.buttonTable, 2);
          this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
          this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
          this.buttonTable.Controls.Add(this.mapBtn, 1, 0);
          this.buttonTable.Dock = System.Windows.Forms.DockStyle.Fill;
          this.buttonTable.Location = new System.Drawing.Point(0, 129);
          this.buttonTable.Margin = new System.Windows.Forms.Padding(0);
          this.buttonTable.Name = "buttonTable";
          this.buttonTable.RowCount = 1;
          this.buttonTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
          this.buttonTable.Size = new System.Drawing.Size(382, 29);
          this.buttonTable.TabIndex = 7;
          // 
          // mapBtn
          // 
          this.mapBtn.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.mapBtn.ForeColor = System.Drawing.Color.DarkGreen;
          this.mapBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
          this.mapBtn.Location = new System.Drawing.Point(226, 0);
          this.mapBtn.Margin = new System.Windows.Forms.Padding(35, 0, 0, 0);
          this.mapBtn.Name = "mapBtn";
          this.mapBtn.Padding = new System.Windows.Forms.Padding(5, 0, 2, 0);
          this.mapBtn.Size = new System.Drawing.Size(156, 29);
          this.mapBtn.TabIndex = 6;
          this.mapBtn.Text = "�������� �� �����";
          this.mapBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
          this.mapBtn.UseVisualStyleBackColor = true;
          this.mapBtn.Click += new System.EventHandler(this.mapBtn_Click);
          // 
          // timer
          // 
          this.timer.Interval = 1000;
          this.timer.Tick += new System.EventHandler(this.timer_Tick);
          // 
          // Popup
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(392, 166);
          this.Controls.Add(this.mainTable);
          this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.ForeColor = System.Drawing.Color.Maroon;
          this.Margin = new System.Windows.Forms.Padding(4);
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "Popup";
          this.Padding = new System.Windows.Forms.Padding(5, 3, 5, 5);
          this.ShowIcon = false;
          this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
          this.TopMost = true;
          this.Load += new System.EventHandler(this.Popup_Load);
          this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Popup_FormClosing);
          this.mainTable.ResumeLayout(false);
          this.mainTable.PerformLayout();
          this.tableLayoutPanel1.ResumeLayout(false);
          this.tableLayoutPanel1.PerformLayout();
          this.locationTable.ResumeLayout(false);
          this.locationTable.PerformLayout();
          this.buttonTable.ResumeLayout(false);
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTable;
        private System.Windows.Forms.Label imageLbl;
        private System.Windows.Forms.Label eventName;
        private System.Windows.Forms.Label carLbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label locationLbl;
        private System.Windows.Forms.TableLayoutPanel locationTable;
        private System.Windows.Forms.Label locationText;
        private System.Windows.Forms.Label carName;
        private System.Windows.Forms.TableLayoutPanel buttonTable;
        private System.Windows.Forms.Button mapBtn;
        private System.Windows.Forms.Timer timer;
    }
}