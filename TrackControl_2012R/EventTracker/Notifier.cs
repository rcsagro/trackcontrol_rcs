using BaseReports.EventTrack;
using BaseReports.Procedure;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.General;
using TrackControl.Zones;
using EventTracker.Properties;

namespace EventTracker
{
    /// <summary>
    /// ����� ����������, ��������������� ������ ������ �� ������������� ��������� � �����������
    /// ������������ �� ����� �������������.
    /// </summary>
    [ToolboxItem(false)]
    public class Notifier : Component
    {
        #region Fields

        /// <summary>
        /// ��������� �������� �������. ������ ������ ����������� �������
        /// ������������� �������.
        /// </summary>
        private List<Tracker> _trackers = new List<Tracker>();

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// ����, ����������� ��������� �� ��������� � ������ ��������
        /// �� ���������.
        /// </summary>
        private bool _isActive;

        /// <summary>
        /// ������ ��� ������� ������������� ������.
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// ������� ��� ������� OnLine � ��.
        /// </summary>
        private OnlineTableAdapter _adapter;

        /// <summary>
        /// ���� �����, ������������ � ����-������.
        /// </summary>
        private List<UInt32> _mapCode = new List<uint>();

        private IZonesManager _zonesManager;

        #endregion

        #region .ctor

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="dataset">������� ����������</param>
        public Notifier(atlantaDataSet dataset, string connectionString, List<UInt32> mapCode,
            IZonesManager zonesManager)
        {
            _dataset = dataset;
            _mapCode = mapCode;

            _timer = new Timer();
            _timer.Tick += timer_Tick;

            _adapter = new OnlineTableAdapter();
            _adapter.CS = connectionString;

            _zonesManager = zonesManager;
        }

        #endregion

        /// <summary>
        /// ��������� �� ��������� � ������ ��������.
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
        }

        #region Public Methods

        /// <summary>
        /// ��������� ����� ������������ ������� ��������.
        /// � ������ ������������� ������� ������������ �����������.
        /// </summary>
        public void Start()
        {
            // ������ ���� ������������� ������ ��� �����.
            //tracker_Checked(null, new TrackerEventArgs(50.448542, 30.482211, DateTime.Now, "It's just testing", "UFO", "Alarm", "DingDong"));

            initialize();

            if (_trackers.Count == 0)
            {
                MessageBox.Show(Resources.NoObserversConnected);
                return;
            }

            timer_Tick(null, null);

            _timer.Interval = 2000;
            _timer.Start();
            _isActive = true;
        }

        /// <summary>
        /// ������������� ����� ������������ ������� ��������.
        /// </summary>
        public void Stop()
        {
            _timer.Stop();
            _isActive = false;
            reset();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// �������������� ������������ �� ���������.
        /// </summary>
        private void initialize()
        {
            foreach (atlantaDataSet.mobitelsRow mobitel in _dataset.mobitels)
            {
                foreach (atlantaDataSet.sensorsRow sensor in mobitel.GetsensorsRows())
                {
                    Tracker tracker = new Tracker(sensor, Target.Alarm);

                    if (tracker.IsOK)
                    {
                        tracker.Checked += tracker_Checked;
                        _trackers.Add(tracker);
                    }
                }
            }
        }

        /// <summary>
        /// ���������� ��������� ����������.
        /// </summary>
        private void reset()
        {
            _trackers.Clear();
        }

        /// <summary>
        /// ����������� ��� ������������� �������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            atlantaDataSet.onlineDataTable lastOnlineData = _adapter.GetLastData();
            if (lastOnlineData.Count > 0)
            {
                if (e != null) // ������������ �������
                {
                    foreach (atlantaDataSet.onlineRow row in lastOnlineData.Select("", "time ASC"))
                    {
                        foreach (Tracker tracker in _trackers)
                        {
                            tracker.CheckEvent(row);
                        }
                    }
                }
                else // ������������� ���������� ��������� �������
                {
                    foreach (atlantaDataSet.onlineRow row in lastOnlineData.Select("", "time ASC"))
                    {
                        foreach (Tracker tracker in _trackers)
                        {
                            tracker.InitState(row);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ������������ ����������� ������� �������. ���������� � ������� ������������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tracker_Checked(object sender, TrackerEventArgs e)
        {
            string location = Algorithm.GeoLocator.GetLocationInfo(e.LatLng);
            Popup popup = new Popup(e.Time, e.Description, e.LatLng, location, e.CarInfo, e.DriverInfo, e.IconName,
                e.SoundName, _mapCode, _zonesManager);
            popup.Show();
        }

        #endregion

        [Conditional("DEBUG")]
        public void ShowTestPopup()
        {
            PointLatLng latLng = new PointLatLng(50.159305, 30.020142);
            Popup p = new Popup(DateTime.Now, "���������, ��� ����������.", latLng,
                "���-�� �� ������� � �������� �������", "����� 5300", "�������� ������� �����������", "FirstAid",
                "Alarm", _mapCode, _zonesManager);
            p.Show();
        }
    }
}
