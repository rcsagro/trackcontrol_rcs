using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace EventTracker
{
    /// <summary>
    /// ����� ��� ������ � ������������� � ��������� ���������.
    /// </summary>
    public static class Helper
    {
        #region External Functions

        private static readonly UInt32 SND_ASYNC = 0x0001;
        private static readonly UInt32 SND_MEMORY = 0x0004;
        private static readonly UInt32 SND_LOOP = 0x0008;
        //private static readonly UInt32 SND_NOSTOP = 0x0010;
        private static readonly UInt32 SND_FILENAME = 0x00020000;

        [DllImport("winmm.dll")]
        private static extern bool PlaySound(byte[] data, IntPtr hMod, UInt32 dwFlags);

        [DllImport("winmm.dll", SetLastError = true)]
        static extern bool PlaySound(string pszSound, UIntPtr hMod, UInt32 dwFlags);

        #endregion

        #region Fields

        /// <summary>
        /// ������ �� ������ ������.
        /// </summary>
        private static Assembly assembly = Assembly.GetExecutingAssembly();

        #endregion

        #region Public Methods

        /// <summary>
        /// ���������� �����������, ���������� ���������� ��������.
        /// </summary>
        /// <param name="name">�������� �����</param>
        /// <returns></returns>
        public static Image GetImage(string name)
        {
            string imagePath = String.Format("{0}.Icons.{1}.png", assembly.GetName().Name, name);
            Stream stream = assembly.GetManifestResourceStream(imagePath);

            if (stream != null)
                return Image.FromStream(stream);
            else
                return GetImage("Default");
        }

        /// <summary>
        /// ���������� �������� ���� ��������� ������.
        /// </summary>
        /// <returns>������ �������� ������</returns>
        public static string[] GetImageNames()
        {
            string prefix = String.Format("{0}.Icons.", assembly.GetName().Name);
            return getResourceNames(prefix);
        }

        /// <summary>
        /// ���������� �������� ���� ��������� �������� ��������.
        /// </summary>
        /// <returns>������ �������� �������� ��������</returns>
        public static string[] GetSoundNames()
        {
            string prefix = String.Format("{0}.Sounds.", assembly.GetName().Name);
            return getResourceNames(prefix);
        }

        /// <summary>
        /// ����������� �������� ������, ���������� ���������� ��������.
        /// </summary>
        /// <param name="name">�������� ��������� �������</param>
        public static void Play(string name)
        {
            byte[] bStr = getSound(name);
            PlaySound(bStr, IntPtr.Zero, SND_ASYNC | SND_MEMORY);
        }

        /// <summary>
        /// ����������� �������� ������, ���������� .wav-������ � ����� �������� ������������ ����������.
        /// </summary>
        /// <param name="name"></param>
        public static void PlayWavFile(string folderName, string name, bool loop)
        {
            string filePath = String.Format(@"{0}\Sounds\{1}.wav", folderName, name);
            if (loop)
            {
                PlaySound(filePath, UIntPtr.Zero, SND_FILENAME | SND_ASYNC | SND_LOOP);
            }
            else
            {
                PlaySound(filePath, UIntPtr.Zero, SND_FILENAME | SND_ASYNC);
            }
        }

        public static void PlayWavFile(string folderName, string name)
        {
            PlayWavFile(folderName, name, false);
        }

        /// <summary>
        /// �������� ���������� ����� � ����� �������� ������������.
        /// </summary>
        /// <param name="folderName"></param>
        public static void InstallSounds(string folderName)
        {
            string directoryPath = String.Format(@"{0}\Sounds", folderName);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            foreach (string soundName in GetSoundNames())
            {
                string filePath = String.Format(@"{0}\{1}.wav", directoryPath, soundName);
                if (!File.Exists(filePath))
                {
                    using (FileStream fs = File.Create(filePath))
                    {
                        byte[] bStr = getSound(soundName);
                        fs.Write(bStr, 0, bStr.Length);
                    }
                }
            }
        }

        /// <summary>
        /// ������� �� �������� ������������ ����� �������� ��������.
        /// </summary>
        /// <param name="folderName"></param>
        public static void UninstallSounds(string folderName)
        {
            string directoryPath = String.Format(@"{0}\Sounds", folderName);
            if (Directory.Exists(directoryPath))
            {
                Directory.Delete(directoryPath, true);
            }
        }

        /// <summary>
        /// ���������� ����������� �������� ������.
        /// </summary>
        public static void StopPlaying()
        {
            PlaySound((byte[]) null, IntPtr.Zero, 0x40);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ���������� ������ �������� ���������� ��������, ������� ��������� �������.
        /// ������������ ������ ������ �������� �� ���� ��������. ��������: .wav, .png, .jpg � �.�.
        /// </summary>
        /// <param name="prefix">�������</param>
        /// <returns>������ �������� ��������</returns>
        private static string[] getResourceNames(string prefix)
        {
            List<string> result = new List<string>();
            foreach (string resourceName in assembly.GetManifestResourceNames())
            {
                if (resourceName.IndexOf(prefix) == 0)
                {
                    string name = resourceName.Substring(prefix.Length); // ������� �������
                    name = name.Remove(name.Length - 4); // ������� ���������� �����

                    result.Add(name);
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// ���������� �������� ������ � ���� ��������� �������.
        /// </summary>
        /// <param name="name">�������� ��������� �������, ����������� ���������� ��������</param>
        /// <returns></returns>
        private static byte[] getSound(string name)
        {
            string soundPath = String.Format("{0}.Sounds.{1}.wav", assembly.GetName().Name, name);

            Stream stream = assembly.GetManifestResourceStream(soundPath);
            if (stream == null)
                return (byte[]) null;

            byte[] bStr = new Byte[stream.Length];
            stream.Read(bStr, 0, (int) stream.Length);

            return bStr;
        }

        #endregion
    }
}
