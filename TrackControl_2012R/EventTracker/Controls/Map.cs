using GeoData;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Xml;

using TrackControl.General;
using TrackControl.Zones;
using EventTracker.Properties;

namespace EventTracker.Controls
{
    /// <summary>
    /// ������� ��� ����������� �� ����� ����� �������������
    /// �������.
    /// </summary>
    [ToolboxItem(false)]
    public partial class Map : UserControl, IMapAdvancedEvents
    {
        #region Fields

        /// <summary>
        /// ������� ��������� �����.
        /// </summary>
        private GisCore _gisMap;

        /// <summary>
        /// ���� � ����� �����.
        /// </summary>
        private string _mapPath = "";

        /// <summary>
        /// ���������� ����� ������������� �������.
        /// </summary>
        private PointLatLng _latLng;

        /// <summary>
        /// ����-���� ��� �����.
        /// </summary>
        private List<UInt32> _mapCode;

        private IZonesManager _zonesManager;

        #endregion

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="mapCode">����-���� ��� �����.</param>
        public Map(List<UInt32> mapCode, IZonesManager zonesManager)
        {
            _mapCode = mapCode;
            _zonesManager = zonesManager;

            InitializeComponent();
            init();
            _reloadButton.Image = Shared.ReloadMap;
            _czButton.Image = Shared.ZonePlus;
            Dock = DockStyle.Fill;

            _gisMap = new GisCore();
            loadSettings();
            OpenMap();

            CzVisible = true; // ���������� ����������� ���� ��� �������� ����
        }

        /// <summary>
        /// ���������� ����� �� �������� � ������, �����������, ���
        /// ��������� �������.
        /// </summary>
        /// <param name="latLng">���������� ����� ������������� �������</param>
        /// <param name="location">�������������� �������� �����, ��� ��������� �������.</param>
        /// <param name="machine">�������� ������, �� ������� ���������� ��������.</param>
        /// <param name="driver">��� �������� ������������� ��������</param>
        public void ShowMap(PointLatLng latLng, string location, string machine, string driver)
        {
            _latLng = latLng;
            try
            {
                _gisMap.Adjust_Map(mapPanel);
                _gisMap.Map.Advise(this);
                _gisMap.Map.ToolName = "Move";

                TRealPoint rPoint = new TRealPoint();
                rPoint.X = _latLng.Lng;
                rPoint.Y = _latLng.Lat;
                _gisMap.Map.View.Scale = 250000.0;
                _gisMap.Map.View.set_Center(ref rPoint);
            }
            catch (Exception)
            {
            }

            locationBox.Text = location;
            carBox.Text = machine;
            driverBox.Text = driver;
        }

        /// <summary>
        /// ����������� ��������� � ������ �������.
        /// </summary>
        public void Close()
        {
            if (_gisMap.Map != null)
            {
                _gisMap.Map.Project.Close();
            }
            if (_gisMap.GDB != null)
            {
                _gisMap.GDB.Close();
            }
        }

        private void init()
        {
            locationLbl.Text = String.Format("{0} :", Resources.Location);
            carLbl.Text = String.Format("{0} :", Resources.Machine);
            driverLbl.Text = String.Format("{0} :", Resources.Driver);

        }

        /// <summary>
        /// ��������� ��������� �����.
        /// </summary>
        private void loadSettings()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string settingsPath = String.Format("{0}{1}map.xml", TrackControl.General.Globals.APP_DATA,
                    Path.DirectorySeparatorChar);
                doc.Load(settingsPath);

                _mapPath = doc.DocumentElement.SelectSingleNode("path").InnerText;
            }
            catch (Exception)
            {
                _mapPath = "";
            }
        }

        /// <summary>
        /// ��������� gdb-�����.
        /// </summary>
        /// <param name="mapPath"></param>
        private void OpenMap()
        {
            Close();
            try
            {
                if (_mapCode.Count == 0)
                {
                    _gisMap.Open_Map(_mapPath);
                }
                else
                {
                    for (int i = 0; i < _mapCode.Count; i++)
                    {
                        _gisMap.Open_Map(_mapPath, _mapCode[i]);
                        if (_gisMap.GDB.Opened)
                            break;
                    }
                    if (!_gisMap.GDB.Opened)
                    {
                        _gisMap.Open_Map(_mapPath);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        /// <param name="g"></param>
        private void drawMarker(Graphics g)
        {
            Point markerLocation = map2Screen(_latLng);
            g.DrawImageUnscaled(Shared.GEvent, markerLocation.X - 1, markerLocation.Y - 32);
        }

        /// <summary>
        /// ������� �������� ��������� � �������
        /// </summary>
        private Point map2Screen(PointLatLng latLng)
        {
            TRealPoint real = new TRealPoint();
            real.X = latLng.Lng;
            real.Y = latLng.Lat;
            TScreenPoint scr = _gisMap.Map.View.MapToClient(ref real);
            return new Point(scr.X, scr.Y);
        }

        #region IMapAdvancedEvents Members

        public void Map_Rendering(ref TMapRenderInfo Data)
        {
            return;
        }

        /// <summary>
        /// ������������ ��� ����������� �����
        /// </summary>
        /// <param name="DC"></param>
        public void Map_PaintWindow(int DC)
        {
            Graphics g = Graphics.FromHwnd(new IntPtr(_gisMap.Map.Handle));
            g.SmoothingMode = SmoothingMode.AntiAlias;
            drawZones(ref g);
            drawMarker(g);
            g.Dispose();
        }

        public void Map_ToolChanged()
        {
            return;
        }

        public void Map_ViewChanged(int Changes)
        {
            return;
        }

        #endregion

        #region --   ��������� ������ �����   --

        /// <summary>
        /// ������������ ���� �� ������ "��������� ������ �����".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _reloadButton_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = _mapPath;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _mapPath = openFileDialog.FileName;
                OpenMap();

                TRealPoint rPoint = new TRealPoint();
                rPoint.X = _latLng.Lng;
                rPoint.Y = _latLng.Lat;
                _gisMap.Map.View.set_Center(ref rPoint);
            }
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ���� �� ������ "�������� �����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _reloadButton_MouseEnter(object sender, EventArgs e)
        {
            _tip.ToolTipTitle = Resources.ChangeMap;
            _tip.Show(Resources.LoadMapHint, _reloadAnchor, 2, 20, 10000);
        }

        /// <summary>
        /// ������������ ��� �������� ��������� ���� � ������ "�������� �����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _reloadButton_MouseLeave(object sender, EventArgs e)
        {
            _tip.Hide(_reloadAnchor);
        }

        #endregion

        #region --   ����������� ����   --

        internal bool CzVisible
        {
            get
            {
                return _czVisible;
            }
            set
            {
                if (value)
                {
                    _czButton.Image = Shared.ZoneCross;
                }
                else
                {
                    _czButton.Image = Shared.ZonePlus;
                }
                _czVisible = value;
                _gisMap.Map.NoEraseBackground();
                Invalidate(true);
            }
        }

        private bool _czVisible;

        /// <summary>
        /// ������������ ��� ����� �� ������ "����������� ����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _czButton_Click(object sender, EventArgs e)
        {
            CzVisible = !CzVisible;
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ���� �� ������ "����������� ����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _czButton_MouseEnter(object sender, EventArgs e)
        {
            string text;
            if (_czVisible)
            {
                text = String.Format("{0} ({1})", Resources.HideZones, _czVisible);
            }
            else
            {
                text = String.Format("{0} ({1})", Resources.ShowZones, _czVisible);
            }
            _tip.ToolTipTitle = Resources.CheckZones;
            _tip.Show(text, _czAnchor, 2, 20, 10000);
        }

        /// <summary>
        /// ������������ ��� �������� ��������� ���� � ������ "����������� ����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _czButton_MouseLeave(object sender, EventArgs e)
        {
            _tip.Hide(_czAnchor);
        }

        /// <summary>
        /// ������ ����������� ����
        /// </summary>
        /// <param name="g"></param>
        private void drawZones(ref Graphics g)
        {
            using (Font font = new Font("Tahoma", 8.25f, FontStyle.Bold))
            {
                if (_czVisible)
                {
                    foreach (IZone zone in _zonesManager.Root.AllItems)
                    {
                        Point[] pts = new Point[zone.Points.Length];
                        for (int i = 0; i < zone.Points.Length; i++)
                        {
                            pts[i] = map2Screen(zone.Points[i]);
                        }
                        using (GraphicsPath path = new GraphicsPath())
                        {
                            path.AddLines(pts);
                            path.CloseFigure();
                            g.DrawPath(new Pen(Color.Red, 2f), path);
                            g.FillPath(new HatchBrush(HatchStyle.BackwardDiagonal, Color.Red, Color.Empty), path);
                        }
                        drawLabel(g, zone.Name, font, Brushes.White, Brushes.Red, pts[0].X, pts[0].Y);
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// ������ �������
        /// </summary>
        /// <param name="g">����������� ��������</param>
        private static void drawLabel(Graphics g, string text, Font font, Brush wrapBrush, Brush stringBrush, float x,
            float y)
        {
            g.DrawString(text, font, wrapBrush, x - 1, y, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x - 1, y - 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x, y - 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x + 1, y - 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x + 1, y, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x + 1, y + 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x, y + 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, wrapBrush, x - 1, y + 1, StringFormat.GenericTypographic);
            g.DrawString(text, font, stringBrush, x, y, StringFormat.GenericTypographic);
        }
    }
}
