namespace EventTracker.Controls
{
  partial class Map
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.headTable = new System.Windows.Forms.TableLayoutPanel();
      this.locationLbl = new System.Windows.Forms.Label();
      this.carBox = new System.Windows.Forms.Label();
      this.locationBox = new System.Windows.Forms.Label();
      this.carLbl = new System.Windows.Forms.Label();
      this.driverLbl = new System.Windows.Forms.Label();
      this.driverBox = new System.Windows.Forms.Label();
      this.mapPanel = new System.Windows.Forms.Panel();
      this._tools = new System.Windows.Forms.TableLayoutPanel();
      this._czButton = new System.Windows.Forms.Button();
      this._reloadButton = new System.Windows.Forms.Button();
      this._reloadAnchor = new System.Windows.Forms.Label();
      this._czAnchor = new System.Windows.Forms.Label();
      this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
      this._tip = new System.Windows.Forms.ToolTip(this.components);
      this.mainTable.SuspendLayout();
      this.headTable.SuspendLayout();
      this._tools.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.BackColor = System.Drawing.Color.Transparent;
      this.mainTable.ColumnCount = 2;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 19F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.Controls.Add(this.headTable, 1, 0);
      this.mainTable.Controls.Add(this.mapPanel, 1, 1);
      this.mainTable.Controls.Add(this._tools, 0, 1);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Location = new System.Drawing.Point(0, 0);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 2;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.Size = new System.Drawing.Size(699, 541);
      this.mainTable.TabIndex = 0;
      // 
      // headTable
      // 
      this.headTable.ColumnCount = 2;
      this.headTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
      this.headTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.headTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.headTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.headTable.Controls.Add(this.locationLbl, 0, 2);
      this.headTable.Controls.Add(this.carBox, 1, 0);
      this.headTable.Controls.Add(this.locationBox, 1, 2);
      this.headTable.Controls.Add(this.carLbl, 0, 0);
      this.headTable.Controls.Add(this.driverLbl, 0, 1);
      this.headTable.Controls.Add(this.driverBox, 1, 1);
      this.headTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.headTable.Location = new System.Drawing.Point(19, 0);
      this.headTable.Margin = new System.Windows.Forms.Padding(0);
      this.headTable.Name = "headTable";
      this.headTable.RowCount = 3;
      this.headTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33342F));
      this.headTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33345F));
      this.headTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33313F));
      this.headTable.Size = new System.Drawing.Size(680, 42);
      this.headTable.TabIndex = 6;
      // 
      // locationLbl
      // 
      this.locationLbl.AutoSize = true;
      this.locationLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.locationLbl.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.locationLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.locationLbl.Location = new System.Drawing.Point(0, 28);
      this.locationLbl.Margin = new System.Windows.Forms.Padding(0);
      this.locationLbl.Name = "locationLbl";
      this.locationLbl.Size = new System.Drawing.Size(80, 14);
      this.locationLbl.TabIndex = 1;
      this.locationLbl.Text = "����� :";
      this.locationLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // carBox
      // 
      this.carBox.AutoSize = true;
      this.carBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carBox.ForeColor = System.Drawing.Color.Navy;
      this.carBox.Location = new System.Drawing.Point(80, 0);
      this.carBox.Margin = new System.Windows.Forms.Padding(0);
      this.carBox.Name = "carBox";
      this.carBox.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.carBox.Size = new System.Drawing.Size(600, 14);
      this.carBox.TabIndex = 2;
      this.carBox.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // locationBox
      // 
      this.locationBox.AutoSize = true;
      this.locationBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.locationBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.locationBox.ForeColor = System.Drawing.Color.DarkGreen;
      this.locationBox.Location = new System.Drawing.Point(80, 28);
      this.locationBox.Margin = new System.Windows.Forms.Padding(0);
      this.locationBox.Name = "locationBox";
      this.locationBox.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.locationBox.Size = new System.Drawing.Size(600, 14);
      this.locationBox.TabIndex = 3;
      // 
      // carLbl
      // 
      this.carLbl.AutoSize = true;
      this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carLbl.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.carLbl.Location = new System.Drawing.Point(0, 0);
      this.carLbl.Margin = new System.Windows.Forms.Padding(0);
      this.carLbl.Name = "carLbl";
      this.carLbl.Size = new System.Drawing.Size(80, 14);
      this.carLbl.TabIndex = 0;
      this.carLbl.Text = "������ :";
      this.carLbl.TextAlign = System.Drawing.ContentAlignment.BottomRight;
      // 
      // driverLbl
      // 
      this.driverLbl.AutoSize = true;
      this.driverLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.driverLbl.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.driverLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.driverLbl.Location = new System.Drawing.Point(0, 14);
      this.driverLbl.Margin = new System.Windows.Forms.Padding(0);
      this.driverLbl.Name = "driverLbl";
      this.driverLbl.Size = new System.Drawing.Size(80, 14);
      this.driverLbl.TabIndex = 7;
      this.driverLbl.Text = "�������� :";
      this.driverLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // driverBox
      // 
      this.driverBox.AutoSize = true;
      this.driverBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.driverBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.driverBox.ForeColor = System.Drawing.Color.DarkRed;
      this.driverBox.Location = new System.Drawing.Point(80, 14);
      this.driverBox.Margin = new System.Windows.Forms.Padding(0);
      this.driverBox.Name = "driverBox";
      this.driverBox.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      this.driverBox.Size = new System.Drawing.Size(600, 14);
      this.driverBox.TabIndex = 8;
      this.driverBox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // mapPanel
      // 
      this.mapPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.mapPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mapPanel.Location = new System.Drawing.Point(22, 45);
      this.mapPanel.Name = "mapPanel";
      this.mapPanel.Padding = new System.Windows.Forms.Padding(3);
      this.mapPanel.Size = new System.Drawing.Size(674, 493);
      this.mapPanel.TabIndex = 4;
      // 
      // _tools
      // 
      this._tools.ColumnCount = 2;
      this._tools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._tools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1F));
      this._tools.Controls.Add(this._czButton, 0, 3);
      this._tools.Controls.Add(this._reloadButton, 0, 1);
      this._tools.Controls.Add(this._reloadAnchor, 1, 1);
      this._tools.Controls.Add(this._czAnchor, 1, 3);
      this._tools.Dock = System.Windows.Forms.DockStyle.Fill;
      this._tools.Location = new System.Drawing.Point(0, 42);
      this._tools.Margin = new System.Windows.Forms.Padding(0);
      this._tools.Name = "_tools";
      this._tools.RowCount = 5;
      this._tools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this._tools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this._tools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
      this._tools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this._tools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._tools.Size = new System.Drawing.Size(19, 499);
      this._tools.TabIndex = 7;
      // 
      // _czButton
      // 
      this._czButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this._czButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this._czButton.Location = new System.Drawing.Point(0, 27);
      this._czButton.Margin = new System.Windows.Forms.Padding(0);
      this._czButton.Name = "_czButton";
      this._czButton.Size = new System.Drawing.Size(18, 20);
      this._czButton.TabIndex = 0;
      this._czButton.Text = " ";
      this._czButton.UseVisualStyleBackColor = true;
      this._czButton.MouseLeave += new System.EventHandler(this._czButton_MouseLeave);
      this._czButton.Click += new System.EventHandler(this._czButton_Click);
      this._czButton.MouseEnter += new System.EventHandler(this._czButton_MouseEnter);
      // 
      // _reloadButton
      //
      this._reloadButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this._reloadButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this._reloadButton.Location = new System.Drawing.Point(0, 5);
      this._reloadButton.Margin = new System.Windows.Forms.Padding(0);
      this._reloadButton.Name = "_reloadButton";
      this._reloadButton.Size = new System.Drawing.Size(18, 20);
      this._reloadButton.TabIndex = 1;
      this._reloadButton.UseVisualStyleBackColor = true;
      this._reloadButton.MouseLeave += new System.EventHandler(this._reloadButton_MouseLeave);
      this._reloadButton.Click += new System.EventHandler(this._reloadButton_Click);
      this._reloadButton.MouseEnter += new System.EventHandler(this._reloadButton_MouseEnter);
      // 
      // _reloadAnchor
      // 
      this._reloadAnchor.AutoSize = true;
      this._reloadAnchor.BackColor = System.Drawing.Color.Transparent;
      this._reloadAnchor.Dock = System.Windows.Forms.DockStyle.Fill;
      this._reloadAnchor.Location = new System.Drawing.Point(18, 5);
      this._reloadAnchor.Margin = new System.Windows.Forms.Padding(0);
      this._reloadAnchor.Name = "_reloadAnchor";
      this._reloadAnchor.Size = new System.Drawing.Size(1, 20);
      this._reloadAnchor.TabIndex = 2;
      // 
      // _czAnchor
      // 
      this._czAnchor.AutoSize = true;
      this._czAnchor.Dock = System.Windows.Forms.DockStyle.Fill;
      this._czAnchor.Location = new System.Drawing.Point(18, 27);
      this._czAnchor.Margin = new System.Windows.Forms.Padding(0);
      this._czAnchor.Name = "_czAnchor";
      this._czAnchor.Size = new System.Drawing.Size(1, 20);
      this._czAnchor.TabIndex = 3;
      // 
      // _tip
      // 
      this._tip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(254)))), ((int)(((byte)(193)))));
      this._tip.ForeColor = System.Drawing.Color.Navy;
      this._tip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      // 
      // Map
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.mainTable);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "Map";
      this.Size = new System.Drawing.Size(699, 541);
      this.mainTable.ResumeLayout(false);
      this.headTable.ResumeLayout(false);
      this.headTable.PerformLayout();
      this._tools.ResumeLayout(false);
      this._tools.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private System.Windows.Forms.TableLayoutPanel headTable;
    private System.Windows.Forms.Label locationLbl;
    private System.Windows.Forms.Label carBox;
    private System.Windows.Forms.Label locationBox;
    private System.Windows.Forms.Label carLbl;
    private System.Windows.Forms.Panel mapPanel;
    private System.Windows.Forms.Label driverLbl;
    private System.Windows.Forms.Label driverBox;
    private System.Windows.Forms.TableLayoutPanel _tools;
    private System.Windows.Forms.Button _czButton;
    private System.Windows.Forms.Button _reloadButton;
    private System.Windows.Forms.Label _reloadAnchor;
    private System.Windows.Forms.Label _czAnchor;
    private System.Windows.Forms.ToolTip _tip;
  }
}
