using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GeoData;

namespace EventTracker
{
  public class GisCore
  {
    [DllImport( "GeoData_COM", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall )]
    public static extern void GetFactory( ref IFactory Factory );

    /* ���������� "��������" ������ ���������� IFactory
     * �� ������������ ��� �������� ���� ������ �������� ����������
     */

    [DllImport( "GeoData_COM", CallingConvention = CallingConvention.StdCall )]
    [return: MarshalAs( UnmanagedType.LPStr )]
    public static extern string GetInfo( [MarshalAs( UnmanagedType.LPStr )] string what );

    public IFactory Factory;
    public IMapControl Map;
    public IBase GDB;

    public GisCore()
    {
      Init();
    }

    public void Init()
    {
      if (Factory == null) GetFactory( ref Factory );
      if (Map == null) Map = Factory.New_Map();
      if (GDB == null) GDB = Factory.New_Base();
      Map.RulerMode = 1;
    }

    /// <summary>
    /// �������� Control ����� � ��������� ���������
    /// � ����������� ������� ����� �� ����� ����������
    /// </summary>
    /// <param name="Container"> ���� �������� ����� (������, �����) </param>
    public void Adjust_Map( object Container )
    {
      if (!(Container is Control) || (Map == null)) 
        return;

      TScreenRect R;
      Control Ctrl = Container as Control;
            
      if (Map.Parent == 0) 
        Map.Parent = Ctrl.Handle.ToInt32();

      R.left = 0;
      R.top = 0;
      R.right = Ctrl.Width;
      R.bottom = Ctrl.Height;

      Map.set_Bounds( ref R );
    }

    /// <summary>
    /// ��������� ����� �� ���������� ��������� (���� GDB)
    /// </summary>

    public void Open_Map(String File_Name)
    {
      Open_Map(File_Name, "");
    }

    public void Open_Map(String File_Name, String Map_Name)
    {
      TBaseOpenParams param;
      param.DB_key = 0; 
      param.Swapping = 0;
      param.ReadOnly = 1;
      if (GDB.Open(File_Name, ref param))
        Map.Project.Open(GDB, Map_Name, 0);
    }
    public bool Open_Map(String File_Name, uint DB_key )
    {
      bool result_open = false;
      try
      {
        TBaseOpenParams param;
        param.DB_key = DB_key;
        param.Swapping = 0;
        param.ReadOnly = 1;
        result_open = GDB.Open(File_Name, ref param);
        if(result_open)
        {
          Map.Project.Open(GDB, "", 0);
          return result_open;
        } // if (GDB.Open)
      } // try
      catch (Exception)
      {
      } // catch
      return false;
    }

    /// <summary>
    /// ����������� �������� ��� �������, ������, ������� ������ ��� �������
    /// </summary>
    
    public static String DegreeToString( double Degree, String NegativeSuffix, String PositiveSiffix )
    {
      Double D,M,S;
      String Suffix = (Degree < 0) ? NegativeSuffix : PositiveSiffix; 
      String Prefix = (Degree < 0) && (Suffix == "") ? "-" : "";
      Degree = Math.Abs( Degree );
      D = Math.Truncate( Degree );
      Degree = (Degree-D)*60;
      M = Math.Truncate( Degree );
      Degree = (Degree-M)*60;
      S = Degree;
      return Prefix + String.Format( "{0:#0}�{1:00}'{2:00.00}\" ", D, M, S ) + Suffix;
    }
  }
}
