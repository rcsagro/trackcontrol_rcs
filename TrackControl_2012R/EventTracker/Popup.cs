using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using EventTracker.Controls;
using TrackControl.General;
using TrackControl.Zones;
using EventTracker.Properties;

namespace EventTracker
{
    public partial class Popup : Form
    {
        #region Fields

        /// <summary>
        /// ����� �������
        /// </summary>
        private DateTime _time;

        /// <summary>
        /// �������� ������� �������
        /// </summary>
        private string _description;

        /// <summary>
        /// ���������� �����, ��� ���������� ������ �������
        /// </summary>
        private PointLatLng _latLng;

        /// <summary>
        /// �������� ���������, ��� ��������� �������
        /// </summary>
        private string _location;

        /// <summary>
        /// �������� ������������� ��������
        /// </summary>
        private string _machineName;

        /// <summary>
        /// ��� �������� ������������� ��������
        /// </summary>
        private string _driverName;

        /// <summary>
        /// �������, ���������� ����� ��� ����������� ��������������
        /// ������������� �������.
        /// </summary>
        private Map map;

        /// <summary>
        /// ���� �����, ������������ � ����-������.
        /// </summary>
        private List<UInt32> _mapCode = new List<uint>();

        /// <summary>
        /// �������� ������, ��������������� � ������ ���������.
        /// </summary>
        private string _iconName;

        /// <summary>
        /// �������� ��������� �������, ���������������� � ������ ���������.
        /// </summary>
        private string _soundName;

        private IZonesManager _zonesManager;

        #endregion

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="time">����� ������������� �������</param>
        /// <param name="description">�������� �������</param>
        /// <param name="latLng">���������� �����, ��� ��������� �������</param>
        /// <param name="location">�������������� ������������� �������</param>
        /// <param name="machineName">���������� � ������������ ��������</param>
        /// <param name="driverName">��� �������� ������������� ��������</param>
        /// <param name="iconName">�������� ������, ��������������� � ��������</param>
        /// <param name="SoundName">�������� ��������� �������, ���������������� � ������ ��������</param>
        /// <param name="mapCode"></param>
        public Popup(DateTime time, string description, PointLatLng latLng, string location, string machineName,
            string driverName, string iconName, string SoundName, List<UInt32> mapCode, IZonesManager zonesManager)
        {
            _time = time;
            _description = description;
            _latLng = latLng;
            _location = location;
            _machineName = machineName;
            _driverName = driverName;
            _iconName = iconName;
            _soundName = SoundName;
            _mapCode = mapCode;
            _zonesManager = zonesManager;

            InitializeComponent();
            init();
            mapBtn.Image = Shared.Map;
        }

        private void init()
        {
            carLbl.Text = String.Format("{0} :", Resources.Machine);
            locationLbl.Text = String.Format("{0} : ", Resources.Location);
            mapBtn.Text = Resources.ShowOnTheMap;

        }

        #region GUI Event Handlers

        /// <summary>
        /// ������������ ������� �������� �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Popup_Load(object sender, EventArgs e)
        {
            SetRandomLocation();

            this.imageLbl.Image = Helper.GetImage(this._iconName);
            this.Text = _time.ToString("F");
            this.carName.Text = _machineName;
            this.eventName.Text = _description;
            this.locationText.Text = this._location;

            Application.DoEvents();
            Helper.PlayWavFile(Globals.APP_DATA, this._soundName, true);
        }

        /// <summary>
        /// ����������� �� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            this.Close();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ������������� ��������� ����� �� ������ � ��������� �������.
        /// ���������� ������ �����������.
        /// </summary>
        private void SetRandomLocation()
        {
            int margin = 50; // ������ �� ����� ������ � ��������.
            int bottomMargin = 100; // ������ �� ������� ���� ������ � ��������.

            Random rnd = new Random();
            int x = rnd.Next(margin, Screen.PrimaryScreen.Bounds.Width - this.Width - margin);
            int y = rnd.Next(margin, Screen.PrimaryScreen.Bounds.Height - this.Height - bottomMargin);
            this.Location = new Point(x, y);

            //timer.Start();
        }

        #endregion

        /// <summary>
        /// ������������ ���� �� ������ "�������� �� �����".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapBtn_Click(object sender, EventArgs e)
        {
            EventTracker.Helper.StopPlaying();
            this.mainTable.Visible = false;
            Application.DoEvents();
            map = new Map(_mapCode, _zonesManager);
            map.Visible = false;
            map.Dock = DockStyle.Fill;
            this.Controls.Add(map);
            this.map.Visible = true;
            int margin = 150; // px
            this.Location = new Point(margin, margin);
            this.Width = Screen.PrimaryScreen.Bounds.Width - margin*2;
            this.Height = Screen.PrimaryScreen.Bounds.Height - margin*2 - 20;
            this.MaximumSize = new Size(this.Width, this.Height);
            map.ShowMap(_latLng, this.locationText.Text, _machineName, _driverName);
            this.Text += String.Format("      {0}", _description);
        }

        /// <summary>
        /// ����������� �������� ������ �����.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Popup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (map != null)
                map.Close();
            EventTracker.Helper.StopPlaying();
        }
    }
}