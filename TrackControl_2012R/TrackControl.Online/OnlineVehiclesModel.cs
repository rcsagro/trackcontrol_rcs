using System;
using System.Collections.Generic;
using TrackControl.Vehicles;

namespace TrackControl.Online
{
    public class OnlineVehiclesModel
    {
        private VehiclesModel _model;
        private List<OnlineVehicle> _vehicles;
        private ISettingsProvider _settingProvider;

        public OnlineVehiclesModel(VehiclesModel model, ISettingsProvider settingProvider)
        {
            if (null == model)
                throw new ArgumentNullException("model");

            _model = model;
            _settingProvider = settingProvider;
            init();
        }

        public void Activate()
        {
            _vehicles.ForEach(delegate(OnlineVehicle ov)
            {
                ov.Bind();
            });
        }

        private void init()
        {
            IList<Vehicle> vehicles = _model.Vehicles;
            _vehicles = new List<OnlineVehicle>(vehicles.Count);
            Dictionary<int, VehicleSettings> _settings = new Dictionary<int, VehicleSettings>();

            foreach (Vehicle vehicle in vehicles)
            {
                if (_settings.ContainsKey(vehicle.Settings.Id))
                {
                    vehicle.Settings = _settings[vehicle.Settings.Id];
                }
                else
                {
                    _settingProvider.GetOne(vehicle.Settings);
                    _settings.Add(vehicle.Settings.Id, vehicle.Settings);
                }

                _vehicles.Add(new OnlineVehicle(vehicle));
            }
            _settings.Clear();
        }
    }
}
