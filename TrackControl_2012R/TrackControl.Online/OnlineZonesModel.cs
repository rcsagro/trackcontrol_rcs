using System;
using System.Collections.Generic;

using TrackControl.General;
using TrackControl.Zones;

namespace TrackControl.Online
{
    public class OnlineZonesModel
    {
        private IZonesManager _manager;
        private List<OnlineZone> _zones;

        public event VoidHandler VisibilityChanged;

        public OnlineZonesModel(IZonesManager manager)
        {
            _manager = manager;
            _manager.ZonesAdded += manager_ZonesAdded;
            _manager.ZonesRemoved += manager_ZonesRemoved;
            IList<IZone> zones = _manager.Zones;
            _zones = new List<OnlineZone>(zones.Count);
            foreach (IZone zone in zones)
                _zones.Add(new OnlineZone(zone));
        }

        public List<IZone> CheckedZones
        {
            get
            {
                List<IZone> zones = new List<IZone>();
                _zones.ForEach(delegate(OnlineZone oz)
                {
                    if (oz.Visible)
                        zones.Add(oz.Zone);
                });
                return zones;
            }
        }

        public void Activate()
        {
            _zones.ForEach(delegate(OnlineZone oz)
            {
                oz.Bind();
            });
        }

        public void DeActivate()
        {
            _zones.ForEach(delegate(OnlineZone oz)
            {
                oz.UnBind();
            });
        }

        public void ChangeVisibility(IList<IZone> zones, bool visibility)
        {
            if (zones.Count > 0)
            {
                foreach (IZone zone in zones)
                {
                    OnlineZone oz = (OnlineZone) zone.Tag;
                    oz.Visible = visibility;
                }
                onVisibilityChanged();
            }
        }

        private void onVisibilityChanged()
        {
            VoidHandler handler = VisibilityChanged;
            if (null != handler)
                handler();
        }

        private void manager_ZonesAdded(IEnumerable<IZone> zones)
        {
            if (null == zones)
                throw new ArgumentNullException("zones");

            foreach (IZone zone in zones)
                _zones.Add(new OnlineZone(zone));
        }

        private void manager_ZonesRemoved(IEnumerable<IZone> zones)
        {
            if (null == zones)
                throw new ArgumentNullException("zones");

            foreach (IZone zone in zones)
            {
                foreach (OnlineZone oz in _zones)
                {
                    if (oz.Zone == zone)
                    {
                        _zones.Remove(oz);
                        break;
                    }
                }
            }
        }
    }
}
