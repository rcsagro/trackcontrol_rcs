using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using TrackControl.Vehicles;
using BaseReports.Procedure;
using System.Diagnostics;
using TrackControl.Online.Properties;
using System.Drawing;
using TrackControl.Reports;

namespace TrackControl.Online
{
    /// <summary>
    /// ���������� ����������� ���������� �� ������������� ��������
    /// </summary>
    public class OnlineVehicle : IVehicle
    {
        private IVehicle _vehicle;
        private object _tag;
        private int _lastLogId = Int32.MinValue;
        private GpsSignal _gpsSignal = GpsSignal.NotDetected;
        private float? _speed;
        private DateTime? _lastTime;
        private MotionState _motionState = MotionState.NotDetected;
        private PowerSource _powerSource = PowerSource.NotDetected;
        private List<GpsData> _onlineData = new List<GpsData>();
        private GpsData _lastPoint;

        /// <summary>
        /// ���������� ������ on-line ��������.������ ���������� - ��������/������ �����
        /// </summary>
        private int _logicSensorState = (int) LogicSensorStates.Absence;

        public int LogicSensorState
        {
            get { return _logicSensorState; }
            set { _logicSensorState = value; }
        }

        /// <summary>
        /// ��������� ���������� � �������  ��������
        /// </summary>
        private int _orderState;

        public int OrderState
        {
            get { return _orderState; }
            set { _orderState = value; }
        }

        /// <summary>
        /// �����-����� ���������� � �������  ��������
        /// </summary>
        private string _orderNumber;

        public string OrderNumber
        {
            get { return _orderNumber; }
            set { _orderNumber = value; }
        }

        private VehiclesGroup _group;

        public VehiclesGroup Group
        {
            get { return _group; }
            set { _group = value; }
        }

        /// <summary>
        /// ����� ��������
        /// </summary>
        private List<Sensor> _sensors;


        public List<Sensor> Sensors
        {
            get { return _sensors; }
            set { _sensors = value; }
        }

        /// <summary>
        /// ����������� ��� ������ TrackControl.Online.OnlineVehicle
        /// </summary>
        /// <param name="vehicle">������������ ��������</param>
        public OnlineVehicle(IVehicle vehicle)
        {
            _vehicle = vehicle;
            _sensors = new List<Sensor>();
        }

        #region --   ����� ���������� IVehicle   --

        public int Id
        {
            get { return _vehicle.Id; }
            set { return; }
        }

        public bool IsNew
        {
            get { return _vehicle.IsNew; }
        }

        public Mobitel Mobitel
        {
            get { return _vehicle.Mobitel; }
        }

        public string CarMaker
        {
            get { return _vehicle.CarMaker; }
        }

        public string CarModel
        {
            get { return _vehicle.CarModel; }
        }

        public string FuelMotor
        {
            get { return _vehicle.FuelMotor; }
        }

        public string FuelWays
        {
            get { return _vehicle.FuelWays; }
        }

        public int Identifier
        {
            get { return _vehicle.Identifier; }
        }

        public string RegNumber
        {
            get { return _vehicle.RegNumber; }
        }

        public Driver Driver
        {
            get { return _vehicle.Driver; }
        }

        public VehicleStyle Style
        {
            get { return _vehicle.Style; }
        }

        public int TimeStatus()
        {
            return _vehicle.TimeStatus();
        }

        public string Info
        {
            get
            {
                return _vehicle.Info;
            }
        }

        public TimeSpan? TimeMoving()
        {
            if (_motionBeginTime == null)
                return null;
            if (_motionState == MotionState.Moving)
            {
                return ((DateTime) _lastTime).Subtract((DateTime) _motionBeginTime);
            }
            else
            {
                return null;
            }
        }

        public object Tag
        {
            [DebuggerStepThrough] get { return _tag; }
            [DebuggerStepThrough] set { _tag = value; }
        }

        #endregion

        public void Bind()
        {
            _vehicle.Tag = this;
        }

        public int MobitelId
        {
            [DebuggerStepThrough] get { return Mobitel.Id; }
        }

        public Image Icon
        {
            get { return Style.Icon; }
        }

        public string MarkModel
        {
            get { return String.Format("{0} {1}", _vehicle.CarMaker, _vehicle.CarModel); }
        }

        public string GetAllFuel
        {
            get
            {
                double allFuel = 0;
                if (_sensors != null)
                {
                    foreach (Sensor s in _sensors)
                    {
                        if (s.Algoritm == (int)AlgorithmType.FUEL1)
                            allFuel += s.Value;
                    }
                }

                return Math.Round(allFuel, 2) + Resources.LiterFuel;
            }
        }

        private string sname = string.Empty;
        private string _numTelefone = string.Empty;

        public string DriverNameStep
        { 
            get
            {
                if (_sensors != null)
                {
                    foreach (Sensor s in _sensors)
                    {
                        if (s.Algoritm == (int)AlgorithmType.DRIVER) // RFID - ��������
                        {
                            if (!s.SensorValue.Contains(Resources.NotPresent))
                            {
                                int length = s.SensorValue.IndexOf('(');
                                sname = s.SensorValue.Substring(0, length);
                                _numTelefone = s.NumTelephone;
                                if (s.NumTelephone != "")
                                    return "(" + s.NumTelephone + ")" + sname;
                                else
                                    return "(" + "tel:--" + ")" + sname;
                            }
                            else
                            {
                                return s.SensorValue;
                            }
                        }
                    }
                }

                if (null != _vehicle.Driver)
                {
                    if (_vehicle.Driver.NumTelephone != "")
                    {
                        _numTelefone = _vehicle.Driver.NumTelephone;
                        sname = _vehicle.Driver.FullName;
                        return "(" + _vehicle.Driver.NumTelephone + ")" + _vehicle.Driver.FullName;
                    }
                    else
                    {
                        _numTelefone = "No Data";
                        sname = _vehicle.Driver.FullName;
                        return "(" + "tel:--" + ")" + _vehicle.Driver.FullName;
                    }
                }
                else
                {
                    _numTelefone = "No Data";
                    sname = "No Data";
                    return String.Empty;
                }
            }
        }

        public string DriverNameVersus
        {
            get
            {
                return sname;
            }
        }

        public string DriverTelefone
        {
            get { return _numTelefone; }
        }

        public string DriverName
        {
            get
            {
                if (null != _vehicle.Driver)
                    if (_vehicle.Driver.NumTelephone != "")
                        return "(" + _vehicle.Driver.NumTelephone + ")" + _vehicle.Driver.FullName;
                    else
                        return "(" + "tel:--" + ")" + _vehicle.Driver.FullName;
                else
                    return String.Empty;
            }
        }

        public int LastLogId
        {
            get { return _lastLogId; }
            set { _lastLogId = value; }
        }

        public GpsSignal GpsSignal
        {
            [DebuggerStepThrough] get { return _gpsSignal; }
            [DebuggerStepThrough] set { _gpsSignal = value; }
        }

        public int IntGpsSignal
        {
            get { return (int) _gpsSignal; }
        }

        public float? Speed
        {
            [DebuggerStepThrough] get { return _speed; }
            [DebuggerStepThrough] set { _speed = value; }
        }

        public Double SpeedLimit
        {
            [DebuggerStepThrough] get { return _vehicle.Settings.SpeedMax; }
        }

        public DateTime? LastTime
        {
            [DebuggerStepThrough] get { return _lastTime; }
            [DebuggerStepThrough] set { _lastTime = value; }
        }

        public MotionState MotionState
        {
            [DebuggerStepThrough] get { return _motionState; }
        }

        public PowerSource PowerSource
        {
            [DebuggerStepThrough] get { return _powerSource; }
            [DebuggerStepThrough] set { _powerSource = value; }
        }

        public int IntPowerSource
        {
            get { return (int) _powerSource; }
        }

        public Color ColorTrack
        {
            get { return _vehicle.Style.ColorTrack; }
        }

        public Track Track
        {
            get
            {
                if (_onlineData.Count <= 0) return null;
                List<GpsData> data = new List<GpsData>(_onlineData);
                List<IGeoPoint> points = new List<IGeoPoint>();

                data.ForEach(delegate(GpsData p)
                {
                    try
                    {
                        //if (p.Valid && p.LatLng.Lat >1 && p.LatLng.Lng >1)
                        if (p != null)
                        {
                            if (p.Valid && p.LatLng.Lat != 0 && p.LatLng.Lng != 0)
                                points.Add(p);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                });

                if (points.Count > 0)
                    return new Track(Mobitel.Id, points, ColorTrack);
                else
                    return null;
            }
        }

        public List<GpsData> OnlineData
        {
            [DebuggerStepThrough] get { return _onlineData; }
        }

        public GpsData LastPoint
        {
            [DebuggerStepThrough] get { return _lastPoint; }
            [DebuggerStepThrough] set { _lastPoint = value; }
        }

        public bool IsSelected
        {
            [DebuggerStepThrough] get { return _isSelected; }
            [DebuggerStepThrough] set { _isSelected = value; }
        }

        private bool _isSelected;

        public void UpdateMotionState()
        {
            if (null == _firstPointTime) // �������������� ����� ��������� �����
                _firstPointTime = _lastTime;

            if (0 < _speed)
            {
                if (_motionState == MotionState.Parking || _motionState == MotionState.ParkingLong)
                {
                    _motionBeginTime = _lastTime;
                    _motionEndTime = null;
                }
                _motionState = MotionState.Moving;
            }
            else if (0 == _speed)
            {
                if (_motionState == MotionState.NotDetected)
                {
                    _motionState = MotionState.Stopped;
                }
                else if (_motionState == MotionState.Moving)
                {
                    _motionState = MotionState.Stopped;
                    _motionEndTime = _lastTime;
                }
                else if (_motionState == MotionState.Stopped &&
                         (DateTime.Now - (_motionEndTime == null ? _firstPointTime : _motionEndTime)) >=
                         _vehicle.Settings.TimeBreak
                         &&
                         (DateTime.Now - (_motionEndTime == null ? _firstPointTime : _motionEndTime)) <
                         _vehicle.Settings.TimeBreakMaxPermitted)
                {
                    _motionState = MotionState.Parking;
                    _motionBeginTime = null;
                    _motionEndTime = _lastTime;
                }
                else if ((_motionState == MotionState.Parking || _motionState == MotionState.Stopped) &&
                         (DateTime.Now - (_motionEndTime == null ? _firstPointTime : _motionEndTime)) >=
                         _vehicle.Settings.TimeBreakMaxPermitted)
                {
                    _motionState = MotionState.ParkingLong;
                    _motionBeginTime = null;
                    _motionEndTime = _lastTime;
                }
            }

        }

        /// <summary>
        /// ����� ������ �������� ����� (����������, ����� ��������� ����������
        /// ����� ���������� � ������� ��������� ��� ������������� ���������� �����
        /// ��������� � ��� ���������
        /// </summary>
        private DateTime? _firstPointTime;

        /// <summary>
        /// ����� ������ ��������
        /// </summary>
        private DateTime? _motionBeginTime;

        /// <summary>
        /// ����� ����������� ��������
        /// </summary>
        private DateTime? _motionEndTime;

        public string StateInfo
        {
            get
            {
                StringBuilder result = new StringBuilder();
                TimeSpan duration;
                switch (_motionState)
                {
                        #region --   ��������   --

                    case MotionState.Moving:
                    {
                        result.AppendFormat("{0}  ", Resources.Moving);
                        if (_motionBeginTime.HasValue)
                        {
                            duration = DateTime.Now - _motionBeginTime.Value;
                            result.Append("  ");
                        }
                        else
                        {
                            duration = DateTime.Now - _firstPointTime.Value;
                            result.Append("> ");
                        }
                        break;
                    }

                        #endregion

                        #region --   ���������   --

                    case MotionState.Stopped:
                    {

                        if (_motionEndTime.HasValue)
                        {
                            duration = DateTime.Now - _motionEndTime.Value;
                            if (duration < _vehicle.Settings.TimeBreak)
                            {
                                result.AppendFormat("{0}   ", Resources.Stop);
                            }
                            else
                            {
                                result.AppendFormat("{0}      ", Resources.Parking);
                            }
                        }
                        else
                        {
                            duration = DateTime.Now - _firstPointTime.Value;
                            if (duration < _vehicle.Settings.TimeBreak)
                            {
                                result.AppendFormat("{0} > ", Resources.Stop);
                            }
                            else
                            {
                                result.AppendFormat("{0}    > ", Resources.Parking);
                            }
                        }
                        break;
                    }

                        #endregion

                        #region --   �������   --

                    case MotionState.Parking:
                    {
                        result.AppendFormat("{0}    ", Resources.Parking);
                        if (_motionEndTime != null)
                        {
                            duration = DateTime.Now - _motionEndTime.Value;
                            result.Append("  ");
                        }
                        else
                        {
                            duration = DateTime.Now - _firstPointTime.Value;
                            result.Append("> ");
                        }
                        break;
                    }
                    case MotionState.ParkingLong:
                    {
                        result.AppendFormat("{0}    ", Resources.Parking);
                        if (_motionEndTime != null)
                        {
                            duration = DateTime.Now - _motionEndTime.Value;
                            result.Append("  ");
                        }
                        else
                        {
                            duration = DateTime.Now - _firstPointTime.Value;
                            result.Append("> ");
                        }
                        break;
                    }

                        #endregion

                        #region --   ������ ������ ����������   --

                    case MotionState.Unsafe:
                    case MotionState.NotDetected:
                    default:
                    {
                        return String.Empty;
                    }

                        #endregion
                }
                if (duration.Days > 0)
                {
                    result.AppendFormat("{0} {1}. ", duration.Days, Resources.DaysShort);
                }
                result.AppendFormat("{0:00}:{1:00}:{2:00}", duration.Hours, duration.Minutes, duration.Seconds);
                return result.ToString();
            }
        }

        public VehicleSettings Settings
        {
            get { return _vehicle.Settings; }
        }

        private bool _checked;

        public bool Checked
        {
            get { return _checked; }
            set { _checked = value; }
        }

        public string Location
        {
            get
            {
                if (_lastPoint == null)
                    return "";
                else
                    return Algorithm.FindLocation(_lastPoint.LatLng);
            }
        }

        public VehicleCategory Category { get; set; }

        public VehicleCategory2 Category2 { get; set; }

        public VehicleCategory3 Category3 { get; set; }

        public VehicleCategory4 Category4 { get; set; }

        public string VehicleComment { get; set; }

        public string CategoryMixed
        {
            get { return ""; }
        }

        public bool Is64BitPackets
        {
            get
            {
                return _vehicle.Is64BitPackets;
            }
            set
            {
                _vehicle.Is64BitPackets = value;
            }
        }

        public void CheckGpsData(GpsData gpsData)
        {
            LastPoint = gpsData;
            OnlineData.Add(gpsData);
            LastLogId = gpsData.LogId;
            if (gpsData.Valid)
            {
                GpsSignal = GpsSignal.Valid;
                LastTime = gpsData.Time;
                Speed = gpsData.Speed;
                UpdateMotionState();
            }
            else
                GpsSignal = GpsSignal.NotValid;
        }
    }
}
