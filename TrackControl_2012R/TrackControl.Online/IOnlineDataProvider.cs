using System;
using System.Collections.Generic;
using TrackControl.Vehicles;

namespace TrackControl.Online
{
  public interface IOnlineDataProvider
  {
    IList<GpsData> GetLastData(OnlineVehicle ov);
  }
}
