namespace TrackControl.Online
{
  partial class VehiclesInZoneForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehiclesInZoneForm));
      this._grid = new DevExpress.XtraGrid.GridControl();
      this._view = new DevExpress.XtraGrid.Views.Grid.GridView();
      this._iconCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._titleCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._descrCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._driverCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._gpsCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._gpsRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
      this._gpsImages = new DevExpress.Utils.ImageCollection(this.components);
      this._speedCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._stateCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._lastTimeCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._powerCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._powerRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
      this._powerImages = new DevExpress.Utils.ImageCollection(this.components);
      this._emptyCol = new DevExpress.XtraGrid.Columns.GridColumn();
      this._iconEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
      this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
      ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._view)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._gpsRepo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._gpsImages)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._powerRepo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._powerImages)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._iconEdit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
      this.SuspendLayout();
      // 
      // _grid
      // 
      this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._grid.Location = new System.Drawing.Point(0, 0);
      this._grid.MainView = this._view;
      this._grid.Name = "_grid";
      this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._iconEdit,
            this._gpsRepo,
            this._powerRepo});
      this._grid.Size = new System.Drawing.Size(833, 440);
      this._grid.TabIndex = 0;
      this._grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._view,
            this.gridView2});
      // 
      // _view
      // 
      this._view.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._iconCol,
            this._titleCol,
            this._descrCol,
            this._driverCol,
            this._gpsCol,
            this._speedCol,
            this._stateCol,
            this._lastTimeCol,
            this._powerCol,
            this._emptyCol});
      this._view.GridControl = this._grid;
      this._view.Name = "_view";
      this._view.OptionsBehavior.Editable = false;
      this._view.OptionsDetail.ShowDetailTabs = false;
      this._view.OptionsMenu.EnableColumnMenu = false;
      this._view.OptionsMenu.EnableGroupPanelMenu = false;
      this._view.OptionsView.ShowDetailButtons = false;
      this._view.OptionsView.ShowGroupPanel = false;
      // 
      // _iconCol
      // 
      this._iconCol.Caption = " ";
      this._iconCol.ColumnEdit = this._iconEdit;
      this._iconCol.FieldName = "Icon";
      this._iconCol.ImageAlignment = System.Drawing.StringAlignment.Center;
      this._iconCol.MaxWidth = 24;
      this._iconCol.MinWidth = 24;
      this._iconCol.Name = "_iconCol";
      this._iconCol.OptionsColumn.AllowEdit = false;
      this._iconCol.OptionsColumn.AllowFocus = false;
      this._iconCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._iconCol.OptionsColumn.AllowIncrementalSearch = false;
      this._iconCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._iconCol.OptionsColumn.AllowMove = false;
      this._iconCol.OptionsColumn.AllowShowHide = false;
      this._iconCol.OptionsColumn.ShowInCustomizationForm = false;
      this._iconCol.Visible = true;
      this._iconCol.VisibleIndex = 0;
      this._iconCol.Width = 24;
      // 
      // _titleCol
      // 
      this._titleCol.Caption = "�����";
      this._titleCol.FieldName = "RegNumber";
      this._titleCol.MinWidth = 40;
      this._titleCol.Name = "_titleCol";
      this._titleCol.OptionsColumn.AllowEdit = false;
      this._titleCol.OptionsColumn.AllowFocus = false;
      this._titleCol.OptionsColumn.AllowIncrementalSearch = false;
      this._titleCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._titleCol.OptionsColumn.AllowMove = false;
      this._titleCol.OptionsColumn.AllowShowHide = false;
      this._titleCol.OptionsColumn.ShowInCustomizationForm = false;
      this._titleCol.Visible = true;
      this._titleCol.VisibleIndex = 1;
      this._titleCol.Width = 80;
      // 
      // _descrCol
      // 
      this._descrCol.Caption = "�����, ������";
      this._descrCol.FieldName = "MarkModel";
      this._descrCol.MinWidth = 50;
      this._descrCol.Name = "_descrCol";
      this._descrCol.OptionsColumn.AllowEdit = false;
      this._descrCol.OptionsColumn.AllowFocus = false;
      this._descrCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._descrCol.OptionsColumn.AllowIncrementalSearch = false;
      this._descrCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._descrCol.OptionsColumn.AllowMove = false;
      this._descrCol.OptionsColumn.AllowShowHide = false;
      this._descrCol.OptionsColumn.ShowInCustomizationForm = false;
      this._descrCol.Visible = true;
      this._descrCol.VisibleIndex = 2;
      this._descrCol.Width = 135;
      // 
      // _driverCol
      // 
      this._driverCol.Caption = "��������";
      this._driverCol.FieldName = "DriverName";
      this._driverCol.Name = "_driverCol";
      this._driverCol.OptionsColumn.AllowEdit = false;
      this._driverCol.OptionsColumn.AllowFocus = false;
      this._driverCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._driverCol.OptionsColumn.AllowIncrementalSearch = false;
      this._driverCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._driverCol.OptionsColumn.AllowMove = false;
      this._driverCol.OptionsColumn.AllowShowHide = false;
      this._driverCol.OptionsColumn.ShowInCustomizationForm = false;
      this._driverCol.Visible = true;
      this._driverCol.VisibleIndex = 3;
      this._driverCol.Width = 49;
      // 
      // _gpsCol
      // 
      this._gpsCol.AppearanceHeader.Options.UseTextOptions = true;
      this._gpsCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this._gpsCol.Caption = "GPS";
      this._gpsCol.ColumnEdit = this._gpsRepo;
      this._gpsCol.FieldName = "IntGpsSignal";
      this._gpsCol.ImageAlignment = System.Drawing.StringAlignment.Center;
      this._gpsCol.MaxWidth = 40;
      this._gpsCol.MinWidth = 40;
      this._gpsCol.Name = "_gpsCol";
      this._gpsCol.OptionsColumn.AllowEdit = false;
      this._gpsCol.OptionsColumn.AllowFocus = false;
      this._gpsCol.OptionsColumn.AllowIncrementalSearch = false;
      this._gpsCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._gpsCol.OptionsColumn.AllowMove = false;
      this._gpsCol.OptionsColumn.AllowShowHide = false;
      this._gpsCol.OptionsColumn.ShowInCustomizationForm = false;
      this._gpsCol.Visible = true;
      this._gpsCol.VisibleIndex = 4;
      this._gpsCol.Width = 40;
      // 
      // _gpsRepo
      // 
      this._gpsRepo.AutoHeight = false;
      this._gpsRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this._gpsRepo.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
      this._gpsRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2)});
      this._gpsRepo.Name = "_gpsRepo";
      this._gpsRepo.SmallImages = this._gpsImages;
      // 
      // _gpsImages
      // 
      this._gpsImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_gpsImages.ImageStream")));
      this._gpsImages.Images.SetKeyName(0, "NotValid.png");
      this._gpsImages.Images.SetKeyName(1, "Valid.png");
      this._gpsImages.Images.SetKeyName(2, "NotDetected.png");
      // 
      // _speedCol
      // 
      this._speedCol.Caption = "��������";
      this._speedCol.DisplayFormat.FormatString = "N2";
      this._speedCol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this._speedCol.FieldName = "Speed";
      this._speedCol.MinWidth = 40;
      this._speedCol.Name = "_speedCol";
      this._speedCol.OptionsColumn.AllowEdit = false;
      this._speedCol.OptionsColumn.AllowFocus = false;
      this._speedCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._speedCol.OptionsColumn.AllowIncrementalSearch = false;
      this._speedCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._speedCol.OptionsColumn.AllowMove = false;
      this._speedCol.OptionsColumn.AllowShowHide = false;
      this._speedCol.OptionsColumn.ShowInCustomizationForm = false;
      this._speedCol.Visible = true;
      this._speedCol.VisibleIndex = 5;
      this._speedCol.Width = 53;
      // 
      // _stateCol
      // 
      this._stateCol.Caption = "���������";
      this._stateCol.FieldName = "StateInfo";
      this._stateCol.MinWidth = 40;
      this._stateCol.Name = "_stateCol";
      this._stateCol.OptionsColumn.AllowEdit = false;
      this._stateCol.OptionsColumn.AllowFocus = false;
      this._stateCol.OptionsColumn.AllowIncrementalSearch = false;
      this._stateCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._stateCol.OptionsColumn.AllowMove = false;
      this._stateCol.OptionsColumn.AllowShowHide = false;
      this._stateCol.OptionsColumn.ShowInCustomizationForm = false;
      this._stateCol.Visible = true;
      this._stateCol.VisibleIndex = 6;
      this._stateCol.Width = 121;
      // 
      // _lastTimeCol
      // 
      this._lastTimeCol.Caption = "��������� ������";
      this._lastTimeCol.DisplayFormat.FormatString = "G";
      this._lastTimeCol.FieldName = "LastTime";
      this._lastTimeCol.MinWidth = 40;
      this._lastTimeCol.Name = "_lastTimeCol";
      this._lastTimeCol.OptionsColumn.AllowEdit = false;
      this._lastTimeCol.OptionsColumn.AllowFocus = false;
      this._lastTimeCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._lastTimeCol.OptionsColumn.AllowIncrementalSearch = false;
      this._lastTimeCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._lastTimeCol.OptionsColumn.AllowMove = false;
      this._lastTimeCol.OptionsColumn.AllowShowHide = false;
      this._lastTimeCol.OptionsColumn.ShowInCustomizationForm = false;
      this._lastTimeCol.Visible = true;
      this._lastTimeCol.VisibleIndex = 7;
      this._lastTimeCol.Width = 121;
      // 
      // _powerCol
      // 
      this._powerCol.Caption = "�������";
      this._powerCol.ColumnEdit = this._powerRepo;
      this._powerCol.FieldName = "IntPowerSource";
      this._powerCol.ImageAlignment = System.Drawing.StringAlignment.Center;
      this._powerCol.MaxWidth = 100;
      this._powerCol.MinWidth = 100;
      this._powerCol.Name = "_powerCol";
      this._powerCol.OptionsColumn.AllowEdit = false;
      this._powerCol.OptionsColumn.AllowFocus = false;
      this._powerCol.OptionsColumn.AllowIncrementalSearch = false;
      this._powerCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._powerCol.OptionsColumn.AllowMove = false;
      this._powerCol.OptionsColumn.AllowShowHide = false;
      this._powerCol.OptionsColumn.ShowInCustomizationForm = false;
      this._powerCol.UnboundType = DevExpress.Data.UnboundColumnType.Object;
      this._powerCol.Visible = true;
      this._powerCol.VisibleIndex = 8;
      this._powerCol.Width = 100;
      // 
      // _powerRepo
      // 
      this._powerRepo.AutoHeight = false;
      this._powerRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this._powerRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ���� ����", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ������", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ���� ����", 2, 2)});
      this._powerRepo.Name = "_powerRepo";
      this._powerRepo.SmallImages = this._powerImages;
      // 
      // _powerImages
      // 
      this._powerImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_powerImages.ImageStream")));
      this._powerImages.Images.SetKeyName(0, "OnBoard.png");
      this._powerImages.Images.SetKeyName(1, "Battery.png");
      this._powerImages.Images.SetKeyName(2, "NotDetected.png");
      // 
      // _emptyCol
      // 
      this._emptyCol.Caption = " ";
      this._emptyCol.Name = "_emptyCol";
      this._emptyCol.OptionsColumn.AllowEdit = false;
      this._emptyCol.OptionsColumn.AllowFocus = false;
      this._emptyCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
      this._emptyCol.OptionsColumn.AllowIncrementalSearch = false;
      this._emptyCol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
      this._emptyCol.OptionsColumn.AllowMove = false;
      this._emptyCol.OptionsColumn.AllowShowHide = false;
      this._emptyCol.OptionsColumn.ShowInCustomizationForm = false;
      this._emptyCol.Visible = true;
      this._emptyCol.VisibleIndex = 9;
      this._emptyCol.Width = 89;
      // 
      // _iconEdit
      // 
      this._iconEdit.Name = "_iconEdit";
      // 
      // gridView2
      // 
      this.gridView2.GridControl = this._grid;
      this.gridView2.Name = "gridView2";
      // 
      // VehiclesInZoneForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(833, 440);
      this.Controls.Add(this._grid);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "VehiclesInZoneForm";
      this.Text = "VehiclesInZoneForm";
      this.Load += new System.EventHandler(this.VehiclesInZoneForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._view)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._gpsRepo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._gpsImages)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._powerRepo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._powerImages)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._iconEdit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraGrid.GridControl _grid;
    private DevExpress.XtraGrid.Views.Grid.GridView _view;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    private DevExpress.XtraGrid.Columns.GridColumn _iconCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _iconEdit;
    private DevExpress.XtraGrid.Columns.GridColumn _titleCol;
    private DevExpress.XtraGrid.Columns.GridColumn _descrCol;
    private DevExpress.XtraGrid.Columns.GridColumn _driverCol;
    private DevExpress.XtraGrid.Columns.GridColumn _gpsCol;
    private DevExpress.XtraGrid.Columns.GridColumn _speedCol;
    private DevExpress.XtraGrid.Columns.GridColumn _stateCol;
    private DevExpress.XtraGrid.Columns.GridColumn _lastTimeCol;
    private DevExpress.XtraGrid.Columns.GridColumn _powerCol;
    private DevExpress.XtraGrid.Columns.GridColumn _emptyCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _gpsRepo;
    private DevExpress.Utils.ImageCollection _gpsImages;
    private DevExpress.Utils.ImageCollection _powerImages;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _powerRepo;
  }
}