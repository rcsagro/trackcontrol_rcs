using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Online.Properties;
using DevExpress.XtraEditors.Controls;

namespace TrackControl.Online
{
    public partial class VehiclesInZoneForm : XtraForm
    {
        private IZone _zone;
        private VehiclesModel _model;

        public VehiclesInZoneForm(IZone zone, VehiclesModel model)
        {
            _zone = zone;
            _model = model;
            InitializeComponent();
            init(zone);
        }

        private void init(IZone zone)
        {
            Text = String.Format("{0} \"{1}\"", Resources.VehiclesInZone, zone.Name);
            _titleCol.Caption = Resources.RegNumber;
            _descrCol.Caption = Resources.MarkModel;
            _driverCol.Caption = Resources.Driver;
            _speedCol.Caption = Resources.Speed;
            _stateCol.Caption = Resources.State;
            _lastTimeCol.Caption = Resources.LastData;
            _powerCol.Caption = Resources.Power;
            _powerRepo.Items.Clear();

            _powerRepo.Items.AddRange(new ImageComboBoxItem[]
            {
                new ImageComboBoxItem(String.Format(" {0}", Resources.OnboardPower), 0, 0),
                new ImageComboBoxItem(String.Format(" {0}", Resources.BatteryPower), 1, 1),
                new ImageComboBoxItem(String.Format(" {0}", Resources.OnboardPower), 2, 2)
            });
        }

        private void VehiclesInZoneForm_Load(object sender, EventArgs e)
        {
            try
            {
                List<OnlineVehicle> vehicles = new List<OnlineVehicle>();

                foreach (IVehicle vehicle in _model.Vehicles)
                {
                    OnlineVehicle ov = (OnlineVehicle) vehicle.Tag;

                    if (null != ov.Track && _zone.Contains(ov.Track.LastPoint.LatLng))
                        vehicles.Add(ov);
                }

                _grid.DataSource = vehicles;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.Message + "\n" + ex.StackTrace, "Error VehiclesInZoneForm", MessageBoxButtons.OK );
            }
        }
    }
}