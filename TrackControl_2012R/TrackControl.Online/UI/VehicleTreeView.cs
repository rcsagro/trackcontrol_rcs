using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.ViewInfo;
using TrackControl.General;
using TrackControl.Online.Properties;
using TrackControl.Vehicles;

namespace TrackControl.Online
{
    public partial class VehicleTreeView : XtraUserControl, ITreeModel<OnlineVehicle>
    {
        private bool _isLoaded;
        private VehiclesModel _model;
        private string _condition;

        public event Action<OnlineVehicle> VehicleSelected = delegate { };
        public event MethodInvoker CheckedChanged = delegate { };
        public event MethodInvoker SynhroCheckedVehicles;

        public VehicleTreeView(VehiclesModel model)
        {
            _model = model;
            InitializeComponent();
            init();
            _model.RefreshTreeView += RefreshLastTime;

        }

        private void init()
        {
            _eraseBtn.Glyph = Shared.FunnelClear;
            _funnelBox.Caption = Resources.Funnel;
            _funnelBox.Hint = Resources.FunnelBoxHint;
            _eraseBtn.Caption = Resources.Clear;
            _titleCol.Caption = Resources.TitleNumber;
            _descriptionCol.Caption = Resources.Desctiption;
            _loginCol.Caption = Resources.Login;
            categCol.Caption = Resources.Category;
            bbiExpand.Glyph = Shared.Expand;
            bbiCollapce.Glyph = Shared.Collapce;
            bbiSynhro.Glyph = Shared.Synchro;
        }

        #region --   ����� ���������� ITreeModel<OnlineVehicle>   --

        public OnlineVehicle Current
        {
            get
            {
                OnlineVehicle ov = null;
                IVehicle vehicle = _tree.GetDataRecordByNode(_tree.FocusedNode) as IVehicle;
                if (null != vehicle)
                    ov = (OnlineVehicle) vehicle.Tag;

                return ov;
            }
        }

        public IList<OnlineVehicle> Checked
        {
            get
            {
                CheckedVehiclesOperation operation = new CheckedVehiclesOperation();
                _tree.NodesIterator.DoOperation(operation);
                return operation.Vehicles;
            }
        }

        public IList<OnlineVehicle> GetAll()
        {
            List<OnlineVehicle> all = new List<OnlineVehicle>();
            int errorFlag = 0;
            string msgerror = "";

            foreach (IVehicle vehicle in _model.Vehicles)
            {
                try
                {
                    all.Add((OnlineVehicle) vehicle.Tag);
                }
                catch(Exception ex)
                {
                    msgerror = ex.Message + '\n' + ex.StackTrace;
                    errorFlag++;
                }
            }

            if (errorFlag > 0)
            {
                XtraMessageBox.Show(
                    "�� ����������� ����������� " + errorFlag + "���������." + '\n' + " ������: " + msgerror,
                    "Error VehicleTreeView", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return all.AsReadOnly();
        }

        public OnlineVehicle GetById(int id)
        {
            return null;
        }

        #endregion

        public void RefreshTree()
        {
            _isLoaded = false;
            _tree.RefreshDataSource();
            SetChecked();
        }

        private void RefreshLastTime()
        {
            MethodInvoker action = RefreshLastTimeInvoke;
            if (_tree.InvokeRequired)
            {
                _tree.Invoke(action);
            }
            else
            {
                action();
            }
        }

        private void RefreshLastTimeInvoke()
        {
            UpdateTimeStatus operation = new UpdateTimeStatus(timeCol);

            _tree.NodesIterator.DoLocalOperation(operation, _tree.Nodes);
        }

        private void this_Load(object sender, EventArgs e)
        {
            _tree.DataSource = new object();
            TreeViewService.TreeListInitialView(_tree);
            setCondition(String.Empty);
            SetChecked();
        }

        private void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new VehiclesGroup[] {_model.Root};
                _isLoaded = true;
            }
            else
            {
                VehiclesGroup group = e.Node as VehiclesGroup;

                if (null != group)
                {
                    IList children = new List<object>();

                    foreach (VehiclesGroup vg in group.OwnGroups)
                        if (true /*vg.AllItems.Count > 0*/)
                            children.Add(vg);

                    foreach (IVehicle vehicle in group.OwnItems)
                        children.Add(vehicle);

                    e.Children = children;
                }
                else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
                {
                    e.Children = new object[] {};
                }
            }
        }

        private void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) e.Node;
                if (e.Column == _titleCol)
                    e.CellData = group.NameWithCounter;
            }
            else if (e.Node is IVehicle)
            {
                IVehicle vehicle = (IVehicle) e.Node;
                if (e.Column == _titleCol)
                    e.CellData = vehicle.RegNumber;
                else if (e.Column == _descriptionCol)
                    e.CellData = String.Format("{0} {1}", vehicle.CarMaker, vehicle.CarModel);
                else if (e.Column == timeCol)
                    e.CellData = vehicle.TimeStatus();
                else if (e.Column == categCol)
                    e.CellData = vehicle.CategoryMixed;
                else if (e.Column == _loginCol)
                    e.CellData = vehicle.Mobitel.Login;
            }
        }


        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeViewService.FixNodeState(e);
        }

        private void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            TreeViewService.SetCheckedChildNodes(e.Node);
            TreeViewService.SetCheckedParentNodes(e.Node);
            if (e.Node.TreeList.GetDataRecordByNode(e.Node) is VehiclesGroup)
            {
                GetChecked();
            }
            else
            {
                IVehicle vehicle = e.Node.TreeList.GetDataRecordByNode(e.Node) as IVehicle;
                if (vehicle != null)
                    vehicle.Checked = e.Node.Checked;
            }
            _tree.FocusedNode = e.Node;
            CheckedChanged();

        }

        private void tree_AfterFocusNode(object sender, NodeEventArgs e)
        {
            OnlineVehicle vehicle = e.Node.Tag as OnlineVehicle;
            if (null != vehicle)
                VehicleSelected(vehicle);
        }


        private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16)/2;
            rect.Y += (rect.Height - 16)/2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle) obj;
                e.Graphics.DrawImage(vehicle.Style.Icon, rect);
            }
            else if (obj is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        }

        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle) return;
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
        }

        private void tree_FilterNode(object sender, FilterNodeEventArgs e)
        {
            IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;
            if (null != vehicle)
            {
                if (_condition != null)
                {
                    bool visibleRegNumber = vehicle.RegNumber.ToLower().Contains(_condition.ToLower());

                    bool visibleModel = vehicle.CarModel.ToLower().Contains(_condition.ToLower());
                    bool visibleMaker = vehicle.CarMaker.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory4 = false;
                    if (vehicle.Category4 != null)
                        visibleCategory4 = vehicle.Category4.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory3 = false;
                    if (vehicle.Category3 != null)
                        visibleCategory3 = vehicle.Category3.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory2 = false;
                    if (vehicle.Category2 != null)
                        visibleCategory2 = vehicle.Category2.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory = false;
                    if (vehicle.Category != null)
                        visibleCategory = vehicle.Category.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategoryMixed = false;
                    if (vehicle.CategoryMixed != null)
                        visibleCategoryMixed = vehicle.CategoryMixed.ToLower().Contains(_condition.ToLower());

                    e.Node.Visible = visibleRegNumber | visibleMaker | visibleModel | visibleCategory4 | visibleCategory3 | visibleCategory2 | visibleCategory | visibleCategoryMixed;
                }
                else
                {
                    e.Node.Visible = vehicle.RegNumber.Contains("");
                }
                e.Handled = true;
            }
        }

        private void funnelRepo_KeyUp(object sender, KeyEventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            setCondition(edit.Text);
        }

        private void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _funnelBox.EditValue = String.Empty;
            setCondition(String.Empty);
        }

        private void setCondition(string condition)
        {
            _condition = condition;
            if (_condition.Length > 0)
            {
                _eraseBtn.Enabled = true;
                _tree.FilterNodes();
                hideEmpty();
            }
            else
            {
                _eraseBtn.Enabled = false;
                TreeListNode root = _tree.Nodes[0];
                TreeViewService.ShowAll(root);
            }
        }

        private void hideEmpty()
        {
            TreeListNode root = _tree.Nodes[0];
            foreach (TreeListNode node in root.Nodes)
            {
                VehiclesGroup group = _tree.GetDataRecordByNode(node) as VehiclesGroup;
                if (null != group)
                {
                    if (TreeViewService.GetVisibleCount(node) > 0)
                        node.Visible = true;
                    else
                        node.Visible = false;
                }
            }
        }

        #region --   Nested Classes   --

        private class CheckedVehiclesOperation : TreeListOperation
        {
            public List<OnlineVehicle> Vehicles
            {
                get { return _vehicles; }
            }

            private List<OnlineVehicle> _vehicles = new List<OnlineVehicle>();
            private VehiclesGroup group;

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle && node.Checked)
                {
                    OnlineVehicle ov = vehicle.Tag as OnlineVehicle;

                    if (null != ov)
                    {
                        ov.Group = group;
                        _vehicles.Add(ov);
                    }
                }
            }
        }

        private class CheckNodeFromObjectOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                if (SetChecked(node)) return;
                if (!node.Expanded)
                {
                    node.Expanded = true;
                    SetChecked(node);
                    node.Expanded = false;
                }
            }

            private bool SetChecked(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    node.Checked = vehicle.Checked;
                    TreeViewService.SetCheckedParentNodes(node);
                    return true;
                }
                return false;
            }
        }

        private class CheckObjectFromNodeOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    vehicle.Checked = node.Checked;
                }
            }
        }

        private class UpdateTimeStatus : TreeListOperation
        {
            private TreeListColumn _colTime;

            public UpdateTimeStatus(TreeListColumn colTime)
                : base()
            {
                _colTime = colTime;
            }

            public override void Execute(TreeListNode node)
            {
                object obj = node.TreeList.GetDataRecordByNode(node);
                if (obj is IVehicle)
                {
                    IVehicle vehicle = (IVehicle) obj;
                    node.SetValue(_colTime.VisibleIndex, vehicle.TimeStatus());
                }
            }
        }

        private class CheckNodeAfterRefresh : TreeListOperation
        {
            private List<string> visibleVehicles;

            public CheckNodeAfterRefresh(List<string> listNodes)
            {
                this.visibleVehicles = listNodes;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    foreach (string ernode in visibleVehicles)
                    {
                        if (vehicle.Info == ernode)
                        {
                            //vehicle.Checked = true;
                            node.Checked = true;
                        }
                        //else
                        //{
                        //    vehicle.Checked = false;
                        //    node.Checked = false;
                        //}
                    }
                }
                else
                {
                    // to do
                }
            }
        }

        #endregion

        private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.ExpandAll();
        }

        private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.CollapseAll();
            TreeViewService.TreeListInitialView(_tree);
        }

        private void ttcTree_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl is DevExpress.XtraTreeList.TreeList)
            {
                TreeList tree = (TreeList) e.SelectedControl;
                TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);

                if (hit.Column == timeCol)
                {
                    if (hit.HitInfoType == HitInfoType.Cell)
                    {
                        object cellInfo = new TreeListCellToolTipInfo(hit.Node, hit.Column, null);
                        IVehicle vehicle = tree.GetDataRecordByNode(hit.Node) as IVehicle;

                        if (null != vehicle)
                        {
                            string toolTip = string.Format("{1}: {0}", vehicle.Mobitel.LastTimeGps.ToString(),
                                Resources.LastData);

                            e.Info = new DevExpress.Utils.ToolTipControlInfo(cellInfo, toolTip);
                        }

                    }
                    else if (hit.HitInfoType == HitInfoType.Column)
                    {
                        e.Info = new DevExpress.Utils.ToolTipControlInfo(hit.Column, TreeViewService.GetToolTip());
                    }
                }
                else if (hit.Column == _titleCol)
                {
                    object cellInfo = new TreeListCellToolTipInfo( hit.Node, hit.Column, null );
                    IVehicle vehicle = tree.GetDataRecordByNode( hit.Node ) as IVehicle;

                    if( null != vehicle )
                    {
                        string toolTip = string.Format( "{0}", vehicle.VehicleComment);
                        e.Info = new DevExpress.Utils.ToolTipControlInfo( cellInfo, toolTip );
                    }
                }
            }
        }

        public void SetChecked()
        {
            CheckNodeFromObjectOperation operation = new CheckNodeFromObjectOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void GetChecked()
        {
            CheckObjectFromNodeOperation operation = new CheckObjectFromNodeOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void SetCheckedFromModel()
        {
            SetChecked();
        }

        private void bbiSynhro_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ConfirmSynhro, StaticMethods.GetTotalMessageCaption(),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) 
                return;

            GetChecked();
            if (SynhroCheckedVehicles != null) SynhroCheckedVehicles();
        }

        bool isFlagVisible = false;
        List<string> treeListVehicel;

        private void RefreshingAfterRefresh()
        {
            CheckNodeAfterRefresh operation = new CheckNodeAfterRefresh(treeListVehicel);
            _tree.NodesIterator.DoOperation(operation);
        }

        private void _tree_VisibleChanged(object sender, EventArgs e)
        {
            if (isFlagVisible)
            {
                RefreshTree();
                //RefreshingAfterRefresh();
                isFlagVisible = false;
            }
            else
            {
                //TreeList treeList = (TreeList)sender;
                //List<TreeListNode> treeListNode = treeList.GetAllCheckedNodes();
                //List<string> treeVehicel = new List<string>();

                //foreach (TreeListNode node in treeListNode)
                //{
                //    IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                //    if (vehicle != null)
                //    {
                //        treeVehicel.Add(vehicle.Info);
                //    }
                //}

                //treeListVehicel = treeVehicel;
                isFlagVisible = true;
            }
        }
    }
}
