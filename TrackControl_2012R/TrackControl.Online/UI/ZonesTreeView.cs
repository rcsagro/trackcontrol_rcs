using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.Utils;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.Zones;
using TrackControl.Online.Properties;

namespace TrackControl.Online
{
  public partial class ZonesTreeView : XtraUserControl
  {
    bool _isLoaded;
    bool _isGaMode;
    IZonesManager _manager;
    string _condition;
    object _current;

    public event ZonesVisibilityChanged VisibilityChanged;
    public event Action<IZone> ZoneNodeDoubleClicked;
    public event Action<IZone> PanZoneClicked;
    public event Action<IZone> VehiclesInsideClicked;
    private DevExpress.XtraBars.BarButtonItem bbiRefresh;

    public ZonesTreeView(IZonesManager manager)
    {
      _manager = manager;
      InitializeComponent();
      init();

      initPopupItems();
      setGaMode();
    }

      private void init()
      {
          this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
          this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {bbiRefresh});
          this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[]
          {
              new DevExpress.XtraBars.LinkPersistInfo(bbiRefresh)
          });
          bbiRefresh.Caption = "Refresh";
          bbiRefresh.Hint = "Refresh";
          this.bbiRefresh.Glyph = _images.Images[1];
          bbiRefresh.Id = 7;
          bbiRefresh.Name = "bbiRefresh";
          bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
          _areaBtn.Glyph = Shared.Area;
          _eraseBtn.Glyph = Shared.FunnelClear;
          _funnelBox.Caption = Resources.Funnel;
          _eraseBtn.Caption = Resources.ClearFunnel;
          _areaBtn.Caption = Resources.Area;
          _areaGaBtn.Caption = Resources.AreaInGa;
          _areaKmBtn.Caption = Resources.AreaInKm;
          _titleCol.Caption = Resources.Title;
          bbiExpand.Glyph = Shared.Expand;
          bbiCollapce.Glyph = Shared.Collapce;
          bbiRefresh.Caption = Resources.Refresh;
          bbiRefresh.Hint = Resources.Refresh;
      }

      public void RefreshTree()
    {
      _isLoaded = false;
      _tree.RefreshDataSource();
    }

      private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
      {
          RefreshTree();
      }

    void this_Load(object sender, EventArgs e)
    {
      _tree.DataSource = new object();
      TreeViewService.TreeListInitialView(_tree);
      setCondition(String.Empty);
    }

    void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
    {
      if (!_isLoaded)
      {
        e.Children = new ZonesGroup[] { _manager.Root };
        _isLoaded = true;
      }
      else
      {
        ZonesGroup group = e.Node as ZonesGroup;
        if (null != group)
        {
          IList children = new List<object>();
          foreach (ZonesGroup zg in group.OwnGroups)
            if (zg.AllItems.Count > 0)
              children.Add(zg);
          foreach (IZone zone in group.OwnItems)
            children.Add(zone);

          e.Children = children;
        }
        else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
        {
          e.Children = new object[] { };
        }
      }
    }

    void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
    {
      if (e.Node is ZonesGroup)
      {
        ZonesGroup group = (ZonesGroup)e.Node;
        if (e.Column == _titleCol)
          e.CellData = group.NameWithCounter;
      }
      else if (e.Node is IZone)
      {
        IZone zone = (IZone)e.Node;
        if (e.Column == _titleCol)
          e.CellData = zone.Name;
        else if (e.Column == _areaCol)
          e.CellData = _isGaMode ? zone.AreaGa : zone.AreaKm;
      }
    }

    void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
    {
      TreeViewService.FixNodeState(e);
    }

    void tree_AfterCheckNode(object sender, NodeEventArgs e)
    {
      TreeListNode node = e.Node;
      TreeViewService.SetCheckedChildNodes(node);
      TreeViewService.SetCheckedParentNodes(node);
      _tree.FocusedNode = node;
      Entity entity = (Entity)_tree.GetDataRecordByNode(node);
      changeVisibility(entity, e.Node.Checked);
    }

    void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
    {
      Rectangle rect = e.SelectRect;
      rect.X += (rect.Width - 16) / 2;
      rect.Y += (rect.Height - 16) / 2;
      rect.Width = 16;
      rect.Height = 16;

      object obj = _tree.GetDataRecordByNode(e.Node);
      if (obj is IZone)
      {
        IZone zone = (IZone)obj;
        e.Graphics.DrawImage(zone.Style.Icon, rect);
      }
      else if (obj is ZonesGroup)
      {
        ZonesGroup group = (ZonesGroup)obj;
        e.Graphics.DrawImage(group.Style.Icon, rect);
      }

      e.Handled = true;
    }

    void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
    {
      object obj = _tree.GetDataRecordByNode(e.Node);
      if (obj is IZone) return;
      e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

      if (e.Node != _tree.FocusedNode)
        e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
    }

    void tree_DoubleClick(object sender, EventArgs e)
    {
      Point point = _tree.PointToClient(Control.MousePosition);
      TreeListHitInfo info = _tree.CalcHitInfo(point);
      TreeListNode node = info.Node;
      if (null != node)
      {
        IZone zone = _tree.GetDataRecordByNode(node) as IZone;
        if (null != zone)
        {
          node.Checked = true;
          TreeViewService.SetCheckedParentNodes(node);
          changeVisibility((Entity)zone, true);
          onZoneNodeDoubleClicked(zone);
        }
      }
    }

    void tree_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None
          && TreeListState.Regular == _tree.State)
      {
        _popup.ItemLinks.Clear();

        Point pt = _tree.PointToClient(MousePosition);
        TreeListHitInfo info = _tree.CalcHitInfo(pt);
        TreeListNode node = info.Node;
        if (null != node)
        {
          _tree.FocusedNode = node;
          _current = _tree.GetDataRecordByNode(info.Node);
          if (_current is IZone)
          {
            _popup.ItemLinks.Add(_panZoneBtn);
            _popup.ItemLinks.Add(_vehiclesInsideBtn);
          }
          _popup.ShowPopup(MousePosition);
        }
      }
    }

    void tree_FilterNode(object sender, FilterNodeEventArgs e)
    {
        if( _condition == null )
        {
            _condition = "";
        }
        else
        {
            _condition = _condition.ToUpper();
        }
        IZone zone = _tree.GetDataRecordByNode(e.Node) as IZone;
        if (null != zone)
        {
            e.Node.Visible = zone.Name.ToUpper().Contains(_condition);
            e.Handled = true;
        }
    }

    void funnelRepo_KeyUp(object sender, KeyEventArgs e)
    {
      TextEdit edit = (TextEdit)sender;
      setCondition(edit.Text);
    }

    void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      _funnelBox.EditValue = String.Empty;
      setCondition(String.Empty);
    }

    void areaKmBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      setKmMode();
    }

    void areaGaBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      setGaMode();
    }

    void setGaMode()
    {
      _isGaMode = true;
      _areaCol.Caption = Resources.S_Ga;
      RefreshTree();
    }

    void setKmMode()
    {
      _isGaMode = false;
      _areaCol.Caption = Resources.S_Km;
      RefreshTree();
    }

    void setCondition(string condition)
    {
      _condition = condition;
      if (_condition.Length > 0)
      {
        _eraseBtn.Enabled = true;
        _tree.FilterNodes();
        hideEmpty();
      }
      else
      {
        _eraseBtn.Enabled = false;
        TreeListNode root = _tree.Nodes[0];
        TreeViewService.ShowAll(root);
      }
    }

    void hideEmpty()
    {
      TreeListNode root = _tree.Nodes[0];
      foreach (TreeListNode node in root.Nodes)
      {
        ZonesGroup group = _tree.GetDataRecordByNode(node) as ZonesGroup;
        if (null != group)
        {
          if (TreeViewService.GetVisibleCount(node) > 0)
            node.Visible = true;
          else
            node.Visible = false;
        }
      }
    }

    void changeVisibility(Entity entity, bool visibility)
    {
      List<IZone> zones = new List<IZone>();
      if (entity is IZone)
      {
        IZone zone = (IZone)entity;
        zones.Add(zone);
      }
      else if (entity is ZonesGroup)
      {
        ZonesGroup group = (ZonesGroup)entity;
        zones.AddRange(group.AllItems);
      }
      onVisibilityChanged(zones, visibility);
    }

    void onVisibilityChanged(IList<IZone> zones, bool visibility)
    {
      ZonesVisibilityChanged handler = VisibilityChanged;
      if (null != handler)
        handler(zones, visibility);
    }

    void onZoneNodeDoubleClicked(IZone zone)
    {
      Action<IZone> handler = ZoneNodeDoubleClicked;
      if (null != handler)
        handler(zone);
    }

    void onPanZoneClicked(IZone zone)
    {
      Action<IZone> handler = PanZoneClicked;
      if (null != handler)
        handler(zone);
    }

    void onVehiclesInsideClicked(IZone zone)
    {
      Action<IZone> handler = VehiclesInsideClicked;
      if (null != handler)
        handler(zone);
    }

    #region --   �������� ������������ ����   --

    BarButtonItem _panZoneBtn;
    BarButtonItem _vehiclesInsideBtn;

    void initPopupItems()
    {
      _panZoneBtn = new BarButtonItem();
      _panZoneBtn.Caption = Resources.ShowZone;
      _panZoneBtn.Glyph = Shared.ZoomFit;
      _panZoneBtn.ItemClick += panZoneBtn_ItemClick;

      _vehiclesInsideBtn = new BarButtonItem();
      _vehiclesInsideBtn.Caption = Resources.VehiclesInZone;
      _vehiclesInsideBtn.Glyph = Shared.Car;
      _vehiclesInsideBtn.ItemClick += vehiclesInsideBtn_ItemClick;
    }

    // ���������� � Dispose().  ��. ���� "ZonesTreeView.Designer.cs";
    void disposePopupItems()
    {
      _panZoneBtn.ItemClick -= panZoneBtn_ItemClick;
      _vehiclesInsideBtn.ItemClick -= vehiclesInsideBtn_ItemClick;
    }

    void panZoneBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      IZone zone = _current as IZone;
      if (null != zone)
      {
        TreeListNode node = _tree.Selection[0];
        node.Checked = true;
        TreeViewService.SetCheckedParentNodes(node);
        changeVisibility((Entity)zone, true);
        onPanZoneClicked(zone);
      }
    }

    void vehiclesInsideBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      IZone zone = _current as IZone;
      if (null != zone)
        onVehiclesInsideClicked(zone);
    }

    #endregion

    private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
    {
        _tree.ExpandAll();  
    }

    private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
    {
        _tree.CollapseAll();
        TreeViewService.TreeListInitialView(_tree);
    }

  }
}
