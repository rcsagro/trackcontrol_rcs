using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;

using System;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.Online.Properties;
using DevExpress.XtraEditors.Controls;

namespace TrackControl.Online
{
    public partial class InfoControl : XtraUserControl
    {
        private ITreeModel<OnlineVehicle> _model;

        public event Action<OnlineVehicle> VehicleChanged = delegate { };
        public event Action<PointLatLng> Selected = delegate { };

        public InfoControl(ITreeModel<OnlineVehicle> model)
        {
            _model = model;
            InitializeComponent();
            init();

            _grid.DataSource = _model.Checked;
            BindingContext[_grid.DataSource, _grid.DataMember].PositionChanged += onPositionChanged;
        }

        /// <summary>
        /// ��������� ������������� �����
        /// </summary>
        public void UpdateInfo()
        {
            if (InvokeRequired && IsHandleCreated)
            {
                MethodInvoker m = delegate
                {
                    _grid.RefreshDataSource();
                    onPositionChanged(null, null);
                };
                Invoke(m);
            }
            else
            {
                _grid.RefreshDataSource();
                onPositionChanged(null, null);
            }
        }

        public void RefreshGrid()
        {
            if (InvokeRequired && IsHandleCreated)
            {
                MethodInvoker m = delegate
                {
                    _grid.DataSource = _model.Checked;
                    onPositionChanged(null, null);
                };
                Invoke(m);
            }
            else
            {
                _grid.DataSource = _model.Checked;
                onPositionChanged(null, null);
            }
        }

        /// <summary>
        /// ������������ ������� � ������, �������������� ������
        /// ������������� �������� � ��������� ID
        /// </summary>
        /// <param name="id">ID ������������� ��������</param>
        public void ScrollTo(int id)
        {
            ColumnView view = (ColumnView) _grid.MainView;
            GridColumn col = view.Columns["MobitelId"];
            int rh = view.LocateByValue(0, col, id);
            if (GridControl.InvalidRowHandle != rh)
            {
                view.FocusedRowHandle = rh;
                OnlineVehicle v = (OnlineVehicle) view.GetRow(rh);
                makeVehicleSelected(v);
                Selected(v.Track.LastPoint.LatLng);
            }
        }

        private void onPositionChanged(object sender, EventArgs e)
        {
            CurrencyManager cm = (CurrencyManager) BindingContext[_grid.DataSource, _grid.DataMember];
            if (cm.Count > 0)
            {
                OnlineVehicle v = (OnlineVehicle) cm.Current;
                VehicleChanged(v);
            }
            else
            {
                VehicleChanged(null);
            }
        }

        private void _grid_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CurrencyManager cm = (CurrencyManager) BindingContext[_grid.DataSource, _grid.DataMember];

            if (cm.Count > 0)
            {
                OnlineVehicle v = (OnlineVehicle) cm.Current;
                makeVehicleSelected(v);
                if (v.Track != null)
                    Selected(v.Track.LastPoint.LatLng);
            }
        }

        private void makeVehicleSelected(OnlineVehicle vehicle)
        {
            foreach (OnlineVehicle ov in _model.GetAll())
            {
                ov.IsSelected = vehicle == ov;
            }
        }

        private void init()
        {
            _location.Caption = Resources.locationCaption;
            _titleCol.Caption = Resources.RegNumber;
            _descriptionCol.Caption = Resources.MarkModel;
            _driverCol.Caption = Resources.Driver;
            _speedCol.Caption = Resources.Speed;
            _stateCol.Caption = Resources.State;
            _lastTimeCol.Caption = Resources.LastData;
            _powerCol.Caption = Resources.Power;
            _logicSensorState.Caption = Resources.Status;
            _powerRepo.Items.Clear();
            _powerRepo.Items.AddRange(new ImageComboBoxItem[]
            {
                new ImageComboBoxItem(String.Format(" {0}", Resources.OnboardPower), 0, 0),
                new ImageComboBoxItem(String.Format(" {0}", Resources.BatteryPower), 1, 1),
                new ImageComboBoxItem(String.Format(" {0}", Resources.OnboardPower), 2, 2)
            });
        }
    }
}
