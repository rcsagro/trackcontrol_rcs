namespace TrackControl.Online
{
  partial class ZonesTreeView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      disposePopupItems();
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZonesTreeView));
            this._barManager = new DevExpress.XtraBars.BarManager();
            this._tools = new DevExpress.XtraBars.Bar();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this._funnelBox = new DevExpress.XtraBars.BarEditItem();
            this._funnelRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this._areaBtn = new DevExpress.XtraBars.BarButtonItem();
            this._areaPopup = new DevExpress.XtraBars.PopupMenu();
            this._areaGaBtn = new DevExpress.XtraBars.BarButtonItem();
            this._areaKmBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this._images = new DevExpress.Utils.ImageCollection();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._titleCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._areaCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._areaRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._popup = new DevExpress.XtraBars.PopupMenu();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._areaPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._areaRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("", new System.Guid("3c390cd6-0ce6-43c8-b760-b849d4cc760a"))});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._funnelBox,
            this._eraseBtn,
            this._areaBtn,
            this._areaGaBtn,
            this._areaKmBtn,
            this.bbiExpand,
            this.bbiCollapce});
            this._barManager.MaxItemId = 8;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._funnelRepo});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCollapce),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._funnelBox, "", false, true, true, 100),
            new DevExpress.XtraBars.LinkPersistInfo(this._eraseBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._areaBtn, "", true, false, true, 0)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // bbiExpand
            // 
            this.bbiExpand.Caption = "Expand";
            this.bbiExpand.Id = 5;
            this.bbiExpand.Name = "bbiExpand";
            this.bbiExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExpand_ItemClick);
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Caption = "Collapce";
            this.bbiCollapce.Id = 6;
            this.bbiCollapce.Name = "bbiCollapce";
            this.bbiCollapce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCollapce_ItemClick);
            // 
            // _funnelBox
            // 
            this._funnelBox.Caption = "������";
            this._funnelBox.Edit = this._funnelRepo;
            this._funnelBox.Id = 0;
            this._funnelBox.Name = "_funnelBox";
            this._funnelBox.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _funnelRepo
            // 
            this._funnelRepo.AutoHeight = false;
            this._funnelRepo.MaxLength = 20;
            this._funnelRepo.Name = "_funnelRepo";
            this._funnelRepo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.funnelRepo_KeyUp);
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Caption = "�������� ������";
            this._eraseBtn.Id = 1;
            this._eraseBtn.Name = "_eraseBtn";
            this._eraseBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eraseBtn_ItemClick);
            // 
            // _areaBtn
            // 
            this._areaBtn.ActAsDropDown = true;
            this._areaBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._areaBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._areaBtn.Caption = "�������";
            this._areaBtn.DropDownControl = this._areaPopup;
            this._areaBtn.Id = 2;
            this._areaBtn.Name = "_areaBtn";
            this._areaBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _areaPopup
            // 
            this._areaPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._areaGaBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._areaKmBtn)});
            this._areaPopup.Manager = this._barManager;
            this._areaPopup.Name = "_areaPopup";
            // 
            // _areaGaBtn
            // 
            this._areaGaBtn.Caption = "������� � ��������";
            this._areaGaBtn.Id = 3;
            this._areaGaBtn.Name = "_areaGaBtn";
            this._areaGaBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.areaGaBtn_ItemClick);
            // 
            // _areaKmBtn
            // 
            this._areaKmBtn.Caption = "������� � ��. ��";
            this._areaKmBtn.Id = 4;
            this._areaKmBtn.Name = "_areaKmBtn";
            this._areaKmBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.areaKmBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(352, 29);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 509);
            this.barDockControlBottom.Size = new System.Drawing.Size(352, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 29);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 480);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(352, 29);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 480);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Id = -1;
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Star");
            this._images.Images.SetKeyName(1, "arrow-circle-045-left.png");
            // 
            // _tree
            // 
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._titleCol,
            this._areaCol});
            this._tree.ColumnsImageList = this._images;
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.Location = new System.Drawing.Point(0, 29);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.AllowIndeterminateCheckState = true;
            this._tree.OptionsBehavior.EnableFiltering = true;
            this._tree.OptionsMenu.EnableColumnMenu = false;
            this._tree.OptionsMenu.EnableFooterMenu = false;
            this._tree.OptionsView.ShowCheckBoxes = true;
            this._tree.OptionsView.ShowIndicator = false;
            this._tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._areaRepo});
            this._tree.SelectImageList = this._images;
            this._tree.Size = new System.Drawing.Size(352, 480);
            this._tree.TabIndex = 4;
            this._tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this._tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            this._tree.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.tree_FilterNode);
            this._tree.DoubleClick += new System.EventHandler(this.tree_DoubleClick);
            this._tree.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tree_MouseUp);
            // 
            // _titleCol
            // 
            this._titleCol.Caption = "��������";
            this._titleCol.FieldName = "Name";
            this._titleCol.MinWidth = 250;
            this._titleCol.Name = "_titleCol";
            this._titleCol.OptionsColumn.AllowEdit = false;
            this._titleCol.OptionsColumn.AllowFocus = false;
            this._titleCol.OptionsColumn.AllowMove = false;
            this._titleCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._titleCol.OptionsColumn.ShowInCustomizationForm = false;
            this._titleCol.Visible = true;
            this._titleCol.VisibleIndex = 0;
            this._titleCol.Width = 250;
            // 
            // _areaCol
            // 
            this._areaCol.AppearanceCell.Options.UseTextOptions = true;
            this._areaCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._areaCol.AppearanceHeader.Options.UseTextOptions = true;
            this._areaCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._areaCol.ColumnEdit = this._areaRepo;
            this._areaCol.FieldName = "Area";
            this._areaCol.ImageIndex = 0;
            this._areaCol.MinWidth = 75;
            this._areaCol.Name = "_areaCol";
            this._areaCol.OptionsColumn.AllowEdit = false;
            this._areaCol.OptionsColumn.AllowFocus = false;
            this._areaCol.OptionsColumn.AllowMove = false;
            this._areaCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._areaCol.OptionsColumn.ShowInCustomizationForm = false;
            this._areaCol.Visible = true;
            this._areaCol.VisibleIndex = 1;
            // 
            // _areaRepo
            // 
            this._areaRepo.AutoHeight = false;
            this._areaRepo.DisplayFormat.FormatString = "N2";
            this._areaRepo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._areaRepo.Name = "_areaRepo";
            // 
            // _popup
            // 
            this._popup.Manager = this._barManager;
            this._popup.Name = "_popup";
            // 
            // ZonesTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ZonesTreeView";
            this.Size = new System.Drawing.Size(352, 509);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._areaPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._areaRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraTreeList.TreeList _tree;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraBars.BarEditItem _funnelBox;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _funnelRepo;
    private DevExpress.XtraBars.BarButtonItem _eraseBtn;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _titleCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _areaCol;
    private DevExpress.XtraBars.PopupMenu _popup;
    private DevExpress.XtraBars.BarButtonItem _areaBtn;
    private DevExpress.XtraBars.PopupMenu _areaPopup;
    private DevExpress.XtraBars.BarButtonItem _areaGaBtn;
    private DevExpress.XtraBars.BarButtonItem _areaKmBtn;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _areaRepo;
    private DevExpress.XtraBars.BarButtonItem bbiExpand;
    private DevExpress.XtraBars.BarButtonItem bbiCollapce;
  }
}
