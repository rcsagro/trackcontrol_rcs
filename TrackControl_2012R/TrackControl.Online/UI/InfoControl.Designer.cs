namespace TrackControl.Online
{
  partial class InfoControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoControl));
            this._grid = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._idCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._iconCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._iconEdit = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this._titleCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._descriptionCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._driverCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._gpsCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._repoGps = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._gpsImages = new DevExpress.Utils.ImageCollection();
            this._speedCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._stateCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._lastTimeCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._location = new DevExpress.XtraGrid.Columns.GridColumn();
            this._powerCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this._powerRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._powerImages = new DevExpress.Utils.ImageCollection();
            this._logicSensorState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._imStatus = new DevExpress.Utils.ImageCollection();
            this._empty = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._iconEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoGps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._gpsImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._powerRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._powerImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.MainView = this.gridView1;
            this._grid.Margin = new System.Windows.Forms.Padding(0);
            this._grid.Name = "_grid";
            this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._iconEdit,
            this._repoGps,
            this._powerRepo,
            this.icbStatus});
            this._grid.Size = new System.Drawing.Size(1087, 249);
            this._grid.TabIndex = 0;
            this._grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this._grid.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this._grid_MouseDoubleClick);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._idCol,
            this._iconCol,
            this._titleCol,
            this._descriptionCol,
            this._driverCol,
            this._gpsCol,
            this._speedCol,
            this._stateCol,
            this._lastTimeCol,
            this._location,
            this._powerCol,
            this._logicSensorState,
            this._empty});
            this.gridView1.GridControl = this._grid;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckDefaultDetail;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.PaintStyleName = "Skin";
            // 
            // _idCol
            // 
            this._idCol.AppearanceHeader.Options.UseTextOptions = true;
            this._idCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._idCol.Caption = "ID";
            this._idCol.FieldName = "MobitelId";
            this._idCol.Name = "_idCol";
            // 
            // _iconCol
            // 
            this._iconCol.AppearanceHeader.Options.UseTextOptions = true;
            this._iconCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._iconCol.Caption = " ";
            this._iconCol.ColumnEdit = this._iconEdit;
            this._iconCol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this._iconCol.FieldName = "Icon";
            this._iconCol.ImageAlignment = System.Drawing.StringAlignment.Center;
            this._iconCol.MaxWidth = 26;
            this._iconCol.MinWidth = 26;
            this._iconCol.Name = "_iconCol";
            this._iconCol.OptionsColumn.AllowEdit = false;
            this._iconCol.OptionsColumn.AllowFocus = false;
            this._iconCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._iconCol.OptionsColumn.AllowIncrementalSearch = false;
            this._iconCol.OptionsColumn.AllowMove = false;
            this._iconCol.OptionsColumn.AllowSize = false;
            this._iconCol.OptionsColumn.FixedWidth = true;
            this._iconCol.OptionsColumn.ReadOnly = true;
            this._iconCol.Visible = true;
            this._iconCol.VisibleIndex = 0;
            this._iconCol.Width = 24;
            // 
            // _iconEdit
            // 
            this._iconEdit.Name = "_iconEdit";
            // 
            // _titleCol
            // 
            this._titleCol.AppearanceHeader.Options.UseTextOptions = true;
            this._titleCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._titleCol.Caption = "�����";
            this._titleCol.FieldName = "RegNumber";
            this._titleCol.MaxWidth = 80;
            this._titleCol.MinWidth = 80;
            this._titleCol.Name = "_titleCol";
            this._titleCol.OptionsColumn.AllowEdit = false;
            this._titleCol.OptionsColumn.AllowFocus = false;
            this._titleCol.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._titleCol.OptionsColumn.ReadOnly = true;
            this._titleCol.Visible = true;
            this._titleCol.VisibleIndex = 1;
            this._titleCol.Width = 80;
            // 
            // _descriptionCol
            // 
            this._descriptionCol.AppearanceHeader.Options.UseTextOptions = true;
            this._descriptionCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._descriptionCol.Caption = "�����, ������";
            this._descriptionCol.FieldName = "MarkModel";
            this._descriptionCol.MaxWidth = 200;
            this._descriptionCol.MinWidth = 200;
            this._descriptionCol.Name = "_descriptionCol";
            this._descriptionCol.OptionsColumn.AllowEdit = false;
            this._descriptionCol.OptionsColumn.AllowFocus = false;
            this._descriptionCol.OptionsColumn.ReadOnly = true;
            this._descriptionCol.Visible = true;
            this._descriptionCol.VisibleIndex = 2;
            this._descriptionCol.Width = 200;
            // 
            // _driverCol
            // 
            this._driverCol.AppearanceHeader.Options.UseTextOptions = true;
            this._driverCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._driverCol.Caption = "��������";
            this._driverCol.FieldName = "DriverNameStep";
            this._driverCol.Name = "_driverCol";
            this._driverCol.OptionsColumn.AllowEdit = false;
            this._driverCol.OptionsColumn.AllowFocus = false;
            this._driverCol.OptionsColumn.ReadOnly = true;
            this._driverCol.Visible = true;
            this._driverCol.VisibleIndex = 3;
            this._driverCol.Width = 250;
            // 
            // _gpsCol
            // 
            this._gpsCol.AppearanceHeader.Options.UseTextOptions = true;
            this._gpsCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._gpsCol.Caption = "GPS";
            this._gpsCol.ColumnEdit = this._repoGps;
            this._gpsCol.FieldName = "IntGpsSignal";
            this._gpsCol.ImageAlignment = System.Drawing.StringAlignment.Center;
            this._gpsCol.MaxWidth = 40;
            this._gpsCol.MinWidth = 40;
            this._gpsCol.Name = "_gpsCol";
            this._gpsCol.OptionsColumn.AllowEdit = false;
            this._gpsCol.OptionsColumn.AllowFocus = false;
            this._gpsCol.OptionsColumn.ReadOnly = true;
            this._gpsCol.Visible = true;
            this._gpsCol.VisibleIndex = 4;
            this._gpsCol.Width = 40;
            // 
            // _repoGps
            // 
            this._repoGps.AutoHeight = false;
            this._repoGps.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._repoGps.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._repoGps.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2)});
            this._repoGps.Name = "_repoGps";
            this._repoGps.SmallImages = this._gpsImages;
            // 
            // _gpsImages
            // 
            this._gpsImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_gpsImages.ImageStream")));
            this._gpsImages.Images.SetKeyName(0, "NotValid.png");
            this._gpsImages.Images.SetKeyName(1, "Valid.png");
            this._gpsImages.Images.SetKeyName(2, "NotDetected.png");
            // 
            // _speedCol
            // 
            this._speedCol.AppearanceHeader.Options.UseTextOptions = true;
            this._speedCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._speedCol.Caption = "��������";
            this._speedCol.DisplayFormat.FormatString = "N2";
            this._speedCol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._speedCol.FieldName = "Speed";
            this._speedCol.MaxWidth = 80;
            this._speedCol.MinWidth = 80;
            this._speedCol.Name = "_speedCol";
            this._speedCol.OptionsColumn.AllowEdit = false;
            this._speedCol.OptionsColumn.AllowFocus = false;
            this._speedCol.OptionsColumn.ReadOnly = true;
            this._speedCol.Visible = true;
            this._speedCol.VisibleIndex = 5;
            this._speedCol.Width = 80;
            // 
            // _stateCol
            // 
            this._stateCol.AppearanceHeader.Options.UseTextOptions = true;
            this._stateCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._stateCol.Caption = "���������";
            this._stateCol.FieldName = "StateInfo";
            this._stateCol.MaxWidth = 160;
            this._stateCol.MinWidth = 160;
            this._stateCol.Name = "_stateCol";
            this._stateCol.OptionsColumn.AllowEdit = false;
            this._stateCol.OptionsColumn.AllowFocus = false;
            this._stateCol.OptionsColumn.ReadOnly = true;
            this._stateCol.Visible = true;
            this._stateCol.VisibleIndex = 6;
            this._stateCol.Width = 160;
            // 
            // _lastTimeCol
            // 
            this._lastTimeCol.AppearanceHeader.Options.UseTextOptions = true;
            this._lastTimeCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._lastTimeCol.Caption = "��������� ������";
            this._lastTimeCol.DisplayFormat.FormatString = "G";
            this._lastTimeCol.FieldName = "LastTime";
            this._lastTimeCol.MaxWidth = 130;
            this._lastTimeCol.MinWidth = 130;
            this._lastTimeCol.Name = "_lastTimeCol";
            this._lastTimeCol.OptionsColumn.AllowEdit = false;
            this._lastTimeCol.OptionsColumn.AllowFocus = false;
            this._lastTimeCol.OptionsColumn.ReadOnly = true;
            this._lastTimeCol.Visible = true;
            this._lastTimeCol.VisibleIndex = 7;
            this._lastTimeCol.Width = 130;
            // 
            // _location
            // 
            this._location.AppearanceHeader.Options.UseTextOptions = true;
            this._location.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._location.Caption = "��������������";
            this._location.FieldName = "Location";
            this._location.Name = "_location";
            this._location.Visible = true;
            this._location.VisibleIndex = 8;
            this._location.Width = 20;
            // 
            // _powerCol
            // 
            this._powerCol.AppearanceHeader.Options.UseTextOptions = true;
            this._powerCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._powerCol.Caption = "�������";
            this._powerCol.ColumnEdit = this._powerRepo;
            this._powerCol.FieldName = "IntPowerSource";
            this._powerCol.MaxWidth = 100;
            this._powerCol.MinWidth = 100;
            this._powerCol.Name = "_powerCol";
            this._powerCol.OptionsColumn.AllowEdit = false;
            this._powerCol.OptionsColumn.AllowFocus = false;
            this._powerCol.OptionsColumn.ReadOnly = true;
            this._powerCol.Visible = true;
            this._powerCol.VisibleIndex = 9;
            this._powerCol.Width = 100;
            // 
            // _powerRepo
            // 
            this._powerRepo.AutoHeight = false;
            this._powerRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._powerRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ���� ����", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ������", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem(" ���� ����", 2, 2)});
            this._powerRepo.Name = "_powerRepo";
            this._powerRepo.SmallImages = this._powerImages;
            // 
            // _powerImages
            // 
            this._powerImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_powerImages.ImageStream")));
            this._powerImages.Images.SetKeyName(0, "OnBoard.png");
            this._powerImages.Images.SetKeyName(1, "Battery.png");
            this._powerImages.Images.SetKeyName(2, "NotDetected.png");
            // 
            // _logicSensorState
            // 
            this._logicSensorState.AppearanceCell.Options.UseImage = true;
            this._logicSensorState.AppearanceHeader.Options.UseTextOptions = true;
            this._logicSensorState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._logicSensorState.Caption = "������";
            this._logicSensorState.ColumnEdit = this.icbStatus;
            this._logicSensorState.FieldName = "LogicSensorState";
            this._logicSensorState.ImageAlignment = System.Drawing.StringAlignment.Center;
            this._logicSensorState.Name = "_logicSensorState";
            this._logicSensorState.OptionsColumn.AllowEdit = false;
            this._logicSensorState.OptionsColumn.AllowFocus = false;
            this._logicSensorState.OptionsColumn.ReadOnly = true;
            this._logicSensorState.Visible = true;
            this._logicSensorState.VisibleIndex = 10;
            this._logicSensorState.Width = 20;
            // 
            // icbStatus
            // 
            this.icbStatus.AutoHeight = false;
            this.icbStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbStatus.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbStatus.ImmediatePopup = true;
            this.icbStatus.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("��������", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����������", 2, -1)});
            this.icbStatus.Name = "icbStatus";
            this.icbStatus.SmallImages = this._imStatus;
            // 
            // _imStatus
            // 
            this._imStatus.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_imStatus.ImageStream")));
            this._imStatus.Images.SetKeyName(0, "car--plus.png");
            this._imStatus.Images.SetKeyName(1, "car-red.png");
            // 
            // _empty
            // 
            this._empty.AppearanceHeader.Options.UseTextOptions = true;
            this._empty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._empty.Caption = " ";
            this._empty.Name = "_empty";
            this._empty.OptionsColumn.AllowEdit = false;
            this._empty.OptionsColumn.AllowFocus = false;
            this._empty.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this._empty.OptionsColumn.ReadOnly = true;
            this._empty.Visible = true;
            this._empty.VisibleIndex = 11;
            this._empty.Width = 20;
            // 
            // InfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "InfoControl";
            this.Size = new System.Drawing.Size(1087, 249);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._iconEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._repoGps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._gpsImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._powerRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._powerImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imStatus)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraGrid.GridControl _grid;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    private DevExpress.XtraGrid.Columns.GridColumn _iconCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit _iconEdit;
    private DevExpress.XtraGrid.Columns.GridColumn _titleCol;
    private DevExpress.XtraGrid.Columns.GridColumn _descriptionCol;
    private DevExpress.XtraGrid.Columns.GridColumn _driverCol;
    private DevExpress.XtraGrid.Columns.GridColumn _gpsCol;
    private DevExpress.XtraGrid.Columns.GridColumn _speedCol;
    private DevExpress.XtraGrid.Columns.GridColumn _stateCol;
    private DevExpress.XtraGrid.Columns.GridColumn _lastTimeCol;
    private DevExpress.XtraGrid.Columns.GridColumn _powerCol;
    private DevExpress.XtraGrid.Columns.GridColumn _empty;
    private DevExpress.XtraGrid.Columns.GridColumn _idCol;
    private DevExpress.Utils.ImageCollection _gpsImages;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _repoGps;
    private DevExpress.Utils.ImageCollection _powerImages;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _powerRepo;
    private DevExpress.XtraGrid.Columns.GridColumn _logicSensorState;
    private DevExpress.Utils.ImageCollection _imStatus;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbStatus;
    private DevExpress.XtraGrid.Columns.GridColumn _location;
  }
}
