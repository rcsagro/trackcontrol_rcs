using System;
using TrackControl.General ;
using TrackControl.Online.Properties;

namespace TrackControl.Online.Services
{
    public partial class AddressFinder : DevExpress.XtraEditors.XtraForm
    {
        public event Action<string> FindPointToAddress ;
        public event Action<PointLatLng> RedrawMarker;
        public event VoidHandler StopSearching;

        public AddressFinder()
        {
            InitializeComponent();
            Localization();
        }

        private void sbFind_Click(object sender, EventArgs e)
        {
            FindAdress();
        }

        private void FindAdress()
        {
            if (teAddress.Text.Length > 0 && FindPointToAddress != null) 
                FindPointToAddress(teAddress.Text);
        }

        public void ViewReceivedData(double lng, double lat, string address)
        {
            teLat.Text = lat.ToString();
            teLng.Text = lng.ToString();
            teAddressFinded.Text = address; 
        }

        private void sbShow_Click(object sender, EventArgs e)
        {
            double _lat, _lng;
            if (Double.TryParse(teLat.Text, out _lat) && Double.TryParse(teLng.Text, out _lng) && RedrawMarker != null && teAddressFinded.Text.Length > 0)
            RedrawMarker(new PointLatLng(_lat,_lng));
        }

        private void AddressFinder_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            if (StopSearching != null) StopSearching();
        }

        private void teAddress_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Return) FindAdress();
        }

        void Localization()
        {
            this.Text = Resources.AddressSearch;
            sbFind.Text = Resources.Find;
            sbShow.Text = Resources.Show;
            lbAddressPoint.Text = string.Format("{0}:",  Resources.Address);
            lbAddress.Text = string.Format("{0}:", Resources.Address);
            lbInfor.Text = Resources.InfoFinder;
            lbLat.Text = string.Format("{0}:", Resources.Latitude);
            lbLng.Text = string.Format("{0}:", Resources.Longitude);
            gcGeoPoint.Text = Resources.GeopointsParameters;
        }


    }
}