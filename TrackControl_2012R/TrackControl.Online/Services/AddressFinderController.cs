﻿using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.GMap.Core;
using TrackControl.GMap.MapProviders;
using System;
using System.Windows.Forms;  

namespace TrackControl.Online.Services
{
    public class AddressFinderController : Singleton<AddressFinderController>
    {
        AddressFinder _formAddressFinder;
        double _lat;
        double _lng;
        string _address;

        public event Action<PointLatLng> DrawPointOnMap;
        public event VoidHandler ClearMarker;
        public event VoidHandler StopSearching;

        public AddressFinderController()
        {
        }

        public void Start(IMainView ownerForm)
        {
            _formAddressFinder = new AddressFinder();  
            _formAddressFinder.FindPointToAddress +=onFindPointToAddress;
            _formAddressFinder.RedrawMarker += onRedrawMarker;
            _formAddressFinder.StopSearching += onStopSearching;
            _formAddressFinder.Owner = (Form)ownerForm;
            _formAddressFinder.Show(); 
        }

        public void FindAddressToPoint(PointLatLng point)
        {
            _lat = Math.Round(point.Lat,5);
            _lng = Math.Round(point.Lng, 5);
            FindAddress(point);
            _formAddressFinder.ViewReceivedData(_lng, _lat, _address);  
        }

        void onFindPointToAddress(string address)
        {
            GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
            {
                _lng = 0;
                _lat = 0;
                _address = "";
                //PointLatLng? pos = GMapProviders.GoogleMap.GetPoint(address, out status);
                PointLatLng? pos = GMapProviders.getOpenStreetMap.GetPoint(address, out status);
                if (pos != null && status == GeoCoderStatusCode.G_GEO_SUCCESS)
                {
                    _lng = pos.Value.Lng;
                    _lat = pos.Value.Lat;
                    PointLatLng findedPoint = new PointLatLng(pos.Value.Lat, pos.Value.Lng);
                    if (DrawPointOnMap != null) DrawPointOnMap(findedPoint);
                    FindAddress(findedPoint);
                }
                else
                    if (ClearMarker !=null) ClearMarker();
                _formAddressFinder.ViewReceivedData(_lng, _lat, _address);  
            }
        }

        private void FindAddress(PointLatLng findedPoint)
        {
            GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
            //var adr = GMapProviders.OpenStreetMap.GetPlacemarkOSM(findedPoint, out status);
            GMapProviders.SetStartParams();
            Placemark adr = null;
            if (GMapProviders.getOpenStreetMap.IsActive)
            {
                adr = GMapProviders.getOpenStreetMap.GetPlacemarkOSM(findedPoint, out status);
            }
            else if(GMapProviders.getRcsNominatim.IsActive)
            {
                adr = GMapProviders.getRcsNominatim.GetPlacemark(findedPoint, out status);
            }
            if (adr != null && status == GeoCoderStatusCode.G_GEO_SUCCESS)
            {
                _address = adr.Address;
            }
        }

        void onRedrawMarker(PointLatLng  point)
        {
            _lng = point.Lng;
            _lat = point.Lat;
            if (DrawPointOnMap !=null) DrawPointOnMap(point);
        }

        void onStopSearching()
        {
            _lng = 0;
            _lat = 0;
            _address = "";
            if (StopSearching != null) StopSearching();
        }
    }

}
