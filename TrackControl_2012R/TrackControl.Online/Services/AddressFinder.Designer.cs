namespace TrackControl.Online.Services
{
    partial class AddressFinder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressFinder));
            this.sbFind = new DevExpress.XtraEditors.SimpleButton();
            this.teAddress = new DevExpress.XtraEditors.TextEdit();
            this.lbAddress = new DevExpress.XtraEditors.LabelControl();
            this.lbInfor = new DevExpress.XtraEditors.LabelControl();
            this.gcGeoPoint = new DevExpress.XtraEditors.GroupControl();
            this.sbShow = new DevExpress.XtraEditors.SimpleButton();
            this.lbAddressPoint = new DevExpress.XtraEditors.LabelControl();
            this.teAddressFinded = new DevExpress.XtraEditors.TextEdit();
            this.lbLng = new DevExpress.XtraEditors.LabelControl();
            this.lbLat = new DevExpress.XtraEditors.LabelControl();
            this.teLng = new DevExpress.XtraEditors.TextEdit();
            this.teLat = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.teAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGeoPoint)).BeginInit();
            this.gcGeoPoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressFinded.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLng.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLat.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // sbFind
            // 
            this.sbFind.Location = new System.Drawing.Point(426, 18);
            this.sbFind.Name = "sbFind";
            this.sbFind.Size = new System.Drawing.Size(71, 23);
            this.sbFind.TabIndex = 1;
            this.sbFind.Text = "�����";
            this.sbFind.Click += new System.EventHandler(this.sbFind_Click);
            // 
            // teAddress
            // 
            this.teAddress.Location = new System.Drawing.Point(95, 21);
            this.teAddress.Name = "teAddress";
            this.teAddress.Size = new System.Drawing.Size(325, 20);
            this.teAddress.TabIndex = 0;
            this.teAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.teAddress_KeyDown);
            // 
            // lbAddress
            // 
            this.lbAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbAddress.Appearance.Options.UseFont = true;
            this.lbAddress.Location = new System.Drawing.Point(29, 24);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(40, 13);
            this.lbAddress.TabIndex = 4;
            this.lbAddress.Text = "�����:";
            // 
            // lbInfor
            // 
            this.lbInfor.Appearance.Options.UseTextOptions = true;
            this.lbInfor.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbInfor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbInfor.Location = new System.Drawing.Point(14, 47);
            this.lbInfor.Name = "lbInfor";
            this.lbInfor.Size = new System.Drawing.Size(489, 44);
            this.lbInfor.TabIndex = 9;
            this.lbInfor.Text = "������� ����� � ������� ���� ��� ������ �����.  ������ ������� ������ �� �����, �" +
                "������� �������� ���������� �� ���� �����.";
            // 
            // gcGeoPoint
            // 
            this.gcGeoPoint.Controls.Add(this.sbShow);
            this.gcGeoPoint.Controls.Add(this.lbAddressPoint);
            this.gcGeoPoint.Controls.Add(this.teAddressFinded);
            this.gcGeoPoint.Controls.Add(this.lbLng);
            this.gcGeoPoint.Controls.Add(this.lbLat);
            this.gcGeoPoint.Controls.Add(this.teLng);
            this.gcGeoPoint.Controls.Add(this.teLat);
            this.gcGeoPoint.Location = new System.Drawing.Point(10, 97);
            this.gcGeoPoint.Name = "gcGeoPoint";
            this.gcGeoPoint.Size = new System.Drawing.Size(493, 133);
            this.gcGeoPoint.TabIndex = 10;
            this.gcGeoPoint.Text = "��������� ��������";
            // 
            // sbShow
            // 
            this.sbShow.Location = new System.Drawing.Point(416, 25);
            this.sbShow.Name = "sbShow";
            this.sbShow.Size = new System.Drawing.Size(71, 23);
            this.sbShow.TabIndex = 10;
            this.sbShow.Text = "��������";
            this.sbShow.Click += new System.EventHandler(this.sbShow_Click);
            // 
            // lbAddressPoint
            // 
            this.lbAddressPoint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbAddressPoint.Appearance.Options.UseFont = true;
            this.lbAddressPoint.Location = new System.Drawing.Point(19, 107);
            this.lbAddressPoint.Name = "lbAddressPoint";
            this.lbAddressPoint.Size = new System.Drawing.Size(40, 13);
            this.lbAddressPoint.TabIndex = 15;
            this.lbAddressPoint.Text = "�����:";
            // 
            // teAddressFinded
            // 
            this.teAddressFinded.Location = new System.Drawing.Point(85, 104);
            this.teAddressFinded.Name = "teAddressFinded";
            this.teAddressFinded.Properties.AutoHeight = false;
            this.teAddressFinded.Properties.ReadOnly = true;
            this.teAddressFinded.Size = new System.Drawing.Size(402, 20);
            this.teAddressFinded.TabIndex = 14;
            this.teAddressFinded.TabStop = false;
            // 
            // lbLng
            // 
            this.lbLng.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbLng.Appearance.Options.UseFont = true;
            this.lbLng.Location = new System.Drawing.Point(19, 70);
            this.lbLng.Name = "lbLng";
            this.lbLng.Size = new System.Drawing.Size(54, 13);
            this.lbLng.TabIndex = 13;
            this.lbLng.Text = "�������:";
            // 
            // lbLat
            // 
            this.lbLat.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lbLat.Appearance.Options.UseFont = true;
            this.lbLat.Location = new System.Drawing.Point(19, 31);
            this.lbLat.Name = "lbLat";
            this.lbLat.Size = new System.Drawing.Size(48, 13);
            this.lbLat.TabIndex = 12;
            this.lbLat.Text = "������:";
            // 
            // teLng
            // 
            this.teLng.Location = new System.Drawing.Point(85, 67);
            this.teLng.Name = "teLng";
            this.teLng.Properties.ReadOnly = true;
            this.teLng.Size = new System.Drawing.Size(105, 20);
            this.teLng.TabIndex = 11;
            this.teLng.TabStop = false;
            // 
            // teLat
            // 
            this.teLat.Location = new System.Drawing.Point(85, 28);
            this.teLat.Name = "teLat";
            this.teLat.Properties.ReadOnly = true;
            this.teLat.Size = new System.Drawing.Size(105, 20);
            this.teLat.TabIndex = 9;
            this.teLat.TabStop = false;
            // 
            // AddressFinder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 238);
            this.Controls.Add(this.gcGeoPoint);
            this.Controls.Add(this.lbInfor);
            this.Controls.Add(this.lbAddress);
            this.Controls.Add(this.teAddress);
            this.Controls.Add(this.sbFind);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddressFinder";
            this.Text = "�������� �����";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddressFinder_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.teAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGeoPoint)).EndInit();
            this.gcGeoPoint.ResumeLayout(false);
            this.gcGeoPoint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teAddressFinded.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLng.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLat.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton sbFind;
        private DevExpress.XtraEditors.TextEdit teAddress;
        private DevExpress.XtraEditors.LabelControl lbAddress;
        private DevExpress.XtraEditors.LabelControl lbInfor;
        private DevExpress.XtraEditors.GroupControl gcGeoPoint;
        private DevExpress.XtraEditors.SimpleButton sbShow;
        private DevExpress.XtraEditors.LabelControl lbAddressPoint;
        private DevExpress.XtraEditors.TextEdit teAddressFinded;
        private DevExpress.XtraEditors.LabelControl lbLng;
        private DevExpress.XtraEditors.LabelControl lbLat;
        private DevExpress.XtraEditors.TextEdit teLng;
        private DevExpress.XtraEditors.TextEdit teLat;
    }
}