using System;

namespace TrackControl.Online
{
  public enum MotionState
  {
    NotDetected = 0, // ��������� �� ������������
    Moving = 1, // ��������
    Stopped = 2, // ���������
    Parking = 3, // �������
    Unsafe = 4,  // ���������
    ParkingLong = 5 // ������� ����� ������ � ����������
  }
}
