using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Vehicles;
using Timer = System.Threading.Timer;

namespace TrackControl.Online
{
    public class OnlineController
    {
        private ITreeModel<OnlineVehicle> _model;
        private IOnlineDataProvider _provider;
        private Timer _timer;
        private bool _isStarted;
        private int _tail;
        private int _interval;

        public event OnlineDataHandled Handled;

        public OnlineController(ITreeModel<OnlineVehicle> model, int tail, int interval, IOnlineDataProvider provider)
        {
            _model = model;
            _tail = tail;
            _interval = interval;
            _provider = provider;
            _timer = new Timer(analize, null, Timeout.Infinite, Timeout.Infinite);
        }

        public int Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public void Start()
        {
            if (_isStarted)
                return;
            _isStarted = true;
            ThreadPool.QueueUserWorkItem(analize);
        }

        public void Stop()
        {
            //���������� LogId �������� ����� � ������ �� ����������
            foreach (OnlineVehicle ov in _model.Checked)
            {
                ov.LastLogId = 0;
                ov.LastPoint = null;
                ov.OnlineData.Clear();
            }
            _isStarted = false;
        }

        private void analize(object obj)
        {
            try
            {
                if (!_isStarted)
                    return;
                _timer.Change(Timeout.Infinite, Timeout.Infinite);

                IList<OnlineVehicle> vehicles = _model.Checked;
                foreach (OnlineVehicle ov in vehicles)
                {
                    if (!_isStarted)
                        break;

                    IList<GpsData> geoData = _provider.GetLastData(ov);
                    foreach (GpsData p in geoData)
                    {
                        ov.LastPoint = p;
                        ov.OnlineData.Add(p);
                        ov.LastLogId = p.LogId;

                        if (p.Valid)
                        {
                            ov.GpsSignal = GpsSignal.Valid;
                            ov.LastTime = p.Time;
                            ov.Speed = p.Speed;
                            ov.UpdateMotionState();
                        }
                        else
                            ov.GpsSignal = GpsSignal.NotValid;

                        if ((p.Events & 0x00000080) != 0)
                            ov.PowerSource = PowerSource.OnBoard;
                        if ((p.Events & 0x00000100) != 0)
                            ov.PowerSource = PowerSource.Reserve;
                    }

                    if (ov.OnlineData.Count > _tail)
                    {
                        ov.OnlineData.RemoveRange(0, ov.OnlineData.Count - _tail);
                    }
                }

                if (_isStarted)
                    if (Handled == null)
                    {
                        _isStarted = false;

                    }
                    else
                    {
                        Handled();
                        _timer.Change(_interval, Timeout.Infinite);
                    }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Online Controller", MessageBoxButtons.OK);
            }
        }
    }
}
