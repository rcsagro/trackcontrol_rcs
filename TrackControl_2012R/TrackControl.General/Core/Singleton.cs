using System;

namespace TrackControl.General.Core
{
    /// <summary>
    /// ���������� �������� � ���������� ��������������
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : new()
    {
        protected Singleton()
        {
        }

        public static T Instance
        {
            get
            {
                return Creator.instance;
            }
        }

        private class Creator
        {
            // ���������� ������ ������������ ������������ ���������
            // ����������� �� �������� ��� ��� beforefieldinit
            static Creator()
            {
            }

            public static readonly T instance = new T();
        }
    }
}