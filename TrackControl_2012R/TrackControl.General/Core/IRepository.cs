using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.General.Core
{
  /// <summary>
  /// ����� ��� ������� � ���� ������
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IRepository<T> where T : Entity
  {
    /// <summary>
    /// ���������� ��� ������������ �������� ������� ����,
    /// ���������� � ��
    /// </summary>
    IList<T> GetAll();

    /// <summary>
    /// ��������� ������ ������ � ���� ������
    /// </summary>
    /// <param name="entity">������ ������</param>
    /// <returns>True - ���� ���������� ��������� �������</returns>
    bool Save(T entity);

    /// <summary>
    /// ������� ������ ������ �� ���� ������
    /// </summary>
    /// <param name="entity">������ ������</param>
    /// <returns>True - ���� �������� ��������� �������</returns>
    bool Delete(T entity);
  }
}
