using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace TrackControl.General.Core
{
  public class SortableBindingList<T> : BindingList<T>
  {
    protected override bool IsSortedCore
    {
      get { return _isSorted; }
    }
    bool _isSorted;

    protected override ListSortDirection SortDirectionCore
    {
      get { return sortDirection; }
    }
    ListSortDirection sortDirection;

    protected override PropertyDescriptor SortPropertyCore
    {
      get { return sortProperty; }
    }
    PropertyDescriptor sortProperty;

    protected override bool SupportsSortingCore
    {
      get { return true; }
    }

    protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
    {

      sortProperty = property;
      // Get list to sort
      List<T> items = this.Items as List<T>;

      // Apply and set the sort, if items to sort
      if (items != null)
      {
        PropertyComparer<T> pc = new PropertyComparer<T>(property, direction);
        items.Sort(pc);
        _isSorted = true;
      }
      else
      {
        _isSorted = false;
      }

      ResetBindings();
      sortDirection = direction;

      // Let bound controls know they should refresh their views
      this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
    }

    protected override void RemoveSortCore()
    {
      _isSorted = false;
    }
  }

  public class PropertyComparer<T> : IComparer<T>
  {
    private PropertyDescriptor _property;
    private ListSortDirection _direction;

    public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
    {
      _property = property;
      _direction = direction;
    }

    #region IComparer<T>

    public int Compare(T xWord, T yWord)
    {
      // Get property values
      object xValue = GetPropertyValue(xWord, _property.Name);
      object yValue = GetPropertyValue(yWord, _property.Name);

      // Determine sort order
      if (_direction == ListSortDirection.Ascending)
      {
        return CompareAscending(xValue, yValue);
      }
      else
      {
        return CompareDescending(xValue, yValue);
      }
    }

    public bool Equals(T xWord, T yWord)
    {
      return xWord.Equals(yWord);
    }

    public int GetHashCode(T obj)
    {
      return obj.GetHashCode();
    }

    #endregion

    // Compare two property values of any type
    private int CompareAscending(object xValue, object yValue)
    {
      int result;

      if (xValue == null && yValue == null)
      {
        return 0;
      }
      else if (yValue == null)
      {
        return 1;
      }
      else if (xValue == null)
      {
        return -1;
      }
      // If values implement IComparer
      if (xValue is IComparable)
      {
        result = ((IComparable)xValue).CompareTo(yValue);
      }
      // If values don't implement IComparer but are equivalent
      else if (xValue.Equals(yValue))
      {
        result = 0;
      }
      // Values don't implement IComparer and are not equivalent, so compare as string values
      else result = xValue.ToString().CompareTo(yValue.ToString());

      // Return result
      return result;
    }

    private int CompareDescending(object xValue, object yValue)
    {
      // Return result adjusted for ascending or descending sort order ie
      // multiplied by 1 for ascending or -1 for descending
      return CompareAscending(xValue, yValue) * (-1);
    }

    private object GetPropertyValue(T value, string property)
    {
      // Get property
      PropertyInfo propertyInfo = value.GetType().GetProperty(property);

      // Return value
      return propertyInfo.GetValue(value, null);
    }
  }
}
