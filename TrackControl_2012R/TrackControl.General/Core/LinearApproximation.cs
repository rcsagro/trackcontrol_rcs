using System;
using System.Collections.Generic;

namespace TrackControl.General.Core
{
    public class LinearApproximationException : Exception
    {
        public LinearApproximationException( string txt )
        {
            textException = txt;
        }
        private string textException = "";
        public string GetText
        {
            get { return textException; }
        }
    }

    public class LinearApproximation
    {
        SortedDictionary<double, ValuePoint> Points = new SortedDictionary<double, ValuePoint>();
        LinkedList<double> _keys;
        public string NotDataErrorMessage = "The data are absent.";

        public void Clear()
        {
            Points.Clear();
            Recalculation();
        }

        public ValuePoint this[double x]
        {
            get { return Points[x]; }
        }

        public int Count
        {
            get { return Points.Count; }
        }

        public IEnumerator<KeyValuePair<double, ValuePoint>> GetEnumerator()
        {
            return Points.GetEnumerator();
        }

        public void AddPoint( double x, double y )
        {
            if( !Points.ContainsKey( x ) )
            {
                Points.Add( x, new ValuePoint( y ) );
                Recalculation();
            }
        }

        public ICollection<double> Keys
        {
            get { return Points.Keys; }
        }

        //double[] arrayk = new double[210];
        //double[] arrayb = new double[210];
        //double[] arrayx = new double[210];
        //double[] arrayy = new double[210];
        //double[] array0 = new double[210];
        //int i = 0;

        void Recalculation()
        {
            _keys = new LinkedList<double>( Points.Keys );

            if( Points.Count == 0 )
                throw new Exception( NotDataErrorMessage );

            if( Points.Count == 1 )
            {
                BasePoint p1 = new BasePoint( _keys.First.Value, Points[_keys.First.Value] );

                //if( p1.X < 0  || p1.Y < 0 )
                //    throw new LinearApproximationException( "Calibration values must be greater than zero!" );

                if( p1.X == 0 ) 
                    p1.X = 1;

                BasePoint p2 = new BasePoint();
                Points[_keys.First.Value].K = ( p2.Y - p1.Y ) / ( p2.X - p1.X );
                Points[_keys.First.Value].b = p1.Y - Points[_keys.First.Value].K * p1.X;
            }
            else
            {
                LinkedListNode<double> prev = _keys.First;

                while( true )
                {
                    LinkedListNode<double> curr = prev.Next;

                    if( curr != null )
                    {
                        double x1 = prev.Value;
                        double x2 = curr.Value;
                        ValuePoint v1 = Points[x1];
                        ValuePoint v2 = Points[x2];

                        if( x1 == x2 || v1.Y == v2.Y )
                            throw new LinearApproximationException( "Calibration values must be unique" );

                        Points[x1].K = ( v2.Y - v1.Y ) / ( x2 - x1 );
                        Points[x1].b = v1.Y - v1.K * x1;

                        prev = curr;
                    }
                    else
                    {
                        curr = _keys.Last;
                        prev = curr.Previous;

                        double x1 = prev.Value;
                        double x2 = curr.Value;

                        Points[x2].K = Points[x1].K;
                        Points[x2].b = Points[x1].b;

                        //arrayb[i] = Points[x1].b;
                        //arrayk[i] = Points[x1].K;
                        //arrayx[i] = x1;
                        //arrayy[i] = Points[x1].Y;
                        //array0[i++] = arrayy[i] - (arrayb[i] + arrayk[i]*arrayx[i]);

                        prev = curr;

                        break;
                    }
                } // while
            }
        }

        public double Y( double x )
        {
            if( Points.Count == 0 )
                throw new Exception( NotDataErrorMessage );
            if( _keys.Contains( x ) )
                return Points[x].Y;

            BasePoint point;
            if( x < _keys.First.Value )
            {
                point = new BasePoint( _keys.First.Value, Points[_keys.First.Value] );
            }
            else if( x > _keys.Last.Value )
            {
                point = new BasePoint( _keys.Last.Value, Points[_keys.Last.Value] );
            }
            else
            {
                LinkedListNode<double> prev = _keys.First;
                while( true )
                {
                    LinkedListNode<double> curr = prev.Next;
                    if( curr == null || curr.Value > x )
                    {
                        break;
                    }
                    prev = curr;
                }
                point = new BasePoint( prev.Value, Points[prev.Value] );
            }
            return point.K * x + point.b;
        }

        #region inner classes

        public class ValuePoint
        {
            double _y, _k, _b;

            public double Y
            {
                get { return _y; }
                set { _y = value; }
            }

            public double K
            {
                get { return _k; }
                set { _k = value; }
            }

            public double b
            {
                get { return _b; }
                set { _b = value; }
            }

            public ValuePoint( double y, double k, double b )
            {
                Y = y;
                K = k;
                this.b = b;
            }

            public ValuePoint( double y )
                : this( y, 0, 0 ) { }

            public ValuePoint()
                : this( 0, 0, 0 ) { }
        }

        public class BasePoint : ValuePoint
        {
            double _x;

            public double X
            {
                get { return _x; }
                set { _x = value; }
            }

            public BasePoint( double x, double y, double k, double b )
            {
                X = x;
                Y = y;
                K = k;
                this.b = b;
            }

            public BasePoint( double x, double y )
                : this( x, y, 0, 0 ) { }

            public BasePoint()
                : this( 0, 0, 0, 0 ) { }

            public BasePoint( KeyValuePair<double, ValuePoint> pair )
                : this( pair.Key, pair.Value.Y, pair.Value.K, pair.Value.b ) { }

            public BasePoint( double x, ValuePoint v )
                : this( x, v.Y, v.K, v.b ) { }
        }

        #endregion
    }
}
