using System;
using System.IO;
using System.Drawing;

namespace TrackControl.General
{
    public delegate void RefreshingTree();

    public class RefreshTree
    {
        public static RefreshingTree refreshReport;
        public static RefreshingTree refreshOnline;
    }

    /// <summary>
    /// �����, ���������� ���������� ��������� ��� ���������� "TrackControl".
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// ���� � ����� � ������� ������ ����������
        /// </summary>
        public static String APP_DATA = String.Format("{0}{1}{2}{1}{3}",
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            Path.DirectorySeparatorChar, "RCS Ltd", "TrackControl");

        /// <summary>
        /// ����� HASP-��������
        /// </summary>
        public const Int32 LICENSE_NUMBER = 8;

        /// <summary>
        /// ����������� ����������� ��������������� ��� ���� GMap.NET
        /// </summary>
        public const Int32 GMAP_MIN = 5;

        /// <summary>
        /// ������������ ����������� ��������������� ��� ���� GMap.NET
        /// </summary>
        public const Int32 GMAP_MAX = 17; //- � ����� ���� �� � ������

        /// <summary>
        /// ����������� ������� ��� ����� Eprasys UkrGIS
        /// </summary>
        public const Double GIS_MIN = 3063.521;

        /// <summary>
        /// ������������ ������� ��� ����� Eprasys UkrGIS
        /// </summary>
        public const Double GIS_MAX = 12548182.016;

        /// <summary>
        /// ���� ����� �� ��������� Color.FromArgb(140, Color.DarkBlue).ToArgb();
        /// </summary>
        public const int TRACK_COLOR_DEFAULT = -16777077;

        /// <summary>
        /// ����������� �������� ������� � ������ � ������ �� � �������� ��������;
        /// </summary>
        public const int LAT_LNG_GPS_CF = 60000;
    }
}
