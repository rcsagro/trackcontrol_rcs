using System;

namespace TrackControl.General
{
  public interface IEntityFinder<T> where T : IEntity
  {
    T GetById(int id);
  }
}
