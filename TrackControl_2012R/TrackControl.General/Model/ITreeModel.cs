using System;
using System.Collections.Generic;

namespace TrackControl.General
{
  public interface ITreeModel<T> : IEntityFinder<T> where T : IEntity
  {
    T Current { get; }
    IList<T> Checked { get; }
    IList<T> GetAll();
  }
}
