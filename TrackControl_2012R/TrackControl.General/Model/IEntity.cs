using System;

namespace TrackControl.General
{
    public interface IEntity
    {
        int Id { get; set; }
        bool IsNew { get; }
    }

    public interface IEntityPcb
    {
        int ID { get; }
        string Name { get; }
    }
}
