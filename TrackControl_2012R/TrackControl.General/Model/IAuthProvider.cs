using System;

namespace TrackControl.General
{
  public interface IAuthProvider
  {
    bool IsAuthorizationNeeded();

    bool IsValidLogin(string login, string password);
  }
}
