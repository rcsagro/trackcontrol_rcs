using System;
using System.Drawing;
using TrackControl.General.Properties;

namespace TrackControl.General
{
  public class GroupStyle
  {
    Image _icon;

    public GroupStyle(Image icon)
    {
      Icon = icon;
    }

    public Image Icon
    {
      get { return _icon; }
      set
      {
        if (null == value)
          throw new Exception("Style icon for group must be not-NULL");

        _icon = value;
      }
    }

    public static GroupStyle Empty
    {
      get
      {
        return new GroupStyle(Resources.Folder);
      }
    }

    public static GroupStyle RootStyle
    {
      get { return new GroupStyle(Resources.FoldersStack); }
    }
  }
}
