using System;
using System.Windows.Forms;

namespace TrackControl.General
{
  public interface IMainView
  {
    void SetView(Control view);
  }
}
