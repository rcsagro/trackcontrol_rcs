﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TrackControl.General
{
    public interface IReportItem
    {

        event Action<string, int> ChangeStatus;

        /// <summary>
        /// Элемент для просмотра данных </summary>
        UserControl DataViewer { get; }

        /// <summary>
        /// Название отчета
        /// </summary>
        string Caption { get; }

        /// <summary>
        /// родительский объект для DataViewer
        /// </summary>
        Control ParentDataViewer { set; }

        /// <summary>
        /// иконки отчета 
        /// </summary>
        Image LargeImage { get; }

        Image SmallImage { get; }

        /// <summary>
        /// запуск на выполнение
        /// </summary>
        /// <param name="begin"> дата начала промежутка</param>
        /// <param name="end">дата окончания промежутка</param>
        void Run(DateTime begin, DateTime end);

        /// <summary>
        /// после выполнения отчет имеет данные
        /// </summary>
        bool IsHasData { get; }

        /// <summary>
        /// вывод в Excel
        /// </summary>
        void ExportToExcel();
    }
}
