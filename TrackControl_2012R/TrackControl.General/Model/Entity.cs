using System;

namespace TrackControl.General
{
    /// <summary>
    /// ������� ����� ��� ��������� ������ ���������� �������
    /// </summary>
    public abstract class Entity : IEntity
    {
        protected Entity()
        {
            _id = -1;
        }

        public Int32 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Int32 MobitelId
        {
            get { return _idMobi; }
            set { _idMobi = value; }
        }

        private Int32 _id;
        private Int32 _idMobi;

        public Boolean IsNew
        {
            get { return -1 == _id; }
        }

        protected string _IdOutLink;
        protected string _IdOutLinkFuel;
        protected string _TypeFuel;

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _IdOutLink; }
            set { _IdOutLink = value; }
        }

        /// <summary>
        /// ��� ������� ���� ���� �������
        /// </summary>
        public string IdOutLinkFuel
        {
            get { return _IdOutLinkFuel; }
            set { _IdOutLinkFuel = value; }
        }

        /// <summary>
        /// ��� �������
        /// </summary>
        public string TypeFuel
        {
            get { return _TypeFuel; }
            set { _TypeFuel = value; }
        }
    }

    public abstract class Entity64
    {
        protected Entity64()
        {
            _id = -1;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private long _id;

        public Boolean IsNew
        {
            get { return -1 == _id; }
        }

        protected string _IdOutLink;

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _IdOutLink; }
            set { _IdOutLink = value; }
        }
    }
}
