using System;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.General.DAL;

namespace TrackControl.General
{
    /// <summary>
    /// ������� ����� ���������� ���� � �������� ����� � ���������
    /// </summary>
    /// <typeparam name="T">������� ��������� ������ �����</typeparam>
    public abstract class Group<T> : Entity
    {
        /// <summary>
        /// �������� ������
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _name;

        /// <summary>
        /// �������� ������ �� ��������� �������� ���������
        /// </summary>
        public string NameWithCounter
        {
            get
            {
                return string.Format("{0}: {1}", _name, AllItems.Count);
            }
        }


        /// <summary>
        /// �������� ������
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _description;

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private object _tag;

        /// <summary>
        /// ������, � ������� ����������� ������ ������
        /// </summary>
        public Group<T> Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private Group<T> _parent;

        /// <summary>
        /// ������, ������� ����������� ������ ������
        /// </summary>
        public IList<Group<T>> OwnGroups
        {
            get { return _ownGroups.AsReadOnly(); }
        }

        private List<Group<T>> _ownGroups = new List<Group<T>>();

        /// <summary>
        /// ������ � ���������, ������������� ������ ������
        /// </summary>
        public IList<Group<T>> AllGroups
        {
            get
            {
                List<Group<T>> groups = new List<Group<T>>();
                _ownGroups.ForEach(delegate(Group<T> group)
                {
                    groups.Add(group);
                    groups.AddRange(group.AllGroups);
                });
                return groups.AsReadOnly();
            }
        }

        /// <summary>
        /// ������ ,������������� ������ ������,������� ������  
        /// </summary>
        public IList<Group<T>> AllGroupsWithItems
        {
            get
            {
                List<Group<T>> groups = new List<Group<T>>();
                _ownGroups.ForEach(delegate(Group<T> group)
                {
                    if (group.AllItems.Count > 0)
                    {
                        groups.Add(group);
                    }
                });

                return groups.AsReadOnly();
            }
        }

        /// <summary>
        /// ��������, ������� ����������� ������ ������ ������
        /// </summary>
        public IList<T> OwnItems
        {
            get { return _ownItems.AsReadOnly(); }
        }

        private List<T> _ownItems = new List<T>();

        /// ��������, ����������� � ������ ������, ������� ���������
        /// </summary>
        public IList<T> AllItems
        {
            get
            {
                List<T> items = new List<T>();
                _ownGroups.ForEach(delegate(Group<T> group)
                {
                    items.AddRange(group.AllItems);
                });
                items.AddRange(_ownItems);

                return items.AsReadOnly();
            }
        }

        /// <summary>
        /// ��������� ������ � ��������� ��������
        /// </summary>
        /// <param name="group">������ ��� ����������</param>
        public void AddGroup(Group<T> group)
        {
            if (!_ownGroups.Contains(group))
                _ownGroups.Add(group);
        }

        /// <summary>
        /// ������� ������ �� ��������� ��������
        /// </summary>
        /// <param name="group">������ ��� ��������</param>
        public void RemoveGroup(Group<T> group)
        {
            if (_ownGroups.Contains(group))
                _ownGroups.Remove(group);
        }

        /// <summary>
        /// ��������� ������� � ���� ���������
        /// </summary>
        /// <param name="item">������� ��� ����������</param>
        protected void AddItem(T item)
        {
            if (!_ownItems.Contains(item))
                _ownItems.Add(item);
        }

        public List<T> GetItem()
        {
            return _ownItems;
        }

        /// <summary>
        /// ������� ������� �� ����� ��������� ���������
        /// </summary>
        /// <param name="item">������� ��� ��������</param>
        protected void RemoveItem(T item)
        {
            if (_ownItems.Contains(item))
                _ownItems.Remove(item);
        }
    }
}
