using System;

namespace TrackControl.General
{
    public enum TuningStatus
    {
        Ok = 0,
        NotSaved = 1,
        Troubles = 2,
        NewAdded = 3
    }
}
