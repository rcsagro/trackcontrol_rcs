using System;
using System.Drawing;
using System.Windows.Forms;

namespace TrackControl.General
{
  public interface ITuning
  {
    string Caption { get; }
    Image Icon { get; }
    Control View { get; }
    void Advise();
    void Release();
  }
}
