using System;
using System.Drawing;

namespace TrackControl.General
{
  /// <summary>
  /// ������������� ����� ������������ ���� ��� ����� ������.
  /// </summary>
  public class PopupMenuItem<T>
  {
    /// <summary>
    /// ��������� ��� ������ ����
    /// </summary>
    public String Caption
    {
      get
      {
        return _caption;
      }
    }
    String _caption;

    /// <summary>
    /// ������ ��� ������ ����
    /// </summary>
    public Image Image
    {
      get
      {
        return _image;
      }
    }
    Image _image;

    /// <summary>
    /// ���������� ���� �� ������ ����
    /// </summary>
    public Action<T> Action
    {
      get
      {
        return _action;
      }
    }
    Action<T> _action;

    /// <summary>
    /// ������� ����� �� ��������� ����� ������ ������� ���� �����������
    /// </summary>
    public Boolean BeginGroup
    {
      get
      {
        return _beginGroup;
      }
    }
    Boolean _beginGroup;

    /// <summary>
    /// ������� � �������������� ����� ��������� ������ TrackControl.General.Main.PopupMenuItem
    /// </summary>
    /// <param name="caption">��������� ������ ����</param>
    /// <param name="image">������ ��� ������ ����</param>
    /// <param name="action">���������� ����� �� ������ ����</param>
    /// <param name="beginGroup">������� ����� �� �������� ����������� ����� ������ �������</param>
    public PopupMenuItem(String caption, Image image, Action<T> action, Boolean beginGroup)
    {
      _caption = caption;
      _image = image;
      _action = action;
      _beginGroup = beginGroup;
    }
  }
}
