using System;
using System.Collections.Generic;

namespace TrackControl.General
{
  public delegate void VoidHandler();
  public delegate void ProgressChanged(string before, string after, int value);
  public delegate void ActionForPeriod(DateTime start, DateTime end);
}
