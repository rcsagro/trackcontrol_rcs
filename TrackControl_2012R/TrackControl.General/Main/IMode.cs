using System;
using System.Windows.Forms;

namespace TrackControl.General
{
  /// <summary>
  /// ��������� ��� ��������� ������ ����������
  /// </summary>
  public interface IMode
  {
    /// <summary>
    /// ����������� ���������� �� ����� ����� ������
    /// </summary>
    /// <param name="form">������� ����� ����������</param>
    void Advise(IMainView main);

    /// <summary>
    /// ����������� ������� ����� ��������� ������
    /// � ������ ������
    /// </summary>
    void Unadvise();

    /// <summary>
    /// ��������������� ������ ����� ������
    /// </summary>
    void RestoreLayout();

    /// <summary>
    /// ��������� ������ ����� ������
    /// </summary>
    void SaveLayout();
  }
}
