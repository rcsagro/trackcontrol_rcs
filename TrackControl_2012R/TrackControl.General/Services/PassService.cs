using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace TrackControl.General.Services
{
  public static class PassService
  {
    /// <summary>
    /// ���� � ��������� ������
    /// </summary>
    static string _path = String.Format(@"{0}\RCS.TrckCntrl.Sttngs.dll", Application.StartupPath);

    /// <summary>
    /// ��������� ������������ ������
    /// </summary>
    /// <param name="pass">������ ������</param>
    /// <returns>True - ���� ������ ������</returns>
    public static bool CheckPassword(string pass)
    {
      byte[] passHash = fetchPassword();
      byte[] hash = getHash(pass);

      if (passHash.Length != hash.Length)
        return false;

      for (int i = 0; i < passHash.Length; i++)
      {
        if (passHash[i] != hash[i])
          return false;
      }

      return true;
    }

    /// <summary>
    /// ������ ������
    /// </summary>
    /// <param name="oldPass">������ ������</param>
    /// <param name="newPass">����� ������</param>
    /// <returns></returns>
    public static bool ChangePassword(string oldPass, string newPass)
    {
      if (!CheckPassword(oldPass))
        return false;

      savePassword(getHash(newPass));
      return true;
    }

    #region --    �������   --
    /// <summary>
    /// ��������� ������ �� ���������
    /// </summary>
    static byte[] fetchPassword()
    {
      byte[] result = new byte[32];
      if (File.Exists(_path))
      {
        using (Stream stream = File.Open(_path, FileMode.OpenOrCreate))
        {
          if (stream.Length > 0)
          {
            using (BinaryReader reader = new BinaryReader(stream))
            {
              result = reader.ReadBytes(32);
            }
          }
        }
      }

      return result;
    }

    /// <summary>
    /// ��������� ������ � ���������
    /// </summary>
    /// <param name="passHash">��� ������</param>
    static void savePassword(byte[] passHash)
    {
      using (Stream stream = File.Create(_path))
      {
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
          writer.Write(passHash);
        }
      }
    }

    /// <summary>
    /// ���������� ������������ ������ ������
    /// </summary>
    /// <param name="pass"></param>
    /// <returns></returns>
    public static byte[] getHash(string pass)
    {
      byte[] result;
      using (SHA256 shaM = new SHA256Managed())
      {
        byte[] bytes = Encoding.UTF8.GetBytes(pass);
        result = shaM.ComputeHash(bytes);
      }

      return result;
    }
    #endregion
  }
}
