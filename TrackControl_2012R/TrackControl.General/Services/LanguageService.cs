using System;
using System.IO;
using System.Text;
using System.Globalization;
using System.Threading;

namespace TrackControl.General.Services
{
    public static class LanguageService
  {
    static string pathLan = Path.Combine(Globals.APP_DATA, "lang.dat");

    public static void Save(string calture)
    {
      if (!Directory.Exists(Globals.APP_DATA))
        Directory.CreateDirectory(Globals.APP_DATA);

      using (StreamWriter writer = new StreamWriter(pathLan, false, Encoding.UTF8))
      {
          writer.Write(calture);
      }
    }

    public static string Get()
    {
        if (File.Exists(pathLan))
      {
          using (StreamReader reader = new StreamReader(pathLan, Encoding.UTF8))
        {
            string calture = reader.ReadLine();
            if (calture.Length > 0)
                return calture;
        }
      }

        return CultureInfo.CurrentCulture.Name;// Thread.CurrentThread.CurrentUICulture.Name;
    }
  }
}
