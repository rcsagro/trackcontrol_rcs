using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General.Properties;

namespace TrackControl.General.Services
{
  public static class MobitelEventsChecker
  {
    static Dictionary<ulong, string> _events;

    static MobitelEventsChecker()
    {
      _events = new Dictionary<ulong, string>();
      _events.Add(0x00000001, Resources.LowSpeedLimit);
      _events.Add(0x00000002, Resources.Timer1);
      _events.Add(0x00000004, Resources.Timer2);
      _events.Add(0x00000008, Resources.CourseShift);
      _events.Add(0x00000010, Resources.Distance1);
      _events.Add(0x00000020, Resources.Distance2);
      _events.Add(0x00000080, Resources.BoardPowerOn);
      _events.Add(0x00000100, Resources.SwitchToBattery);
      _events.Add(0x00000200, Resources.GsmRegistration);
      _events.Add(0x00000400, Resources.GsmSignalLost);
      _events.Add(0x00000800, Resources.LogFileFilledOverLimit);
      _events.Add(0x00040000, Resources.SensorsBoardEvent);
      _events.Add(0x00080000, Resources.AppStarted);
      _events.Add(0x00100000, Resources.AppRestart);
      _events.Add(0x00400000, Resources.CheckZoneEvent);
      _events.Add(0x00800000, Resources.HighAccelerationLimit);
    }

    public static string CheckEvents(ulong data)
    {
      StringBuilder info = new StringBuilder();
      foreach (ulong key in _events.Keys)
      {
        if ((data & key) != 0)
          info.AppendFormat("{0}, ", _events[key]);
      }

      if (info.Length > 2)
        info.Length -= 2;

      return info.ToString();
    }
  }
}
