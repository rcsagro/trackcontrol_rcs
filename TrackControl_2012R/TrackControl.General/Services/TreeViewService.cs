using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using TrackControl.General.Properties; 

using System;
using System.Windows.Forms;

namespace TrackControl.General
{
    public static class TreeViewService
    {
        public static void SetCheckedChildNodes(TreeListNode node)
        {
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                if (node.Nodes[i].Visible)
                {
                    node.Nodes[i].CheckState = node.CheckState;
                    SetCheckedChildNodes(node.Nodes[i]);
                }
            }
        }

        public static void SetCheckedParentNodes(TreeListNode node)
        {
            if (node.ParentNode != null && node.Visible)
            {
                bool b = false;
                CheckState check = node.CheckState;
                CheckState state;

                for (int i = 0; i < node.ParentNode.Nodes.Count; i++)
                {
                    state = (CheckState) node.ParentNode.Nodes[i].CheckState;

                    if (!check.Equals(state))
                    {
                        b = !b;
                        break;
                    }
                }

                node.ParentNode.CheckState = b ? CheckState.Indeterminate : check;
                SetCheckedParentNodes(node.ParentNode);
            }
        }

        public static void FixNodeState(CheckNodeEventArgs e)
        {
            e.State = (e.PrevState == CheckState.Checked) ? CheckState.Unchecked : CheckState.Checked;
        }

        public static int GetVisibleCount(TreeListNode node)
        {
            int result = 0;
            foreach (TreeListNode child in node.Nodes)
            {
                if (child.Visible)
                    result++;
            }
            return result;
        }

        public static void ShowAll(TreeListNode node)
        {
            foreach (TreeListNode child in node.Nodes)
            {
                child.Visible = true;
                ShowAll(child);
            }
        }

        public static void TreeListInitialView(TreeList tree)
        {
            tree.ExpandAll();
            tree.CollapseAll();

            if (tree.Nodes.Count > 0) 
                tree.Nodes.FirstNode.Expanded = true;
        }

        public static string GetToolTip()
        {
            return String.Format(Resources.ColorsForTreeView);
        }
    }
}
