using System;
using System.IO;
using System.Globalization;
using System.Threading;

namespace TrackControl.General
{
  public sealed class LayoutService
  {
    /// <summary>
    /// ���������� ���� � ��������� ������� ��� ���������� ������
    /// </summary>
    /// <param name="key">���� ������ ������ ����������</param>
    public static string GetPath(string key)
    {
      if (String.IsNullOrEmpty(key))
        throw new Exception("key can not be NULL or Empty");

      string dir = Path.Combine(Globals.APP_DATA, "Modes");
      dir = Path.Combine(dir, GetLocalization());
      if (!Directory.Exists(dir))
        Directory.CreateDirectory(dir);

      string file = String.Format("{0}.layout", key);

      return Path.Combine(dir, file);
    }

    public static string GetLocalization()
    {
      CultureInfo info = Thread.CurrentThread.CurrentUICulture;
      string local = info.Name;
      if (local.Length < 2)
        return "en";
      else
        return local.Substring(0, 2);
    }
  }
}
