using System;
using System.Net;
using System.IO;
using System.Xml;
using TrackControl.General;

namespace TrackControl.General.Services
{
  /// <summary>
  /// ����� ��� ��������� HTTP ������.
  /// </summary>
  public static class ProxyService
  {
    /// <summary>
    /// ��� ����� ���������.
    /// </summary>
    private const string REPOSITORY_NAME = "Proxy.xml";
    /// <summary>
    /// �������, ����������� ��� ��������� ������.
    /// </summary>
    public static event Action<WebProxy> ProxyChanged = delegate { };

    /// <summary>
    /// ��������� ��������� � ���������.
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="proxy"></param>
    public static void SaveSettings(ProxySettings settings)
    {
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?><Proxy><Mode></Mode><Host></Host><Port></Port><Login></Login><Password></Password></Proxy>");
      XmlNode mode = doc.DocumentElement.SelectSingleNode("Mode");
      mode.InnerText = settings.Mode.ToString();
      if (settings.Mode == ProxyMode.Custom)
      {
        XmlNode ip = doc.DocumentElement.SelectSingleNode("Host");
        ip.InnerText = settings.Proxy.Address.DnsSafeHost;

        XmlNode port = doc.DocumentElement.SelectSingleNode("Port");
        port.InnerText = settings.Proxy.Address.Port.ToString();

        XmlNode login = doc.DocumentElement.SelectSingleNode("Login");
        login.InnerText = settings.Login;

        XmlNode pass = doc.DocumentElement.SelectSingleNode("Password");
        pass.InnerText = settings.Password;
      }

      if (!Directory.Exists(Globals.APP_DATA))
        Directory.CreateDirectory(Globals.APP_DATA);

      doc.Save(Path.Combine(Globals.APP_DATA, REPOSITORY_NAME));
      
      ProxyChanged(settings.Proxy);
    }
    /// <summary>
    /// ��������� ��������� �� ���������.
    /// </summary>
    /// <returns></returns>
    public static ProxySettings FetchSettings()
    {
      string path = Path.Combine(Globals.APP_DATA, REPOSITORY_NAME);
      if (File.Exists(path))
      {
        XmlDocument doc = new XmlDocument();
        try
        {
          doc.Load(path);
        }
        catch (Exception)
        {
          return new ProxySettings(ProxyMode.Nope, null);
        }
        ProxyMode proxyMode = ProxyMode.Nope;
        XmlNode modeNode = doc.DocumentElement.SelectSingleNode("Mode");
        if (modeNode != null && Enum.IsDefined(typeof(ProxyMode), modeNode.InnerText))
        {
          proxyMode = (ProxyMode)Enum.Parse(typeof(ProxyMode), modeNode.InnerText, true);
        }
        switch (proxyMode)
        {
          case ProxyMode.Custom:
            {
              XmlNode hostNode = doc.DocumentElement.SelectSingleNode("Host");
              if (hostNode == null || hostNode.InnerText.Trim().Length == 0)
              {
                return new ProxySettings(ProxyMode.Nope, null);
              }
              XmlNode portNode = doc.DocumentElement.SelectSingleNode("Port");
              int port = -1;
              if (portNode == null || !Int32.TryParse(portNode.InnerText, out port))
              {
                return new ProxySettings(ProxyMode.Nope, null);
              }
              if (port < 0 || port > 65535)
              {
                return new ProxySettings(ProxyMode.Nope, null);
              }
              XmlNode loginNode = doc.DocumentElement.SelectSingleNode("Login");
              XmlNode passNode = doc.DocumentElement.SelectSingleNode("Password");
              if (IsLoginPasswordExist(loginNode, passNode))
              {
                  return new ProxySettings(proxyMode, new WebProxy(hostNode.InnerText, port), loginNode.InnerText, passNode.InnerText);
              }
              else
              return new ProxySettings(proxyMode, new WebProxy(hostNode.InnerText, port));
            }
          case ProxyMode.Default:
            {
              return new ProxySettings(proxyMode, (WebProxy)GlobalProxySelection.Select);
            }
          case ProxyMode.Nope:
          default:
            {
              return new ProxySettings(proxyMode, null);
            }
        }
      }
      else
      {
        return new ProxySettings(ProxyMode.Nope, (WebProxy)GlobalProxySelection.Select);
      }
    }

    private static bool IsLoginPasswordExist(XmlNode loginNode, XmlNode passNode)
    {
        return !(loginNode == null || loginNode.InnerText.Trim().Length == 0) &&
                          !(passNode == null || passNode.InnerText.Trim().Length == 0);
    }
  }



  /// <summary>
  /// ����� ������������� ������.
  /// </summary>
  public enum ProxyMode
  {
    Nope = 1, // �� ������������ ������
    Default = 2, // �������������� �����������
    Custom = 4 // ������ ���������
  }
  /// <summary>
  /// ����� � ����������� ������.
  /// </summary>
  public class ProxySettings
  {
    private ProxyMode _mode;
    public ProxyMode Mode
    {
      get { return _mode; }
      set { _mode = value; }
    }

    private WebProxy _proxy;
    public WebProxy Proxy
    {
      get { return _proxy; }
      set { _proxy = value; }
    }

    public ProxySettings(ProxyMode mode, WebProxy proxy)
    {
      _mode = mode;
      _proxy = proxy;
    }

    public ProxySettings(ProxyMode mode, WebProxy proxy, string login, string password)
    {
        _mode = mode;
        _proxy = proxy;
        _login = login;
        _password = password;
    }

    string _login;
    public string Login
    {
        get { return _login; }
        set { _login = value; }
    }

    string _password;
    public string Password
    {
        get { return _password; }
        set { _password = value; }
    }

    public Boolean IsUserAuthenticationNeeded
    {
        get
        {
            return !(_login == null || _login.Trim().Length == 0) &&
                !(_password == null || _password.Trim().Length == 0); ;
        }
    }

  }
}
