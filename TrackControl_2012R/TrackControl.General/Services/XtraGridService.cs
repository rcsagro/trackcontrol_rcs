﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraEditors;
using TrackControl.General.Properties;

namespace TrackControl.General
{
    public static class XtraGridService
    {
        public delegate bool DeleteRecord(int idForDelete);

        public static bool DeleteSelectedFromGrid(GridView gv, DeleteRecord DeleteFunction,int Id_column)
        {
            if (gv == null || gv.SelectedRowsCount == 0) return false;
            if (DialogResult.No == XtraMessageBox.Show(Resources.DeleteQuestion, "TrackControl", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
            DataRow[] rows = new DataRow[gv.SelectedRowsCount];
            for (int i = 0; i < gv.SelectedRowsCount; i++)
                rows[i] = gv.GetDataRow(gv.GetSelectedRows()[i]);
            Array.ForEach(rows,
            delegate(DataRow row)
            {
                int id = (int)row[Id_column];
                if (DeleteFunction(id))
                    row.Delete();
            });
            return true;
        }

        public static void SetupGidViewForPrint(GridView gv, bool ExpandAllGroups, bool ExpandAllDetails)
        {
            gv.OptionsPrint.ExpandAllDetails = ExpandAllDetails;
            gv.OptionsPrint.PrintDetails = true;
            gv.OptionsPrint.AutoWidth = false;
            gv.OptionsPrint.ExpandAllGroups = ExpandAllGroups;
            gv.BestFitColumns();
            gv.OptionsPrint.AutoWidth = true;
        }

        public static void DevExpressReportHeader(string header, CreateAreaEventArgs e)
        {
            DevExpress.XtraPrinting.TextBrick brick = e.Graph.DrawString(header, Color.Black, new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 20),
            DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Arial", 14, FontStyle.Bold);
            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);
            brick.BackColor = Color.White;
        }

        public static void DevExpressReportSubHeader(string subHeader, int height, CreateAreaEventArgs e)
        {

            DevExpress.XtraPrinting.TextBrick brick = e.Graph.DrawString(subHeader, Color.Black, new RectangleF(0, 30, e.Graph.ClientPageSize.Width, height),
            DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Arial", 12);
            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);
            brick.BackColor = Color.White;
        }

        public static bool LayOutSave(GridControl gc, GridView gv, string directory, string XMLfile)
        {
            try
            {
                gc.ForceInitialize();
                string sPath = directory;
                if (!Directory.Exists(sPath))
                    Directory.CreateDirectory(sPath);
                sPath = Path.Combine(sPath, XMLfile);
                FileStream fs = new FileStream(sPath, FileMode.Create);
                gv.SaveLayoutToStream(fs);
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool LayOutRead(GridView gv, string directory, string XMLfile)
        {
            try
            {
                string sPath = Path.Combine(directory, XMLfile);
                if (File.Exists(sPath))
                    gv.RestoreLayoutFromXml(sPath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void ExportToExcel(GridView gv,string fileName)
        {
            string xslFilePath = StaticMethods.ShowSaveFileDialog(string.Format("{0} {1}",fileName ,StaticMethods.SuffixDateToFileName()), "Excel", "Excel|*.xls"); ;
            if (xslFilePath.Length > 0)
            {
                gv.OptionsPrint.ExpandAllDetails = true;
                gv.OptionsPrint.PrintDetails = true;
                gv.OptionsPrint.AutoWidth = true;
                gv.ExportToXls(xslFilePath);
                System.Diagnostics.Process.Start(xslFilePath);
            }
        }
    }
}
