using DevExpress.XtraEditors;

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using TrackControl.General.Properties;
using System.Text;

namespace TrackControl.General
{
    /// <summary>
    /// ������ ������ � �������������.
    /// </summary>
    public sealed class ImageService
    {
        /// <summary>
        /// ������������ �������������� ����������� � ������ ����.
        /// </summary>
        public static byte[] ToBytes(Image image)
        {
            if (null == image)
                throw new ArgumentNullException("image");

            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Png);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// ������������ �������������� ������� ���� � �����������.
        /// </summary>
        public static Image ToImage(byte[] bytes)
        {
            if (null == bytes)
                throw new ArgumentNullException("bytes");

            MemoryStream stream = new MemoryStream(bytes);
            try
            {
                return Image.FromStream(stream);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                stream.Dispose();
            }
        }

        /// <summary>
        /// ������������ ���������������� ���������� �������� �������� �����������, �����
        /// ������� � �������� ������������� �������.
        /// </summary>
        /// <param name="image">�����������, ������� ����� ���������</param>
        /// <param name="widthLimit">���������� ������ �����������</param>
        /// <param name="heightLimit">���������� ������ �����������</param>
        public static Image Inscribe(Image image, int widthLimit, int heightLimit)
        {
            if (null == image)
                throw new ArgumentNullException("photo");
            if (widthLimit <= 0)
                throw new ArgumentException("widthLimit must be greater than nil");
            if (heightLimit <= 0)
                throw new ArgumentException("heightLimit must be greater than nil");
            if (image.Width <= widthLimit && image.Height <= heightLimit)
                return image;

            if (image.Width > widthLimit && image.Height <= heightLimit)
            {
                int height = Convert.ToInt32(image.Height*(widthLimit*1F/image.Width));
                return new Bitmap(image, widthLimit, height);
            }

            if (image.Height > heightLimit && image.Width <= widthLimit)
            {
                int width = Convert.ToInt32(image.Width*(heightLimit*1F/image.Height));
                return new Bitmap(image, width, heightLimit);
            }

            if (image.Width == image.Height)
            {
                int side = Math.Min(widthLimit, heightLimit);
                return new Bitmap(image, side, side);
            }

            double originalFactor = image.Width*1F/image.Height;
            double limitsFactor = widthLimit*1F/heightLimit;
            if (originalFactor > 1 && limitsFactor < 1)
            {
                int height = Convert.ToInt32(widthLimit/originalFactor);
                return new Bitmap(image, widthLimit, height);
            }
            if (originalFactor < 1 && limitsFactor > 1)
            {
                int width = Convert.ToInt32(heightLimit*originalFactor);
                return new Bitmap(image, width, heightLimit);
            }
            if (originalFactor > 1 && limitsFactor > 1)
            {
                if (originalFactor > limitsFactor)
                {
                    int height = Convert.ToInt32(widthLimit/originalFactor);
                    return new Bitmap(image, widthLimit, height);
                }
                if (originalFactor < limitsFactor)
                {
                    int width = Convert.ToInt32(heightLimit*originalFactor);
                    return new Bitmap(image, width, heightLimit);
                }
            }
            if (originalFactor < 1 && limitsFactor < 1)
            {
                if (originalFactor < limitsFactor)
                {
                    int width = Convert.ToInt32(heightLimit*originalFactor);
                    return new Bitmap(image, width, heightLimit);
                }
                if (originalFactor > limitsFactor)
                {
                    int height = Convert.ToInt32(widthLimit/originalFactor);
                    return new Bitmap(image, widthLimit, height);
                }
            }

            return new Bitmap(image, 1, 1);
        }

        /// <summary>
        /// ��������� ����������� �� ��������� ����������
        /// </summary>
        /// <param name="image">�����������, ������� ����� ���������</param>
        public static void SaveImage(Image image, string FileName)
        {
            if (null == image)
                throw new ArgumentNullException("image");

            try
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.Filter = "PNG (*.png)|*.png";

                    sfd.FileName = FileName;

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        image.Save(sfd.FileName);

                        string title = Resources.Notification;
                        StringBuilder text = new StringBuilder();
                        text.AppendLine(Resources.ScreenShotSaved);
                        text.AppendLine(sfd.FileName);
                        text.AppendLine();
                        text.AppendLine(Resources.JumpToFolder);

                        DialogResult dr = XtraMessageBox.Show(text.ToString(), title, MessageBoxButtons.OKCancel,
                            MessageBoxIcon.Asterisk);
                        if (dr == DialogResult.OK)
                        {
                            using (Process proc = new Process())
                            {
                                proc.StartInfo.FileName = String.Format("{0}{1}explorer.exe",
                                    Environment.GetEnvironmentVariable("WINDIR"), Path.DirectorySeparatorChar);
                                proc.StartInfo.Arguments = Path.GetDirectoryName(sfd.FileName);
                                proc.Start();
                            }
                        }

                    }
                }
            }
            catch
            {
                //TODO: ��������� ����������. ��� �������, ������ � ���.
#if DEBUG
                throw;
#endif
            }
        }
    }
}
