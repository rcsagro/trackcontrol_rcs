using System;
using System.IO;
using System.Text;

namespace TrackControl.General.Services
{
  public static class SkinService
  {
    static string path = Path.Combine(Globals.APP_DATA, "skin.dat");

    public static void Save(string name)
    {
      if (!Directory.Exists(Globals.APP_DATA))
        Directory.CreateDirectory(Globals.APP_DATA);

      using (StreamWriter writer = new StreamWriter(path, false, Encoding.UTF8))
      {
        writer.Write(name);
      }
    }

    public static string Get()
    {
      if (File.Exists(path))
      {
        using (StreamReader reader = new StreamReader(path, Encoding.UTF8))
        {
          string name = reader.ReadLine();
          if (name.Length > 0)
            return name;
        }
      }

      return "Blue";
    }
  }
}
