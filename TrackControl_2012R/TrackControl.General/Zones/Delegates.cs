using System;
using System.Collections.Generic;

namespace TrackControl.General
{
  public delegate void GeometryChangedHandler(int zoneId, IList<PointLatLng> points);
}
