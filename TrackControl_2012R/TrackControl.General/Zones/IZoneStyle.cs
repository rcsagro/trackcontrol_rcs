using System;
using System.Drawing;

namespace TrackControl.General
{
  /// <summary>
  /// Style for Check Zone
  /// </summary>
  public interface IZoneStyle
  {
    /// <summary>
    /// Icon for Check Zone Node in TreeView
    /// </summary>
    Image Icon { get; set; }

    /// <summary>
    /// Color for zone rendering
    /// </summary>
    Color Color { get; set; }
  }
}
