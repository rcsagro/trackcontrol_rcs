using System;
using System.Collections.Generic;

namespace TrackControl.General
{
    public interface IZonesEditorMap
    {
        List<PointLatLng> PointsFromTrack { get; set; }
        void PanTo(PointLatLng point);
        void PanTo(RectLatLng rect);
        void CreateNewZone(IZone zone);
        void ReleaseEditor();
        void ClearZones();
        void AddZone(IZone zone);
        void AddZones(IEnumerable<IZone> zones);
        void SelectZone(IZone zone);
        bool IsZoneEditor { get; set; }
        bool EditorZonesOn { get; set; }
        MapState State { get; set; }
        event Action<IZone> ZoneSelected;
    }
}
