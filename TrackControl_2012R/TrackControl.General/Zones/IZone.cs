using System;
using System.Drawing;

namespace TrackControl.General
{
    public interface IZone : IEntity
    {
        string Name { get; set; }
        string Description { get; set; }
        IZoneStyle Style { get; set; }
        PointLatLng[] Points { get; set; }
        RectLatLng Bounds { get; }
        double AreaKm { get; set; }
        double AreaGa { get; set; }
        bool IsGeometryChanged { get; set; }
        ZonesGroup Group { get; set; }
        object Tag { get; set; }

        object Category { get; set; }
        object Category2 { get; set; }
        
        /// <summary>
        /// ���� ��������� ������� ����
        /// </summary>
        DateTime? DateChange { get; set; }

        bool Contains(PointLatLng point);
        bool BoundContains(PointLatLng point);
        string IdOutLink { get; set; }
        bool PassengerCalc { get; set; }
        bool PassagerCalc { get; set; }
        string GetCultureActive();
    }
}
