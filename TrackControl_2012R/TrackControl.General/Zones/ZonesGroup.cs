using System;

namespace TrackControl.General
{
  using Properties;

  /// <summary>
  /// �����, ��������������� ������ ����������� ���
  /// </summary>
  public class ZonesGroup : Group<IZone>
  {
    readonly static GroupStyle DEFAULT_STYLE = new GroupStyle(Resources.Folder);

    bool _isRoot;
    GroupStyle _style;

    public ZonesGroup(string name, string description, GroupStyle style)
    {
      if (null == name)
        throw new ArgumentNullException("name");
      Name = name;

      if (null == description)
        throw new ArgumentNullException("description");
      Description = description;

      if (null == style)
        throw new ArgumentNullException("style");
      Style = style;
    }

    public ZonesGroup(string name, string description)
      : this(name, description, DEFAULT_STYLE)
    {
    }

    public bool IsRoot
    {
      get { return _isRoot; }
    }

    public GroupStyle Style
    {
      get { return _style; }
      set
      {
        if (null == value)
          throw new Exception("Render Style must be not-NULL");

        _style = value;
      }
    }

    public static ZonesGroup CreateRoot()
    {
      ZonesGroup root = new ZonesGroup(Resources.AllZones, "", new GroupStyle(Resources.FoldersStack));
      root._isRoot = true;
      return root;
    }

    public void AddZone(IZone zone)
    {
      if (null != zone.Group)
        zone.Group.RemoveItem(zone);

      AddItem(zone);
      zone.Group = this;
    }

    public void RemoveZone(IZone zone)
    {
      RemoveItem(zone);
    }

    public override string ToString()
    {
      return Name;
    }
  }
}