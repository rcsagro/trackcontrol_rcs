﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Properties;

namespace TrackControl.Reports
{
    public partial class FormInterval : Form
    {
        private CheckEdit[] checkEdits;
        static private string week = "";
        static private string month = "";
        static private bool flagEditTime = false;
        static private bool flagEditWeek = false;
        static private bool flagEditMonth = false;
        static private string periodTimeBegin = "00:00:00";
        static private string periodTimeEnd = "23:59:59";
        public static bool isActivateNewAlgorythm = true; // регулирует отображение дней со стоянками в алгоритмах пробег и пробег суточный 20.10.2014

        /// <summary>
        /// Конструктор
        /// </summary>
        public FormInterval()
        {
            InitializeComponent();
            Localized();

            checkEdits = new CheckEdit[31]; //  число дней в месяце
            checkEdits[0] = checkEdit1;
            checkEdits[1] = checkEdit2;
            checkEdits[2] = checkEdit3;
            checkEdits[3] = checkEdit4;
            checkEdits[4] = checkEdit5;
            checkEdits[5] = checkEdit6;
            checkEdits[6] = checkEdit7;
            checkEdits[7] = checkEdit8;
            checkEdits[8] = checkEdit9;
            checkEdits[9] = checkEdit10;
            checkEdits[10] = checkEdit11;
            checkEdits[11] = checkEdit12;
            checkEdits[12] = checkEdit13;
            checkEdits[13] = checkEdit14;
            checkEdits[14] = checkEdit15;
            checkEdits[15] = checkEdit16;
            checkEdits[16] = checkEdit17;
            checkEdits[17] = checkEdit18;
            checkEdits[18] = checkEdit19;
            checkEdits[19] = checkEdit20;
            checkEdits[20] = checkEdit21;
            checkEdits[21] = checkEdit22;
            checkEdits[22] = checkEdit23;
            checkEdits[23] = checkEdit24;
            checkEdits[24] = checkEdit25;
            checkEdits[25] = checkEdit26;
            checkEdits[26] = checkEdit27;
            checkEdits[27] = checkEdit28;
            checkEdits[28] = checkEdit29;
            checkEdits[29] = checkEdit30;
            checkEdits[30] = checkEdit31;

            checkEditMonth.Checked = false;
            checkEditWeek.Checked = false;
            checkEditTime.Checked = false;

            BlockingTime(checkEditTime.Checked);
            BlockingWeek(checkEditWeek.Checked);
            BlockingMonth(checkEditMonth.Checked);
        }

        public bool GetCheckTime
        {
            get { return checkEditTime.Checked; }
        }

        public bool GetCheckWeek
        {
            get { return checkEditWeek.Checked; }
        }

        public bool GetCheckMonth
        {
            get { return checkEditMonth.Checked; }
        }

        private void Localized()
        {
            Text = Resources.IntervalForm;
            checkEditTime.Text = Resources.HoursMinute;
            labelControl1.Text = Resources.FromBegin;
            labelControl2.Text = Resources.FromEnd;
            checkEditWeek.Text = Resources.WeekDay;
            ButtonSelectDay.Text = Resources.DayWeekSel;
            ButtonDayClear.Text = Resources.DayWeekReset;
            checkEditMonth.Text = Resources.DayMonth;
            ButtonOdd.Text = Resources.DayMonthEven;
            ButtonNoOdd.Text = Resources.DayMonthOdd;
            ButtonSelectMonth.Text = Resources.DayMonthSel;
            ButtonMonthClear.Text = Resources.DayMonthRes;
            ButtonOK.Text = Resources.ButtonOK;
            ButtonCancel.Text = Resources.ButtonCancel;
        }

        static public bool isActivate()
        {
            return  (flagEditTime | flagEditWeek  | flagEditMonth);
        }

        static public string getWeekPeriod()
        {
            return week;
        }

        static public string getMonthPeriod()
        {
            return month;
        }

        private void formatingDateTimeInterval()
        {
            periodTimeBegin = "00:00:00";
            periodTimeEnd = "23:59:59";

            if (checkEditTime.Checked)
            {
                if (!((Convert.ToInt32(textBoxHour1.Text) > -1 && Convert.ToInt32(textBoxHour1.Text) < 24) &&
                    (Convert.ToInt32(textBoxHour2.Text) > -1 && Convert.ToInt32(textBoxHour2.Text) < 24)))
                {
                    XtraMessageBox.Show(Resources.TimeIntervalType, "Error", MessageBoxButtons.OK);
                    return;
                }

                if( !( ( Convert.ToInt32( textBoxMinute1.Text ) > -1 && Convert.ToInt32( textBoxMinute1.Text ) < 60 ) &&
                    ( Convert.ToInt32( textBoxMinute2.Text ) > -1 && Convert.ToInt32( textBoxMinute2.Text ) < 60 ) ) )
                {
                    XtraMessageBox.Show( Resources.TimeIntervalMinute, "Error", MessageBoxButtons.OK );
                    return;
                }
                
                periodTimeBegin = textBoxHour1.Text + ":" + textBoxMinute1.Text + ":" + "00";
                DateTime begin = Convert.ToDateTime( periodTimeBegin );
                periodTimeEnd = textBoxHour2.Text + ":" + textBoxMinute2.Text + ":" + "00";
                DateTime end = Convert.ToDateTime( periodTimeEnd );
                periodTimeBegin = begin.ToString( "HH:mm:ss" );
                periodTimeEnd = end.ToString( "HH:mm:ss" );
            } // if

            week = "";

            if (checkEditWeek.Checked)
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    long weekd = (long) weekDaysCheckEdit1.WeekDays;

                    if ((weekd & 0x2) == 0x2) // mon - 2
                    {
                        week = week + 0 + ";";
                    }

                    if ((weekd & 0x4) == 0x4) // tue - 3
                    {
                        week = week + 1 + ";";
                    }

                    if ((weekd & 0x8) == 0x8) // wed - 4
                    {
                        week = week + 2 + ";";
                    }

                    if ((weekd & 0x10) == 0x10) // thu - 5
                    {
                        week = week + 3 + ";";
                    }

                    if ((weekd & 0x20) == 0x20) // fri - 6
                    {
                        week = week + 4 + ";";
                    }

                    if ((weekd & 0x40) == 0x40) // sat - 7
                    {
                        week = week + 5 + ";";
                    }

                    if ((weekd & 0x1) == 0x1) // sun - 1
                    {
                        week = week + 6 + ";";
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    long weekd = (long)weekDaysCheckEdit1.WeekDays;

                    if( ( weekd & 0x2 ) == 0x2 ) // mon - 2
                    {
                        week = week + 1 + ";";
                    }

                    if( ( weekd & 0x4 ) == 0x4 ) // tue - 3
                    {
                        week = week + 2 + ";";
                    }

                    if( ( weekd & 0x8 ) == 0x8 ) // wed - 4
                    {
                        week = week + 3 + ";";
                    }

                    if( ( weekd & 0x10 ) == 0x10 ) // thu - 5
                    {
                        week = week + 4 + ";";
                    }

                    if( ( weekd & 0x20 ) == 0x20 ) // fri - 6
                    {
                        week = week + 5 + ";";
                    }

                    if( ( weekd & 0x40 ) == 0x40 ) // sat - 7
                    {
                        week = week + 6 + ";";
                    }

                    if( ( weekd & 0x1 ) == 0x1 ) // sun - 1
                    {
                        week = week + 7 + ";";
                    }
                } // if
            } // if

            month = "";

            if (checkEditMonth.Checked)
            {
                for (int i = 0; i < checkEdits.Length; i++)
                {
                    if (checkEdits[i].Checked)
                        month = month + (i + 1) + ";";
                }
            } // if
        }

        private void BlockingTime(bool check)
        {
            textBoxHour1.Enabled = check;
            textBoxHour2.Enabled = check;
            textBoxMinute1.Enabled = check;
            textBoxMinute2.Enabled = check;
        }

        private void BlockingWeek(bool check)
        {
            ButtonDayClear.Enabled = check;
            ButtonSelectDay.Enabled = check;
            weekDaysCheckEdit1.Enabled = check;
        }

        private void BlockingMonth(bool check)
        {
            ButtonOdd.Enabled = check;
            ButtonNoOdd.Enabled = check;
            ButtonSelectMonth.Enabled = check;
            ButtonMonthClear.Enabled = check;

            for (int i = 0; i < checkEdits.Length; i++)
            {
                checkEdits[i].Enabled = check;
            }
        }

        private void ButtonOK_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
            // Формируем строку запроса на основе выбранных дат
            formatingDateTimeInterval();
        }

        private void ButtonCancel_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }

        private void ButtonDayClear_Click( object sender, EventArgs e )
        {
            weekDaysCheckEdit1.WeekDays = (WeekDays)0;
        }

        private void ButtonMonthClear_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }
        }

        private void ButtonNoOdd_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }

            for( int i = 0; i < checkEdits.Length; i = i + 2 )
            {
                checkEdits[i].Checked = true;
            }
        }

        private void ButtonOdd_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }

            for( int i = 1; i < checkEdits.Length; i = i + 2 )
            {
                checkEdits[i].Checked = true;
            }
        }

        private void ButtonSelectDay_Click( object sender, EventArgs e )
        {
            weekDaysCheckEdit1.WeekDays = WeekDays.EveryDay;
        }

        private void ButtonSelectMonth_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkEdits.Length; i++)
            {
                checkEdits[i].Checked = true;
            }
        }

        private void checkEditTime_CheckedChanged(object sender, EventArgs e)
        {
            BlockingTime(checkEditTime.Checked);
            flagEditTime = checkEditTime.Checked;
            periodTimeBegin = "00:00:00";
            periodTimeEnd = "23:59:59";
        }

        private void checkEditWeek_CheckedChanged( object sender, EventArgs e )
        {
            BlockingWeek(checkEditWeek.Checked);
            flagEditWeek = checkEditWeek.Checked;
        }

        private void checkEditMonth_CheckedChanged( object sender, EventArgs e )
        {
            BlockingMonth(checkEditMonth.Checked);
            flagEditMonth = checkEditMonth.Checked;
        }

        public static string getTimePeriodBegin()
        {
            return periodTimeBegin;
        }

        public static string getTimePeriodEnd()
        {
            return periodTimeEnd;
        }

        public static bool isActiveMonth()
        {
            return flagEditMonth;
        }

        public static bool isActiveTime()
        {
            return flagEditTime;
        }

        public static bool isActiveWeek()
        {
            return flagEditWeek;
        }
    }
}
