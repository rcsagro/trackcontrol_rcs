﻿using System;
using System.Collections.Generic;
using System.Text;


namespace TrackControl.General.Patameters
{
    public class TotalParamsProvider
    {
        /// <summary>
        /// Управление запуском анализа дней дат при загрузке программы
        /// </summary>
        public bool IsDataAnalizOn
        {
            get
            {
                return Convert.ToBoolean(Parameters.ReadParTotal(Parameters.Numpar.TotalIsDataAnalizOn, "True"));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.TotalIsDataAnalizOn, newValue.ToString());
            }
        }

        /// <summary>
        /// Версия спутниковых карт Google Map
        /// </summary
        public string GoogleSatteliteKey
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.TotalGoogleSatteliteKey, "812"); 
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.TotalGoogleSatteliteKey, value.ToString());
            }
        }

        /// <summary>
        /// Управление отображением графиков топлива
        /// </summary>
        public bool IsGraphFuelShow
        {
            get
            {
                return Convert.ToBoolean( Parameters.ReadParTotal( Parameters.Numpar.TotalIsGraphFuelShow, "False" ) );
            }
            set
            {
                bool newValue;
                if( bool.TryParse( value.ToString(), out newValue ) )
                    Parameters.SetParTotal( Parameters.Numpar.TotalIsGraphFuelShow, newValue.ToString() );
            }
        }
			
        /// <summary>
        /// Использовать адресный слой Luxena
        /// </summary>
        public bool IsLuxenaUse
        {
            get
            {
                return Convert.ToBoolean(Parameters.ReadParTotal(Parameters.Numpar.LuxenaUse, "False"));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.LuxenaUse, newValue.ToString());
            }
        }

        /// <summary>
        /// Использовать адресный слой GisFile
        /// </summary>
        public bool IsGisFileUse
        {
            get
            {
                return Convert.ToBoolean(Parameters.ReadParTotal(Parameters.Numpar.GisFileUse, "False"));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.GisFileUse, newValue.ToString());
            }
        }
			
        /// <summary>
        /// <summary>
        /// адрес сервера Luxena
        /// </summary>
        public string LuxenaServer
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.LuxenaServer, "api.mq.ua");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.LuxenaServer, value);
            }
        }

        /// <summary>
        /// <summary>
        /// адрес сервера GisFile
        /// </summary>
        public string GisFileServer
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.GisFileServer, "gisfile.com");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.GisFileServer, value);
            }
        }
        /// <summary>
        /// Использовать адресный слой OSM
        /// </summary>
        public bool IsOpenStreetMapUse // это получать адресс из слоя любого геокодера
        {
            get
            {
                return Convert.ToBoolean( Parameters.ReadParTotal( Parameters.Numpar.OSMUse, "False" ) );
            }
            set
            {
                bool newValue;
                if( bool.TryParse( value.ToString(), out newValue ) )
                    Parameters.SetParTotal( Parameters.Numpar.OSMUse, newValue.ToString() );
                if (newValue){
                    IsRcsNominatimUse = false;
                    IsGoogleMapUse = false;
                }
            }
        }

        /// <summary>
        /// адрес сервера OpenStreetMap
        /// </summary>
        public string OpenStreetMapServer
       {
            get
            {
                return Parameters.ReadParTotalOsmServer(Parameters.Numpar.OSMServer, "http://nominatim.openstreetmap.org/reverse?format=xml&accept-language={2}&lat={0}&lon={1}&zoom=18&addressdetails=1");
            }
            set
            {
                Parameters.SetParTotal( Parameters.Numpar.OSMServer, value );
            }
        }

        /// <summary>
        /// адрес порта OpenStreetMap
        /// </summary>
        public string OpenStreetMapPort
        {
            get
            {
                return Parameters.ReadParTotal( Parameters.Numpar.OSMPort, "----" );
            }
            set
            {
                Parameters.SetParTotal( Parameters.Numpar.OSMPort, value );
            }
        }

        /// <summary>
        /// язык адресов
        public string MapsLang{
            get
            {
                return Parameters.ReadParTotal( Parameters.Numpar.MapsLang, "ru" );
            }
            set
            {
                Parameters.SetParTotal( Parameters.Numpar.MapsLang, value );
            }
        }

        /// <summary>
        /// ключ доступа к OpenStreetMap </summary>
        public string OpenStreetMapKey
        {
            get
            {
                return Parameters.ReadParTotalOsmKey(Parameters.Numpar.OSMKey, "QkC9Ip4YS49P28ocINmGujVItxLo3EPg");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.OSMKey, value);
            }
        }

        public bool IsGoogleMapUse 
        {
            get
            {
                return Convert.ToBoolean(Parameters.ReadParTotal(Parameters.Numpar.GoogleMapUse, "False"));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.GoogleMapUse, newValue.ToString());
                if (newValue)
                {
                    IsOpenStreetMapUse = false;
                    IsRcsNominatimUse = false;
                }
            }
        }

        /// <summary>
        /// ключ доступа к GoogleMap </summary>
        public string GoogleMapKey
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.GoogleMapKey, "ABQIAAAAWaQgWiEBF3lW97ifKnAczhRAzBk5Igf8Z5n2W3hNnMT0j2TikxTLtVIGU7hCLLHMAuAMt-BO5UrEWA");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.GoogleMapKey, value);
            }
        }
        /// <summary>
        /// язык адресного слоя OpenStreetMap
        /// </summary>
        public List<Lang> GetLangListOSM
        {
            get
            {
                List<Lang> langs = new List<Lang>();
                langs.Add( new Lang( "uk" ) );
                langs.Add( new Lang( "ru" ) );
                langs.Add( new Lang( "en" ) );
                langs.Add(new Lang("es"));
                return langs;
            }
        }

        /// <summary>
        /// Использовать адресный слой RcsNominatim
        /// </summary>
        public bool IsRcsNominatimUse
        {
            get
            {
                return Convert.ToBoolean(Parameters.ReadParTotal(Parameters.Numpar.RcsNominatimUse, "False"));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.RcsNominatimUse, newValue.ToString());
                if (newValue)
                {
                    IsOpenStreetMapUse = false;
                    IsGoogleMapUse = false;
                }
            }
        }

        /// <summary>
        /// Минимальное время движения в начале смены
        /// </summary>
        public string MinTimeMove
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.MinTimeMove,"0");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.MinTimeMove, value);
            }
        }

        /// <summary>
        /// список языков
        /// </summary>
        public List<Lang> GetLangList
        {
            get
            {
                List<Lang> langs = new List<Lang>();
                langs.Add(new Lang("uk"));
                langs.Add(new Lang("ru"));
                langs.Add(new Lang("en")); 
                return langs;
            }
        }

        public class Lang
        {
            string _name;
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public Lang(string name)
            {
                _name = name;
            }
        }
    }
}
