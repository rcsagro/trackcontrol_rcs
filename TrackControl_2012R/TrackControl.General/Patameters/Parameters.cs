﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.General
{
    public static class Parameters
    {
        public enum Numpar
        {
            NtfTimeForDelete = 1,
            NtfTimeForControl,
            NtfTimePeriod,
            NtfLastIdLog,
            MailAddressFrom,
            MailSmtpAddress,
            MailSmtpPort,
            MailSmtpLogin,
            MailSmtpPassword,
            ComDriverType,
            MailStartTls,
            TotalIsDataAnalizOn,
            TotalIsGraphFuelShow,
            ReceiveAdressFromInternet,
            LuxenaUse,
            LuxenaLang,
            LuxenaServer,
            OSMUse,
            OSMServer,
            OSMPort,
            MapsLang,
            MinTimeMove,
            OSMKey,
            RcsNominatimUse,
            GoogleMapUse,
            GoogleMapKey,
            GisFileUse,
            GisFileServer,
            TotalGoogleSatteliteKey
        }

        public static string ReadParTotal(Numpar number)
        {
            string sReturn = "";
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "SELECT  parameters.Value FROM  parameters "
                + " WHERE parameters.Number = " + (int)number;
                //object ob = cn.GetScalarValue(sql);
                object ob = db.GetScalarValue( sql );

                if (ob == null)
                {
                    sql = "INSERT INTO parameters (Number,Value) VALUES  (" + (int)number + ",'" + "" + "')";
                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand( sql );
                }
                else
                {
                    sReturn = ob.ToString();
                }
            }
            db.CloseDbConnection();
            return sReturn;
        }

        public static string ReadParTotal(Numpar number, string sDefault)
        {
            string sReturn = "";
            if (String.IsNullOrEmpty(sDefault))
                sDefault = "False";

           // using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "SELECT  parameters.Value FROM  parameters "
                + " WHERE parameters.Number = " + (int)number;
                //object ob = cn.GetScalarValue(sql);
                object ob = db.GetScalarValue( sql );

                if (ob == null)
                {
                    sql = "INSERT INTO parameters (Number,Value) VALUES  (" + (int)number + ",'" + sDefault + "')";
                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand( sql );
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();
                    sReturn = sDefault;
                }
                else
                {
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();

                    sReturn = ob.ToString();
                }
            }
            db.CloseDbConnection();

            if (String.IsNullOrEmpty(sReturn))
                sReturn = "False";

            if (number == Parameters.Numpar.OSMUse)
                if (sReturn != "True" && sReturn != "False")
                    return "False";
            return sReturn;
        }

        public static string ReadParTotalOsmKey(Numpar number, string sDefault)
        {
            string sReturn = "";

            if (String.IsNullOrEmpty(sDefault))
                sDefault = "";

            // using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "SELECT  parameters.Value FROM  parameters "
                + " WHERE parameters.Number = " + (int)number;
                //object ob = cn.GetScalarValue(sql);
                object ob = db.GetScalarValue(sql);

                if (ob == null)
                {
                    sql = "INSERT INTO parameters (Number,Value) VALUES  (" + (int)number + ",'" + sDefault + "')";
                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand(sql);
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();
                    sReturn = sDefault;
                }
                else
                {
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();

                    sReturn = ob.ToString();
                }
            }
            db.CloseDbConnection();

            if (String.IsNullOrEmpty(sReturn))
                sReturn = "";

            return sReturn;
        }

        public static string ReadParTotalOsmServer(Numpar number, string sDefault)
        {
            string sReturn = "";
            if (String.IsNullOrEmpty(sDefault))
                sDefault = "";

            // using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "SELECT  parameters.Value FROM  parameters "
                + " WHERE parameters.Number = " + (int)number;
                //object ob = cn.GetScalarValue(sql);
                object ob = db.GetScalarValue(sql);

                if (ob == null)
                {
                    sql = "INSERT INTO parameters (Number,Value) VALUES  (" + (int)number + ",'" + sDefault + "')";
                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand(sql);
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();
                    sReturn = sDefault;
                }
                else
                {
                    //cn.CloseMySQLConnection();
                    db.CloseDbConnection();

                    sReturn = ob.ToString();
                }
            }
            db.CloseDbConnection();

            if (String.IsNullOrEmpty(sReturn))
                sReturn = "";
            return sReturn;
        }

        public static void SetParTotal(Numpar number, string sValue)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "SELECT  parameters.Value FROM  parameters "
                + " WHERE parameters.Number = " + (int)number;
                //int iCount = cn.GetDataReaderRecordsCount(sql);
                int iCount = db.GetDataReaderRecordsCount( sql );
                if (iCount == 0)
                {
                    sql = "INSERT INTO parameters (Number,Value) VALUES  ('" + (int)number + "','" + sValue + "')";
                }
                if (iCount > 0)
                {
                    sql = "UPDATE parameters Set  Value = '" + sValue + "' WHERE Number = " + (int)number;
                }
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
            return;
        }
    }
}
