using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace TrackControl.General
{
    public class Excel : IDisposable
    {
        public const string UID = "Excel.Application";
        object oExcel = null;
        object WorkBooks, WorkBook, WorkSheets, WorkSheet, Range, Interior, Cells;
        private System.Globalization.CultureInfo cultureInfo;

        public Excel()
        {
            System.Type exc = Type.GetTypeFromProgID(UID);
            oExcel = Activator.CreateInstance(exc);
        }
        /// <summary>
        /// ��������� EXCEL
        /// </summary>
        public bool Visible
        {
            set
            {
                oExcel.GetType().InvokeMember("Visible", BindingFlags.SetProperty,
                    null, oExcel, new object[] { value });
            }
            get
            {
                return Convert.ToBoolean(oExcel.GetType().InvokeMember("Visible", BindingFlags.GetProperty,
                   null, oExcel, null));
            }
        }
        /// <summary>
        /// ������ ��������� EXCEL
        /// </summary>
        public bool DisplayScrollBarsVisible
        {
            set
            {
                oExcel.GetType().InvokeMember("DisplayScrollBars", BindingFlags.SetProperty,
                    null, oExcel, new object[] { value });
            }
            get
            {
                return Convert.ToBoolean(oExcel.GetType().InvokeMember("DisplayScrollBars", BindingFlags.GetProperty,
                   null, oExcel, null));
            }
        }
        /// <summary>
        /// ������ ��������� EXCEL
        /// </summary>
        public bool DisplayStatusBarVisible
        {
            set
            {
                oExcel.GetType().InvokeMember("DisplayStatusBar", BindingFlags.SetProperty,
                    null, oExcel, new object[] { value });
            }
            get
            {
                return Convert.ToBoolean(oExcel.GetType().InvokeMember("DisplayStatusBar", BindingFlags.GetProperty,
                   null, oExcel, null));
            }
        }

        /// <summary>
        /// ��������� ����
        /// </summary>
        public string Caption
        {
            set
            {
                oExcel.GetType().InvokeMember("Caption", BindingFlags.SetProperty,
                    null, oExcel, new object[] { value });
            }
            get
            {
                return Convert.ToString(oExcel.GetType().InvokeMember("Caption", BindingFlags.GetProperty,
                    null, oExcel, null));
            }
        }
        /// <summary>
        /// ��������� ����� ����
        /// </summary>
        public enum XlWindowState
        {
            xlMaximized = -4137,
            xlMinimized = -4140,
            xlNormal = -4143
        }
        /// <summary>
        /// �������� ���� EXCEL
        /// </summary>
        public XlWindowState WindowState
        {
            set
            {
                oExcel.GetType().InvokeMember("WindowState", BindingFlags.SetProperty,
                    null, oExcel, new object[] { value });
            }
        }
        /// <summary>
        /// ������� ��������
        /// </summary>
        /// <param name="name"></param>
        public bool OpenDocument(string name)
        {
            cultureInfo = new System.Globalization.CultureInfo( "en-US" );
            try
            {
                WorkBooks = oExcel.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, oExcel, null);
                WorkBook = WorkBooks.GetType().InvokeMember( "Open", BindingFlags.InvokeMethod, null, WorkBooks, new object[] { name, true }, cultureInfo );
                WorkSheets = WorkBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, WorkBook, null);
                WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.InnerException, "������ �������� ����� " + name, MessageBoxButtons.OK);
                return false;
            }

            return true;
            }
        /// <summary>
        /// ������� ����� ��������
        /// </summary>
        public void NewDocument()
        {
            WorkBooks = oExcel.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, oExcel, null);
            WorkBook = WorkBooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, WorkBooks, null);
            WorkSheets = WorkBook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, WorkBook, null);
            WorkSheet = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty, null, WorkSheet, new object[1] { "A1" });
        }
        /// <summary>
        /// ������� ��������
        /// </summary>
        public void CloseDocument()
        {
            WorkBook.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, WorkBook, new object[] { true }, cultureInfo);
        }
        /// <summary>
        /// ��������� ��������
        /// </summary>
        /// <param name="name"></param>
        public void SaveDocument(string name)
        {
            if (File.Exists(name))
                WorkBook.GetType().InvokeMember("Save", BindingFlags.InvokeMethod, null,
                    WorkBook, null);
            else
                WorkBook.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null,
                    WorkBook, new object[] { name });
        }
        /// <summary>
        /// ��������� ����� ���� ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="color"></param>
        public void SetColor(string range, int color)
        {
            //Range.Interior.ColorIndex
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });

            Interior = Range.GetType().InvokeMember("Interior", BindingFlags.GetProperty,
                null, Range, null);

            Range.GetType().InvokeMember("ColorIndex", BindingFlags.SetProperty, null,
                Interior, new object[] { color });
        }
        /// <summary>
        /// ���������� ��������
        /// </summary>
        public enum XlPageOrientation
        {
            xlPortrait = 1, //�������
            xlLandscape = 2 // ���������
        }
        /// <summary>
        /// ��������� ���������� ��������
        /// </summary>
        /// <param name="Orientation"></param>
        public void SetOrientation(XlPageOrientation Orientation)
        {
            //Range.Interior.ColorIndex
            object PageSetup = WorkSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, WorkSheet, null);

            PageSetup.GetType().InvokeMember("Orientation", BindingFlags.SetProperty,
                null, PageSetup, new object[] { 2 });
        }
        /// <summary>
        /// ��������� �������� ����� �����
        /// </summary>
        /// <param name="Left"></param>
        /// <param name="Right"></param>
        /// <param name="Top"></param>
        /// <param name="Bottom"></param>
        public void SetMargin(double Left, double Right, double Top, double Bottom)
        {
            //Range.PageSetup.LeftMargin - �����
            //Range.PageSetup.RightMargin - ������ 
            //Range.PageSetup.TopMargin - �������
            //Range.PageSetup.BottomMargin - ������
            object PageSetup = WorkSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, WorkSheet, null);

            PageSetup.GetType().InvokeMember("LeftMargin", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Left });
            PageSetup.GetType().InvokeMember("RightMargin", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Right });
            PageSetup.GetType().InvokeMember("TopMargin", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Top });
            PageSetup.GetType().InvokeMember("BottomMargin", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Bottom });
        }
        /// <summary>
        /// ������� �����
        /// </summary>
        public enum xlPaperSize
        {
            xlPaperA4 = 9,
            xlPaperA4Small = 10,
            xlPaperA5 = 11,
            xlPaperLetter = 1,
            xlPaperLetterSmall = 2,
            xlPaper10x14 = 16,
            xlPaper11x17 = 17,
            xlPaperA3 = 9,
            xlPaperB4 = 12,
            xlPaperB5 = 13,
            xlPaperExecutive = 7,
            xlPaperFolio = 14,
            xlPaperLedger = 4,
            xlPaperLegal = 5,
            xlPaperNote = 18,
            xlPaperQuarto = 15,
            xlPaperStatement = 6,
            xlPaperTabloid = 3
        }
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        /// <param name="Size"></param>
        public void SetPaperSize(xlPaperSize Size)
        {
            //Range.PageSetup.PaperSize - ������ �����
            object PageSetup = WorkSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, WorkSheet, null);

            PageSetup.GetType().InvokeMember("PaperSize", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Size });
        }
        /// <summary>
        /// ��������� �������� ������
        /// </summary>
        /// <param name="Percent"></param>
        public void SetZoom(int Percent)
        {
            //Range.PageSetup.Zoom - ������� ������
            object PageSetup = WorkSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, WorkSheet, null);

            PageSetup.GetType().InvokeMember("Zoom", BindingFlags.SetProperty,
                null, PageSetup, new object[] { Percent });
        }
        /// <summary>
        /// ������������� ����
        /// </summary>
        /// <param name="n"></param>
        /// <param name="Name"></param>
        public void ReNamePage(int n, string Name)
        {
            //Range.Interior.ColorIndex
            object Page = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { n });

            Page.GetType().InvokeMember("Name", BindingFlags.SetProperty,
                null, Page, new object[] { Name });
        }
        /// <summary>
        /// ���������� �����
        /// </summary>
        /// <param name="Name"></param>
        public void AddNewPage(string Name)
        {
            //Worksheet.Add.Item
            //Name - �������� ��������
            WorkSheet = WorkSheets.GetType().InvokeMember("Add", BindingFlags.GetProperty, null, WorkSheets, null);

            object Page = WorkSheets.GetType().InvokeMember("Item", BindingFlags.GetProperty, null, WorkSheets, new object[] { 1 });
            Page.GetType().InvokeMember("Name", BindingFlags.SetProperty, null, Page, new object[] { Name });
        }
        /// <summary>
        /// ���������� ������ � ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="font"></param>
        public void SetFont(string range, Font font)
        {
            //Range.Font.Name
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });

            object Font = Range.GetType().InvokeMember("Font", BindingFlags.GetProperty,
                null, Range, null);

            Range.GetType().InvokeMember("Name", BindingFlags.SetProperty, null,
                Font, new object[] { font.Name });

            Range.GetType().InvokeMember("Size", BindingFlags.SetProperty, null,
                Font, new object[] { font.Size });
        }
        /// <summary>
        /// ������ �������� � ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="value"></param>
        public void SetValue(string range, string value)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("Value", BindingFlags.SetProperty, null, Range, new object[] { value });
        }
        /// <summary>
        /// ����������� �����
        /// </summary>
        /// <param name="range"></param>
        /// <param name="MergeCells"></param>
        public void SetMerge(string range, bool MergeCells)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("MergeCells", BindingFlags.SetProperty, null, Range, new object[] { MergeCells });
        }
        /// <summary>
        /// ��������� ������ ��������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Width"></param>
        public void SetColumnWidth(string range, double Width)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Width };
            Range.GetType().InvokeMember("ColumnWidth", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// ��������� ����������� ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Orientation"></param>
        public void SetTextOrientation(string range, int Orientation)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Orientation };
            Range.GetType().InvokeMember("Orientation", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// ������������ ������ � ������ �� ���������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Alignment"></param>
        public void SetVerticalAlignment(string range, int Alignment)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Alignment };
            Range.GetType().InvokeMember("VerticalAlignment", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// ������������ ������ � ������ �� �����������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Alignment"></param>
        public void SetHorisontalAlignment(string range, int Alignment)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Alignment };
            Range.GetType().InvokeMember("HorizontalAlignment", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// �������������� ���������� ������ � ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Start"></param>
        /// <param name="Length"></param>
        /// <param name="Color"></param>
        /// <param name="FontStyle"></param>
        /// <param name="FontSize"></param>
        public void SelectText(string range, int Start, int Length, int Color, string FontStyle, int FontSize)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Start, Length };
            object Characters = Range.GetType().InvokeMember("Characters", BindingFlags.GetProperty, null, Range, args);
            object Font = Characters.GetType().InvokeMember("Font", BindingFlags.GetProperty, null, Characters, null);
            Font.GetType().InvokeMember("ColorIndex", BindingFlags.SetProperty, null, Font, new object[] { Color });
            Font.GetType().InvokeMember("FontStyle", BindingFlags.SetProperty, null, Font, new object[] { FontStyle });
            Font.GetType().InvokeMember("Size", BindingFlags.SetProperty, null, Font, new object[] { FontSize });

        }
        /// <summary>
        /// ������� ���� � ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Value"></param>
        public void SetWrapText(string range, bool Value)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Value };
            Range.GetType().InvokeMember("WrapText", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// �������� ����������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="CommentVisible"></param>
        /// <param name="Text"></param>
        public void CreateComment(string range, bool CommentVisible, string Text)
        {
            //Range.Addcomment
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("AddComment", BindingFlags.InvokeMethod, null, Range, null);
            object Comment = Range.GetType().InvokeMember("Comment", BindingFlags.GetProperty, null, Range, null);
            Comment.GetType().InvokeMember("Visible", BindingFlags.SetProperty, null, Comment, new object[] { false });
            Comment.GetType().InvokeMember("Text", BindingFlags.InvokeMethod, null, Comment, new object[] { Text });
        }
        /// <summary>
        /// �������� ����������
        /// </summary>
        /// <param name="range"></param>
        public void DeleteComment(string range)
        {
            //Range.ClearComment
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            Range.GetType().InvokeMember("ClearComments", BindingFlags.InvokeMethod, null, Range, null);
        }
        /// <summary>
        /// ������ ���������� ����������
        /// </summary>
        public enum XlCommentDisplayMode
        {
            xlCommentAndIndicator = 1,
            xlCommentIndicatorOnly = -1,
            xlNoIndicator = 0
        }
        /// <summary>
        /// �������� ��� ������ ��� ���������� 
        /// </summary>
        /// <param name="Mode"></param>
        public void DisplayCommentIndicator(XlCommentDisplayMode Mode)
        {
            //Application.DisplayCommentIndicator
            oExcel.GetType().InvokeMember("DisplayCommentIndicator", BindingFlags.SetProperty,
                null, oExcel, new object[] { Mode });
        }
        /// <summary>
        /// ����������� �����
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Value"></param>
        public void SetRowsGroup(string range, bool Value)
        {
            //Selection.Rows.Group
            //Selection.Rows.Ungroup
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object Rows = Range.GetType().InvokeMember("Rows", BindingFlags.GetProperty, null, Range, null);
            if (Value)
                Rows.GetType().InvokeMember("Group", BindingFlags.GetProperty, null, Rows, null);
            else
                Rows.GetType().InvokeMember("Ungroup", BindingFlags.GetProperty, null, Rows, null);
        }
        /// <summary>
        /// ����������� ��������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Value"></param>
        public void SetColumnsGroup(string range, bool Value)
        {
            //Selection.Columns.Group
            //Selection.Columns.Ungroup
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object Columns = Range.GetType().InvokeMember("Columns", BindingFlags.GetProperty, null, Range, null);
            if (Value)
                Columns.GetType().InvokeMember("Group", BindingFlags.GetProperty, null, Columns, null);
            else
                Columns.GetType().InvokeMember("Ungroup", BindingFlags.GetProperty, null, Columns, null);
        }

        /// <summary>
        /// ��������� ������ ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Height"></param>
        public void SetRowHeight(string range, double Height)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { Height };
            Range.GetType().InvokeMember("RowHeight", BindingFlags.SetProperty, null, Range, args);
        }
        /// <summary>
        /// ��������� ���� ������
        /// </summary>
        /// <param name="range"></param>
        /// <param name="Style"></param>
        public void SetBorderStyle(string range, int Style)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range });
            object[] args = new object[] { 1 };
            object[] args1 = new object[] { 1 };
            object Borders = Range.GetType().InvokeMember("Borders", BindingFlags.GetProperty, null, Range, null);
            Borders = Range.GetType().InvokeMember("LineStyle", BindingFlags.SetProperty, null, Borders, args);
        }
        /// <summary>
        /// ������ �������� �� ������
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public string GetValue(string range)
        {
            Range = WorkSheet.GetType().InvokeMember("Range", BindingFlags.GetProperty,
                null, WorkSheet, new object[] { range }, cultureInfo);
            object CellValue = Range.GetType().InvokeMember("Value", BindingFlags.GetProperty,
                null, Range, null, cultureInfo);
            if (Range != null) Marshal.ReleaseComObject(Range);
            if (CellValue == null)
                return "";
            else
                return CellValue.ToString ();
        }
        /// <summary>
        /// ���������� �����
        /// </summary>
        /// <returns></returns>
        public int RowMax()
        {
            Cells = WorkSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, WorkSheet, null);
            Range = Cells.GetType().InvokeMember("SpecialCells", BindingFlags.GetProperty, null, Cells, new object[] { 11 /*xlCellTypeLastCell*/}, cultureInfo);
            int RowMax = 0;
            int.TryParse(Cells.GetType().InvokeMember("Row", BindingFlags.GetProperty, null, Range, null).ToString(), out RowMax);
            if (Cells != null) Marshal.ReleaseComObject(Cells);
            if (Range != null) Marshal.ReleaseComObject(Range);
            return RowMax;

        }
        /// <summary>
        /// ���������� ��������
        /// </summary>
        /// <returns></returns>
        public int ColumnMax()
        {
            Cells = WorkSheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, WorkSheet, null);
            Range = Cells.GetType().InvokeMember("SpecialCells", BindingFlags.GetProperty, null, Cells, new object[] { 11 /*xlCellTypeLastCell*/}, cultureInfo);
            int ColumnMax = 0;
            int.TryParse(Cells.GetType().InvokeMember("Column", BindingFlags.GetProperty, null, Range, null).ToString(), out ColumnMax);
            if (Cells != null) Marshal.ReleaseComObject(Cells);
            if (Range != null) Marshal.ReleaseComObject(Range);
            return ColumnMax;

        }
        /// <summary>
        /// ��������� ���������� ����� ������� � ��� ��������� ����������.
        /// </summary>
        public string ParseColNum(int ColNum)
        {
            //StringBuilder sb = new StringBuilder();

            //if (ColNum <= 0) return "A";

            //while (ColNum != 0)
            //{
            //    sb.Append((char)('A' + (ColNum % 26)));
            //    ColNum /= 26;
            //}

            //return sb.ToString();
            string Out = "";
            if (ColNum <= 0) return "A";

            while (ColNum != 0)
            {
                Out=Out+ (char)('A' + (ColNum % 26));
                ColNum /= 26;
            }

            return Out;
        }
        public void Dispose()
        {
            if (Cells != null) Marshal.ReleaseComObject(Cells);
            if (Range != null) Marshal.ReleaseComObject(Range);
            if (WorkSheet != null) Marshal.ReleaseComObject(WorkSheet);
            if (WorkSheets != null) Marshal.ReleaseComObject(WorkSheets);
            if (WorkBook != null) Marshal.ReleaseComObject(WorkBook);
            if (WorkBooks != null) Marshal.ReleaseComObject(WorkBooks);
            if (oExcel != null) Marshal.ReleaseComObject(oExcel);
            // �������� ������� ������ ��� ����������� ������� ������
            GC.GetTotalMemory(true);
        }
    }
}
