using System;
using System.Data;
using DevExpress.XtraGrid.Views.Grid; 

namespace TrackControl.General
{
    public interface IDocument:IDocumentEntity 
    {
        /// <summary>
        /// �������� ���������
        /// </summary>
        int AddDoc();
        /// <summary>
        /// �������������� ���������
        /// </summary>
        bool UpdateDoc();
        /// <summary>
        /// �������� ���������
        /// </summary>
        bool DeleteDoc(bool bQuestionNeed);
        /// <summary>
        /// �������� ������������ ���������
        /// </summary>
        bool DeleteDocContent();
        /// <summary>
        /// �������� ������� ������ ���������
        /// </summary>
        bool DeleteDocTest();
        /// <summary>
        /// ����������� �� ���������� ���������� � ���� ������
        /// </summary>
        /// <returns></returns>
        bool TestDemo();
        /// <summary>
        /// ���������� ���������
        /// </summary>
        /// <returns></returns>
        DataTable GetContent();
        /// <summary>
        /// �������� ���������� ������� �� ������� ����������
        /// </summary>
        bool DeleteSelectedFromGrid(GridView gv);
        /// <summary>
        /// ���������� �������� �������� ������ ��������� 
        /// </summary>
        void UpdateDocTotals();
        /// <summary>
        /// ������� �������� ��������� �� ��� ��������������
        /// </summary>
        bool GetDocById(int id);
    }
}
