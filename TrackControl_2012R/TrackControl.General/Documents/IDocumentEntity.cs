﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.General
{
    public interface IDocumentEntity
    {
        /// <summary>
        /// Код документа
        /// </summary>
        int ID
        {
            get;
            set;
        }
        /// <summary>
        /// Дата документа
        /// </summary>
        DateTime Date
        {
            get;
            set;
        }
        /// <summary>
        /// Примечание документа
        /// </summary>
        string Remark
        {
            get;
            set;
        }
    }
}
