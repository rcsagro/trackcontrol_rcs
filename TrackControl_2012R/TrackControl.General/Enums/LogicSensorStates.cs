﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.General
{
    public enum LogicSensorStates
    {
        Absence = 2, // отсутствует
        Off = 0, // выключен
        On = 1, // включен
    }
}
