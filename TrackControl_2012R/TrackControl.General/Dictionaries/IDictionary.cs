﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.General
{
    public interface IDictionary:IEntity
    {
            string Name{ get; set; }
            string Comment{ get; set; }
            int AddItem();
            bool DeleteItem(bool bQuestionNeed);
            bool EditItem();
            bool TestLinks();
            bool TestNameForExist(string sNameForTest, bool bViewInfo = true);
    }
}
