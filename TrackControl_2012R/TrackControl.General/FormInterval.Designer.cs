﻿namespace TrackControl.Reports
{
    partial class FormInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlInterval = new DevExpress.XtraEditors.PanelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkEditMonth = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.ButtonSelectMonth = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonOdd = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonNoOdd = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonMonthClear = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkEditWeek = new DevExpress.XtraEditors.CheckEdit();
            this.ButtonSelectDay = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonDayClear = new DevExpress.XtraEditors.SimpleButton();
            this.weekDaysCheckEdit1 = new DevExpress.XtraScheduler.UI.WeekDaysCheckEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkEditTime = new DevExpress.XtraEditors.CheckEdit();
            this.textBoxMinute2 = new System.Windows.Forms.TextBox();
            this.textBoxHour2 = new System.Windows.Forms.TextBox();
            this.textBoxMinute1 = new System.Windows.Forms.TextBox();
            this.textBoxHour1 = new System.Windows.Forms.TextBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ButtonOK = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInterval)).BeginInit();
            this.panelControlInterval.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWeek.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTime.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlInterval
            // 
            this.panelControlInterval.Controls.Add(this.groupBox3);
            this.panelControlInterval.Controls.Add(this.groupBox2);
            this.panelControlInterval.Controls.Add(this.groupBox1);
            this.panelControlInterval.Controls.Add(this.ButtonOK);
            this.panelControlInterval.Controls.Add(this.ButtonCancel);
            this.panelControlInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlInterval.Location = new System.Drawing.Point(0, 0);
            this.panelControlInterval.Name = "panelControlInterval";
            this.panelControlInterval.Size = new System.Drawing.Size(461, 365);
            this.panelControlInterval.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkEditMonth);
            this.groupBox3.Controls.Add(this.checkEdit23);
            this.groupBox3.Controls.Add(this.checkEdit22);
            this.groupBox3.Controls.Add(this.checkEdit21);
            this.groupBox3.Controls.Add(this.checkEdit20);
            this.groupBox3.Controls.Add(this.checkEdit19);
            this.groupBox3.Controls.Add(this.checkEdit12);
            this.groupBox3.Controls.Add(this.checkEdit18);
            this.groupBox3.Controls.Add(this.checkEdit17);
            this.groupBox3.Controls.Add(this.checkEdit16);
            this.groupBox3.Controls.Add(this.checkEdit15);
            this.groupBox3.Controls.Add(this.checkEdit14);
            this.groupBox3.Controls.Add(this.checkEdit31);
            this.groupBox3.Controls.Add(this.checkEdit30);
            this.groupBox3.Controls.Add(this.checkEdit29);
            this.groupBox3.Controls.Add(this.checkEdit28);
            this.groupBox3.Controls.Add(this.checkEdit27);
            this.groupBox3.Controls.Add(this.checkEdit26);
            this.groupBox3.Controls.Add(this.checkEdit25);
            this.groupBox3.Controls.Add(this.checkEdit24);
            this.groupBox3.Controls.Add(this.checkEdit13);
            this.groupBox3.Controls.Add(this.checkEdit11);
            this.groupBox3.Controls.Add(this.checkEdit10);
            this.groupBox3.Controls.Add(this.checkEdit9);
            this.groupBox3.Controls.Add(this.checkEdit8);
            this.groupBox3.Controls.Add(this.checkEdit7);
            this.groupBox3.Controls.Add(this.checkEdit6);
            this.groupBox3.Controls.Add(this.checkEdit5);
            this.groupBox3.Controls.Add(this.checkEdit4);
            this.groupBox3.Controls.Add(this.checkEdit3);
            this.groupBox3.Controls.Add(this.checkEdit2);
            this.groupBox3.Controls.Add(this.checkEdit1);
            this.groupBox3.Controls.Add(this.ButtonSelectMonth);
            this.groupBox3.Controls.Add(this.ButtonOdd);
            this.groupBox3.Controls.Add(this.ButtonNoOdd);
            this.groupBox3.Controls.Add(this.ButtonMonthClear);
            this.groupBox3.Location = new System.Drawing.Point(9, 187);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(448, 144);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            // 
            // checkEditMonth
            // 
            this.checkEditMonth.Location = new System.Drawing.Point(4, 9);
            this.checkEditMonth.Name = "checkEditMonth";
            this.checkEditMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditMonth.Properties.Appearance.Options.UseFont = true;
            this.checkEditMonth.Properties.Caption = "Дни месяца";
            this.checkEditMonth.Size = new System.Drawing.Size(136, 19);
            this.checkEditMonth.TabIndex = 66;
            this.checkEditMonth.CheckedChanged += new System.EventHandler(this.checkEditMonth_CheckedChanged);
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(3, 86);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "23";
            this.checkEdit23.Size = new System.Drawing.Size(40, 19);
            this.checkEdit23.TabIndex = 57;
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(403, 59);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "22";
            this.checkEdit22.Size = new System.Drawing.Size(40, 19);
            this.checkEdit22.TabIndex = 56;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(363, 59);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "21";
            this.checkEdit21.Size = new System.Drawing.Size(40, 19);
            this.checkEdit21.TabIndex = 55;
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(323, 59);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "20";
            this.checkEdit20.Size = new System.Drawing.Size(40, 19);
            this.checkEdit20.TabIndex = 54;
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(283, 59);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "19";
            this.checkEdit19.Size = new System.Drawing.Size(40, 19);
            this.checkEdit19.TabIndex = 53;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(3, 59);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "12";
            this.checkEdit12.Size = new System.Drawing.Size(40, 19);
            this.checkEdit12.TabIndex = 47;
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(243, 59);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "18";
            this.checkEdit18.Size = new System.Drawing.Size(40, 19);
            this.checkEdit18.TabIndex = 5;
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(203, 59);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "17";
            this.checkEdit17.Size = new System.Drawing.Size(40, 19);
            this.checkEdit17.TabIndex = 52;
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(163, 59);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "16";
            this.checkEdit16.Size = new System.Drawing.Size(40, 19);
            this.checkEdit16.TabIndex = 51;
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(123, 59);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "15";
            this.checkEdit15.Size = new System.Drawing.Size(40, 19);
            this.checkEdit15.TabIndex = 50;
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(83, 59);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "14";
            this.checkEdit14.Size = new System.Drawing.Size(40, 19);
            this.checkEdit14.TabIndex = 49;
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(323, 86);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Caption = "31";
            this.checkEdit31.Size = new System.Drawing.Size(40, 19);
            this.checkEdit31.TabIndex = 65;
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(283, 86);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "30";
            this.checkEdit30.Size = new System.Drawing.Size(40, 19);
            this.checkEdit30.TabIndex = 64;
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(243, 86);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "29";
            this.checkEdit29.Size = new System.Drawing.Size(40, 19);
            this.checkEdit29.TabIndex = 63;
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(203, 86);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "28";
            this.checkEdit28.Size = new System.Drawing.Size(40, 19);
            this.checkEdit28.TabIndex = 62;
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(163, 86);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "27";
            this.checkEdit27.Size = new System.Drawing.Size(40, 19);
            this.checkEdit27.TabIndex = 61;
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(123, 86);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Caption = "26";
            this.checkEdit26.Size = new System.Drawing.Size(40, 19);
            this.checkEdit26.TabIndex = 60;
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(83, 86);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Caption = "25";
            this.checkEdit25.Size = new System.Drawing.Size(40, 19);
            this.checkEdit25.TabIndex = 59;
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(43, 86);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "24";
            this.checkEdit24.Size = new System.Drawing.Size(40, 19);
            this.checkEdit24.TabIndex = 58;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(43, 59);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "13";
            this.checkEdit13.Size = new System.Drawing.Size(39, 19);
            this.checkEdit13.TabIndex = 48;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(403, 32);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "11";
            this.checkEdit11.Size = new System.Drawing.Size(39, 19);
            this.checkEdit11.TabIndex = 46;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(363, 32);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "10";
            this.checkEdit10.Size = new System.Drawing.Size(39, 19);
            this.checkEdit10.TabIndex = 45;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(323, 32);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "9";
            this.checkEdit9.Size = new System.Drawing.Size(31, 19);
            this.checkEdit9.TabIndex = 44;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(283, 32);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "8";
            this.checkEdit8.Size = new System.Drawing.Size(31, 19);
            this.checkEdit8.TabIndex = 43;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(243, 32);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "7";
            this.checkEdit7.Size = new System.Drawing.Size(31, 19);
            this.checkEdit7.TabIndex = 42;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(203, 32);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "6";
            this.checkEdit6.Size = new System.Drawing.Size(31, 19);
            this.checkEdit6.TabIndex = 41;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(163, 32);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "5";
            this.checkEdit5.Size = new System.Drawing.Size(31, 19);
            this.checkEdit5.TabIndex = 40;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(123, 32);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "4";
            this.checkEdit4.Size = new System.Drawing.Size(31, 19);
            this.checkEdit4.TabIndex = 39;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(83, 32);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "3";
            this.checkEdit3.Size = new System.Drawing.Size(31, 19);
            this.checkEdit3.TabIndex = 38;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(43, 32);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "2";
            this.checkEdit2.Size = new System.Drawing.Size(31, 19);
            this.checkEdit2.TabIndex = 37;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(3, 32);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "1";
            this.checkEdit1.Size = new System.Drawing.Size(31, 19);
            this.checkEdit1.TabIndex = 36;
            // 
            // ButtonSelectMonth
            // 
            this.ButtonSelectMonth.Location = new System.Drawing.Point(256, 114);
            this.ButtonSelectMonth.Name = "ButtonSelectMonth";
            this.ButtonSelectMonth.Size = new System.Drawing.Size(99, 22);
            this.ButtonSelectMonth.TabIndex = 3;
            this.ButtonSelectMonth.Text = "Отметить все";
            this.ButtonSelectMonth.Click += new System.EventHandler(this.ButtonSelectMonth_Click);
            // 
            // ButtonOdd
            // 
            this.ButtonOdd.Location = new System.Drawing.Point(45, 114);
            this.ButtonOdd.Name = "ButtonOdd";
            this.ButtonOdd.Size = new System.Drawing.Size(100, 22);
            this.ButtonOdd.TabIndex = 2;
            this.ButtonOdd.Text = "Четные";
            this.ButtonOdd.Click += new System.EventHandler(this.ButtonOdd_Click);
            // 
            // ButtonNoOdd
            // 
            this.ButtonNoOdd.Location = new System.Drawing.Point(151, 114);
            this.ButtonNoOdd.Name = "ButtonNoOdd";
            this.ButtonNoOdd.Size = new System.Drawing.Size(99, 22);
            this.ButtonNoOdd.TabIndex = 1;
            this.ButtonNoOdd.Text = "Не Четные";
            this.ButtonNoOdd.Click += new System.EventHandler(this.ButtonNoOdd_Click);
            // 
            // ButtonMonthClear
            // 
            this.ButtonMonthClear.Location = new System.Drawing.Point(361, 114);
            this.ButtonMonthClear.Name = "ButtonMonthClear";
            this.ButtonMonthClear.Size = new System.Drawing.Size(81, 22);
            this.ButtonMonthClear.TabIndex = 0;
            this.ButtonMonthClear.Text = "Сбросить";
            this.ButtonMonthClear.Click += new System.EventHandler(this.ButtonMonthClear_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkEditWeek);
            this.groupBox2.Controls.Add(this.ButtonSelectDay);
            this.groupBox2.Controls.Add(this.ButtonDayClear);
            this.groupBox2.Controls.Add(this.weekDaysCheckEdit1);
            this.groupBox2.Location = new System.Drawing.Point(9, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(447, 103);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // checkEditWeek
            // 
            this.checkEditWeek.Location = new System.Drawing.Point(6, 10);
            this.checkEditWeek.Name = "checkEditWeek";
            this.checkEditWeek.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditWeek.Properties.Appearance.Options.UseFont = true;
            this.checkEditWeek.Properties.Caption = "Дни недели";
            this.checkEditWeek.Size = new System.Drawing.Size(148, 19);
            this.checkEditWeek.TabIndex = 3;
            this.checkEditWeek.CheckedChanged += new System.EventHandler(this.checkEditWeek_CheckedChanged);
            // 
            // ButtonSelectDay
            // 
            this.ButtonSelectDay.Location = new System.Drawing.Point(237, 74);
            this.ButtonSelectDay.Name = "ButtonSelectDay";
            this.ButtonSelectDay.Size = new System.Drawing.Size(113, 23);
            this.ButtonSelectDay.TabIndex = 2;
            this.ButtonSelectDay.Text = "Отметить все";
            this.ButtonSelectDay.Click += new System.EventHandler(this.ButtonSelectDay_Click);
            // 
            // ButtonDayClear
            // 
            this.ButtonDayClear.Location = new System.Drawing.Point(361, 74);
            this.ButtonDayClear.Name = "ButtonDayClear";
            this.ButtonDayClear.Size = new System.Drawing.Size(75, 23);
            this.ButtonDayClear.TabIndex = 1;
            this.ButtonDayClear.Text = "Сбросить";
            this.ButtonDayClear.Click += new System.EventHandler(this.ButtonDayClear_Click);
            // 
            // weekDaysCheckEdit1
            // 
            this.weekDaysCheckEdit1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.weekDaysCheckEdit1.Appearance.Options.UseBackColor = true;
            this.weekDaysCheckEdit1.Location = new System.Drawing.Point(6, 35);
            this.weekDaysCheckEdit1.Name = "weekDaysCheckEdit1";
            this.weekDaysCheckEdit1.Size = new System.Drawing.Size(432, 62);
            this.weekDaysCheckEdit1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkEditTime);
            this.groupBox1.Controls.Add(this.textBoxMinute2);
            this.groupBox1.Controls.Add(this.textBoxHour2);
            this.groupBox1.Controls.Add(this.textBoxMinute1);
            this.groupBox1.Controls.Add(this.textBoxHour1);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Location = new System.Drawing.Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 60);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // checkEditTime
            // 
            this.checkEditTime.Location = new System.Drawing.Point(6, 8);
            this.checkEditTime.Name = "checkEditTime";
            this.checkEditTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditTime.Properties.Appearance.Options.UseFont = true;
            this.checkEditTime.Properties.Caption = "Часы и минуты";
            this.checkEditTime.Size = new System.Drawing.Size(167, 19);
            this.checkEditTime.TabIndex = 11;
            this.checkEditTime.CheckedChanged += new System.EventHandler(this.checkEditTime_CheckedChanged);
            // 
            // textBoxMinute2
            // 
            this.textBoxMinute2.Location = new System.Drawing.Point(361, 33);
            this.textBoxMinute2.Name = "textBoxMinute2";
            this.textBoxMinute2.Size = new System.Drawing.Size(42, 20);
            this.textBoxMinute2.TabIndex = 10;
            this.textBoxMinute2.Text = "59";
            this.textBoxMinute2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxHour2
            // 
            this.textBoxHour2.Location = new System.Drawing.Point(283, 33);
            this.textBoxHour2.Name = "textBoxHour2";
            this.textBoxHour2.Size = new System.Drawing.Size(42, 20);
            this.textBoxHour2.TabIndex = 9;
            this.textBoxHour2.Text = "23";
            this.textBoxHour2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxMinute1
            // 
            this.textBoxMinute1.Location = new System.Drawing.Point(151, 33);
            this.textBoxMinute1.Name = "textBoxMinute1";
            this.textBoxMinute1.Size = new System.Drawing.Size(42, 20);
            this.textBoxMinute1.TabIndex = 8;
            this.textBoxMinute1.Text = "00";
            this.textBoxMinute1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxHour1
            // 
            this.textBoxHour1.Location = new System.Drawing.Point(77, 33);
            this.textBoxHour1.Name = "textBoxHour1";
            this.textBoxHour1.Size = new System.Drawing.Size(42, 20);
            this.textBoxHour1.TabIndex = 7;
            this.textBoxHour1.Text = "00";
            this.textBoxHour1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(341, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(8, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "--";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(132, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(8, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "--";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(256, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(18, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "До:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(36, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(18, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "От:";
            // 
            // ButtonOK
            // 
            this.ButtonOK.Location = new System.Drawing.Point(293, 337);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 1;
            this.ButtonOK.Text = "Да";
            this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(376, 337);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 0;
            this.ButtonCancel.Text = "Отмена";
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // FormInterval
            // 
            this.AcceptButton = this.ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(461, 365);
            this.ControlBox = false;
            this.Controls.Add(this.panelControlInterval);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInterval";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Интервалы времени";
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInterval)).EndInit();
            this.panelControlInterval.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWeek.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTime.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlInterval;
        private DevExpress.XtraEditors.SimpleButton ButtonOK;
        private DevExpress.XtraEditors.SimpleButton ButtonCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraScheduler.UI.WeekDaysCheckEdit weekDaysCheckEdit1;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.SimpleButton ButtonOdd;
        private DevExpress.XtraEditors.SimpleButton ButtonNoOdd;
        private DevExpress.XtraEditors.SimpleButton ButtonMonthClear;
        private DevExpress.XtraEditors.SimpleButton ButtonDayClear;
        private DevExpress.XtraEditors.SimpleButton ButtonSelectMonth;
        private DevExpress.XtraEditors.SimpleButton ButtonSelectDay;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEditMonth;
        private DevExpress.XtraEditors.CheckEdit checkEditWeek;
        private System.Windows.Forms.TextBox textBoxMinute2;
        private System.Windows.Forms.TextBox textBoxHour2;
        private System.Windows.Forms.TextBox textBoxMinute1;
        private System.Windows.Forms.TextBox textBoxHour1;
        private DevExpress.XtraEditors.CheckEdit checkEditTime;
    }
}