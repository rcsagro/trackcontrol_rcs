using System;
using System.Drawing;

namespace TrackControl.General
{
  using Properties;

  public sealed partial class Shared
  {
    public static Image BadConnection { get { return Resources.BadConnection; } }
    public static Image DeleteSensor { get { return Resources.Delete_14; } }
    public static Image EditSensor { get { return Resources.Edit_14; } }
    public static Image Networking { get { return Resources.Networking; } }
    public static Image NetworkingTime { get { return Resources.Networking_time; } }
    public static Image NetworkingEvent { get { return Resources.Networking_events; } }
    public static Image NetworkingDistance { get { return Resources.Networking_distance; } }
    public static Image NetworkingDispatchers{ get { return Resources.Networking_dispatchers; } }
    public static Image NetworkingFactory { get { return Resources.Networking_factory; } }
    public static Image PassChange { get { return Resources.PassChange; } }
  }
}
