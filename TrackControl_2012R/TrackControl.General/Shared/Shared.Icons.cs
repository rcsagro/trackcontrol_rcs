﻿using System;
using System.Drawing;


namespace TrackControl.General
{
    using Properties;
    public sealed partial class Shared
    {
        public static Icon DocNew{ get { return Resources.docNew; } }
        public static Icon DocUpdate { get { return Resources.docUpdate; } }
        public static Icon DocTest { get { return Resources.docTest; } }
        public static Icon DocClose { get { return Resources.docClose; } }
    }
}
