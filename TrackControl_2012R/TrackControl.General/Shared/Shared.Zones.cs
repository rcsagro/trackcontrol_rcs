using System;
using System.Drawing;

namespace TrackControl.General
{
  using Properties;

  public sealed partial class Shared
  {
    public static Image DbSave { get { return Resources.DbSave; } }
    public static Image Editor { get { return Resources.Editor; } }
    public static Image MIFNoAccess { get { return Resources.MIFNoAccess; } }
    public static Image MIFSearch { get { return Resources.MIFSearch; } }
    public static Image MIFWarning { get { return Resources.MIFWarning; } }
    public static Image Zone { get { return Resources.Zone; } }
    public static Image ZoneCross { get { return Resources.ZoneCross; } }
    public static Image ZoneMIF { get { return Resources.ZoneMIF; } }
    public static Image ZonePlus { get { return Resources.ZonePlus; } }
    public static Image ZonePoints { get { return Resources.ZonePoints; } }
    public static Image ZonePoligon { get { return Resources.ZonePolygon; } }
  }
}
