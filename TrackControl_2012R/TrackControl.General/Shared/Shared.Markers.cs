using System;
using System.Drawing;

namespace TrackControl.General
{
    using Properties;

    public sealed partial class Shared
    {
        public static Image ForTrack
        {
            get
            {
                return Resources.ForTrack;
            }
        }

        public static Image Fuel
        {
            get
            {
                return Resources.Fuel;
            }
        }

        public static Image GEvent
        {
            get
            {
                return Resources.GEvent;
            }
        }

        public static Image GMove
        {
            get
            {
                return Resources.GMove;
            }
        }

        public static Image GPark
        {
            get
            {
                return Resources.GPark;
            }
        }

        public static Image GParkLong
        {
            get
            {
                return Resources.GRest;
            }
        }

        public static Image GStop
        {
            get
            {
                return Resources.GStop;
            }
        }

        public static Image GStroke
        {
            get
            {
                return Resources.GStroke;
            }
        }

        public static Image GStrokeGreen
        {
            get
            {
                return Resources.GStrokeG;
            }
        }

        public static Image GStrokeRed
        {
            get
            {
                return Resources.GStrokeR;
            }
        }

        public static Image GWarning
        {
            get
            {
                return Resources.GWarning;
            }
        }

        public static Image GActive
        {
            get
            {
                return Resources.ArrowDownRedGloss;
            }
        }

        public static Image Pin
        {
            get
            {
                return Resources.Pin;
            }
        }

        public static Image Finish
        {
            get
            {
                return Resources.flag_finish;
            }
        }

        public static Image BallGreen
        {
            get
            {
                return Resources.MarkerBallGreen;
            }
        }

        public static Image DistancePoint
        {
            get
            {
                return Resources.status_offline;
            }
        }

        public static Image MdExecGo
        {
            get
            {
                return Resources.md_exec_go;
            }
        }

        public static Image MdExecStop
        {
            get
            {
                return Resources.md_exec_stop;
            }
        }

        public static Image MdWaitGo
        {
            get
            {
                return Resources.md_waiting_go;
            }
        }

        public static Image MdWaitStop
        {
            get
            {
                return Resources.md_waiting_stop;
            }
        }

        public static Image MdRepareGo
        {
            get
            {
                return Resources.md_repare_go;
            }
        }

        public static Image MdRepareStop
        {
            get
            {
                return Resources.md_repare_stop;
            }
        }

        public static Image MdDinnerGo
        {
            get
            {
                return Resources.md_dinner_go;
            }
        }

        public static Image MdDinnerStop
        {
            get
            {
                return Resources.md_dinner_stop;
            }
        }
    }
}
