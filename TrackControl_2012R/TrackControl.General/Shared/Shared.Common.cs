using System;
using System.Drawing;

namespace TrackControl.General
{
  using Properties;

  public sealed partial class Shared
  {
    public static Image AutoScale { get { return Resources.AutoScale; } }
    public static Image BalloonMinus { get { return Resources.BalloonMines; } }
    public static Image BalloonPlus { get { return Resources.BalloonPlus; } }
    public static Image Cancel { get { return Resources.Cancel; } }
    public static Image Cards { get { return Resources.Cards; } }
    public static Image Carousel { get { return Resources.Carousel; } }
    public static Image Calculator { get { return Resources.calculator; } }
    public static Image Chart { get { return Resources.Chart; } }
    public static Image ChartLine { get { return Resources.ChartLine; } }
    public static Image ChartCircle { get { return Resources.ChartCircle; } }
    public static Image Cross { get { return Resources.Cross; } }
    public static Image Download { get { return Resources.Download; } }
    public static Image DeleteChecked { get { return Resources.DeleteChecked; } }
    public static Image Exit { get { return Resources.Exit; } }
    public static Image Eye { get { return Resources.Eye; } }
    public static Image Folder { get { return Resources.Folder; } }
    public static Image FolderMinus { get { return Resources.FolderMinus; } }
    public static Image FolderOpen { get { return Resources.FolderOpen; } }
    public static Image FolderPlus { get { return Resources.FolderPlus; } }
    public static Image FolderStack { get { return Resources.FoldersStack; } }
    public static Image FunnelClear { get { return Resources.FunnelClear; } }
    public static Image Gauge { get { return Resources.Gauge; } }
    public static Image Help { get { return Resources.Help; } }
    public static Image Info { get { return Resources.Info; } }
    public static Image LayoutPencil { get { return Resources.LayoutPencil; } }
    public static Image Logo { get { return Resources.Logo; } }
    public static Image Megaphone { get { return Resources.Megaphone; } }
    public static Image PlusLittle { get { return Resources.Plus; } }
    public static Image Printer { get { return Resources.Printer; } }
    public static Image Reset { get { return Resources.Reset; } }
    public static Image Return { get { return Resources.Return; } }
    public static Image Right { get { return Resources.Right; } }
    public static Image Save { get { return Resources.Save; } }
    public static Image Settings { get { return Resources.Settings; } }
    public static Image Start { get { return Resources.Start; } }
    public static Image Stop { get { return Resources.Stop; } }
    public static Image Switch { get { return Resources.Switch; } }
    public static Image Table { get { return Resources.Table; } }
    public static Image TableExport { get { return Resources.TableExport; } }
    public static Image TickBig { get { return Resources.Tick_64; } }
    public static Image TickLittle { get { return Resources.Tick_16; } }
    public static Image ToHtml { get { return Resources.ToHtml; } }
    public static Image ToPdf { get { return Resources.ToPdf; } }
    public static Image ToXls { get { return Resources.ToXls; } }
    public static Image Up { get { return Resources.Up; } }
    public static Image User { get { return Resources.User; } }
    public static Image WarningBig { get { return Resources.Warning_64; } }
    public static Image WarningLittle { get { return Resources.Warning_16; } }
    public static Image Expand { get { return Resources.Expand; } }
    public static Image Collapce { get { return Resources.Collapce; } }
    public static Image StartMain { get { return Resources.StartMain; } }
    public static Image StopMain { get { return Resources.StopMain; } }
    public static Image DriverRFID { get { return Resources.driver_rfid; } }
    public static Image Synchro{ get { return Resources.Synchro; } }
    public static Image Mail { get { return Resources.mail_medium; } }
    public static Image Log { get { return Resources.Log; } }
  }
}
