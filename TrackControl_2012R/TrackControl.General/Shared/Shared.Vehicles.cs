using System;
using System.Drawing;

namespace TrackControl.General
{
  using Properties;

  public sealed partial class Shared
  {
    public static Image Car { get { return Resources.Car; } }
    public static Image CarPencil { get { return Resources.CarPencil; } }
    public static Image DriverPencil { get { return Resources.DriverPencil; } }
    public static Image NoUser { get { return Resources.NoUser; } }
    public static Image Truck { get { return Resources.Truck; } }
    public static Image UserMinus { get { return Resources.UserMinus; } }
    public static Image UserPlus { get { return Resources.UserPlus; } }
    public static Image Tractor64 { get { return Resources.tractor64; } }
    public static Image FieldsProcessing { get { return Resources.field_report; } }
    public static Image Harvester { get { return Resources.harvester64; } }
    public static Image SpeedControl { get { return Resources.speedometer_1; } }
  }
}
