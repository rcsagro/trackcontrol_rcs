using System;
using System.Drawing;

namespace TrackControl.General
{
  using Properties;

  public sealed partial class Shared
  {
    public static Image AllStops { get { return Resources.AllStops; } }
    public static Image Area { get { return Resources.Area; } }
    public static Image Distance { get { return Resources.Distance; } }
    public static Image Hand { get { return Resources.Hand; } }
    public static Image InfoBalloon { get { return Resources.InfoBalloon; } }
    public static Image Map { get { return Resources.Map; } }
    public static Image ReloadMap { get { return Resources.ReloadMap; } }
    public static Image ScreenShot { get { return Resources.ScreenShot; } }
    public static Image ZoomFit { get { return  Resources.ZoomFit; } }
    public static Image ZoomIn { get { return Resources.ZoomIn; } }
    public static Image ZoomLine { get { return Resources.ZoomLine; } }
    public static Image ZoomOut { get { return Resources.ZoomOut; } }
    public static Image ArrowLine0 { get { return Resources.trace_icon244; } }
    public static Image ArrowLine45 { get { return Resources.trace_icon245; } }
    public static Image ArrowLine90 { get { return Resources.trace_icon241; } }
    public static Image ArrowLine135 { get { return Resources.trace_icon247; } }
    public static Image ArrowLine180 { get { return Resources.trace_icon242; } }
    public static Image ArrowLine225 { get { return Resources.trace_icon246; } }
    public static Image ArrowLine270 { get { return Resources.trace_icon243; } }
    public static Image ArrowLine315 { get { return Resources.trace_icon248; } }
    public static Image ShowStop { get { return Resources.ShowStop; } }
  }
}
