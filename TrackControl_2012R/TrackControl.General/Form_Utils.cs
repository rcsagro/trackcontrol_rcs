using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms; 

namespace TrackControl.General
{
    public static class Form_Utils
    {
        //���������� ����� �������� ��������� �� ������� ����
        public class DrawEventArgs : EventArgs
        {
            private DateTime _startDate;
            private DateTime _endDate;
            private int _iMobitel;
            private List<int> listActiveZones = null;

            public DrawEventArgs(DateTime startDate, DateTime endDate, int iMobitel)
            {
                _startDate = startDate;
                _endDate = endDate;
                _iMobitel = iMobitel;
            }

            public DrawEventArgs(DateTime startDate, DateTime endDate, int iMobitel, List<int> listZones)
            {
                _startDate = startDate;
                _endDate = endDate;
                _iMobitel = iMobitel;
                listActiveZones = listZones;
            }

            public List<int> getListZones
            {
                get { return listActiveZones; }
            }

            public DateTime StartDate
            {
                get { return _startDate; }
                set { _startDate = value; }
            }
            public DateTime EndDate
            {
                get { return _endDate; }
                set { _endDate = value; }
            }
            public int Mobitel
            {
                get { return _iMobitel; }
                set { _iMobitel = value; }
            }
        }
        public delegate void DrawMapTeletreck(object sender, DrawEventArgs e);
        public static DrawMapTeletreck DrawMapEventHandler;
        //��������� �� ����� � ������ �� ��� ���� ��
        public static bool IsLoaded(string sFormName)
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.GetType().ToString() == sFormName)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
