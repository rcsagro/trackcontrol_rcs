﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General.DatabaseDriver;
using System.Diagnostics.Contracts;

namespace TrackControl.General.DAL
{
    public class TrackControlQuery
    {
        private static int TypeDataBaseForUsing = DriverDb.TypeDataBaseUsing;
        private const int MySqlUse = 0;
        private const int MssqlUse = 1;
        private const string SqlVehicleIdent = "CONCAT(IFNULL(vehicle.NumberPlate,''),' | ' , IFNULL(vehicle.CarModel,''),' | ' , IFNULL(vehicle.MakeCar,''))";
        private const string msSqlVehicleIdent = "cast(ISNULL(vehicle.NumberPlate,'') AS VARCHAR) + ' | ' + cast(ISNULL(vehicle.CarModel,'') AS VARCHAR) + ' | ' + cast(ISNULL(vehicle.MakeCar,'') AS varchar)";
        private static string selectDataGps = string.Format (@"SELECT `datagps`.`DataGps_ID` AS `DataGps_ID`
                         , `datagps`.`Mobitel_ID` AS `Mobitel_ID`
                         , (`datagps`.`Latitude` / 600000.00000) AS `Lat`
                         , (`datagps`.`Longitude` / 600000.00000) AS `Lon`
                         , `datagps`.`Altitude` AS `Altitude`
                         , from_unixtime(`datagps`.`UnixTime`) AS `time`
                         , (`datagps`.`Speed` * 1.852) AS `speed`
                         , ((`datagps`.`Direction` * 360) / 255) AS `direction`
                         , `datagps`.`Valid` AS `Valid`
                         , (((((((`datagps`.`Sensor8` + (`datagps`.`Sensor7` << 8)) + (`datagps`.`Sensor6` << 16)) + (`datagps`.`Sensor5` << 24)) + (`datagps`.`Sensor4` << 32)) + (`datagps`.`Sensor3` << 40)) + (`datagps`.`Sensor2` << 48)) + (`datagps`.`Sensor1` << 56)) AS `sensor`
                         , `datagps`.`Events` AS `Events`
                         , `datagps`.`LogID` AS `LogID`");
        private static string msSelectDataGpsCommon = @"SELECT {0} datagps.DataGps_ID AS DataGps_ID
                         , datagps.Mobitel_ID AS Mobitel_ID
                         , datagps.Latitude as Lat
                         , datagps.Longitude as Lon
                         , datagps.Altitude AS Altitude
                         , dbo.from_unixtime(datagps.UnixTime) AS time
                         , datagps.Speed
                         , ((datagps.Direction * 360) / 255) AS direction
                         , datagps.Valid
                         , datagps.Sensor8, datagps.Sensor7, datagps.Sensor6, datagps.Sensor5, datagps.Sensor4, datagps.Sensor3, datagps.Sensor2, datagps.Sensor1, datagps.Events AS Events
                         , datagps.LogID AS LogID";
        private static string msSelectDataGpsTop1 = string.Format(msSelectDataGpsCommon," TOP 1");

        private static string msSelectDataGps = string.Format(msSelectDataGpsCommon, "");

        private static string selectDataGps64 = @"SELECT  DataGps_ID, datagps64.Mobitel_ID,
                                   ((Latitude * 1.000) / 10000000) AS Lat,
                                   ((Longitude * 1.000) / 10000000) AS Lng,
                                    Acceleration , FROM_UNIXTIME(UnixTime) AS `Time` , Speed /10 AS Speed, Valid,
                                    Sensors,
                                    Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm,SrvPacketID";
        private static string msSelectDataGps64 = @"SELECT DataGps_ID, datagps64.Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Acceleration , dbo.FROM_UNIXTIME(UnixTime) AS Time  , Speed AS Speed, Valid ,
                                    Sensors,
                                    Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm,SrvPacketID";
        private static string msSelectDataGps64Top = @"SELECT {0}  DataGps_ID, datagps64.Mobitel_ID,
                                    Latitude AS Lng, Longitude AS Lon,
                                    Acceleration , dbo.FROM_UNIXTIME(UnixTime) AS Time  , Speed AS Speed, Valid ,
                                    Sensors,
                                    Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm,SrvPacketID";
        private const string GET_ONLINE_DATA_COMMON = @"
                            SELECT
                            DataGps_ID,
                            LogID,
                            Mobitel_ID,
                            FROM_UNIXTIME(UnixTime) AS `Time`,
                            ((Latitude * 1.000) / 600000) AS Lat,
                            ((Longitude * 1.000) / 600000) AS Lon,
                            (Speed * 1.852) AS Speed,
                            `Valid`,
                            (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + (Sensor2 << 48) + (Sensor1 << 56)) AS Sensor,
                            Events FROM online ";
        private const string GET_ONLINE_DATA_COMMON_64 = @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                            ((Latitude * 1.000) / 10000000) AS Lat,
                            ((Longitude * 1.000) / 10000000) AS Lng,
                            Acceleration , FROM_UNIXTIME(UnixTime) AS `Time` , Speed/10 AS Speed, Valid AS Valid,
                            Sensors,
                            Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm
                            FROM online64 ";
        private const string msGET_ONLINE_DATA_COMMON_TOP = @"
                            SELECT {0}
                            DataGps_ID,
                            LogID,
                            Mobitel_ID,
                            dbo.FROM_UNIXTIME(UnixTime) AS Time,
                            Latitude AS Lat, Longitude AS Lon,
                            Speed,
                            Valid,
                            Sensor8,Sensor7,Sensor6,Sensor5,Sensor4,Sensor3,Sensor2,Sensor1,
                            Events
                            FROM online";
        private const string msGET_ONLINE_DATA_COMMON_TOP_64 = @"
                            SELECT {0}
                            DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                            Latitude AS Lat, Longitude AS Lon,
                            Acceleration , dbo.FROM_UNIXTIME(UnixTime) AS Time ,  Speed/10 AS Speed, Valid AS Valid,
                            Sensors,
                            Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm
                            FROM online64";
        private readonly static string msGET_ONLINE_DATA_COMMON = string.Format (msGET_ONLINE_DATA_COMMON_TOP, "");
        private readonly static string msGET_ONLINE_DATA_COMMON_64 = string.Format(msGET_ONLINE_DATA_COMMON_TOP_64 , "");
        private const string GET_ONLINE_DATA_MOBITEL = GET_ONLINE_DATA_COMMON + @" WHERE Mobitel_ID = {0} AND LogID > {1} AND Speed < 108 ORDER BY LogID ASC";
        private const string GET_ONLINE_DATA_MOBITEL_64 = GET_ONLINE_DATA_COMMON_64 + @" WHERE Mobitel_ID = {0} AND LogID > {1} ORDER BY LogID ASC";
        private const string GET_ONLINE_DATA_MOBITEL_LAST = GET_ONLINE_DATA_COMMON + @" WHERE Mobitel_ID = {0} AND Speed < 108 ORDER BY LogID DESC LIMIT 1";
        private const string GET_ONLINE_DATA_MOBITEL_LAST_64 = GET_ONLINE_DATA_COMMON_64 + @" WHERE Mobitel_ID = {0} ORDER BY LogID DESC LIMIT 1";
        private const string GET_ONLINE_DATA = GET_ONLINE_DATA_COMMON + @" WHERE DataGps_ID > {0} AND UnixTime < UNIX_TIMESTAMP(?FUTime) AND Speed < 108 ORDER BY DataGps_ID ASC";

        private readonly static string msGET_ONLINE_DATA_MOBITEL_LAST = string.Format (msGET_ONLINE_DATA_COMMON_TOP, " TOP 1 ") + @" WHERE Mobitel_ID = {0} AND Speed < 108 ORDER BY UnixTime ASC";
        private readonly static string msGET_ONLINE_DATA_MOBITEL_LAST_64 = string.Format(msGET_ONLINE_DATA_COMMON_TOP_64, " TOP 1 ") + @" WHERE Mobitel_ID = {0} ORDER BY UnixTime ASC";
        private readonly static string msGET_ONLINE_DATA_MOBITEL = msGET_ONLINE_DATA_COMMON + @" WHERE Mobitel_ID = {0} AND LogID > {1} AND Speed < 108 ORDER BY UnixTime ASC";
        private readonly static string msGET_ONLINE_DATA_MOBITEL_64 = msGET_ONLINE_DATA_COMMON_64 + @" WHERE Mobitel_ID = {0} AND LogID >= {1} ORDER BY UnixTime ASC";
        private readonly static string msGET_ONLINE_DATA = msGET_ONLINE_DATA_COMMON + @" WHERE DataGps_ID > {0} AND UnixTime < dbo.UNIX_TIMESTAMPS(@FUTime) AND Speed < 108 ORDER BY DataGps_ID ASC";

        private const string INSERT_ONE = @"INSERT INTO `vehicle` (`MakeCar`, `CarModel`, `NumberPlate`, `Team_id`, `setting_id`, `driver_id`, `Mobitel_id`,`OutLinkId`,`FuelWayLiter`,`FuelMotorLiter`,`Category_id`, `Identifier`,`Category_id2`, `PcbVersionId`, `Category_id3`, `Category_id4`) VALUES (@CarMaker, @CarModel, @RegNumber, @GroupId, @SettingsId, @DriverId, @MobitelId,@OutLinkId,@FuelWayLiter,@FuelMotorLiter,@Category_id, @Identifier,@Category_id2, @PcbVersionId, @Category_id3, @Category_id4); SELECT LAST_INSERT_ID()";
        private const string msINSERT_ONE = @"INSERT INTO vehicle (MakeCar, CarModel, NumberPlate, Team_id, setting_id, driver_id, Mobitel_id, OutLinkId, FuelWayLiter,FuelMotorLiter,Category_id,Identifier,Category_id2, PcbVersionId, Category_id3, Category_id4) VALUES (@CarMaker, @CarModel, @RegNumber, @GroupId, @SettingsId, @DriverId, @MobitelId, @OutLinkId, @FuelWayLiter,@FuelMotorLiter,@Category_id,@Identifier,@Category_id2, @PcbVersionId, @Category_id3, @Category_id4); SELECT max(id) FROM vehicle";
        private const string UPDATE_ONE = @"UPDATE `vehicle` SET `MakeCar` = @CarMaker, `NumberPlate` = @RegNumber, `Team_id` = @GroupId, `CarModel` = @CarModel, `setting_id` = @SettingsId, `driver_id` = @DriverId,`OutLinkId` = @OutLinkId,FuelWayLiter = @FuelWayLiter,FuelMotorLiter = @FuelMotorLiter, `Category_id` = @Category_id, `Identifier` = @Identifier, `Category_id2` = @Category_id2, `PcbVersionId` = @PcbVersionId, `Category_id3` = @Category_id3, `Category_id4` = @Category_id4 WHERE `id` = @Id";
        private const string msUPDATE_ONE = @"UPDATE vehicle SET MakeCar = @CarMaker, NumberPlate = @RegNumber, Team_id = @GroupId, CarModel = @CarModel, setting_id = @SettingsId, driver_id = @DriverId, OutLinkId = @OutLinkId, FuelWayLiter = @FuelWayLiter,FuelMotorLiter = @FuelMotorLiter, Category_id = @Category_id, Identifier = @Identifier, Category_id2 = @Category_id2, PcbVersionId = @PcbVersionId, Category_id3 = @Category_id3, Category_id4 = @Category_id4 WHERE id = @Id";
        private const string SELECT_ALL = @"SELECT * FROM `team`";
        private const string msSELECT_ALL = @"SELECT * FROM team";
        private const string _INSERT_ONE =
            @"INSERT INTO `team` (`Name`, `Descr`, `Setting_id`, `OutLinkId`, `FuelWayKmGrp`, `FuelMotoHrGrp`) VALUES (@Name, @Description, 1, @OutLinkId, @FuelWayKmGrp, @FuelMotoHrGrp); SELECT LAST_INSERT_ID()";
        private const string _msINSERT_ONE =
            @"INSERT INTO team (Name, Descr, Setting_id, OutLinkId, FuelWayKmGrp, FuelMotoHrGrp) VALUES (@Name, @Description, 1, @OutLinkId, @FuelWayKmGrp, @FuelMotoHrGrp); SELECT max(id) FROM team";
        private const string _UPDATE_ONE =
            @"UPDATE `team` SET `Name`=@Name, `Descr`=@Description , `OutLinkId`=@OutLinkId, `FuelWayKmGrp`=@FuelWayKmGrp, `FuelMotoHrGrp`=@FuelMotoHrGrp WHERE `id`=@Id";
        private const string _msUPDATE_ONE =
            @"UPDATE team SET Name=@Name, Descr=@Description, OutLinkId=@OutLinkId, FuelWayKmGrp=@FuelWayKmGrp, FuelMotoHrGrp=@FuelMotoHrGrp WHERE id=@Id";
        private const string DELETE_ONE = @"DELETE FROM `team` WHERE `id`=@Id";
        private const string msDELETE_ONE = @"DELETE FROM team WHERE id=@Id";
        private const string SELECT_ALL_ = @"SELECT `Id`,`Title`,`Description` FROM `zonesgroup`";
        private const string INSERT_ONE_ = @"INSERT INTO `zonesgroup` (`Title`, `Description`,`OutLinkId`) VALUES (@Title, @Description,@OutLinkId); SELECT LAST_INSERT_ID()";
        private const string UPDATE_ONE_ = @"UPDATE `zonesgroup` SET `Title`=@Title, `Description`=@Description,`OutLinkId`=@OutLinkId WHERE `Id`=@Id";
        private const string DELETE_ONE_ = @"DELETE FROM `zonesgroup` WHERE `Id`=@Id";
        private const string msSELECT_ALL_ = @"SELECT Id,Title,Description FROM zonesgroup";
        private const string msINSERT_ONE_ = @"INSERT INTO zonesgroup (Title, Description,OutLinkId) VALUES (@Title, @Description,@OutLinkId); SELECT MAX(Id) FROM zonesgroup";
        private const string msUPDATE_ONE_ = @"UPDATE zonesgroup SET Title=@Title, Description=@Description,OutLinkId=@OutLinkId WHERE Id=@Id";
        private const string msDELETE_ONE_ = @"DELETE FROM zonesgroup WHERE Id=@Id";
        private const string SELECT_POINTS = @"SELECT ROUND((`Latitude` / 600000.00000), 6) AS `Lat`, ROUND((`Longitude` / 600000.00000), 6) AS `Lng` FROM `points` WHERE `Zone_ID` = @ZoneId";
        private const string msSELECT_POINTS = @"SELECT Latitude,Longitude FROM points WHERE Zone_ID = @ZoneId ORDER BY Point_ID";
        const string xSELECT_ALL = @"SELECT * FROM `vehicle_category` ORDER BY Name";
        const string xSELECT_ONE = @"SELECT * FROM `vehicle_category` WHERE `Id`={0}";
        const string xINSERT_ONE = @"INSERT INTO vehicle_category (`Name`, `Tonnage`,FuelNormMoving,FuelNormParking) VALUES (?Name, ?Tonnage,?FuelNormMoving,?FuelNormParking);";
        const string xUPDATE_ONE = @"UPDATE vehicle_category SET `Name`=?Name, `Tonnage`=?Tonnage,FuelNormMoving = ?FuelNormMoving,FuelNormParking=?FuelNormParking  WHERE `Id`=?Id";
        const string xDELETE_ONE = @"DELETE FROM vehicle_category WHERE `Id`=?Id";
        const string xCOUNT_VEHICLES_WITH_CATEGORY = @"SELECT count(vehicle.id) FROM   vehicle WHERE vehicle.Category_id = ?Id GROUP BY  vehicle.Category_id";
        const string xSELECT_ALL2 = @"SELECT * FROM `vehicle_category2` ORDER BY Name";
        const string xSELECT_ONE2 = @"SELECT * FROM `vehicle_category2` WHERE `Id`={0}";
        const string xINSERT_ONE2 = @"INSERT INTO vehicle_category2 (`Name`) VALUES (?Name);";
        const string xUPDATE_ONE2 = @"UPDATE vehicle_category2 SET `Name`=?Name  WHERE `Id`=?Id";
        const string xDELETE_ONE2 = @"DELETE FROM vehicle_category2 WHERE `Id`=?Id";
        const string xSELECT_ALL3 = @"SELECT * FROM `vehicle_category3` ORDER BY Name";
        const string xSELECT_ONE3 = @"SELECT * FROM `vehicle_category3` WHERE `Id`={0}";
        const string xINSERT_ONE3 = @"INSERT INTO vehicle_category3 (`Name`) VALUES (?Name);";
        const string xUPDATE_ONE3 = @"UPDATE vehicle_category3 SET `Name`=?Name  WHERE `Id`=?Id";
        const string xDELETE_ONE3 = @"DELETE FROM vehicle_category3 WHERE `Id`=?Id";
        const string xSELECT_ALL4 = @"SELECT * FROM `vehicle_category4` ORDER BY Name";
        const string xSELECT_ONE4 = @"SELECT * FROM `vehicle_category4` WHERE `Id`={0}";
        const string xINSERT_ONE4 = @"INSERT INTO vehicle_category4 (`Name`) VALUES (?Name);";
        const string xUPDATE_ONE4 = @"UPDATE vehicle_category4 SET `Name`=?Name  WHERE `Id`=?Id";
        const string xDELETE_ONE4 = @"DELETE FROM vehicle_category4 WHERE `Id`=?Id";
        const string xCOUNT_VEHICLES_WITH_CATEGORY2 = @"SELECT count(vehicle.id) FROM   vehicle WHERE vehicle.Category_id2 = ?Id GROUP BY  vehicle.Category_id2";
        const string xCOUNT_VEHICLES_WITH_CATEGORY3 = @"SELECT count(vehicle.id) FROM   vehicle WHERE vehicle.Category_id3 = ?Id GROUP BY  vehicle.Category_id3";
        const string xCOUNT_VEHICLES_WITH_CATEGORY4 = @"SELECT count(vehicle.id) FROM   vehicle WHERE vehicle.Category_id4 = ?Id GROUP BY  vehicle.Category_id4";
        const string xmsSELECT_ALL = @"SELECT * FROM vehicle_category ORDER BY Name";
        const string xmsSELECT_ONE = @"SELECT * FROM vehicle_category WHERE Id={0}";
        const string xmsINSERT_ONE = @"INSERT INTO vehicle_category (Name, Tonnage,FuelNormMoving,FuelNormParking) VALUES (@Name, @Tonnage,@FuelNormMoving,@FuelNormParking);";
        const string xmsUPDATE_ONE = @"UPDATE vehicle_category SET Name=@Name, Tonnage=@Tonnage,FuelNormMoving = @FuelNormMoving,FuelNormParking=@FuelNormParking  WHERE Id=@Id";
        const string xmsDELETE_ONE = @"DELETE FROM vehicle_category WHERE Id=@Id";
        const string xmsCOUNT_VEHICLES_WITH_CATEGORY = @"SELECT count(vehicle.id) FROM vehicle WHERE vehicle.Category_id = @Id GROUP BY vehicle.Category_id";
        const string xmsSELECT_ALL2 = @"SELECT * FROM vehicle_category2 ORDER BY Name";
        const string xmsSELECT_ALL3 = @"SELECT * FROM vehicle_category3 ORDER BY Name";
        const string xmsSELECT_ALL4 = @"SELECT * FROM vehicle_category4 ORDER BY Name";
        const string xmsSELECT_ONE2 = @"SELECT * FROM vehicle_category2 WHERE Id={0}";
        const string xmsINSERT_ONE2 = @"INSERT INTO vehicle_category2 (Name) VALUES (@Name);";
        const string xmsUPDATE_ONE2 = @"UPDATE vehicle_category2 SET Name=@Name  WHERE Id=@Id";
        const string xmsDELETE_ONE2 = @"DELETE FROM vehicle_category2 WHERE Id=@Id";
        const string xmsSELECT_ONE3 = @"SELECT * FROM vehicle_category3 WHERE Id={0}";
        const string xmsINSERT_ONE3 = @"INSERT INTO vehicle_category3 (Name) VALUES (@Name);";
        const string xmsUPDATE_ONE3 = @"UPDATE vehicle_category3 SET Name=@Name  WHERE Id=@Id";
        const string xmsDELETE_ONE3 = @"DELETE FROM vehicle_category3 WHERE Id=@Id";
        const string xmsSELECT_ONE4 = @"SELECT * FROM vehicle_category4 WHERE Id={0}";
        const string xmsINSERT_ONE4 = @"INSERT INTO vehicle_category4 (Name) VALUES (@Name);";
        const string xmsUPDATE_ONE4 = @"UPDATE vehicle_category4 SET Name=@Name  WHERE Id=@Id";
        const string xmsDELETE_ONE4 = @"DELETE FROM vehicle_category4 WHERE Id=@Id";
        const string xmsCOUNT_VEHICLES_WITH_CATEGORY2 = @"SELECT count(vehicle.id) FROM vehicle WHERE vehicle.Category_id2 = @Id GROUP BY  vehicle.Category_id2";
        const string xmsCOUNT_VEHICLES_WITH_CATEGORY3 = @"SELECT count(vehicle.id) FROM vehicle WHERE vehicle.Category_id3 = @Id GROUP BY  vehicle.Category_id3";
        const string xmsCOUNT_VEHICLES_WITH_CATEGORY4 = @"SELECT count(vehicle.id) FROM vehicle WHERE vehicle.Category_id4 = @Id GROUP BY  vehicle.Category_id4";
        private const string mySqlDriverIdent = "CONCAT(IFNULL(driver.Family,''), ' ', IFNULL(driver.Name,''))";
        private const string msSqlDriverIdent = "(ISNULL(driver.Family,'') + ' ' + ISNULL(driver.Name,''))";
        
        public static string VehicleIdent {
            get {
                switch (TypeDataBaseForUsing) {
                case MySqlUse:
                    return SqlVehicleIdent;

                case MssqlUse:
                    return msSqlVehicleIdent;
                }

                return "";
            }
        }


        public static string SqlDriverIdent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return mySqlDriverIdent;

                    case MssqlUse:
                        return msSqlDriverIdent;
                }

                return "";
            }
        }

        public static string SelDataGps {
            get {
                switch (TypeDataBaseForUsing) {
                case MySqlUse:
                    return selectDataGps;

                case MssqlUse:
                    return msSelectDataGps;
                }

                return "";
            }
        }

                public static string SelDataGps64 {
            get {
                switch (TypeDataBaseForUsing) {
                case MySqlUse:
                    return selectDataGps64;

                case MssqlUse:
                    return msSelectDataGps64;
                }

                return "";
            }
        }

        public class DataviewTableAdapter
        {
            public static string DatagpsSelectDinParameters {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,FROM_UNIXTIME(UnixTime) AS `time`
                                    FROM datagps FORCE INDEX(Index_3)
                                    WHERE (UnixTime BETWEEN UNIX_TIMESTAMP({1}Begin) AND UNIX_TIMESTAMP({1}End)) AND                                                                                                                                
                                    (Valid = 1) {0} ORDER BY UnixTime";

                    case MssqlUse:
                        return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,dbo.FROM_UNIXTIME(UnixTime) AS [time]
                                    FROM datagps WITH (INDEX (Index_3)) 
                                    WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS({1}Begin) AND dbo.UNIX_TIMESTAMPS({1}End)) AND
                                    (Valid = 1) {0} ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string DatagpsSelectRadiusParameters
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,FROM_UNIXTIME(UnixTime) AS `time`,
                                    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + (Sensor2 << 48) + (Sensor1 << 56)) AS sensors
                                    FROM datagps FORCE INDEX(Index_3)
                                    WHERE (UnixTime BETWEEN UNIX_TIMESTAMP({1}Begin) AND UNIX_TIMESTAMP({1}End)) AND                                                                                                                                
                                    (Valid = 1) {0} ORDER BY UnixTime";

                        case MssqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,dbo.FROM_UNIXTIME(UnixTime) AS [time],
                                    (Sensor8 + (Sensor7 * 256) + (Sensor6 *  65536) + (cast(Sensor5 AS BIGINT) * 16777216) +
                                    (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensors
                                    FROM datagps WITH (INDEX (Index_3)) 
                                    WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS({1}Begin) AND dbo.UNIX_TIMESTAMPS({1}End)) AND
                                    (Valid = 1) {0} ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string DatagpsSelectDinParameters64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 10000000) AS Lat, ((Longitude * 1.0000) / 10000000) AS Lon,FROM_UNIXTIME(UnixTime) AS `time`
                                    FROM datagps64 FORCE INDEX(Index_3)
                                    WHERE (UnixTime BETWEEN UNIX_TIMESTAMP({1}Begin) AND UNIX_TIMESTAMP({1}End)) AND                                                                                                                                
                                    (Valid = 1) {0} ORDER BY UnixTime";

                        case MssqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 10000000) AS Lat, ((Longitude * 1.0000) / 10000000) AS Lon,dbo.FROM_UNIXTIME(UnixTime) AS [time]
                                    FROM datagps64 WITH (INDEX (Index_3_64)) 
                                    WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS({1}Begin) AND dbo.UNIX_TIMESTAMPS({1}End)) AND
                                    (Valid = 1) {0} ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string DatagpsSelectRadiusParameters64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 10000000) AS Lat, ((Longitude * 1.0000) / 10000000) AS Lon,FROM_UNIXTIME(UnixTime) AS `time`, Sensors AS sensor
                                    FROM datagps64 FORCE INDEX(Index_3)
                                    WHERE (UnixTime BETWEEN UNIX_TIMESTAMP({1}Begin) AND UNIX_TIMESTAMP({1}End)) AND                                                                                                                                
                                    (Valid = 1) {0} ORDER BY UnixTime";

                        case MssqlUse:
                            return @"SELECT DataGps_ID, Mobitel_ID,
                                    ((Latitude * 1.0000) / 10000000) AS Lat, ((Longitude * 1.0000) / 10000000) AS Lon,dbo.FROM_UNIXTIME(UnixTime) AS [time], Sensors AS sensor
                                    FROM datagps64 WITH (INDEX (Index_3_64)) 
                                    WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS({1}Begin) AND dbo.UNIX_TIMESTAMPS({1}End)) AND
                                    (Valid = 1) {0} ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string CmdHistory {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT DISTINCT CAST(FROM_UNIXTIME(t1.unixtime) AS DATE) AS aDay
                                    FROM (SELECT unixtime FROM datagps FORCE INDEX(Index_3) 
                                    WHERE (Mobitel_id = ?m_id) AND (unixtime > (UNIX_TIMESTAMP( SUBDATE((SELECT FROM_UNIXTIME(unixtime) 
                                    FROM datagps 
                                    WHERE (mobitel_id = ?m_id) AND (valid = 1) 
                                    ORDER by logid DESC LIMIT 1), INTERVAL {0} DAY)))) AND (Valid = 1)) t1 ORDER BY aDay DESC";

                    case MssqlUse:
                        return "SELECT d.unixtime FROM datagps d WHERE d.Mobitel_ID = {0} AND Valid = 1 AND d.UnixTime > (SELECT TOP 1 d.UnixTime - {1} FROM datagps d WHERE d.Mobitel_ID = {0} AND d.Valid = 1 ORDER BY LogID DESC)";
                    }

                    return "";
                }
            }

            public static string CmdHistory64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DISTINCT CAST(FROM_UNIXTIME(t1.unixtime) AS DATE) AS aDay
                                    FROM (SELECT unixtime FROM datagps64 FORCE INDEX(Index_3_64) 
                                    WHERE (Mobitel_id = ?m_id) AND (unixtime > (UNIX_TIMESTAMP( SUBDATE((SELECT FROM_UNIXTIME(unixtime) 
                                    FROM datagps64 
                                    WHERE (mobitel_id = ?m_id) AND (Valid = 1) 
                                    ORDER by logid DESC LIMIT 1), INTERVAL {0} DAY)))) AND (Valid = 1)) t1 ORDER BY aDay DESC";

                        case MssqlUse:
                            return "SELECT d.unixtime FROM datagps64 d WHERE d.Mobitel_ID = {0} AND Valid = 1 AND d.UnixTime > (SELECT TOP 1 d.UnixTime - {1} FROM datagps64 d WHERE d.Mobitel_ID = {0} AND d.Valid = 1 ORDER BY LogID DESC)";
                    }

                    return "";
                }}

            public static string SelectDatagpsId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Altitude AS Altitude, UnixTime AS `time`, Speed AS speed, Valid AS Valid,
                                    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + (Sensor2 << 48) + (Sensor1 << 56)) AS sensor,
                                    Events AS Events, LogID AS LogID 
                                    FROM datagps 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN UNIX_TIMESTAMP(?Begin) AND UNIX_TIMESTAMP(?End))                                                                                                                                
                                    AND (Valid = 1) 
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime";

                    case MssqlUse:
                        return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Altitude AS Altitude, UnixTime AS time, Speed AS speed, Valid AS Valid,
                                    (Sensor8 + (Sensor7 * 256) + (Sensor6 *  65536) + (cast(Sensor5 AS BIGINT) * 16777216) +
                                    (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor,
                                    Events AS Events, LogID AS LogID FROM datagps 
                                    WHERE Mobitel_ID = @MobitelId AND 
                                    (UnixTime BETWEEN @Begin AND @End) AND (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0) 
                                    ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string SelectDatagpsId64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Acceleration , UnixTime , Speed AS speed, Valid AS Valid,
                                    Sensors AS sensor,
                                    Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm
                                    FROM datagps64 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN UNIX_TIMESTAMP(?Begin) AND UNIX_TIMESTAMP(?End))                                                                                                                                
                                    AND (Valid = 1) 
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime";
                        case MssqlUse:
                            return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Acceleration , UnixTime, Speed AS speed, Valid AS Valid,
                                    Sensors AS sensor,
                                    Events, LogID ,Voltage,DGPS,Direction,Satellites,SensorsSet,RssiGsm
                                    FROM datagps64 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN @Begin AND @End)                                                                                                                                
                                    AND (Valid = 1) 
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string SelectDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Altitude AS Altitude,UnixTime AS `time`, Speed AS speed, `Valid` AS Valid,
                                    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + (Sensor2 << 48) + (Sensor1 << 56)) AS sensor,
                                    `Events` AS Events, LogID AS LogID 
                                    FROM datagps 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN ?Begin AND ?End)                                                                                                                                
                                    AND (Valid = 1) 
                                    AND (Speed < 100)
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime";
                    case MssqlUse:
                        return @"SELECT DataGps_ID AS DataGps_ID, Mobitel_ID AS Mobitel_ID,
                                    Latitude AS Lat, Longitude AS Lon,
                                    Altitude AS Altitude, UnixTime AS time, Speed AS speed, Valid AS Valid,
                                    (Sensor8 + (Sensor7 * 256) + (Sensor6 *  65536) + (cast(Sensor5 AS BIGINT) * 16777216) +
                                    (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor,
                                    Events AS Events, LogID AS LogID FROM datagps 
                                    WHERE Mobitel_ID = @MobitelId AND 
                                    (UnixTime BETWEEN @Begin AND @End) AND (Speed < 100) AND (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0) 
                                    ORDER BY UnixTime";
                    }

                    return "";
                }
            }

            public static string SelectDatagpsAllPoints
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return string.Format(@"{0},SrvPacketID
                                    FROM datagps 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN ?Begin AND ?End)       
                                    ORDER BY UnixTime", selectDataGps);
                        case MssqlUse: return string.Format(@"{0},SrvPacketID
                                    FROM datagps 
                                    WHERE Mobitel_ID = @MobitelId
                                    AND (UnixTime BETWEEN @Begin AND @End)                                                                                                                                
                                    ORDER BY UnixTime", msSelectDataGps);
                    }

                    return "";
                }
            }

            public static string SelectLast100DatagpsAllPoints
            {
                get{
                    switch (TypeDataBaseForUsing)
                    {
                        //43420 - секунд в 12 часах = 12 * 60 * 60
                        case MySqlUse:
                            return string.Format(@"{0},SrvPacketID
                                    FROM datagps 
                                    WHERE Mobitel_ID = ?MobitelId and LogID > ?LogId AND UnixTime <= UNIX_TIMESTAMP(?FUTime) AND UnixTime >= (UNIX_TIMESTAMP(?FUTime) - 43420)
                                    ORDER BY UnixTime DESC LIMIT 100", selectDataGps);
                        case MssqlUse: return string.Format(@"{0},SrvPacketID
                                    FROM datagps 
                                    WHERE Mobitel_ID = @MobitelId and LogID > @LogId AND UnixTime < dbo.UNIX_TIMESTAMPS(@FUTime) AND UnixTime >= (dbo.UNIX_TIMESTAMPS(@FUTime) - 43420)
                                    ORDER BY UnixTime DESC ", string.Format(msSelectDataGpsCommon, " TOP 100 "));
                    }

                    return "";
                }
            }
            public static string SelectDatagps64
            {
               get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN ?Begin AND ?End)       
                                    AND (Valid = 1)                                                                                                                          
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime",selectDataGps64);
                        case MssqlUse:return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = @MobitelId
                                    AND (UnixTime BETWEEN @Begin AND @End)                                                                                                                                
                                    AND (Valid = 1) 
                                    AND NOT (Latitude = 0 AND Longitude = 0)
                                    ORDER BY UnixTime",msSelectDataGps64);
                    }

                    return "";
                }
            }
            public static string SelectDatagps64AllPoints
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = ?MobitelId
                                    AND (UnixTime BETWEEN ?Begin AND ?End)       
                                    ORDER BY UnixTime", selectDataGps64);
                        case MssqlUse: return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = @MobitelId
                                    AND (UnixTime BETWEEN @Begin AND @End)                                                                                                                                
                                    ORDER BY UnixTime", msSelectDataGps64);
                    }

                    return "";
                }
            }

            public static string SelectLast100Datagps64AllPoints
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = ?MobitelId  and LogID > ?LoginId AND UnixTime <= UNIX_TIMESTAMP(?FUTime) AND UnixTime >= UNIX_TIMESTAMP(?FUTime) - 43420
                                    ORDER BY UnixTime DESC LIMIT 100", selectDataGps64);
                        case MssqlUse: return string.Format(@"{0}
                                    FROM datagps64 
                                    WHERE Mobitel_ID = @MobitelId  and LogID > @LoginId AND UnixTime < dbo.UNIX_TIMESTAMPS(@FUTime)  AND UnixTime >= dbo.UNIX_TIMESTAMPS(@FUTime) - 43420
                                    ORDER BY UnixTime DESC", string.Format(msSelectDataGps64Top, " TOP 100 "));
                    }

                    return "";
                }
            }
        }// DataviewTableAdapter
        public class DriverTableAdapter
        {
            public static string InsertIntoDriver {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                            return "INSERT INTO driver (Family, Name, ByrdDay, Category, Permis, foto, Identifier,numTelephone,Department) \r\n" +
                        "VALUES (?family, ?name, ?byrdDay, ?category, ?permis, ?foto, ?Identifier, ?numTelephone,?Department); " +
                        "SELECT * FROM driver WHERE Id = LAST_INSERT_ID()";

                    case MssqlUse:
                            return "INSERT INTO driver (Family, Name, ByrdDay, Category, Permis, foto, Identifier,numTelephone,Department) \r\n" +
                        "VALUES (@family, @name, @byrdDay, @category, @permis, @foto, @Identifier,@numTelephone,@Department); " +
                        "SELECT * FROM driver WHERE Id = (SELECT max(id) FROM driver)";
                    }

                    return "";
                }
            }

            public static string UpdateDriverFamily {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "UPDATE driver SET Family = ?Family, Name = ?Name, ByrdDay = ?ByrdDay," +
                        " Category = ?Category, Permis = ?Permis, foto = ?foto, Identifier = ?Identifier, numTelephone = ?numTelephone,Department = ?Department " +
                        "WHERE id = ?id";

                    case MssqlUse:
                        return "UPDATE driver SET Family = @Family, Name = @Name, ByrdDay = @ByrdDay," +
                        " Category = @Category, Permis = @Permis, foto = @foto, Identifier = @Identifier, numTelephone = @numTelephone,Department = @Department " +
                        "WHERE id = @id";
                    }

                    return "";
                }
            }
        }
        // DriverTableAdapter
        public class LocalCacheItem
        {
            public static string SelectDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT datagps.DataGps_ID "
                        + " FROM  datagps WHERE   FROM_UNIXTIME(datagps.UnixTime) >= ?TimeStart"
                        + " AND FROM_UNIXTIME(datagps.UnixTime) <= ?TimeLast"
                        + " AND datagps.Mobitel_ID = {0}"
                        + " AND datagps.speed > 0 and datagps.Valid=1";

                    case MssqlUse:
                        return "SELECT datagps.DataGps_ID "
                        + " FROM  datagps WHERE dbo.FROM_UNIXTIME(datagps.UnixTime) >= @TimeStart"
                        + " AND dbo.FROM_UNIXTIME(datagps.UnixTime) <= @TimeLast"
                        + " AND datagps.Mobitel_ID = {0}"
                        + " AND datagps.speed > 0 and datagps.Valid=1";
                    }

                    return "";
                }
            }
        }
        // LocalCacheItem
        public class MobitelsProvider
        {
            public static string SelectMobitel {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                            return @"SELECT m.mobitel_id, m.Name, m.Descr, COALESCE(imc.devIdShort, 'No Data') AS devIdShort,
                                    COALESCE(tel.TelNumber, 'No Data') AS TelNumber,ls.Color,From_UnixTime(m.LastInsertTime) as LastTimeGps,m.Is64bitPackets,m.IsNotDrawDgps
                                    FROM mobitels m INNER JOIN servicesend srvs ON srvs.ServiceSend_ID = (
                                    SELECT MAX(ServiceSend_ID)
                                    FROM servicesend WHERE ID = m.servicesend_id)
                                    LEFT JOIN internalmobitelconfig imc ON imc.InternalMobitelConfig_ID = (
                                    SELECT MAX(InternalMobitelConfig_ID)
                                    FROM internalmobitelconfig
                                    WHERE ID = m.InternalMobitelConfig_ID)
                                    LEFT JOIN telnumbers tel ON (srvs.telMobitel = tel.TelNumber_ID)
                                    LEFT OUTER JOIN `lines` ls
                                    ON (ls.Mobitel_ID = m.Mobitel_ID)";

                    case MssqlUse:
                        return @"SELECT m.mobitel_id, m.Name, m.Descr, coalesce(imc.devIdShort, 'No Data') AS devIdShort
                                    , coalesce(tel.TelNumber, 'No Data') AS TelNumber
                                    , ls.Color, dbo.From_UnixTime(m.LastInsertTime) AS LastTimeGps,m.Is64bitPackets,m.IsNotDrawDgps
                                    FROM mobitels m
                                    INNER JOIN servicesend srvs
                                    ON srvs.ServiceSend_ID = (SELECT max(ServiceSend_ID)
                                    FROM
                                    servicesend
                                    WHERE
                                    ID = m.servicesend_id)
                                    LEFT JOIN internalmobitelconfig imc
                                    ON imc.InternalMobitelConfig_ID = (SELECT max(InternalMobitelConfig_ID)
                                    FROM
                                    internalmobitelconfig
                                    WHERE
                                    ID = m.InternalMobitelConfig_ID)
                                    LEFT JOIN telnumbers tel
                                    ON (srvs.telMobitel = tel.TelNumber_ID)
                                    LEFT OUTER JOIN lines ls
                                    ON (ls.Mobitel_ID = m.Mobitel_ID)";
                    }

                    return "";
                }
            }

            public static string SelectMobitelId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT mobitels.Mobitel_ID, From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels";

                    case MssqlUse:
                        return @"SELECT mobitels.Mobitel_ID, dbo.From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels";
                    }

                    return "";
                }
            }

            public static string SelectFromMobitels {
                get {
                    return @"SELECT mls.Mobitel_ID
                    FROM
                    mobitels mls
                    INNER JOIN internalmobitelconfig
                    ON mls.InternalMobitelConfig_ID = internalmobitelconfig.ID
                    WHERE
                    internalmobitelconfig.devIdShort = {0} nameTT";
                }
            }
        }
        // MobitelsProvider
        public class RelationalgorithmsTableAdapter
        {
            public static string UpdateRelationalgorithms {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "UPDATE relationalgorithms\r\n " +
                        "SET ID = ?ID, AlgorithmID = ?AlgorithmID, SensorID = ?SensorID \r\n" +
                        "WHERE (ID = ?ID)";

                    case MssqlUse:
                        return "UPDATE relationalgorithms\r\n " +
                        "SET AlgorithmID = @AlgorithmID, SensorID = @SensorID \r\n" +
                        "WHERE (ID = @ID)";
                    }

                    return "";
                }
            }

            public static string DeleteRelationalAlgorithms
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return "DELETE FROM relationalgorithms WHERE ID = ?ID";

                        case MssqlUse:
                            return "DELETE FROM relationalgorithms WHERE ID = @ID";
                    }

                    return "";
                }
            }

            public static string SelectRelationalAlgorithms
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return "SELECT `ID`, `AlgorithmID`, `SensorID` FROM `relationalgorithms`";

                        case MssqlUse:
                            return "SELECT ID, AlgorithmID, SensorID FROM relationalgorithms";
                    }

                    return "";
                }
            }

            public static string InsertIntoRelationalgorithms {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO relationalgorithms ( ID, AlgorithmID, SensorID ) VALUES ( ?ID, ?AlgorithmID, ?SensorID);" +
                        "Select ?ID = LAST_INSERT_ID()";

                    case MssqlUse:
                        return  "INSERT INTO relationalgorithms ( AlgorithmID, SensorID ) VALUES ( @AlgorithmID, @SensorID);" +
                        "Select @ID = (SELECT max(id) FROM relationalgorithms)";
                    }

                    return "";
                }
            }
        }
        // RelationalgorithmsTableAdapter
        public class SensoralgorithmTableAdapter
        {
            public static string InsertIntoAtlanta {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO `atlanta`.`sensoralgorithms` (`Name`, `Description`, `AlgorithmID`) V" +
                        "ALUES (?Name, ?Description, ?AlgorithmID)";

                    case MssqlUse:
                        return  "INSERT INTO atlanta.sensoralgorithms (Name, Description, AlgorithmID) V" +
                        "ALUES (@Name, @Description, @AlgorithmID)";
                    }

                    return "";
                }
            }
        }
        // SensoralgorithmTableAdapter
        public class SensorcoefficientTableAdapter
        {
            public static string InsertIntoSensorcoefficient {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO sensorcoefficient ( UserValue, SensorValue, K, b, Sensor_id )" +
                        " VALUES ( ?UserValue, ?SensorValue, ?K, ?b, ?Sensor_id)";

                    case MssqlUse:
                        return  "INSERT INTO sensorcoefficient ( UserValue, SensorValue, K, b, Sensor_id )" +
                        " VALUES ( @UserValue, @SensorValue, @K, @b, @Sensor_id)";
                    }

                    return "";
                }
            }

            public static string UpdateSensorcoefficient {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "UPDATE sensorcoefficient SET Sensor_id = ?Sensor_id, UserValue = ?Us" +
                        "erValue, SensorValue = ?SensorValue, K = ?K, b = ?b WHERE id = ?id";

                    case MssqlUse:
                        return  "UPDATE sensorcoefficient SET Sensor_id = @Sensor_id, UserValue = @Us" +
                        "erValue, SensorValue = @SensorValue, K = @K, b = @b WHERE id = @id";
                    }

                    return "";
                }
            }
        }
        // SensoralgorithmTableAdapter
        public class SensorsTableAdapter
        {
            public static string InsertIntoSensors {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO sensors ( mobitel_id, Name, Description, StartBit, Length, NameUnit, " +
                        "K, B, S ) VALUES ( ?mobitel_id, ?Name, ?Description, ?StartBit, ?Length, ?NameUnit, ?K, ?B, ?S " +
                        "); SELECT * FROM sensors where Id = LAST_INSERT_ID()";

                    case MssqlUse:
                        return  "INSERT INTO sensors ( mobitel_id, Name, Description, StartBit, Length, NameUnit, " +
                        "K, B, S ) VALUES ( @Mobitel_id, @Name, @Description, @StartBit, @Length, @NameUnit, @K, @B, @S " +
                        "); SELECT * FROM sensors where Id = (SELECT max(id) FROM sensors)";
                    }

                    return "";
                }
            }

            public static string UpdateSensors {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "UPDATE sensors " +
                        "SET Name = ?Name, Description = ?Description, StartBit = ?StartBit," +
                        "  Length = ?Length, NameUnit = ?NameUnit, K = ?K, B = ?B, S = ?S WHERE (id = ?id)";

                    case MssqlUse:
                        return  "UPDATE sensors " +
                        "SET Name = @Name, Description = @Description, StartBit = @StartBit," +
                        "  Length = @Length, NameUnit = @NameUnit, K = @K, B = @B, S = @S WHERE (id = @id)";
                    }

                    return "";
                }
            }
        }
        // SensorsTableAdapter
        public class SettingTableAdapter
        {
            public static string InsertIntoSetting {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"INSERT INTO `setting` (`band`, `FuelingDischarge`, `idleRunningMain`, `idleRunningAdd`, `FuelingEdge`, 
                                    `RotationAdd`, `RotationMain`, `TimeBreak`, `Name`, `Desc`, `accelMax`, `breakMax`, `speedMax`,
                                    `AvgFuelRatePerHour`, `FuelApproximationTime`, `TimeGetFuelAfterStop`, `TimeGetFuelBeforeMotion`, 
                                    `CzMinCrossingPairTime`, `FuelerRadiusFinds`, `FuelerMaxTimeStop`, `FuelerMinFuelrate`,
                                    `FuelerCountInMotion`, `FuelrateWithDischarge`, `FuelingMinMaxAlgorithm`, `TimeBreakMaxPermitted`, `InclinometerMaxAngleAxisX`, `InclinometerMaxAngleAxisY`, `speedMin`, `TimeMinimMovingSize`, `MinTimeSwitch`) 
                                    VALUES (?band, ?FuelingDischarge, ?idleRunningMain, ?idleRunningAdd, ?FuelingEdge, 
                                    ?RotationAdd, ?RotationMain, ?TimeBreak, ?Name, ?Desc, ?accelMax, ?breakMax, ?speedMax, 
                                    ?AvgFuelRatePerHour, ?FuelApproximationTime, ?TimeGetFuelAfterStop, ?TimeGetFuelBeforeMotion,
                                    ?CzMinCrossingPairTime, ?FuelerRadiusFinds, ?FuelerMaxTimeStop, ?FuelerMinFuelrate,
                                    ?FuelerCountInMotion, ?FuelrateWithDischarge, ?FuelingMinMaxAlgorithm, ?TimeBreakMaxPermitted, ?InclinometerMaxAngleAxisX, ?InclinometerMaxAngleAxisY, ?speedMin, ?TimeMinimMovingSize, ?MinTimeSwitch)";

                    case MssqlUse:
                        return @"INSERT INTO dbo.setting (band, FuelingDischarge, idleRunningMain, idleRunningAdd, FuelingEdge, 
                                    RotationAdd, RotationMain, TimeBreak, Name, [Desc], accelMax, breakMax, speedMax,
                                    AvgFuelRatePerHour,FuelApproximationTime,TimeGetFuelAfterStop,TimeGetFuelBeforeMotion, 
                                    CzMinCrossingPairTime, FuelerRadiusFinds, FuelerMaxTimeStop, FuelerMinFuelrate,
                                    FuelerCountInMotion, FuelrateWithDischarge, FuelingMinMaxAlgorithm, TimeBreakMaxPermitted, InclinometerMaxAngleAxisX, InclinometerMaxAngleAxisY, speedMin, TimeMinimMovingSize, MinTimeSwitch) 
                                    VALUES (@band, @FuelingDischarge, @idleRunningMain, @idleRunningAdd, @FuelingEdge, 
                                    @RotationAdd, @RotationMain, @TimeBreak, @Name, @Desc, @accelMax, @breakMax, @speedMax, 
                                    @AvgFuelRatePerHour, @FuelApproximationTime, @TimeGetFuelAfterStop, @TimeGetFuelBeforeMotion,
                                    @CzMinCrossingPairTime, @FuelerRadiusFinds, @FuelerMaxTimeStop, @FuelerMinFuelrate,
                                    @FuelerCountInMotion, @FuelrateWithDischarge, @FuelingMinMaxAlgorithm, @TimeBreakMaxPermitted, @InclinometerMaxAngleAxisX, @InclinometerMaxAngleAxisY, @speedMin, @TimeMinimMovingSize, @MinTimeSwitch)";
                    }

                    return "";
                }
            }

            public static string UpdateSetting {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE setting
                                    SET TimeBreak = ?p1, RotationMain = ?p2, RotationAdd = ?p3, FuelingEdge = ?p4, band = ?p5, 
                                    FuelingDischarge = ?p6, accelMax = ?p7, breakMax = ?p8, speedMax = ?p9, 
                                    idleRunningMain = ?p10, idleRunningAdd = ?p11, Name = ?p12, `Desc` = ?p13, 
                                    AvgFuelRatePerHour = ?p14, FuelApproximationTime = ?p15, TimeGetFuelAfterStop = ?p16, 
                                    TimeGetFuelBeforeMotion = ?p17, CzMinCrossingPairTime = ?p18,
                                    FuelerRadiusFinds = ?p19, FuelerMaxTimeStop = ?p20, FuelerMinFuelrate = ?p21,
                                    FuelerCountInMotion = ?p22, FuelrateWithDischarge = ?p23, FuelingMinMaxAlgorithm = ?p24, TimeBreakMaxPermitted = ?p25,
                                    InclinometerMaxAngleAxisX = ?p26 , InclinometerMaxAngleAxisY = ?p27, speedMin = ?p99, TimeMinimMovingSize = ?p28,
                                    MinTimeSwitch = ?p29 WHERE (id = ?id)";

                    case MssqlUse:
                        return @"UPDATE setting
                                    SET TimeBreak = @p1, RotationMain = @p2, RotationAdd = @p3, FuelingEdge = @p4, band = @p5, 
                                    FuelingDischarge = @p6, accelMax = @p7, breakMax = @p8, speedMax = @p9, 
                                    idleRunningMain = @p10, idleRunningAdd = @p11, Name = @p12, [Desc] = @p13, 
                                    AvgFuelRatePerHour = @p14, FuelApproximationTime = @p15, TimeGetFuelAfterStop = @p16, 
                                    TimeGetFuelBeforeMotion = @p17, CzMinCrossingPairTime = @p18,
                                    FuelerRadiusFinds = @p19, FuelerMaxTimeStop = @p20, FuelerMinFuelrate = @p21,
                                    FuelerCountInMotion = @p22, FuelrateWithDischarge = @p23, FuelingMinMaxAlgorithm = @p24, TimeBreakMaxPermitted = @p25,
                                    InclinometerMaxAngleAxisX = @p26,InclinometerMaxAngleAxisY = @p27, speedMin = @p99, TimeMinimMovingSize = @p28,
                                    MinTimeSwitch = @p29 WHERE (id = @id)";
                    }

                    return "";
                }
            }

            public static string SelectFromSetting {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT band, FuelingDischarge, idleRunningMain, idleRunningAdd, FuelingEdge, \r\n" +
                        "  RotationAdd, RotationMain, TimeBreak, id, Name, `Desc`, accelMax, \r\n" +
                        "  breakMax, speedMax, AvgFuelRatePerHour, FuelApproximationTime, TimeGetFuelAfterStop, \r\n" +
                        "  TimeGetFuelBeforeMotion, CzMinCrossingPairTime, \r\n" +
                        "  FuelerRadiusFinds, FuelerMaxTimeStop, FuelerMinFuelrate, \r\n" +
                        "  FuelerCountInMotion, FuelrateWithDischarge, FuelingMinMaxAlgorithm,TimeBreakMaxPermitted,InclinometerMaxAngleAxisX,InclinometerMaxAngleAxisY,speedMin, TimeMinimMovingSize, MinTimeSwitch \r\n" +
                        " FROM setting";

                    case MssqlUse:
                        return @"SELECT band, FuelingDischarge, idleRunningMain, idleRunningAdd, FuelingEdge," +
                        " RotationAdd, RotationMain, TimeBreak, id, Name, [Desc], accelMax," +
                        " breakMax, speedMax, AvgFuelRatePerHour, FuelApproximationTime, TimeGetFuelAfterStop," +
                        " TimeGetFuelBeforeMotion, CzMinCrossingPairTime," +
                        " FuelerRadiusFinds, FuelerMaxTimeStop, FuelerMinFuelrate," +
                        " FuelerCountInMotion, FuelrateWithDischarge, FuelingMinMaxAlgorithm, TimeBreakMaxPermitted,InclinometerMaxAngleAxisX,InclinometerMaxAngleAxisY,speedMin, TimeMinimMovingSize, MinTimeSwitch " +
                        " FROM setting";
                    }

                    return "";
                }
            }
        }
        // SettingTableAdapter
        public class StateTableAdapter
        {
            public static string DeleteFromState {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"DELETE FROM `State` WHERE ((`Id` = @Original_Id) AND (`MinValue` = @Original_MinValue) AND (`MaxValue` = @Original_MaxValue) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)))";

                    case MssqlUse:
                        return @"DELETE FROM State WHERE ((Id = @Original_Id) AND (MinValue = @Original_MinValue) AND (MaxValue = @Original_MaxValue) AND (Title = @Original_Title) AND ((@IsNull_SensorId = 1 AND SensorId IS NULL) OR (SensorId = @Original_SensorId)))";
                    }

                    return "";
                }
            }

            public static string InsertIntoState {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO `State` (`MinValue`, `MaxValue`, `Title`, `SensorId`) VALUES" +
                        " (@MinValue, @MaxValue, @Title, @SensorId); SELECT * FROM `State` WHERE Id = LAST_INSERT_ID()";

                    case MssqlUse:
                        return  "INSERT INTO State (MinValue, MaxValue, Title, SensorId) VALUES" +
                        " (@MinValue, @MaxValue, @Title, @SensorId); SELECT * FROM State WHERE Id = (SELECT max(id) FROM State);";
                    }

                    return "";
                }
            }

            public static string UpdateState {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        //return @"UPDATE `State` SET `MinValue` = @MinValue, `MaxValue` = @MaxValue, `Title` = @Title, `SensorId` = @SensorId WHERE ((`Id` = @Original_Id) AND (`MinValue` = @Original_MinValue) AND (`MaxValue` = @Original_MaxValue) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)))";
                        return @"UPDATE `State` SET `MinValue` = @MinValue, `MaxValue` = @MaxValue, `Title` = @Title, `SensorId` = @SensorId WHERE `Id` = @Original_Id";

                    case MssqlUse:
                        // return @"UPDATE State SET MinValue = @MinValue, MaxValue = @MaxValue, Title = @Title, SensorId = @SensorId WHERE ((Id = @Original_Id) AND (MinValue = @Original_MinValue) AND (MaxValue = @Original_MaxValue) AND (Title = @Original_Title) AND ((@IsNull_SensorId = 1 AND SensorId IS NULL) OR (SensorId = @Original_SensorId)))";
                        return @"UPDATE [State] SET MinValue = @MinValue, MaxValue = @MaxValue, Title = @Title, SensorId = @SensorId WHERE Id = @Original_Id";
                    }

                    return "";
                }
            }

            public static string SelectState {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT `Id`, `MinValue`, `MaxValue`, `Title`, `SensorId` FROM `state`";

                    case MssqlUse:
                        return "SELECT Id, MinValue, MaxValue, Title, SensorId FROM state";
                    }

                    return "";
                }
            }
        }
        // StateTableAdapter
        public class TeamTableAdapter
        {
            public static string InsertIntoTeam {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO team (`Name`, `Descr`, `Setting_id`, `FuelWayKmGrp`, `FuelMotoHrGrp`) " +
                        "VALUES (?Name, ?Descr, ?Setting_id, ?FuelWayKmGrp, ?FuelMotoHrGrp); " +
                        "SELECT * FROM team WHERE Id = LAST_INSERT_ID()";

                    case MssqlUse:
                        return  "INSERT INTO team (Name, Descr, Setting_id, FuelWayKmGrp, FuelMotoHrGrp) " +
                        "VALUES (@Name, @Descr, @Setting_id, @FuelWayKmGrp, @FuelMotoHrGrp); " +
                        "SELECT * FROM team WHERE Id = (SELECT max(id) FROM team)";
                    }

                    return "";
                }
            }

            public static string UpdateTeam {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "UPDATE team SET Name = ?p1, Descr = ?p2, Setting_id = ?p3, `FuelWayKmGrp` = ?FuelWayKmGrp, `FuelMotoHrGrp` = ?FuelMotoHrGrp WHERE id = ?id";

                    case MssqlUse:
                        return "UPDATE team SET Name = @p1, Descr = @p2, Setting_id = @p3, FuelWayKmGrp = @FuelWayKmGrp, FuelMotoHrGrp = @FuelMotoHrGrp WHERE id = @id";
                    }

                    return "";
                }
            }
        }
        // TeamTableAdapter
        public class TransitionTableAdapter
        {
            public static string DeleteFromTransition {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"DELETE FROM `Transition` WHERE ((`Id` = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND `InitialStateId` IS NULL) OR (`InitialStateId` = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND `FinalStateId` IS NULL) OR (`FinalStateId` = @Original_FinalStateId)) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)) AND (`IconName` = @Original_IconName) AND (`SoundName` = @Original_SoundName) AND (`Enabled` = @Original_Enabled) AND (`ViewInReport` = @Original_ViewInReport))";

                    case MssqlUse:
                        return @"DELETE FROM Transition WHERE ((Id = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND InitialStateId IS NULL) OR (InitialStateId = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND FinalStateId IS NULL) OR (FinalStateId = @Original_FinalStateId)) AND (Title = @Original_Title) AND ((@IsNull_SensorId = 1 AND SensorId IS NULL) OR (SensorId = @Original_SensorId)) AND (IconName = @Original_IconName) AND (SoundName = @Original_SoundName) AND (Enabled = @Original_Enabled) AND (ViewInReport = @Original_ViewInReport))";
                    }

                    return "";
                }
            }

            public static string InsertIntoTransition {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO `Transition` (`InitialStateId`, `FinalStateId`, `Title`, `SensorId`, `IconName`, `SoundName`, `Enabled`, `ViewInReport`) VALUES (@InitialStateId, @FinalStateId, @Title, @SensorId, @IconName, @SoundName, @Enabled, @ViewInReport); SELECT * FROM Transition where Id = LAST_INSERT_ID()";

                    case MssqlUse:
                        return "INSERT INTO Transition (InitialStateId, FinalStateId, Title, SensorId, IconName, SoundName, Enabled, ViewInReport) VALUES (@InitialStateId, @FinalStateId, @Title, @SensorId, @IconName, @SoundName, @Enabled, @ViewInReport); SELECT * FROM Transition where Id = (SELECT max(id) FROM Transition)";
                    }

                    return "";
                }
            }

            public static string UpdateTransition {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE `Transition` SET `InitialStateId` = @InitialStateId, `FinalStateId` = @FinalStateId, `Title` = @Title, `SensorId` = @SensorId, `IconName` = @IconName, `SoundName` = @SoundName, `Enabled` = @Enabled, `ViewInReport` = @ViewInReport WHERE ((`Id` = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND `InitialStateId` IS NULL) OR (`InitialStateId` = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND `FinalStateId` IS NULL) OR (`FinalStateId` = @Original_FinalStateId)) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)) AND (`IconName` = @Original_IconName) AND (`SoundName` = @Original_SoundName) AND (`Enabled` = @Original_Enabled) AND (`ViewInReport` = @Original_ViewInReport))";

                    case MssqlUse:
                        return @"UPDATE Transition SET InitialStateId = @InitialStateId, FinalStateId = @FinalStateId, Title = @Title, SensorId = @SensorId, IconName = @IconName, SoundName = @SoundName, Enabled = @Enabled, ViewInReport = @ViewInReport WHERE ((Id = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND InitialStateId IS NULL) OR (InitialStateId = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND FinalStateId IS NULL) OR (FinalStateId = @Original_FinalStateId)) AND (Title = @Original_Title) AND ((@IsNull_SensorId = 1 AND SensorId IS NULL) OR (SensorId = @Original_SensorId)) AND (IconName = @Original_IconName) AND (SoundName = @Original_SoundName) AND (Enabled = @Original_Enabled) AND (ViewInReport = @Original_ViewInReport))";
                    }

                    return "";
                }
            }

            public static string SelectTransition {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "SELECT `Id`, `InitialStateId`, `FinalStateId`, `Title`, `SensorId`, `IconName`, `" +
                        "SoundName`, `Enabled`, `ViewInReport` FROM `transition`";

                    case MssqlUse:
                        return  "SELECT Id, InitialStateId, FinalStateId, Title, SensorId, IconName, " +
                        "SoundName, Enabled, ViewInReport FROM transition";
                    }

                    return "";
                }
            }
        }
        // StateTableAdapter
        public class VehicleTableAdapter
        {
            public static string InsertIntoVehicle {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO vehicle (MakeCar, NumberPlate, Team_id, Mobitel_id, " +
                        " CarModel, setting_id, driver_id,OutLinkId,FuelWayLiter, FuelMotorLiter,Category_id,Identifier,Category_id2, PcbVersionId, Category_id3, Category_id4) " +
                        "VALUES (?MakeCar, ?NumberPlate, ?Team_id, ?Mobitel_id, ?CarModel," +
                        " ?setting_id, ?driver_id,?OutLinkId,?FuelWayLiter,?FuelMotorLiter,?Category_id, ?Identifier,?Category_id2, ?PcbVersionId, ?Category_id3, ?Category_id4); " +
                        "SELECT * FROM vehicle WHERE Id = LAST_INSERT_ID()";

                    case MssqlUse:
                        return "INSERT INTO vehicle (MakeCar, NumberPlate, Team_id, Mobitel_id, " +
                        " CarModel, setting_id, driver_id,OutLinkId,FuelWayLiter,FuelMotorLiter,Category_id,Identifier,Category_id2, PcbVersionId, Category_id3, Category_id4) " +
                        "VALUES (@MakeCar, @NumberPlate, @Team_id, @Mobitel_id, @CarModel," +
                        " @setting_id, @driver_id, @OutLinkId, @FuelWayLiter, @FuelMotorLiter, @Category_id, @Identifier, @Category_id2, @PcbVersionId, @Category_id3, @Category_id4); " +
                        "SELECT * FROM vehicle WHERE Id = (SELECT max(id) FROM vehicle)";
                    }

                    return "";
                }
            }

            public static string SelectFromVehicle
            {
                get
                {
                    return "SELECT id, MakeCar, NumberPlate, Team_id, Mobitel_id, CarModel, setting_id, driver_id, OutLinkId, FuelWayLiter, FuelMotorLiter, Category_id, Identifier, Category_id2, PcbVersionId, Category_id3, Category_id4 FROM vehicle";
                }
            }

            public static string UpdateVehicle {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE vehicle SET MakeCar = ?MakeCar, NumberPlate = ?NumberPlate," +
                        " Team_id = ?Team_id, Mobitel_id = ?Mobitel_id," +
                        " CarModel = ?CarModel, setting_id = ?setting_id, driver_id = ?driver_id, OutLinkId = ?OutLinkId, FuelWayLiter = ?FuelWayLiter, FuelMotorLiter = ?FuelMotorLiter, " +
                        " Category_id = ?Category_id, Identifier = ?Identifier,Category_id2 = ?Category_id2, PcbVersionId = ?PcbVersionId,Category_id3 = ?Category_id3,Category_id4 = ?Category_id4  WHERE id = ?id";

                    case MssqlUse:
                        return @"UPDATE vehicle SET MakeCar = @MakeCar, NumberPlate = @NumberPlate," +
                        " Team_id = @Team_id, Mobitel_id = @Mobitel_id," +
                        " CarModel = @CarModel, setting_id = @setting_id, driver_id = @driver_id, OutLinkId = @OutLinkId, FuelWayLiter = @FuelWayLiter, FuelMotorLiter = @FuelMotorLiter, " +
                        " Category_id = @Category_id, Identifier = @Identifier,Category_id2 = @Category_id2, PcbVersionId = @PcbVersionId, Category_id3 = @Category_id3,Category_id4 = @Category_id4 WHERE id = @id";
                    }

                    return "";
                }
            }
        }
        // VehicleTableAdapter
        public class PointsAnalyzer
        {
            public static string SelectDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT datagps.DataGps_ID,datagps.Valid, datagps.LogID,From_UnixTime(datagps.UnixTime) as UT"
                        + " FROM  datagps "
                        + " WHERE ( datagps.UnixTime BETWEEN UNIX_TIMESTAMP(?TimeStart) AND UNIX_TIMESTAMP(?TimeEnd)) AND datagps.Mobitel_ID = {0}"
                        + " ORDER BY datagps.LogID DESC";

                    case MssqlUse:
                        return "SELECT datagps.DataGps_ID,datagps.Valid, datagps.LogID,dbo.From_UnixTime(datagps.UnixTime) as UT"
                        + " FROM  datagps "
                        + " WHERE ( datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@TimeStart) AND dbo.UNIX_TIMESTAMPS(@TimeEnd)) AND datagps.Mobitel_ID = {0}" +
                        " ORDER BY datagps.LogID DESC";
                    }

                    return "";
                }
            }

            public static string Selectdatagps64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT datagps64.datagps_ID,datagps64.Valid, datagps64.LogID,From_UnixTime(datagps64.UnixTime) as UT"
                            + " FROM  datagps64 "
                            + " WHERE ( datagps64.UnixTime BETWEEN UNIX_TIMESTAMP(?TimeStart) AND UNIX_TIMESTAMP(?TimeEnd)) AND datagps64.Mobitel_ID = {0}"
                            + " ORDER BY datagps64.LogID DESC";
                            case MssqlUse:
                            return "SELECT datagps64.datagps_ID,datagps64.Valid, datagps64.LogID,dbo.From_UnixTime(datagps64.UnixTime) as UT"
                            + " FROM  datagps64 "
                            + " WHERE ( datagps64.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@TimeStart) AND dbo.UNIX_TIMESTAMPS(@TimeEnd)) AND datagps64.Mobitel_ID = {0}" +
                            " ORDER BY datagps64.LogID DESC";
                    }

                    return "";
                }
            }

            public static string TruncateDatagpsbuffer_on {
                get {
                    return "TRUNCATE TABLE datagpsbuffer_on";
                }
            }

            public static string TruncateDatagpsbuffer_on64
            {
                get
                {
                    return "TRUNCATE TABLE datagpsbuffer_on64";
                }
            }

            public static string UpdateMobitels {
                get {
                    return "UPDATE mobitels SET ConfirmedID = {0}" + "  WHERE Mobitel_ID = {1}";
                }
            }

            public static string UpdateValid
            {
                get
                {
                    return "UPDATE datagps SET Valid = {0}" + "  WHERE DataGps_ID = {1}";
                }
            }

            public static string UpdateValid64
            {
                get
                {
                    return "UPDATE datagps64 SET Valid = {0}" + "  WHERE DataGps_ID = {1}";
                }
            }

            public static string TruncateDatagpslost_on {
                get {
                    return "TRUNCATE TABLE datagpslost_on";
                }
            }

            public static string TruncateDatagpslost_ontmp {
                get {
                    return "TRUNCATE TABLE datagpslost_ontmp";
                }
            }

            public static string SelectCount {
                get {
                    return "SELECT COUNT(datagpslost_on.End_LogID) FROM  datagpslost_on "
                    + " WHERE datagpslost_on.Mobitel_ID = {0}" +
                    " AND datagpslost_on.Begin_SrvPacketID = {1}"
                    + " OR datagpslost_on.Mobitel_ID = {2}" + " AND datagpslost_on.Begin_LogID = {3}";
                }
            }

            public static string InsertIntoDatagpslost_on {
                get {
                    return "INSERT INTO datagpslost_on(Mobitel_ID,Begin_LogID,End_LogID,Begin_SrvPacketID,End_SrvPacketID)" +
                    " values   (" + "{0}" + "," + "{1}" +
                    "," + "{2}" +
                    "," + "{3}" +
                    "," + "{4}" + ")";
                }
            }

            public static string UpdateMobitelsSet {
                get {
                    return "UPDATE mobitels SET ConfirmedID = {0} WHERE Mobitel_ID = {1}";
                }
            }

            public static string SelectDataGpsLostOn {
                get {
                    return "SELECT datagpslost_on.Mobitel_ID FROM datagpslost_on WHERE datagpslost_on.Mobitel_ID = ";
                }
            }

            public static string SelectMaxDatagps {
                get {
                    return "SELECT MAX(DataGps_ID) FROM DataGps WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId < 0)";
                }
            }

            public static string SelectMaxDatagpsid {
                get {
                    return "SELECT MAX(DataGps_ID) FROM DataGps WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId >= 0)";
                }
            }

            public static string SelectCoalesceMaxDatagps {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0) FROM datagps WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId < 0)";
                }
            }

            public static string SelectCoalesceMaxLogId {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0) FROM datagps WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId >= 0)";
                }
            }

            public static string SelectCoalesceDatagps {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0)  FROM datagps   WHERE (Mobitel_ID = " + "{0}" + ")";
                }
            }

            public static string UpdateMobitelsSetConfirmed {
                get {
                    return "UPDATE mobitels SET ConfirmedID = " + "{0}" + "  WHERE Mobitel_ID = {1}";
                }
            }

            public static string SelectTop1FromDataGps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT 1 FROM DataGps  WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId < 0) LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 FROM DataGps  WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId < 0)";
                    }

                    return "";
                }
            }

            public static string Select1FromDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT 1 FROM DataGps  WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId > 0) LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 FROM DataGps WHERE (Mobitel_ID = " + "{0}" + ") AND (LogId > 0)";
                    }

                    return "";
                }
            }

            public static string SelectDataGpsId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT datagps.DataGps_ID, ((datagps.Latitude * 1.0000) / 600000) AS Lat, ((datagps.Longitude * 1.0000) / 600000) AS Lon,From_UnixTime(datagps.UnixTime) as UT, datagps.Speed * 1.852 as Speed,"
                            + " datagps.Sensor8,datagps.Sensor7,datagps.Sensor6,datagps.Sensor5,datagps.Sensor4,datagps.Sensor3 ,datagps.Sensor2,datagps.Sensor1,"
                            + " datagps.Valid, datagps.LogID, datagps.SrvPacketID "
                            + " FROM  datagps "
                            + " WHERE ( datagps.UnixTime BETWEEN UNIX_TIMESTAMP(?TimeStart) AND UNIX_TIMESTAMP(?TimeEnd)) AND datagps.Mobitel_ID = {0}"
                            + " ORDER BY datagps.LogID DESC";

                        case MssqlUse:
                            return "SELECT datagps.DataGps_ID, ((datagps.Latitude * 1.0000) / 600000) AS Lat, ((datagps.Longitude * 1.0000) / 600000) AS Lon,dbo.From_UnixTime(datagps.UnixTime) as UT, datagps.Speed * 1.852 as Speed,"
                            + " datagps.Sensor8,datagps.Sensor7,datagps.Sensor6,datagps.Sensor5,datagps.Sensor4,datagps.Sensor3 ,datagps.Sensor2,datagps.Sensor1,"
                            + " datagps.Valid, datagps.LogID, datagps.SrvPacketID "
                            + " FROM  datagps "
                            + " WHERE ( datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@TimeStart) AND dbo.UNIX_TIMESTAMPS(@TimeEnd)) AND datagps.Mobitel_ID = {0}"
                            + " ORDER BY datagps.LogID DESC";
                    }

                    return "";
                }
            }
        }
        // PointsAnalyzer
        public class DataAnalysis
        {
            public static string SelectVehicleMobitel {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT vehicle.Mobitel_id,  Concat(vehicle.NumberPlate,'|', vehicle.MakeCar,'|',  vehicle.CarModel, '|', internalmobitelconfig.devIdShort)AS Name "
                        + " FROM vehicle   INNER JOIN  mobitels mls ON vehicle.Mobitel_id = mls.Mobitel_ID INNER JOIN  internalmobitelconfig ON mls.InternalMobitelConfig_ID = internalmobitelconfig.ID"
                        + " WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT MAX(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID "
                        + " FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mls.InternalMobitelConfig_ID "
                        + " GROUP BY internalmobitelconfig.ID) ORDER BY Name";

                    case MssqlUse:
                        return @"SELECT vehicle.Mobitel_id," +
                        "vehicle.NumberPlate + '|' + vehicle.MakeCar + '|' + vehicle.CarModel + '|' + internalmobitelconfig.devIdShort AS Name" +
                        " FROM" +
                        " vehicle" +
                        " INNER JOIN mobitels mls" +
                        " ON vehicle.Mobitel_id = mls.Mobitel_ID" +
                        " INNER JOIN internalmobitelconfig" +
                        " ON mls.InternalMobitelConfig_ID = internalmobitelconfig.ID" +
                        " WHERE" +
                        " internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID" +
                        " FROM" +
                        " internalmobitelconfig" +
                        " WHERE" +
                        " internalmobitelconfig.ID = mls.InternalMobitelConfig_ID" +
                        " GROUP BY" +
                        " internalmobitelconfig.ID)" +
                        " ORDER BY" +
                        " Name";
                    }

                    return "";
                }
            }

            public static string SelectFromMobitels {
                get {
                    return "SELECT mobitels.Mobitel_ID, mobitels.Name as Mobitel_Name FROM  mobitels ORDER BY mobitels.Name";
                }
            }

            public static string SelectDatagpsData {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT datagps.DataGPS_ID, datagps.Mobitel_ID, datagps.Valid, datagps.LogID, datagps.SrvPacketID,FROM_UNIXTIME(datagps.UnixTime) as DataGPS 
                                    FROM datagps WHERE (UnixTime BETWEEN UNIX_TIMESTAMP(?Begin) AND UNIX_TIMESTAMP(?End)) AND datagps.Mobitel_ID = ?Mobitel_id";

                    case MssqlUse:
                        return
                                @"SELECT datagps.DataGPS_ID, datagps.Mobitel_ID, datagps.Valid, datagps.LogID, datagps.SrvPacketID,
                                 dbo.From_UnixTime(datagps.UnixTime) as DataGPS FROM datagps WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@Begin) AND dbo.UNIX_TIMESTAMPS(@End)) AND 
                                 datagps.Mobitel_ID = @Mobitel_id";
                    }

                    return "";
                }
            }

            public static string SelectDatagpsData64
            {
                get
                {
                    switch (TypeDataBaseForUsing){
                        case MySqlUse:
                            return @"SELECT datagps64.datagps_ID, datagps64.Mobitel_ID, datagps64.Valid, datagps64.LogID, datagps64.SrvPacketID,FROM_UNIXTIME(datagps64.UnixTime) as DataGPS 
                                    FROM datagps64 WHERE (UnixTime BETWEEN UNIX_TIMESTAMP(?Begin) AND UNIX_TIMESTAMP(?End)) AND datagps64.Mobitel_ID = ?Mobitel_id";

                        case MssqlUse:
                            return
                                    @"SELECT datagps64.datagps_ID, datagps64.Mobitel_ID, datagps64.Valid, datagps64.LogID, datagps64.SrvPacketID,
                                 dbo.From_UnixTime(datagps64.UnixTime) as DataGPS FROM datagps64 WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@Begin) AND dbo.UNIX_TIMESTAMPS(@End)) AND 
                                 datagps64.Mobitel_ID = @Mobitel_id";
                    }return "";
                }
            }

            public static string SelectCountDatagps {
                get {
                    return "SELECT Count(datagps.DataGPS_ID) FROM datagps "
                    + " WHERE (datagps.Mobitel_ID = " + "{0}" + ")"
                    + " AND (datagps.LogID > " + "{1}" + " AND datagps.LogID < " + "{2}" +
                    ")";
                }
            }

            public static string SelectDataGpsId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT datagps.DataGps_ID, FROM_UNIXTIME(datagps.UnixTime) as DataGPS, datagps.LogID, datagps.Valid"
                        + " FROM  datagps WHERE datagps.LogID > {0}" +
                        " AND datagps.Valid = 0"
                        + " AND datagps.Mobitel_ID = {1}"
                        + " ORDER BY datagps.LogID";

                    case MssqlUse:
                        return
                                "SELECT datagps.DataGps_ID, dbo.From_UnixTime(datagps.UnixTime) as DataGPS, datagps.LogID, datagps.Valid"
                        + " FROM  datagps WHERE   datagps.LogID > {0}" +
                        " AND datagps.Valid = 0"
                        + " AND datagps.Mobitel_ID = {1}"
                        + " ORDER BY datagps.LogID";
                        ;
                    }

                    return "";
                }
            }

            public static string UpdateMobitels {
                get {
                    return "UPDATE mobitels SET ConfirmedID = {0} WHERE Mobitel_ID = {1}";
                }
            }

            public static string SelectCountDatagpslost {
                get {
                    return "SELECT  COUNT(datagpslost_on.End_LogID) FROM  datagpslost_on "
                    + " WHERE   datagpslost_on.Mobitel_ID = {0}" +
                    " AND datagpslost_on.Begin_SrvPacketID = {1}"
                    + " OR datagpslost_on.Mobitel_ID = {2}" +
                    " AND datagpslost_on.Begin_LogID = {3}";
                }
            }

            public static string InsertIntoDatagpslost {
                get {
                    return "INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID)" +
                    " values   (" + "{0}" + "," + "{1}" + "," + "{2}" + "," + "{3}" + "," + "{4}" + ")";
                }
            }

            public static string UpdateMobitelsConfirmedId {
                get {
                    return "UPDATE mobitels SET ConfirmedID = {0} WHERE Mobitel_ID = {1}";
                }
            }

            public static string SelectDatagpsLost_on {
                get {
                    return "SELECT datagpslost_on.Mobitel_ID FROM datagpslost_on WHERE datagpslost_on.Mobitel_ID = ";
                }
            }

            public static string SelectMaxDataGpsId {
                get {
                    return "SELECT MAX(DataGps_ID) FROM DataGps WHERE (Mobitel_ID = {0}) AND (LogId < 0)";
                }
            }

            public static string Select1FromDataGps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT 1 FROM DataGps WHERE (Mobitel_ID = {0}) AND (LogId < 0) LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 * FROM DataGps WHERE (Mobitel_ID = {0}) AND (LogId < 0)";
                    }

                    return "";
                }
            }

            public static string Select1DataGps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT 1 FROM DataGps  WHERE (Mobitel_ID = {0}) AND (LogId > 0) LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 * FROM DataGps WHERE (Mobitel_ID = {0}) AND (LogId > 0)";
                    }

                    return "";
                }
            }

            public static string SelectMaxDataGpsIdFromDataGps {
                get {
                    return "SELECT MAX(DataGps_ID) FROM DataGps WHERE (Mobitel_ID = {0}) AND (LogId >= 0)";
                }
            }

            public static string SelectCoalesceMaxLogID {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0)  FROM datagps WHERE (Mobitel_ID = {0}) AND (LogId < 0)";
                }
            }

            public static string SelectCoalesceMaxLog_id {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0) FROM datagps WHERE (Mobitel_ID = {0}) AND (LogId >= 0)";
                }
            }

            public static string SelectCoalesceMaxDataGps {
                get {
                    return "SELECT COALESCE(MAX(LogID), 0) FROM datagps WHERE (Mobitel_ID = {0})";
                }
            }

            public static string UpdateMobitelsConfirmed_ID {
                get {
                    return "UPDATE mobitels SET ConfirmedID = {0} WHERE Mobitel_ID = {1}";
                }
            }

            public static string UpdateDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return
                                "UPDATE datagps SET Valid = 0 WHERE UnixTime > UNIX_TIMESTAMP(adddate(now(), 1)) AND Mobitel_ID = {0}";

                    case MssqlUse:
                        return
                                "UPDATE datagps SET Valid = 0 WHERE UnixTime > dbo.UNIX_TIMESTAMPS(dateadd(day, 1, getdate())) AND Mobitel_ID = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateDatagps64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                    "UPDATE datagps64 SET Valid = 0 WHERE UnixTime > UNIX_TIMESTAMP(adddate(now(), 1)) AND Mobitel_ID = {0}";

                        case MssqlUse:
                            return
                                    "UPDATE datagps64 SET Valid = 0 WHERE UnixTime > dbo.UNIX_TIMESTAMPS(dateadd(day, 1, getdate())) AND Mobitel_ID = {0}";
                    }

                    return "";
                }
            }

            public static string DeleteFromOnline {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return
                                "DELETE FROM online WHERE UnixTime > UNIX_TIMESTAMP(now()) AND Mobitel_ID = {0}";

                    case MssqlUse:
                        return
                                "DELETE FROM online WHERE UnixTime > dbo.UNIX_TIMESTAMPS(getdate()) AND Mobitel_ID = {0}";
                    }

                    return "";
                }
            }
            public static string DeleteFromOnline64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return
                                    "DELETE FROM online64 WHERE UnixTime > UNIX_TIMESTAMP(now()) AND Mobitel_ID = {0}";

                        case MssqlUse:
                            return
                                    "DELETE FROM online64 WHERE UnixTime > dbo.UNIX_TIMESTAMPS(getdate()) AND Mobitel_ID = {0}";
                    }

                    return "";
                }
            }
        }
        // DataAnalysis
        public class UserBaseProvider
        {
            public static string SelectUserList {
                get {
                    return @"SELECT users_list.* FROM users_list WHERE users_list.Id = {0}";
                }
            }

            public static string SelectUserListOnName
            {
                get
                {
                    return @"SELECT users_list.* FROM users_list WHERE users_list.WinLogin = '{0}'";
                }
            }

            public static string InsertIntoUserList {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"INSERT INTO users_list (Name,Password,Admin,WinLogin,Id_role) 
                            Values (?Name, ?Password, ?Admin,?WinLogin,?Id_role)";

                    case MssqlUse:
                        return @"INSERT INTO users_list (Name,Password,Admin,WinLogin,Id_role) 
                            Values (@Name, @Password, @Admin,@WinLogin,@Id_role)";
                    }

                    return "";
                }
            }

            public static string UpdateUserList {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE  users_list Set Name = ?Name,Password = ?Password,
                                    Admin = ?Admin ,WinLogin = ?WinLogin,Id_role = ?Id_role 
                                    WHERE Id = {0}";

                    case MssqlUse:
                        return @"UPDATE  users_list Set Name = @Name,Password = @Password,
                                    Admin = @Admin ,WinLogin = @WinLogin,Id_role = @Id_role 
                                    WHERE Id = {0}";
                    }

                    return "";
                }
            }

            public static string SelectFromUserList {
                get {
                    return @"SELECT users_list.* FROM users_list ORDER BY users_list.Name";
                }
            }

            public static string ShowDatabases {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SHOW DATABASES";

                    case MssqlUse:
                        return @"SELECT * FROM sys.databases;";
                    }

                    return "";
                }
            }

            public static string SelectUsers_List {
                get {
                    return @"SELECT users_list.Id,users_list.Name FROM users_list ORDER BY users_list.Name";
                }
            }

            public static string DeleteFromUsersList {
                get {
                    return @"DELETE FROM users_list WHERE Id = {0}";
                }
            }

            public static string SelectCountUsersList {
                get {
                    return @"SELECT Count(*) FROM users_list WHERE Id <> {0} AND WinLogin = " + "{1}" + "WinLogin ";
                }
            }

            public static string InsertIntoUsersAction {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"INSERT INTO users_actions_log (TypeLog, DateAction, Id_document, TextLog, User, Computer) 
                            Values (?TypeLog, ?DateAction, ?Id_document, ?TextLog, ?User, ?Computer)";

                    case MssqlUse:
                        return @"INSERT INTO users_actions_log (TypeLog, DateAction, Id_document, TextLog, [User], Computer) 
                            Values (@TypeLog, @DateAction, @Id_document, @TextLog, @User, @Computer)";
                    }

                    return "";
                }
            }

            public static string SelectUsersActionsLog {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT users_actions_log.Id
                             , users_actions_log.DateAction
                             , users_actions_log.User
                             , users_actions_log.Computer
                             , users_actions_log.TextLog
                                FROM
                                users_actions_log
                                WHERE
                                users_actions_log.TypeLog = {0}
                                AND users_actions_log.Id_document = {1}
                                ORDER BY
                                users_actions_log.DateAction DESC,users_actions_log.Id DESC";

                    case MssqlUse:
                        return @"SELECT users_actions_log.Id
                             , users_actions_log.DateAction
                             , users_actions_log.[User]
                             , users_actions_log.Computer
                             , users_actions_log.TextLog
                                FROM
                                users_actions_log
                                WHERE
                                users_actions_log.TypeLog = {0}
                                AND users_actions_log.Id_document = {1}
                                ORDER BY
                                users_actions_log.DateAction DESC,users_actions_log.Id DESC";
                    }

                    return "";
                }
            }

            public static string SelectUsersAcsRoles {
                get {
                    return @"SELECT users_acs_roles.* FROM users_acs_roles ORDER BY users_acs_roles.Name";
                }
            }

            public static string InsertIntoUsersAcsRoles {
                get {
                    return @"INSERT INTO users_acs_roles (Name) Values (" + "{0}" + "Name)";
                }
            }

            public static string UpdateUsersAcsRoles {
                get {
                    return @"UPDATE users_acs_roles Set Name = " + "{1}" + "Name WHERE Id = {0}";
                }
            }

            public static string DeleteFromUsersAcsRoles {
                get {
                    return @"DELETE  FROM users_acs_roles WHERE Id = {0}";
                }
            }

            public static string SelectCountUsersListId {
                get {
                    return @"SELECT count(users_list.Id) AS CNT
                            FROM users_list WHERE   users_list.Id_role = {0}
                            GROUP BY users_list.Id_role";
                }
            }

            public static string SelectCountUsersAcsRolesId {
                get {
                    return @"SELECT count(users_acs_roles.Id) AS CNT 
                            FROM users_acs_roles GROUP BY users_acs_roles.Id";
                }
            }

            public static string SelectCountUsersListIdAs {
                get {
                    return @"SELECT count(users_list.Id) AS CNT FROM users_list GROUP BY users_list.Id";
                }
            }

            public static string SelectUsers_acs_roles {
                get {
                    return @"SELECT users_acs_roles.* FROM users_acs_roles
                            WHERE users_acs_roles.Id = {0}";
                }
            }

            public static string Select_users_acs_objects {
                get {
                    return @"SELECT users_acs_objects.Id_object
                            FROM users_acs_objects WHERE users_acs_objects.Id_type = {0} AND users_acs_objects.Id_role = {1}";
                }
            }

            public static string DeleteFrom_users_acs_objects {
                get {
                    return @"DELETE FROM users_acs_objects
                            WHERE users_acs_objects.Id_type = {0} AND users_acs_objects.Id_role = {1}";
                }
            }

            public static string INSERT_INTO_users_acs_objects {
                get {
                    return @"INSERT INTO users_acs_objects (Id_type,Id_role,Id_object) Values ({0},{1},{2})";
                }
            }
        }
        // UserBaseProvider
        public class DbCommon
        {
            public static string CheckTable (string connect, string table)
            {
                switch (TypeDataBaseForUsing) {
                case MySqlUse:
                    return string.Format (@"SELECT 1 FROM information_schema.tables WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}';", connect, table);

                case MssqlUse:
                    return string.Format (@"SELECT name FROM sysobjects WHERE name = '{0}' AND xtype = 'U'", table);
                }

                return "";
            }

            public static string CheckColumn (string connect, string table, string collumn)
            {
                switch (TypeDataBaseForUsing) {
                case MySqlUse:
                    return string.Format (@"SELECT 1 FROM information_schema.columns WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}' AND COLUMN_NAME = '{2}';", connect, table, collumn);

                case MssqlUse:
                    return string.Format (@"SELECT name FROM syscolumns WHERE id = object_id('{0}') AND name = '{1}'", table, collumn);
                }

                return "";
            }
        }
        // DbCommon
        public class DriverProvider
        {
            public static string InsertIntoDriver {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                            return @"INSERT INTO driver (Name, Family, ByrdDay, Category, Permis, Identifier, foto, OutLinkId, idType,Department,numTelephone)
                                    VALUES (@FirstName, @LastName, @DateOfBirth, @Categories, @DrivingLicence, @Identifier, @Photo, @OutLinkId, @TypeDriverId,@Department,@numTelephone);
                                    SELECT LAST_INSERT_ID()";

                    case MssqlUse:
                            return @"INSERT INTO driver (Name, Family, ByrdDay, Category, Permis, Identifier, foto, OutLinkId, idType,Department,numTelephone)
                                    VALUES (@FirstName, @LastName, @DateOfBirth, @Categories, @DrivingLicence, @Identifier, @Photo, @OutLinkId, @TypeDriverId,@Department,@numTelephone);
                                    SELECT max(id) FROM driver";
                    }

                    return "";
                }
            }

            public static string UpdateDriver {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE driver SET Name=@FirstName, Family=@LastName, ByrdDay=@DateOfBirth,
                                    Category=@Categories, Permis=@DrivingLicence, Identifier=@Identifier, foto=@Photo,OutLinkId=@OutLinkId,idType = @TypeDriverId,numTelephone=@numTelephone,Department=@Department
                                    WHERE id=@Id";

                    case MssqlUse:
                        return @"UPDATE driver SET Name=@FirstName, Family=@LastName, ByrdDay=@DateOfBirth,
                                    Category=@Categories, Permis=@DrivingLicence, Identifier=@Identifier, foto=@Photo, OutLinkId=@OutLinkId, idType=@TypeDriverId,numTelephone=@numTelephone,Department=@Department
                                    WHERE id=@Id";
                    }

                    return "";
                }
            }

            public static string DeleteFromDriver {
                get {
                    return @"DELETE FROM driver WHERE id=@Id";
                }
            }

            public static string SelectFromVehicle {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT {0} FROM vehicle WHERE vehicle.driver_id = {1}";

                    case MssqlUse:
                        return @"SELECT {0} FROM vehicle WHERE vehicle.driver_id = {1}";
                    }

                    return "";
                }
            }

            public static string SelectIdByOutLinkId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT   driver.Id FROM   driver WHERE   driver.OutLinkId = '{0}'";
                        case MssqlUse:
                            return "SELECT   driver.Id FROM   driver WHERE   driver.OutLinkId = N'{0}'";
                    }
                    return "";
                }
            }
        }
        // DriverProvider
        public class GpsDataProvider
        {
            public static string SELECT_From_Unixtime {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "SELECT From_Unixtime( max(mobitels.LastInsertTime)) AS LD FROM mobitels";

                    case MssqlUse:
                        return "SELECT dbo.From_Unixtime( max(mobitels.LastInsertTime)) AS LD FROM mobitels";
                    }

                    return "";
                }
            }

            public static string SelectFromMobitels {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"{0} FROM mobitels
                                    INNER JOIN datagps ON datagps.Mobitel_ID = mobitels.Mobitel_ID AND mobitels.ConfirmedID = datagps.LogID
                                    WHERE mobitels.Mobitel_ID = {1}  AND datagps.Valid = 1 AND NOT (Latitude = 0 AND Longitude = 0)";

                    case MssqlUse:
                        return @"{0} FROM mobitels
                                    INNER JOIN datagps ON datagps.Mobitel_ID = mobitels.Mobitel_ID AND mobitels.ConfirmedID = datagps.LogID
                                    WHERE mobitels.Mobitel_ID = {1}  AND datagps.Valid = 1 AND NOT (Latitude = 0 AND Longitude =0)";
                    }

                    return "";
                }
            }

            public static string SelectFromMobitels64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"{0} FROM mobitels
                                    INNER JOIN datagps64 ON datagps64.Mobitel_ID = mobitels.Mobitel_ID AND mobitels.ConfirmedID = datagps64.LogID
                                    WHERE mobitels.Mobitel_ID = {1}  AND datagps64.Valid = 1 AND NOT (Latitude = 0 AND Longitude = 0)";

                        case MssqlUse:
                            return @"{0} FROM mobitels
                                    INNER JOIN datagps64 ON datagps64.Mobitel_ID = mobitels.Mobitel_ID AND mobitels.ConfirmedID = datagps64.LogID
                                    WHERE mobitels.Mobitel_ID = {1}  AND datagps64.Valid = 1 AND NOT (Latitude = 0 AND Longitude =0)";
                    }

                    return "";
                }
            }

            public static string SelectFromDatagps {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"{0} FROM datagps
                                    INNER JOIN  mobitels
                                    ON datagps.Mobitel_ID = mobitels.Mobitel_ID 
                                    WHERE
                                    mobitels.Mobitel_ID = {1}
                                    AND datagps.Valid = 1
                                    AND datagps.UnixTime <= (SELECT LastInsertTime FROM mobitels
                                    WHERE mobitels.Mobitel_ID = {1})
                                    AND from_unixtime(datagps.UnixTime)<= ?TodayNow ORDER BY datagps.UnixTime DESC Limit 1";

                    case MssqlUse:
                        return @"{0} FROM datagps
                                    INNER JOIN mobitels
                                    ON datagps.Mobitel_ID = mobitels.Mobitel_ID 
                                    WHERE
                                    mobitels.Mobitel_ID = {1}
                                    AND datagps.Valid = 1
                                    AND datagps.UnixTime <= (SELECT TOP 1 LastInsertTime FROM mobitels
                                    WHERE mobitels.Mobitel_ID = {1})
                                    AND dbo.from_unixtime(datagps.UnixTime)<= @TodayNow ORDER BY datagps.UnixTime DESC";
                    }

                    return "";
                }
            }

                public static string SelectFromDatagps64 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"{0} FROM datagps64
                                    INNER JOIN  mobitels
                                    ON datagps64.Mobitel_ID = mobitels.Mobitel_ID 
                                    WHERE
                                    mobitels.Mobitel_ID = {1}
                                    AND datagps64.Valid = 1
                                    AND datagps64.UnixTime <= (SELECT LastInsertTime FROM mobitels
                                    WHERE mobitels.Mobitel_ID = {1})
                                    AND from_unixtime(datagps64.UnixTime)<= ?TodayNow ORDER BY datagps64.UnixTime DESC Limit 1";

                    case MssqlUse:
                        return @"{0} FROM datagps64
                                    INNER JOIN mobitels
                                    ON datagps64.Mobitel_ID = mobitels.Mobitel_ID 
                                    WHERE
                                    mobitels.Mobitel_ID = {1}
                                    AND datagps64.Valid = 1
                                    AND datagps64.UnixTime <= (SELECT TOP 1 LastInsertTime FROM mobitels
                                    WHERE mobitels.Mobitel_ID = {1})
                                    AND dbo.from_unixtime(datagps64.UnixTime)<= @TodayNow ORDER BY datagps64.UnixTime DESC";
                    }

                    return "";
                }
            }

            public static string SelectFromDatagpsUnixTime {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"{0} FROM datagps
                                    WHERE datagps.UnixTime BETWEEN UNIX_TIMESTAMP(?TimeStart) AND UNIX_TIMESTAMP(?TimeEnd) AND datagps.Valid = 1";

                    case MssqlUse:
                        return @"{0} FROM datagps
                                    WHERE datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@TimeStart) AND dbo.UNIX_TIMESTAMPS(@TimeEnd) AND datagps.Valid = 1";
                    }

                    return "";
                }
            }

            public static string SelectFromDatagpsId
            {
                get
                {
                    return @"{0} FROM datagps
                            WHERE datagps.Mobitel_ID = {1} AND datagps.DataGps_ID >= {2} AND datagps.DataGps_ID <= {3}  AND datagps.Valid = 1  AND NOT (Latitude = 0 AND Longitude =0) ORDER BY datagps.LogID";

                }
            }
        }
        // GpsDataProvider
        public class OnlineDataProvider
        {
            public static string GetOnlineDataMobitel {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return GET_ONLINE_DATA_MOBITEL;

                    case MssqlUse:
                        return msGET_ONLINE_DATA_MOBITEL;
                    }

                    return "";
                }
            }

            public static string GetOnlineDataMobitel64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return GET_ONLINE_DATA_MOBITEL_64;

                        case MssqlUse:
                            return msGET_ONLINE_DATA_MOBITEL_64;
                    }

                    return "";
                }
            }

            public static string GetOnlineDataMobitelLast {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return GET_ONLINE_DATA_MOBITEL_LAST;

                    case MssqlUse:
                        return msGET_ONLINE_DATA_MOBITEL_LAST;
                    }

                    return "";
                }
            }

            public static string GetOnlineDataMobitelLast64
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return GET_ONLINE_DATA_MOBITEL_LAST_64;

                        case MssqlUse:
                            return msGET_ONLINE_DATA_MOBITEL_LAST_64;
                    }

                    return "";
                }
            }

            public static string GetOnlineData {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return GET_ONLINE_DATA;

                    case MssqlUse:
                        return msGET_ONLINE_DATA;
                    }

                    return "";
                }
            }
        }
        // OnlineDataProvider
        public class ReportPassZonesProvider
        {
            public static string SelectReportPassZones {
                get {
                    return "SELECT * FROM report_pass_zones";
                }
            }

            public static string InsertIntoReportPassZones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO report_pass_zones (ZoneID) VALUES (@ZoneID); " +
                        "SELECT ID FROM report_pass_zones WHERE ID = LAST_INSERT_ID()";

                    case MssqlUse:
                        return "INSERT INTO report_pass_zones (ZoneID) VALUES (@ZoneID); " +
                        "SELECT ID FROM report_pass_zones WHERE ID = (SELECT max(id) FROM report_pass_zones)";
                    }

                    return "";
                }
            }

            public static string DeleteFromReportPassZones {
                get {
                    return "DELETE FROM report_pass_zones WHERE ID = @ID";
                }
            }
        }
        // ReportPassZonesProvider
        public class VehicleProvider
        {
            public static string SelectVehicleFromVehicle {
                get {
                    return @"SELECT vehicle.* FROM vehicle";
                }
            }

            public static string SelectVehicleFromVehicleMobitelSettings
            {
                get
                {

                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT
                            vehicle.*,
                            mobitels.*,
                            setting.*,
                            team.*,
                            setting.id as Sid,
                            vehicle.Mobitel_id as Mid,
                            team.Name as Tname,
                            From_UnixTime(mobitels.LastInsertTime) as LastTimeGps,
                            (SELECT internalmobitelconfig.devIdShort
                            FROM  mobitels
                            INNER JOIN internalmobitelconfig
                            ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                            WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                            FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                            AND  mobitels.Mobitel_ID = vehicle.Mobitel_id) as devIdShort
                            FROM vehicle
                            LEFT OUTER JOIN setting
                            ON vehicle.setting_id = setting.id
                            INNER JOIN mobitels
                            ON vehicle.Mobitel_id = mobitels.Mobitel_ID
                            LEFT OUTER JOIN team
                            ON vehicle.Team_id = team.id";

                        case MssqlUse:
                            return @"SELECT
                            vehicle.*,
                            mobitels.*,
                            setting.*,
                            team.*,
                            setting.id as Sid,
                            vehicle.Mobitel_id as Mid,
                            team.Name as Tname,
                            dbo.From_UnixTime(mobitels.LastInsertTime) as LastTimeGps,
                            (SELECT internalmobitelconfig.devIdShort
                            FROM  mobitels
                            INNER JOIN internalmobitelconfig
                            ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                            WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                            FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                            AND  mobitels.Mobitel_ID = vehicle.Mobitel_id) as devIdShort
                            FROM vehicle
                            LEFT OUTER JOIN setting
                            ON vehicle.setting_id = setting.id
                            INNER JOIN mobitels
                            ON vehicle.Mobitel_id = mobitels.Mobitel_ID
                            LEFT OUTER JOIN team
                            ON vehicle.Team_id = team.id";
                    }
                    return "";
                }
            }

            public static string SelectVehicleMobitel_id {
                get {
                    return @"SELECT vehicle.Mobitel_id as TID, team.Name as GName,  vehicle.MakeCar , vehicle.CarModel, vehicle.NumberPlate, driver.Name as DName
                            FROM vehicle LEFT OUTER JOIN team ON vehicle.Team_id = team.id LEFT OUTER JOIN driver ON vehicle.driver_id = driver.id
                            ORDER BY team.Name, vehicle.MakeCar";
                }
            }

            public static string SelectVehicleMobitelIDLogic {
                get {
                    return @"SELECT vehicle.Mobitel_id as TID, team.Name as GName,  vehicle.MakeCar , vehicle.CarModel, vehicle.NumberPlate,sensors.id as SID, sensors.Name as SName
                            FROM vehicle LEFT OUTER JOIN team ON vehicle.Team_id = team.id INNER JOIN sensors ON vehicle.Mobitel_id = sensors.mobitel_id
                            WHERE sensors.Length = 1 ORDER BY team.Name, vehicle.MakeCar";
                }
            }

            public static string SelectVehicleMobitelIDLogicAlgorithm
            {
                get
                {
                    return @"SELECT vehicle.Mobitel_id as TID, team.Name as GName,  vehicle.MakeCar , vehicle.CarModel, vehicle.NumberPlate,sensors.id as SID, sensors.Name as SName
                            FROM vehicle LEFT OUTER JOIN team ON vehicle.Team_id = team.id INNER JOIN sensors ON vehicle.Mobitel_id = sensors.mobitel_id LEFT OUTER JOIN relationalgorithms ON sensors.id = relationalgorithms.SensorID LEFT OUTER JOIN sensoralgorithms ON relationalgorithms.AlgorithmID = sensoralgorithms.ID
                            WHERE sensors.Length = 1 and sensoralgorithms.ID = {0} ORDER BY team.Name, vehicle.MakeCar";
                }
            }

            public static string SelectFromVehicleMobitelId {
                get {
                    return @"SELECT vehicle.Mobitel_id as TID, team.Name as GName,  vehicle.MakeCar, vehicle.CarModel, vehicle.NumberPlate,sensors.id as SID, sensors.Name as SName, sensoralgorithms.Name as AName
                            FROM vehicle LEFT OUTER JOIN team ON vehicle.Team_id = team.id INNER JOIN sensors ON vehicle.Mobitel_id = sensors.mobitel_id LEFT OUTER JOIN relationalgorithms ON sensors.id = relationalgorithms.SensorID LEFT OUTER JOIN sensoralgorithms ON relationalgorithms.AlgorithmID = sensoralgorithms.ID
                            WHERE sensors.Length > 1 ORDER BY team.Name, vehicle.MakeCar";
                }
            }

            public static string SelectVehicleMobitelTid {
                get {
                    return @"SELECT vehicle.Mobitel_id as TID, team.Name as GName,  vehicle.MakeCar, vehicle.CarModel, vehicle.NumberPlate,sensors.id as SID, sensors.Name as SName, sensoralgorithms.Name as AName
                            FROM vehicle LEFT OUTER JOIN team ON vehicle.Team_id = team.id INNER JOIN sensors ON vehicle.Mobitel_id = sensors.mobitel_id LEFT OUTER JOIN relationalgorithms ON sensors.id = relationalgorithms.SensorID LEFT OUTER JOIN sensoralgorithms ON relationalgorithms.AlgorithmID = sensoralgorithms.ID
                            WHERE sensoralgorithms.ID = {0} ORDER BY team.Name, vehicle.MakeCar";
                }
            }

            public static string InsertOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  INSERT_ONE;

                    case MssqlUse:
                        return msINSERT_ONE;
                    }

                    return "";
                }
            }

            public static string SelectIdFromLines {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  @"SELECT Id From `lines` WHERE Mobitel_ID = {0}";

                    case MssqlUse:
                        return @"SELECT Id From lines WHERE Mobitel_ID = {0}";
                    }

                    return "";
                }
            }

            public static string InsertIntoLines {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO `lines` (Mobitel_ID,Color) Values ({0},{1})";

                    case MssqlUse:
                        return "INSERT INTO lines (Mobitel_ID,Color) Values ({0},{1})";
                    }

                    return "";
                }
            }

            public static string UpdateLines {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "UPDATE `lines` SET Color = {1}   WHERE id = {0}";

                    case MssqlUse:
                        return "UPDATE lines SET Color = {1}   WHERE id = {0}";
                    }

                    return "";
                }
            }

            public static string UpdateOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  UPDATE_ONE;

                    case MssqlUse:
                        return msUPDATE_ONE;
                    }

                    return "";
                }
            }
            public static string UpdateMobitel{
                get
                {
                    return "UPDATE mobitels SET Is64bitPackets = {0}Is64bitPackets,IsNotDrawDgps = {0}IsNotDrawDgps    WHERE Mobitel_ID = {1}";

                }
            }

            public static string SelectFromVehicle {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  @"SELECT {0} FROM  vehicle WHERE   vehicle.Mobitel_id = {1}";

                    case MssqlUse:
                        return @"SELECT {0} FROM  vehicle WHERE   vehicle.Mobitel_id = {1}";
                    }

                    return "";
                }
            }
        }
        // VehicleProvider
        public class VehiclesGroupProvider
        {
            public static string SelectAll {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  SELECT_ALL;

                    case MssqlUse:
                        return msSELECT_ALL;
                    }

                    return "";
                }
            }

            public static string InsertOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  _INSERT_ONE;

                    case MssqlUse:
                        return _msINSERT_ONE;
                    }

                    return "";
                }
            }

            public static string UpdateOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  _UPDATE_ONE;

                    case MssqlUse:
                        return _msUPDATE_ONE;
                    }

                    return "";
                }
            }

            public static string DeleteOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  DELETE_ONE;

                    case MssqlUse:
                        return msDELETE_ONE;
                    }

                    return "";
                }
            }
        }
        // VehiclesGroupProvider
        public class ZoneGroupsProvider
        {
            public static string SelectAll {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  SELECT_ALL_;

                    case MssqlUse:
                        return msSELECT_ALL_;
                    }

                    return "";
                }
            }

            public static string InsertOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  INSERT_ONE_;

                    case MssqlUse:
                        return msINSERT_ONE_;
                    }

                    return "";
                }
            }

            public static string UpdateOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  UPDATE_ONE_;

                    case MssqlUse:
                        return msUPDATE_ONE_;
                    }

                    return "";
                }
            }

            public static string DeleteOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  DELETE_ONE_;

                    case MssqlUse:
                        return msDELETE_ONE_;
                    }

                    return "";
                }
            }
        }
        // ZoneGroupsProvider
        public class ZonesProvider
        {
            public static string UpdateZones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  @"UPDATE `zones` SET `Name`=@Title, `Descr`=@Description, `ZoneColor`=@Color, `Square`=@Square, `ZonesGroupId`=@ZonesGroupId, `OutLinkId`=@OutLinkId, `Category_id`=@Category_id, `Category_id2`=@Category_id2 WHERE `Zone_ID`=@ZoneId";

                    case MssqlUse:
                        return @"UPDATE zones SET Name=@Title, Descr=@Description, ZoneColor=@Color, Square=@Square, ZonesGroupId=@ZonesGroupId, OutLinkId=@OutLinkId, Category_id=@Category_id, Category_id2=@Category_id2 WHERE Zone_ID=@ZoneId";
                    }

                    return "";
                }
            }

            public static string DeleteFromPoints {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  @"DELETE FROM `points` WHERE `Zone_ID` = @ZoneId; UPDATE `zones` SET `Name`=@Title, `Descr`=@Description, `ZoneColor`=@Color, `Square`=@Square, `ZonesGroupId`=@ZonesGroupId,`DateCreate` = @DateCreate WHERE `Zone_ID`=@ZoneId";

                    case MssqlUse:
                        return @"DELETE FROM points WHERE Zone_ID = @ZoneId; UPDATE zones SET Name=@Title, Descr=@Description, ZoneColor=@Color, Square=@Square, ZonesGroupId=@ZonesGroupId,DateCreate = @DateCreate WHERE Zone_ID=@ZoneId";
                    }

                    return "";
                }
            }

            public static string InsertIntoPoints {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return  "INSERT INTO `points` (`Latitude`, `Longitude`, `Zone_ID`) VALUES ";

                    case MssqlUse:
                        return "INSERT INTO points (Latitude, Longitude, Zone_ID) VALUES ";
                    }

                    return "";
                }
            }

            public static string CommandSel {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"INSERT INTO `zones` (`Name`, `Descr`, `ZoneColor`, `Square`, `ZonesGroupId`, `DateCreate`,`OutLinkId`, `Category_id`, `Category_id2`) VALUES (@Title, @Description, @Color, @Square, @ZonesGroupId, @DateCreate, @OutLinkId, @Category_id, @Category_id2); SELECT LAST_INSERT_ID()";

                    case MssqlUse:
                        return @"INSERT INTO zones (Name, Descr, ZoneColor, Square, ZonesGroupId, DateCreate,OutLinkId, Category_id, Category_id2) VALUES (@Title, @Description, @Color, @Square, @ZonesGroupId,@DateCreate,@OutLinkId,@Category_id,@Category_id2); SELECT max(id) FROM zones";
                    }

                    return "";
                }
            }

            public static string InsertPoints {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO `points` (`Latitude`, `Longitude`, `Zone_ID`) VALUES ";

                    case MssqlUse:
                        return "INSERT INTO points (Latitude, Longitude, Zone_ID) VALUES ";
                    }

                    return "";
                }
            }

            public static string UpdateZonesSetZonesGroupId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "UPDATE `zones` SET `ZonesGroupId`=@GroupId WHERE `Zone_ID` IN ({0})";

                    case MssqlUse:
                        return "UPDATE zones SET ZonesGroupId=@GroupId WHERE Zone_ID IN ({0})";
                    }

                    return "";
                }
            }

            public static string DeleteZones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"DELETE FROM `zones` WHERE `Zone_ID`=@ZoneId";

                    case MssqlUse:
                        return @"DELETE FROM zones WHERE Zone_ID=@ZoneId";
                    }

                    return "";
                }
            }

            public static string SelectAgrofildName {
                get {
                    return @"SELECT agro_field.Name FROM agro_field WHERE agro_field.Id_zone = @ZoneId";
                }
            }

            public static string SelectFromZones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT * FROM `zones` WHERE `ZonesGroupId`= @ZonesGroupId";

                    case MssqlUse:
                        return @"SELECT * FROM zones WHERE ZonesGroupId = @ZonesGroupId";
                    }

                    return "";
                }
            }

            public static string Select_From_Zones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT * FROM `zones`";

                    case MssqlUse:
                        return @"SELECT * FROM zones";
                    }

                    return "";
                }
            }

            public static string Select_Points {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return SELECT_POINTS;

                    case MssqlUse:
                        return msSELECT_POINTS;
                    }

                    return "";
                }
            }

            public static string SelectZonesId {
                get {
                    return @"SELECT zones.* FROM  zones WHERE zones.Zone_ID = @ID";
                }
            }

            public static string SelectZonesGroup {
                get {
                    return @"SELECT zonesgroup.* FROM  zonesgroup WHERE zonesgroup.Id = @ID";
                }
            }

            public static string SelectZonesListWithGroups {
                get {
                    return "SELECT zonesgroup.Id , zonesgroup.Title, zones.Zone_ID, zones.Name, zones.Square, zones.ZoneColor, zones.Category_id, zones.Category_id2"
                    + " FROM zones INNER JOIN zonesgroup ON zones.ZonesGroupId = zonesgroup.Id";
                }
            }
        }// ZonesProvider

        public class NoticeProvider
        {
            public static string InsertIntoNtf_Main {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                            return "INSERT INTO ntf_main (IsActive, IsPopup, Title, IconSmall, IconLarge, SoundName, TimeForControl, IsEmailActive,Period) VALUES (?IsActive, ?IsPopup, ?Title, ?IconSmall, ?IconLarge, ?SoundName, ?TimeForControl, ?IsEmailActive,?Period)";

                    case MssqlUse:
                            return "INSERT INTO ntf_main (IsActive, IsPopup, Title, IconSmall, IconLarge, SoundName, TimeForControl, IsEmailActive,Period) VALUES (@IsActive, @IsPopup, @Title, @IconSmall, @IconLarge, @SoundName, @TimeForControl, @IsEmailActive,@Period)";
                    }

                    return "";
                }
            }

            public static string UpdateNtf_Main {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"UPDATE ntf_main SET IsActive = ?IsActive ,IsPopup = ?IsPopup 
                                ,Title = ?Title, IconSmall = ?IconSmall, IconLarge = ?IconLarge
                                , SoundName = ?SoundName,TimeForControl = ?TimeForControl
                                ,IsEmailActive=?IsEmailActive,Period=?Period
                                 WHERE ( Id = {0})";

                    case MssqlUse:
                        return @"UPDATE ntf_main SET IsActive = @IsActive ,IsPopup = @IsPopup 
                                ,Title = @Title, IconSmall = @IconSmall, IconLarge = @IconLarge
                                , SoundName = @SoundName,TimeForControl = @TimeForControl
                                ,IsEmailActive=@IsEmailActive,Period=@Period
                                 WHERE ( Id = {0})";
                    }

                    return "";
                }
            }

            public static string DeleteFromNtf_mobitels {
                get {
                    return "DELETE FROM ntf_mobitels WHERE Id_main = {0}";
                }
            }

            public static string InsertIntoNtf_mobitels {
                get {
                    return @"INSERT INTO ntf_mobitels (Id_main, MobitelId, SensorLogicId, SensorId) 
                            VALUES ({0}Id_main, {0}MobitelId, {0}SensorLogicId, {0}SensorId)";
                }
            }

            public static string DeleteFromNtf_events {
                get {
                    return "DELETE FROM ntf_events WHERE Id_main = {0} AND TypeEvent = {1}";
                }
            }

            public static string InsertIntoNtf_events {
                get 
                    {
                        return "INSERT INTO ntf_events (Id_main, TypeEvent, EventId, ParInt1) VALUES ({0}Id_main, {0}TypeEvent, {0}EventId, {0}ParInt1)";
                    }
                }
            public static string DeleteNtf_events {
                get {
                    return "DELETE FROM ntf_events WHERE Id_main = {0} AND TypeEvent = {1}";
                }
            }

            public static string InsertNtf_events {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent,ParDbl1) VALUES (" + "{0}" +
                    "Id_main, " + "{0}" + "TypeEvent, " + "{0}" + "ParDbl1)";
                }
            }

            public static string DeleteNtfEventsIdMain {
                get {
                    return "DELETE FROM ntf_events WHERE Id_main = {0} AND TypeEvent = {1}";
                }
            }

            public static string Insert_Into_Ntf_Events_WithoutParams
            {
                get
                {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent) VALUES (" + "{0}" +
                    "Id_main, " + "{0}" + "TypeEvent)";
                }
            }

            public static string Insert_Into_Ntf_Events {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent, ParBool) VALUES (" + "{0}" +
                    "Id_main, " + "{0}" + "TypeEvent, " + "{0}" + "ParBool)";
                }
            }

            public static string Insert_Into_Ntf_Events_Id {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent, ParBool, ParDbl1, ParDbl2) VALUES (" +
                    "{0}" + "Id_main, " + "{0}" + "TypeEvent, " + "{0}" +
                    "ParBool, " + "{0}" + "ParDbl1, " + "{0}" + "ParDbl2)";
                }
            }

            public static string InsertIntoNtfEventsValue {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent, ParDbl1, ParDbl2) VALUES (" + "{0}" +
                    "Id_main, " + "{0}" + "TypeEvent, " + "{0}" + "ParDbl1, " +
                    "{0}" + "ParDbl2)";
                }
            }

            public static string InsertIntoNtfEventsParam {
                get {
                    return @"INSERT INTO ntf_events (Id_main, TypeEvent, ParInt1, ParInt2, ParDbl1, ParDbl2, ParBool) 
                            VALUES ({0}Id_main, {0}TypeEvent, {0}ParInt1, {0}ParInt2, {0}ParDbl1, {0}ParDbl2, {0}ParBool)";
                }
            }

            public static string InsertIntoNtfEventsTypeEvent {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent, ParDbl1) VALUES (" + "{0}" +
                    "Id_main, " + "{0}" + "TypeEvent, " + "{0}" + "ParDbl1)";
                }
            }

            public static string InsertIntoNtfEventsParDbl {
                get {
                    return "INSERT INTO ntf_events (Id_main, TypeEvent, ParDbl1, ParDbl2, ParInt1) VALUES ({0}Id_main, {0}TypeEvent, {0}ParDbl1, {0}ParDbl2, {0}ParInt1 )";
                }
            }

            public static string Select_ntf_main_Id {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT ntf_main.Id , ntf_main.Title,ntf_main.IconSmall,ntf_main.IsActive,ntf_main.IsPopup,
               (SELECT count(ntf_emails.Id) AS CNT FROM   ntf_emails WHERE   ntf_emails.Id_main = ntf_main.Id GROUP BY  ntf_emails.Id_main) as CNT_MAIL,
               (SELECT count(ntf_mobitels.Id) AS CNT FROM   ntf_mobitels WHERE   ntf_mobitels.Id_main = ntf_main.Id GROUP BY  ntf_mobitels.Id_main) as CNT_VEH,
               (SELECT count(ntf_events.Id) AS CNT FROM   ntf_events WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {0}  GROUP BY   ntf_events.Id_main) as CNT_ZONE,
               (SELECT ntf_events.ParDbl1 FROM ntf_events  WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {1} LIMIT 1) as SPEED_MIN,
               (SELECT ntf_events.ParDbl2 FROM ntf_events  WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {1} LIMIT 1) as SPEED_MAX,
               (SELECT ntf_events.ParDbl1 FROM ntf_events  WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {2} LIMIT 1) as STOP,
               (SELECT ntf_events.ParDbl1 FROM ntf_events  WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {3} LIMIT 1) as LOSS_DATA,
               (SELECT ntf_events.ParDbl1 FROM ntf_events  WHERE   ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {4} LIMIT 1) as LOSS_GPS
               FROM   ntf_main ORDER BY   ntf_main.Title";

                    case MssqlUse:
                        return @"SELECT ntf_main.Id, ntf_main.Title, ntf_main.IconSmall, ntf_main.IsActive, ntf_main.IsPopup, 
                (SELECT count(ntf_emails.Id) AS CNT FROM ntf_emails WHERE ntf_emails.Id_main = ntf_main.Id GROUP BY ntf_emails.Id_main) AS CNT_MAIL, 
                (SELECT count(ntf_mobitels.Id) AS CNT FROM ntf_mobitels WHERE ntf_mobitels.Id_main = ntf_main.Id GROUP BY ntf_mobitels.Id_main) AS CNT_VEH, 
                (SELECT count(ntf_events.Id) AS CNT FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = 0 GROUP BY ntf_events.Id_main) AS CNT_ZONE, 
                (SELECT TOP 1 ntf_events.ParDbl1 FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {1}) AS SPEED_MIN, 
                (SELECT TOP 1 ntf_events.ParDbl2 FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {1}) AS SPEED_MAX, 
                (SELECT TOP 1 ntf_events.ParDbl1 FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {2}) AS STOP, 
                (SELECT TOP 1 ntf_events.ParDbl1 FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {3}) AS LOSS_DATA, 
                (SELECT TOP 1 ntf_events.ParDbl1 FROM ntf_events WHERE ntf_events.Id_main = ntf_main.Id AND ntf_events.TypeEvent = {4}) AS LOSS_GPS FROM ntf_main ORDER BY ntf_main.Title";
                    }

                    return "";
                }
            }

            public static string SelectNtfMainFromNtfMain {
                get {
                    return "SELECT ntf_main.Id FROM   ntf_main WHERE IsActive = 1 ORDER BY   ntf_main.Title";
                }
            }

            public static string SelectFromNtfMainIsActiveOrderBy {
                get {
                    return string.Format("{0} {1}", SelectFromNtfMainIsActive, "ORDER BY ntf_main.Title"); //
                }
            }

            public static string SelectFromNtfMainIsActive
            {
                get
                {
                    return "SELECT ntf_main.* FROM ntf_main WHERE IsActive = 1"; 
                }
            }

            public static string DeleteFromNtfLog {
                get {
                    return "DELETE FROM ntf_log WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfEvents {
                get {
                    return "DELETE FROM ntf_events WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfEmails {
                get {
                    return "DELETE FROM ntf_emails WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfMobitels {
                get {
                    return "DELETE FROM ntf_mobitels WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfPopupusers {
                get {
                    return "DELETE FROM ntf_popupusers WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfMainId {
                get {
                    return "DELETE FROM ntf_main WHERE ID = {0}";
                }
            }

            public static string SelectNtfEventsMainId {
                get {
                    return "SELECT * FROM ntf_events WHERE ID_main = {0}";
                }
            }

            public static string SelectNtfEventsTypeEvent {
                get {
                    return "SELECT * FROM ntf_events WHERE ID_main = {0}  AND  TypeEvent <> {1}";
                }
            }

            public static string SelectNtfMobitels {
                get {
                    return "SELECT * FROM ntf_mobitels WHERE ID_main = {0}";
                }
            }

            public static string SelectNtfAgroWorks
            {
                get
                {
                    return "SELECT * FROM ntf_agro_works WHERE ID_main = {0}";
                }
            }

            public static string SelectNtfMobitelsId_main {
                get {
                    return "SELECT * FROM ntf_mobitels WHERE ID_main = {0} AND (SensorId > 0 OR SensorLogicId > 0)";
                }
            }

            public static string SelectNtfMobitelsSensorId {
                get {
                    return "SELECT * FROM ntf_mobitels WHERE ID_main = {0} AND SensorId = 0 AND SensorLogicId = 0";
                }
            }

            public static string SelectNtfMobitelsFromAgroWorks
            {
                get
                {
                    return @"SELECT DISTINCT vehicle.Mobitel_id,agro_agregat.Id_work,agro_agregat.Id as AgregatId FROM agro_agregat_vehicle
                    INNER JOIN agro_agregat ON agro_agregat_vehicle.Id_agregat = agro_agregat.Id
                    INNER JOIN vehicle ON agro_agregat_vehicle.Id_vehicle = vehicle.id
                    INNER JOIN agro_work  ON agro_agregat.Id_work = agro_work.Id
                    WHERE NOT (agro_work.SpeedBottom=0 AND agro_work.SpeedTop=0)";
                }
            }

            public static string SelectNtfMobitelsFromSensorAlgorithm
            {
                get
                {
                    return @"SELECT sensors.mobitel_id,sensors.id as SensorId
                            FROM  relationalgorithms
                            INNER JOIN sensors
                            ON relationalgorithms.SensorID = sensors.id
                            WHERE
                            relationalgorithms.AlgorithmID = {0}";
                }
            }

            public static string SelectFromNtfMainID {
                get {
                    return "SELECT * FROM ntf_main WHERE ID = {0}";
                }
            }

            public static string InsertIntoNtfLogValue {
                get {
                    return @"INSERT INTO ntf_log (DateEvent, Mobitel_id, Location, Infor, Lng, Lat, DataGps_ID, Id_main, TypeEvent, Zone_id, Speed, DateWrite, Value) 
                        VALUES ({0}DateEvent, {0}Mobitel_id, {0}Location, {0}Infor, {0}Lng, {0}Lat, {0}DataGps_ID, {0}Id_main, {0}TypeEvent, {0}Zone_id, {0}Speed, {0}DateWrite, {0}Value)";
                }
            }

            public static string DeleteFromNtfLogByDataGps_ID
            {
                get
                {
                    return @"DELETE FROM ntf_log WHERE DataGps_ID ={0} and TypeEvent = {1}";
                }
            }

            public static string SelectCountNtf_Log {
                get {
                    return @"SELECT count(ntf_log.DataGps_ID) AS CNT FROM   
                    ntf_log WHERE   ntf_log.DataGps_ID = {0} AND ntf_log.Id_main = {1}";
                }
            }

            public static string SelectCountNtfLogDataGps {
                get {
                    return @"SELECT count(ntf_log.DataGps_ID) AS CNT FROM ntf_log
                            WHERE ntf_log.DataGps_ID = 0 AND ntf_log.Mobitel_id = {1} AND (ntf_log.DateEvent BETWEEN {0}DateStart and {0}DateEnd)
                            AND ntf_log.Id_main = {2}";
                }
            }

            public static string DeleteFromNtfLogDateWrite {
                get {
                    return @"DELETE FROM ntf_log WHERE ntf_log.DateWrite < " + "{0}" + "DateWrite";
                }
            }

            public static string WhereVehicleId {
                get {
                    return @"WHERE vehicle.id NOT IN (SELECT users_acs_objects.Id_object
                            FROM   users_acs_objects
                            WHERE   users_acs_objects.Id_role = {0}
                            AND users_acs_objects.Id_type = {1})";
                }
            }

            public static string SelectNtfLogParam {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @" SELECT ntf_log.Id , ntf_log.DateEvent , ntf_main.IconSmall , ntf_main.IsPopup ,{0} as VehicleFullName , ntf_log.Infor , ntf_log.IsRead, 
                            ntf_log.Mobitel_id , ntf_log.Lat, ntf_log.Lng, ntf_log.TypeEvent, ntf_log.Zone_id, ntf_log.Id_main, ntf_log.Speed
                            FROM ntf_log INNER JOIN ntf_main ON ntf_log.Id_main = ntf_main.Id
                            LEFT OUTER JOIN vehicle ON ntf_log.Mobitel_id = vehicle.Mobitel_id {1}
                            ORDER BY ntf_log.DateEvent DESC";

                    case MssqlUse:
                        return @" SELECT ntf_log.Id , ntf_log.DateEvent , ntf_main.IconSmall , ntf_main.IsPopup ,{0} as VehicleFullName , ntf_log.Infor , ntf_log.IsRead, 
                                ntf_log.Mobitel_id, ntf_log.Lat, ntf_log.Lng, ntf_log.TypeEvent, ntf_log.Zone_id, ntf_log.Id_main, ntf_log.Speed
                                FROM ntf_log INNER JOIN ntf_main ON ntf_log.Id_main = ntf_main.Id
                                LEFT OUTER JOIN vehicle ON ntf_log.Mobitel_id = vehicle.Mobitel_id {1}
                                ORDER BY ntf_log.DateEvent DESC";
                    }

                    return "";
                }
            }

            public static string SelectNtfLogData {
                get {
                    return @" SELECT ntf_log.Id, ntf_log.DateEvent, ntf_log.Location  
                        FROM ntf_log LEFT OUTER JOIN vehicle ON ntf_log.Mobitel_id = vehicle.Mobitel_id {0}";
                }
            }

            public static string Delete_Ntf_Log {
                get {
                    return "DELETE FROM ntf_log";
                }
            }

            public static string UpdateNtfMain {
                get {
                    return "UPDATE ntf_main SET IsActive = {1} WHERE Id= {0}";
                }
            }

            public static string UpdateNtfLog {
                get {
                    return "UPDATE ntf_log SET IsRead = 1 WHERE Id= {0}";
                }
            }

            public static string SelectNtfLogIdFromNtfLog {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT ntf_log.Id FROM  ntf_log ORDER BY  ntf_log.Id DESC LIMIT 1";

                    case MssqlUse:
                        return @"SELECT TOP 1 ntf_log.Id FROM  ntf_log ORDER BY  ntf_log.Id DESC";
                    }

                    return "";
                }
            }

            public static string SelectNtfLogMobitelId {
                get {
                    return @"SELECT ntf_log.Mobitel_id
                    FROM ntf_log
                    WHERE ntf_log.TypeEvent = {0} AND ntf_log.Value = {1} AND ntf_log.DateEvent between " +
                    "{2}" + "TimeBegin and " + "{2}" + "TimeEnd" +
                    " ORDER BY ntf_log.DateEvent ";
                }
            }

            public static string SelectNtfLogZoneId {
                get {
                    return @"SELECT ntf_log.Zone_id FROM ntf_log
                            WHERE ntf_log.TypeEvent = {0} AND ntf_log.Mobitel_id = {1} AND ntf_log.DateEvent between " +
                    "{3}" + "TimeBegin and " + "{3}" + "TimeEnd" +
                    " AND (SELECT count(ntf_events.Id) AS CNT FROM   ntf_events WHERE   ntf_events.Id_main = ntf_log.Id_main" +
                    " AND ntf_events.TypeEvent = {2} GROUP BY   ntf_events.Id_main , ntf_events.TypeEvent) > 0 " +
                    " ORDER BY ntf_log.DateEvent ";
                }
            }

            public static string SelectNtfEmailsId {
                get {
                    return @"SELECT ntf_emails.Id, ntf_emails.IsActive, ntf_emails.Email
                            FROM ntf_emails WHERE ntf_emails.Id_main = {0}";
                }
            }

            public static string SelectNtfPopupUsersId {
                get {
                    return @"SELECT ntf_popupUsers.Id, ntf_popupUsers.Id_user FROM ntf_popupUsers
                            WHERE ntf_popupUsers.Id_main = {0}";
                }
            }

            public static string InsertIntoNtfEmails {
                get {
                    return "INSERT INTO ntf_emails (Id_main, IsActive,Email) VALUES (" + "{0}" + "Id_main, " +
                    "{0}" + "IsActive, " + "{0}" + "Email)";
                }
            }

            public static string DeleteFromNtfEmailsWhere {
                get {
                    return "DELETE FROM ntf_emails WHERE Id_main = {0}";
                }
            }

            public static string DeleteFromNtfPopupUsersWhere {
                get {
                    return "DELETE FROM ntf_popupusers WHERE Id_main = {0}";
                }
            }

            public static string InsertIntoNtfPopupUsers {
                get {
                    return "INSERT INTO ntf_popupusers (Id_main, Id_user) VALUES ({0}Id_main,{0}Id_user)";
                }

            }

            public static string DeleteFromNtfAgroWorksWhere
            {
                get
                {
                    return "DELETE FROM ntf_agro_works WHERE Id_main = {0}";
                }
            }

            public static string InsertIntoNtfAgroWorks
            {
                get
                {
                    return "INSERT INTO ntf_agro_works (Id_main, WorkId) VALUES ({0}Id_main,{0}WorkId)";
                }

            }

            public static string SelectAgroZones
            {
                get
                {
                    return
                         @"SELECT zones.Zone_ID FROM agro_field  INNER JOIN zones  ON agro_field.Id_zone = zones.Zone_ID";
                }
            }
            
        }
        // NoticeProvider
        public class MobitelProvider
        {
            public static string SelectMobitelId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT mobitels.Mobitel_ID, From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels";

                    case MssqlUse:
                        return @"SELECT mobitels.Mobitel_ID, dbo.From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels";
                    }

                    return "";
                }
            }

            public static string SelectOnlineMobitelId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT online.Mobitel_ID.Mobitel_ID, from_unixtime(max(online.UnixTime)) as LastTime FROM online WHERE Speed < 108";

                    case MssqlUse:
                        return @"SELECT online.Mobitel_ID.Mobitel_ID , dbo.from_UnixTime(max(online.UnixTime)) as LastTime FROM online WHERE Speed < 108";
                    }

                    return "";
                }
            }

            public static string SelectFromUnixTime {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return @"SELECT From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels WHERE mobitels.Mobitel_ID = {0}";

                    case MssqlUse:
                        return @"SELECT dbo.From_UnixTime(mobitels.LastInsertTime) as LastTime FROM mobitels WHERE mobitels.Mobitel_ID = {0}";
                    }

                    return "";
                }
            }
        }
        // MobitelProvider
        public class VehicleCategoryProvider
        {
            public static string SelectAll {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xSELECT_ALL;

                    case MssqlUse:
                        return xmsSELECT_ALL;
                    }

                    return "";
                }
            }

            public static string SelectOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xSELECT_ONE;

                    case MssqlUse:
                        return xmsSELECT_ONE;
                    }

                    return "";
                }
            }

            public static string UpdateOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xUPDATE_ONE;

                    case MssqlUse:
                        return xmsUPDATE_ONE;
                    }

                    return "";
                }
            }

            public static string InsertOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xINSERT_ONE;

                    case MssqlUse:
                        return xmsINSERT_ONE;
                    }

                    return "";
                }
            }

            public static string DeleteOne {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xDELETE_ONE;

                    case MssqlUse:
                        return xmsDELETE_ONE;
                    }

                    return "";
                }
            }

            public static string CountVehiclesWithCategory {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xCOUNT_VEHICLES_WITH_CATEGORY;

                    case MssqlUse:
                        return xmsCOUNT_VEHICLES_WITH_CATEGORY;
                    }

                    return "";
                }
            }

            public static string SelectAll2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xSELECT_ALL2;

                    case MssqlUse:
                        return xmsSELECT_ALL2;
                    }

                    return "";
                }
            }

            public static string SelectAll3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xSELECT_ALL3;

                        case MssqlUse:
                            return xmsSELECT_ALL3;
                    }

                    return "";
                }
            }

            public static string SelectAll4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xSELECT_ALL4;

                        case MssqlUse:
                            return xmsSELECT_ALL4;
                    }

                    return "";
                }
            }

            public static string SelectOne2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xSELECT_ONE2;

                    case MssqlUse:
                        return xmsSELECT_ONE2;
                    }

                    return "";
                }
            }

            public static string SelectOne3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xSELECT_ONE3;

                        case MssqlUse:
                            return xmsSELECT_ONE3;
                    }

                    return "";
                }
            }

            public static string SelectOne4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xSELECT_ONE4;

                        case MssqlUse:
                            return xmsSELECT_ONE4;
                    }

                    return "";
                }
            }


            public static string UpdateOne2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xUPDATE_ONE2;

                    case MssqlUse:
                        return xmsUPDATE_ONE2;
                    }

                    return "";
                }
            }

            public static string UpdateOne3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xUPDATE_ONE3;

                        case MssqlUse:
                            return xmsUPDATE_ONE3;
                    }

                    return "";
                }
            }

            public static string UpdateOne4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xUPDATE_ONE4;

                        case MssqlUse:
                            return xmsUPDATE_ONE4;
                    }

                    return "";
                }
            }

            public static string InsertOne2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xINSERT_ONE2;

                    case MssqlUse:
                        return xmsINSERT_ONE2;
                    }

                    return "";
                }
            }

            public static string InsertOne3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xINSERT_ONE3;

                        case MssqlUse:
                            return xmsINSERT_ONE3;
                    }

                    return "";
                }
            }

            public static string InsertOne4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xINSERT_ONE4;

                        case MssqlUse:
                            return xmsINSERT_ONE4;
                    }

                    return "";
                }
            }

            public static string DeleteOne2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xDELETE_ONE2;

                    case MssqlUse:
                        return xmsDELETE_ONE2;
                    }

                    return "";
                }
            }

            public static string DeleteOne3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xDELETE_ONE3;

                        case MssqlUse:
                            return xmsDELETE_ONE3;
                    }

                    return "";
                }
            }

            public static string DeleteOne4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xDELETE_ONE4;

                        case MssqlUse:
                            return xmsDELETE_ONE4;
                    }

                    return "";
                }
            }

            public static string CountVehicleWithCategory2 {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return xCOUNT_VEHICLES_WITH_CATEGORY2;

                    case MssqlUse:
                        return xmsCOUNT_VEHICLES_WITH_CATEGORY2;
                    }

                    return "";
                }
            }

            public static string CountVehicleWithCategory3
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xCOUNT_VEHICLES_WITH_CATEGORY3;

                        case MssqlUse:
                            return xmsCOUNT_VEHICLES_WITH_CATEGORY3;
                    }

                    return "";
                }
            }

            public static string CountVehicleWithCategory4
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return xCOUNT_VEHICLES_WITH_CATEGORY4;

                        case MssqlUse:
                            return xmsCOUNT_VEHICLES_WITH_CATEGORY4;
                    }

                    return "";
                }
            }
        } // VehicleCategoryProvider

        // ZonesProvider
        public class ZoneCategoryProvider
        {
            public static string SelectAll
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT * FROM `zone_category` ORDER BY Name";

                        case MssqlUse:
                            return @"SELECT * FROM zone_category ORDER BY Name";
                    }

                    return "";
                }
            }

            public static string SelectOne
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT * FROM `zone_category` WHERE `Id`={0}";

                        case MssqlUse:
                            return @"SELECT * FROM zone_category WHERE Id={0}";
                    }

                    return "";
                }
            }
            
            public static string UpdateOne
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"UPDATE zone_category SET `Name`=?Name  WHERE `Id`=?Id";

                        case MssqlUse:
                            return @"UPDATE zone_category SET Name=@Name  WHERE Id=@Id";
                    }

                    return "";
                }
            }
            
            public static string InsertOne
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"INSERT INTO zone_category (`Name`) VALUES (?Name);";

                        case MssqlUse:
                            return @"INSERT INTO zone_category (Name) VALUES (@Name);";
                    }

                    return "";
                }
            }
            
            public static string DeleteOne
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"DELETE FROM zone_category WHERE `Id`=?Id";

                        case MssqlUse:
                            return @"DELETE FROM zone_category WHERE Id=@Id";
                    }

                    return "";
                }
            }
            
            public static string CountVehicleWithCategory
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT count(zones.Zone_ID) FROM zones WHERE zones.Category_id = ?Id GROUP BY zones.Category_id";

                        case MssqlUse:
                            return @"SELECT count(zones.Zone_ID) FROM zones WHERE zones.Category_id = @Id GROUP BY zones.Category_id";
                    }

                    return "";
                }
            }

            public static string SelectAll2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT * FROM `zone_category2` ORDER BY Name";

                        case MssqlUse:
                            return @"SELECT * FROM zone_category2 ORDER BY Name";
                    }

                    return "";
                }
            }

            public static string SelectOne2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT * FROM `zone_category2` WHERE `Id`={0}";

                        case MssqlUse:
                            return @"SELECT * FROM zone_category2 WHERE Id={0}";
                    }

                    return "";
                }
            }

            public static string UpdateOne2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"UPDATE zone_category2 SET `Name`=?Name  WHERE `Id`=?Id";

                        case MssqlUse:
                            return @"UPDATE zone_category2 SET Name=@Name  WHERE Id=@Id";
                    }

                    return "";
                }
            }

            public static string InsertOne2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"INSERT INTO zone_category2 (`Name`) VALUES (?Name);";

                        case MssqlUse:
                            return @"INSERT INTO zone_category2 (Name) VALUES (@Name);";
                    }

                    return "";
                }
            }

            public static string DeleteOne2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"DELETE FROM zone_category2 WHERE `Id`=?Id";

                        case MssqlUse:
                            return @"DELETE FROM zone_category2 WHERE Id=@Id";
                    }

                    return "";
                }
            }

            public static string CountVehicleWithCategory2
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"SELECT count(zones.Zone_ID) FROM zones WHERE zones.Category_id2 = ?Id GROUP BY zones.Category_id2";

                        case MssqlUse:
                            return @"SELECT count(zones.Zone_ID) FROM zones WHERE zones.Category_id2 = @Id GROUP BY zones.Category_id2";
                    }

                    return "";
                }
            }
        }

        public static class ZonesAgroProvider
        {
            public static string GetCultureActive
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT   CONCAT(IFNULL(agro_culture.Name,''),' ' ,YEAR(agro_fieldseason_tc.DateStart)) as SeasonName
                                    FROM agro_season
                                    INNER JOIN agro_culture
                                    ON agro_season.Id_culture = agro_culture.Id
                                    INNER JOIN agro_fieldseason_tc
                                    ON agro_fieldseason_tc.IdSeason = agro_season.Id
                                    INNER JOIN agro_field
                                    ON agro_fieldseason_tc.IdField = agro_field.Id
                                    WHERE agro_field.Id_zone = {0}
                                    AND {1}DateToday between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd";
                        case MssqlUse:
                            return @"SELECT (RTRIM(ISNULL(agro_culture.Name,'')) + ' ' + CONVERT(char(10),YEAR(agro_fieldseason_tc.DateStart))) as SeasonName
                                    FROM agro_season
                                    INNER JOIN agro_culture
                                    ON agro_season.Id_culture = agro_culture.Id
                                    INNER JOIN agro_fieldseason_tc
                                    ON agro_fieldseason_tc.IdSeason = agro_season.Id
                                    INNER JOIN agro_field
                                    ON agro_fieldseason_tc.IdField = agro_field.Id
                                    WHERE agro_field.Id_zone = {0}
                                    AND {1}DateToday between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd";
                    }
                    return "";
                }
            }

            public static string GetCultureNameActive
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT CONCAT(IFNULL(agro_culture.Name,''),',' ,YEAR(agro_fieldseason_tc.DateStart),',',YEAR(agro_fieldseason_tc.DateEnd)) as SeasonName
                                    FROM agro_season
                                    INNER JOIN agro_culture
                                    ON agro_season.Id_culture = agro_culture.Id
                                    INNER JOIN agro_fieldseason_tc
                                    ON agro_fieldseason_tc.IdSeason = agro_season.Id
                                    INNER JOIN agro_field
                                    ON agro_fieldseason_tc.IdField = agro_field.Id
                                    WHERE agro_field.Id_zone = {0}
                                    AND {1}DateToday between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd";
                        case MssqlUse:
                            return @"SELECT (RTRIM(ISNULL(agro_culture.Name,'')) + ',' + CONVERT(char(10),YEAR(agro_fieldseason_tc.DateStart)) + ',' + CONVERT(char(10),YEAR(agro_fieldseason_tc.DateEnd))) as SeasonName
                                    FROM agro_season
                                    INNER JOIN agro_culture
                                    ON agro_season.Id_culture = agro_culture.Id
                                    INNER JOIN agro_fieldseason_tc
                                    ON agro_fieldseason_tc.IdSeason = agro_season.Id
                                    INNER JOIN agro_field
                                    ON agro_fieldseason_tc.IdField = agro_field.Id
                                    WHERE agro_field.Id_zone = {0}
                                    AND {1}DateToday between agro_fieldseason_tc.DateStart and agro_fieldseason_tc.DateEnd";
                    }
                    return "";
                }
            }
        }
        public class VehicleProvider2
        {
            public static string SelectVehicleFrom {
                get {

                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT vehicle.*,`lines`.Color, `lines`.Width, `lines`.Icon 
                            FROM  vehicle  LEFT OUTER JOIN `lines` ON vehicle.Mobitel_id = `lines`.Mobitel_ID
                            WHERE vehicle.Mobitel_id = @ID";

                        case MssqlUse:
                            return @"SELECT vehicle.*,lines.Color, lines.Width, lines.Icon 
                            FROM  vehicle  LEFT OUTER JOIN lines ON vehicle.Mobitel_id = lines.Mobitel_ID
                            WHERE vehicle.Mobitel_id = @ID";
                    }

                    return "";

                }
            }

            public static string SelectSettingFrom {
                get {
                    return "SELECT setting.* FROM  setting WHERE setting.id = {0}";
                }
            }

            public static string SelectMobitelIdByOutLinkId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT vehicle.Mobitel_id FROM  vehicle WHERE vehicle.OutLinkId = '{0}'";
                        case MssqlUse:
                            return "SELECT vehicle.Mobitel_id FROM  vehicle WHERE vehicle.OutLinkId = N'{0}'";
                    }
                    return "";
                }
            }
            public static string SelectMobitelsId {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                            return @"SELECT mobitels.Mobitel_ID ,internalmobitelconfig.devIdShort,From_UnixTime(mobitels.LastInsertTime) as LastTimeGps,mobitels.Is64bitPackets,mobitels.IsNotDrawDgps
                                FROM  mobitels 
                                INNER JOIN internalmobitelconfig
                                ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                                WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                                FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                                AND  mobitels.Mobitel_ID = {0}";
                    case MssqlUse:
                            return @"SELECT mobitels.Mobitel_ID ,internalmobitelconfig.devIdShort,dbo.From_UnixTime(mobitels.LastInsertTime) as LastTimeGps,mobitels.Is64bitPackets,mobitels.IsNotDrawDgps
                                FROM  mobitels 
                                INNER JOIN internalmobitelconfig
                                ON mobitels.InternalMobitelConfig_ID = internalmobitelconfig.ID
                                WHERE internalmobitelconfig.InternalMobitelConfig_ID = (SELECT max(internalmobitelconfig.InternalMobitelConfig_ID) AS MAX_ID
                                FROM internalmobitelconfig WHERE internalmobitelconfig.ID = mobitels.InternalMobitelConfig_ID  GROUP BY internalmobitelconfig.ID)
                                AND  mobitels.Mobitel_ID = {0}";
                    }

                    return "";
                }
            }
        }

        // VehicleProvider2
        public class ImportPointsFromXML
        {
            public static string InsertIntoZonesGroup {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO `zonesgroup` (`Title`, `Description`) VALUES (?Title, ?Description)";
                    case MssqlUse:
                        return "INSERT INTO zonesgroup (Title, Description) VALUES (@Title, @Description)";
                    }

                    return "";
                }
            }

            public static string InsertIntoZones {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO `zones` (`Name`, `Descr`, `Square`, `ZonesGroupId`) VALUES (?Name, ?Description,?Square,  ?ZonesGroupId)";
                    case MssqlUse:
                        return "INSERT INTO zones (Name, Descr, Square, ZonesGroupId) VALUES (@Name, @Description, @Square,  @ZonesGroupId)";
                    }

                    return "";
                }
            }

            public static string InsertIntoPoints {
                get {
                    switch (TypeDataBaseForUsing) {
                    case MySqlUse:
                        return "INSERT INTO `points` (`Latitude`, `Longitude`, `Zone_ID`) VALUES (?Latitude, ?Longitude, ?Zone_ID)";
                    case MssqlUse:
                        return "INSERT INTO points (Latitude, Longitude, Zone_ID) VALUES (@Latitude, @Longitude, @Zone_ID)";
                    }

                    return "";
                }
            }
        }
        // ImportPointsFromXML
        public class PcbSensors
        {
            public static string UpdateVehicleSet
            {
                get
                {
                    return "UPDATE vehicle SET PcbVersionId = -1 where PcbVersionId = {0}";
                }
            }

            public static string SelectVehiclePcbVersionId
            {
                get
                {
                    return "select vehicle.PcbVersionId from vehicle where PcbVersionId = {0}";
                }
            }

            public static string UpdatePcbVersion
            {
                get
                {
                    return "UPDATE pcbversion SET Name = '{1}', Comment = '{2}' WHERE (ID = {0})";
                }
            }

            public static string UpdatePcbData
            {
                get
                {
                    return "UPDATE pcbdata SET Typedata = '{1}', koeffK = {2}, Startbit = {3}, Lengthbit = {4}, Comment = '{5}', NameVal = '{6}', NumAlg = '{7}' WHERE ID = {0}";
                }
            }

            public static string InsertIntoPcbVersion
            {
                get
                {
                    return "INSERT INTO pcbversion (Name, Comment) VALUES ('{0}', '{1}')";
                }
            }

            public static string DeleteFromPcbVersion
            {
                get
                {
                    return "DELETE FROM pcbversion WHERE Id = ";
                }
            }

            public static string DeleteFromPcbData
            {
                get
                {
                    return "DELETE FROM pcbdata WHERE Id = ";
                }
            }

            public static string InsertIntoPcbData
            {
                get
                {
                    return "INSERT INTO pcbdata (Typedata, koeffK, Startbit, Lengthbit, Comment, Idpcb, NameVal, NumAlg) VALUES ('{0}', {1}, {2}, {3}, '{4}', {5}, '{6}', {7})";
                }
            }

            public static string DeletePcbDataIdPcb
            {
                get
                {
                    return "DELETE FROM pcbdata WHERE Idpcb = ";
                }
            }

            public static string SelectFromPcbVersion
            {
                get
                {
                    return "SELECT * FROM pcbversion";
                }
            }

            public static string SelectFromPcbData
            {
                get
                {
                    return "SELECT * FROM pcbdata WHERE Idpcb = ";
                }
            }
        } // PcbSensors

        public class AdminForm
        {
            public static string SelectVehicleMobitelId
            {
                get
                {
                    return "SELECT vehicle.Mobitel_id, vehicle.PcbVersionId FROM vehicle WHERE NumberPlate = '{0}'";
                }
            }

            public static string SelectVehicleNameTrack
            {
                get
                {
                    return "SELECT mobitels.Mobitel_ID FROM mobitels WHERE Name = '{0}'";
                }
            }

            public static string SelectVehiclePcbVersionId
            {
                get
                {
                    return "SELECT vehicle.PcbVersionId FROM vehicle WHERE Mobitel_id = ";
                }
            }

            public static string UpdateSensors
            {
                get
                {
                    return "UPDATE sensors SET Name = '{0}', StartBit = {1}, Length = {2}, K = {3}, B = {4}, S = {5}, NameUnit = '{8}' WHERE mobitel_id = {6} AND id = {7}";
                }
            }

            public static string SelectPcbVersionId
            {
                get
                {
                    return "SELECT pcbversion.Id, pcbversion.Name, pcbversion.Comment FROM pcbversion";
                }
            }

            public static string UpdateVehicle
            {
                get
                {
                    return "UPDATE vehicle SET PcbVersionId = {0} WHERE Mobitel_id = {1}";
                }
            }

            public static string SelectFromPcbData
            {
                get
                {
                    return "select * from pcbdata where Idpcb = {0}";
                }
            }
        }

        public class ExcelFormDvx
        {
            public static string InsertInto
            {
                get
                {
                    return "INSERT INTO {0} ({1}) VALUES ({2})";
                }
            }

            public static string SelectFrom
            {
                get
                {
                    return "SELECT * FROM [{0}]";
                }
            }
        }

        public class VehicleCommentProvider
        {
            public static string InsertOne
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"INSERT INTO vehicle_commentary (`Comment`, `Mobitel_id`) VALUES (?Comment, ?Mobitel_id);";

                        case MssqlUse:
                            return @"INSERT INTO vehicle_commentary (Comment, Mobitel_id) VALUES (@Comment, @Mobitel_id);";
                    }

                    return "";
                }
            }

            public static string SelectComment
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return @"SELECT * FROM vehicle_commentary WHERE Mobitel_id = ";

                        case MssqlUse:
                            return @"SELECT * FROM vehicle_commentary WHERE Mobitel_id = ";
                    }

                    return "";
                }
            }

            public static string UpdateComment
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"UPDATE vehicle_commentary SET `Comment`=?Comment  WHERE `Mobitel_id`=?Mobitel_id";

                        case MssqlUse:
                            return @"UPDATE vehicle_commentary SET Comment=@Comment  WHERE Mobitel_id=@Mobitel_id";
                    }

                    return "";
                }
            }

            public static string DeleteComment
            {
                get
                {
                    switch( TypeDataBaseForUsing )
                    {
                        case MySqlUse:
                            return @"DELETE FROM vehicle_commentary WHERE `Mobitel_id`=?Mobitel_id";

                        case MssqlUse:
                            return @"DELETE FROM vehicle_commentary WHERE Mobitel_id=@Mobitel_id";
                    }

                    return "";
                }
            }
        }

        public class RouteProvider
        {
            public static string SelectIdByOutLinkId
            {
                get
                {
                    switch (TypeDataBaseForUsing)
                    {
                        case MySqlUse:
                            return "SELECT   rt_sample.Id FROM   rt_sample WHERE   rt_sample.OutLinkId = '{0}'";
                        case MssqlUse:
                            return "SELECT   rt_sample.Id FROM   rt_sample WHERE   rt_sample.OutLinkId = N'{0}'";
                    }
                    return "";
                }
            }
        }

    } // TrackControlQuery
} // TrackControl.General.DAL
