﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Services;

namespace TrackControl.General
{
    public static class UserBaseProvider
    {
        public class DataBaseName
        {
            private string dBStr;

            public DataBaseName(string str)
            {
                this.dBStr = str;
            }

            public string NameDB
            {
                get { return dBStr; }
            }
        } // DataBaseName

        public static int GetUserID(string nameUsr)
        {
            int id_user = -1;
            DriverDb db = new DriverDb();
            db.ConnectDb();
            string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectUserListOnName, nameUsr);
            db.GetDataReader(sql);
            if (db.Read())
            {
                int ordi = db.GetOrdinal("Id");
                id_user = Convert.ToInt32(db.GetInt32(ordi).ToString());
            }
            db.CloseDbConnection();
            return id_user;
        }

        public static UserBase GetUserBase(int id_user)
        {
            UserBase ub = null;
            //using (ConnectMySQL cn  = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectUserList, id_user);

                //MySqlDataReader dr = cn.GetDataReader(sql); 
                db.GetDataReader(sql);
                //if (dr.Read())
                if (db.Read())
                {
                    //ub = SetUserBase(ub, dr);
                    ub = SetUserBase(ub, db.GetSqlDataReader());
                }
            }
            db.CloseDbConnection();
            return ub;
        }

        // aketner - 12.09.2012
        private static DataBaseName SetDataBases(DataBaseName db, object dr)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataReader drs = dr as MySqlDataReader;

                if (drs == null)
                    throw new Exception("Type MySqlDataReader is not peresent!");

                return new DataBaseName((string) drs.GetValue(0));
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataReader drs = dr as SqlDataReader;

                if (drs == null)
                    throw new Exception("Type SqlDataReader is not peresent!");

                return new DataBaseName((string) drs.GetValue(0));
            } // if

            return null;
        } // SetDataBases

        private static UserBase SetUserBase(UserBase ub, object dr)
        {
            if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
            {
                MySqlDataReader drs = dr as MySqlDataReader;

                if (drs == null)
                    throw new Exception("Type MySqlDataReader is not peresent!");

                ub = new UserBase();
                ub.Id = drs.GetInt32("Id");
                ub.Name = drs.GetString("Name");
                ub.Password = (byte[]) drs["Password"];
                ub.Admin = drs.GetBoolean("Admin");
                ub.WinLogin = drs.IsDBNull(drs.GetOrdinal("WinLogin")) ? "" : drs.GetString("WinLogin");
                int id_role = drs.IsDBNull(drs.GetOrdinal("Id_role")) ? 0 : drs.GetInt32("Id_role");

                if (id_role > 0)
                {
                    UserRole ur = GetUserRole(id_role);

                    if (ur != null) 
                        ub.Role = ur;
                }

                return ub;
            } // if
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                SqlDataReader drs = dr as SqlDataReader;

                if (drs == null)
                    throw new Exception("Type SqlDataReader is not peresent!");

                ub = new UserBase();
                int k = drs.GetOrdinal("Id");
                ub.Id = drs.GetInt32(k);
                k = drs.GetOrdinal("Name");
                ub.Name = drs.GetString(k);
                ub.Password = (byte[]) drs["Password"];

                k = drs.GetOrdinal("Admin");
                if (drs.GetInt16(k) == 0)
                    ub.Admin = false;
                else
                    ub.Admin = true;

                k = drs.GetOrdinal("WinLogin");
                ub.WinLogin = drs.IsDBNull(drs.GetOrdinal("WinLogin")) ? "" : drs.GetString(k);
                k = drs.GetOrdinal("Id_role");
                int id_role = drs.IsDBNull(drs.GetOrdinal("Id_role")) ? 0 : drs.GetInt32(k);

                if (id_role > 0)
                {
                    UserRole ur = GetUserRole(id_role);
                    if (ur != null) ub.Role = ur;
                }

                return ub;
            } // else if

            return null;
        } // SetUserBase

        public static int InsertUserBase(UserBase ub)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                db.ConnectDb();
                string sql = TrackControlQuery.UserBaseProvider.InsertIntoUserList;
                //MySqlParameter[] parSql = new MySqlParameter[5];
                db.NewSqlParameterArray(5);
                //CreateParameters(ub, parSql);
                CreateParameters(ub, db);
                //return cn.ExecuteReturnLastInsert(sql, parSql);
                //return db.ExecuteReturnLastInsert( sql, parSql );
                return db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "users_list");
                //db.CloseDbConnection();
            }
            catch
            {
                db.CloseDbConnection();
                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool UpdateUserBase(UserBase ub)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    string sql = string.Format(TrackControlQuery.UserBaseProvider.UpdateUserList, ub.Id);

                    //MySqlParameter[] parSql = new MySqlParameter[5];
                    db.NewSqlParameterArray(5);
                    CreateParameters(ub, db);
                    //cn.ExecuteNonQueryCommand(sql, parSql);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        //private static void CreateParameters(UserBase ub, MySqlParameter[] parSql)
        //{
        //    parSql[0] = new MySqlParameter("?Name", ub.Name);
        //    parSql[1] = new MySqlParameter("?Password", ub.Password);
        //    parSql[2] = new MySqlParameter("?Admin", ub.Admin);
        //    parSql[3] = new MySqlParameter("?WinLogin", ub.WinLogin);
        //    parSql[4] = new MySqlParameter("?Id_role",ub.Role==null ? 0 : ub.Role.Id);
        //}

        private static void CreateParameters(UserBase ub, DriverDb db)
        {
            //parSql[0] = new MySqlParameter( "?Name", ub.Name );
            db.NewSqlParameter(db.ParamPrefics + "Name", ub.Name == null ? "" : ub.Name, 0);
            //parSql[1] = new MySqlParameter( "?Password", ub.Password );
            byte[] bt = new byte[1];
            bt[0] = 0;db.NewSqlParameter(db.ParamPrefics + "Password", ub.Password == null ? bt : ub.Password, 1);
            //parSql[2] = new MySqlParameter( "?Admin", ub.Admin );
            db.NewSqlParameter(db.ParamPrefics + "Admin", ub.Admin, 2);
            //parSql[3] = new MySqlParameter( "?WinLogin", ub.WinLogin );
            db.NewSqlParameter(db.ParamPrefics + "WinLogin", ub.WinLogin == null ? "" : ub.WinLogin, 3);
            //parSql[4] = new MySqlParameter( "?Id_role", ub.Role == null ? 0 : ub.Role.Id );
            db.NewSqlParameter(db.ParamPrefics + "Id_role", ub.Role == null ? 0 : ub.Role.Id, 4);
        }

        public static BindingList<UserBase> GetList()
        {
            BindingList<UserBase> ubs = new BindingList<UserBase>();
            UserBase ub = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = TrackControlQuery.UserBaseProvider.SelectFromUserList;

                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //while (dr.Read())
                while (db.Read())
                {
                    //ub = SetUserBase(ub, dr);
                    ub = SetUserBase(ub, db.GetSqlDataReader());
                    ubs.Add(ub);
                }
            }
            db.CloseDbConnection();
            return ubs;
        }

        // aketner - 12.09.2012 - получить список базы данных
        public static BindingList<DataBaseName> GetDataBaseList()
        {
            BindingList<DataBaseName> dbl = new BindingList<DataBaseName>();
            DataBaseName db = null;
            //using ( ConnectMySQL cN = new ConnectMySQL() )
            DriverDb driverDb = new DriverDb();
            driverDb.ConnectDb();
            {
                string sql = TrackControlQuery.UserBaseProvider.ShowDatabases;

                //MySqlDataReader dR = cN.GetDataReader( sql );
                driverDb.GetDataReader(sql);
                //while ( dR.Read() )
                while (driverDb.Read())
                {
                    //db = SetDataBases( db, dR );
                    db = SetDataBases(db, driverDb.GetSqlDataReader());
                    dbl.Add(db);
                } // while
            } // using
            driverDb.CloseDbConnection();
            return dbl;
        } /* GetDataList */

        public static DataTable GetListDataTable()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                //string sql = @"SELECT users_list.* FROM users_list ORDER BY users_list.Name";

                string sql = TrackControlQuery.UserBaseProvider.SelectUsers_List;

                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public static bool DeleteUserBase(UserBase ub)
        {

            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = string.Format(TrackControlQuery.UserBaseProvider.DeleteFromUsersList, ub.Id);

                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand(sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool IsWinLoginExist(UserBase Exclude, string winLogin)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectCountUsersList, Exclude.Id,
                    db.ParamPrefics);

                //MySqlParameter[] parSql = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //parSql[0] = new MySqlParameter("?WinLogin", winLogin);
                db.NewSqlParameter(db.ParamPrefics + "WinLogin", winLogin, 0);
                //if (cn.GetScalarValueIntNull(sql,parSql) == 0)
                if (db.GetScalarValueIntNull(sql, db.GetSqlParameterArray) == 0)
                    return false;
                else
                    return true;
            }
        }

        public static void InsertLog(UserLogTypes TypeLog, string TextLog, int Id_document)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = TrackControlQuery.UserBaseProvider.InsertIntoUsersAction;

                //MySqlParameter[] parSql = new MySqlParameter[6];
                db.NewSqlParameterArray(6);
                //parSql[0] = new MySqlParameter("?TypeLog", (int)TypeLog);
                db.NewSqlParameter(db.ParamPrefics + "TypeLog", (int) TypeLog, 0);
                //parSql[1] = new MySqlParameter("?DateAction", DateTime.Now);
                db.NewSqlParameter(db.ParamPrefics + "DateAction", DateTime.Now, 1);
                //parSql[2] = new MySqlParameter("?Id_document", Id_document);
                db.NewSqlParameter(db.ParamPrefics + "Id_document", Id_document, 2);
                //parSql[3] = new MySqlParameter("?TextLog", TextLog);
                db.NewSqlParameter(db.ParamPrefics + "TextLog", TextLog, 3);
                //parSql[4] = new MySqlParameter("?User", UserBaseCurrent.Instance.Name);
                db.NewSqlParameter(db.ParamPrefics + "User", UserBaseCurrent.Instance.Name, 4);
                //parSql[5] = new MySqlParameter("?Computer", Environment.MachineName);
                db.NewSqlParameter(db.ParamPrefics + "Computer", Environment.MachineName, 5);
                //cn.ExecuteNonQueryCommand(sql, parSql); 
                db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
            }
            db.CloseDbConnection();
        }

        public static DataTable GetLog(UserLogTypes TypeAction, int Id_document)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectUsersActionsLog, (int) TypeAction,
                    Id_document);

                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public static BindingList<UserRole> GetRolesList()
        {
            BindingList<UserRole> urs = new BindingList<UserRole>();
            UserRole ur = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = TrackControlQuery.UserBaseProvider.SelectUsersAcsRoles;

                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //while (dr.Read())
                while (db.Read())
                {
                    //ur = new UserRole(dr.GetInt32("Id"), dr.GetString("Name"));
                    ur = new UserRole(db.GetInt32("Id"), db.GetString("Name"));
                    urs.Add(ur);
                }
            }
            db.CloseDbConnection();
            return urs;
        }

        public static int InsertUserRole(UserRole ur)
        {

            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = string.Format(TrackControlQuery.UserBaseProvider.InsertIntoUsersAcsRoles,
                        db.ParamPrefics);

                    //MySqlParameter[] parSql = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //parSql[0] = new MySqlParameter("?Name", ur.Name);
                    db.NewSqlParameter(db.ParamPrefics + "Name", ur.Name, 0);
                    //return cn.ExecuteReturnLastInsert(sql, parSql);
                    return db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "users_acs_roles");
                }
            }
            catch
            {
                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool UpdateUserRole(UserRole ur)
        {

            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = string.Format(TrackControlQuery.UserBaseProvider.UpdateUsersAcsRoles, ur.Id,
                        db.ParamPrefics);

                    //MySqlParameter[] parSql = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //parSql[0] = new MySqlParameter("?Name", ur.Name);
                    db.NewSqlParameter(db.ParamPrefics + "Name", ur.Name, 0);
                    //cn.ExecuteNonQueryCommand(sql, parSql);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool DeleteUserRole(UserRole UserRole)
        {

            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = string.Format(TrackControlQuery.UserBaseProvider.DeleteFromUsersAcsRoles, UserRole.Id);

                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand(sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static int CountUsersHasRole(UserRole UserRole)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectCountUsersListId, UserRole.Id);

                //return cn.GetScalarValueNull<int>(sql,0) ;
                return db.GetScalarValueNull<int>(sql, 0);
            }
        }

        public static int GetRolesCounter()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.UserBaseProvider.SelectCountUsersAcsRolesId;

                //return cn.GetScalarValueNull<int>(sql, 0);
                return db.GetScalarValueNull<int>(sql, 0);
            }
        }

        public static int GetUsersCounter()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.UserBaseProvider.SelectCountUsersListIdAs;

                //return cn.GetScalarValueNull<int>(sql, 0);
                return db.GetScalarValueNull<int>(sql, 0);
            }
        }

        public static UserRole GetUserRole(int id_role)
        {
            UserRole ur = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.UserBaseProvider.SelectUsers_acs_roles, id_role);

                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //if (dr.Read())
                if (db.Read())
                {
                    //ur = new UserRole(dr.GetInt32("Id"), dr.GetString("Name"));
                    ur = new UserRole(db.GetInt32("Id"), db.GetString("Name"));
                }
            }
            db.CloseDbConnection();
            return ur;
        }

        public static List<int> GetUnvisibleRoleItems(int id_type, int id_role)
        {
            List<int> items = new List<int>();
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.UserBaseProvider.Select_users_acs_objects, id_type, id_role);

                // MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //while (dr.Read())
                while (db.Read())
                {
                    //items.Add(dr.GetInt32("Id_object")); 
                    items.Add(db.GetInt32("Id_object"));
                }
            }

            db.CloseDbConnection();

            return items;
        }

        public static void SaveUnvisibleRoleItems(int id_type, int id_role, IList<IEntity> objects)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string sql = string.Format(TrackControlQuery.UserBaseProvider.DeleteFrom_users_acs_objects, id_type,
                        id_role);

                    //cn.ExecuteNonQueryCommand(sql); 
                    db.ExecuteNonQueryCommand(sql);

                    if (objects == null)
                        return;

                    foreach (IEntity  entity in objects)
                    {
                        sql = string.Format(TrackControlQuery.UserBaseProvider.INSERT_INTO_users_acs_objects, (int)id_type,
                            (int)id_role, (int) entity.Id);

                        //cn.ExecuteNonQueryCommand(sql); 
                        db.ExecuteNonQueryCommand(sql);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "SaveUnvisibleRoleItems", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        } 
    }
}
