﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.General
{
    public enum UserLogTypes
    {
        TOTAL,
        AGRO ,
        ROUTE,
        MD_ORDER,
        MD_DICTIONARY,
        AGRO_PLAN,
        CheckZone,
        AGRO_FieldTc,
        Settings,
        SensorCalibration,
        AgroAgregatCalibration
    }
}
