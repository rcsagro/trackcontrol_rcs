﻿using System.Collections.Generic;
using System.ComponentModel;
using TrackControl.General.Core;
using TrackControl.General.Services;

namespace TrackControl.General
{
    public class UserBaseCurrent : Singleton<UserBaseCurrent>
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        bool _admin;

        public bool Admin
        {
            get { return _admin; }
            set { _admin = value; }
        }

        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        string _server;

        public string Server
        {
            get { return _server; }
            set { _server = value; }
        }

        string _database;

        public string Database
        {
            get { return _database; }
            set { _database = value; }
        }

        UserRole _role;

        public UserRole Role
        {
            get { return _role; }

            set
            {
                if (_role != value)
                {
                    _role = value;
                    SetRoleLimits();
                }
            }
        }

        List<int> _unvisibleZonesId;

        public List<int> UnvisibleZonesId
        {
            get { return _unvisibleZonesId; }
            set { _unvisibleZonesId = value; }
        }

        List<int> _unvisibleVehiclesId;

        public List<int> UnvisibleVehiclesId
        {
            get { return _unvisibleVehiclesId; }
            set { _unvisibleVehiclesId = value; }
        }

        List<int> _unvisibleReportsId;

        public List<int> UnvisibleReportsId
        {
            get { return _unvisibleReportsId; }
            set { _unvisibleReportsId = value; }
        }

        List<int> UnvisibleModulesId { get; set; }
        BindingList<TCModule> _modules;

        public UserBaseCurrent()
        {
            _modules = ModulesController.GetModulesList();
        }

        public void SetUserBase(UserBase ub)
        {
            _name = ub.Name;
            Id = ub.Id;
            _admin = ub.Admin;
            _role = ub.Role;

            if (_role != null)
                SetRoleLimits();
        }

        public bool SetDefault(string password)
        {
            if (PassService.CheckPassword(password))
            {
                _name = ConstsGen.USER_ADMIN_NAME;
                _id = ConstsGen.USER_ADMIN_CODE;
                _admin = true;
                if (UserRole.GetRolesCounter() == 0 && UserBase.GetUsersCounter() == 0)
                {
                    _role = UserRole.CreateDefaultRole();
                    UserBase ub = UserBase.CreateDefaultUser(_role);
                    SetUserBase(ub);
                }
                return true;
            }
            else
                return false;
        }

        void SetRoleLimits()
        {
            if (UserBaseCurrent.Instance.Role == null)
                return;

            _unvisibleZonesId = UserBaseProvider.GetUnvisibleRoleItems((int) UserAccessObjectsTypes.CheckZones,
                UserBaseCurrent.Instance.Role.Id);
            _unvisibleVehiclesId = UserBaseProvider.GetUnvisibleRoleItems((int) UserAccessObjectsTypes.Vehicles,
                UserBaseCurrent.Instance.Role.Id);
            _unvisibleReportsId = UserBaseProvider.GetUnvisibleRoleItems((int) UserAccessObjectsTypes.ReportsList,
                UserBaseCurrent.Instance.Role.Id);
            UnvisibleModulesId = UserBaseProvider.GetUnvisibleRoleItems((int) UserAccessObjectsTypes.ModulesList,
                UserBaseCurrent.Instance.Role.Id);
        }

        bool IsZonesLimited()
        {
            if (_unvisibleZonesId == null)
                return false;

            if (_unvisibleZonesId.Count == 0)
                return false;

            return true;
        }

        bool IsVehiclesLimited()
        {
            if (_unvisibleVehiclesId == null)
                return false;

            if (_unvisibleVehiclesId.Count == 0)
                return false;

            return true;
        }

        bool IsReportsLimited()
        {
            if (_unvisibleReportsId == null) return false;
            if (_unvisibleReportsId.Count == 0) return false;
            return true;
        }

        bool IsModulesLimited()
        {
            if (UnvisibleModulesId == null) return false;
            if (UnvisibleModulesId.Count == 0) return false;
            return true;
        }


        public bool IsZoneVisibleForRole(int idZone)
        {
            if (!IsZonesLimited()) return true;
            foreach (int id_unvis in _unvisibleZonesId)
            {
                if (id_unvis == idZone)
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsVehicleVisibleForRole(int idVehicle)
        {
            if (!IsVehiclesLimited())
                return true;

            foreach (int id_unvis in _unvisibleVehiclesId)
            {
                if (id_unvis == idVehicle)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsReportVisibleForRole(int idReport)
        {
            if (!IsReportsLimited()) return true;
            foreach (int id_unvis in _unvisibleReportsId)
            {
                if (id_unvis == idReport)
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsModuleVisible(int idModule)
        {
            foreach (TCModule module in _modules)
            {
                if (idModule == module.Id)
                {
                    if (_admin) return true;
                    if (!IsModulesLimited()) return true;
                    foreach (int id_unvis in UnvisibleModulesId)
                    {
                        if (id_unvis == idModule)
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
