﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.General
{
    public static class UserLog
    {
        public static void InsertLog(UserLogTypes TypeLog, string TextLog, int Id_document)
           {
               UserBaseProvider.InsertLog(TypeLog, TextLog, Id_document);  
           }
        public static void ViewLog(UserLogTypes type, int id_document)
           {
            UserLogView view = new UserLogView(type,id_document);
            view.Show(); 
           }
    }
    
}
