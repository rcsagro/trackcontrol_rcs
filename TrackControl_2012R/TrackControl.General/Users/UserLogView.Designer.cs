namespace TrackControl.General
{
    partial class UserLogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcLog = new DevExpress.XtraGrid.GridControl();
            this.gvLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComputer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextLog = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcLog
            // 
            this.gcLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLog.Location = new System.Drawing.Point(0, 0);
            this.gcLog.MainView = this.gvLog;
            this.gcLog.Name = "gcLog";
            this.gcLog.Size = new System.Drawing.Size(882, 525);
            this.gcLog.TabIndex = 0;
            this.gcLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLog,
            this.gridView2});
            // 
            // gvLog
            // 
            this.gvLog.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvLog.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.Empty.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvLog.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvLog.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvLog.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvLog.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.OddRow.Options.UseForeColor = true;
            this.gvLog.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvLog.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvLog.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvLog.Appearance.Preview.Options.UseBackColor = true;
            this.gvLog.Appearance.Preview.Options.UseFont = true;
            this.gvLog.Appearance.Preview.Options.UseForeColor = true;
            this.gvLog.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.Row.Options.UseBackColor = true;
            this.gvLog.Appearance.Row.Options.UseForeColor = true;
            this.gvLog.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvLog.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.VertLine.Options.UseBackColor = true;
            this.gvLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colDateAction,
            this.colUser,
            this.colComputer,
            this.colTextLog});
            this.gvLog.GridControl = this.gcLog;
            this.gvLog.Name = "gvLog";
            this.gvLog.OptionsView.EnableAppearanceEvenRow = true;
            this.gvLog.OptionsView.EnableAppearanceOddRow = true;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colDateAction
            // 
            this.colDateAction.AppearanceCell.Options.UseTextOptions = true;
            this.colDateAction.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateAction.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateAction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateAction.Caption = "���� �������";
            this.colDateAction.DisplayFormat.FormatString = "g";
            this.colDateAction.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateAction.FieldName = "DateAction";
            this.colDateAction.Name = "colDateAction";
            this.colDateAction.OptionsColumn.AllowEdit = false;
            this.colDateAction.OptionsColumn.AllowFocus = false;
            this.colDateAction.OptionsColumn.ReadOnly = true;
            this.colDateAction.Visible = true;
            this.colDateAction.VisibleIndex = 0;
            this.colDateAction.Width = 216;
            // 
            // colUser
            // 
            this.colUser.AppearanceCell.Options.UseTextOptions = true;
            this.colUser.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUser.Caption = "������������";
            this.colUser.FieldName = "User";
            this.colUser.Name = "colUser";
            this.colUser.OptionsColumn.AllowEdit = false;
            this.colUser.OptionsColumn.AllowFocus = false;
            this.colUser.OptionsColumn.ReadOnly = true;
            this.colUser.Visible = true;
            this.colUser.VisibleIndex = 1;
            this.colUser.Width = 217;
            // 
            // colComputer
            // 
            this.colComputer.AppearanceCell.Options.UseTextOptions = true;
            this.colComputer.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colComputer.AppearanceHeader.Options.UseTextOptions = true;
            this.colComputer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComputer.Caption = "���������";
            this.colComputer.FieldName = "Computer";
            this.colComputer.Name = "colComputer";
            this.colComputer.OptionsColumn.AllowEdit = false;
            this.colComputer.OptionsColumn.AllowFocus = false;
            this.colComputer.OptionsColumn.ReadOnly = true;
            this.colComputer.Visible = true;
            this.colComputer.VisibleIndex = 2;
            this.colComputer.Width = 211;
            // 
            // colTextLog
            // 
            this.colTextLog.AppearanceCell.Options.UseTextOptions = true;
            this.colTextLog.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTextLog.AppearanceHeader.Options.UseTextOptions = true;
            this.colTextLog.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTextLog.Caption = "�������";
            this.colTextLog.FieldName = "TextLog";
            this.colTextLog.Name = "colTextLog";
            this.colTextLog.OptionsColumn.AllowEdit = false;
            this.colTextLog.OptionsColumn.AllowFocus = false;
            this.colTextLog.OptionsColumn.ReadOnly = true;
            this.colTextLog.Visible = true;
            this.colTextLog.VisibleIndex = 3;
            this.colTextLog.Width = 606;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcLog;
            this.gridView2.Name = "gridView2";
            // 
            // UserLogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 525);
            this.Controls.Add(this.gcLog);
            this.Name = "UserLogView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������ �������";
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAction;
        private DevExpress.XtraGrid.Columns.GridColumn colUser;
        private DevExpress.XtraGrid.Columns.GridColumn colComputer;
        private DevExpress.XtraGrid.Columns.GridColumn colTextLog;
    }
}