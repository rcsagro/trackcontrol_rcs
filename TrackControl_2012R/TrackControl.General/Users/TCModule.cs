﻿using TrackControl.General;

namespace TrackControl.General
{
    public class TCModule : Entity
    {
        public TCModule(int id, string name)
        {
            Id = id;
            Name = name;
            Visible = true;
        }
        public string Name { get; set; }

        public bool Visible { get; set; }
    }
}
