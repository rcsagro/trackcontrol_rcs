﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General.Services;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General.Properties;

namespace TrackControl.General
{
    public class UserBase : Entity
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        byte[] _password = {0};

        public byte[] Password
        {
            get { return _password; }
            set { _password = value; }
        }

        bool _admin;

        public bool Admin
        {
            get { return _admin; }
            set { _admin = value; }
        }

        public string PasswordGrid
        {
            get { return _password==null ? "" : (_password.Length > 0 ? "*****" : ""); }
            set { SetPassword(value); }
        }

        string _winLogin;

        public string WinLogin
        {
            get { return _winLogin; }
            set { _winLogin = value; }
        }

        UserRole _role;

        public UserRole Role
        {
            get
            {
                int rl = UserRole.GetRolesCounter();
                    if (_role == null && rl == 0)
                    {
                        _role = UserRole.CreateDefaultRole();
                        _id_role = _role.Id;
                        Save();
                    }

                    return _role; 
                }
            set { 
                _role = value;
                _id_role = _role.Id;
                }
        }

        // поле для грида
        int _id_role;

        public int Id_role
        {
            get { return _id_role; }
            set { 
                _id_role = value;
                if (_id_role > 0) _role = UserBaseProvider.GetUserRole(_id_role);  

            }
        }

        public UserBase()
        {

        }

        public bool Validate(int id_user,string Password)
        {
            UserBase ub = UserBaseProvider.GetUserBase(id_user);
            
            byte[] passForTest = PassService.getHash(Password);

            if (ub.Password.Length != passForTest.Length)
                return false;

            for (int i = 0; i < ub.Password.Length; i++)
            {
                if (passForTest[i] != ub.Password[i])
                {
                    return false;
                }
            }

            SetUserBase(ub);
            return true;
        }

        public bool Validate(int id_user)
        {
            try
            {
                UserBase ub = UserBaseProvider.GetUserBase(id_user);
                SetUserBase(ub);
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error UserBase", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;}
        }

        public int Id_User(string nameUser)
        {
            int id_user = UserBaseProvider.GetUserID(nameUser);
            return id_user;
        }

        public bool Save()
        {
            if (IsNew)
            {
                if (!IsPasswordSet()) return false;
                Id = UserBaseProvider.InsertUserBase(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return UserBaseProvider.UpdateUserBase(this); 
            }
        }

        public void SetPassword(string password)
        {
            _password = PassService.getHash(password);
        }

        public void SetUserBase(UserBase ub)
        {
            Id = ub.Id;
            _name = ub.Name;
            _password = ub.Password;
            _admin = ub.Admin;
            _role = ub.Role;
        }

        public bool Delete()
        {
            return UserBaseProvider.DeleteUserBase(this);  
        }

        public bool IsWinLoginExist(string loginForTest)
        {
            return UserBaseProvider.IsWinLoginExist(this, loginForTest);
        }

        public bool IsPasswordSet()
        {
            return !(_password[0] == 0);
        }

        public static int GetUsersCounter()
        {
            return UserBaseProvider.GetUsersCounter();
        }

        public static UserBase CreateDefaultUser(UserRole ur)
        {
            UserBase ub = new UserBase();
            ub.Name = Resources.Admin;
            ub.Admin = true;
            ub.SetPassword("123");
            ub.Role = ur;
            ub.Save();
            return ub;
        }}
}
