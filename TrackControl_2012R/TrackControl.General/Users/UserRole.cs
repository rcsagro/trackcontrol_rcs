﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General.Properties;

namespace TrackControl.General
{
    public  class UserRole : Entity
    {
        public UserRole(int idRole,string Name)
        {
            Id = idRole;
            _name = Name;
        }

        public UserRole()
        {

        }

        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Save()
        {
            if (IsNew)
            {
                Id = UserBaseProvider.InsertUserRole(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return UserBaseProvider.UpdateUserRole(this);
            }
        }

        public bool Delete()
        {
            return UserBaseProvider.DeleteUserRole(this);
        }

        public static int GetRolesCounter()
        {
            return UserBaseProvider.GetRolesCounter();
        }

        public static UserRole CreateDefaultRole()
        {
            UserRole ur = new UserRole();
            ur.Name = Resources.Admin;
            ur.Save();
            return ur;

        }
    }
}
