using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace TrackControl.General
{
    public partial class UserLogView : DevExpress.XtraEditors.XtraForm
    {
        public UserLogView(UserLogTypes type, int id_document)
        {
            InitializeComponent();
            gcLog.DataSource = UserBaseProvider.GetLog(type, id_document);   
        }
    }
}