using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General.Properties;

namespace TrackControl.General.Users
{
    public partial class UserRolesEditor : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshRolesList;
        bool RolesChanged;

        public UserRolesEditor(Point point)
        {
            InitializeComponent();
            Localization();
            gcRoles.DataSource = UserBaseProvider.GetRolesList();
            this.Location = point;
        }

        private void gvRoles_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            UserRole ur = GetCurrentUserRole();
            if (ur != null)
            {
                ur.Save();
                RolesChanged = true;
            }
        }

        private void gcRoles_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowRoles();
                RolesChanged = !e.Handled ;
            }
        }

        bool DeleteRowRoles()
        {
            UserRole ur = GetCurrentUserRole();
            if (ur != null)
            {
                int users = UserBaseProvider.CountUsersHasRole(ur) ;
                if (users > 0)
                {
                    XtraMessageBox.Show(Resources.DeleteBan, string.Format(Resources.UsersWithRole, users));  
                    return false;
                }
                if (XtraMessageBox.Show(Resources.ConfirmDeleteQuestion,
               Resources.ConfirmDeletion,
               MessageBoxButtons.YesNoCancel,
               MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return ur.Delete();
                }
                else
                    return false;
            }
            else
                return false;
        }

        private UserRole GetCurrentUserRole()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[gcRoles.DataSource];
            UserRole ur = (UserRole)cm.Current;
            return ur;
        }

        private void UserRolesEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (RolesChanged && RefreshRolesList != null)
                RefreshRolesList();
        }

        void Localization()
        {
            this.Text = Resources.UsersRolesEditor;
            colName.Caption = Resources.UserRole; 
        }

    }
}