﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Properties;

namespace TrackControl.General
{
    public static class ModulesController
    {
        public static BindingList<TCModule> GetModulesList()
        {
            BindingList<TCModule> modules = new BindingList<TCModule>();
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                if (db.IsTableExist("agro_order")) modules.Add(new TCModule((int)TrackControlModules.Agro, Resources.ModuleAgro));
                if (db.IsTableExist("rt_route")) modules.Add(new TCModule((int)TrackControlModules.Route, Resources.ModuleRoutes));
                if (db.IsTableExist("ntf_main")) modules.Add(new TCModule((int)TrackControlModules.Notifications, Resources.Notification));
                if (db.IsTableExist("md_order")) modules.Add(new TCModule((int)TrackControlModules.Route, Resources.ModuleMotoDeport));
            }
            return modules;
        }
    }
    public enum TrackControlModules
    {
        Agro = 1,
        Route,
        Notifications,
        MotoDeport
    }
}
