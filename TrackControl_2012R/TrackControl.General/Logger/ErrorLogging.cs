using System;
using System.IO;

namespace TrackControl.General.Log
{
    class ErrorLogging
    {
        private const string LOG_FORMAT = "{0} {1}";

        private const string DATE_FORMAT = " dd_MM_yyyy";

        /// <summary>
        /// ������ ������� ���� � ����
        /// 0 - ���������
        /// 1 - �������(��������)
        /// 2 - ����
        /// </summary>
        private const string FULL_PATH_FORMAT = @"{0}\{1}_EXCEPT{2}.log";

        private static object syncObject = new Object();

        public static void Log(string AppLevel, string logMessage)
        {
            lock (syncObject)
            {
                string fullPath = String.Format(FULL_PATH_FORMAT, AppDomain.CurrentDomain.BaseDirectory,
                       AppLevel, DateTime.Now.ToString(DATE_FORMAT));

                using (StreamWriter logWriter = File.AppendText(fullPath))
                {
                    DateTime date = DateTime.Now;
                    logWriter.Write("\r\nLog Entry : ");
                    logWriter.WriteLine("{0}:{1}", date.ToLongTimeString(), date.Millisecond);
                    logWriter.WriteLine("  :");
                    logWriter.WriteLine("  :{0}", logMessage);
                    logWriter.WriteLine("------------------------------------------------");
                    // Update the underlying file.
                    //wlogWriterFlush();
                    logWriter.Close();
                }
            }
        }
        public static void DumpLog(string file)
        {
            lock (syncObject)
            {
                using (StreamReader r = File.OpenText(file))
                {

                    // While not at the end of the file, read and write lines.
                    String line;
                    while ((line = r.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                    r.Close();
                }
            }
        }
    }
}
