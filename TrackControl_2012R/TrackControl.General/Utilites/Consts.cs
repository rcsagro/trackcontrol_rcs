﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Drawing ;

namespace TrackControl.General
{
    public  static class ConstsGen
    {
        public const int RECORD_MISSING = -1;
        public const int USER_ADMIN_CODE = -2;
        public const string USER_ADMIN_NAME = "admin";
        public const string SqlVehicleIdent = "CONCAT(IFNULL(vehicle.NumberPlate,''),' | ' , IFNULL(vehicle.CarModel,''),' | ' , IFNULL(vehicle.MakeCar,''))";
        public const string msSqlVehicleIdent = "cast(ISNULL(vehicle.NumberPlate,'') AS VARCHAR) + ' | ' + cast(ISNULL(vehicle.CarModel,'') AS VARCHAR) + ' | ' + cast(ISNULL(vehicle.MakeCar,'') AS varchar)";
        private static string keyLat = ConfigurationManager.AppSettings.GetKey(2);
        private static string keyLon = ConfigurationManager.AppSettings.GetKey(3);
        private static string keyZoom = ConfigurationManager.AppSettings.GetKey(4);
        private static double lat = Convert.ToDouble(keyLat);
        private static double lng = Convert.ToDouble(keyLon);
        private static int zoom = Convert.ToInt32(keyZoom);
        public static double KievLat = lat;
        public static double KievLng = lng;
        public static int KievZoom = zoom;
    }
}
