﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Data; 
using System.Windows.Forms;
using TrackControl.General.Properties;
using System.Diagnostics;

namespace TrackControl.General
{
    public static class StaticMethods
    {
        public static string TimeSpanFormattingExtensionDHM(TimeSpan ts)
        {
            string toreturn;
            if (ts.Days > 0)
                toreturn = ts.Days.ToString() + ".";
            else
                toreturn = "";toreturn = toreturn + ts.Hours + ":" + ts.Minutes;
            return toreturn;
        }

        public static string ShowSaveFileDialog(string FileName, string Title, string Filter)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Title = Resources.ExportTo + " " + Title;
            dlg.FileName = FileName;
            dlg.Filter = Filter;
            if (dlg.ShowDialog() == DialogResult.OK) return dlg.FileName;
            return "";
        }

        public static string ShowOpenFileDialog(string Title, string Filter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = Title;
            dlg.Filter = Filter;
            if (dlg.ShowDialog() == DialogResult.OK) return dlg.FileName;
            return "";
        }

        public static string SuffixDateToFileName()
        {
            return (String.Format("{0}_{1}_{2}_{3}" ,DateTime.Now.Day,DateTime.Now.Hour,DateTime.Now.Minute,DateTime.Now.Second));
        }

        public static void DebugPrintDataTable(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Work = "";

                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    Work = Work + dt.Rows[i][j].ToString() + " ";
                }

                Debug.Print(Work);
            }
        }

        public static string GetTotalMessageCaption()
        {
            return String.Format("TrackControl {0}", Application.ProductVersion);
        }

        /// <summary>
        /// Определение метра по долготе
        /// </summary>
        public static double GetMetrLng(double Lng, double Lat)
        {
            double segment = 0;
            double step = 0;
            for (int i = 0; i < 100; i++)
            {
                step = step + 0.000001;
                segment = СGeoDistance.CalculateFromGrad(Lat, Lng, Lat, Lng + step);
                if (segment >= 0.001) break;
            }
            return step;
        }
        /// <summary>
        /// Определение метра по широте
        /// </summary>
        public static double GetMetrLat(double Lng, double Lat)
        {
            double segment = 0;
            double step = 0;
            for (int i = 0; i < 100; i++)
            {
                step = step + 0.000001;
                segment = СGeoDistance.CalculateFromGrad(Lat, Lng, Lat + step, Lng);
                if (segment >= 0.001) break;
            }

            return step;
        }

        /// <summary>
        /// Среднее значение метра по широте и долготе
        /// </summary>
        public static double GetMetrAvgLatLng(double Lng, double Lat)
        {
            return Math.Round((GetMetrLat(Lng, Lat) + GetMetrLng(Lng, Lat)) / 2, 6);
        }
        
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static long ConvertToUnixTimestamp(DateTime date)
        {
            //TimeSpan dt =
            //    new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour,
            //                 DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond) - DateTime.UtcNow;
            //DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0) + dt;
            

            //TimeSpan diff = date - origin; 
            //return Convert.ToInt64(diff.TotalSeconds);

            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64((date.ToUniversalTime() - origin).TotalSeconds);
        }

        public static int GetStatusColor(DateTime timeStatus)
        {
            if (DateTime.Now.Subtract(timeStatus).TotalHours < 1)
                return (int)TimeStatuses.Active;
            else if (DateTime.Now.Subtract(timeStatus).TotalHours < 24)
                return (int)TimeStatuses.IntervalNear;
            else if (DateTime.Now.Subtract(timeStatus).TotalDays < 100)
                return (int)TimeStatuses.IntervalFar;
            else
                return (int)TimeStatuses.Offline;
        }

        public enum TimeStatuses
        {
            Active,
            IntervalNear,
            IntervalFar,
            Offline
        }
    }}
