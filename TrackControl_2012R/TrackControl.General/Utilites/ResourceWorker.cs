﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Media;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TrackControl.General
{
    public class ResourceWorker
    {
        private Assembly _assembly;
        public ResourceWorker(Assembly assembly)
        {
            _assembly = assembly;
        }

        /// <summary>
        /// Возвращает массив названий встроенных ресурсов, имеющих указанный префикс.
        /// Расширениние файлов должно состоять из трех символов. Например: .wav, .png, .jpg и т.д.
        /// </summary>
        /// <param name="prefix">Префикс</param>
        /// <returns>Массив названий ресурсов</returns>
        private  string[] getResourceNames(string prefix)
        {
            List<string> result = new List<string>();
            foreach (string resourceName in _assembly.GetManifestResourceNames())
            {
                if (resourceName.IndexOf(prefix) == 0)
                {
                    string name = resourceName.Substring(prefix.Length); // Удаляем префикс
                    name = name.Remove(name.Length - 4); // Удаляем расширение файла

                    result.Add(name);
                }
            }
            result.Sort(); 
            return result.ToArray();
        }
        /// <summary>
        /// Возвращает названия всех доступных звуковых сигналов.
        /// </summary>
        /// <returns>Массив названий звуковых сигналов</returns>
        public  string[] GetSoundNames()
        {
            string prefix = String.Format("{0}.Sounds.", _assembly.GetName().Name);
            return getResourceNames(prefix);
        }
        public void PlaySound(string soundName)
        {
            System.IO.Stream s = _assembly.GetManifestResourceStream(string.Format("{0}.Sounds.{1}.wav", _assembly.GetName().Name, soundName));
            if (s != null)
            {
                try
                {
                    SoundPlayer player = new SoundPlayer(s);
                    player.Play();
                }
                catch (Exception ex)
                {
                   MessageBox.Show(ex.Message)  ; 
                }
            }
         }
    }
}
