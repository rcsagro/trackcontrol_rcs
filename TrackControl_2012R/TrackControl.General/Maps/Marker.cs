using System;

namespace TrackControl.General
{
    /// <summary>
    /// ������ ��� ����������� �� �����
    /// </summary>
    public class Marker
    {
        /// <summary>
        /// ��� �������
        /// </summary>
        public MarkerType MarkerType
        {
            get { return _markerType; }
        }

        private MarkerType _markerType;

        /// <summary>
        /// ID �������
        /// </summary>
        public Int32 Id
        {
            get { return _id; }
        }

        private Int32 _id;

        /// <summary>
        /// �������������� �����, �� ������� ��������� ������
        /// </summary>
        public PointLatLng Point
        {
            get { return _point; }
        }

        private PointLatLng _point;

        /// <summary>
        /// ������� � �������
        /// </summary>
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _title;

        /// <summary>
        /// �������� �������� ��� �������. ��������, ������������
        /// ��� ����������� ���������
        /// </summary>
        public string Descriptoin
        {
            get { return _descriptoin; }
        }

        private string _descriptoin;

        /// <summary>
        /// ��������� �������� ��� �������. ������������
        /// ��� ����������� ���������
        /// </summary>
        public string DescriptoinDetailed
        {
            get { return _descriptoinDetailed; }
            set { _descriptoinDetailed = value; }

        }

        private string _descriptoinDetailed;

        /// <summary>
        /// �������, �������� �� ������ ����������
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private bool _isActive;

        /// <summary>
        /// ������� ������������ ����������� ������� �������� (��������� �����)
        /// </summary>
        private int _logicSensorState = (int) LogicSensorStates.Absence;

        public int LogicSensorState
        {
            get { return _logicSensorState; }
            set { _logicSensorState = value; }
        }

        /// <summary>
        /// ������, ���������� ������ ��� �������
        /// </summary>
        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private object _tag;

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.General.Maps.Marker
        /// </summary>
        /// <param name="markerType">��� �������</param>
        /// <param name="id">ID �������</param>
        /// <param name="point">���������� ����� ��� �������</param>
        /// <param name="title">������� ��� �������</param>
        /// <param name="description">����������� ��������� ��� �������</param>
        public Marker(MarkerType markerType, Int32 id, PointLatLng point, string title, string description)
        {
            _markerType = markerType;
            _id = id;
            _point = point;
            _title = title;
            _descriptoin = description;
        }
    }
}