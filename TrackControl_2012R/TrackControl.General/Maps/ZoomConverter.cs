using System;

namespace TrackControl.General
{
  public static class ZoomConverter
  {
    public static double ToUkrGIS(int scale)
    {
      scale = Math.Max(scale, Globals.GMAP_MIN);
      scale = Math.Min(scale, Globals.GMAP_MAX);
      return Globals.GIS_MIN * Math.Exp((Globals.GMAP_MAX - scale) * Math.Log(2));
    }

    public static int ToGMap(double scale)
    {
      scale = Math.Max(scale, Globals.GIS_MIN);
      scale = Math.Min(scale, Globals.GIS_MAX);
      return Convert.ToInt32(Math.Floor(17 - Math.Log((scale / Globals.GIS_MIN), 2)));
    }
  }
}
