using System;

namespace TrackControl.General
{
    public interface IGeoLocator
    {
        /// <summary>
        /// ���������� ��������� ������������� ��������������� �������
        /// </summary>
        /// <param name="point">�������������� ����� ���������� �������</param>
        string GetLocationInfo(PointLatLng point);

        /// <summary>
        /// ���������� ��������� ������������� ��������������� ������� � ��������� �������
        /// </summary>
        /// <param name="location">�������������� ����� ���������� �������</param>
        /// <param name="iRadius">������ ����������� ����� � ������. ���� ���� ������������ ������ �� ���������</param>
        /// <returns></returns>
        string GetLocationInfoRadius(PointLatLng location, int iRadius);
    }
}
