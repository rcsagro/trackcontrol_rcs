using System;
using System.Collections.Generic;

namespace TrackControl.General
{
    public interface IMap
    {
        /// <summary>
        /// ��������� ����� (���������� ������ � �������)
        /// </summary>
        MapState State { get; set; }

        /// <summary>
        /// �������� ���������� ������ ���� �����
        /// </summary>
        /// <param name="p">�������������� ����� ������ ������ ���� �����</param>
        void PanTo( PointLatLng point );

        /// <summary>
        /// �������� ��������� ���� �����, ����� ����������� ��������
        /// �������� ������������� �������
        /// </summary>
        /// <param name="r">������������� �������, ������� ���������� ��������</param>
        void PanTo( RectLatLng rect );

        void AddMarker( Marker marker );
        void AddMarkers( IEnumerable<Marker> markers );
        void ClearMarkers();

        void AddTrack( Track track );
        void AddTracks( IEnumerable<Track> tracks );
        void ClearTracks();

        void AddZone( IZone zone );
        void AddZones( IEnumerable<IZone> zones );
        void ClearZones();

        /// <summary>
        /// ������� � ����� ��� ����������� ������� (�����������
        /// ����, �����, �������)
        /// </summary>
        void ClearAll();

        /// <summary>
        /// ������������� ������������ �������, ����� �������� ��� �����������
        /// ������� � �����
        /// </summary>
        void ZoomAndCenterAll();

        void Repaint();

        void ResetTools();

        List<PointLatLng> PointsFromTrack { get; set; }
    }
}
