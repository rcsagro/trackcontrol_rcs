﻿using System;
using System.Collections.Generic;

namespace TrackControl.General
{
  public class СGeoDistance
  {
    //Equatorial radius of the earth from WGS 84 
    //in meters, semi major axis = a
    private static int a = 6378137;

    //flattening = 1/298.257223563 = 0.0033528106647474805
    //first eccentricity squared = e = (2-flattening)*flattening
    private static double e = 0.0066943799901413165;

    /// <summary>
    /// Коэффициент пересчета градусов в радианы.
    /// Еквивалент Math.PI / 180
    /// </summary>
    const double RADIAN_DEGREE_RATIO = 0.0174532925199433;

    //расстояние в километрах
    /// <summary>
    /// Calculate from grad
    /// </summary>
    /// <param name="lat0">Lat 0</param>
    /// <param name="lon0">Lon 0</param>
    /// <param name="lat">Lat</param>
    /// <param name="lon">Lon</param>
    /// <returns>Double</returns>
    public static double CalculateFromGrad(double lat0, double lon0, double lat, double lon)
    {
      lat0 = lat0 * RADIAN_DEGREE_RATIO;
      lon0 = lon0 * RADIAN_DEGREE_RATIO;
      lat = lat * RADIAN_DEGREE_RATIO;
      lon = lon * RADIAN_DEGREE_RATIO;

      return CalculateFromRadian(lat0, lon0, lat, lon);
    }

    /// <summary>
    /// Возвращает расстояние в километрах между двумя точка
    /// </summary>
    /// <param name="a">Первая точка в географических координатах</param>
    /// <param name="b">Вторая точка в географических координатах</param>
    public static double GetDistance(PointLatLng a, PointLatLng b)
    {
      double latA = a.Lat * RADIAN_DEGREE_RATIO;
      double lngA = a.Lng * RADIAN_DEGREE_RATIO;

      double latB = b.Lat * RADIAN_DEGREE_RATIO;
      double lngB = b.Lng * RADIAN_DEGREE_RATIO;

      return CalculateFromRadian(latA, lngA, latB, lngB);
    }

    /// <summary>
    /// Возвращает расстояние в километрах между точками массива
    /// </summary>
    public static double GetDistance(List<PointLatLng> points)
    {
        double distance = 0;
        for (int i = 1; i < points.Count ; i++)
        {
            distance += GetDistance(points[i - 1], points[i]);
        }
        return distance;
    }

    public static double CalculateArea(IList<PointLatLng> points)
    {
      if (points.Count < 3)
        return 0.0;

      int count = points.Count;
      double x, y, area = 0.0;
      for (int i = 0; i < count; i++)
      {
        x = dLng(points[i], points[0]);
        y = dLat(points[(i - 1 + count) % count], points[(i + 1) % count]);
        area += x * y / 2;
      }

      return Math.Abs(area / 1000000);
    }

    static double dLat(PointLatLng p1, PointLatLng p2)
    {
      double Rm = calcMeridionalRadiusOfCurvature(p1.Lat * RADIAN_DEGREE_RATIO);
      return Rm * (p1.Lat * RADIAN_DEGREE_RATIO - p2.Lat * RADIAN_DEGREE_RATIO);
    }

    static double dLng(PointLatLng p1, PointLatLng p2)
    {
      double Rpv = calcRoCinPrimeVertical(p1.Lat * RADIAN_DEGREE_RATIO);
      return Rpv * Math.Cos(p1.Lat * RADIAN_DEGREE_RATIO) * (p2.Lng * RADIAN_DEGREE_RATIO - p1.Lng * RADIAN_DEGREE_RATIO);
    }

    /// <summary>
    /// Calculate from radian
    /// </summary>
    public static double CalculateFromRadian(double lat0, double lon0, double lat, double lon)
    {
      double Rm = calcMeridionalRadiusOfCurvature(lat0);

      double Rpv = calcRoCinPrimeVertical(lat0);

      double distance = Math.Sqrt(Math.Pow(Rm, 2) * Math.Pow(lat - lat0, 2) +
          Math.Pow(Rpv, 2) * Math.Pow(Math.Cos(lat0), 2) * Math.Pow(lon - lon0, 2));

      return distance / 1000;
    }

    /// <summary>
    /// Calc ro cin prime vertical
    /// </summary>
    private static double calcRoCinPrimeVertical(double lat0)
    {
      return a / Math.Sqrt(1 - e * Math.Pow(Math.Sin(lat0), 2));
    }

    /// <summary>
    /// Calc meridional radius of curvature
    /// </summary>
    private static double calcMeridionalRadiusOfCurvature(double lat0)
    {
      return a * (1 - e) / Math.Pow(1 - e * (Math.Pow(Math.Sin(lat0), 2)), 1.5);
    }
  }
}