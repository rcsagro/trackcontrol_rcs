﻿using System;
using System.Collections.Generic;

namespace TrackControl.General
{
  /// <summary>
  /// Прямоугольная область в географических координатах.
  /// </summary>
  public struct RectLatLng
  {
    public static readonly RectLatLng Empty;

    public double Lng
    {
      get { return _lng; }
      set { _lng = value; }
    }
    double _lng;

    public double Lat
    {
      get { return _lat; }
      set { _lat = value; }
    }
    double _lat;

    public double WidthLng
    {
      get { return _widthLng; }
      set { _widthLng = value; }
    }
    double _widthLng;

    public double HeightLat
    {
      get { return _heightLat; }
      set { _heightLat = value; }
    }
    double _heightLat;

    public RectLatLng(double lat, double lng, double widthLng, double heightLat)
    {      
      _lng = lng;
      _lat = lat;
      _widthLng = widthLng;
      _heightLat = heightLat;
    }

    public RectLatLng(PointLatLng location, SizeLatLng size)
    {
      _lng = location.Lng;
      _lat = location.Lat;
      _widthLng = size.WidthLng;
      _heightLat = size.HeightLat;
    }

    public static RectLatLng FromLTRB(double lng, double lat, double rightLng, double bottomLat)
    {
      return new RectLatLng(lat, lng, rightLng - lng, lat - bottomLat);
    }

    /// <summary>
    /// Вычисляет прямоугольную область, которая охватывает коллекцию точек
    /// </summary>
    /// <param name="points">Коллекция географических точек</param>
    /// <returns>Прямоугольная область в географических координатах</returns>
    public static RectLatLng Calculate(IEnumerable<PointLatLng> points)
    {
      double left = double.MaxValue;
      double top = double.MinValue;
      double right = double.MinValue;
      double bottom = double.MaxValue;
      foreach (PointLatLng p in points)
      {
        // left
        if (p.Lng < left)
          left = p.Lng;

        // top
        if (p.Lat > top)
          top = p.Lat;

        // right
        if (p.Lng > right)
          right = p.Lng;

        // bottom
        if (p.Lat < bottom)
          bottom = p.Lat;
      }
      return (double.MaxValue != left) ? FromLTRB(left, top, right, bottom) : Empty;
    }

    public static RectLatLng Calculate(IEnumerable<IGeoPoint> points)
    {
      double left = double.MaxValue;
      double top = double.MinValue;
      double right = double.MinValue;
      double bottom = double.MaxValue;
      foreach (IGeoPoint igp in points)
      {
        if (igp.LatLng.Lng < left) left = igp.LatLng.Lng;     // LEFT
        if (igp.LatLng.Lat > top) top = igp.LatLng.Lat;       // TOP
        if (igp.LatLng.Lng > right) right = igp.LatLng.Lng;   // RIGHT
        if (igp.LatLng.Lat < bottom) bottom = igp.LatLng.Lat; // BOTTOM
      }
      return (double.MaxValue != left) ? FromLTRB(left, top, right, bottom) : Empty;
    }

    public PointLatLng LocationTopLeft
    {
      get
      {
        return new PointLatLng(Lat, Lng);
      }
      set
      {
        Lng = value.Lng;
        Lat = value.Lat;
      }
    }

    public PointLatLng LocationRightBottom
    {
      get
      {
        PointLatLng ret = new PointLatLng(Lat, Lng);
        ret.Offset(HeightLat, WidthLng);
        return ret;
      }
    }

    public PointLatLng LocationTopRight
    {
        get
        {
            PointLatLng ret = new PointLatLng(Lat, Lng);
            ret.Offset(0, WidthLng);
            return ret;
        }
    }

    public PointLatLng LocationLeftBottom
    {
        get
        {
            PointLatLng ret = new PointLatLng(Lat, Lng);
            ret.Offset(HeightLat, 0);
            return ret;
        }
    }

    public PointLatLng LocationCenter
    {
        get
        {
            PointLatLng ret = new PointLatLng(Lat, Lng);
            ret.Offset(HeightLat/2, WidthLng/2);
            return ret;
        }
    }

    public SizeLatLng Size
    {
      get
      {
        return new SizeLatLng(HeightLat, WidthLng);
      }
      set
      {
        WidthLng = value.WidthLng;
        HeightLat = value.HeightLat;
      }
    }
    public double Left
    {
      get
      {
        return Lng;
      }
    }

    public double Top
    {
      get
      {
        return Lat;
      }
    }

    public double Right
    {
      get
      {
        return (Lng + WidthLng);
      }
    }

    public double Bottom
    {
      get
      {
        return (Lat - HeightLat);
      }
    }

    public bool IsEmpty
    {
      get
      {
        if (WidthLng > 0d)
        {
          return (HeightLat <= 0d);
        }
        return true;
      }
    }

    public override bool Equals(object obj)
    {
      if (!(obj is RectLatLng))
      {
        return false;
      }
      RectLatLng ef = (RectLatLng)obj;
      return ((((ef.Lng == Lng) && (ef.Lat == Lat)) && (ef.WidthLng == WidthLng)) && (ef.HeightLat == HeightLat));
    }

    public static bool operator ==(RectLatLng left, RectLatLng right)
    {
      return ((((left.Lng == right.Lng) && (left.Lat == right.Lat)) && (left.WidthLng == right.WidthLng)) && (left.HeightLat == right.HeightLat));
    }

    public static bool operator !=(RectLatLng left, RectLatLng right)
    {
      return !(left == right);
    }

    public bool Contains(double lat, double lng)
    {
      return ((((Lng <= lng) && (lng < (Lng + WidthLng))) && (Lat >= lat)) && (lat > (Lat - HeightLat)));
    }

    public bool Contains(PointLatLng pt)
    {
      return Contains(pt.Lat, pt.Lng);
    }

    public bool Contains(RectLatLng rect)
    {
      return ((((Lng <= rect.Lng) && ((rect.Lng + rect.WidthLng) <= (Lng + WidthLng))) && (Lat >= rect.Lat)) && ((rect.Lat - rect.HeightLat) >= (Lat - HeightLat)));
    }

    public override int GetHashCode()
    {
      return (int)(((((uint)Lng) ^ ((((uint)Lat) << 13) | (((uint)Lat) >> 0x13))) ^ ((((uint)WidthLng) << 0x1a) | (((uint)WidthLng) >> 6))) ^ ((((uint)HeightLat) << 7) | (((uint)HeightLat) >> 0x19)));
    }

    #region -- Не тестировалось --
    public void Inflate(double lat, double lng)
    {
      Lng -= lng;
      Lat += lat;
      WidthLng += 2d * lng;
      HeightLat += 2d * lat;
    }

    public void Inflate(SizeLatLng size)
    {
      Inflate(size.HeightLat, size.WidthLng);
    }

    public static RectLatLng Inflate(RectLatLng rect, double lat, double lng)
    {
      RectLatLng ef = rect;
      ef.Inflate(lat, lng);
      return ef;
    }

    public void Intersect(RectLatLng rect)
    {
      RectLatLng ef = Intersect(rect, this);
      Lng = ef.Lng;
      Lat = ef.Lat;
      WidthLng = ef.WidthLng;
      HeightLat = ef.HeightLat;
    }

    // ok ???
    public static RectLatLng Intersect(RectLatLng a, RectLatLng b)
    {
      double lng = Math.Max(a.Lng, b.Lng);
      double num2 = Math.Min((double)(a.Lng + a.WidthLng), (double)(b.Lng + b.WidthLng));

      double lat = Math.Max(a.Lat, b.Lat);
      double num4 = Math.Min((double)(a.Lat + a.HeightLat), (double)(b.Lat + b.HeightLat));

      if ((num2 >= lng) && (num4 >= lat))
      {
        return new RectLatLng(lng, lat, num2 - lng, num4 - lat);
      }
      return Empty;
    }

    // ok ???
    public bool IntersectsWith(RectLatLng rect)
    {
      return ((((rect.Lng < (Lng + WidthLng)) && (Lng < (rect.Lng + rect.WidthLng))) && (rect.Lat > (Lat - HeightLat))) && (Lat > (rect.Lat - rect.HeightLat)));
    }

    // ok ???
    public static RectLatLng Union(RectLatLng a, RectLatLng b)
    {
      double left = Math.Min(a.Left, b.Left);
      double right = Math.Max(a.Right, b.Right);
      double top = Math.Max(a.Top, b.Top);
      double bottom = Math.Min(a.Bottom, b.Bottom);
      return new RectLatLng(top, left, right - left, top - bottom);
    }
    #endregion

    public void Offset(PointLatLng pos)
    {
      Offset(pos.Lat, pos.Lng);
    }

    public void Offset(double lat, double lng)
    {
      Lng += lng;
      Lat -= lat;
    }

    public override string ToString()
    {
      return String.Format("Lat={0:N5} Lng={1:N5} Width={2:N5} Height={3:N5}", Lat, Lng, WidthLng, HeightLat);
    }

    static RectLatLng()
    {
      Empty = new RectLatLng();
    }
  }
}