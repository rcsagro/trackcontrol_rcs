﻿using System;

namespace TrackControl.General
{
    /// <summary>
    /// Точка в географических координатах.
    /// </summary>
    public struct PointLatLng
    {
        public static readonly PointLatLng Empty = new PointLatLng();

        public double Lat
        {

            get { return _lat; }
            set { _lat = value; }

        }

        private double _lat;

        public double Lng
        {
            get { return _lng; }
            set { _lng = value; }
        }

        private double _lng;

        public PointLatLng(double lat, double lng)
        {
            _lat = lat;
            _lng = lng;
        }

        public static PointLatLng IncorrectPoint
        {
            get { return new PointLatLng(Double.MaxValue, Double.MaxValue); }
        }

        public bool IsCorrect
        {
            get { return _lat < Double.MaxValue && _lng < Double.MaxValue; }
        }

        public bool IsEmpty
        {
            get { return ((_lng == 0d) && (_lat == 0d)); }
        }

        public double Magnitude
        {
            get { return Math.Sqrt(_lat*_lat + _lng*_lng); }
        }

        public static PointLatLng operator -(PointLatLng left, PointLatLng right)
        {
            return new PointLatLng(left.Lat - right.Lat, left.Lng - right.Lng);
        }

        public static bool operator ==(PointLatLng left, PointLatLng right)
        {
            return ((left.Lng == right.Lng) && (left.Lat == right.Lat));
        }

        public static bool operator !=(PointLatLng left, PointLatLng right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PointLatLng))
            {
                return false;
            }
            PointLatLng tf = (PointLatLng) obj;
            return (((tf.Lng == Lng) && (tf.Lat == Lat)) && tf.GetType().Equals(base.GetType()));
        }

        public void Offset(PointLatLng pos)
        {
            Offset(pos.Lat, pos.Lng);
        }

        public void Offset(double lat, double lng)
        {
            _lng += lng;
            _lat -= lat;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Lat={0:N5}, Lng={1:N5}", _lat, _lng);
        }
    }
}