using System;

namespace TrackControl.General
{
    /// <summary>
    /// ��������� �����
    /// </summary>
    public class MapState
    {
        public Int32 Zoom
        {
            get { return _zoom; }
            set { _zoom = value; }
        }

        private Int32 _zoom;

        /// <summary>
        /// �������������� ���������� ������ ���� �����
        /// </summary>
        public PointLatLng Center
        {
            get { return _center; }
            set { _center = value; }
        }

        private PointLatLng _center;

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.General.Maps.MapState
        /// </summary>
        /// <param name="zoom">������� �����</param>
        /// <param name="center">����� ���� �����</param>
        public MapState(Int32 zoom, PointLatLng center)
        {
            _zoom = zoom;
            _center = center;
        }
    }
}
