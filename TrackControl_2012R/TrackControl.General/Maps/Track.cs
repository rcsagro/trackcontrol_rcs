using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using DevExpress.XtraEditors;

namespace TrackControl.General
{
    /// <summary>
    /// ���� ��� ����������� �� �����
    /// </summary>
    public class Track
    {
        public int Id
        {
            get { return _id; }
        }

        private int _id;

        /// <summary>
        /// ���� ����� ��� ��������� �� �����
        /// </summary>
        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private Color _color;

        /// <summary>
        /// ������ ����� ����� � ��������
        /// </summary>
        public Single Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private Single _width;

        /// <summary>
        /// ������ ���������� �������� � ������ - �������������� ��������� ��������� ���� ("������� ����")
        /// </summary>
        private double _widthAgregatMeter;

        public double WidthAgregatMeter
        {
            get { return _widthAgregatMeter; }
            set { _widthAgregatMeter = value; }
        }

        /// <summary>
        /// ���� �������������� ��������� ��������� ���� ("������� ����")
        /// </summary>
        private Color _colorAgregat;

        public Color ColorAgregat
        {
            get { return _colorAgregat; }
            set { _colorAgregat = value; }
        }

        public List<IGeoPoint> GeoPoints
        {
            get
            {
                try
                {
                    return new List<IGeoPoint>(_geoPoints);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        private List<IGeoPoint> _geoPoints;

        public IGeoPoint LastPoint
        {
            get { return _geoPoints[_geoPoints.Count - 1]; }
        }

        /// <summary>
        /// ������������� �������, ������������ ��� ����� �����
        /// </summary>
        public RectLatLng Bounds
        {
            get { return _bounds; }
        }

        private RectLatLng _bounds;

        /// <summary>
        /// ������, ���������� ������ ��� �����
        /// </summary>
        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private object _tag;

        /// <summary>
        /// �������, �������� �� ������ ����������
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private bool _isActive;

        /// <summary>
        /// ����������� ��� ������ TrackControl.General.Maps.Track
        /// </summary>
        public Track(int id, Color color, float width, IEnumerable<IGeoPoint> geoPoints)
        {
            if (!geoPoints.GetEnumerator().MoveNext())
            {
               // throw new Exception("Track must contain at least one point");
                return;
            }

            _id = id;
            _color = color;
            _width = width;
            _geoPoints = new List<IGeoPoint>(geoPoints);
            _bounds = RectLatLng.Calculate(geoPoints);
        }

        public Track( int id, IEnumerable<IGeoPoint> geoPoints)
            : this(id, Color.Navy, 1F, geoPoints)
        {
            // to do
        }

        public Track( int id, IEnumerable<IGeoPoint> geoPoints, Color colorTrack )
            : this( id, colorTrack, 1F, geoPoints )
        {
            // to do
        }
    }
}
