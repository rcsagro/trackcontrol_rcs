using System;

namespace TrackControl.General
{
  /// <summary>
  /// ��������� ��� ������ � �������������� ���������
  /// </summary>
  public interface IGeoPoint
  {
    /// <summary>
    /// �������������� ���������� ����� ������
    /// </summary>
    PointLatLng LatLng { get; }

    // ��������, ������� �� ���� ��������� �����
    float Speed { get; }
  }
}
