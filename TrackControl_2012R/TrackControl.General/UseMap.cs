﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using DevExpress.Utils.Drawing;

namespace TrackControl.General
{
    public static class UseMap
    {
        public static string getTypeMap()
        {
            XmlDocument doc = null;
            string path = "";

            try
            {
                doc = new XmlDocument();
                path = String.Format("{0}{1}map.xml", Globals.APP_DATA, Path.DirectorySeparatorChar);

                if (!File.Exists(path))
                    return "GoogleMap";

                doc.Load(path);
                return doc.DocumentElement.SelectSingleNode("type").InnerText;
            }
            catch (Exception e)
            {
                doc = null;
                saveParamIntoXml("GoogleMap", "type");
                return "GoogleMap";
            }
        }

        private static void saveMapFile(string path)
        {
            string settingsPath = Path.Combine(Globals.APP_DATA, "map.xml");
            string text = String.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
<map>
  <id>2</id>
  <path>{0}</path>
  <type>GoogleMap</type>
  <zoom>8</zoom>
</map>", path);
            using (StreamWriter writer = new StreamWriter(settingsPath, false, Encoding.UTF8))
            {
                writer.Write(text);
            }
        }

        public static void saveParamIntoXml(string param, string name)
        {
            XmlDocument doc = null;
            string path = "";

            try
            {
                doc = new XmlDocument();
                path = String.Format("{0}{1}map.xml", Globals.APP_DATA, Path.DirectorySeparatorChar);

                if (!File.Exists(path))
                    return;

                doc.Load(path);
                XmlNode map = doc.DocumentElement.SelectSingleNode(name);

                if (map == null)
                {
                    string paths = doc.DocumentElement.SelectSingleNode("path").InnerText;
                    saveMapFile(paths);
                    return;
                }

                map.InnerText = param;
                doc.Save(path);
            }
            catch (Exception e)
            {
                doc = null;
            }
        }

        public static string getZoomMap()
        {
            XmlDocument doc = null;
            string path = "";

            try
            {
                doc = new XmlDocument();
                path = String.Format("{0}{1}map.xml", Globals.APP_DATA, Path.DirectorySeparatorChar);

                if (!File.Exists(path))
                    return "8";

                doc.Load(path);
                return doc.DocumentElement.SelectSingleNode("zoom").InnerText;
            }
            catch (Exception e)
            {
                doc = null;
                saveParamIntoXml("8", "zoom");
                return "8";
            }
        }
    }
}
