using System;
using System.Configuration;

namespace TrackControl.SrvRepLib.Test
{
  /// <summary>
  /// ��������� ������� ������� ������ �� ��.
  /// </summary>
  public static class QueryParams
  {
    private static String ttId;
    /// <summary>
    /// ����������������� ������������� ���������.
    /// </summary>
    public static String TtId
    {
      get { return ttId; }
    }

    private static DateTime beginTime;
    /// <summary>
    /// ��������� �����.
    /// </summary>
    public static DateTime BeginTime
    {
      get { return beginTime; }
    }

    private static DateTime endTime;
    /// <summary>
    /// �������� �����.
    /// </summary>
    public static DateTime EndTime
    {
      get { return endTime; }
    }

    private static string connectionString;
    /// <summary>
    /// ������ ����������� � ��.
    /// </summary>
    public static string ConnectionString
    {
      get { return connectionString; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    static QueryParams()
    {
      try
      {
        ttId = ConfigurationManager.AppSettings["TtId"];
        beginTime = Convert.ToDateTime(ConfigurationManager.AppSettings["BeginTime"]);
        endTime = Convert.ToDateTime(ConfigurationManager.AppSettings["EndTime"]);
        connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
      }
      catch (Exception ex)
      {
        throw new Exception("������ �������� ����� �������� TrackControl.SrvRepLib.Test.exe.config.", ex);
      }
    }
  }
}
