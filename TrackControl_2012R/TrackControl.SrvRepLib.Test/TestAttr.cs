using System;

namespace TrackControl.SrvRepLib.Test
{
  class Test : Attribute
  {
    private string name;
    public string Name
    {
      get { return name; }
      set { name = value; }
    }

    public Test(string name)
    {
      Name = name;
    }
  }
}
