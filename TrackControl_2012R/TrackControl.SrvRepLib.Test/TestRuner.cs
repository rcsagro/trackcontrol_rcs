using System;
using System.Reflection;

namespace TrackControl.SrvRepLib.Test
{
  public class TestRuner
  {
    public void Run()
    {
      WriteHeader();
      try
      {
        ParseAndRun();
      }
      finally
      {
        WriteFooter();
      }
    }

    private void ParseAndRun()
    {
      object obj;
      object[] attrArray;
      foreach (Type t in Assembly.GetExecutingAssembly().GetTypes())
      {
        if ((t.IsClass) && (t.GetCustomAttributes(typeof(Test), false).Length > 0))
        {
          obj = Activator.CreateInstance(t);
          foreach (MethodInfo m in t.GetMethods())
          {
            attrArray = m.GetCustomAttributes(typeof(Test), false);
            if (attrArray.Length > 0)
            {
              RunTest(obj, m, ((Test)attrArray[0]).Name);
            }
          }
        }
      }
    }

    private void RunTest(object obj, MethodInfo m, string testName)
    {
      try
      {
        WriteStartMsg(testName);
        WriteResultMsg(obj.GetType().InvokeMember(
          m.Name,
          BindingFlags.InvokeMethod,
          null,
          obj,
          null));
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
      }
    }

    private void WriteHeader()
    {
      Console.WriteLine("����� ������ ��� ��������� {0}.",
        QueryParams.TtId);
      Console.WriteLine("������ �����������: {0}.",
        QueryParams.ConnectionString);
      Console.WriteLine("������� ������ � {0} �� {1}.",
        QueryParams.BeginTime, QueryParams.EndTime);
    }

    private void WriteFooter()
    {
      Console.WriteLine(String.Empty);
      Console.WriteLine("--------------------------------------");
      Console.WriteLine("����� ���������. ������� ����� �������");
    }

    private void WriteStartMsg(string testName)
    {
      Console.WriteLine(String.Empty);
      Console.WriteLine("���� \"{0}\"", testName);
    }

    private void WriteResultMsg(object result)
    {
      Console.WriteLine("���������: {0}", result);
    }
  }
}
