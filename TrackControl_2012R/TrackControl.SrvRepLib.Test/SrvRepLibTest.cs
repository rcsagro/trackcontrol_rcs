using System;

namespace TrackControl.SrvRepLib.Test
{
  [Test("")]
  public class SrvRepLibTest
  {
    private IReporter reporter;

    public SrvRepLibTest()
    {
      reporter = new Reporter(QueryParams.ConnectionString);
    }

    [Test("������")]
    public object KilometrageTest()
    {
      return String.Format("{0} ��.", reporter.GetKilometrage(
        QueryParams.TtId, QueryParams.BeginTime, QueryParams.EndTime));
    }

    [Test("����� ����� � ��������")]
    public object TotalMotionTimeTest()
    {
      return reporter.GetTotalMotionTime(
        QueryParams.TtId, QueryParams.BeginTime, QueryParams.EndTime);
    }

    [Test("����� ����� ������� � ���������")]
    public object TotalStopsTimeTest()
    {
      return reporter.GetTotalStopsTime(
        QueryParams.TtId, QueryParams.BeginTime, QueryParams.EndTime);
    }
  }
}
