using System;

namespace TrackControl.SrvRepLib.Test
{
  class Program
  {
    static void Main(string[] args)
    {
      try
      {
        new TestRuner().Run();
      }
      catch (Exception ex)
      {
        Console.WriteLine(String.Empty);
        Console.WriteLine(ex);
      }
      finally
      {
        Console.ReadKey();
      }
    }
  }
}
