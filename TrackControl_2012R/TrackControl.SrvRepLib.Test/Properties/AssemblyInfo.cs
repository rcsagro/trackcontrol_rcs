﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TrackControl.SrvRepLib.Test")]
[assembly: AssemblyDescription("Набор тестов для библиотеки TrackControl.SrvRepLib")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS")]
[assembly: AssemblyProduct("TrackControl")]
[assembly: AssemblyCopyright("Copyright © 2008 -2010")]
[assembly: AssemblyTrademark("AVS")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8c2de79b-d6ac-4ab4-a8a7-14ddd54eba4a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
