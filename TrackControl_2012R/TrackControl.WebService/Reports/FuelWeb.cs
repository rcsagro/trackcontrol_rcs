﻿using BaseReports.Procedure;
using LocalCache;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.Vehicles; 

namespace TrackControl.WebService
{
    public class FuelWeb:IDisposable 
    {
        atlantaDataSet _dsAtlanta;
        int _mobitelId;
        DateTime _start;
        DateTime _end;

        public FuelWeb(int mobitelId, DateTime start, DateTime end)
        {
            _mobitelId = mobitelId;
            _start = start;
            _end = end;
        }

        public Dictionary<string, object> GetData()
        {
            var resultDict = new Dictionary<string, object>();
            try
            {
                SetDataSet();
                var kilometrage = new Kilometrage();
                List<GpsData> gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(_mobitelId, _start, _end).ToList();
                kilometrage.SelectItem(_mobitelId, gpsDatas);
                kilometrage.Run();
                List<atlantaDataSet.KilometrageReportRow> retListKm = ((atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_mobitelId)).GetKilometrageReportRows().ToList();
                if (retListKm.Count == 0)
                {
                    resultDict.Add("Status:", false);
                    resultDict.Add("Data:", "");
                }
                else
                {
                    var fuel = new Fuel(AlgorithmType.FUEL1);
                    fuel.SelectItem(_mobitelId, gpsDatas);
                    fuel.Run();
                    List<atlantaDataSet.FuelReportRow> retListFuel = ((atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_mobitelId)).GetFuelReportRows().ToList();
                    if (retListFuel.Count == 0)
                    {
                        resultDict.Add("Status:", false);
                        resultDict.Add("Data:", "");
                    }
                    else
                    {
                        var points = retListFuel.Select(p => new
                        {
                            Location = p.Location,
                            Lat = p.Lat,
                            Lng = p.Lon,
                            Time = p.time_.ToString("yyyy-MM-dd HH:mm"),
                            Filled = Math.Round(p.dValue,2),
                            LevelBefor = Math.Round(p.beginValue,2) ,
                            LevelAfter = Math.Round(p.endValue,2) 
                        });
                        resultDict.Add("Status:", true);
                        resultDict.Add("Data:", points);
                        gpsDatas.Clear(); 
                    }
                }

            }
            catch (Exception ex)
            {
                resultDict.Add("Status:", false);
                resultDict.Add("Data:", ex.Message);
            }
            return resultDict;
        }

        private void SetDataSet()
        {
            _dsAtlanta = new atlantaDataSet();
            var lci = new LocalCacheItem(_start, _end, _mobitelId);
            lci.CreateDSAtlantaForReports(ref _dsAtlanta);
            Algorithm.AtlantaDataSet = _dsAtlanta;
        }

        public object GetFuelLevelByMobitelBetweenDates()
        {

            var resultDict = new Dictionary<string, object>();
            try
            {
                Sensor sensor1 = new Sensor((int)AlgorithmType.FUEL1, _mobitelId);
                if (sensor1.Id <= 0)
                {

                    resultDict.Add("Status:", false);
                    resultDict.Add("Data:", "");
                    return resultDict;
                }
                List<GpsData> gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(_mobitelId, _start, _end).ToList();
                if (gpsDatas.Count == 0)
                {
                    resultDict.Add("Status:", false);
                    resultDict.Add("Data:", "");
                }
                else
                {
                    SetDataSet();
                    FuelDictionarys fuelDict = new FuelDictionarys();
                    Fuel fuel = new Fuel();
                    fuel.SelectItem(_mobitelId, gpsDatas);
                    fuel.GettingValuesDUT(fuelDict);
                    List<object> fuelLevelOut = new List<object>();
                    DateTime timeAddData = DateTime.MinValue;
                    foreach (GpsData d_row in Algorithm.GpsDatas)
                    {
                        if (d_row.Time.Subtract(timeAddData).TotalSeconds > 60)
                        {
                            fuelLevelOut.Add(new { Level = Math.Round(fuelDict.ValueFuelSum[d_row.Id].value, 2),Speed = d_row.Speed , Time = d_row.Time.ToString("yyyy-MM-dd HH:mm") });
                            timeAddData = d_row.Time;
                        }
                    }
                    resultDict.Add("Status:", true);
                    resultDict.Add("Data:", fuelLevelOut);
                    gpsDatas.Clear(); 
                }
            }
            catch (Exception ex)
            {
                resultDict.Add("Status:", false);
                resultDict.Add("Data:", ex.Message);
            }
            return resultDict;
        }

        public void Dispose()
        {
            if (_dsAtlanta != null) _dsAtlanta.Dispose();
        }
    }
}