﻿using BaseReports.Procedure;
using LocalCache;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;

namespace TrackControl.WebService
{
    public class KilometrageWeb
    {
        atlantaDataSet _dsAtlanta;
        int _mobitelId;
        DateTime _start;
        DateTime _end;

        public KilometrageWeb(int mobitelId, DateTime start, DateTime end)
        {
            _mobitelId = mobitelId;
            _start = start;
            _end = end;
        }

        public Dictionary<string, object> GetData()
        {
            Dictionary<string, object> resultDict = new Dictionary<string, object>();
            try
            {
                _dsAtlanta = new atlantaDataSet();
                LocalCacheItem lci = new LocalCacheItem(_start, _end, _mobitelId);
                lci.CreateDSAtlantaForReports(ref _dsAtlanta);
                Algorithm.AtlantaDataSet = _dsAtlanta;
                Kilometrage kilometrage = new Kilometrage();
                List<GpsData> gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(_mobitelId, _start, _end).ToList();
                kilometrage.SelectItem(_mobitelId, gpsDatas);
                kilometrage.Run();
                List<atlantaDataSet.KilometrageReportRow> retList = ((atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_mobitelId)).GetKilometrageReportRows().ToList();
                _dsAtlanta.Dispose();
                if (retList.Count == 0)
                {
                    resultDict.Add("Status:", false);
                    resultDict.Add("Data:", string.Format("GpsDatas.Count = {0}", gpsDatas.Count));
                }
                else
                {
                    var points = retList.Select(p => new
                    {
                        State = (p.State == kilometrage.MovementNameFromResource()) ? 1 : 0,
                        Location = p.Location,
                        Lat = p.Latitude,
                        Lng = p.Longitude,
                        TimeStart = p.InitialTime.ToString("yyyy-MM-dd HH:mm"),
                        TimeEnd = p.FinalTime.ToString("yyyy-MM-dd HH:mm"),
                        Interval = p.Interval.ToString() ,
                        Distance = Math.Round(p.Distance,2),
                        GeneralDistance = Math.Round(p.GeneralDistance,2),
                        MaxSpeed = Math.Round(p.MaxSpeed,2),
                        AvgSpeed = Math.Round(p.AverageSpeed,2)
                    });
                     resultDict.Add("Status:", true);
                    resultDict.Add("Data:", points);
                }
            }
            catch (Exception ex)
            {
                resultDict.Add("Status:", false);
                resultDict.Add("Data:", ex.Message);
            }
            return resultDict;
        }
    }
}