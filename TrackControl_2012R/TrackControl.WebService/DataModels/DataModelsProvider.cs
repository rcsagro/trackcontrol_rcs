﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;
using TrackControl.Zones;
using TrackControl.Reports;
using BaseReports.Procedure.Calibration;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.WebService
{
    public static class DataModelsProvider
    {
         static DataModelsProvider()
         {
             SetDriverDb();
         }
       
        public static List<VehicleDataModel> GetVehicleListByUserId(int UserId)
        {
            var vehProv = new VehicleProvider();
            List<Vehicle> vehicles = vehProv.GetAllOneQuery();
            List<VehicleDataModel> vehicleDataModels = null;
            if (vehicles.Count > 0)
            {
                if (UserId > 0)
                {
                    UserBase ub = UserBaseProvider.GetUserBase(UserId);
                    if (ub != null && !ub.Admin && ub.Role != null)
                    {
                        List<int> unvisibleVehiclesId = UserBaseProvider.GetUnvisibleRoleItems((int)UserAccessObjectsTypes.Vehicles, ub.Role.Id);
                        var vehiclesFiltered = vehicles.Where(vh => !unvisibleVehiclesId.Contains(vh.Id)).ToList();
                        vehicleDataModels = ConvertToVehicleDataModel(vehiclesFiltered);
                    }
                }
            }
            if (vehicleDataModels == null) vehicleDataModels = ConvertToVehicleDataModel(vehicles);
            vehicles.Clear(); 
            return vehicleDataModels;
        }

        private static List<VehicleDataModel> ConvertToVehicleDataModel(List<Vehicle> vehiclesFiltered)
        {
            List<VehicleDataModel> vehicleDataModels = new List<VehicleDataModel>();
            vehiclesFiltered.ForEach(delegate(Vehicle vh)
            {
                VehicleDataModel vdm = new VehicleDataModel();
                vdm.GroupName = string.Format("{0} ({1})", vh.Group.Name, vh.Group.OwnItems.Count());
                vdm.Login = vh.Mobitel.Login;
                vdm.MobitelId = vh.Mobitel.Id;
                vdm.VehicleId = vh.Id;
                vdm.Checked = false;
                vdm.GarageNumber = vh.RegNumber;
                vdm.Model = String.Format("{0} {1}", vh.CarMaker,vh.CarModel);
                vdm.Mark = vh.CarMaker;
                vdm.LastDataSend = vh.Mobitel.LastTimeGps.ToString();
                vdm.MaxStayValueInMinuts = vh.Settings.TimeBreakMaxPermitted.TotalSeconds;
                vdm.MinStayValueInMinuts = vh.Settings.TimeBreak.TotalSeconds;
                vdm.Speed = vh.Settings.SpeedMax;
                vehicleDataModels.Add(vdm); 
            });
            return vehicleDataModels;
        }

        public static Dictionary<string, object> GetLastDataByParams(params string[] deviceList)
        {
             
            Dictionary<string, object> result = new Dictionary<string, object>();
            OnlineDataProvider online = new OnlineDataProvider(); 
            foreach (string device in deviceList)
            {
                int mobitelId = 0;
                if (Int32.TryParse(device, out   mobitelId))
                {
                    GpsData dataGps = online.GetMostRecentDataMobitel(mobitelId) ??
                                      GpsDataProvider.GetLastActiveDataGps(mobitelId);
                    result.Add(device, dataGps == null ? null : ConvertGpsDataForSevice(dataGps));
                }
            }
            return result; 
        }

        public static object GetDataByMobitelBetweenDates(int mobitelId, DateTime start, DateTime end)
        {
            Dictionary<string, object> resultDict = new Dictionary<string, object>();
            try
            {
                IList<GpsData> gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(mobitelId, start, end);
                if (gpsDatas == null || gpsDatas.Count ==0)
                {
                    resultDict.Add("Status:", false);
                    resultDict.Add("Data:", "");
                }
                else
                {
                    List<object> gpsDatasOut = new List<object>();
                    for (int i = 0; i < gpsDatas.Count; i++)
                    {
                        if (i == 0)
                        {
                            AddGpsDataToOutList(gpsDatas, gpsDatasOut,0);
                        }
                        else
                        {
                            if ((gpsDatas[i].LatLng.Lat != gpsDatas[i - 1].LatLng.Lat || gpsDatas[i].LatLng.Lng != gpsDatas[i - 1].LatLng.Lng || gpsDatas[i].Speed  != gpsDatas[i - 1].Speed))
                            {
                                AddGpsDataToOutList(gpsDatas, gpsDatasOut, i);
                            }
                        }
                    }
                    gpsDatas.Clear();
                    resultDict.Add("Status:", true);
                    resultDict.Add("Data:", gpsDatasOut);
                }
            }
            catch (Exception ex)
            {
                resultDict.Add("Status:", false);
                resultDict.Add("Data:", ex.Message);
            }
            return resultDict;
        }
         
        private static void AddGpsDataToOutList(IList<GpsData> gpsDatas, List<object> gpsDatasOut,int index)
        {
            gpsDatasOut.Add(new { Lat = gpsDatas[index].LatLng.Lat, Lng = gpsDatas[index].LatLng.Lng, Speed = gpsDatas[index].Speed, Time = gpsDatas[index].Time.ToString("yyyy-MM-dd HH:mm") });
        }

        private static GpsDataForService ConvertGpsDataForSevice(GpsData gpsData)
        {
                GpsDataForService gpsDataService = new GpsDataForService();
                gpsDataService.Latitude = gpsData.LatLng.Lat;
                gpsDataService.Longtitude = gpsData.LatLng.Lng;
                gpsDataService.Speed = Math.Round(gpsData.Speed,1);
                gpsDataService.LastTimeDataSend = gpsData.Time.ToString();
                return gpsDataService;
        }

        public static Dictionary<string, object> GetHundredPointByDEvicesIdFast(int pointCount, params string[] deviceList)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            OnlineDataProvider online = new OnlineDataProvider();
                IList<GpsData> gpsDatas = online.GetLastData(0);
                foreach (string device in deviceList)
                {
                    int mobitelId = 0;
                    if (Int32.TryParse(device, out   mobitelId))
                    {
                        var gpsDatasServ = gpsDatas.Where(gpsData => gpsData.Mobitel == mobitelId).OrderByDescending(gpsData => gpsData.Id).Select(gpsData => new { Lat = gpsData.LatLng.Lat, Lng = gpsData.LatLng.Lng }).ToList();
                        if (pointCount < gpsDatasServ.Count)
                            gpsDatasServ = gpsDatasServ.GetRange(0, pointCount);
                        result.Add(device, gpsDatasServ);
                    }
                }
                gpsDatas.Clear(); 
            return result;
        }

        public static Dictionary<string, object> GetHundredPointByDEvicesId(int pointCount, params string[] deviceList)
        {
            var result = new Dictionary<string, object>();
            var online = new OnlineDataProvider();
            foreach (string device in deviceList)
            {
                int mobitelId = 0;
                if (Int32.TryParse(device, out   mobitelId))
                {
                    Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelId);
                    var gpsDatasServ = online.GetLastDataMobitelAllProtocols(mobitelId, 0, mobitel.Is64BitPackets,mobitel.IsNotDrawDgps).OrderByDescending(gpsData => gpsData.Id).Select(gpsData => new { Lat = gpsData.LatLng.Lat, Lng = gpsData.LatLng.Lng }).ToList(); ;
                    if (pointCount < gpsDatasServ.Count)
                        gpsDatasServ = gpsDatasServ.GetRange(0, pointCount);
                    result.Add(device, gpsDatasServ);
                }
            }
            return result;
        }

        public static List<object> GetZoneListByUSerID(int UserId)
        {
            ZonesProvider zoneProv = new ZonesProvider();
            List<Zone> zones = zoneProv.GetAll();
            List<object> zonesFiltered = new List<object>();
            if (zones.Count > 0)
            {
                if (UserId > 0)
                {
                    UserBase ub = UserBaseProvider.GetUserBase(UserId);
                    if (ub != null && !ub.Admin)
                    {
                        List<int> unvisibleVehiclesId = UserBaseProvider.GetUnvisibleRoleItems((int)UserAccessObjectsTypes.CheckZones, ub.Role.Id);
                        zones = zones.Where(zone => !unvisibleVehiclesId.Contains(zone.Id)).ToList(); 
                    }
                }
                var zonesForWeb = zones.Select(zoneForWeb =>
                new { ZoneGroupName = zoneForWeb.Group.Name, ZoneId = zoneForWeb.Id, ZoneName = zoneForWeb.Name, ZoneArea = Math.Round(zoneForWeb.AreaGa,3), ZoneColor = ColorTranslator.ToHtml(zoneForWeb.Style.Color) }).ToList();
                zonesForWeb.ForEach(z => zonesFiltered.Add(z));
            }
            return zonesFiltered;
        }

        public static Dictionary<string, object> GetZoneCordsByZoneID(int zoneID)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
             var zonesProv = new ZonesProvider();
            IZone zone = zonesProv.GetZone(zoneID);
            var points = zone.Points.ToList().Select(p => new { Lat = p.Lat, Lng = p.Lng });
            result.Add(zoneID.ToString(), points);
            return result;
        }

        public static List<object> GetSensorsInfoByDeviceID(int deviceID)
        {
            var sensorsObj = new List<object>();
            List<Sensor> sensors = SensorProvider.GetVehicleSensors(deviceID);
            sensors.Select(s => new { SensorName = s.Name, UnitName = s.UnitName,SensorId = s.Id }).ToList().ForEach(so => sensorsObj.Add(so));
            return sensorsObj;
        }

        public static Dictionary<string, string> GetSensorsValuesBySensorsId( params string[] sensors)
        {
            var result = new Dictionary<string, string>();
            var online = new OnlineDataProvider();
            foreach (string sensor in sensors)
            {
                int sensorId = 0;
                if (Int32.TryParse(sensor, out   sensorId))
                {
                    Sensor s = SensorProvider.GetSensor(sensorId);
                    if (s != null)
                    {
                        GpsData gps = online.GetMostRecentDataMobitel(s.MobitelId);
                        string sensorValue = "0";
                        if (gps != null)
                        {
                            if (s.Algoritm == (int)AlgorithmType.FUEL1 || s.Algoritm == (int)AlgorithmType.FUEL2)
                                sensorValue = FuelLevel.GetValueFromSensor(s, gps).ToString();
                            else if (s.Algoritm == (int)AlgorithmType.DRIVER)
                            {

                                sensorValue = s.GetValue(gps.Sensors).ToString();
                                string driverName = "--------";
                                ushort driverRfid;
                                if (ushort.TryParse(s.GetValue(gps.Sensors).ToString(), out driverRfid))
                                {
                                    var driver = new Driver(0, driverRfid);
                                    if (driver.FullName.Length > 0)
                                        driverName = driver.FullName;
                                }

                                sensorValue = string.Format("{0}({1})", driverName, sensorValue);
                            }
                            else
                            {
                                if (gps.Sensors != null) sensorValue = (s.GetValue(gps.Sensors) * s.K).ToString();
                            }
                        }
                        result.Add(sensor, sensorValue);
                    }
                }
            }
            return result;
        }

        public static void SetDriverDb()
        {

            DriverDb.ConnectionString = ConfigurationManager.ConnectionStrings["CS"].ConnectionString;
            string nameDataBase = ConfigurationManager.AppSettings.GetKey(0);

            if (nameDataBase.Equals("MYSQL"))
            {
                DriverDb.TypeDataBaseForUsing = DriverDb.MySqlUse; // switch MySQL Data Base 
            }
            else if (nameDataBase.Equals("MSSQL"))
            {
                DriverDb.TypeDataBaseForUsing = DriverDb.MssqlUse; // switch MSSQL Data Base 
            }
            else
            {
                DriverDb.TypeDataBaseForUsing = -1;
            }
        }
    }
}