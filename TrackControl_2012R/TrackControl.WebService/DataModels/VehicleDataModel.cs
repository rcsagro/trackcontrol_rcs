﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackControl.WebService
{
    public class VehicleDataModel
    {
        public string GroupName { get; set; }
        public int VehicleId { get; set; }
        public int MobitelId { get; set; }
        public bool Checked { get; set; }
        public string GarageNumber { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string LastDataSend { get; set; }
        public string Login { get; set; }
        public double Speed { get; set; }
        public double MinStayValueInMinuts { get; set; }
        public double MaxStayValueInMinuts { get; set; }
    }
}