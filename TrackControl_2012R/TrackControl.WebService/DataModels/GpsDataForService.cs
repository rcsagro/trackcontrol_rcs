﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackControl.WebService
{
    public class GpsDataForService
    {
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public double Speed { get; set; }
        public string LastTimeDataSend { get; set; }
    }
}