﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace TrackControl.WebService
{
    public static class JsonService
    {

        public static void ForceJson(HttpContext context, object res)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = 10485760; 
            string str = js.Serialize(res);
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(str);
            EnableCrossDmainAjaxCall(context);
            //Context.Response.End();
        }

        private static void EnableCrossDmainAjaxCall(HttpContext context)
        {
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (context.Request.HttpMethod == "OPTIONS")
            {
                context.Response.AddHeader("Access-Control-Allow-Methods",
                              "GET, POST, OPTIONS");
                context.Response.AddHeader("Access-Control-Allow-Headers",
                              "Content-Type, Accept");
                context.Response.End();
            }
        }
    }
}