﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using LocalCache; 

namespace TrackControl.WebService
{
    /// <summary>
    /// Получения данных для внешних приложений TrackControl
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
     [System.Web.Script.Services.ScriptService]
    public class DataProvider : System.Web.Services.WebService
    {
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetDeviceListByUserID(int TrackControlUserID) 
        {
            List<VehicleDataModel> vehData =  DataModelsProvider.GetVehicleListByUserId(TrackControlUserID);
            JsonService.ForceJson(Context,vehData);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public  void GetLastDataByParams(string deviceList)
        {
            string[] array = deviceList.Split(',');
            Dictionary<string, object> result = DataModelsProvider.GetLastDataByParams(array);
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetHundredPointByDEvicesId(string deviceList,int pointCount)
        {
            string[] array = deviceList.Split(',');
            Dictionary<string, object> result = DataModelsProvider.GetHundredPointByDEvicesId(pointCount,array);
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetZoneListByUSerID(int TrackControlUserID)
        {
            List<object> zones = DataModelsProvider.GetZoneListByUSerID(TrackControlUserID);
            JsonService.ForceJson(Context, zones);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetZoneCordsByZoneID(int zoneID)
        {
            Dictionary<string, object> result = DataModelsProvider.GetZoneCordsByZoneID(zoneID);
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetSensorsInfoByDeviceID(int deviceID)
        {
            List<object> sensorsObj = DataModelsProvider.GetSensorsInfoByDeviceID(deviceID);
            JsonService.ForceJson(Context, sensorsObj);
        }


        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetSensorsValuesBySensorsId(string sensorsList)
        {
            string[] array = sensorsList.Split(',');
            Dictionary<string, string> result = DataModelsProvider.GetSensorsValuesBySensorsId(array);
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetReportKilometrage(int mobitelId, DateTime start, DateTime end)
        {
            DataModelsProvider.SetDriverDb(); 
            KilometrageWeb reportKilom = new KilometrageWeb(mobitelId, start, end);
            Dictionary<string, object> result = reportKilom.GetData();
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetDataByMobitelBetweenDates(int mobitelId, DateTime start, DateTime end)
        {
            object result = DataModelsProvider.GetDataByMobitelBetweenDates(mobitelId, start, end);
            JsonService.ForceJson(Context, result);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetReportFuel(int mobitelId, DateTime start, DateTime end)
        {
            DataModelsProvider.SetDriverDb(); 
            using (FuelWeb reportFuel = new FuelWeb(mobitelId, start, end))
            {
                Dictionary<string, object> result = reportFuel.GetData();
                JsonService.ForceJson(Context, result);
            }
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetFuelLevelByMobitelBetweenDates(int mobitelId, DateTime start, DateTime end)
        {
            DataModelsProvider.SetDriverDb(); 
            using (FuelWeb reportFuel = new FuelWeb(mobitelId, start, end))
            {
                object result = reportFuel.GetFuelLevelByMobitelBetweenDates();
                JsonService.ForceJson(Context, result);
            }
        }

        [WebMethod]
        public string GetValue()
        {
            return "Test";
        }

    }

   }
