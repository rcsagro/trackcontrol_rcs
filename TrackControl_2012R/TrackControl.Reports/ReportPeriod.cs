using System;
using TrackControl.Reports.Properties;

namespace TrackControl.Reports
{
  public class ReportPeriod
  {
    public event ReportPeriodChanged BeginChanged;
    public event ReportPeriodChanged EndChanged;

    public DateTime Begin
    {
      get { return _begin; }
      set
      {
        _begin = value;
        _name = String.Format(" {0}", Resources.ForPeriod);

        if (_begin > _end) // ���� ������ ��������� ����� ���������
        {
          _end = new DateTime(_begin.Year, _begin.Month, _begin.Day, _end.Hour, _end.Minute, 0);
        }
        if (_begin >= _end) // ����� ������ ��������� �� ������ ���������
        {
          _begin = new DateTime(_begin.Year, _begin.Month, _begin.Day, 0, 0, 0);
          _end = new DateTime(_end.Year, _end.Month, _end.Day, 23, 59, 0);
        }

        onBeginChanged();
      }
    }
    DateTime _begin;

    public DateTime End
    {
      get { return _end; }
      set
      {
        _end = value;
        _name = String.Format(" {0}", Resources.ForPeriod);

        if (_begin > _end) // ���� ��������� ��������� ������ ������
        {
          _begin = new DateTime(_end.Year, _end.Month, _end.Day, _begin.Hour, _begin.Minute, 0);
        }
        if (_begin >= _end) // ����� ��������� ��������� �� ����� ������
        {
          _begin = new DateTime(_begin.Year, _begin.Month, _begin.Day, 0, 0, 0);
          _end = new DateTime(_end.Year, _end.Month, _end.Day, 23, 59, 0);
        }

        onEndChanged();
      }
    }
    DateTime _end;

    public string  Name
    {
      get { return _name; }
    }
    string _name;

    private ReportPeriod(DateTime begin, DateTime end, string name)
    {
      Begin = begin;
      End = end;
      _name = name;
    }

    public ReportPeriod(DateTime begin, DateTime end)
      : this(begin, end, String.Format(" {0}", Resources.ForPeriod))
    {
    }

    public override bool Equals(object obj)
    {
      if (!(obj is ReportPeriod))
      {
        return false;
      }
      ReportPeriod rp = (ReportPeriod)obj;
      return (rp.Begin == Begin && rp.End == End);
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static ReportPeriod Today
    {
      get
      {
        DateTime today = DateTime.Now;
        return new ReportPeriod(
          new DateTime(today.Year, today.Month, today.Day, 0, 0, 0),
          new DateTime(today.Year, today.Month, today.Day, 23, 59, 0),
          String.Format(" {0}", Resources.ForToday));
      }
    }

    public static ReportPeriod Yesterday
    {
      get
      {
        DateTime yesterday = DateTime.Now.AddDays(-1);
        return new ReportPeriod(
          new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 0, 0, 0),
          new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 23, 59, 0),
          String.Format(" {0}  ", Resources.ForYesterday));
      }
    }

    public static ReportPeriod ThisWeek
    {
      get
      {
        DateTime now = DateTime.Now;
        double lag = 0;
        switch (now.DayOfWeek)
        {
          case DayOfWeek.Monday:
            return Today;
          case DayOfWeek.Tuesday:
            lag = -1;
            break;
          case DayOfWeek.Wednesday:
            lag = -2;
            break;
          case DayOfWeek.Thursday:
            lag = -3;
            break;
          case DayOfWeek.Friday:
            lag = -4;
            break;
          case DayOfWeek.Saturday:
            lag = -5;
            break;
          case DayOfWeek.Sunday:
            lag = -6;
            break;
        }
        DateTime begin = now.AddDays(lag);
        return new ReportPeriod(
          new DateTime(begin.Year, begin.Month, begin.Day, 0, 0, 0),
          new DateTime(now.Year, now.Month, now.Day, 23, 59, 0),
          String.Format(" {0}", Resources.ForWeek));
      }
    }

    public static ReportPeriod LastSevenDays
    {
      get
      {
        DateTime end = DateTime.Now;
        DateTime begin = end.AddDays(-6);
        return new ReportPeriod(
          new DateTime(begin.Year, begin.Month, begin.Day, 0, 0, 0),
          new DateTime(end.Year, end.Month, end.Day, 23, 59, 0),
          String.Format(" {0} ", Resources.For7days));
      }
    }

    public static ReportPeriod ThisMonth
    {
      get
      {
        DateTime now = DateTime.Now;
        return new ReportPeriod(
          new DateTime(now.Year, now.Month, 1, 0, 0, 0),
          new DateTime(now.Year, now.Month, now.Day, 23, 59, 0),
          String.Format(" {0}  ", Resources.ForMonth));
      }
    }

    public static ReportPeriod LastThirtyDays
    {
      get
      {
        DateTime end = DateTime.Now;
        DateTime begin = end.AddDays(-29);
        return new ReportPeriod(
          new DateTime(begin.Year, begin.Month, begin.Day, 0, 0, 0),
          new DateTime(end.Year, end.Month, end.Day, 23, 59, 0),
          String.Format(" {0}", Resources.For30days));
      }
    }

    void onBeginChanged()
    {
      ReportPeriodChanged handler = BeginChanged;
      if (null != handler)
        handler();
    }

    void onEndChanged()
    {
      ReportPeriodChanged handler = EndChanged;
      if (null != handler)
        handler();
    }
  }
}
