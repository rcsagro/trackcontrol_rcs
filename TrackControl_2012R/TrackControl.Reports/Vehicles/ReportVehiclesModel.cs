using System;
using System.Collections.Generic;
using TrackControl.Vehicles;

namespace TrackControl.Reports
{
  public class ReportVehiclesModel
  {
    VehiclesModel _model;
    List<ReportVehicle> _vehicles;

    public ReportVehiclesModel(VehiclesModel model)
    {
      if (null == model)
        throw new ArgumentNullException("model");

      _model = model;
      init();
    }

    public void Activate()
    {
      _vehicles.ForEach(delegate(ReportVehicle rv)
      {
        rv.Bind();
      });
    }

    public void ClearTracks()
    {
      _vehicles.ForEach(delegate(ReportVehicle rv)
      {
        rv.Track = null;
      });
    }

    void init()
    {
      IList<Vehicle> vehicles = _model.Vehicles;
      _vehicles = new List<ReportVehicle>(vehicles.Count);
      foreach (IVehicle vehicle in vehicles)
      {
        _vehicles.Add(new ReportVehicle(vehicle));
      }
    }
  }
}
