using System;
using TrackControl.General;
using TrackControl.Vehicles;
using System.Collections.Generic;
using TrackControl.Reports.Properties;

namespace TrackControl.Reports
{
    /// <summary>
    /// ���������� ������� �� ������������� ��������
    /// </summary>
    public class ReportVehicle : IVehicle
    {
        private IVehicle _vehicle;
        private object _tag;
        private Track _track;
        private bool _selected;

        /// <summary>
        /// ����������� ��� ������ TrackControl.Reports.ReportVehicle
        /// </summary>
        /// <param name="vehicle">������������ ��������</param>
        public ReportVehicle(IVehicle vehicle)
        {
            _vehicle = vehicle;
            _sensors = new List<Sensor>();
        }

        #region --   ����� ���������� IVehicle   --

        public int Id
        {
            get { return _vehicle.Id; }
            set { return; }
        }

        public bool IsNew
        {
            get { return _vehicle.IsNew; }
        }

        public Mobitel Mobitel
        {
            get { return _vehicle.Mobitel; }
        }

        public string CarMaker
        {
            get { return _vehicle.CarMaker; }
        }

        public string FuelMotor
        {
            get { return _vehicle.FuelMotor; }
        }

        public string FuelWays
        {
            get { return _vehicle.FuelWays; }
        }

        public string CarModel
        {
            get { return _vehicle.CarModel; }
        }

        public int Identifier
        {
            get { return _vehicle.Identifier; }
        }

        public string RegNumber
        {
            get { return _vehicle.RegNumber; }
        }

        public Driver Driver
        {
            get { return _vehicle.Driver; }
        }

        public VehicleStyle Style
        {
            get { return _vehicle.Style; }
        }

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public int TimeStatus()
        {
            return _vehicle.TimeStatus();
        }

        public VehicleSettings Settings
        {
            get { return _vehicle.Settings; }
        }

        private bool _checked;

        public bool Checked
        {
            get { return _checked; }
            set { _checked = value; }
        }

        #endregion

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (value != _selected)
                    _selected = value;
            }
        }

        public void Bind()
        {
            _vehicle.Tag = this;
        }

        public Track Track
        {
            get { return _track; }
            set { _track = value; }
        }

        /// <summary>
        /// ����� ��������
        /// </summary>
        private List<Sensor> _sensors;

        public List<Sensor> Sensors
        {
            get { return _sensors; }
            set { _sensors = value; }
        }

        /// <summary>
        /// ���������� ������ on-line ��������.������ ���������� - ��������/������ �����
        /// </summary>
        private int _logicSensorState = (int) LogicSensorStates.Absence;

        public int LogicSensorState
        {
            get { return _logicSensorState; }
            set { _logicSensorState = value; }
        }

        private GpsData _lastPoint;

        public GpsData LastPoint
        {
            get
            {
                return _lastPoint;
            }
            set
            {
                _lastPoint = value;
            }
        }

        private VehiclesGroup _group;

        public VehiclesGroup Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public string DriverName
        {
            get
            {
                if (null != _vehicle.Driver)
                    if(_vehicle.Driver.NumTelephone != "")
                        return "(" + _vehicle.Driver.NumTelephone + ")" + _vehicle.Driver.FullName;
                    else
                        return "(" + "tel:--" + ")" + _vehicle.Driver.FullName;
                else
                    return String.Empty;
            }
        }

        public string GetAllFuel
        {
            get
            {
                double allFuel = 0;
                if (_sensors != null)
                {
                    foreach (Sensor s in _sensors)
                    {
                        if (s.Algoritm == (int)AlgorithmType.FUEL1)
                            allFuel += s.Value;
                    }
                }

                return Math.Round(allFuel, 2) + " " + Resources.LiterFuel;
            }
        }

        private string sname = string.Empty;
        private string _numTelefone = string.Empty;

        public string DriverNameStep
        { 
            get
            {
                if (_sensors != null)
                {
                    foreach (Sensor s in _sensors)
                    {
                        if (s.Algoritm == (int)AlgorithmType.DRIVER) // RFID - ��������
                        {
                            if (!s.SensorValue.Contains(Resources.NotPresent))
                            {
                                int length = s.SensorValue.IndexOf('(');
                                sname = s.SensorValue.Substring(0, length);
                                _numTelefone = s.NumTelephone;
                                if (s.NumTelephone != "")
                                    return "(" + _numTelefone + ")" + sname;
                                else
                                    return "(" + "tel:--" + ")" + sname;
                            }
                        }           
                    }
                }

                if (null != _vehicle.Driver)
                {
                    if (_vehicle.Driver.NumTelephone != "")
                    {
                        _numTelefone = _vehicle.Driver.NumTelephone;
                        sname = _vehicle.Driver.FullName;
                        return "(" + _vehicle.Driver.NumTelephone + ")" + _vehicle.Driver.FullName;
                    }
                    else
                    {
                        _numTelefone = "No Data";
                        sname = _vehicle.Driver.FullName;
                        return "(" + "tel:--" + ")" + _vehicle.Driver.FullName;
                    }
                }
                else
                {
                    _numTelefone = "No Data";
                    sname = "No Data";
                    return String.Empty;
                }
            }
        }

        public VehicleCategory Category { get; set; }

        public VehicleCategory2 Category2 { get; set; }

        public VehicleCategory3 Category3 { get; set; }

        public VehicleCategory4 Category4 { get; set; }

        public string VehicleComment { get; set; }

        public string CategoryMixed
        {
            get { return ""; }
        }

        public bool Is64BitPackets
        {
            get {return _vehicle.Is64BitPackets; } 
            set { _vehicle.Is64BitPackets = value; }
        }
   
        public string Info
        {
            get { return _vehicle.Info; }
        }

        public string DriverNameVersus
        {
            get
            {
                return sname;
            }
            set { sname = value; }
        }

        public string DriverTelefone
        {
            get { return _numTelefone; }
            set { _numTelefone = value; }
        }
    }
}
