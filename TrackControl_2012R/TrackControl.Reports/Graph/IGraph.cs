using System;
using System.Drawing;

namespace TrackControl.Reports.Graph
{
    public interface IGraph
    {
        void AddSeriesL(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type);
        void AddSeriesR(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type);
        void AddSeriesL(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type, bool visible);
        void AddSeriesR(string name, Color color, double[] Y, DateTime[] X, AlgorithmType type, bool visible);
        void ShowSeries(string title);
        void AddLabel(DateTime time, double beginY, double endY, string label, Color color);
        void ClearSeries();
        void ClearLabel();
        void ClearRegion();
        void AddTimeRegion(DateTime begin, DateTime end);
        void AddDangerRegion(DateTime begin, DateTime end, Color dangerColor);
        bool GetSelectPoint(out DateTime time, out double value);
        void AddMouseEventHandler();
        void RemoveMouseEventHandler();
        void ClearGraphZoom();
        void SellectZoom();
        void VisibleGraph(bool visible);
        //������������ �������
        event ActionEventHandler Action;
        //add
        void SimulateMenuItemRightCheck(string seriesName);
        int SeriesIsShow(string seriesName);
    }
}
