
namespace TrackControl.Reports.Graph
{
  public delegate void ActionEventHandler(object sender, ActionCancelEventArgs ev);
  public delegate void BtnEventHandler(object sender, bool flag);
}
