using System;

namespace TrackControl.Reports.Graph
{
    public struct SelPoint
    {
        public DateTime time;
        public double value;
        public string Label;
        public int Type;
    }
}
