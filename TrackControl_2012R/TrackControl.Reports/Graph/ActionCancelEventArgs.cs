using System;
using System.ComponentModel;

namespace TrackControl.Reports.Graph
{
  public class ActionCancelEventArgs : CancelEventArgs
  {
    SelPoint point;
    public SelPoint Point
    {
      get { return point; }
      set { point = value; }
    }

    public ActionCancelEventArgs() : base() { }
    public ActionCancelEventArgs(SelPoint point)
      : base()
    {
      this.point = point;
    }
  }
}
