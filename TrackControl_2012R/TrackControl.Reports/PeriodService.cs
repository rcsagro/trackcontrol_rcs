using System;

namespace TrackControl.Reports
{
  public static class PeriodService
  {
    static object _sync = new object();

    public static ReportPeriod Period
    {
      get
      {
        lock (_sync)
        {
          return _period;
        }
      }
      set
      {
        if (null == value)
          throw new Exception("Report Period can not be NULL");

        lock (_sync)
        {
          _period.BeginChanged -= onPeriodChanged;
          _period.EndChanged -= onPeriodChanged;
          _period = value;
          _period.BeginChanged += onPeriodChanged;
          _period.EndChanged += onPeriodChanged;
        }

        onPeriodChanged();
      }
    }
    static ReportPeriod _period;

    public static event ReportPeriodChanged PeriodChanged = delegate { };

    static PeriodService()
    {
      _period = ReportPeriod.Today;
      _period.BeginChanged += onPeriodChanged;
      _period.EndChanged += onPeriodChanged;
    }

    static void onPeriodChanged()
    {
      PeriodChanged();
    }
  }
}
