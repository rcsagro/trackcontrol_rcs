using System;
using System.Xml.Schema;
using TrackControl.Vehicles;

namespace TrackControl.Reports
{
    /// <summary>
    /// ������� ������ ������� ����������� �������.
    /// </summary>
    public static class BasicMethods
    {
        /// <summary>
        /// ����������� ����������� ����, � ��.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetTravel �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <returns>���������� ����, � ��.</returns>
        public static double GetKilometrage(GpsData[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            double result = 0;
            // ������� �������� ����� � ������ �����, �.�. � ���� Dist
            // ��������� ������ ������������ ���������� �����
            for (int i = 1; i < array.Length; i++)
            {
                result += array[i].Dist;
            }
            return result;
        }

        /// <summary>
        /// ����� ������������ �������� �� ������� ��������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetMaxSpeed �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <returns>������������ ��������.</returns>
        public static float GetMaxSpeed(GpsData[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            float result = 0;
            foreach (GpsData d in array)
            {
                if (d.Speed > result)
                {
                    result = (float) d.Speed;
                }
            }

            //if (result > 180.0)
            //{
            //    result = (float)180.0;
            //}

            return result;
        }

        /// <summary>
        /// ����������� ������ ������� � ��������.
        /// ����������� ��� ����� ����� ����������� ����� �����,
        /// ����������� �� ��������� � �������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetTimeMotion �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <returns>����� ����� � ��������, TimeSpan.</returns>
        public static TimeSpan GetTotalMotionTime(GpsData[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            TimeSpan result = new TimeSpan();
            if (array.Length > 0)
            {
                result = (array[array.Length - 1].Time - array[0].Time);
                result = (result - GetTotalStopsTime(array, 0));
            }
            return result;
        }

        /// <summary>
        /// ����������� ������ ������� ������� � ���������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetTimeStops �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <param name="vmin">����������� �������� ��� ���������� ������ ���������.</param>
        /// <returns>����� ����� �������, TimeSpan.</returns>
        public static TimeSpan GetTotalStopsTime(GpsData[] array, int vmin)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            TimeSpan result = new TimeSpan(0);
            if (array.Length > 0)
            {
                DateTime beginTime = array[0].Time;
                bool bstart = false;
                foreach (GpsData d in array)
                {
                    if (!bstart & d.Speed <= vmin)
                    {
                        bstart = true;
                        beginTime = d.Time;
                    }
                    else if (bstart & d.Speed > vmin)
                    {
                        result += (d.Time - beginTime);
                        bstart = false;
                    }
                }
                if (bstart)
                {
                    GpsData row = array[array.Length - 1];
                    result += (row.Time - beginTime);
                }
            }
            return result;
        }

        /// <summary>
        /// ����� ������������ ��������� ������� � ������ ������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetMinTime �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <returns>����������� �������� ����� � ������ �������� ������.
        /// � ������ ���������� ������ - DateTime.MinValue.</returns>
        public static DateTime GetMinTime(GpsData[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            DateTime result = DateTime.MaxValue;
            foreach (GpsData d in array)
            {
                if (d.Valid && (d.Time < result))
                {
                    result = d.Time;
                }
            }
            return result == DateTime.MaxValue ? result = DateTime.MinValue : result;
        }

        /// <summary>
        /// ����� ������������� ��������� ������� � ������ ������.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetMaxTime �� ������� ������ �������� DataGps.</exception>
        /// <param name="array">������ DataGps.</param>
        /// <returns>������������ �������� ����� � ������ �������� ������.
        /// � ������ ���������� ������ - DateTime.MinValue.</returns>
        public static DateTime GetMaxTime(GpsData[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            DateTime result = DateTime.MinValue;
            foreach (GpsData d in array)
            {
                if (d.Valid && (d.Time > result))
                {
                    result = d.Time;
                }
            }
            return result;
        }
    }
}
