using System;
using TrackControl.General;

namespace TrackControl.Reports
{
  public class ReportZone
  {
    IZone _zone;
    bool _visible;

    public ReportZone(IZone zone)
    {
      if (null == zone)
        throw new ArgumentNullException("zone");

      _zone = zone;
    }

    public IZone Zone
    {
      get { return _zone; }
    }

    public bool Visible
    {
      get { return _visible; }
      set
      {
        if (value != _visible)
          _visible = value;
      }
    }

    public void Bind()
    {
      _zone.Tag = this;
    }

    public void UnBind()
    {
      _zone.Tag = null;
    }
  }
}
