using System;
using System.Collections.Generic;

using TrackControl.General;
using TrackControl.Zones;

namespace TrackControl.Reports
{
  public class ReportZonesModel : ITreeModel<IZone>
  {
    IZonesManager _manager;
    List<ReportZone> _zones;

    public event VoidHandler VisibilityChanged;

    public ReportZonesModel(IZonesManager manager)
    {
      _manager = manager;
      _manager.ZonesAdded += manager_ZonesAdded;
      _manager.ZonesRemoved += manager_ZonesRemoved;
      IList<IZone> zones = _manager.Zones;
      _zones = new List<ReportZone>(zones.Count);
      foreach (IZone zone in zones)
        _zones.Add(new ReportZone(zone));
    }

    public IZone Current
    {
      get { return null; }
    }

    public IList<IZone> Checked
    {
      get
      {
        List<IZone> zones = new List<IZone>();
        _zones.ForEach(delegate(ReportZone rz)
        {
          if (rz.Visible)
            zones.Add(rz.Zone);
        });
        return zones;
      }
    }

    public IList<IZone> GetAll()
    {
      return new List<IZone>(_manager.Root.AllItems);
    }

    public int ZonesCount
    {
      get { return _zones.Count; }
    }

    public int CheckedZonesCount
    {
      get { return Checked.Count; }
    }

    public void Activate()
    {
      _zones.ForEach(delegate(ReportZone rz)
      {
        rz.Bind();
      });
    }

    public void DeActivate()
    {
      _zones.ForEach(delegate(ReportZone rz)
      {
        rz.UnBind();
      });
    }

    public void ChangeVisibility(IList<IZone> zones, bool visibility)
    {
      if (zones.Count > 0)
      {
        foreach (IZone zone in zones)
        {
            if (zone.Tag == null) return;
            ReportZone rz = (ReportZone)zone.Tag;
          rz.Visible = visibility;
        }
        onVisibilityChanged();
      }
    }

    public IZone GetById(int id)
    {
      return _manager.GetById(id);
    }

    void onVisibilityChanged()
    {
      VoidHandler handler = VisibilityChanged;
      if (null != handler)
        handler();
    }

    void manager_ZonesAdded(IEnumerable<IZone> zones)
    {
      if (null == zones)
        throw new ArgumentNullException("zones");

      foreach (IZone zone in zones)
        _zones.Add(new ReportZone(zone));
    }

    void manager_ZonesRemoved(IEnumerable<IZone> zones)
    {
      if (null == zones)
        throw new ArgumentNullException("zones");

      foreach (IZone zone in zones)
      {
        foreach (ReportZone rz in _zones)
        {
          if (rz.Zone == zone)
          {
            _zones.Remove(rz);
            break;
          }
        }
      }
    }
  }
}
