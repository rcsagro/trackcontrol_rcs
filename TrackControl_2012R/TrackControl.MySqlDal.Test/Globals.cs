using System;

namespace TrackControl.MySqlDal.Test
{
  public static class Globals
  {
    public const string ACTUAL_CONNECTION_STRING = "server=srvavs;user id=user;database=mca_dispatcher_test";
    public const string FAULTY_CONNECTION_STRING = "server=srvavs;user=bobik;database=bobik_forever";

    static DbCommon _dbCommon;
    public static DbCommon ActualDbCommon
    {
      get
      {
        if (null == _dbCommon)
          _dbCommon = new DbCommon(Globals.ACTUAL_CONNECTION_STRING);

        return _dbCommon;
      }
    }
  }
}
