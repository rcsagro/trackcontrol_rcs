using System;
using NUnit.Framework;
using TrackControl.Vehicles;

namespace TrackControl.MySqlDal.Test
{
  [TestFixture]
  public class VehiclesGroupProviderTest
  {
    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_NullReferenceExcepton_If_DbCommon_Is_Null()
    {
      IVehiclesGroupProvider provider = new VehiclesGroupProvider(null);
    }

  }
}
