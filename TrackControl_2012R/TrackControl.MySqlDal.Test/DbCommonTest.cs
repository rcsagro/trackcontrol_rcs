using System;
using NUnit.Framework;

namespace TrackControl.MySqlDal.Test
{
  [TestFixture]
  public class DbCommonTest
  {
    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void DbCommonCtor_Should_Throw_An_Exception_When_ConnectionString_Is_Null()
    {
      DbCommon db = new DbCommon(null);
    }

    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void DbCommonCtor_Should_Throw_An_Exception_When_ConnectionString_Is_Empty()
    {
      DbCommon db = new DbCommon(String.Empty);
    }

    [Test]
    public void CheckConnection_Should_Return_True_For_Correct_ConnectionString()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.IsTrue(db.CheckConnection());
    }

    [Test]
    public void CheckConnection_Should_Return_False_For_Invalid_ConnectionString()
    {
      DbCommon db = new DbCommon(Globals.FAULTY_CONNECTION_STRING);
      Assert.IsFalse(db.CheckConnection());
    }

    [Test]
    public void ServerName_Should_Return_Correct_ServerName()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.AreEqual("srvavs", db.ServerName);
    }

    [Test]
    public void DbName_Should_Return_Correct_DatabaseName()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.AreEqual("mca_dispatcher_test", db.DbName);
    }

    [Test]
    public void IsTableExist_Should_Return_True_For_Existing_Table()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.IsTrue(db.IsTableExist("datagps"));
    }

    [Test]
    public void IsTableExist_Should_Return_False_For_Absent_Table()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.IsFalse(db.IsTableExist("this_is_not_exist"));
    }

    [Test]
    public void IsColumnExist_Should_Return_True_For_Existing_Column()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.IsTrue(db.IsColumnExist("datagps", "UnixTime"));
    }

    [Test]
    public void IsColumnExist_Should_Return_False_For_Absent_Column()
    {
      DbCommon db = Globals.ActualDbCommon;
      Assert.IsFalse(db.IsColumnExist("datagps", "datagps_UnixTime"));
    }
  }
}
