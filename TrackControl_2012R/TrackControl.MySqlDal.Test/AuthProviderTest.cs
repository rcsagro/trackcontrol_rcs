using System;
using MySql.Data.MySqlClient;
using TrackControl.General;
using NUnit.Framework;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.MySqlDal.Test
{
  [TestFixture]
  public class AuthProviderTest
  {
    const string DROP_USER_TABLE = "DROP TABLE IF EXISTS `Users`;";
    const string CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS `users` (`Login` varchar(50) NOT NULL, `Pass` varchar(50) NOT NULL, PRIMARY KEY(`Login`)) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='������������ � ������';";
    const string INSERT_USER = "INSERT INTO `Users` VALUES (@Login, @Pass);";
    const string USER_ROW_LOGIN = "UserName";
    const string USER_ROW_PASS = "UserPass";

    #region --   Helper Methods   --
    
    static void dropUsersTableIfExists(DbCommon dbCommon)
    {
      if (dbCommon.IsTableExist("Users"))
      {
      //  using (MySqlCommand command = new MySqlCommand())
          DriverDb db = new DriverDb();
          db.NewSqlCommand();
        {
          //command.CommandText = DROP_USER_TABLE;
            db.CommandText( DROP_USER_TABLE );
          //using (MySqlConnection conn = dbCommon.GetConnection())
            db.SqlConnection = dbCommon.GetConnection();
          {
            //command.Connection = conn;
              db.CommandConnection(db.SqlConnection);
            db.SqlConnectionOpen(); //conn.Open();
            //command.ExecuteNonQuery();
              db.CommandExecuteNonQuery();
          }
          db.SqlConnectionClose();
        }
      }
    }

    static void reCreateUsersTable(DbCommon dbCommon)
    {
      dropUsersTableIfExists(dbCommon);
      //using (MySqlCommand command = new MySqlCommand())
      {
          DriverDb db = new DriverDb();
          db.NewSqlCommand();
        //command.CommandText = CREATE_USER_TABLE;
          db.CommandText( CREATE_USER_TABLE );
        //using (MySqlConnection conn = dbCommon.GetConnection())
          db.SqlConnection = dbCommon.GetConnection();
        {
          //command.Connection = conn;
            db.CommandConnection(db.SqlConnection);
          db.SqlConnectionOpen(); //conn.Open();
          //command.ExecuteNonQuery();
            db.CommandExecuteNonQuery();
        }
        db.SqlConnectionClose();
      }
    }

    static void reCreateUsersTableWithOneEmptyLoginRow(DbCommon dbCommon)
    {
      reCreateUsersTable(dbCommon);
      //using (MySqlCommand command = new MySqlCommand())
      {
          DriverDb db = new DriverDb();
          db.NewSqlCommand();
        //command.CommandText = INSERT_USER;
          db.CommandText( INSERT_USER );
        //command.Parameters.Add("@Login", MySqlDbType.VarChar).Value = String.Empty;
          db.CommandParametersAdd("@Login", db.GettingVarChar(), String.Empty);
        //command.Parameters.Add("@Pass", MySqlDbType.VarChar).Value = USER_ROW_PASS;
          db.CommandParametersAdd("@Pass", db.GettingVarChar(), USER_ROW_PASS);

        //using (MySqlConnection conn = dbCommon.GetConnection())
          db.SqlConnection = dbCommon.GetConnection();
        {
          //command.Connection = conn;
            db.CommandConnection(db.SqlConnection);
          //conn.Open();
            db.SqlConnectionOpen();
          //command.ExecuteNonQuery();
            db.CommandExecuteNonQuery();
        }
        db.SqlConnectionClose();
      }
    }

    static void reCreateUsersTableWithOneRow(DbCommon dbCommon)
    {
      reCreateUsersTable(dbCommon);
      //using (MySqlCommand command = new MySqlCommand())
        DriverDb db = new DriverDb();
        db.NewSqlCommand();
      {
        //command.CommandText = INSERT_USER;
          db.CommandText( INSERT_USER );
        //command.Parameters.Add("@Login", MySqlDbType.VarChar).Value = USER_ROW_LOGIN;
          db.CommandParametersAdd("@Login", db.GettingVarChar(), USER_ROW_LOGIN);
        //command.Parameters.Add("@Pass", MySqlDbType.VarChar).Value = USER_ROW_PASS;
          db.CommandParametersAdd("@Pass", db.GettingVarChar(), USER_ROW_PASS);

        //using (MySqlConnection conn = dbCommon.GetConnection())
          db.SqlConnection = dbCommon.GetConnection();
        {
          //command.Connection = conn;
            db.CommandConnection(db.SqlConnection);
          db.SqlConnectionOpen(); //conn.Open();
          //command.ExecuteNonQuery();
            db.CommandExecuteNonQuery();
        }
        db.SqlConnectionClose();
      }
    }
    #endregion

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void AuthProvider_Should_Throw_NullReferenceExcepton_If_DbCommon_Is_Null()
    {
      IAuthProvider provider = new AuthProvider(null);
    }

    [Test]
    public void IsAuthorizationNeeded_Should_Return_False_If_Table_Users_Is_Not_Exist()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;

      dropUsersTableIfExists(dbCommon);

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsFalse(provider.IsAuthorizationNeeded());
    }

    [Test]
    public void IsAuthorizationNeeded_Should_Return_False_If_Table_Users_Is_Empty()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;

      reCreateUsersTable(dbCommon);
      if (!dbCommon.IsTableExist("Users"))
        throw new Exception("������� Users �� ���� �������");

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsFalse(provider.IsAuthorizationNeeded());
    }

    [Test]
    public void IsAuthorizationNeeded_Should_Return_False_If_FirstRowLogin_Is_Empty()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;
      reCreateUsersTableWithOneEmptyLoginRow(dbCommon);

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsFalse(provider.IsAuthorizationNeeded());
    }

    [Test]
    public void IsAuthorizationNeeded_Should_Return_True_If_FirstRowLogin_IsNot_Empty()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;
      reCreateUsersTableWithOneRow(dbCommon);

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsTrue(provider.IsAuthorizationNeeded());
    }

    [Test]
    public void IsValidLogin_Should_Return_False_For_Incorect_AuthData()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;
      reCreateUsersTableWithOneRow(dbCommon);

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsFalse(provider.IsValidLogin("BadName", "WrongPass"));
    }

    [Test]
    public void IsValidLogin_Should_Return_True_For_Corect_AuthData()
    {
      DbCommon dbCommon = Globals.ActualDbCommon;
      reCreateUsersTableWithOneRow(dbCommon);

      IAuthProvider provider = new AuthProvider(dbCommon);
      Assert.IsTrue(provider.IsValidLogin(USER_ROW_LOGIN, USER_ROW_PASS));
    }
  }
}
