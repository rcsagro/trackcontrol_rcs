﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraEditors;
using TrackControl.Notification;
using TrackControl.Online;
using TrackControl.Vehicles;

namespace MotorDepot.MonitoringGps
{
    public class OrderObserver
    {
        private order _order;
        private Vehicle _modelVh;
        private OnlineVehicle _modelVhOnline;
        private bool _isStarted;
        private MotorDepotEntities _сontextMotorDepot;
        private List<OrderRecordPointChecker> _recordCheckers;
        private OnlineController _onlineController;
        private int _indexChecker;
        public event Action<order> UpdateOrder;
        public OrderObserver(order order)
        {

            _сontextMotorDepot = OnlineController.Instance.ContextMotorDepotOnline;
            if (_сontextMotorDepot == null) return;
            _order = _сontextMotorDepot.orderSet.Include("md_waybill").Where(od => od.Id == order.Id).FirstOrDefault();
            _сontextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, _order);
            if (_order == null || _order.IsClose) return;
            if (_order.md_waybill != null && _order.md_waybill.vehicle != null)
            {
                _modelVh = EntityFrameworkModelConverter.ConvertVehicleEfToModel(_order.md_waybill.vehicle);
                _recordCheckers = new List<OrderRecordPointChecker>();
            }
            else
            {
                XtraMessageBox.Show(String.Format("Заказ - наряд № {0} не имеет ссылки на путевой лист!", _order.Number), "Слежение невозможно!");
            }
        }

        public void Start()
        {
            if (_isStarted || _modelVh == null || _order.DateStartWork == null) return;
            _isStarted = true;
            _recordCheckers.Clear();
            _onlineController = OnlineController.Instance;
            _modelVhOnline = new OnlineVehicle(_modelVh);
            _modelVhOnline.OrderState = _order.md_waybill.vehicle.vehicle_state.Id;
            _modelVhOnline.OrderNumber = _order.Number;
            if (CreateRecordPointCheckersList())
            {
                if (!_onlineController.IsVehicleInCollaction(_modelVhOnline)) 
                _onlineController.OnlineVehicles.Add(_modelVhOnline);
                _indexChecker = 0;
                AddRecordCheckerToController();
            }
            else
            {
                XtraMessageBox.Show(String.Format("Отсутствие записей для слежения в заказ-наряде № {0}!", _order.Number), "Контроль заказа невозможен!");
            }
        }

        public void StartFact()
        {
            if (_isStarted || _modelVh == null || _order.DateStartWork == null) return;
            _isStarted = true;
            _recordCheckers.Clear();
            if (CreateRecordPointCheckersList())
            {
                _indexChecker = 0;
                AddRecordCheckerToController();
            }
            else
            {
                XtraMessageBox.Show(String.Format("Отсутствие записей для слежения в заказ-наряде № {0}!", _order.Number), "Контроль заказа невозможен!");
            }
        }

        public void Stop()
        {
            _isStarted = false;
        }

        void NextStep()
        {
            if (!_isStarted) return;
            _recordCheckers[_indexChecker].NextStep -= NextStep;
            if (_indexChecker == _recordCheckers.Count - 1)
            {
                CloseOrderRecord();
                if (_recordCheckers[_indexChecker].ZoneEventType == ZoneEventTypes.ExitFromZone) CloseOrder();
            }
            else
            {
                if (_recordCheckers[_indexChecker].ZoneEventType == ZoneEventTypes.ExitFromZone)
                {
                    CloseOrderRecord();
                }
                _indexChecker += 1;
                 if (_recordCheckers[_indexChecker - 1].LastGpsData != null)
                {
                    _recordCheckers[_indexChecker].PrevGpsData = _recordCheckers[_indexChecker - 1].LastGpsData;
                }
                AddRecordCheckerToController();
            }
        }

        bool CreateRecordPointCheckersList()
        {
            if (_сontextMotorDepot.ordertSet.Where(ordT => ordT.md_order.Id == _order.Id).Count() == 0) return false;
            var orderRecords = _сontextMotorDepot.ordertSet.Where(ordT => ordT.md_order.Id == _order.Id).ToList();
            bool first = true;
            DateTime? exitDatePrevRecord = _order.DateStartWork;
            double timeDelayHours = _order.TimeDelay == null ? 0 : _order.TimeDelay.Value.TotalHours;
            foreach (ordert orderRecord in orderRecords)
            {
                if (orderRecord.md_object.zones != null)
                {
                    if (orderRecord.IsClose)
                    {
                        exitDatePrevRecord = orderRecord.TimeExit;
                        timeDelayHours = 0;
                    }
                    else
                    {
                        if (first)
                        {
                            first = false;
                            CreatePointChecker(orderRecord, ZoneEventTypes.EntryToZone,
                               timeDelayHours + (double)orderRecord.TimeDelayInPath, exitDatePrevRecord);
                        }
                        else
                        {
                            CreatePointChecker(orderRecord, ZoneEventTypes.EntryToZone, (double)orderRecord.TimeDelayInPath, null);
                        }
                        CreatePointChecker(orderRecord, ZoneEventTypes.ExitFromZone, (double)orderRecord.TimeDelay, null);
                    }
                }

            }
            if (_recordCheckers.Count==0)
                return false;
            else
            return true;
        }

        private void CreatePointChecker( ordert orderRecord, ZoneEventTypes zoneEventType, double delayHours, DateTime? startTime)
        {
            OrderRecordPointChecker pointCheck = new OrderRecordPointChecker(orderRecord, zoneEventType, _modelVhOnline, delayHours, startTime,true);
            _recordCheckers.Add(pointCheck);
        }

        void CloseOrder()
        {
            _order.md_order_state = _сontextMotorDepot.order_stateSet.Where(st => st.Id == (int)OrderStates.Finish).FirstOrDefault();
            OrderProvider.SetOrderState(_order, OrderStates.Finish);
            if (UpdateOrder != null) UpdateOrder(_order);
        }

        void CloseOrderRecord()
        {
            _recordCheckers[_indexChecker].OrderRecord.IsClose = true;
            OrderTProvider.CloseRecord(_recordCheckers[_indexChecker].OrderRecord);
        }

        void AddRecordCheckerToController()
        {
            _recordCheckers[_indexChecker].NextStep += NextStep;
            if (!_onlineController.RecordCheckers.Contains(_recordCheckers[_indexChecker])) _onlineController.RecordCheckers.Add(_recordCheckers[_indexChecker]);

        }


    }
}
