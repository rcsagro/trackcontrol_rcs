﻿using MotorDepot.AccessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.Online;
using TrackControl.Zones;

namespace MotorDepot.MonitoringGps
{
    public class MapDrawController : IDisposable
    {
        public static event VoidHandler UpdateLog;
        
        MdVehicles _vehList;
        GoogleMapControl _googleMap;
        bool _isStarted;
        OnlineController _onlineController;
        MainMD _view;
        private List<IZone> _objectsZones;

        #region Сообщения о событиях
        private readonly TimeSpan _tsMoveTimeForWriteToLog = new TimeSpan(0, 5, 0); 
        private const string LogMesMoveWithoutOrder = "Движение без заказа";
        #endregion

        public MapDrawController(MdVehicles vehList, GoogleMapControl googleMap,MainMD view)
         {
             _vehList = vehList;
             _googleMap = googleMap;
             _view = view;
             _view.StartOnline += Start;
             _view.StopOnline += Stop;
             _view.SelecteVehicleOnMap += UpdateMapInvoke;
             _objectsZones = new List<IZone>();
         }

         public void Start(int tail)
         {
             if (_isStarted)
                 return;
             _isStarted = true;
             if (_onlineController == null)
             {
                 _onlineController = OnlineController.Instance;
                 _onlineController.Tail  = tail;
                 _onlineController.DrawVehicles += UpdateMapInvoke;
             }
             _onlineController.Clear();
             _onlineController.CreateContext();
             _onlineController.OnlineVehicles = ConvertEfVehiclesToOnlineVehicles();
             SetObjectsZones();
             _googleMap.ClearZones(); 
             _googleMap.AddZones(_objectsZones);
             AddOpenedOrdersToOnlineController();
             _onlineController.Start();

         }

         public void Stop()
         {
             _isStarted = false;
             _onlineController.Stop();
         }

         List<OnlineVehicle> ConvertEfVehiclesToOnlineVehicles()
         {
             List<OnlineVehicle> ovs = new List<OnlineVehicle>();
             //foreach (vehicle vh in _vehList.Checked)
             foreach (vehicle vh in _vehList.GetAll)
             {
                 OnlineController.Instance.ContextMotorDepotOnline.Refresh(System.Data.Objects.RefreshMode.StoreWins, vh);
                 OnlineVehicle ov = EntityFrameworkModelConverter.ConvertVehicleEfToModelOnlineVehicle(vh);
                 if (ov != null) ovs.Add(ov);

             }
             return ovs;
         }

         /// <summary>
         /// Обновляет карту
         /// </summary>
         public void UpdateMapInvoke()
         {
             if (!_googleMap.IsDisposed)
             {
                 if (_googleMap.InvokeRequired)
                 {
                     MethodInvoker m = delegate()
                     {
                         UpdateMap();
                     };
                     if (_view != null && _googleMap != null) _view.Invoke(m);
                 }
                 else
                 {
                     UpdateMap();
                 }
             }
         }

         void UpdateMap()
         {
             if (_onlineController == null || _onlineController.OnlineVehicles == null) return;
             List<Marker> markers = new List<Marker>();
             List<Track> tracks = new List<Track>();
             Marker topMaprker = null;
             foreach (OnlineVehicle ov in _onlineController.OnlineVehicles)
             {
                 if (null != ov.Track)
                 {
                     tracks.Add(ov.Track);

                     EntityFrameworkModelConverter.SetVehicleOrderStateToOnlineVehicle(OnlineController.Instance.ContextMotorDepotOnline, ov);
                     MarkerType t = GetMarkerType(ov);
                     Marker m = new Marker(t, ov.Mobitel.Id, ov.Track.LastPoint.LatLng, ov.RegNumber, String.Format("<size=+4>{0} {1} {2}</size>", ov.RegNumber, ov.CarModel, ov.CarMaker));
                     if (ov.IsSelected)
                     {
                         m.IsActive = true;
                         topMaprker = m;
                     }
                     else
                     {
                         markers.Add(m);

                     }
                     SetToolTipDetaled(ov, m);
                 }
             }
             if (null != topMaprker)
             {
                 markers.Add(topMaprker);
             }
             Application.DoEvents();
             _googleMap.ClearTracks();
             _googleMap.ClearMarkers();
             _googleMap.AddTracks(tracks);
             _googleMap.AddMarkers(markers);
             _googleMap.Repaint();
             _vehList.RefreshVehicleStates(); 

             if (_view.IsAutoFit)
             {
                 _googleMap.ZoomAndCenterAll();
             }
             else
             {
                 if (null != topMaprker)
                 {
                     _googleMap.PanTo(topMaprker.Point);
                 }
             }
         }

         private MarkerType GetMarkerType(OnlineVehicle ov)
         {
             MarkerType t;
             switch (ov.OrderState)
             {
                 case ((int)VehicleStates.OrderExecuting):
                     {
                         if (ov.Speed.HasValue && ov.Speed.Value > 0)
                         {
                             t = MarkerType.MdExecGo;
                         }
                         else
                         {
                             t = MarkerType.MdExecStop;
                         }
                         break;
                     }
                 case ((int)VehicleStates.OrderWaitng):
                     {
                         if (ov.Speed.HasValue && ov.Speed.Value > 0)
                         {
                             t = MarkerType.MdWaitGo;
                             SetLogMoveWithoutOrder(ov);
                         }
                         else
                         {
                             t = MarkerType.MdWaitStop;
                         }
                         break;
                     }
                 case ((int)VehicleStates.Repair):
                     {
                         if (ov.Speed.HasValue && ov.Speed.Value > 0)
                         {
                             t = MarkerType.MdRepareGo;
                         }
                         else
                         {
                             t = MarkerType.MdRepareStop;
                         }
                         break;
                     }
                 case ((int)VehicleStates.Lunch):
                     {
                         if (ov.Speed.HasValue && ov.Speed.Value > 0)
                         {
                             t = MarkerType.MdDinnerGo;
                         }
                         else
                         {
                             t = MarkerType.MdDinnerStop;
                         }
                         break;
                     }
                 default:
                     {
                         if (ov.Speed.HasValue && ov.Speed.Value > 0)
                         {
                             t = MarkerType.Moving;
                         }
                         else
                         {
                             t = MarkerType.Parking;
                         }
                         break;
                     }
             }
             return t;
         }

         private void SetLogMoveWithoutOrder(OnlineVehicle ov)
         {
             TimeSpan? timeMoving = ov.TimeMoving();
             if (ov.Tag == null && timeMoving != null && timeMoving >= _tsMoveTimeForWriteToLog)
             {
                 SetLogRecord(ov, LogMesMoveWithoutOrder);
                 ov.Tag = "LogWrited";
             }
         }

         private void SetToolTipDetaled(OnlineVehicle ov, Marker m)
         {
             string orderState = "Ожидание заказа";
             switch (ov.OrderState )
             {
                 case ((int)VehicleStates.OrderExecuting):
                     {
                         orderState = string.Format("Заказ № {0}", ov.OrderNumber);
                         break;
                     }
             }

             m.DescriptoinDetailed = string.Format(@"<b>Статус: </b> {4}{0}<b>Состояние: </b> {3}{0}<b>Cкорость: </b> {1} Км/час{0}<b>Последние данные: </b>{2}{0}{0}",
             Environment.NewLine
             ,ov.Speed
             , ov.LastTime
             , ov.StateInfo
             , orderState
             );

         }

        void SetObjectsZones()
        {
                _objectsZones.Clear();
                var objectsZones = (from mdo in OnlineController.Instance.ContextMotorDepotOnline.md_objectSet select mdo.zones).ToList();
                if (objectsZones != null)
                {
                    foreach (zones objectZone in objectsZones)
                    {
                        if (objectZone != null)
                        {
                            IZone modelZone = EntityFrameworkModelConverter.ConvertZoneEfToModel(objectZone);
                            if (modelZone != null) _objectsZones.Add((Zone)modelZone);
                        }
                    }
                }

        }

        void AddOpenedOrdersToOnlineController()
        {
            var orders = OnlineController.Instance.ContextMotorDepotOnline.orderSet.Where(ord => ord.IsClose == false
                && ord.md_waybill.IsClose == false
                && ord.md_waybill.vehicle != null && ord.md_order_state.Id != (int)OrderStates.Delete);
            if (!UserBaseCurrent.Instance.Admin)
                orders = orders.Where(ord => ord.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
            foreach (order orderItem in orders.ToList())
            {
                //OnlineController.Instance.ContextMotorDepotOnline.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderItem);
                OrderController.StartOrderTracking(orderItem);
            }
        }

        void SetLogRecord(OnlineVehicle ov, string parMessage)
        {
            if (LogController.SetLogRecord(ov, parMessage))
            {
                if (UpdateLog != null) UpdateLog();
            }
        }


         #region IDisposable Members

         public void Dispose()
         {
             if (_onlineController != null)
             {
                 _onlineController.Stop();
                 _onlineController.DrawVehicles -= UpdateMapInvoke;
             }
             if (_view!=null)
             {
                 _view.StartOnline -= Start;
                 _view.StopOnline -= Stop; 
             }
         }

         #endregion
    }
}
