﻿using System;
using System.Linq;
using TrackControl.General;
using TrackControl.Online;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace MotorDepot.MonitoringGps
{
    public static class EntityFrameworkModelConverter
    {
        public static IZonesManager ZonesModel;
        public static VehiclesModel VehiclesModel;

        public static Vehicle ConvertVehicleEfToModel(vehicle vhEf)
        {
            foreach (Vehicle modelVehicle in VehiclesModel.Vehicles)
            {
                if (modelVehicle.Id == vhEf.id)
                {
                    return modelVehicle;
                }

            }
            return null;
        }

        public static OnlineVehicle ConvertVehicleEfToModelOnlineVehicle(vehicle vhEf)
        {
            foreach (Vehicle modelVehicle in VehiclesModel.Vehicles)
            {
                if (modelVehicle.Id == vhEf.id)
                {
                    OnlineVehicle ov = new OnlineVehicle(modelVehicle);
                    if (vhEf.vehicle_state != null)
                    {
                        ov.OrderState = vhEf.vehicle_state.Id;
                        if (ov.OrderState == (int)VehicleStates.OrderExecuting)
                        {
                            ov.OrderNumber = GetVehicleOrderNumber(OnlineController.Instance.ContextMotorDepotOnline, vhEf);
                        }
                    }
                    return ov;
                }

            }
            return null;
        }

        public static IZone ConvertZoneEfToModel(zones zone)
        {
            return ZonesModel.GetZoneById((int)zone.Zone_ID);

        }

        public static void SetVehicleOrderStateToOnlineVehicle(MotorDepotEntities contextMotorDepot, OnlineVehicle ov)
        {
            if (ov == null || contextMotorDepot == null) return;
            var efVehicles = contextMotorDepot.vehicleSet.ToList();
            if (efVehicles == null) return;
            foreach (vehicle efVehicle in efVehicles)
            {
                if (efVehicle != null && efVehicle.id == ov.Id && efVehicle.vehicle_state != null)
                {
                    contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, efVehicle);
                    if (efVehicle.vehicle_state.Id != (int)VehicleStates.OrderExecuting)
                    {
                        DateTime dateForTestState = DateTime.Now;// new DateTime(2013, 03, 01, 14, 30, 00); OrderTestGpsEvents.OnlineTime  ;// 
                        int idState = 0;
                        if (WayBillController.IsVehicleInState(contextMotorDepot,efVehicle, dateForTestState, out idState))
                        {
                            efVehicle.vehicle_state = GetVehicleState(contextMotorDepot,idState);
                        }
                        else
                        {
                            efVehicle.vehicle_state = GetVehicleState(contextMotorDepot,(int)VehicleStates.OrderWaitng);
                        }
                        VehicleProvider.SaveVehicleState(efVehicle); 
                    }
                     ov.OrderState = efVehicle.vehicle_state.Id;
                    if (ov.OrderState == (int)VehicleStates.OrderExecuting)
                    {
                        ov.OrderNumber = GetVehicleOrderNumber(contextMotorDepot,efVehicle);
                    }
                    
                    return;
                }
            }
        }

        private static vehicle_state GetVehicleState(MotorDepotEntities contextMotorDepot,int idState)
        {
            var vehState = contextMotorDepot.vehicle_stateSet.Where(vs => vs.Id == idState).FirstOrDefault();
            return vehState;
        }

        public static waybill GetWayBillOnlineVehicle(OnlineVehicle ov)
        {
            var efVehicles = MDController.Instance.ContextMotorDepot.vehicleSet.ToList();
            foreach (vehicle efVehicle in efVehicles)
            {
                if (efVehicle.id == ov.Id)
                {
                    return efVehicle.md_waybill.Where(wb => wb.vehicle.id == efVehicle.id).FirstOrDefault();
                }
            }
            return null;
        }

        public static users_list ConvertUserModelToEf(UserBase ub)
        {
            var efUsers = MDController.Instance.ContextMotorDepot.users_listSet.ToList();
            foreach (users_list user in efUsers)
            {
                if (ub.Id == user.Id)
                {
                    return user;
                }
            }
            return null;
        }

        static string GetVehicleOrderNumber(MotorDepotEntities contextMotorDepot,vehicle vh)
        {
            //var orderCurrent = contextMotorDepot.orderSet.Where(od => od.md_waybill.vehicle.id == vh.id 
            //& od.md_waybill.IsClose== false &   od.IsClose == false).FirstOrDefault();
            //if (orderCurrent != null) return orderCurrent.Number;
            //return "";
            return OrderProvider.GetVehicleOrderNumber(vh.id);
        }
    }
}
