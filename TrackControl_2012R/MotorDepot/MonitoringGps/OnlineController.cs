﻿using System.Collections.Generic;
using System.Threading;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Online;
using TrackControl.Vehicles;
using System.Windows.Forms;  


namespace MotorDepot.MonitoringGps
{
    public class OnlineController : Singleton <OnlineController>
    {
        IOnlineDataProvider _provider;
        System.Threading.Timer _timer;
        bool _isStarted;
        int _tail;
        private List<OnlineVehicle> _onlineVehicles;
        private List<OnlineVehicle> _trackOnlineVehicles;
        private List<OrderRecordPointChecker> _recordCheckers;
        private List<OrderRecordPointChecker> _trackRecordCheckers;
        private OrderTestGpsEvents _dataCreator;

        public List<OnlineVehicle> OnlineVehicles
        {
            get { return _onlineVehicles; }
            set { _onlineVehicles = value; }
        }

        public List<OrderRecordPointChecker> RecordCheckers
        {
            get { return _recordCheckers; }
            set { _recordCheckers = value; }
        }

        public event VoidHandler DrawVehicles;

        MotorDepotEntities _contextMotorDepotOnline;
        public MotorDepotEntities ContextMotorDepotOnline
        {
            get { return _contextMotorDepotOnline; }
        }

        public OnlineController()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                _provider = new OnlineDataProvider(driverDb);
                _timer = new System.Threading.Timer(Analize, null, Timeout.Infinite, Timeout.Infinite);
                _onlineVehicles = new List<OnlineVehicle>();
                _trackOnlineVehicles = new List<OnlineVehicle>();
                _recordCheckers = new List<OrderRecordPointChecker>();
                _trackRecordCheckers = new List<OrderRecordPointChecker>();
            }
        }


        public int Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public void Start()
        {
            if (_isStarted)
                return;
            _isStarted = true;
        #if DEBUG
            //StartGenerateTestOnlineData();
        #endif
           
            ThreadPool.QueueUserWorkItem(Analize);
        }

        public void CreateContext()
        {
            if (_contextMotorDepotOnline == null)
            {
                _contextMotorDepotOnline = new MotorDepotEntities(MDController.ModelConString);
                MDController.InitEntyties(_contextMotorDepotOnline);
            }
        }

        public bool IsStarted
        {
            get { return _isStarted; }
            set { _isStarted = value; }
        }

        public void Stop()
        {
            _isStarted = false;
            if (_dataCreator != null) _dataCreator.Stop();
            if (_contextMotorDepotOnline != null)
            {
                _contextMotorDepotOnline.Dispose();
                _contextMotorDepotOnline = null;
            }
        }

        void Analize(object obj)
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            if (!_isStarted)
                return;
            AddVehiclesFromRecordCheckers();
            int newPointsCount = 0;
            if (_onlineVehicles.Count >0)
            {
                _trackOnlineVehicles.Clear();
                _trackOnlineVehicles.AddRange(_onlineVehicles);


                foreach (OnlineVehicle ov in _trackOnlineVehicles)
                {
                    IList<GpsData>  geoData = _provider.GetLastData(ov);
                    newPointsCount += geoData.Count;
                    foreach (GpsData p in geoData)
                    {
                        ov.LastPoint = p;
                        ov.OnlineData.Add(p);
                        ov.LastLogId = p.LogId;
                        if (p.Valid)
                        {
                            ov.GpsSignal = GpsSignal.Valid;
                            ov.LastTime = p.Time;
                            ov.Speed = p.Speed;
                            ov.UpdateMotionState();
                            if (_recordCheckers.Count > 0)
                            {
                                _trackRecordCheckers.Clear();
                                _trackRecordCheckers.AddRange(_recordCheckers);
                                _trackRecordCheckers.ForEach(delegate(OrderRecordPointChecker pointChecker)
                                {
                                    if (pointChecker.ModelVhOnline.MobitelId == ov.MobitelId)
                                    {
                                        pointChecker.CheckDataGps(p);
                                    }
                                    Application.DoEvents();  
                                });
                            }
                        }
                        else
                            ov.GpsSignal = GpsSignal.NotValid;
 
                    }
                    if (ov.OnlineData.Count > _tail)
                    {
                        ov.OnlineData.RemoveRange(0, ov.OnlineData.Count - _tail);
                    }
                }
            }


            if (_isStarted && newPointsCount > 0)
                DrawVehicles();
             _timer.Change(5000, Timeout.Infinite);
        }

        void AddVehiclesFromRecordCheckers()
        {
            if (_recordCheckers.Count > 0)
            {
                _recordCheckers.ForEach(delegate(OrderRecordPointChecker pointChecker)
                {
                    if (!IsVehicleInCollaction(pointChecker.ModelVhOnline)) _onlineVehicles.Add(pointChecker.ModelVhOnline);
                });
            }
        }

        public bool IsVehicleInCollaction(OnlineVehicle ov)
        {
            foreach (OnlineVehicle onlineVeh in _onlineVehicles)
            {
                if (onlineVeh.MobitelId == ov.MobitelId) return true;
            }
            return false;
        }

        void StartGenerateTestOnlineData()
        {
            _dataCreator = new OrderTestGpsEvents();
            _dataCreator.Start();
        }

        public void Clear()
        {
            _onlineVehicles.Clear();
            _trackOnlineVehicles.Clear();
            _recordCheckers.Clear();
            _trackRecordCheckers.Clear();
        }
    }
}
