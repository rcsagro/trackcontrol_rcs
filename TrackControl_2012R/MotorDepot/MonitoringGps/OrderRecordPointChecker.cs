﻿using System;
using System.Diagnostics; 
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Linq;
using TrackControl.General;
using TrackControl.Notification;
using TrackControl.Online;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace MotorDepot.MonitoringGps
{
    public class OrderRecordPointChecker
    {
        public event VoidHandler NextStep;
        public static event VoidHandler UpdateLog;

        private MotorDepotEntities _сontextMotorDepot;
        public ZoneEventTypes ZoneEventType { get; set; }
        public ordert OrderRecord { get; set; }
        public OnlineVehicle ModelVhOnline { get; set; }
        private double _distance;
        private double _timeForControlInObject;
        private GpsData _prevGpsData;
        private bool _isDistanceExcessWrited;
        private bool _isTimeExcessWrited;
        private DateTime? _timeStart;
        private IZone _recordsZone;
        private List<GpsData> _dataGps;
        private OnlineController  _onlineController;
        private double _delayHours;
        private bool _isOnlineMode;
        private bool _isCheckerFinish;
        private bool _isSetTimeStart;

        public bool IsCheckerFinish
        {
            get { return _isCheckerFinish; }
        }

        public GpsData PrevGpsData
        {
            get { return _prevGpsData; }
            set { 
                _prevGpsData = value;
                _dataGps.Add(_prevGpsData);
                }
        }

        #region Сообщения о событиях
            private const string GrnMesTimeOverInObject = "Превышено время пребывания на объекте";
            private const string GrnMesTimeOverMoveToObject = "Превышено время движения к объекту";
            private const string GrnMesDistOverMoveToObject = "Превышено расстояние движения до объекта";
        #endregion
        

        public GpsData LastGpsData { get; set; }

        public OrderRecordPointChecker(ordert orderRecord, ZoneEventTypes zoneEventType, OnlineVehicle modelVhOnline, double delayHours, DateTime? timeStart,bool isOnlineMode)
        {
            if (isOnlineMode)
            {
                _сontextMotorDepot = OnlineController.Instance.ContextMotorDepotOnline;
            }
            else
            {
                _сontextMotorDepot = MDController.Instance.ContextMotorDepot;
            }
            OrderRecord = orderRecord;
            ZoneEventType = zoneEventType;
            _timeStart = timeStart;
            _recordsZone = EntityFrameworkModelConverter.ConvertZoneEfToModel(OrderRecord.md_object.zones);
            _isOnlineMode = isOnlineMode;
            ModelVhOnline = modelVhOnline;
            _onlineController = OnlineController.Instance;
            _dataGps = new List<GpsData>();
            _delayHours = delayHours;
            _timeForControlInObject =(double)(OrderRecord.TimeDelay + OrderRecord.TimeLoading + OrderRecord.TimeUnLoading);
            if (isOnlineMode && (OrderRecord.IsClose || IsOrderClose())) StopCheking(true);; 
        }

        public void CheckDataGps(GpsData gpsData)
        {
            if (_isCheckerFinish || ((_prevGpsData != null) && _prevGpsData.Time.Subtract (gpsData.Time).TotalSeconds >0) 
                || _dataGps.Contains(gpsData) || (gpsData.Mobitel != ModelVhOnline.MobitelId)) 
                return;
            if (!_isSetTimeStart)
            {
                
                _timeStart = gpsData.Time;
                //Debug.Print(string.Format("Started {0} / {1} -> {2} ", OrderRecord.Id, OrderRecord.md_order.Id, _timeStart, gpsData.LogID));
                _isSetTimeStart = true;
            }
            else
            {
                
                CheckDistance(gpsData);
            }
            _prevGpsData = gpsData;
            //Debug.Print("Check {0} ", gpsData.Time); 
            _dataGps.Add(gpsData);
            if (_isOnlineMode) CheckTime(gpsData);
            CheckZone(gpsData);
        }

        private void CheckTime(GpsData gpsData)
        {
            if (!_isTimeExcessWrited)
            {
                //Debug.Print("Check Time {0} from {1} = {3} with {2}  ", gpsData.Time, _timeStart, (double)OrderRecord.TimeToObject, gpsData.Time.Subtract((DateTime)_timeStart).TotalHours); 
                switch (ZoneEventType)
                {
                    case ZoneEventTypes.EntryToZone:
                        {
                            if (_timeStart != null && ((double)OrderRecord.TimeToObject + _delayHours) >0 && gpsData.Time.Subtract((DateTime)_timeStart).TotalHours > ((double)OrderRecord.TimeToObject + _delayHours)) WriteLogTimeExcess(GrnMesTimeOverMoveToObject, gpsData,Math.Round((double)OrderRecord.TimeToObject + _delayHours,2));
                            break;
                        }
                    case ZoneEventTypes.ExitFromZone:
                        {

                            if (_timeStart != null && _timeForControlInObject > 0 && gpsData.Time.Subtract((DateTime)_timeStart).TotalHours > _timeForControlInObject)
                                WriteLogTimeExcess(GrnMesTimeOverInObject, gpsData,
                                                   Math.Round(_timeForControlInObject, 2));

                            break;
                        }
                }
                
            }
        }

        private void CheckDistance(GpsData gpsData)
        {
            if (!_isDistanceExcessWrited)
            {
                _distance += СGeoDistance.GetDistance(_prevGpsData.LatLng, gpsData.LatLng);
                switch (ZoneEventType)
                {
                    case ZoneEventTypes.EntryToZone:
                        {
                            if (_distance > (double)OrderRecord.DistanceToObject && _isOnlineMode && (double)OrderRecord.DistanceToObject > 0) WriteLogDistanceExcess(gpsData);
                            break;
                        }
                    case ZoneEventTypes.ExitFromZone:
                        {
                            break;
                        }
                }
                
            }
        }

        private void CheckZone(GpsData gpsData)
        {
            if (OrderRecord == null) return;
            switch (ZoneEventType)
            {
                case  ZoneEventTypes.EntryToZone:
                    {
                        if (_recordsZone.Contains(gpsData.LatLng))
                        {
                            if (_isOnlineMode && OrderRecord.md_object != null) WriteLogZoneEntryExit(string.Format("Въезд в зону {0}", OrderRecord.md_object.Name), gpsData);
                            SetReady();
                        }
                        break;
                    }
                case ZoneEventTypes.ExitFromZone:
                    {
                        if (!_recordsZone.Contains(gpsData.LatLng))
                        {
                            if (_isOnlineMode && OrderRecord.md_object != null) WriteLogZoneEntryExit(string.Format("Выезд из зоны {0}", OrderRecord.md_object.Name), gpsData);
                            SetReady();
                        }
                        break;
                    }
            }
        }

        void SetReady()
        {
            LastGpsData = _dataGps[_dataGps.Count - 1];
            switch (ZoneEventType)
            {
                case ZoneEventTypes.EntryToZone:
                    {
                        SetFactEntryToZoneParams();
                        break;
                    }
                case ZoneEventTypes.ExitFromZone:
                    {

                        SetFactExitFromZoneParams();
                        break;
                    }
                default:
                    {
                        XtraMessageBox.Show("Не указан тип определения зоны!");
                        SetFactExitFromZoneParams();
                        break;
                    }
            }
            StopCheking(true);
        }

        public void StopCheking(bool SetNextStep)
        {
            _isCheckerFinish = true;
            if (_isOnlineMode)
            {
                if (_onlineController.RecordCheckers.Contains(this))
                    _onlineController.RecordCheckers.Remove(this);
                if (NextStep != null && SetNextStep)
                {
                    NextStep();
                    //Debug.Print("NextStep {0} {1}", OrderRecord.Id, ZoneEventType.ToString());
                }
            }
        }



        private decimal GetDistance()
        {
            List<PointLatLng> points = new List<PointLatLng>();
            foreach (GpsData dataGps in _dataGps)
            {
                points.Add(dataGps.LatLng);
            }
            return Math.Round((decimal)СGeoDistance.GetDistance(points), 2);
        }

        void SetFactEntryToZoneParams()
        {
            if (_dataGps == null || _dataGps.Count == 0 || IsRecordActionAfterOrderExecute()) return;
            OrderRecord.DistanceToObjectFact = GetDistance();
            OrderRecord.TimeToObjectFact = Math.Round((decimal)_dataGps[_dataGps.Count - 1].Time.Subtract(_dataGps[0].Time).TotalHours, 2);
            OrderRecord.TimeEnter = _dataGps[_dataGps.Count - 1].Time;
            OrderRecord.TimeStopFact = (decimal)BasicMethods.GetTotalStopsTime(_dataGps.ToArray(), 0).TotalHours;
            OrderTProvider.SetFactEntryToZoneParams(OrderRecord);
        }

        void SetFactExitFromZoneParams()
        {
             //IsOrderClose() 
            if (_dataGps == null || _dataGps.Count == 0 || IsRecordActionAfterOrderExecute()) return;

            OrderRecord.TimeExit = _dataGps[_dataGps.Count - 1].Time;
            OrderRecord.DistanceInObject = GetDistance();
            OrderRecord.TimeStopFactInObject = (decimal)BasicMethods.GetTotalStopsTime(_dataGps.ToArray(), 0).TotalHours;
            OrderTProvider.SetFactExitFromZoneParams(OrderRecord);
        }

        bool IsRecordActionAfterOrderExecute()
        {
            if (OrderRecord.md_order.DateExecute != null)
            {
                if (OrderRecord.md_order.DateExecute.Value.Subtract(_dataGps[_dataGps.Count - 1].Time).TotalSeconds < 0) return true;
            }
            return false;
        }

        void WriteLogTimeExcess(string message, GpsData gpsData,double timeExcess)
        {
            SetLogRecord(string.Format("{0} час", timeExcess), message, gpsData);
             //Debug.Print(string.Format("start {0} end {1} Exc {2} {3} {4} {5}", _timeStart, gpsData.Time, timeExcess, message, OrderRecord.Id, OrderRecord.md_order.Id , PrevGpsData));
            _isTimeExcessWrited = true;
        }

        void WriteLogDistanceExcess(GpsData gpsData)
        {

            SetLogRecord(string.Format("{0} км", Math.Round((double)OrderRecord.DistanceToObject, 2)), GrnMesDistOverMoveToObject, gpsData);
            //Debug.Print(string.Format("{1}->{0} км start {2} end {3} {4} {5}", Math.Round((double)OrderRecord.DistanceToObject, 2), _distance, _timeStart, gpsData.Time, OrderRecord.Id, OrderRecord.md_order.Id));
            _isDistanceExcessWrited = true;
        }

        void WriteLogZoneEntryExit(string message, GpsData gpsData)
        {
            #if DEBUG
            //SetLogRecord(gpsData.Time.ToString("HH:mm"), message, gpsData);
            //Debug.Print("SetLogRecord {0} in {1} ", message, gpsData.Time); 
            #endif
        }

        /// <summary>
        /// Создание записи лога
        /// </summary>
        /// <param name="parValue"> значение параметра для записи в лог</param>
        /// <param name="parMessage">описание момента первышения параметра</param>
        /// <param name="gpsData">точка, вызвавшая событие (для контроля за единичночтью записи в логе пр слежении на разных компах)</param>
        void SetLogRecord(string parValue, string parMessage, GpsData gpsData)
        {
            //if (LogController.SetLogRecord(OrderRecord, parValue,parMessage,gpsData ))
            //Debug.Print("Log {0} {1} {2}", parMessage, parValue, gpsData.Time); 
             if (LogProvider.SetLogRecord(OrderRecord, parValue, parMessage, gpsData))
            {
                UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("{0}:{1} значение:{2}", parMessage, OrderRecord.md_object.Name, parValue), OrderRecord.md_order.Id);
            }
             if (UpdateLog != null) UpdateLog();
        }

        bool IsOrderClose()
        {
            order ord = _сontextMotorDepot.orderSet.Where(od => od.Id == OrderRecord.md_order.Id).FirstOrDefault();
            _сontextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ord);
            return ord.IsClose; 
        }
    }
}
