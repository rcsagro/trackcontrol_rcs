using System;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using MotorDepot.Properties;

namespace MotorDepot
{
    public partial class DevFormReportDeviationTime : BaseReports.ReportsDE.BaseControl
    {
        /// <summary>
        /// 2. ������ ���������� �� ������� �� ����������� ��������
        /// </summary>
        protected ReportBase<reportDeviatTime, TEventInfo> reportingDevTime;

        public DevFormReportDeviationTime()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();                                                       
            VisionPanel(gViewDeviatTime, gControlDeviatTime, bar1);
            gControlDeviatTime.DataSource = motodepotBindingSource;
            reportDeviatTimeSet = null;

            gViewDeviatTime.RowClick += new RowClickEventHandler(gViewDeviatTime_RowClick);

            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            reportingDevTime =
                new ReportBase<reportDeviatTime, TEventInfo>(Controls, compositeLink1, gViewDeviatTime,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        public override string Caption
        {
            get
            {
                return Resources.DevFormReportDeviationTime;
            }

        } // Caption
		
        protected void gViewDeviatTime_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                int indexRow = gViewDeviatTime.GetFocusedDataSourceRowIndex();
            }
        } // gViewEvent_RowClick

        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; 

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                // to do algoritm

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                } // if

                SetStartButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        public void Select(/* parameters */)
        {
            // to do
        }

        public override void ClearReport()
        {
            // to do
        }

        protected void CtearStatusBar()
        {
            // to do
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            // to do
        }
       
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                // to do this
                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader("", e);
            TEventInfo info = reportingDevTime.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + "0" + " " +
                Resources.PeriodTo + " " + "0";
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        protected string GetStringBreackLeft()
        {
            reportingDevTime.SetRectangleBrckLetf(0, 0, 300, 85);
            TEventInfo info = reportingDevTime.GetInfoStructure;
            return ("" + ": " + "0" + "\n" + "" + ": " + "0" + "\n" +
                "" + ": " + "0");
        }

        protected string GetStringBreackUp()
        {
            TEventInfo info = reportingDevTime.GetInfoStructure;
            reportingDevTime.SetRectangleBrckUP(370, 0, 340, 85);
            return ("" + ": " + "0" + "\n" + "" + ": " + "0");
        }

        protected string GetStringBreackRight()
        {
            TEventInfo info = reportingDevTime.GetInfoStructure;
            reportingDevTime.SetRectangleBrckRight(770, 0, 300, 85);
            return ("" + ": " + "0" + "\n" + "" + ": " + "0");
        } // GetStringBreackRight

        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gViewDeviatTime, true, true);

            TEventInfo t_info = new TEventInfo();

            // to do this

            reportingDevTime.AddInfoStructToList(t_info); 
            reportingDevTime.CreateAndShowReport(gControlDeviatTime);
        }

        protected override void ExportAllDevToReport()
        {
            // to do
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gViewDeviatTime);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gViewDeviatTime);
        }

        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gControlDeviatTime);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }

        public void Localization()
        {
            colIdCount.Caption = Resources.CaptionIdCount;
            colIdCount.ToolTip = Resources.ToolTipIdCount;

            colDate.Caption = Resources.Date;
            colDate.ToolTip = Resources.Date;

            colTime.Caption = Resources.Time;
            colTime.ToolTip = Resources.Time;

            colStateNumberAuto.Caption = Resources.CaptionStateNumberAuto;
            colStateNumberAuto.ToolTip = Resources.StateNumberAuto;

            colTimePlanRepriev.Caption = Resources.TimePlanRepriev;
            colTimePlanRepriev.ToolTip = Resources.TimePlanRepriev;

            colTimePlanMove.Caption = Resources.TimePlanMove;
            colTimePlanMove.ToolTip = Resources.TimePlanMove;

            colTimePlanLoading.Caption = Resources.TimePlanLoading;
            colTimePlanLoading.ToolTip = Resources.TimePlanLoading;

            colTimePlanUnloading.Caption = Resources.TimePlanUnloading;
            colTimePlanUnloading.ToolTip = Resources.TimePlanUnloading;

            colTimePlanDelay.Caption = Resources.TimePlanDelay;
            colTimePlanDelay.ToolTip = Resources.TimePlanDelay;

            colTimeFactMove.Caption = Resources.TimeFactMove;
            colTimeFactMove.ToolTip = Resources.TimeFactMove;

            colTimeFactStoping.Caption = Resources.TimeFactStoping;
            colTimeFactStoping.ToolTip = Resources.TimeFactStoping;

            colDifferenceMove.Caption = Resources.DifferenceMove;
            colDifferenceMove.ToolTip = Resources.DifferenceMove;


            colDiffernceStop.Caption = Resources.DiffernceStop;
            colDiffernceStop.ToolTip = Resources.DiffernceStop;
        } /* Localization */

        public class reportDeviatTime
        {
            // to do
            public reportDeviatTime(/* parameters */)
            {
                // to do
            }
        } // reportDeviatTime
    } // DevFormReportDeviationTime
} // MotorDepot
