namespace MotorDepot
{
    partial class DevFormReportDeviationTime
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevFormReportDeviationTime ) );
            this.barManager1 = new DevExpress.XtraBars.BarManager( this.components );
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            this.gControlDeviatTime = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceReportDeviatTime = new System.Windows.Forms.BindingSource( this.components );
            this.gViewDeviatTime = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateNumberAuto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanRepriev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanLoading = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanUnloading = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanDelay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFactMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFactStoping = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDifferenceMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiffernceStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.reportDeviatTimeSet = new System.Data.DataSet();
            this.motodepotBindingSource = new System.Windows.Forms.BindingSource( this.components );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.barManager1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlDeviatTime ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.bindingSourceReportDeviatTime ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gViewDeviatTime ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.reportDeviatTimeSet ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.motodepotBindingSource ) ).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this.bar1} );
            this.barManager1.DockControls.Add( this.barDockControl1 );
            this.barManager1.DockControls.Add( this.barDockControl2 );
            this.barManager1.DockControls.Add( this.barDockControl3 );
            this.barManager1.DockControls.Add( this.barDockControl4 );
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4} );
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4)} );
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "barStaticItem2";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "barStaticItem3";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "barStaticItem4";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer ) ( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins( 10, 10, 70, 10 );
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins( 10, 10, 15, 10 );
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // gControlDeviatTime
            // 
            this.gControlDeviatTime.DataSource = this.bindingSourceReportDeviatTime;
            this.gControlDeviatTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gControlDeviatTime.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gControlDeviatTime.Location = new System.Drawing.Point( 0, 26 );
            this.gControlDeviatTime.MainView = this.gViewDeviatTime;
            this.gControlDeviatTime.Name = "gControlDeviatTime";
            this.gControlDeviatTime.Size = new System.Drawing.Size( 781, 287 );
            this.gControlDeviatTime.TabIndex = 12;
            this.gControlDeviatTime.UseEmbeddedNavigator = true;
            this.gControlDeviatTime.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gViewDeviatTime,
            this.gridView2} );
            // 
            // gViewDeviatTime
            // 
            this.gViewDeviatTime.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gViewDeviatTime.Appearance.Empty.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gViewDeviatTime.Appearance.EvenRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gViewDeviatTime.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gViewDeviatTime.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gViewDeviatTime.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gViewDeviatTime.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewDeviatTime.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gViewDeviatTime.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gViewDeviatTime.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewDeviatTime.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewDeviatTime.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewDeviatTime.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gViewDeviatTime.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gViewDeviatTime.Appearance.GroupButton.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gViewDeviatTime.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gViewDeviatTime.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gViewDeviatTime.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gViewDeviatTime.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.gViewDeviatTime.Appearance.GroupRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.GroupRow.Options.UseFont = true;
            this.gViewDeviatTime.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewDeviatTime.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gViewDeviatTime.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewDeviatTime.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewDeviatTime.Appearance.HorzLine.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gViewDeviatTime.Appearance.OddRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gViewDeviatTime.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.Preview.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.Preview.Options.UseForeColor = true;
            this.gViewDeviatTime.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gViewDeviatTime.Appearance.Row.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gViewDeviatTime.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gViewDeviatTime.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewDeviatTime.Appearance.VertLine.Options.UseBackColor = true;
            this.gViewDeviatTime.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gViewDeviatTime.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewDeviatTime.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewDeviatTime.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewDeviatTime.BestFitMaxRowCount = 2;
            this.gViewDeviatTime.ColumnPanelRowHeight = 40;
            this.gViewDeviatTime.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdCount,
            this.colDate,
            this.colTime,
            this.colStateNumberAuto,
            this.colTimePlanRepriev,
            this.colTimePlanMove,
            this.colTimePlanLoading,
            this.colTimePlanUnloading,
            this.colTimePlanDelay,
            this.colTimeFactMove,
            this.colTimeFactStoping,
            this.colDifferenceMove,
            this.colDiffernceStop} );
            this.gViewDeviatTime.GridControl = this.gControlDeviatTime;
            this.gViewDeviatTime.GroupSummary.AddRange( new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colTimePlanDelay, "")} );
            this.gViewDeviatTime.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gViewDeviatTime.Name = "gViewDeviatTime";
            this.gViewDeviatTime.OptionsDetail.AllowZoomDetail = false;
            this.gViewDeviatTime.OptionsDetail.EnableMasterViewMode = false;
            this.gViewDeviatTime.OptionsDetail.ShowDetailTabs = false;
            this.gViewDeviatTime.OptionsDetail.SmartDetailExpand = false;
            this.gViewDeviatTime.OptionsSelection.MultiSelect = true;
            this.gViewDeviatTime.OptionsView.ColumnAutoWidth = false;
            this.gViewDeviatTime.OptionsView.EnableAppearanceEvenRow = true;
            this.gViewDeviatTime.OptionsView.EnableAppearanceOddRow = true;
            this.gViewDeviatTime.OptionsView.ShowFooter = true;
            // 
            // colIdCount
            // 
            this.colIdCount.AppearanceCell.Options.UseTextOptions = true;
            this.colIdCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIdCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdCount.Caption = "� �/�";
            this.colIdCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIdCount.FieldName = "IdCount";
            this.colIdCount.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colIdCount.MinWidth = 10;
            this.colIdCount.Name = "colIdCount";
            this.colIdCount.OptionsColumn.AllowEdit = false;
            this.colIdCount.OptionsColumn.AllowFocus = false;
            this.colIdCount.OptionsColumn.FixedWidth = true;
            this.colIdCount.OptionsColumn.ReadOnly = true;
            this.colIdCount.ToolTip = "����� ������ �� �������";
            this.colIdCount.Visible = true;
            this.colIdCount.VisibleIndex = 0;
            this.colIdCount.Width = 20;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.Caption = "����";
            this.colDate.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "Date";
            this.colDate.MinWidth = 10;
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.AllowFocus = false;
            this.colDate.OptionsColumn.FixedWidth = true;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.ToolTip = "����";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            this.colDate.Width = 60;
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.Caption = "�����";
            this.colTime.DisplayFormat.FormatString = "HH:mm:ss";
            this.colTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTime.FieldName = "Time";
            this.colTime.MinWidth = 10;
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.AllowEdit = false;
            this.colTime.OptionsColumn.AllowFocus = false;
            this.colTime.OptionsColumn.FixedWidth = true;
            this.colTime.OptionsColumn.ReadOnly = true;
            this.colTime.ToolTip = "�����";
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 2;
            this.colTime.Width = 60;
            // 
            // colStateNumberAuto
            // 
            this.colStateNumberAuto.AppearanceCell.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateNumberAuto.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStateNumberAuto.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStateNumberAuto.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStateNumberAuto.Caption = "�������� ������";
            this.colStateNumberAuto.FieldName = "StateNumberAuto";
            this.colStateNumberAuto.MinWidth = 10;
            this.colStateNumberAuto.Name = "colStateNumberAuto";
            this.colStateNumberAuto.OptionsColumn.AllowEdit = false;
            this.colStateNumberAuto.OptionsColumn.AllowFocus = false;
            this.colStateNumberAuto.OptionsColumn.FixedWidth = true;
            this.colStateNumberAuto.OptionsColumn.ReadOnly = true;
            this.colStateNumberAuto.ToolTip = "��������������� ����� ������";
            this.colStateNumberAuto.Visible = true;
            this.colStateNumberAuto.VisibleIndex = 3;
            this.colStateNumberAuto.Width = 60;
            // 
            // colTimePlanRepriev
            // 
            this.colTimePlanRepriev.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanRepriev.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanRepriev.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanRepriev.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanRepriev.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanRepriev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanRepriev.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanRepriev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanRepriev.Caption = "����� �� �����. ��������";
            this.colTimePlanRepriev.DisplayFormat.FormatString = "HH:mm:ss";
            this.colTimePlanRepriev.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanRepriev.FieldName = "TimePlanRepriev";
            this.colTimePlanRepriev.MinWidth = 10;
            this.colTimePlanRepriev.Name = "colTimePlanRepriev";
            this.colTimePlanRepriev.OptionsColumn.AllowEdit = false;
            this.colTimePlanRepriev.OptionsColumn.AllowFocus = false;
            this.colTimePlanRepriev.OptionsColumn.FixedWidth = true;
            this.colTimePlanRepriev.OptionsColumn.ReadOnly = true;
            this.colTimePlanRepriev.ToolTip = "����� �� �����. ��������";
            this.colTimePlanRepriev.Visible = true;
            this.colTimePlanRepriev.VisibleIndex = 4;
            this.colTimePlanRepriev.Width = 60;
            // 
            // colTimePlanMove
            // 
            this.colTimePlanMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanMove.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanMove.Caption = "����� �� �����. ��������";
            this.colTimePlanMove.DisplayFormat.FormatString = "HH:mm:ss";
            this.colTimePlanMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanMove.FieldName = "TimePlanMove";
            this.colTimePlanMove.MinWidth = 10;
            this.colTimePlanMove.Name = "colTimePlanMove";
            this.colTimePlanMove.OptionsColumn.AllowEdit = false;
            this.colTimePlanMove.OptionsColumn.AllowFocus = false;
            this.colTimePlanMove.OptionsColumn.FixedWidth = true;
            this.colTimePlanMove.OptionsColumn.ReadOnly = true;
            this.colTimePlanMove.ToolTip = "����� �� �����. ��������";
            this.colTimePlanMove.Visible = true;
            this.colTimePlanMove.VisibleIndex = 5;
            this.colTimePlanMove.Width = 60;
            // 
            // colTimePlanLoading
            // 
            this.colTimePlanLoading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanLoading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanLoading.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanLoading.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanLoading.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanLoading.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanLoading.Caption = "����� �� �����. ��������";
            this.colTimePlanLoading.DisplayFormat.FormatString = "HH:mm:ss";
            this.colTimePlanLoading.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanLoading.FieldName = "TimePlanLoading";
            this.colTimePlanLoading.MinWidth = 10;
            this.colTimePlanLoading.Name = "colTimePlanLoading";
            this.colTimePlanLoading.OptionsColumn.AllowEdit = false;
            this.colTimePlanLoading.OptionsColumn.AllowFocus = false;
            this.colTimePlanLoading.OptionsColumn.FixedWidth = true;
            this.colTimePlanLoading.OptionsColumn.ReadOnly = true;
            this.colTimePlanLoading.ToolTip = "����� �� �����. ��������";
            this.colTimePlanLoading.Visible = true;
            this.colTimePlanLoading.VisibleIndex = 6;
            this.colTimePlanLoading.Width = 60;
            // 
            // colTimePlanUnloading
            // 
            this.colTimePlanUnloading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanUnloading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanUnloading.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanUnloading.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanUnloading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanUnloading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanUnloading.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanUnloading.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanUnloading.Caption = "����� �� �����. ���������";
            this.colTimePlanUnloading.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimePlanUnloading.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanUnloading.FieldName = "TimePlanUnloading";
            this.colTimePlanUnloading.MinWidth = 10;
            this.colTimePlanUnloading.Name = "colTimePlanUnloading";
            this.colTimePlanUnloading.OptionsColumn.AllowEdit = false;
            this.colTimePlanUnloading.OptionsColumn.AllowFocus = false;
            this.colTimePlanUnloading.OptionsColumn.FixedWidth = true;
            this.colTimePlanUnloading.OptionsColumn.ReadOnly = true;
            this.colTimePlanUnloading.ToolTip = "����� �� �����. ���������";
            this.colTimePlanUnloading.Visible = true;
            this.colTimePlanUnloading.VisibleIndex = 7;
            this.colTimePlanUnloading.Width = 60;
            // 
            // colTimePlanDelay
            // 
            this.colTimePlanDelay.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colTimePlanDelay.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanDelay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanDelay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanDelay.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanDelay.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanDelay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanDelay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimePlanDelay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimePlanDelay.Caption = "����� �� �����. ��������";
            this.colTimePlanDelay.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimePlanDelay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanDelay.FieldName = "TimePlanDelay";
            this.colTimePlanDelay.MinWidth = 10;
            this.colTimePlanDelay.Name = "colTimePlanDelay";
            this.colTimePlanDelay.OptionsColumn.AllowEdit = false;
            this.colTimePlanDelay.OptionsColumn.AllowFocus = false;
            this.colTimePlanDelay.OptionsColumn.FixedWidth = true;
            this.colTimePlanDelay.OptionsColumn.ReadOnly = true;
            this.colTimePlanDelay.ToolTip = "����� �� �����. ��������";
            this.colTimePlanDelay.Visible = true;
            this.colTimePlanDelay.VisibleIndex = 8;
            this.colTimePlanDelay.Width = 60;
            // 
            // colTimeFactMove
            // 
            this.colTimeFactMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFactMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFactMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFactMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFactMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactMove.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFactMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFactMove.Caption = "����� �� �����. ��������";
            this.colTimeFactMove.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimeFactMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeFactMove.FieldName = "TimeFactMove";
            this.colTimeFactMove.MinWidth = 10;
            this.colTimeFactMove.Name = "colTimeFactMove";
            this.colTimeFactMove.OptionsColumn.AllowEdit = false;
            this.colTimeFactMove.OptionsColumn.AllowFocus = false;
            this.colTimeFactMove.OptionsColumn.FixedWidth = true;
            this.colTimeFactMove.OptionsColumn.ReadOnly = true;
            this.colTimeFactMove.ToolTip = "����� �� �����. ��������";
            this.colTimeFactMove.Visible = true;
            this.colTimeFactMove.VisibleIndex = 9;
            this.colTimeFactMove.Width = 60;
            // 
            // colTimeFactStoping
            // 
            this.colTimeFactStoping.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFactStoping.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactStoping.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFactStoping.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFactStoping.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFactStoping.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactStoping.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFactStoping.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFactStoping.Caption = "����� �� �����. �������";
            this.colTimeFactStoping.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimeFactStoping.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeFactStoping.FieldName = "TimeFactStoping";
            this.colTimeFactStoping.MinWidth = 10;
            this.colTimeFactStoping.Name = "colTimeFactStoping";
            this.colTimeFactStoping.OptionsColumn.AllowEdit = false;
            this.colTimeFactStoping.OptionsColumn.AllowFocus = false;
            this.colTimeFactStoping.OptionsColumn.FixedWidth = true;
            this.colTimeFactStoping.OptionsColumn.ReadOnly = true;
            this.colTimeFactStoping.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTimeFactStoping.SummaryItem.FieldName = "TotalFuelAdd";
            this.colTimeFactStoping.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeFactStoping.ToolTip = "����� �� �����. �������";
            this.colTimeFactStoping.Visible = true;
            this.colTimeFactStoping.VisibleIndex = 10;
            this.colTimeFactStoping.Width = 50;
            // 
            // colDifferenceMove
            // 
            this.colDifferenceMove.AppearanceCell.Options.UseTextOptions = true;
            this.colDifferenceMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDifferenceMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDifferenceMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDifferenceMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colDifferenceMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDifferenceMove.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDifferenceMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDifferenceMove.Caption = "������� ��������";
            this.colDifferenceMove.DisplayFormat.FormatString = "hh:mm:ss";
            this.colDifferenceMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDifferenceMove.FieldName = "DifferenceMove";
            this.colDifferenceMove.MinWidth = 10;
            this.colDifferenceMove.Name = "colDifferenceMove";
            this.colDifferenceMove.OptionsColumn.AllowEdit = false;
            this.colDifferenceMove.OptionsColumn.AllowFocus = false;
            this.colDifferenceMove.OptionsColumn.FixedWidth = true;
            this.colDifferenceMove.OptionsColumn.ReadOnly = true;
            this.colDifferenceMove.SummaryItem.DisplayFormat = "{0:f2}";
            this.colDifferenceMove.SummaryItem.FieldName = "TotalFuelSub";
            this.colDifferenceMove.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDifferenceMove.ToolTip = "������� ��������";
            this.colDifferenceMove.Visible = true;
            this.colDifferenceMove.VisibleIndex = 11;
            this.colDifferenceMove.Width = 60;
            // 
            // colDiffernceStop
            // 
            this.colDiffernceStop.AppearanceCell.Options.UseTextOptions = true;
            this.colDiffernceStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiffernceStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDiffernceStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDiffernceStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colDiffernceStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiffernceStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDiffernceStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDiffernceStop.Caption = "������� �������";
            this.colDiffernceStop.DisplayFormat.FormatString = "hh:mm:ss";
            this.colDiffernceStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDiffernceStop.FieldName = "DiffernceStop";
            this.colDiffernceStop.MinWidth = 10;
            this.colDiffernceStop.Name = "colDiffernceStop";
            this.colDiffernceStop.OptionsColumn.AllowEdit = false;
            this.colDiffernceStop.OptionsColumn.AllowFocus = false;
            this.colDiffernceStop.OptionsColumn.FixedWidth = true;
            this.colDiffernceStop.OptionsColumn.ReadOnly = true;
            this.colDiffernceStop.ToolTip = "������� �������";
            this.colDiffernceStop.Visible = true;
            this.colDiffernceStop.VisibleIndex = 12;
            this.colDiffernceStop.Width = 60;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gControlDeviatTime;
            this.gridView2.Name = "gridView2";
            // 
            // reportDeviatTimeSet
            // 
            this.reportDeviatTimeSet.DataSetName = "NewDataSet";
            // 
            // DevFormReportDeviationTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.gControlDeviatTime );
            this.Controls.Add( this.barDockControl3 );
            this.Controls.Add( this.barDockControl4 );
            this.Controls.Add( this.barDockControl2 );
            this.Controls.Add( this.barDockControl1 );
            this.Name = "DevFormReportDeviationTime";
            this.Size = new System.Drawing.Size( 781, 362 );
            this.Controls.SetChildIndex( this.barDockControl1, 0 );
            this.Controls.SetChildIndex( this.barDockControl2, 0 );
            this.Controls.SetChildIndex( this.barDockControl4, 0 );
            this.Controls.SetChildIndex( this.barDockControl3, 0 );
            this.Controls.SetChildIndex( this.gControlDeviatTime, 0 );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.barManager1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeLink1.ImageCollection ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlDeviatTime ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.bindingSourceReportDeviatTime ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gViewDeviatTime ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.reportDeviatTimeSet ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.motodepotBindingSource ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private DevExpress.XtraGrid.GridControl gControlDeviatTime;
        private DevExpress.XtraGrid.Views.Grid.GridView gViewDeviatTime;
        private DevExpress.XtraGrid.Columns.GridColumn colIdCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStateNumberAuto;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanRepriev;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanLoading;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanUnloading;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanDelay;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFactMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFactStoping;
        private DevExpress.XtraGrid.Columns.GridColumn colDifferenceMove;
        private DevExpress.XtraGrid.Columns.GridColumn colDiffernceStop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Data.DataSet reportDeviatTimeSet;
        protected System.Windows.Forms.BindingSource bindingSourceReportDeviatTime;
        protected System.Windows.Forms.BindingSource motodepotBindingSource;
    }
}
