namespace MotorDepot.DifferentSheets
{
    partial class CardCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardCustomer));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer6 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer7 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer8 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer9 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer10 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer11 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer13 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer12 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer14 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer15 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer16 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer17 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer18 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer19 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer20 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer21 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer22 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer23 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer24 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer25 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer26 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer27 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer28 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer29 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer30 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer31 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer32 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer33 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer34 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer35 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer36 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer37 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer38 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer39 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer40 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer41 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gControlInfo = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEstimateValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTraveled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTraveledSender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPay = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gControlInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1,
            this.winControlContainer2,
            this.winControlContainer3,
            this.winControlContainer4,
            this.winControlContainer5,
            this.winControlContainer6,
            this.winControlContainer7,
            this.winControlContainer8,
            this.winControlContainer9,
            this.winControlContainer10,
            this.winControlContainer11,
            this.winControlContainer13,
            this.winControlContainer12,
            this.winControlContainer14,
            this.winControlContainer15,
            this.winControlContainer16,
            this.winControlContainer17,
            this.winControlContainer18,
            this.winControlContainer19,
            this.winControlContainer20,
            this.winControlContainer21,
            this.winControlContainer22,
            this.winControlContainer23,
            this.winControlContainer24,
            this.winControlContainer25,
            this.winControlContainer26,
            this.winControlContainer27,
            this.winControlContainer28,
            this.winControlContainer29,
            this.winControlContainer30,
            this.winControlContainer31,
            this.winControlContainer32,
            this.winControlContainer33,
            this.winControlContainer34,
            this.winControlContainer35,
            this.winControlContainer36,
            this.winControlContainer37,
            this.winControlContainer38,
            this.winControlContainer39,
            this.winControlContainer40,
            this.winControlContainer41});
            this.Detail.Height = 826;
            this.Detail.MultiColumn.ColumnCount = 3;
            this.Detail.MultiColumn.ColumnSpacing = 10F;
            this.Detail.MultiColumn.ColumnWidth = 380F;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            resources.ApplyResources(this.Detail, "Detail");
            // 
            // winControlContainer1
            // 
            resources.ApplyResources(this.winControlContainer1, "winControlContainer1");
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.WinControl = this.labelControl1;
            // 
            // labelControl1
            // 
            this.labelControl1.AccessibleDescription = null;
            this.labelControl1.AccessibleName = null;
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Name = "labelControl1";
            // 
            // winControlContainer2
            // 
            resources.ApplyResources(this.winControlContainer2, "winControlContainer2");
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.WinControl = this.labelControl2;
            // 
            // labelControl2
            // 
            this.labelControl2.AccessibleDescription = null;
            this.labelControl2.AccessibleName = null;
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // winControlContainer3
            // 
            resources.ApplyResources(this.winControlContainer3, "winControlContainer3");
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.WinControl = this.labelControl3;
            // 
            // labelControl3
            // 
            this.labelControl3.AccessibleDescription = null;
            this.labelControl3.AccessibleName = null;
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Name = "labelControl3";
            // 
            // winControlContainer4
            // 
            resources.ApplyResources(this.winControlContainer4, "winControlContainer4");
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.WinControl = this.labelControl4;
            // 
            // labelControl4
            // 
            this.labelControl4.AccessibleDescription = null;
            this.labelControl4.AccessibleName = null;
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Name = "labelControl4";
            // 
            // winControlContainer5
            // 
            resources.ApplyResources(this.winControlContainer5, "winControlContainer5");
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.WinControl = this.labelControl5;
            // 
            // labelControl5
            // 
            this.labelControl5.AccessibleDescription = null;
            this.labelControl5.AccessibleName = null;
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Name = "labelControl5";
            // 
            // winControlContainer6
            // 
            resources.ApplyResources(this.winControlContainer6, "winControlContainer6");
            this.winControlContainer6.Name = "winControlContainer6";
            this.winControlContainer6.WinControl = this.labelControl6;
            // 
            // labelControl6
            // 
            this.labelControl6.AccessibleDescription = null;
            this.labelControl6.AccessibleName = null;
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Name = "labelControl6";
            // 
            // winControlContainer7
            // 
            resources.ApplyResources(this.winControlContainer7, "winControlContainer7");
            this.winControlContainer7.Name = "winControlContainer7";
            this.winControlContainer7.WinControl = this.labelControl7;
            // 
            // labelControl7
            // 
            this.labelControl7.AccessibleDescription = null;
            this.labelControl7.AccessibleName = null;
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Name = "labelControl7";
            // 
            // winControlContainer8
            // 
            resources.ApplyResources(this.winControlContainer8, "winControlContainer8");
            this.winControlContainer8.Name = "winControlContainer8";
            this.winControlContainer8.WinControl = this.labelControl8;
            // 
            // labelControl8
            // 
            this.labelControl8.AccessibleDescription = null;
            this.labelControl8.AccessibleName = null;
            resources.ApplyResources(this.labelControl8, "labelControl8");
            this.labelControl8.Name = "labelControl8";
            // 
            // winControlContainer9
            // 
            resources.ApplyResources(this.winControlContainer9, "winControlContainer9");
            this.winControlContainer9.Name = "winControlContainer9";
            this.winControlContainer9.WinControl = this.labelControl9;
            // 
            // labelControl9
            // 
            this.labelControl9.AccessibleDescription = null;
            this.labelControl9.AccessibleName = null;
            resources.ApplyResources(this.labelControl9, "labelControl9");
            this.labelControl9.Name = "labelControl9";
            // 
            // winControlContainer10
            // 
            resources.ApplyResources(this.winControlContainer10, "winControlContainer10");
            this.winControlContainer10.Name = "winControlContainer10";
            this.winControlContainer10.WinControl = this.labelControl10;
            // 
            // labelControl10
            // 
            this.labelControl10.AccessibleDescription = null;
            this.labelControl10.AccessibleName = null;
            resources.ApplyResources(this.labelControl10, "labelControl10");
            this.labelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl10.Name = "labelControl10";
            // 
            // winControlContainer11
            // 
            resources.ApplyResources(this.winControlContainer11, "winControlContainer11");
            this.winControlContainer11.Name = "winControlContainer11";
            this.winControlContainer11.WinControl = this.labelControl11;
            // 
            // labelControl11
            // 
            this.labelControl11.AccessibleDescription = null;
            this.labelControl11.AccessibleName = null;
            resources.ApplyResources(this.labelControl11, "labelControl11");
            this.labelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl11.Name = "labelControl11";
            // 
            // winControlContainer13
            // 
            resources.ApplyResources(this.winControlContainer13, "winControlContainer13");
            this.winControlContainer13.Name = "winControlContainer13";
            this.winControlContainer13.WinControl = this.labelControl12;
            // 
            // labelControl12
            // 
            this.labelControl12.AccessibleDescription = null;
            this.labelControl12.AccessibleName = null;
            resources.ApplyResources(this.labelControl12, "labelControl12");
            this.labelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl12.Name = "labelControl12";
            // 
            // winControlContainer12
            // 
            resources.ApplyResources(this.winControlContainer12, "winControlContainer12");
            this.winControlContainer12.Name = "winControlContainer12";
            this.winControlContainer12.WinControl = this.labelControl13;
            // 
            // labelControl13
            // 
            this.labelControl13.AccessibleDescription = null;
            this.labelControl13.AccessibleName = null;
            resources.ApplyResources(this.labelControl13, "labelControl13");
            this.labelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl13.Name = "labelControl13";
            // 
            // winControlContainer14
            // 
            resources.ApplyResources(this.winControlContainer14, "winControlContainer14");
            this.winControlContainer14.Name = "winControlContainer14";
            this.winControlContainer14.WinControl = this.labelControl14;
            // 
            // labelControl14
            // 
            this.labelControl14.AccessibleDescription = null;
            this.labelControl14.AccessibleName = null;
            resources.ApplyResources(this.labelControl14, "labelControl14");
            this.labelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl14.Name = "labelControl14";
            // 
            // winControlContainer15
            // 
            resources.ApplyResources(this.winControlContainer15, "winControlContainer15");
            this.winControlContainer15.Name = "winControlContainer15";
            this.winControlContainer15.WinControl = this.labelControl15;
            // 
            // labelControl15
            // 
            this.labelControl15.AccessibleDescription = null;
            this.labelControl15.AccessibleName = null;
            resources.ApplyResources(this.labelControl15, "labelControl15");
            this.labelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl15.Name = "labelControl15";
            // 
            // winControlContainer16
            // 
            resources.ApplyResources(this.winControlContainer16, "winControlContainer16");
            this.winControlContainer16.Name = "winControlContainer16";
            this.winControlContainer16.WinControl = this.labelControl16;
            // 
            // labelControl16
            // 
            this.labelControl16.AccessibleDescription = null;
            this.labelControl16.AccessibleName = null;
            resources.ApplyResources(this.labelControl16, "labelControl16");
            this.labelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl16.Name = "labelControl16";
            // 
            // winControlContainer17
            // 
            resources.ApplyResources(this.winControlContainer17, "winControlContainer17");
            this.winControlContainer17.Name = "winControlContainer17";
            this.winControlContainer17.WinControl = this.labelControl17;
            // 
            // labelControl17
            // 
            this.labelControl17.AccessibleDescription = null;
            this.labelControl17.AccessibleName = null;
            resources.ApplyResources(this.labelControl17, "labelControl17");
            this.labelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl17.Name = "labelControl17";
            // 
            // winControlContainer18
            // 
            resources.ApplyResources(this.winControlContainer18, "winControlContainer18");
            this.winControlContainer18.Name = "winControlContainer18";
            this.winControlContainer18.WinControl = this.labelControl18;
            // 
            // labelControl18
            // 
            this.labelControl18.AccessibleDescription = null;
            this.labelControl18.AccessibleName = null;
            resources.ApplyResources(this.labelControl18, "labelControl18");
            this.labelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl18.Name = "labelControl18";
            // 
            // winControlContainer19
            // 
            resources.ApplyResources(this.winControlContainer19, "winControlContainer19");
            this.winControlContainer19.Name = "winControlContainer19";
            this.winControlContainer19.WinControl = this.labelControl19;
            // 
            // labelControl19
            // 
            this.labelControl19.AccessibleDescription = null;
            this.labelControl19.AccessibleName = null;
            resources.ApplyResources(this.labelControl19, "labelControl19");
            this.labelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl19.Name = "labelControl19";
            // 
            // winControlContainer20
            // 
            resources.ApplyResources(this.winControlContainer20, "winControlContainer20");
            this.winControlContainer20.Name = "winControlContainer20";
            this.winControlContainer20.WinControl = this.labelControl20;
            // 
            // labelControl20
            // 
            this.labelControl20.AccessibleDescription = null;
            this.labelControl20.AccessibleName = null;
            resources.ApplyResources(this.labelControl20, "labelControl20");
            this.labelControl20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl20.Name = "labelControl20";
            // 
            // winControlContainer21
            // 
            resources.ApplyResources(this.winControlContainer21, "winControlContainer21");
            this.winControlContainer21.Name = "winControlContainer21";
            this.winControlContainer21.WinControl = this.labelControl21;
            // 
            // labelControl21
            // 
            this.labelControl21.AccessibleDescription = null;
            this.labelControl21.AccessibleName = null;
            resources.ApplyResources(this.labelControl21, "labelControl21");
            this.labelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl21.Name = "labelControl21";
            // 
            // winControlContainer22
            // 
            resources.ApplyResources(this.winControlContainer22, "winControlContainer22");
            this.winControlContainer22.Name = "winControlContainer22";
            this.winControlContainer22.WinControl = this.labelControl22;
            // 
            // labelControl22
            // 
            this.labelControl22.AccessibleDescription = null;
            this.labelControl22.AccessibleName = null;
            resources.ApplyResources(this.labelControl22, "labelControl22");
            this.labelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl22.Name = "labelControl22";
            // 
            // winControlContainer23
            // 
            resources.ApplyResources(this.winControlContainer23, "winControlContainer23");
            this.winControlContainer23.Name = "winControlContainer23";
            this.winControlContainer23.WinControl = this.labelControl23;
            // 
            // labelControl23
            // 
            this.labelControl23.AccessibleDescription = null;
            this.labelControl23.AccessibleName = null;
            resources.ApplyResources(this.labelControl23, "labelControl23");
            this.labelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl23.Name = "labelControl23";
            // 
            // winControlContainer24
            // 
            resources.ApplyResources(this.winControlContainer24, "winControlContainer24");
            this.winControlContainer24.Name = "winControlContainer24";
            this.winControlContainer24.WinControl = this.labelControl24;
            // 
            // labelControl24
            // 
            this.labelControl24.AccessibleDescription = null;
            this.labelControl24.AccessibleName = null;
            resources.ApplyResources(this.labelControl24, "labelControl24");
            this.labelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl24.Name = "labelControl24";
            // 
            // winControlContainer25
            // 
            resources.ApplyResources(this.winControlContainer25, "winControlContainer25");
            this.winControlContainer25.Name = "winControlContainer25";
            this.winControlContainer25.WinControl = this.labelControl25;
            // 
            // labelControl25
            // 
            this.labelControl25.AccessibleDescription = null;
            this.labelControl25.AccessibleName = null;
            resources.ApplyResources(this.labelControl25, "labelControl25");
            this.labelControl25.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl25.Name = "labelControl25";
            // 
            // winControlContainer26
            // 
            resources.ApplyResources(this.winControlContainer26, "winControlContainer26");
            this.winControlContainer26.Name = "winControlContainer26";
            this.winControlContainer26.WinControl = this.labelControl26;
            // 
            // labelControl26
            // 
            this.labelControl26.AccessibleDescription = null;
            this.labelControl26.AccessibleName = null;
            resources.ApplyResources(this.labelControl26, "labelControl26");
            this.labelControl26.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl26.Name = "labelControl26";
            // 
            // winControlContainer27
            // 
            resources.ApplyResources(this.winControlContainer27, "winControlContainer27");
            this.winControlContainer27.Name = "winControlContainer27";
            this.winControlContainer27.WinControl = this.labelControl27;
            // 
            // labelControl27
            // 
            this.labelControl27.AccessibleDescription = null;
            this.labelControl27.AccessibleName = null;
            resources.ApplyResources(this.labelControl27, "labelControl27");
            this.labelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl27.Name = "labelControl27";
            // 
            // winControlContainer28
            // 
            resources.ApplyResources(this.winControlContainer28, "winControlContainer28");
            this.winControlContainer28.Name = "winControlContainer28";
            this.winControlContainer28.WinControl = this.labelControl28;
            // 
            // labelControl28
            // 
            this.labelControl28.AccessibleDescription = null;
            this.labelControl28.AccessibleName = null;
            resources.ApplyResources(this.labelControl28, "labelControl28");
            this.labelControl28.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl28.Name = "labelControl28";
            // 
            // winControlContainer29
            // 
            resources.ApplyResources(this.winControlContainer29, "winControlContainer29");
            this.winControlContainer29.Name = "winControlContainer29";
            this.winControlContainer29.WinControl = this.labelControl29;
            // 
            // labelControl29
            // 
            this.labelControl29.AccessibleDescription = null;
            this.labelControl29.AccessibleName = null;
            resources.ApplyResources(this.labelControl29, "labelControl29");
            this.labelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl29.Name = "labelControl29";
            // 
            // winControlContainer30
            // 
            resources.ApplyResources(this.winControlContainer30, "winControlContainer30");
            this.winControlContainer30.Name = "winControlContainer30";
            this.winControlContainer30.WinControl = this.labelControl30;
            // 
            // labelControl30
            // 
            this.labelControl30.AccessibleDescription = null;
            this.labelControl30.AccessibleName = null;
            resources.ApplyResources(this.labelControl30, "labelControl30");
            this.labelControl30.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl30.Name = "labelControl30";
            // 
            // winControlContainer31
            // 
            resources.ApplyResources(this.winControlContainer31, "winControlContainer31");
            this.winControlContainer31.Name = "winControlContainer31";
            this.winControlContainer31.WinControl = this.labelControl31;
            // 
            // labelControl31
            // 
            this.labelControl31.AccessibleDescription = null;
            this.labelControl31.AccessibleName = null;
            resources.ApplyResources(this.labelControl31, "labelControl31");
            this.labelControl31.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl31.Name = "labelControl31";
            // 
            // winControlContainer32
            // 
            resources.ApplyResources(this.winControlContainer32, "winControlContainer32");
            this.winControlContainer32.Name = "winControlContainer32";
            this.winControlContainer32.WinControl = this.labelControl32;
            // 
            // labelControl32
            // 
            this.labelControl32.AccessibleDescription = null;
            this.labelControl32.AccessibleName = null;
            resources.ApplyResources(this.labelControl32, "labelControl32");
            this.labelControl32.Name = "labelControl32";
            // 
            // winControlContainer33
            // 
            resources.ApplyResources(this.winControlContainer33, "winControlContainer33");
            this.winControlContainer33.Name = "winControlContainer33";
            this.winControlContainer33.WinControl = this.labelControl33;
            // 
            // labelControl33
            // 
            this.labelControl33.AccessibleDescription = null;
            this.labelControl33.AccessibleName = null;
            resources.ApplyResources(this.labelControl33, "labelControl33");
            this.labelControl33.Name = "labelControl33";
            // 
            // winControlContainer34
            // 
            resources.ApplyResources(this.winControlContainer34, "winControlContainer34");
            this.winControlContainer34.Name = "winControlContainer34";
            this.winControlContainer34.WinControl = this.labelControl34;
            // 
            // labelControl34
            // 
            this.labelControl34.AccessibleDescription = null;
            this.labelControl34.AccessibleName = null;
            resources.ApplyResources(this.labelControl34, "labelControl34");
            this.labelControl34.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl34.Name = "labelControl34";
            // 
            // winControlContainer35
            // 
            resources.ApplyResources(this.winControlContainer35, "winControlContainer35");
            this.winControlContainer35.Name = "winControlContainer35";
            this.winControlContainer35.WinControl = this.labelControl35;
            // 
            // labelControl35
            // 
            this.labelControl35.AccessibleDescription = null;
            this.labelControl35.AccessibleName = null;
            resources.ApplyResources(this.labelControl35, "labelControl35");
            this.labelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl35.Name = "labelControl35";
            // 
            // winControlContainer36
            // 
            resources.ApplyResources(this.winControlContainer36, "winControlContainer36");
            this.winControlContainer36.Name = "winControlContainer36";
            this.winControlContainer36.WinControl = this.labelControl36;
            // 
            // labelControl36
            // 
            this.labelControl36.AccessibleDescription = null;
            this.labelControl36.AccessibleName = null;
            resources.ApplyResources(this.labelControl36, "labelControl36");
            this.labelControl36.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl36.Name = "labelControl36";
            // 
            // winControlContainer37
            // 
            resources.ApplyResources(this.winControlContainer37, "winControlContainer37");
            this.winControlContainer37.Name = "winControlContainer37";
            this.winControlContainer37.WinControl = this.labelControl37;
            // 
            // labelControl37
            // 
            this.labelControl37.AccessibleDescription = null;
            this.labelControl37.AccessibleName = null;
            resources.ApplyResources(this.labelControl37, "labelControl37");
            this.labelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl37.Name = "labelControl37";
            // 
            // winControlContainer38
            // 
            resources.ApplyResources(this.winControlContainer38, "winControlContainer38");
            this.winControlContainer38.Name = "winControlContainer38";
            this.winControlContainer38.WinControl = this.labelControl38;
            // 
            // labelControl38
            // 
            this.labelControl38.AccessibleDescription = null;
            this.labelControl38.AccessibleName = null;
            resources.ApplyResources(this.labelControl38, "labelControl38");
            this.labelControl38.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl38.Name = "labelControl38";
            // 
            // winControlContainer39
            // 
            resources.ApplyResources(this.winControlContainer39, "winControlContainer39");
            this.winControlContainer39.Name = "winControlContainer39";
            this.winControlContainer39.WinControl = this.labelControl39;
            // 
            // labelControl39
            // 
            this.labelControl39.AccessibleDescription = null;
            this.labelControl39.AccessibleName = null;
            resources.ApplyResources(this.labelControl39, "labelControl39");
            this.labelControl39.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl39.Name = "labelControl39";
            // 
            // winControlContainer40
            // 
            resources.ApplyResources(this.winControlContainer40, "winControlContainer40");
            this.winControlContainer40.Name = "winControlContainer40";
            this.winControlContainer40.WinControl = this.labelControl40;
            // 
            // labelControl40
            // 
            this.labelControl40.AccessibleDescription = null;
            this.labelControl40.AccessibleName = null;
            resources.ApplyResources(this.labelControl40, "labelControl40");
            this.labelControl40.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl40.Name = "labelControl40";
            // 
            // winControlContainer41
            // 
            resources.ApplyResources(this.winControlContainer41, "winControlContainer41");
            this.winControlContainer41.Name = "winControlContainer41";
            this.winControlContainer41.WinControl = this.gControlInfo;
            // 
            // gControlInfo
            // 
            this.gControlInfo.AccessibleDescription = null;
            this.gControlInfo.AccessibleName = null;
            resources.ApplyResources(this.gControlInfo, "gControlInfo");
            this.gControlInfo.BackgroundImage = null;
            this.gControlInfo.EmbeddedNavigator.AccessibleDescription = null;
            this.gControlInfo.EmbeddedNavigator.AccessibleName = null;
            this.gControlInfo.EmbeddedNavigator.AllowHtmlTextInToolTip = ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("gControlInfo.EmbeddedNavigator.AllowHtmlTextInToolTip")));
            this.gControlInfo.EmbeddedNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("gControlInfo.EmbeddedNavigator.Anchor")));
            this.gControlInfo.EmbeddedNavigator.BackgroundImage = null;
            this.gControlInfo.EmbeddedNavigator.BackgroundImageLayout = ((System.Windows.Forms.ImageLayout)(resources.GetObject("gControlInfo.EmbeddedNavigator.BackgroundImageLayout")));
            this.gControlInfo.EmbeddedNavigator.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("gControlInfo.EmbeddedNavigator.ImeMode")));
            this.gControlInfo.EmbeddedNavigator.TextLocation = ((DevExpress.XtraEditors.NavigatorButtonsTextLocation)(resources.GetObject("gControlInfo.EmbeddedNavigator.TextLocation")));
            this.gControlInfo.EmbeddedNavigator.ToolTip = resources.GetString("gControlInfo.EmbeddedNavigator.ToolTip");
            this.gControlInfo.EmbeddedNavigator.ToolTipIconType = ((DevExpress.Utils.ToolTipIconType)(resources.GetObject("gControlInfo.EmbeddedNavigator.ToolTipIconType")));
            this.gControlInfo.EmbeddedNavigator.ToolTipTitle = resources.GetString("gControlInfo.EmbeddedNavigator.ToolTipTitle");
            this.gControlInfo.Font = null;
            this.gControlInfo.MainView = this.gridView1;
            this.gControlInfo.Name = "gControlInfo";
            this.gControlInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.AppearancePrint.Lines.BackColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.BackColor2 = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.BorderColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.ForeColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.Lines.Options.UseBorderColor = true;
            this.gridView1.AppearancePrint.Lines.Options.UseForeColor = true;
            resources.ApplyResources(this.gridView1, "gridView1");
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEstimateValue,
            this.colPayTime,
            this.colTraveled,
            this.colTraveledSender,
            this.colTotalPay});
            this.gridView1.GridControl = this.gControlInfo;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsPrint.PrintFooter = false;
            this.gridView1.OptionsPrint.PrintGroupFooter = false;
            this.gridView1.OptionsPrint.UsePrintStyles = true;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 40;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // colEstimateValue
            // 
            this.colEstimateValue.AppearanceCell.Options.UseTextOptions = true;
            this.colEstimateValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEstimateValue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEstimateValue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEstimateValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colEstimateValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEstimateValue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEstimateValue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.colEstimateValue, "colEstimateValue");
            this.colEstimateValue.FieldName = "EstimateValue";
            this.colEstimateValue.Name = "colEstimateValue";
            // 
            // colPayTime
            // 
            this.colPayTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPayTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPayTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPayTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPayTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPayTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.colPayTime, "colPayTime");
            this.colPayTime.FieldName = "PayTime";
            this.colPayTime.Name = "colPayTime";
            // 
            // colTraveled
            // 
            this.colTraveled.AppearanceHeader.Options.UseTextOptions = true;
            this.colTraveled.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTraveled.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTraveled.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.colTraveled, "colTraveled");
            this.colTraveled.FieldName = "Traveled";
            this.colTraveled.Name = "colTraveled";
            // 
            // colTraveledSender
            // 
            this.colTraveledSender.AppearanceCell.Options.UseTextOptions = true;
            this.colTraveledSender.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTraveledSender.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTraveledSender.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTraveledSender.AppearanceHeader.Options.UseTextOptions = true;
            this.colTraveledSender.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTraveledSender.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTraveledSender.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.colTraveledSender, "colTraveledSender");
            this.colTraveledSender.FieldName = "TraveledSender";
            this.colTraveledSender.Name = "colTraveledSender";
            // 
            // colTotalPay
            // 
            this.colTotalPay.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalPay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalPay.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalPay.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalPay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalPay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.colTotalPay, "colTotalPay");
            this.colTotalPay.FieldName = "TotalPay";
            this.colTotalPay.Name = "colTotalPay";
            // 
            // CardCustomer
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.ExportOptions.Csv.EncodingType = ((DevExpress.XtraPrinting.EncodingType)(resources.GetObject("CardCustomer.ExportOptions.Csv.EncodingType")));
            this.ExportOptions.Html.CharacterSet = resources.GetString("CardCustomer.ExportOptions.Html.CharacterSet");
            this.ExportOptions.Html.Title = resources.GetString("CardCustomer.ExportOptions.Html.Title");
            this.ExportOptions.Mht.CharacterSet = resources.GetString("CardCustomer.ExportOptions.Mht.CharacterSet");
            this.ExportOptions.Mht.Title = resources.GetString("CardCustomer.ExportOptions.Mht.Title");
            this.ExportOptions.Pdf.DocumentOptions.Application = resources.GetString("CardCustomer.ExportOptions.Pdf.DocumentOptions.Application");
            this.ExportOptions.Pdf.DocumentOptions.Author = resources.GetString("CardCustomer.ExportOptions.Pdf.DocumentOptions.Author");
            this.ExportOptions.Pdf.DocumentOptions.Keywords = resources.GetString("CardCustomer.ExportOptions.Pdf.DocumentOptions.Keywords");
            this.ExportOptions.Pdf.DocumentOptions.Subject = resources.GetString("CardCustomer.ExportOptions.Pdf.DocumentOptions.Subject");
            this.ExportOptions.Pdf.DocumentOptions.Title = resources.GetString("CardCustomer.ExportOptions.Pdf.DocumentOptions.Title");
            this.ExportOptions.Text.EncodingType = ((DevExpress.XtraPrinting.EncodingType)(resources.GetObject("CardCustomer.ExportOptions.Text.EncodingType")));
            this.ExportOptions.Xls.SheetName = resources.GetString("CardCustomer.ExportOptions.Xls.SheetName");
            this.ExportOptions.Xlsx.SheetName = resources.GetString("CardCustomer.ExportOptions.Xlsx.SheetName");
            this.GridSize = new System.Drawing.Size(2, 5);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            resources.ApplyResources(this, "$this");
            ((System.ComponentModel.ISupportInitialize)(this.gControlInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer6;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer8;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer9;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer10;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer11;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer14;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer15;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer16;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer17;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer18;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer19;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer20;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer21;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer22;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer23;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer24;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer25;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer26;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer27;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer28;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer29;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer30;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer31;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer32;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer33;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer34;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer35;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer36;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer37;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer38;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer39;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer40;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer41;
        private DevExpress.XtraGrid.GridControl gControlInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimateValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPayTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTraveled;
        private DevExpress.XtraGrid.Columns.GridColumn colTraveledSender;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPay;
    }
}
