﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;

namespace MotorDepot.DifferentSheets
{
    class WayReportPageTwo : XtraReport
    {
        private DetailBand Detail;
        private PageHeaderBand PageHeader;
        private PageFooterBand PageFooter;
        private DevExpress.XtraPrinting.BorderSide visibleBorders;
        private string dFont;

        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private XRLabel xrLabel10;
        private XRLabel xrLabel11;
        private XRLabel xrLabel12;
        private XRLabel xrLabel13;
        private XRLabel xrLabel14;

        private XRTable xrTable1;
        private XRTable xrTable2;

        public WayReportPageTwo()
        {
            visibleBorders = DevExpress.XtraPrinting.BorderSide.None;
            dFont = "Times New Roman";

            this.Detail = new DetailBand();
            this.PageHeader = new PageHeaderBand();
            this.PageFooter = new PageFooterBand();

            Landscape = true;
            PaperKind = System.Drawing.Printing.PaperKind.A4;
            Margins.Bottom = 20;
            Margins.Top = 20;
            this.PageFooter.Height = 20;
            this.PageHeader.Height = 20;
            this.Bands.AddRange( new Band[] { this.Detail, this.PageHeader, this.PageFooter } );

            this.xrLabel1 = new XRLabel();
            this.xrLabel1.Location = new Point( 2, 2 );
            this.xrLabel1.Size = new Size( 967, 10 );
            this.xrLabel1.WordWrap = false;
            this.xrLabel1.Multiline = true;
            this.xrLabel1.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel1.Text = "ПОСЛІДОВНІСТЬ ВИКОНАННЯ ЗАВДАННЯ";
            this.xrLabel1.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel2 = new XRLabel();
            this.xrLabel2.Location = new Point( 100, 205 );
            this.xrLabel2.Size = new Size( 105, 10 );
            this.xrLabel2.WordWrap = false;
            this.xrLabel2.Multiline = true;
            this.xrLabel2.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel2.Text = "ТТН у кількості:";
            this.xrLabel2.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel2.Borders = visibleBorders;

            this.xrLabel3 = new XRLabel();
            this.xrLabel3.Location = new Point( 210, 205 );
            this.xrLabel3.Size = new Size( 80, 10 );
            this.xrLabel3.WordWrap = false;
            this.xrLabel3.Multiline = true;
            this.xrLabel3.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel3.Text = "          ";
            this.xrLabel3.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel4 = new XRLabel();
            this.xrLabel4.Location = new Point( 300, 205 );
            this.xrLabel4.Size = new Size( 200, 10 );
            this.xrLabel4.WordWrap = false;
            this.xrLabel4.Multiline = true;
            this.xrLabel4.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel4.Text = "______________шт. (прописом)";
            this.xrLabel4.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel4.Borders = visibleBorders;

            this.xrLabel5 = new XRLabel();
            this.xrLabel5.Location = new Point( 550, 205 );
            this.xrLabel5.Size = new Size( 160, 10 );
            this.xrLabel5.WordWrap = false;
            this.xrLabel5.Multiline = true;
            this.xrLabel5.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel5.Text = "Здав водій____________";
            this.xrLabel5.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel5.Borders = visibleBorders;

            this.xrLabel6 = new XRLabel();
            this.xrLabel6.Location = new Point( 750, 205 );
            this.xrLabel6.Size = new Size( 210, 10 );
            this.xrLabel6.WordWrap = false;
            this.xrLabel6.Multiline = true;
            this.xrLabel6.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel6.Text = "Прийняв диспетчер___________";
            this.xrLabel6.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel6.Borders = visibleBorders;

            this.xrLabel7 = new XRLabel();
            this.xrLabel7.Location = new Point( 2, 230 );
            this.xrLabel7.Size = new Size( 967, 95 );
            this.xrLabel7.WordWrap = false;
            this.xrLabel7.Multiline = true;
            this.xrLabel7.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel7.Text = "ТАКСУВАННЯ___________________________________________________________________________________________________________\n_____________________________________________________________________________________________________________________\n_____________________________________________________________________________________________________________________\n_____________________________________________________________________________________________________________________";
            this.xrLabel7.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel7.Borders = visibleBorders;

            this.xrLabel8 = new XRLabel();
            this.xrLabel8.Location = new Point( 2, 325 );
            this.xrLabel8.Size = new Size( 967, 10 );
            this.xrLabel8.WordWrap = false;
            this.xrLabel8.Multiline = true;
            this.xrLabel8.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel8.Text = "РЕЗУЛЬТАТИ РОБОТИ АВТОМОБІЛЯ І ПРИЧЕПІВ";
            this.xrLabel8.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel9 = new XRLabel();
            this.xrLabel9.Location = new Point( 20, 465 );
            this.xrLabel9.Size = new Size( 140, 10 );
            this.xrLabel9.WordWrap = false;
            this.xrLabel9.Multiline = true;
            this.xrLabel9.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel9.Text = "Коди марок автомобіля:";
            this.xrLabel9.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel9.Borders = visibleBorders;

            this.xrLabel10 = new XRLabel();
            this.xrLabel10.Location = new Point( 170, 465 );
            this.xrLabel10.Size = new Size( 100, 10 );
            this.xrLabel10.WordWrap = false;
            this.xrLabel10.Multiline = true;
            this.xrLabel10.TextAlignment = TextAlignment.BottomCenter;
            this.xrLabel10.Text = "     ";
            this.xrLabel10.Font = new Font( dFont, 16, FontStyle.Regular );
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel11 = new XRLabel();
            this.xrLabel11.Location = new Point( 285, 465 );
            this.xrLabel11.Size = new Size( 60, 10 );
            this.xrLabel11.WordWrap = false;
            this.xrLabel11.Multiline = true;
            this.xrLabel11.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel11.Text = "причепів";
            this.xrLabel11.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel11.Borders = visibleBorders;

            this.xrLabel12 = new XRLabel();
            this.xrLabel12.Location = new Point( 350, 465 );
            this.xrLabel12.Size = new Size( 100, 10 );
            this.xrLabel12.WordWrap = false;
            this.xrLabel12.Multiline = true;
            this.xrLabel12.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel12.Text = "     ";
            this.xrLabel12.Font = new Font( dFont, 16, FontStyle.Regular );
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel13 = new XRLabel();
            this.xrLabel13.Location = new Point( 500, 465 );
            this.xrLabel13.Size = new Size( 140, 10 );
            this.xrLabel13.WordWrap = false;
            this.xrLabel13.Multiline = true;
            this.xrLabel13.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel13.Text = "Автомобіле-дні у роботі";
            this.xrLabel13.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel13.Borders = visibleBorders;

            this.xrLabel14 = new XRLabel();
            this.xrLabel14.Location = new Point( 645, 465 );
            this.xrLabel14.Size = new Size( 100, 10 );
            this.xrLabel14.WordWrap = false;
            this.xrLabel14.Multiline = true;
            this.xrLabel14.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel14.Text = "     ";
            this.xrLabel14.Font = new Font( dFont, 16, FontStyle.Regular );
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrTable1 = new XRTable();
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable1.Location = new Point( 2, 12 );
            this.xrTable1.Size = new Size( 967, 180 );
            this.xrTable1.BeginInit();

            for ( int i = 0; i < 8; i++ )
            {
                XRTableRow row = new XRTableRow();
                for ( int j = 0; j < 8; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable1.Rows.Add( row );
            } // for

            this.xrTable1.EndInit();
            this.xrTable1.Rows[0].Cells[0].Text = "№№ поїздки";
            this.xrTable1.Rows[1].Cells[0].Text = "24";
            this.xrTable1.Rows[0].Cells[1].Text = "номери прикладених товаротранспортних\nнакладних і талонів замовника";
            this.xrTable1.Rows[1].Cells[1].Text = "25";
            this.xrTable1.Rows[0].Cells[2].Text = "відпрацьовано,\nгод., хв.";
            this.xrTable1.Rows[1].Cells[2].Text = "26";
            this.xrTable1.Rows[0].Cells[3].Text = "перевезено,\nтонн";
            this.xrTable1.Rows[1].Cells[3].Text = "27";
            this.xrTable1.Rows[0].Cells[4].Text = "виконано,\nткм";
            this.xrTable1.Rows[1].Cells[4].Text = "28";
            this.xrTable1.Rows[0].Cells[5].Text = "підпис та печатка\nвантажовідпраника";
            this.xrTable1.Rows[1].Cells[5].Text = "29";
            this.xrTable1.Rows[0].Cells[6].Text = "маршрут руху (заповнюється\nзамовником), звідки";
            this.xrTable1.Rows[1].Cells[6].Text = "30";
            this.xrTable1.Rows[0].Cells[7].Text = "маршрут руху (заповнюється\nзамовником), куди";
            this.xrTable1.Rows[1].Cells[7].Text = "31";

            this.xrTable2 = new XRTable();
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable2.Location = new Point( 2, 335 );
            this.xrTable2.Size = new Size( 967, 120 );
            this.xrTable2.BeginInit();

            for ( int i = 0; i < 3; i++ )
            {
                XRTableRow row = new XRTableRow();
                for ( int j = 0; j < 18; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 8, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable2.Rows.Add( row );
            } // for

            this.xrTable2.EndInit();
            this.xrTable2.Rows[0].Cells[0].Text = "витрата пального\n(літрів) за нормою";
            this.xrTable2.Rows[1].Cells[0].Text = "32";
            this.xrTable2.Rows[0].Cells[1].Text = "витрата пального\n(літрів) фактично";
            this.xrTable2.Rows[1].Cells[1].Text = "33";
            this.xrTable2.Rows[0].Cells[2].Text = "час в наряді, год. хв.\nвсього\nавтомобіля";
            this.xrTable2.Rows[1].Cells[2].Text = "34";
            this.xrTable2.Rows[0].Cells[3].Text = "час в наряді, год. хв.\nвсього\nпричепа";
            this.xrTable2.Rows[1].Cells[3].Text = "35";
            this.xrTable2.Rows[0].Cells[4].Text = "час в наряді, год. хв.\nв т.ч.автомобіля\nу русі";
            this.xrTable2.Rows[1].Cells[4].Text = "36";
            this.xrTable2.Rows[0].Cells[5].Text = "час в наряді, год. хв.\nв т.ч.автомобіля\nу простої на лінії";
            this.xrTable2.Rows[1].Cells[5].Text = "37";
            this.xrTable2.Rows[0].Cells[6].Text = "час в наряді, год. хв.\nв т.ч.автомобіля\nу простої\nпо тех. неспр.";
            this.xrTable2.Rows[1].Cells[6].Text = "38";
            this.xrTable2.Rows[0].Cells[7].Text = "кільк. поїздок\nз вантажем";
            this.xrTable2.Rows[1].Cells[7].Text = "39";
            this.xrTable2.Rows[0].Cells[8].Text = "пробіг, км\nзагальний\nавтомобіля";
            this.xrTable2.Rows[1].Cells[8].Text = "40";
            this.xrTable2.Rows[0].Cells[9].Text = "пробіг, км\nзагальний\nпричепа";
            this.xrTable2.Rows[1].Cells[9].Text = "41";
            this.xrTable2.Rows[0].Cells[10].Text = "пробіг, км\nв т.ч. з вантажем\nавтомобіля";
            this.xrTable2.Rows[1].Cells[10].Text = "42";
            this.xrTable2.Rows[0].Cells[11].Text = "пробіг, км\nв т.ч. з вантажем\nпричепа";
            this.xrTable2.Rows[1].Cells[11].Text = "43";
            this.xrTable2.Rows[0].Cells[12].Text = "перевезено,\nтонн всього";
            this.xrTable2.Rows[1].Cells[12].Text = "44";
            this.xrTable2.Rows[0].Cells[13].Text = "перевезено,\nтонн в т.ч. в\nпричепах";
            this.xrTable2.Rows[1].Cells[13].Text = "45";
            this.xrTable2.Rows[0].Cells[14].Text = "виконано, ткм\nвсього";
            this.xrTable2.Rows[1].Cells[14].Text = "46";
            this.xrTable2.Rows[0].Cells[15].Text = "виконано, ткм\n в т.ч. в причепах";
            this.xrTable2.Rows[1].Cells[15].Text = "47";
            this.xrTable2.Rows[0].Cells[16].Text = "зарплата\nкод";
            this.xrTable2.Rows[1].Cells[16].Text = "48";
            this.xrTable2.Rows[0].Cells[17].Text = "зарплата\nсума";
            this.xrTable2.Rows[1].Cells[17].Text = "49";

            this.Detail.Controls.Add( this.xrLabel1 );
            this.Detail.Controls.Add( this.xrTable1 );
            this.Detail.Controls.Add( this.xrLabel2 );
            this.Detail.Controls.Add( this.xrLabel3 );
            this.Detail.Controls.Add( this.xrLabel4 );
            this.Detail.Controls.Add( this.xrLabel5 );
            this.Detail.Controls.Add( this.xrLabel6 );
            this.Detail.Controls.Add( this.xrLabel7 );
            this.Detail.Controls.Add( this.xrLabel8 );
            this.Detail.Controls.Add( this.xrTable2 );
            this.Detail.Controls.Add( this.xrLabel9 );
            this.Detail.Controls.Add( this.xrLabel10 );
            this.Detail.Controls.Add( this.xrLabel11 );
            this.Detail.Controls.Add( this.xrLabel12 );
            this.Detail.Controls.Add( this.xrLabel13 );
            this.Detail.Controls.Add( this.xrLabel14 );
        } // WayReportPageTwo
    } // WayReportPageTwo
} // MotorDeport.DifferentSheets
