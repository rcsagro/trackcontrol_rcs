﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;

namespace MotorDepot.DifferentSheets
{
    class CustomerTicket : XtraReport
    {
        private DetailBand Detail;
        private PageHeaderBand PageHeader;
        private PageFooterBand PageFooter;

        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;
        private XRLabel xrLabel6;
        private XRTable xrTable1;
        private XRTable xrTable5;
        private XRTable xrTable6;
        private XRLabel xrLabel7;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private XRLabel xrLabel10;
        private XRLabel xrLabel11;
        private XRLabel xrLabel12;
        private XRTable xrTable2;
        private XRTable xrTable3;
        private XRTable xrTable4;
        private XRLabel xrLabel13;
        private XRLabel xrLabel14;
        private XRLabel xrLabel15;

        private DevExpress.XtraPrinting.BorderSide visibleBorders;
        private string dFont;

        private string tickets_customer = "ТАЛОН ЗАМОВНИКА_____________________№________\nДо подорожнього листа №________________від________р.\nАвтопідприємство__________________________________\nАвтомобіль_________________________________________\nПричеп___________________________________________\nЗамовник__________________________________________";
        private string text_boxes = "Типова форма N 2-TH\nЗатверджена наказом Мінтрансу,\nМінстату України\n29.12.95р. № 488/346";

        public CustomerTicket()
        {
            visibleBorders = DevExpress.XtraPrinting.BorderSide.None;
            dFont = "Times New Roman";

            this.Detail = new DetailBand();
            this.PageHeader = new PageHeaderBand();
            this.PageFooter = new PageFooterBand();

            Landscape = true;
            PaperKind = System.Drawing.Printing.PaperKind.A4;
            Margins.Bottom = 20;
            Margins.Top = 20;
            this.PageFooter.Height = 20;
            this.PageHeader.Height = 20;
            this.Bands.AddRange( new Band[] { this.Detail, this.PageHeader, this.PageFooter } );

            this.xrLabel1 = new XRLabel();
            this.xrLabel1.Location = new Point( 142, 0 );
            this.xrLabel1.Size = new Size( 178, 40 );
            this.xrLabel1.WordWrap = false;
            this.xrLabel1.Multiline = true;
            this.xrLabel1.TextAlignment = TextAlignment.TopRight;
            this.xrLabel1.Text = text_boxes;
            this.xrLabel1.Font = new Font( dFont, 8, FontStyle.Regular );
            this.xrLabel1.Borders = visibleBorders;

            this.xrLabel2 = new XRLabel();
            this.xrLabel2.Location = new Point( 462, 0 );
            this.xrLabel2.Size = new Size( 178, 40 );
            this.xrLabel2.WordWrap = false;
            this.xrLabel2.Multiline = true;
            this.xrLabel2.TextAlignment = TextAlignment.TopRight;
            this.xrLabel2.Text = text_boxes;
            this.xrLabel2.Font = new Font( dFont, 8, FontStyle.Regular );
            this.xrLabel2.Borders = visibleBorders;

            this.xrLabel3 = new XRLabel();
            this.xrLabel3.Location = new Point( 790, 0 );
            this.xrLabel3.Size = new Size( 175, 40 );
            this.xrLabel3.WordWrap = false;
            this.xrLabel3.Multiline = true;
            this.xrLabel3.TextAlignment = TextAlignment.TopRight;
            this.xrLabel3.Text = text_boxes;
            this.xrLabel3.Font = new Font( dFont, 8, FontStyle.Regular );
            this.xrLabel3.Borders = visibleBorders;

            this.xrLabel4 = new XRLabel();
            this.xrLabel4.Location = new Point( 0, 50 );
            this.xrLabel4.Size = new Size( 320, 40 );
            this.xrLabel4.WordWrap = false;
            this.xrLabel4.Multiline = true;
            this.xrLabel4.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel4.Text = tickets_customer;
            this.xrLabel4.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel4.Borders = visibleBorders;

            this.xrLabel5 = new XRLabel();
            this.xrLabel5.Location = new Point( 323, 50 );
            this.xrLabel5.Size = new Size( 320, 40 );
            this.xrLabel5.WordWrap = false;
            this.xrLabel5.Multiline = true;
            this.xrLabel5.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel5.Text = tickets_customer;
            this.xrLabel5.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel5.Borders = visibleBorders;

            this.xrLabel6 = new XRLabel();
            this.xrLabel6.Location = new Point( 645, 50 );
            this.xrLabel6.Size = new Size( 320, 40 );
            this.xrLabel6.WordWrap = false;
            this.xrLabel6.Multiline = true;
            this.xrLabel6.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel6.Text = tickets_customer;
            this.xrLabel6.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel6.Borders = visibleBorders;

            this.xrTable1 = new XRTable();
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable1.Location = new Point( 0, 100 );
            this.xrTable1.Size = new Size( 320, 200 );
            this.xrTable1.BeginInit();

            for ( int i = 0; i < 8; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 4; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleLeft;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable1.Rows.Add( row );
            }

            this.xrTable1.EndInit();
            this.xrTable1.Rows[0].Cells[0].Text = "Час\n(год., хв)";
            this.xrTable1.Rows[0].Cells[1].Text = "при\nприбутті";
            this.xrTable1.Rows[0].Cells[2].Text = "       \n       ";
            this.xrTable1.Rows[0].Cells[3].Text = "     ";
            this.xrTable1.Rows[1].Cells[0].Text = "        \n         ";
            this.xrTable1.Rows[1].Cells[1].Text = "при\nвибутті";
            this.xrTable1.Rows[2].Cells[0].Text = "показання\nспідометра";
            this.xrTable1.Rows[2].Cells[1].Text = "при\nприбутті";
            this.xrTable1.Rows[3].Cells[1].Text = "при\nвибутті";
            this.xrTable1.Rows[4].Cells[0].Text = "Прикладені";
            this.xrTable1.Rows[4].Cells[0].Borders = BorderSide.Bottom | BorderSide.Left | BorderSide.Top;
            this.xrTable1.Rows[4].Cells[1].Text = "ТТН";
            this.xrTable1.Rows[4].Cells[1].Borders = BorderSide.Bottom | BorderSide.Right | BorderSide.Top;
            this.xrTable1.Rows[4].Cells[2].Text = "кількість\nТТН";
            this.xrTable1.Rows[5].Cells[2].Text = "кількість\nпоїздок";
            this.xrTable1.Rows[7].Cells[3].Borders = BorderSide.None;
            this.xrTable1.Rows[7].Cells[2].Borders = BorderSide.None;
            this.xrTable1.Rows[6].Cells[3].Borders = BorderSide.Top;
            this.xrTable1.Rows[6].Cells[2].Borders = BorderSide.None;

            this.xrTable5 = new XRTable();
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable5.Location = new Point( 325, 100 );
            this.xrTable5.Size = new Size( 320, 200 );
            this.xrTable5.BeginInit();

            for ( int i = 0; i < 8; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 4; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleLeft;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable5.Rows.Add( row );
            }

            this.xrTable5.EndInit();
            this.xrTable5.Rows[0].Cells[0].Text = "Час\n(год., хв)";
            this.xrTable5.Rows[0].Cells[1].Text = "при\nприбутті";
            this.xrTable5.Rows[0].Cells[2].Text = "       \n       ";
            this.xrTable5.Rows[0].Cells[3].Text = "     ";
            this.xrTable5.Rows[1].Cells[0].Text = "        \n         ";
            this.xrTable5.Rows[1].Cells[1].Text = "при\nвибутті";
            this.xrTable5.Rows[2].Cells[0].Text = "показання\nспідометра";
            this.xrTable5.Rows[2].Cells[1].Text = "при\nприбутті";
            this.xrTable5.Rows[3].Cells[1].Text = "при\nвибутті";
            this.xrTable5.Rows[4].Cells[0].Text = "Прикладені";
            this.xrTable5.Rows[4].Cells[0].Borders = BorderSide.Bottom | BorderSide.Left | BorderSide.Top;
            this.xrTable5.Rows[4].Cells[1].Text = "ТТН";
            this.xrTable5.Rows[4].Cells[1].Borders = BorderSide.Bottom | BorderSide.Right | BorderSide.Top;
            this.xrTable5.Rows[4].Cells[2].Text = "кількість\nТТН";
            this.xrTable5.Rows[5].Cells[2].Text = "кількість\nпоїздок";
            this.xrTable5.Rows[7].Cells[3].Borders = BorderSide.None;
            this.xrTable5.Rows[7].Cells[2].Borders = BorderSide.None;
            this.xrTable5.Rows[6].Cells[3].Borders = BorderSide.Top;
            this.xrTable5.Rows[6].Cells[2].Borders = BorderSide.None;

            this.xrTable6 = new XRTable();
            this.xrTable6.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable6.Location = new Point( 647, 100 );
            this.xrTable6.Size = new Size( 320, 200 );
            this.xrTable6.BeginInit();

            for ( int i = 0; i < 8; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 4; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleLeft;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable6.Rows.Add( row );
            }

            this.xrTable6.EndInit();
            this.xrTable6.Rows[0].Cells[0].Text = "Час\n(год., хв)";
            this.xrTable6.Rows[0].Cells[1].Text = "при\nприбутті";
            this.xrTable6.Rows[0].Cells[2].Text = "       \n       ";
            this.xrTable6.Rows[0].Cells[3].Text = "     ";
            this.xrTable6.Rows[1].Cells[0].Text = "        \n         ";
            this.xrTable6.Rows[1].Cells[1].Text = "при\nвибутті";
            this.xrTable6.Rows[2].Cells[0].Text = "показання\nспідометра";
            this.xrTable6.Rows[2].Cells[1].Text = "при\nприбутті";
            this.xrTable6.Rows[3].Cells[1].Text = "при\nвибутті";
            this.xrTable6.Rows[4].Cells[0].Text = "Прикладені";
            this.xrTable6.Rows[4].Cells[0].Borders = BorderSide.Bottom | BorderSide.Left | BorderSide.Top;
            this.xrTable6.Rows[4].Cells[1].Text = "ТТН";
            this.xrTable6.Rows[4].Cells[1].Borders = BorderSide.Bottom | BorderSide.Right | BorderSide.Top;
            this.xrTable6.Rows[4].Cells[2].Text = "кількість\nТТН";
            this.xrTable6.Rows[5].Cells[2].Text = "кількість\nпоїздок";
            this.xrTable6.Rows[7].Cells[3].Borders = BorderSide.None;
            this.xrTable6.Rows[7].Cells[2].Borders = BorderSide.None;
            this.xrTable6.Rows[6].Cells[3].Borders = BorderSide.Top;
            this.xrTable6.Rows[6].Cells[2].Borders = BorderSide.None;

            this.xrLabel7 = new XRLabel();
            this.xrLabel7.Location = new Point( 0, 300 );
            this.xrLabel7.Size = new Size( 320, 40 );
            this.xrLabel7.WordWrap = false;
            this.xrLabel7.Multiline = true;
            this.xrLabel7.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel7.Text = "Підпис та штамп\nзаявника";
            this.xrLabel7.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel7.Borders = visibleBorders;

            this.xrLabel8 = new XRLabel();
            this.xrLabel8.Location = new Point( 325, 300 );
            this.xrLabel8.Size = new Size( 320, 40 );
            this.xrLabel8.WordWrap = false;
            this.xrLabel8.Multiline = true;
            this.xrLabel8.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel8.Text = "Підпис та штамп\nзаявника";
            this.xrLabel8.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel8.Borders = visibleBorders;

            this.xrLabel9 = new XRLabel();
            this.xrLabel9.Location = new Point( 648, 300 );
            this.xrLabel9.Size = new Size( 320, 40 );
            this.xrLabel9.WordWrap = false;
            this.xrLabel9.Multiline = true;
            this.xrLabel9.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel9.Text = "Підпис та штамп\nзаявника";
            this.xrLabel9.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel9.Borders = visibleBorders;

            this.xrLabel10 = new XRLabel();
            this.xrLabel10.Location = new Point( 0, 340 );
            this.xrLabel10.Size = new Size( 320, 10 );
            this.xrLabel10.WordWrap = false;
            this.xrLabel10.Multiline = true;
            this.xrLabel10.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel10.Text = "(заповнюється автопідприємством)";
            this.xrLabel10.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.xrLabel11 = new XRLabel();
            this.xrLabel11.Location = new Point( 325, 340 );
            this.xrLabel11.Size = new Size( 320, 10 );
            this.xrLabel11.WordWrap = false;
            this.xrLabel11.Multiline = true;
            this.xrLabel11.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel11.Text = "(заповнюється автопідприємством)";
            this.xrLabel11.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.xrLabel12 = new XRLabel();
            this.xrLabel12.Location = new Point( 648, 340 );
            this.xrLabel12.Size = new Size( 320, 10 );
            this.xrLabel12.WordWrap = false;
            this.xrLabel12.Multiline = true;
            this.xrLabel12.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel12.Text = "(заповнюється автопідприємством)";
            this.xrLabel12.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.xrTable2 = new XRTable();
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable2.Location = new Point( 0, 350 );
            this.xrTable2.Size = new Size( 320, 200 );
            this.xrTable2.BeginInit();

            for ( int i = 0; i < 6; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 5; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable2.Rows.Add( row );
            }

            this.xrTable2.EndInit();
            this.xrTable2.Rows[0].Cells[0].Text = "розрах.\nвартість";
            this.xrTable2.Rows[1].Cells[0].Text = "21";
            this.xrTable2.Rows[0].Cells[1].Text = "час\nоплач.";
            this.xrTable2.Rows[1].Cells[1].Text = "22";
            this.xrTable2.Rows[0].Cells[2].Text = "пробіг\nвсього";
            this.xrTable2.Rows[1].Cells[2].Text = "23";
            this.xrTable2.Rows[0].Cells[3].Text = "поїздки,\nвідправ-\nника";
            this.xrTable2.Rows[1].Cells[3].Text = "24";
            this.xrTable2.Rows[0].Cells[4].Text = "всього\nдо\nсплати";
            this.xrTable2.Rows[1].Cells[4].Text = "25";
            this.xrTable2.Rows[2].Cells[0].Text = "Одиниця\nвиміру";
            this.xrTable2.Rows[3].Cells[0].Text = "Виконано";
            this.xrTable2.Rows[4].Cells[0].Text = "Тариф";
            this.xrTable2.Rows[5].Cells[0].Text = "До\nсплати";
            this.xrTable2.Rows[2].Cells[1].Text = "год.\nхв.";
            this.xrTable2.Rows[2].Cells[2].Text = "км";
            this.xrTable2.Rows[2].Cells[4].Text = "грн.";

            this.xrTable3 = new XRTable();
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable3.Location = new Point( 325, 350 );
            this.xrTable3.Size = new Size( 320, 200 );
            this.xrTable3.BeginInit();

            for ( int i = 0; i < 6; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 5; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable3.Rows.Add( row );
            }

            this.xrTable3.EndInit();
            this.xrTable3.Rows[0].Cells[0].Text = "розрах.\nвартість";
            this.xrTable3.Rows[1].Cells[0].Text = "21";
            this.xrTable3.Rows[0].Cells[1].Text = "час\nоплач.";
            this.xrTable3.Rows[1].Cells[1].Text = "22";
            this.xrTable3.Rows[0].Cells[2].Text = "пробіг\nвсього";
            this.xrTable3.Rows[1].Cells[2].Text = "23";
            this.xrTable3.Rows[0].Cells[3].Text = "поїздки,\nвідправ-\nника";
            this.xrTable3.Rows[1].Cells[3].Text = "24";
            this.xrTable3.Rows[0].Cells[4].Text = "всього\nдо\nсплати";
            this.xrTable3.Rows[1].Cells[4].Text = "25";
            this.xrTable3.Rows[2].Cells[0].Text = "Одиниця\nвиміру";
            this.xrTable3.Rows[3].Cells[0].Text = "Виконано";
            this.xrTable3.Rows[4].Cells[0].Text = "Тариф";
            this.xrTable3.Rows[5].Cells[0].Text = "До\nсплати";
            this.xrTable3.Rows[2].Cells[1].Text = "год.\nхв.";
            this.xrTable3.Rows[2].Cells[2].Text = "км";
            this.xrTable3.Rows[2].Cells[4].Text = "грн.";

            this.xrTable4 = new XRTable();
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable4.Location = new Point( 648, 350 );
            this.xrTable4.Size = new Size( 320, 200 );
            this.xrTable4.BeginInit();

            for ( int i = 0; i < 6; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 5; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable4.Rows.Add( row );
            }

            this.xrTable4.EndInit();
            this.xrTable4.Rows[0].Cells[0].Text = "розрах.\nвартість";
            this.xrTable4.Rows[1].Cells[0].Text = "21";
            this.xrTable4.Rows[0].Cells[1].Text = "час\nоплач.";
            this.xrTable4.Rows[1].Cells[1].Text = "22";
            this.xrTable4.Rows[0].Cells[2].Text = "пробіг\nвсього";
            this.xrTable4.Rows[1].Cells[2].Text = "23";
            this.xrTable4.Rows[0].Cells[3].Text = "поїздки,\nвідправ-\nника";
            this.xrTable4.Rows[1].Cells[3].Text = "24";
            this.xrTable4.Rows[0].Cells[4].Text = "всього\nдо\nсплати";
            this.xrTable4.Rows[1].Cells[4].Text = "25";
            this.xrTable4.Rows[2].Cells[0].Text = "Одиниця\nвиміру";
            this.xrTable4.Rows[3].Cells[0].Text = "Виконано";
            this.xrTable4.Rows[4].Cells[0].Text = "Тариф";
            this.xrTable4.Rows[5].Cells[0].Text = "До\nсплати";
            this.xrTable4.Rows[2].Cells[1].Text = "год.\nхв.";
            this.xrTable4.Rows[2].Cells[2].Text = "км";
            this.xrTable4.Rows[2].Cells[4].Text = "грн.";

            this.xrLabel13 = new XRLabel();
            this.xrLabel13.Location = new Point( 0, 550 );
            this.xrLabel13.Size = new Size( 320, 10 );
            this.xrLabel13.WordWrap = false;
            this.xrLabel13.Multiline = true;
            this.xrLabel13.TextAlignment = TextAlignment.MiddleLeft;
            this.xrLabel13.Text = "Підпис таксувальника";
            this.xrLabel13.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.xrLabel14 = new XRLabel();
            this.xrLabel14.Location = new Point( 325, 550 );
            this.xrLabel14.Size = new Size( 320, 10 );
            this.xrLabel14.WordWrap = false;
            this.xrLabel14.Multiline = true;
            this.xrLabel14.TextAlignment = TextAlignment.MiddleLeft;
            this.xrLabel14.Text = "Підпис таксувальника";
            this.xrLabel14.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.xrLabel15 = new XRLabel();
            this.xrLabel15.Location = new Point( 648, 550 );
            this.xrLabel15.Size = new Size( 320, 10 );
            this.xrLabel15.WordWrap = false;
            this.xrLabel15.Multiline = true;
            this.xrLabel15.TextAlignment = TextAlignment.MiddleLeft;
            this.xrLabel15.Text = "Підпис таксувальника";
            this.xrLabel15.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.All; 

            this.Detail.Controls.Add( this.xrLabel1 );
            this.Detail.Controls.Add( this.xrLabel2 );
            this.Detail.Controls.Add( this.xrLabel3 );
            this.Detail.Controls.Add( this.xrLabel4 );
            this.Detail.Controls.Add( this.xrLabel5 );
            this.Detail.Controls.Add( this.xrLabel6 );
            this.Detail.Controls.Add( this.xrTable1 );
            this.Detail.Controls.Add( this.xrTable5 );
            this.Detail.Controls.Add( this.xrTable6 );
            this.Detail.Controls.Add( this.xrLabel7 );
            this.Detail.Controls.Add( this.xrLabel8 );
            this.Detail.Controls.Add( this.xrLabel9 );
            this.Detail.Controls.Add( this.xrLabel10 );
            this.Detail.Controls.Add( this.xrLabel11 );
            this.Detail.Controls.Add( this.xrLabel12 );
            this.Detail.Controls.Add( this.xrTable2 );
            this.Detail.Controls.Add( this.xrTable3 );
            this.Detail.Controls.Add( this.xrTable4 );
            this.Detail.Controls.Add( this.xrLabel13 );
            this.Detail.Controls.Add( this.xrLabel14 );
            this.Detail.Controls.Add( this.xrLabel15 );
        }  // CustomerTicket

        public void setDataForWayTicket( MotorDepot.waybill Row )
        {
            this.xrLabel4.Text = "ТАЛОН ЗАМОВНИКА_____________________№________\nДо подорожнього листа № " + 
                Row.Id + " від " + Row.Date.Day.ToString() + "." + Row.Date.Month.ToString() + "." 
                + Row.Date.Year.ToString() + " р.\nАвтопідприємство: " + 
                Row.md_enterprise.Name + "\nАвтомобіль: " + Row.vehicle.CarModel + "  " +  
                Row.vehicle.NumberPlate + "  " + Row.vehicle.MakeCar + 
                "\nПричеп___________________________________________\nЗамовник: " + 
                Row.md_enterprise.Name;

            this.xrLabel5.Text = "ТАЛОН ЗАМОВНИКА_____________________№________\nДо подорожнього листа № " + 
                Row.Id + " від " + Row.Date.Day.ToString() + "." + Row.Date.Month.ToString() + "." 
                + Row.Date.Year.ToString() + " р.\nАвтопідприємство: " + 
                Row.md_enterprise.Name + "\nАвтомобіль: " + Row.vehicle.CarModel + "  " +  
                Row.vehicle.NumberPlate + "  " + Row.vehicle.MakeCar + 
                "\nПричеп___________________________________________\nЗамовник: " + 
                Row.md_enterprise.Name;

            this.xrLabel6.Text = "ТАЛОН ЗАМОВНИКА_____________________№________\nДо подорожнього листа № " + 
                Row.Id + " від " + Row.Date.Day.ToString() + "." + Row.Date.Month.ToString() + "." 
                + Row.Date.Year.ToString() + " р.\nАвтопідприємство: " + 
                Row.md_enterprise.Name + "\nАвтомобіль: " + Row.vehicle.CarModel + "  " +  
                Row.vehicle.NumberPlate + "  " + Row.vehicle.MakeCar + 
                "\nПричеп___________________________________________\nЗамовник: " + 
                Row.md_enterprise.Name;
        } // setDataForWaySheet
    } // CustomerTicket
} // MotorDeport.DifferentSheets 
