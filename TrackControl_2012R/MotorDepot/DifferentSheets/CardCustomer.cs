using System;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using MotorDepot.Properties;

namespace MotorDepot.DifferentSheets
{
    public partial class CardCustomer : DevExpress.XtraScheduler.Reporting.XtraSchedulerReport
    {
        int countColumn = 0; // ������� �������� - ��������

        public CardCustomer()
        {
            InitializeComponent();
            InitializeTableReport();
        } // CardCustomer

        // ����������� ����� ������ �������
        protected void InitializeTableReport()
        {
            var dT = new DataTable();
            dT.Columns.AddRange( new[]
                {
                    new DataColumn("EstimateValue", typeof (string)),
                    new DataColumn("PayTime", typeof (string)),
                    new DataColumn("Traveled", typeof (string)),
                    new DataColumn("TraveledSender", typeof (string)),
                    new DataColumn("TotalPay", typeof (string)),
                } );

            for ( var i = 0 ; i < 3; i++ )
            {
                var dr = dT.NewRow();
                dT.Rows.Add( dr );
            }

            DataRow row = dT.Rows[0];
	        for ( int i = 0; i < dT.Columns.Count; i++ )
            {
                row[i] = countColumn++;
            }

	        row = dT.Rows[1];
	        row[0] = "������� �����";
	        row[1] = "���., ��.";
	        row[2] = "��";
	        row[4] = "���.";

	        row = dT.Rows[2];
	        row[0] = "��������";
	        row[4] = "x";

	        row = dT.Rows[3];
	        row[0] = "�����";
	        row[4] = "x";

	        row = dT.Rows[4];
	        row[0] = "�� ������";
	    
            gControlInfo.DataSource = dT;
        } // InitializeTableReport
    } // CardCustomer
} // MotorDepot.DifferentSheets
