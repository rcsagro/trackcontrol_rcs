namespace MotorDepot
{
    partial class WaySheet1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( WaySheet1 ) );
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colFuelMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelDate = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer6 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer7 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer8 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer9 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer10 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer11 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer12 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer13 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer14 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer15 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer16 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer17 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer18 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer19 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.winControlContainer20 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.textGarageNumber = new DevExpress.XtraEditors.TextEdit();
            this.winControlContainer21 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.textTabelDriver = new DevExpress.XtraEditors.TextEdit();
            this.winControlContainer22 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.winControlContainer23 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.winControlContainer24 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer25 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer26 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer27 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer32 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer58 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer37 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer70 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer71 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer72 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer73 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer74 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer75 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer76 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer77 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer78 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer79 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer80 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer81 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer82 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer83 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer84 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer85 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer112 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer115 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gControlTaskDriver = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeIn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounterTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTakeWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberTravelWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.winControlContainer86 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gControlFuelTravel = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodeMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceIn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeSpecific = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMotor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gControlWorkDriverAuto = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOperation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMinute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer28 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer29 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer30 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.colControlSignature = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFueler = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMechanic1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMechanic2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDispatcher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.winControlContainer31 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer33 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer34 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer35 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelNumberSheet = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer36 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelModelAuto = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer38 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelRegNumber = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer39 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelTypeAuto = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer40 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelFIO = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer41 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelNumService = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer42 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelClassDriver = new DevExpress.XtraEditors.LabelControl();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit1.Properties ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textGarageNumber.Properties ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textTabelDriver.Properties ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit4.Properties ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit5.Properties ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlTaskDriver ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlFuelTravel ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlWorkDriverAuto ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView3 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.colControlSignature ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView4 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this ) ).BeginInit();
            // 
            // colFuelMark
            // 
            this.colFuelMark.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelMark.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMark.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMark.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelMark.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelMark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMark.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colFuelMark, "colFuelMark" );
            this.colFuelMark.FieldName = "FuelMark";
            this.colFuelMark.Name = "colFuelMark";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange( new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer3,
            this.winControlContainer4,
            this.winControlContainer5,
            this.winControlContainer6,
            this.winControlContainer7,
            this.winControlContainer8,
            this.winControlContainer9,
            this.winControlContainer10,
            this.winControlContainer11,
            this.winControlContainer12,
            this.winControlContainer13,
            this.winControlContainer14,
            this.winControlContainer15,
            this.winControlContainer16,
            this.winControlContainer17,
            this.winControlContainer18,
            this.winControlContainer19,
            this.winControlContainer20,
            this.winControlContainer21,
            this.winControlContainer22,
            this.winControlContainer23,
            this.winControlContainer24,
            this.winControlContainer25,
            this.winControlContainer26,
            this.winControlContainer27,
            this.winControlContainer32,
            this.winControlContainer58,
            this.winControlContainer37,
            this.winControlContainer70,
            this.winControlContainer71,
            this.winControlContainer72,
            this.winControlContainer73,
            this.winControlContainer74,
            this.winControlContainer75,
            this.winControlContainer76,
            this.winControlContainer77,
            this.winControlContainer78,
            this.winControlContainer79,
            this.winControlContainer80,
            this.winControlContainer81,
            this.winControlContainer82,
            this.winControlContainer83,
            this.winControlContainer84,
            this.winControlContainer85,
            this.winControlContainer112,
            this.winControlContainer115,
            this.winControlContainer86,
            this.winControlContainer1,
            this.winControlContainer2,
            this.winControlContainer28,
            this.winControlContainer29,
            this.winControlContainer30,
            this.winControlContainer31,
            this.winControlContainer33,
            this.winControlContainer34,
            this.winControlContainer35,
            this.winControlContainer36,
            this.winControlContainer38,
            this.winControlContainer39,
            this.winControlContainer40,
            this.winControlContainer41,
            this.winControlContainer42} );
            this.Detail.Height = 799;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo( 0, 0, 0, 0, 100F );
            resources.ApplyResources( this.Detail, "Detail" );
            // 
            // winControlContainer3
            // 
            resources.ApplyResources( this.winControlContainer3, "winControlContainer3" );
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.WinControl = this.labelControl3;
            // 
            // labelControl3
            // 
            this.labelControl3.AccessibleDescription = null;
            this.labelControl3.AccessibleName = null;
            resources.ApplyResources( this.labelControl3, "labelControl3" );
            this.labelControl3.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Name = "labelControl3";
            // 
            // winControlContainer4
            // 
            resources.ApplyResources( this.winControlContainer4, "winControlContainer4" );
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.WinControl = this.labelControl4;
            // 
            // labelControl4
            // 
            this.labelControl4.AccessibleDescription = null;
            this.labelControl4.AccessibleName = null;
            resources.ApplyResources( this.labelControl4, "labelControl4" );
            this.labelControl4.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Name = "labelControl4";
            // 
            // winControlContainer5
            // 
            resources.ApplyResources( this.winControlContainer5, "winControlContainer5" );
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.WinControl = this.labelDate;
            // 
            // labelDate
            // 
            this.labelDate.AccessibleDescription = null;
            this.labelDate.AccessibleName = null;
            resources.ApplyResources( this.labelDate, "labelDate" );
            this.labelDate.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Name = "labelDate";
            // 
            // winControlContainer6
            // 
            resources.ApplyResources( this.winControlContainer6, "winControlContainer6" );
            this.winControlContainer6.Name = "winControlContainer6";
            this.winControlContainer6.WinControl = this.labelControl6;
            // 
            // labelControl6
            // 
            this.labelControl6.AccessibleDescription = null;
            this.labelControl6.AccessibleName = null;
            resources.ApplyResources( this.labelControl6, "labelControl6" );
            this.labelControl6.Name = "labelControl6";
            // 
            // winControlContainer7
            // 
            resources.ApplyResources( this.winControlContainer7, "winControlContainer7" );
            this.winControlContainer7.Name = "winControlContainer7";
            this.winControlContainer7.WinControl = this.labelControl7;
            // 
            // labelControl7
            // 
            this.labelControl7.AccessibleDescription = null;
            this.labelControl7.AccessibleName = null;
            resources.ApplyResources( this.labelControl7, "labelControl7" );
            this.labelControl7.Name = "labelControl7";
            // 
            // winControlContainer8
            // 
            resources.ApplyResources( this.winControlContainer8, "winControlContainer8" );
            this.winControlContainer8.Name = "winControlContainer8";
            this.winControlContainer8.WinControl = this.labelControl8;
            // 
            // labelControl8
            // 
            this.labelControl8.AccessibleDescription = null;
            this.labelControl8.AccessibleName = null;
            resources.ApplyResources( this.labelControl8, "labelControl8" );
            this.labelControl8.Name = "labelControl8";
            // 
            // winControlContainer9
            // 
            resources.ApplyResources( this.winControlContainer9, "winControlContainer9" );
            this.winControlContainer9.Name = "winControlContainer9";
            this.winControlContainer9.WinControl = this.labelControl9;
            // 
            // labelControl9
            // 
            this.labelControl9.AccessibleDescription = null;
            this.labelControl9.AccessibleName = null;
            resources.ApplyResources( this.labelControl9, "labelControl9" );
            this.labelControl9.Name = "labelControl9";
            // 
            // winControlContainer10
            // 
            resources.ApplyResources( this.winControlContainer10, "winControlContainer10" );
            this.winControlContainer10.Name = "winControlContainer10";
            this.winControlContainer10.WinControl = this.labelControl10;
            // 
            // labelControl10
            // 
            this.labelControl10.AccessibleDescription = null;
            this.labelControl10.AccessibleName = null;
            resources.ApplyResources( this.labelControl10, "labelControl10" );
            this.labelControl10.Name = "labelControl10";
            // 
            // winControlContainer11
            // 
            resources.ApplyResources( this.winControlContainer11, "winControlContainer11" );
            this.winControlContainer11.Name = "winControlContainer11";
            this.winControlContainer11.WinControl = this.labelControl11;
            // 
            // labelControl11
            // 
            this.labelControl11.AccessibleDescription = null;
            this.labelControl11.AccessibleName = null;
            resources.ApplyResources( this.labelControl11, "labelControl11" );
            this.labelControl11.Name = "labelControl11";
            // 
            // winControlContainer12
            // 
            resources.ApplyResources( this.winControlContainer12, "winControlContainer12" );
            this.winControlContainer12.Name = "winControlContainer12";
            this.winControlContainer12.WinControl = this.labelControl12;
            // 
            // labelControl12
            // 
            this.labelControl12.AccessibleDescription = null;
            this.labelControl12.AccessibleName = null;
            resources.ApplyResources( this.labelControl12, "labelControl12" );
            this.labelControl12.Name = "labelControl12";
            // 
            // winControlContainer13
            // 
            resources.ApplyResources( this.winControlContainer13, "winControlContainer13" );
            this.winControlContainer13.Name = "winControlContainer13";
            this.winControlContainer13.WinControl = this.labelControl13;
            // 
            // labelControl13
            // 
            this.labelControl13.AccessibleDescription = null;
            this.labelControl13.AccessibleName = null;
            resources.ApplyResources( this.labelControl13, "labelControl13" );
            this.labelControl13.Name = "labelControl13";
            // 
            // winControlContainer14
            // 
            resources.ApplyResources( this.winControlContainer14, "winControlContainer14" );
            this.winControlContainer14.Name = "winControlContainer14";
            this.winControlContainer14.WinControl = this.labelControl14;
            // 
            // labelControl14
            // 
            this.labelControl14.AccessibleDescription = null;
            this.labelControl14.AccessibleName = null;
            resources.ApplyResources( this.labelControl14, "labelControl14" );
            this.labelControl14.Name = "labelControl14";
            // 
            // winControlContainer15
            // 
            resources.ApplyResources( this.winControlContainer15, "winControlContainer15" );
            this.winControlContainer15.Name = "winControlContainer15";
            this.winControlContainer15.WinControl = this.labelControl15;
            // 
            // labelControl15
            // 
            this.labelControl15.AccessibleDescription = null;
            this.labelControl15.AccessibleName = null;
            resources.ApplyResources( this.labelControl15, "labelControl15" );
            this.labelControl15.Name = "labelControl15";
            // 
            // winControlContainer16
            // 
            resources.ApplyResources( this.winControlContainer16, "winControlContainer16" );
            this.winControlContainer16.Name = "winControlContainer16";
            this.winControlContainer16.WinControl = this.labelControl16;
            // 
            // labelControl16
            // 
            this.labelControl16.AccessibleDescription = null;
            this.labelControl16.AccessibleName = null;
            resources.ApplyResources( this.labelControl16, "labelControl16" );
            this.labelControl16.Name = "labelControl16";
            // 
            // winControlContainer17
            // 
            resources.ApplyResources( this.winControlContainer17, "winControlContainer17" );
            this.winControlContainer17.Name = "winControlContainer17";
            this.winControlContainer17.WinControl = this.labelControl17;
            // 
            // labelControl17
            // 
            this.labelControl17.AccessibleDescription = null;
            this.labelControl17.AccessibleName = null;
            resources.ApplyResources( this.labelControl17, "labelControl17" );
            this.labelControl17.Name = "labelControl17";
            // 
            // winControlContainer18
            // 
            resources.ApplyResources( this.winControlContainer18, "winControlContainer18" );
            this.winControlContainer18.Name = "winControlContainer18";
            this.winControlContainer18.WinControl = this.labelControl18;
            // 
            // labelControl18
            // 
            this.labelControl18.AccessibleDescription = null;
            this.labelControl18.AccessibleName = null;
            resources.ApplyResources( this.labelControl18, "labelControl18" );
            this.labelControl18.Name = "labelControl18";
            // 
            // winControlContainer19
            // 
            resources.ApplyResources( this.winControlContainer19, "winControlContainer19" );
            this.winControlContainer19.Name = "winControlContainer19";
            this.winControlContainer19.WinControl = this.textEdit1;
            // 
            // textEdit1
            // 
            resources.ApplyResources( this.textEdit1, "textEdit1" );
            this.textEdit1.BackgroundImage = null;
            this.textEdit1.EditValue = null;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.AccessibleDescription = null;
            this.textEdit1.Properties.AccessibleName = null;
            this.textEdit1.Properties.AutoHeight = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.AutoHeight" ) ) );
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Properties.Mask.AutoComplete = ( ( DevExpress.XtraEditors.Mask.AutoCompleteType ) ( resources.GetObject( "textEdit1.Properties.Mask.AutoComplete" ) ) );
            this.textEdit1.Properties.Mask.BeepOnError = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.Mask.BeepOnError" ) ) );
            this.textEdit1.Properties.Mask.EditMask = resources.GetString( "textEdit1.Properties.Mask.EditMask" );
            this.textEdit1.Properties.Mask.IgnoreMaskBlank = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.Mask.IgnoreMaskBlank" ) ) );
            this.textEdit1.Properties.Mask.MaskType = ( ( DevExpress.XtraEditors.Mask.MaskType ) ( resources.GetObject( "textEdit1.Properties.Mask.MaskType" ) ) );
            this.textEdit1.Properties.Mask.PlaceHolder = ( ( char ) ( resources.GetObject( "textEdit1.Properties.Mask.PlaceHolder" ) ) );
            this.textEdit1.Properties.Mask.SaveLiteral = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.Mask.SaveLiteral" ) ) );
            this.textEdit1.Properties.Mask.ShowPlaceHolders = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.Mask.ShowPlaceHolders" ) ) );
            this.textEdit1.Properties.Mask.UseMaskAsDisplayFormat = ( ( bool ) ( resources.GetObject( "textEdit1.Properties.Mask.UseMaskAsDisplayFormat" ) ) );
            this.textEdit1.Properties.NullValuePrompt = resources.GetString( "textEdit1.Properties.NullValuePrompt" );
            // 
            // winControlContainer20
            // 
            resources.ApplyResources( this.winControlContainer20, "winControlContainer20" );
            this.winControlContainer20.Name = "winControlContainer20";
            this.winControlContainer20.WinControl = this.textGarageNumber;
            // 
            // textGarageNumber
            // 
            resources.ApplyResources( this.textGarageNumber, "textGarageNumber" );
            this.textGarageNumber.BackgroundImage = null;
            this.textGarageNumber.EditValue = null;
            this.textGarageNumber.Name = "textGarageNumber";
            this.textGarageNumber.Properties.AccessibleDescription = null;
            this.textGarageNumber.Properties.AccessibleName = null;
            this.textGarageNumber.Properties.AutoHeight = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.AutoHeight" ) ) );
            this.textGarageNumber.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textGarageNumber.Properties.Mask.AutoComplete = ( ( DevExpress.XtraEditors.Mask.AutoCompleteType ) ( resources.GetObject( "textGarageNumber.Properties.Mask.AutoComplete" ) ) );
            this.textGarageNumber.Properties.Mask.BeepOnError = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.Mask.BeepOnError" ) ) );
            this.textGarageNumber.Properties.Mask.EditMask = resources.GetString( "textGarageNumber.Properties.Mask.EditMask" );
            this.textGarageNumber.Properties.Mask.IgnoreMaskBlank = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.Mask.IgnoreMaskBlank" ) ) );
            this.textGarageNumber.Properties.Mask.MaskType = ( ( DevExpress.XtraEditors.Mask.MaskType ) ( resources.GetObject( "textGarageNumber.Properties.Mask.MaskType" ) ) );
            this.textGarageNumber.Properties.Mask.PlaceHolder = ( ( char ) ( resources.GetObject( "textGarageNumber.Properties.Mask.PlaceHolder" ) ) );
            this.textGarageNumber.Properties.Mask.SaveLiteral = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.Mask.SaveLiteral" ) ) );
            this.textGarageNumber.Properties.Mask.ShowPlaceHolders = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.Mask.ShowPlaceHolders" ) ) );
            this.textGarageNumber.Properties.Mask.UseMaskAsDisplayFormat = ( ( bool ) ( resources.GetObject( "textGarageNumber.Properties.Mask.UseMaskAsDisplayFormat" ) ) );
            this.textGarageNumber.Properties.NullValuePrompt = resources.GetString( "textGarageNumber.Properties.NullValuePrompt" );
            // 
            // winControlContainer21
            // 
            resources.ApplyResources( this.winControlContainer21, "winControlContainer21" );
            this.winControlContainer21.Name = "winControlContainer21";
            this.winControlContainer21.WinControl = this.textTabelDriver;
            // 
            // textTabelDriver
            // 
            resources.ApplyResources( this.textTabelDriver, "textTabelDriver" );
            this.textTabelDriver.BackgroundImage = null;
            this.textTabelDriver.EditValue = null;
            this.textTabelDriver.Name = "textTabelDriver";
            this.textTabelDriver.Properties.AccessibleDescription = null;
            this.textTabelDriver.Properties.AccessibleName = null;
            this.textTabelDriver.Properties.AutoHeight = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.AutoHeight" ) ) );
            this.textTabelDriver.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textTabelDriver.Properties.Mask.AutoComplete = ( ( DevExpress.XtraEditors.Mask.AutoCompleteType ) ( resources.GetObject( "textTabelDriver.Properties.Mask.AutoComplete" ) ) );
            this.textTabelDriver.Properties.Mask.BeepOnError = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.Mask.BeepOnError" ) ) );
            this.textTabelDriver.Properties.Mask.EditMask = resources.GetString( "textTabelDriver.Properties.Mask.EditMask" );
            this.textTabelDriver.Properties.Mask.IgnoreMaskBlank = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.Mask.IgnoreMaskBlank" ) ) );
            this.textTabelDriver.Properties.Mask.MaskType = ( ( DevExpress.XtraEditors.Mask.MaskType ) ( resources.GetObject( "textTabelDriver.Properties.Mask.MaskType" ) ) );
            this.textTabelDriver.Properties.Mask.PlaceHolder = ( ( char ) ( resources.GetObject( "textTabelDriver.Properties.Mask.PlaceHolder" ) ) );
            this.textTabelDriver.Properties.Mask.SaveLiteral = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.Mask.SaveLiteral" ) ) );
            this.textTabelDriver.Properties.Mask.ShowPlaceHolders = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.Mask.ShowPlaceHolders" ) ) );
            this.textTabelDriver.Properties.Mask.UseMaskAsDisplayFormat = ( ( bool ) ( resources.GetObject( "textTabelDriver.Properties.Mask.UseMaskAsDisplayFormat" ) ) );
            this.textTabelDriver.Properties.NullValuePrompt = resources.GetString( "textTabelDriver.Properties.NullValuePrompt" );
            // 
            // winControlContainer22
            // 
            resources.ApplyResources( this.winControlContainer22, "winControlContainer22" );
            this.winControlContainer22.Name = "winControlContainer22";
            this.winControlContainer22.WinControl = this.textEdit4;
            // 
            // textEdit4
            // 
            resources.ApplyResources( this.textEdit4, "textEdit4" );
            this.textEdit4.BackgroundImage = null;
            this.textEdit4.EditValue = null;
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.AccessibleDescription = null;
            this.textEdit4.Properties.AccessibleName = null;
            this.textEdit4.Properties.AutoHeight = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.AutoHeight" ) ) );
            this.textEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit4.Properties.Mask.AutoComplete = ( ( DevExpress.XtraEditors.Mask.AutoCompleteType ) ( resources.GetObject( "textEdit4.Properties.Mask.AutoComplete" ) ) );
            this.textEdit4.Properties.Mask.BeepOnError = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.Mask.BeepOnError" ) ) );
            this.textEdit4.Properties.Mask.EditMask = resources.GetString( "textEdit4.Properties.Mask.EditMask" );
            this.textEdit4.Properties.Mask.IgnoreMaskBlank = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.Mask.IgnoreMaskBlank" ) ) );
            this.textEdit4.Properties.Mask.MaskType = ( ( DevExpress.XtraEditors.Mask.MaskType ) ( resources.GetObject( "textEdit4.Properties.Mask.MaskType" ) ) );
            this.textEdit4.Properties.Mask.PlaceHolder = ( ( char ) ( resources.GetObject( "textEdit4.Properties.Mask.PlaceHolder" ) ) );
            this.textEdit4.Properties.Mask.SaveLiteral = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.Mask.SaveLiteral" ) ) );
            this.textEdit4.Properties.Mask.ShowPlaceHolders = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.Mask.ShowPlaceHolders" ) ) );
            this.textEdit4.Properties.Mask.UseMaskAsDisplayFormat = ( ( bool ) ( resources.GetObject( "textEdit4.Properties.Mask.UseMaskAsDisplayFormat" ) ) );
            this.textEdit4.Properties.NullValuePrompt = resources.GetString( "textEdit4.Properties.NullValuePrompt" );
            // 
            // winControlContainer23
            // 
            resources.ApplyResources( this.winControlContainer23, "winControlContainer23" );
            this.winControlContainer23.Name = "winControlContainer23";
            this.winControlContainer23.WinControl = this.textEdit5;
            // 
            // textEdit5
            // 
            resources.ApplyResources( this.textEdit5, "textEdit5" );
            this.textEdit5.BackgroundImage = null;
            this.textEdit5.EditValue = null;
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.AccessibleDescription = null;
            this.textEdit5.Properties.AccessibleName = null;
            this.textEdit5.Properties.AutoHeight = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.AutoHeight" ) ) );
            this.textEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit5.Properties.Mask.AutoComplete = ( ( DevExpress.XtraEditors.Mask.AutoCompleteType ) ( resources.GetObject( "textEdit5.Properties.Mask.AutoComplete" ) ) );
            this.textEdit5.Properties.Mask.BeepOnError = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.Mask.BeepOnError" ) ) );
            this.textEdit5.Properties.Mask.EditMask = resources.GetString( "textEdit5.Properties.Mask.EditMask" );
            this.textEdit5.Properties.Mask.IgnoreMaskBlank = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.Mask.IgnoreMaskBlank" ) ) );
            this.textEdit5.Properties.Mask.MaskType = ( ( DevExpress.XtraEditors.Mask.MaskType ) ( resources.GetObject( "textEdit5.Properties.Mask.MaskType" ) ) );
            this.textEdit5.Properties.Mask.PlaceHolder = ( ( char ) ( resources.GetObject( "textEdit5.Properties.Mask.PlaceHolder" ) ) );
            this.textEdit5.Properties.Mask.SaveLiteral = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.Mask.SaveLiteral" ) ) );
            this.textEdit5.Properties.Mask.ShowPlaceHolders = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.Mask.ShowPlaceHolders" ) ) );
            this.textEdit5.Properties.Mask.UseMaskAsDisplayFormat = ( ( bool ) ( resources.GetObject( "textEdit5.Properties.Mask.UseMaskAsDisplayFormat" ) ) );
            this.textEdit5.Properties.NullValuePrompt = resources.GetString( "textEdit5.Properties.NullValuePrompt" );
            // 
            // winControlContainer24
            // 
            resources.ApplyResources( this.winControlContainer24, "winControlContainer24" );
            this.winControlContainer24.Name = "winControlContainer24";
            this.winControlContainer24.WinControl = this.labelControl19;
            // 
            // labelControl19
            // 
            this.labelControl19.AccessibleDescription = null;
            this.labelControl19.AccessibleName = null;
            resources.ApplyResources( this.labelControl19, "labelControl19" );
            this.labelControl19.Name = "labelControl19";
            // 
            // winControlContainer25
            // 
            resources.ApplyResources( this.winControlContainer25, "winControlContainer25" );
            this.winControlContainer25.Name = "winControlContainer25";
            this.winControlContainer25.WinControl = this.labelControl20;
            // 
            // labelControl20
            // 
            this.labelControl20.AccessibleDescription = null;
            this.labelControl20.AccessibleName = null;
            resources.ApplyResources( this.labelControl20, "labelControl20" );
            this.labelControl20.Name = "labelControl20";
            // 
            // winControlContainer26
            // 
            resources.ApplyResources( this.winControlContainer26, "winControlContainer26" );
            this.winControlContainer26.Name = "winControlContainer26";
            this.winControlContainer26.WinControl = this.labelControl21;
            // 
            // labelControl21
            // 
            this.labelControl21.AccessibleDescription = null;
            this.labelControl21.AccessibleName = null;
            resources.ApplyResources( this.labelControl21, "labelControl21" );
            this.labelControl21.Name = "labelControl21";
            // 
            // winControlContainer27
            // 
            resources.ApplyResources( this.winControlContainer27, "winControlContainer27" );
            this.winControlContainer27.Name = "winControlContainer27";
            this.winControlContainer27.WinControl = this.labelControl22;
            // 
            // labelControl22
            // 
            this.labelControl22.AccessibleDescription = null;
            this.labelControl22.AccessibleName = null;
            resources.ApplyResources( this.labelControl22, "labelControl22" );
            this.labelControl22.Name = "labelControl22";
            // 
            // winControlContainer32
            // 
            resources.ApplyResources( this.winControlContainer32, "winControlContainer32" );
            this.winControlContainer32.Name = "winControlContainer32";
            this.winControlContainer32.WinControl = this.labelControl27;
            // 
            // labelControl27
            // 
            this.labelControl27.AccessibleDescription = null;
            this.labelControl27.AccessibleName = null;
            resources.ApplyResources( this.labelControl27, "labelControl27" );
            this.labelControl27.Name = "labelControl27";
            // 
            // winControlContainer58
            // 
            resources.ApplyResources( this.winControlContainer58, "winControlContainer58" );
            this.winControlContainer58.Name = "winControlContainer58";
            this.winControlContainer58.WinControl = this.labelControl52;
            // 
            // labelControl52
            // 
            this.labelControl52.AccessibleDescription = null;
            this.labelControl52.AccessibleName = null;
            resources.ApplyResources( this.labelControl52, "labelControl52" );
            this.labelControl52.Name = "labelControl52";
            // 
            // winControlContainer37
            // 
            resources.ApplyResources( this.winControlContainer37, "winControlContainer37" );
            this.winControlContainer37.Name = "winControlContainer37";
            this.winControlContainer37.WinControl = this.labelControl64;
            // 
            // labelControl64
            // 
            this.labelControl64.AccessibleDescription = null;
            this.labelControl64.AccessibleName = null;
            resources.ApplyResources( this.labelControl64, "labelControl64" );
            this.labelControl64.Name = "labelControl64";
            // 
            // winControlContainer70
            // 
            resources.ApplyResources( this.winControlContainer70, "winControlContainer70" );
            this.winControlContainer70.Name = "winControlContainer70";
            this.winControlContainer70.WinControl = this.labelControl65;
            // 
            // labelControl65
            // 
            this.labelControl65.AccessibleDescription = null;
            this.labelControl65.AccessibleName = null;
            resources.ApplyResources( this.labelControl65, "labelControl65" );
            this.labelControl65.Name = "labelControl65";
            // 
            // winControlContainer71
            // 
            resources.ApplyResources( this.winControlContainer71, "winControlContainer71" );
            this.winControlContainer71.Name = "winControlContainer71";
            this.winControlContainer71.WinControl = this.labelControl66;
            // 
            // labelControl66
            // 
            this.labelControl66.AccessibleDescription = null;
            this.labelControl66.AccessibleName = null;
            resources.ApplyResources( this.labelControl66, "labelControl66" );
            this.labelControl66.Name = "labelControl66";
            // 
            // winControlContainer72
            // 
            resources.ApplyResources( this.winControlContainer72, "winControlContainer72" );
            this.winControlContainer72.Name = "winControlContainer72";
            this.winControlContainer72.WinControl = this.labelControl67;
            // 
            // labelControl67
            // 
            this.labelControl67.AccessibleDescription = null;
            this.labelControl67.AccessibleName = null;
            resources.ApplyResources( this.labelControl67, "labelControl67" );
            this.labelControl67.Name = "labelControl67";
            // 
            // winControlContainer73
            // 
            resources.ApplyResources( this.winControlContainer73, "winControlContainer73" );
            this.winControlContainer73.Name = "winControlContainer73";
            this.winControlContainer73.WinControl = this.labelControl68;
            // 
            // labelControl68
            // 
            this.labelControl68.AccessibleDescription = null;
            this.labelControl68.AccessibleName = null;
            resources.ApplyResources( this.labelControl68, "labelControl68" );
            this.labelControl68.Name = "labelControl68";
            // 
            // winControlContainer74
            // 
            resources.ApplyResources( this.winControlContainer74, "winControlContainer74" );
            this.winControlContainer74.Name = "winControlContainer74";
            this.winControlContainer74.WinControl = this.labelControl69;
            // 
            // labelControl69
            // 
            this.labelControl69.AccessibleDescription = null;
            this.labelControl69.AccessibleName = null;
            resources.ApplyResources( this.labelControl69, "labelControl69" );
            this.labelControl69.Name = "labelControl69";
            // 
            // winControlContainer75
            // 
            resources.ApplyResources( this.winControlContainer75, "winControlContainer75" );
            this.winControlContainer75.Name = "winControlContainer75";
            this.winControlContainer75.WinControl = this.labelControl70;
            // 
            // labelControl70
            // 
            this.labelControl70.AccessibleDescription = null;
            this.labelControl70.AccessibleName = null;
            resources.ApplyResources( this.labelControl70, "labelControl70" );
            this.labelControl70.Name = "labelControl70";
            // 
            // winControlContainer76
            // 
            resources.ApplyResources( this.winControlContainer76, "winControlContainer76" );
            this.winControlContainer76.Name = "winControlContainer76";
            this.winControlContainer76.WinControl = this.labelControl71;
            // 
            // labelControl71
            // 
            this.labelControl71.AccessibleDescription = null;
            this.labelControl71.AccessibleName = null;
            resources.ApplyResources( this.labelControl71, "labelControl71" );
            this.labelControl71.Name = "labelControl71";
            // 
            // winControlContainer77
            // 
            resources.ApplyResources( this.winControlContainer77, "winControlContainer77" );
            this.winControlContainer77.Name = "winControlContainer77";
            this.winControlContainer77.WinControl = this.labelControl72;
            // 
            // labelControl72
            // 
            this.labelControl72.AccessibleDescription = null;
            this.labelControl72.AccessibleName = null;
            resources.ApplyResources( this.labelControl72, "labelControl72" );
            this.labelControl72.Name = "labelControl72";
            // 
            // winControlContainer78
            // 
            resources.ApplyResources( this.winControlContainer78, "winControlContainer78" );
            this.winControlContainer78.Name = "winControlContainer78";
            this.winControlContainer78.WinControl = this.labelControl73;
            // 
            // labelControl73
            // 
            this.labelControl73.AccessibleDescription = null;
            this.labelControl73.AccessibleName = null;
            resources.ApplyResources( this.labelControl73, "labelControl73" );
            this.labelControl73.Name = "labelControl73";
            // 
            // winControlContainer79
            // 
            resources.ApplyResources( this.winControlContainer79, "winControlContainer79" );
            this.winControlContainer79.Name = "winControlContainer79";
            this.winControlContainer79.WinControl = this.labelControl74;
            // 
            // labelControl74
            // 
            this.labelControl74.AccessibleDescription = null;
            this.labelControl74.AccessibleName = null;
            resources.ApplyResources( this.labelControl74, "labelControl74" );
            this.labelControl74.Name = "labelControl74";
            // 
            // winControlContainer80
            // 
            resources.ApplyResources( this.winControlContainer80, "winControlContainer80" );
            this.winControlContainer80.Name = "winControlContainer80";
            this.winControlContainer80.WinControl = this.labelControl75;
            // 
            // labelControl75
            // 
            this.labelControl75.AccessibleDescription = null;
            this.labelControl75.AccessibleName = null;
            resources.ApplyResources( this.labelControl75, "labelControl75" );
            this.labelControl75.Name = "labelControl75";
            // 
            // winControlContainer81
            // 
            resources.ApplyResources( this.winControlContainer81, "winControlContainer81" );
            this.winControlContainer81.Name = "winControlContainer81";
            this.winControlContainer81.WinControl = this.labelControl76;
            // 
            // labelControl76
            // 
            this.labelControl76.AccessibleDescription = null;
            this.labelControl76.AccessibleName = null;
            resources.ApplyResources( this.labelControl76, "labelControl76" );
            this.labelControl76.Name = "labelControl76";
            // 
            // winControlContainer82
            // 
            resources.ApplyResources( this.winControlContainer82, "winControlContainer82" );
            this.winControlContainer82.Name = "winControlContainer82";
            this.winControlContainer82.WinControl = this.labelControl77;
            // 
            // labelControl77
            // 
            this.labelControl77.AccessibleDescription = null;
            this.labelControl77.AccessibleName = null;
            resources.ApplyResources( this.labelControl77, "labelControl77" );
            this.labelControl77.Name = "labelControl77";
            // 
            // winControlContainer83
            // 
            resources.ApplyResources( this.winControlContainer83, "winControlContainer83" );
            this.winControlContainer83.Name = "winControlContainer83";
            this.winControlContainer83.WinControl = this.labelControl78;
            // 
            // labelControl78
            // 
            this.labelControl78.AccessibleDescription = null;
            this.labelControl78.AccessibleName = null;
            resources.ApplyResources( this.labelControl78, "labelControl78" );
            this.labelControl78.Name = "labelControl78";
            // 
            // winControlContainer84
            // 
            resources.ApplyResources( this.winControlContainer84, "winControlContainer84" );
            this.winControlContainer84.Name = "winControlContainer84";
            this.winControlContainer84.WinControl = this.labelControl79;
            // 
            // labelControl79
            // 
            this.labelControl79.AccessibleDescription = null;
            this.labelControl79.AccessibleName = null;
            resources.ApplyResources( this.labelControl79, "labelControl79" );
            this.labelControl79.Name = "labelControl79";
            // 
            // winControlContainer85
            // 
            resources.ApplyResources( this.winControlContainer85, "winControlContainer85" );
            this.winControlContainer85.Name = "winControlContainer85";
            this.winControlContainer85.WinControl = this.labelControl80;
            // 
            // labelControl80
            // 
            this.labelControl80.AccessibleDescription = null;
            this.labelControl80.AccessibleName = null;
            resources.ApplyResources( this.labelControl80, "labelControl80" );
            this.labelControl80.Name = "labelControl80";
            // 
            // winControlContainer112
            // 
            resources.ApplyResources( this.winControlContainer112, "winControlContainer112" );
            this.winControlContainer112.Name = "winControlContainer112";
            this.winControlContainer112.WinControl = this.labelControl107;
            // 
            // labelControl107
            // 
            this.labelControl107.AccessibleDescription = null;
            this.labelControl107.AccessibleName = null;
            resources.ApplyResources( this.labelControl107, "labelControl107" );
            this.labelControl107.Name = "labelControl107";
            // 
            // winControlContainer115
            // 
            resources.ApplyResources( this.winControlContainer115, "winControlContainer115" );
            this.winControlContainer115.Name = "winControlContainer115";
            this.winControlContainer115.WinControl = this.gControlTaskDriver;
            // 
            // gControlTaskDriver
            // 
            this.gControlTaskDriver.AccessibleDescription = null;
            this.gControlTaskDriver.AccessibleName = null;
            resources.ApplyResources( this.gControlTaskDriver, "gControlTaskDriver" );
            this.gControlTaskDriver.BackgroundImage = null;
            this.gControlTaskDriver.EmbeddedNavigator.AccessibleDescription = null;
            this.gControlTaskDriver.EmbeddedNavigator.AccessibleName = null;
            this.gControlTaskDriver.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.Anchor" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.BackgroundImage = null;
            this.gControlTaskDriver.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.ImeMode" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.TextLocation" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.ToolTip = resources.GetString( "gControlTaskDriver.EmbeddedNavigator.ToolTip" );
            this.gControlTaskDriver.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "gControlTaskDriver.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.gControlTaskDriver.EmbeddedNavigator.ToolTipTitle = resources.GetString( "gControlTaskDriver.EmbeddedNavigator.ToolTipTitle" );
            this.gControlTaskDriver.Font = null;
            this.gControlTaskDriver.MainView = this.gridView1;
            this.gControlTaskDriver.Name = "gControlTaskDriver";
            this.gControlTaskDriver.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1} );
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.Transparent;
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Appearance.ViewCaption.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ViewCaption.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.ViewCaption.BorderColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ViewCaption.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ViewCaption.Options.UseBackColor = true;
            this.gridView1.Appearance.ViewCaption.Options.UseBorderColor = true;
            this.gridView1.Appearance.ViewCaption.Options.UseForeColor = true;
            this.gridView1.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.gridView1.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.ViewCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.ViewCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.AppearancePrint.Lines.BackColor = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.Lines.BackColor2 = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.Lines.BorderColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.ForeColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.Lines.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.Lines.Options.UseBorderColor = true;
            this.gridView1.AppearancePrint.Lines.Options.UseForeColor = true;
            resources.ApplyResources( this.gridView1, "gridView1" );
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwner,
            this.colTimeIn,
            this.colTimeOut,
            this.colCounterTime,
            this.colTakeWeight,
            this.colOutWeight,
            this.colNameWeight,
            this.colNumberTravelWeight,
            this.colTravel2,
            this.colTravelWeight} );
            this.gridView1.GridControl = this.gControlTaskDriver;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsPrint.PrintFooter = false;
            this.gridView1.OptionsPrint.PrintGroupFooter = false;
            this.gridView1.OptionsPrint.PrintPreview = true;
            this.gridView1.OptionsPrint.UsePrintStyles = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // colOwner
            // 
            this.colOwner.AppearanceHeader.Options.UseTextOptions = true;
            this.colOwner.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOwner.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOwner.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colOwner, "colOwner" );
            this.colOwner.FieldName = "Owner";
            this.colOwner.Name = "colOwner";
            // 
            // colTimeIn
            // 
            this.colTimeIn.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeIn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeIn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeIn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeIn, "colTimeIn" );
            this.colTimeIn.FieldName = "TimeIn";
            this.colTimeIn.Name = "colTimeIn";
            // 
            // colTimeOut
            // 
            this.colTimeOut.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeOut.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeOut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeOut, "colTimeOut" );
            this.colTimeOut.FieldName = "TimeOut";
            this.colTimeOut.Name = "colTimeOut";
            // 
            // colCounterTime
            // 
            this.colCounterTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colCounterTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCounterTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCounterTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colCounterTime, "colCounterTime" );
            this.colCounterTime.FieldName = "CounterTime";
            this.colCounterTime.Name = "colCounterTime";
            // 
            // colTakeWeight
            // 
            this.colTakeWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTakeWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTakeWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTakeWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTakeWeight, "colTakeWeight" );
            this.colTakeWeight.FieldName = "TakeWeight";
            this.colTakeWeight.Name = "colTakeWeight";
            // 
            // colOutWeight
            // 
            this.colOutWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colOutWeight, "colOutWeight" );
            this.colOutWeight.FieldName = "OutWeight";
            this.colOutWeight.Name = "colOutWeight";
            // 
            // colNameWeight
            // 
            this.colNameWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colNameWeight, "colNameWeight" );
            this.colNameWeight.FieldName = "NameWeight";
            this.colNameWeight.Name = "colNameWeight";
            // 
            // colNumberTravelWeight
            // 
            this.colNumberTravelWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colNumberTravelWeight, "colNumberTravelWeight" );
            this.colNumberTravelWeight.FieldName = "NumberTravelWeight";
            this.colNumberTravelWeight.Name = "colNumberTravelWeight";
            // 
            // colTravel2
            // 
            this.colTravel2.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravel2, "colTravel2" );
            this.colTravel2.FieldName = "Travel";
            this.colTravel2.Name = "colTravel2";
            // 
            // colTravelWeight
            // 
            this.colTravelWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravelWeight, "colTravelWeight" );
            this.colTravelWeight.FieldName = "TravelWeight";
            this.colTravelWeight.Name = "colTravelWeight";
            // 
            // winControlContainer86
            // 
            resources.ApplyResources( this.winControlContainer86, "winControlContainer86" );
            this.winControlContainer86.Name = "winControlContainer86";
            this.winControlContainer86.WinControl = this.gControlFuelTravel;
            // 
            // gControlFuelTravel
            // 
            this.gControlFuelTravel.AccessibleDescription = null;
            this.gControlFuelTravel.AccessibleName = null;
            resources.ApplyResources( this.gControlFuelTravel, "gControlFuelTravel" );
            this.gControlFuelTravel.BackgroundImage = null;
            this.gControlFuelTravel.EmbeddedNavigator.AccessibleDescription = null;
            this.gControlFuelTravel.EmbeddedNavigator.AccessibleName = null;
            this.gControlFuelTravel.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.Anchor" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.BackgroundImage = null;
            this.gControlFuelTravel.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.ImeMode" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.TextLocation" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.ToolTip = resources.GetString( "gControlFuelTravel.EmbeddedNavigator.ToolTip" );
            this.gControlFuelTravel.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "gControlFuelTravel.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.gControlFuelTravel.EmbeddedNavigator.ToolTipTitle = resources.GetString( "gControlFuelTravel.EmbeddedNavigator.ToolTipTitle" );
            this.gControlFuelTravel.Font = null;
            this.gControlFuelTravel.MainView = this.gridView2;
            this.gControlFuelTravel.MaximumSize = new System.Drawing.Size( 600, 90 );
            this.gControlFuelTravel.Name = "gControlFuelTravel";
            this.gControlFuelTravel.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2} );
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.White;
            this.gridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView2.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.Appearance.ViewCaption.BackColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ViewCaption.BackColor2 = System.Drawing.Color.White;
            this.gridView2.Appearance.ViewCaption.BorderColor = System.Drawing.Color.White;
            this.gridView2.Appearance.ViewCaption.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.gridView2.Appearance.ViewCaption.ForeColor = System.Drawing.Color.Black;
            this.gridView2.Appearance.ViewCaption.Options.UseBackColor = true;
            this.gridView2.Appearance.ViewCaption.Options.UseBorderColor = true;
            this.gridView2.Appearance.ViewCaption.Options.UseFont = true;
            this.gridView2.Appearance.ViewCaption.Options.UseForeColor = true;
            this.gridView2.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.gridView2.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.ViewCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView2.Appearance.ViewCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.AppearancePrint.Lines.BackColor = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.Lines.BackColor2 = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.Lines.BorderColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.Lines.ForeColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.Lines.Options.UseBackColor = true;
            this.gridView2.AppearancePrint.Lines.Options.UseBorderColor = true;
            this.gridView2.AppearancePrint.Lines.Options.UseForeColor = true;
            this.gridView2.AppearancePrint.Row.BackColor = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.Row.BackColor2 = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.Row.BorderColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.Row.ForeColor = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.Row.Options.UseBackColor = true;
            this.gridView2.AppearancePrint.Row.Options.UseBorderColor = true;
            this.gridView2.AppearancePrint.Row.Options.UseForeColor = true;
            resources.ApplyResources( this.gridView2, "gridView2" );
            this.gridView2.ColumnPanelRowHeight = 40;
            this.gridView2.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFuelMark,
            this.colCodeMark,
            this.colIssue,
            this.colBalanceIn,
            this.colBalanceOut,
            this.colTimeSpecific,
            this.colTimeMotor} );
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colFuelMark;
            this.gridView2.FormatConditions.AddRange( new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1} );
            this.gridView2.GridControl = this.gControlFuelTravel;
            this.gridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsPrint.AutoWidth = false;
            this.gridView2.OptionsPrint.ExpandAllGroups = false;
            this.gridView2.OptionsPrint.PrintFooter = false;
            this.gridView2.OptionsPrint.PrintGroupFooter = false;
            this.gridView2.OptionsPrint.PrintPreview = true;
            this.gridView2.OptionsPrint.UsePrintStyles = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.RowAutoHeight = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView2.ViewCaptionHeight = 20;
            // 
            // colCodeMark
            // 
            this.colCodeMark.AppearanceHeader.Options.UseTextOptions = true;
            this.colCodeMark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCodeMark.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCodeMark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colCodeMark, "colCodeMark" );
            this.colCodeMark.FieldName = "CodeMark";
            this.colCodeMark.Name = "colCodeMark";
            // 
            // colIssue
            // 
            this.colIssue.AppearanceCell.Options.UseTextOptions = true;
            this.colIssue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIssue.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIssue.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIssue.AppearanceHeader.Options.UseTextOptions = true;
            this.colIssue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIssue.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIssue.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colIssue, "colIssue" );
            this.colIssue.FieldName = "Issue";
            this.colIssue.Name = "colIssue";
            // 
            // colBalanceIn
            // 
            this.colBalanceIn.AppearanceCell.Options.UseTextOptions = true;
            this.colBalanceIn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceIn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceIn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBalanceIn.AppearanceHeader.Options.UseTextOptions = true;
            this.colBalanceIn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceIn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceIn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colBalanceIn, "colBalanceIn" );
            this.colBalanceIn.FieldName = "BalanceIn";
            this.colBalanceIn.Name = "colBalanceIn";
            // 
            // colBalanceOut
            // 
            this.colBalanceOut.AppearanceCell.Options.UseTextOptions = true;
            this.colBalanceOut.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceOut.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceOut.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBalanceOut.AppearanceHeader.Options.UseTextOptions = true;
            this.colBalanceOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceOut.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceOut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colBalanceOut, "colBalanceOut" );
            this.colBalanceOut.FieldName = "BalanceOut";
            this.colBalanceOut.Name = "colBalanceOut";
            // 
            // colTimeSpecific
            // 
            this.colTimeSpecific.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeSpecific.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSpecific.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeSpecific.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeSpecific.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeSpecific.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSpecific.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeSpecific.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeSpecific, "colTimeSpecific" );
            this.colTimeSpecific.FieldName = "TimeSpecific";
            this.colTimeSpecific.Name = "colTimeSpecific";
            // 
            // colTimeMotor
            // 
            this.colTimeMotor.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMotor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMotor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMotor.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMotor.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMotor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMotor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMotor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeMotor, "colTimeMotor" );
            this.colTimeMotor.FieldName = "TimeMotor";
            this.colTimeMotor.Name = "colTimeMotor";
            // 
            // winControlContainer1
            // 
            resources.ApplyResources( this.winControlContainer1, "winControlContainer1" );
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.WinControl = this.gControlWorkDriverAuto;
            // 
            // gControlWorkDriverAuto
            // 
            this.gControlWorkDriverAuto.AccessibleDescription = null;
            this.gControlWorkDriverAuto.AccessibleName = null;
            resources.ApplyResources( this.gControlWorkDriverAuto, "gControlWorkDriverAuto" );
            this.gControlWorkDriverAuto.BackgroundImage = null;
            this.gControlWorkDriverAuto.EmbeddedNavigator.AccessibleDescription = null;
            this.gControlWorkDriverAuto.EmbeddedNavigator.AccessibleName = null;
            this.gControlWorkDriverAuto.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.Anchor" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.BackgroundImage = null;
            this.gControlWorkDriverAuto.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.ImeMode" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.TextLocation" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.ToolTip = resources.GetString( "gControlWorkDriverAuto.EmbeddedNavigator.ToolTip" );
            this.gControlWorkDriverAuto.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "gControlWorkDriverAuto.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.gControlWorkDriverAuto.EmbeddedNavigator.ToolTipTitle = resources.GetString( "gControlWorkDriverAuto.EmbeddedNavigator.ToolTipTitle" );
            this.gControlWorkDriverAuto.Font = null;
            this.gControlWorkDriverAuto.MainView = this.gridView3;
            this.gControlWorkDriverAuto.MaximumSize = new System.Drawing.Size( 553, 90 );
            this.gControlWorkDriverAuto.Name = "gControlWorkDriverAuto";
            this.gControlWorkDriverAuto.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3} );
            // 
            // gridView3
            // 
            this.gridView3.Appearance.ViewCaption.ForeColor = System.Drawing.Color.Black;
            this.gridView3.Appearance.ViewCaption.Options.UseForeColor = true;
            this.gridView3.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.gridView3.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView3.Appearance.ViewCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView3.Appearance.ViewCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView3.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView3.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView3.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView3.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView3.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView3.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView3.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView3.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView3.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.gridView3, "gridView3" );
            this.gridView3.ColumnPanelRowHeight = 40;
            this.gridView3.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOperation,
            this.colTimeHour,
            this.colTimeMinute,
            this.colTravel,
            this.colTimeFact} );
            this.gridView3.GridControl = this.gControlWorkDriverAuto;
            this.gridView3.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsPrint.AutoWidth = false;
            this.gridView3.OptionsPrint.ExpandAllGroups = false;
            this.gridView3.OptionsPrint.PrintFooter = false;
            this.gridView3.OptionsPrint.PrintGroupFooter = false;
            this.gridView3.OptionsPrint.UsePrintStyles = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView3.ViewCaptionHeight = 20;
            // 
            // colOperation
            // 
            this.colOperation.AppearanceCell.Options.UseTextOptions = true;
            this.colOperation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOperation.AppearanceHeader.Options.UseTextOptions = true;
            this.colOperation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOperation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOperation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colOperation, "colOperation" );
            this.colOperation.FieldName = "Operation";
            this.colOperation.Name = "colOperation";
            // 
            // colTimeHour
            // 
            this.colTimeHour.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeHour.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeHour.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeHour.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeHour.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeHour, "colTimeHour" );
            this.colTimeHour.FieldName = "TimeHour";
            this.colTimeHour.Name = "colTimeHour";
            // 
            // colTimeMinute
            // 
            this.colTimeMinute.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMinute.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMinute.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMinute.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMinute.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMinute.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMinute.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMinute.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeMinute, "colTimeMinute" );
            this.colTimeMinute.FieldName = "TimeMinute";
            this.colTimeMinute.Name = "colTimeMinute";
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravel, "colTravel" );
            this.colTravel.FieldName = "Travel";
            this.colTravel.Name = "colTravel";
            // 
            // colTimeFact
            // 
            this.colTimeFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFact.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFact.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFact.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeFact, "colTimeFact" );
            this.colTimeFact.FieldName = "TimeFact";
            this.colTimeFact.Name = "colTimeFact";
            // 
            // winControlContainer2
            // 
            resources.ApplyResources( this.winControlContainer2, "winControlContainer2" );
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.WinControl = this.labelControl1;
            // 
            // labelControl1
            // 
            this.labelControl1.AccessibleDescription = null;
            this.labelControl1.AccessibleName = null;
            resources.ApplyResources( this.labelControl1, "labelControl1" );
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Name = "labelControl1";
            // 
            // winControlContainer28
            // 
            resources.ApplyResources( this.winControlContainer28, "winControlContainer28" );
            this.winControlContainer28.Name = "winControlContainer28";
            this.winControlContainer28.WinControl = this.labelControl2;
            // 
            // labelControl2
            // 
            this.labelControl2.AccessibleDescription = null;
            this.labelControl2.AccessibleName = null;
            resources.ApplyResources( this.labelControl2, "labelControl2" );
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Name = "labelControl2";
            // 
            // winControlContainer29
            // 
            resources.ApplyResources( this.winControlContainer29, "winControlContainer29" );
            this.winControlContainer29.Name = "winControlContainer29";
            this.winControlContainer29.WinControl = this.labelControl23;
            // 
            // labelControl23
            // 
            this.labelControl23.AccessibleDescription = null;
            this.labelControl23.AccessibleName = null;
            resources.ApplyResources( this.labelControl23, "labelControl23" );
            this.labelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl23.Name = "labelControl23";
            // 
            // winControlContainer30
            // 
            resources.ApplyResources( this.winControlContainer30, "winControlContainer30" );
            this.winControlContainer30.Name = "winControlContainer30";
            this.winControlContainer30.WinControl = this.colControlSignature;
            // 
            // colControlSignature
            // 
            this.colControlSignature.AccessibleDescription = null;
            this.colControlSignature.AccessibleName = null;
            resources.ApplyResources( this.colControlSignature, "colControlSignature" );
            this.colControlSignature.BackgroundImage = null;
            this.colControlSignature.EmbeddedNavigator.AccessibleDescription = null;
            this.colControlSignature.EmbeddedNavigator.AccessibleName = null;
            this.colControlSignature.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.colControlSignature.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.Anchor" ) ) );
            this.colControlSignature.EmbeddedNavigator.BackgroundImage = null;
            this.colControlSignature.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.colControlSignature.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.ImeMode" ) ) );
            this.colControlSignature.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.TextLocation" ) ) );
            this.colControlSignature.EmbeddedNavigator.ToolTip = resources.GetString( "colControlSignature.EmbeddedNavigator.ToolTip" );
            this.colControlSignature.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "colControlSignature.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.colControlSignature.EmbeddedNavigator.ToolTipTitle = resources.GetString( "colControlSignature.EmbeddedNavigator.ToolTipTitle" );
            this.colControlSignature.Font = null;
            this.colControlSignature.MainView = this.gridView4;
            this.colControlSignature.MaximumSize = new System.Drawing.Size( 453, 43 );
            this.colControlSignature.MinimumSize = new System.Drawing.Size( 453, 43 );
            this.colControlSignature.Name = "colControlSignature";
            this.colControlSignature.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4} );
            // 
            // gridView4
            // 
            this.gridView4.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView4.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView4.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView4.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView4.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView4.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView4.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView4.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView4.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView4.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView4.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView4.AppearancePrint.Lines.BackColor = System.Drawing.Color.White;
            this.gridView4.AppearancePrint.Lines.BackColor2 = System.Drawing.Color.White;
            this.gridView4.AppearancePrint.Lines.BorderColor = System.Drawing.Color.Black;
            this.gridView4.AppearancePrint.Lines.ForeColor = System.Drawing.Color.Black;
            this.gridView4.AppearancePrint.Lines.Options.UseBackColor = true;
            this.gridView4.AppearancePrint.Lines.Options.UseBorderColor = true;
            this.gridView4.AppearancePrint.Lines.Options.UseForeColor = true;
            resources.ApplyResources( this.gridView4, "gridView4" );
            this.gridView4.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFueler,
            this.colMechanic1,
            this.colMechanic2,
            this.colDispatcher} );
            this.gridView4.GridControl = this.colControlSignature;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsPrint.AutoWidth = false;
            this.gridView4.OptionsPrint.ExpandAllGroups = false;
            this.gridView4.OptionsPrint.PrintFooter = false;
            this.gridView4.OptionsPrint.PrintGroupFooter = false;
            this.gridView4.OptionsPrint.UsePrintStyles = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // colFueler
            // 
            this.colFueler.AppearanceCell.Options.UseTextOptions = true;
            this.colFueler.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFueler.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFueler.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFueler.AppearanceHeader.Options.UseTextOptions = true;
            this.colFueler.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFueler.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFueler.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colFueler, "colFueler" );
            this.colFueler.FieldName = "Fueler";
            this.colFueler.Name = "colFueler";
            // 
            // colMechanic1
            // 
            this.colMechanic1.AppearanceCell.Options.UseTextOptions = true;
            this.colMechanic1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMechanic1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMechanic1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMechanic1.AppearanceHeader.Options.UseTextOptions = true;
            this.colMechanic1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMechanic1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMechanic1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colMechanic1, "colMechanic1" );
            this.colMechanic1.FieldName = "Mechanic1";
            this.colMechanic1.Name = "colMechanic1";
            // 
            // colMechanic2
            // 
            this.colMechanic2.AppearanceCell.Options.UseTextOptions = true;
            this.colMechanic2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMechanic2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMechanic2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colMechanic2, "colMechanic2" );
            this.colMechanic2.FieldName = "Mechanic2";
            this.colMechanic2.Name = "colMechanic2";
            // 
            // colDispatcher
            // 
            this.colDispatcher.AppearanceCell.Options.UseTextOptions = true;
            this.colDispatcher.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDispatcher.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDispatcher.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDispatcher.AppearanceHeader.Options.UseTextOptions = true;
            this.colDispatcher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDispatcher.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDispatcher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colDispatcher, "colDispatcher" );
            this.colDispatcher.FieldName = "Dispatcher";
            this.colDispatcher.Name = "colDispatcher";
            // 
            // winControlContainer31
            // 
            resources.ApplyResources( this.winControlContainer31, "winControlContainer31" );
            this.winControlContainer31.Name = "winControlContainer31";
            this.winControlContainer31.WinControl = this.labelControl24;
            // 
            // labelControl24
            // 
            this.labelControl24.AccessibleDescription = null;
            this.labelControl24.AccessibleName = null;
            resources.ApplyResources( this.labelControl24, "labelControl24" );
            this.labelControl24.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Name = "labelControl24";
            // 
            // winControlContainer33
            // 
            resources.ApplyResources( this.winControlContainer33, "winControlContainer33" );
            this.winControlContainer33.Name = "winControlContainer33";
            this.winControlContainer33.WinControl = this.labelControl25;
            // 
            // labelControl25
            // 
            this.labelControl25.AccessibleDescription = null;
            this.labelControl25.AccessibleName = null;
            resources.ApplyResources( this.labelControl25, "labelControl25" );
            this.labelControl25.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Name = "labelControl25";
            // 
            // winControlContainer34
            // 
            resources.ApplyResources( this.winControlContainer34, "winControlContainer34" );
            this.winControlContainer34.Name = "winControlContainer34";
            this.winControlContainer34.WinControl = this.labelControl26;
            // 
            // labelControl26
            // 
            this.labelControl26.AccessibleDescription = null;
            this.labelControl26.AccessibleName = null;
            resources.ApplyResources( this.labelControl26, "labelControl26" );
            this.labelControl26.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.Name = "labelControl26";
            // 
            // winControlContainer35
            // 
            resources.ApplyResources( this.winControlContainer35, "winControlContainer35" );
            this.winControlContainer35.Name = "winControlContainer35";
            this.winControlContainer35.WinControl = this.labelNumberSheet;
            // 
            // labelNumberSheet
            // 
            this.labelNumberSheet.AccessibleDescription = null;
            this.labelNumberSheet.AccessibleName = null;
            resources.ApplyResources( this.labelNumberSheet, "labelNumberSheet" );
            this.labelNumberSheet.Name = "labelNumberSheet";
            // 
            // winControlContainer36
            // 
            resources.ApplyResources( this.winControlContainer36, "winControlContainer36" );
            this.winControlContainer36.Name = "winControlContainer36";
            this.winControlContainer36.WinControl = this.labelModelAuto;
            // 
            // labelModelAuto
            // 
            this.labelModelAuto.AccessibleDescription = null;
            this.labelModelAuto.AccessibleName = null;
            resources.ApplyResources( this.labelModelAuto, "labelModelAuto" );
            this.labelModelAuto.Name = "labelModelAuto";
            // 
            // winControlContainer38
            // 
            resources.ApplyResources( this.winControlContainer38, "winControlContainer38" );
            this.winControlContainer38.Name = "winControlContainer38";
            this.winControlContainer38.WinControl = this.labelRegNumber;
            // 
            // labelRegNumber
            // 
            this.labelRegNumber.AccessibleDescription = null;
            this.labelRegNumber.AccessibleName = null;
            resources.ApplyResources( this.labelRegNumber, "labelRegNumber" );
            this.labelRegNumber.Name = "labelRegNumber";
            // 
            // winControlContainer39
            // 
            resources.ApplyResources( this.winControlContainer39, "winControlContainer39" );
            this.winControlContainer39.Name = "winControlContainer39";
            this.winControlContainer39.WinControl = this.labelTypeAuto;
            // 
            // labelTypeAuto
            // 
            this.labelTypeAuto.AccessibleDescription = null;
            this.labelTypeAuto.AccessibleName = null;
            resources.ApplyResources( this.labelTypeAuto, "labelTypeAuto" );
            this.labelTypeAuto.Name = "labelTypeAuto";
            // 
            // winControlContainer40
            // 
            resources.ApplyResources( this.winControlContainer40, "winControlContainer40" );
            this.winControlContainer40.Name = "winControlContainer40";
            this.winControlContainer40.WinControl = this.labelFIO;
            // 
            // labelFIO
            // 
            this.labelFIO.AccessibleDescription = null;
            this.labelFIO.AccessibleName = null;
            resources.ApplyResources( this.labelFIO, "labelFIO" );
            this.labelFIO.Name = "labelFIO";
            // 
            // winControlContainer41
            // 
            resources.ApplyResources( this.winControlContainer41, "winControlContainer41" );
            this.winControlContainer41.Name = "winControlContainer41";
            this.winControlContainer41.WinControl = this.labelNumService;
            // 
            // labelNumService
            // 
            this.labelNumService.AccessibleDescription = null;
            this.labelNumService.AccessibleName = null;
            resources.ApplyResources( this.labelNumService, "labelNumService" );
            this.labelNumService.Name = "labelNumService";
            // 
            // winControlContainer42
            // 
            resources.ApplyResources( this.winControlContainer42, "winControlContainer42" );
            this.winControlContainer42.Name = "winControlContainer42";
            this.winControlContainer42.WinControl = this.labelClassDriver;
            // 
            // labelClassDriver
            // 
            this.labelClassDriver.AccessibleDescription = null;
            this.labelClassDriver.AccessibleName = null;
            resources.ApplyResources( this.labelClassDriver, "labelClassDriver" );
            this.labelClassDriver.Name = "labelClassDriver";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // WaySheet1
            // 
            this.Bands.AddRange( new DevExpress.XtraReports.UI.Band[] {
            this.Detail} );
            this.Borders = ( ( DevExpress.XtraPrinting.BorderSide ) ( ( ( ( DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top ) 
            | DevExpress.XtraPrinting.BorderSide.Right ) 
            | DevExpress.XtraPrinting.BorderSide.Bottom ) ) );
            this.ExportOptions.Csv.EncodingType = ( ( DevExpress.XtraPrinting.EncodingType ) ( resources.GetObject( "WaySheet1.ExportOptions.Csv.EncodingType" ) ) );
            this.ExportOptions.Html.CharacterSet = resources.GetString( "WaySheet1.ExportOptions.Html.CharacterSet" );
            this.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.ExportOptions.Html.Title = resources.GetString( "WaySheet1.ExportOptions.Html.Title" );
            this.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.ExportOptions.Mht.CharacterSet = resources.GetString( "WaySheet1.ExportOptions.Mht.CharacterSet" );
            this.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.ExportOptions.Mht.Title = resources.GetString( "WaySheet1.ExportOptions.Mht.Title" );
            this.ExportOptions.Pdf.DocumentOptions.Application = resources.GetString( "WaySheet1.ExportOptions.Pdf.DocumentOptions.Application" );
            this.ExportOptions.Pdf.DocumentOptions.Author = resources.GetString( "WaySheet1.ExportOptions.Pdf.DocumentOptions.Author" );
            this.ExportOptions.Pdf.DocumentOptions.Keywords = resources.GetString( "WaySheet1.ExportOptions.Pdf.DocumentOptions.Keywords" );
            this.ExportOptions.Pdf.DocumentOptions.Subject = resources.GetString( "WaySheet1.ExportOptions.Pdf.DocumentOptions.Subject" );
            this.ExportOptions.Pdf.DocumentOptions.Title = resources.GetString( "WaySheet1.ExportOptions.Pdf.DocumentOptions.Title" );
            this.ExportOptions.Text.EncodingType = ( ( DevExpress.XtraPrinting.EncodingType ) ( resources.GetObject( "WaySheet1.ExportOptions.Text.EncodingType" ) ) );
            this.ExportOptions.Xls.SheetName = resources.GetString( "WaySheet1.ExportOptions.Xls.SheetName" );
            this.ExportOptions.Xls.ShowGridLines = true;
            this.ExportOptions.Xlsx.SheetName = resources.GetString( "WaySheet1.ExportOptions.Xlsx.SheetName" );
            this.ExportOptions.Xlsx.ShowGridLines = true;
            this.GridSize = new System.Drawing.Size( 5, 5 );
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins( 10, 10, 10, 10 );
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange( new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1} );
            this.Version = "9.2";
            resources.ApplyResources( this, "$this" );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit1.Properties ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textGarageNumber.Properties ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textTabelDriver.Properties ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit4.Properties ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.textEdit5.Properties ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlTaskDriver ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlFuelTravel ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gControlWorkDriverAuto ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView3 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.colControlSignature ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView4 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this ) ).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private DevExpress.XtraEditors.LabelControl labelDate;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer6;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer7;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer8;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer9;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer10;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer11;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer12;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer13;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer14;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer15;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer16;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer17;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer18;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer19;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer20;
        private DevExpress.XtraEditors.TextEdit textGarageNumber;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer21;
        private DevExpress.XtraEditors.TextEdit textTabelDriver;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer22;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer23;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer24;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer25;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer26;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer27;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer32;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer58;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer37;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer70;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer71;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer72;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer73;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer74;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer75;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer76;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer77;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer78;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer79;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer80;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer81;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer82;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer83;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer84;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer85;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer112;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer115;
        private DevExpress.XtraGrid.GridControl gControlTaskDriver;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeIn;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeOut;
        private DevExpress.XtraGrid.Columns.GridColumn colCounterTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTakeWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colOutWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colNameWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberTravelWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel2;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelWeight;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer86;
        private DevExpress.XtraGrid.GridControl gControlFuelTravel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelMark;
        private DevExpress.XtraGrid.Columns.GridColumn colCodeMark;
        private DevExpress.XtraGrid.Columns.GridColumn colIssue;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceIn;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceOut;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSpecific;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMotor;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private DevExpress.XtraGrid.GridControl gControlWorkDriverAuto;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colOperation;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeHour;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMinute;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFact;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer28;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer29;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer30;
        private DevExpress.XtraGrid.GridControl colControlSignature;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colFueler;
        private DevExpress.XtraGrid.Columns.GridColumn colMechanic1;
        private DevExpress.XtraGrid.Columns.GridColumn colMechanic2;
        private DevExpress.XtraGrid.Columns.GridColumn colDispatcher;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer31;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer33;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer34;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer35;
        private DevExpress.XtraEditors.LabelControl labelNumberSheet;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer36;
        private DevExpress.XtraEditors.LabelControl labelModelAuto;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer38;
        private DevExpress.XtraEditors.LabelControl labelRegNumber;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer39;
        private DevExpress.XtraEditors.LabelControl labelTypeAuto;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer40;
        private DevExpress.XtraEditors.LabelControl labelFIO;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer41;
        private DevExpress.XtraEditors.LabelControl labelNumService;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer42;
        private DevExpress.XtraEditors.LabelControl labelClassDriver;
    }
}
