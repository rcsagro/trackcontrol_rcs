using System;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using MotorDepot.Properties;

namespace MotorDepot
{
    public partial class XtraSchedulerReport2 : DevExpress.XtraScheduler.Reporting.XtraSchedulerReport
    {
        int countColumn = 23;  // ������� ��������

        public XtraSchedulerReport2()
        {
            InitializeComponent();
            InitializeTableReport();
        }

        // ����������� ����� ������ ������
        protected void InitializeTableReport()
        {
            var dT1 = new DataTable();
            dT1.Columns.AddRange( new[]
                {
                    new DataColumn("NumberTravel", typeof (string)),
                    new DataColumn("Tikets", typeof (string)),
                    new DataColumn("WorkTimeAll", typeof (string)),
                    new DataColumn("WeightTravel", typeof (string)),
                    new DataColumn("WorkKTonn", typeof (string)),
                    new DataColumn("DescribeWorker", typeof (string)),
                    new DataColumn("TravelFrom", typeof (string)),
                    new DataColumn("TravelOutSide", typeof (string)),
                } );

            for ( var i = 0 ; i < 7; i++ )
            {
                var dr = dT1.NewRow();
                dT1.Rows.Add( dr );
            }

            DataRow row = dT1.Rows[0];
            for ( int i = 0 ; i < dT1.Columns.Count ; i++ )
            {
                row[i] = countColumn++;
            }
            
            gridControlTask.DataSource = dT1;

            var dT2 = new DataTable();
            dT2.Columns.AddRange( new[]
                {
                    new DataColumn("BalanceFuelNorm", typeof (string)),
                    new DataColumn("BalanceFuelFact", typeof (string)),
                    new DataColumn("TimeWorkAuto", typeof (string)),
                    new DataColumn("TimeWorkTrailer", typeof (string)),
                    new DataColumn("TimeWorkAutoMove", typeof (string)),
                    new DataColumn("TimeWorkAutoStopLine", typeof (string)),
                    new DataColumn("TimeWorkAutoCrach", typeof (string)),
                    new DataColumn("NumberTravelWeight", typeof (string)),
                    new DataColumn("TravelAutoAll", typeof (string)),
                    new DataColumn("TravelTrailerAll", typeof (string)),
                    new DataColumn("TravelAutoWeight", typeof (string)),
                    new DataColumn("TravelTrailerWeight", typeof (string)),
                    new DataColumn("WeightTravelAll", typeof (string)),
                    new DataColumn("WeightTrailerAll", typeof (string)),
                    new DataColumn("WorkKTonnAll", typeof (string)),
                    new DataColumn("WorkKTonnTrailerAll", typeof (string)),
                    new DataColumn("SalaryCode", typeof (string)),
                    new DataColumn("SalarySumma", typeof (string)),
                } );

            for ( var i = 0 ; i < 2; i++ )
            {
                var dr = dT2.NewRow();
                dT2.Rows.Add( dr );
            }

            row = dT2.Rows[0];
            for ( int i = 0 ; i < dT2.Columns.Count ; i++ )
            {  
                row[i] = countColumn++;
            }

            gridControlResultWork.DataSource = dT2;
        } // InitializeTableReport
    } // XtraSchedulerReport2
} // MotorDepot
