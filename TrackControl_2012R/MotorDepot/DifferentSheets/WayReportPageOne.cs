﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;

namespace MotorDepot.DifferentSheets
{
    class WayReportPageOne : XtraReport
    {
        string[] Month = {"січня", "лютого", "березня", "квітня", "травня", "червня", "липня", "серпня", "вересня", "жовтня", "листопада", "грудня"};

        private DetailBand Detail;
        private PageHeaderBand PageHeader;
        private PageFooterBand PageFooter;

        private XRLabel xrLabel1;
        private XRLabel xrLabel2;
        private XRLabel xrLabel3;
        private XRLabel xrLabel4;
        private XRLabel xrLabel5;                  
        private XRLabel xrLabel6;
        private XRLabel xrLabel7;
        private XRLabel xrLabel8;
        private XRLabel xrLabel9;
        private XRLabel xrLabel10;
        private XRLabel xrLabel11;
        private XRLabel xrLabel12;
        private XRLabel xrLabel13;
        private XRLabel xrLabel14;
        private XRLabel xrLabel15;
        private XRLabel xrLabel16;
        private XRLabel xrLabel17;
        private XRLabel xrLabel18;
        private XRLabel xrLabel19;
        private XRLabel xrLabel20;
        private XRLabel xrLabel21;
        private XRLabel xrLabel22;
        private XRLabel xrLabel23;
        //private XRLabel xrLabel24;
        //private XRLabel xrLabel25;
        //private XRLabel xrLabel26;
        //private XRLabel xrLabel27;

        private XRTable xrTable1;
        private XRTable xrTable2;
        private XRTable xrTable3;
        private XRTable xrTable4;

        private DevExpress.XtraPrinting.BorderSide visibleBorders;
        private string dFont;

        public WayReportPageOne()
        {
            visibleBorders = DevExpress.XtraPrinting.BorderSide.None;
            dFont = "Times New Roman";

            this.Detail = new DetailBand();
            this.PageHeader = new PageHeaderBand();
            this.PageFooter = new PageFooterBand();

            Landscape = true;
            PaperKind = System.Drawing.Printing.PaperKind.A4;
            Margins.Bottom = 20;
            Margins.Top = 20;
            this.PageFooter.Height = 20;
            this.PageHeader.Height = 20;
            this.Bands.AddRange( new Band[] { this.Detail, this.PageHeader, this.PageFooter } );

            this.xrLabel1 = new XRLabel();
            this.xrLabel1.Location = new Point( 300, 0 );
            this.xrLabel1.Size = new Size( 210, 10 );
            this.xrLabel1.WordWrap = false;
            this.xrLabel1.Multiline = true;
            this.xrLabel1.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel1.Borders = visibleBorders;
            this.xrLabel1.Text = "ПОДОРОЖНІЙ ЛИСТ № ____\nВАНТАЖНОГО АВТОМОБІЛЯ";
            this.xrLabel1.Font = new Font( dFont, 10, FontStyle.Bold );

            this.xrLabel2 = new XRLabel();
            this.xrLabel2.Location = new Point( 300, 10 );
            this.xrLabel2.Size = new Size( 200, 10 );
            this.xrLabel2.WordWrap = false;
            this.xrLabel2.Multiline = true;
            this.xrLabel2.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel2.Text = "\"___\"____________20____р.";
            this.xrLabel2.Font = new Font( dFont, 10, FontStyle.Bold );
            this.xrLabel2.Borders = visibleBorders;

            this.xrLabel3 = new XRLabel();
            this.xrLabel3.Location = new Point( 50, 0 );
            this.xrLabel3.Size = new Size( 120, 10 );
            this.xrLabel3.WordWrap = false;
            this.xrLabel3.Multiline = true;
            this.xrLabel3.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel3.Text = "Місце для штампу\nпідприємства";
            this.xrLabel3.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel3.Borders = visibleBorders;

            this.xrLabel4 = new XRLabel();
            this.xrLabel4.Location = new Point( 120, 64 );
            this.xrLabel4.Size = new Size( 280, 10 );
            this.xrLabel4.WordWrap = false;
            this.xrLabel4.Multiline = true;
            this.xrLabel4.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel4.Text = "Режим роботи_________________";
            this.xrLabel4.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel4.Borders = visibleBorders;

            this.xrLabel5 = new XRLabel();
            this.xrLabel5.Location = new Point( 120, 75 );
            this.xrLabel5.Size = new Size( 280, 10 );
            this.xrLabel5.WordWrap = false;
            this.xrLabel5.Multiline = true;
            this.xrLabel5.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel5.Text = "Колонна____________ Бригада________";
            this.xrLabel5.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel5.Borders = visibleBorders;

            this.xrLabel6 = new XRLabel();
            this.xrLabel6.Location = new Point( 420, 64 );
            this.xrLabel6.Size = new Size( 30, 10 );
            this.xrLabel6.WordWrap = false;
            this.xrLabel6.Multiline = true;
            this.xrLabel6.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel6.Text = "Код";
            this.xrLabel6.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel6.Borders = visibleBorders;

            this.xrLabel7 = new XRLabel();
            this.xrLabel7.Location = new Point( 450, 64 );
            this.xrLabel7.Size = new Size( 60, 10 );
            this.xrLabel7.WordWrap = false;
            this.xrLabel7.Multiline = true;
            this.xrLabel7.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel7.Text = "    ";
            this.xrLabel7.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel8 = new XRLabel();
            this.xrLabel8.Location = new Point( 10, 86 );
            this.xrLabel8.Size = new Size( 410, 10 );
            this.xrLabel8.WordWrap = false;
            this.xrLabel8.Multiline = true;
            this.xrLabel8.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel8.Text = "Автомобіль_____________________________________ Гар. №";
            this.xrLabel8.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel8.Borders = visibleBorders;

            this.xrLabel16 = new XRLabel();
            this.xrLabel16.Location = new Point( 425, 86 );
            this.xrLabel16.Size = new Size( 85, 10 );
            this.xrLabel16.WordWrap = false;
            this.xrLabel16.Multiline = true;
            this.xrLabel16.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel16.Text = "     ";
            this.xrLabel16.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel9 = new XRLabel();
            this.xrLabel9.Location = new Point( 10, 97 );
            this.xrLabel9.Size = new Size( 410, 10 );
            this.xrLabel9.WordWrap = false;
            this.xrLabel9.Multiline = true;
            this.xrLabel9.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel9.Text = "Водій__________________________________________ Таб. №";
            this.xrLabel9.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel9.Borders = visibleBorders;

            this.xrLabel17 = new XRLabel();
            this.xrLabel17.Location = new Point( 425, 97 );
            this.xrLabel17.Size = new Size( 85, 10 );
            this.xrLabel17.WordWrap = false;
            this.xrLabel17.Multiline = true;
            this.xrLabel17.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel17.Text = "   ";
            this.xrLabel17.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel10 = new XRLabel();
            this.xrLabel10.Location = new Point( 10, 108 );
            this.xrLabel10.Size = new Size( 410, 10 );
            this.xrLabel10.WordWrap = false;
            this.xrLabel10.Multiline = true;
            this.xrLabel10.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel10.Text = "Причеп 1_______________________________________ Гар. №";
            this.xrLabel10.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel10.Borders = visibleBorders;

            this.xrLabel18 = new XRLabel();
            this.xrLabel18.Location = new Point( 425, 108 );
            this.xrLabel18.Size = new Size( 85, 10 );
            this.xrLabel18.WordWrap = false;
            this.xrLabel18.Multiline = true;
            this.xrLabel18.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel18.Text = "   ";
            this.xrLabel18.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel19 = new XRLabel();
            this.xrLabel19.Location = new Point( 425, 120 );
            this.xrLabel19.Size = new Size( 85, 10 );
            this.xrLabel19.WordWrap = false;
            this.xrLabel19.Multiline = true;
            this.xrLabel19.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel19.Text = "  ";
            this.xrLabel19.Font = new Font( dFont, 12, FontStyle.Regular );
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel11 = new XRLabel();
            this.xrLabel11.Location = new Point( 10, 135 );
            this.xrLabel11.Size = new Size( 495, 160 );
            this.xrLabel11.WordWrap = false;
            this.xrLabel11.Multiline = true;
            this.xrLabel11.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel11.Text = "Супроводжуючі особи_______________________________________\n__________________________________________________________\n__________________________________________________________\n__________________________________________________________\n__________________________________________________________\n__________________________________________________________\n__________________________________________________________\n__________________________________________________________";
            this.xrLabel11.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel11.Borders = visibleBorders;

            this.xrLabel12 = new XRLabel();
            this.xrLabel12.Location = new Point( 790, 0 );
            this.xrLabel12.Size = new Size( 178, 40 );
            this.xrLabel12.WordWrap = false;
            this.xrLabel12.Multiline = true;
            this.xrLabel12.TextAlignment = TextAlignment.TopRight;
            this.xrLabel12.Text = "Типова форма N 2-TH\nЗатверджена наказом Мінтрансу,\nМінстату України\n29.12.95р. № 488/346";
            this.xrLabel12.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel12.Borders = visibleBorders;

            this.xrLabel13 = new XRLabel();
            this.xrLabel13.Location = new Point( 10, 475 );
            this.xrLabel13.Size = new Size( 330, 50 );
            this.xrLabel13.WordWrap = false;
            this.xrLabel13.Multiline = true;
            this.xrLabel13.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel13.Text = "Посвідчення водія, перевірив, завдання видав, видати\nпального________________________літрів\nПідпис диспетчера_____________\nВодій за станом здоров'я до управління допущенний\nпідпис__________________штамп_____________";
            this.xrLabel13.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel13.Borders = visibleBorders;

            this.xrLabel14 = new XRLabel();
            this.xrLabel14.Location = new Point( 360, 475 );
            this.xrLabel14.Size = new Size( 310, 50 );
            this.xrLabel14.WordWrap = false;
            this.xrLabel14.Multiline = true;
            this.xrLabel14.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel14.Text = "Виїзд дозволений, підпис механіка_________\nАвтомобіль прийняв, підпис водія_________\nПри поверненні автомобіль: справний|несправний\nЗдав водій________________________\nПрийняв механік______________________";
            this.xrLabel14.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel14.Borders = visibleBorders;

            this.xrLabel15 = new XRLabel();
            this.xrLabel15.Location = new Point( 695, 475 );
            this.xrLabel15.Size = new Size( 270, 50 );
            this.xrLabel15.WordWrap = false;
            this.xrLabel15.Multiline = true;
            this.xrLabel15.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel15.Text = "Особливі відмітки_______________________\n________________________________________\n________________________________________\n________________________________________\n________________________________________";
            this.xrLabel15.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel15.Borders = visibleBorders;

            this.xrLabel20 = new XRLabel();
            this.xrLabel20.Location = new Point( 10, 120 );
            this.xrLabel20.Size = new Size( 410, 10 );
            this.xrLabel20.WordWrap = false;
            this.xrLabel20.Multiline = true;
            this.xrLabel20.TextAlignment = TextAlignment.TopLeft;
            this.xrLabel20.Text = "Причеп 2_______________________________________ Гар. №";
            this.xrLabel20.Font = new Font( dFont, 11, FontStyle.Regular );
            this.xrLabel20.Borders = visibleBorders;

            this.xrLabel21 = new XRLabel();
            this.xrLabel21.Location = new Point( 520, 40 );
            this.xrLabel21.Size = new Size( 448, 10 );
            this.xrLabel21.WordWrap = false;
            this.xrLabel21.Multiline = true;
            this.xrLabel21.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel21.Text = "РОБОТА ВОДІЯ ТА АВТОМОБІЛЯ";
            this.xrLabel21.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.All; ;

            this.xrLabel22 = new XRLabel();
            this.xrLabel22.Location = new Point( 520, 130 );
            this.xrLabel22.Size = new Size( 448, 10 );
            this.xrLabel22.WordWrap = false;
            this.xrLabel22.Multiline = true;
            this.xrLabel22.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel22.Text = "РУХ ПАЛЬНОГО ЛІТРІВ";
            this.xrLabel22.Font = new Font( dFont, 9, FontStyle.Regular );
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.All;

            this.xrLabel23 = new XRLabel();
            this.xrLabel23.Location = new Point( 0, 279 );
            this.xrLabel23.Size = new Size( 968, 10 );
            this.xrLabel23.WordWrap = false;
            this.xrLabel23.Multiline = true;
            this.xrLabel23.TextAlignment = TextAlignment.MiddleCenter;
            this.xrLabel23.Text = "ЗАВДАННЯ ВОДІЄВІ";
            this.xrLabel23.Font = new Font( dFont, 10, FontStyle.Regular );
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.All;

            //this.xrLabel24 = new XRLabel();
            //this.xrLabel24.Location = new Point( 60, 95 );
            //this.xrLabel24.Size = new Size( 250, 5 );
            //this.xrLabel24.WordWrap = false;
            //this.xrLabel24.Multiline = true;
            //this.xrLabel24.TextAlignment = TextAlignment.BottomCenter;
            //this.xrLabel24.Text = "марка           держ. №             тип";
            //this.xrLabel24.Font = new Font( dFont, 8, FontStyle.Italic );
            //this.xrLabel24.Borders = visibleBorders;

            //this.xrLabel25 = new XRLabel();
            //this.xrLabel25.Location = new Point( 60, 106 );
            //this.xrLabel25.Size = new Size( 250, 5 );
            //this.xrLabel25.WordWrap = false;
            //this.xrLabel25.Multiline = true;
            //this.xrLabel25.TextAlignment = TextAlignment.TopCenter;
            //this.xrLabel25.Text = "прізвище,ім'я,по батькові    № служ. пос.    клас";
            //this.xrLabel25.Font = new Font( dFont, 8, FontStyle.Italic );
            //this.xrLabel25.Borders = visibleBorders;

            //this.xrLabel26 = new XRLabel();
            //this.xrLabel26.Location = new Point( 60, 118 );
            //this.xrLabel26.Size = new Size( 250, 5 );
            //this.xrLabel26.WordWrap = false;
            //this.xrLabel26.Multiline = true;
            //this.xrLabel26.TextAlignment = TextAlignment.TopCenter;
            //this.xrLabel26.Text = "марка          держ. №";
            //this.xrLabel26.Font = new Font( dFont, 8, FontStyle.Italic );
            //this.xrLabel26.Borders = visibleBorders;

            //this.xrLabel27 = new XRLabel();
            //this.xrLabel27.Location = new Point( 60, 129 );
            //this.xrLabel27.Size = new Size( 250, 5 );
            //this.xrLabel27.WordWrap = false;
            //this.xrLabel27.Multiline = true;
            //this.xrLabel27.TextAlignment = TextAlignment.TopCenter;
            //this.xrLabel27.Text = "марка          держ. №";
            //this.xrLabel27.Font = new Font( dFont, 8, FontStyle.Italic );
            //this.xrLabel27.Borders = visibleBorders;

            this.xrTable1 = new XRTable();
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable1.Location = new Point( 520, 50 );
            this.xrTable1.Size = new Size( 448, 80 );
            this.xrTable1.BeginInit();

            for ( int i = 0; i < 4; i++ )
            {
                XRTableRow row = new XRTableRow();
                row = new XRTableRow();
                for ( int j = 0; j < 6; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable1.Rows.Add( row );
            }

            this.xrTable1.EndInit();
            this.xrTable1.Rows[0].Cells[0].Text = "операція";
            this.xrTable1.Rows[1].Cells[0].Text = "1";
            this.xrTable1.Rows[0].Cells[1].Text = "час за графіком\nгод.";
            this.xrTable1.Rows[1].Cells[1].Text = "2";
            this.xrTable1.Rows[0].Cells[2].Text = "час за графіком\nхв.";
            this.xrTable1.Rows[1].Cells[2].Text = "3";
            this.xrTable1.Rows[0].Cells[3].Text = "пул. пробіг км.";
            this.xrTable1.Rows[1].Cells[3].Text = "4";
            this.xrTable1.Rows[0].Cells[4].Text = "показ\nспідометра";
            this.xrTable1.Rows[1].Cells[4].Text = "5";
            this.xrTable1.Rows[0].Cells[5].Text = "час фактичний\nчас, міс., год., хв.";
            this.xrTable1.Rows[1].Cells[5].Text = "6";
            this.xrTable1.Rows[2].Cells[0].Text = "виїзд із гаража";
            this.xrTable1.Rows[3].Cells[0].Text = "повернення в гараж";

            this.xrTable2 = new XRTable();
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable2.Location = new Point( 520, 140 );
            this.xrTable2.Size = new Size( 448, 100 );
            this.xrTable2.BeginInit();

            for ( int i = 0; i < 4; i++ )
            {
                XRTableRow row = new XRTableRow();
                for ( int j = 0; j < 7; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable2.Rows.Add( row );
            }

            this.xrTable2.EndInit();
            this.xrTable2.Rows[0].Cells[0].Text = "марки\nпального";
            this.xrTable2.Rows[1].Cells[0].Text = "7";
            this.xrTable2.Rows[0].Cells[1].Text = "код\nмарки";
            this.xrTable2.Rows[1].Cells[1].Text = "8";
            this.xrTable2.Rows[0].Cells[2].Text = "видано";
            this.xrTable2.Rows[1].Cells[2].Text = "9";
            this.xrTable2.Rows[0].Cells[3].Text = "залишок при\nвиїзді";
            this.xrTable2.Rows[1].Cells[3].Text = "10";
            this.xrTable2.Rows[0].Cells[4].Text = "залишок при\nповерненні";
            this.xrTable2.Rows[1].Cells[4].Text = "11";
            this.xrTable2.Rows[0].Cells[5].Text = "час роб., год.\nспецустат";
            this.xrTable2.Rows[1].Cells[5].Text = "12";
            this.xrTable2.Rows[0].Cells[6].Text = "час роб., год.\nдвигуна";
            this.xrTable2.Rows[1].Cells[6].Text = "13";

            this.xrTable4 = new XRTable();
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable4.Location = new Point( 520, 240 );
            this.xrTable4.Size = new Size( 448, 40 );
            this.xrTable4.BeginInit();

            for ( int i = 0; i < 2; i++ )
            {
                XRTableRow row = new XRTableRow();
                for ( int j = 0; j < 5; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable4.Rows.Add( row );
            }

            this.xrTable4.EndInit();
            this.xrTable4.Rows[0].Cells[0].Text = "посада";
            this.xrTable4.Rows[0].Cells[1].Text = "заправник";
            this.xrTable4.Rows[0].Cells[2].Text = "механік";
            this.xrTable4.Rows[0].Cells[3].Text = "механік";
            this.xrTable4.Rows[0].Cells[4].Text = "диспетчер";
            this.xrTable4.Rows[1].Cells[0].Text = "підписи";

            this.xrTable3 = new XRTable();
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.All;
            this.xrTable3.Location = new Point( 0, 289 );
            this.xrTable3.Size = new Size( 968, 180 );
            this.xrTable3.BeginInit();

            for ( int i = 0; i < 9; i++ )
            {
                XRTableRow row = new XRTableRow();
                for ( int j = 0; j < 10; j++ )
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Font = new Font( dFont, 9, FontStyle.Regular );
                    cell.TextAlignment = TextAlignment.MiddleCenter;
                    cell.WordWrap = true;
                    cell.Multiline = true;
                    row.Cells.Add( cell );
                }
                this.xrTable3.Rows.Add( row );
            } // for

            this.xrTable3.EndInit();
            this.xrTable3.Rows[0].Cells[0].Text = "в чиє розпорядження";
            this.xrTable3.Rows[1].Cells[0].Text = "14";
            this.xrTable3.Rows[0].Cells[1].Text = "час\nприбуття";
            this.xrTable3.Rows[1].Cells[1].Text = "15";
            this.xrTable3.Rows[0].Cells[2].Text = "час\nвибуття";
            this.xrTable3.Rows[1].Cells[2].Text = "16";
            this.xrTable3.Rows[0].Cells[3].Text = "кількість годин";
            this.xrTable3.Rows[1].Cells[3].Text = "17";
            this.xrTable3.Rows[0].Cells[4].Text = "звідки взяти\nвантаж";
            this.xrTable3.Rows[1].Cells[4].Text = "18";
            this.xrTable3.Rows[0].Cells[5].Text = "куди доставити\nвантаж";
            this.xrTable3.Rows[1].Cells[5].Text = "19";
            this.xrTable3.Rows[0].Cells[6].Text = "найменування\nвантажу";
            this.xrTable3.Rows[1].Cells[6].Text = "20";
            this.xrTable3.Rows[0].Cells[7].Text = "кільк. поїздок\nз вантажем";
            this.xrTable3.Rows[1].Cells[7].Text = "21";
            this.xrTable3.Rows[0].Cells[8].Text = "відстань,\nкм";
            this.xrTable3.Rows[1].Cells[8].Text = "22";
            this.xrTable3.Rows[0].Cells[9].Text = "перевозки,\nтонн";
            this.xrTable3.Rows[1].Cells[9].Text = "23";
            this.xrTable3.Rows[8].Cells[6].Text = "Разом";
            this.xrTable3.Rows[8].Cells[0].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[1].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[2].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[3].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[4].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[5].Borders = BorderSide.Top;
            this.xrTable3.Rows[8].Cells[5].Borders = BorderSide.Right;

            this.Detail.Controls.Add( this.xrLabel1 );
            this.Detail.Controls.Add( this.xrLabel2 );
            this.Detail.Controls.Add( this.xrLabel3 );
            this.Detail.Controls.Add( this.xrLabel4 );
            this.Detail.Controls.Add( this.xrLabel5 );
            this.Detail.Controls.Add( this.xrLabel6 );
            this.Detail.Controls.Add( this.xrLabel7 );
            this.Detail.Controls.Add( this.xrLabel8 );
            this.Detail.Controls.Add( this.xrLabel9 );
            this.Detail.Controls.Add( this.xrLabel10 );
            this.Detail.Controls.Add( this.xrLabel11 );
            this.Detail.Controls.Add( this.xrLabel12 );
            this.Detail.Controls.Add( this.xrLabel13 );
            this.Detail.Controls.Add( this.xrLabel14 );
            this.Detail.Controls.Add( this.xrLabel15 );
            this.Detail.Controls.Add( this.xrLabel16 );
            this.Detail.Controls.Add( this.xrLabel17 );
            this.Detail.Controls.Add( this.xrLabel18 );
            this.Detail.Controls.Add( this.xrLabel19 );
            this.Detail.Controls.Add( this.xrLabel20 );
            this.Detail.Controls.Add( this.xrLabel21 );
            this.Detail.Controls.Add( this.xrLabel22 );
            this.Detail.Controls.Add( this.xrLabel23 );
            //this.Detail.Controls.Add( this.xrLabel24 );
            //this.Detail.Controls.Add( this.xrLabel25 );
            //this.Detail.Controls.Add( this.xrLabel26 );
            //this.Detail.Controls.Add( this.xrLabel27 );
            this.Detail.Controls.Add( this.xrTable1 );
            this.Detail.Controls.Add( this.xrTable2 );
            this.Detail.Controls.Add( this.xrTable3 );
            this.Detail.Controls.Add( this.xrTable4 );
        } // WayReportPageOne

        public void setDataForWaySheet( MotorDepot.waybill Row )
        {
            this.xrLabel1.Text = "ПОДОРОЖНІЙ ЛИСТ № " + Row.Id + "\n" + "ВАНТАЖНОГО АВТОМОБІЛЯ";
            DateTime dataTime = Row.Date;
            this.xrLabel2.Text = dataTime.Day + "  " + Month[dataTime.Month - 1] + "  " + dataTime.Year + " р.";

            this.xrLabel8.Text = "Автомобіль: " + Row.vehicle.NumberPlate + ",   " + Row.vehicle.MakeCar + ",   " +
                Row.vehicle.CarModel + ". " + "Гар. №";

            this.xrLabel9.Text = "Водій: " + Row.driver.Family + ",  категорія:  " + "\"" + Row.driver.Category + "\"" + ". " + "Tаб. №";
        } // setDataForWaySheet
    } // WayReportPageOne
} // MotorDepot.DifferentSheets