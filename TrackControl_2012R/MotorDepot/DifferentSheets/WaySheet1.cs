using System;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using MotorDepot.Properties;

namespace MotorDepot
{
    /// <summary>
    /// ������� ���� �������� 1-�.
    /// </summary>
    public partial class WaySheet1 : DevExpress.XtraScheduler.Reporting.XtraSchedulerReport
    {
        int countColumn = 0; // ������� �������� - ��������
        int iCountWaySheet = 0; // �������� �� �� ������� ������� �����

        public struct TInfoAuto // �������� �� �� ������� ����������
        {
            public string modelAuto;
            public string registerNumber;
            public string typeAuto;
            public string numberGarage;
        }

        public struct TInfoDriver // �������� �� �� ������� ��������
        {
            public string FIO;
            public string numberService;
            public string classDriver;
            public string numDriverTable;
        }

        public WaySheet1()
        {
            InitializeComponent();
            InitializeTableReport();
            InitializeActiveRecordReport();
        } // WaySheet1

        // ��������� �������� ����� ������
        protected void InitializeActiveRecordReport()
        {
            TInfoAuto infoAuto = new TInfoAuto();
            TInfoDriver infoDriver = new TInfoDriver();

            labelNumberSheet.Text = iCountWaySheet.ToString();

            labelModelAuto.Text = infoAuto.modelAuto;
            labelRegNumber.Text = infoAuto.registerNumber;
            labelTypeAuto.Text = infoAuto.typeAuto;
            textGarageNumber.Text = infoAuto.numberGarage;

            labelFIO.Text = infoDriver.FIO;
            labelNumService.Text = infoDriver.numberService;
            labelClassDriver.Text = infoDriver.classDriver;
            textTabelDriver.Text = infoDriver.numDriverTable;

            DateTime date = DateTime.Now.Date;
            labelDate.Text = date.ToString( "dd_MM_yyyy" ) + " " + Resources.YearLetter;
        } // InitializeActiveRecordReport

        // ����������� ����� ������ ������
        protected void InitializeTableReport()
        {
            var dT3 = new DataTable();
            dT3.Columns.AddRange( new[]
                {
                    new DataColumn("Operation", typeof (string)),
                    new DataColumn("TimeHour", typeof (string)),
                    new DataColumn("TimeMinute", typeof (string)),
                    new DataColumn("Travel", typeof (string)),
                    new DataColumn("Speedometer", typeof (string)),
                    new DataColumn("TimeFact", typeof (string)),
                } );

            for ( var i = 0 ; i < 3; i++ )
            {
                var dr = dT3.NewRow();
                dT3.Rows.Add( dr );
            }

            DataRow row = dT3.Rows[0];
            for ( int i = 0; i < dT3.Columns.Count; i++ )
            {
                row[i] = countColumn++;
            }

            gControlWorkDriverAuto.DataSource = dT3;

            var dT2 = new DataTable();
            dT2.Columns.AddRange( new[]
                {
                    new DataColumn("FuelMark", typeof (string)),
                    new DataColumn("CodeMark", typeof (string)),
                    new DataColumn("Issue", typeof (string)),
                    new DataColumn("BalanceIn", typeof (string)),
                    new DataColumn("BalanceOut", typeof (string)),
                    new DataColumn("TimeSpecific", typeof (string)),
                    new DataColumn("TimeMotor", typeof (string)),
                } );

            for ( var i = 0 ; i < 3; i++ )
            {
                var dr = dT2.NewRow();
                dT2.Rows.Add( dr );
            }

            row = dT2.Rows[0];
            for ( int i = 0 ; i < dT2.Columns.Count ; i++ )
            {
                row[i] = countColumn++;
            }

            gControlFuelTravel.DataSource = dT2;

            var dT4 = new DataTable();
            dT4.Columns.AddRange( new[]
                {
                    new DataColumn("Fueler", typeof (string)),
                    new DataColumn("Mechanic1", typeof (string)),
                    new DataColumn("Mechanic2", typeof (string)),
                    new DataColumn("Dispatcher", typeof (string)),
                } );

            for ( var i = 0 ; i < 1; i++ )
            {
                var dr = dT4.NewRow();
                dT4.Rows.Add( dr );
            }

            row = dT4.Rows[0];
            for ( int i = 0 ; i < dT4.Columns.Count ; i++ )
            {
                row[i] = countColumn++;
            }

            colControlSignature.DataSource = dT4;

            var dT1 = new DataTable();
            dT1.Columns.AddRange( new[]
                {
                    new DataColumn("Owner", typeof (string)),
                    new DataColumn("TimeIn", typeof (string)),
                    new DataColumn("TimeOut", typeof (string)),
                    new DataColumn("CounterTime", typeof (string)),
                    new DataColumn("TakeWeight", typeof (string)),
                    new DataColumn("OutWeight", typeof (string)),
                    new DataColumn("NameWeight", typeof (string)),
                    new DataColumn("NumberTravelWeight", typeof (string)),
                    new DataColumn("Travel", typeof (string)),
                    new DataColumn("TravelWeight", typeof (string)),
                } );

            for ( var i = 0 ; i < 8; i++ )
            {
                var dr = dT1.NewRow();
                dT1.Rows.Add( dr );
            }

            row = dT1.Rows[0];
            for ( int i = 0 ; i < dT1.Columns.Count ; i++ )
            {
                row[i] = countColumn++;
            }

            gControlTaskDriver.DataSource = dT1;
        } // InitializeTableReport
    } // WaySheet1
} // MotorDepot
