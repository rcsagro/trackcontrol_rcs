namespace MotorDepot
{
    partial class XtraSchedulerReport2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( XtraSchedulerReport2 ) );
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gridControlTask = new DevExpress.XtraGrid.GridControl();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNumberTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTikets = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTimeAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkKTonn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescribeWorker = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelOutSide = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer6 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer7 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer8 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer9 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer10 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer11 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer12 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer13 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer14 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gridControlResultWork = new DevExpress.XtraGrid.GridControl();
            this.winControlContainer15 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBalanceFuelNorm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWorkAuto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWorkTrailer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWorkAutoMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWorkAutoStopLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWorkAutoCrach = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberTravelWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TravelAutoAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TravelTrailerAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelAutoWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelTrailerWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightTravelAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeightTrailerAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkKTonnAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkKTonnTrailerAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalaryCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalarySumma = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceFuelFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer16 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer17 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer18 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer19 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer20 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer21 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer22 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer23 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer24 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.winControlContainer25 = new DevExpress.XtraReports.UI.WinControlContainer();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlTask ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlResultWork ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this ) ).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange( new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1,
            this.winControlContainer2,
            this.winControlContainer3,
            this.winControlContainer4,
            this.winControlContainer5,
            this.winControlContainer6,
            this.winControlContainer7,
            this.winControlContainer8,
            this.winControlContainer9,
            this.winControlContainer10,
            this.winControlContainer11,
            this.winControlContainer12,
            this.winControlContainer13,
            this.winControlContainer14,
            this.winControlContainer15,
            this.winControlContainer16,
            this.winControlContainer17,
            this.winControlContainer18,
            this.winControlContainer19,
            this.winControlContainer20,
            this.winControlContainer21,
            this.winControlContainer22,
            this.winControlContainer23,
            this.winControlContainer24,
            this.winControlContainer25} );
            this.Detail.Height = 825;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo( 0, 0, 0, 0, 100F );
            resources.ApplyResources( this.Detail, "Detail" );
            // 
            // labelControl1
            // 
            this.labelControl1.AccessibleDescription = null;
            this.labelControl1.AccessibleName = null;
            resources.ApplyResources( this.labelControl1, "labelControl1" );
            this.labelControl1.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Name = "labelControl1";
            // 
            // winControlContainer1
            // 
            resources.ApplyResources( this.winControlContainer1, "winControlContainer1" );
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.WinControl = this.labelControl1;
            // 
            // gridControlTask
            // 
            this.gridControlTask.AccessibleDescription = null;
            this.gridControlTask.AccessibleName = null;
            resources.ApplyResources( this.gridControlTask, "gridControlTask" );
            this.gridControlTask.BackgroundImage = null;
            this.gridControlTask.EmbeddedNavigator.AccessibleDescription = null;
            this.gridControlTask.EmbeddedNavigator.AccessibleName = null;
            this.gridControlTask.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.gridControlTask.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.Anchor" ) ) );
            this.gridControlTask.EmbeddedNavigator.BackgroundImage = null;
            this.gridControlTask.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.gridControlTask.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.ImeMode" ) ) );
            this.gridControlTask.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.TextLocation" ) ) );
            this.gridControlTask.EmbeddedNavigator.ToolTip = resources.GetString( "gridControl1.EmbeddedNavigator.ToolTip" );
            this.gridControlTask.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "gridControl1.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.gridControlTask.EmbeddedNavigator.ToolTipTitle = resources.GetString( "gridControl1.EmbeddedNavigator.ToolTipTitle" );
            this.gridControlTask.Font = null;
            this.gridControlTask.MainView = this.gridView1;
            this.gridControlTask.Name = "gridControlTask";
            this.gridControlTask.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1} );
            // 
            // winControlContainer2
            // 
            resources.ApplyResources( this.winControlContainer2, "winControlContainer2" );
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.WinControl = this.gridControlTask;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.gridView1, "gridView1" );
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNumberTravel,
            this.colTikets,
            this.colWorkTimeAll,
            this.colWeightTravel,
            this.colWorkKTonn,
            this.colDescribeWorker,
            this.colTravelFrom,
            this.colTravelOutSide} );
            this.gridView1.GridControl = this.gridControlTask;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.AutoWidth = false;
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsPrint.PrintFooter = false;
            this.gridView1.OptionsPrint.PrintGroupFooter = false;
            this.gridView1.OptionsPrint.UsePrintStyles = true;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // colNumberTravel
            // 
            this.colNumberTravel.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colNumberTravel.AppearanceCell.Options.UseForeColor = true;
            this.colNumberTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colNumberTravel, "colNumberTravel" );
            this.colNumberTravel.FieldName = "NumberTravel";
            this.colNumberTravel.Name = "colNumberTravel";
            // 
            // colTikets
            // 
            this.colTikets.AppearanceCell.Options.UseTextOptions = true;
            this.colTikets.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTikets.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTikets.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTikets.AppearanceHeader.Options.UseTextOptions = true;
            this.colTikets.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTikets.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTikets.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTikets, "colTikets" );
            this.colTikets.FieldName = "Tikets";
            this.colTikets.Name = "colTikets";
            // 
            // colWorkTimeAll
            // 
            this.colWorkTimeAll.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkTimeAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTimeAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkTimeAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkTimeAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkTimeAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkTimeAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkTimeAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWorkTimeAll, "colWorkTimeAll" );
            this.colWorkTimeAll.FieldName = "WorkTimeAll";
            this.colWorkTimeAll.Name = "colWorkTimeAll";
            // 
            // colWeightTravel
            // 
            this.colWeightTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colWeightTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeightTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeightTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWeightTravel, "colWeightTravel" );
            this.colWeightTravel.FieldName = "WeightTravel";
            this.colWeightTravel.Name = "colWeightTravel";
            // 
            // colWorkKTonn
            // 
            this.colWorkKTonn.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkKTonn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkKTonn.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkKTonn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWorkKTonn, "colWorkKTonn" );
            this.colWorkKTonn.FieldName = "WorkKTonn";
            this.colWorkKTonn.Name = "colWorkKTonn";
            // 
            // colDescribeWorker
            // 
            this.colDescribeWorker.AppearanceCell.Options.UseTextOptions = true;
            this.colDescribeWorker.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescribeWorker.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDescribeWorker.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDescribeWorker.AppearanceHeader.Options.UseTextOptions = true;
            this.colDescribeWorker.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDescribeWorker.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDescribeWorker.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colDescribeWorker, "colDescribeWorker" );
            this.colDescribeWorker.FieldName = "DescribeWorker";
            this.colDescribeWorker.Name = "colDescribeWorker";
            // 
            // colTravelFrom
            // 
            this.colTravelFrom.AppearanceCell.Options.UseTextOptions = true;
            this.colTravelFrom.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelFrom.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelFrom.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelFrom.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelFrom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelFrom.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelFrom.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravelFrom, "colTravelFrom" );
            this.colTravelFrom.FieldName = "TravelFrom";
            this.colTravelFrom.Name = "colTravelFrom";
            // 
            // colTravelOutSide
            // 
            this.colTravelOutSide.AppearanceCell.Options.UseTextOptions = true;
            this.colTravelOutSide.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelOutSide.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelOutSide.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelOutSide.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelOutSide.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelOutSide.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelOutSide.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravelOutSide, "colTravelOutSide" );
            this.colTravelOutSide.FieldName = "TravelOutSide";
            this.colTravelOutSide.Name = "colTravelOutSide";
            // 
            // labelControl2
            // 
            this.labelControl2.AccessibleDescription = null;
            this.labelControl2.AccessibleName = null;
            resources.ApplyResources( this.labelControl2, "labelControl2" );
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.MaximumSize = new System.Drawing.Size( 80, 40 );
            this.labelControl2.Name = "labelControl2";
            // 
            // winControlContainer3
            // 
            resources.ApplyResources( this.winControlContainer3, "winControlContainer3" );
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.WinControl = this.labelControl2;
            // 
            // labelControl3
            // 
            this.labelControl3.AccessibleDescription = null;
            this.labelControl3.AccessibleName = null;
            resources.ApplyResources( this.labelControl3, "labelControl3" );
            this.labelControl3.Name = "labelControl3";
            // 
            // winControlContainer4
            // 
            resources.ApplyResources( this.winControlContainer4, "winControlContainer4" );
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.WinControl = this.labelControl3;
            // 
            // labelControl4
            // 
            this.labelControl4.AccessibleDescription = null;
            this.labelControl4.AccessibleName = null;
            resources.ApplyResources( this.labelControl4, "labelControl4" );
            this.labelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl4.Name = "labelControl4";
            // 
            // winControlContainer5
            // 
            resources.ApplyResources( this.winControlContainer5, "winControlContainer5" );
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.WinControl = this.labelControl4;
            // 
            // labelControl5
            // 
            this.labelControl5.AccessibleDescription = null;
            this.labelControl5.AccessibleName = null;
            resources.ApplyResources( this.labelControl5, "labelControl5" );
            this.labelControl5.Name = "labelControl5";
            // 
            // winControlContainer6
            // 
            resources.ApplyResources( this.winControlContainer6, "winControlContainer6" );
            this.winControlContainer6.Name = "winControlContainer6";
            this.winControlContainer6.WinControl = this.labelControl5;
            // 
            // labelControl6
            // 
            this.labelControl6.AccessibleDescription = null;
            this.labelControl6.AccessibleName = null;
            resources.ApplyResources( this.labelControl6, "labelControl6" );
            this.labelControl6.Name = "labelControl6";
            // 
            // winControlContainer7
            // 
            resources.ApplyResources( this.winControlContainer7, "winControlContainer7" );
            this.winControlContainer7.Name = "winControlContainer7";
            this.winControlContainer7.WinControl = this.labelControl6;
            // 
            // labelControl7
            // 
            this.labelControl7.AccessibleDescription = null;
            this.labelControl7.AccessibleName = null;
            resources.ApplyResources( this.labelControl7, "labelControl7" );
            this.labelControl7.Name = "labelControl7";
            // 
            // winControlContainer8
            // 
            resources.ApplyResources( this.winControlContainer8, "winControlContainer8" );
            this.winControlContainer8.Name = "winControlContainer8";
            this.winControlContainer8.WinControl = this.labelControl7;
            // 
            // labelControl8
            // 
            this.labelControl8.AccessibleDescription = null;
            this.labelControl8.AccessibleName = null;
            resources.ApplyResources( this.labelControl8, "labelControl8" );
            this.labelControl8.Name = "labelControl8";
            // 
            // winControlContainer9
            // 
            resources.ApplyResources( this.winControlContainer9, "winControlContainer9" );
            this.winControlContainer9.Name = "winControlContainer9";
            this.winControlContainer9.WinControl = this.labelControl8;
            // 
            // labelControl9
            // 
            this.labelControl9.AccessibleDescription = null;
            this.labelControl9.AccessibleName = null;
            resources.ApplyResources( this.labelControl9, "labelControl9" );
            this.labelControl9.Name = "labelControl9";
            // 
            // winControlContainer10
            // 
            resources.ApplyResources( this.winControlContainer10, "winControlContainer10" );
            this.winControlContainer10.Name = "winControlContainer10";
            this.winControlContainer10.WinControl = this.labelControl9;
            // 
            // labelControl10
            // 
            this.labelControl10.AccessibleDescription = null;
            this.labelControl10.AccessibleName = null;
            resources.ApplyResources( this.labelControl10, "labelControl10" );
            this.labelControl10.Name = "labelControl10";
            // 
            // winControlContainer11
            // 
            resources.ApplyResources( this.winControlContainer11, "winControlContainer11" );
            this.winControlContainer11.Name = "winControlContainer11";
            this.winControlContainer11.WinControl = this.labelControl10;
            // 
            // labelControl11
            // 
            this.labelControl11.AccessibleDescription = null;
            this.labelControl11.AccessibleName = null;
            resources.ApplyResources( this.labelControl11, "labelControl11" );
            this.labelControl11.Name = "labelControl11";
            // 
            // winControlContainer12
            // 
            resources.ApplyResources( this.winControlContainer12, "winControlContainer12" );
            this.winControlContainer12.Name = "winControlContainer12";
            this.winControlContainer12.WinControl = this.labelControl11;
            // 
            // labelControl12
            // 
            this.labelControl12.AccessibleDescription = null;
            this.labelControl12.AccessibleName = null;
            resources.ApplyResources( this.labelControl12, "labelControl12" );
            this.labelControl12.Name = "labelControl12";
            // 
            // winControlContainer13
            // 
            resources.ApplyResources( this.winControlContainer13, "winControlContainer13" );
            this.winControlContainer13.Name = "winControlContainer13";
            this.winControlContainer13.WinControl = this.labelControl12;
            // 
            // labelControl13
            // 
            this.labelControl13.AccessibleDescription = null;
            this.labelControl13.AccessibleName = null;
            resources.ApplyResources( this.labelControl13, "labelControl13" );
            this.labelControl13.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Name = "labelControl13";
            // 
            // winControlContainer14
            // 
            resources.ApplyResources( this.winControlContainer14, "winControlContainer14" );
            this.winControlContainer14.Name = "winControlContainer14";
            this.winControlContainer14.WinControl = this.labelControl13;
            // 
            // gridControlResultWork
            // 
            this.gridControlResultWork.AccessibleDescription = null;
            this.gridControlResultWork.AccessibleName = null;
            resources.ApplyResources( this.gridControlResultWork, "gridControlResultWork" );
            this.gridControlResultWork.BackgroundImage = null;
            this.gridControlResultWork.EmbeddedNavigator.AccessibleDescription = null;
            this.gridControlResultWork.EmbeddedNavigator.AccessibleName = null;
            this.gridControlResultWork.EmbeddedNavigator.AllowHtmlTextInToolTip = ( ( DevExpress.Utils.DefaultBoolean ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.AllowHtmlTextInToolTip" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.Anchor = ( ( System.Windows.Forms.AnchorStyles ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.Anchor" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.BackgroundImage = null;
            this.gridControlResultWork.EmbeddedNavigator.BackgroundImageLayout = ( ( System.Windows.Forms.ImageLayout ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.BackgroundImageLayout" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.ImeMode = ( ( System.Windows.Forms.ImeMode ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.ImeMode" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.TextLocation = ( ( DevExpress.XtraEditors.NavigatorButtonsTextLocation ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.TextLocation" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.ToolTip = resources.GetString( "gridControl2.EmbeddedNavigator.ToolTip" );
            this.gridControlResultWork.EmbeddedNavigator.ToolTipIconType = ( ( DevExpress.Utils.ToolTipIconType ) ( resources.GetObject( "gridControl2.EmbeddedNavigator.ToolTipIconType" ) ) );
            this.gridControlResultWork.EmbeddedNavigator.ToolTipTitle = resources.GetString( "gridControl2.EmbeddedNavigator.ToolTipTitle" );
            this.gridControlResultWork.Font = null;
            this.gridControlResultWork.MainView = this.gridView2;
            this.gridControlResultWork.Name = "gridControlResultWork";
            this.gridControlResultWork.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2} );
            // 
            // winControlContainer15
            // 
            resources.ApplyResources( this.winControlContainer15, "winControlContainer15" );
            this.winControlContainer15.Name = "winControlContainer15";
            this.winControlContainer15.WinControl = this.gridControlResultWork;
            // 
            // gridView2
            // 
            this.gridView2.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView2.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView2.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView2.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.gridView2, "gridView2" );
            this.gridView2.ColumnPanelRowHeight = 80;
            this.gridView2.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBalanceFuelNorm,
            this.colBalanceFuelFact,
            this.colTimeWorkAuto,
            this.colTimeWorkTrailer,
            this.colTimeWorkAutoMove,
            this.colTimeWorkAutoStopLine,
            this.colTimeWorkAutoCrach,
            this.colNumberTravelWeight,
            this.TravelAutoAll,
            this.TravelTrailerAll,
            this.colTravelAutoWeight,
            this.colTravelTrailerWeight,
            this.colWeightTravelAll,
            this.colWeightTrailerAll,
            this.colWorkKTonnAll,
            this.colWorkKTonnTrailerAll,
            this.colSalaryCode,
            this.colSalarySumma} );
            this.gridView2.GridControl = this.gridControlResultWork;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsPrint.AutoWidth = false;
            this.gridView2.OptionsPrint.EnableAppearanceOddRow = true;
            this.gridView2.OptionsPrint.ExpandAllGroups = false;
            this.gridView2.OptionsPrint.PrintFooter = false;
            this.gridView2.OptionsPrint.PrintGroupFooter = false;
            this.gridView2.OptionsPrint.UsePrintStyles = true;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.EnableAppearanceOddRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // colBalanceFuelNorm
            // 
            this.colBalanceFuelNorm.AppearanceCell.Options.UseTextOptions = true;
            this.colBalanceFuelNorm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceFuelNorm.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceFuelNorm.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBalanceFuelNorm.AppearanceHeader.Options.UseTextOptions = true;
            this.colBalanceFuelNorm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceFuelNorm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceFuelNorm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colBalanceFuelNorm, "colBalanceFuelNorm" );
            this.colBalanceFuelNorm.FieldName = "BalanceFuelNorm";
            this.colBalanceFuelNorm.Name = "colBalanceFuelNorm";
            // 
            // colTimeWorkAuto
            // 
            this.colTimeWorkAuto.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWorkAuto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAuto.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAuto.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWorkAuto.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWorkAuto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAuto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAuto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeWorkAuto, "colTimeWorkAuto" );
            this.colTimeWorkAuto.FieldName = "TimeWorkAuto";
            this.colTimeWorkAuto.Name = "colTimeWorkAuto";
            // 
            // colTimeWorkTrailer
            // 
            this.colTimeWorkTrailer.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWorkTrailer.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkTrailer.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkTrailer.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWorkTrailer.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWorkTrailer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkTrailer.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkTrailer.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeWorkTrailer, "colTimeWorkTrailer" );
            this.colTimeWorkTrailer.FieldName = "TimeWorkTrailer";
            this.colTimeWorkTrailer.Name = "colTimeWorkTrailer";
            // 
            // colTimeWorkAutoMove
            // 
            this.colTimeWorkAutoMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWorkAutoMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWorkAutoMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWorkAutoMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoMove.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeWorkAutoMove, "colTimeWorkAutoMove" );
            this.colTimeWorkAutoMove.FieldName = "TimeWorkAutoMove";
            this.colTimeWorkAutoMove.Name = "colTimeWorkAutoMove";
            // 
            // colTimeWorkAutoStopLine
            // 
            this.colTimeWorkAutoStopLine.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWorkAutoStopLine.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoStopLine.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoStopLine.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWorkAutoStopLine.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWorkAutoStopLine.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoStopLine.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoStopLine.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeWorkAutoStopLine, "colTimeWorkAutoStopLine" );
            this.colTimeWorkAutoStopLine.FieldName = "TimeWorkAutoStopLine";
            this.colTimeWorkAutoStopLine.Name = "colTimeWorkAutoStopLine";
            // 
            // colTimeWorkAutoCrach
            // 
            this.colTimeWorkAutoCrach.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWorkAutoCrach.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoCrach.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoCrach.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWorkAutoCrach.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWorkAutoCrach.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWorkAutoCrach.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWorkAutoCrach.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTimeWorkAutoCrach, "colTimeWorkAutoCrach" );
            this.colTimeWorkAutoCrach.FieldName = "TimeWorkAutoCrach";
            this.colTimeWorkAutoCrach.Name = "colTimeWorkAutoCrach";
            // 
            // colNumberTravelWeight
            // 
            this.colNumberTravelWeight.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberTravelWeight.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberTravelWeight.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberTravelWeight.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberTravelWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumberTravelWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colNumberTravelWeight, "colNumberTravelWeight" );
            this.colNumberTravelWeight.FieldName = "NumberTravelWeight";
            this.colNumberTravelWeight.Name = "colNumberTravelWeight";
            // 
            // TravelAutoAll
            // 
            this.TravelAutoAll.AppearanceCell.Options.UseTextOptions = true;
            this.TravelAutoAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TravelAutoAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TravelAutoAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TravelAutoAll.AppearanceHeader.Options.UseTextOptions = true;
            this.TravelAutoAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TravelAutoAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TravelAutoAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.TravelAutoAll, "TravelAutoAll" );
            this.TravelAutoAll.FieldName = "TravelAutoAll";
            this.TravelAutoAll.Name = "TravelAutoAll";
            // 
            // TravelTrailerAll
            // 
            this.TravelTrailerAll.AppearanceCell.Options.UseTextOptions = true;
            this.TravelTrailerAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TravelTrailerAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TravelTrailerAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TravelTrailerAll.AppearanceHeader.Options.UseTextOptions = true;
            this.TravelTrailerAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TravelTrailerAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TravelTrailerAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.TravelTrailerAll, "TravelTrailerAll" );
            this.TravelTrailerAll.FieldName = "TravelTrailerAll";
            this.TravelTrailerAll.Name = "TravelTrailerAll";
            // 
            // colTravelAutoWeight
            // 
            this.colTravelAutoWeight.AppearanceCell.Options.UseTextOptions = true;
            this.colTravelAutoWeight.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelAutoWeight.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelAutoWeight.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelAutoWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelAutoWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelAutoWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelAutoWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravelAutoWeight, "colTravelAutoWeight" );
            this.colTravelAutoWeight.FieldName = "TravelAutoWeight";
            this.colTravelAutoWeight.Name = "colTravelAutoWeight";
            // 
            // colTravelTrailerWeight
            // 
            this.colTravelTrailerWeight.AppearanceCell.Options.UseTextOptions = true;
            this.colTravelTrailerWeight.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelTrailerWeight.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelTrailerWeight.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelTrailerWeight.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelTrailerWeight.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelTrailerWeight.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelTrailerWeight.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colTravelTrailerWeight, "colTravelTrailerWeight" );
            this.colTravelTrailerWeight.FieldName = "TravelTrailerWeight";
            this.colTravelTrailerWeight.Name = "colTravelTrailerWeight";
            // 
            // colWeightTravelAll
            // 
            this.colWeightTravelAll.AppearanceCell.Options.UseTextOptions = true;
            this.colWeightTravelAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTravelAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTravelAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeightTravelAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeightTravelAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTravelAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTravelAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWeightTravelAll, "colWeightTravelAll" );
            this.colWeightTravelAll.FieldName = "WeightTravelAll";
            this.colWeightTravelAll.Name = "colWeightTravelAll";
            // 
            // colWeightTrailerAll
            // 
            this.colWeightTrailerAll.AppearanceCell.Options.UseTextOptions = true;
            this.colWeightTrailerAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTrailerAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTrailerAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWeightTrailerAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colWeightTrailerAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWeightTrailerAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWeightTrailerAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWeightTrailerAll, "colWeightTrailerAll" );
            this.colWeightTrailerAll.FieldName = "WeightTrailerAll";
            this.colWeightTrailerAll.Name = "colWeightTrailerAll";
            // 
            // colWorkKTonnAll
            // 
            this.colWorkKTonnAll.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkKTonnAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonnAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonnAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkKTonnAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkKTonnAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonnAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonnAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWorkKTonnAll, "colWorkKTonnAll" );
            this.colWorkKTonnAll.FieldName = "WorkKTonnAll";
            this.colWorkKTonnAll.Name = "colWorkKTonnAll";
            // 
            // colWorkKTonnTrailerAll
            // 
            this.colWorkKTonnTrailerAll.AppearanceCell.Options.UseTextOptions = true;
            this.colWorkKTonnTrailerAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonnTrailerAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonnTrailerAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWorkKTonnTrailerAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colWorkKTonnTrailerAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWorkKTonnTrailerAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWorkKTonnTrailerAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colWorkKTonnTrailerAll, "colWorkKTonnTrailerAll" );
            this.colWorkKTonnTrailerAll.FieldName = "WorkKTonnTrailerAll";
            this.colWorkKTonnTrailerAll.Name = "colWorkKTonnTrailerAll";
            // 
            // colSalaryCode
            // 
            this.colSalaryCode.AppearanceCell.Options.UseTextOptions = true;
            this.colSalaryCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalaryCode.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSalaryCode.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSalaryCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colSalaryCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalaryCode.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSalaryCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colSalaryCode, "colSalaryCode" );
            this.colSalaryCode.FieldName = "SalaryCode";
            this.colSalaryCode.Name = "colSalaryCode";
            // 
            // colSalarySumma
            // 
            this.colSalarySumma.AppearanceCell.Options.UseTextOptions = true;
            this.colSalarySumma.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalarySumma.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSalarySumma.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSalarySumma.AppearanceHeader.Options.UseTextOptions = true;
            this.colSalarySumma.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalarySumma.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSalarySumma.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colSalarySumma, "colSalarySumma" );
            this.colSalarySumma.FieldName = "SalarySumma";
            this.colSalarySumma.Name = "colSalarySumma";
            // 
            // colBalanceFuelFact
            // 
            this.colBalanceFuelFact.AppearanceCell.Options.UseTextOptions = true;
            this.colBalanceFuelFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceFuelFact.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceFuelFact.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBalanceFuelFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colBalanceFuelFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBalanceFuelFact.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBalanceFuelFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources( this.colBalanceFuelFact, "colBalanceFuelFact" );
            this.colBalanceFuelFact.FieldName = "BalanceFuelFact";
            this.colBalanceFuelFact.Name = "colBalanceFuelFact";
            // 
            // labelControl14
            // 
            this.labelControl14.AccessibleDescription = null;
            this.labelControl14.AccessibleName = null;
            resources.ApplyResources( this.labelControl14, "labelControl14" );
            this.labelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl14.Name = "labelControl14";
            // 
            // winControlContainer16
            // 
            resources.ApplyResources( this.winControlContainer16, "winControlContainer16" );
            this.winControlContainer16.Name = "winControlContainer16";
            this.winControlContainer16.WinControl = this.labelControl14;
            // 
            // labelControl15
            // 
            this.labelControl15.AccessibleDescription = null;
            this.labelControl15.AccessibleName = null;
            resources.ApplyResources( this.labelControl15, "labelControl15" );
            this.labelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl15.Name = "labelControl15";
            // 
            // winControlContainer17
            // 
            resources.ApplyResources( this.winControlContainer17, "winControlContainer17" );
            this.winControlContainer17.Name = "winControlContainer17";
            this.winControlContainer17.WinControl = this.labelControl15;
            // 
            // labelControl16
            // 
            this.labelControl16.AccessibleDescription = null;
            this.labelControl16.AccessibleName = null;
            resources.ApplyResources( this.labelControl16, "labelControl16" );
            this.labelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl16.Name = "labelControl16";
            // 
            // winControlContainer18
            // 
            resources.ApplyResources( this.winControlContainer18, "winControlContainer18" );
            this.winControlContainer18.Name = "winControlContainer18";
            this.winControlContainer18.WinControl = this.labelControl16;
            // 
            // labelControl17
            // 
            this.labelControl17.AccessibleDescription = null;
            this.labelControl17.AccessibleName = null;
            resources.ApplyResources( this.labelControl17, "labelControl17" );
            this.labelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl17.Name = "labelControl17";
            // 
            // winControlContainer19
            // 
            resources.ApplyResources( this.winControlContainer19, "winControlContainer19" );
            this.winControlContainer19.Name = "winControlContainer19";
            this.winControlContainer19.WinControl = this.labelControl17;
            // 
            // labelControl18
            // 
            this.labelControl18.AccessibleDescription = null;
            this.labelControl18.AccessibleName = null;
            resources.ApplyResources( this.labelControl18, "labelControl18" );
            this.labelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl18.Name = "labelControl18";
            // 
            // winControlContainer20
            // 
            resources.ApplyResources( this.winControlContainer20, "winControlContainer20" );
            this.winControlContainer20.Name = "winControlContainer20";
            this.winControlContainer20.WinControl = this.labelControl18;
            // 
            // labelControl19
            // 
            this.labelControl19.AccessibleDescription = null;
            this.labelControl19.AccessibleName = null;
            resources.ApplyResources( this.labelControl19, "labelControl19" );
            this.labelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl19.Name = "labelControl19";
            // 
            // winControlContainer21
            // 
            resources.ApplyResources( this.winControlContainer21, "winControlContainer21" );
            this.winControlContainer21.Name = "winControlContainer21";
            this.winControlContainer21.WinControl = this.labelControl19;
            // 
            // labelControl20
            // 
            this.labelControl20.AccessibleDescription = null;
            this.labelControl20.AccessibleName = null;
            resources.ApplyResources( this.labelControl20, "labelControl20" );
            this.labelControl20.Name = "labelControl20";
            // 
            // winControlContainer22
            // 
            resources.ApplyResources( this.winControlContainer22, "winControlContainer22" );
            this.winControlContainer22.Name = "winControlContainer22";
            this.winControlContainer22.WinControl = this.labelControl20;
            // 
            // labelControl21
            // 
            this.labelControl21.AccessibleDescription = null;
            this.labelControl21.AccessibleName = null;
            resources.ApplyResources( this.labelControl21, "labelControl21" );
            this.labelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl21.Name = "labelControl21";
            // 
            // winControlContainer23
            // 
            resources.ApplyResources( this.winControlContainer23, "winControlContainer23" );
            this.winControlContainer23.Name = "winControlContainer23";
            this.winControlContainer23.WinControl = this.labelControl21;
            // 
            // labelControl22
            // 
            this.labelControl22.AccessibleDescription = null;
            this.labelControl22.AccessibleName = null;
            resources.ApplyResources( this.labelControl22, "labelControl22" );
            this.labelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl22.Name = "labelControl22";
            // 
            // winControlContainer24
            // 
            resources.ApplyResources( this.winControlContainer24, "winControlContainer24" );
            this.winControlContainer24.Name = "winControlContainer24";
            this.winControlContainer24.WinControl = this.labelControl22;
            // 
            // labelControl23
            // 
            this.labelControl23.AccessibleDescription = null;
            this.labelControl23.AccessibleName = null;
            resources.ApplyResources( this.labelControl23, "labelControl23" );
            this.labelControl23.Appearance.Font = new System.Drawing.Font( "Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte ) ( 204 ) ) );
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl23.Name = "labelControl23";
            // 
            // winControlContainer25
            // 
            resources.ApplyResources( this.winControlContainer25, "winControlContainer25" );
            this.winControlContainer25.Name = "winControlContainer25";
            this.winControlContainer25.WinControl = this.labelControl23;
            // 
            // XtraSchedulerReport2
            // 
            this.Bands.AddRange( new DevExpress.XtraReports.UI.Band[] {
            this.Detail} );
            this.ExportOptions.Csv.EncodingType = ( ( DevExpress.XtraPrinting.EncodingType ) ( resources.GetObject( "XtraSchedulerReport2.ExportOptions.Csv.EncodingType" ) ) );
            this.ExportOptions.Html.CharacterSet = resources.GetString( "XtraSchedulerReport2.ExportOptions.Html.CharacterSet" );
            this.ExportOptions.Html.Title = resources.GetString( "XtraSchedulerReport2.ExportOptions.Html.Title" );
            this.ExportOptions.Mht.CharacterSet = resources.GetString( "XtraSchedulerReport2.ExportOptions.Mht.CharacterSet" );
            this.ExportOptions.Mht.Title = resources.GetString( "XtraSchedulerReport2.ExportOptions.Mht.Title" );
            this.ExportOptions.Pdf.DocumentOptions.Application = resources.GetString( "XtraSchedulerReport2.ExportOptions.Pdf.DocumentOptions.Application" );
            this.ExportOptions.Pdf.DocumentOptions.Author = resources.GetString( "XtraSchedulerReport2.ExportOptions.Pdf.DocumentOptions.Author" );
            this.ExportOptions.Pdf.DocumentOptions.Keywords = resources.GetString( "XtraSchedulerReport2.ExportOptions.Pdf.DocumentOptions.Keywords" );
            this.ExportOptions.Pdf.DocumentOptions.Subject = resources.GetString( "XtraSchedulerReport2.ExportOptions.Pdf.DocumentOptions.Subject" );
            this.ExportOptions.Pdf.DocumentOptions.Title = resources.GetString( "XtraSchedulerReport2.ExportOptions.Pdf.DocumentOptions.Title" );
            this.ExportOptions.Text.EncodingType = ( ( DevExpress.XtraPrinting.EncodingType ) ( resources.GetObject( "XtraSchedulerReport2.ExportOptions.Text.EncodingType" ) ) );
            this.ExportOptions.Xls.SheetName = resources.GetString( "XtraSchedulerReport2.ExportOptions.Xls.SheetName" );
            this.ExportOptions.Xlsx.SheetName = resources.GetString( "XtraSchedulerReport2.ExportOptions.Xlsx.SheetName" );
            this.GridSize = new System.Drawing.Size( 5, 5 );
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins( 10, 10, 10, 10 );
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            resources.ApplyResources( this, "$this" );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlTask ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridControlResultWork ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this ) ).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private DevExpress.XtraGrid.GridControl gridControlTask;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colTikets;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTimeAll;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkKTonn;
        private DevExpress.XtraGrid.Columns.GridColumn colDescribeWorker;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelOutSide;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer15;
        private DevExpress.XtraGrid.GridControl gridControlResultWork;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceFuelNorm;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWorkAuto;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWorkTrailer;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWorkAutoMove;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWorkAutoStopLine;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWorkAutoCrach;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberTravelWeight;
        private DevExpress.XtraGrid.Columns.GridColumn TravelAutoAll;
        private DevExpress.XtraGrid.Columns.GridColumn TravelTrailerAll;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelAutoWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelTrailerWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightTravelAll;
        private DevExpress.XtraGrid.Columns.GridColumn colWeightTrailerAll;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkKTonnAll;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkKTonnTrailerAll;
        private DevExpress.XtraGrid.Columns.GridColumn colSalaryCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSalarySumma;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceFuelFact;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer16;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer17;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer18;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer19;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer20;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer21;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer22;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer23;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer24;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer25;
        private DevExpress.XtraEditors.LabelControl labelControl23;
    }
}
