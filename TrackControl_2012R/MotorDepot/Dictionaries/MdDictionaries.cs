﻿
namespace MotorDepot
{
    public enum MdDictionaries
    {
        TransportationType,
        LoadUnloadTime,
        Cargo,
        ObjectsGroup,
        Object,
        OrderCategory,
        Route,
        Enterprise,
        OrderPriority,
        WorkingShift,
        VehicleState,
        Tariffs,
        AlarmingType
        
    }
}
