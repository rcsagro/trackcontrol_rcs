using System.Linq; 
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using MotorDepot.AccessObjects;
using TrackControl.General;
using System;


namespace MotorDepot
{
    public partial class MdDictionariesList : DevExpress.XtraEditors.XtraUserControl
    {
        public bool IsObjectsExists { get { return tlDictionary.Nodes.Count > 0 ; } }

        public MdDictionariesList()
        {
            InitializeComponent();
            LoadNodes();
        }

        private void tlDictionary_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            SetDataSource();
        }

        public void SetDataSource()
        {
            if (tlDictionary.FocusedNode == null) return;
            bool isEditable = MdUser.Instance.IsObjectEditableForRole(tlDictionary.FocusedNode.GetValue(DicTypeEf).ToString(), (int)UserAccessObjectsTypes.DictionariesMd);
            DictController.SetViewDataSource(this, (MdDictionaries)tlDictionary.FocusedNode.GetValue(DicCode), isEditable);
        }

        #region ChangeValue
        private void gvEnterpise_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            enterprise ent = gvEnterpise.GetRow(e.RowHandle) as enterprise;
            if (e.Column == colEntDisp)
            {
                ent.Dispartchable = (bool)e.Value;
            }
        }

        private void gvEnterpise_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
             
            DictController.SaveItem (this,MdDictionaries.Enterprise);
        }

        private void gvCargo_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.Cargo);
        }

        private void gvTranspType_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.TransportationType);
        }

        private void gvAlarmType_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.AlarmingType);
        }

        private void gvObjGr_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.ObjectsGroup);
        }

        private void gvCateg_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.OrderCategory);
        }

        private void gvState_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this,MdDictionaries.VehicleState);
        }

        private void gvObject_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (gvObject.GetRowCellValue(e.RowHandle, colObjGroupe) != null
                && gvObject.GetRowCellValue(e.RowHandle, colObjName) != null
                && gvObject.GetRowCellValue(e.RowHandle, colObjName).ToString().Length > 0)
                DictController.SaveItem(this,MdDictionaries.Object);
        }

        private void gvRoute_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (gvRoute.GetRowCellValue(e.RowHandle, colRouteObjFrom) != null
            && gvRoute.GetRowCellValue(e.RowHandle, colRouteObjTo) != null)
                DictController.SaveItem(this,MdDictionaries.Route);
        }



        private void gvLoading_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (gvLoading.GetRowCellValue(e.RowHandle, colLoadCargo) != null
                && gvLoading.GetRowCellValue(e.RowHandle, colLoadObj) != null)
                DictController.SaveItem(this,MdDictionaries.LoadUnloadTime);
        }



        private void gvShift_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this, MdDictionaries.WorkingShift);
        }

        private void gvPriority_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DictController.SaveItem(this, MdDictionaries.OrderPriority);
        }

        private void bgvTariff_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            bgvTariff.SetRowCellValue(e.RowHandle, colTDateIssue, DateTime.Today);
        }

        private void bgvTariff_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (bgvTariff.GetRowCellValue(e.RowHandle, colTvehicle_category) != null )
            DictController.SaveItem(this, MdDictionaries.Tariffs);
            
        }
        #endregion

        #region DeleteValue
        private void gcDict_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (tlDictionary.FocusedNode == null) return;
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:
                    {
                        if (XtraMessageBox.Show("������������� �������� ������ �� �����������?",
                        "��������",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            e.Handled = !DictController.DeleteItem(this, (MdDictionaries)tlDictionary.FocusedNode.Id);
                        }
                        else
                        {
                            e.Handled = true;
                        }
                        break;
                    }
            }

        }
        #endregion

        #region Validation
        private void gvEnterpise_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
           // if (DialogResult.No == XtraMessageBox.Show(_editQuestion, MDController.MesCaption, MessageBoxButtons.YesNo))
           //{
           //    e.Valid = false;
           //    return;
           //}
            e.Valid = ValidateName(gvEnterpise, colEntName, e.RowHandle);
        }

        private void gvCargo_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvCargo, colCargName, e.RowHandle);
        }

        private void gvTranspType_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvTranspType, colTransName, e.RowHandle);
        }

        private void gvAlarmType_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvAlarmType, colAlarmName, e.RowHandle);
        }

        private void gvObjGr_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvObjGr, colObjGrName, e.RowHandle);
        }

        private void gvCateg_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvCateg, colCategName, e.RowHandle);
        }

        private void gvState_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvState, colStateName, e.RowHandle);

        }

        private void gvObject_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvObject, colObjName, e.RowHandle) && ValidateName(gvObject, colObjGroupe, e.RowHandle);
        }

        private void gvShift_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvShift, colShiftName, e.RowHandle);
        }

        bool ValidateName(DevExpress.XtraGrid.Views.Grid.GridView gridView, DevExpress.XtraGrid.Columns.GridColumn gridColumn, int gridRow)
        {
            if (gridView.GetRowCellValue(gridRow, gridColumn) == null || gridView.GetRowCellValue(gridRow, gridColumn).ToString().Length == 0)
            {
                gridView.SetColumnError(gridColumn, "������� ��������!");
                return false;
            }
            else
            {
                return true;
            }
        }
        private void gvPriority_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            e.Valid = ValidateName(gvPriority, colPriorName, e.RowHandle);
        }

        private void gvLoading_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gvLoading.GetRowCellValue(e.RowHandle, colLoadObj) == null)
            {
                gvLoading.SetColumnError(colLoadObj, "������� ������!");
                e.Valid = false;
                return;
            }
            if (gvLoading.GetRowCellValue(e.RowHandle, colLoadCargo) == null)
            {
                gvLoading.SetColumnError(colLoadCargo, "������� ����!");
                e.Valid = false;
                return;
            }
        }

        private void gvRoute_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gvRoute.GetRowCellValue(e.RowHandle, colRouteObjFrom) == null)
            {
                gvRoute.SetColumnError(colRouteObjFrom, "������� ������!");
                e.Valid = false;
                return;
            }
            if (gvRoute.GetRowCellValue(e.RowHandle, colRouteObjTo) == null)
            {
                gvRoute.SetColumnError(colRouteObjTo, "������� ������!");
                e.Valid = false;
                return;
            }
        }
        #endregion

        #region ValidatingEditor
        bool GetAllowEditValue()
        {
            if (XtraMessageBox.Show("������������� ��������� ������?", MDController.MesCaption, MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void gvEnterpise_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvCargo_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvTranspType_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvAlarmType_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvObjGr_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvCateg_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvState_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvObject_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvRoute_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvLoading_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvShift_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }

        private void gvPriority_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            e.Valid = GetAllowEditValue();
        }
        #endregion

        void LoadNodes()
        {
            tlDictionary.Nodes.Clear();
            var nodes = MDController.Instance.ContextMotorDepot.acs_objectsSet.Where(dt => dt.IdType == (int)UserAccessObjectsTypes.DictionariesMd).OrderBy(dt=>dt.Id).ToList();
            if (nodes != null)
            {
                TreeListNode parentForRootNodes = null;
                tlDictionary.BeginUnboundLoad();
                foreach (acs_objects dict in nodes)
                {
                    if (MdUser.Instance.IsObjectVisibleForRole(dict.TypeEF, (int)UserAccessObjectsTypes.DictionariesMd))
                    {
                        if (dict.Level == 0) parentForRootNodes = null;
                        TreeListNode rootNode = tlDictionary.AppendNode(
                            new object[] { DictController.GetDictionaryCode(dict.TypeEF), dict.TypeEF, dict.Name },
                            parentForRootNodes);
                        parentForRootNodes = rootNode; 
                    }
                }
                tlDictionary.EndUnboundLoad();
                if (tlDictionary.Nodes.Count > 0)
                {
                    bool isEditable = MdUser.Instance.IsObjectEditableForRole(tlDictionary.Nodes[0].GetValue(DicTypeEf).ToString(), (int)UserAccessObjectsTypes.DictionariesMd);
                    DictController.SetViewDataSource(this, (MdDictionaries)tlDictionary.Nodes[0].GetValue(DicCode), isEditable);
                }
            }
            
        }

        public int FocusedDictionaryCode()
        {
            return (int)(MdDictionaries)tlDictionary.FocusedNode.GetValue(DicCode);
        }



    }
}
