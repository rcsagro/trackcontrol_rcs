namespace MotorDepot
{
    partial class MdDictionariesList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode12 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MdDictionariesList));
            this.gvCargo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCargId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCargName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCargUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCargRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDict = new DevExpress.XtraGrid.GridControl();
            this.bsMotorDepotEntity = new System.Windows.Forms.BindingSource(this.components);
            this.gvTranspType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransRem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvAlarmType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAlarmId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlarmName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlarmRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvObjGr = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colObjGrId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjGrName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjGrRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvCateg = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCategId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvState = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStateId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateMaxTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTariffication = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvObject = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colObjId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjGroupe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleObjGroup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colObjNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleZones = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colObjName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvRoute = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRouteId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRouteObjFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colRouteObjTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRouteDist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRouteTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRouteRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvLoading = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLoadId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadObj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadCargo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleCargo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colLoadTimeLoad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadTimeUnLoad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoadRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvShift = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colShiftId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShiftName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShiftStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colShiftEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDinnerStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDinnerEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShiftRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvPriority = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPriorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bgvTariff = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colTId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTDateIssue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rdeDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colTvehicle_category = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleVehCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTTimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTMoveBt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTTimeBt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTTimeStopBt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gvEnterpise = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEntId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEntName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEntDisp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.riCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEntRem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tlDictionary = new DevExpress.XtraTreeList.TreeList();
            this.DicCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.DicTypeEf = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.DicName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.colTLimit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.gvCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMotorDepotEntity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTranspType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAlarmType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObjGr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCateg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObjGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEnterpise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gvCargo
            // 
            this.gvCargo.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCargo.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCargo.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCargo.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCargo.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCargo.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCargo.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCargo.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCargo.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCargo.Appearance.Empty.Options.UseBackColor = true;
            this.gvCargo.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCargo.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCargo.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCargo.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCargo.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCargo.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCargo.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCargo.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCargo.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCargo.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCargo.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCargo.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCargo.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCargo.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCargo.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCargo.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCargo.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCargo.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCargo.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCargo.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCargo.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCargo.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCargo.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCargo.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCargo.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCargo.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCargo.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCargo.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCargo.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCargo.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCargo.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCargo.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCargo.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCargo.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCargo.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCargo.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCargo.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCargo.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCargo.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCargo.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCargo.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCargo.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCargo.Appearance.Preview.Options.UseBackColor = true;
            this.gvCargo.Appearance.Preview.Options.UseFont = true;
            this.gvCargo.Appearance.Preview.Options.UseForeColor = true;
            this.gvCargo.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCargo.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.Row.Options.UseBackColor = true;
            this.gvCargo.Appearance.Row.Options.UseForeColor = true;
            this.gvCargo.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCargo.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCargo.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCargo.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCargo.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCargo.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCargo.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCargo.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCargo.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCargo.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCargo.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCargo.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCargo.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCargId,
            this.colCargName,
            this.colCargUnit,
            this.colCargRemark});
            this.gvCargo.GridControl = this.gcDict;
            this.gvCargo.Name = "gvCargo";
            this.gvCargo.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCargo.OptionsView.EnableAppearanceOddRow = true;
            this.gvCargo.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvCargo_CellValueChanged);
            this.gvCargo.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvCargo_ValidateRow);
            this.gvCargo.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvCargo_ValidatingEditor);
            // 
            // colCargId
            // 
            this.colCargId.AppearanceCell.Options.UseTextOptions = true;
            this.colCargId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargId.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargId.Caption = "Id";
            this.colCargId.FieldName = "Id";
            this.colCargId.Name = "colCargId";
            this.colCargId.OptionsColumn.ReadOnly = true;
            this.colCargId.Visible = true;
            this.colCargId.VisibleIndex = 0;
            this.colCargId.Width = 41;
            // 
            // colCargName
            // 
            this.colCargName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargName.Caption = "��������";
            this.colCargName.FieldName = "Name";
            this.colCargName.Name = "colCargName";
            this.colCargName.Visible = true;
            this.colCargName.VisibleIndex = 1;
            this.colCargName.Width = 401;
            // 
            // colCargUnit
            // 
            this.colCargUnit.AppearanceCell.Options.UseTextOptions = true;
            this.colCargUnit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargUnit.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargUnit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargUnit.Caption = "��.���.";
            this.colCargUnit.FieldName = "Unit";
            this.colCargUnit.Name = "colCargUnit";
            this.colCargUnit.Visible = true;
            this.colCargUnit.VisibleIndex = 2;
            this.colCargUnit.Width = 127;
            // 
            // colCargRemark
            // 
            this.colCargRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargRemark.Caption = "����������";
            this.colCargRemark.FieldName = "Remark";
            this.colCargRemark.Name = "colCargRemark";
            this.colCargRemark.Visible = true;
            this.colCargRemark.VisibleIndex = 3;
            this.colCargRemark.Width = 681;
            // 
            // gcDict
            // 
            this.gcDict.DataSource = this.bsMotorDepotEntity;
            this.gcDict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDict.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcDict_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gvCargo;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.gvTranspType;
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode3.LevelTemplate = this.gvAlarmType;
            gridLevelNode3.RelationName = "Level3";
            gridLevelNode4.LevelTemplate = this.gvObjGr;
            gridLevelNode4.RelationName = "Level4";
            gridLevelNode5.LevelTemplate = this.gvCateg;
            gridLevelNode5.RelationName = "Level5";
            gridLevelNode6.LevelTemplate = this.gvState;
            gridLevelNode6.RelationName = "Level6";
            gridLevelNode7.LevelTemplate = this.gvObject;
            gridLevelNode7.RelationName = "Level7";
            gridLevelNode8.LevelTemplate = this.gvRoute;
            gridLevelNode8.RelationName = "Level8";
            gridLevelNode9.LevelTemplate = this.gvLoading;
            gridLevelNode9.RelationName = "Level9";
            gridLevelNode10.LevelTemplate = this.gvShift;
            gridLevelNode10.RelationName = "Level10";
            gridLevelNode11.LevelTemplate = this.gvPriority;
            gridLevelNode11.RelationName = "Level11";
            gridLevelNode12.LevelTemplate = this.bgvTariff;
            gridLevelNode12.RelationName = "Level12";
            this.gcDict.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11,
            gridLevelNode12});
            this.gcDict.Location = new System.Drawing.Point(0, 0);
            this.gcDict.MainView = this.gvEnterpise;
            this.gcDict.Name = "gcDict";
            this.gcDict.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riCheckEdit,
            this.rleObjGroup,
            this.rleObject,
            this.rleCargo,
            this.teTime,
            this.rleZones,
            this.rdeDate,
            this.rleVehCategory});
            this.gcDict.Size = new System.Drawing.Size(490, 647);
            this.gcDict.TabIndex = 0;
            this.gcDict.UseEmbeddedNavigator = true;
            this.gcDict.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTranspType,
            this.gvAlarmType,
            this.gvObjGr,
            this.gvCateg,
            this.gvState,
            this.gvObject,
            this.gvRoute,
            this.gvLoading,
            this.gvShift,
            this.gvPriority,
            this.bgvTariff,
            this.gvEnterpise,
            this.gvCargo});
            // 
            // gvTranspType
            // 
            this.gvTranspType.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvTranspType.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvTranspType.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTranspType.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTranspType.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvTranspType.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvTranspType.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvTranspType.Appearance.Empty.Options.UseBackColor = true;
            this.gvTranspType.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTranspType.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTranspType.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTranspType.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTranspType.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvTranspType.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTranspType.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvTranspType.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvTranspType.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvTranspType.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvTranspType.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvTranspType.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvTranspType.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvTranspType.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvTranspType.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTranspType.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTranspType.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvTranspType.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvTranspType.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvTranspType.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvTranspType.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTranspType.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvTranspType.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvTranspType.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvTranspType.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvTranspType.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTranspType.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTranspType.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvTranspType.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTranspType.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTranspType.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.OddRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.OddRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvTranspType.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvTranspType.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvTranspType.Appearance.Preview.Options.UseBackColor = true;
            this.gvTranspType.Appearance.Preview.Options.UseFont = true;
            this.gvTranspType.Appearance.Preview.Options.UseForeColor = true;
            this.gvTranspType.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTranspType.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.Row.Options.UseBackColor = true;
            this.gvTranspType.Appearance.Row.Options.UseForeColor = true;
            this.gvTranspType.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTranspType.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvTranspType.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvTranspType.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvTranspType.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTranspType.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTranspType.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvTranspType.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvTranspType.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvTranspType.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvTranspType.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTranspType.Appearance.VertLine.Options.UseBackColor = true;
            this.gvTranspType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransId,
            this.colTransName,
            this.colTransRem});
            this.gvTranspType.GridControl = this.gcDict;
            this.gvTranspType.Name = "gvTranspType";
            this.gvTranspType.OptionsView.EnableAppearanceEvenRow = true;
            this.gvTranspType.OptionsView.EnableAppearanceOddRow = true;
            this.gvTranspType.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvTranspType_CellValueChanged);
            this.gvTranspType.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvTranspType_ValidateRow);
            this.gvTranspType.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvTranspType_ValidatingEditor);
            // 
            // colTransId
            // 
            this.colTransId.AppearanceCell.Options.UseTextOptions = true;
            this.colTransId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransId.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransId.Caption = "Id";
            this.colTransId.FieldName = "Id";
            this.colTransId.Name = "colTransId";
            this.colTransId.Visible = true;
            this.colTransId.VisibleIndex = 0;
            this.colTransId.Width = 43;
            // 
            // colTransName
            // 
            this.colTransName.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransName.Caption = "��������";
            this.colTransName.FieldName = "Name";
            this.colTransName.Name = "colTransName";
            this.colTransName.Visible = true;
            this.colTransName.VisibleIndex = 1;
            this.colTransName.Width = 601;
            // 
            // colTransRem
            // 
            this.colTransRem.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransRem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransRem.Caption = "����������";
            this.colTransRem.FieldName = "Remark";
            this.colTransRem.Name = "colTransRem";
            this.colTransRem.Visible = true;
            this.colTransRem.VisibleIndex = 2;
            this.colTransRem.Width = 606;
            // 
            // gvAlarmType
            // 
            this.gvAlarmType.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.Empty.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAlarmType.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvAlarmType.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAlarmType.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAlarmType.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvAlarmType.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvAlarmType.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvAlarmType.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAlarmType.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvAlarmType.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvAlarmType.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAlarmType.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAlarmType.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAlarmType.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAlarmType.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.OddRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.OddRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvAlarmType.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvAlarmType.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvAlarmType.Appearance.Preview.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.Preview.Options.UseFont = true;
            this.gvAlarmType.Appearance.Preview.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvAlarmType.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.Row.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.Row.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvAlarmType.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvAlarmType.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvAlarmType.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvAlarmType.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvAlarmType.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvAlarmType.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvAlarmType.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvAlarmType.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvAlarmType.Appearance.VertLine.Options.UseBackColor = true;
            this.gvAlarmType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAlarmId,
            this.colAlarmName,
            this.colAlarmRemark});
            this.gvAlarmType.GridControl = this.gcDict;
            this.gvAlarmType.Name = "gvAlarmType";
            this.gvAlarmType.OptionsView.EnableAppearanceEvenRow = true;
            this.gvAlarmType.OptionsView.EnableAppearanceOddRow = true;
            this.gvAlarmType.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvAlarmType_CellValueChanged);
            this.gvAlarmType.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvAlarmType_ValidateRow);
            this.gvAlarmType.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvAlarmType_ValidatingEditor);
            // 
            // colAlarmId
            // 
            this.colAlarmId.AppearanceCell.Options.UseTextOptions = true;
            this.colAlarmId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlarmId.AppearanceHeader.Options.UseTextOptions = true;
            this.colAlarmId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlarmId.Caption = "Id";
            this.colAlarmId.FieldName = "Id";
            this.colAlarmId.Name = "colAlarmId";
            this.colAlarmId.Visible = true;
            this.colAlarmId.VisibleIndex = 0;
            this.colAlarmId.Width = 41;
            // 
            // colAlarmName
            // 
            this.colAlarmName.AppearanceHeader.Options.UseTextOptions = true;
            this.colAlarmName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlarmName.Caption = "��������";
            this.colAlarmName.FieldName = "Name";
            this.colAlarmName.Name = "colAlarmName";
            this.colAlarmName.Visible = true;
            this.colAlarmName.VisibleIndex = 1;
            this.colAlarmName.Width = 602;
            // 
            // colAlarmRemark
            // 
            this.colAlarmRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colAlarmRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlarmRemark.Caption = "����������";
            this.colAlarmRemark.FieldName = "Remark";
            this.colAlarmRemark.Name = "colAlarmRemark";
            this.colAlarmRemark.Visible = true;
            this.colAlarmRemark.VisibleIndex = 2;
            this.colAlarmRemark.Width = 607;
            // 
            // gvObjGr
            // 
            this.gvObjGr.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvObjGr.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvObjGr.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjGr.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjGr.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvObjGr.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvObjGr.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvObjGr.Appearance.Empty.Options.UseBackColor = true;
            this.gvObjGr.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjGr.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjGr.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjGr.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjGr.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvObjGr.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvObjGr.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvObjGr.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvObjGr.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvObjGr.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvObjGr.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvObjGr.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvObjGr.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvObjGr.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvObjGr.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjGr.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjGr.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvObjGr.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvObjGr.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvObjGr.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvObjGr.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvObjGr.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvObjGr.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvObjGr.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvObjGr.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvObjGr.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjGr.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjGr.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvObjGr.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjGr.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjGr.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.OddRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.OddRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvObjGr.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvObjGr.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvObjGr.Appearance.Preview.Options.UseBackColor = true;
            this.gvObjGr.Appearance.Preview.Options.UseFont = true;
            this.gvObjGr.Appearance.Preview.Options.UseForeColor = true;
            this.gvObjGr.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjGr.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.Row.Options.UseBackColor = true;
            this.gvObjGr.Appearance.Row.Options.UseForeColor = true;
            this.gvObjGr.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjGr.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvObjGr.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvObjGr.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvObjGr.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjGr.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjGr.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvObjGr.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvObjGr.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvObjGr.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvObjGr.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjGr.Appearance.VertLine.Options.UseBackColor = true;
            this.gvObjGr.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colObjGrId,
            this.colObjGrName,
            this.colObjGrRemark});
            this.gvObjGr.GridControl = this.gcDict;
            this.gvObjGr.Name = "gvObjGr";
            this.gvObjGr.OptionsView.EnableAppearanceEvenRow = true;
            this.gvObjGr.OptionsView.EnableAppearanceOddRow = true;
            this.gvObjGr.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvObjGr_CellValueChanged);
            this.gvObjGr.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvObjGr_ValidateRow);
            this.gvObjGr.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvObjGr_ValidatingEditor);
            // 
            // colObjGrId
            // 
            this.colObjGrId.AppearanceCell.Options.UseTextOptions = true;
            this.colObjGrId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjGrId.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjGrId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjGrId.Caption = "Id";
            this.colObjGrId.FieldName = "Id";
            this.colObjGrId.Name = "colObjGrId";
            this.colObjGrId.Visible = true;
            this.colObjGrId.VisibleIndex = 0;
            this.colObjGrId.Width = 41;
            // 
            // colObjGrName
            // 
            this.colObjGrName.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjGrName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjGrName.Caption = "��������";
            this.colObjGrName.FieldName = "Name";
            this.colObjGrName.Name = "colObjGrName";
            this.colObjGrName.Visible = true;
            this.colObjGrName.VisibleIndex = 1;
            this.colObjGrName.Width = 602;
            // 
            // colObjGrRemark
            // 
            this.colObjGrRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjGrRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjGrRemark.Caption = "����������";
            this.colObjGrRemark.FieldName = "Remark";
            this.colObjGrRemark.Name = "colObjGrRemark";
            this.colObjGrRemark.Visible = true;
            this.colObjGrRemark.VisibleIndex = 2;
            this.colObjGrRemark.Width = 607;
            // 
            // gvCateg
            // 
            this.gvCateg.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCateg.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCateg.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.Empty.Options.UseBackColor = true;
            this.gvCateg.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCateg.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCateg.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCateg.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCateg.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCateg.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCateg.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCateg.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCateg.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCateg.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCateg.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCateg.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCateg.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCateg.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCateg.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCateg.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCateg.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCateg.Appearance.Preview.Options.UseBackColor = true;
            this.gvCateg.Appearance.Preview.Options.UseFont = true;
            this.gvCateg.Appearance.Preview.Options.UseForeColor = true;
            this.gvCateg.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCateg.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.Row.Options.UseBackColor = true;
            this.gvCateg.Appearance.Row.Options.UseForeColor = true;
            this.gvCateg.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCateg.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCateg.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCateg.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCateg.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCateg.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCateg.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCateg.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCateg.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCateg.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCateg.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCateg.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCateg.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCategId,
            this.colCategName,
            this.colCategRemark});
            this.gvCateg.GridControl = this.gcDict;
            this.gvCateg.Name = "gvCateg";
            this.gvCateg.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCateg.OptionsView.EnableAppearanceOddRow = true;
            this.gvCateg.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvCateg_CellValueChanged);
            this.gvCateg.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvCateg_ValidateRow);
            this.gvCateg.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvCateg_ValidatingEditor);
            // 
            // colCategId
            // 
            this.colCategId.AppearanceCell.Options.UseTextOptions = true;
            this.colCategId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategId.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategId.Caption = "Id";
            this.colCategId.FieldName = "Id";
            this.colCategId.Name = "colCategId";
            this.colCategId.Visible = true;
            this.colCategId.VisibleIndex = 0;
            this.colCategId.Width = 41;
            // 
            // colCategName
            // 
            this.colCategName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategName.Caption = "��������";
            this.colCategName.FieldName = "Name";
            this.colCategName.Name = "colCategName";
            this.colCategName.Visible = true;
            this.colCategName.VisibleIndex = 1;
            this.colCategName.Width = 602;
            // 
            // colCategRemark
            // 
            this.colCategRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colCategRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCategRemark.Caption = "����������";
            this.colCategRemark.FieldName = "Remark";
            this.colCategRemark.Name = "colCategRemark";
            this.colCategRemark.Visible = true;
            this.colCategRemark.VisibleIndex = 2;
            this.colCategRemark.Width = 607;
            // 
            // gvState
            // 
            this.gvState.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvState.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvState.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvState.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvState.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvState.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvState.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvState.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvState.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvState.Appearance.Empty.Options.UseBackColor = true;
            this.gvState.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvState.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvState.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvState.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvState.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvState.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvState.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvState.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvState.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvState.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvState.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvState.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvState.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvState.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvState.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvState.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvState.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvState.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvState.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvState.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvState.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvState.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvState.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvState.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvState.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvState.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvState.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvState.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvState.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvState.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvState.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvState.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvState.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvState.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvState.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvState.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvState.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvState.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvState.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvState.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvState.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvState.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvState.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvState.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvState.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvState.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvState.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvState.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvState.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvState.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvState.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvState.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.OddRow.Options.UseBackColor = true;
            this.gvState.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvState.Appearance.OddRow.Options.UseForeColor = true;
            this.gvState.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvState.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvState.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvState.Appearance.Preview.Options.UseBackColor = true;
            this.gvState.Appearance.Preview.Options.UseFont = true;
            this.gvState.Appearance.Preview.Options.UseForeColor = true;
            this.gvState.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvState.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.Row.Options.UseBackColor = true;
            this.gvState.Appearance.Row.Options.UseForeColor = true;
            this.gvState.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvState.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvState.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvState.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvState.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvState.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvState.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvState.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvState.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvState.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvState.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvState.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvState.Appearance.VertLine.Options.UseBackColor = true;
            this.gvState.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStateId,
            this.colStateName,
            this.colStateMaxTime,
            this.colTariffication,
            this.colStateRemark});
            this.gvState.GridControl = this.gcDict;
            this.gvState.Name = "gvState";
            this.gvState.OptionsView.EnableAppearanceEvenRow = true;
            this.gvState.OptionsView.EnableAppearanceOddRow = true;
            this.gvState.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvState_CellValueChanged);
            this.gvState.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvState_ValidateRow);
            this.gvState.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvState_ValidatingEditor);
            // 
            // colStateId
            // 
            this.colStateId.AppearanceCell.Options.UseTextOptions = true;
            this.colStateId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateId.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateId.Caption = "Id";
            this.colStateId.FieldName = "Id";
            this.colStateId.Name = "colStateId";
            this.colStateId.Visible = true;
            this.colStateId.VisibleIndex = 0;
            this.colStateId.Width = 39;
            // 
            // colStateName
            // 
            this.colStateName.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateName.Caption = "��������";
            this.colStateName.FieldName = "Name";
            this.colStateName.Name = "colStateName";
            this.colStateName.Visible = true;
            this.colStateName.VisibleIndex = 1;
            this.colStateName.Width = 378;
            // 
            // colStateMaxTime
            // 
            this.colStateMaxTime.AppearanceCell.Options.UseTextOptions = true;
            this.colStateMaxTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateMaxTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateMaxTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateMaxTime.Caption = "��� ����� ���������� � ���������";
            this.colStateMaxTime.FieldName = "MaxTime";
            this.colStateMaxTime.Name = "colStateMaxTime";
            this.colStateMaxTime.Visible = true;
            this.colStateMaxTime.VisibleIndex = 2;
            this.colStateMaxTime.Width = 264;
            // 
            // colTariffication
            // 
            this.colTariffication.AppearanceHeader.Options.UseTextOptions = true;
            this.colTariffication.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTariffication.Caption = "�����������";
            this.colTariffication.FieldName = "Tariffication";
            this.colTariffication.Name = "colTariffication";
            this.colTariffication.Visible = true;
            this.colTariffication.VisibleIndex = 3;
            this.colTariffication.Width = 163;
            // 
            // colStateRemark
            // 
            this.colStateRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateRemark.Caption = "����������";
            this.colStateRemark.FieldName = "Remark";
            this.colStateRemark.Name = "colStateRemark";
            this.colStateRemark.Visible = true;
            this.colStateRemark.VisibleIndex = 4;
            this.colStateRemark.Width = 406;
            // 
            // gvObject
            // 
            this.gvObject.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colObjId,
            this.colObjGroupe,
            this.colObjNumber,
            this.colObjZone,
            this.colObjName,
            this.colObjParent,
            this.colObjTime,
            this.colObjRemark});
            this.gvObject.GridControl = this.gcDict;
            this.gvObject.Name = "gvObject";
            this.gvObject.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvObject_CellValueChanged);
            this.gvObject.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvObject_ValidateRow);
            this.gvObject.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvObject_ValidatingEditor);
            // 
            // colObjId
            // 
            this.colObjId.AppearanceCell.Options.UseTextOptions = true;
            this.colObjId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjId.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjId.Caption = "Id";
            this.colObjId.FieldName = "Id";
            this.colObjId.Name = "colObjId";
            this.colObjId.Visible = true;
            this.colObjId.VisibleIndex = 0;
            this.colObjId.Width = 42;
            // 
            // colObjGroupe
            // 
            this.colObjGroupe.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjGroupe.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjGroupe.Caption = "������";
            this.colObjGroupe.ColumnEdit = this.rleObjGroup;
            this.colObjGroupe.FieldName = "md_object_group";
            this.colObjGroupe.Name = "colObjGroupe";
            this.colObjGroupe.Visible = true;
            this.colObjGroupe.VisibleIndex = 1;
            this.colObjGroupe.Width = 171;
            // 
            // rleObjGroup
            // 
            this.rleObjGroup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleObjGroup.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.rleObjGroup.DisplayMember = "Name";
            this.rleObjGroup.Name = "rleObjGroup";
            this.rleObjGroup.NullText = "";
            // 
            // colObjNumber
            // 
            this.colObjNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colObjNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjNumber.Caption = "�����";
            this.colObjNumber.FieldName = "Number";
            this.colObjNumber.Name = "colObjNumber";
            this.colObjNumber.Visible = true;
            this.colObjNumber.VisibleIndex = 4;
            this.colObjNumber.Width = 171;
            // 
            // colObjZone
            // 
            this.colObjZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjZone.Caption = "����������� ����";
            this.colObjZone.ColumnEdit = this.rleZones;
            this.colObjZone.FieldName = "zones";
            this.colObjZone.Name = "colObjZone";
            this.colObjZone.Visible = true;
            this.colObjZone.VisibleIndex = 3;
            this.colObjZone.Width = 171;
            // 
            // rleZones
            // 
            this.rleZones.AutoHeight = false;
            this.rleZones.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleZones.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.rleZones.DisplayMember = "Name";
            this.rleZones.Name = "rleZones";
            this.rleZones.NullText = "";
            // 
            // colObjName
            // 
            this.colObjName.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjName.Caption = "��������";
            this.colObjName.FieldName = "Name";
            this.colObjName.Name = "colObjName";
            this.colObjName.Visible = true;
            this.colObjName.VisibleIndex = 2;
            this.colObjName.Width = 171;
            // 
            // colObjParent
            // 
            this.colObjParent.AppearanceCell.Options.UseTextOptions = true;
            this.colObjParent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjParent.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjParent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjParent.Caption = "����� �������-��������";
            this.colObjParent.FieldName = "NumberParent";
            this.colObjParent.Name = "colObjParent";
            this.colObjParent.Visible = true;
            this.colObjParent.VisibleIndex = 5;
            this.colObjParent.Width = 171;
            // 
            // colObjTime
            // 
            this.colObjTime.AppearanceCell.Options.UseTextOptions = true;
            this.colObjTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjTime.Caption = "����� ��������";
            this.colObjTime.FieldName = "DelayTime";
            this.colObjTime.Name = "colObjTime";
            this.colObjTime.Visible = true;
            this.colObjTime.VisibleIndex = 6;
            this.colObjTime.Width = 171;
            // 
            // colObjRemark
            // 
            this.colObjRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjRemark.Caption = "����������";
            this.colObjRemark.FieldName = "Remark";
            this.colObjRemark.Name = "colObjRemark";
            this.colObjRemark.Visible = true;
            this.colObjRemark.VisibleIndex = 7;
            this.colObjRemark.Width = 182;
            // 
            // gvRoute
            // 
            this.gvRoute.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRoute.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.Empty.Options.UseBackColor = true;
            this.gvRoute.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvRoute.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvRoute.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvRoute.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvRoute.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvRoute.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvRoute.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvRoute.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRoute.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRoute.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.OddRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvRoute.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvRoute.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvRoute.Appearance.Preview.Options.UseBackColor = true;
            this.gvRoute.Appearance.Preview.Options.UseFont = true;
            this.gvRoute.Appearance.Preview.Options.UseForeColor = true;
            this.gvRoute.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.Row.Options.UseBackColor = true;
            this.gvRoute.Appearance.Row.Options.UseForeColor = true;
            this.gvRoute.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRoute.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvRoute.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvRoute.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRoute.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRouteId,
            this.colRouteObjFrom,
            this.colRouteObjTo,
            this.colRouteDist,
            this.colRouteTime,
            this.colRouteRemark});
            this.gvRoute.GridControl = this.gcDict;
            this.gvRoute.Name = "gvRoute";
            this.gvRoute.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRoute.OptionsView.EnableAppearanceOddRow = true;
            this.gvRoute.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvRoute_CellValueChanged);
            this.gvRoute.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvRoute_ValidateRow);
            this.gvRoute.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvRoute_ValidatingEditor);
            // 
            // colRouteId
            // 
            this.colRouteId.AppearanceCell.Options.UseTextOptions = true;
            this.colRouteId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteId.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteId.Caption = "Id";
            this.colRouteId.FieldName = "Id";
            this.colRouteId.Name = "colRouteId";
            this.colRouteId.Visible = true;
            this.colRouteId.VisibleIndex = 0;
            this.colRouteId.Width = 41;
            // 
            // colRouteObjFrom
            // 
            this.colRouteObjFrom.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteObjFrom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteObjFrom.Caption = "������ ������";
            this.colRouteObjFrom.ColumnEdit = this.rleObject;
            this.colRouteObjFrom.FieldName = "md_object_from";
            this.colRouteObjFrom.Name = "colRouteObjFrom";
            this.colRouteObjFrom.Visible = true;
            this.colRouteObjFrom.VisibleIndex = 1;
            this.colRouteObjFrom.Width = 240;
            // 
            // rleObject
            // 
            this.rleObject.AutoHeight = false;
            this.rleObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleObject.DisplayMember = "Name";
            this.rleObject.Name = "rleObject";
            this.rleObject.NullText = "";
            // 
            // colRouteObjTo
            // 
            this.colRouteObjTo.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteObjTo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteObjTo.Caption = "������ ����";
            this.colRouteObjTo.ColumnEdit = this.rleObject;
            this.colRouteObjTo.FieldName = "md_object_to";
            this.colRouteObjTo.Name = "colRouteObjTo";
            this.colRouteObjTo.Visible = true;
            this.colRouteObjTo.VisibleIndex = 2;
            this.colRouteObjTo.Width = 310;
            // 
            // colRouteDist
            // 
            this.colRouteDist.AppearanceCell.Options.UseTextOptions = true;
            this.colRouteDist.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteDist.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteDist.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteDist.Caption = "����������";
            this.colRouteDist.FieldName = "Distance";
            this.colRouteDist.Name = "colRouteDist";
            this.colRouteDist.Visible = true;
            this.colRouteDist.VisibleIndex = 3;
            this.colRouteDist.Width = 136;
            // 
            // colRouteTime
            // 
            this.colRouteTime.AppearanceCell.Options.UseTextOptions = true;
            this.colRouteTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteTime.Caption = "�����";
            this.colRouteTime.FieldName = "Time";
            this.colRouteTime.Name = "colRouteTime";
            this.colRouteTime.Visible = true;
            this.colRouteTime.VisibleIndex = 4;
            this.colRouteTime.Width = 126;
            // 
            // colRouteRemark
            // 
            this.colRouteRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRouteRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRouteRemark.Caption = "����������";
            this.colRouteRemark.FieldName = "Remark";
            this.colRouteRemark.Name = "colRouteRemark";
            this.colRouteRemark.Visible = true;
            this.colRouteRemark.VisibleIndex = 5;
            this.colRouteRemark.Width = 397;
            // 
            // gvLoading
            // 
            this.gvLoading.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLoadId,
            this.colLoadObj,
            this.colLoadCargo,
            this.colLoadTimeLoad,
            this.colLoadTimeUnLoad,
            this.colLoadRemark});
            this.gvLoading.GridControl = this.gcDict;
            this.gvLoading.Name = "gvLoading";
            this.gvLoading.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvLoading_CellValueChanged);
            this.gvLoading.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvLoading_ValidateRow);
            this.gvLoading.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvLoading_ValidatingEditor);
            // 
            // colLoadId
            // 
            this.colLoadId.AppearanceCell.Options.UseTextOptions = true;
            this.colLoadId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadId.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadId.Caption = "Id";
            this.colLoadId.FieldName = "Id";
            this.colLoadId.Name = "colLoadId";
            this.colLoadId.Visible = true;
            this.colLoadId.VisibleIndex = 0;
            this.colLoadId.Width = 42;
            // 
            // colLoadObj
            // 
            this.colLoadObj.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadObj.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadObj.Caption = "������";
            this.colLoadObj.ColumnEdit = this.rleObject;
            this.colLoadObj.FieldName = "md_object";
            this.colLoadObj.Name = "colLoadObj";
            this.colLoadObj.Visible = true;
            this.colLoadObj.VisibleIndex = 1;
            this.colLoadObj.Width = 240;
            // 
            // colLoadCargo
            // 
            this.colLoadCargo.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadCargo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadCargo.Caption = "����";
            this.colLoadCargo.ColumnEdit = this.rleCargo;
            this.colLoadCargo.FieldName = "md_cargo";
            this.colLoadCargo.Name = "colLoadCargo";
            this.colLoadCargo.Visible = true;
            this.colLoadCargo.VisibleIndex = 2;
            this.colLoadCargo.Width = 240;
            // 
            // rleCargo
            // 
            this.rleCargo.AutoHeight = false;
            this.rleCargo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleCargo.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleCargo.DisplayMember = "Name";
            this.rleCargo.Name = "rleCargo";
            this.rleCargo.NullText = "";
            // 
            // colLoadTimeLoad
            // 
            this.colLoadTimeLoad.AppearanceCell.Options.UseTextOptions = true;
            this.colLoadTimeLoad.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadTimeLoad.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadTimeLoad.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadTimeLoad.Caption = "����� ��������";
            this.colLoadTimeLoad.FieldName = "TimeLoading";
            this.colLoadTimeLoad.Name = "colLoadTimeLoad";
            this.colLoadTimeLoad.Visible = true;
            this.colLoadTimeLoad.VisibleIndex = 3;
            this.colLoadTimeLoad.Width = 240;
            // 
            // colLoadTimeUnLoad
            // 
            this.colLoadTimeUnLoad.AppearanceCell.Options.UseTextOptions = true;
            this.colLoadTimeUnLoad.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadTimeUnLoad.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadTimeUnLoad.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadTimeUnLoad.Caption = "����� ���������";
            this.colLoadTimeUnLoad.FieldName = "TimeUnLoading";
            this.colLoadTimeUnLoad.Name = "colLoadTimeUnLoad";
            this.colLoadTimeUnLoad.Visible = true;
            this.colLoadTimeUnLoad.VisibleIndex = 4;
            this.colLoadTimeUnLoad.Width = 240;
            // 
            // colLoadRemark
            // 
            this.colLoadRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colLoadRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLoadRemark.Caption = "����������";
            this.colLoadRemark.FieldName = "Remark";
            this.colLoadRemark.Name = "colLoadRemark";
            this.colLoadRemark.Visible = true;
            this.colLoadRemark.VisibleIndex = 5;
            this.colLoadRemark.Width = 248;
            // 
            // gvShift
            // 
            this.gvShift.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvShift.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvShift.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvShift.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShift.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShift.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvShift.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvShift.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvShift.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvShift.Appearance.Empty.Options.UseBackColor = true;
            this.gvShift.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShift.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShift.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvShift.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvShift.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShift.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShift.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvShift.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvShift.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvShift.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvShift.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvShift.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvShift.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvShift.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvShift.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvShift.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvShift.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvShift.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvShift.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvShift.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvShift.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvShift.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvShift.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvShift.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvShift.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShift.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShift.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvShift.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvShift.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvShift.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvShift.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvShift.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvShift.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvShift.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvShift.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvShift.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvShift.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvShift.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvShift.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvShift.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvShift.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShift.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShift.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvShift.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvShift.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvShift.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShift.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShift.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.OddRow.Options.UseBackColor = true;
            this.gvShift.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.OddRow.Options.UseForeColor = true;
            this.gvShift.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvShift.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvShift.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvShift.Appearance.Preview.Options.UseBackColor = true;
            this.gvShift.Appearance.Preview.Options.UseFont = true;
            this.gvShift.Appearance.Preview.Options.UseForeColor = true;
            this.gvShift.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShift.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.Row.Options.UseBackColor = true;
            this.gvShift.Appearance.Row.Options.UseForeColor = true;
            this.gvShift.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShift.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvShift.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvShift.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvShift.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShift.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvShift.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvShift.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvShift.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvShift.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvShift.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvShift.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShift.Appearance.VertLine.Options.UseBackColor = true;
            this.gvShift.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colShiftId,
            this.colShiftName,
            this.colShiftStart,
            this.colShiftEnd,
            this.colDinnerStart,
            this.colDinnerEnd,
            this.colShiftRemark});
            this.gvShift.GridControl = this.gcDict;
            this.gvShift.Name = "gvShift";
            this.gvShift.OptionsView.EnableAppearanceEvenRow = true;
            this.gvShift.OptionsView.EnableAppearanceOddRow = true;
            this.gvShift.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvShift_CellValueChanged);
            this.gvShift.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvShift_ValidateRow);
            this.gvShift.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvShift_ValidatingEditor);
            // 
            // colShiftId
            // 
            this.colShiftId.AppearanceCell.Options.UseTextOptions = true;
            this.colShiftId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftId.AppearanceHeader.Options.UseTextOptions = true;
            this.colShiftId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftId.Caption = "Id";
            this.colShiftId.FieldName = "Id";
            this.colShiftId.Name = "colShiftId";
            this.colShiftId.Visible = true;
            this.colShiftId.VisibleIndex = 0;
            this.colShiftId.Width = 42;
            // 
            // colShiftName
            // 
            this.colShiftName.AppearanceHeader.Options.UseTextOptions = true;
            this.colShiftName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftName.Caption = "��������";
            this.colShiftName.FieldName = "Name";
            this.colShiftName.Name = "colShiftName";
            this.colShiftName.Visible = true;
            this.colShiftName.VisibleIndex = 1;
            this.colShiftName.Width = 301;
            // 
            // colShiftStart
            // 
            this.colShiftStart.AppearanceCell.Options.UseTextOptions = true;
            this.colShiftStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colShiftStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftStart.Caption = "����� ������";
            this.colShiftStart.ColumnEdit = this.teTime;
            this.colShiftStart.FieldName = "Start";
            this.colShiftStart.Name = "colShiftStart";
            this.colShiftStart.Visible = true;
            this.colShiftStart.VisibleIndex = 2;
            this.colShiftStart.Width = 137;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.DisplayFormat.FormatString = "hh:mm";
            this.teTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teTime.EditFormat.FormatString = "t";
            this.teTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teTime.Mask.EditMask = "90:00";
            this.teTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTime.Mask.UseMaskAsDisplayFormat = true;
            this.teTime.Name = "teTime";
            // 
            // colShiftEnd
            // 
            this.colShiftEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colShiftEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colShiftEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftEnd.Caption = "����� ���������";
            this.colShiftEnd.ColumnEdit = this.teTime;
            this.colShiftEnd.FieldName = "End";
            this.colShiftEnd.Name = "colShiftEnd";
            this.colShiftEnd.Visible = true;
            this.colShiftEnd.VisibleIndex = 3;
            this.colShiftEnd.Width = 137;
            // 
            // colDinnerStart
            // 
            this.colDinnerStart.AppearanceCell.Options.UseTextOptions = true;
            this.colDinnerStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDinnerStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colDinnerStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDinnerStart.Caption = "������ �����";
            this.colDinnerStart.ColumnEdit = this.teTime;
            this.colDinnerStart.FieldName = "DinnerStart";
            this.colDinnerStart.Name = "colDinnerStart";
            this.colDinnerStart.Visible = true;
            this.colDinnerStart.VisibleIndex = 4;
            this.colDinnerStart.Width = 144;
            // 
            // colDinnerEnd
            // 
            this.colDinnerEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colDinnerEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDinnerEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colDinnerEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDinnerEnd.Caption = "��������� �����";
            this.colDinnerEnd.ColumnEdit = this.teTime;
            this.colDinnerEnd.FieldName = "DinnerEnd";
            this.colDinnerEnd.Name = "colDinnerEnd";
            this.colDinnerEnd.Visible = true;
            this.colDinnerEnd.VisibleIndex = 5;
            this.colDinnerEnd.Width = 144;
            // 
            // colShiftRemark
            // 
            this.colShiftRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colShiftRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShiftRemark.Caption = "����������";
            this.colShiftRemark.FieldName = "Remark";
            this.colShiftRemark.Name = "colShiftRemark";
            this.colShiftRemark.Visible = true;
            this.colShiftRemark.VisibleIndex = 6;
            this.colShiftRemark.Width = 345;
            // 
            // gvPriority
            // 
            this.gvPriority.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvPriority.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvPriority.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvPriority.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPriority.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPriority.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvPriority.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvPriority.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvPriority.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvPriority.Appearance.Empty.Options.UseBackColor = true;
            this.gvPriority.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPriority.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPriority.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPriority.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPriority.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvPriority.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvPriority.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvPriority.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPriority.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvPriority.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvPriority.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvPriority.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvPriority.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvPriority.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvPriority.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvPriority.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvPriority.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvPriority.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvPriority.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvPriority.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvPriority.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPriority.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPriority.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvPriority.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvPriority.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvPriority.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvPriority.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvPriority.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvPriority.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPriority.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvPriority.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvPriority.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvPriority.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvPriority.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvPriority.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPriority.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPriority.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvPriority.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPriority.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPriority.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.OddRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvPriority.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvPriority.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvPriority.Appearance.Preview.Options.UseBackColor = true;
            this.gvPriority.Appearance.Preview.Options.UseFont = true;
            this.gvPriority.Appearance.Preview.Options.UseForeColor = true;
            this.gvPriority.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPriority.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.Row.Options.UseBackColor = true;
            this.gvPriority.Appearance.Row.Options.UseForeColor = true;
            this.gvPriority.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPriority.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvPriority.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvPriority.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvPriority.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPriority.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPriority.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvPriority.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvPriority.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvPriority.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvPriority.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPriority.Appearance.VertLine.Options.UseBackColor = true;
            this.gvPriority.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPriorId,
            this.colPriorName,
            this.colPriorRemark});
            this.gvPriority.GridControl = this.gcDict;
            this.gvPriority.Name = "gvPriority";
            this.gvPriority.OptionsView.EnableAppearanceEvenRow = true;
            this.gvPriority.OptionsView.EnableAppearanceOddRow = true;
            this.gvPriority.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvPriority_CellValueChanged);
            this.gvPriority.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvPriority_ValidateRow);
            this.gvPriority.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvPriority_ValidatingEditor);
            // 
            // colPriorId
            // 
            this.colPriorId.AppearanceCell.Options.UseTextOptions = true;
            this.colPriorId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriorId.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriorId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriorId.Caption = "Id";
            this.colPriorId.FieldName = "Id";
            this.colPriorId.Name = "colPriorId";
            this.colPriorId.Visible = true;
            this.colPriorId.VisibleIndex = 0;
            this.colPriorId.Width = 39;
            // 
            // colPriorName
            // 
            this.colPriorName.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriorName.Caption = "��������";
            this.colPriorName.FieldName = "Name";
            this.colPriorName.Name = "colPriorName";
            this.colPriorName.Visible = true;
            this.colPriorName.VisibleIndex = 1;
            this.colPriorName.Width = 603;
            // 
            // colPriorRemark
            // 
            this.colPriorRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriorRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriorRemark.Caption = "����������";
            this.colPriorRemark.FieldName = "Remark";
            this.colPriorRemark.Name = "colPriorRemark";
            this.colPriorRemark.Visible = true;
            this.colPriorRemark.VisibleIndex = 2;
            this.colPriorRemark.Width = 608;
            // 
            // bgvTariff
            // 
            this.bgvTariff.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand1,
            this.gridBand2});
            this.bgvTariff.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colTId,
            this.colTDateIssue,
            this.colTvehicle_category,
            this.colTLimit,
            this.colTMove,
            this.colTMoveBt,
            this.colTTime,
            this.colTTimeBt,
            this.colTTimeStop,
            this.colTTimeStopBt});
            this.bgvTariff.GridControl = this.gcDict;
            this.bgvTariff.Name = "bgvTariff";
            this.bgvTariff.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.bgvTariff_InitNewRow);
            this.bgvTariff.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.bgvTariff_CellValueChanged);
            // 
            // colTId
            // 
            this.colTId.AppearanceCell.Options.UseTextOptions = true;
            this.colTId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTId.AppearanceHeader.Options.UseTextOptions = true;
            this.colTId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTId.Caption = "Id";
            this.colTId.FieldName = "Id";
            this.colTId.Name = "colTId";
            this.colTId.Visible = true;
            this.colTId.Width = 56;
            // 
            // colTDateIssue
            // 
            this.colTDateIssue.AppearanceCell.Options.UseTextOptions = true;
            this.colTDateIssue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTDateIssue.AppearanceHeader.Options.UseTextOptions = true;
            this.colTDateIssue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTDateIssue.Caption = "���� ���������";
            this.colTDateIssue.ColumnEdit = this.rdeDate;
            this.colTDateIssue.FieldName = "DateIssue";
            this.colTDateIssue.Name = "colTDateIssue";
            this.colTDateIssue.Visible = true;
            this.colTDateIssue.Width = 167;
            // 
            // rdeDate
            // 
            this.rdeDate.AutoHeight = false;
            this.rdeDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.Name = "rdeDate";
            this.rdeDate.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // colTvehicle_category
            // 
            this.colTvehicle_category.AppearanceCell.Options.UseTextOptions = true;
            this.colTvehicle_category.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colTvehicle_category.AppearanceHeader.Options.UseTextOptions = true;
            this.colTvehicle_category.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTvehicle_category.Caption = "��������� ����������";
            this.colTvehicle_category.ColumnEdit = this.rleVehCategory;
            this.colTvehicle_category.FieldName = "vehicle_category";
            this.colTvehicle_category.Name = "colTvehicle_category";
            this.colTvehicle_category.Visible = true;
            this.colTvehicle_category.Width = 223;
            // 
            // rleVehCategory
            // 
            this.rleVehCategory.AutoHeight = false;
            this.rleVehCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehCategory.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleVehCategory.DisplayMember = "Name";
            this.rleVehCategory.Name = "rleVehCategory";
            this.rleVehCategory.NullText = "";
            // 
            // colTMove
            // 
            this.colTMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTMove.Caption = "������";
            this.colTMove.FieldName = "TMove";
            this.colTMove.Name = "colTMove";
            this.colTMove.Visible = true;
            this.colTMove.Width = 99;
            // 
            // colTTime
            // 
            this.colTTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTime.Caption = "�����";
            this.colTTime.FieldName = "TTime";
            this.colTTime.Name = "colTTime";
            this.colTTime.Visible = true;
            this.colTTime.Width = 99;
            // 
            // colTTimeStop
            // 
            this.colTTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colTTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colTTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeStop.Caption = "�������";
            this.colTTimeStop.FieldName = "TTimeStop";
            this.colTTimeStop.Name = "colTTimeStop";
            this.colTTimeStop.Visible = true;
            this.colTTimeStop.Width = 127;
            // 
            // colTMoveBt
            // 
            this.colTMoveBt.AppearanceCell.Options.UseTextOptions = true;
            this.colTMoveBt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTMoveBt.AppearanceHeader.Options.UseTextOptions = true;
            this.colTMoveBt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTMoveBt.Caption = "������";
            this.colTMoveBt.FieldName = "TMoveBt";
            this.colTMoveBt.Name = "colTMoveBt";
            this.colTMoveBt.Visible = true;
            this.colTMoveBt.Width = 106;
            // 
            // colTTimeBt
            // 
            this.colTTimeBt.AppearanceCell.Options.UseTextOptions = true;
            this.colTTimeBt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeBt.AppearanceHeader.Options.UseTextOptions = true;
            this.colTTimeBt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeBt.Caption = "�����";
            this.colTTimeBt.FieldName = "TTimeBt";
            this.colTTimeBt.Name = "colTTimeBt";
            this.colTTimeBt.Visible = true;
            this.colTTimeBt.Width = 109;
            // 
            // colTTimeStopBt
            // 
            this.colTTimeStopBt.AppearanceCell.Options.UseTextOptions = true;
            this.colTTimeStopBt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeStopBt.AppearanceHeader.Options.UseTextOptions = true;
            this.colTTimeStopBt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTTimeStopBt.Caption = "�������";
            this.colTTimeStopBt.FieldName = "TTimeStopBt";
            this.colTTimeStopBt.Name = "colTTimeStopBt";
            this.colTTimeStopBt.Visible = true;
            this.colTTimeStopBt.Width = 131;
            // 
            // gvEnterpise
            // 
            this.gvEnterpise.ActiveFilterEnabled = false;
            this.gvEnterpise.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.Empty.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvEnterpise.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvEnterpise.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvEnterpise.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvEnterpise.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvEnterpise.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvEnterpise.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvEnterpise.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvEnterpise.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvEnterpise.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvEnterpise.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvEnterpise.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvEnterpise.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvEnterpise.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvEnterpise.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.OddRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.OddRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvEnterpise.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvEnterpise.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvEnterpise.Appearance.Preview.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.Preview.Options.UseFont = true;
            this.gvEnterpise.Appearance.Preview.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvEnterpise.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.Row.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.Row.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvEnterpise.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvEnterpise.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvEnterpise.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvEnterpise.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvEnterpise.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvEnterpise.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvEnterpise.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvEnterpise.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvEnterpise.Appearance.VertLine.Options.UseBackColor = true;
            this.gvEnterpise.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEntId,
            this.colEntName,
            this.colEntDisp,
            this.colEntRem});
            this.gvEnterpise.GridControl = this.gcDict;
            this.gvEnterpise.Name = "gvEnterpise";
            this.gvEnterpise.OptionsView.EnableAppearanceEvenRow = true;
            this.gvEnterpise.OptionsView.EnableAppearanceOddRow = true;
            this.gvEnterpise.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvEnterpise_CellValueChanged);
            this.gvEnterpise.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvEnterpise_CellValueChanging);
            this.gvEnterpise.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvEnterpise_ValidateRow);
            this.gvEnterpise.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvEnterpise_ValidatingEditor);
            // 
            // colEntId
            // 
            this.colEntId.AppearanceCell.Options.UseTextOptions = true;
            this.colEntId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEntId.AppearanceHeader.Options.UseTextOptions = true;
            this.colEntId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEntId.Caption = "Id";
            this.colEntId.FieldName = "Id";
            this.colEntId.Name = "colEntId";
            this.colEntId.OptionsColumn.AllowEdit = false;
            this.colEntId.OptionsColumn.ReadOnly = true;
            this.colEntId.Visible = true;
            this.colEntId.VisibleIndex = 0;
            this.colEntId.Width = 41;
            // 
            // colEntName
            // 
            this.colEntName.AppearanceHeader.Options.UseTextOptions = true;
            this.colEntName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEntName.Caption = "��������";
            this.colEntName.FieldName = "Name";
            this.colEntName.Name = "colEntName";
            this.colEntName.Visible = true;
            this.colEntName.VisibleIndex = 1;
            this.colEntName.Width = 459;
            // 
            // colEntDisp
            // 
            this.colEntDisp.AppearanceHeader.Options.UseTextOptions = true;
            this.colEntDisp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEntDisp.Caption = "���������������";
            this.colEntDisp.ColumnEdit = this.riCheckEdit;
            this.colEntDisp.FieldName = "Dispartchable";
            this.colEntDisp.Name = "colEntDisp";
            this.colEntDisp.Visible = true;
            this.colEntDisp.VisibleIndex = 2;
            this.colEntDisp.Width = 204;
            // 
            // riCheckEdit
            // 
            this.riCheckEdit.AutoHeight = false;
            this.riCheckEdit.Name = "riCheckEdit";
            // 
            // colEntRem
            // 
            this.colEntRem.AppearanceHeader.Options.UseTextOptions = true;
            this.colEntRem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEntRem.Caption = "����������";
            this.colEntRem.FieldName = "Remark";
            this.colEntRem.Name = "colEntRem";
            this.colEntRem.Visible = true;
            this.colEntRem.VisibleIndex = 3;
            this.colEntRem.Width = 546;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tlDictionary);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcDict);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(816, 647);
            this.splitContainerControl1.SplitterPosition = 320;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tlDictionary
            // 
            this.tlDictionary.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.tlDictionary.Appearance.Empty.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.EvenRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.tlDictionary.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedCell.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedCell.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.tlDictionary.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupButton.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HorzLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.OddRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.tlDictionary.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.tlDictionary.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.tlDictionary.Appearance.Preview.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Preview.Options.UseFont = true;
            this.tlDictionary.Appearance.Preview.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.Row.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Row.Options.UseForeColor = true;
            this.tlDictionary.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.tlDictionary.Appearance.TreeLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.VertLine.Options.UseBackColor = true;
            this.tlDictionary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.DicCode,
            this.DicTypeEf,
            this.DicName});
            this.tlDictionary.CustomizationFormBounds = new System.Drawing.Rectangle(60, 493, 208, 168);
            this.tlDictionary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlDictionary.Location = new System.Drawing.Point(0, 0);
            this.tlDictionary.Name = "tlDictionary";
            this.tlDictionary.BeginUnboundLoad();
            this.tlDictionary.AppendNode(new object[] {
            null,
            "���� ���������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "����� �������� - ���������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "�����",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������ ��������",
            null}, -1, 1, 1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "�������",
            null}, 3);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������� ������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "�����������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������� ������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� �����",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "��������� ������������ �������",
            null}, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������, �����",
            null}, -1, -1, -1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������, ����� �������",
            null}, -1, -1, -1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������, ��",
            null}, -1, -1, -1, -1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "���� ��������� ��������",
            null}, -1);
            this.tlDictionary.EndUnboundLoad();
            this.tlDictionary.OptionsBehavior.Editable = false;
            this.tlDictionary.OptionsBehavior.ExpandNodeOnDrag = false;
            this.tlDictionary.OptionsBehavior.PopulateServiceColumns = true;
            this.tlDictionary.OptionsView.EnableAppearanceEvenRow = true;
            this.tlDictionary.OptionsView.EnableAppearanceOddRow = true;
            this.tlDictionary.SelectImageList = this.imCollection;
            this.tlDictionary.Size = new System.Drawing.Size(320, 647);
            this.tlDictionary.StateImageList = this.imCollection;
            this.tlDictionary.TabIndex = 1;
            this.tlDictionary.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.Solid;
            this.tlDictionary.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlDictionary_FocusedNodeChanged);
            // 
            // DicCode
            // 
            this.DicCode.Caption = "���";
            this.DicCode.FieldName = "���";
            this.DicCode.Name = "DicCode";
            this.DicCode.OptionsColumn.ReadOnly = true;
            // 
            // DicTypeEf
            // 
            this.DicTypeEf.Caption = "���";
            this.DicTypeEf.FieldName = "���";
            this.DicTypeEf.Name = "DicTypeEf";
            this.DicTypeEf.Width = 144;
            // 
            // DicName
            // 
            this.DicName.AppearanceHeader.Options.UseTextOptions = true;
            this.DicName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DicName.Caption = "�������� �����������";
            this.DicName.FieldName = "�������� �����������";
            this.DicName.MinWidth = 73;
            this.DicName.Name = "DicName";
            this.DicName.OptionsColumn.ReadOnly = true;
            this.DicName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.DicName.Visible = true;
            this.DicName.VisibleIndex = 0;
            this.DicName.Width = 230;
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "bullet_green.png");
            this.imCollection.Images.SetKeyName(1, "bullet_red.png");
            this.imCollection.Images.SetKeyName(2, "image_edit.png");
            this.imCollection.Images.SetKeyName(3, "checkbox_no.png");
            // 
            // colTLimit
            // 
            this.colTLimit.AppearanceCell.Options.UseTextOptions = true;
            this.colTLimit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTLimit.AppearanceHeader.Options.UseTextOptions = true;
            this.colTLimit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTLimit.Caption = "�����, ��";
            this.colTLimit.FieldName = "LimitKm";
            this.colTLimit.Name = "colTLimit";
            this.colTLimit.Visible = true;
            this.colTLimit.Width = 133;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Columns.Add(this.colTId);
            this.gridBand3.Columns.Add(this.colTDateIssue);
            this.gridBand3.Columns.Add(this.colTvehicle_category);
            this.gridBand3.Columns.Add(this.colTLimit);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.OptionsBand.ShowCaption = false;
            this.gridBand3.Width = 579;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "�����";
            this.gridBand1.Columns.Add(this.colTMove);
            this.gridBand1.Columns.Add(this.colTTime);
            this.gridBand1.Columns.Add(this.colTTimeStop);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 325;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "����� � ������������ ";
            this.gridBand2.Columns.Add(this.colTMoveBt);
            this.gridBand2.Columns.Add(this.colTTimeBt);
            this.gridBand2.Columns.Add(this.colTTimeStopBt);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 346;
            // 
            // MdDictionariesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "MdDictionariesList";
            this.Size = new System.Drawing.Size(816, 647);
            ((System.ComponentModel.ISupportInitialize)(this.gvCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsMotorDepotEntity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTranspType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAlarmType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObjGr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCateg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObjGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEnterpise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        internal DevExpress.XtraTreeList.TreeList tlDictionary;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicName;
        internal DevExpress.XtraGrid.GridControl gcDict;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvCargo;
        private DevExpress.XtraGrid.Columns.GridColumn colCargId;
        private DevExpress.XtraGrid.Columns.GridColumn colCargName;
        private DevExpress.XtraGrid.Columns.GridColumn colCargUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colCargRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvTranspType;
        private DevExpress.XtraGrid.Columns.GridColumn colTransId;
        private DevExpress.XtraGrid.Columns.GridColumn colTransName;
        private DevExpress.XtraGrid.Columns.GridColumn colTransRem;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvAlarmType;
        private DevExpress.XtraGrid.Columns.GridColumn colAlarmId;
        private DevExpress.XtraGrid.Columns.GridColumn colAlarmName;
        private DevExpress.XtraGrid.Columns.GridColumn colAlarmRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvObjGr;
        private DevExpress.XtraGrid.Columns.GridColumn colObjGrId;
        private DevExpress.XtraGrid.Columns.GridColumn colObjGrName;
        private DevExpress.XtraGrid.Columns.GridColumn colObjGrRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvCateg;
        private DevExpress.XtraGrid.Columns.GridColumn colCategId;
        private DevExpress.XtraGrid.Columns.GridColumn colCategName;
        private DevExpress.XtraGrid.Columns.GridColumn colCategRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvState;
        private DevExpress.XtraGrid.Columns.GridColumn colStateId;
        private DevExpress.XtraGrid.Columns.GridColumn colStateName;
        private DevExpress.XtraGrid.Columns.GridColumn colStateMaxTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStateRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvObject;
        private DevExpress.XtraGrid.Columns.GridColumn colObjId;
        private DevExpress.XtraGrid.Columns.GridColumn colObjGroupe;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleObjGroup;
        private DevExpress.XtraGrid.Columns.GridColumn colObjNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colObjZone;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleZones;
        private DevExpress.XtraGrid.Columns.GridColumn colObjName;
        private DevExpress.XtraGrid.Columns.GridColumn colObjParent;
        private DevExpress.XtraGrid.Columns.GridColumn colObjTime;
        private DevExpress.XtraGrid.Columns.GridColumn colObjRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteId;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteObjFrom;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleObject;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteObjTo;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteDist;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvLoading;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadId;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadObj;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadCargo;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleCargo;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadTimeLoad;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadTimeUnLoad;
        private DevExpress.XtraGrid.Columns.GridColumn colLoadRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvShift;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftId;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftName;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorId;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorName;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvEnterpise;
        private DevExpress.XtraGrid.Columns.GridColumn colEntId;
        private DevExpress.XtraGrid.Columns.GridColumn colEntName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit riCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colEntDisp;
        private DevExpress.XtraGrid.Columns.GridColumn colEntRem;
        private DevExpress.Utils.ImageCollection imCollection;
        internal System.Windows.Forms.BindingSource bsMotorDepotEntity;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicTypeEf;
        private DevExpress.XtraGrid.Columns.GridColumn colDinnerStart;
        private DevExpress.XtraGrid.Columns.GridColumn colDinnerEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTariffication;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTDateIssue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTvehicle_category;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTTimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTMoveBt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTTimeBt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTTimeStopBt;
        internal DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvTariff;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDate;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehCategory;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTLimit;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;

    }
}
