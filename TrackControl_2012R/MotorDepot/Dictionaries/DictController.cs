﻿using System;
using System.Diagnostics;
using System.Linq;
using DevExpress.XtraEditors;

namespace MotorDepot
{
    internal static class DictController
    {
        internal static MotorDepotEntities ContextMotorDepot;

        internal static void SetViewDataSource(MdDictionariesList  view, MdDictionaries typeDictionary,bool isEditable)
        {

            try
            {
                switch ( typeDictionary )
                {
                    case MdDictionaries.Enterprise:
                        {
                           
                            view.gcDict.MainView = view.gvEnterpise;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.enterpriseSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.enterpriseSet.ToList();
                            view.gvEnterpise.OptionsBehavior.Editable = isEditable; 
                            break;
                        }
                    case MdDictionaries.Cargo:
                        {
                            view.gcDict.MainView = view.gvCargo;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.cargoSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.cargoSet.ToList();
                            view.gvCargo.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.TransportationType:
                        {
                            view.gcDict.MainView = view.gvTranspType;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.transportation_typesSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.transportation_typesSet.ToList();
                            view.gvTranspType.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.AlarmingType:
                        {
                            view.gcDict.MainView = view.gvAlarmType;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.alarming_typesSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.alarming_typesSet.ToList();
                            view.gvAlarmType.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.ObjectsGroup:
                        {
                            view.gcDict.MainView = view.gvObjGr;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.object_groupSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.object_groupSet.ToList();
                            view.gvObjGr.OptionsBehavior.Editable = isEditable;
                                break;
                        }
                    case MdDictionaries.Object:
                        {
                            view.gcDict.MainView = view.gvObject;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.object_groupSet);
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.zonesSet);
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.md_objectSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.md_objectSet.ToList();
                            view.rleObjGroup.DataSource = ContextMotorDepot.object_groupSet.ToList();
                            view.rleZones.DataSource = ContextMotorDepot.zonesSet.ToList();
                            view.gvObject.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.OrderCategory:
                        {
                            view.gcDict.MainView = view.gvCateg;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.order_categorySet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.order_categorySet.ToList();
                            view.gvCateg.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.VehicleState:
                        {
                            view.gcDict.MainView = view.gvState;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.order_categorySet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.vehicle_stateSet.ToList();
                            view.gvState.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.Route:
                        {
                            view.gcDict.MainView = view.gvRoute;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.routeSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.routeSet.ToList();
                            view.rleObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
                            view.gvRoute.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.LoadUnloadTime:
                        {
                            view.gcDict.MainView = view.gvLoading;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.cargoSet);
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.md_objectSet);
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.loading_timeSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.loading_timeSet.ToList();
                            view.rleObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
                            view.rleCargo.DataSource = ContextMotorDepot.cargoSet.ToList();
                            view.gvLoading.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.WorkingShift:
                        {
                            view.gcDict.MainView = view.gvShift;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.working_shiftSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.working_shiftSet.ToList();
                            view.gvShift.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.OrderPriority:
                        {
                            view.gcDict.MainView = view.gvPriority;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.order_prioritySet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.order_prioritySet.ToList();
                            view.gvPriority.OptionsBehavior.Editable = isEditable;
                            break;
                        }
                    case MdDictionaries.Tariffs:
                        {
                            view.gcDict.MainView = view.bgvTariff;
                            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ContextMotorDepot.tariffSet);
                            view.bsMotorDepotEntity.DataSource = ContextMotorDepot.tariffSet.ToList();
                            view.bgvTariff.OptionsBehavior.Editable = isEditable;
                            view.rleVehCategory.DataSource = ContextMotorDepot.vehicle_categorySet.ToList();
                            break;
                        }
                }
            } // try
            catch ( Exception e )
            {
                StackTrace stckTrace = new StackTrace( e, true );
                if ( stckTrace.FrameCount > 0 )
                {
                    StackFrame frame = stckTrace.GetFrame( stckTrace.FrameCount - 1 );
                    int errorLine = frame.GetFileLineNumber();
                    string functionName = frame.GetMethod().Name;
                    XtraMessageBox.Show( "File DictController.cs: " + e.Message + "\n" +
                        "Error in " + functionName + " method on line " + errorLine );
                }
            }
        }

        internal static bool DeleteItem(MdDictionariesList view, MdDictionaries typeDictionary)
        {
            object entity = null;
            try
            {
                switch (typeDictionary)
                {
                    case MdDictionaries.Enterprise:
                        {
                            entity = view.gvEnterpise.GetRow(view.gvEnterpise.FocusedRowHandle) as enterprise;
                           
                        }
                        break;
                    case MdDictionaries.Cargo:
                        {
                            entity = view.gvCargo.GetRow(view.gvCargo.FocusedRowHandle) as cargo;
                        }
                        break;
                    case MdDictionaries.TransportationType:
                        {
                            entity = view.gvTranspType.GetRow(view.gvTranspType.FocusedRowHandle) as transportation_types;
                        }
                        break;
                    case MdDictionaries.AlarmingType:
                        {
                            entity = view.gvAlarmType.GetRow(view.gvAlarmType.FocusedRowHandle) as alarming_types;
                        }
                        break;
                    case MdDictionaries.ObjectsGroup:
                        {
                            entity = view.gvObjGr.GetRow(view.gvObjGr.FocusedRowHandle) as object_group;
                            break;
                        }
                    case MdDictionaries.Object:
                        {
                            entity = view.gvObject.GetRow(view.gvObject.FocusedRowHandle) as md_object;
                            break;
                        }
                    case MdDictionaries.OrderCategory:
                        {
                            entity = view.gvCateg.GetRow(view.gvCateg.FocusedRowHandle) as order_category;
                            break;
                        }
                    case MdDictionaries.VehicleState:
                        {
                            entity = view.gvState.GetRow(view.gvState.FocusedRowHandle) as vehicle_state;
                            break;
                        }
                    case MdDictionaries.Route:
                        {
                            entity = view.gvRoute.GetRow(view.gvRoute.FocusedRowHandle) as route;
                            break;
                        }
                    case MdDictionaries.LoadUnloadTime:
                        {
                            entity = view.gvLoading.GetRow(view.gvLoading.FocusedRowHandle) as loading_time;
                            break;
                        }
                    case MdDictionaries.WorkingShift:
                        {
                            entity = view.gvShift.GetRow(view.gvShift.FocusedRowHandle) as working_shift;
                            break;
                        }
                    case MdDictionaries.OrderPriority:
                        {
                            entity = view.gvPriority.GetRow(view.gvPriority.FocusedRowHandle) as order_priority;
                            break;
                        }
                    case MdDictionaries.Tariffs:
                        {
                            entity = view.bgvTariff.GetRow(view.bgvTariff.FocusedRowHandle) as tariff;
                            break;
                        }
                }
                if (entity != null)
                {
                    ContextMotorDepot.DeleteObject(entity);
                    return MDController.Instance.MotorDepotEntitiesSaveChanges(entity);
                }
                return false; 
            }
            catch
            {
                return false;
            }
        }

        internal static void SaveItem(MdDictionariesList view, MdDictionaries typeDictionary)
        {
            object entity = null;
            switch (typeDictionary)
            {
                case MdDictionaries.Enterprise:
                    {
                        entity = view.gvEnterpise.GetRow(view.gvEnterpise.FocusedRowHandle) as enterprise;
                        if (((enterprise)entity).Id == 0) ContextMotorDepot.AddToenterpriseSet((enterprise)entity);
                        break;
                    }
                    
                case MdDictionaries.Cargo:
                    {
                        entity = view.gvCargo.GetRow(view.gvCargo.FocusedRowHandle) as cargo;
                        if (entity != null && ((cargo)entity).Id == 0) ContextMotorDepot.AddTocargoSet((cargo)entity);
                        break;
                    }
                    
                case MdDictionaries.TransportationType:
                    {
                         entity = view.gvTranspType.GetRow(view.gvTranspType.FocusedRowHandle) as transportation_types;
                         if (entity != null && ((transportation_types)entity).Id == 0) ContextMotorDepot.AddTotransportation_typesSet((transportation_types)entity);
                        break;
                    }

                case MdDictionaries.AlarmingType:
                    {
                        entity = view.gvAlarmType.GetRow(view.gvAlarmType.FocusedRowHandle) as alarming_types;
                        if (entity != null && ((alarming_types)entity).Id == 0) ContextMotorDepot.AddToalarming_typesSet((alarming_types)entity);
                        break;
                    }
                case MdDictionaries.ObjectsGroup:
                    {
                        entity = view.gvObjGr.GetRow(view.gvObjGr.FocusedRowHandle) as object_group;
                        if (entity != null && ((object_group)entity).Id == 0) ContextMotorDepot.AddToobject_groupSet((object_group)entity);
                        break;
                    }
                case MdDictionaries.Object:
                    {
                         entity = view.gvObject.GetRow(view.gvObject.FocusedRowHandle) as md_object;
                         if (entity != null && ((md_object)entity).Id == 0) ContextMotorDepot.AddTomd_objectSet((md_object)entity);
                        break;
                    }
                case MdDictionaries.OrderCategory:
                    {
                         entity = view.gvCateg.GetRow(view.gvCateg.FocusedRowHandle) as order_category;
                         if (entity != null && ((order_category)entity).Id == 0) ContextMotorDepot.AddToorder_categorySet((order_category)entity);
                        break;
                    }
                case MdDictionaries.VehicleState:
                    {
                         entity = view.gvState.GetRow(view.gvState.FocusedRowHandle) as vehicle_state;
                         if (entity != null && ((vehicle_state)entity).Id == 0) ContextMotorDepot.AddTovehicle_stateSet((vehicle_state)entity);
                        break;
                    }
                case MdDictionaries.Route:
                    {
                         entity = view.gvRoute.GetRow(view.gvRoute.FocusedRowHandle) as route;
                         if (entity != null && ((route)entity).Id == 0) ContextMotorDepot.AddTorouteSet((route)entity);
                        break;
                    }
                case MdDictionaries.LoadUnloadTime:
                    {
                         entity = view.gvLoading.GetRow(view.gvLoading.FocusedRowHandle) as loading_time;
                         if (entity != null && ((loading_time)entity).Id == 0) ContextMotorDepot.AddToloading_timeSet((loading_time)entity);
                        break;
                    }
                case MdDictionaries.WorkingShift:
                    {
                        entity = view.gvShift.GetRow(view.gvShift.FocusedRowHandle) as working_shift;
                        if (entity != null && ((working_shift)entity).Id == 0) ContextMotorDepot.AddToworking_shiftSet((working_shift)entity);
                        break;
                    }
                case MdDictionaries.OrderPriority:
                    {
                         entity = view.gvPriority.GetRow(view.gvPriority.FocusedRowHandle) as order_priority;
                         if (entity != null && ((order_priority)entity).Id == 0) ContextMotorDepot.AddToorder_prioritySet((order_priority)entity);
                        break;
                    }
                case MdDictionaries.Tariffs:
                    {
                         entity = view.bgvTariff.GetRow(view.bgvTariff.FocusedRowHandle) as tariff;
                         if (entity != null && ((tariff)entity).Id == 0) ContextMotorDepot.AddTotariffSet((tariff)entity);
                        break;
                    } 
            }
                
            MDController.Instance.MotorDepotEntitiesSaveChanges(entity);

        }

        internal static MdDictionaries GetDictionaryCode(string typeEf)
        {
            switch (typeEf)
            {
                case "enterprise":
                    {
                        return MdDictionaries.Enterprise;
                    }
                case "transportation_types":
                    {
                        return MdDictionaries.TransportationType;
                    }
                case "loading_time":
                    {
                        return MdDictionaries.LoadUnloadTime;
                    }
                case "cargo":
                    {
                        return MdDictionaries.Cargo;
                    }
                case "object_group":
                    {
                        return MdDictionaries.ObjectsGroup;
                    }
                case "md_object":
                    {
                        return MdDictionaries.Object;
                    }
                case "order_category":
                    {
                        return MdDictionaries.OrderCategory;
                    }
                case "route":
                    {
                        return MdDictionaries.Route;
                    }
                case "order_priority":
                    {
                        return MdDictionaries.OrderPriority;
                    }
                case "working_shift":
                    {
                        return MdDictionaries.WorkingShift;
                    }
                case "vehicle_state":
                    {
                        return MdDictionaries.VehicleState;
                    }
                case "tariff":
                    {
                        return MdDictionaries.Tariffs;
                    }
                case "alarming_types":
                    {
                        return MdDictionaries.AlarmingType;
                    }
                default:
                    return MdDictionaries.Enterprise;

            }
        }
    }
}
