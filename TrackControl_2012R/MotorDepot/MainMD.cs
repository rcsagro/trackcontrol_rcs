using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using MotorDepot.AccessObjects;
using MotorDepot.DifferentSheets;
using MotorDepot.Reports;
using TrackControl.General;
using TrackControl.GMap.Core;
using TrackControl.Online;
using OnlineController = MotorDepot.MonitoringGps.OnlineController;

namespace MotorDepot
{
    public partial class MainMD : DevExpress.XtraEditors.XtraForm, IAccessFormControls
    {
        private MDController _formController;
        public event Action<int> StartOnline = delegate { };
        public event VoidHandler StopOnline = delegate { };
        public event VoidHandler SelecteVehicleOnMap = delegate { };

        public MainMD(MDController formController)
        {
            InitializeComponent();
            _formController = formController;
            InitControls();
        }

        private void MainMD_Load(object sender, EventArgs e)
        {
            SetControls();
            SetAccessRules();
            SetMapLogTab();
        }

        void SetControls()
        {
            switch (xtbMain.SelectedTabPageIndex)
            {
                case 0:
                    {
                        bbiDelete.Enabled = false;
                        bbiAdd.Enabled = false;
                        bbiEdit.Enabled = false;
                        bbiCopy.Enabled = false;
                        beiNumber.Enabled = false;
                        bdeStart.Enabled = false;
                        bdeEnd.Enabled = false;
                        bbiPrintForms.Enabled = false;
                        beSwShowDeleted.Enabled = false;
                        bbiShowLog.Enabled = false;
                        break;
                    }
                case 1:
                    {
                        bbiDelete.Enabled = false;
                        bbiAdd.Enabled = false;
                        bbiEdit.Enabled = false;
                        bbiCopy.Enabled = false;
                        beiNumber.Enabled = false;
                        bdeStart.Enabled = false;
                        bdeEnd.Enabled = false;
                        bbiPrintForms.Enabled = false;
                        beSwShowDeleted.Enabled = false;
                        bbiShowLog.Enabled = true;
                        break;
                    }
                case 2:
                    {
                        if (MdUser.Instance.IsObjectVisibleForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd)
                        && MdUser.Instance.IsObjectEditableForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd))
                        {
                            bbiDelete.Enabled = true;
                            bbiAdd.Enabled = true;
                        }
                        else
                        {
                            bbiDelete.Enabled = false;
                            bbiAdd.Enabled = false;
                        }
                        bbiEdit.Enabled = true;
                        bbiCopy.Enabled = false;
                        beiNumber.Enabled = true;
                        bdeStart.Enabled = true;
                        bdeEnd.Enabled = true;
                        bbiPrintForms.Enabled = true;
                        beSwShowDeleted.Enabled = false;
                        bbiShowLog.Enabled = false;
                        break;
                    }
                case 3:
                    {
                        bbiEdit.Enabled = true;
                        if (MdUser.Instance.IsObjectVisibleForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd)
                            && MdUser.Instance.IsObjectEditableForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd))
                        {
                            bbiCopy.Enabled = true;
                            bbiAdd.Enabled = true;
                            bbiDelete.Enabled = true;
                        }
                        else
                        {
                            bbiCopy.Enabled = false;
                            bbiAdd.Enabled = false;
                            bbiDelete.Enabled = false;
                        }
                        beiNumber.Enabled = true;
                        bdeStart.Enabled = true;
                        bdeEnd.Enabled = true;
                        bbiPrintForms.Enabled = false;
                        beSwShowDeleted.Enabled = true;
                        bbiShowLog.Enabled = false;
                        break;
                    }
                case 4:
                    {
                        bbiDelete.Enabled = false;
                        bbiAdd.Enabled = false;
                        bbiEdit.Enabled = false;
                        bbiCopy.Enabled = false;
                        beiNumber.Enabled = false;
                        bdeStart.Enabled = false;
                        bdeEnd.Enabled = false;
                        bbiPrintForms.Enabled = false;
                        beSwShowDeleted.Enabled = false;
                        bbiShowLog.Enabled = false;
                        break;
                    }
            }
        }

        void InitControls()
        {

            DateTime start =DateTime.Today;
            DateTime end = DateTime.Today.AddDays(1);
            deStart.NullDate = start;
            deEnd.NullDate = end;
            bdeStart.EditValue = start;
            bdeEnd.EditValue = end;
            deStart.NullText = start.ToString("dd.MM.yyyy");
            deEnd.NullText = end.ToString("dd.MM.yyyy");
            bbiStart.Glyph = Shared.StartMain;
            InitVehiclesList();
            InitGoogleMapControl();
            InitDictionaryList();
            InitReports();
            InitUserRoles();
        }

        #region IAccessFormControls
        public void SetAccessRules()
        {
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd))
                xtpWB.PageVisible = false;
            else
                xtpWB.PageVisible = true;
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd))
                xtpOrder.PageVisible = false;
            else
                xtpOrder.PageVisible = true;
            if (!UserBaseCurrent.Instance.Admin)
            {
                xtpUserRoles.PageVisible = false;
            }
            else
            {
                xtpUserRoles.PageVisible = true;
            }

        } 
        #endregion

        private void InitVehiclesList()
        {
            splitMap.Panel1.Controls.Add(_formController.MdVehList);
            _formController.MdVehList.SelecteVehicleOnMap += RunSelecteVehicleOnMap;
            _formController.MdVehList.Dock = DockStyle.Fill;
        }

        private void InitDictionaryList()
        {
            if (_formController.MdDictList.IsObjectsExists)
            {
                xtpDictionary.PageVisible = true;
                xtpDictionary.Controls.Add(_formController.MdDictList);
                _formController.MdDictList.Dock = DockStyle.Fill;
            }
            else
            {
                xtpDictionary.PageVisible = false;
            }

        }

        void RunSelecteVehicleOnMap()
        {
            if (SelecteVehicleOnMap != null) SelecteVehicleOnMap();
        }

        private void InitGoogleMapControl()
        {

            splitMap.Panel2.Controls.Add(_formController.MdGoogleMap);
            _formController.MdGoogleMap.Dock = DockStyle.Fill;
            _formController.MdGoogleMap.MapType = MapType.GoogleMap;
            _formController.MdGoogleMap.State = new MapState(ConstsGen.KievZoom, new PointLatLng(ConstsGen.KievLat, ConstsGen.KievLng));
        }

        private void InitYandexMapControl()
        {
            splitMap.Panel2.Controls.Add(_formController.MdGoogleMap);
            _formController.MdGoogleMap.Dock = DockStyle.Fill;
            _formController.MdGoogleMap.MapType = MapType.YandexMap;
            _formController.MdGoogleMap.State = new MapState(ConstsGen.KievZoom, new PointLatLng(ConstsGen.KievLat, ConstsGen.KievLng));
        }

        private void InitReports()
        {
            if (_formController.MdReports.IsObjectsExists)
            {
                xtpReports.PageVisible = true;
                xtpReports.Controls.Add(_formController.MdReports);
                _formController.MdReports.Dock = DockStyle.Fill;
            }
            else
            {
                xtpReports.PageVisible = false;
            }
        }

        private void InitUserRoles()
        {
            xtpUserRoles.Controls.Add(_formController.MdUsersList);
            _formController.MdUsersList.Dock = DockStyle.Fill;
        }

        private void xtbMain_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            LoadTab(false);
            SetControls();
        }

        private void LoadTab(bool setMapLogTab)
        {
            switch (xtbMain.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (setMapLogTab) SetMapLogTab();
                        break;
                    }
                case 1:
                    {
                        _formController.MdDictList.SetDataSource();   
                        break;
                    }
                case 2:
                    {
                        SetWayBillTab();
                        break;
                    }
                case 3:
                    {
                        SetOrderTab();
                        break;
                    }
                case 5:
                    {
                        _formController.MdUsersList.SetUsersWithRolesListDataSource();  
                        break;
                    }
                                       
            }
        }

        private void SetMapLogTab()
        {
            _formController.MdVehList.RefreshTree();
            _formController.UpdateLogInvoke();
        }
		
        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (xtbMain.SelectedTabPageIndex)
            {
                case 1:
                    {
                        break;
                    }
                case 2:
                    {
                        DeleteWayBill();
                        break;
                    }
                case 3:
                    {
                        DeleteOrder();
                        break;
                    }
            }
            SetControls();
        }


        // aketner
        #region ������� ����
        
		 void SetWayBillTab()
		 {
		     _formController.WayBillSetViewDataSource();
		 }

        private void ViewWayBillFromGournal()
        {
            waybill wbForView = gvWayBill.GetRow(gvWayBill.FocusedRowHandle) as waybill;
            if (wbForView is waybill)
            {
                _formController.WayBillView(wbForView);
            }
        }

        private void DeleteWayBill()
        {
            {
                waybill wbForDelete = gvWayBill.GetRow(gvWayBill.FocusedRowHandle) as waybill;
                if (WayBillController.DeleteItem(wbForDelete,false))
                {
                    gvWayBill.DeleteRow(gvWayBill.FocusedRowHandle);
                }
            }
        }

        private void gvWayBill_DoubleClick(object sender, EventArgs e)
        {
            ViewWayBillFromGournal();
        }

	    #endregion

        #region ����� - �����
        
        void SetOrderTab()
        {
            _formController.OrderSetViewDataSource((bool)beSwShowDeleted.EditValue);
        }

        private void gvOrder_DoubleClick(object sender, EventArgs e)
        {
            ViewOrderFromGournal();
        }

        private void ViewOrderFromGournal()
        {
            var orderForView = gvOrder.GetRow(gvOrder.FocusedRowHandle);
            if (orderForView is order)
            {
                _formController.OrderViewOrder((order)orderForView);
            }
        }

        private void DeleteOrder()
        {
            {
                order entity = gvOrder.GetRow(gvOrder.FocusedRowHandle) as order;
                if (OrderController.DeleteOrder(entity))
                {
                    gvOrder.DeleteRow(gvOrder.FocusedRowHandle);
                }
            }
        }

        private void gvOrder_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var entity = gvOrder.GetRow(gvOrder.FocusedRowHandle);
            if (entity is order)
            {
                OrderController.SaveOrder((order)entity);
            }
        }

        #endregion

        private void MainMD_FormClosed(object sender, FormClosedEventArgs e)
            {
                if (_formController != null) 
                    _formController.Dispose();
            }

        #region ������ �������� ����
        private void bbiAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbMain.SelectedTabPageIndex)
            {
                case 1:
                    {
                        //SetDictionaryTab();
                        break;
                    }
                case 2:
                    {
                        _formController.WayBillAddNew();
                        break;
                    }
                case 3:
                    {
                        _formController.OrderAddNewOrder();
                        break;
                    }
            }
        }

        private void bbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (xtbMain.SelectedTabPageIndex)
            {
               
                case 1:
                    {
                        //SetDictionaryTab();
                        break;
                    }
                case 2:
                    {
                        ViewWayBillFromGournal();
                        break;
                    }
                case 3:
                    {
                        ViewOrderFromGournal();
                        break;
                    }
            }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadTab(true);
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            Int32[] numSelectRows = gvWayBill.GetSelectedRows();

            for (int i = 0; i < numSelectRows.Length; i++)
            {
                WayReportPageOne reportWayPageOne = new WayReportPageOne();
                MotorDepot.waybill Row = (MotorDepot.waybill)gvWayBill.GetRow(numSelectRows[i]);
                reportWayPageOne.setDataForWaySheet(Row);
                reportWayPageOne.PrintingSystem.PageMargins.Bottom = 10;
                reportWayPageOne.PrintingSystem.PageMargins.Top = 20;
                reportWayPageOne.CreateDocument();

                WayReportPageTwo reportWayPageTwo = new WayReportPageTwo();
                reportWayPageTwo.CreateDocument();

                reportWayPageOne.Pages.AddRange(reportWayPageTwo.Pages);
                reportWayPageOne.PrintingSystem.ContinuousPageNumbering = true;
                //reportWayPageOne.ShowPreview(); aketner
            } // for
        } // barButtonItem4_ItemClick

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            WayReportPageOne reportWayPageOne = new WayReportPageOne();
            reportWayPageOne.PrintingSystem.PageMargins.Bottom = 10;
            reportWayPageOne.PrintingSystem.PageMargins.Top = 20;
            reportWayPageOne.CreateDocument();
            WayReportPageTwo reportWayPageTwo = new WayReportPageTwo();
            reportWayPageTwo.CreateDocument();
            reportWayPageOne.Pages.AddRange(reportWayPageTwo.Pages);
            reportWayPageOne.PrintingSystem.ContinuousPageNumbering = true;
            //reportWayPageOne.ShowPreview(); aketner
        } // barButtonItem5_ItemClick

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            Int32[] numSelectRows = gvWayBill.GetSelectedRows();

            for (int i = 0; i < numSelectRows.Length; i++)
            {
                CustomerTicket ticketReport = new CustomerTicket();
                MotorDepot.waybill Row = (MotorDepot.waybill)gvWayBill.GetRow(numSelectRows[i]);
                ticketReport.setDataForWayTicket(Row);
                //ticketReport.ShowPreview(); aketner
            }
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            CustomerTicket ticketReport = new CustomerTicket();
            //ticketReport.ShowPreview(); aketner
        }
        #endregion

        private void SwitchWorkRegime()
        {
            if (bbiStart.Caption == "����")
            {
                bbiStart.Enabled = true;
                bbiStart.Caption = "����";
                bbiStart.Glyph = Shared.StopMain;
                if (StartOnline != null)
                {
                    
                    StartOnline(Convert.ToInt32(beiTail.EditValue));
                    OrderController.UpdateOrderState += _formController.UpdateLogInvoke;
                }
            }
            else
            {
                if (bbiStart.Caption == "����") return;
                bbiStart.Caption = "����";
                bbiStart.Glyph = Shared.StartMain;
                if (StopOnline != null)
                {
                    StopOnline();
                    OrderController.UpdateOrderState -= _formController.UpdateLogInvoke;
                }
            }
            Application.DoEvents();
        }

        private void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            SwitchWorkRegime();
        }

        /// <summary>
        /// ��������� ������ �������������������
        /// </summary>
        public bool IsAutoFit
        {
            get { return bchAutoFit.Checked; }
            set { bchAutoFit.Checked = value; }
        }

        /// <summary>
        /// �������� � ���� "�����"
        /// </summary>
        public int Tail
        {
            get { return Convert.ToInt32(beiTail.EditValue); }
            set { beiTail.EditValue = value; }
        }

        private void bbiCopy_ItemClick(object sender, ItemClickEventArgs e)
        {
            var entity = gvOrder.GetRow(gvOrder.FocusedRowHandle) ;
            if (entity is order)
            {
                if (OrderController.CopyOrder((order)entity)) SetOrderTab();   
            }
        }

        private void gvLog_DoubleClick(object sender, EventArgs e)
        {
            var logWithOrder = gvLog.GetRow(gvLog.FocusedRowHandle);
            if (logWithOrder is log && ((log)logWithOrder).md_order != null)
                if (((log)logWithOrder).md_order is order)
            {
                _formController.OrderViewOrder((order)((log)logWithOrder).md_order);
            }
        }

        private void gvLog_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            log logRecord = gvLog.GetRow(e.RowHandle) as log;
            if (logRecord != null && logRecord.IsRead != null)
            {
                if (!(bool)logRecord.IsRead)
                    e.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
                else
                    e.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F);
            }

        }

        private void SetIsRead(int rowHandle)
        {
            log logRecord = gvLog.GetRow(rowHandle) as log;
            if (logRecord != null && !(bool)logRecord.IsRead)
            {
            logRecord.IsRead = true;
            LogProvider.SetLogRecordReady(logRecord);  
            }
        }

        private void gvLog_Click(object sender, EventArgs e)
        {
            SetIsRead(gvLog.FocusedRowHandle);
            SelecteVehicle(gvLog.FocusedRowHandle);
        }


        void SelecteVehicle(int rowHandle)
        {
            log logRecord = gvLog.GetRow(rowHandle) as log;
            if (logRecord != null && OnlineController.Instance.IsStarted)
            {
                foreach (OnlineVehicle ov in OnlineController.Instance.OnlineVehicles)
                {
                    if(logRecord.md_waybill.vehicle.id == ov.Id)
                    {
                        ov.IsSelected = true;
                        
                    }
                    else
                    {
                        ov.IsSelected = false;
                    }
                }
                SelecteVehicleOnMap();
            }
        }

        void DeselectVehicle()
        {
            foreach (OnlineVehicle ov in OnlineController.Instance.OnlineVehicles)
            {
                ov.IsSelected = false;
            }
            SelecteVehicleOnMap();
        }

        private void gvLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
           if (gvLog.FocusedRowHandle >= 0) SelecteVehicle(gvLog.FocusedRowHandle);
        }

        private void bbiShowLog_ItemClick(object sender, ItemClickEventArgs e)
        {
            UserLog.ViewLog(UserLogTypes.MD_DICTIONARY, _formController.MdDictList.FocusedDictionaryCode());
        }



        private void bbiClearVehSelection_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeselectVehicle(); 
        }



    }
}