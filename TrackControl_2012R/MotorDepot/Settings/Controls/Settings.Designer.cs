namespace MotorDepot.Settings
{
    partial class MDSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.chDataAnaliz = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.crTotalSettings = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsDataAnalizOn = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsReceiveAdressOn = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDataAnaliz)).BeginInit();
            this.SuspendLayout();
            // 
            // pgParams
            // 
            this.pgParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgParams.Location = new System.Drawing.Point(0, 0);
            this.pgParams.Name = "pgParams";
            this.pgParams.RecordWidth = 51;
            this.pgParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chDataAnaliz});
            this.pgParams.RowHeaderWidth = 149;
            this.pgParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crTotalSettings});
            this.pgParams.Size = new System.Drawing.Size(414, 483);
            this.pgParams.TabIndex = 3;
            // 
            // chDataAnaliz
            // 
            this.chDataAnaliz.AutoHeight = false;
            this.chDataAnaliz.Name = "chDataAnaliz";
            // 
            // crTotalSettings
            // 
            this.crTotalSettings.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsDataAnalizOn,
            this.erIsReceiveAdressOn});
            this.crTotalSettings.Name = "crTotalSettings";
            this.crTotalSettings.Properties.Caption = "����� ���������";
            // 
            // erIsDataAnalizOn
            // 
            this.erIsDataAnalizOn.Name = "erIsDataAnalizOn";
            this.erIsDataAnalizOn.Properties.Caption = "�������� ���������� �������� ��� ��������";
            this.erIsDataAnalizOn.Properties.FieldName = "IsDataAnalizOn";
            this.erIsDataAnalizOn.Properties.RowEdit = this.chDataAnaliz;
            // 
            // erIsReceiveAdressOn
            // 
            this.erIsReceiveAdressOn.Name = "erIsReceiveAdressOn";
            this.erIsReceiveAdressOn.Properties.Caption = "�������� �������������� ����������� ������ ��� ��������� ������";
            this.erIsReceiveAdressOn.Properties.FieldName = "IsReceiveAdressOn";
            this.erIsReceiveAdressOn.Properties.RowEdit = this.chDataAnaliz;
            // 
            // MDSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pgParams);
            this.Name = "MDSettings";
            this.Size = new System.Drawing.Size(414, 483);
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDataAnaliz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl pgParams;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chDataAnaliz;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotalSettings;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsDataAnalizOn;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsReceiveAdressOn;

    }
}
