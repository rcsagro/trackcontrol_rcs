﻿using TrackControl.General;

namespace MotorDepot.AccessObjects
{
    public class AccessDocuments : AccessObject
    {
        public override int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.DocumentsMd; }
        }
    }
}
