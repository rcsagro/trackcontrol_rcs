﻿
namespace MotorDepot.AccessObjects
{
    public class AccessDataRecord
    {
        public string TypeEf { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
        public bool IsEdit { get; set; }
        public AccessDataRecord(string typeEf, string name)
        {
            TypeEf = typeEf;
            Name = name;
            IsVisible = false;
            IsEdit = false;
        }
    }
}
