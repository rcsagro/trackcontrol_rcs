﻿using System;
using System.Linq;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.Core;

namespace MotorDepot.AccessObjects
{
    public class MdUser : Singleton <MdUser>
    {

        acs_roles _mdUserRole;
        acs_users _mdUser;
        enterprise _mdUserEnterprise;

        public enterprise Enterprise
        {
            get { return _mdUserEnterprise; }
            set { _mdUserEnterprise = value; }
        }

        public MdUser()
        {

        }

        public bool IsUserHasAccessToModule()
        {
            if (UserBaseCurrent.Instance.Admin) return true;
            int countLinks = MDController.Instance.ContextMotorDepot.acs_usersSet.Where(u => u.users_list.Id == UserBaseCurrent.Instance.Id).Count();
            if (countLinks == 0)
            {
                XtraMessageBox.Show(String.Format("Пользователь {0} не имеет доступа к модулю!", UserBaseCurrent.Instance.Name), MDController.MesCaption);
                return false;
            }
            else
            {
                _mdUser = MDController.Instance.ContextMotorDepot.acs_usersSet.Where(u => u.users_list.Id == UserBaseCurrent.Instance.Id).FirstOrDefault();
                _mdUserRole = _mdUser.md_acs_roles;
                _mdUserEnterprise = _mdUser.md_enterprise;  
                return true;
            }

        }

        public bool IsObjectVisibleForRole(string objectDateType, int objectType)
        {
            if (UserBaseCurrent.Instance.Admin) return true;
            if (_mdUserRole == null) return false;
            var userRoleObject = GetRoleObject(objectDateType, objectType); 
            if (userRoleObject != null)
            {
                return userRoleObject.IsVisible;
            }
            else
            return false;
        }

        public bool IsObjectEditableForRole(string objectDateType, int objectType)
        {
            if (UserBaseCurrent.Instance.Admin) return true;
            if (_mdUserRole == null) return false;
            var userRoleObject = GetRoleObject(objectDateType, objectType);
            if (userRoleObject != null)
            {
                return userRoleObject.IsEdit;
            }
            else
                return false;
        }

        private acs_roles_objects GetRoleObject(string objectDateType, int objectType)
        {
            var userRoleObject = MDController.Instance.ContextMotorDepot.acs_roles_objectsSet.Where(r => r.Id_type == objectType
                && r.ObjectDateType == objectDateType
                && r.md_acs_roles.Id == _mdUserRole.Id).FirstOrDefault();
            return userRoleObject;
        }


    }
}
