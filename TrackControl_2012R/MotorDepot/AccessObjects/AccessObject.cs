﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MotorDepot.AccessObjects
{
    public abstract class AccessObject: IMdUserAccessObject
    {
        protected readonly AccessObjectViewer _dataViewer;
        protected acs_roles _mdAccessRole;
        protected List<AccessDataRecord> _objectRecords ;
        protected MotorDepotEntities _contextMotorDepot;

        public AccessObject()
        {
            _dataViewer = new AccessObjectViewer();
            _dataViewer.Dock = DockStyle.Fill;
            _objectRecords = new List<AccessDataRecord>();
            _contextMotorDepot = MDController.Instance.ContextMotorDepot;
        }

        #region IMdUserAccessObject Members

        public AccessObjectViewer DataViewer
        {
            get { return _dataViewer; }
        }

        public virtual int ObjectType
        {
            get { throw new NotImplementedException(); }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public acs_roles AccessRole
        {
            get
            {
                return _mdAccessRole; ;
            }
            set
            {
                if (_mdAccessRole != value)
                {
                    _mdAccessRole = value;
                    SetViewerDataSource();
                }
            }
        }

        public virtual void SaveRoleObjects()
        {
            if (_mdAccessRole == null) return;
            _contextMotorDepot.acs_roles_objectsSet.Where(
           ob => ob.Id_type == ObjectType
           && ob.md_acs_roles.Id == _mdAccessRole.Id).ToList().ForEach(robj => _contextMotorDepot.DeleteObject(robj));
            if (_objectRecords.Count >0)
            {
                foreach (AccessDataRecord objectRecord in _objectRecords)
                {
                    acs_roles_objects acsRolesObjectForSave = new acs_roles_objects();
                    acsRolesObjectForSave.ObjectDateType = objectRecord.TypeEf;
                    acsRolesObjectForSave.IsEdit = objectRecord.IsEdit;
                    acsRolesObjectForSave.IsVisible = objectRecord.IsVisible;
                    acsRolesObjectForSave.md_acs_roles = _mdAccessRole;
                    acsRolesObjectForSave.Id_type = ObjectType;
                    _contextMotorDepot.AddToacs_roles_objectsSet(acsRolesObjectForSave);
                }
            }
            _contextMotorDepot.SaveChanges();
        }

        public void SetViewerDataSource()
        {
            GetAccessObjectRecords();
            _dataViewer.DataSource = _objectRecords;
        }

        public  void GetAccessObjectRecords()
        {
            GetInitDataSource();
            if (_mdAccessRole != null && _objectRecords.Count > 0)
            {

                var objects =
             MDController.Instance.ContextMotorDepot.acs_roles_objectsSet.Where(
            ob => ob.Id_type == ObjectType
            && ob.md_acs_roles.Id == _mdAccessRole.Id).ToList();

                if (objects != null && objects.Count>0)
                {
                    foreach (acs_roles_objects acsRolesObject in objects)
                    {
                        foreach (AccessDataRecord objectRecord in _objectRecords)
                        {
                            if (objectRecord.TypeEf == acsRolesObject.ObjectDateType)
                            {
                                objectRecord.IsVisible = acsRolesObject.IsVisible;
                                objectRecord.IsEdit = acsRolesObject.IsEdit;
                            }
                        }
                    }
                }
                else
                {
                    // начальное положение - все видимое и редактируемое
                    foreach (AccessDataRecord objectRecord in _objectRecords)
                    {
                            objectRecord.IsVisible = true;
                            objectRecord.IsEdit = true;
                    }
                }
            }
        }

        #endregion

        public virtual void GetInitDataSource()
        {
            _objectRecords.Clear();
            var acsObjects = _contextMotorDepot.acs_objectsSet.Where(ao => ao.IdType == ObjectType).ToList();
            foreach (acs_objects acsObjectse in acsObjects)
            {
                AccessDataRecord ri = new AccessDataRecord(acsObjectse.TypeEF, acsObjectse.Name);
                _objectRecords.Add(ri);
            }
        }

    }
}
