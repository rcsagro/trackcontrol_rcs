using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MotorDepot.AccessObjects
{
    public partial class MdUsersList : DevExpress.XtraEditors.XtraUserControl
    {
        List<IMdUserAccessObject> _objects;
        acs_roles _activeRole;
        IMdUserAccessObject _activeObject;

        public MdUsersList()
        {
            InitializeComponent();
            
        }

        #region GridUsersList

        internal void SetUsersWithRolesListDataSource()
        {
            bsUsers.DataSource =  UsersController.GetUsersWithRolesList();
        }

        private void gvUsers_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            acs_users user = gvUsers.GetRow(gvUsers.FocusedRowHandle) as acs_users;
            if (user != null && user.users_list !=null  && user.md_acs_roles !=null && user.md_enterprise !=null) UsersController.SaveUser(user);
        }



        private void gcUsers_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowUsers();
            }
        }

        bool DeleteRowUsers()
        {
            acs_users user = gvUsers.GetRow(gvUsers.FocusedRowHandle) as acs_users;
            if (user != null)
            {
                if (XtraMessageBox.Show("������������� �������� ������������?",
               MDController.MesCaption,
               MessageBoxButtons.YesNoCancel,
               MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return UsersController.DeleteObject<acs_users>(user);
                }
                else
                    return false;
            }
            else
                return false;
        }

        private void gvUsers_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            acs_users user = gvUsers.GetRow(gvUsers.FocusedRowHandle) as acs_users;
            if (user.users_list==null)
            {
                e.Valid = false;
                gvUsers.SetColumnError(colName, "������� ������������");
                return;
            }
            if (user.md_acs_roles == null)
            {
                e.Valid = false;
                gvUsers.SetColumnError(colUserRole, "������� ���� ������������");
                return;
            }
            if (user.md_enterprise == null)
            {
                e.Valid = false;
                gvUsers.SetColumnError(colEnterprise, "������� ����������� ������������");
                return;
            }
            e.Valid = true;
        }

        private void gvUsers_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            acs_users user = gvUsers.GetRow(gvUsers.FocusedRowHandle) as acs_users;
            if (user == null) return;
            if (user.md_acs_roles  != null)
            {
                leUserRoles.EditValue = user.md_acs_roles.Id;
                SetCurrentUserRole();
            }
        }

        #endregion

        #region Roles

        private void SetCurrentUserRole()
        {
            _activeRole = (acs_roles)leUserRoles.Properties.GetDataSourceRowByKeyValue(leUserRoles.EditValue);
        }


        void SetRoleForTypesObjects()
        {
            if (_objects == null) return;
            SetCurrentUserRole();
            if (_activeObject != null) _activeObject.AccessRole = _activeRole; 
         }

        private void leUserRoles_EditValueChanged(object sender, EventArgs e)
        {
            ViewObjectTypeControl();
        }
        #endregion

        #region TypesObjects

        private void InitObjectTypes()
        {
            _objects = new List<IMdUserAccessObject>();
            AccessReports arep = new AccessReports();
            _objects.Add(arep);
            AccessDictionaries adic = new AccessDictionaries();
            _objects.Add(adic);
            AccessDocuments adoc = new AccessDocuments();
            _objects.Add(adoc);
        }

        private void gvObjectTypes_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ViewObjectTypeControl();
        }

        void ViewObjectTypeControl()
        {
            pnViews.Controls.Clear();
            SetCurrentObjectType();
            SetCurrentUserRole();
            if (_activeObject == null) return;
            if (_activeRole == null) return;
            _activeObject.AccessRole = _activeRole;
            _activeObject.ParentDataViewer = pnViews;
            
        }

        private void SetCurrentObjectType()
        {
            foreach (IMdUserAccessObject uao in _objects)
            {
                if (uao.ObjectType == (int)rgObjectTypes.EditValue)
                {
                    _activeObject = uao;
                    break;
                }
            }
        }

        private void rgObjectTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewObjectTypeControl();
        }
        private void sbSaveObjectValues_Click(object sender, EventArgs e)
        {
            if (_activeObject!=null) _activeObject.SaveRoleObjects();
        }
        #endregion

        private void UsersList_Load(object sender, EventArgs e)
        {
            InitObjectTypes();
            OnRefreshRolesList();
            SetFirstItemInRolesList();
            SetUsersWithRolesListDataSource();
            SetLookUpsDataSource();

        }

        private void SetFirstItemInRolesList()
        {
            int position = leUserRoles.Properties.GetDataSourceRowIndex(leUserRoles.Properties.ValueMember, leUserRoles.EditValue);
            if (position == -1)
            {
                leUserRoles.EditValue = leUserRoles.Properties.GetDataSourceValue(leUserRoles.Properties.ValueMember, 0);
            }
        }

        private void sbEdit_Click(object sender, EventArgs e)
        {
            MdUserRolesEditor formEditor = new MdUserRolesEditor(new Point(pnViews.Left + splitContainerControl.Panel2.Left, pnViews.Top + splitContainerControl.Panel2.Top)); 
            formEditor.RefreshRolesList += OnRefreshRolesList;
            formEditor.ShowDialog();  
        }

        void OnRefreshRolesList()
        {
            //rleRoles.DataSource =  MDController.Instance.ContextMotorDepot.acs_rolesSet.OrderBy(r=>r.Name).ToList();
            rleRoles.DataSource = UsersController.GetRolesList();
            leUserRoles.Properties.DataSource = rleRoles.DataSource;
        }

        void SetLookUpsDataSource()
        {
            rleUsers.DataSource = UsersController.GetUsersList();
            rleEnterprise.DataSource = UsersController.GetEnterpriseList();
        }
    }
}
