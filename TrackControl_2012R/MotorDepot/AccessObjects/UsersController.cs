﻿using System.Collections.Generic;
using System.Linq;


namespace MotorDepot.AccessObjects
{
    public static class UsersController
    {
        internal static MotorDepotEntities ContextMotorDepot;

        internal static List<acs_users> GetUsersWithRolesList()
        {
            return ContextMotorDepot.acs_usersSet.OrderBy(u=>u.users_list.Name).ToList();
        }

        internal static List<acs_roles> GetRolesList()
        {
            return ContextMotorDepot.acs_rolesSet.OrderBy(r=>r.Name).ToList();
        }

        internal static List<users_list> GetUsersList()
        {
            return ContextMotorDepot.users_listSet.OrderBy(u=>u.Name).ToList(); 
        }

        internal static List<enterprise> GetEnterpriseList()
        {
            return ContextMotorDepot.enterpriseSet.OrderBy(e => e.Name).ToList();
        }

        internal static void SaveUser(acs_users user)
        {
            if (user == null) return;
            if (user.Id == 0)
            {
                ContextMotorDepot.AddToacs_usersSet(user);
            }
            MDController.Instance.MotorDepotEntitiesSaveChanges(user);
        }

        internal static void SaveRole(acs_roles uRole)
        {
            if (uRole == null) return;
            if (uRole.Id == 0)
            {
                ContextMotorDepot.AddToacs_rolesSet(uRole);
            }
            MDController.Instance.MotorDepotEntitiesSaveChanges(uRole);
        }

        internal static bool DeleteUser(acs_users user)
        {
            try
            {
                ContextMotorDepot.DeleteObject(user);
                MDController.Instance.MotorDepotEntitiesSaveChanges(user);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static bool DeleteObject<T>(T ur)
        {
            try
            {
                ContextMotorDepot.DeleteObject(ur);
                MDController.Instance.MotorDepotEntitiesSaveChanges(ur);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static int GetUsersInRoleCount(acs_roles ur)
        {
            return ContextMotorDepot.acs_usersSet.Where(u => u.md_acs_roles.Id == ur.Id).Count();
        }

    }
} 
