using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;


namespace MotorDepot.AccessObjects
{
    public partial class MdUserRolesEditor : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshRolesList;
        bool _rolesChanged;

        public MdUserRolesEditor(Point point)
        {
            InitializeComponent();
            bsRoles.DataSource =UsersController.GetRolesList()  ;
            this.Location = point;
        }

        private void gvRoles_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            acs_roles ur = gvRoles.GetRow(e.RowHandle) as acs_roles;
            if (ur != null)
            {
                UsersController.SaveRole(ur); 
                _rolesChanged = true;
            }
        }

        private void gcRoles_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowRoles();
                _rolesChanged = !e.Handled ;
            }
        }

        bool DeleteRowRoles()
        {
            acs_roles ur = gvRoles.GetRow(gvRoles.FocusedRowHandle) as acs_roles;
            if (ur != null)
            {
                var userwithRoles = UsersController.GetUsersWithRolesList();
                if (userwithRoles != null)
                {
                    int users = UsersController.GetUsersInRoleCount(ur);
                if (users > 0)
                    {
                        XtraMessageBox.Show("�������� ���� ���������!", string.Format("������������� � �����: {0}", users));
                        return false;
                    }
                }
                if (XtraMessageBox.Show("������������� �������� ����?",
                   ur.Name,
                   MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    return UsersController.DeleteObject<acs_roles>(ur);
                }
                else
                    return false;
            }
            else
                return false;
        }

        private void UserRolesEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_rolesChanged && RefreshRolesList != null)
                RefreshRolesList();
        }



    }
}