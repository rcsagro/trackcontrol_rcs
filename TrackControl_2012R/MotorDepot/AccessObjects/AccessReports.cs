﻿using MotorDepot.Reports;
using TrackControl.General;

namespace MotorDepot.AccessObjects
{
    public class AccessReports: AccessObject
    {

        public AccessReports():base()
        {
            _dataViewer.colEdit.Visible = false;
        }
        
        public override int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.ReportsList; }
        }


        public override void GetInitDataSource()
        {
            _objectRecords.Clear();
            DeviationKilometrage reportDk = new DeviationKilometrage();
            AccessDataRecord ri = new AccessDataRecord(reportDk.GetType().ToString(), reportDk.Caption);
            _objectRecords.Add(ri);
            DeviationTime reportDt = new DeviationTime();
            ri = new AccessDataRecord(reportDt.GetType().ToString(), reportDt.Caption);
            _objectRecords.Add(ri);
            LogEvents reportLe = new LogEvents();
            ri = new AccessDataRecord(reportLe.GetType().ToString(), reportLe.Caption);
            _objectRecords.Add(ri);
            Dispatcher reportDis = new Dispatcher();
            ri = new AccessDataRecord(reportDis.GetType().ToString(), reportDis.Caption);
            _objectRecords.Add(ri);
            Customer reportCust = new Customer();
            ri = new AccessDataRecord(reportCust.GetType().ToString(), reportCust.Caption);
            _objectRecords.Add(ri);
        }
    }
}
