﻿using System.Windows.Forms;

namespace MotorDepot.AccessObjects
{
    public interface IMdUserAccessObject
    {
        /// <summary>
        /// Элемент для просмотра и редактирования данных
        /// </summary>
        AccessObjectViewer DataViewer { get; }

        /// <summary>
        /// тип объекта доступа
        /// </summary>
        int ObjectType { get; }

        /// <summary>
        /// родительский объект для DataViewer
        /// </summary>
        Control ParentDataViewer { set; }

        /// <summary>
        /// роль пользователя для доступа к данным в проекте Мотопредприятия
        /// </summary>
        acs_roles AccessRole { get; set; }

        /// <summary>
        /// Сохранение в базе  объектов роли
        /// </summary>
        void SaveRoleObjects();

        /// <summary>
        /// Формирование источника данных с определнием видимых и редактируемых объектов
        /// </summary>
        void SetViewerDataSource();

    }
}
