namespace MotorDepot.AccessObjects
{
    partial class MdUserRolesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gcRoles = new DevExpress.XtraGrid.GridControl();
            this.gvRoles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bsRoles = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRoles)).BeginInit();
            this.SuspendLayout();
            // 
            // gcRoles
            // 
            this.gcRoles.DataSource = this.bsRoles;
            this.gcRoles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRoles.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcRoles_EmbeddedNavigator_ButtonClick);
            this.gcRoles.Location = new System.Drawing.Point(0, 0);
            this.gcRoles.MainView = this.gvRoles;
            this.gcRoles.Name = "gcRoles";
            this.gcRoles.Size = new System.Drawing.Size(381, 439);
            this.gcRoles.TabIndex = 0;
            this.gcRoles.UseEmbeddedNavigator = true;
            this.gcRoles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRoles});
            // 
            // gvRoles
            // 
            this.gvRoles.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRoles.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRoles.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRoles.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoles.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoles.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRoles.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRoles.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRoles.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvRoles.Appearance.Empty.Options.UseBackColor = true;
            this.gvRoles.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoles.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoles.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoles.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoles.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRoles.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRoles.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvRoles.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoles.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRoles.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRoles.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvRoles.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvRoles.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvRoles.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvRoles.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvRoles.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvRoles.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvRoles.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRoles.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRoles.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvRoles.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoles.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoles.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRoles.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRoles.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRoles.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRoles.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvRoles.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvRoles.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoles.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRoles.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRoles.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRoles.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRoles.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRoles.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoles.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoles.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRoles.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoles.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoles.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.OddRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvRoles.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvRoles.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvRoles.Appearance.Preview.Options.UseBackColor = true;
            this.gvRoles.Appearance.Preview.Options.UseFont = true;
            this.gvRoles.Appearance.Preview.Options.UseForeColor = true;
            this.gvRoles.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoles.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.Row.Options.UseBackColor = true;
            this.gvRoles.Appearance.Row.Options.UseForeColor = true;
            this.gvRoles.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoles.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvRoles.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRoles.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvRoles.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoles.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoles.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvRoles.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvRoles.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvRoles.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvRoles.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoles.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRoles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName});
            this.gvRoles.GridControl = this.gcRoles;
            this.gvRoles.Name = "gvRoles";
            this.gvRoles.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRoles.OptionsView.EnableAppearanceOddRow = true;
            this.gvRoles.OptionsView.ShowGroupPanel = false;
            this.gvRoles.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvRoles_CellValueChanged);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "���� ������������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // MdUserRolesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 439);
            this.Controls.Add(this.gcRoles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MdUserRolesEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "�������� ����� �������������";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UserRolesEditor_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRoles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcRoles;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRoles;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private System.Windows.Forms.BindingSource bsRoles;

    }
}