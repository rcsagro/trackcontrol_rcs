namespace MotorDepot.AccessObjects
{
    partial class AccessObjectViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcObjects = new DevExpress.XtraGrid.GridControl();
            this.gvObjects = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rchEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcObjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // gcObjects
            // 
            this.gcObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcObjects.Location = new System.Drawing.Point(0, 0);
            this.gcObjects.MainView = this.gvObjects;
            this.gcObjects.Name = "gcObjects";
            this.gcObjects.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rchEdit});
            this.gcObjects.Size = new System.Drawing.Size(492, 427);
            this.gcObjects.TabIndex = 0;
            this.gcObjects.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvObjects});
            // 
            // gvObjects
            // 
            this.gvObjects.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvObjects.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvObjects.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvObjects.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjects.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjects.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvObjects.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvObjects.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvObjects.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvObjects.Appearance.Empty.Options.UseBackColor = true;
            this.gvObjects.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjects.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvObjects.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjects.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjects.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvObjects.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvObjects.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvObjects.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvObjects.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvObjects.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvObjects.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvObjects.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvObjects.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvObjects.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvObjects.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvObjects.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvObjects.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvObjects.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvObjects.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvObjects.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvObjects.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjects.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvObjects.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvObjects.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvObjects.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvObjects.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvObjects.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvObjects.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvObjects.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvObjects.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvObjects.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvObjects.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvObjects.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvObjects.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvObjects.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjects.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjects.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvObjects.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjects.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjects.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.OddRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.OddRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvObjects.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvObjects.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvObjects.Appearance.Preview.Options.UseBackColor = true;
            this.gvObjects.Appearance.Preview.Options.UseFont = true;
            this.gvObjects.Appearance.Preview.Options.UseForeColor = true;
            this.gvObjects.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvObjects.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.Row.Options.UseBackColor = true;
            this.gvObjects.Appearance.Row.Options.UseForeColor = true;
            this.gvObjects.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvObjects.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvObjects.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvObjects.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvObjects.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvObjects.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvObjects.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvObjects.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvObjects.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvObjects.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvObjects.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvObjects.Appearance.VertLine.Options.UseBackColor = true;
            this.gvObjects.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colType,
            this.colName,
            this.colVisible,
            this.colEdit});
            this.gvObjects.GridControl = this.gcObjects;
            this.gvObjects.IndicatorWidth = 30;
            this.gvObjects.Name = "gvObjects";
            this.gvObjects.OptionsView.EnableAppearanceEvenRow = true;
            this.gvObjects.OptionsView.EnableAppearanceOddRow = true;
            this.gvObjects.OptionsView.ShowGroupPanel = false;
            // 
            // colType
            // 
            this.colType.AppearanceHeader.Options.UseTextOptions = true;
            this.colType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colType.Caption = "��� �������";
            this.colType.FieldName = "TypeEF";
            this.colType.Name = "colType";
            this.colType.OptionsColumn.AllowEdit = false;
            this.colType.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "��������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 184;
            // 
            // colVisible
            // 
            this.colVisible.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisible.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisible.Caption = "��������";
            this.colVisible.ColumnEdit = this.rchEdit;
            this.colVisible.FieldName = "IsVisible";
            this.colVisible.Name = "colVisible";
            this.colVisible.Visible = true;
            this.colVisible.VisibleIndex = 1;
            this.colVisible.Width = 129;
            // 
            // rchEdit
            // 
            this.rchEdit.AutoHeight = false;
            this.rchEdit.Name = "rchEdit";
            // 
            // colEdit
            // 
            this.colEdit.AppearanceHeader.Options.UseTextOptions = true;
            this.colEdit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEdit.Caption = "��������������";
            this.colEdit.ColumnEdit = this.rchEdit;
            this.colEdit.FieldName = "IsEdit";
            this.colEdit.Name = "colEdit";
            this.colEdit.Visible = true;
            this.colEdit.VisibleIndex = 2;
            this.colEdit.Width = 145;
            // 
            // AccessObjectViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcObjects);
            this.Name = "AccessObjectViewer";
            this.Size = new System.Drawing.Size(492, 427);
            ((System.ComponentModel.ISupportInitialize)(this.gcObjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcObjects;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisible;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchEdit;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvObjects;
        internal DevExpress.XtraGrid.Columns.GridColumn colEdit;
    }
}
