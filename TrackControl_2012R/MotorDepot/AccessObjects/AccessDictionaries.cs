﻿using TrackControl.General;

namespace MotorDepot.AccessObjects
{
    public class AccessDictionaries : AccessObject
    {
        public override int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.DictionariesMd; }
        }

    }
}
