using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MotorDepot.AccessObjects
{
    public partial class AccessObjectViewer : DevExpress.XtraEditors.XtraUserControl
    {
        public AccessObjectViewer()
        {
            InitializeComponent();
        }

        public Object DataSource
        { 
            set
            {
                gcObjects.DataSource = value;
                gcObjects.RefreshDataSource();
            }
        }

    }
}
