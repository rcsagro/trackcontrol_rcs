﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MotorDepot.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MotorDepot.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ArrowDownRedGloss {
            get {
                object obj = ResourceManager.GetObject("ArrowDownRedGloss", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of order.
        /// </summary>
        internal static string CaptionIdCount {
            get {
                return ResourceManager.GetString("CaptionIdCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The registration number of auto.
        /// </summary>
        internal static string CaptionStateNumberAuto {
            get {
                return ResourceManager.GetString("CaptionStateNumberAuto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Cross {
            get {
                object obj = ResourceManager.GetObject("Cross", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date.
        /// </summary>
        internal static string Date {
            get {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log deviation in time.
        /// </summary>
        internal static string DevFormReportDeviationTime {
            get {
                return ResourceManager.GetString("DevFormReportDeviationTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Event log.
        /// </summary>
        internal static string DevFormReportEvent {
            get {
                return ResourceManager.GetString("DevFormReportEvent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Difference motion.
        /// </summary>
        internal static string DifferenceMove {
            get {
                return ResourceManager.GetString("DifferenceMove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Difference idle.
        /// </summary>
        internal static string DiffernceStop {
            get {
                return ResourceManager.GetString("DiffernceStop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Distance to object, km.
        /// </summary>
        internal static string DistanceToObject {
            get {
                return ResourceManager.GetString("DistanceToObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time on objects.
        /// </summary>
        internal static string DurationOnObject {
            get {
                return ResourceManager.GetString("DurationOnObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duration of movement to the object.
        /// </summary>
        internal static string LongTimeToObject {
            get {
                return ResourceManager.GetString("LongTimeToObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Object.
        /// </summary>
        internal static string NameObject {
            get {
                return ResourceManager.GetString("NameObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notification.
        /// </summary>
        internal static string Notification {
            get {
                return ResourceManager.GetString("Notification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Period from.
        /// </summary>
        internal static string PeriodFrom {
            get {
                return ResourceManager.GetString("PeriodFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to to.
        /// </summary>
        internal static string PeriodTo {
            get {
                return ResourceManager.GetString("PeriodTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to START.
        /// </summary>
        internal static string Start {
            get {
                return ResourceManager.GetString("Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The registration number of auto.
        /// </summary>
        internal static string StateNumberAuto {
            get {
                return ResourceManager.GetString("StateNumberAuto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to STOP.
        /// </summary>
        internal static string Stop {
            get {
                return ResourceManager.GetString("Stop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time.
        /// </summary>
        internal static string Time {
            get {
                return ResourceManager.GetString("Time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time the fact. Movement.
        /// </summary>
        internal static string TimeFactMove {
            get {
                return ResourceManager.GetString("TimeFactMove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time the fact.Stopping.
        /// </summary>
        internal static string TimeFactStoping {
            get {
                return ResourceManager.GetString("TimeFactStoping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The time leaving the object.
        /// </summary>
        internal static string TimeFromObject {
            get {
                return ResourceManager.GetString("TimeFromObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time of entry to object.
        /// </summary>
        internal static string TimeGoObject {
            get {
                return ResourceManager.GetString("TimeGoObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time to plan. Delay.
        /// </summary>
        internal static string TimePlanDelay {
            get {
                return ResourceManager.GetString("TimePlanDelay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time to plan. Loading.
        /// </summary>
        internal static string TimePlanLoading {
            get {
                return ResourceManager.GetString("TimePlanLoading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time to plan. Movement.
        /// </summary>
        internal static string TimePlanMove {
            get {
                return ResourceManager.GetString("TimePlanMove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time to plan. Reprieve.
        /// </summary>
        internal static string TimePlanRepriev {
            get {
                return ResourceManager.GetString("TimePlanRepriev", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time to plan. Unloading.
        /// </summary>
        internal static string TimePlanUnloading {
            get {
                return ResourceManager.GetString("TimePlanUnloading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name of the object.
        /// </summary>
        internal static string TipNameObject {
            get {
                return ResourceManager.GetString("TipNameObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of order.
        /// </summary>
        internal static string ToolTipIdCount {
            get {
                return ResourceManager.GetString("ToolTipIdCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alarm Events.
        /// </summary>
        internal static string UrgentEvent {
            get {
                return ResourceManager.GetString("UrgentEvent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The event log does not contain the data for the selected reporting period.
        /// </summary>
        internal static string WarningNoData {
            get {
                return ResourceManager.GetString("WarningNoData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to y..
        /// </summary>
        internal static string YearLetter {
            get {
                return ResourceManager.GetString("YearLetter", resourceCulture);
            }
        }
    }
}
