namespace MotorDepot
{
    partial class MainMD 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMD));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopy = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShowLog = new DevExpress.XtraBars.BarButtonItem();
            this.beiNumber = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bdeStart = new DevExpress.XtraBars.BarEditItem();
            this.deStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEnd = new DevExpress.XtraBars.BarEditItem();
            this.deEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.beSwShowDeleted = new DevExpress.XtraBars.BarEditItem();
            this.rgDeleted = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.bbiPrintForms = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.beiTail = new DevExpress.XtraBars.BarEditItem();
            this.rseTail = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bchAutoFit = new DevExpress.XtraBars.BarCheckItem();
            this.bbiClearVehSelection = new DevExpress.XtraBars.BarButtonItem();
            this.dockControlOnline = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btStart = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtbMain = new DevExpress.XtraTab.XtraTabControl();
            this.xtpMap = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitMap = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcLog = new DevExpress.XtraGrid.GridControl();
            this.gvLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLogId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsRead = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogOrderState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ricOrderState = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imOrderStates = new DevExpress.Utils.ImageCollection(this.components);
            this.colLogOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogParMes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogParVal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleLogWayBill = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleLogOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleLogObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtpDictionary = new DevExpress.XtraTab.XtraTabPage();
            this.xtpWB = new DevExpress.XtraTab.XtraTabPage();
            this.gcWayBill = new DevExpress.XtraGrid.GridControl();
            this.bsWayBill = new System.Windows.Forms.BindingSource(this.components);
            this.gvWayBill = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWbId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWbDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWbVehicleState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ricVehicleState = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imVehStates = new DevExpress.Utils.ImageCollection(this.components);
            this.colWbVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rgleVehicle = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.rgleVehicleView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWbDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rgleDriver = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.rgleDriverView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWbShift = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleShift = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colWbEnterprise = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleEnterprise = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colWbClose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWbBTrip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleVehicle = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleDriver = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleVehicleState = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rgleVehicleState = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.rieVehicleState = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.xtpOrder = new DevExpress.XtraTab.XtraTabPage();
            this.gcOrder = new DevExpress.XtraGrid.GridControl();
            this.bsOrder = new System.Windows.Forms.BindingSource(this.components);
            this.gvOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrdId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ricOrderStates = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colOrdNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleOrderObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOrdCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleOrderCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOrdTranspType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleOrderTranspType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOrdPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleOrderPriority = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOrdWayBill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleWayBill = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOrdUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrdRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtpReports = new DevExpress.XtraTab.XtraTabPage();
            this.xtpUserRoles = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rseTail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbMain)).BeginInit();
            this.xtbMain.SuspendLayout();
            this.xtpMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMap)).BeginInit();
            this.splitMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricOrderState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogObject)).BeginInit();
            this.xtpWB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricVehicleState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imVehStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicleView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriverView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicleState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicleState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rieVehicleState)).BeginInit();
            this.xtpOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricOrderStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderTranspType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.dockControlOnline);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAdd,
            this.bbiDelete,
            this.bbiEdit,
            this.bbiRefresh,
            this.btStart,
            this.beiNumber,
            this.bdeStart,
            this.bdeEnd,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.bbiPrintForms,
            this.barStaticItem5,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.bbiStart,
            this.beiTail,
            this.bchAutoFit,
            this.bbiCopy,
            this.beSwShowDeleted,
            this.bbiShowLog,
            this.bbiClearVehSelection});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 30;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.deStart,
            this.deEnd,
            this.rseTail,
            this.rgDeleted});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCopy, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiShowLog, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.beiNumber, "", true, true, true, 79, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeStart, "", false, true, true, 87),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEnd, "", false, true, true, 84),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beSwShowDeleted, "", false, true, true, 173),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiPrintForms, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiAdd
            // 
            this.bbiAdd.Caption = "��������";
            this.bbiAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAdd.Glyph")));
            this.bbiAdd.Id = 0;
            this.bbiAdd.Name = "bbiAdd";
            this.bbiAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAdd_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "�������";
            this.bbiDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.Glyph")));
            this.bbiDelete.Id = 1;
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "�������������";
            this.bbiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEdit.Glyph")));
            this.bbiEdit.Id = 2;
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEdit_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "��������";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 3;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiCopy
            // 
            this.bbiCopy.Caption = "����������";
            this.bbiCopy.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCopy.Glyph")));
            this.bbiCopy.Id = 25;
            this.bbiCopy.Name = "bbiCopy";
            this.bbiCopy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopy_ItemClick);
            // 
            // bbiShowLog
            // 
            this.bbiShowLog.Caption = "������ ���������";
            this.bbiShowLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShowLog.Glyph")));
            this.bbiShowLog.Id = 28;
            this.bbiShowLog.Name = "bbiShowLog";
            this.bbiShowLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowLog_ItemClick);
            // 
            // beiNumber
            // 
            this.beiNumber.Caption = "�����";
            this.beiNumber.Edit = this.repositoryItemTextEdit1;
            this.beiNumber.Id = 6;
            this.beiNumber.Name = "beiNumber";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 11;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // bdeStart
            // 
            this.bdeStart.Caption = "���� �";
            this.bdeStart.Edit = this.deStart;
            this.bdeStart.Id = 7;
            this.bdeStart.Name = "bdeStart";
            this.bdeStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deStart
            // 
            this.deStart.AutoHeight = false;
            this.deStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStart.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.Name = "deStart";
            // 
            // bdeEnd
            // 
            this.bdeEnd.Caption = "��";
            this.bdeEnd.Edit = this.deEnd;
            this.bdeEnd.Id = 8;
            this.bdeEnd.Name = "bdeEnd";
            this.bdeEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // deEnd
            // 
            this.deEnd.AutoHeight = false;
            this.deEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEnd.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.Name = "deEnd";
            // 
            // beSwShowDeleted
            // 
            this.beSwShowDeleted.Edit = this.rgDeleted;
            this.beSwShowDeleted.EditValue = true;
            this.beSwShowDeleted.Id = 26;
            this.beSwShowDeleted.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Transparent;
            this.beSwShowDeleted.ItemAppearance.Normal.Options.UseForeColor = true;
            this.beSwShowDeleted.Name = "beSwShowDeleted";
            // 
            // rgDeleted
            // 
            this.rgDeleted.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgDeleted.Appearance.Options.UseBackColor = true;
            this.rgDeleted.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.rgDeleted.AppearanceDisabled.Options.UseBackColor = true;
            this.rgDeleted.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.rgDeleted.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.rgDeleted.AppearanceFocused.Options.UseBackColor = true;
            this.rgDeleted.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.rgDeleted.AppearanceReadOnly.Options.UseBackColor = true;
            this.rgDeleted.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "�������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "���������")});
            this.rgDeleted.Name = "rgDeleted";
            // 
            // bbiPrintForms
            // 
            this.bbiPrintForms.ActAsDropDown = true;
            this.bbiPrintForms.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiPrintForms.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiPrintForms.Caption = "�������� ������";
            this.bbiPrintForms.DropDownControl = this.popupMenu1;
            this.bbiPrintForms.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrintForms.Glyph")));
            this.bbiPrintForms.Hint = "������ ������� ������ � ������� ���������";
            this.bbiPrintForms.Id = 15;
            this.bbiPrintForms.Name = "bbiPrintForms";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "������� ����� �� ��";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 17;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "������� ����� ������";
            this.barButtonItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.Glyph")));
            this.barButtonItem5.Id = 18;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "������ ��������� �� ��";
            this.barButtonItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.Glyph")));
            this.barButtonItem6.Id = 19;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "������ ��������� ������";
            this.barButtonItem7.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.Glyph")));
            this.barButtonItem7.Id = 20;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bar1
            // 
            this.bar1.BarName = "Online";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiTail, "", true, true, true, 59),
            new DevExpress.XtraBars.LinkPersistInfo(this.bchAutoFit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearVehSelection)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.StandaloneBarDockControl = this.dockControlOnline;
            this.bar1.Text = "Online";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "����";
            this.bbiStart.Id = 22;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // beiTail
            // 
            this.beiTail.Caption = "\"�����\"";
            this.beiTail.Edit = this.rseTail;
            this.beiTail.EditValue = 50;
            this.beiTail.Hint = "���������� ����� ��� �����������\n���������� ������� ����";
            this.beiTail.Id = 23;
            this.beiTail.Name = "beiTail";
            this.beiTail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // rseTail
            // 
            this.rseTail.AutoHeight = false;
            this.rseTail.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rseTail.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rseTail.IsFloatValue = false;
            this.rseTail.Mask.EditMask = "N00";
            this.rseTail.MaxLength = 3;
            this.rseTail.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.rseTail.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rseTail.Name = "rseTail";
            // 
            // bchAutoFit
            // 
            this.bchAutoFit.Checked = true;
            this.bchAutoFit.Glyph = ((System.Drawing.Image)(resources.GetObject("bchAutoFit.Glyph")));
            this.bchAutoFit.Hint = "�������� / ��������� �������������������";
            this.bchAutoFit.Id = 24;
            this.bchAutoFit.Name = "bchAutoFit";
            // 
            // bbiClearVehSelection
            // 
            this.bbiClearVehSelection.Caption = "�������� ���������";
            this.bbiClearVehSelection.Hint = "������� ��������� ������������� �������� ������� ��������";
            this.bbiClearVehSelection.Glyph = global::MotorDepot.Properties.Resources.Cross;
            this.bbiClearVehSelection.Id = 29;
            this.bbiClearVehSelection.Name = "bbiClearVehSelection";
            this.bbiClearVehSelection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearVehSelection_ItemClick);
            // 
            // dockControlOnline
            // 
            this.dockControlOnline.CausesValidation = false;
            this.dockControlOnline.Dock = System.Windows.Forms.DockStyle.Top;
            this.dockControlOnline.Location = new System.Drawing.Point(0, 0);
            this.dockControlOnline.Name = "dockControlOnline";
            this.dockControlOnline.Size = new System.Drawing.Size(358, 31);
            this.dockControlOnline.Text = "standaloneBarDockControl1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(939, 59);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 654);
            this.barDockControlBottom.Size = new System.Drawing.Size(939, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 59);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 595);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(939, 59);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 595);
            // 
            // btStart
            // 
            this.btStart.Id = 21;
            this.btStart.Name = "btStart";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "������� �����";
            this.barButtonItem1.Id = 9;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "������ ���������";
            this.barButtonItem2.Id = 10;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "������� ����� �� ��";
            this.barStaticItem1.Id = 11;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "������� ����� ������";
            this.barStaticItem2.Id = 12;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "������ ��������� �� ��";
            this.barStaticItem3.Id = 13;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "������ ��������� ������";
            this.barStaticItem4.Id = 14;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "barStaticItem5";
            this.barStaticItem5.Id = 16;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // gridView2
            // 
            this.gridView2.Name = "gridView2";
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // xtbMain
            // 
            this.xtbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtbMain.Location = new System.Drawing.Point(0, 59);
            this.xtbMain.Name = "xtbMain";
            this.xtbMain.SelectedTabPage = this.xtpMap;
            this.xtbMain.Size = new System.Drawing.Size(939, 595);
            this.xtbMain.TabIndex = 0;
            this.xtbMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpMap,
            this.xtpDictionary,
            this.xtpWB,
            this.xtpOrder,
            this.xtpReports,
            this.xtpUserRoles});
            this.xtbMain.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtbMain_SelectedPageChanged);
            // 
            // xtpMap
            // 
            this.xtpMap.Controls.Add(this.splitContainerControl2);
            this.xtpMap.Name = "xtpMap";
            this.xtpMap.Size = new System.Drawing.Size(933, 567);
            this.xtpMap.Text = "�����";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitMap);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gcLog);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(933, 567);
            this.splitContainerControl2.SplitterPosition = 463;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitMap
            // 
            this.splitMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMap.Location = new System.Drawing.Point(0, 0);
            this.splitMap.Name = "splitMap";
            this.splitMap.Panel1.Controls.Add(this.dockControlOnline);
            this.splitMap.Panel1.Text = "Panel1";
            this.splitMap.Panel2.Text = "Panel2";
            this.splitMap.Size = new System.Drawing.Size(933, 463);
            this.splitMap.SplitterPosition = 358;
            this.splitMap.TabIndex = 0;
            this.splitMap.Text = "splitContainerControl2";
            // 
            // gcLog
            // 
            this.gcLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLog.Location = new System.Drawing.Point(0, 0);
            this.gcLog.MainView = this.gvLog;
            this.gcLog.MenuManager = this.barManager1;
            this.gcLog.Name = "gcLog";
            this.gcLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleLogWayBill,
            this.rleLogOrder,
            this.rleLogObject,
            this.ricOrderState});
            this.gcLog.Size = new System.Drawing.Size(933, 99);
            this.gcLog.TabIndex = 0;
            this.gcLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLog});
            // 
            // gvLog
            // 
            this.gvLog.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvLog.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.Empty.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvLog.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvLog.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvLog.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvLog.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.OddRow.Options.UseForeColor = true;
            this.gvLog.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvLog.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvLog.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvLog.Appearance.Preview.Options.UseBackColor = true;
            this.gvLog.Appearance.Preview.Options.UseFont = true;
            this.gvLog.Appearance.Preview.Options.UseForeColor = true;
            this.gvLog.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.Row.Options.UseBackColor = true;
            this.gvLog.Appearance.Row.Options.UseForeColor = true;
            this.gvLog.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvLog.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.VertLine.Options.UseBackColor = true;
            this.gvLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLogId,
            this.colIsRead,
            this.colLogData,
            this.colLogObject,
            this.colLogOrderState,
            this.colLogOrder,
            this.colLogVehicle,
            this.colLogParMes,
            this.colLogParVal,
            this.colLogUser});
            this.gvLog.GridControl = this.gcLog;
            this.gvLog.Name = "gvLog";
            this.gvLog.OptionsView.EnableAppearanceEvenRow = true;
            this.gvLog.OptionsView.EnableAppearanceOddRow = true;
            this.gvLog.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvLog_RowStyle);
            this.gvLog.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvLog_FocusedRowChanged);
            this.gvLog.Click += new System.EventHandler(this.gvLog_Click);
            this.gvLog.DoubleClick += new System.EventHandler(this.gvLog_DoubleClick);
            // 
            // colLogId
            // 
            this.colLogId.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogId.Caption = "Id";
            this.colLogId.FieldName = "Id";
            this.colLogId.Name = "colLogId";
            this.colLogId.OptionsColumn.AllowEdit = false;
            this.colLogId.OptionsColumn.ReadOnly = true;
            // 
            // colIsRead
            // 
            this.colIsRead.Caption = "���������";
            this.colIsRead.FieldName = "IsRead";
            this.colIsRead.Name = "colIsRead";
            // 
            // colLogData
            // 
            this.colLogData.AppearanceCell.Options.UseTextOptions = true;
            this.colLogData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogData.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogData.Caption = "�����";
            this.colLogData.DisplayFormat.FormatString = "g";
            this.colLogData.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colLogData.FieldName = "DateEvent";
            this.colLogData.Name = "colLogData";
            this.colLogData.OptionsColumn.AllowEdit = false;
            this.colLogData.OptionsColumn.ReadOnly = true;
            this.colLogData.Visible = true;
            this.colLogData.VisibleIndex = 0;
            this.colLogData.Width = 116;
            // 
            // colLogObject
            // 
            this.colLogObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogObject.Caption = "������";
            this.colLogObject.FieldName = "ObjectName";
            this.colLogObject.Name = "colLogObject";
            this.colLogObject.OptionsColumn.AllowEdit = false;
            this.colLogObject.OptionsColumn.ReadOnly = true;
            this.colLogObject.Visible = true;
            this.colLogObject.VisibleIndex = 5;
            this.colLogObject.Width = 130;
            // 
            // colLogOrderState
            // 
            this.colLogOrderState.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogOrderState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogOrderState.Caption = "������";
            this.colLogOrderState.ColumnEdit = this.ricOrderState;
            this.colLogOrderState.FieldName = "OrderStateId";
            this.colLogOrderState.Name = "colLogOrderState";
            this.colLogOrderState.Visible = true;
            this.colLogOrderState.VisibleIndex = 1;
            this.colLogOrderState.Width = 83;
            // 
            // ricOrderState
            // 
            this.ricOrderState.AutoHeight = false;
            this.ricOrderState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricOrderState.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ricOrderState.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����������", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("��������", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�������� ������", 0, -1)});
            this.ricOrderState.Name = "ricOrderState";
            this.ricOrderState.SmallImages = this.imOrderStates;
            // 
            // imOrderStates
            // 
            this.imOrderStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imOrderStates.ImageStream")));
            this.imOrderStates.Images.SetKeyName(0, "document.png");
            this.imOrderStates.Images.SetKeyName(1, "document-broken.png");
            this.imOrderStates.Images.SetKeyName(2, "document-hf-insert-footer.png");
            this.imOrderStates.Images.SetKeyName(3, "document--exclamation.png");
            this.imOrderStates.Images.SetKeyName(4, "document-task.png");
            this.imOrderStates.Images.SetKeyName(5, "document-hf-delete-footer.png");
            // 
            // colLogOrder
            // 
            this.colLogOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colLogOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogOrder.Caption = "�����";
            this.colLogOrder.FieldName = "OrderNumber";
            this.colLogOrder.Name = "colLogOrder";
            this.colLogOrder.OptionsColumn.AllowEdit = false;
            this.colLogOrder.OptionsColumn.ReadOnly = true;
            this.colLogOrder.Visible = true;
            this.colLogOrder.VisibleIndex = 2;
            this.colLogOrder.Width = 150;
            // 
            // colLogVehicle
            // 
            this.colLogVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogVehicle.Caption = "���������";
            this.colLogVehicle.FieldName = "VehicleName";
            this.colLogVehicle.Name = "colLogVehicle";
            this.colLogVehicle.OptionsColumn.AllowEdit = false;
            this.colLogVehicle.OptionsColumn.ReadOnly = true;
            this.colLogVehicle.Visible = true;
            this.colLogVehicle.VisibleIndex = 3;
            this.colLogVehicle.Width = 138;
            // 
            // colLogParMes
            // 
            this.colLogParMes.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogParMes.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogParMes.Caption = "���������";
            this.colLogParMes.FieldName = "ParamMessage";
            this.colLogParMes.Name = "colLogParMes";
            this.colLogParMes.OptionsColumn.AllowEdit = false;
            this.colLogParMes.OptionsColumn.ReadOnly = true;
            this.colLogParMes.Visible = true;
            this.colLogParMes.VisibleIndex = 4;
            this.colLogParMes.Width = 412;
            // 
            // colLogParVal
            // 
            this.colLogParVal.AppearanceCell.Options.UseTextOptions = true;
            this.colLogParVal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogParVal.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogParVal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogParVal.Caption = "��������";
            this.colLogParVal.FieldName = "ParamValue";
            this.colLogParVal.Name = "colLogParVal";
            this.colLogParVal.OptionsColumn.AllowEdit = false;
            this.colLogParVal.OptionsColumn.ReadOnly = true;
            this.colLogParVal.Visible = true;
            this.colLogParVal.VisibleIndex = 6;
            this.colLogParVal.Width = 99;
            // 
            // colLogUser
            // 
            this.colLogUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogUser.Caption = "���������";
            this.colLogUser.FieldName = "OrderUserName";
            this.colLogUser.Name = "colLogUser";
            this.colLogUser.Visible = true;
            this.colLogUser.VisibleIndex = 7;
            this.colLogUser.Width = 122;
            // 
            // rleLogWayBill
            // 
            this.rleLogWayBill.AutoHeight = false;
            this.rleLogWayBill.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleLogWayBill.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VehicleForOrder", "��������")});
            this.rleLogWayBill.DisplayMember = "VehicleForOrder";
            this.rleLogWayBill.Name = "rleLogWayBill";
            this.rleLogWayBill.NullText = "";
            // 
            // rleLogOrder
            // 
            this.rleLogOrder.AutoHeight = false;
            this.rleLogOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleLogOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Number", "�����")});
            this.rleLogOrder.DisplayMember = "Number";
            this.rleLogOrder.Name = "rleLogOrder";
            this.rleLogOrder.NullText = "";
            // 
            // rleLogObject
            // 
            this.rleLogObject.AutoHeight = false;
            this.rleLogObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleLogObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleLogObject.DisplayMember = "Name";
            this.rleLogObject.Name = "rleLogObject";
            this.rleLogObject.NullText = "";
            // 
            // xtpDictionary
            // 
            this.xtpDictionary.Name = "xtpDictionary";
            this.xtpDictionary.Size = new System.Drawing.Size(933, 567);
            this.xtpDictionary.Text = "�����������";
            // 
            // xtpWB
            // 
            this.xtpWB.Controls.Add(this.gcWayBill);
            this.xtpWB.Name = "xtpWB";
            this.xtpWB.Size = new System.Drawing.Size(933, 567);
            this.xtpWB.Text = "������� �����";
            // 
            // gcWayBill
            // 
            this.gcWayBill.DataSource = this.bsWayBill;
            this.gcWayBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcWayBill.Location = new System.Drawing.Point(0, 0);
            this.gcWayBill.MainView = this.gvWayBill;
            this.gcWayBill.MenuManager = this.barManager1;
            this.gcWayBill.Name = "gcWayBill";
            this.gcWayBill.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleVehicle,
            this.rleDriver,
            this.rleEnterprise,
            this.rleShift,
            this.rleVehicleState,
            this.rgleVehicleState,
            this.rieVehicleState,
            this.ricVehicleState,
            this.rgleVehicle,
            this.rgleDriver});
            this.gcWayBill.Size = new System.Drawing.Size(933, 567);
            this.gcWayBill.TabIndex = 1;
            this.gcWayBill.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvWayBill});
            // 
            // gvWayBill
            // 
            this.gvWayBill.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWbId,
            this.colWbDate,
            this.colWbVehicleState,
            this.colWbVehicle,
            this.colWbDriver,
            this.colWbShift,
            this.colWbEnterprise,
            this.colWbClose,
            this.colWbBTrip});
            this.gvWayBill.GridControl = this.gcWayBill;
            this.gvWayBill.Images = this.imVehStates;
            this.gvWayBill.Name = "gvWayBill";
            this.gvWayBill.OptionsView.ShowAutoFilterRow = true;
            this.gvWayBill.DoubleClick += new System.EventHandler(this.gvWayBill_DoubleClick);
            // 
            // colWbId
            // 
            this.colWbId.AppearanceCell.Options.UseTextOptions = true;
            this.colWbId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbId.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbId.Caption = "�����";
            this.colWbId.FieldName = "Id";
            this.colWbId.Name = "colWbId";
            this.colWbId.OptionsColumn.AllowEdit = false;
            this.colWbId.OptionsColumn.ReadOnly = true;
            this.colWbId.Visible = true;
            this.colWbId.VisibleIndex = 0;
            this.colWbId.Width = 90;
            // 
            // colWbDate
            // 
            this.colWbDate.AppearanceCell.Options.UseTextOptions = true;
            this.colWbDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbDate.Caption = "����";
            this.colWbDate.FieldName = "Date";
            this.colWbDate.Name = "colWbDate";
            this.colWbDate.OptionsColumn.AllowEdit = false;
            this.colWbDate.OptionsColumn.ReadOnly = true;
            this.colWbDate.Visible = true;
            this.colWbDate.VisibleIndex = 1;
            this.colWbDate.Width = 120;
            // 
            // colWbVehicleState
            // 
            this.colWbVehicleState.ColumnEdit = this.ricVehicleState;
            this.colWbVehicleState.FieldName = "VehicleStateId";
            this.colWbVehicleState.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colWbVehicleState.Name = "colWbVehicleState";
            this.colWbVehicleState.OptionsColumn.AllowEdit = false;
            this.colWbVehicleState.OptionsColumn.ReadOnly = true;
            this.colWbVehicleState.OptionsColumn.ShowCaption = false;
            this.colWbVehicleState.Visible = true;
            this.colWbVehicleState.VisibleIndex = 2;
            this.colWbVehicleState.Width = 23;
            // 
            // ricVehicleState
            // 
            this.ricVehicleState.AutoHeight = false;
            this.ricVehicleState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricVehicleState.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2)});
            this.ricVehicleState.Name = "ricVehicleState";
            this.ricVehicleState.SmallImages = this.imVehStates;
            // 
            // imVehStates
            // 
            this.imVehStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imVehStates.ImageStream")));
            this.imVehStates.Images.SetKeyName(0, "status-offline.png");
            this.imVehStates.Images.SetKeyName(1, "circle_green.png");
            this.imVehStates.Images.SetKeyName(2, "square_yellow.png");
            this.imVehStates.Images.SetKeyName(3, "triangle_red.png");
            this.imVehStates.Images.SetKeyName(4, "romb_blue.png");
            // 
            // colWbVehicle
            // 
            this.colWbVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbVehicle.Caption = "���������";
            this.colWbVehicle.ColumnEdit = this.rgleVehicle;
            this.colWbVehicle.FieldName = "vehicle";
            this.colWbVehicle.Name = "colWbVehicle";
            this.colWbVehicle.OptionsColumn.AllowEdit = false;
            this.colWbVehicle.OptionsColumn.ReadOnly = true;
            this.colWbVehicle.Visible = true;
            this.colWbVehicle.VisibleIndex = 3;
            this.colWbVehicle.Width = 275;
            // 
            // rgleVehicle
            // 
            this.rgleVehicle.AutoHeight = false;
            this.rgleVehicle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleVehicle.DisplayMember = "NumberPlate";
            this.rgleVehicle.Name = "rgleVehicle";
            this.rgleVehicle.NullText = "";
            this.rgleVehicle.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleVehicle.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleVehicle.View = this.rgleVehicleView;
            // 
            // rgleVehicleView
            // 
            this.rgleVehicleView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVehicle});
            this.rgleVehicleView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.rgleVehicleView.Name = "rgleVehicleView";
            this.rgleVehicleView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.rgleVehicleView.OptionsView.ShowGroupPanel = false;
            this.rgleVehicleView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVehicle, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "���������";
            this.colVehicle.FieldName = "NumberPlate";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 0;
            // 
            // colWbDriver
            // 
            this.colWbDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbDriver.Caption = "��������";
            this.colWbDriver.ColumnEdit = this.rgleDriver;
            this.colWbDriver.FieldName = "driver";
            this.colWbDriver.Name = "colWbDriver";
            this.colWbDriver.OptionsColumn.AllowEdit = false;
            this.colWbDriver.OptionsColumn.ReadOnly = true;
            this.colWbDriver.Visible = true;
            this.colWbDriver.VisibleIndex = 4;
            this.colWbDriver.Width = 155;
            // 
            // rgleDriver
            // 
            this.rgleDriver.AutoHeight = false;
            this.rgleDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleDriver.DisplayMember = "Family";
            this.rgleDriver.Name = "rgleDriver";
            this.rgleDriver.NullText = "";
            this.rgleDriver.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleDriver.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleDriver.View = this.rgleDriverView;
            // 
            // rgleDriverView
            // 
            this.rgleDriverView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDriver});
            this.rgleDriverView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.rgleDriverView.Name = "rgleDriverView";
            this.rgleDriverView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.rgleDriverView.OptionsView.ShowGroupPanel = false;
            this.rgleDriverView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDriver, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDriver
            // 
            this.colDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.colDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDriver.Caption = "��������";
            this.colDriver.FieldName = "Family";
            this.colDriver.Name = "colDriver";
            this.colDriver.Visible = true;
            this.colDriver.VisibleIndex = 0;
            // 
            // colWbShift
            // 
            this.colWbShift.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbShift.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbShift.Caption = "�����";
            this.colWbShift.ColumnEdit = this.rleShift;
            this.colWbShift.FieldName = "md_working_shift";
            this.colWbShift.Name = "colWbShift";
            this.colWbShift.OptionsColumn.AllowEdit = false;
            this.colWbShift.OptionsColumn.ReadOnly = true;
            this.colWbShift.Visible = true;
            this.colWbShift.VisibleIndex = 5;
            this.colWbShift.Width = 109;
            // 
            // rleShift
            // 
            this.rleShift.AutoHeight = false;
            this.rleShift.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleShift.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleShift.DisplayMember = "Name";
            this.rleShift.Name = "rleShift";
            this.rleShift.NullText = "";
            // 
            // colWbEnterprise
            // 
            this.colWbEnterprise.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbEnterprise.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbEnterprise.Caption = "�����������";
            this.colWbEnterprise.ColumnEdit = this.rleEnterprise;
            this.colWbEnterprise.FieldName = "md_enterprise";
            this.colWbEnterprise.Name = "colWbEnterprise";
            this.colWbEnterprise.OptionsColumn.AllowEdit = false;
            this.colWbEnterprise.OptionsColumn.ReadOnly = true;
            this.colWbEnterprise.Visible = true;
            this.colWbEnterprise.VisibleIndex = 6;
            this.colWbEnterprise.Width = 218;
            // 
            // rleEnterprise
            // 
            this.rleEnterprise.AutoHeight = false;
            this.rleEnterprise.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleEnterprise.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleEnterprise.DisplayMember = "Name";
            this.rleEnterprise.Name = "rleEnterprise";
            this.rleEnterprise.NullText = "";
            // 
            // colWbClose
            // 
            this.colWbClose.AppearanceCell.Options.UseTextOptions = true;
            this.colWbClose.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbClose.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbClose.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbClose.Caption = "������";
            this.colWbClose.FieldName = "IsClose";
            this.colWbClose.Name = "colWbClose";
            this.colWbClose.OptionsColumn.AllowEdit = false;
            this.colWbClose.OptionsColumn.ReadOnly = true;
            this.colWbClose.Visible = true;
            this.colWbClose.VisibleIndex = 7;
            this.colWbClose.Width = 81;
            // 
            // colWbBTrip
            // 
            this.colWbBTrip.AppearanceCell.Options.UseTextOptions = true;
            this.colWbBTrip.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbBTrip.AppearanceHeader.Options.UseTextOptions = true;
            this.colWbBTrip.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWbBTrip.Caption = "�������������";
            this.colWbBTrip.FieldName = "IsBusinessTrip";
            this.colWbBTrip.Name = "colWbBTrip";
            this.colWbBTrip.OptionsColumn.AllowEdit = false;
            this.colWbBTrip.OptionsColumn.ReadOnly = true;
            this.colWbBTrip.Visible = true;
            this.colWbBTrip.VisibleIndex = 8;
            this.colWbBTrip.Width = 179;
            // 
            // rleVehicle
            // 
            this.rleVehicle.AutoHeight = false;
            this.rleVehicle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehicle.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo( "NumberPlate", "�����", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.rleVehicle.DisplayMember = "NumberPlate";
            this.rleVehicle.Name = "rleVehicle";
            this.rleVehicle.NullText = "";
            // 
            // rleDriver
            // 
            this.rleDriver.AutoHeight = false;
            this.rleDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleDriver.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Family", "�������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.rleDriver.DisplayMember = "Family";
            this.rleDriver.Name = "rleDriver";
            this.rleDriver.NullText = "";
            // 
            // rleVehicleState
            // 
            this.rleVehicleState.AutoHeight = false;
            this.rleVehicleState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehicleState.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("StateId", "������")});
            this.rleVehicleState.DisplayMember = "StateId";
            this.rleVehicleState.Name = "rleVehicleState";
            this.rleVehicleState.NullText = "";
            // 
            // rgleVehicleState
            // 
            this.rgleVehicleState.AutoHeight = false;
            this.rgleVehicleState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleVehicleState.Name = "rgleVehicleState";
            this.rgleVehicleState.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Images = this.imVehStates;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // rieVehicleState
            // 
            this.rieVehicleState.AutoHeight = false;
            this.rieVehicleState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rieVehicleState.Images = this.imVehStates;
            this.rieVehicleState.Name = "rieVehicleState";
            // 
            // xtpOrder
            // 
            this.xtpOrder.Controls.Add(this.gcOrder);
            this.xtpOrder.Name = "xtpOrder";
            this.xtpOrder.Size = new System.Drawing.Size(933, 567);
            this.xtpOrder.Text = "����� - ������";
            // 
            // gcOrder
            // 
            this.gcOrder.DataSource = this.bsOrder;
            this.gcOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrder.Location = new System.Drawing.Point(0, 0);
            this.gcOrder.MainView = this.gvOrder;
            this.gcOrder.MenuManager = this.barManager1;
            this.gcOrder.Name = "gcOrder";
            this.gcOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleOrderObject,
            this.rleOrderCategory,
            this.rleOrderTranspType,
            this.rleOrderPriority,
            this.rleWayBill,
            this.ricOrderStates});
            this.gcOrder.Size = new System.Drawing.Size(933, 567);
            this.gcOrder.TabIndex = 0;
            this.gcOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOrder});
            // 
            // gvOrder
            // 
            this.gvOrder.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvOrder.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvOrder.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.Empty.Options.UseBackColor = true;
            this.gvOrder.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvOrder.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvOrder.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvOrder.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvOrder.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvOrder.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvOrder.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvOrder.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvOrder.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvOrder.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvOrder.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvOrder.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.OddRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.OddRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvOrder.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvOrder.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvOrder.Appearance.Preview.Options.UseBackColor = true;
            this.gvOrder.Appearance.Preview.Options.UseFont = true;
            this.gvOrder.Appearance.Preview.Options.UseForeColor = true;
            this.gvOrder.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvOrder.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.Row.Options.UseBackColor = true;
            this.gvOrder.Appearance.Row.Options.UseForeColor = true;
            this.gvOrder.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvOrder.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvOrder.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvOrder.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvOrder.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvOrder.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvOrder.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvOrder.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvOrder.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvOrder.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvOrder.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvOrder.Appearance.VertLine.Options.UseBackColor = true;
            this.gvOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrdId,
            this.colOrdState,
            this.colOrdNumber,
            this.colOrdDate,
            this.colOrdCustomer,
            this.colOrdCategory,
            this.colOrdTranspType,
            this.colOrdPriority,
            this.colOrdWayBill,
            this.colOrdUser,
            this.colOrdRemark});
            this.gvOrder.GridControl = this.gcOrder;
            this.gvOrder.Name = "gvOrder";
            this.gvOrder.OptionsView.EnableAppearanceEvenRow = true;
            this.gvOrder.OptionsView.EnableAppearanceOddRow = true;
            this.gvOrder.OptionsView.ShowAutoFilterRow = true;
            this.gvOrder.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvOrder_CellValueChanged);
            this.gvOrder.DoubleClick += new System.EventHandler(this.gvOrder_DoubleClick);
            // 
            // colOrdId
            // 
            this.colOrdId.Caption = "Id";
            this.colOrdId.FieldName = "Id";
            this.colOrdId.Name = "colOrdId";
            this.colOrdId.OptionsColumn.AllowEdit = false;
            this.colOrdId.OptionsColumn.ReadOnly = true;
            // 
            // colOrdState
            // 
            this.colOrdState.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdState.Caption = "������";
            this.colOrdState.ColumnEdit = this.ricOrderStates;
            this.colOrdState.FieldName = "StateId";
            this.colOrdState.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colOrdState.Name = "colOrdState";
            this.colOrdState.OptionsColumn.ShowCaption = false;
            this.colOrdState.Visible = true;
            this.colOrdState.VisibleIndex = 0;
            this.colOrdState.Width = 31;
            // 
            // ricOrderStates
            // 
            this.ricOrderStates.AutoHeight = false;
            this.ricOrderStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricOrderStates.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ricOrderStates.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("����������", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("��������", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("������", 6, 5)});
            this.ricOrderStates.Name = "ricOrderStates";
            this.ricOrderStates.SmallImages = this.imOrderStates;
            // 
            // colOrdNumber
            // 
            this.colOrdNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.Caption = "�����";
            this.colOrdNumber.FieldName = "Number";
            this.colOrdNumber.Name = "colOrdNumber";
            this.colOrdNumber.OptionsColumn.AllowEdit = false;
            this.colOrdNumber.OptionsColumn.ReadOnly = true;
            this.colOrdNumber.Visible = true;
            this.colOrdNumber.VisibleIndex = 1;
            this.colOrdNumber.Width = 102;
            // 
            // colOrdDate
            // 
            this.colOrdDate.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.Caption = "����";
            this.colOrdDate.FieldName = "DateCreate";
            this.colOrdDate.Name = "colOrdDate";
            this.colOrdDate.OptionsColumn.AllowEdit = false;
            this.colOrdDate.OptionsColumn.ReadOnly = true;
            this.colOrdDate.Visible = true;
            this.colOrdDate.VisibleIndex = 2;
            this.colOrdDate.Width = 99;
            // 
            // colOrdCustomer
            // 
            this.colOrdCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdCustomer.Caption = "��������";
            this.colOrdCustomer.ColumnEdit = this.rleOrderObject;
            this.colOrdCustomer.FieldName = "md_objectCustomer";
            this.colOrdCustomer.Name = "colOrdCustomer";
            this.colOrdCustomer.OptionsColumn.AllowEdit = false;
            this.colOrdCustomer.OptionsColumn.ReadOnly = true;
            this.colOrdCustomer.Visible = true;
            this.colOrdCustomer.VisibleIndex = 3;
            this.colOrdCustomer.Width = 222;
            // 
            // rleOrderObject
            // 
            this.rleOrderObject.AutoHeight = false;
            this.rleOrderObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrderObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleOrderObject.DisplayMember = "Name";
            this.rleOrderObject.Name = "rleOrderObject";
            this.rleOrderObject.NullText = "";
            // 
            // colOrdCategory
            // 
            this.colOrdCategory.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdCategory.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdCategory.Caption = "���������";
            this.colOrdCategory.ColumnEdit = this.rleOrderCategory;
            this.colOrdCategory.FieldName = "md_order_category";
            this.colOrdCategory.Name = "colOrdCategory";
            this.colOrdCategory.OptionsColumn.AllowEdit = false;
            this.colOrdCategory.OptionsColumn.ReadOnly = true;
            this.colOrdCategory.Visible = true;
            this.colOrdCategory.VisibleIndex = 4;
            this.colOrdCategory.Width = 141;
            // 
            // rleOrderCategory
            // 
            this.rleOrderCategory.AutoHeight = false;
            this.rleOrderCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrderCategory.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleOrderCategory.DisplayMember = "Name";
            this.rleOrderCategory.Name = "rleOrderCategory";
            this.rleOrderCategory.NullText = "";
            // 
            // colOrdTranspType
            // 
            this.colOrdTranspType.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdTranspType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdTranspType.Caption = "��� ���������";
            this.colOrdTranspType.ColumnEdit = this.rleOrderTranspType;
            this.colOrdTranspType.FieldName = "md_transportation_types";
            this.colOrdTranspType.Name = "colOrdTranspType";
            this.colOrdTranspType.OptionsColumn.AllowEdit = false;
            this.colOrdTranspType.OptionsColumn.ReadOnly = true;
            this.colOrdTranspType.Visible = true;
            this.colOrdTranspType.VisibleIndex = 5;
            this.colOrdTranspType.Width = 124;
            // 
            // rleOrderTranspType
            // 
            this.rleOrderTranspType.AutoHeight = false;
            this.rleOrderTranspType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrderTranspType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleOrderTranspType.DisplayMember = "Name";
            this.rleOrderTranspType.Name = "rleOrderTranspType";
            this.rleOrderTranspType.NullText = "";
            // 
            // colOrdPriority
            // 
            this.colOrdPriority.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdPriority.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdPriority.Caption = "���������";
            this.colOrdPriority.ColumnEdit = this.rleOrderPriority;
            this.colOrdPriority.FieldName = "md_order_priority";
            this.colOrdPriority.Name = "colOrdPriority";
            this.colOrdPriority.OptionsColumn.AllowEdit = false;
            this.colOrdPriority.OptionsColumn.ReadOnly = true;
            this.colOrdPriority.Visible = true;
            this.colOrdPriority.VisibleIndex = 6;
            this.colOrdPriority.Width = 99;
            // 
            // rleOrderPriority
            // 
            this.rleOrderPriority.AutoHeight = false;
            this.rleOrderPriority.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrderPriority.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleOrderPriority.DisplayMember = "Name";
            this.rleOrderPriority.Name = "rleOrderPriority";
            this.rleOrderPriority.NullText = "";
            // 
            // colOrdWayBill
            // 
            this.colOrdWayBill.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdWayBill.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colOrdWayBill.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdWayBill.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdWayBill.Caption = "������� ����";
            this.colOrdWayBill.ColumnEdit = this.rleWayBill;
            this.colOrdWayBill.FieldName = "md_waybill";
            this.colOrdWayBill.Name = "colOrdWayBill";
            this.colOrdWayBill.Visible = true;
            this.colOrdWayBill.VisibleIndex = 7;
            this.colOrdWayBill.Width = 119;
            // 
            // rleWayBill
            // 
            this.rleWayBill.AutoHeight = false;
            this.rleWayBill.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleWayBill.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VehicleForOrder", "���������")});
            this.rleWayBill.DisplayMember = "VehicleForOrder";
            this.rleWayBill.Name = "rleWayBill";
            this.rleWayBill.NullText = "";
            // 
            // colOrdUser
            // 
            this.colOrdUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdUser.Caption = "���������";
            this.colOrdUser.FieldName = "UserCreator";
            this.colOrdUser.Name = "colOrdUser";
            this.colOrdUser.OptionsColumn.AllowEdit = false;
            this.colOrdUser.OptionsColumn.ReadOnly = true;
            this.colOrdUser.Visible = true;
            this.colOrdUser.VisibleIndex = 8;
            this.colOrdUser.Width = 113;
            // 
            // colOrdRemark
            // 
            this.colOrdRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdRemark.Caption = "����������";
            this.colOrdRemark.FieldName = "Remark";
            this.colOrdRemark.Name = "colOrdRemark";
            this.colOrdRemark.OptionsColumn.AllowEdit = false;
            this.colOrdRemark.OptionsColumn.ReadOnly = true;
            this.colOrdRemark.Visible = true;
            this.colOrdRemark.VisibleIndex = 9;
            this.colOrdRemark.Width = 200;
            // 
            // xtpReports
            // 
            this.xtpReports.Name = "xtpReports";
            this.xtpReports.Size = new System.Drawing.Size(933, 567);
            this.xtpReports.Text = "������";
            // 
            // xtpUserRoles
            // 
            this.xtpUserRoles.Name = "xtpUserRoles";
            this.xtpUserRoles.Size = new System.Drawing.Size(933, 567);
            this.xtpUserRoles.Text = "������������";
            // 
            // MainMD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 677);
            this.Controls.Add(this.xtbMain);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMD";
            this.Text = "���������������";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainMD_FormClosed);
            this.Load += new System.EventHandler(this.MainMD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rseTail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbMain)).EndInit();
            this.xtbMain.ResumeLayout(false);
            this.xtpMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMap)).EndInit();
            this.splitMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricOrderState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLogObject)).EndInit();
            this.xtpWB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricVehicleState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imVehStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicleView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriverView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehicleState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicleState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rieVehicleState)).EndInit();
            this.xtpOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricOrderStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderTranspType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrderPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraTab.XtraTabControl xtbMain;
        private DevExpress.XtraTab.XtraTabPage xtpDictionary;
        private DevExpress.XtraTab.XtraTabPage xtpWB;
        private DevExpress.XtraTab.XtraTabPage xtpOrder;
        private DevExpress.XtraTab.XtraTabPage xtpReports;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAdd;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem btStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEnd;
        private DevExpress.XtraGrid.GridControl gcWayBill;
        private DevExpress.XtraGrid.Columns.GridColumn colWbId;
        private DevExpress.XtraGrid.Columns.GridColumn colWbDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWbVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colWbDriver;
        private DevExpress.XtraGrid.Columns.GridColumn colWbEnterprise;
        private DevExpress.XtraGrid.Columns.GridColumn colWbClose;
        private DevExpress.XtraGrid.Columns.GridColumn colWbBTrip;
        private DevExpress.XtraGrid.Columns.GridColumn colWbShift;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvWayBill;
        internal System.Windows.Forms.BindingSource bsWayBill;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehicle;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleDriver;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleEnterprise;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleShift;
        internal DevExpress.XtraBars.BarEditItem bdeStart;
        internal DevExpress.XtraBars.BarEditItem bdeEnd;
        internal DevExpress.XtraBars.BarEditItem beiNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdId;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdTranspType;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdWayBill;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdRemark;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvOrder;
        internal System.Windows.Forms.BindingSource bsOrder;
        private DevExpress.XtraGrid.GridControl gcOrder;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrderObject;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrderCategory;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrderTranspType;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrderPriority;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem bbiPrintForms;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleWayBill;
        private DevExpress.XtraGrid.Columns.GridColumn colWbVehicleState;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehicleState;
        private DevExpress.Utils.ImageCollection imVehStates;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleVehicleState;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit rieVehicleState;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricVehicleState;
        private DevExpress.XtraTab.XtraTabPage xtpMap;
        private DevExpress.XtraEditors.SplitContainerControl splitMap;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraBars.StandaloneBarDockControl dockControlOnline;
        private DevExpress.XtraBars.BarEditItem beiTail;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit rseTail;
        private DevExpress.XtraBars.BarCheckItem bchAutoFit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.Columns.GridColumn colLogId;
        private DevExpress.XtraGrid.Columns.GridColumn colLogData;
        private DevExpress.XtraGrid.Columns.GridColumn colLogObject;
        private DevExpress.XtraGrid.Columns.GridColumn colLogOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colLogVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colLogParVal;
        private DevExpress.XtraGrid.Columns.GridColumn colLogParMes;
        internal DevExpress.XtraGrid.GridControl gcLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLog;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleLogWayBill;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleLogObject;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleLogOrder;
        private DevExpress.XtraBars.BarButtonItem bbiCopy;
        private DevExpress.XtraGrid.Columns.GridColumn colLogOrderState;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdUser;
        private DevExpress.XtraGrid.Columns.GridColumn colOrdState;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricOrderStates;
        internal DevExpress.Utils.ImageCollection imOrderStates;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricOrderState;
        private DevExpress.XtraGrid.Columns.GridColumn colLogUser;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleVehicle;
        private DevExpress.XtraGrid.Views.Grid.GridView rgleVehicleView;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraGrid.Views.Grid.GridView rgleDriverView;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleDriver;
        private DevExpress.XtraGrid.Columns.GridColumn colDriver;
        private DevExpress.XtraBars.BarEditItem beSwShowDeleted;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup rgDeleted;
        private DevExpress.XtraTab.XtraTabPage xtpUserRoles;
        private DevExpress.XtraGrid.Columns.GridColumn colIsRead;
        private DevExpress.XtraBars.BarButtonItem bbiShowLog;
        private DevExpress.XtraBars.BarButtonItem bbiClearVehSelection;

    }
}