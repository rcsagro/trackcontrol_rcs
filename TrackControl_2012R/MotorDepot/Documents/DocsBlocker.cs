﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using DevExpress.XtraEditors; 

namespace MotorDepot
{
    public static class DocsBlocker
    {
        private const int BlockHouers = 8;
        public static bool IsDocBlocked(ref int BlockUserId,ref DateTime? BlockDate)
        {

            if (BlockUserId == 0 || BlockUserId == UserBaseCurrent.Instance.Id)
                    return false;
                else
                    // автосброс блокировки, если прошло BLOCK_HOUERS с момента блокировки
                    if (DateTime.Now.AddHours(-BlockHouers).Subtract(BlockDate ?? DateTime.MinValue).TotalMinutes > 0)
                    {
                        BlockUserId = 0;
                        BlockDate = null;
                        //WayBillProvider.UpdateBlockState(this);
                        return false;
                    }
                    else
                    {
                        string userName;
                        if (BlockUserId == ConstsGen.USER_ADMIN_CODE)
                            userName = ConstsGen.USER_ADMIN_NAME;
                        else
                        {
                            UserBase ub = UserBaseProvider.GetUserBase(BlockUserId);
                            userName = ub.Name;
                        }
                        XtraMessageBox.Show(string.Format("{0} {1}", "С документом работает", userName), string.Format("{0}", BlockDate));
                        return true;
                    }
        }
    }
}
