﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.Objects;
using DevExpress.XtraEditors;
using MotorDepot.AccessObjects;
using MotorDepot.MonitoringGps;
using TrackControl.General;

namespace MotorDepot
{
    /// <summary>
    /// управление данными шапки заказ - наряда
    /// </summary>
    public static class OrderController
    {
        public static event VoidHandler UpdateOrderState;
        
        internal static MotorDepotEntities ContextMotorDepot;
        

        private static vehicle _vehiclePrev;

        internal static void SetViewDataSource(MainMD view,bool showWorked)
        {
            DateTime start = (DateTime)view.bdeStart.EditValue;
            DateTime end = (DateTime)view.bdeEnd.EditValue;
            //ContextMotorDepot.vehicleSet.ToList();
            view.rleOrderObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
            view.rleOrderCategory.DataSource = ContextMotorDepot.order_categorySet.ToList();
            view.rleOrderTranspType.DataSource = ContextMotorDepot.transportation_typesSet.ToList();
            view.rleOrderPriority.DataSource = ContextMotorDepot.order_prioritySet.ToList();
            view.rleWayBill.DataSource = ContextMotorDepot.waybillSet.Include("vehicle").Where(wb => (wb.Date >= start && wb.Date <= end) || wb.IsClose == false).ToList();
            if (view.beiNumber.EditValue != null && view.beiNumber.EditValue.ToString().Length > 0)
            {
                string numberOrder = view.beiNumber.EditValue.ToString();
                var ordersNumber = ContextMotorDepot.orderSet.Include("md_waybill").Where(t => t.Number == numberOrder);
                if (!UserBaseCurrent.Instance.Admin)
                    ordersNumber = ordersNumber.Where(ord => ord.md_waybill.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ordersNumber); 
                view.bsOrder.DataSource = ordersNumber.ToList();
            }
            else
            {
                var orders = ContextMotorDepot.orderSet.Include("md_waybill").Where(ord => ord.DateCreate >= start && ord.DateCreate <= end);
                if (showWorked)
                {
                    orders = orders.Where(ord => ord.md_order_state.Id != (int)OrderStates.Delete);
                }
                else
                {
                    orders = orders.Where(ord => ord.md_order_state.Id == (int)OrderStates.Delete);
                }
                if (!UserBaseCurrent.Instance.Admin)
                    orders = orders.Where(ord => ord.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orders); 
                view.bsOrder.DataSource = orders.ToList();
            }

        }

        internal static bool DeleteOrder(order orderForDelete)
        {
            if (!MdUser.Instance.IsObjectEditableForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return false;
            if (XtraMessageBox.Show(string.Format("Подтверждаете удаление заказ-наряд № {0}?", orderForDelete.Number),
                      MDController.MesCaption,MessageBoxButtons.YesNo) != DialogResult.Yes) return false; 
            try
            {
                    ChangeOrderVehicleState(orderForDelete, (int)VehicleStates.OrderWaitng);
                //Заказы без содержания удаляем, с содержанием помечаем как удаленные
                    if (orderForDelete.md_ordert.Count == 0)
                    {
                        //ContextMotorDepot.DeleteObject(orderForDelete);
                        //MDController.Instance.MotorDepotEntitiesSaveChanges(orderForDelete);
                        OrderProvider.Delete(orderForDelete);  
                        return true;
                    }
                    else
                    {
                        //Не разрешать удалять активный заказ
                        order_state ordState = GetOrderState(orderForDelete);
                        if (ordState.Id == (int)OrderStates.Execute || ordState.Id == (int)OrderStates.Finish)
                        {
                            XtraMessageBox.Show("Нельзя удалять активный заказ-наряд!",MDController.MesCaption);
                            return false;
                        }
                        else
                        {
                            SetOrderState(orderForDelete, OrderStates.Delete);
                            orderForDelete.DateDelete = DateTime.Now;
                            OrderProvider.UpdateOrder(orderForDelete);  
                            UserLog.InsertLog(UserLogTypes.MD_ORDER, "Удаление", orderForDelete.Id);
                            return true;
                        }
                        
                    }
                    
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("Не удалось удалить заказ-наряд №{0}{1}{2}", orderForDelete.Number, Environment.NewLine, ex.Message),
                MDController.MesCaption); 
                return false;
            }
        }

        internal static void SaveOrder(order orderToSave)
        {
            if (orderToSave != null)
            {
                if (orderToSave.Id == 0)
                {
                    //ContextMotorDepot.AddToorderSet(orderToSave);
                    UserLog.InsertLog(UserLogTypes.MD_ORDER, "Создание", orderToSave.Id);
                    SetOrderDefaults(orderToSave);
                    OrderProvider.InsertOrder(orderToSave); 
                    //SetOrderState(orderToSave, OrderStates.Design);

                    //MDController.Instance.MotorDepotEntitiesSaveChanges(orderToSave);
                }
                else
                {
                    orderToSave.DateEdit = DateTime.Now;
                    if (!orderToSave.IsClose) ChangeVehiclesStates(orderToSave);
                    OrderProvider.UpdateOrder(orderToSave);
                }

            }
            
        }

        internal static bool RecalcOrder(order orderToRecalc,bool isMessageShow)
        {
            if (orderToRecalc.md_waybill == null || orderToRecalc.md_waybill.vehicle == null || orderToRecalc.md_ordert.Count ==0 )
                return false;
            if (isMessageShow)
            {
                if (XtraMessageBox.Show(string.Format("Подтверждаете перерасчет фактических данных заказ-наряд № {0}?", orderToRecalc.Number),
              MDController.MesCaption, MessageBoxButtons.YesNo) != DialogResult.Yes) return false; 
            }
            if (orderToRecalc != null)
            {
                using (OrderFactCalculator fact = new OrderFactCalculator(orderToRecalc))
                {
                    fact.Start();
                   
                }
                if (isMessageShow)
                {
                    XtraMessageBox.Show(string.Format("Заказ-наряд № {0} пересчитан", orderToRecalc.Number),
              MDController.MesCaption);
                    UserLog.InsertLog(UserLogTypes.MD_ORDER, "Перерасчет фактических данных", orderToRecalc.Id);
                }


            }
            return true;
        }

        private static void ChangeVehiclesStates(order orderToSave)
        {
            if (_vehiclePrev != null)
            {
                _vehiclePrev.vehicle_state = ContextMotorDepot.vehicle_stateSet.Where(vs => vs.Id == (int)VehicleStates.OrderWaitng).FirstOrDefault();
            }
            ChangeOrderVehicleState(orderToSave, (int)VehicleStates.OrderExecuting);
 }

        internal static void SetOrderState(order orderToSave,OrderStates orderState)
        {
            if (orderToSave != null)
            {
                int state = (int)orderState;
                orderToSave.md_order_state = ContextMotorDepot.order_stateSet.Where(st => st.Id == state).FirstOrDefault();
                OrderProvider.SetOrderState(orderToSave,orderState);
                //MDController.Instance.MotorDepotEntitiesSaveChanges(orderToSave);
                if (UpdateOrderState != null) UpdateOrderState();
                //UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("Установка статуса в {0}", GetOrderStateName(orderState)), orderToSave.Id);
            }
        }


        internal static order_state GetOrderState(order orderForCheck)
        {

            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderForCheck);
            return  ContextMotorDepot.order_stateSet.Where(st => st.Id == orderForCheck.StateId).FirstOrDefault();
        }

        internal static string GetOrderStateName(OrderStates orderState)
        {
            string sState = "ОФОРМЛЕНИЕ";
            switch (orderState)
            {
                case OrderStates.Design:
                    {
                        sState = "ОФОРМЛЕНИЕ";
                        break;
                    }
                case OrderStates.Refuse:
                    {
                        sState = "ОТКАЗ";
                        break;
                    }
                case OrderStates.Execute:
                    {
                        sState = "ВЫПОЛНЕНИЕ";
                        break;
                    }
                case OrderStates.Finish:
                    {
                        sState = "ЗАВЕРШЕН";
                        break;
                    }
                case OrderStates.Close:
                    {
                        sState = "ЗАКРЫТ";
                        break;
                    }
                case OrderStates.Delete:
                    {
                        sState = "УДАЛЕН";
                        break;
                    }
            }
            return sState;
        }

        internal static string GetNextNumber()
        {
            //ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orders);
            //1.	номер заказа имеет вид XXYYZZZZZZZ, где: XX-две последние цифры текущего года, YY – месяц,   
            //ZZZZZZZ – порядковый номер заказа, генерируемый системой;
            //var lastNumberRecord = (from item in ContextMotorDepot.orderSet
            //                        where item.DateCreate.Year == DateTime.Today.Year
            //                      && item.DateCreate.Month == DateTime.Today.Month
            //                      && item.Number.Length == codeLength
            //                      orderby item.Id descending 
            //                      select item).FirstOrDefault();
            int lastNumber=0;
            const int codeLength = 11;
            string lastOrderNumber = OrderProvider.GetLastOrderNumberForInterval(codeLength);
            if (lastOrderNumber.Length == codeLength)
            {
                if (Int32.TryParse(lastOrderNumber.Substring(4, 7), out lastNumber))
                {
                    lastNumber = Convert.ToInt32(lastOrderNumber.Substring(4, 7));
                }
            }
            int newNumber = lastNumber+1;
            return string.Format("{0}{1}{2}", DateTime.Today.Year.ToString().Substring(2, 2), DateTime.Today.Month.ToString("D2") , newNumber.ToString("D7"));
        }

        internal static void LoadOrderFormControls(OrderForm view)
        {
            view.rgleObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
            view.rleCategory.DataSource = ContextMotorDepot.order_categorySet.ToList();
            view.rleTranType.DataSource = ContextMotorDepot.transportation_typesSet.ToList();
            view.rlePriority.DataSource = ContextMotorDepot.order_prioritySet.ToList();
            view.rgleTObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
            view.rleTCargo.DataSource = ContextMotorDepot.cargoSet.ToList();
            view.rleAlarm.DataSource = ContextMotorDepot.alarming_typesSet.ToList();
            view.rleStateOrder.DataSource = ContextMotorDepot.order_stateSet.ToList();
            view.rleEnterprise.DataSource = ContextMotorDepot.enterpriseSet.ToList(); 
        }

        internal static void AddNewOrder(OrderForm form)
        {
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return;
            OrderController.LoadOrderFormControls(form);
            order newOrder = new order();
            SetOrderDefaults(newOrder);
            SetDataSourceOrderWayBillNew(form);
            //ContextMotorDepot.AddToorderSet(newOrder);
            OrderProvider.InsertOrder(newOrder);  
            //ContextMotorDepot.SaveChanges();
            form.pgOrder.SelectedObject = newOrder;
            form.bsOrderT.DataSource = ContextMotorDepot.ordertSet.Where(t => t.md_order.Id == newOrder.Id).ToList();
            form.Show();

        }

        private static void SetDataSourceOrderWayBillNew(OrderForm form)
        {
            var wbs = ContextMotorDepot.waybillSet.Where(
                    wb => wb.IsClose == false
                    && wb.vehicle.vehicle_state.Id == (int)VehicleStates.OrderWaitng);
            if (!UserBaseCurrent.Instance.Admin)
                wbs = wbs.Where(wb => wb.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
            form.rgleWayBill.DataSource = wbs.OrderBy(wb => wb.vehicle.NumberPlate).ToList();
             _vehiclePrev = null;
        }

        private static void SetDataSourceOrderWayBillExist(OrderForm viewOrder, order orderForView)
        {
            var wbs = ContextMotorDepot.waybillSet.Where(
        wb => ((wb.Id == orderForView.md_waybill.Id)
       | (wb.IsClose == false
        && wb.vehicle.vehicle_state.Id == (int)VehicleStates.OrderWaitng)));
            if (!UserBaseCurrent.Instance.Admin)
                wbs = wbs.Where(wb => wb.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
            viewOrder.rgleWayBill.DataSource = wbs.OrderBy(wb => wb.vehicle.NumberPlate).ToList();
        }

        internal static bool ViewOrder(order orderForView, OrderForm viewOrder)
        {
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return false;
            OrderController.LoadOrderFormControls(viewOrder);
            if (orderForView != null)
            {
                if (orderForView.md_waybill != null)
                {
                    SetDataSourceOrderWayBillExist(viewOrder, orderForView);
                    SetVehiclePrevious(orderForView);
                }
                else
                {
                    _vehiclePrev = null;
                    SetDataSourceOrderWayBillNew(viewOrder);
                }

                viewOrder.pgOrder.SelectedObject = orderForView;
                SetContentDataSource(orderForView, viewOrder);
                if (!orderForView.IsClose)  RecalcOrder(orderForView,false);
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderForView);
                viewOrder.Show();
                SetBlock(orderForView, viewOrder);
                return true;
            }
            return false;
        }

        public static void SetContentDataSource(order orderForView, OrderForm viewOrder)
        {
            List<ordert> content = ContextMotorDepot.ordertSet.Where(t => t.md_order.Id == orderForView.Id).ToList();
            viewOrder.bsOrderT.DataSource = content;
            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, content);
        }



        private static void SetVehiclePrevious(order orderForView)
        {
            if (orderForView != null && orderForView.md_waybill != null && orderForView.md_waybill.vehicle != null)
            _vehiclePrev = ContextMotorDepot.vehicleSet.Where(veh => veh.id == orderForView.md_waybill.vehicle.id).FirstOrDefault();
            else
            {
                _vehiclePrev = null;
            }
        }

        internal static void SetOrderDefaults(order newOrder)
        {
            newOrder.DateCreate = DateTime.Now;
            if (newOrder.DateInit == null) newOrder.DateInit = DateTime.Now;
            newOrder.Number = OrderController.GetNextNumber();
            newOrder.md_order_category = ContextMotorDepot.order_categorySet.Where(t => t.Name.ToUpper() == "ЗАВОДСКОЙ").FirstOrDefault();
            newOrder.md_order_priority = ContextMotorDepot.order_prioritySet.Where(t => t.Name.ToUpper() == "СРЕДНИЙ").FirstOrDefault();
            newOrder.md_order_state = ContextMotorDepot.order_stateSet.Where(st => st.Id == (int)OrderStates.Design).FirstOrDefault();
            newOrder.UserCreator = UserBaseCurrent.Instance.Name;
            if (!UserBaseCurrent.Instance.Admin)
                newOrder.md_enterprise = MdUser.Instance.Enterprise;
        }

        internal static void SetLastVehicleObject(order orderVehicle)
        {
            int orders = ContextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == orderVehicle.md_waybill.Id && ord.Id != orderVehicle.Id).OrderBy(ord => ord.DateStartWork).Count();
            if (orders == 0) return;
            var lastOrders = ContextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == orderVehicle.md_waybill.Id && ord.Id != orderVehicle.Id).OrderBy(ord => ord.DateStartWork).ToArray();
            if (lastOrders != null)
            {
                order ord = (order)lastOrders[lastOrders.Length -1];
                var lastOrdersT = (from ordT in ContextMotorDepot.ordertSet where ordT.md_order.Id == ord.Id orderby ordT.Id ascending  select ordT.md_object).ToArray();

                if (lastOrdersT != null && lastOrdersT.Length >0)
                {
                    orderVehicle.md_objectStart = (md_object)lastOrdersT[lastOrdersT.Length - 1];
                }
            }
        }

        internal static void StartOrder(order orderForStart)
        {
            if (!TestStartOrderConditions(orderForStart)) return;

                orderForStart.DateStartWork = DateTime.Now;
                OrderProvider.UpdateDateStart(orderForStart);
                ChangeOrderVehicleState(orderForStart, (int) VehicleStates.OrderExecuting);
                SetOrderState(orderForStart, OrderStates.Execute);
                StartOrderTracking(orderForStart);
        }

        private static bool TestStartOrderConditions(order orderForStart)
        {
            if (orderForStart.DateStartWork != null)
            {
                XtraMessageBox.Show(string.Format("Заказ-наряд № {0} уже стартовал!", orderForStart.Number),
                                    MDController.MesCaption);
                return false;
            }
            if (orderForStart.md_order_state.Id != (int)OrderStates.Design)
            {
                XtraMessageBox.Show(string.Format("Заказ-наряд № {0} не имеет статус ОФОРМЛЕНИЕ!", orderForStart.Number),
                                    MDController.MesCaption);
                return false;
            }
            if (orderForStart.md_objectStart == null)
            {
                XtraMessageBox.Show(string.Format("Укажите начальную точку заказ-наряда № {0}!", orderForStart.Number),
                                    MDController.MesCaption);
                return false;
            }
            if (orderForStart.md_ordert.Count == 0)
            {
                XtraMessageBox.Show(string.Format("Заказ-наряд № {0} не имеет содержимого!", orderForStart.Number),
                                    MDController.MesCaption);
                return false;
            }
            if (orderForStart.md_waybill == null)
            {
                XtraMessageBox.Show(string.Format("Заказ-наряд № {0} не имеет ссылки на путевой лист!", orderForStart.Number),
                                    MDController.MesCaption);
                return false;
            }
            return true;
        }

        internal static void StartOrderTracking(order orderForTrack)
        {
            OrderObserver observer = new OrderObserver(orderForTrack);
            observer.Start();
            observer.UpdateOrder += UpdateOrderFromOnline;
        }

        internal static void UpdateOrderFromOnline(order orderFromOnline)
        {
            order ord = MDController.Instance.ContextMotorDepot.orderSet.Where(od =>od.Id == orderFromOnline.Id).FirstOrDefault() ;
            order_state ordState = MDController.Instance.ContextMotorDepot.order_stateSet.Where(odSt => odSt.Id == orderFromOnline.md_order_state.Id).FirstOrDefault();
            if (ord != null && ordState!=null)
            {
                ord.md_order_state = ordState;
                if (UpdateOrderState != null) UpdateOrderState();
            }
            
        }

        internal static void CloseOrder(order orderForClose,bool isMessageShow)
        {
            //Закрытие заказа-наряда должно происходить  вручную диспетчером
            if (orderForClose.IsClose)
            {
                orderForClose.DateExecute = DateTime.Now;
                ChangeOrderVehicleState(orderForClose, (int)VehicleStates.OrderWaitng);
                SetOrderState(orderForClose, OrderStates.Close);
                StopOrderTracking(orderForClose);
                RecalcOrder(orderForClose, isMessageShow);
            }
            else
            {
                orderForClose.DateExecute = null;
                ChangeOrderVehicleState(orderForClose, (int)VehicleStates.OrderExecuting);
                SetOrderState(orderForClose, OrderStates.Finish);
            }
            OrderProvider.UpdateOrder(orderForClose);  
            
            //MDController.Instance.MotorDepotEntitiesSaveChanges(orderForClose); ;
        }

        internal static void ChangeOrderVehicleState(order order, int newVehicleState)
        {
            if (order.md_waybill != null && order.md_waybill.vehicle != null)
            {
                order.md_waybill.vehicle.vehicle_state = ContextMotorDepot.vehicle_stateSet.Where(vs => vs.Id == newVehicleState).FirstOrDefault();
                VehicleProvider.SaveVehicleState(order.md_waybill.vehicle);
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, order.md_waybill.vehicle);
                //MDController.Instance.MotorDepotEntitiesSaveChanges(order.md_waybill.vehicle);
            }
        }

        internal static bool CopyOrder(order orderForCopy)
        {
            if (!MdUser.Instance.IsObjectEditableForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return false;
            if (XtraMessageBox.Show(string.Format("Подтверждаете копирование заказ-наряд № {0}?", orderForCopy.Number),
                                MDController.MesCaption, MessageBoxButtons.YesNo) != DialogResult.Yes) return false; 
            try
            {
                order newOrder = new order();
                newOrder.DateCreate = DateTime.Now;
                newOrder.DateInit = DateTime.Now;
                newOrder.Number = OrderController.GetNextNumber();
                newOrder.Remark = orderForCopy.Remark;
                newOrder.md_objectCustomer = orderForCopy.md_objectCustomer;
                newOrder.md_objectStart = orderForCopy.md_objectStart;
                newOrder.TimeDelay = orderForCopy.TimeDelay;
                newOrder.md_order_priority = orderForCopy.md_order_priority;
                newOrder.md_transportation_types = orderForCopy.md_transportation_types;
                newOrder.md_order_category = orderForCopy.md_order_category;
                newOrder.UserCreator = UserBaseCurrent.Instance.Name;
                newOrder.md_enterprise = MdUser.Instance.Enterprise;
                newOrder.md_order_state = ContextMotorDepot.order_stateSet.Where(st => st.Id == (int)OrderStates.Design).FirstOrDefault();
                //ContextMotorDepot.SaveChanges();
                OrderProvider.InsertOrder(newOrder);  
                List<ordert> orderForCopyT =ContextMotorDepot.ordertSet.Where(ordt=>ordt.md_order.Id == orderForCopy.Id ).ToList();
                foreach (ordert orderForCopyTItem in orderForCopyT)
                {
                    ordert orderTnew = new ordert();
                    orderTnew.md_order = newOrder;
                    orderTnew.md_object = orderForCopyTItem.md_object;
                    orderTnew.DistanceToObject = orderForCopyTItem.DistanceToObject;
                    orderTnew.TimeToObject = orderForCopyTItem.TimeToObject;

                    orderTnew.md_cargoLoad = orderForCopyTItem.md_cargoLoad;
                    orderTnew.QtyLoading = orderForCopyTItem.QtyLoading;
                    orderTnew.TimeLoading = orderForCopyTItem.TimeLoading;

                    orderTnew.md_cargoUnLoad = orderForCopyTItem.md_cargoUnLoad;
                    orderTnew.QtyUnLoading = orderForCopyTItem.QtyUnLoading;
                    orderTnew.TimeUnLoading = orderForCopyTItem.TimeUnLoading;

                    orderTnew.md_alarming_types = orderForCopyTItem.md_alarming_types;
                    orderTnew.Remark = orderForCopyTItem.Remark;
                    //ContextMotorDepot.SaveChanges();
                    OrderTProvider.Save(orderTnew);  
                }
                UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("Копирование с заказ - наряда №{0}", orderForCopy.Number), newOrder.Id);
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("Не удалось скопировать заказ-наряд №{0}{1}{2}", orderForCopy.Number,Environment.NewLine,ex.Message),
                                MDController.MesCaption); 
                return false;
            }

        }

        internal static bool RefuseFromOrder(order orderForRefuse)
        {
            if (XtraMessageBox.Show(string.Format("Подтверждаете отказ от заказ-наряда № {0}?", orderForRefuse.Number),
                                MDController.MesCaption, MessageBoxButtons.YesNo) != DialogResult.Yes) return false;
            try
            {
                orderForRefuse.DateRefuse = DateTime.Now;
                orderForRefuse.IsClose = true;
                StopOrderTracking(orderForRefuse);
                ChangeOrderVehicleState(orderForRefuse, (int)VehicleStates.OrderWaitng);
                SetOrderState(orderForRefuse, OrderStates.Refuse);
                OrderProvider.UpdateOrder(orderForRefuse); 
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("Не удалось отказаться от заказ-наряда №{0}{1}{2}", orderForRefuse.Number, Environment.NewLine, ex.Message),
                                MDController.MesCaption);
                return false;
            }

        }

        internal static order GetVehicleOrder(vehicle vh, MotorDepotEntities contextMotorDepot)
        {
            if (contextMotorDepot == null) return null;
            var ord = contextMotorDepot.orderSet.Include("md_waybill").Where(od => od.md_waybill.vehicle.id == vh.id && od.md_waybill.IsClose == false && od.IsClose == false && od.md_order_state.Id != (int)OrderStates.Delete).OrderByDescending(od => od.DateStartWork).FirstOrDefault();
            if (ord != null)
            {
                contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, (order)ord);
                return (order)ord;

            }
            else
            {
                return null;
            }
        }

        internal static void StopOrderTracking(order orderForStop)
        {
            var orderRecords = ContextMotorDepot.ordertSet.Where(or => or.md_order.Id == orderForStop.Id).ToList();
            if (orderRecords == null) return;
            foreach (var orderRecord in orderRecords)
            {
                OrderTController.StopOrderRecordTracking(orderRecord);
            }
        }

        internal static void SetBlock(order orderForBlock, OrderForm view)
        {

            if (orderForBlock != null)
            {
                if (orderForBlock.IsDocBlocked)
                {
                    view.SetReadOnly();
                }
                else
                {
                    view.IsReadOnly = false;
                    orderForBlock.BlockUserId = UserBaseCurrent.Instance.Id;
                    orderForBlock.BlockDate = DateTime.Now;
                    OrderProvider.UpdateBlockState(orderForBlock);
                }
            }
        }
    }
}
