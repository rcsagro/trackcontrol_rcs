﻿using System;
using System.Linq;
using TrackControl.General;
using MotorDepot.MonitoringGps; 

namespace MotorDepot
{
    /// <summary>
    /// управление данными содержимого заказ - наряда
    /// </summary>
    public static class OrderTController
    {
        internal static MotorDepotEntities ContextMotorDepot;

        internal static ordert Save(order orderHeader, ordert orderTableRecord)
        {
            if (orderTableRecord == null || orderTableRecord.md_object == null || orderHeader == null) return orderTableRecord;
                orderTableRecord.md_order = orderHeader;
               return OrderTProvider.Save(orderTableRecord);  
                //MDController.Instance.MotorDepotEntitiesSaveChanges(orderTableRecord);
        }

        internal static void SetDistTimeBetweenObjects(ordert orderTableRecord, md_object objectFrom)
        {
            if (orderTableRecord == null || objectFrom == null || orderTableRecord.md_object == null) return; 
            var varRouteEntity = ContextMotorDepot.routeSet.Where(t => t.md_object_from.Id == objectFrom.Id && t.md_object_to.Id == orderTableRecord.md_object.Id).FirstOrDefault();
            if (varRouteEntity != null)
            {
                route routeEntity = (route)varRouteEntity;
                orderTableRecord.DistanceToObject = routeEntity.Distance;
                orderTableRecord.TimeToObject = routeEntity.Time;
            }
            else
            {
                orderTableRecord.DistanceToObject = 0;
                orderTableRecord.TimeToObject = 0;
            }

        }

        internal static void SetTimeDelay(ordert orderTableRecord)
        {
            if (orderTableRecord == null || orderTableRecord.md_object == null) return;
            var varObject = ContextMotorDepot.md_objectSet.Where(t => t.Id == orderTableRecord.md_object.Id).FirstOrDefault();
            if (varObject != null)
            {
                orderTableRecord.TimeDelay = (varObject as md_object).DelayTime;
            }
            else
            {
                orderTableRecord.TimeDelay = 0;
            }
        }

        internal static void SetCargoLoadTime(ordert orderTableRecord)
        {
            if (orderTableRecord == null || orderTableRecord.md_object == null || orderTableRecord.md_cargoLoad == null) return; 
            var varLoadingTimeEntity = ContextMotorDepot.loading_timeSet.Where(t => t.md_object.Id == orderTableRecord.md_object.Id && t.md_cargo.Id == orderTableRecord.md_cargoLoad.Id).FirstOrDefault();
            if (varLoadingTimeEntity != null)
            {
                orderTableRecord.TimeLoading = (varLoadingTimeEntity as loading_time).TimeLoading;
            }
            else
            {
                orderTableRecord.TimeLoading = 0;
            }
        }

        internal static void SetCargoUnLoadTime(ordert orderTableRecord)
        {
            if (orderTableRecord == null || orderTableRecord.md_object == null || orderTableRecord.md_cargoUnLoad == null) return;
            var varLoadingTimeEntity = ContextMotorDepot.loading_timeSet.Where(t => t.md_object.Id == orderTableRecord.md_object.Id && t.md_cargo.Id == orderTableRecord.md_cargoUnLoad.Id).FirstOrDefault();
            if (varLoadingTimeEntity != null)
            {
                orderTableRecord.TimeUnLoading = (varLoadingTimeEntity as loading_time).TimeUnLoading;
            }
            else
            {
                orderTableRecord.TimeUnLoading = 0;
            }

        }

        internal static bool Delete(ordert orderTableRecord)
        {
            if (orderTableRecord == null) return false;
            try
            {
               // если запись наряда учавствует в слежении, завершить слежение 
                StopOrderRecordTracking(orderTableRecord);
                UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("Удаление {0}", orderTableRecord.md_object.Name), orderTableRecord.md_order.Id);
                //ContextMotorDepot.DeleteObject(orderTableRecord);
                OrderTProvider.Delete(orderTableRecord);  
                //MDController.Instance.MotorDepotEntitiesSaveChanges(orderTableRecord);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static void StopOrderRecordTracking(ordert orderTableRecord)
        {
            if (!orderTableRecord.IsClose  && OnlineController.Instance.IsStarted)
            {
                foreach (OrderRecordPointChecker recordChecker in OnlineController.Instance.RecordCheckers)
                {
                    if (recordChecker.OrderRecord.Id == orderTableRecord.Id)
                    {
                        recordChecker.StopCheking(false);
                        break;
                    }
                }
            }
        }

 
    }
}
