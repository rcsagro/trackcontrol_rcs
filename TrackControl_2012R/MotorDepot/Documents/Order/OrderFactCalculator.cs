﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification;
using TrackControl.Online;
using TrackControl.Vehicles;

namespace MotorDepot.MonitoringGps
{
    public class OrderFactCalculator:IDisposable 
    {
        private order _order;
        private List<OrderRecordPointChecker> _recordCheckers;
        private static MotorDepotEntities _сontextMotorDepot;
        private Vehicle _modelVh;
        private OnlineVehicle _modelVhOnline;
        private int _indexChecker;

        public OrderFactCalculator(order order)
        {
            _order = order;
            _сontextMotorDepot = MDController.Instance.ContextMotorDepot;   
            if (_order.md_waybill != null && _order.md_waybill.vehicle != null)
            {
                _modelVh = EntityFrameworkModelConverter.ConvertVehicleEfToModel(_order.md_waybill.vehicle);
                _recordCheckers = new List<OrderRecordPointChecker>();
            }
            else
            {
                XtraMessageBox.Show(String.Format("Заказ - наряд № {0} не имеет ссылки на путевой лист!", _order.Number), "Перерасчет невозможен!");
            }
        }

        public void Start()
        {
            if (_order.DateStartWork == null) return;
            _recordCheckers.Clear();
            _modelVhOnline = new OnlineVehicle(_modelVh);
            if (CreateRecordPointCheckersList())
            {
                _indexChecker = 0;
                ScanDataGps();
            }
            else
            {
                XtraMessageBox.Show(String.Format("Отсутствие записей для слежения в заказ-наряде № {0}!", _order.Number), "Контроль заказа невозможен!");
            }
        }

        bool CreateRecordPointCheckersList()
        {
            if (_сontextMotorDepot.ordertSet.Where(ordT => ordT.md_order.Id == _order.Id).Count() == 0) return false;
            List<ordert> orderRecords = _сontextMotorDepot.ordertSet.Where(ordT => ordT.md_order.Id == _order.Id).ToList();
            bool first = true;
            foreach (ordert orderRecord in orderRecords)
            {
                if (orderRecord.md_object != null && orderRecord.md_object.zones != null)
                {

                    if (first)
                    {
                        first = false;
                        CreatePointChecker(orderRecord, ZoneEventTypes.EntryToZone,
                            _order.TimeDelay == null ? 0 : _order.TimeDelay.Value.TotalHours, _order.DateStartWork);
                    }
                    else
                    {
                        CreatePointChecker(orderRecord, ZoneEventTypes.EntryToZone, 0, null);
                    }
                    CreatePointChecker(orderRecord, ZoneEventTypes.ExitFromZone, (double)orderRecord.TimeDelay, null);
                }

            }
            if (_recordCheckers.Count == 0)
                return false;
            else
                return true;
        }

        private void CreatePointChecker(ordert orderRecord, ZoneEventTypes zoneEventType, double delayHours, DateTime? startTime)
        {
            OrderRecordPointChecker pointCheck = new OrderRecordPointChecker(orderRecord, zoneEventType, _modelVhOnline, delayHours, startTime,false);
            _recordCheckers.Add(pointCheck);
        }

        void ScanDataGps()
        {
            IList<GpsData> gpsData;
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                GpsDataProvider gpsProvider = new GpsDataProvider(driverDb);
                DateTime startFact = (DateTime)_order.DateStartWork;
                DateTime endFact = GetForecastDataEnd();// new DateTime(startFact.Year, startFact.Month, startFact.Day, 23, 59, 59);
                gpsData = gpsProvider.GetValidDataForPeriod(_modelVh, startFact, endFact);
            }
                foreach (GpsData data in gpsData)
                {

                    _recordCheckers[_indexChecker].CheckDataGps(data);
                    if (_recordCheckers[_indexChecker].IsCheckerFinish)
                    {
                        _indexChecker += 1;
                        if (_indexChecker >= _recordCheckers.Count) break;
                        _recordCheckers[_indexChecker].PrevGpsData = data;
                        //Debug.Print(string.Format("{0} LogID {1} Id {2} Zone {3}", _indexChecker, data.LogID, data.Id, _recordCheckers[_indexChecker].OrderRecord.md_object.zones.Name));
                    }

                }
             gpsData.Clear();
        }

        internal  DateTime GetForecastDataEnd()
        {
            // на межгороде не считает пробег и время на движение, хотя время в зоне заполнено
            DateTime end = ((DateTime)_order.DateStartWork) ;
            if (_order.TimeDelay != null) end = end.AddHours(_order.TimeDelay.Value.TotalHours);
            var orderRecords = _сontextMotorDepot.ordertSet.Where(odr => odr.md_order.Id == _order.Id).OrderBy(odr=>odr.Id).ToList();
            if (orderRecords != null)
            {
                foreach (var orderRecord in orderRecords)
                {
                    if (orderRecord.TimeExit != null)
                    {
                        end = (DateTime)orderRecord.TimeExit;
                    }
                    else
                    {
                        end = end.AddHours((double)orderRecord.TimeToObject);
                        end = end.AddHours((double)orderRecord.TimeLoading);
                        end = end.AddHours((double)orderRecord.TimeUnLoading);
                    }
                }
            }

            end = end.AddHours(3); 
            return end;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_recordCheckers != null) _recordCheckers.Clear();
        }

        #endregion
    }
}
