﻿using System;
using System.Diagnostics;
using System.Threading;
using TrackControl.General;
using Timer = System.Threading.Timer;

namespace MotorDepot
{
    public class OrderTestGpsEvents
    {

        bool _isStarted;
        Timer _timer;
        int _latStart = 30578300;
        int _lonStart = 17169950;
        private int _latDelta = 50;
        private int _lonDelta = 10;
        private int _logId = 1;
        public static DateTime OnlineTime = DateTime.Now;

        int _latStart1 = 30578300;
        int _lonStart1 = 17169950;
        private int _latDelta1 = 10;
        private int _lonDelta1 = 50;

        //azov
        //int Mobilel_Id = 169;
        //int Mobilel_Id1 = 172;

        //agrofront
        int Mobilel_Id = 118;
        int Mobilel_Id1 = 114;

        public OrderTestGpsEvents()
        {
            _timer = new Timer(InsertOnlineData, null, Timeout.Infinite, Timeout.Infinite);
            ClearOnlineMobitel();
        }


        public void Start()
        {
            if (_isStarted)
                return;

            _isStarted = true;
            ThreadPool.QueueUserWorkItem(InsertOnlineData);
        }

        public void Stop()
        {
            _isStarted = false;
        }



        private void ClearOnlineMobitel()
        {
            using (ConnectMySQL cn = new ConnectMySQL())
            {
                string sql = string.Format("Delete FROM Online Where Mobitel_ID = {0}", Mobilel_Id);
                cn.ExecuteNonQueryCommand(sql);
                sql = string.Format("Delete FROM Online Where Mobitel_ID = {0}", Mobilel_Id1);
                cn.ExecuteNonQueryCommand(sql);
            }

        }

        private int TestConvertToTimestamp(DateTime value)
        {
            //TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            //return (int)span.TotalSeconds;

            TimeSpan dt = new DateTime( DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond ) - DateTime.UtcNow;
            DateTime origin = new DateTime( 1970, 1, 1, 0, 0, 0, 0 ) + dt;
            TimeSpan diff = value - origin;

            return Convert.ToInt32( diff.TotalSeconds );
        }

        private void InsertOnlineData(object obj)
        {
            using (ConnectMySQL cn = new ConnectMySQL())
            {
                int speed = 40;
                if (_logId > 30 && _logId < 35) speed = 0;
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed,LogID) 
                Values ({0},{1},{2},{3},{4},{5},{6},{7})", Mobilel_Id, _latStart, _lonStart, 0, TestConvertToTimestamp(OnlineTime), 1, speed, _logId);
                cn.ExecuteNonQueryCommand(sql);
                _latStart += _latDelta;
                _lonStart += _lonDelta;
                sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed,LogID) 
                Values ({0},{1},{2},{3},{4},{5},{6},{7})", Mobilel_Id1, _latStart1, _lonStart1, 0, TestConvertToTimestamp(OnlineTime), 1, speed, _logId);
                cn.ExecuteNonQueryCommand(sql);
                _latStart1 += _latDelta1;
                _lonStart1 += _lonDelta1;
                //Debug.Print("Insert {0} ", OnlineTime); 
                OnlineTime=OnlineTime.AddMinutes(2);
                _logId++;
            }
            _timer.Change(1000, Timeout.Infinite);
        }




    }
}
