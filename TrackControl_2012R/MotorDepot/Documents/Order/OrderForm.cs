using System;
using System.Windows.Forms;
using System.Collections.Generic;  
using DevExpress.XtraEditors;
using MotorDepot.AccessObjects;
using TrackControl.General;
using System.Linq;

namespace MotorDepot
{
    public partial class OrderForm : DevExpress.XtraEditors.XtraForm,IAccessFormControls
    {
        public event Action<order> SaveOrder;
        public event VoidHandler OrderViewClosed;
        bool _IsReadOnly;

        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; }
        }


        public OrderForm()
        {
            InitializeComponent();
            InitControls();
            SetAccessRules();
        }

        void InitControls()
        {
            bbiRecalc.Glyph = Shared.Calculator;
        }

        void SetEnabledStartButton(order orderHeader)
        {
            if (orderHeader.DateStartWork != null)
            {
                bbiStart.Enabled = false;
                bbiStart.Caption = "����� �����������";
            }
            else if (orderHeader.md_waybill != null)
            {
                bbiStart.Enabled = true;
            }
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (_IsReadOnly) return;
            if (XtraMessageBox.Show("������������� ���������� ���������?", MDController.MesCaption, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                order ordHeader = (order)pgOrder.SelectedObject;
                if (SaveOrder != null && pgOrder.SelectedObject != null) SaveOrder(ordHeader);
                bbiSave.Enabled = false;
                RefreshOrder();
                SetEnabledStartButton(pgOrder.SelectedObject as order);
            }
        }

        void RefreshOrder()
        {
            pgOrder.Refresh();
            abgvOrderT.RefreshData();
        }

        private void OrderForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ResetBlock();
            if (OrderViewClosed != null) OrderViewClosed();
        }

        private void pgOrder_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {

            order orderHeader = (order)pgOrder.SelectedObject;
            //string logMessage = string.Format("��������� {0} � {1}", e.Row.Properties.Caption, e.Value);
            switch (e.Row.Name)
            {
                case "erWayBill":
                    {
                        OrderController.SetLastVehicleObject(orderHeader); 
                        break;
                    }
                case "erObjectStart":
                    {
                        ordert orderTableRecordFirst = abgvOrderT.GetRow(0) as ordert;
                        if (abgvOrderT.RowCount > 0) SetDistTime(orderHeader, 0, orderTableRecordFirst); 
                        break;
                    }
                case "erIsClose":
                    {
                        OrderController.CloseOrder(orderHeader,true);
                        break;
                    }
            }

            //UserLog.InsertLog(UserLogTypes.MD_ORDER, logMessage, orderHeader.Id);
            bbiSave.Enabled = true;
        }

        private void abgvOrderT_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            SaveOrderTableRecord(e.RowHandle, e.Column.FieldName.ToString());
        }

        private void SaveOrderTableRecord(int rowHandle,string columnFieldName )
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader != null)
            {
                ordert orderTableRecord = abgvOrderT.GetRow(rowHandle) as ordert;
                switch (columnFieldName)
                {
                    case "md_object":
                        {
                            SetDistTime(orderHeader, rowHandle, orderTableRecord);
                            OrderTController.SetTimeDelay(orderTableRecord); 
                            break;
                        }
                    case "md_cargoLoad":
                        {
                            OrderTController.SetCargoLoadTime(orderTableRecord); 
                            break;
                        }
                    case "md_cargoUnLoad":
                        {
                            OrderTController.SetCargoUnLoadTime(orderTableRecord); 
                            break;
                        }
                }
                bool addNew = orderTableRecord.Id == 0 ? true: false;
                ordert orderTableRecordNew = OrderTController.Save(orderHeader, orderTableRecord);
                if (addNew) abgvOrderT.SetFocusedValue(orderTableRecordNew.md_object); 
               //OrderController.SetContentDataSource(orderHeader, this);

            }
        }

        private void SetDistTime(order orderHeader, int rowHandle, ordert orderTableRecord)
        {
            if (orderHeader.md_ordert.Count == 0 || rowHandle==0)
            {
                if (orderHeader.md_objectStart == null)
                {
                    XtraMessageBox.Show("�������� ��������� �����!", "������ �� ���������");
                    return;
                }
                OrderTController.SetDistTimeBetweenObjects(orderTableRecord, orderHeader.md_objectStart);
            }
            else
            {
                int rowHandlePrev = rowHandle;
                if (rowHandle < 0)
                    rowHandlePrev = abgvOrderT.DataRowCount - 1;
                ordert orderTableRecordPrev = abgvOrderT.GetRow(rowHandlePrev) as ordert;
                if (orderTableRecordPrev != null)
                    OrderTController.SetDistTimeBetweenObjects(orderTableRecord, orderTableRecordPrev.md_object);
            }
        }

        private void abgvOrderT_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (abgvOrderT.GetRowCellValue(e.RowHandle, bcolObject) == null || abgvOrderT.GetRowCellValue(e.RowHandle, bcolObject).ToString().Length == 0)
            {
                abgvOrderT.SetColumnError(bcolObject, "������� ����� ����������!");
                e.Valid = false;
            }
            else
            {
                e.Valid = true;
            }
        }

        private void gcOrderT_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:
                    {
                        if (XtraMessageBox.Show("������������� �������� ������?",
                        "��������",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            ordert orderTableRecord = abgvOrderT.GetRow(abgvOrderT.FocusedRowHandle) as ordert;
                            if (orderTableRecord == null)
                                e.Handled = true;
                            else
                                e.Handled = !OrderTController.Delete(orderTableRecord);
                        }
                        else
                        {
                            e.Handled = true;
                        }
                        break;
                    }
            }
        }

        #region Buttons
        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshOrder();
        }

        private void bbiRefuse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (OrderController.RefuseFromOrder(orderHeader))
            {
                RefreshOrder();
            }
        }

        private void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader != null && orderHeader.md_waybill != null)
            {
                OrderController.StartOrder(orderHeader);
                RefreshOrder();
                SetEnabledStartButton(orderHeader);
            }
        }

        private void bbiLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader != null)
            {
                UserLog.ViewLog(UserLogTypes.MD_ORDER, orderHeader.Id);
            }

        }

        private void bbiRecalc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (pgOrder.SelectedObject != null)
            {
                OrderController.RecalcOrder(pgOrder.SelectedObject as order, true);
                RefreshOrder();
            }
        } 
        #endregion

        private void OrderForm_Load(object sender, EventArgs e)
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader != null)
            {
                SetEnabledStartButton(orderHeader);
                UserLog.InsertLog(UserLogTypes.MD_ORDER, "�������� ����� - ������", orderHeader.Id);
            }
        }

        #region IAccessFormControls
        public void SetAccessRules()
        {
            if (!MdUser.Instance.IsObjectEditableForRole(typeof(order).Name, (int)UserAccessObjectsTypes.DocumentsMd))
            {
                pgOrder.OptionsBehavior.Editable  = false;
                abgvOrderT.OptionsBehavior.Editable = false;
                bbiSave.Enabled = false;
                bbiStart.Enabled = false;
                bbiRefuse.Enabled = false;
                bbiRecalc.Enabled = false; 
            }
        } 
        #endregion

        private void abgvOrderT_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader != null && orderHeader.md_order_state.Id != (int)OrderStates.Design && orderHeader.md_order_state.Id != (int)OrderStates.WaitingFor)
            {
                if (XtraMessageBox.Show("������������� ��������� ������?", MDController.MesCaption, MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    e.Valid = false;
                }
                else
                {
                    e.Valid = true;
                }
            }
        }

        public void SetReadOnly()
        {
            pgOrder.OptionsBehavior.Editable = false;
            abgvOrderT.OptionsBehavior.Editable = false;
            bbiSave.Enabled = false;
            bbiRecalc.Enabled = false;
            bbiStart.Enabled = false;
            bbiRefuse.Enabled = false;
            _IsReadOnly = true;
        }

        void ResetBlock()
        {
            order orderHeader = (order)pgOrder.SelectedObject;
            if (orderHeader.BlockUserId == UserBaseCurrent.Instance.Id)
            {
                orderHeader.BlockUserId = 0;
                orderHeader.BlockDate = null;
                OrderProvider.UpdateBlockState(orderHeader);
            }
        }

        private void abgvOrderT_KeyDown(object sender, KeyEventArgs e)
        {

        }

    }
}