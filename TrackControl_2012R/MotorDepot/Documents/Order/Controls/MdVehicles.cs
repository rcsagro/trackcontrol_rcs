using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using MotorDepot.AccessObjects;
using TrackControl.General;
using TrackControl.Online;
using OnlineController = MotorDepot.MonitoringGps.OnlineController;
using DevExpress.XtraTreeList.Columns;

namespace MotorDepot
{
    public partial class MdVehicles : XtraUserControl,IDisposable
  {
    bool _isLoaded;
    string _condition; // ������� ������
    public event VoidHandler SelecteVehicleOnMap;
    private MDController _formController;
    private static MotorDepotEntities _contextMotorDepot;

    public MdVehicles(MDController formController)
    {
        //_contextMotorDepot = contextMotorDepot;
        _formController = formController;
        _contextMotorDepot = new MotorDepotEntities(MDController.ModelConString);
        MDController.InitEntyties(_contextMotorDepot);
        //_contextMotorDepot = MDController.Instance.ContextMotorDepot;   
        InitializeComponent();
        Init();
    }

    #region --   ����� ���������� ITreeModel<ReportVehicle>   --

    //public ReportVehicle Current
    //{
    //  get
    //  {
    //    ReportVehicle rv = null;
    //    vehicle vehicle = _tree.GetDataRecordByNode(_tree.FocusedNode) as vehicle;
    //    if (null != vehicle)
    //      rv = vehicle.Tag as ReportVehicle;

    //    return rv;
    //  }
    //}

    public List<vehicle> Checked
    {
        get
        {
            CheckedVehiclesOperation operation = new CheckedVehiclesOperation();
            _tree.NodesIterator.DoOperation(operation);
            return operation.Vehicles;
        }
    }

    public List<vehicle> GetAll
    {
        get
        {
            GetAllVehiclesOperation operation = new GetAllVehiclesOperation();
            _tree.NodesIterator.DoOperation(operation);
            return operation.Vehicles;
        }
    }

    //public IList<IEntity> UnChecked
    //{
    //    get
    //    {
    //         UnCheckedVehiclesOperation operation = new UnCheckedVehiclesOperation();
    //        _tree.NodesIterator.DoOperation(operation);
    //        return operation.VehiclesEntity;
    //    }
    //}



    #endregion

    public void SelectByModitelId(int id)
    {
        _tree.ExpandAll();
        _tree.NodesIterator.DoOperation(new SelectCertainVehicleOperation(id));
    }

    public void RefreshTree()
    {
        _contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, _contextMotorDepot.vehicleSet);
        _isLoaded = false;
        _tree.RefreshDataSource();
    }


    void this_Load(object sender, EventArgs e)
    {
        _tree.DataSource = new object();
        _tree.ExpandAll();
        setCondition(String.Empty);
        CheckAllNodes operation = new CheckAllNodes();
        _tree.NodesIterator.DoOperation(operation);
    }

    void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
    {
        if (!_isLoaded)
        {
            GetTeamListForWbUnclosed(e);
            _isLoaded = true;
        }
        else
        {
            team group = e.Node as team;
            if (null != group)
            {
                GetVehicleListForWbUnclosed(group.id, e);
 }
            else 
            {
                e.Children = new object[] { };
            }
        }
    }

    private void GetTeamListForWbUnclosed(VirtualTreeGetChildNodesInfo e)
    {
        List<team> teamVh = new List<team>();
        IQueryable<waybill> wbToday = GetWbUnclosed();
        if (wbToday == null) return;
        foreach (waybill  wb in wbToday)
        {
            if (wb.vehicle != null && wb.vehicle.team != null)
            {
                if (!teamVh.Contains(wb.vehicle.team)) teamVh.Add(wb.vehicle.team);
            }
        }
        e.Children = teamVh;
    }

    private IQueryable<waybill> GetWbUnclosed()
    {
        //IQueryable<waybill> wbs = from wb in _contextMotorDepot.waybillSet
        //                          where wb.IsClose == false
        //                          select wb;
        var wbs = _contextMotorDepot.waybillSet.Include("md_order").Where(wb=>wb.IsClose == false);  
        //foreach (waybill wb in wbs)
        //{
        //    _contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, wb);
        //    _contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, wb.md_order);
        //} 
        if (UserBaseCurrent.Instance.Admin)
            return wbs;
        else
            return wbs.Where(wb => wb.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
    }

    private void GetVehicleListForWbUnclosed(int teamId, VirtualTreeGetChildNodesInfo e)
    {
        IQueryable<waybill> wbToday = GetWbUnclosed();
        List<vehicle> Vh = new List<vehicle>();
        foreach (waybill wb in wbToday)
        {
            if (wb.vehicle != null && wb.vehicle.team != null && wb.vehicle.team.id == teamId)
            {
                if (!Vh.Contains(wb.vehicle)) Vh.Add(wb.vehicle);
            }
        }
        e.Children = Vh;
    }

    void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
{
    if (e.Node is team)
    {
        team group = (team)e.Node;
        if (e.Column == _titleCol)
            e.CellData = group.Name;
    }
    else if (e.Node is vehicle)
    {
        vehicle vh = (vehicle)e.Node;
        if (e.Column == _titleCol)
            e.CellData = vh.NumberPlate;
        else if (e.Column == _descriptionCol)
            e.CellData = String.Format("{0} {1}", vh.MakeCar, vh.CarModel);
        else if (e.Column == stateVehCol)
            if (vh.vehicle_state==null)
                e.CellData =0;
            else
                e.CellData = vh.vehicle_state.Id;
        else if (e.Column == stateOrderCol)
        {
            if (vh.StateId == (int)VehicleStates.OrderWaitng)
            {
                e.CellData = 0;
            }
            else
            {
                var od = OrderController.GetVehicleOrder(vh,_contextMotorDepot);
                if (od == null)
                {
                    e.CellData = 0;
                }
                else
                {
                    if (od is order)
                        e.CellData = od.md_order_state.Id;
                }
            }
        }
        else if (e.Column == orderCol)
        {
            if (vh.StateId == (int)VehicleStates.OrderWaitng)
            {
                e.CellData = "";
            }
            else
            {
                var od = OrderController.GetVehicleOrder(vh,_contextMotorDepot);
                if (od != null && od is order)
                    e.CellData = od.Number;
                else
                    e.CellData = "";
            }
        }
    }
}


    void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
    {
        TreeViewService.FixNodeState(e);
    }

    void tree_AfterCheckNode(object sender, NodeEventArgs e)
    {
        //TreeViewService.SetCheckedChildNodes(e.Node);
        //TreeViewService.SetCheckedParentNodes(e.Node);
        //_tree.FocusedNode = e.Node;
        //vehicle vh = _tree.GetDataRecordByNode(e.Node) as vehicle;
        //if (null != vehicle)
        //{
            //ReportVehicle report = (ReportVehicle)vehicle.Tag;
            //report.Selected = e.Node.Checked;
            //vehicle.Checked = e.Node.Checked;
        //}
        //if (VehicleChecked != null) VehicleChecked();
    }

    private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
    {
        //Rectangle rect = e.SelectRect;
        //rect.X += (rect.Width - 16) / 2;
        //rect.Y += (rect.Height - 16) / 2;
        //rect.Width = 16;
        //rect.Height = 16;

        //object obj = _tree.GetDataRecordByNode(e.Node);
        //if (obj is vehicle)
        //{
        //    vehicle vh = (vehicle)obj;
        //    e.Graphics.DrawImage(Shared.Truck, rect);
        //}
        //else if (obj is VehiclesGroup)
        //{
        //    team group = (VehiclesGroup)obj;
        //    e.Graphics.DrawImage(group.Style.Icon, rect);
        //}

        //e.Handled = true;
    }

    void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
    {
        object obj = _tree.GetDataRecordByNode(e.Node);
        if (obj is team)
        {
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
            {
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
            }
        }


    }

    void tree_AfterFocusNode(object sender, NodeEventArgs e)
    {
            vehicle vh = _tree.GetDataRecordByNode(e.Node) as vehicle;
            if (null != vh)
            {
                foreach (OnlineVehicle ov in OnlineController.Instance.OnlineVehicles)
                {
                    if (vh.id == ov.Id)
                    {
                        ov.IsSelected = true;

                    }
                    else
                    {
                        ov.IsSelected = false;
                    }
                }
                if (SelecteVehicleOnMap != null) SelecteVehicleOnMap();
            }
    }

    void tree_FilterNode(object sender, FilterNodeEventArgs e)
    {
        vehicle vehicle = _tree.GetDataRecordByNode(e.Node) as vehicle;
        if (null != vehicle)
        {
            if (_condition != null)
            {
                e.Node.Visible = vehicle.NumberPlate.Contains(_condition);
            }
            else
            {
                e.Node.Visible = vehicle.NumberPlate.Contains("");
            }
            e.Handled = true;
        }
    }

    void funnelRepo_KeyUp(object sender, KeyEventArgs e)
    {
        TextEdit edit = (TextEdit)sender;
        setCondition(edit.Text);
    }

    void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
        _funnelBox.EditValue = String.Empty;
        setCondition(String.Empty);
    }

    void hideEmpty()
    {
        TreeListNode root = _tree.Nodes[0];
        foreach (TreeListNode node in root.Nodes)
        {
            team group = _tree.GetDataRecordByNode(node) as team;
            if (null != group)
            {
                if (TreeViewService.GetVisibleCount(node) > 0)
                    node.Visible = true;
                else
                    node.Visible = false;
            }
        }
    }

    void setCondition(string condition)
    {
        _condition = condition;
        if (_condition.Length > 0)
        {
            _eraseBtn.Enabled = true;
            _tree.FilterNodes();
            hideEmpty();
        }
        else
        {
            _eraseBtn.Enabled = false;
            //TreeListNode root = _tree.Nodes[0];
            //TreeViewService.ShowAll(root);
        }
    }

    #region ---   Nested Classes   ---
    private class SelectCertainVehicleOperation : TreeListOperation
    {
        int _mobitelId;

        public SelectCertainVehicleOperation(int mobitelId)
        {
            _mobitelId = mobitelId;
        }

        public override bool NeedsVisitChildren(TreeListNode node)
        {
            team group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override bool CanContinueIteration(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            if (null != vh && vh.mobitels.Mobitel_ID == _mobitelId)
            {
                node.TreeList.FocusedNode = node;
                node.Checked = true;
                TreeViewService.SetCheckedParentNodes(node);
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void Execute(TreeListNode node)
        {
            return;
        }
    }

    private class CheckedVehiclesOperation : TreeListOperation
    {
        public List<vehicle> Vehicles
        {
            get { return _vehicles; }
        }
        List<vehicle> _vehicles = new List<vehicle>();
        team group;

        public override bool NeedsVisitChildren(TreeListNode node)
        {
            group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override void Execute(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            if (null != vh && node.Checked)
            {
                _vehicles.Add(vh);
            }
        }
    }

    private class GetAllVehiclesOperation : TreeListOperation
    {
        public List<vehicle> Vehicles
        {
            get { return _vehicles; }
        }
        List<vehicle> _vehicles = new List<vehicle>();
        team group;

        public override bool NeedsVisitChildren(TreeListNode node)
        {
            group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override void Execute(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            if (null != vh)
            {
                _vehicles.Add(vh);
            }
        }
    }

    private class UnCheckedVehiclesOperation : TreeListOperation
    {
        public List<vehicle> VehiclesEntity
        {
            get { return _vehicles; }
        }
        List<vehicle> _vehicles = new List<vehicle>();

        public override bool NeedsVisitChildren(TreeListNode node)
        {
            team group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override void Execute(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            if (null != vh && !node.Checked)
                _vehicles.Add(vh);
        }
    }

    private class RefreshNodeStatesOperation : TreeListOperation
    {
        TreeListColumn _stateVehCol;
        TreeListColumn _stateOrderCol;
        TreeListColumn _orderCol;
        public RefreshNodeStatesOperation(TreeListColumn stateVehCol, TreeListColumn stateOrderCol ,TreeListColumn orderCol)
            : base()
        {
            _stateVehCol = stateVehCol;
            _stateOrderCol = stateOrderCol;
            _orderCol = orderCol;
        }
        public override bool NeedsVisitChildren(TreeListNode node)
        {
            team group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override void Execute(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            if (vh != null)
            {
                //_contextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, vh);
                int newVehicleState = VehicleProvider.GetVehicleStateId(vh);
                if (newVehicleState != vh.StateId)
                {
                    node.SetValue(_stateVehCol.VisibleIndex, newVehicleState);
                    if (newVehicleState == (int)VehicleStates.OrderWaitng)
                    {
                        node.SetValue(_stateOrderCol.VisibleIndex, 0);
                        node.SetValue(_orderCol.VisibleIndex, "");
                    }
                    else
                    {
                        order od = OrderController.GetVehicleOrder(vh, _contextMotorDepot);
                        if (od != null)
                        {
                            if (od.md_order_state != null) node.SetValue(_stateOrderCol.VisibleIndex, od.md_order_state.Id);
                            node.SetValue(_orderCol.VisibleIndex, od.Number);
                        }
                    }
                }
            }
            Application.DoEvents();  


            
        }


    }

    private class CheckAllNodes : TreeListOperation
    {
        //team group;
        //public override bool NeedsVisitChildren(TreeListNode node)
        //{
        //    group = node.TreeList.GetDataRecordByNode(node) as team;
        //    return group != null;
        //}

        public override void Execute(TreeListNode node)
        {
            //vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            //if (null != vh)
            //{
                node.Checked = true;
            //}
        }
    }

    //private class UpdateTimeStatus : TreeListOperation
    //{
    //    TreeListColumn _colTime;
    //    public UpdateTimeStatus(TreeListColumn colTime)
    //        : base()
    //    {
    //        _colTime = colTime;
    //    }
    //    public override void Execute(TreeListNode node)
    //    {
    //        object obj = node.TreeList.GetDataRecordByNode(node);
    //        if (obj is vehicle)
    //        {
    //            vehicle vh = (vehicle)obj;
    //            node.SetValue(_colTime.VisibleIndex, vehicle.TimeStatus());
    //        }
    //    }
    //}

    private class UncheckUnvisibleOperation : TreeListOperation
    {
        List<vehicle> unvisibleVehicles;
        public UncheckUnvisibleOperation(List<vehicle> unvisibleVehicles)
        {
            this.unvisibleVehicles = unvisibleVehicles;
        }
        public override bool NeedsVisitChildren(TreeListNode node)
        {
            team group = node.TreeList.GetDataRecordByNode(node) as team;
            return group != null;
        }

        public override void Execute(TreeListNode node)
        {
            vehicle vh = node.TreeList.GetDataRecordByNode(node) as vehicle;
            node.Checked = true;
            foreach (vehicle veh in unvisibleVehicles)
            {
                if (veh == vh)
                {
                    node.Checked = false;
                }
            }
            TreeViewService.SetCheckedParentNodes(node);
        }
    }
    #endregion

    void Init()
    {
        //_funnelBox.Caption = Resources.Funnel;
        //_funnelBox.Hint = Resources.Vehicles_FunnelHint;
        //_eraseBtn.Caption = Resources.Clear;
        //_eraseBtn.Hint = Resources.Vehicles_ClearHint;
        //_titleCol.Caption = Resources.Vehicles_TitleAndNumber;
        //_descriptionCol.Caption = Resources.Description;
        //_loginCol.Caption = Resources.Login;
        bbiExpand.Glyph = Shared.Expand;
        bbiCollapce.Glyph = Shared.Collapce;
    }

    private void _tree_DoubleClick(object sender, EventArgs e)
    {
        TreeList tree = sender as TreeList;
        TreeListHitInfo hi = tree.CalcHitInfo(tree.PointToClient(Control.MousePosition));
        if (hi.Node != null)
        {
            object obj = _tree.GetDataRecordByNode(hi.Node);
            if (obj is vehicle)
            {
                vehicle veh = (vehicle)obj;
                var ord = OrderController.GetVehicleOrder(veh,MDController.Instance.ContextMotorDepot);
                if (ord is order)
                {
                    _formController.OrderViewOrder(ord);
                }
                //Color vehcol = vehicle.Style.ColorTrack;

            }
        }
    }

    private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
    {
        _tree.ExpandAll();
    }

    private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
    {
        _tree.CollapseAll();
    }

    private void ttcTree_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
    {
        if (e.SelectedControl is DevExpress.XtraTreeList.TreeList)
        {
            //TreeList tree = (TreeList)e.SelectedControl;
            //TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);
            //if (hit.Column == timeCol)
            //{
            //    if (hit.HitInfoType == HitInfoType.Cell)
            //    {
            //        object cellInfo = new TreeListCellToolTipInfo(hit.Node, hit.Column, null);
            //        vehicle vehicle = tree.GetDataRecordByNode(hit.Node) as vehicle;
            //        if (null != vehicle)
            //        {
            //            string toolTip = string.Format("{1}: {0}", vehicle.Mobitel.LastTimeGps.ToString(), Resources.LastData);
            //            e.Info = new DevExpress.Utils.ToolTipControlInfo(cellInfo, toolTip);
            //        }

            //    }
            //    else if (hit.HitInfoType == HitInfoType.Column)
            //    {
            //        e.Info = new DevExpress.Utils.ToolTipControlInfo(hit.Column, TreeViewService.GetToolTip());
            //    }
            //}
        }
    }

    public void RefreshVehicleStates()
    {
        RefreshNodeStatesOperation operation = new RefreshNodeStatesOperation(stateVehCol, stateOrderCol, orderCol);
        _tree.NodesIterator.DoLocalOperation(operation, _tree.Nodes);
    }



    #region ����� IDisposable

    public new void Dispose()
    {
        if (_contextMotorDepot != null) _contextMotorDepot.Dispose();
    }

    #endregion
  }
}
