namespace MotorDepot
{
    partial class MdVehicles
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MdVehicles));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this._funnelBox = new DevExpress.XtraBars.BarEditItem();
            this._funnelRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._titleCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.stateVehCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rcbState = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imVehStates = new DevExpress.Utils.ImageCollection(this.components);
            this._descriptionCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.stateOrderCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rcbStateOrder = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imOrderStates = new DevExpress.Utils.ImageCollection(this.components);
            this.orderCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.ttcTree = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imVehStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbStateOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._funnelBox,
            this._eraseBtn,
            this.bbiExpand,
            this.bbiCollapce});
            this._barManager.MaxItemId = 5;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._funnelRepo});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCollapce),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._funnelBox, "", true, true, true, 100),
            new DevExpress.XtraBars.LinkPersistInfo(this._eraseBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // bbiExpand
            // 
            this.bbiExpand.Caption = "E";
            this.bbiExpand.Id = 2;
            this.bbiExpand.Name = "bbiExpand";
            this.bbiExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExpand_ItemClick);
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Caption = "C";
            this.bbiCollapce.Id = 3;
            this.bbiCollapce.Name = "bbiCollapce";
            this.bbiCollapce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCollapce_ItemClick);
            // 
            // _funnelBox
            // 
            this._funnelBox.Caption = "������";
            this._funnelBox.Edit = this._funnelRepo;
            this._funnelBox.Hint = "������� ������� ���������� �� ���������������� ������";
            this._funnelBox.Id = 0;
            this._funnelBox.Name = "_funnelBox";
            this._funnelBox.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _funnelRepo
            // 
            this._funnelRepo.AutoHeight = false;
            this._funnelRepo.MaxLength = 20;
            this._funnelRepo.Name = "_funnelRepo";
            this._funnelRepo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.funnelRepo_KeyUp);
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Caption = "��������";
            this._eraseBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_eraseBtn.Glyph")));
            this._eraseBtn.Hint = "�������� ���������� �������";
            this._eraseBtn.Id = 1;
            this._eraseBtn.Name = "_eraseBtn";
            this._eraseBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eraseBtn_ItemClick);
            // 
            // _tree
            // 
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._titleCol,
            this.stateVehCol,
            this._descriptionCol,
            this.stateOrderCol,
            this.orderCol});
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.ImageIndexFieldName = "IconIndex";
            this._tree.Location = new System.Drawing.Point(0, 26);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.AllowIndeterminateCheckState = true;
            this._tree.OptionsBehavior.Editable = false;
            this._tree.OptionsBehavior.EnableFiltering = true;
            this._tree.OptionsBehavior.PopulateServiceColumns = true;
            this._tree.OptionsMenu.EnableColumnMenu = false;
            this._tree.OptionsMenu.EnableFooterMenu = false;
            this._tree.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._tree.OptionsView.ShowIndicator = false;
            this._tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbState,
            this.rcbStateOrder});
            this._tree.Size = new System.Drawing.Size(364, 432);
            this._tree.TabIndex = 4;
            this._tree.ToolTipController = this.ttcTree;
            this._tree.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterFocusNode);
            this._tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this._tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            this._tree.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.tree_FilterNode);
            this._tree.DoubleClick += new System.EventHandler(this._tree_DoubleClick);
            // 
            // _titleCol
            // 
            this._titleCol.Caption = "��������, �����";
            this._titleCol.FieldName = "Title";
            this._titleCol.MinWidth = 150;
            this._titleCol.Name = "_titleCol";
            this._titleCol.OptionsColumn.AllowEdit = false;
            this._titleCol.OptionsColumn.AllowFocus = false;
            this._titleCol.OptionsColumn.ReadOnly = true;
            this._titleCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._titleCol.Visible = true;
            this._titleCol.VisibleIndex = 0;
            this._titleCol.Width = 150;
            // 
            // stateVehCol
            // 
            this.stateVehCol.ColumnEdit = this.rcbState;
            this.stateVehCol.Name = "stateVehCol";
            this.stateVehCol.OptionsColumn.AllowEdit = false;
            this.stateVehCol.OptionsColumn.AllowFocus = false;
            this.stateVehCol.OptionsColumn.AllowMove = false;
            this.stateVehCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.stateVehCol.OptionsColumn.AllowSize = false;
            this.stateVehCol.OptionsColumn.FixedWidth = true;
            this.stateVehCol.OptionsColumn.ReadOnly = true;
            this.stateVehCol.OptionsColumn.ShowInCustomizationForm = false;
            this.stateVehCol.Visible = true;
            this.stateVehCol.VisibleIndex = 1;
            this.stateVehCol.Width = 20;
            // 
            // rcbState
            // 
            this.rcbState.AutoHeight = false;
            this.rcbState.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbState.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rcbState.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 4, 4)});
            this.rcbState.Name = "rcbState";
            this.rcbState.SmallImages = this.imVehStates;
            // 
            // imVehStates
            // 
            this.imVehStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imVehStates.ImageStream")));
            this.imVehStates.Images.SetKeyName(0, "status-offline.png");
            this.imVehStates.Images.SetKeyName(1, "circle_green.png");
            this.imVehStates.Images.SetKeyName(2, "square_yellow.png");
            this.imVehStates.Images.SetKeyName(3, "triangle_red.png");
            this.imVehStates.Images.SetKeyName(4, "romb_blue.png");
            // 
            // _descriptionCol
            // 
            this._descriptionCol.Caption = "��������";
            this._descriptionCol.FieldName = "Description";
            this._descriptionCol.MinWidth = 100;
            this._descriptionCol.Name = "_descriptionCol";
            this._descriptionCol.OptionsColumn.AllowEdit = false;
            this._descriptionCol.OptionsColumn.AllowFocus = false;
            this._descriptionCol.OptionsColumn.ReadOnly = true;
            this._descriptionCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._descriptionCol.Visible = true;
            this._descriptionCol.VisibleIndex = 2;
            this._descriptionCol.Width = 100;
            // 
            // stateOrderCol
            // 
            this.stateOrderCol.ColumnEdit = this.rcbStateOrder;
            this.stateOrderCol.Name = "stateOrderCol";
            this.stateOrderCol.OptionsColumn.AllowEdit = false;
            this.stateOrderCol.OptionsColumn.AllowFocus = false;
            this.stateOrderCol.OptionsColumn.AllowMove = false;
            this.stateOrderCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.stateOrderCol.OptionsColumn.AllowSize = false;
            this.stateOrderCol.OptionsColumn.FixedWidth = true;
            this.stateOrderCol.OptionsColumn.ReadOnly = true;
            this.stateOrderCol.OptionsColumn.ShowInCustomizationForm = false;
            this.stateOrderCol.Visible = true;
            this.stateOrderCol.VisibleIndex = 3;
            this.stateOrderCol.Width = 20;
            // 
            // rcbStateOrder
            // 
            this.rcbStateOrder.AutoHeight = false;
            this.rcbStateOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbStateOrder.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 6, 5)});
            this.rcbStateOrder.Name = "rcbStateOrder";
            this.rcbStateOrder.SmallImages = this.imOrderStates;
            // 
            // imOrderStates
            // 
            this.imOrderStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imOrderStates.ImageStream")));
            this.imOrderStates.Images.SetKeyName(0, "document.png");
            this.imOrderStates.Images.SetKeyName(1, "document-broken.png");
            this.imOrderStates.Images.SetKeyName(2, "document-hf-insert-footer.png");
            this.imOrderStates.Images.SetKeyName(3, "document--exclamation.png");
            this.imOrderStates.Images.SetKeyName(4, "document-task.png");
            this.imOrderStates.Images.SetKeyName(5, "document-hf-delete-footer.png");
            // 
            // orderCol
            // 
            this.orderCol.AppearanceCell.Options.UseTextOptions = true;
            this.orderCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.orderCol.AppearanceHeader.Options.UseTextOptions = true;
            this.orderCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.orderCol.Caption = "����� - �����";
            this.orderCol.FieldName = "Login";
            this.orderCol.MinWidth = 40;
            this.orderCol.Name = "orderCol";
            this.orderCol.OptionsColumn.AllowEdit = false;
            this.orderCol.OptionsColumn.AllowFocus = false;
            this.orderCol.OptionsColumn.ReadOnly = true;
            this.orderCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.orderCol.Visible = true;
            this.orderCol.VisibleIndex = 4;
            this.orderCol.Width = 90;
            // 
            // ttcTree
            // 
            this.ttcTree.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcTree_GetActiveObjectInfo);
            // 
            // MdVehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MdVehicles";
            this.Size = new System.Drawing.Size(364, 458);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imVehStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbStateOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraTreeList.TreeList _tree;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _titleCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _descriptionCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn orderCol;
    private DevExpress.XtraBars.BarEditItem _funnelBox;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _funnelRepo;
    private DevExpress.XtraBars.BarButtonItem _eraseBtn;
    private DevExpress.XtraBars.BarButtonItem bbiExpand;
    private DevExpress.XtraBars.BarButtonItem bbiCollapce;
    private DevExpress.XtraTreeList.Columns.TreeListColumn stateOrderCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbState;
    private DevExpress.Utils.ToolTipController ttcTree;
    private DevExpress.Utils.ImageCollection imVehStates;
    internal DevExpress.Utils.ImageCollection imOrderStates;
    private DevExpress.XtraTreeList.Columns.TreeListColumn stateVehCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbStateOrder;
  }
}
