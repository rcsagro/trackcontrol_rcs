﻿
namespace MotorDepot
{
    public enum OrderStates
    {
        WaitingFor,
        Design,
        Refuse,
        Execute,
        Finish,
        Close,
        Delete
    }
}
