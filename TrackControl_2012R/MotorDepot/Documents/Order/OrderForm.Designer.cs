namespace MotorDepot
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraVerticalGrid.Rows.EditorRow erTranType;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderForm));
            this.rleTranType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pgOrder = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.rleObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleCategory = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rlePriority = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rdeDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rteTime = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.txTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rleWayBill = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rchClose = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.rleStateOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rgleObject = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.rgleObjectView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolObjectName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rgleWayBill = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.rgleWayBillView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWBVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleEnterprise = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.erId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erObjectCustomer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erObjectStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erCategory = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPriority = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeDilay = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erWayBill = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateCreate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateInit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateStartWork = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateRefuse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateDelete = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateExecute = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsClose = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erUser = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erState = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erEnterprise = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcOrderT = new DevExpress.XtraGrid.GridControl();
            this.bsOrderT = new System.Windows.Forms.BindingSource(this.components);
            this.abgvOrderT = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rgleTObject = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.rgleTObjectView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolDistanceToObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeToObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolDistanceToObjectFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeToObjectFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolCargoLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleTCargo = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.bcolQtyLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolCargoUnLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolQtyUnLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeUnLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolTimeDelayInPath = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeDelay = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolTimeEnter = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rteTimeEntry = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.bcolTimeExit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bcolRemark = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bcolAlarm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleAlarm = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleTObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRecalc = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefuse = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLog = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            erTranType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.rleTranType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStateOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleObjectView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleWayBillView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrderT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrderT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abgvOrderT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleTObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleTObjectView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTCargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTimeEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAlarm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // erTranType
            // 
            erTranType.Name = "erTranType";
            erTranType.Properties.Caption = "��� ���������";
            erTranType.Properties.FieldName = "md_transportation_types";
            erTranType.Properties.RowEdit = this.rleTranType;
            // 
            // rleTranType
            // 
            this.rleTranType.AutoHeight = false;
            this.rleTranType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTranType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleTranType.DisplayMember = "Name";
            this.rleTranType.Name = "rleTranType";
            this.rleTranType.NullText = "";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 24);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pgOrder);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcOrderT);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1370, 497);
            this.splitContainerControl1.SplitterPosition = 329;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // pgOrder
            // 
            this.pgOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgOrder.Location = new System.Drawing.Point(0, 0);
            this.pgOrder.Name = "pgOrder";
            this.pgOrder.RecordWidth = 88;
            this.pgOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleObject,
            this.rleCategory,
            this.rleTranType,
            this.rlePriority,
            this.rdeDate,
            this.rteTime,
            this.txTime,
            this.rleWayBill,
            this.rchClose,
            this.rleStateOrder,
            this.rgleObject,
            this.rgleWayBill,
            this.rleEnterprise});
            this.pgOrder.RowHeaderWidth = 112;
            this.pgOrder.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erId,
            this.erNumber,
            this.erObjectCustomer,
            this.erObjectStart,
            this.erCategory,
            erTranType,
            this.erPriority,
            this.erTimeDilay,
            this.erWayBill,
            this.erDateCreate,
            this.erDateInit,
            this.erDateStartWork,
            this.erDateRefuse,
            this.erDateEdit,
            this.erDateDelete,
            this.erDateExecute,
            this.erRemark,
            this.erIsClose,
            this.erUser,
            this.erState,
            this.erEnterprise});
            this.pgOrder.Size = new System.Drawing.Size(329, 497);
            this.pgOrder.TabIndex = 0;
            this.pgOrder.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgOrder_CellValueChanged);
            // 
            // rleObject
            // 
            this.rleObject.AutoHeight = false;
            this.rleObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleObject.DisplayMember = "Name";
            this.rleObject.Name = "rleObject";
            this.rleObject.NullText = "";
            // 
            // rleCategory
            // 
            this.rleCategory.AutoHeight = false;
            this.rleCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleCategory.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleCategory.DisplayMember = "Name";
            this.rleCategory.Name = "rleCategory";
            this.rleCategory.NullText = "";
            // 
            // rlePriority
            // 
            this.rlePriority.AutoHeight = false;
            this.rlePriority.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rlePriority.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rlePriority.DisplayMember = "Name";
            this.rlePriority.Name = "rlePriority";
            this.rlePriority.NullText = "";
            // 
            // rdeDate
            // 
            this.rdeDate.AutoHeight = false;
            this.rdeDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rdeDate.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rdeDate.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.rdeDate.DisplayFormat.FormatString = "g";
            this.rdeDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDate.EditFormat.FormatString = "g";
            this.rdeDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDate.Mask.EditMask = "g";
            this.rdeDate.Name = "rdeDate";
            // 
            // rteTime
            // 
            this.rteTime.AutoHeight = false;
            this.rteTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rteTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.rteTime.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rteTime.DisplayFormat.FormatString = "hh:mm";
            this.rteTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.rteTime.Mask.EditMask = "hh:mm";
            this.rteTime.Name = "rteTime";
            this.rteTime.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // txTime
            // 
            this.txTime.AutoHeight = false;
            this.txTime.EditFormat.FormatString = "t";
            this.txTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txTime.Mask.EditMask = "90:00";
            this.txTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txTime.Mask.UseMaskAsDisplayFormat = true;
            this.txTime.Name = "txTime";
            // 
            // rleWayBill
            // 
            this.rleWayBill.AutoHeight = false;
            this.rleWayBill.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleWayBill.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VehicleForOrder", "���������")});
            this.rleWayBill.DisplayMember = "VehicleForOrder";
            this.rleWayBill.Name = "rleWayBill";
            this.rleWayBill.NullText = "";
            // 
            // rchClose
            // 
            this.rchClose.AutoHeight = false;
            this.rchClose.Caption = "Check";
            this.rchClose.Name = "rchClose";
            // 
            // rleStateOrder
            // 
            this.rleStateOrder.AutoHeight = false;
            this.rleStateOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleStateOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleStateOrder.DisplayMember = "Name";
            this.rleStateOrder.Name = "rleStateOrder";
            this.rleStateOrder.NullText = "";
            // 
            // rgleObject
            // 
            this.rgleObject.AutoHeight = false;
            this.rgleObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleObject.DisplayMember = "Name";
            this.rgleObject.Name = "rgleObject";
            this.rgleObject.NullText = "";
            this.rgleObject.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleObject.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleObject.View = this.rgleObjectView;
            // 
            // rgleObjectView
            // 
            this.rgleObjectView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolObjectName});
            this.rgleObjectView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.rgleObjectView.Name = "rgleObjectView";
            this.rgleObjectView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.rgleObjectView.OptionsView.ShowGroupPanel = false;
            // 
            // gcolObjectName
            // 
            this.gcolObjectName.AppearanceHeader.Options.UseTextOptions = true;
            this.gcolObjectName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolObjectName.Caption = "��������";
            this.gcolObjectName.FieldName = "Name";
            this.gcolObjectName.Name = "gcolObjectName";
            this.gcolObjectName.Visible = true;
            this.gcolObjectName.VisibleIndex = 0;
            // 
            // rgleWayBill
            // 
            this.rgleWayBill.AutoHeight = false;
            this.rgleWayBill.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleWayBill.DisplayMember = "VehicleForOrder";
            this.rgleWayBill.Name = "rgleWayBill";
            this.rgleWayBill.NullText = "";
            this.rgleWayBill.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleWayBill.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleWayBill.View = this.rgleWayBillView;
            // 
            // rgleWayBillView
            // 
            this.rgleWayBillView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWBVehicle});
            this.rgleWayBillView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.rgleWayBillView.Name = "rgleWayBillView";
            this.rgleWayBillView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.rgleWayBillView.OptionsView.ShowGroupPanel = false;
            // 
            // colWBVehicle
            // 
            this.colWBVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colWBVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWBVehicle.Caption = "���������";
            this.colWBVehicle.FieldName = "VehicleForOrder";
            this.colWBVehicle.Name = "colWBVehicle";
            this.colWBVehicle.Visible = true;
            this.colWBVehicle.VisibleIndex = 0;
            // 
            // rleEnterprise
            // 
            this.rleEnterprise.AutoHeight = false;
            this.rleEnterprise.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleEnterprise.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleEnterprise.DisplayMember = "Name";
            this.rleEnterprise.Name = "rleEnterprise";
            this.rleEnterprise.NullText = "";
            // 
            // erId
            // 
            this.erId.Name = "erId";
            this.erId.Properties.Caption = "�������������";
            this.erId.Properties.FieldName = "Id";
            this.erId.Properties.ReadOnly = true;
            // 
            // erNumber
            // 
            this.erNumber.Name = "erNumber";
            this.erNumber.Properties.Caption = "�����";
            this.erNumber.Properties.FieldName = "Number";
            this.erNumber.Properties.ReadOnly = true;
            // 
            // erObjectCustomer
            // 
            this.erObjectCustomer.Expanded = false;
            this.erObjectCustomer.Height = 19;
            this.erObjectCustomer.Name = "erObjectCustomer";
            this.erObjectCustomer.Properties.Caption = "��������";
            this.erObjectCustomer.Properties.FieldName = "md_objectCustomer";
            this.erObjectCustomer.Properties.RowEdit = this.rgleObject;
            // 
            // erObjectStart
            // 
            this.erObjectStart.Name = "erObjectStart";
            this.erObjectStart.Properties.Caption = "��������� �����";
            this.erObjectStart.Properties.FieldName = "md_objectStart";
            this.erObjectStart.Properties.RowEdit = this.rgleObject;
            // 
            // erCategory
            // 
            this.erCategory.Name = "erCategory";
            this.erCategory.Properties.Caption = "��������� ������";
            this.erCategory.Properties.FieldName = "md_order_category";
            this.erCategory.Properties.RowEdit = this.rleCategory;
            // 
            // erPriority
            // 
            this.erPriority.Height = 19;
            this.erPriority.Name = "erPriority";
            this.erPriority.Properties.Caption = "���������";
            this.erPriority.Properties.FieldName = "md_order_priority";
            this.erPriority.Properties.RowEdit = this.rlePriority;
            // 
            // erTimeDilay
            // 
            this.erTimeDilay.Name = "erTimeDilay";
            this.erTimeDilay.Properties.Caption = "����� �������� ����������";
            this.erTimeDilay.Properties.FieldName = "TimeDelay";
            this.erTimeDilay.Properties.RowEdit = this.txTime;
            // 
            // erWayBill
            // 
            this.erWayBill.Name = "erWayBill";
            this.erWayBill.Properties.Caption = "���������";
            this.erWayBill.Properties.FieldName = "md_waybill";
            this.erWayBill.Properties.RowEdit = this.rgleWayBill;
            // 
            // erDateCreate
            // 
            this.erDateCreate.Name = "erDateCreate";
            this.erDateCreate.Properties.Caption = "���� �������� ";
            this.erDateCreate.Properties.FieldName = "DateCreate";
            this.erDateCreate.Properties.Format.FormatString = "g";
            this.erDateCreate.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.erDateCreate.Properties.ReadOnly = true;
            // 
            // erDateInit
            // 
            this.erDateInit.Name = "erDateInit";
            this.erDateInit.Properties.Caption = "���� ���������";
            this.erDateInit.Properties.FieldName = "DateInit";
            this.erDateInit.Properties.Format.FormatString = "g";
            this.erDateInit.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.erDateInit.Properties.RowEdit = this.rdeDate;
            // 
            // erDateStartWork
            // 
            this.erDateStartWork.Enabled = false;
            this.erDateStartWork.Name = "erDateStartWork";
            this.erDateStartWork.Properties.Caption = "���� ������ ������";
            this.erDateStartWork.Properties.FieldName = "DateStartWork";
            this.erDateStartWork.Properties.ReadOnly = true;
            // 
            // erDateRefuse
            // 
            this.erDateRefuse.Name = "erDateRefuse";
            this.erDateRefuse.Properties.Caption = "���� ������";
            this.erDateRefuse.Properties.FieldName = "DateRefuse";
            this.erDateRefuse.Properties.ReadOnly = true;
            // 
            // erDateEdit
            // 
            this.erDateEdit.Name = "erDateEdit";
            this.erDateEdit.Properties.Caption = "���� ��������������";
            this.erDateEdit.Properties.FieldName = "DateEdit";
            this.erDateEdit.Properties.ReadOnly = true;
            // 
            // erDateDelete
            // 
            this.erDateDelete.Name = "erDateDelete";
            this.erDateDelete.Properties.Caption = "���� ��������";
            this.erDateDelete.Properties.FieldName = "DateDelete";
            this.erDateDelete.Properties.ReadOnly = true;
            // 
            // erDateExecute
            // 
            this.erDateExecute.Enabled = false;
            this.erDateExecute.Name = "erDateExecute";
            this.erDateExecute.Properties.Caption = "���� ����������";
            this.erDateExecute.Properties.FieldName = "DateExecute";
            this.erDateExecute.Properties.ReadOnly = true;
            // 
            // erRemark
            // 
            this.erRemark.Name = "erRemark";
            this.erRemark.Properties.Caption = "����������";
            this.erRemark.Properties.FieldName = "Remark";
            // 
            // erIsClose
            // 
            this.erIsClose.Name = "erIsClose";
            this.erIsClose.Properties.Caption = "������";
            this.erIsClose.Properties.FieldName = "IsClose";
            this.erIsClose.Properties.RowEdit = this.rchClose;
            // 
            // erUser
            // 
            this.erUser.Name = "erUser";
            this.erUser.Properties.Caption = "����������";
            this.erUser.Properties.FieldName = "UserCreator";
            this.erUser.Properties.ReadOnly = true;
            // 
            // erState
            // 
            this.erState.Name = "erState";
            this.erState.Properties.Caption = "������";
            this.erState.Properties.FieldName = "md_order_state";
            this.erState.Properties.ReadOnly = true;
            this.erState.Properties.RowEdit = this.rleStateOrder;
            // 
            // erEnterprise
            // 
            this.erEnterprise.Name = "erEnterprise";
            this.erEnterprise.Properties.Caption = "�����������";
            this.erEnterprise.Properties.FieldName = "md_enterprise";
            this.erEnterprise.Properties.ReadOnly = true;
            this.erEnterprise.Properties.RowEdit = this.rleEnterprise;
            // 
            // gcOrderT
            // 
            this.gcOrderT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.gcOrderT.DataSource = this.bsOrderT;
            this.gcOrderT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOrderT.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcOrderT_EmbeddedNavigator_ButtonClick);
            this.gcOrderT.Location = new System.Drawing.Point(0, 0);
            this.gcOrderT.MainView = this.abgvOrderT;
            this.gcOrderT.Name = "gcOrderT";
            this.gcOrderT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleTObject,
            this.rleTCargo,
            this.rleAlarm,
            this.rteTimeEntry,
            this.rgleTObject});
            this.gcOrderT.Size = new System.Drawing.Size(1036, 497);
            this.gcOrderT.TabIndex = 0;
            this.gcOrderT.UseEmbeddedNavigator = true;
            this.gcOrderT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.abgvOrderT});
            // 
            // abgvOrderT
            // 
            this.abgvOrderT.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand7,
            this.gridBand6});
            this.abgvOrderT.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bcolId,
            this.bcolObject,
            this.bcolDistanceToObject,
            this.bcolTimeToObject,
            this.bcolCargoLoading,
            this.bcolQtyLoading,
            this.bcolTimeLoading,
            this.bcolCargoUnLoading,
            this.bcolQtyUnLoading,
            this.bcolTimeUnLoading,
            this.bcolTimeDelayInPath,
            this.bcolTimeDelay,
            this.bcolRemark,
            this.bcolDistanceToObjectFact,
            this.bcolTimeToObjectFact,
            this.bcolTimeEnter,
            this.bcolTimeExit,
            this.bcolAlarm});
            this.abgvOrderT.GridControl = this.gcOrderT;
            this.abgvOrderT.Name = "abgvOrderT";
            this.abgvOrderT.OptionsView.ShowGroupPanel = false;
            this.abgvOrderT.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.abgvOrderT_CellValueChanged);
            this.abgvOrderT.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.abgvOrderT_ValidateRow);
            this.abgvOrderT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.abgvOrderT_KeyDown);
            this.abgvOrderT.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.abgvOrderT_ValidatingEditor);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "����� ����������";
            this.gridBand4.Columns.Add(this.bcolId);
            this.gridBand4.Columns.Add(this.bcolObject);
            this.gridBand4.MinWidth = 20;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.OptionsBand.ShowCaption = false;
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 173;
            // 
            // bcolId
            // 
            this.bcolId.AppearanceCell.Options.UseTextOptions = true;
            this.bcolId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolId.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolId.AutoFillDown = true;
            this.bcolId.Caption = "Id";
            this.bcolId.FieldName = "Id";
            this.bcolId.Name = "bcolId";
            this.bcolId.OptionsColumn.AllowEdit = false;
            this.bcolId.OptionsColumn.ReadOnly = true;
            this.bcolId.Visible = true;
            this.bcolId.Width = 43;
            // 
            // bcolObject
            // 
            this.bcolObject.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolObject.AutoFillDown = true;
            this.bcolObject.Caption = "����� ����������";
            this.bcolObject.ColumnEdit = this.rgleTObject;
            this.bcolObject.FieldName = "md_object";
            this.bcolObject.Name = "bcolObject";
            this.bcolObject.Visible = true;
            this.bcolObject.Width = 130;
            // 
            // rgleTObject
            // 
            this.rgleTObject.AutoHeight = false;
            this.rgleTObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleTObject.DisplayMember = "Name";
            this.rgleTObject.Name = "rgleTObject";
            this.rgleTObject.NullText = "";
            this.rgleTObject.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleTObject.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleTObject.View = this.rgleTObjectView;
            // 
            // rgleTObjectView
            // 
            this.rgleTObjectView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolObject});
            this.rgleTObjectView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.rgleTObjectView.Name = "rgleTObjectView";
            this.rgleTObjectView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.rgleTObjectView.OptionsView.ShowGroupPanel = false;
            // 
            // gcolObject
            // 
            this.gcolObject.Caption = "������";
            this.gcolObject.FieldName = "Name";
            this.gcolObject.Name = "gcolObject";
            this.gcolObject.Visible = true;
            this.gcolObject.VisibleIndex = 0;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "����";
            this.gridBand1.Columns.Add(this.bcolDistanceToObject);
            this.gridBand1.Columns.Add(this.bcolTimeToObject);
            this.gridBand1.Columns.Add(this.bcolDistanceToObjectFact);
            this.gridBand1.Columns.Add(this.bcolTimeToObjectFact);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 173;
            // 
            // bcolDistanceToObject
            // 
            this.bcolDistanceToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolDistanceToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolDistanceToObject.Caption = "���������� (����)";
            this.bcolDistanceToObject.FieldName = "DistanceToObject";
            this.bcolDistanceToObject.Name = "bcolDistanceToObject";
            this.bcolDistanceToObject.Visible = true;
            this.bcolDistanceToObject.Width = 86;
            // 
            // bcolTimeToObject
            // 
            this.bcolTimeToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeToObject.Caption = "�����(����)";
            this.bcolTimeToObject.FieldName = "TimeToObject";
            this.bcolTimeToObject.Name = "bcolTimeToObject";
            this.bcolTimeToObject.Visible = true;
            this.bcolTimeToObject.Width = 87;
            // 
            // bcolDistanceToObjectFact
            // 
            this.bcolDistanceToObjectFact.Caption = "����������(����)";
            this.bcolDistanceToObjectFact.FieldName = "DistanceToObjectFact";
            this.bcolDistanceToObjectFact.Name = "bcolDistanceToObjectFact";
            this.bcolDistanceToObjectFact.OptionsColumn.AllowEdit = false;
            this.bcolDistanceToObjectFact.OptionsColumn.ReadOnly = true;
            this.bcolDistanceToObjectFact.RowIndex = 1;
            this.bcolDistanceToObjectFact.Visible = true;
            this.bcolDistanceToObjectFact.Width = 86;
            // 
            // bcolTimeToObjectFact
            // 
            this.bcolTimeToObjectFact.Caption = "�����(����)";
            this.bcolTimeToObjectFact.FieldName = "TimeToObjectFact";
            this.bcolTimeToObjectFact.Name = "bcolTimeToObjectFact";
            this.bcolTimeToObjectFact.OptionsColumn.AllowEdit = false;
            this.bcolTimeToObjectFact.OptionsColumn.ReadOnly = true;
            this.bcolTimeToObjectFact.RowIndex = 1;
            this.bcolTimeToObjectFact.Visible = true;
            this.bcolTimeToObjectFact.Width = 87;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "��������";
            this.gridBand2.Columns.Add(this.bcolCargoLoading);
            this.gridBand2.Columns.Add(this.bcolQtyLoading);
            this.gridBand2.Columns.Add(this.bcolTimeLoading);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 197;
            // 
            // bcolCargoLoading
            // 
            this.bcolCargoLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolCargoLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolCargoLoading.AutoFillDown = true;
            this.bcolCargoLoading.Caption = "��� �����";
            this.bcolCargoLoading.ColumnEdit = this.rleTCargo;
            this.bcolCargoLoading.FieldName = "md_cargoLoad";
            this.bcolCargoLoading.Name = "bcolCargoLoading";
            this.bcolCargoLoading.Visible = true;
            this.bcolCargoLoading.Width = 65;
            // 
            // rleTCargo
            // 
            this.rleTCargo.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rleTCargo.AutoHeight = false;
            this.rleTCargo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTCargo.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleTCargo.DisplayMember = "Name";
            this.rleTCargo.Name = "rleTCargo";
            this.rleTCargo.NullText = "";
            // 
            // bcolQtyLoading
            // 
            this.bcolQtyLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolQtyLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolQtyLoading.AutoFillDown = true;
            this.bcolQtyLoading.Caption = "���-��";
            this.bcolQtyLoading.FieldName = "QtyLoading";
            this.bcolQtyLoading.Name = "bcolQtyLoading";
            this.bcolQtyLoading.Visible = true;
            this.bcolQtyLoading.Width = 65;
            // 
            // bcolTimeLoading
            // 
            this.bcolTimeLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeLoading.AutoFillDown = true;
            this.bcolTimeLoading.Caption = "�����";
            this.bcolTimeLoading.FieldName = "TimeLoading";
            this.bcolTimeLoading.Name = "bcolTimeLoading";
            this.bcolTimeLoading.Visible = true;
            this.bcolTimeLoading.Width = 67;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "���������";
            this.gridBand3.Columns.Add(this.bcolCargoUnLoading);
            this.gridBand3.Columns.Add(this.bcolQtyUnLoading);
            this.gridBand3.Columns.Add(this.bcolTimeUnLoading);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 3;
            this.gridBand3.Width = 197;
            // 
            // bcolCargoUnLoading
            // 
            this.bcolCargoUnLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolCargoUnLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolCargoUnLoading.AutoFillDown = true;
            this.bcolCargoUnLoading.Caption = "��� �����";
            this.bcolCargoUnLoading.ColumnEdit = this.rleTCargo;
            this.bcolCargoUnLoading.FieldName = "md_cargoUnLoad";
            this.bcolCargoUnLoading.Name = "bcolCargoUnLoading";
            this.bcolCargoUnLoading.Visible = true;
            this.bcolCargoUnLoading.Width = 65;
            // 
            // bcolQtyUnLoading
            // 
            this.bcolQtyUnLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolQtyUnLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolQtyUnLoading.AutoFillDown = true;
            this.bcolQtyUnLoading.Caption = "���-��";
            this.bcolQtyUnLoading.FieldName = "QtyUnLoading";
            this.bcolQtyUnLoading.Name = "bcolQtyUnLoading";
            this.bcolQtyUnLoading.Visible = true;
            this.bcolQtyUnLoading.Width = 65;
            // 
            // bcolTimeUnLoading
            // 
            this.bcolTimeUnLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeUnLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeUnLoading.AutoFillDown = true;
            this.bcolTimeUnLoading.Caption = "�����";
            this.bcolTimeUnLoading.FieldName = "TimeUnLoading";
            this.bcolTimeUnLoading.Name = "bcolTimeUnLoading";
            this.bcolTimeUnLoading.Visible = true;
            this.bcolTimeUnLoading.Width = 67;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "�����";
            this.gridBand7.Columns.Add(this.bcolTimeDelayInPath);
            this.gridBand7.Columns.Add(this.bcolTimeDelay);
            this.gridBand7.Columns.Add(this.bcolTimeEnter);
            this.gridBand7.Columns.Add(this.bcolTimeExit);
            this.gridBand7.MinWidth = 20;
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 222;
            // 
            // bcolTimeDelayInPath
            // 
            this.bcolTimeDelayInPath.AppearanceCell.Options.UseTextOptions = true;
            this.bcolTimeDelayInPath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeDelayInPath.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeDelayInPath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeDelayInPath.Caption = "�������� � ����";
            this.bcolTimeDelayInPath.FieldName = "TimeDelayInPath";
            this.bcolTimeDelayInPath.Name = "bcolTimeDelayInPath";
            this.bcolTimeDelayInPath.Visible = true;
            this.bcolTimeDelayInPath.Width = 102;
            // 
            // bcolTimeDelay
            // 
            this.bcolTimeDelay.AppearanceCell.Options.UseTextOptions = true;
            this.bcolTimeDelay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeDelay.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeDelay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeDelay.Caption = "�������� �� �������";
            this.bcolTimeDelay.FieldName = "TimeDelay";
            this.bcolTimeDelay.Name = "bcolTimeDelay";
            this.bcolTimeDelay.Visible = true;
            this.bcolTimeDelay.Width = 120;
            // 
            // bcolTimeEnter
            // 
            this.bcolTimeEnter.AppearanceCell.Options.UseTextOptions = true;
            this.bcolTimeEnter.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeEnter.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeEnter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeEnter.Caption = "�����";
            this.bcolTimeEnter.ColumnEdit = this.rteTimeEntry;
            this.bcolTimeEnter.DisplayFormat.FormatString = "t";
            this.bcolTimeEnter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bcolTimeEnter.FieldName = "TimeEnter";
            this.bcolTimeEnter.Name = "bcolTimeEnter";
            this.bcolTimeEnter.OptionsColumn.AllowEdit = false;
            this.bcolTimeEnter.OptionsColumn.ReadOnly = true;
            this.bcolTimeEnter.RowIndex = 1;
            this.bcolTimeEnter.Visible = true;
            this.bcolTimeEnter.Width = 99;
            // 
            // rteTimeEntry
            // 
            this.rteTimeEntry.AutoHeight = false;
            this.rteTimeEntry.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rteTimeEntry.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rteTimeEntry.DisplayFormat.FormatString = "hh:mm";
            this.rteTimeEntry.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rteTimeEntry.EditFormat.FormatString = "hh:mm";
            this.rteTimeEntry.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rteTimeEntry.Mask.EditMask = "hh:mm";
            this.rteTimeEntry.Name = "rteTimeEntry";
            this.rteTimeEntry.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // bcolTimeExit
            // 
            this.bcolTimeExit.AppearanceCell.Options.UseTextOptions = true;
            this.bcolTimeExit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeExit.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolTimeExit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolTimeExit.Caption = "�����";
            this.bcolTimeExit.ColumnEdit = this.rteTimeEntry;
            this.bcolTimeExit.DisplayFormat.FormatString = "t";
            this.bcolTimeExit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bcolTimeExit.FieldName = "TimeExit";
            this.bcolTimeExit.Name = "bcolTimeExit";
            this.bcolTimeExit.OptionsColumn.AllowEdit = false;
            this.bcolTimeExit.OptionsColumn.ReadOnly = true;
            this.bcolTimeExit.RowIndex = 1;
            this.bcolTimeExit.Visible = true;
            this.bcolTimeExit.Width = 115;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "����������";
            this.gridBand6.Columns.Add(this.bcolRemark);
            this.gridBand6.Columns.Add(this.bcolAlarm);
            this.gridBand6.MinWidth = 20;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.OptionsBand.ShowCaption = false;
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 188;
            // 
            // bcolRemark
            // 
            this.bcolRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolRemark.Caption = "����������";
            this.bcolRemark.FieldName = "Remark";
            this.bcolRemark.Name = "bcolRemark";
            this.bcolRemark.Visible = true;
            this.bcolRemark.Width = 188;
            // 
            // bcolAlarm
            // 
            this.bcolAlarm.AppearanceHeader.Options.UseTextOptions = true;
            this.bcolAlarm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bcolAlarm.Caption = "��������� ��������";
            this.bcolAlarm.ColumnEdit = this.rleAlarm;
            this.bcolAlarm.FieldName = "md_alarming_types";
            this.bcolAlarm.Name = "bcolAlarm";
            this.bcolAlarm.RowIndex = 1;
            this.bcolAlarm.Visible = true;
            this.bcolAlarm.Width = 188;
            // 
            // rleAlarm
            // 
            this.rleAlarm.AutoHeight = false;
            this.rleAlarm.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleAlarm.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleAlarm.DisplayMember = "Name";
            this.rleAlarm.Name = "rleAlarm";
            this.rleAlarm.NullText = "";
            // 
            // rleTObject
            // 
            this.rleTObject.AutoHeight = false;
            this.rleTObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleTObject.DisplayMember = "Name";
            this.rleTObject.Name = "rleTObject";
            this.rleTObject.NullText = "";
            this.rleTObject.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiRefresh,
            this.bbiRefuse,
            this.bbiStart,
            this.bbiLog,
            this.bbiRecalc});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 7;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(690, 118);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRecalc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefuse, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiLog, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "���������";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "��������";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "������������ �����";
            this.bbiStart.Enabled = false;
            this.bbiStart.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStart.Glyph")));
            this.bbiStart.Id = 4;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // bbiRecalc
            // 
            this.bbiRecalc.Caption = "�����������";
            this.bbiRecalc.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRecalc.Glyph")));
            this.bbiRecalc.Id = 6;
            this.bbiRecalc.Name = "bbiRecalc";
            this.bbiRecalc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalc_ItemClick);
            // 
            // bbiRefuse
            // 
            this.bbiRefuse.Caption = "����� �� ������";
            this.bbiRefuse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefuse.Glyph")));
            this.bbiRefuse.Id = 3;
            this.bbiRefuse.Name = "bbiRefuse";
            this.bbiRefuse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefuse_ItemClick);
            // 
            // bbiLog
            // 
            this.bbiLog.Caption = "������ �������";
            this.bbiLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLog.Glyph")));
            this.bbiLog.Id = 5;
            this.bbiLog.Name = "bbiLog";
            this.bbiLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLog_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1370, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 521);
            this.barDockControlBottom.Size = new System.Drawing.Size(1370, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 497);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1370, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 497);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 544);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� - �����";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderForm_FormClosed);
            this.Load += new System.EventHandler(this.OrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rleTranType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStateOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleObjectView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleWayBillView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOrderT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrderT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abgvOrderT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleTObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleTObjectView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTCargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTimeEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleAlarm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateInit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erObjectCustomer;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erCategory;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPriority;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeDilay;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateCreate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateRefuse;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateDelete;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateExecute;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateStartWork;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRemark;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleObject;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleCategory;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTranType;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rlePriority;
        internal DevExpress.XtraVerticalGrid.PropertyGridControl pgOrder;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erObjectStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit rteTime;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraGrid.GridControl gcOrderT;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTObject;
        internal System.Windows.Forms.BindingSource bsOrderT;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTCargo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txTime;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleWayBill;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchClose;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsClose;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView abgvOrderT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolDistanceToObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeToObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolCargoLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolQtyLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolCargoUnLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolQtyUnLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeUnLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeDelay;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolRemark;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolDistanceToObjectFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeToObjectFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeEnter;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeExit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolAlarm;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleAlarm;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit rteTimeEntry;
        private DevExpress.XtraBars.BarButtonItem bbiRefuse;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erUser;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erState;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleStateOrder;
        private DevExpress.XtraBars.BarButtonItem bbiLog;
        private DevExpress.XtraGrid.Views.Grid.GridView rgleTObjectView;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleTObject;
        private DevExpress.XtraGrid.Columns.GridColumn gcolObject;
        private DevExpress.XtraGrid.Views.Grid.GridView rgleObjectView;
        private DevExpress.XtraGrid.Columns.GridColumn gcolObjectName;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleObject;
        private DevExpress.XtraGrid.Views.Grid.GridView rgleWayBillView;
        private DevExpress.XtraGrid.Columns.GridColumn colWBVehicle;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleWayBill;
        internal DevExpress.XtraVerticalGrid.Rows.EditorRow erWayBill;
        private DevExpress.XtraBars.BarButtonItem bbiRecalc;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleEnterprise;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erEnterprise;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bcolTimeDelayInPath;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
    }
}