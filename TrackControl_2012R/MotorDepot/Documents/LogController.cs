﻿using System;
using System.Linq;
using MotorDepot.MonitoringGps;
using TrackControl.General;
using TrackControl.Online;
using TrackControl.Vehicles;
using MotorDepot.AccessObjects;  

namespace MotorDepot
{
    public static class LogController
    {
        public static bool SetLogRecord(OnlineVehicle ov, string parMessage)
        {

            waybill wb = EntityFrameworkModelConverter.GetWayBillOnlineVehicle(ov);
            if (wb == null) return false;
            return LogProvider.SetLogRecord(ov, wb, parMessage); 
        }


        internal static void SetViewDataSource(MainMD view)
        {
            DateTime start = DateTime.Today;
            DateTime end = start.AddDays(1).AddMinutes(-1);
            var ent = MDController.Instance.ContextMotorDepot.logSet.Include("md_order").Include("md_waybill").Where(
                    l =>
                    l.DateEvent >= start && l.DateEvent <= end && (!l.md_waybill.IsClose || l.md_waybill == null)
                   && (!l.md_order.IsClose || l.md_order == null));
                if (!UserBaseCurrent.Instance.Admin)
                    ent = ent.Where(l => l.md_waybill.md_enterprise.Id == MdUser.Instance.Enterprise.Id);
                view.gcLog.DataSource = ent.OrderByDescending(l => l.DateEvent).ToList();

        }

        public static bool SetLogRecord( ordert ordetRecord, string parValue, string parMessage, GpsData gpsData)
        {
            ordert ordt = MDController.Instance.ContextMotorDepot.ordertSet.Include("md_object").Where(odt => odt.Id == ordetRecord.Id).FirstOrDefault();
            order ord = MDController.Instance.ContextMotorDepot.orderSet.Include("md_waybill").Where(od => od.Id == ordetRecord.md_order.Id).FirstOrDefault();
            if (ordt == null || ord==null) return false;
            MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ordt);
            MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ordt.md_object);
            MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ord);
            MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, ord.md_waybill);
            if (MDController.Instance.ContextMotorDepot.logSet.Where(l => l.IdOrderT == ordt.Id && l.ParamMessage == parMessage).Count() == 0)
            {
                    log logRecord = new log();
                    logRecord.DateEvent = gpsData.Time;
                    logRecord.md_object = ordt.md_object;
                    logRecord.md_waybill = ord.md_waybill;
                    logRecord.md_order = ord;
                    logRecord.ParamValue = parValue;
                    logRecord.ParamMessage = parMessage;
                    logRecord.IdDataGps = gpsData.Id;
                    logRecord.IdOrderT = ordt.Id;
                    logRecord.IsRead = false;
                    MDController.Instance.ContextMotorDepot.AddTologSet(logRecord);
                    MDController.Instance.MotorDepotEntitiesSaveChanges(logRecord);
                return true;
            }
            return false;
        }
    }
}
