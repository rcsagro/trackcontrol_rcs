using System;
using System.Windows.Forms;
using MotorDepot.AccessObjects;
using MotorDepot.DifferentSheets;
using MotorDepot.Reports;
using TrackControl.General;
using DevExpress.XtraEditors;

namespace MotorDepot
{
    public partial class WayBillForm : DevExpress.XtraEditors.XtraForm, IAccessFormControls
    {
        public event Action<waybill> SaveWayBill;
        public event VoidHandler WayBillViewClosed;
        bool _IsReadOnly;

        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; }
        }

        public WayBillForm()
        {
            InitializeComponent();
            SetAccessRules();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshWayBill();
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (_IsReadOnly) return;
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            bool newWayBill = false;
            if (wbHeader.vehicle == null) return;
            if (wbHeader.Id == 0)
            {
                if (!CheckVehicleHasUnClosedWb() || !CheckDriverHasUnClosedWb())
                {
                    bbiSave.Enabled = false;
                    return;
                }
                newWayBill = true;
            }

            if (newWayBill)
            {
                waybill wbHeaderNew = WayBillController.SaveItem(wbHeader);
                pgWayBill.SelectedObject = wbHeaderNew;
            }
            else
            {
                SaveWayBill(wbHeader);
                RefreshWayBill();
            }
            bbiSave.Enabled = false;
            
        }

        private void bbiRecalc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RecalcWayBill();
        }

        private void RecalcWayBill()
        {
            if (pgWayBill.SelectedObject != null)
            {
                WayBillController.RecalcWayBill(pgWayBill.SelectedObject as waybill, true);
                RefreshWayBill();
            }
        }
        
        void RefreshWayBill()
        {
            pgWayBill.Refresh();
            bgvWbOrders.RefreshData();
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            if (wbHeader != null) bsWbOrders.DataSource = RepCustomerData.GetData(wbHeader);
            gvDelays.RefreshData();  
        }

        private void pgWayBill_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            switch (e.Row.Name)
            {
                case "erShift":
                    {
                        if (wbHeader.md_working_shift != null) 
                        {

                            wbHeader.DinnerTime = (decimal)wbHeader.md_working_shift.DinnerEnd.Subtract(wbHeader.md_working_shift.DinnerStart).TotalHours;
                        }
                        break;
                    }
                case "erIsClose":
                    {
                        if (wbHeader.IsClose)
                        {
                            WayBillController.CloseWayBill(wbHeader); 
                            RecalcWayBill();
                        }
                        break;
                    }
            }
            if (wbHeader!=null && wbHeader.md_enterprise != null  && wbHeader.driver  != null  && wbHeader.vehicle  != null )
            bbiSave.Enabled = true;
        }
        
        private bool CheckVehicleHasUnClosedWb()
        {
            vehicle entityVeh = erVehicle.Properties.Value as vehicle;
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            if (entityVeh == null || wbHeader == null) return true;
            waybill vehWbUnClosed;
            if (WayBillController.IsVehicleHasUnClosedWayBill(entityVeh, wbHeader,out vehWbUnClosed))
            {
                if (DialogResult.Yes ==  XtraMessageBox.Show(
                   string.Format("��������� {0} ����� ���������� ������� ���� � {1} {2}������� �� ������� ���� � {1}?", entityVeh.NumberPlate,
                   vehWbUnClosed.Id, Environment.NewLine), "�� ���� ��������� ���� �������� ������� ����!",MessageBoxButtons.YesNo))
                {
                    WayBillController.DeleteItem(wbHeader,true); 
                    WayBillController.GetDocumentData(vehWbUnClosed,this);
                }
                else
                {
                erVehicle.Properties.Value =null;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckDriverHasUnClosedWb()
        {
            driver entityDrv = erDriver.Properties.Value as driver;
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            if (entityDrv == null || wbHeader == null) return true;
            waybill drvWbUnClosed;
            if (entityDrv != null && WayBillController.IsDriverHasUnClosedWayBill(entityDrv, wbHeader, out drvWbUnClosed))
            {

                if (DialogResult.Yes == XtraMessageBox.Show(
                   string.Format("�������� {0} ����� ���������� ������� ���� � {1} {2}������� �� ������� ���� � {1}?", entityDrv.FullName,
                   drvWbUnClosed.Id, Environment.NewLine), "�������� ��� �����!", MessageBoxButtons.YesNo))
                {
                    WayBillController.DeleteItem(wbHeader, true);
                    WayBillController.GetDocumentData(drvWbUnClosed, this);
                }
                else
                {
                    erDriver.Properties.Value = null;
                }
                return false;


            }
            else
            {
                return true;
            }
        }

        private void WayBillForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ResetBlock();
            if (WayBillViewClosed != null) WayBillViewClosed();
        }

        // ������ �������� �����
        private void buttonPrintWayPage_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            WayReportPageOne reportWayPageOne = new WayReportPageOne();
            waybill Row = ( MotorDepot.waybill )pgWayBill.SelectedObject;
            reportWayPageOne.setDataForWaySheet( Row );
            reportWayPageOne.PrintingSystem.PageMargins.Bottom = 10;
            reportWayPageOne.PrintingSystem.PageMargins.Top = 20;
            reportWayPageOne.CreateDocument();

            WayReportPageTwo reportWayPageTwo = new WayReportPageTwo();
            reportWayPageTwo.CreateDocument();

            reportWayPageOne.Pages.AddRange( reportWayPageTwo.Pages );
            reportWayPageOne.PrintingSystem.ContinuousPageNumbering = true;
            //reportWayPageOne.ShowPreview(); aketner
        } // buttonPrintWayPage_ItemClick

        #region ����� IAccessFormControls

        public void SetAccessRules()
        {
            if (!MdUser.Instance.IsObjectEditableForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd))
            {
                SetReadOnly();
            }
        }

        public void SetReadOnly()
        {
            pgWayBill.OptionsBehavior.Editable = false;
            gvDelays.OptionsBehavior.Editable = false; 
            bbiSave.Enabled = false;
            bbiRecalc.Enabled = false;
            _IsReadOnly = true;
        }


        #endregion

        private void SaveWbDelayRecord(int rowHandle, string columnFieldName)
        {
            waybill wb = (waybill)pgWayBill.SelectedObject;
            if (wb != null)
            {
                waybill_delays wbDelayTableRecord = gvDelays.GetRow(rowHandle) as waybill_delays;
                switch (columnFieldName)
                {
                    case "vehicle_state":
                        {
                            wbDelayTableRecord.md_waybill = wb;
                            WayBillController.SetEndData(wbDelayTableRecord);
                            break;
                        }
                    case "Start":
                        {
                            WayBillController.SetEndData(wbDelayTableRecord);
                            WayBillController.SetDelaysHours(wbDelayTableRecord);
                            break;
                        }
                    case "End":
                        {
                            WayBillController.SetDelaysHours(wbDelayTableRecord);
                            break;
                        }

                }
                WayBillController.SaveDelay(wbDelayTableRecord);
            }
        }

        private void gvDelays_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (gvDelays.GetRowCellValue(e.RowHandle, colDState) != null)
            SaveWbDelayRecord(e.RowHandle, e.Column.FieldName.ToString()); 
        }

        private void gvDelays_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            waybill wb = (waybill)pgWayBill.SelectedObject;
            if (wb != null)
            {
                DateTime start = DateTime.Now;
                DateTime end = DateTime.Now;
                if (wb.Date != null)
                {
                    start = new DateTime(wb.Date.Year, wb.Date.Month, wb.Date.Day , DateTime.Now.Hour, DateTime.Now.Minute,0);  
                }
                gvDelays.SetRowCellValue(e.RowHandle, colDStart, start);
            }
        }


        private void bgvWbOrders_DoubleClick(object sender, EventArgs e)
        {
            if (bgvWbOrders.GetRowCellValue(bgvWbOrders.FocusedRowHandle, colOrdId) == null) return;
            int orderId = 0;
            if (Int32.TryParse(bgvWbOrders.GetRowCellValue(bgvWbOrders.FocusedRowHandle, colOrdId).ToString(), out orderId))
            {
                MDController.Instance.OrderViewOrder(orderId);
            }
        }

        void ResetBlock()
        {
            waybill wbHeader = (waybill)pgWayBill.SelectedObject;
            if (wbHeader.BlockUserId == UserBaseCurrent.Instance.Id)
            {
                wbHeader.BlockUserId = 0;
                wbHeader.BlockDate = null;
                WayBillProvider.UpdateBlockState(wbHeader);
            }
        }



    }
}