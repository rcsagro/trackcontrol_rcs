﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MotorDepot;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification;
using TrackControl.Online;
using TrackControl.Vehicles;
using TrackControl.Reports;
using MotorDepot.MonitoringGps;

namespace MotorDepot.Documents.WayBill
{
    public class WayBillFactCalculator : IDisposable 
    {
        waybill _wb;
        public WayBillFactCalculator(waybill wb)
        {
            _wb = wb;
        }

        public bool SetFactData()
        {
            if (!TestDateStart() || !TestDateEnd()) return false;
            if (_wb.DateStart > _wb.DateEnd)
            {
                XtraMessageBox.Show("Дата начала не может быть больше даты окончания!", MDController.MesCaption);
                return false;
            }
            SetPathTime();
            SetTariff();
            vehicle vh = _wb.vehicle;
            WayBillProvider.Save(_wb);  
            //MDController.Instance.MotorDepotEntitiesSaveChanges(_wb);
            return true;
        }

        private void SetTariff()
        {
            if (_wb.TotalPath > 0)
            {
                if (_wb.vehicle.vehicle_category == null)
                {
                    XtraMessageBox.Show("Укажите категорию транспорта из путевого листа!", MDController.MesCaption);
                    return;
                }
                MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, MDController.Instance.ContextMotorDepot.tariffSet);
                var tar = MDController.Instance.ContextMotorDepot.tariffSet.Where(t => t.vehicle_category.Id == _wb.vehicle.vehicle_category.Id
                    && t.DateIssue <= _wb.Date && t.LimitKm >= _wb.TotalPath).OrderByDescending(t => t.DateIssue).OrderBy(t => t.LimitKm).FirstOrDefault()  ;
                if (tar == null)
                {
                    XtraMessageBox.Show(string.Format("Для транспорта путевого листа № {0} нет тарифа для общего пути {1} км!", _wb.Id, _wb.TotalPath), MDController.MesCaption);
                    _wb.md_tariff = null;
                }
                else
                    _wb.md_tariff = (tariff)tar;
            }
        }

        private void SetPathTime()
        {
            using (DriverDb driverDb = new DriverDb())
            {
                driverDb.ConnectDb(); 
                GpsDataProvider gpsProvider = new GpsDataProvider(driverDb);
                Vehicle vh = EntityFrameworkModelConverter.ConvertVehicleEfToModel(_wb.vehicle);
                if (vh != null)
                {
                    IList<GpsData> gpsData = gpsProvider.GetValidDataForPeriod(vh.MobitelId, (DateTime)_wb.DateStart, (DateTime)_wb.DateEnd);
                    _wb.TotalPath = GetDistance(gpsData);
                    _wb.TotalTimeStop = Math.Round(BasicMethods.GetTotalStopsTime(gpsData.ToArray(), 0).TotalHours,2);
                }
            }
        }

        private bool TestDateStart()
        {
            if (_wb.DateStart == null)
            {
                DateTime dateStart = _wb.Date;
                if (_wb.md_working_shift != null)
                {
                    dateStart = dateStart.AddHours(_wb.md_working_shift.Start.TotalHours);
                }
                else
                {
                    DateTime? dateFromOrders = GetFirstStartDateOrder();
                    if (dateFromOrders != null)
                        dateStart = (DateTime)dateFromOrders;
                }
                
                if (dateStart == null)
                {
                    XtraMessageBox.Show(string.Format("Установите дату начала работы по путевому листу № {0}!", _wb.Id),
                    MDController.MesCaption);
                    return false;
                }
                else
                {
                    _wb.DateStart = dateStart;
                    return true;
                }
            }
            else
                return true;
        }

        private bool TestDateEnd()
        {
            if (_wb.DateEnd == null)
            {

                DateTime dateEnd = _wb.Date;
                if (_wb.md_working_shift != null)
                {
                    dateEnd = dateEnd.AddHours(_wb.md_working_shift.End.TotalHours);
                }
                else
                {
                    DateTime? dateFromOrders = GetLastExecuteDateOrder();
                    if (dateFromOrders != null)
                        dateEnd = (DateTime)dateFromOrders;
                }

                if (dateEnd == null)
                {
                    XtraMessageBox.Show(string.Format("Установите дату окончания работы по путевому листу № {0}!", _wb.Id),
                      MDController.MesCaption);
                    return false;
                }
                else
                {
                    _wb.DateEnd = dateEnd;
                    return true;
                }
            }
            else
                return true;
        }

        DateTime? GetFirstStartDateOrder()
        {
            order odFirst = MDController.Instance.ContextMotorDepot.orderSet.Where(od => od.md_waybill.Id == _wb.Id && od.DateStartWork != null).OrderBy(od => od.DateStartWork).FirstOrDefault();
            if (odFirst != null)
                return odFirst.DateStartWork;
            else
                return null;
        }

        DateTime? GetLastExecuteDateOrder()
        {
            order odLast = MDController.Instance.ContextMotorDepot.orderSet.Where(od => od.md_waybill.Id == _wb.Id && od.DateExecute != null).OrderByDescending(od => od.DateExecute).FirstOrDefault();
            if (odLast != null)
                return odLast.DateExecute;
            else
                return null;
        }

        private double GetDistance(IList<GpsData> gpsData)
        {
            List<PointLatLng> points = new List<PointLatLng>();
            foreach (GpsData dataGps in gpsData)
            {
                points.Add(dataGps.LatLng);
            }
            return Math.Round(СGeoDistance.GetDistance(points), 2);
        }

        #region Члены IDisposable

        public void Dispose()
        {
            _wb = null;
        }

        #endregion
    }
}
