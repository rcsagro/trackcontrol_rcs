namespace MotorDepot
{
    partial class WayBillForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraVerticalGrid.Rows.EditorRow erShift;
            DevExpress.XtraVerticalGrid.Rows.EditorRow erEnterprise;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WayBillForm));
            this.erDinnerTime = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rleShift = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rleEnterprise = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pgWayBill = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.rleObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rlePriority = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rdeDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rteTime = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.txTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.rleWayBill = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rchClose = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.rleStateOrder = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rgleVehicle = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rgleDriver = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleTariff = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.erId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erVehicle = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDriver = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsClose = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erBusinessTrip = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDateEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTotalPath = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTotalTimeStop = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTariff = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcDelays = new DevExpress.XtraGrid.GridControl();
            this.bsWbDelays = new System.Windows.Forms.BindingSource();
            this.gvDelays = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleVehStates = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rdeDateDelay = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colDEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDRem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.buttonPrintWayPage = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRecalc = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gcWbOrders = new DevExpress.XtraGrid.GridControl();
            this.bsWbOrders = new System.Windows.Forms.BindingSource();
            this.bgvWbOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOrdId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdState = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ricbStates = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imOrderStates = new DevExpress.Utils.ImageCollection();
            this.colOrdNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdCustomer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCargoName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCargoQty = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeUnLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactTimeInObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalOrderTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalPath = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStopsToObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPricePath = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleOrdObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            erShift = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            erEnterprise = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.rleShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStateOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTariff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDelays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWbDelays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDelays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateDelay.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWbOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWbOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvWbOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrdObject)).BeginInit();
            this.SuspendLayout();
            // 
            // erShift
            // 
            erShift.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erDinnerTime});
            erShift.Name = "erShift";
            erShift.Properties.Caption = "�����";
            erShift.Properties.FieldName = "md_working_shift";
            erShift.Properties.RowEdit = this.rleShift;
            // 
            // erDinnerTime
            // 
            this.erDinnerTime.Name = "erDinnerTime";
            this.erDinnerTime.Properties.Caption = "����� �����, �";
            this.erDinnerTime.Properties.FieldName = "DinnerTime";
            // 
            // rleShift
            // 
            this.rleShift.AutoHeight = false;
            this.rleShift.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleShift.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleShift.DisplayMember = "Name";
            this.rleShift.Name = "rleShift";
            this.rleShift.NullText = "";
            // 
            // erEnterprise
            // 
            erEnterprise.Height = 19;
            erEnterprise.Name = "erEnterprise";
            erEnterprise.Properties.Caption = "�����������";
            erEnterprise.Properties.FieldName = "md_enterprise";
            erEnterprise.Properties.RowEdit = this.rleEnterprise;
            // 
            // rleEnterprise
            // 
            this.rleEnterprise.AutoHeight = false;
            this.rleEnterprise.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleEnterprise.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleEnterprise.DisplayMember = "Name";
            this.rleEnterprise.Name = "rleEnterprise";
            this.rleEnterprise.NullText = "";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcWbOrders);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1377, 741);
            this.splitContainerControl1.SplitterPosition = 301;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.pgWayBill);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gcDelays);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1377, 301);
            this.splitContainerControl2.SplitterPosition = 306;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // pgWayBill
            // 
            this.pgWayBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgWayBill.Location = new System.Drawing.Point(0, 0);
            this.pgWayBill.Name = "pgWayBill";
            this.pgWayBill.RecordWidth = 88;
            this.pgWayBill.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleObject,
            this.rleShift,
            this.rleEnterprise,
            this.rlePriority,
            this.rdeDate,
            this.rteTime,
            this.txTime,
            this.rleWayBill,
            this.rchClose,
            this.rleStateOrder,
            this.rgleVehicle,
            this.rgleDriver,
            this.rleTariff});
            this.pgWayBill.RowHeaderWidth = 112;
            this.pgWayBill.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erId,
            this.erDate,
            this.erVehicle,
            this.erDriver,
            erShift,
            erEnterprise,
            this.erRemark,
            this.erIsClose,
            this.erBusinessTrip,
            this.erDateStart,
            this.erDateEnd,
            this.erTotalPath,
            this.erTotalTimeStop,
            this.erTariff});
            this.pgWayBill.Size = new System.Drawing.Size(306, 301);
            this.pgWayBill.TabIndex = 1;
            this.pgWayBill.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgWayBill_CellValueChanged);
            // 
            // rleObject
            // 
            this.rleObject.AutoHeight = false;
            this.rleObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleObject.DisplayMember = "Name";
            this.rleObject.Name = "rleObject";
            this.rleObject.NullText = "";
            // 
            // rlePriority
            // 
            this.rlePriority.AutoHeight = false;
            this.rlePriority.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rlePriority.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rlePriority.DisplayMember = "Name";
            this.rlePriority.Name = "rlePriority";
            this.rlePriority.NullText = "";
            // 
            // rdeDate
            // 
            this.rdeDate.AutoHeight = false;
            this.rdeDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.DisplayFormat.FormatString = "g";
            this.rdeDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDate.EditFormat.FormatString = "g";
            this.rdeDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDate.Mask.EditMask = "g";
            this.rdeDate.Name = "rdeDate";
            this.rdeDate.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // rteTime
            // 
            this.rteTime.AutoHeight = false;
            this.rteTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rteTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.rteTime.DisplayFormat.FormatString = "hh:mm";
            this.rteTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.rteTime.Mask.EditMask = "hh:mm";
            this.rteTime.Name = "rteTime";
            // 
            // txTime
            // 
            this.txTime.AutoHeight = false;
            this.txTime.EditFormat.FormatString = "t";
            this.txTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txTime.Mask.EditMask = "90:00";
            this.txTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txTime.Mask.UseMaskAsDisplayFormat = true;
            this.txTime.Name = "txTime";
            // 
            // rleWayBill
            // 
            this.rleWayBill.AutoHeight = false;
            this.rleWayBill.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleWayBill.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VehicleForOrder", "���������")});
            this.rleWayBill.DisplayMember = "VehicleForOrder";
            this.rleWayBill.Name = "rleWayBill";
            this.rleWayBill.NullText = "";
            // 
            // rchClose
            // 
            this.rchClose.AutoHeight = false;
            this.rchClose.Name = "rchClose";
            // 
            // rleStateOrder
            // 
            this.rleStateOrder.AutoHeight = false;
            this.rleStateOrder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleStateOrder.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleStateOrder.DisplayMember = "Name";
            this.rleStateOrder.Name = "rleStateOrder";
            this.rleStateOrder.NullText = "";
            // 
            // rgleVehicle
            // 
            this.rgleVehicle.AutoHeight = false;
            this.rgleVehicle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleVehicle.DisplayMember = "NumberPlate";
            this.rgleVehicle.Name = "rgleVehicle";
            this.rgleVehicle.NullText = "";
            this.rgleVehicle.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleVehicle.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleVehicle.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolVehicle});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gcolVehicle, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gcolVehicle
            // 
            this.gcolVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.gcolVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolVehicle.Caption = "���������";
            this.gcolVehicle.FieldName = "NumberPlate";
            this.gcolVehicle.Name = "gcolVehicle";
            this.gcolVehicle.Visible = true;
            this.gcolVehicle.VisibleIndex = 0;
            // 
            // rgleDriver
            // 
            this.rgleDriver.AutoHeight = false;
            this.rgleDriver.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rgleDriver.DisplayMember = "Family";
            this.rgleDriver.Name = "rgleDriver";
            this.rgleDriver.NullText = "";
            this.rgleDriver.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.rgleDriver.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.rgleDriver.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolDriver});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gcolDriver, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gcolDriver
            // 
            this.gcolDriver.AppearanceHeader.Options.UseTextOptions = true;
            this.gcolDriver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolDriver.Caption = "��������";
            this.gcolDriver.FieldName = "Family";
            this.gcolDriver.Name = "gcolDriver";
            this.gcolDriver.Visible = true;
            this.gcolDriver.VisibleIndex = 0;
            // 
            // rleTariff
            // 
            this.rleTariff.AutoHeight = false;
            this.rleTariff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleTariff.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "���� ���������")});
            this.rleTariff.DisplayMember = "FullName";
            this.rleTariff.Name = "rleTariff";
            this.rleTariff.NullText = "";
            // 
            // erId
            // 
            this.erId.Name = "erId";
            this.erId.Properties.Caption = "�����";
            this.erId.Properties.FieldName = "Id";
            this.erId.Properties.ReadOnly = true;
            // 
            // erDate
            // 
            this.erDate.Name = "erDate";
            this.erDate.Properties.Caption = "����";
            this.erDate.Properties.FieldName = "Date";
            this.erDate.Properties.ReadOnly = true;
            // 
            // erVehicle
            // 
            this.erVehicle.Name = "erVehicle";
            this.erVehicle.Properties.Caption = "���������";
            this.erVehicle.Properties.FieldName = "vehicle";
            this.erVehicle.Properties.RowEdit = this.rgleVehicle;
            // 
            // erDriver
            // 
            this.erDriver.Height = 19;
            this.erDriver.Name = "erDriver";
            this.erDriver.Properties.Caption = "��������";
            this.erDriver.Properties.FieldName = "driver";
            this.erDriver.Properties.RowEdit = this.rgleDriver;
            // 
            // erRemark
            // 
            this.erRemark.Name = "erRemark";
            this.erRemark.Properties.Caption = "����������";
            this.erRemark.Properties.FieldName = "Remark";
            // 
            // erIsClose
            // 
            this.erIsClose.Name = "erIsClose";
            this.erIsClose.Properties.Caption = "������";
            this.erIsClose.Properties.FieldName = "IsClose";
            this.erIsClose.Properties.RowEdit = this.rchClose;
            // 
            // erBusinessTrip
            // 
            this.erBusinessTrip.Name = "erBusinessTrip";
            this.erBusinessTrip.Properties.Caption = "������������";
            this.erBusinessTrip.Properties.FieldName = "IsBusinessTrip";
            this.erBusinessTrip.Properties.RowEdit = this.rchClose;
            // 
            // erDateStart
            // 
            this.erDateStart.Name = "erDateStart";
            this.erDateStart.Properties.Caption = "���� ������";
            this.erDateStart.Properties.FieldName = "DateStart";
            this.erDateStart.Properties.RowEdit = this.rdeDate;
            // 
            // erDateEnd
            // 
            this.erDateEnd.Name = "erDateEnd";
            this.erDateEnd.Properties.Caption = "���� ���������";
            this.erDateEnd.Properties.FieldName = "DateEnd";
            this.erDateEnd.Properties.RowEdit = this.rdeDate;
            // 
            // erTotalPath
            // 
            this.erTotalPath.Height = 16;
            this.erTotalPath.Name = "erTotalPath";
            this.erTotalPath.Properties.Caption = "����� ����, ��";
            this.erTotalPath.Properties.FieldName = "TotalPath";
            this.erTotalPath.Properties.ReadOnly = true;
            // 
            // erTotalTimeStop
            // 
            this.erTotalTimeStop.Height = 16;
            this.erTotalTimeStop.Name = "erTotalTimeStop";
            this.erTotalTimeStop.Properties.Caption = "����� ����� �������, �";
            this.erTotalTimeStop.Properties.FieldName = "TotalTimeStop";
            this.erTotalTimeStop.Properties.ReadOnly = true;
            // 
            // erTariff
            // 
            this.erTariff.Name = "erTariff";
            this.erTariff.Properties.Caption = "�����";
            this.erTariff.Properties.FieldName = "md_tariff";
            this.erTariff.Properties.ReadOnly = true;
            this.erTariff.Properties.RowEdit = this.rleTariff;
            // 
            // gcDelays
            // 
            this.gcDelays.DataSource = this.bsWbDelays;
            this.gcDelays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDelays.Location = new System.Drawing.Point(0, 0);
            this.gcDelays.MainView = this.gvDelays;
            this.gcDelays.MenuManager = this.barManager1;
            this.gcDelays.Name = "gcDelays";
            this.gcDelays.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleVehStates,
            this.rdeDateDelay});
            this.gcDelays.Size = new System.Drawing.Size(1065, 301);
            this.gcDelays.TabIndex = 0;
            this.gcDelays.UseEmbeddedNavigator = true;
            this.gcDelays.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDelays});
            // 
            // gvDelays
            // 
            this.gvDelays.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDId,
            this.colDState,
            this.colDStart,
            this.colDEnd,
            this.colDTime,
            this.colDRem});
            this.gvDelays.GridControl = this.gcDelays;
            this.gvDelays.Name = "gvDelays";
            this.gvDelays.OptionsView.ShowGroupPanel = false;
            this.gvDelays.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvDelays_InitNewRow);
            this.gvDelays.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvDelays_CellValueChanged);
            // 
            // colDId
            // 
            this.colDId.AppearanceHeader.Options.UseTextOptions = true;
            this.colDId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDId.Caption = "Id";
            this.colDId.FieldName = "Id";
            this.colDId.Name = "colDId";
            // 
            // colDState
            // 
            this.colDState.AppearanceHeader.Options.UseTextOptions = true;
            this.colDState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDState.Caption = "��������� ����������";
            this.colDState.ColumnEdit = this.rleVehStates;
            this.colDState.FieldName = "vehicle_state";
            this.colDState.Name = "colDState";
            this.colDState.Visible = true;
            this.colDState.VisibleIndex = 0;
            this.colDState.Width = 390;
            // 
            // rleVehStates
            // 
            this.rleVehStates.AutoHeight = false;
            this.rleVehStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleVehStates.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleVehStates.DisplayMember = "Name";
            this.rleVehStates.Name = "rleVehStates";
            this.rleVehStates.NullText = "";
            // 
            // colDStart
            // 
            this.colDStart.AppearanceCell.Options.UseTextOptions = true;
            this.colDStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colDStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDStart.Caption = "������";
            this.colDStart.ColumnEdit = this.rdeDateDelay;
            this.colDStart.FieldName = "Start";
            this.colDStart.Name = "colDStart";
            this.colDStart.Visible = true;
            this.colDStart.VisibleIndex = 1;
            this.colDStart.Width = 200;
            // 
            // rdeDateDelay
            // 
            this.rdeDateDelay.AutoHeight = false;
            this.rdeDateDelay.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDateDelay.DisplayFormat.FormatString = "g";
            this.rdeDateDelay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDateDelay.EditFormat.FormatString = "g";
            this.rdeDateDelay.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rdeDateDelay.Mask.EditMask = "g";
            this.rdeDateDelay.Name = "rdeDateDelay";
            this.rdeDateDelay.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // colDEnd
            // 
            this.colDEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colDEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colDEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDEnd.Caption = "���������";
            this.colDEnd.ColumnEdit = this.rdeDateDelay;
            this.colDEnd.FieldName = "End";
            this.colDEnd.Name = "colDEnd";
            this.colDEnd.Visible = true;
            this.colDEnd.VisibleIndex = 2;
            this.colDEnd.Width = 186;
            // 
            // colDTime
            // 
            this.colDTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colDTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDTime.Caption = "����� �����";
            this.colDTime.FieldName = "Hours";
            this.colDTime.Name = "colDTime";
            this.colDTime.Visible = true;
            this.colDTime.VisibleIndex = 3;
            this.colDTime.Width = 134;
            // 
            // colDRem
            // 
            this.colDRem.AppearanceHeader.Options.UseTextOptions = true;
            this.colDRem.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDRem.Caption = "����������";
            this.colDRem.FieldName = "Remark";
            this.colDRem.Name = "colDRem";
            this.colDRem.Visible = true;
            this.colDRem.VisibleIndex = 4;
            this.colDRem.Width = 340;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiRefresh,
            this.buttonPrintWayPage,
            this.bbiRecalc});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.buttonPrintWayPage, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRecalc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "���������";
            this.bbiSave.Enabled = false;
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "��������";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // buttonPrintWayPage
            // 
            this.buttonPrintWayPage.Caption = "������";
            this.buttonPrintWayPage.Glyph = ((System.Drawing.Image)(resources.GetObject("buttonPrintWayPage.Glyph")));
            this.buttonPrintWayPage.Hint = "������ �������� �����";
            this.buttonPrintWayPage.Id = 2;
            this.buttonPrintWayPage.Name = "buttonPrintWayPage";
            this.buttonPrintWayPage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.buttonPrintWayPage_ItemClick);
            // 
            // bbiRecalc
            // 
            this.bbiRecalc.Caption = "�����������";
            this.bbiRecalc.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRecalc.Glyph")));
            this.bbiRecalc.Id = 3;
            this.bbiRecalc.Name = "bbiRecalc";
            this.bbiRecalc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalc_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // gcWbOrders
            // 
            this.gcWbOrders.DataSource = this.bsWbOrders;
            this.gcWbOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcWbOrders.Location = new System.Drawing.Point(0, 0);
            this.gcWbOrders.MainView = this.bgvWbOrders;
            this.gcWbOrders.MenuManager = this.barManager1;
            this.gcWbOrders.Name = "gcWbOrders";
            this.gcWbOrders.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleOrdObject,
            this.ricbStates});
            this.gcWbOrders.Size = new System.Drawing.Size(1377, 434);
            this.gcWbOrders.TabIndex = 0;
            this.gcWbOrders.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvWbOrders});
            // 
            // bgvWbOrders
            // 
            this.bgvWbOrders.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.bgvWbOrders.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOrdId,
            this.colOrdState,
            this.colOrdNumber,
            this.colOrdDate,
            this.colOrdCustomer,
            this.colCargoName,
            this.colCargoQty,
            this.colTimeLoading,
            this.colTimeUnLoading,
            this.colFactTimeInObject,
            this.colTotalOrderTime,
            this.colTotalPath,
            this.colTimeStop,
            this.colTimeStopsToObject,
            this.colPricePath,
            this.colPriceTimeStop,
            this.colPriceTime,
            this.colPriceTotal});
            this.bgvWbOrders.GridControl = this.gcWbOrders;
            this.bgvWbOrders.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.bgvWbOrders.Name = "bgvWbOrders";
            this.bgvWbOrders.OptionsBehavior.Editable = false;
            this.bgvWbOrders.OptionsView.ShowFooter = true;
            this.bgvWbOrders.DoubleClick += new System.EventHandler(this.bgvWbOrders_DoubleClick);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "���������� � �����-������";
            this.gridBand1.Columns.Add(this.colOrdId);
            this.gridBand1.Columns.Add(this.colOrdState);
            this.gridBand1.Columns.Add(this.colOrdNumber);
            this.gridBand1.Columns.Add(this.colOrdDate);
            this.gridBand1.Columns.Add(this.colOrdCustomer);
            this.gridBand1.Columns.Add(this.colCargoName);
            this.gridBand1.Columns.Add(this.colCargoQty);
            this.gridBand1.Columns.Add(this.colTimeLoading);
            this.gridBand1.Columns.Add(this.colTimeUnLoading);
            this.gridBand1.Columns.Add(this.colFactTimeInObject);
            this.gridBand1.Columns.Add(this.colTotalOrderTime);
            this.gridBand1.Columns.Add(this.colTotalPath);
            this.gridBand1.Columns.Add(this.colTimeStop);
            this.gridBand1.Columns.Add(this.colTimeStopsToObject);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 1000;
            // 
            // colOrdId
            // 
            this.colOrdId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdId.FieldName = "Id";
            this.colOrdId.Name = "colOrdId";
            this.colOrdId.OptionsColumn.AllowEdit = false;
            this.colOrdId.OptionsColumn.ReadOnly = true;
            this.colOrdId.Width = 50;
            // 
            // colOrdState
            // 
            this.colOrdState.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdState.Caption = "������";
            this.colOrdState.ColumnEdit = this.ricbStates;
            this.colOrdState.FieldName = "StateId";
            this.colOrdState.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colOrdState.Name = "colOrdState";
            this.colOrdState.OptionsColumn.AllowEdit = false;
            this.colOrdState.OptionsColumn.ReadOnly = true;
            this.colOrdState.OptionsColumn.ShowCaption = false;
            this.colOrdState.Visible = true;
            this.colOrdState.Width = 20;
            // 
            // ricbStates
            // 
            this.ricbStates.AutoHeight = false;
            this.ricbStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricbStates.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 6, 5)});
            this.ricbStates.Name = "ricbStates";
            this.ricbStates.SmallImages = this.imOrderStates;
            // 
            // imOrderStates
            // 
            this.imOrderStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imOrderStates.ImageStream")));
            this.imOrderStates.Images.SetKeyName(0, "document.png");
            this.imOrderStates.Images.SetKeyName(1, "document-broken.png");
            this.imOrderStates.Images.SetKeyName(2, "document-hf-insert-footer.png");
            this.imOrderStates.Images.SetKeyName(3, "document--exclamation.png");
            this.imOrderStates.Images.SetKeyName(4, "document-task.png");
            this.imOrderStates.Images.SetKeyName(5, "document-hf-delete-footer.png");
            // 
            // colOrdNumber
            // 
            this.colOrdNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.Caption = "�����";
            this.colOrdNumber.FieldName = "Number";
            this.colOrdNumber.Name = "colOrdNumber";
            this.colOrdNumber.OptionsColumn.AllowEdit = false;
            this.colOrdNumber.OptionsColumn.ReadOnly = true;
            this.colOrdNumber.Visible = true;
            this.colOrdNumber.Width = 67;
            // 
            // colOrdDate
            // 
            this.colOrdDate.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.Caption = "����";
            this.colOrdDate.DisplayFormat.FormatString = "g";
            this.colOrdDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colOrdDate.FieldName = "DateCreate";
            this.colOrdDate.Name = "colOrdDate";
            this.colOrdDate.OptionsColumn.AllowEdit = false;
            this.colOrdDate.OptionsColumn.ReadOnly = true;
            this.colOrdDate.Visible = true;
            this.colOrdDate.Width = 97;
            // 
            // colOrdCustomer
            // 
            this.colOrdCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdCustomer.Caption = "��������";
            this.colOrdCustomer.FieldName = "Customer";
            this.colOrdCustomer.Name = "colOrdCustomer";
            this.colOrdCustomer.OptionsColumn.AllowEdit = false;
            this.colOrdCustomer.OptionsColumn.ReadOnly = true;
            this.colOrdCustomer.Visible = true;
            this.colOrdCustomer.Width = 76;
            // 
            // colCargoName
            // 
            this.colCargoName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargoName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoName.Caption = "����";
            this.colCargoName.FieldName = "CargoName";
            this.colCargoName.Name = "colCargoName";
            this.colCargoName.Visible = true;
            this.colCargoName.Width = 50;
            // 
            // colCargoQty
            // 
            this.colCargoQty.AppearanceCell.Options.UseTextOptions = true;
            this.colCargoQty.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoQty.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargoQty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoQty.Caption = "���-��";
            this.colCargoQty.FieldName = "CargoQty";
            this.colCargoQty.Name = "colCargoQty";
            this.colCargoQty.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCargoQty.Visible = true;
            this.colCargoQty.Width = 70;
            // 
            // colTimeLoading
            // 
            this.colTimeLoading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeLoading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoading.Caption = "��������, �";
            this.colTimeLoading.FieldName = "TimeLoading";
            this.colTimeLoading.Name = "colTimeLoading";
            this.colTimeLoading.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeLoading.Visible = true;
            this.colTimeLoading.Width = 80;
            // 
            // colTimeUnLoading
            // 
            this.colTimeUnLoading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeUnLoading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeUnLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnLoading.Caption = "��������, �";
            this.colTimeUnLoading.FieldName = "TimeUnLoading";
            this.colTimeUnLoading.Name = "colTimeUnLoading";
            this.colTimeUnLoading.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeUnLoading.Visible = true;
            this.colTimeUnLoading.Width = 78;
            // 
            // colFactTimeInObject
            // 
            this.colFactTimeInObject.AppearanceCell.Options.UseTextOptions = true;
            this.colFactTimeInObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactTimeInObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactTimeInObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactTimeInObject.Caption = "����� �� �������, �";
            this.colFactTimeInObject.FieldName = "FactTimeInObject";
            this.colFactTimeInObject.Name = "colFactTimeInObject";
            this.colFactTimeInObject.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFactTimeInObject.Visible = true;
            this.colFactTimeInObject.Width = 116;
            // 
            // colTotalOrderTime
            // 
            this.colTotalOrderTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalOrderTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalOrderTime.Caption = "������������ �����, �";
            this.colTotalOrderTime.FieldName = "TotalOrderTime";
            this.colTotalOrderTime.Name = "colTotalOrderTime";
            this.colTotalOrderTime.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalOrderTime.Visible = true;
            this.colTotalOrderTime.Width = 126;
            // 
            // colTotalPath
            // 
            this.colTotalPath.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalPath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPath.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalPath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPath.Caption = "������, ��";
            this.colTotalPath.FieldName = "TotalPath";
            this.colTotalPath.Name = "colTotalPath";
            this.colTotalPath.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalPath.Visible = true;
            this.colTotalPath.Width = 67;
            // 
            // colTimeStop
            // 
            this.colTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.Caption = "�������, �";
            this.colTimeStop.FieldName = "TimeExcess";
            this.colTimeStop.Name = "colTimeStop";
            this.colTimeStop.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStop.Visible = true;
            this.colTimeStop.Width = 71;
            // 
            // colTimeStopsToObject
            // 
            this.colTimeStopsToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopsToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopsToObject.Caption = "�������, �";
            this.colTimeStopsToObject.FieldName = "TimeStopsToObject";
            this.colTimeStopsToObject.Name = "colTimeStopsToObject";
            this.colTimeStopsToObject.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStopsToObject.Visible = true;
            this.colTimeStopsToObject.Width = 82;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "���������, ���";
            this.gridBand2.Columns.Add(this.colPricePath);
            this.gridBand2.Columns.Add(this.colPriceTime);
            this.gridBand2.Columns.Add(this.colPriceTimeStop);
            this.gridBand2.Columns.Add(this.colPriceTotal);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 250;
            // 
            // colPricePath
            // 
            this.colPricePath.AppearanceCell.Options.UseTextOptions = true;
            this.colPricePath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePath.AppearanceHeader.Options.UseTextOptions = true;
            this.colPricePath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePath.Caption = "������";
            this.colPricePath.FieldName = "PricePath";
            this.colPricePath.Name = "colPricePath";
            this.colPricePath.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPricePath.Visible = true;
            this.colPricePath.Width = 59;
            // 
            // colPriceTime
            // 
            this.colPriceTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTime.Caption = "�����";
            this.colPriceTime.FieldName = "PriceTime";
            this.colPriceTime.Name = "colPriceTime";
            this.colPriceTime.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTime.Visible = true;
            this.colPriceTime.Width = 66;
            // 
            // colPriceTimeStop
            // 
            this.colPriceTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTimeStop.Caption = "�������";
            this.colPriceTimeStop.FieldName = "PriceTimeStop";
            this.colPriceTimeStop.Name = "colPriceTimeStop";
            this.colPriceTimeStop.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTimeStop.Visible = true;
            this.colPriceTimeStop.Width = 65;
            // 
            // colPriceTotal
            // 
            this.colPriceTotal.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTotal.Caption = "�����";
            this.colPriceTotal.FieldName = "PriceTotal";
            this.colPriceTotal.Name = "colPriceTotal";
            this.colPriceTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTotal.Visible = true;
            this.colPriceTotal.Width = 60;
            // 
            // rleOrdObject
            // 
            this.rleOrdObject.AutoHeight = false;
            this.rleOrdObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrdObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleOrdObject.DisplayMember = "Name";
            this.rleOrdObject.Name = "rleOrdObject";
            this.rleOrdObject.NullText = "";
            // 
            // WayBillForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1377, 789);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WayBillForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������� ����";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WayBillForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.rleShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleEnterprise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleWayBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleStateOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgleDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleTariff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDelays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWbDelays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDelays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleVehStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateDelay.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWbOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsWbOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvWbOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrdObject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        internal DevExpress.XtraVerticalGrid.PropertyGridControl pgWayBill;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleObject;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleShift;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleEnterprise;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rlePriority;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit rteTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txTime;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleWayBill;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchClose;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleStateOrder;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleVehicle;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcolVehicle;
        internal DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rgleDriver;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gcolDriver;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDriver;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRemark;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsClose;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erBusinessTrip;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        internal DevExpress.XtraVerticalGrid.Rows.EditorRow erVehicle;
        private DevExpress.XtraGrid.GridControl gcWbOrders;
        internal DevExpress.Utils.ImageCollection imOrderStates;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrdObject;
        internal System.Windows.Forms.BindingSource bsWbOrders;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricbStates;
        private DevExpress.XtraBars.BarButtonItem buttonPrintWayPage;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDinnerTime;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gcDelays;
        private DevExpress.XtraGrid.Columns.GridColumn colDId;
        private DevExpress.XtraGrid.Columns.GridColumn colDState;
        private DevExpress.XtraGrid.Columns.GridColumn colDStart;
        private DevExpress.XtraGrid.Columns.GridColumn colDEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colDTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDRem;
        internal System.Windows.Forms.BindingSource bsWbDelays;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDateDelay;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleVehStates;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvDelays;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDateEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTotalPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTotalTimeStop;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTariff;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleTariff;
        private DevExpress.XtraBars.BarButtonItem bbiRecalc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvWbOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdState;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdCustomer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCargoName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCargoQty;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeUnLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactTimeInObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalOrderTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalPath;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStopsToObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPricePath;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
    }
}