﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MotorDepot.AccessObjects;
using MotorDepot.Documents.WayBill;
using MotorDepot.Reports;
using TrackControl.General;

namespace MotorDepot
{
    public static class WayBillController
    {
        internal static MotorDepotEntities ContextMotorDepot;

        internal static void SetViewDataSource(MainMD view)
        {
            int numberWayBill;
            try
            {

                List<waybill> waybills;
                if (view.beiNumber.EditValue != null && int.TryParse(view.beiNumber.EditValue.ToString(), out numberWayBill))
                {
                    waybills = ContextMotorDepot.waybillSet.Where(t => t.Id == numberWayBill).ToList();
                }
                else
                {
                   DateTime start = (DateTime)view.bdeStart.EditValue;
                   DateTime end = (DateTime)view.bdeEnd.EditValue;
                   waybills = ContextMotorDepot.waybillSet.Where(t => t.Date >= start && t.Date <= end).ToList();
                }
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, waybills);
                view.bsWayBill.DataSource = waybills;
                view.rgleVehicle.DataSource = ContextMotorDepot.vehicleSet.ToList();
                ContextMotorDepot.vehicle_stateSet.ToList();
                view.rgleDriver.DataSource = ContextMotorDepot.driverSet.ToList();
                view.rleShift.DataSource = ContextMotorDepot.working_shiftSet.ToList();
                view.rleEnterprise.DataSource = ContextMotorDepot.enterpriseSet.ToList();
            }
            catch (Exception ex)
            {
                StackTrace stckTrace = new StackTrace(ex, true);
                if (stckTrace.FrameCount > 0)
                {
                    StackFrame frame = stckTrace.GetFrame(stckTrace.FrameCount - 1);
                    int errorLine = frame.GetFileLineNumber();
                    string functionName = frame.GetMethod().Name;
                    XtraMessageBox.Show("File WayBillController.cs: " + ex.Message + "\n" +
                        "Error in " + functionName + " method on line " + errorLine);
                }
            }
        }

        internal static bool DeleteItem(waybill wbForDelete, bool withoutQuestion)
        {
            if (!MdUser.Instance.IsObjectEditableForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return false;
            try
            {
                if (wbForDelete != null)
                {
                    if (withoutQuestion || XtraMessageBox.Show("Подтверждаете удаление записи путевого листа?",
                      MDController.MesCaption,
                      MessageBoxButtons.YesNoCancel,
                      MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        int countOrders = OrdersCount(wbForDelete);
                        if (countOrders == 0)
                        {
                            //ContextMotorDepot.DeleteObject(wbForDelete);
                            //MDController.Instance.MotorDepotEntitiesSaveChanges(wbForDelete);
                            WayBillProvider.Delete(wbForDelete);  
                            return true;
                        }
                        else
                        {
                            if (XtraMessageBox.Show(string.Format("Удаление путевого листа № {0} запрещено.Сформировано заказ-нарядов: {1}", wbForDelete.Id, countOrders),
                                      MDController.MesCaption, MessageBoxButtons.OK) != DialogResult.Yes) return false;
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }

        internal static int OrdersCount(waybill wbForTest)
        {
            if (wbForTest == null) return 0;
            return ContextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == wbForTest.Id && ord.md_order_state.Id != (int)OrderStates.Delete).Count();
        }

        internal static waybill SaveItem(waybill wbToSave)
        {
            if (wbToSave != null)
            {
                if (wbToSave.vehicle != null) 
                {
                    wbToSave.vehicle.vehicle_state = ContextMotorDepot.vehicle_stateSet.Where(vs => vs.Id == (int)VehicleStates.OrderWaitng).FirstOrDefault();
                    return  WayBillProvider.Save(wbToSave);  
                }
            }
            return wbToSave;
        }

        internal static void SaveDelay(waybill_delays wbDelayToSave)
        {
            if (wbDelayToSave != null)
            {
                if (wbDelayToSave.Id == 0)
                {
                    ContextMotorDepot.AddTowaybill_delaysSet(wbDelayToSave);
                }
                MDController.Instance.MotorDepotEntitiesSaveChanges(wbDelayToSave);
            }
        }

        internal static void SetDelaysHours(waybill_delays wbDelayToSave)
        {
            if (wbDelayToSave != null)
            {
                DateTime start;
                DateTime end;
                if (DateTime.TryParse(wbDelayToSave.Start.ToString(), out start) && DateTime.TryParse(wbDelayToSave.End.ToString(), out end))
                {
                    double h = end.Subtract(start).TotalHours;
                    if (h > 0 && h < 1000) wbDelayToSave.Hours = Math.Round(h, 1);
                }

            }
        }

        internal static void SetEndData(waybill_delays wbDelayToSave)
        {
            if (wbDelayToSave != null && wbDelayToSave.vehicle_state != null && wbDelayToSave.End == null)
            {
                wbDelayToSave.Hours = (double)wbDelayToSave.vehicle_state.MaxTime;
                wbDelayToSave.End = wbDelayToSave.Start.AddHours(wbDelayToSave.Hours);
                
            }
        }

        internal static bool IsVehicleHasUnClosedWayBill(vehicle vehForTerst, waybill wbEdit, out waybill vehWbUnClosed)
        {
            vehWbUnClosed = null;
            var vehWb =
                ContextMotorDepot.waybillSet.Where(
                    wb => wb.vehicle.id == vehForTerst.id && wb.IsClose == false  && wb.Id != wbEdit.Id).FirstOrDefault();
            if (vehWb == null)
            {
                return false;
            }
            else
            {
                vehWbUnClosed = vehWb;
                return true;
            }

        }

        internal static bool IsDriverHasUnClosedWayBill(driver drvForTerst, waybill wbEdit, out waybill drvWbUnClosed)
        {
            drvWbUnClosed = null;
            var drvWb =
                ContextMotorDepot.waybillSet.Where(
                    wb => wb.driver.id == drvForTerst.id && wb.IsClose == false && wb.Id != wbEdit.Id).FirstOrDefault();
            if (drvWb == null)
            {
                return false;
            }
            else
            {
                drvWbUnClosed = drvWb;
                return true;
            }

        }

        internal static bool IsVehicleInState(MotorDepotEntities contextMotorDepot,vehicle vehForTerst, DateTime dateForTest,out int idState)
        {
            var vehWb =
                contextMotorDepot.waybillSet.Where(
                    wb => wb.vehicle.id == vehForTerst.id && wb.Date == dateForTest.Date).FirstOrDefault();
            idState = 0;
            if (vehWb == null)
            {
                return false;
            }
            else
            {
                var wbDelays = contextMotorDepot.waybill_delaysSet.Where(wbd => wbd.md_waybill.Id == vehWb.Id).ToList();

                foreach (waybill_delays wbDelay in wbDelays)
                {
                    if (wbDelay.Start <= dateForTest && wbDelay.End >= dateForTest)
                    {
                        idState = wbDelay.vehicle_state.Id;  
                        return true;
                    }
                }
                return false;
            }

        }

        internal static void AddNewWayBill(WayBillForm view)
        {
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return;
            WayBillController.LoadWayBillFormControls(view);
            waybill newWayBill = new waybill();
            //ContextMotorDepot.AddTowaybillSet(newWayBill);
            //ContextMotorDepot.SaveChanges();
            newWayBill.Date = DateTime.Today;
            view.pgWayBill.SelectedObject = newWayBill;
            //form.bsOrderT.DataSource = ContextMotorDepot.ordertSet.Where(t => t.md_order.Id == newOrder.Id).ToList();
            view.Show();

        }

        internal static void LoadWayBillFormControls(WayBillForm view)
        {
            view.rgleDriver.DataSource = ContextMotorDepot.driverSet.ToList();
            view.rgleVehicle.DataSource = ContextMotorDepot.vehicleSet.ToList();
            view.rleShift.DataSource = ContextMotorDepot.working_shiftSet.ToList();
            view.rleEnterprise.DataSource = ContextMotorDepot.enterpriseSet.ToList();
            view.rleOrdObject.DataSource = ContextMotorDepot.md_objectSet.ToList();
            view.rleVehStates.DataSource = ContextMotorDepot.vehicle_stateSet.Where(vs => vs.Tariffication == false).ToList();
            view.rleTariff.DataSource = ContextMotorDepot.tariffSet.ToList();
        }

        internal static void ViewWayBill(waybill wayBillForView, WayBillForm view)
        {
            if (!MdUser.Instance.IsObjectVisibleForRole(typeof(waybill).Name, (int)UserAccessObjectsTypes.DocumentsMd)) return;
            if (wayBillForView != null)
            {
                WayBillController.LoadWayBillFormControls(view);
                ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, wayBillForView);
                GetDocumentData(wayBillForView, view);
                view.Show();
                SetBlock(wayBillForView, view);
            }

        }


        internal static void GetDocumentData(waybill wayBillForView, WayBillForm view)
        {
            view.pgWayBill.SelectedObject = wayBillForView;
            //Не разрешать менять машину в путевом листе у которого уже есть заказы
            if (OrdersCount(wayBillForView) > 0) view.erVehicle.Properties.ReadOnly = true;
            view.bsWbOrders.DataSource = RepCustomerData.GetData(wayBillForView);
            List <waybill_delays> content = ContextMotorDepot.waybill_delaysSet.Where(wbd => wbd.md_waybill.Id == wayBillForView.Id).ToList();
            ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, content);
            view.bsWbDelays.DataSource = content;
        }

        internal static bool RecalcWayBill(waybill wbToRecalc, bool isMessageShow)
        {
            if (wbToRecalc == null || wbToRecalc.vehicle == null )  return false;
            if (isMessageShow)
            {
                if (XtraMessageBox.Show(string.Format("Подтверждаете перерасчет фактических данных путевого листа № {0}?", wbToRecalc.Id),
              MDController.MesCaption, MessageBoxButtons.YesNo) != DialogResult.Yes) return false;
            }
                using (WayBillFactCalculator fact = new WayBillFactCalculator(wbToRecalc))
                {
                    if (fact.SetFactData())
                    {
                        if (isMessageShow)
                        {
                            XtraMessageBox.Show(string.Format("Путевой лист № {0} пересчитан", wbToRecalc.Id),
                      MDController.MesCaption);
                        }
                    }

                }

            return true;
        }

        internal static void CloseWayBill(waybill wbToClose)
        {
            if (wbToClose == null) return;
            var orders = ContextMotorDepot.orderSet.Where(od => od.md_waybill.Id == wbToClose.Id).ToList();
            if (orders == null) return;
            foreach (var order in orders)
            {
                order.IsClose = true;
                OrderController.CloseOrder(order,false);  
            }
        }

        internal static void SetBlock(waybill wbForBlock, WayBillForm view)
        {

            if (wbForBlock != null)
            {
                if (wbForBlock.IsDocBlocked)
                {
                    view.SetReadOnly();
                }
                else
                {
                    view.IsReadOnly = false;
                    wbForBlock.BlockUserId = UserBaseCurrent.Instance.Id;
                    wbForBlock.BlockDate = DateTime.Now;
                    WayBillProvider.UpdateBlockState(wbForBlock);
                }
            }
        }
    }
}

