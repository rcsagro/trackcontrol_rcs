﻿using System;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using MotorDepot.Properties;

namespace MotorDepot
{
    public partial class DevFormReportEvent : BaseReports.ReportsDE.BaseControl
    {
        /// <summary>
        /// 1. Журнал событий. Должен формироваться из данных программы trackcontrol.
        /// </summary>
        protected ReportBase<reportFormEvent, TEventInfo> reportEvent;

        public DevFormReportEvent()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gViewEvent, gControlEvent, bar1);
            gControlEvent.DataSource = motodepotBindingSource;
            reportEventSet = null;

            gViewEvent.RowClick += new RowClickEventHandler(gViewEvent_RowClick);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            reportEvent =
                new ReportBase<reportFormEvent, TEventInfo>(Controls, compositeReportLink, gViewEvent,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevFormReportEvent

        public override string Caption
        {
            get
            {
                return Resources.DevFormReportEvent;
            }

        } // Caption

         //клик по строке таблицы
        protected void gViewEvent_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                int indexRow = gViewEvent.GetFocusedDataSourceRowIndex();
            }
        } // gViewEvent_RowClick

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                // to do algoritm

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                } // if

                SetStartButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        public void Select(/* parameters */)
        {
            // to do
        }

        // очистим структуры данных отчета
        public override void ClearReport()
        {
            // to do
        }

        protected void CtearStatusBar()
        {
            // to do
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            // to do
        }

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                // to do this
                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader("", e);
            TEventInfo info = reportEvent.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + "0" + " " +
                Resources.PeriodTo + " " + "0";
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            reportEvent.SetRectangleBrckLetf(0, 0, 300, 85);
            TEventInfo info = reportEvent.GetInfoStructure;
            return ("" + ": " + "0" + "\n" + "" + ": " + "0" + "\n" +
                "" + ": " + "0");
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
            TEventInfo info = reportEvent.GetInfoStructure;
            reportEvent.SetRectangleBrckUP(370, 0, 340, 85);
            return ("" + ": " + "0" + "\n" + "" + ": " + "0");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TEventInfo info = reportEvent.GetInfoStructure;
            reportEvent.SetRectangleBrckRight(770, 0, 300, 85);
            return ("" + ": " + "0" + "\n" + "" + ": " + "0");
        } // GetStringBreackRight

        // формирование отчета событий
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gViewEvent, true, true);

            TEventInfo t_info = new TEventInfo();

			// to do this
            reportEvent.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            reportEvent.CreateAndShowReport(gControlEvent);
        }

        // Формирует данные для отчета по нескольким журналам
        protected override void ExportAllDevToReport()
        {
            // to do
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gViewEvent);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gViewEvent);
        }

        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gControlEvent);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }

        public void Localization()
        {
            colIdCount.Caption = Resources.CaptionIdCount;
            colIdCount.ToolTip = Resources.ToolTipIdCount;

            colDate.Caption = Resources.Date;
            colDate.ToolTip = Resources.Date;

            colTime.Caption = Resources.Time;
            colTime.ToolTip = Resources.Time;

            colStateNumberAuto.Caption = Resources.CaptionStateNumberAuto; 
            colStateNumberAuto.ToolTip = Resources.StateNumberAuto;

            colObject.Caption = Resources.NameObject;
            colObject.ToolTip = Resources.TipNameObject;

            colTimeGoObject.Caption = Resources.TimeGoObject;
            colTimeGoObject.ToolTip = Resources.TimeGoObject;

            colTimeFromObject.Caption = Resources.TimeFromObject;
            colTimeFromObject.ToolTip = Resources.TimeFromObject;

            colLongTimeToObject.Caption = Resources.LongTimeToObject;
            colLongTimeToObject.ToolTip = Resources.LongTimeToObject;

            colDistanceToObject.Caption = Resources.DistanceToObject;
            colDistanceToObject.ToolTip = Resources.DistanceToObject;

            colDurationOnObject.Caption = Resources.DurationOnObject;
            colDurationOnObject.ToolTip = Resources.DurationOnObject;

            colUrgentEvent.Caption = Resources.UrgentEvent;
            colUrgentEvent.ToolTip = Resources.UrgentEvent;
        } // Localization

        public class reportFormEvent
        {
            // to do
            public reportFormEvent(/* parameters */)
            {
                // to do
            }
        } // reportFromEvent
    } // DevFormReportEvent
} // MotorDepot 
