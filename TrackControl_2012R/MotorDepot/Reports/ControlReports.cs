﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public partial class ControlReports : UserControl
    {
        RepController _reportsController;

        public bool IsObjectsExists { get { return _reportsController.IsReportsExists; } }

        public ControlReports()
        {
            InitializeComponent();

            if (_reportsController == null)
            {
                _reportsController = new RepController(nbReports, nbgReports, splitContainerControl4.Panel2);
                _reportsController.ChangeActiveReport += onChangeActiveReport;
                _reportsController.InitReportsList();
                bdeStart.EditValue = DateTime.Today;
                bdeEnd.EditValue = DateTime.Today.AddDays(1).AddMinutes(-1);
            }
            bbiStart.Caption = "ПУСК";
            bbiStart.Glyph = Shared.StartMain;
            bbiPrint.Glyph = Shared.Printer;
        } // ControlReports


        private void bbiStart_ItemClick( object sender, ItemClickEventArgs e )
        {

            RunReport();
        } // bbiStart_ItemClick

        private void RunReport()
        {
            if (bdeStart.EditValue == null || bdeEnd.EditValue == null) return;
            _reportsController.RunReport(Convert.ToDateTime(bdeStart.EditValue), Convert.ToDateTime(bdeEnd.EditValue));
            if (_reportsController.IsReportHasData)
            {
                bbiPrint.Enabled = true;
            }
            else
            {
                bbiPrint.Enabled = false;
            }
        }

        void onChangeActiveReport(string caption)
        {
            //bsiActiveReport.Caption = string.Format("{0}", caption);
            RunReport();
        }

        private void bbiPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            _reportsController.ExportToExcel();
        }
    } // ControlReports
} // MotorDepot.Reports
