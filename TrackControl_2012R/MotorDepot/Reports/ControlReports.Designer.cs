﻿namespace MotorDepot.Reports
{
    partial class ControlReports
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.bdeStart = new DevExpress.XtraBars.BarEditItem();
            this.beStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bdeEnd = new DevExpress.XtraBars.BarEditItem();
            this.beEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bbiPrint = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.deStart = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.deEnd = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.nbReports = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgReports = new DevExpress.XtraNavBar.NavBarGroup();
            this.reportControl = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beStart.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beEnd.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportControl)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItem1,
            this.bbiStart,
            this.bdeEnd,
            this.bdeStart,
            this.bbiPrint});
            this.barManager1.MaxItemId = 11;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.deStart,
            this.deEnd,
            this.beEnd,
            this.beStart});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem1)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemProgressBar1;
            this.barEditItem1.Id = 0;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.Width = 200;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(507, 156);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeStart, "", false, true, true, 107),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.bdeEnd, "", false, true, true, 116),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiPrint, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar1.Text = "Custom 3";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "ПУСК";
            this.bbiStart.Hint = "Запуск отчета";
            this.bbiStart.Id = 1;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // bdeStart
            // 
            this.bdeStart.Caption = "Дата с";
            this.bdeStart.Edit = this.beStart;
            this.bdeStart.Id = 8;
            this.bdeStart.Name = "bdeStart";
            this.bdeStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // beStart
            // 
            this.beStart.AutoHeight = false;
            this.beStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.beStart.DisplayFormat.FormatString = "g";
            this.beStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.beStart.EditFormat.FormatString = "g";
            this.beStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.beStart.Mask.EditMask = "g";
            this.beStart.Name = "beStart";
            this.beStart.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // bdeEnd
            // 
            this.bdeEnd.Caption = "по";
            this.bdeEnd.Edit = this.beEnd;
            this.bdeEnd.Id = 7;
            this.bdeEnd.Name = "bdeEnd";
            this.bdeEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // beEnd
            // 
            this.beEnd.AutoHeight = false;
            this.beEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.beEnd.DisplayFormat.FormatString = "g";
            this.beEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.beEnd.EditFormat.FormatString = "g";
            this.beEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.beEnd.Mask.EditMask = "g";
            this.beEnd.Name = "beEnd";
            this.beEnd.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // bbiPrint
            // 
            this.bbiPrint.Caption = "Печать";
            this.bbiPrint.Enabled = false;
            this.bbiPrint.Hint = "Печать отчета";
            this.bbiPrint.Id = 10;
            this.bbiPrint.Name = "bbiPrint";
            this.bbiPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrint_ItemClick);
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(985, 35);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // deStart
            // 
            this.deStart.AutoHeight = false;
            this.deStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.DisplayFormat.FormatString = "g";
            this.deStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.EditFormat.FormatString = "g";
            this.deStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Name = "deStart";
            // 
            // deEnd
            // 
            this.deEnd.AutoHeight = false;
            this.deEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.Name = "deEnd";
            // 
            // bar4
            // 
            this.bar4.BarName = "Report bar";
            this.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(810, 287);
            this.bar4.OptionsBar.AllowRename = true;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar4.Text = "Report bar";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 35);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.nbReports);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.reportControl);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(985, 464);
            this.splitContainerControl4.SplitterPosition = 195;
            this.splitContainerControl4.TabIndex = 31;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // nbReports
            // 
            this.nbReports.ActiveGroup = this.nbgReports;
            this.nbReports.ContentButtonHint = null;
            this.nbReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nbReports.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgReports});
            this.nbReports.Location = new System.Drawing.Point(0, 0);
            this.nbReports.Name = "nbReports";
            this.nbReports.OptionsNavPane.ExpandedWidth = 211;
            this.nbReports.Size = new System.Drawing.Size(195, 464);
            this.nbReports.TabIndex = 0;
            this.nbReports.Text = "Отчеты";
            // 
            // nbgReports
            // 
            this.nbgReports.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.nbgReports.Appearance.ForeColor = System.Drawing.Color.Black;
            this.nbgReports.Appearance.Options.UseFont = true;
            this.nbgReports.Appearance.Options.UseForeColor = true;
            this.nbgReports.Appearance.Options.UseTextOptions = true;
            this.nbgReports.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nbgReports.Caption = "Отчеты";
            this.nbgReports.Expanded = true;
            this.nbgReports.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.nbgReports.Name = "nbgReports";
            // 
            // reportControl
            // 
            this.reportControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportControl.Location = new System.Drawing.Point(0, 0);
            this.reportControl.Name = "reportControl";
            this.reportControl.Size = new System.Drawing.Size(784, 464);
            this.reportControl.TabIndex = 0;
            // 
            // ControlReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl4);
            this.Controls.Add(this.standaloneBarDockControl2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ControlReports";
            this.Size = new System.Drawing.Size(985, 521);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beStart.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beEnd.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraNavBar.NavBarControl nbReports;
        private DevExpress.XtraNavBar.NavBarGroup nbgReports;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraEditors.PanelControl reportControl;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox deStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox deEnd;
        private DevExpress.XtraBars.BarEditItem bdeEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit beEnd;
        private DevExpress.XtraBars.BarEditItem bdeStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit beStart;
        private DevExpress.XtraBars.BarButtonItem bbiPrint;
    }
}
