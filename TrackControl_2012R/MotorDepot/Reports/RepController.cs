﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using MotorDepot.AccessObjects;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class RepController
    {
        List<IReportItem> _reportsList;
        IReportItem _reportActive;
        Control _parentControl;
        NavBarGroup _barGroup;
        NavBarControl _bar;

        public event Action<string>  ChangeActiveReport;

         public bool IsReportsExists {
             get { return _reportsList.Count > 0; } 
         }

        public RepController(NavBarControl bar, NavBarGroup barGroup, Control parentControl)
        {
            _bar = bar;
            _barGroup = barGroup;
            _parentControl = parentControl;
            _reportsList = new List<IReportItem>();
        }
        public void InitReportsList()
        {
            
            LogEvents repEvents = new LogEvents();
            if (MdUser.Instance.IsObjectVisibleForRole(repEvents.GetType().ToString(), (int)UserAccessObjectsTypes.ReportsList))
            _reportsList.Add(repEvents);
            DeviationTime repDevTime = new DeviationTime();
            if (MdUser.Instance.IsObjectVisibleForRole(repDevTime.GetType().ToString(), (int)UserAccessObjectsTypes.ReportsList))
            _reportsList.Add(repDevTime);
            DeviationKilometrage repDevDist = new DeviationKilometrage();
            if (MdUser.Instance.IsObjectVisibleForRole(repDevDist.GetType().ToString(), (int)UserAccessObjectsTypes.ReportsList))
            _reportsList.Add(repDevDist);
            Dispatcher repDis = new Dispatcher();
            if (MdUser.Instance.IsObjectVisibleForRole(repDis.GetType().ToString(), (int)UserAccessObjectsTypes.ReportsList))
            _reportsList.Add(repDis);
            Customer repCust = new Customer();
            if (MdUser.Instance.IsObjectVisibleForRole(repCust.GetType().ToString(), (int)UserAccessObjectsTypes.ReportsList))
                _reportsList.Add(repCust);
            foreach (IReportItem repOrder in _reportsList)
            {
                NavBarItem item = new NavBarItem(repOrder.Caption);
                item.LargeImage = repOrder.LargeImage;
                item.Tag = repOrder;
                item.LinkClicked += OnSelectReport;
                _barGroup.ItemLinks.Add(item);
                _bar.Items.Add(item);
            }
            _reportActive = repEvents;
            SelectReport(repEvents);
        }

        void OnSelectReport(object sender, NavBarLinkEventArgs e)
        {
            NavBarItem item = (NavBarItem)sender;
            SelectReport((IReportItem)item.Tag);
        }

        private void SelectReport(IReportItem report)
        {
            _parentControl.Controls.Clear();
            report.ParentDataViewer = _parentControl;
            _parentControl.Controls.Add(report.DataViewer);
            foreach (NavBarItem item in _bar.Items)
            {
                if ((IReportItem)item.Tag==report)
                {
                    item.Appearance.Font = new Font(item.Appearance.Font.Name, item.Appearance.Font.Size, FontStyle.Bold);
                }
                else
                {
                    item.Appearance.Font = new Font(item.Appearance.Font.Name, item.Appearance.Font.Size, FontStyle.Regular);
                }
            }
            SetActiveReport(report);

        }

        public void RunReport(DateTime begin, DateTime end)
        {
            _reportActive.Run(begin, end);
        }

        public bool IsReportHasData
        {
            get { return _reportActive.IsHasData; }
        }

        public void SetActiveReport(IReportItem report)
        {
            _reportActive = report;
            if (ChangeActiveReport != null) ChangeActiveReport(GetActiveReportCuption());
        }

        public void ExportToExcel()
        {
            _reportActive.ExportToExcel(); 
        }

        public string GetActiveReportCuption()
        {
            return _reportActive.Caption;
        }
    }
}
