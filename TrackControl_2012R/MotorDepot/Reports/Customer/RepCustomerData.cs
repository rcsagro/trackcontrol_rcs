﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorDepot.Reports
{
    public class RepCustomerData
    {
        private const string _nameDispatcher = "Гл.диспетчер";
        private const string _nameDinner = "Обед";
        private const string _nameDelay = "Задержки";

        private static Dictionary<int, WeyBillOrdersData> _weyBillOrdersDatas; 

        public static List<RepCustomerFields> GetData(waybill wayBill)
        {
            if (wayBill == null) throw new ArgumentException("WayBill can not be NULL or Empty.");
            List<RepCustomerFields> repData = new List<RepCustomerFields>();
            var orders = MDController.Instance.ContextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == wayBill.Id && ord.md_order_state.Id != (int)OrderStates.Delete).ToList();
            if (orders != null && orders.Count >0)
            {
                GetOrdersData(repData, orders);
                AddFinalRecords(wayBill, repData,true);
            }
            if (_weyBillOrdersDatas != null) _weyBillOrdersDatas.Clear(); 
            return repData;
        }

        public static List<RepCustomerFields> GetData(DateTime dateStart, DateTime dateEnd)
        {
            List<RepCustomerFields> repData = new List<RepCustomerFields>();

            var orders = MDController.Instance.ContextMotorDepot.orderSet.Include("md_waybill").Where(ord =>
                ord.DateCreate >= dateStart
                && ord.DateCreate <= dateEnd
                && ord.md_order_state.Id != (int)OrderStates.Delete).OrderBy(ord => ord.md_objectCustomer.Name).ToList();
            if (orders != null && orders.Count > 0) GetOrdersData(repData, orders);
            AddFinalRecordsToWayBills(dateStart, dateEnd, repData);
            _weyBillOrdersDatas.Clear();
            return repData;
        }

        private static void AddFinalRecords(waybill wayBill, List<RepCustomerFields> repData,bool addNoTariffData)
        {
            if (wayBill.TotalTimeStop != null && _weyBillOrdersDatas.ContainsKey(wayBill.Id))
            {
                if (((decimal)wayBill.TotalTimeStop - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject) > 0 && wayBill.DinnerTime > 0 && addNoTariffData)
                {
                    AddRecordWbDinnerTime(repData, wayBill);
                }
                _weyBillOrdersDatas[wayBill.Id].TotalDelay = GetDelaysHours(wayBill);
                if (wayBill.DinnerTime > 0 && addNoTariffData)
                {
                    if (((decimal)wayBill.TotalTimeStop - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject - wayBill.DinnerTime) > 0 && _weyBillOrdersDatas[wayBill.Id].TotalDelay > 0) AddRecordWbDelays(repData, wayBill);
                }
                if (_weyBillOrdersDatas[wayBill.Id].TotalPath < (decimal)wayBill.TotalPath ||
                    _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject < (decimal)wayBill.TotalTimeStop)
                    AddRecordWbPathExceed(repData, wayBill);
            }
        }

        private static void AddFinalRecordsToWayBills(DateTime dateStart, DateTime dateEnd, List<RepCustomerFields> repData)
        {
            var wayBills = MDController.Instance.ContextMotorDepot.waybillSet.Where(wb => wb.DateStart >= dateStart && wb.DateEnd <= dateEnd && wb.IsClose == true).ToList();
            if (wayBills != null)
            {
                foreach (waybill wb in wayBills)
                {
                    AddFinalRecords(wb, repData,false);
                }
            }
        }

        private static decimal GetDelaysHours(waybill wayBill)
        {
            decimal delays = 0;
            var wbDelays = from wbd in MDController.Instance.ContextMotorDepot.waybill_delaysSet
                           where (wbd.md_waybill.Id == wayBill.Id)
                           group wbd by wbd.md_waybill.Id into MyGroup
                           select new { Hours = MyGroup.Sum(wbd => wbd.Hours) };
            if (wbDelays != null)
            {
                foreach (var item in wbDelays)
                {
                    delays = (decimal)item.Hours;
                }
            }
            return delays;
        }



        private static void GetOrdersData(List<RepCustomerFields> repData, List<order> orders)
        {
            _weyBillOrdersDatas = new Dictionary<int, WeyBillOrdersData>();
            //_totalPath = 0;
            //_totalTimeStopsToObject = 0;
            //_totalTimeFact = 0;
            foreach (order orderItem in orders)
            {
                GetTariffs(orderItem.md_waybill);
                var orderRecords = MDController.Instance.ContextMotorDepot.ordertSet.Where(rec => rec.md_order.Id == orderItem.Id).ToList();
                if (orderRecords != null && orderRecords.Count > 0)
                {
                    foreach (ordert orderRecord in orderRecords)
                    {
                        RepCustomerFields repDataItem = new RepCustomerFields();
                        repDataItem.Id = orderItem.Id;
                        repDataItem.Number = orderItem.Number;
                        repDataItem.StateId = orderItem.StateId;
                        repDataItem.DateCreate = orderItem.DateCreate;
                        if (orderItem.md_objectCustomer != null)
                            repDataItem.Customer = orderItem.md_objectCustomer.Name;
                        if (orderRecord.md_cargoLoad != null)
                            repDataItem.CargoName = orderRecord.md_cargoLoad.Name;
                        else
                            repDataItem.CargoName = string.Empty;
                        if (orderRecord.md_cargoUnLoad != null && repDataItem.CargoName != orderRecord.md_cargoUnLoad.Name)
                            repDataItem.CargoName = string.Format("{0} {1}", repDataItem.CargoName, orderRecord.md_cargoUnLoad.Name);
                        repDataItem.CargoQty = orderRecord.QtyLoading + orderRecord.QtyUnLoading;
                        repDataItem.TimeLoading = orderRecord.TimeLoading;
                        repDataItem.TimeUnLoading = orderRecord.TimeUnLoading;
                        if (orderRecord.TimeExit != null && orderRecord.TimeEnter != null)
                            repDataItem.FactTimeInObject = Math.Round((decimal)orderRecord.TimeExit.Value.Subtract(orderRecord.TimeEnter.Value).TotalHours, 2);
                        else
                            repDataItem.FactTimeInObject = 0;
                        repDataItem.TotalOrderTime = Math.Round(orderRecord.TimeToObjectFact + repDataItem.FactTimeInObject, 2);
                        repDataItem.TotalPath = Math.Round(orderRecord.DistanceToObjectFact + orderRecord.DistanceInObject, 2);
                        if (repDataItem.FactTimeInObject > 0 && repDataItem.FactTimeInObject > (orderRecord.TimeLoading + orderRecord.TimeUnLoading))
                            repDataItem.TimeExcess = Math.Round(repDataItem.FactTimeInObject - (orderRecord.TimeLoading + orderRecord.TimeUnLoading), 2);
                        else
                            repDataItem.TimeExcess = 0;
                        repDataItem.TimeStopsToObject = Math.Round(orderRecord.TimeStopFact, 2);
                        repDataItem.TimeStopsInObject = Math.Round(orderRecord.TimeStopFactInObject, 2);
                        if (orderItem.md_waybill != null && orderItem.md_waybill.vehicle != null)
                        {
                            repDataItem.NumberPlate = orderItem.md_waybill.vehicle.NumberPlate;
                            repDataItem.NumberWayBill = orderItem.md_waybill.Id;
                            //тариф за время движения умножается на  время под заказом как движения так и простоя + тариф за пробег умноженный на пробег за это время
                            repDataItem.PricePath = Math.Round(_weyBillOrdersDatas[orderItem.md_waybill.Id].TariffKm * repDataItem.TotalPath, 2);
                            //Остановки - меняем на Время тариф остановок * (время движения + время стоянок)
                            repDataItem.PriceTime = Math.Round(_weyBillOrdersDatas[orderItem.md_waybill.Id].TariffTimeStop * orderRecord.TimeToObjectFact, 2);
                            //repDataItem.PriceTime = Math.Round(_tariffTimeStop * repDataItem.TimeStopsToObject, 2);
                            //простой
                            repDataItem.PriceTimeStop = Math.Round(_weyBillOrdersDatas[orderItem.md_waybill.Id].TariffTime * repDataItem.TimeExcess, 2);
                            repDataItem.PriceTotal = repDataItem.PricePath + repDataItem.PriceTime + repDataItem.PriceTimeStop;
                            
                        }
                        AddOrdersData(orderItem.md_waybill, repDataItem);
                        //_totalPath += repDataItem.TotalPath;
                        //_totalTimeStopsToObject += repDataItem.TimeStopsToObject;
                        //_totalTimeFact += repDataItem.TotalOrderTime;
                        
                        repData.Add(repDataItem);
                    }
                }
            }
        }

        private static void GetTariffs(waybill wb)
        {
            if (wb == null) return;
            if (!_weyBillOrdersDatas.ContainsKey(wb.Id))
            {
                WeyBillOrdersData data = new WeyBillOrdersData();
                _weyBillOrdersDatas.Add(wb.Id, data);
            }
            if (wb != null && wb.md_tariff == null) wb = MDController.Instance.ContextMotorDepot.waybillSet.Include("md_tariff").Where(wbt => wbt.Id == wb.Id).FirstOrDefault(); 
            if (wb != null && wb.md_tariff != null)
            {
                   
                if (wb.IsBusinessTrip)
                {
                    //_tariffKm = wb.md_tariff.TMoveBt;
                    //_tariffTime = wb.md_tariff.TTimeBt;
                    //_tariffTimeStop = wb.md_tariff.TTimeStopBt;
                    _weyBillOrdersDatas[wb.Id].SetTarifs(wb.md_tariff.TMoveBt, wb.md_tariff.TTimeBt, wb.md_tariff.TTimeStopBt);
                }
                else
                {
                    //_tariffKm = wb.md_tariff.TMove;
                    //_tariffTime = wb.md_tariff.TTime;
                    //_tariffTimeStop = wb.md_tariff.TTimeStop;
                    _weyBillOrdersDatas[wb.Id].SetTarifs(wb.md_tariff.TMove, wb.md_tariff.TTime, wb.md_tariff.TTimeStop);
                }
            }
            else
            {
                //_tariffKm = 0;
                //_tariffTime = 0;
                //_tariffTimeStop = 0;
                _weyBillOrdersDatas[wb.Id].SetTarifs(0, 0, 0);
            }
        }

        public static void AddRecordWbPathExceed(List<RepCustomerFields> repData,waybill wayBill)
        {
            RepCustomerFields repDataItem = new RepCustomerFields();
            repDataItem.Customer = _nameDispatcher;
            repDataItem.NumberWayBill = wayBill.Id;
            repDataItem.DateCreate = wayBill.DateEnd;
            repDataItem.NumberPlate = wayBill.vehicle.NumberPlate;
            repDataItem.TotalPath = (decimal)wayBill.TotalPath - _weyBillOrdersDatas[wayBill.Id].TotalPath;
            if (repDataItem.TotalPath < 0) 
                repDataItem.TotalPath = 0;
            else if (repDataItem.TotalPath > 0)
            {
                repDataItem.PricePath = Math.Round(_weyBillOrdersDatas[wayBill.Id].TariffKm * repDataItem.TotalPath, 2);
                //decimal timeDelta = (decimal)((DateTime)wayBill.DateEnd).Subtract((DateTime)wayBill.DateStart).TotalHours - _totalTimeFact;
                //if (timeDelta > 0)
                //{
                //    repDataItem.PricePath += Math.Round(_tariffTimeStop * timeDelta, 2);
                //}
            }

            repDataItem.TimeStopsToObject = (decimal)wayBill.TotalTimeStop - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsInObject
                 - wayBill.DinnerTime - _weyBillOrdersDatas[wayBill.Id].TotalDelay;
            if (repDataItem.TimeStopsToObject < 0)
                repDataItem.TimeStopsToObject = 0;
            else if (repDataItem.TimeStopsToObject > 0)
            {
                repDataItem.PriceTime = Math.Round(_weyBillOrdersDatas[wayBill.Id].TariffTimeStop * repDataItem.TimeStopsToObject, 2);
            }

            repDataItem.PriceTotal = repDataItem.PricePath + repDataItem.PriceTime + repDataItem.PriceTimeStop;
            repData.Add(repDataItem);
        }

        private static void AddRecordWbDinnerTime(List<RepCustomerFields> repData, waybill wayBill)
        {
            if (wayBill.DinnerTime == 0) return;
            RepCustomerFields repDataItem = new RepCustomerFields();
            repDataItem.Customer = _nameDinner;
            repDataItem.NumberWayBill = wayBill.Id;
            repDataItem.DateCreate = wayBill.DateEnd;
            repDataItem.NumberPlate = wayBill.vehicle.NumberPlate;
            decimal timeDelta = ((decimal)wayBill.TotalTimeStop - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsInObject);
            if (timeDelta > wayBill.DinnerTime)
                repDataItem.TimeStopsToObject = wayBill.DinnerTime;
            else
                repDataItem.TimeStopsToObject = timeDelta;
            repData.Add(repDataItem);
        }

        private static void AddRecordWbDelays(List<RepCustomerFields> repData, waybill wayBill)
        {
            if (wayBill.DinnerTime == 0) return;
            RepCustomerFields repDataItem = new RepCustomerFields();
            repDataItem.Customer = _nameDelay;
            repDataItem.NumberWayBill = wayBill.Id;
            repDataItem.DateCreate = wayBill.DateEnd;
            repDataItem.NumberPlate = wayBill.vehicle.NumberPlate;
            decimal timeDelta = ((decimal)wayBill.TotalTimeStop - _weyBillOrdersDatas[wayBill.Id].TotalTimeStopsToObject - wayBill.DinnerTime);
            if (timeDelta > _weyBillOrdersDatas[wayBill.Id].TotalDelay)
                repDataItem.TimeStopsToObject = _weyBillOrdersDatas[wayBill.Id].TotalDelay;
            else
                repDataItem.TimeStopsToObject = timeDelta;
            repData.Add(repDataItem);
        }

        private static void AddOrdersData(waybill wb, RepCustomerFields repDataItem)
        {
            if (wb == null) return;
            if (!_weyBillOrdersDatas.ContainsKey(wb.Id))
            {
                WeyBillOrdersData data = new WeyBillOrdersData();
                _weyBillOrdersDatas.Add(wb.Id, data); 
            }
            _weyBillOrdersDatas[wb.Id].AddOrderData(repDataItem);
        }

        private class WeyBillOrdersData
        {
            public WeyBillOrdersData()
            {
            }
            public decimal TotalPath;
            public decimal TotalTimeStopsToObject;
            public decimal TotalTimeStopsInObject;
            public decimal TotalTimeFact;
            public decimal TotalDelay;
            public decimal TariffKm;
            public decimal TariffTime;
            public decimal TariffTimeStop;
            public void AddOrderData(RepCustomerFields repDataItem)
            {
                TotalPath += repDataItem.TotalPath;
                TotalTimeStopsToObject += repDataItem.TimeStopsToObject;
                TotalTimeStopsInObject += repDataItem.TimeStopsInObject;
                TotalTimeFact += repDataItem.TotalOrderTime;
            }

            public void SetTarifs(decimal tariffKm, decimal tariffTime, decimal tariffTimeStop)
            {
                TariffKm = tariffKm;
                TariffTime = tariffTime;
                TariffTimeStop = tariffTimeStop;
            }
        }
    }
}
