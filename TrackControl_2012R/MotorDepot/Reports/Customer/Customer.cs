﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class Customer : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        string _caption = "Отчет по заказчикам";
        Image _largeImage = Shared.NetworkingFactory;
        Image _smallImage = Shared.NetworkingFactory;
        private List<RepCustomerFields> _reportData;

        public Customer()
        {
            _dataViewer = new RepCustomerView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run( DateTime begin, DateTime end )
        {
            _reportData = RepCustomerData.GetData(begin, end);
            ((RepCustomerView)_dataViewer).gcView.DataSource = _reportData;
            ((RepCustomerView)_dataViewer).Begin = begin;
            ((RepCustomerView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return _reportData == null ? false : !(_reportData.Count == 0); }
        }

        public void ExportToExcel()
        {
            ((RepCustomerView)_dataViewer).ExportToExcel();
        }
    }
}
