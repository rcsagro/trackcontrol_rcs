﻿namespace MotorDepot.Reports
{
    partial class RepCustomerView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepCustomerView));
            this.gcView = new DevExpress.XtraGrid.GridControl();
            this.bgvView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colOrdId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdState = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ricbStates = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imOrderStates = new DevExpress.Utils.ImageCollection(this.components);
            this.colOrdNumber = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumberWayBill = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colOrdCustomer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCargoName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCargoQty = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeUnLoading = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colFactTimeInObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalOrderTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTotalPath = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStopsToObject = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumPlate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPricePath = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPriceTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.rleOrdObject = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrdObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            this.SuspendLayout();
            // 
            // gcView
            // 
            this.gcView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcView.Location = new System.Drawing.Point(0, 0);
            this.gcView.MainView = this.bgvView;
            this.gcView.Name = "gcView";
            this.gcView.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleOrdObject,
            this.ricbStates});
            this.gcView.Size = new System.Drawing.Size(811, 448);
            this.gcView.TabIndex = 1;
            this.gcView.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvView});
            // 
            // bgvView
            // 
            this.bgvView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.bgvView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colOrdId,
            this.colOrdState,
            this.colOrdNumber,
            this.colOrdDate,
            this.colOrdCustomer,
            this.colCargoName,
            this.colCargoQty,
            this.colTimeLoading,
            this.colTimeUnLoading,
            this.colFactTimeInObject,
            this.colTotalOrderTime,
            this.colTotalPath,
            this.colTimeStop,
            this.colTimeStopsToObject,
            this.colNumberWayBill,
            this.colNumPlate,
            this.colPricePath,
            this.colPriceTimeStop,
            this.colPriceTime,
            this.colPriceTotal});
            this.bgvView.GridControl = this.gcView;
            this.bgvView.GroupCount = 1;
            this.bgvView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CargoQty", this.colCargoQty, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Id", null, "Строк в заказах: {0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalPath", null, "пробег {0} км"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalOrderTime", null, "Длительность работ {0} час"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalOrderTime", this.colTotalOrderTime, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeLoading", this.colTimeLoading, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeUnLoading", this.colTimeUnLoading, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalPath", this.colTotalPath, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeExcess", this.colTimeStop, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TimeStopsToObject", this.colTimeStopsToObject, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FactTimeInObject", this.colFactTimeInObject, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PricePath", this.colPricePath, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceTimeStop", this.colPriceTimeStop, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceTime", this.colPriceTime, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceTotal", this.colPriceTotal, "")});
            this.bgvView.IndicatorWidth = 30;
            this.bgvView.Name = "bgvView";
            this.bgvView.OptionsBehavior.Editable = false;
            this.bgvView.OptionsView.ShowFooter = true;
            this.bgvView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrdCustomer, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bgvView.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.bgvView_CustomDrawRowIndicator);
            this.bgvView.DoubleClick += new System.EventHandler(this.bgvView_DoubleClick);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Информация о заказ-наряде";
            this.gridBand1.Columns.Add(this.colOrdId);
            this.gridBand1.Columns.Add(this.colOrdState);
            this.gridBand1.Columns.Add(this.colOrdNumber);
            this.gridBand1.Columns.Add(this.colOrdDate);
            this.gridBand1.Columns.Add(this.colNumberWayBill);
            this.gridBand1.Columns.Add(this.colOrdCustomer);
            this.gridBand1.Columns.Add(this.colCargoName);
            this.gridBand1.Columns.Add(this.colCargoQty);
            this.gridBand1.Columns.Add(this.colTimeLoading);
            this.gridBand1.Columns.Add(this.colTimeUnLoading);
            this.gridBand1.Columns.Add(this.colFactTimeInObject);
            this.gridBand1.Columns.Add(this.colTotalOrderTime);
            this.gridBand1.Columns.Add(this.colTotalPath);
            this.gridBand1.Columns.Add(this.colTimeStop);
            this.gridBand1.Columns.Add(this.colTimeStopsToObject);
            this.gridBand1.Columns.Add(this.colNumPlate);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 1263;
            // 
            // colOrdId
            // 
            this.colOrdId.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdId.FieldName = "Id";
            this.colOrdId.Name = "colOrdId";
            this.colOrdId.OptionsColumn.AllowEdit = false;
            this.colOrdId.OptionsColumn.ReadOnly = true;
            this.colOrdId.Width = 50;
            // 
            // colOrdState
            // 
            this.colOrdState.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdState.Caption = "Статус";
            this.colOrdState.ColumnEdit = this.ricbStates;
            this.colOrdState.FieldName = "StateId";
            this.colOrdState.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colOrdState.Name = "colOrdState";
            this.colOrdState.OptionsColumn.AllowEdit = false;
            this.colOrdState.OptionsColumn.ReadOnly = true;
            this.colOrdState.OptionsColumn.ShowCaption = false;
            this.colOrdState.Visible = true;
            this.colOrdState.Width = 39;
            // 
            // ricbStates
            // 
            this.ricbStates.AutoHeight = false;
            this.ricbStates.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ricbStates.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 6, 5)});
            this.ricbStates.Name = "ricbStates";
            this.ricbStates.SmallImages = this.imOrderStates;
            // 
            // imOrderStates
            // 
            this.imOrderStates.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imOrderStates.ImageStream")));
            this.imOrderStates.Images.SetKeyName(0, "document.png");
            this.imOrderStates.Images.SetKeyName(1, "document-broken.png");
            this.imOrderStates.Images.SetKeyName(2, "document-hf-insert-footer.png");
            this.imOrderStates.Images.SetKeyName(3, "document--exclamation.png");
            this.imOrderStates.Images.SetKeyName(4, "document-task.png");
            this.imOrderStates.Images.SetKeyName(5, "document-hf-delete-footer.png");
            // 
            // colOrdNumber
            // 
            this.colOrdNumber.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdNumber.Caption = "Заказ";
            this.colOrdNumber.FieldName = "Number";
            this.colOrdNumber.Name = "colOrdNumber";
            this.colOrdNumber.OptionsColumn.AllowEdit = false;
            this.colOrdNumber.OptionsColumn.ReadOnly = true;
            this.colOrdNumber.Visible = true;
            this.colOrdNumber.Width = 117;
            // 
            // colOrdDate
            // 
            this.colOrdDate.AppearanceCell.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdDate.Caption = "Дата";
            this.colOrdDate.DisplayFormat.FormatString = "g";
            this.colOrdDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colOrdDate.FieldName = "DateCreate";
            this.colOrdDate.Name = "colOrdDate";
            this.colOrdDate.OptionsColumn.AllowEdit = false;
            this.colOrdDate.OptionsColumn.ReadOnly = true;
            this.colOrdDate.Visible = true;
            this.colOrdDate.Width = 103;
            // 
            // colNumberWayBill
            // 
            this.colNumberWayBill.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberWayBill.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberWayBill.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberWayBill.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNumberWayBill.Caption = "№ пут.листа";
            this.colNumberWayBill.FieldName = "NumberWayBill";
            this.colNumberWayBill.Name = "colNumberWayBill";
            this.colNumberWayBill.OptionsColumn.AllowEdit = false;
            this.colNumberWayBill.OptionsColumn.ReadOnly = true;
            this.colNumberWayBill.Visible = true;
            this.colNumberWayBill.Width = 115;
            // 
            // colOrdCustomer
            // 
            this.colOrdCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrdCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrdCustomer.Caption = "Заказчик";
            this.colOrdCustomer.FieldName = "Customer";
            this.colOrdCustomer.Name = "colOrdCustomer";
            this.colOrdCustomer.OptionsColumn.AllowEdit = false;
            this.colOrdCustomer.OptionsColumn.ReadOnly = true;
            this.colOrdCustomer.Visible = true;
            this.colOrdCustomer.Width = 101;
            // 
            // colCargoName
            // 
            this.colCargoName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargoName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoName.Caption = "Груз";
            this.colCargoName.FieldName = "CargoName";
            this.colCargoName.Name = "colCargoName";
            this.colCargoName.OptionsColumn.AllowEdit = false;
            this.colCargoName.OptionsColumn.ReadOnly = true;
            this.colCargoName.Visible = true;
            this.colCargoName.Width = 60;
            // 
            // colCargoQty
            // 
            this.colCargoQty.AppearanceCell.Options.UseTextOptions = true;
            this.colCargoQty.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoQty.AppearanceHeader.Options.UseTextOptions = true;
            this.colCargoQty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCargoQty.Caption = "Кол-во";
            this.colCargoQty.FieldName = "CargoQty";
            this.colCargoQty.Name = "colCargoQty";
            this.colCargoQty.OptionsColumn.AllowEdit = false;
            this.colCargoQty.OptionsColumn.ReadOnly = true;
            this.colCargoQty.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCargoQty.Visible = true;
            this.colCargoQty.Width = 62;
            // 
            // colTimeLoading
            // 
            this.colTimeLoading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeLoading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoading.Caption = "Погрузка";
            this.colTimeLoading.FieldName = "TimeLoading";
            this.colTimeLoading.Name = "colTimeLoading";
            this.colTimeLoading.OptionsColumn.AllowEdit = false;
            this.colTimeLoading.OptionsColumn.ReadOnly = true;
            this.colTimeLoading.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeLoading.Visible = true;
            this.colTimeLoading.Width = 68;
            // 
            // colTimeUnLoading
            // 
            this.colTimeUnLoading.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeUnLoading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnLoading.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeUnLoading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnLoading.Caption = "Разрузка";
            this.colTimeUnLoading.FieldName = "TimeUnLoading";
            this.colTimeUnLoading.Name = "colTimeUnLoading";
            this.colTimeUnLoading.OptionsColumn.AllowEdit = false;
            this.colTimeUnLoading.OptionsColumn.ReadOnly = true;
            this.colTimeUnLoading.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeUnLoading.Visible = true;
            this.colTimeUnLoading.Width = 65;
            // 
            // colFactTimeInObject
            // 
            this.colFactTimeInObject.AppearanceCell.Options.UseTextOptions = true;
            this.colFactTimeInObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactTimeInObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colFactTimeInObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFactTimeInObject.Caption = "Время на объекте";
            this.colFactTimeInObject.FieldName = "FactTimeInObject";
            this.colFactTimeInObject.Name = "colFactTimeInObject";
            this.colFactTimeInObject.OptionsColumn.AllowEdit = false;
            this.colFactTimeInObject.OptionsColumn.ReadOnly = true;
            this.colFactTimeInObject.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFactTimeInObject.Visible = true;
            this.colFactTimeInObject.Width = 111;
            // 
            // colTotalOrderTime
            // 
            this.colTotalOrderTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalOrderTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalOrderTime.Caption = "Длительность работ";
            this.colTotalOrderTime.FieldName = "TotalOrderTime";
            this.colTotalOrderTime.Name = "colTotalOrderTime";
            this.colTotalOrderTime.OptionsColumn.AllowEdit = false;
            this.colTotalOrderTime.OptionsColumn.ReadOnly = true;
            this.colTotalOrderTime.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalOrderTime.Visible = true;
            this.colTotalOrderTime.Width = 124;
            // 
            // colTotalPath
            // 
            this.colTotalPath.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalPath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPath.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalPath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPath.Caption = "Пробег";
            this.colTotalPath.FieldName = "TotalPath";
            this.colTotalPath.Name = "colTotalPath";
            this.colTotalPath.OptionsColumn.AllowEdit = false;
            this.colTotalPath.OptionsColumn.ReadOnly = true;
            this.colTotalPath.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalPath.Visible = true;
            this.colTotalPath.Width = 57;
            // 
            // colTimeStop
            // 
            this.colTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStop.Caption = "Простой";
            this.colTimeStop.FieldName = "TimeExcess";
            this.colTimeStop.Name = "colTimeStop";
            this.colTimeStop.OptionsColumn.AllowEdit = false;
            this.colTimeStop.OptionsColumn.ReadOnly = true;
            this.colTimeStop.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStop.Visible = true;
            this.colTimeStop.Width = 72;
            // 
            // colTimeStopsToObject
            // 
            this.colTimeStopsToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopsToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopsToObject.Caption = "Стоянки";
            this.colTimeStopsToObject.FieldName = "TimeStopsToObject";
            this.colTimeStopsToObject.Name = "colTimeStopsToObject";
            this.colTimeStopsToObject.OptionsColumn.AllowEdit = false;
            this.colTimeStopsToObject.OptionsColumn.ReadOnly = true;
            this.colTimeStopsToObject.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStopsToObject.Visible = true;
            this.colTimeStopsToObject.Width = 70;
            // 
            // colNumPlate
            // 
            this.colNumPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNumPlate.Caption = "Гос.номер";
            this.colNumPlate.FieldName = "NumberPlate";
            this.colNumPlate.Name = "colNumPlate";
            this.colNumPlate.OptionsColumn.AllowEdit = false;
            this.colNumPlate.OptionsColumn.ReadOnly = true;
            this.colNumPlate.Visible = true;
            this.colNumPlate.Width = 99;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Стоимость ,грн";
            this.gridBand2.Columns.Add(this.colPricePath);
            this.gridBand2.Columns.Add(this.colPriceTime);
            this.gridBand2.Columns.Add(this.colPriceTimeStop);
            this.gridBand2.Columns.Add(this.colPriceTotal);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 300;
            // 
            // colPricePath
            // 
            this.colPricePath.AppearanceCell.Options.UseTextOptions = true;
            this.colPricePath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePath.AppearanceHeader.Options.UseTextOptions = true;
            this.colPricePath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePath.Caption = "Пробег";
            this.colPricePath.FieldName = "PricePath";
            this.colPricePath.Name = "colPricePath";
            this.colPricePath.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPricePath.Visible = true;
            // 
            // colPriceTime
            // 
            this.colPriceTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTime.Caption = "Время";
            this.colPriceTime.FieldName = "PriceTime";
            this.colPriceTime.Name = "colPriceTime";
            this.colPriceTime.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTime.Visible = true;
            // 
            // colPriceTimeStop
            // 
            this.colPriceTimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTimeStop.Caption = "Простой";
            this.colPriceTimeStop.FieldName = "PriceTimeStop";
            this.colPriceTimeStop.Name = "colPriceTimeStop";
            this.colPriceTimeStop.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTimeStop.Visible = true;
            // 
            // colPriceTotal
            // 
            this.colPriceTotal.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceTotal.Caption = "Всего";
            this.colPriceTotal.FieldName = "PriceTotal";
            this.colPriceTotal.Name = "colPriceTotal";
            this.colPriceTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colPriceTotal.Visible = true;
            // 
            // rleOrdObject
            // 
            this.rleOrdObject.AutoHeight = false;
            this.rleOrdObject.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleOrdObject.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Название")});
            this.rleOrdObject.DisplayMember = "Name";
            this.rleOrdObject.Name = "rleOrdObject";
            this.rleOrdObject.NullText = "";
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcView;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystem = this.psReport;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepCustomerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcView);
            this.Name = "RepCustomerView";
            this.Size = new System.Drawing.Size(811, 448);
            ((System.ComponentModel.ISupportInitialize)(this.gcView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricbStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imOrderStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleOrdObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricbStates;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleOrdObject;
        internal DevExpress.Utils.ImageCollection imOrderStates;
        internal DevExpress.XtraGrid.GridControl gcView;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        internal DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdState;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdNumber;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumberWayBill;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colOrdCustomer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCargoName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCargoQty;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeUnLoading;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colFactTimeInObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalOrderTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTotalPath;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStopsToObject;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumPlate;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPricePath;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPriceTotal;
    }
}
