﻿using System;

namespace MotorDepot.Reports
{
    public class RepCustomerFields
    {
        // № путевого листа
        public int NumberWayBill { get; set; }
        // гос номер транспорта
        public string NumberPlate { get; set; }
        // код заказа
        public int Id { get; set; }
        // номер заказа
        public string Number { get; set; }
        // код статуса заказа
        public int StateId { get; set; }
        // дата создания заказа
        public DateTime? DateCreate { get; set; }
        // заказчик
        public string Customer { get; set; }
        // груз
        public string CargoName { get; set; }
        // колитчество груза
        public decimal CargoQty { get; set; }
        // время погрузки
        public decimal TimeLoading { get; set; }
        // время разгрузки
        public decimal TimeUnLoading { get; set; }
        // фактическое фремя движения на объекте
        public decimal FactTimeInObject { get; set; }

        // Длительность работ это общее время выполнения заказа, как и пробег, суммарно относительно данного заказа  + общий итого за весь путевой лист
        public decimal TotalOrderTime { get; set; }

        // пробег - сумма пробега до объекта и в объекте
        public decimal TotalPath { get; set; }

        //простой это значение на которое превышено пребывание в зоне + все остановки вне зоны (желательно  чтобы этот параметр состоял из двух величин)
        public decimal TimeExcess { get; set; }
        public decimal TimeStopsToObject { get; set; }
        //время стоянок на объекте
        public decimal TimeStopsInObject { get; set; }

        //стоимость работ - пробег,время,простой 
        public decimal PricePath { get; set; }
        public decimal PriceTime { get; set; }
        public decimal PriceTimeStop { get; set; }
        public decimal PriceTotal { get; set; }
    }
}
