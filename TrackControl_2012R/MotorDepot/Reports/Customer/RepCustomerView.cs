﻿using System;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public partial class RepCustomerView : UserControl
    {
        public RepCustomerView()
        {
            InitializeComponent();
        }

        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        private void bgvView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(bgvView, false, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = "Отчет по заказчикам";
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), "Период с", "по"), 60, e);
        }

        private void bgvView_DoubleClick(object sender, EventArgs e)
        {
            if (bgvView.FocusedRowHandle < 0) return;
            int idOrder = 0;
            if (Int32.TryParse(bgvView.GetRowCellValue(bgvView.FocusedRowHandle, colOrdId).ToString(), out idOrder))
            {
                order ordFromGrid =
                    MDController.Instance.ContextMotorDepot.orderSet.Where(od => od.Id == idOrder).FirstOrDefault() ;
                if (ordFromGrid != null)
                    MDController.Instance.OrderViewOrder(ordFromGrid);
            }
        }
    }
}
