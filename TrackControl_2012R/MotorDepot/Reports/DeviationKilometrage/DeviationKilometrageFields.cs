﻿using System;

namespace MotorDepot.Reports
{
    public class DeviationKilometrageFields
    {
        public int IdOrder { get; set; }

        public int IdWayBill { get; set; }

        public DateTime DateWayBill { get; set; }

        public string NumberOrder { get; set; }

        public string NumberPlate { get; set; }

        public string ObjectName { get; set; }

        public double DistToObjectPlan { get; set; }

        public double DistToObjectFact { get; set; }

        public double DistToObjectDeviation { get; set; }

        public string Remark { get; set; }
    }
}
