﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class DeviationKilometrage : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        string _caption = "Журнал отклонений по пробегу";
        Image _largeImage = Shared.NetworkingDistance;
        Image _smallImage = Shared.NetworkingDistance;
        private List<DeviationKilometrageFields> _reportData;

        public DeviationKilometrage()
        {
            _dataViewer = new RepDeviationKilometrageView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run( DateTime begin, DateTime end )
        {
            RepDeviationKilometrageData dataSource = new RepDeviationKilometrageData();
            _reportData = dataSource.GetData(begin, end);
            ((RepDeviationKilometrageView)_dataViewer).gcDevKm.DataSource = _reportData;
            ((RepDeviationKilometrageView)_dataViewer).Begin = begin;
            ((RepDeviationKilometrageView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return _reportData == null ? false : !(_reportData.Count == 0); }
        }

        public void ExportToExcel()
        {
            ( ( RepDeviationKilometrageView)_dataViewer ).ExportToExcel();
        }
    }
}
