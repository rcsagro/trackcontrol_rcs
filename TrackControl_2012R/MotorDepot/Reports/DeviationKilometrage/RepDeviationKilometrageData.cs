﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorDepot.Reports
{
    
    public class RepDeviationKilometrageData
    {
        MotorDepotEntities _contextMotorDepot;

        public RepDeviationKilometrageData()
        {
            _contextMotorDepot = MDController.Instance.ContextMotorDepot;
        }

        public List<DeviationKilometrageFields> GetData(DateTime start,DateTime end)
        {
            List<DeviationKilometrageFields> repData = new List<DeviationKilometrageFields>();
            var wbs = _contextMotorDepot.waybillSet.Where(wb => wb.Date >= start && wb.Date <= end ).ToList();
            foreach (waybill wbItem in wbs)
            {
                var orders = _contextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == wbItem.Id && ord.IsClose == true && ord.md_order_state.Id != (int)OrderStates.Delete).ToList();
                foreach (order orderItem in orders)
                {
                    var orderRecords = _contextMotorDepot.ordertSet.Where(rec => rec.md_order.Id == orderItem.Id).ToList();
                    foreach (ordert orderRecord in orderRecords)
                    { 
                        DeviationKilometrageFields repDataItem = new DeviationKilometrageFields();
                        repDataItem.IdOrder = orderItem.Id;
                        repDataItem.IdWayBill = wbItem.Id;
                        repDataItem.DateWayBill = wbItem.Date;
                        repDataItem.NumberOrder = orderItem.Number;
                        if (wbItem.vehicle != null) repDataItem.NumberPlate = wbItem.vehicle.NumberPlate;
                        if (orderRecord.md_object != null) repDataItem.ObjectName = orderRecord.md_object.Name; 
                        repDataItem.DistToObjectPlan = (double)orderRecord.DistanceToObject;
                        repDataItem.DistToObjectFact = (double)orderRecord.DistanceToObjectFact;
                        repDataItem.DistToObjectDeviation = (double)orderRecord.DistanceToObject - (double)orderRecord.DistanceToObjectFact;
                        repDataItem.Remark = orderRecord.Remark;
                        repData.Add(repDataItem);
                    }
                }
            }

            return repData;
        }
    }
}
