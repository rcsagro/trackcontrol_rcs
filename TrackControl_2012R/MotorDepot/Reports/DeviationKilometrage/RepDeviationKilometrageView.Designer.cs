﻿namespace MotorDepot.Reports
{
    partial class RepDeviationKilometrageView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepDeviationKilometrageView));
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.gcDevKm = new DevExpress.XtraGrid.GridControl();
            this.gvDevKm = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdWayBill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeviation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDevKm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDevKm)).BeginInit();
            this.SuspendLayout();
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcDevKm;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystem = this.psReport;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // gcDevKm
            // 
            this.gcDevKm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDevKm.Location = new System.Drawing.Point(0, 0);
            this.gcDevKm.MainView = this.gvDevKm;
            this.gcDevKm.Name = "gcDevKm";
            this.gcDevKm.Size = new System.Drawing.Size(827, 422);
            this.gcDevKm.TabIndex = 0;
            this.gcDevKm.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDevKm});
            // 
            // gvDevKm
            // 
            this.gvDevKm.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdOrder,
            this.colIdWayBill,
            this.colDate,
            this.colNumberOrder,
            this.colObject,
            this.colNumberPlate,
            this.colDistPlan,
            this.colDistFact,
            this.colDeviation,
            this.colRemark});
            this.gvDevKm.GridControl = this.gcDevKm;
            this.gvDevKm.IndicatorWidth = 30;
            this.gvDevKm.Name = "gvDevKm";
            this.gvDevKm.OptionsView.ShowFooter = true;
            this.gvDevKm.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvDevKm_CustomDrawRowIndicator);
            this.gvDevKm.DoubleClick += new System.EventHandler(this.gvDevKm_DoubleClick);
            // 
            // colIdOrder
            // 
            this.colIdOrder.Caption = "Id";
            this.colIdOrder.FieldName = "IdOrder";
            this.colIdOrder.Name = "colIdOrder";
            // 
            // colIdWayBill
            // 
            this.colIdWayBill.AppearanceCell.Options.UseTextOptions = true;
            this.colIdWayBill.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdWayBill.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdWayBill.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdWayBill.Caption = "Путевой лист";
            this.colIdWayBill.FieldName = "IdWayBill";
            this.colIdWayBill.Name = "colIdWayBill";
            this.colIdWayBill.OptionsColumn.AllowEdit = false;
            this.colIdWayBill.OptionsColumn.ReadOnly = true;
            this.colIdWayBill.Visible = true;
            this.colIdWayBill.VisibleIndex = 0;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.Caption = "Дата";
            this.colDate.FieldName = "DateWayBill";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            // 
            // colNumberOrder
            // 
            this.colNumberOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.Caption = "Заказ-наряд";
            this.colNumberOrder.FieldName = "NumberOrder";
            this.colNumberOrder.Name = "colNumberOrder";
            this.colNumberOrder.OptionsColumn.AllowEdit = false;
            this.colNumberOrder.OptionsColumn.ReadOnly = true;
            this.colNumberOrder.Visible = true;
            this.colNumberOrder.VisibleIndex = 2;
            // 
            // colObject
            // 
            this.colObject.AppearanceCell.Options.UseTextOptions = true;
            this.colObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObject.Caption = "Объект";
            this.colObject.FieldName = "ObjectName";
            this.colObject.Name = "colObject";
            this.colObject.OptionsColumn.AllowEdit = false;
            this.colObject.OptionsColumn.ReadOnly = true;
            this.colObject.Visible = true;
            this.colObject.VisibleIndex = 3;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.Caption = "Транспорт";
            this.colNumberPlate.FieldName = "NumberPlate";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.Visible = true;
            this.colNumberPlate.VisibleIndex = 4;
            // 
            // colDistPlan
            // 
            this.colDistPlan.AppearanceCell.Options.UseTextOptions = true;
            this.colDistPlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistPlan.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistPlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistPlan.Caption = "Пробег план, км";
            this.colDistPlan.FieldName = "DistToObjectPlan";
            this.colDistPlan.Name = "colDistPlan";
            this.colDistPlan.OptionsColumn.AllowEdit = false;
            this.colDistPlan.OptionsColumn.ReadOnly = true;
            this.colDistPlan.SummaryItem.DisplayFormat = "{0:F2}";
            this.colDistPlan.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDistPlan.Visible = true;
            this.colDistPlan.VisibleIndex = 5;
            // 
            // colDistFact
            // 
            this.colDistFact.AppearanceCell.Options.UseTextOptions = true;
            this.colDistFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistFact.Caption = "Пробег факт, км";
            this.colDistFact.FieldName = "DistToObjectFact";
            this.colDistFact.Name = "colDistFact";
            this.colDistFact.OptionsColumn.AllowEdit = false;
            this.colDistFact.OptionsColumn.ReadOnly = true;
            this.colDistFact.SummaryItem.DisplayFormat = "{0:F2}";
            this.colDistFact.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDistFact.Visible = true;
            this.colDistFact.VisibleIndex = 6;
            // 
            // colDeviation
            // 
            this.colDeviation.AppearanceCell.Options.UseTextOptions = true;
            this.colDeviation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeviation.Caption = "Разница, км";
            this.colDeviation.FieldName = "DistToObjectDeviation";
            this.colDeviation.Name = "colDeviation";
            this.colDeviation.OptionsColumn.AllowEdit = false;
            this.colDeviation.OptionsColumn.ReadOnly = true;
            this.colDeviation.SummaryItem.DisplayFormat = "{0:F2}";
            this.colDeviation.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDeviation.Visible = true;
            this.colDeviation.VisibleIndex = 7;
            // 
            // colRemark
            // 
            this.colRemark.AppearanceCell.Options.UseTextOptions = true;
            this.colRemark.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRemark.Caption = "Примечание";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.OptionsColumn.AllowEdit = false;
            this.colRemark.OptionsColumn.ReadOnly = true;
            this.colRemark.Visible = true;
            this.colRemark.VisibleIndex = 8;
            // 
            // RepDeviationKilometrageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcDevKm);
            this.Name = "RepDeviationKilometrageView";
            this.Size = new System.Drawing.Size(827, 422);
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDevKm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDevKm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDevKm;
        private DevExpress.XtraGrid.Columns.GridColumn colIdWayBill;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colDistPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colDistFact;
        private DevExpress.XtraGrid.Columns.GridColumn colDeviation;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        internal DevExpress.XtraGrid.GridControl gcDevKm;
        private DevExpress.XtraGrid.Columns.GridColumn colObject;
        private DevExpress.XtraGrid.Columns.GridColumn colIdOrder;
    }
}
