﻿namespace MotorDepot.Reports
{
    partial class RepLogEventsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepLogEventsView));
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.gcEvent = new DevExpress.XtraGrid.GridControl();
            this.gvEvent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateNumberAuto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEntryToObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeExitFromObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeInObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeToObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceToObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcEvent;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystem = this.psReport;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // gcEvent
            // 
            this.gcEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcEvent.Location = new System.Drawing.Point(0, 0);
            this.gcEvent.MainView = this.gvEvent;
            this.gcEvent.Name = "gcEvent";
            this.gcEvent.Size = new System.Drawing.Size(744, 365);
            this.gcEvent.TabIndex = 0;
            this.gcEvent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEvent});
            // 
            // gvEvent
            // 
            this.gvEvent.ColumnPanelRowHeight = 40;
            this.gvEvent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdOrder,
            this.colDate,
            this.colOrder,
            this.colStateNumberAuto,
            this.colObject,
            this.colTimeEntryToObject,
            this.colTimeExitFromObject,
            this.colTimeInObject,
            this.colTimeToObject,
            this.colDistanceToObject,
            this.colRemark});
            this.gvEvent.GridControl = this.gcEvent;
            this.gvEvent.IndicatorWidth = 30;
            this.gvEvent.Name = "gvEvent";
            this.gvEvent.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvEvent_CustomDrawRowIndicator);
            this.gvEvent.DoubleClick += new System.EventHandler(this.gvEvent_DoubleClick);
            // 
            // colIdOrder
            // 
            this.colIdOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdOrder.Caption = "Id";
            this.colIdOrder.FieldName = "IdOrder";
            this.colIdOrder.Name = "colIdOrder";
            this.colIdOrder.OptionsColumn.AllowEdit = false;
            this.colIdOrder.OptionsColumn.ReadOnly = true;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.Caption = "Дата";
            this.colDate.DisplayFormat.FormatString = "g";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "DateStart";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 0;
            this.colDate.Width = 71;
            // 
            // colOrder
            // 
            this.colOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOrder.Caption = "Заказ-наряд";
            this.colOrder.FieldName = "NumberOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Visible = true;
            this.colOrder.VisibleIndex = 1;
            this.colOrder.Width = 88;
            // 
            // colStateNumberAuto
            // 
            this.colStateNumberAuto.AppearanceCell.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colStateNumberAuto.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStateNumberAuto.Caption = "Госномер машины";
            this.colStateNumberAuto.FieldName = "NumberPlate";
            this.colStateNumberAuto.Name = "colStateNumberAuto";
            this.colStateNumberAuto.OptionsColumn.AllowEdit = false;
            this.colStateNumberAuto.OptionsColumn.ReadOnly = true;
            this.colStateNumberAuto.Visible = true;
            this.colStateNumberAuto.VisibleIndex = 2;
            this.colStateNumberAuto.Width = 68;
            // 
            // colObject
            // 
            this.colObject.AppearanceCell.Options.UseTextOptions = true;
            this.colObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colObject.Caption = "Объект";
            this.colObject.FieldName = "ObjectName";
            this.colObject.Name = "colObject";
            this.colObject.OptionsColumn.AllowEdit = false;
            this.colObject.OptionsColumn.ReadOnly = true;
            this.colObject.Visible = true;
            this.colObject.VisibleIndex = 3;
            this.colObject.Width = 68;
            // 
            // colTimeEntryToObject
            // 
            this.colTimeEntryToObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEntryToObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEntryToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEntryToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEntryToObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeEntryToObject.Caption = "Время въезда";
            this.colTimeEntryToObject.DisplayFormat.FormatString = "HH:mm";
            this.colTimeEntryToObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEntryToObject.FieldName = "TimeEntryToObject";
            this.colTimeEntryToObject.Name = "colTimeEntryToObject";
            this.colTimeEntryToObject.OptionsColumn.AllowEdit = false;
            this.colTimeEntryToObject.OptionsColumn.ReadOnly = true;
            this.colTimeEntryToObject.Visible = true;
            this.colTimeEntryToObject.VisibleIndex = 4;
            this.colTimeEntryToObject.Width = 68;
            // 
            // colTimeExitFromObject
            // 
            this.colTimeExitFromObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeExitFromObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeExitFromObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeExitFromObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeExitFromObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeExitFromObject.Caption = "Время выезда";
            this.colTimeExitFromObject.DisplayFormat.FormatString = "HH:mm";
            this.colTimeExitFromObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeExitFromObject.FieldName = "TimeExitFromObject";
            this.colTimeExitFromObject.Name = "colTimeExitFromObject";
            this.colTimeExitFromObject.OptionsColumn.AllowEdit = false;
            this.colTimeExitFromObject.OptionsColumn.ReadOnly = true;
            this.colTimeExitFromObject.Visible = true;
            this.colTimeExitFromObject.VisibleIndex = 5;
            this.colTimeExitFromObject.Width = 68;
            // 
            // colTimeInObject
            // 
            this.colTimeInObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeInObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeInObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeInObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeInObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeInObject.Caption = "Время на объекте ,ч";
            this.colTimeInObject.DisplayFormat.FormatString = "{0:f2}";
            this.colTimeInObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeInObject.FieldName = "TimeInObject";
            this.colTimeInObject.Name = "colTimeInObject";
            this.colTimeInObject.OptionsColumn.AllowEdit = false;
            this.colTimeInObject.OptionsColumn.ReadOnly = true;
            this.colTimeInObject.Visible = true;
            this.colTimeInObject.VisibleIndex = 6;
            this.colTimeInObject.Width = 68;
            // 
            // colTimeToObject
            // 
            this.colTimeToObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeToObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeToObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeToObject.Caption = "Время движения к объекту ,ч";
            this.colTimeToObject.DisplayFormat.FormatString = "{0:f2}";
            this.colTimeToObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeToObject.FieldName = "TimeMoveToObject";
            this.colTimeToObject.Name = "colTimeToObject";
            this.colTimeToObject.OptionsColumn.AllowEdit = false;
            this.colTimeToObject.OptionsColumn.ReadOnly = true;
            this.colTimeToObject.Visible = true;
            this.colTimeToObject.VisibleIndex = 7;
            this.colTimeToObject.Width = 68;
            // 
            // colDistanceToObject
            // 
            this.colDistanceToObject.AppearanceCell.Options.UseTextOptions = true;
            this.colDistanceToObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistanceToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistanceToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistanceToObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistanceToObject.Caption = "Расстояние до объекта, км";
            this.colDistanceToObject.DisplayFormat.FormatString = "{0:f2}";
            this.colDistanceToObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistanceToObject.FieldName = "DistToObject";
            this.colDistanceToObject.Name = "colDistanceToObject";
            this.colDistanceToObject.OptionsColumn.AllowEdit = false;
            this.colDistanceToObject.OptionsColumn.ReadOnly = true;
            this.colDistanceToObject.Visible = true;
            this.colDistanceToObject.VisibleIndex = 8;
            this.colDistanceToObject.Width = 68;
            // 
            // colRemark
            // 
            this.colRemark.AppearanceCell.Options.UseTextOptions = true;
            this.colRemark.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.colRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRemark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRemark.Caption = "Примечание";
            this.colRemark.FieldName = "Remark";
            this.colRemark.Name = "colRemark";
            this.colRemark.OptionsColumn.AllowEdit = false;
            this.colRemark.OptionsColumn.ReadOnly = true;
            this.colRemark.Visible = true;
            this.colRemark.VisibleIndex = 9;
            // 
            // RepLogEventsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcEvent);
            this.Name = "RepLogEventsView";
            this.Size = new System.Drawing.Size(744, 365);
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEvent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gvEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colIdOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStateNumberAuto;
        private DevExpress.XtraGrid.Columns.GridColumn colObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEntryToObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeExitFromObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeInObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeToObject;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceToObject;
        private DevExpress.XtraGrid.Columns.GridColumn colRemark;
        public DevExpress.XtraGrid.GridControl gcEvent;
    }
}
