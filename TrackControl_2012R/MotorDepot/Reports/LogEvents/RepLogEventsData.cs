﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorDepot.Reports
{
    public class RepLogEventsData
    {
        MotorDepotEntities _contextMotorDepot;
        public RepLogEventsData()
        {
            _contextMotorDepot = MDController.Instance.ContextMotorDepot;
        }

        public List<LogEventsFields> GetData(DateTime start,DateTime end)
        {
            List<LogEventsFields> repData = new List<LogEventsFields>();
            var orders = _contextMotorDepot.orderSet.Include("md_waybill").Where(ord => ord.DateStartWork >= start && ord.DateStartWork <= end && ord.md_order_state.Id != (int)OrderStates.Delete && ord.IsClose == true).ToList();
            foreach (order orderItem in orders)
            {
                var orderRecords = _contextMotorDepot.ordertSet.Where(rec => rec.md_order.Id == orderItem.Id).ToList();
                foreach (ordert orderRecord in orderRecords)
                {
                    LogEventsFields repDataItem = new LogEventsFields();
                    repDataItem.IdOrder = orderItem.Id;
                    repDataItem.DateStart = (DateTime)orderItem.DateStartWork;
                    repDataItem.NumberOrder = orderItem.Number;
                    if (orderItem.md_waybill != null && orderItem.md_waybill.vehicle != null)
                    repDataItem.NumberPlate = orderItem.md_waybill.vehicle.NumberPlate;
                    if (orderRecord.md_object != null)
                    repDataItem.ObjectName = orderRecord.md_object.Name;
                    repDataItem.TimeMoveToObject = (double)orderRecord.TimeToObjectFact;
                    repDataItem.TimeEntryToObject = orderRecord.TimeEnter;
                    repDataItem.TimeExitFromObject = orderRecord.TimeExit;
                    if (repDataItem.TimeExitFromObject != null && repDataItem.TimeEntryToObject != null)
                    repDataItem.TimeInObject =
                        ((DateTime)repDataItem.TimeExitFromObject).Subtract((DateTime)repDataItem.TimeEntryToObject).TotalHours;
                    repDataItem.DistToObject = (double)orderRecord.DistanceToObjectFact;
                    repDataItem.Remark = orderRecord.Remark;
                    repData.Add(repDataItem);
                }
            }

            return repData;
        }
    }
}
