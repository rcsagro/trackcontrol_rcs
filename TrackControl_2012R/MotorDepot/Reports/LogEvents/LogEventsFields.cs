﻿using System;

namespace MotorDepot.Reports
{
    public class LogEventsFields
    {
        public int IdOrder { get; set; }
        
        public DateTime DateStart { get; set; }

        public string NumberOrder { get; set; }

        public string NumberPlate { get; set; }

        public string ObjectName { get; set; }

        public DateTime? TimeEntryToObject { get; set; }

        public DateTime? TimeExitFromObject { get; set; }

        public double TimeMoveToObject { get; set; }

        public double TimeInObject { get; set; }

        public double DistToObject { get; set; }

        public string Remark { get; set; }
    }
}
