﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class LogEvents : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        string _caption = "Журнал событий";
        Image _largeImage = Shared.NetworkingEvent;
        Image _smallImage = Shared.NetworkingEvent;
        private List<LogEventsFields> _reportData;

        public LogEvents()
        {
            _dataViewer = new RepLogEventsView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run( DateTime begin, DateTime end )
        {
            RepLogEventsData dataSource = new RepLogEventsData();
            _reportData = dataSource.GetData(begin, end);
            ((RepLogEventsView)_dataViewer).gcEvent.DataSource = _reportData;
            ((RepLogEventsView) _dataViewer).Begin = begin;
            ((RepLogEventsView)_dataViewer).End = end;
        }

        public bool IsHasData
        {

            get { return _reportData == null ? false : !(_reportData.Count == 0); }
        }

        public void ExportToExcel()
        {
            ( ( RepLogEventsView )_dataViewer ).ExportToExcel();
        }
    } // LogEvents
} // MotorDepot.Reports
