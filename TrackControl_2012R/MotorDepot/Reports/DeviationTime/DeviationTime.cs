﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class DeviationTime : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        string _caption = "Журнал отклонений по времени";
        Image _largeImage = Shared.NetworkingTime;
        Image _smallImage = Shared.NetworkingTime;

        private List<RepDeviationTimeFields> _reportData;

        public DeviationTime()
        {
            _dataViewer = new RepDeviationTimeView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run( DateTime begin, DateTime end )
        {
            RepDeviationTimeData dataSource = new RepDeviationTimeData();
            _reportData = dataSource.GetData(begin, end);
            ((RepDeviationTimeView)_dataViewer).gcDevTime.DataSource = _reportData;
            ((RepDeviationTimeView)_dataViewer).Begin = begin;
            ((RepDeviationTimeView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return _reportData == null ? false : !(_reportData.Count == 0); }
        }

        public void ExportToExcel()
        {
            ( ( RepDeviationTimeView )_dataViewer ).ExportToExcel();
        }
    }
}
