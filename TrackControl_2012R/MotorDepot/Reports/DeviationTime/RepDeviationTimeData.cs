﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorDepot.Reports
{
    public  class RepDeviationTimeData
    {
        MotorDepotEntities _contextMotorDepot;

        public RepDeviationTimeData()
        {
            _contextMotorDepot = MDController.Instance.ContextMotorDepot;
        }

        public List<RepDeviationTimeFields> GetData(DateTime start, DateTime end)
        {
            bool firstOrderRecord = true;
            List<RepDeviationTimeFields> repData = new List<RepDeviationTimeFields>();
            var wbs = _contextMotorDepot.waybillSet.Where(wb => wb.Date >= start && wb.Date <= end ).ToList();
            foreach (waybill wbItem in wbs)
            {
                var orders = _contextMotorDepot.orderSet.Where(ord => ord.md_waybill.Id == wbItem.Id && ord.IsClose == true && ord.md_order_state.Id != (int)OrderStates.Delete).ToList();
                foreach (order orderItem in orders)
                {
                    var orderRecords = _contextMotorDepot.ordertSet.Where(rec => rec.md_order.Id == orderItem.Id).ToList();
                    firstOrderRecord = true;
                    foreach (ordert orderRecord in orderRecords)
                    {
                        RepDeviationTimeFields repDataItem = new RepDeviationTimeFields();
                        repDataItem.IdOrder = orderItem.Id;
                        repDataItem.IdWayBill = wbItem.Id;
                        repDataItem.DateWayBill = wbItem.Date;
                        repDataItem.NumberOrder = orderItem.Number;
                        if (wbItem.vehicle != null) repDataItem.NumberPlate = wbItem.vehicle.NumberPlate;
                        if (orderRecord.md_object != null) repDataItem.ObjectName = orderRecord.md_object.Name;

                        repDataItem.TimeShiftDelay = orderItem.TimeDelay;
                        repDataItem.TimeMove = orderRecord.TimeToObject;
                        repDataItem.TimeLoad = orderRecord.TimeLoading;
                        repDataItem.TimeUnload = orderRecord.TimeUnLoading;
                        repDataItem.TimeDelay= orderRecord.TimeDelay;

                        repDataItem.TimeMoveFact  = orderRecord.TimeToObjectFact;
                        repDataItem.TimeStopToObjectFact = orderRecord.TimeStopFact;
                        repDataItem.TimeStopInObjectFact= orderRecord.TimeStopFactInObject;
                        if (firstOrderRecord && repDataItem.TimeShiftDelay != null)
                        {
                            repDataItem.TimeMoveToObjectDeviation = repDataItem.TimeMoveFact - (repDataItem.TimeMove + (decimal)((TimeSpan)repDataItem.TimeShiftDelay).TotalHours);
                        }
                        else
                        {
                            repDataItem.TimeMoveToObjectDeviation = repDataItem.TimeMoveFact - repDataItem.TimeMove;
                        }
                        repDataItem.TimeMoveToObjectDeviation = Math.Round(repDataItem.TimeMoveToObjectDeviation, 3); 
                        repDataItem.TimeStopInObjectDeviation = repDataItem.TimeStopInObjectFact - (orderRecord.TimeLoading + orderRecord.TimeUnLoading + orderRecord.TimeDelay);
                        repDataItem.TimeStopInObjectDeviation = Math.Round(repDataItem.TimeStopInObjectDeviation, 3);
                        repData.Add(repDataItem);
                        if (firstOrderRecord) firstOrderRecord = false; ;
                    }
                }
            }

            return repData;
        }

    }
}
