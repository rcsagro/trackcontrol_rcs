﻿namespace MotorDepot.Reports
{
    partial class RepDeviationTimeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepDeviationTimeView));
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.gcDevTime = new DevExpress.XtraGrid.GridControl();
            this.bgvDevTime = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colIdOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colIdWayBill = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumberOrder = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colObjectName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimeShiftDelay = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeLoad = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeUnload = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeDelay = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimeMoveFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStopInObjectFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStopToObjectFact = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimeMoveToObjectDeviation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimeStopInObjectDeviation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDevTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvDevTime)).BeginInit();
            this.SuspendLayout();
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcDevTime;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystem = this.psReport;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // gcDevTime
            // 
            this.gcDevTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDevTime.Location = new System.Drawing.Point(0, 0);
            this.gcDevTime.MainView = this.bgvDevTime;
            this.gcDevTime.Name = "gcDevTime";
            this.gcDevTime.Size = new System.Drawing.Size(868, 430);
            this.gcDevTime.TabIndex = 0;
            this.gcDevTime.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvDevTime});
            // 
            // bgvDevTime
            // 
            this.bgvDevTime.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.bgvDevTime.ColumnPanelRowHeight = 40;
            this.bgvDevTime.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colIdOrder,
            this.colIdWayBill,
            this.colDate,
            this.colNumberOrder,
            this.colNumberPlate,
            this.colObjectName,
            this.colTimeShiftDelay,
            this.colTimeMove,
            this.colTimeLoad,
            this.colTimeUnload,
            this.colTimeDelay,
            this.colTimeMoveFact,
            this.colTimeStopToObjectFact,
            this.colTimeStopInObjectFact,
            this.colTimeMoveToObjectDeviation,
            this.colTimeStopInObjectDeviation});
            this.bgvDevTime.GridControl = this.gcDevTime;
            this.bgvDevTime.IndicatorWidth = 30;
            this.bgvDevTime.Name = "bgvDevTime";
            this.bgvDevTime.OptionsView.ShowFooter = true;
            this.bgvDevTime.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.bgvDevTime_CustomDrawRowIndicator);
            this.bgvDevTime.DoubleClick += new System.EventHandler(this.bgvDevTime_DoubleClick);
            // 
            // gridBand1
            // 
            this.gridBand1.Columns.Add(this.colIdOrder);
            this.gridBand1.Columns.Add(this.colDate);
            this.gridBand1.Columns.Add(this.colIdWayBill);
            this.gridBand1.Columns.Add(this.colNumberOrder);
            this.gridBand1.Columns.Add(this.colObjectName);
            this.gridBand1.Columns.Add(this.colNumberPlate);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.OptionsBand.ShowCaption = false;
            this.gridBand1.Width = 375;
            // 
            // colIdOrder
            // 
            this.colIdOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colIdOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdOrder.Caption = "Id";
            this.colIdOrder.FieldName = "IdOrder";
            this.colIdOrder.Name = "colIdOrder";
            this.colIdOrder.OptionsColumn.AllowEdit = false;
            this.colIdOrder.OptionsColumn.ReadOnly = true;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.Caption = "Дата";
            this.colDate.FieldName = "DateWayBill";
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.Visible = true;
            // 
            // colIdWayBill
            // 
            this.colIdWayBill.AppearanceCell.Options.UseTextOptions = true;
            this.colIdWayBill.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdWayBill.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdWayBill.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdWayBill.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdWayBill.Caption = "Путевой лист";
            this.colIdWayBill.FieldName = "IdWayBill";
            this.colIdWayBill.Name = "colIdWayBill";
            this.colIdWayBill.OptionsColumn.AllowEdit = false;
            this.colIdWayBill.OptionsColumn.ReadOnly = true;
            this.colIdWayBill.Visible = true;
            // 
            // colNumberOrder
            // 
            this.colNumberOrder.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberOrder.Caption = "Заказ - наряд";
            this.colNumberOrder.FieldName = "NumberOrder";
            this.colNumberOrder.Name = "colNumberOrder";
            this.colNumberOrder.OptionsColumn.AllowEdit = false;
            this.colNumberOrder.OptionsColumn.ReadOnly = true;
            this.colNumberOrder.Visible = true;
            // 
            // colObjectName
            // 
            this.colObjectName.AppearanceCell.Options.UseTextOptions = true;
            this.colObjectName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colObjectName.AppearanceHeader.Options.UseTextOptions = true;
            this.colObjectName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObjectName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colObjectName.Caption = "Объект";
            this.colObjectName.FieldName = "ObjectName";
            this.colObjectName.Name = "colObjectName";
            this.colObjectName.OptionsColumn.AllowEdit = false;
            this.colObjectName.OptionsColumn.ReadOnly = true;
            this.colObjectName.Visible = true;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceCell.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumberPlate.Caption = "Транспорт";
            this.colNumberPlate.FieldName = "NumberPlate";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.Visible = true;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "время по плану, ч";
            this.gridBand2.Columns.Add(this.colTimeShiftDelay);
            this.gridBand2.Columns.Add(this.colTimeMove);
            this.gridBand2.Columns.Add(this.colTimeLoad);
            this.gridBand2.Columns.Add(this.colTimeUnload);
            this.gridBand2.Columns.Add(this.colTimeDelay);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 375;
            // 
            // colTimeShiftDelay
            // 
            this.colTimeShiftDelay.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeShiftDelay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeShiftDelay.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeShiftDelay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeShiftDelay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeShiftDelay.Caption = "отсрочки";
            this.colTimeShiftDelay.FieldName = "TimeShiftDelay";
            this.colTimeShiftDelay.Name = "colTimeShiftDelay";
            this.colTimeShiftDelay.OptionsColumn.AllowEdit = false;
            this.colTimeShiftDelay.OptionsColumn.ReadOnly = true;
            this.colTimeShiftDelay.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeShiftDelay.Visible = true;
            // 
            // colTimeMove
            // 
            this.colTimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMove.Caption = "движения";
            this.colTimeMove.FieldName = "TimeMove";
            this.colTimeMove.Name = "colTimeMove";
            this.colTimeMove.OptionsColumn.AllowEdit = false;
            this.colTimeMove.OptionsColumn.ReadOnly = true;
            this.colTimeMove.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeMove.Visible = true;
            // 
            // colTimeLoad
            // 
            this.colTimeLoad.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeLoad.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoad.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeLoad.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeLoad.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeLoad.Caption = "погрузки";
            this.colTimeLoad.FieldName = "TimeLoad";
            this.colTimeLoad.Name = "colTimeLoad";
            this.colTimeLoad.OptionsColumn.AllowEdit = false;
            this.colTimeLoad.OptionsColumn.ReadOnly = true;
            this.colTimeLoad.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeLoad.Visible = true;
            // 
            // colTimeUnload
            // 
            this.colTimeUnload.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeUnload.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnload.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeUnload.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeUnload.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeUnload.Caption = "разгрузки";
            this.colTimeUnload.FieldName = "TimeUnload";
            this.colTimeUnload.Name = "colTimeUnload";
            this.colTimeUnload.OptionsColumn.AllowEdit = false;
            this.colTimeUnload.OptionsColumn.ReadOnly = true;
            this.colTimeUnload.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeUnload.Visible = true;
            // 
            // colTimeDelay
            // 
            this.colTimeDelay.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeDelay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDelay.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeDelay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeDelay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeDelay.Caption = "задержки";
            this.colTimeDelay.FieldName = "TimeDelay";
            this.colTimeDelay.Name = "colTimeDelay";
            this.colTimeDelay.OptionsColumn.AllowEdit = false;
            this.colTimeDelay.OptionsColumn.ReadOnly = true;
            this.colTimeDelay.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeDelay.Visible = true;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "время по факту, ч";
            this.gridBand3.Columns.Add(this.colTimeMoveFact);
            this.gridBand3.Columns.Add(this.colTimeStopInObjectFact);
            this.gridBand3.Columns.Add(this.colTimeStopToObjectFact);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 279;
            // 
            // colTimeMoveFact
            // 
            this.colTimeMoveFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMoveFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoveFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMoveFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoveFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoveFact.Caption = "движения";
            this.colTimeMoveFact.DisplayFormat.FormatString = "{0:f2}";
            this.colTimeMoveFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeMoveFact.FieldName = "TimeMoveFact";
            this.colTimeMoveFact.Name = "colTimeMoveFact";
            this.colTimeMoveFact.OptionsColumn.AllowEdit = false;
            this.colTimeMoveFact.OptionsColumn.ReadOnly = true;
            this.colTimeMoveFact.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeMoveFact.Visible = true;
            // 
            // colTimeStopInObjectFact
            // 
            this.colTimeStopInObjectFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopInObjectFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopInObjectFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopInObjectFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopInObjectFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopInObjectFact.Caption = "стоянки в объекте";
            this.colTimeStopInObjectFact.DisplayFormat.FormatString = "{0:f2}";
            this.colTimeStopInObjectFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeStopInObjectFact.FieldName = "TimeStopInObjectFact";
            this.colTimeStopInObjectFact.Name = "colTimeStopInObjectFact";
            this.colTimeStopInObjectFact.OptionsColumn.AllowEdit = false;
            this.colTimeStopInObjectFact.OptionsColumn.ReadOnly = true;
            this.colTimeStopInObjectFact.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStopInObjectFact.Visible = true;
            this.colTimeStopInObjectFact.Width = 110;
            // 
            // colTimeStopToObjectFact
            // 
            this.colTimeStopToObjectFact.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopToObjectFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopToObjectFact.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopToObjectFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopToObjectFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopToObjectFact.Caption = "стоянки до объекта";
            this.colTimeStopToObjectFact.DisplayFormat.FormatString = "{0:f2}";
            this.colTimeStopToObjectFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeStopToObjectFact.FieldName = "TimeStopToObjectFact";
            this.colTimeStopToObjectFact.Name = "colTimeStopToObjectFact";
            this.colTimeStopToObjectFact.OptionsColumn.AllowEdit = false;
            this.colTimeStopToObjectFact.OptionsColumn.ReadOnly = true;
            this.colTimeStopToObjectFact.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStopToObjectFact.Visible = true;
            this.colTimeStopToObjectFact.Width = 94;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "разница, ч";
            this.gridBand4.Columns.Add(this.colTimeMoveToObjectDeviation);
            this.gridBand4.Columns.Add(this.colTimeStopInObjectDeviation);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 150;
            // 
            // colTimeMoveToObjectDeviation
            // 
            this.colTimeMoveToObjectDeviation.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMoveToObjectDeviation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoveToObjectDeviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMoveToObjectDeviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoveToObjectDeviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoveToObjectDeviation.Caption = "движение";
            this.colTimeMoveToObjectDeviation.FieldName = "TimeMoveToObjectDeviation";
            this.colTimeMoveToObjectDeviation.Name = "colTimeMoveToObjectDeviation";
            this.colTimeMoveToObjectDeviation.OptionsColumn.AllowEdit = false;
            this.colTimeMoveToObjectDeviation.OptionsColumn.ReadOnly = true;
            this.colTimeMoveToObjectDeviation.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeMoveToObjectDeviation.Visible = true;
            // 
            // colTimeStopInObjectDeviation
            // 
            this.colTimeStopInObjectDeviation.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopInObjectDeviation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopInObjectDeviation.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopInObjectDeviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopInObjectDeviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopInObjectDeviation.Caption = "простой";
            this.colTimeStopInObjectDeviation.FieldName = "TimeStopInObjectDeviation";
            this.colTimeStopInObjectDeviation.Name = "colTimeStopInObjectDeviation";
            this.colTimeStopInObjectDeviation.OptionsColumn.AllowEdit = false;
            this.colTimeStopInObjectDeviation.OptionsColumn.ReadOnly = true;
            this.colTimeStopInObjectDeviation.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimeStopInObjectDeviation.Visible = true;
            // 
            // RepDeviationTimeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcDevTime);
            this.Name = "RepDeviationTimeView";
            this.Size = new System.Drawing.Size(868, 430);
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDevTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvDevTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvDevTime;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIdOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colIdWayBill;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumberOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNumberPlate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colObjectName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeShiftDelay;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeLoad;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeUnload;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeDelay;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeMoveFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStopToObjectFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStopInObjectFact;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeMoveToObjectDeviation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimeStopInObjectDeviation;
        internal DevExpress.XtraGrid.GridControl gcDevTime;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
    }
}
