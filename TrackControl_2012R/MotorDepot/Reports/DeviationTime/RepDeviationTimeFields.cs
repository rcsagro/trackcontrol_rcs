﻿using System;

namespace MotorDepot.Reports
{
    public class RepDeviationTimeFields
    {
        public int IdOrder { get; set; }

        public int IdWayBill { get; set; }

        public DateTime DateWayBill { get; set; }

        public string NumberOrder { get; set; }

        public string NumberPlate { get; set; }

        public string ObjectName { get; set; }


        public TimeSpan? TimeShiftDelay { get; set; }

        public decimal TimeMove { get; set; }

        public decimal TimeLoad { get; set; }

        public decimal TimeUnload { get; set; }

        public decimal TimeDelay { get; set; }


        public decimal TimeMoveFact { get; set; }

        public decimal TimeStopToObjectFact { get; set; }

        public decimal TimeStopInObjectFact { get; set; }


        public decimal TimeMoveToObjectDeviation { get; set; }

        public decimal TimeStopInObjectDeviation { get; set; }
    }
}
