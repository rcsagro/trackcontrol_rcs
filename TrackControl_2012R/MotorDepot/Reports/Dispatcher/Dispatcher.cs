﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public class Dispatcher : IReportItem
    {
        public event Action<string, int> ChangeStatus;
        UserControl _dataViewer;
        string _caption = "Отчет о работе диспетчеров";
        Image _largeImage = Shared.NetworkingDispatchers;
        Image _smallImage = Shared.NetworkingDispatchers;
        private List<RepDispatcherFields> _reportData;

        public Dispatcher()
        {
            _dataViewer = new RepDispatcherView();
            _dataViewer.Dock = DockStyle.Fill;
        }

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string Caption
        {
            get { return _caption; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public System.Drawing.Image LargeImage
        {
            get { return _largeImage; }
        }

        public System.Drawing.Image SmallImage
        {
            get { return _smallImage; }
        }

        public void Run( DateTime begin, DateTime end )
        {
            RepDispatcherData dataSource = new RepDispatcherData();
            _reportData = dataSource.GetData(begin, end);
            ((RepDispatcherView)_dataViewer).gcView.DataSource = _reportData;
            ((RepDispatcherView)_dataViewer).Begin = begin;
            ((RepDispatcherView)_dataViewer).End = end;
        }

        public bool IsHasData
        {
            get { return _reportData == null ? false : !(_reportData.Count == 0); }
        }

        public void ExportToExcel()
        {
            ((RepDispatcherView)_dataViewer).ExportToExcel();
        }
    }
}
