﻿using System;
using System.Windows.Forms;
using TrackControl.General;

namespace MotorDepot.Reports
{
    public partial class RepDispatcherView : UserControl
    {
        public RepDispatcherView()
        {
            InitializeComponent();
        }

        DateTime _begin;

        public DateTime Begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        DateTime _end;

        public DateTime End
        {
            get { return _end; }
            set { _end = value; }
        }

        private void bgvView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        public void ExportToExcel()
        {
            XtraGridService.SetupGidViewForPrint(bgvView, false, false);
            linkGrid.CreateDocument();
            linkGrid.ShowPreview();
        }

        private void linkGrid_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            string reportHeader = "Отчет о работе диспетчеров";
            XtraGridService.DevExpressReportHeader(reportHeader, e);
            XtraGridService.DevExpressReportSubHeader(string.Format("{2} {0} {3} {1}",
            _begin.ToString("dd.MM.yyyy"), _end.ToString("dd.MM.yyyy"), "Период с", "по"), 60, e);
        }
    }
}
