﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MotorDepot.Reports
{
    public  class RepDispatcherData
    {
        MotorDepotEntities _contextMotorDepot;
        int _countOrdersWithDateInit = 0;
        public RepDispatcherData()
        {
            _contextMotorDepot = MDController.Instance.ContextMotorDepot;
        }

        public List<RepDispatcherFields> GetData(DateTime start, DateTime end)
        {
            List<RepDispatcherFields> repData = new List<RepDispatcherFields>();
            var orders = _contextMotorDepot.orderSet.Include("md_waybill").Where(ord => ord.DateCreate >= start && ord.DateCreate <= end && ord.md_order_state.Id != (int)OrderStates.Delete).OrderBy(ord => ord.UserCreator).ToList();
            if (orders == null) return repData;
            string dispName = "";
            RepDispatcherFields repRow = new RepDispatcherFields(); ;
            foreach (order orderItem in orders)
            {
                //Debug.Print(orderItem.Number);
                if (orderItem.UserCreator != null)
                {
                    if (dispName != orderItem.UserCreator)
                    {
                        if (dispName.Length > 0)
                        {
                            UpdateDataRecordTimes(repRow);
                            repRow = new RepDispatcherFields();
                        }

                        dispName = orderItem.UserCreator;
                        repRow.DispName = dispName;
                        UpdateDataRecord(repRow, orderItem);
                        repData.Add(repRow);
                    }
                    else
                    {
                        UpdateDataRecord(repRow, orderItem);
                    }
                }
            }
            if (dispName.Length > 0)
            {
                UpdateDataRecordTimes(repRow);
            }
            return repData;
        }

        private void UpdateDataRecordTimes(RepDispatcherFields repRow)
        {
            if (_countOrdersWithDateInit > 0)
            {
                repRow.TimedWaitingOrdersAvg = Math.Round(repRow.TimedWaitingOrders / _countOrdersWithDateInit, 2);
                repRow.TimedWaitingOrders = Math.Round(repRow.TimedWaitingOrders, 2);
                _countOrdersWithDateInit = 0;
            }
        }

        private  void UpdateDataRecord(RepDispatcherFields repRow, order orderItem)
        {
            repRow.CountOrders++;
            if (orderItem.StateId == (int)OrderStates.Refuse) repRow.CountCanceledOrders++;
            if (orderItem.StateId == (int)OrderStates.Close && orderItem.DateExecute != null) repRow.CountExecutedOrders++;
            if (orderItem.md_waybill == null) 
                repRow.CountNotExecutedOrders++;
            if (orderItem.DateInit != null && orderItem.DateStartWork != null)
            {
                repRow.TimedWaitingOrders += orderItem.DateStartWork.Value.Subtract(orderItem.DateInit.Value).TotalHours;
                _countOrdersWithDateInit++;
            }
        }
    }
}
