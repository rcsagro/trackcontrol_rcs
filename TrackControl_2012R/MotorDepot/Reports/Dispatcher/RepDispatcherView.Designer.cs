﻿namespace MotorDepot.Reports
{
    partial class RepDispatcherView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepDispatcherView));
            this.gcView = new DevExpress.XtraGrid.GridControl();
            this.bgvView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDispId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDispName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCountOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCountCanceledOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCountExecutedOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCountNotExecutedOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colTimedWaitingOrders = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colTimedWaitingOrdersAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.linkGrid = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            this.SuspendLayout();
            // 
            // gcView
            // 
            this.gcView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcView.Location = new System.Drawing.Point(0, 0);
            this.gcView.MainView = this.bgvView;
            this.gcView.Name = "gcView";
            this.gcView.Size = new System.Drawing.Size(753, 411);
            this.gcView.TabIndex = 0;
            this.gcView.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgvView});
            // 
            // bgvView
            // 
            this.bgvView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand1,
            this.gridBand2});
            this.bgvView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colDispId,
            this.colDispName,
            this.colCountOrders,
            this.colCountCanceledOrders,
            this.colCountExecutedOrders,
            this.colCountNotExecutedOrders,
            this.colTimedWaitingOrders,
            this.colTimedWaitingOrdersAvg});
            this.bgvView.GridControl = this.gcView;
            this.bgvView.IndicatorWidth = 30;
            this.bgvView.Name = "bgvView";
            this.bgvView.OptionsBehavior.Editable = false;
            this.bgvView.OptionsView.ShowFooter = true;
            this.bgvView.OptionsView.ShowGroupPanel = false;
            this.bgvView.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.bgvView_CustomDrawRowIndicator);
            // 
            // gridBand3
            // 
            this.gridBand3.Columns.Add(this.colDispId);
            this.gridBand3.Columns.Add(this.colDispName);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 95;
            // 
            // colDispId
            // 
            this.colDispId.Caption = "DispId";
            this.colDispId.FieldName = "DispId";
            this.colDispId.Name = "colDispId";
            // 
            // colDispName
            // 
            this.colDispName.AppearanceCell.Options.UseTextOptions = true;
            this.colDispName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDispName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDispName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDispName.Caption = "Диспетчер";
            this.colDispName.FieldName = "DispName";
            this.colDispName.Name = "colDispName";
            this.colDispName.Visible = true;
            this.colDispName.Width = 95;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Количество заказов";
            this.gridBand1.Columns.Add(this.colCountOrders);
            this.gridBand1.Columns.Add(this.colCountCanceledOrders);
            this.gridBand1.Columns.Add(this.colCountExecutedOrders);
            this.gridBand1.Columns.Add(this.colCountNotExecutedOrders);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 337;
            // 
            // colCountOrders
            // 
            this.colCountOrders.AppearanceCell.Options.UseTextOptions = true;
            this.colCountOrders.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountOrders.AppearanceHeader.Options.UseTextOptions = true;
            this.colCountOrders.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountOrders.Caption = "поступивших";
            this.colCountOrders.FieldName = "CountOrders";
            this.colCountOrders.Name = "colCountOrders";
            this.colCountOrders.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCountOrders.Visible = true;
            this.colCountOrders.Width = 82;
            // 
            // colCountCanceledOrders
            // 
            this.colCountCanceledOrders.AppearanceCell.Options.UseTextOptions = true;
            this.colCountCanceledOrders.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountCanceledOrders.AppearanceHeader.Options.UseTextOptions = true;
            this.colCountCanceledOrders.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountCanceledOrders.Caption = "отмененных заказчиком";
            this.colCountCanceledOrders.FieldName = "CountCanceledOrders";
            this.colCountCanceledOrders.Name = "colCountCanceledOrders";
            this.colCountCanceledOrders.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCountCanceledOrders.Visible = true;
            this.colCountCanceledOrders.Width = 94;
            // 
            // colCountExecutedOrders
            // 
            this.colCountExecutedOrders.AppearanceCell.Options.UseTextOptions = true;
            this.colCountExecutedOrders.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountExecutedOrders.AppearanceHeader.Options.UseTextOptions = true;
            this.colCountExecutedOrders.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountExecutedOrders.Caption = "выполненных";
            this.colCountExecutedOrders.FieldName = "CountExecutedOrders";
            this.colCountExecutedOrders.Name = "colCountExecutedOrders";
            this.colCountExecutedOrders.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCountExecutedOrders.Visible = true;
            this.colCountExecutedOrders.Width = 76;
            // 
            // colCountNotExecutedOrders
            // 
            this.colCountNotExecutedOrders.AppearanceCell.Options.UseTextOptions = true;
            this.colCountNotExecutedOrders.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountNotExecutedOrders.AppearanceHeader.Options.UseTextOptions = true;
            this.colCountNotExecutedOrders.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountNotExecutedOrders.Caption = "не выполненных";
            this.colCountNotExecutedOrders.FieldName = "CountNotExecutedOrders";
            this.colCountNotExecutedOrders.Name = "colCountNotExecutedOrders";
            this.colCountNotExecutedOrders.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colCountNotExecutedOrders.Visible = true;
            this.colCountNotExecutedOrders.Width = 85;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Отчет по времени";
            this.gridBand2.Columns.Add(this.colTimedWaitingOrders);
            this.gridBand2.Columns.Add(this.colTimedWaitingOrdersAvg);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 300;
            // 
            // colTimedWaitingOrders
            // 
            this.colTimedWaitingOrders.AppearanceCell.Options.UseTextOptions = true;
            this.colTimedWaitingOrders.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimedWaitingOrders.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimedWaitingOrders.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimedWaitingOrders.Caption = "Время ожидания заказа заказчиком ,час";
            this.colTimedWaitingOrders.FieldName = "TimedWaitingOrders";
            this.colTimedWaitingOrders.Name = "colTimedWaitingOrders";
            this.colTimedWaitingOrders.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTimedWaitingOrders.Visible = true;
            this.colTimedWaitingOrders.Width = 151;
            // 
            // colTimedWaitingOrdersAvg
            // 
            this.colTimedWaitingOrdersAvg.AppearanceCell.Options.UseTextOptions = true;
            this.colTimedWaitingOrdersAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimedWaitingOrdersAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimedWaitingOrdersAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimedWaitingOrdersAvg.Caption = "Среднее время ожидания заказа ,час";
            this.colTimedWaitingOrdersAvg.DisplayFormat.FormatString = "N2";
            this.colTimedWaitingOrdersAvg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimedWaitingOrdersAvg.FieldName = "TimedWaitingOrdersAvg";
            this.colTimedWaitingOrdersAvg.Name = "colTimedWaitingOrdersAvg";
            this.colTimedWaitingOrdersAvg.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTimedWaitingOrdersAvg.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Average;
            this.colTimedWaitingOrdersAvg.Visible = true;
            this.colTimedWaitingOrdersAvg.Width = 149;
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.linkGrid});
            // 
            // linkGrid
            // 
            this.linkGrid.Component = this.gcView;
            // 
            // 
            // 
            this.linkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("linkGrid.ImageCollection.ImageStream")));
            this.linkGrid.Landscape = true;
            this.linkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.linkGrid.PrintingSystem = this.psReport;
            this.linkGrid.PrintingSystemBase = this.psReport;
            this.linkGrid.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.linkGrid_CreateReportHeaderArea);
            // 
            // RepDispatcherView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcView);
            this.Name = "RepDispatcherView";
            this.Size = new System.Drawing.Size(753, 411);
            ((System.ComponentModel.ISupportInitialize)(this.gcView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgvView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgvView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCountOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCountCanceledOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCountExecutedOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCountNotExecutedOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimedWaitingOrders;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colTimedWaitingOrdersAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDispId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDispName;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        internal DevExpress.XtraGrid.GridControl gcView;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink linkGrid;
    }
}
