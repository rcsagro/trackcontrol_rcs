﻿
namespace MotorDepot.Reports
{
    public class RepDispatcherFields
    {
        //Диспетчер имя
        public string DispName { get; set; }
        //Количество поступивших заказов
        public int CountOrders { get; set; }
        //Количество заказов отмененных заказчиком
        public int CountCanceledOrders { get; set; }
        //Количество выполненных заказов
        public int CountExecutedOrders { get; set; }
        //Количество не выполненных заказов (под невыполненным заказом понимается заказ который не был закреплен за автомобилем)
        public int CountNotExecutedOrders { get; set; }
        //Время ожидания заказа заказчиком ( общее время нахождения заказов в ожидании)
        public double TimedWaitingOrders { get; set; }
        //Среднее время ожидания заказа
        public double TimedWaitingOrdersAvg { get; set; }
    }
}
