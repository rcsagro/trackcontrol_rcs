﻿using System;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using MotorDepot.MonitoringGps;
using System.Linq;
using System.Collections.Generic; 

namespace MotorDepot
{
    public static class OrderTProvider
    {

        static readonly string sqlInsert = @"INSERT INTO md_ordert (Id_main, IdObject, IdCargoLoading, QtyLoading, TimeLoading, IdCargoUnLoading, QtyUnLoading, TimeUnLoading, TimeDelay, TimeToObject, DistanceToObject, Remark, IsClose, DistanceToObjectFact, TimeToObjectFact, TimeStopFact, TimeEnter, TimeExit, IdAlarm, TimeStopFactInObject, TimeDelayInPath, DistanceInObject)
  VALUES ({0}Id_main,  {0}IdObject,  {0}IdCargoLoading,  {0}QtyLoading,  {0}TimeLoading,  {0}IdCargoUnLoading,  {0}QtyUnLoading,  {0}TimeUnLoading,  {0}TimeDelay,  {0}TimeToObject,  {0}DistanceToObject,  {0}Remark,  {0}IsClose,  {0}DistanceToObjectFact,  {0}TimeToObjectFact,  {0}TimeStopFact,  {0}TimeEnter,  {0}TimeExit,  {0}IdAlarm,  {0}TimeStopFactInObject,  {0}TimeDelayInPath,  {0}DistanceInObject)";

        static readonly string sqlUpdate = @"UPDATE md_ordert
SET IdObject = {1}IdObject,
    IdCargoLoading = {1}IdCargoLoading,
    QtyLoading = {1}QtyLoading,
    TimeLoading = {1}TimeLoading,
    IdCargoUnLoading = {1}IdCargoUnLoading,
    QtyUnLoading = {1}QtyUnLoading,
    TimeUnLoading = {1}TimeUnLoading,
    TimeDelay = {1}TimeDelay,
    TimeToObject = {1}TimeToObject,
    DistanceToObject = {1}DistanceToObject,
    Remark = {1}Remark,
    IsClose = {1}IsClose,
    DistanceToObjectFact = {1}DistanceToObjectFact,
    TimeToObjectFact = {1}TimeToObjectFact,
    TimeStopFact = {1}TimeStopFact,
    TimeEnter = {1}TimeEnter,
    TimeExit = {1}TimeExit,
    IdAlarm = {1}IdAlarm,
    TimeStopFactInObject = {1}TimeStopFactInObject,
    TimeDelayInPath = {1}TimeDelayInPath,
    DistanceInObject = {1}DistanceInObject,
    Id_main = {1}Id_main
    WHERE Id = {0}";
        
        internal static void CloseRecord(ordert orderRecordToClose)
        {
            if (orderRecordToClose != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = string.Format(@"UPDATE md_ordert SET IsClose = true WHERE Id = {0}", orderRecordToClose.Id);
                    DriverDb.ExecuteNonQueryCommand(sSQL);
                }
            }
        }

        internal static void SetFactExitFromZoneParams(ordert orderRecord)
        {

            if (orderRecord != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = string.Format(@"UPDATE md_ordert SET TimeExit = {0}TimeExit,DistanceInObject = {0}DistanceInObject,
                    TimeStopFactInObject = {0}TimeStopFactInObject WHERE Id = {0}Id",DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(4);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeExit", orderRecord.TimeExit );
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DistanceInObject", orderRecord.DistanceInObject);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "timeStopFactInObject", orderRecord.TimeStopFactInObject);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Id", orderRecord.Id);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
            }
        }


        internal static void SetFactEntryToZoneParams(ordert orderRecord)
        {

            if (orderRecord != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = string.Format(@"UPDATE md_ordert SET DistanceToObjectFact  = {0}DistanceToObjectFact, TimeToObjectFact  = {0}TimeToObjectFact,TimeEnter  = {0}TimeEnter,
                    TimeStopFact  = {0}TimeStopFact WHERE Id  = {0}Id", DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(5);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DistanceToObjectFact", orderRecord.DistanceToObjectFact);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeToObjectFact", orderRecord.TimeToObjectFact);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeEnter", orderRecord.TimeEnter);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeStopFact", orderRecord.TimeStopFact);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Id", orderRecord.Id);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
                //OnlineController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderRecord);
            }
        }

        internal static ordert Save(ordert orderRecordToSave)
        {
            if (orderRecordToSave != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = "";
                    if (orderRecordToSave.Id == 0)
                    {
                        //MDController.Instance.MotorDepotEntitiesSaveChanges(orderRecordToSave);
                        sql = string.Format(sqlInsert, DriverDb.ParamPrefics);
                        SetTotalParams(orderRecordToSave, DriverDb);
                        orderRecordToSave.Id = DriverDb.ExecuteReturnLastInsert(sql, DriverDb.GetSqlParameterArray, "md_ordert");
                        UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("Добавление объекта {0}", orderRecordToSave.md_object.Name), orderRecordToSave.md_order.Id);
                        int orderTId = orderRecordToSave.Id;
                        MDController.Instance.ContextMotorDepot.Detach(orderRecordToSave);
                        ordert orderRecordToAttach = MDController.Instance.ContextMotorDepot.ordertSet.Where(t => t.Id == orderTId).FirstOrDefault();
                        MDController.Instance.ContextMotorDepot.Attach(orderRecordToAttach);
                        return orderRecordToAttach;
                    }
                    else
                    {
                        sql = string.Format(sqlUpdate, orderRecordToSave.Id, DriverDb.ParamPrefics);
                        SetTotalParams(orderRecordToSave, DriverDb);
                        DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                    }

                }
            }
            return orderRecordToSave;
        }

        private static void SetTotalParams(ordert orderRecordToSave, DriverDb DriverDb)
        {
            DriverDb.NewSqlParameterArray(22);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Id_main", orderRecordToSave.md_order.Id);
            if (orderRecordToSave.md_object != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObject", orderRecordToSave.md_object.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObject", null);
            if (orderRecordToSave.md_cargoLoad != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCargoLoading", orderRecordToSave.md_cargoLoad.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCargoLoading", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "QtyLoading", orderRecordToSave.QtyLoading);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeLoading", orderRecordToSave.TimeLoading);
            if (orderRecordToSave.md_cargoUnLoad  != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCargoUnLoading", orderRecordToSave.md_cargoUnLoad.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCargoUnLoading", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "QtyUnLoading", orderRecordToSave.QtyUnLoading);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeUnLoading", orderRecordToSave.TimeUnLoading);

            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeDelay", orderRecordToSave.TimeDelay);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeToObject", orderRecordToSave.TimeToObject);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DistanceToObject", orderRecordToSave.DistanceToObject);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Remark", orderRecordToSave.Remark);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IsClose", orderRecordToSave.IsClose);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DistanceToObjectFact", orderRecordToSave.DistanceToObjectFact);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeToObjectFact", orderRecordToSave.TimeToObjectFact);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeStopFact", orderRecordToSave.TimeStopFact);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeEnter", orderRecordToSave.TimeEnter);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeExit", orderRecordToSave.TimeExit);
            if (orderRecordToSave.md_alarming_types != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdAlarm", orderRecordToSave.md_alarming_types.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdAlarm", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeStopFactInObject", orderRecordToSave.TimeStopFactInObject);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeDelayInPath", orderRecordToSave.TimeDelayInPath);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DistanceInObject", orderRecordToSave.DistanceInObject);

        }

        internal static void Delete(ordert orderRecordToDelete)
        {
            if (orderRecordToDelete != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format("DELETE FROM md_ordert WHERE Id = {0} ",orderRecordToDelete.Id);
                    DriverDb.ExecuteNonQueryCommand(sql);
                    MDController.Instance.ContextMotorDepot.Detach(orderRecordToDelete);

                }
            }
        }
    }
}
