﻿using System;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using MotorDepot.MonitoringGps;

namespace MotorDepot
{
    public static class VehicleProvider
    {
        internal static void SaveVehicleState(vehicle vehToSave)
        {
            if (vehToSave != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = @"UPDATE vehicle SET State = ?IdState WHERE Id = ?Id";
                    DriverDb.NewSqlParameterArray(2);
                    DriverDb.SetNewSqlParameter("?IdState", vehToSave.StateId);
                    DriverDb.SetNewSqlParameter("?Id", vehToSave.id);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
            }
        }

        internal static int GetVehicleStateId(vehicle veh)
        {
            if (veh != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = @"SELECT State FROM vehicle  WHERE Id = ?Id";
                    DriverDb.NewSqlParameterArray(1);
                    DriverDb.SetNewSqlParameter("?Id", veh.id);
                    return DriverDb.GetScalarValueIntNull(sSQL, DriverDb.GetSqlParameterArray);
                }
            }
            else
                return 0;
        }
    }
}
