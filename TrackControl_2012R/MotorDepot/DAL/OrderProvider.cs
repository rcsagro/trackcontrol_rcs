﻿using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using MotorDepot.MonitoringGps;
using System;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.Collections.Generic;

namespace MotorDepot
{
    public static class OrderProvider
    {
        internal static void SetOrderState(order orderToSave, OrderStates orderState)
        {
            if (orderToSave != null && orderToSave.Id>0)
            {
                int state = (int)orderState;
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = string.Format(@"UPDATE md_order SET IdState = {0}IdState,DateExecute = {0}DateExecute,IsClose = {0}IsClose WHERE Id = {0}Id", DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(4);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdState", (int)orderState);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Id", orderToSave.Id);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateExecute", orderToSave.DateExecute);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IsClose", orderToSave.IsClose);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
                //OnlineController.Instance.ContextMotorDepotOnline.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderToSave);
                UserLog.InsertLog(UserLogTypes.MD_ORDER, string.Format("Установка статуса в {0}", OrderController.GetOrderStateName(orderState)), orderToSave.Id);
            }
        }

        internal static void UpdateBlockState(order orderToSave)
        {
            if (orderToSave != null && orderToSave.Id > 0)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format(@"UPDATE md_order SET BlockUserId = {1}BlockUserId,BlockDate = {1}BlockDate
                        WHERE (ID = {0})", orderToSave.Id,DriverDb.ParamPrefics );
                    DriverDb.NewSqlParameterArray(2);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "BlockUserId", orderToSave.BlockUserId);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "BlockDate", orderToSave.BlockDate);
                    DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                }
            }
        }

        internal static void UpdateDateStart(order orderToSave)
        {
            if (orderToSave != null && orderToSave.Id > 0)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format(@"UPDATE md_order SET DateStartWork = {1}DateStartWork
                        WHERE (ID = {0})", orderToSave.Id,DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(1);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateStartWork", orderToSave.DateStartWork);
                    DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                }
            }
        }

        internal static void UpdateOrder(order orderToSave)
        {
            if (orderToSave != null && orderToSave.Id > 0)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format(@"UPDATE md_order 
                    SET IdObjectCustomer = {1}IdObjectCustomer
                    , IdObjectStart = {1}IdObjectStart
                    , IdPriority = {1}IdPriority
                    , DateRefuse = {1}DateRefuse
                    , DateEdit = {1}DateEdit
                    , DateDelete = {1}DateDelete
                    , DateExecute = {1}DateExecute
                    , DateStartWork = {1}DateStartWork
                    , IdCategory = {1}IdCategory
                    , IdTransportationYipe = {1}IdTransportationYipe
                    , Remark = {1}Remark
                    , IdWayBill = {1}IdWayBill
                    , TimeDelay = {1}TimeDelay
                    , IsClose = {1}IsClose
                    , IdState = {1}IdState
                    , IdEnterprise = {1}IdEnterprise
                     WHERE (ID = {0})", orderToSave.Id, DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(16);
                    SetTotalParams(orderToSave, DriverDb);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateRefuse", orderToSave.DateRefuse);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateEdit", orderToSave.DateEdit);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateDelete", orderToSave.DateDelete);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateExecute", orderToSave.DateExecute);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateStartWork", orderToSave.DateStartWork);
                   DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IsClose", orderToSave.IsClose);
                       DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                       //MDController.Instance.ContextMotorDepot.Refresh(System.Data.Objects.RefreshMode.StoreWins, orderToSave);
                }
            }
        }

        private static void SetTotalParams(order orderToSave, DriverDb DriverDb)
        {
            if (orderToSave.md_order_category != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCategory", orderToSave.md_order_category.Id);
            else
            {
                orderToSave.md_order_category = MDController.Instance.ContextMotorDepot.order_categorySet.Where(t => t.Name.ToUpper() == "ЗАВОДСКОЙ").FirstOrDefault();
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdCategory", orderToSave.md_order_category.Id);
            }
            if (orderToSave.md_transportation_types != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdTransportationYipe", orderToSave.md_transportation_types.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdTransportationYipe", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Remark", orderToSave.Remark);
            if (orderToSave.md_objectCustomer != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObjectCustomer", orderToSave.md_objectCustomer.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObjectCustomer", null);
            if (orderToSave.md_objectStart != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObjectStart", orderToSave.md_objectStart.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdObjectStart", null);
            if (orderToSave.md_order_priority != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdPriority", orderToSave.md_order_priority.Id);
            else
            {
                orderToSave.md_order_priority = MDController.Instance.ContextMotorDepot.order_prioritySet.Where(t => t.Name.ToUpper() == "СРЕДНИЙ").FirstOrDefault();
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdPriority", orderToSave.md_order_priority.Id);
            }
            if (orderToSave.md_order_state != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdState", orderToSave.md_order_state.Id);
            else
            {
                orderToSave.md_order_state = MDController.Instance.ContextMotorDepot.order_stateSet.Where(st => st.Id == (int)OrderStates.Design).FirstOrDefault();
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdState", orderToSave.md_order_state.Id);
            }
            if (orderToSave.md_enterprise != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdEnterprise", orderToSave.md_enterprise.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdEnterprise", null);
            if (orderToSave.md_waybill != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdWayBill", orderToSave.md_waybill.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdWayBill", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TimeDelay", orderToSave.TimeDelay);
        }

        internal static void InsertOrder(order orderToSave)
        {
            if (orderToSave != null && orderToSave.Id == 0)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format(@"INSERT INTO md_order (
                    Number
                    ,DateCreate
                    ,DateInit
                    ,IdCategory
                    ,IdPriority
                    ,IdState
                    ,UserCreator
                    ,IdEnterprise
                    ,Remark
                    ,IdObjectCustomer
                    ,IdObjectStart
                    , TimeDelay
                    , IdTransportationYipe
                    , IdWayBill) values
                    ({0}Number
                    ,{0}DateCreate
                    ,{0}DateInit
                    ,{0}IdCategory
                    ,{0}IdPriority
                    ,{0}IdState
                    ,{0}UserCreator
                    ,{0}IdEnterprise
                    ,{0}Remark
                    ,{0}IdObjectCustomer
                    ,{0}IdObjectStart
                    ,{0}TimeDelay
                    ,{0}IdTransportationYipe
                    ,{0}IdWayBill
                    )", DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(14);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Number", orderToSave.Number);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateCreate", orderToSave.DateCreate);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateInit", orderToSave.DateInit);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "UserCreator", orderToSave.UserCreator);
                    SetTotalParams(orderToSave, DriverDb);
                    orderToSave.Id = DriverDb.ExecuteReturnLastInsert(sql, DriverDb.GetSqlParameterArray, "md_order");
                    MDController.Instance.ContextMotorDepot.Detach(orderToSave);
                }
            }
        }

        internal static void Delete(order orderToDelete)
        {
            if (orderToDelete != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format("DELETE FROM md_order WHERE Id = {0} ", orderToDelete.Id);
                    DriverDb.ExecuteNonQueryCommand(sql);
                    MDController.Instance.ContextMotorDepot.Detach(orderToDelete);

                }
            }
        }

        internal static string GetVehicleOrderNumber(int vehicleId)
        {
            string orderNumber = "";
            using (DriverDb DriverDb = new DriverDb())
            {
                DriverDb.ConnectDb();
                string sql = string.Format(@"SELECT
                md_order.Number
                FROM md_order
                INNER JOIN md_waybill
                ON md_order.IdWayBill = md_waybill.Id
                WHERE md_waybill.IdVehicle = {0} AND md_waybill.IsClose = 0 AND md_order.IsClose = 0  ", vehicleId);
                DriverDb.GetDataReader(sql);
                if (DriverDb.Read())
                {
                    orderNumber = DriverDb.GetString("Number");
                }
            }
            return orderNumber;
        }

        internal static string GetLastOrderNumberForInterval(int codeLength)
        {
            string orderNumber = "";
            using (DriverDb DriverDb = new DriverDb())
            {
                DriverDb.ConnectDb();
                string sql = string.Format(@"SELECT
                md_order.Number
                FROM md_order
                WHERE YEAR(md_order.DateCreate)= {0} 
                AND MONTH(md_order.DateCreate) = {1} 
                AND LENGTH(md_order.Number) = {2}
                ORDER BY md_order.Id DESC LIMIT 1", DateTime.Today.Year, DateTime.Today.Month, codeLength);
                DriverDb.GetDataReader(sql);
                if (DriverDb.Read())
                {
                    orderNumber = DriverDb.GetString("Number");
                }
            }
            return orderNumber;
        }
    }
}
