﻿using System;
using System.Linq;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.Online;

namespace MotorDepot
{
    public static class  LogProvider
    {
        public static bool SetLogRecord(ordert ordetRecord, string parValue, string parMessage, GpsData gpsData)
        {
            if (!IsLogRecordExist(ordetRecord, parMessage))
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = @"INSERT INTO  md_log (DateEvent,IdObject,IdOrder,IdWayBill,ParamValue,ParamMessage,IdDataGps,IsRead,IdOrderT)
                    values (?DateEvent,?IdObject,?IdOrder,?IdWayBill,?ParamValue,?ParamMessage,?IdDataGps,?IsRead,?IdOrderT)";
                    DriverDb.NewSqlParameterArray(9);
                    DriverDb.SetNewSqlParameter("?DateEvent", gpsData.Time);
                    DriverDb.SetNewSqlParameter("?IdObject", ordetRecord.md_object.Id);
                    DriverDb.SetNewSqlParameter("?IdOrder", ordetRecord.md_order.Id);
                    DriverDb.SetNewSqlParameter("?IdWayBill", ordetRecord.md_order.md_waybill.Id);
                    DriverDb.SetNewSqlParameter("?ParamValue", parValue);
                    DriverDb.SetNewSqlParameter("?ParamMessage", parMessage);
                    DriverDb.SetNewSqlParameter("?IdDataGps", gpsData.Id);
                    DriverDb.SetNewSqlParameter("?IdOrderT", ordetRecord.Id);
                    DriverDb.SetNewSqlParameter("?IsRead", false);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
                 return true;
            }
            return false;
        }

        public static bool SetLogRecord(OnlineVehicle ov,waybill wb, string parMessage)
        {
            if (!IsLogRecordExist(wb, parMessage))
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sSQL = @"INSERT INTO  md_log (DateEvent,IdWayBill,ParamValue,ParamMessage,IsRead)
                    values (?DateEvent,?IdWayBill,?ParamValue,?ParamMessage,?IsRead)";
                    DriverDb.NewSqlParameterArray(5);
                    DriverDb.SetNewSqlParameter("?DateEvent", (DateTime)ov.LastTime);
                    DriverDb.SetNewSqlParameter("?IdWayBill", wb.Id);
                    DriverDb.SetNewSqlParameter("?ParamValue", string.Format("{0:f1} км/час", ov.Speed));
                    DriverDb.SetNewSqlParameter("?ParamMessage", parMessage);
                    DriverDb.SetNewSqlParameter("?IsRead", false);
                    DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
                }
                return true;
            }
            return false;
        }

        public static void SetLogRecordReady(log logRecord)
        {
            using (DriverDb DriverDb = new DriverDb())
            {
                DriverDb.ConnectDb();
                string sSQL = @"UPDATE  md_log Set IsRead = ?IsRead WHERE Id = ?Id";
                DriverDb.NewSqlParameterArray(2);
                DriverDb.SetNewSqlParameter("?Id", logRecord.Id);
                DriverDb.SetNewSqlParameter("?IsRead", true);
                DriverDb.ExecuteNonQueryCommand(sSQL, DriverDb.GetSqlParameterArray);
            }
        }

        private static bool IsLogRecordExist(ordert ordetRecord, string parMessage)
        {
            using (DriverDb DriverDb = new DriverDb())
            {
                DriverDb.ConnectDb();
                string sSQL = @"SELECT md_log.Id
                FROM
                md_log
                WHERE
                md_log.IdOrderT = ?IdOrderT
                AND md_log.ParamMessage = ?ParamMessage";
                DriverDb.NewSqlParameterArray(2);
                DriverDb.SetNewSqlParameter("?IdOrderT", ordetRecord.Id);
                DriverDb.SetNewSqlParameter("?ParamMessage", parMessage);
                if (DriverDb.GetDataReaderRecordsCount(sSQL, DriverDb.GetSqlParameterArray) == 0)
                    return false;
                else
                    return true;
            }
        }

        private static bool IsLogRecordExist(waybill wb, string parMessage)
        {
            using (DriverDb DriverDb = new DriverDb())
            {
                DriverDb.ConnectDb();
                string sSQL = @"SELECT md_log.Id
                FROM
                md_log
                WHERE
                md_log.IdWayBill = ?IdWayBill
                AND md_log.ParamMessage = ?ParamMessage";
                DriverDb.NewSqlParameterArray(2);
                DriverDb.SetNewSqlParameter("?IdWayBill", wb.Id);
                DriverDb.SetNewSqlParameter("?ParamMessage", parMessage);
                if (DriverDb.GetDataReaderRecordsCount(sSQL, DriverDb.GetSqlParameterArray) == 0)
                    return false;
                else
                    return true;
            }
        }

    }
}
