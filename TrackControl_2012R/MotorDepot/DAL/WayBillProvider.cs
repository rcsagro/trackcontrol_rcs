﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using MotorDepot.MonitoringGps;

namespace MotorDepot
{
    public static class WayBillProvider
    {
       
        static readonly string sqlInsert =@"INSERT INTO md_waybill (Date, IdDriver, IdVehicle, IdEnterprise, IdShift, DinnerTime, IsClose, IsBusinessTrip, 
        Remark, DateStart, DateEnd, TotalPath, TotalTimeStop, IdTariff)
        VALUES ({0}Date, {0}IdDriver, {0}IdVehicle, {0}IdEnterprise, {0}IdShift, {0}DinnerTime, {0}IsClose, {0}IsBusinessTrip, {0}Remark, 
        {0}DateStart, {0}DateEnd, {0}TotalPath, {0}TotalTimeStop, {0}IdTariff)";

        static readonly string sqlUpdate = @"UPDATE md_waybill
        SET Date = {1}Date,
        IdDriver = {1}IdDriver,
        IdVehicle = {1}IdVehicle,
        IdEnterprise = {1}IdEnterprise,
        IdShift = {1}IdShift,
        DinnerTime = {1}DinnerTime,
        IsClose = {1}IsClose,
        IsBusinessTrip = {1}IsBusinessTrip,
        Remark = {1}Remark,
        DateStart = {1}DateStart,
        DateEnd = {1}DateEnd,
        TotalPath = {1}TotalPath,
        TotalTimeStop = {1}TotalTimeStop,
        IdTariff = {1}IdTariff
        WHERE Id = {0}";

        
        internal static void UpdateBlockState(waybill wbToSave)
        {
            if (wbToSave != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                     string sql = string.Format(@"UPDATE md_waybill SET BlockUserId = {1}BlockUserId,BlockDate = {1}BlockDate
                        WHERE (ID = {0})", wbToSave.Id, DriverDb.ParamPrefics);
                    DriverDb.NewSqlParameterArray(2);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "BlockUserId", wbToSave.BlockUserId);
                    DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "BlockDate", wbToSave.BlockDate);
                    DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                }
            }
        }

        internal static waybill Save(waybill wbToSave)
        {
            if (wbToSave != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = "";
                    if (wbToSave.Id == 0)
                    {
                        sql = string.Format(sqlInsert, DriverDb.ParamPrefics);
                        SetTotalParams(wbToSave, DriverDb);
                        wbToSave.Id = DriverDb.ExecuteReturnLastInsert(sql, DriverDb.GetSqlParameterArray, "md_waybill");
                        int idWb = wbToSave.Id;
                        MDController.Instance.ContextMotorDepot.Detach(wbToSave);
                        waybill wbToAttach = MDController.Instance.ContextMotorDepot.waybillSet.Where(wb => wb.Id == idWb).FirstOrDefault();
                        MDController.Instance.ContextMotorDepot.Attach(wbToAttach);
                        return wbToAttach;
                    }
                    else
                    {
                        sql = string.Format(sqlUpdate, wbToSave.Id, DriverDb.ParamPrefics);
                        SetTotalParams(wbToSave, DriverDb);
                        DriverDb.ExecuteNonQueryCommand(sql, DriverDb.GetSqlParameterArray);
                    }

                }
                
            }
            return wbToSave;
        }

        private static void SetTotalParams(waybill wbToSave, DriverDb DriverDb)
        {
            DriverDb.NewSqlParameterArray(14);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Date", wbToSave.Date);
            if (wbToSave.driver != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdDriver", wbToSave.driver.id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdDriver", null);

            if (wbToSave.vehicle != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdVehicle", wbToSave.vehicle.id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdVehicle", null);

            if (wbToSave.md_enterprise != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdEnterprise", wbToSave.md_enterprise.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdEnterprise", null);

            if (wbToSave.md_working_shift != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdShift", wbToSave.md_working_shift.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdShift", null);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DinnerTime", wbToSave.DinnerTime);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IsClose", wbToSave.IsClose);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IsBusinessTrip", wbToSave.IsBusinessTrip);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "Remark", wbToSave.Remark);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateStart", wbToSave.DateStart);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "DateEnd", wbToSave.DateEnd);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TotalPath", wbToSave.TotalPath);
            DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "TotalTimeStop", wbToSave.TotalTimeStop);
            if (wbToSave.md_tariff != null)
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdTariff", wbToSave.md_tariff.Id);
            else
                DriverDb.SetNewSqlParameter(DriverDb.ParamPrefics + "IdTariff", null);
        }

        internal static void Delete(waybill wbToDelete)
        {
            if (wbToDelete != null)
            {
                using (DriverDb DriverDb = new DriverDb())
                {
                    DriverDb.ConnectDb();
                    string sql = string.Format("DELETE FROM md_waybill WHERE Id = {0} ", wbToDelete.Id);
                    DriverDb.ExecuteNonQueryCommand(sql);
                    MDController.Instance.ContextMotorDepot.Detach(wbToDelete);

                }
            }
        }

    }
}
