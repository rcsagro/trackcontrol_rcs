﻿namespace MotorDepot
{
    partial class DevFormReportEvent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevFormReportEvent));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.gControlEvent = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceReportEvent = new System.Windows.Forms.BindingSource(this.components);
            this.reportEventSet = new System.Data.DataSet();
            this.gViewEvent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateNumberAuto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeGoObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFromObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongTimeToObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceToObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDurationOnObject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUrgentEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            this.motodepotBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gControlEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceReportEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportEventSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motodepotBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4});
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "barStaticItem2";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "barStaticItem3";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "barStaticItem4";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // gControlEvent
            // 
            this.gControlEvent.DataSource = this.bindingSourceReportEvent;
            this.gControlEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gControlEvent.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gControlEvent.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gControlEvent.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gControlEvent.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gControlEvent.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gControlEvent.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gControlEvent.Location = new System.Drawing.Point(0, 26);
            this.gControlEvent.MainView = this.gViewEvent;
            this.gControlEvent.Name = "gControlEvent";
            this.gControlEvent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gControlEvent.Size = new System.Drawing.Size(809, 370);
            this.gControlEvent.TabIndex = 11;
            this.gControlEvent.UseEmbeddedNavigator = true;
            this.gControlEvent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gViewEvent,
            this.gridView2});
            // 
            // bindingSourceReportEvent
            // 
            this.bindingSourceReportEvent.DataSource = this.reportEventSet;
            this.bindingSourceReportEvent.Position = 0;
            // 
            // reportEventSet
            // 
            this.reportEventSet.DataSetName = "NewDataSet";
            // 
            // gViewEvent
            // 
            this.gViewEvent.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gViewEvent.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gViewEvent.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gViewEvent.Appearance.Empty.Options.UseBackColor = true;
            this.gViewEvent.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gViewEvent.Appearance.EvenRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gViewEvent.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gViewEvent.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gViewEvent.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gViewEvent.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gViewEvent.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gViewEvent.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gViewEvent.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewEvent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gViewEvent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gViewEvent.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gViewEvent.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gViewEvent.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewEvent.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewEvent.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewEvent.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gViewEvent.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gViewEvent.Appearance.GroupButton.Options.UseBackColor = true;
            this.gViewEvent.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gViewEvent.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gViewEvent.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gViewEvent.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gViewEvent.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gViewEvent.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gViewEvent.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gViewEvent.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gViewEvent.Appearance.GroupRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.GroupRow.Options.UseFont = true;
            this.gViewEvent.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gViewEvent.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gViewEvent.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gViewEvent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gViewEvent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewEvent.Appearance.HorzLine.Options.UseBackColor = true;
            this.gViewEvent.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gViewEvent.Appearance.OddRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gViewEvent.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.Preview.Options.UseBackColor = true;
            this.gViewEvent.Appearance.Preview.Options.UseForeColor = true;
            this.gViewEvent.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gViewEvent.Appearance.Row.Options.UseBackColor = true;
            this.gViewEvent.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gViewEvent.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gViewEvent.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gViewEvent.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gViewEvent.Appearance.VertLine.Options.UseBackColor = true;
            this.gViewEvent.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gViewEvent.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gViewEvent.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gViewEvent.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gViewEvent.BestFitMaxRowCount = 2;
            this.gViewEvent.ColumnPanelRowHeight = 40;
            this.gViewEvent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdCount,
            this.colDate,
            this.colTime,
            this.colStateNumberAuto,
            this.colObject,
            this.colTimeGoObject,
            this.colTimeFromObject,
            this.colLongTimeToObject,
            this.colDistanceToObject,
            this.colDurationOnObject,
            this.colUrgentEvent});
            this.gViewEvent.GridControl = this.gControlEvent;
            this.gViewEvent.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colDistanceToObject, "")});
            this.gViewEvent.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gViewEvent.Name = "gViewEvent";
            this.gViewEvent.OptionsDetail.AllowZoomDetail = false;
            this.gViewEvent.OptionsDetail.EnableMasterViewMode = false;
            this.gViewEvent.OptionsDetail.ShowDetailTabs = false;
            this.gViewEvent.OptionsDetail.SmartDetailExpand = false;
            this.gViewEvent.OptionsSelection.MultiSelect = true;
            this.gViewEvent.OptionsView.ColumnAutoWidth = false;
            this.gViewEvent.OptionsView.EnableAppearanceEvenRow = true;
            this.gViewEvent.OptionsView.EnableAppearanceOddRow = true;
            this.gViewEvent.OptionsView.ShowFooter = true;
            // 
            // colIdCount
            // 
            this.colIdCount.AppearanceCell.Options.UseTextOptions = true;
            this.colIdCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIdCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdCount.Caption = "№ п/п";
            this.colIdCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIdCount.FieldName = "IdCount";
            this.colIdCount.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colIdCount.MinWidth = 10;
            this.colIdCount.Name = "colIdCount";
            this.colIdCount.OptionsColumn.AllowEdit = false;
            this.colIdCount.OptionsColumn.AllowFocus = false;
            this.colIdCount.OptionsColumn.FixedWidth = true;
            this.colIdCount.OptionsColumn.ReadOnly = true;
            this.colIdCount.ToolTip = "Номер пункта по порядку";
            this.colIdCount.Visible = true;
            this.colIdCount.VisibleIndex = 0;
            this.colIdCount.Width = 40;
            // 
            // colDate
            // 
            this.colDate.AppearanceCell.Options.UseTextOptions = true;
            this.colDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDate.Caption = "Дата";
            this.colDate.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "Date";
            this.colDate.MinWidth = 10;
            this.colDate.Name = "colDate";
            this.colDate.OptionsColumn.AllowEdit = false;
            this.colDate.OptionsColumn.AllowFocus = false;
            this.colDate.OptionsColumn.FixedWidth = true;
            this.colDate.OptionsColumn.ReadOnly = true;
            this.colDate.ToolTip = "Дата";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 1;
            this.colDate.Width = 40;
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTime.Caption = "Время";
            this.colTime.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTime.FieldName = "Time";
            this.colTime.MinWidth = 10;
            this.colTime.Name = "colTime";
            this.colTime.OptionsColumn.AllowEdit = false;
            this.colTime.OptionsColumn.AllowFocus = false;
            this.colTime.OptionsColumn.FixedWidth = true;
            this.colTime.OptionsColumn.ReadOnly = true;
            this.colTime.ToolTip = "Время";
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 2;
            this.colTime.Width = 40;
            // 
            // colStateNumberAuto
            // 
            this.colStateNumberAuto.AppearanceCell.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateNumberAuto.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStateNumberAuto.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStateNumberAuto.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStateNumberAuto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStateNumberAuto.Caption = "Госномер машины";
            this.colStateNumberAuto.FieldName = "StateNumberAuto";
            this.colStateNumberAuto.MinWidth = 10;
            this.colStateNumberAuto.Name = "colStateNumberAuto";
            this.colStateNumberAuto.OptionsColumn.AllowEdit = false;
            this.colStateNumberAuto.OptionsColumn.AllowFocus = false;
            this.colStateNumberAuto.OptionsColumn.FixedWidth = true;
            this.colStateNumberAuto.OptionsColumn.ReadOnly = true;
            this.colStateNumberAuto.ToolTip = "Государственный номер машины";
            this.colStateNumberAuto.Visible = true;
            this.colStateNumberAuto.VisibleIndex = 3;
            this.colStateNumberAuto.Width = 80;
            // 
            // colObject
            // 
            this.colObject.AppearanceCell.Options.UseTextOptions = true;
            this.colObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colObject.Caption = "Объект";
            this.colObject.FieldName = "Object";
            this.colObject.MinWidth = 10;
            this.colObject.Name = "colObject";
            this.colObject.OptionsColumn.AllowEdit = false;
            this.colObject.OptionsColumn.AllowFocus = false;
            this.colObject.OptionsColumn.FixedWidth = true;
            this.colObject.OptionsColumn.ReadOnly = true;
            this.colObject.ToolTip = "Название объекта";
            this.colObject.Visible = true;
            this.colObject.VisibleIndex = 4;
            // 
            // colTimeGoObject
            // 
            this.colTimeGoObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeGoObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeGoObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeGoObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeGoObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeGoObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeGoObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeGoObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeGoObject.Caption = "Время выезда на объект";
            this.colTimeGoObject.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimeGoObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeGoObject.FieldName = "TimeGoObject";
            this.colTimeGoObject.MinWidth = 10;
            this.colTimeGoObject.Name = "colTimeGoObject";
            this.colTimeGoObject.OptionsColumn.AllowEdit = false;
            this.colTimeGoObject.OptionsColumn.AllowFocus = false;
            this.colTimeGoObject.OptionsColumn.FixedWidth = true;
            this.colTimeGoObject.OptionsColumn.ReadOnly = true;
            this.colTimeGoObject.ToolTip = "Время выезда на объект";
            this.colTimeGoObject.Visible = true;
            this.colTimeGoObject.VisibleIndex = 5;
            this.colTimeGoObject.Width = 60;
            // 
            // colTimeFromObject
            // 
            this.colTimeFromObject.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFromObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFromObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFromObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFromObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFromObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFromObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeFromObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeFromObject.Caption = "Время покидания объекта";
            this.colTimeFromObject.DisplayFormat.FormatString = "hh:mm:ss";
            this.colTimeFromObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeFromObject.FieldName = "TimeFromObject";
            this.colTimeFromObject.MinWidth = 10;
            this.colTimeFromObject.Name = "colTimeFromObject";
            this.colTimeFromObject.OptionsColumn.AllowEdit = false;
            this.colTimeFromObject.OptionsColumn.AllowFocus = false;
            this.colTimeFromObject.OptionsColumn.FixedWidth = true;
            this.colTimeFromObject.OptionsColumn.ReadOnly = true;
            this.colTimeFromObject.ToolTip = "Время покидания объекта";
            this.colTimeFromObject.Visible = true;
            this.colTimeFromObject.VisibleIndex = 6;
            this.colTimeFromObject.Width = 60;
            // 
            // colLongTimeToObject
            // 
            this.colLongTimeToObject.AppearanceCell.Options.UseTextOptions = true;
            this.colLongTimeToObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongTimeToObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongTimeToObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongTimeToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colLongTimeToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongTimeToObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongTimeToObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongTimeToObject.Caption = "Длительность движения к объекту";
            this.colLongTimeToObject.FieldName = "LongTimeToObject";
            this.colLongTimeToObject.MinWidth = 10;
            this.colLongTimeToObject.Name = "colLongTimeToObject";
            this.colLongTimeToObject.OptionsColumn.AllowEdit = false;
            this.colLongTimeToObject.OptionsColumn.AllowFocus = false;
            this.colLongTimeToObject.OptionsColumn.FixedWidth = true;
            this.colLongTimeToObject.OptionsColumn.ReadOnly = true;
            this.colLongTimeToObject.ToolTip = "Длительность движения к объекту";
            this.colLongTimeToObject.Visible = true;
            this.colLongTimeToObject.VisibleIndex = 7;
            this.colLongTimeToObject.Width = 50;
            // 
            // colDistanceToObject
            // 
            this.colDistanceToObject.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colDistanceToObject.AppearanceCell.Options.UseTextOptions = true;
            this.colDistanceToObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistanceToObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistanceToObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistanceToObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistanceToObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistanceToObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistanceToObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistanceToObject.Caption = "Расстояние до объекта, км";
            this.colDistanceToObject.DisplayFormat.FormatString = "{0:f2}";
            this.colDistanceToObject.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistanceToObject.FieldName = "DistanceToObject";
            this.colDistanceToObject.MinWidth = 10;
            this.colDistanceToObject.Name = "colDistanceToObject";
            this.colDistanceToObject.OptionsColumn.AllowEdit = false;
            this.colDistanceToObject.OptionsColumn.AllowFocus = false;
            this.colDistanceToObject.OptionsColumn.FixedWidth = true;
            this.colDistanceToObject.OptionsColumn.ReadOnly = true;
            this.colDistanceToObject.SummaryItem.DisplayFormat = "{0:f2}";
            this.colDistanceToObject.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDistanceToObject.ToolTip = "Расстояние до объекта, км";
            this.colDistanceToObject.Visible = true;
            this.colDistanceToObject.VisibleIndex = 8;
            this.colDistanceToObject.Width = 60;
            // 
            // colDurationOnObject
            // 
            this.colDurationOnObject.AppearanceCell.Options.UseTextOptions = true;
            this.colDurationOnObject.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDurationOnObject.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDurationOnObject.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDurationOnObject.AppearanceHeader.Options.UseTextOptions = true;
            this.colDurationOnObject.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDurationOnObject.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDurationOnObject.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDurationOnObject.Caption = "Длительность пребывания на объекте";
            this.colDurationOnObject.FieldName = "DurationOnObject";
            this.colDurationOnObject.MinWidth = 10;
            this.colDurationOnObject.Name = "colDurationOnObject";
            this.colDurationOnObject.OptionsColumn.AllowEdit = false;
            this.colDurationOnObject.OptionsColumn.AllowFocus = false;
            this.colDurationOnObject.OptionsColumn.FixedWidth = true;
            this.colDurationOnObject.OptionsColumn.ReadOnly = true;
            this.colDurationOnObject.ToolTip = "Длительность пребывания на объекте";
            this.colDurationOnObject.Visible = true;
            this.colDurationOnObject.VisibleIndex = 9;
            this.colDurationOnObject.Width = 60;
            // 
            // colUrgentEvent
            // 
            this.colUrgentEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colUrgentEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUrgentEvent.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUrgentEvent.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUrgentEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colUrgentEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUrgentEvent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUrgentEvent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUrgentEvent.Caption = "Тревожные события";
            this.colUrgentEvent.FieldName = "UrgentEvent";
            this.colUrgentEvent.MinWidth = 10;
            this.colUrgentEvent.Name = "colUrgentEvent";
            this.colUrgentEvent.OptionsColumn.AllowEdit = false;
            this.colUrgentEvent.OptionsColumn.AllowFocus = false;
            this.colUrgentEvent.OptionsColumn.FixedWidth = true;
            this.colUrgentEvent.OptionsColumn.ReadOnly = true;
            this.colUrgentEvent.ToolTip = "Тревожные события";
            this.colUrgentEvent.Visible = true;
            this.colUrgentEvent.VisibleIndex = 10;
            this.colUrgentEvent.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gControlEvent;
            this.gridView2.Name = "gridView2";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink});
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink.ImageCollection.ImageStream")));
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystem = this.printingSystem1;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // motodepotBindingSource
            // 
            this.motodepotBindingSource.DataSource = this.bindingSourceReportEvent;
            this.motodepotBindingSource.Position = 0;
            // 
            // DevFormReportEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gControlEvent);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "DevFormReportEvent";
            this.Size = new System.Drawing.Size(809, 445);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gControlEvent, 0);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gControlEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceReportEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportEventSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gViewEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motodepotBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraGrid.GridControl gControlEvent;
        private DevExpress.XtraGrid.Views.Grid.GridView gViewEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colIdCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraGrid.Columns.GridColumn colStateNumberAuto;
        private DevExpress.XtraGrid.Columns.GridColumn colObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeGoObject;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFromObject;
        private DevExpress.XtraGrid.Columns.GridColumn colLongTimeToObject;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceToObject;
        private DevExpress.XtraGrid.Columns.GridColumn colDurationOnObject;
        private DevExpress.XtraGrid.Columns.GridColumn colUrgentEvent;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private System.Data.DataSet reportEventSet;
        protected System.Windows.Forms.BindingSource bindingSourceReportEvent;
        protected System.Windows.Forms.BindingSource motodepotBindingSource;
    }
}
