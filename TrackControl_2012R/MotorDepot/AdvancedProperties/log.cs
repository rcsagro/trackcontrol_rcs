﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MotorDepot
{
    public partial class log
    {
        public int OrderStateId
        {
            get
            {
                if (this.md_order == null || this.md_order.md_order_state == null) return (int)OrderStates.WaitingFor;
                return this.md_order.md_order_state.Id;
            }

        }
        public string OrderUserName
        {
            get
            {
                if (this.md_order == null || this.md_order.UserCreator == null) return "";
                return this.md_order.UserCreator;
            }

        }
        public string OrderNumber
        {
            get
            {
                if (this.md_order == null || this.md_order.Number == null) return "";
                return this.md_order.Number;
            }

        }

        public string VehicleName
        {
            get
            {
                if (this.md_waybill == null || this.md_waybill.vehicle == null) return "";
                return this.md_waybill.vehicle.NumberPlate;
            }

        }

        public string ObjectName
        {
            get
            {
                if (this.md_object == null || this.md_object.Name == null) return "";
                return this.md_object.Name;
            }

        }
    }
}
