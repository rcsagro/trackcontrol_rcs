﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MotorDepot
{
    public partial class vehicle 
    {
        public string FullName
        {
            get
            {
                return string.Format("{0} | {1} | {2}", this._NumberPlate, this._CarModel, this._MakeCar);
            }
        }

        public int StateId
        {
            get
            {
                return this.vehicle_state == null ? 0 : this.vehicle_state.Id;
            }

        }

    }
}
