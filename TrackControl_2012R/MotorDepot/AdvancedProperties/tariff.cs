﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MotorDepot
{
    public partial  class tariff
    {
        public string FullName
        {
            get
            {
                return string.Format("{0} км от {1:dd.MM.yyyy}", this.LimitKm, this.DateIssue);
            }
        }
    }
}
