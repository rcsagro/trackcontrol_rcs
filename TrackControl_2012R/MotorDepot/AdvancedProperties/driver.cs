﻿namespace MotorDepot
{
    public partial class driver
    {
        public string FullName
        {
            get
            {
                return string.Format("{0}{1}", this._Family, this._Name);
            }
        }
    }
}
