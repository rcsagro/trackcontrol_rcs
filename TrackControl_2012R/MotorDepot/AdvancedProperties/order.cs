﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MotorDepot
{
    public partial class order 
    {
        public int StateId
        {
            get
            {
                if (this==null || this.md_order_state==null)
                {
                    return (int)OrderStates.WaitingFor;
                }
                    else
                {
                    return this.md_order_state.Id; 
                }
                
            }
        }

        #region Блокирование документа для однопользовательской работы
        public bool IsDocBlocked
        {
            get
            {
                int blockUserId = BlockUserId;
                DateTime? blockDate = BlockDate;
                bool block = DocsBlocker.IsDocBlocked(ref blockUserId, ref blockDate);
                if (blockUserId != BlockUserId)
                {
                    BlockUserId = blockUserId;
                    BlockDate = blockDate;
                    OrderProvider.UpdateBlockState(this);
                }
                return block;
            }
        }
        #endregion
    }
}
