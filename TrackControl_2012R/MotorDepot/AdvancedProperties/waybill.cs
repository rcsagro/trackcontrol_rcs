﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using DevExpress.XtraEditors; 

namespace MotorDepot
{
    public partial class waybill
    {
        public string VehicleForOrder
        {
            get
            {
                return string.Format("ПЛ № {0} - {1}", this.Id, this.vehicle.NumberPlate);
            }

        }

        public int VehicleStateId
        {
            get
            {
                return this.vehicle == null ? 0 : this.vehicle.vehicle_state == null ? 0 : this.vehicle.vehicle_state.Id;
            }

        }

        #region Блокирование документа для однопользовательской работы
        public bool IsDocBlocked
        {
            get
            {
                int blockUserId = BlockUserId;
                DateTime? blockDate = BlockDate;
                bool block = DocsBlocker.IsDocBlocked(ref blockUserId, ref blockDate);
                if (blockUserId != BlockUserId)
                {
                    BlockUserId = blockUserId;
                    BlockDate = blockDate;
                    WayBillProvider.UpdateBlockState(this);
                }
                return block;
            }
        }
        #endregion
    }
}
