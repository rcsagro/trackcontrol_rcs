﻿using System;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using DevExpress.XtraEditors;
using MotorDepot.AccessObjects;
using MotorDepot.MonitoringGps;
using MotorDepot.Reports;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.General.DatabaseDriver;
using TrackControl.GMap;

namespace MotorDepot
{
    public partial class MDController : Singleton<MDController>, IDisposable
    {
        private bool _isStarted;

        MainMD _view;
        internal static readonly string MesCaption = "Автопредприятие";
        internal MdVehicles MdVehList;
        internal GoogleMapControl MdGoogleMap;
        internal ControlReports MdReports;
        internal MdUsersList MdUsersList;
        internal MdDictionariesList MdDictList;
        OrderForm _viewOrder;
        WayBillForm _viewWayBill;
        internal static string ModelConString;
        private MapDrawController _MapDrawController;

        MotorDepotEntities _contextMotorDepot;
        public MotorDepotEntities ContextMotorDepot
        {
            get { return _contextMotorDepot; }
        }


        public MDController()
        {

        }
        public void Start()
        {
            if (_isStarted)
            {
                if (_view != null) _view.BringToFront();
                return;
            }
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
            SetConnectToEntities();
            if (!MdUser.Instance.IsUserHasAccessToModule()) return;
            MdVehList = new MdVehicles(this);
            MdGoogleMap = new GoogleMapControl();
            MdReports = new ControlReports();
            MdUsersList = new MdUsersList();
            MdDictList = new MdDictionariesList();
            _view = new MainMD(this);
            _view.Text = string.Format("Автопредприятие {0} пользователь {1}", System.Reflection.Assembly.GetCallingAssembly().GetName().Name.ToString() + " "
            + System.Reflection.Assembly.GetCallingAssembly().GetName().Version, UserBaseCurrent.Instance.Name);
            OrderRecordPointChecker.UpdateLog += UpdateLogInvoke;
            MapDrawController.UpdateLog += UpdateLogInvoke;
            UpdateLog();
            _MapDrawController = new MapDrawController(MdVehList, MdGoogleMap, _view);
            _view.Show();
            _isStarted = true;
        }

        private void SetConnectToEntities()
        {
            CreateConnectionString();
            _contextMotorDepot = new MotorDepotEntities(ModelConString);
            _contextMotorDepot.SavingChanges += new EventHandler(_contextMotorDepot_SavingChanges);
            OrderController.ContextMotorDepot = _contextMotorDepot;
            OrderTController.ContextMotorDepot = _contextMotorDepot;
            WayBillController.ContextMotorDepot = _contextMotorDepot;
            DictController.ContextMotorDepot = _contextMotorDepot;
            UsersController.ContextMotorDepot = _contextMotorDepot;
            InitEntyties(_contextMotorDepot);

        }

        public static void InitEntyties(MotorDepotEntities contextMotorDepot)
        {
            contextMotorDepot.zonesSet.ToList();
            contextMotorDepot.vehicle_stateSet.ToList();
            contextMotorDepot.teamSet.ToList();
            contextMotorDepot.vehicleSet.ToList();
            contextMotorDepot.order_stateSet.ToList();
            contextMotorDepot.object_groupSet.ToList();
            contextMotorDepot.md_objectSet.ToList();
            contextMotorDepot.acs_rolesSet.ToList();
            contextMotorDepot.enterpriseSet.ToList();
            contextMotorDepot.vehicle_categorySet.ToList();
            contextMotorDepot.cargoSet.ToList();
        }


        private void CreateConnectionString()
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                GetMySQLConnectionString();
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                GetMsSQLConnectionString();
            }
        }

        private void GetMySQLConnectionString()
        {
            var newConnection = new EntityConnectionStringBuilder()
            {
                Metadata =
                    @"res://*/MotorDepotModel.csdl|res://*/MotorDepotModel.ssdl|res://*/MotorDepotModel.msl",
                Provider = @"MySql.Data.MySqlClient",
                ProviderConnectionString = DriverDb.ConnectionString
            };
            ModelConString = newConnection.ConnectionString;
        }

        private void GetMsSQLConnectionString()
        {
            var newConnection = new EntityConnectionStringBuilder()
            {
                Metadata =
                    @"res://*/MotorDepotModel.csdl|MotorDepotModelMsSQL2008.ssdl|res://*/MotorDepotModel.msl",
                Provider = @"System.Data.SqlClient",
                ProviderConnectionString = DriverDb.ConnectionString
            };
            ModelConString = newConnection.ConnectionString;
        }

        // aketner
        #region Путевые листы
        internal void WayBillSetViewDataSource()
        {
            WayBillController.SetViewDataSource(_view);
        }

        internal void WayBillAddNew()
        {
            _viewWayBill = new WayBillForm();
            SubscribeToWayBillFormEvents(_viewWayBill);
            WayBillController.AddNewWayBill(_viewWayBill);
        }
        void SubscribeToWayBillFormEvents(WayBillForm viewWayBill)
        {
            viewWayBill.SaveWayBill += WayBillSaveItem;
            viewWayBill.WayBillViewClosed += WayBillViewClosed;
        }

        internal void WayBillSaveItem(waybill wbToSave)
        {
            WayBillController.SaveItem(wbToSave);
        }

        void WayBillViewClosed()
        {
            if (_viewWayBill != null)
            {
                _viewWayBill.SaveWayBill -= WayBillSaveItem;
                _viewWayBill.WayBillViewClosed -= WayBillViewClosed;
                _viewWayBill = null;
            }
        }

        internal void WayBillView(waybill wbForView)
        {
            _viewWayBill = new WayBillForm();
            SubscribeToWayBillFormEvents(_viewWayBill);
            WayBillController.ViewWayBill(wbForView, _viewWayBill);
        }
        #endregion

        #region Заказ - наряды
        internal void OrderSetViewDataSource(bool showWorked)
        {
            OrderController.SetViewDataSource(_view, showWorked);
        }

        void OrderSaveItem(order orderToSave)
        {
            OrderController.SaveOrder(orderToSave);
        }

        internal void OrderAddNewOrder()
        {
            _viewOrder = new OrderForm();
            SubscribeToOrderFormEvents(_viewOrder);
            OrderController.AddNewOrder(_viewOrder);
        }

        internal void OrderViewOrder(order orderForView)
        {
            _viewOrder = new OrderForm();
            SubscribeToOrderFormEvents(_viewOrder);
            if (!OrderController.ViewOrder(orderForView, _viewOrder)) _viewOrder.Dispose();
        }

        internal void OrderViewOrder(int orderId)
        {
            order orderForView = _contextMotorDepot.orderSet.Where(od => od.Id == orderId).FirstOrDefault();
            if (orderForView != null) OrderViewOrder(orderForView);
        }

        void SubscribeToOrderFormEvents(OrderForm viewOrder)
        {
            viewOrder.SaveOrder += OrderSaveItem;
            viewOrder.OrderViewClosed += OrderViewClosed;
        }

        void OrderViewClosed()
        {
            if (_viewOrder != null)
            {
                _viewOrder.SaveOrder -= OrderSaveItem;
                _viewOrder.OrderViewClosed -= OrderViewClosed;
                _viewOrder = null;
            }
        }

        #endregion

        #region Карта + Журнал событий

        public void UpdateLogInvoke()
        {
            VoidHandler action = UpdateLog;
            if (_view.InvokeRequired)
            {
                _view.Invoke(action);
            }
            else
            {
                action();
            }
        }

        void UpdateLog()
        {
            LogController.SetViewDataSource(_view);
            MdVehList.RefreshTree();
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {

            if (_contextMotorDepot != null)
                _contextMotorDepot.Dispose();
            OnlineController.Instance.Stop();
            if (MdVehList != null) MdVehList.Dispose();
            _isStarted = false;
            if (_view != null) _view = null;
            OrderRecordPointChecker.UpdateLog -= UpdateLogInvoke;
        }
        #endregion

        public bool MotorDepotEntitiesSaveChanges(object objToSave)
        {
            try
            {
                // Try to save changes, which may cause a conflict.
                _contextMotorDepot.SaveChanges();
                return true;
            }
            catch (OptimisticConcurrencyException)
            {
                // Resolve the concurrency conflict by refreshing the 
                // object context before re-saving changes. 
                _contextMotorDepot.Refresh(RefreshMode.ClientWins, objToSave);
                // Save changes.
                _contextMotorDepot.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(string.Format("{0}{1}{2}", ex.Message, Environment.NewLine, ex.InnerException));
                return false;
            }

        }
    }
}
