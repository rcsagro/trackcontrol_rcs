﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using TrackControl.General;

namespace MotorDepot
{
    partial class MDController
    {
        string _addRelationValue = string.Empty;
        ObjectStateEntry _prevEntity;

        private void _contextMotorDepot_SavingChanges(object sender, EventArgs e)
        {
            IEnumerable<ObjectStateEntry> changes =
            _contextMotorDepot.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Added | EntityState.Deleted);
            foreach (ObjectStateEntry stateEntryEntity in changes)
            {
                if (IsRelationOnlyAdd(stateEntryEntity)) WriteToLogEntityRelAdded(_prevEntity);
                _prevEntity = stateEntryEntity;
                if (stateEntryEntity.IsRelationship)
                {
                    switch (stateEntryEntity.State)
                    {
                        case EntityState.Deleted:
                            {
                                WriteToLogEntityRelDeleted(stateEntryEntity);
                                break;
                            }
                        case EntityState.Added:
                            {
                                _addRelationValue = "";
                                GetEntityRelAdded(stateEntryEntity);
                                break;
                            }

                        default:
                            return;
                     }
                  }
                else if (stateEntryEntity.Entity != null)
                {
                    switch (stateEntryEntity.State)
                    {
                        case EntityState.Deleted:
                            {
                                WriteToLogEntityDeleted(stateEntryEntity);
                                break;
                            }
                        case EntityState.Modified:
                            {
                                WriteToLogEntityModified(stateEntryEntity);
                                break;
                            }

                        default:
                            return;
                    }
                }
            }
            if (IsRelationOnlyAdd(_prevEntity)) WriteToLogEntityRelAdded(_prevEntity);
        }

        private bool IsRelationOnlyAdd(ObjectStateEntry stateEntryEntity)
        {
            if (_prevEntity == null || _prevEntity.State == EntityState.Detached) return false;
            return _prevEntity.IsRelationship && _prevEntity.State == EntityState.Added && stateEntryEntity.State != EntityState.Deleted && _addRelationValue.Length > 0;
        }

        private string GetNavigationPropertyName(ObjectStateEntry entry, EntityKey key)
        {
            var relationshipType = entry.EntitySet.ElementType;
            var entitySet = key.GetEntitySet(
                    entry.ObjectStateManager.MetadataWorkspace);
            var property = entitySet.ElementType.NavigationProperties.Where(
                    p => p.RelationshipType == relationshipType)
                    .SingleOrDefault();

            if (property == null)
            {
                return null;
            }

            return property.Name;
        }

        private void WriteToLogEntityModified(ObjectStateEntry entry)
        {
            if (entry.State != EntityState.Modified) return;
            string dbTableName = GetEntityDbTableName(entry);
            switch (dbTableName)
            {
                case "order":
                    WriteToLogEntityProperties(entry, UserLogTypes.MD_ORDER, (int)entry.OriginalValues["Id"],false);
                    break;
                case "ordert":
                        WriteToLogEntityProperties(entry, UserLogTypes.MD_ORDER, ((ordert)entry.Entity).md_order.Id, true);
                    break;
                default:
                    int codeDictionary = GetDictionaryCode(dbTableName);
                    if (codeDictionary >= 0)
                    {
                        WriteToLogEntityProperties(entry, UserLogTypes.MD_DICTIONARY, codeDictionary,false);
                    }
                    break;
            }
        }

        private void WriteToLogEntityDeleted(ObjectStateEntry entry)
        {
            if (entry.State != EntityState.Deleted && entry.EntityKey == null) return;
            string dbTableName = GetEntityDbTableName(entry);
            int idEntity = (int)entry.EntityKey.EntityKeyValues[0].Value;
             string logMessage = string.Format("Id {0}: ", idEntity);
            switch (dbTableName)
            {
                case "order":
                    logMessage = string.Format("{0} Удаление", logMessage);
                    UserLog.InsertLog(UserLogTypes.MD_ORDER, logMessage, idEntity);
                    break;
                case "ordert":
                    logMessage = string.Format("{0} Удаление содержания", logMessage);
                    var orderT = _contextMotorDepot.ordertSet.Where(ot => ot.Id == idEntity).FirstOrDefault();
                    if (orderT != null && orderT.md_order != null)
                    UserLog.InsertLog(UserLogTypes.MD_ORDER, logMessage, orderT.md_order.Id);
                    break;
                default:
                    int codeDictionary = GetDictionaryCode(dbTableName);
                    if (codeDictionary >= 0)
                    {
                        logMessage = string.Format("{0} Удаление ", logMessage);
                        UserLog.InsertLog(UserLogTypes.MD_DICTIONARY, logMessage, codeDictionary);
                    }
                    break;
            }
        }

        private static string GetEntityDbTableName(ObjectStateEntry entry)
        {
            string entityName = entry.EntitySet.ElementType.ToString();
            string[] entityNames = entityName.Split(new Char[] { '.' });
            return entityNames[1];
        }

        private void WriteToLogEntityProperties(ObjectStateEntry entry, UserLogTypes typeLog, int entryId, bool contentDoc)
        {
            if (entry == null) return;
            if (entry.Entity is EntityObject)
            {
                foreach (string propName in entry.GetModifiedProperties())
                {
                    object oldValue = entry.OriginalValues[propName];
                    if (oldValue == DBNull.Value) oldValue = "";
                    object newValue = entry.CurrentValues[propName];
                    if (newValue == DBNull.Value) newValue = "";
                    if (newValue.ToString()  == oldValue.ToString() ) return;
                    string logMessage = string.Format("{0} {1} -> {2}", GetPropertyRuName(propName), oldValue, newValue);
                    int idEntity = (int)entry.OriginalValues["Id"];
                    if (contentDoc)
                    {
                        logMessage = string.Format("Id {0}: Изменение содержания  {1}", idEntity, logMessage);
                    }
                    else
                    {

                        logMessage = string.Format("Id {0}: Изменение {1}", idEntity, logMessage);
                    }
                    UserLog.InsertLog(typeLog, logMessage, entryId);
                }
            }
        }

        private string GetPropertyRuName(string propName)
        {
            switch (propName)
            {
                case "Name":
                    {
                        return "Название";
                    }
                case "Remark":
                    {
                        return "Примечание";
                    }
                case "Unit":
                    {
                        return "ед.изм";
                    }
                case "Dispartchable":
                    {
                        return "Диспетчиризация";
                    }
                case "TimeLoading":
                    {
                        return "Время погрузки";
                    }
                case "TimeUnLoading":
                    {
                        return "Время разгрузки";
                    }
                case "Number":
                    {
                        return "Номер";
                    }
                case "NumberParent":
                    {
                        return "Номер объекта-родителя";
                    }
                case "DelayTime":
                    {
                        return "Время задержки";
                    }
                case "Distance":
                    {
                        return "Расстояние";
                    }
                case "Time":
                    {
                        return "Время";
                    }
                case "Start":
                    {
                        return "Время начала";
                    }
                case "End":
                    {
                        return "Время окончания";
                    }
                case "md_object_to":
                    {
                        return "Объект куда";
                    }
                case "md_object_from":
                    {
                        return "Объект откуда";
                    }
                case "md_object":
                    {
                        return "Объект";
                    }
                case "zones":
                    {
                        return "Контрольная зона";
                    }
                case "md_object_group":
                    {
                        return "Группа объектов";
                    }
                case "md_cargo":
                    {
                        return "Груз";
                    }
                case "DateEdit":
                    {
                        return "Дата редактирования";
                    }
                case "IsClose":
                    {
                        return "Закрыт";
                    }
                case "DateCreate":
                    {
                        return "Дата создания";
                    }
                case "DateRefuse":
                    {
                        return "Дата отказа";
                    }
                case "DateDelete":
                    {
                        return "Дата удаления";
                    }
                case "DateExecute":
                    {
                        return "Дата выполнения";
                    }
                case "DateStartWork":
                    {
                        return "Дата начала работы";
                    }
                case "TimeDelay":
                    {
                        return "Время задержки";
                    }
                case "UserCreator":
                    {
                        return "Оформил";
                    }
                case "md_objectCustomer":
                    {
                        return "Заказчик";
                    }
                case "md_objectStart":
                    {
                        return "Начальная точка";
                    }
                case "md_transportation_types":
                    {
                        return "Вид перевозок";
                    }
                case "md_order_priority":
                    {
                        return "Приоритет";
                    }
                case "QtyLoading":
                    {
                        return "Количество погрузки";
                    }
                case "QtyUnLoading":
                    {
                        return "Количество разгрузки";
                    }
                case "TimeDelayInPath":
                    {
                        return "Время задержки в пути";
                    }
                case "TimeToObject":
                    {
                        return "Время движения к объекту";
                    }
                case "DistanceToObject":
                    {
                        return "Расстояние к объекту";
                    }
                case "TimeToObjectFact":
                    {
                        return "Время движения к объекту(факт)";
                    }
                case "DistanceToObjectFact":
                    {
                        return "Расстояние к объекту(факт)";
                    }
                case "TimeStopFact":
                    {
                        return "Время остановок в пути";
                    }
                case "TimeStopFactInObject":
                    {
                        return "Время остановок на объекте";
                    }
                case "TimeEnter":
                    {
                        return "Время въезда";
                    }
                case "TimeExit":
                    {
                        return "Время выезда";
                    }
                case "DistanceInObject":
                    {
                        return "Путь по объекту";
                    }
                case "md_cargoLoad":
                    {
                        return "Груз погрузки";
                    }
                case "md_cargoUnLoad":
                    {
                        return "Груз разгрузки";
                    }
                case "md_alarming_types":
                    {
                        return "Тревожная ситуация";
                    }
                case "DateInit":
                    {
                        return "Дата получения";
                    }
                case "md_waybill":
                    {
                        return "Транспорт";
                    }
                case "DinnerStart":
                    {
                        return "Начало обеда";
                    }
                case "DinnerEnd":
                    {
                        return "Окончание обеда";
                    }
                case "Tariffication":
                    {
                        return "Тарификация";
                    }
                case "TMove":
                    {
                        return "Тариф пробег";
                    }
                case "TMoveBt":
                    {
                        return "Тариф пробег ком.";
                    }
                case "TTime":
                    {
                        return "Тариф время";
                    }
                case "TTimeBt":
                    {
                        return "Тариф время ком.";
                    }
                case "TTimeStop":
                    {
                        return "Тариф простой";
                    }
                case "TTimeStopBt":
                    {
                        return "Тариф простой ком.";
                    }
                case "DateIssue":
                    {
                        return "Дата внедрения";
                    }
                case "vehicle_category":
                    {
                        return "Категория транспорта";
                    }
                case "LimitKm":
                    {
                        return "Лимит";
                    }
                case "md_order_state":
                    {
                        return "Статус заказа";
                    }                   
                default :
                    return propName; 
            }
        }

        private void WriteToLogEntityRelDeleted(ObjectStateEntry entry)
        {
            if (entry.State != EntityState.Deleted) return;
            var key = entry.OriginalValues[0] as EntityKey;
            var key1 = entry.OriginalValues[1] as EntityKey;
            string delRelationValue = GetNavigationPropertyValue(key);
            if (delRelationValue == _addRelationValue) return;
            WriteToLogRel(entry, key1, delRelationValue);
 
        }

        private void WriteToLogEntityRelAdded(ObjectStateEntry entry)
        {
            if (entry.State != EntityState.Added) return;
            var key1 = entry.CurrentValues[1] as EntityKey;
            WriteToLogRel(entry, key1, "");
            _addRelationValue = ""; 
        }

        private void WriteToLogRel(ObjectStateEntry entry, EntityKey key1, string delRelationValue)
        {
            if (key1.EntityKeyValues == null) return;
            string propertName = GetNavigationPropertyName(entry, key1);
            var dbTableName = entry.EntitySet.ElementType.KeyMembers[1].Name;
            string logMessage = string.Format("{0} {1} -> {2}", GetPropertyRuName(propertName), delRelationValue, _addRelationValue);
            _addRelationValue = "";
            if (dbTableName == "order" || dbTableName == "md_order")
            {
                logMessage = string.Format("Изменение {0}", logMessage);
                UserLog.InsertLog(UserLogTypes.MD_ORDER, logMessage, (int)key1.EntityKeyValues[0].Value);
            }
            else if (dbTableName == "md_ordert" || dbTableName == "md_ordert")
            {
                logMessage = string.Format("Id {0}: Изменение содержания {1}", (int)key1.EntityKeyValues[0].Value, logMessage);
                int idOrderT = (int)key1.EntityKeyValues[0].Value;
                var orderT = _contextMotorDepot.ordertSet.Where(ot => ot.Id == idOrderT).FirstOrDefault();
                if (orderT != null && orderT.md_order != null)
                {
                    UserLog.InsertLog(UserLogTypes.MD_ORDER, logMessage, (int)orderT.md_order.Id);
                }
            }
            else
            {
                int codeDictionary = GetDictionaryCode(dbTableName);
                if (codeDictionary >= 0)
                {
                    logMessage = string.Format("Id {0}: Изменение {1}", (int)key1.EntityKeyValues[0].Value, logMessage);
                    UserLog.InsertLog(UserLogTypes.MD_DICTIONARY, logMessage, codeDictionary);
                }
            }

        }

        int GetDictionaryCode(string  dbTableName)
        {
            if (dbTableName.Length == 0) return -1;
            if (dbTableName.Substring(0, 3) == "md_" & dbTableName != "md_object")
                dbTableName = dbTableName.Substring(3, dbTableName.Length - 3); 
            var dictName = MDController.Instance.ContextMotorDepot.acs_objectsSet.Where(dt => dt.IdType == (int)UserAccessObjectsTypes.DictionariesMd && dt.TypeEF == dbTableName).FirstOrDefault();
            if (dictName == null)
            {
                return -1;
            }
            else
                return (int)DictController.GetDictionaryCode(dbTableName);
        }

        private void GetEntityRelAdded(ObjectStateEntry entry)
        {
            if (entry.State != EntityState.Added) return;
            var key = entry.CurrentValues[0] as EntityKey;
            _addRelationValue = GetNavigationPropertyValue(key); 
        }

        string GetNavigationPropertyValue(EntityKey key)
        {
            int idEntity = (int)key.EntityKeyValues[0].Value;
            string retValue = string.Empty;
            switch (key.EntitySetName)
            {
                case "md_objectSet":
                    {
                        var obj = _contextMotorDepot.md_objectSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name; 
                        break;
                    }
                case "object_groupSet":
                    {
                        var obj = _contextMotorDepot.object_groupSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name; 
                        break;
                    }
                case "zonesSet":
                    {
                        var obj = _contextMotorDepot.zonesSet.Where(ob => ob.Zone_ID == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name; 
                        break;
                    }
                case "cargoSet":
                    {
                        var obj = _contextMotorDepot.cargoSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name; 
                        break;
                    }
                case "order_prioritySet":
                    {
                        var obj = _contextMotorDepot.order_prioritySet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name;
                        break;
                    }
                case "order_stateSet":
                    {
                        var obj = _contextMotorDepot.order_stateSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name;
                        break;
                    }
                case "waybillSet":
                    {
                        var obj = _contextMotorDepot.waybillSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.VehicleForOrder;
                        break;
                    }
                case "transportation_typesSet":
                    {
                        var obj = _contextMotorDepot.transportation_typesSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name;
                        break;
                    }
                case "alarming_typesSet":
                    {
                        var obj = _contextMotorDepot.alarming_typesSet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name;
                        break;
                    }
                case "vehicle_categorySet":
                    {
                        var obj = _contextMotorDepot.vehicle_categorySet.Where(ob => ob.Id == idEntity).FirstOrDefault();
                        if (obj != null) retValue = obj.Name;
                        break;
                    }
            }
            return retValue;
            
        }
    }
}
