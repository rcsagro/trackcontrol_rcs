﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Configuration;
using TrackControl.Statistics;
using TrackControl.Vehicles;
using TrackControl.General; 

namespace TrackControlWs
{
    public class GpsDataProvider
    {
        readonly string _connectionString;
        readonly string _selectDataGps = string.Format(@"SELECT `datagps`.`DataGps_ID` AS `DataGps_ID`
     , `datagps`.`Mobitel_ID` AS `Mobitel_ID`
     , (`datagps`.`Latitude` / 600000.00000) AS `Lat`
     , (`datagps`.`Longitude` / 600000.00000) AS `Lon`
     , `datagps`.`Altitude` AS `Altitude`
     , from_unixtime(`datagps`.`UnixTime`) AS `time`
     , (`datagps`.`Speed` * 1.852) AS `speed`
     , ((`datagps`.`Direction` * 360) / 255) AS `direction`,UnixTime,LogID");

        public GpsDataProvider()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["CS"].ConnectionString;
            ConnectMySQL.ConnectionString = _connectionString;
            //TrackControl.Vehicles.GpsDataWs;  
        }

        public List<GpsDataWs> GetValidDataForPeriod(int idMobitel, DateTime start, DateTime end)
        {
            List<GpsData> GpsDatas = new List<GpsData>();
            

            using (ConnectMySQL cnn = new ConnectMySQL())
            {
                string sSQL = string.Format(@"{0} FROM datagps
               WHERE datagps.UnixTime BETWEEN UNIX_TIMESTAMP(?TimeStart) AND UNIX_TIMESTAMP(?TimeEnd) AND datagps.Valid = 1  AND datagps.Mobitel_ID = {1}",
            _selectDataGps, idMobitel);
                MySqlParameter[] parSql = new MySqlParameter[2];
                parSql[0] = new MySqlParameter("?TimeStart", start);
                parSql[1] = new MySqlParameter("?TimeEnd", end);
                MySqlDataReader dr = cnn.GetDataReader(sSQL, parSql);
                while (dr.Read())
                {
                    GpsData data = new GpsData();
                    GetDataGpsRecord(dr, data);
                    GpsDatas.Add(data);
                }
            }
            TrackRepairService.CheckTrack(GpsDatas);
            List<GpsDataWs> GpsDataWss = ConvertDataGpsToDataGpsWs(GpsDatas);
            return GpsDataWss;
        }

        private List<GpsDataWs> ConvertDataGpsToDataGpsWs(List<GpsData> GpsDatas)
        {
            List<GpsDataWs> GpsDataWss = new List<GpsDataWs>(); 
            foreach (GpsData data in GpsDatas)
            {
                GpsDataWs dataWs = new GpsDataWs();
                dataWs.RecordID = data.Id;
                dataWs.Altitude = data.Altitude;
                dataWs.Latitude = data.LatLng.Lat ;
                dataWs.Longitude = data.LatLng.Lng;
                dataWs.Speed = data.Speed ;
                dataWs.RecordTime = data.Time;
                dataWs.RecordUnixTime = data.UnixTime ;
                dataWs.Direction = data.Direction ;
                dataWs.VehicleID = data.Mobitel;
                GpsDataWss.Add(dataWs); 
            }
            GpsDatas.Clear();
            return GpsDataWss;
        }

        private void GetDataGpsRecord(MySqlDataReader reader, GpsData dataGps)
        {

            dataGps.Id= reader.GetInt64("DataGps_ID");
            dataGps.Altitude = reader.GetInt32("Altitude");
            dataGps.Speed = reader.GetFloat("speed");
            dataGps.UnixTime = reader.GetInt32("UnixTime");
            dataGps.Direction  = reader.GetDouble("direction");
            dataGps.Mobitel = reader.GetInt32("Mobitel_ID");
            dataGps.LogID = reader.GetInt32("LogID");
            dataGps.LatLng = new PointLatLng(reader.GetDouble("Lat"), reader.GetDouble("Lon"));
            dataGps.Time = reader.GetDateTime("time");
        }

        private void GetDataGpsWsRecord(MySqlDataReader reader, GpsDataWs dataGps)
        {

            dataGps.RecordID= reader.GetInt64("DataGps_ID");
            dataGps.Altitude = reader.GetInt32("Altitude");
            dataGps.Latitude = reader.GetDouble("Lat");
            dataGps.Longitude = reader.GetDouble("Lon");
            dataGps.Speed = reader.GetFloat("speed");
            dataGps.RecordTime = reader.GetDateTime("time");
            dataGps.RecordUnixTime = reader.GetInt32("UnixTime");
            dataGps.Direction = reader.GetDouble("direction");
            dataGps.VehicleID = reader.GetInt32("Mobitel_ID");
        }

        private GpsDataWs GetLastActiveDataGps(int mobitelID)
        {
            GpsDataWs dataGps = new GpsDataWs();
            using (ConnectMySQL cn = new ConnectMySQL())
            {
                string sSQL = string.Format(@"{0} FROM    mobitels
                INNER JOIN  datagps    ON datagps.Mobitel_ID = mobitels.Mobitel_ID AND mobitels.ConfirmedID = datagps.LogID
                WHERE   mobitels.Mobitel_ID = {1}  AND datagps.Valid = 1 AND NOT (Latitude = 0 AND Longitude =0)", _selectDataGps, mobitelID);
                MySqlDataReader dr = cn.GetDataReader(sSQL);
                if (dr.Read())
                {
                    GetDataGpsWsRecord(dr, dataGps);
                }
                else
                {
                    dr.Close();
                    sSQL = string.Format(@"{0} FROM datagps
                INNER JOIN  mobitels
                ON datagps.Mobitel_ID = mobitels.Mobitel_ID 
                WHERE
                  mobitels.Mobitel_ID = {1}
                  AND datagps.Valid = 1
                  AND datagps.UnixTime <= (SELECT LastInsertTime FROM mobitels
                    WHERE   mobitels.Mobitel_ID = {1})
                  AND from_unixtime(datagps.UnixTime)<= ?TodayNow ORDER BY datagps.UnixTime DESC Limit 1", _selectDataGps,
                                         mobitelID);
                    MySqlParameter[] parSql = new MySqlParameter[1];
                    parSql[0] = new MySqlParameter("?TodayNow", DateTime.Now);
                    dr = cn.GetDataReader(sSQL, parSql);

                    if (dr.Read())

                        GetDataGpsWsRecord(dr, dataGps);
                    else
                        dataGps = null;
                }
                dr.Close();
            }
            return dataGps;
        }

        public List<GpsDataWs> GetLastRecordForAllVehicles()
        {
            List<GpsDataWs> GpsDatas = new List<GpsDataWs>();
            using (ConnectMySQL cn = new ConnectMySQL())
            {
                string sSQL = "SELECT vehicle.Mobitel_id FROM vehicle";
                MySqlDataReader dr = cn.GetDataReader(sSQL);
                while (dr.Read())
                {
                    GpsDataWs data = GetLastActiveDataGps(dr.GetInt32("Mobitel_id"));
                    if (data != null)
                    {
                        GpsDatas.Add(data);
                    }
                }
            }
            return GpsDatas;
        }
    }
}