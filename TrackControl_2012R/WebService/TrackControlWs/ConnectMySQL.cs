using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using MySql.Data.MySqlClient;
using System.Threading;

namespace TrackControlWs
{
    public class ConnectMySQL:IDisposable 
    {
        private MySqlConnection cnn ;
        static string _connectionString = "";

        public static string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public ConnectMySQL()
        {
            if (_connectionString.Length == 0) 
                _connectionString = ConfigurationManager.ConnectionStrings["CS"].ConnectionString;
            cnn = new MySqlConnection(_connectionString);
            try
            {
                cnn.Open();
            }
            catch (MySqlException)
            {
            }
        }
        public ConnectMySQL(string conString)
        {
            _connectionString = conString;
            cnn = new MySqlConnection(_connectionString);
            try
            {
                cnn.Open();
            }
            catch (MySqlException)
            {
            }
        }
        ~ConnectMySQL()
        {
            if (cnn != null) 
                cnn.Dispose();
        }
        public MySqlConnection Connect()
        {
            return cnn;
        }
        public MySqlDataReader GetDataReader(string sSQL)
        {
          MySqlCommand cmd = new MySqlCommand();
          cmd.Connection = cnn;
          cmd.CommandType = CommandType.Text;
          cmd.CommandText = sSQL;
          cmd.CommandTimeout = 0; 
          return cmd.ExecuteReader();
        }
        public MySqlDataReader GetDataReader(string sSQL,MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            return cmd.ExecuteReader();
        }
        public MySqlDataReader GetProcDataReader(string sProcName, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = sProcName;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandTimeout = 0;
            return cmd.ExecuteReader();
        }
        public Int32   GetDataReaderRecordsCount(string sSQL)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText ="select count(*) as CNT from (" + sSQL + ") as CC";
            cmd.CommandTimeout = 0; 
            return Convert.ToInt32(cmd.ExecuteScalar());
        }
        public Int32 GetDataReaderRecordsCount(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = "select count(*) as CNT from (" + sSQL + ") as CC";
            cmd.CommandTimeout = 0;
            return Convert.ToInt32(cmd.ExecuteScalar());
        }
        public int ExecuteNonQueryCommand(string sSQL)
        {
            //Console.WriteLine(sSQL);   
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();  
            return 0;
        }
        public int ExecuteReturnLastInsert(string sSQL)
        {
            //Console.WriteLine(sSQL);   
            
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            return  (int)cmd.LastInsertedId;
            //int li1 = Convert.ToInt32(GetScalarValue("SELECT Id FROM " + sTableName + " WHERE ID = LAST_INSERT_ID()"));
            //return  Convert.ToInt32(GetScalarValue("SELECT Id FROM " + sTableName + " WHERE ID = LAST_INSERT_ID()"));
        }
        public int ExecuteReturnLastInsert(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            return (int)cmd.LastInsertedId;
        }
        public int ExecuteNonQueryCommand(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            return 0;
        }
        public object  GetScalarValue(string sSQL)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            return cmd.ExecuteScalar();
        }
        public double GetScalarValueDblNull(string sSQL)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            double ret;
            if ((obj != null) && Double.TryParse(obj.ToString(), out ret))
                return ret;
            else
            {
                return 0;
            }
        }
        public double GetScalarValueDblNull(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            double ret;
            if ((obj != null) && Double.TryParse(obj.ToString(), out ret))
                return ret;
            else
            {
                return 0;
            }
        }
        public TimeSpan GetScalarValueTimeSpanNull(string sSQL)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            TimeSpan ts;
            if ((obj != null) && TimeSpan.TryParse(obj.ToString(), out ts))
                return ts;
            else
            {
                return TimeSpan.Zero;
            }
        }
        public DateTime GetScalarValueDateTimeNull(string sSQL, DateTime DateReplace)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            DateTime dt;
            if ((obj != null) && DateTime.TryParse(obj.ToString(), out dt))
                return dt;
            else
            {
                return DateReplace;
            }
        }
        public int GetScalarValueIntNull(string sSQL)
        {
            if (cnn.State == ConnectionState.Closed)
            {
                return 0;  
            }
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            int ret;
            if ((obj!=null) && Int32.TryParse(obj.ToString(), out ret))
                return ret;
            else
            {
                return 0;
            }
        }
        public int GetScalarValueIntNull(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            object obj = cmd.ExecuteScalar();
            int ret;
            if ((obj != null) && Int32.TryParse(obj.ToString(), out ret))
                return ret;
            else
            {
                return 0;
            }
        }
        public DataTable GetDataTable(string sSQL, MySqlParameter[] paramArray)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataSet dsWork = new DataSet();
            da.SelectCommand = cmd;
            cmd.CommandTimeout = 0;
            da.Fill(dsWork);
            return dsWork.Tables[0];
        }
        public DataTable GetDataTable(string sSQL)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            cmd.CommandTimeout = 0;
            DataTable table = new DataTable();
            MySqlDataReader dread = cmd.ExecuteReader();
            table.Load(dread);
            return table;
        }
        public DataSet GetDataSet(string sSQL,string sTableName)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataSet dsWork = new DataSet();
            da.SelectCommand = cmd;
            da.Fill(dsWork, sTableName);
            return dsWork;
        }
        public DataSet GetDataSet(string sSQL, MySqlParameter[] paramArray, string sTableName)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            MySqlDataAdapter da = new MySqlDataAdapter();
            DataSet dsWork = new DataSet();
            da.SelectCommand = cmd;
            da.Fill(dsWork, sTableName);
            return dsWork;
        }
        public bool FillDataSet(string sSQL, DataSet dsWork,  string sTableName)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dsWork, sTableName);
            return true;
        }
        public bool FillDataSet(string sSQL, MySqlParameter[] paramArray, DataSet dsWork, string sTableName)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dsWork, sTableName);
            return true;
        }

        public void CloseMySQLConnection()
        {
            cnn.Dispose(); 
        }
        //public static string ConnectionString()
        //{
        //    return Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);
        //}
        public bool TablePresent(string sTableName)
          { 
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TABLE_NAME FROM information_schema.`TABLES`" 
            + " WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = '" + sTableName + "' LIMIT 1" ;
            string sRes = (string)cmd.ExecuteScalar();
            if (sRes == sTableName)
                return true;
            else
                return false;
        }
        public bool TableFieldPresent(string TableName, string FieldName)
        {
            if (TablePresent(TableName))
            {
            string sSQL= "SHOW COLUMNS FROM " + TableName + " LIKE '" + FieldName + "'" ;
                MySqlDataReader dr = GetDataReader(sSQL);
                if (dr.Read())
                {
                    dr.Close();
                    return true;
                }
                else
                {
                    dr.Close();
                    return false;
                }
                
            }
            else
                return false;
        }
        public T GetScalarValueNull<T>(string sSQL, T retInsteadOfNullValue)
        {
            if (cnn.State == ConnectionState.Closed)
            {
                return retInsteadOfNullValue;
            }
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sSQL;
            return GetCommandValue<T>(retInsteadOfNullValue, cmd);
        }

        public T GetScalarValueNull<T>(string sSQL, MySqlParameter[] paramArray, T retInsteadOfNullValue)
        {
            if (cnn.State == ConnectionState.Closed)
            {
                return retInsteadOfNullValue;
            }
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.Text;
            for (int j = 0; j < paramArray.Length; j++)
            {
                cmd.Parameters.Add(paramArray[j]);
            }
            cmd.CommandText = sSQL;
            return GetCommandValue<T>(retInsteadOfNullValue, cmd);
        }

        private T GetCommandValue<T>(T retInsteadOfNullValue, MySqlCommand cmd)
        {
            object obj = cmd.ExecuteScalar();
            if (obj == null) return retInsteadOfNullValue;
            Type t = typeof(T);
            if (t.Name == "String") return (T)obj;
            MethodInfo TryParseT = t.GetMethod("TryParse", new Type[] { typeof(string), t.MakeByRefType() });
            if (TryParseT == null) return retInsteadOfNullValue;
            object[] args = { obj.ToString(), null };
            if ((bool)TryParseT.Invoke(null, args))
                return (T)args[1];
            else
            {
                return retInsteadOfNullValue;
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            cnn.Dispose(); 
        }

        #endregion
    }
}
