﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Script.Services;
using System.Web.Script.Serialization;
using TrackControl.Vehicles; 

namespace TrackControlWs
{
    public class GpsDataWs 
    {
        public double Altitude { get; set; }
        public double Direction { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Speed { get; set; }
        public int VehicleID { get; set; }
        public DateTime RecordTime { get; set; }
        public int RecordUnixTime { get; set; }
        public long RecordID { get; set; }
    }
}