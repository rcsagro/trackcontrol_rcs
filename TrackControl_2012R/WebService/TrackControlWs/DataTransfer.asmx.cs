﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using TrackControl.Vehicles;


namespace TrackControlWs
{
    /// <summary>
    /// Сводное описание для DataTransfer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
    [System.Web.Script.Services.ScriptService]
    public class DataTransfer : System.Web.Services.WebService
    {
        
        public DataTransfer()
        {
            
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void LoadHistory(int idMobitel, DateTime start, DateTime end)
        {
            GpsDataProvider gpsProv = new GpsDataProvider();
            List<GpsDataWs> gpoints = gpsProv.GetValidDataForPeriod(idMobitel, start, end);
            ForceJson(gpoints);
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        [WebMethod]
        public void GetLastRecordForAllVehicles()
        {
            GpsDataProvider gpsProv = new GpsDataProvider();
            List<GpsDataWs> gpoints = gpsProv.GetLastRecordForAllVehicles();
            ForceJson(gpoints);
        }

        private void ForceJson(object res)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            string str= js.Serialize(res);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(str);
            EnableCrossDmainAjaxCall();
            //Context.Response.End();
        }


        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    Context.Response.AddHeader("Access-Control-Allow-Origin", "*");
        //}

        [WebMethod]
        public string GetValue()
        {
            return "Test1";
        }

        private void EnableCrossDmainAjaxCall()
        {
            Context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (Context.Request.HttpMethod == "OPTIONS")
            {
                Context.Response.AddHeader("Access-Control-Allow-Methods",
                              "GET, POST, OPTIONS");
                Context.Response.AddHeader("Access-Control-Allow-Headers",
                              "Content-Type, Accept");
                Context.Response.End();
            }
        }
    }

}
