using TrackControl.General;

namespace TrackControl.UkrGIS.Markers
{
  /// <summary>
  /// ������, ������������ �������������� ����������� � ���������� ���������
  /// ������������� ��������
  /// </summary>
  internal class MoveMarker : OnlineMarker
  {
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.MoveMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position">�������������� ���������� ������������� ��������</param>
    /// <param name="label">������� � �������</param>
    internal MoveMarker(int id, PointLatLng position, string label)
      : base(id, position, label, Shared.GMove)
    {
    }
  }

  /// <summary>
  /// ������, ������������ �������������� ������������ ������������� ��������, �����
  /// ������� �������� ������������� ������� "���������"
  /// </summary>
  internal class StopMarker : OnlineMarker
  {
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.StopMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position">�������������� ���������� ������������� ��������</param>
    /// <param name="label">������� � �������</param>
    internal StopMarker(int id, PointLatLng position, string label)
      : base(id, position, label, Shared.GStop)
    {
    }
  }

  /// <summary>
  /// ������, ������������ �������������� ������� ������������� ��������
  /// </summary>
  internal class ParkMarker : OnlineMarker
  {
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.ParkMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position">�������������� ���������� ������������� ��������</param>
    /// <param name="label">������� � �������</param>
    internal ParkMarker(int id, PointLatLng position, string label)
      : base(id, position, label, Shared.GPark)
    {
    }
  }

  /// <summary>
  /// ������, ������������ �������������� ������������� ��������, ��������� �������� ��������
  /// ��������� ���������� �������
  /// </summary>
  internal class WarningMarker : OnlineMarker
  {
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.WarningMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position"></param>
    /// <param name="label"></param>
    internal WarningMarker(int id, PointLatLng position, string label)
      : base(id, position, label, Shared.GWarning)
    {
    }
  }
}
