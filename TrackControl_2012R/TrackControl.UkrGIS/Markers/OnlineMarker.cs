using System;
using System.Drawing;
using TrackControl.General;
using TrackControl.UkrGIS.Core;

namespace TrackControl.UkrGIS.Markers
{
  internal class OnlineMarker : GisMarker
  {    
    string _label = ""; // ������� � �������
    Image _icon; // ����������� ������� ��� ������ �����
    public bool IsActive; // �������, �������� �� ������ ����������
    public int LogicSensorState; // ��������� ����������� ������� - (��� ������� - ��������� �����)
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.MoveMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position">�������������� ���������� �������</param>
    /// <param name="label">������� � �������</param>
    /// <param name="icon">����������� �������</param>
    protected OnlineMarker(int id, PointLatLng position, string label, Image icon)
      : base(id, position)
    {
      _label = label;
      _icon = icon;
    }

    /// <summary>
    /// ���������������� ����� ��� ��������� �������.
    /// </summary>
    /// <param name="g"></param>
    internal override void OnRender(Graphics g)
    {
      if (IsActive)
      {
          g.DrawImageUnscaled(Shared.GActive, LocalPosition.X - 3, LocalPosition.Y - 60);
      }
      if (LogicSensorState == (short)LogicSensorStates.Off)
      {
          g.DrawImageUnscaled(Shared.GStrokeGreen, LocalPosition.X - 3, LocalPosition.Y - 35);
      }
      else
      {
          if (LogicSensorState == (short)LogicSensorStates.On)
          {
              g.DrawImageUnscaled(Shared.GStrokeRed, LocalPosition.X - 3, LocalPosition.Y - 35);
          }
      }
      g.DrawImageUnscaled(_icon, LocalPosition.X, LocalPosition.Y - 32);

      if (_label.Length > 0)
      {
        int x = LocalPosition.X + 10;
        int y = LocalPosition.Y - 1;
        if (IsActive)
        {
          DrawLabel(g, _label, Brushes.LightPink, Brushes.Black, x, y);
        }
        else
        {
          DrawLabel(g, _label, Brushes.White, Brushes.Navy, x, y);
        }
      }
    }
  }
}
