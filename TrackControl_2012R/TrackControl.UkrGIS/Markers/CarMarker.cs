using System;
using System.Drawing;
using TrackControl.General;
using TrackControl.UkrGIS.Core;

namespace TrackControl.UkrGIS.Markers
{
  public class CarMarker : GisMarker
  {
    /// <summary>
    /// ������� � �������.
    /// </summary>
    string _label = "";
   
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Markers.CarMarker
    /// </summary>
    /// <param name="position">�������������� ���������� �������</param>
    /// <param name="label">������� � �������</param>
    public CarMarker(int id, PointLatLng position, string label)
      : base(id, position)
    {
      _label = label;
    }

    /// <summary>
    /// ���������������� ����� ��� ��������� �������.
    /// </summary>
    /// <param name="g"></param>
    internal override void OnRender(Graphics g)
    {
      g.DrawImageUnscaled(Shared.ForTrack, LocalPosition.X - 13, LocalPosition.Y - 33);

      if (_label.Length > 0)
        DrawLabel(g, _label, Brushes.White, Brushes.Navy, LocalPosition.X + 5, LocalPosition.Y - 6);
    }
  }
}
