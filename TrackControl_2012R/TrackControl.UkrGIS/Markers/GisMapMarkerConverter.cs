using System;
using TrackControl.General;
using TrackControl.UkrGIS.Core;

namespace TrackControl.UkrGIS.Markers
{
  public static class GisMapMarkerConverter
  {
    public static GisMarker Convert(Marker marker)
    {
      GisMarker m;
      switch (marker.MarkerType)
      {
        case MarkerType.Stop:
          m = new StopMarker(marker.Id, marker.Point, marker.Title);
          break;
        case MarkerType.Alarm:
          m = new WarningMarker(marker.Id, marker.Point, marker.Title);
          break;
        case MarkerType.Moving:
          m = new MoveMarker(marker.Id, marker.Point, marker.Title);
          break;
        case MarkerType.Parking:
        case MarkerType.Custom:
          m = new ParkMarker(marker.Id, marker.Point, marker.Title);
          break;
        case MarkerType.Fueling:
          m = new FuelMarker(marker.Id, marker.Point, marker.Title);
          break;
        case MarkerType.Report:
        default:
          m = new CarMarker(marker.Id, marker.Point, marker.Title);
          break;
      }
      if (m is OnlineMarker)
      {
          ((OnlineMarker)m).IsActive = marker.IsActive;
          ((OnlineMarker)m).LogicSensorState = marker.LogicSensorState;
      }
      return m;
    }
  }
}
