using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using GeoData;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.UkrGIS.Core;


namespace TrackControl.UkrGIS
{
  /// <summary>
  /// ����� �������� ����� UkrGIS.
  /// </summary>
    public class UkrGisMap : UserControl, IMapAdvancedEvents, IMapMouseEvents
  {
    #region --   Fields   --   
    /// <summary>
    /// �������� ��� COM-�������� �����.
    /// </summary>
    private ComUkrGIS _GIS;
    /// <summary>
    /// ������������� �������, ������������ �� ����� � �������� � �����. �����������
    /// </summary>
    internal TRealRect? ScreenRect = null;
    /// <summary>
    /// ���������������� ��������� ���������������� �����.
    /// </summary>
    public readonly ObservableCollectionThreadSafe<GisOverlay> Overlays = new ObservableCollectionThreadSafe<GisOverlay>();
    #endregion

        public event Action<TRealPoint> on_Map_MouseMove;  

    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Core.UkrGIS
    /// </summary>
    public UkrGisMap()
    {
      _GIS = ComUkrGIS.Instance;
    }

    /// <summary>
    /// �������� �������� ������� �����
    /// </summary>
    public Image Screenshot
    {
      get
      {
        Image result = null;
        try
        {
          using (Bitmap bitmap = new Bitmap(Width, Height))
          {
            using (Graphics g = Graphics.FromImage(bitmap))
            {
              g.CopyFromScreen(PointToScreen(new Point()).X, PointToScreen(new Point()).Y, 0, 0, new Size(Width, Height));
              using (Font f = new Font("Arial", 9f))
              {
                string s = String.Format("TrackControl : {0:F}", DateTime.Now);
                Brush shadowBrush = Brushes.Yellow;
                g.DrawString(s, f, shadowBrush, 9, 2);
                g.DrawString(s, f, shadowBrush, 9, 1);
                g.DrawString(s, f, shadowBrush, 10, 1);
                g.DrawString(s, f, shadowBrush, 11, 1);
                g.DrawString(s, f, shadowBrush, 11, 2);
                g.DrawString(s, f, shadowBrush, 11, 3);
                g.DrawString(s, f, shadowBrush, 10, 3);
                g.DrawString(s, f, shadowBrush, 9, 3);
                g.DrawString(s, f, Brushes.Red, 10, 2);
              }
            }

            using (MemoryStream stream = new MemoryStream())
            {
              bitmap.Save(stream, ImageFormat.Png);
              result = Image.FromStream(stream);
            }
          }
        }
        catch
        {
          return null;
        }
        return result;
      }
    }

    /// <summary>
    /// ���������� ������ ���� �����
    /// </summary>
    public PointLatLng CurrentPosition
    {
      get
      {
        return _GIS.CurrentPosition;
      }
      set
      {
        _GIS.CurrentPosition = value;
      }
    }

    public double Zoom
    {
        get
        {
            if (_GIS.Map != null)
                return _GIS.Map.View.Scale;
            else
                return 0;
        }
      set
      {
          if(_GIS.Map != null)
              _GIS.Map.View.Scale = value;
      }
    }

    public void PanTo(RectLatLng rect)
    {
      TRealRect tr = new TRealRect();
      tr.XMin = rect.Left;
      tr.XMax = rect.Right;
      tr.YMin = rect.Bottom;
      tr.YMax = rect.Top;
      _GIS.Map.View.ViewRect(ref tr);
    }
    
    #region --   Overrides Members   --
    /// <summary>
    /// ������������ ��� ��������� �������� ��������
    /// </summary>
    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);

      _GIS.Resize();
    }

    /// <summary>
    /// ������������ ��� �������� ��������
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      _GIS.Adjust(this);
    }
    #endregion
    
    public void ZoomAndCenterAll()
    {
      if (InvokeRequired)
      {
        MethodInvoker m = delegate
        {
          zoomAll();
        };
        Invoke(m);
      }
      else
      {
        zoomAll();
      }
    }

    #region --   IMapAdvancedEvents Members   --
    /// <summary>
    /// ������������ ��� ����������� �����.
    /// </summary>
    /// <param name="DC"></param>
    public void Map_PaintWindow(int DC)
    {
      using (Graphics g = Graphics.FromHwnd(new IntPtr(_GIS.Map.Handle)))
      {
        g.SmoothingMode = SmoothingMode.AntiAlias;
        foreach (GisOverlay o in Overlays)
        {
          o.Render(g);
        }
      }
    }
    /// <summary>
    /// ������������ ��� ���������� ����� � �����.
    /// </summary>
    /// <param name="Data"></param>
    public void Map_Rendering(ref TMapRenderInfo Data)
    {
    }
    /// <summary>
    /// ������������ ��� ��������� ������ �����.
    /// </summary>
    public void Map_ToolChanged()
    {
    }
    public void Map_ViewChanged(int Changes)
    {
    }
    #endregion
    
    #region --   Private Members   --   
    
    /// <summary>
    /// ���������� ������������� �������, ������������ ��� ����������������
    /// ������� �� �����
    /// </summary>
    private TRealRect? getRectOfAllObjects()
    {
      bool valid = false;
      TRealRect rect = new TRealRect();
      rect.XMin = double.MaxValue;
      rect.YMin = double.MaxValue;
      rect.XMax = double.MinValue;
      rect.YMax = double.MinValue;

      foreach (GisOverlay o in Overlays)
      {
        if (o.Visible && (o.Markers.Count > 0 || o.Tracks.Count > 0))
        {
          valid = true;
          foreach (GisMarker m in o.Markers)
          {
            if (m.Position.Lng > rect.XMax) { rect.XMax = m.Position.Lng; }
            if (m.Position.Lng < rect.XMin) { rect.XMin = m.Position.Lng; }
            if (m.Position.Lat > rect.YMax) { rect.YMax = m.Position.Lat; }
            if (m.Position.Lat < rect.YMin) { rect.YMin = m.Position.Lat; }
          }
          foreach (GisTrack t in o.Tracks)
          {
            t.GeoPoints.ForEach(delegate(IGeoPoint igp)
            {
              PointLatLng p = igp.LatLng;
              if (p.Lng > rect.XMax)
                rect.XMax = p.Lng;
              if (p.Lng < rect.XMin)
                rect.XMin = p.Lng;
              if (p.Lat > rect.YMax)
                rect.YMax = p.Lat;
              if (p.Lat < rect.YMin)
                rect.YMin = p.Lat;
            });
          }
        }
      }
      if (valid)
      {
        if (rect.XMax - rect.XMin < 0.01)
        {
          rect.XMin -= 0.005;
          rect.XMax += 0.005;
        }
        if (rect.YMax - rect.YMin < 0.01)
        {
          rect.YMin -= 0.005;
          rect.YMax += 0.005;
        }

        return rect;
      }
      return null;
    }

    private void zoomAll()
    {
      TRealRect? r = getRectOfAllObjects();
      if (r.HasValue)
      {
        TRealRect rect = r.Value;
        double hPadding = (rect.XMax - rect.XMin) * 0.1;
        double vPadding = (rect.YMax - rect.YMin) * 0.1;
        if (!ScreenRect.HasValue ||
            rect.XMin > ScreenRect.Value.XMin ||
            rect.XMax < ScreenRect.Value.XMax ||
            rect.YMin > ScreenRect.Value.YMin ||
            rect.YMax < ScreenRect.Value.YMax)
        {
          rect.XMin -= hPadding;
          rect.XMax += hPadding;
          rect.YMin -= vPadding;
          rect.YMax += vPadding * 1.5; // ����� ������ �������

          _GIS.Map.View.ViewRect(ref rect);
          rect.YMax -= vPadding * 0.5;
          ScreenRect = rect;
        }
      }
    }
    #endregion

    #region IMapMouseEvents Members

    public void Map_MouseDown(TxMouseButton Button, TxShiftState Shift, ref TScreenPoint P)
    {

    }

    public void Map_MouseMove(TxShiftState Shift, ref TScreenPoint P)
    {
        //TRealPoint Pt = _gisMap.Map.View.ClientToMap(ref P);
        TRealPoint Pt = _GIS.Map.View.ClientToMap(ref P);
        on_Map_MouseMove(Pt);
    }

    public void Map_MouseUp(TxMouseButton Button, TxShiftState Shift, ref TScreenPoint P)
    {

    }

    #endregion

    private void InitializeComponent()
    {
        this.SuspendLayout();
        // 
        // UkrGisMap
        // 
        this.Name = "UkrGisMap";
        this.Size = new System.Drawing.Size(161, 150);
        this.ResumeLayout(false);

    }
  }
}
