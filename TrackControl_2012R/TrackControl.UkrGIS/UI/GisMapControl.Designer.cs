namespace TrackControl.UkrGIS
{
  partial class GisMapControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this._barManager = new DevExpress.XtraBars.BarManager( this.components );
        this._gisTools = new DevExpress.XtraBars.Bar();
        this._reloadBtn = new DevExpress.XtraBars.BarButtonItem();
        this._infoBtn = new DevExpress.XtraBars.BarButtonItem();
        this._dragBtn = new DevExpress.XtraBars.BarButtonItem();
        this._distanceBtn = new DevExpress.XtraBars.BarButtonItem();
        this._areaBtn = new DevExpress.XtraBars.BarButtonItem();
        this._zoomInBtn = new DevExpress.XtraBars.BarButtonItem();
        this._zoomOutBtn = new DevExpress.XtraBars.BarButtonItem();
        this._screenshotBtn = new DevExpress.XtraBars.BarButtonItem();
        this.beiPosition = new DevExpress.XtraBars.BarEditItem();
        this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
        this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
        this.bsiPosition = new DevExpress.XtraBars.BarStaticItem();
        ( ( System.ComponentModel.ISupportInitialize )( this._barManager ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).BeginInit();
        this.SuspendLayout();
        // 
        // _barManager
        // 
        this._barManager.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this._gisTools} );
        this._barManager.DockControls.Add( this.barDockControlTop );
        this._barManager.DockControls.Add( this.barDockControlBottom );
        this._barManager.DockControls.Add( this.barDockControlLeft );
        this._barManager.DockControls.Add( this.barDockControlRight );
        this._barManager.Form = this;
        this._barManager.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this._reloadBtn,
            this._dragBtn,
            this._distanceBtn,
            this._areaBtn,
            this._screenshotBtn,
            this._zoomInBtn,
            this._infoBtn,
            this._zoomOutBtn,
            this.bsiPosition,
            this.beiPosition} );
        this._barManager.MaxItemId = 11;
        this._barManager.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1} );
        // 
        // _gisTools
        // 
        this._gisTools.BarItemHorzIndent = 5;
        this._gisTools.BarName = "Tools";
        this._gisTools.CanDockStyle = ( ( DevExpress.XtraBars.BarCanDockStyle )( ( DevExpress.XtraBars.BarCanDockStyle.Top | DevExpress.XtraBars.BarCanDockStyle.Bottom ) ) );
        this._gisTools.DockCol = 0;
        this._gisTools.DockRow = 0;
        this._gisTools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
        this._gisTools.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._reloadBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._infoBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._dragBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._distanceBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._areaBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._zoomInBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._zoomOutBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._screenshotBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiPosition, "", false, true, true, 155)} );
        this._gisTools.OptionsBar.AllowQuickCustomization = false;
        this._gisTools.OptionsBar.UseWholeRow = true;
        this._gisTools.Text = "Tools";
        // 
        // _reloadBtn
        // 
        this._reloadBtn.Caption = "Reload";
        this._reloadBtn.Hint = "��������� ������ �����";
        this._reloadBtn.Id = 0;
        this._reloadBtn.Name = "_reloadBtn";
        this._reloadBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._reloadBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.reload_ItemClick );
        // 
        // _infoBtn
        // 
        this._infoBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._infoBtn.Caption = "����";
        this._infoBtn.Hint = "���������� � ���������";
        this._infoBtn.Id = 7;
        this._infoBtn.Name = "_infoBtn";
        this._infoBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._infoBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.info_ItemClick );
        // 
        // _dragBtn
        // 
        this._dragBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._dragBtn.Caption = "Dragging Map";
        this._dragBtn.Hint = "����������� �����";
        this._dragBtn.Id = 1;
        this._dragBtn.Name = "_dragBtn";
        this._dragBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._dragBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.drag_ItemClick );
        // 
        // _distanceBtn
        // 
        this._distanceBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._distanceBtn.Caption = "Distance";
        this._distanceBtn.Hint = "��������� ����������";
        this._distanceBtn.Id = 2;
        this._distanceBtn.Name = "_distanceBtn";
        this._distanceBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._distanceBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.distance_ItemClick );
        // 
        // _areaBtn
        // 
        this._areaBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._areaBtn.Caption = "Area";
        this._areaBtn.Hint = "��������� ��������";
        this._areaBtn.Id = 4;
        this._areaBtn.Name = "_areaBtn";
        this._areaBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.area_ItemClick );
        // 
        // _zoomInBtn
        // 
        this._zoomInBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._zoomInBtn.Caption = "Zoom In";
        this._zoomInBtn.Hint = "��������������� ������������";
        this._zoomInBtn.Id = 6;
        this._zoomInBtn.Name = "_zoomInBtn";
        this._zoomInBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.zoomIn_ItemClick );
        // 
        // _zoomOutBtn
        // 
        this._zoomOutBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
        this._zoomOutBtn.Caption = "Zoom Out";
        this._zoomOutBtn.Hint = "��������������� ���������";
        this._zoomOutBtn.Id = 8;
        this._zoomOutBtn.Name = "_zoomOutBtn";
        this._zoomOutBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._zoomOutBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.zoomOut_ItemClick );
        // 
        // _screenshotBtn
        // 
        this._screenshotBtn.Caption = "Screenshot";
        this._screenshotBtn.Hint = "������ ������� �����";
        this._screenshotBtn.Id = 5;
        this._screenshotBtn.Name = "_screenshotBtn";
        this._screenshotBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
        this._screenshotBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.screenshot_ItemClick );
        // 
        // beiPosition
        // 
        this.beiPosition.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
        this.beiPosition.Edit = this.repositoryItemTextEdit1;
        this.beiPosition.EditValue = "Lan = 0; Long =0;";
        this.beiPosition.Id = 10;
        this.beiPosition.Name = "beiPosition";
        // 
        // repositoryItemTextEdit1
        // 
        this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
        this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        this.repositoryItemTextEdit1.AutoHeight = false;
        this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
        // 
        // bsiPosition
        // 
        this.bsiPosition.Caption = "Lan = 0; Long = 0;";
        this.bsiPosition.Id = 9;
        this.bsiPosition.Name = "bsiPosition";
        this.bsiPosition.TextAlignment = System.Drawing.StringAlignment.Near;
        // 
        // GisMapControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add( this.barDockControlLeft );
        this.Controls.Add( this.barDockControlRight );
        this.Controls.Add( this.barDockControlBottom );
        this.Controls.Add( this.barDockControlTop );
        this.Name = "GisMapControl";
        this.Size = new System.Drawing.Size( 661, 477 );
        ( ( System.ComponentModel.ISupportInitialize )( this._barManager ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this.repositoryItemTextEdit1 ) ).EndInit();
        this.ResumeLayout( false );

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _gisTools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private TrackControl.UkrGIS.UkrGisMap _map;
    private DevExpress.XtraBars.BarButtonItem _reloadBtn;
    private DevExpress.XtraBars.BarButtonItem _dragBtn;
    private DevExpress.XtraBars.BarButtonItem _distanceBtn;
    private DevExpress.XtraBars.BarButtonItem _areaBtn;
    private DevExpress.XtraBars.BarButtonItem _screenshotBtn;
    private DevExpress.XtraBars.BarButtonItem _zoomInBtn;
    private DevExpress.XtraBars.BarButtonItem _infoBtn;
    private DevExpress.XtraBars.BarButtonItem _zoomOutBtn;
    private DevExpress.XtraBars.BarStaticItem bsiPosition;
    private DevExpress.XtraBars.BarEditItem beiPosition;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
  }
}
