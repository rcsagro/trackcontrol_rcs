using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using GeoData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Hasp;
using TrackControl.UkrGIS.Core;
using TrackControl.UkrGIS.Markers;
using TrackControl.UkrGIS.Properties;

namespace TrackControl.UkrGIS
{
    public partial class GisMapControl : XtraUserControl, IMap
    {
        /// <summary>
        /// ���� ��� ������ � ��������
        /// </summary>
        private GisOverlay _tracks;

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.Maps.GisMapControl
        /// </summary>
        public GisMapControl()
        {
            InitializeComponent();
            init();

            _map = new UkrGisMap();
            _map.BorderStyle = BorderStyle.FixedSingle;
            _map.Dock = DockStyle.Fill;
            Controls.Add(_map);
            _map.on_Map_MouseMove += Map_MouseMove;

            _tracks = new GisOverlay(_map, "Tracks");
            _map.Overlays.Add(_tracks);
            _infoBtn.Down = true;
        }

        #region IMap Members

        /// <summary>
        /// ��������� �����
        /// </summary>
        public MapState State
        {
            get
            {
                return new MapState(ZoomConverter.ToGMap(_map.Zoom), _map.CurrentPosition);
            }
            set
            {
                _map.CurrentPosition = value.Center;
                _map.Zoom = ZoomConverter.ToUkrGIS(value.Zoom);
            }
        }

        private int selMobitelId = 0;

        public int SelectMobitelId
        {
            get { return selMobitelId; }
            set { selMobitelId = value; }
        }

        /// <summary>
        /// ���������� ����� ���� ����� � ��������� �������������� �����
        /// </summary>
        /// <param name="point">�������������� ����� ������ ������ �����</param>
        public void PanTo(PointLatLng point)
        {
            _map.CurrentPosition = point;
        }

        /// <summary>
        /// ������������� ����������� ��������� �������, ����� �������� ���������
        /// �������������� ������������� �������.
        /// </summary>
        /// <param name="rect">������������� ������� � �������������� �����������</param>
        public void PanTo(RectLatLng rect)
        {
            _map.PanTo(rect);
        }

        /// <summary>
        /// ��������� ������ �� �����
        /// </summary>
        /// <param name="marker">������ ��� �����</param>
        public void AddMarker(Marker marker)
        {
            GisMarker m = GisMapMarkerConverter.Convert(marker);
            _tracks.Markers.Add(m);
        }

        /// <summary>
        /// ��������� �� ����� ������ ��������
        /// </summary>
        /// <param name="markers">������ �������� ��� �����</param>
        public void AddMarkers(IEnumerable<Marker> markers)
        {
            foreach (Marker m in markers)
                AddMarker(m);
        }

        /// <summary>
        /// ������� � ����� ��� ����������� �������
        /// </summary>
        public void ClearMarkers()
        {
            _tracks.Markers.Clear();
        }

        /// <summary>
        /// ��������� �� ����� ����
        /// </summary>
        /// <param name="track">���� ��� �����</param>
        public void AddTrack(Track track)
        {
            GisTrack t = new GisTrack(0, track.GeoPoints, track.Color, 2f);
            _tracks.Tracks.Add(t);
        }

        /// <summary>
        /// ��������� �� ����� ������ ������
        /// </summary>
        /// <param name="tracks">������ ������ ��� �����</param>
        public void AddTracks(IEnumerable<Track> tracks)
        {
            foreach (Track t in tracks)
                AddTrack(t);
        }

        /// <summary>
        /// ������� � ����� ��� ����������� �����
        /// </summary>
        public void ClearTracks()
        {
            _tracks.Tracks.Clear();
        }

        /// <summary>
        /// ������������� ������������ �������, ����� �������� ��� �����������
        /// ������� � �����
        /// </summary>
        public void ZoomAndCenterAll()
        {
            _map.ZoomAndCenterAll();
        }

        /// <summary>
        /// ��������� �� ����� ����������� ����
        /// </summary>
        /// <param name="zone">����������� ����</param>
        public void AddZone(IZone zone)
        {
            ComUkrGIS map = ComUkrGIS.Instance;
            map.AddZone(zone.Name, zone.Points, 0);
            map.Refresh();
        }

        /// <summary>
        /// ��������� �� ����� ������ ����������� ���
        /// </summary>
        /// <param name="zones">������ ����������� ���</param>
        public void AddZones(IEnumerable<IZone> zones)
        {
            ComUkrGIS map = ComUkrGIS.Instance;
            foreach (IZone z in zones)
            {
                map.AddZone(z.Name, z.Points, 0);
            }
            map.Refresh();
        }

        /// <summary>
        /// ������� � ����� ��� ����������� ����������� ����
        /// </summary>
        public void ClearZones()
        {
            ComUkrGIS map = ComUkrGIS.Instance;
            map.ClearZones();
            map.Refresh();
        }

        public void ClearAll()
        {
            ComUkrGIS map = ComUkrGIS.Instance;
            map.ClearZones();
            _tracks.Tracks.Clear();
            _tracks.Markers.Clear();
        }

        public void Repaint()
        {
            ComUkrGIS.Instance.Repaint();
        }

        public void ResetTools()
        {
        }

        #endregion

        #region --    Privates   --

        private void init()
        {
            this._reloadBtn.Caption = Resources.CaptionReload;
            this._reloadBtn.Hint = Resources.HintReload;
            this._infoBtn.Caption = Resources.CaptionInfo;
            this._infoBtn.Hint = Resources.HintInfo;
            this._dragBtn.Caption = Resources.CaptionDraggingMap;
            this._dragBtn.Hint = Resources.HintDraggingMap;
            this._zoomInBtn.Caption = Resources.CaptionZoomIn;
            this._zoomInBtn.Hint = Resources.HintZoomIn;
            this._distanceBtn.Caption = Resources.CaptionDistance;
            this._distanceBtn.Hint = Resources.HintDistance;
            this._areaBtn.Caption = Resources.CaptionArea;
            this._areaBtn.Hint = Resources.HintArea;
            this._screenshotBtn.Caption = Resources.CaptionScreenshot;
            this._screenshotBtn.Hint = Resources.HintScreenshot;
            this._zoomOutBtn.Caption = Resources.CaptionZoomOut;
            this._zoomOutBtn.Hint = Resources.HintZoomOut;

            _reloadBtn.Glyph = Shared.ReloadMap;
            _infoBtn.Glyph = Shared.InfoBalloon;
            _dragBtn.Glyph = Shared.Hand;
            _distanceBtn.Glyph = Shared.Distance;
            _areaBtn.Glyph = Shared.Area;
            _screenshotBtn.Glyph = Shared.ScreenShot;
            _zoomInBtn.Glyph = Shared.ZoomIn;
            _zoomOutBtn.Glyph = Shared.ZoomOut;
        }

        #region --   GUI ����������� �������   --

        /// <summary>
        /// ������������ ���� �� ������ "Reload Map"
        /// </summary>
        private void reload_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = Resources.UkrGisFile + " \"GDB\" (*.gdb)|*.gdb";
                if (DialogResult.OK == ofd.ShowDialog())
                {
                    bool opened = ComUkrGIS.Instance.Open(ofd.FileName, HaspWrap.MapCodes);
                    if (opened)
                        saveMapFilePath(ofd.FileName);
                }
            }
        }

        /// <summary>
        /// ������������ ���� �� ������ "����"
        /// </summary>
        private void info_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.Info;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Dragging Map"
        /// </summary>
        private void drag_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.Move;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Zoom In"
        /// </summary>
        private void zoomIn_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.ZoomIn;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Zoom Out"
        /// </summary>
        private void zoomOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.ZoomOut;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Distance"
        /// </summary>
        private void distance_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.Distance;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Area"
        /// </summary>
        private void area_ItemClick(object sender, ItemClickEventArgs e)
        {
            highlightButton(e.Item as BarButtonItem);
            ComUkrGIS.Instance.Tool = MapTool.Area;
        }

        /// <summary>
        /// ������������ ���� �� ������ "Screenshot"
        /// </summary>
        private void screenshot_ItemClick(object sender, ItemClickEventArgs e)
        {
            ImageService.SaveImage(_map.Screenshot, "Map_Shot");
        }

        #endregion

        /// <summary>
        /// ������������ ��������� ������ ����� ������ ������ ������ �����
        /// </summary>
        /// <param name="button">������, ������� ������� ������� ��������</param>
        private void highlightButton(BarButtonItem button)
        {
            _infoBtn.Down = false;
            _dragBtn.Down = false;
            _zoomInBtn.Down = false;
            _zoomOutBtn.Down = false;
            _distanceBtn.Down = false;
            _areaBtn.Down = false;

            button.Down = true;
        }

        private static void saveMapFilePath(string path)
        {
            string settingsPath = Path.Combine(Globals.APP_DATA, "map.xml");
            string text = String.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
<map>
  <id>2</id>
  <path>{0}</path>
  <type>GoogleMap</type>
  <zoom>8</zoom>
</map>", path);
            using (StreamWriter writer = new StreamWriter(settingsPath, false, Encoding.UTF8))
            {
                writer.Write(text);
            }
        }

        #endregion

        #region IMap Members

        public List<PointLatLng> PointsFromTrack
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void Map_MouseMove(TRealPoint Pt)
        {
            if ((Math.Abs(Pt.X) <= 180) && (Math.Abs(Pt.Y) <= 90))
            {
                beiPosition.EditValue = String.Format("Lon={0:#.####} Lat={1:#.####}", Pt.X, Pt.Y);
            }
        }


    }
}
