using System;
using System.Collections.Generic;

namespace TrackControl.UkrGIS.SearchEngine
{
  internal class MapLayerGroup
  {
    /// <summary>
    /// �������� �� � ������ ������ ���� ������������������.
    /// </summary>
    internal bool Single
    {
      get { return _single; }
      set { _single = value; }
    }
    bool _single;

    /// <summary>
    /// ������ ����� ��� ������, ��������������� �� �������� ����
    /// </summary>
    internal List<MapLayer> Layers
    {
      get { return _layers; }
    }
    List<MapLayer> _layers = new List<MapLayer>();
  }
}
