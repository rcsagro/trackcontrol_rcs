using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using GeoData;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.UkrGIS.Properties;

namespace TrackControl.UkrGIS.SearchEngine
{
    public class GeoLocator : Singleton<GeoLocator>, IGeoLocator
    {
        private readonly string GDB_PATH = Path.Combine(TrackControl.General.Globals.APP_DATA, "Gdb.xml");

        /// <summary>
        /// ��������� �� �������� �������� ������
        /// </summary>
        public bool Valid
        {
            get { return _valid; }
        }

        private bool _valid;

        /// <summary>
        /// GIS �������, ������������ ����� � �������������� �������� ��� ������ � ���
        /// </summary>
        private IMapControl _map;

        /// <summary>
        /// ��������� �� � ������� GDB
        /// </summary>
        private IBase _gdb;

        /// <summary>
        /// ������ �����, � ������� ����� �������������� ����� ��������
        /// </summary>
        private List<MapLayerGroup> _groups = new List<MapLayerGroup>();

        /// <summary>
        /// ������������� ��������� �������� ������ �����������
        /// </summary>
        /// <param name="mapId">������������� �� �����</param>
        /// <param name="map">GIS �����</param>
        /// <param name="gdb">��������� �� � ������� GDB</param>
        public void Adjust(string mapId, IMapControl map, IBase gdb)
        {
            _map = map;
            _gdb = gdb;
            _groups.Clear();

            if (!File.Exists(GDB_PATH))
            {
                _valid = false;
                return;
            }
            if (null == _map || null == _gdb)
            {
                _valid = false;
                return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(GDB_PATH);

                string xpath = String.Format("map[@file='{0}']", mapId);
                XmlNode mapNode = doc.DocumentElement.SelectSingleNode(xpath);
                if (mapNode != null)
                {
                    XmlNodeList groupNodeList = mapNode.SelectNodes("group");
                    foreach (XmlNode groupNode in groupNodeList)
                    {
                        MapLayerGroup group = new MapLayerGroup();
                        if (groupNode.Attributes["single"] != null)
                        {
                            group.Single = true;
                        }
                        XmlNodeList layerNodeList = groupNode.SelectNodes("layer");
                        foreach (XmlNode layerNode in layerNodeList)
                        {
                            MapLayer layer = new MapLayer();
                            layer.Name = layerNode.Attributes["name"].Value;
                            layer.Min = Int32.Parse(layerNode.Attributes["min"].Value);
                            layer.Max = Int32.Parse(layerNode.Attributes["max"].Value);

                            XmlNodeList fieldNodeList = layerNode.SelectNodes("field");
                            foreach (XmlNode fieldNode in fieldNodeList)
                            {
                                string name = fieldNode.Attributes["name"].Value;
                                if (fieldNode.Attributes["linked"] != null)
                                {
                                    string relative = fieldNode.Attributes["relative"].Value;
                                    string key = fieldNode.Attributes["key"].Value;
                                    MapLinkedField linkedField = new MapLinkedField(name, relative, key);
                                    XmlNodeList list = fieldNode.SelectNodes("field");
                                    foreach (XmlNode fn in list)
                                    {
                                        string desc = "";
                                        if (fn.Attributes["description"] != null)
                                        {
                                            desc = fn.Attributes["description"].Value;
                                        }
                                        MapSimpleField f = new MapSimpleField(fn.Attributes["name"].Value, desc);
                                        if (fn.Attributes["preceded"] != null)
                                        {
                                            f.Preceded = true;
                                        }
                                        if (fn.Attributes["space"] != null)
                                        {
                                            f.Space = true;
                                        }
                                        linkedField.FieldList.Add(f);
                                    }
                                    layer.FieldList.Add(linkedField);
                                }
                                else
                                {
                                    string description = "";
                                    if (fieldNode.Attributes["description"] != null)
                                    {
                                        description = fieldNode.Attributes["description"].Value;
                                    }
                                    MapSimpleField simpleField = new MapSimpleField(name, description);
                                    if (fieldNode.Attributes["preceded"] != null)
                                    {
                                        simpleField.Preceded = true;
                                    }
                                    if (fieldNode.Attributes["space"] != null)
                                    {
                                        simpleField.Space = true;
                                    }
                                    if (fieldNode.Attributes["empty"] != null)
                                    {
                                        simpleField.Empty = true;
                                    }
                                    layer.FieldList.Add(simpleField);
                                }
                            }
                            group.Layers.Add(layer);
                        }
                        _groups.Add(group);
                    }
                    _valid = true;
                }
            }
            catch
            {
                _valid = false;
            }
        }

        /// <summary>
        /// ������������ ����� ������� � ���� GDB, ���������� � �����, �������� ������������
        /// </summary>
        public string GetLocationInfo(PointLatLng location)
        {
            string reserve =
                String.Format(
                    @"(" + Resources.UkrGisLatitude + @": {0:N5}, " + Resources.UkrGisLongitude + @": {1:N5})",
                    location.Lat, location.Lng);

            if (_valid)
            {
                StringBuilder result = new StringBuilder();
                TRealPoint point;
                point.X = location.Lng;
                point.Y = location.Lat;

                foreach (MapLayerGroup group in _groups)
                {
                    foreach (MapLayer mapLayer in group.Layers)
                    {
                        bool ok = false;
                        IMapLayer layer = _map.Project.LayerByName(mapLayer.Name);

                        if (layer == null)
                        {
                            _valid = false;
                            return reserve;
                        }
                        object obj = layer.ItemsByGeoPoint(_map.View, ref point, mapLayer.Min);
                        if (Convert.IsDBNull(obj) == true)
                        {
                            obj = layer.ItemsByGeoPoint(_map.View, ref point, mapLayer.Max);
                            if (Convert.IsDBNull(obj) == true)
                            {
                                continue;
                            }
                        }
                        obj = layer.SortItemsByDist(obj, point);
                        Array index = (Array) obj;

                        #region -- Old way  --

                        //int rec = Convert.ToInt32(index.GetValue(0));
                        //if (!ScanFields(mapLayer,layer, ref result,rec,ref ok)) return reserve;

                        #endregion

                        #region -- Another way --

                        StringBuilder builder = new StringBuilder();

                        for (int i = 0; i < index.Length; i++)
                        {
                            int rec = Convert.ToInt32(index.GetValue(i));

                            if (!ScanFields(mapLayer, layer, ref builder, rec, ref ok)) 
                                return reserve;

                            if (builder.Length > 0) break;
                        }

                        result.Append(builder.ToString());

                        #endregion

                        if (group.Single && ok)
                        {
                            break;
                        }
                    }
                }
                if (result.Length > 0)
                {
                    result.Length -= 2;
                    return result.ToString();
                }
                else
                {
                    return reserve;
                }
            }
            else
            {
                return reserve;
            }
        }

        /// <summary>
        /// ������������ ����� ������� � ���� GDB, ���������� � ����� � �������� �������
        /// </summary>
        /// <param name="location"> ���������� ����� </param>
        /// <param name="iRadius">������ ����������� ����� � ������. ���� ���� ������������ ������ �� ���������</param>
        /// <returns></returns>
        public string GetLocationInfoRadius(PointLatLng location, int iRadius)
        {
            string reserve = "";
            if (_valid)
            {
                StringBuilder result = new StringBuilder();
                TRealPoint point;
                point.X = location.Lng;
                point.Y = location.Lat;

                foreach (MapLayerGroup group in _groups)
                {
                    foreach (MapLayer mapLayer in group.Layers)
                    {
                        bool ok = false;
                        IMapLayer layer = _map.Project.LayerByName(mapLayer.Name);
                        if (layer == null)
                        {
                            _valid = false;
                            return reserve;
                        }
                        object obj = layer.ItemsByGeoPoint(_map.View, ref point, iRadius);
                        if (Convert.IsDBNull(obj) == true)
                        {
                            continue;
                        }
                        obj = layer.SortItemsByDist(obj, point);
                        Array index = (Array) obj;
                        int rec = Convert.ToInt32(index.GetValue(0));
                        if (!ScanFields(mapLayer, layer, ref result, rec, ref ok)) return reserve;
                        if (group.Single && ok)
                        {
                            break;
                        }
                    }
                }
                if (result.Length > 0)
                {
                    result.Length -= 2;
                    return result.ToString();
                }
                else
                {
                    return reserve;
                }
            }
            else
            {
                return reserve;
            }
        }

        private bool ScanFields(MapLayer mapLayer, IMapLayer layer, ref StringBuilder result, int rec, ref bool ok)
        {
            foreach (IMapField mapField in mapLayer.FieldList)
            {
                if (mapField is MapSimpleField)
                {
                    MapSimpleField field = (MapSimpleField) mapField;

                    if (field.Empty)
                    {
                        if (field.Space)
                        {
                            result.AppendFormat("{0} ", field.Description);
                        }
                        else
                        {
                            result.AppendFormat("{0}, ", field.Description);
                        }
                        ok = true;
                    }
                    else
                    {
                        try
                        {
                            int fldNumber = layer.Table.FindField(field.Name);

                            string fldValue = layer.Table.get_AsString(fldNumber, rec); // ����� ��������

                            if (fldValue.Length > 0)
                            {
                                ok = true;
                                if (field.Description.Length > 0)
                                {
                                    if (field.Preceded)
                                    {
                                        result.AppendFormat("{0} {1}, ", field.Description, fldValue);
                                    }
                                    else
                                    {
                                        result.AppendFormat("{0} {1}, ", fldValue, field.Description);
                                    }
                                }
                                else
                                {
                                    if (field.Space)
                                    {
                                        result.AppendFormat("{0} ", fldValue);
                                    }
                                    else
                                    {
                                        result.AppendFormat("{0}, ", fldValue);
                                    }
                                }
                            }
                        }
                        catch
                        {
                            _valid = false;
                            return false;
                        }
                    }
                }
                else if (mapField is MapLinkedField)
                {
                    MapLinkedField field = (MapLinkedField) mapField;
                    try
                    {
                        int fldNumber = layer.Table.FindField(field.Name);
                        int code = layer.Table.get_AsInteger(fldNumber, rec);
                        IGdbTable table = _gdb.TableByName(field.Relative);
                        int fld = table.FindField(field.Key);
                        object o = table.FindRecsByIndex(fld, "=", code);
                        if (Convert.IsDBNull(o) != true)
                        {
                            Array index1 = (Array) o;
                            int r = Convert.ToInt32(index1.GetValue(0));
                            foreach (MapSimpleField msf in field.FieldList)
                            {
                                int f = table.FindField(msf.Name);
                                string fldValue = table.get_AsString(f, r);
                                if (fldValue.Length > 0)
                                {
                                    ok = true;
                                    if (msf.Description.Length > 0)
                                    {
                                        if (msf.Preceded)
                                        {
                                            result.AppendFormat("{0} {1}, ", msf.Description, fldValue);
                                        }
                                        else
                                        {
                                            result.AppendFormat("{0} {1}, ", fldValue, msf.Description);
                                        }
                                    }
                                    else
                                    {
                                        if (msf.Space)
                                        {
                                            result.AppendFormat("{0} ", fldValue);
                                        }
                                        else
                                        {
                                            result.AppendFormat("{0}, ", fldValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        _valid = false;
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// ��������� ������� ����������� � ����������� ���������� ���������. � ������ ���
        /// ���������� �������, ��������� ���������� ������ Gdb.xml
        /// </summary>
        public static void FixRepository()
        {
            string dir = Globals.APP_DATA;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            string file = Path.Combine(dir, "Gdb.xml");
            if (!File.Exists(file))
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                string copy = String.Format("{0}.Gdb.xml", assembly.GetName().Name);
                using (Stream stream = assembly.GetManifestResourceStream(copy))
                {
                    byte[] bStr = new Byte[stream.Length];
                    stream.Read(bStr, 0, (int) stream.Length);
                    using (FileStream fs = File.Create(file))
                    {
                        fs.Write(bStr, 0, bStr.Length);
                    }
                }
            }
        }
    }
}
