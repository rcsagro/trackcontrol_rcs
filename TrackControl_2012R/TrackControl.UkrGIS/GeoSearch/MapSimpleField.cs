using System;

namespace TrackControl.UkrGIS.SearchEngine
{
  internal class MapSimpleField : IMapField
  {
    /// <summary>
    /// Название поля в таблице GDB
    /// </summary>
    public string Name
    {
      get { return _name; }
    }
    string _name;

    /// <summary>
    /// Описание поля
    /// </summary>
    public string Description
    {
      get { return _description; }
    }
    string _description;

    /// <summary>
    /// Следует ли начинать с описания?
    /// </summary>
    public bool Preceded;

    /// <summary>
    /// После описания следует пробел или запятая?
    /// </summary>
    public bool Space;

    /// <summary>
    /// Не использовать таблицу, достаточно только описания
    /// </summary>
    public bool Empty;

    #region --   .ctor   --
    /// <summary>
    /// Создает и инициализирует экземпляр класса GIS.SearchEngine.MapField
    /// </summary>
    /// <param name="name">Название поля</param>
    /// <param name="description">Описание поля</param>
    internal MapSimpleField(string name, string description)
    {
      _name = name;
      _description = description;
    }
    #endregion
  }
}
