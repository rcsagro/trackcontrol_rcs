using System;
using System.Collections.Generic;

namespace TrackControl.UkrGIS.SearchEngine
{
  internal class MapLinkedField : IMapField
  {
    /// <summary>
    /// Название поля в таблице GDB
    /// </summary>
    public string Name
    {
      get { return _name; }
    }
    string _name;

    /// <summary>
    /// Название связанной таблицы GDB
    /// </summary>
    public string Relative
    {
      get { return _relative; }
    }
    string _relative;

    /// <summary>
    /// Название поля в связанной таблице GDB, по которому осуществляется связь
    /// </summary>
    public string Key
    {
      get { return _key; }
    }
    string _key;

    /// <summary>
    /// Список необходимых полей в связанной таблице
    /// </summary>
    public List<MapSimpleField> FieldList
    {
      get { return _fieldList; }
    }
    List<MapSimpleField> _fieldList;

    #region --   .ctor   --
    /// <summary>
    /// Создает и инициализирует экземпляр класса GIS.SearchEngine.MapComplexField
    /// </summary>
    /// <param name="name">Название поля</param>
    /// <param name="relative">Название связанной таблицы GDB</param>
    /// <param name="key">Название поля в связанной таблице, по которому осуществляется связь</param>
    internal MapLinkedField(string name, string relative, string key)
    {
      _name = name;
      _relative = relative;
      _key = key;
      _fieldList = new List<MapSimpleField>();
    }
    #endregion
  }
}
