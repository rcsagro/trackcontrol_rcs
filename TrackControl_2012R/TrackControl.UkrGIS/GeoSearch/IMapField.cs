using System;

namespace TrackControl.UkrGIS.SearchEngine
{
  internal interface IMapField
  {
    string Name { get; }
  }
}