using System;
using System.Collections.Generic;

namespace TrackControl.UkrGIS.SearchEngine
{
  internal class MapLayer
  {
    /// <summary>
    /// �������� ����
    /// </summary>
    internal string Name;

    /// <summary>
    /// ����������� ������ ������ �������� � ����
    /// </summary>
    internal Int32 Min;

    /// <summary>
    /// ������������ ������ ������ �������� � ����
    /// </summary>
    internal Int32 Max;

    /// <summary>
    /// ������ ����� ����
    /// </summary>
    internal List<IMapField> FieldList
    {
      get { return _fieldList; }
    }
    List<IMapField> _fieldList = new List<IMapField>();
  }
}
