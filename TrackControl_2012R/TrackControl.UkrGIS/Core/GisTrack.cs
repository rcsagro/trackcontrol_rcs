using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.UkrGIS.Core
{
  public class GisTrack
  {
    /// <summary>
    /// ID �����
    /// </summary>
    public int Id
    {
      get { return _id; }
    }
    int _id;
    /// <summary>
    /// ���� �����
    /// </summary>
    private Color _color;
    /// <summary>
    /// ������� ����� �����
    /// </summary>
    private float _width;
    /// <summary>
    /// ���� ��� ��������� ����� ����� � ���������� � ���� ���������
    /// </summary>
    private Pen _endCapPen;
    /// <summary>
    /// ���� ��� ��������� ������� ����� ��� �����
    /// </summary>
    private Pen _pen;

    public readonly List<IGeoPoint> GeoPoints;
    /// <summary>
    /// ��������� ����� ����� � ����������� ��������
    /// </summary>
    public readonly List<Point> LocalPoints;

    #region --   .ctor   --   
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UkrGIS.Core.GisTrack
    /// </summary>
    /// <param name="id">ID �����</param>
    /// <param name="points">��������� ����� �����</param>
    /// <param name="color">���� �����</param>
    /// <param name="width">������� ����� �����</param>
    public GisTrack(int id, IEnumerable<IGeoPoint> points, Color color, float width)
    {
      GeoPoints = new List<IGeoPoint>(points);
      LocalPoints = new List<Point>(GeoPoints.Count);
      _id = id;
      _color = color;
      _width = width;
      _pen = new Pen(color, 1F);
      _endCapPen = getEndLinePen(color, 1F);
    }
    #endregion

    /// <summary>
    /// ������ GIS-���� � ��������� ����������� ���������
    /// </summary>
    /// <param name="g">����������� ��������</param>
    internal void OnRender(Graphics g)
    {
      if (LocalPoints.Count > 0)
      {
        double d1 = 0;
        double d2 = 0;
        Point last = LocalPoints[0];
        for (int i = 0; i < LocalPoints.Count; i++)
        {
          Point current = LocalPoints[i];
          d1 = getDistance(current, last);
          if (d1 > 10)
          {
            d2 += d1;
            if (d2 > 50)
            {
              g.DrawLine(_endCapPen, last.X, last.Y, current.X, current.Y);
              d2 = 0;
            }
            else
            {
              g.DrawLine(_pen, last.X, last.Y, current.X, current.Y);
            }
            last = current;
          }
        }
      }
    }

    #region --   Private Members   --
    /// <summary>
    /// ������� ���� ��� ��������� ����� � ���������� � ���� ���������
    /// </summary>
    /// <param name="color">���� ����� �����</param>
    /// <param name="width">������� ����� �����</param>
    /// <returns>���� ��� ��������� ����� � ���������� � ���� ���������</returns>
    private static Pen getEndLinePen(Color color, float width)
    {
      float sizeEndCap = 4F;
      PointF p_1 = new PointF(0, 0);
      PointF p_2 = new PointF(-1 * sizeEndCap / 3, -1 * (sizeEndCap + 0));
      PointF p_3 = new PointF(0, -1 * sizeEndCap);
      PointF p_4 = new PointF(sizeEndCap / 3, -1 * (sizeEndCap + 0));
      PointF[] points = { p_1, p_2, p_3, p_4, p_1 };
      GraphicsPath endLinePath = new GraphicsPath();
      endLinePath.AddLines(points);

      Pen endLinePen = new Pen(color, width);
      endLinePen.CustomEndCap = new CustomLineCap(endLinePath, null);

      return endLinePen;
    }

    static double getDistance(Point p1, Point p2)
    {
      return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
    }
    #endregion
  }
}
