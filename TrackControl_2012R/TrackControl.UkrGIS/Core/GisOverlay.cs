using System.Drawing;
using TrackControl.General;
using TrackControl.General.Core;

namespace TrackControl.UkrGIS.Core
{
  /// <summary>
  /// ����, ������� �������� ��� ������. �������� ����� � ������� ��������.
  /// </summary>
  public class GisOverlay
  {
    /// <summary>
    /// GIS-�������, ���������� ������ ����
    /// </summary>
    internal UkrGisMap Map;
    /// <summary>
    /// �������� ������� ����
    /// </summary>
    public string Name;
    /// <summary>
    /// ��������� ������� ���� �� �����
    /// </summary>
    public bool Visible = true;
    /// <summary>
    /// ���������������� ��������� ��������
    /// </summary>
    public readonly ObservableCollectionThreadSafe<GisMarker> Markers = new ObservableCollectionThreadSafe<GisMarker>();
    /// <summary>
    /// ���������������� ��������� ������
    /// </summary>
    public readonly ObservableCollectionThreadSafe<GisTrack> Tracks = new ObservableCollectionThreadSafe<GisTrack>();

    #region --   .ctor   --   
    /// <summary>
    /// �������������� ����� ��������� ������ TrackControl.UrkGIS.Core.GisOverlay
    /// </summary>
    /// <param name="map">GIS-�������, ���������� ������ ����</param>
    /// <param name="name">�������� ����</param>
    public GisOverlay(UkrGisMap map, string name)
    {
      Map = map;
      Name = name;
    }
    #endregion

    /// <summary>
    /// ��������� ��������� ���������� ����� ������ ���
    /// ��������� ��������� ���� ����� ��� ��������� ��������.
    /// </summary>
    public void UpdateLocalPositions()
    {
      foreach (GisTrack track in Tracks)
      {
        track.LocalPoints.Clear();
        foreach (IGeoPoint igp in track.GeoPoints)
        {
          Point p = ComUkrGIS.Instance.FromLatLngToLocal(igp.LatLng);
          track.LocalPoints.Add(p);
        }
      }
    }

    /// <summary>
    /// �������� ����� � �������
    /// </summary>
    /// <param name="g"></param>
    public void Render(Graphics g)
    {
      UpdateLocalPositions();
      
      foreach (GisTrack t in Tracks)
      {
        t.OnRender(g);
      }
      foreach (GisMarker m in Markers)
      {
        m.LocalPosition = ComUkrGIS.Instance.FromLatLngToLocal(m.Position);
        m.OnRender(g);
      }
    }
  }
}
