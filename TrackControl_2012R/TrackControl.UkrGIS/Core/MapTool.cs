using System;
namespace TrackControl.UkrGIS.Core
{
  public enum MapTool
  {
    /// <summary>
    /// ����������� �����
    /// </summary>
    Move = 1,
    /// <summary>
    /// ���������� (�����) �� �������� ����� ��� ��������
    /// </summary>
    Info = 2,
    /// <summary>
    /// -- ����� "Info*" --
    /// </summary>
    Infos = 3,
    /// <summary>
    /// ���������� (����)
    /// </summary>
    ZoomIn = 4,
    /// <summary>
    /// ���������� (����)
    /// </summary>
    ZoomOut = 5,
    /// <summary>
    /// ��������� ����������
    /// </summary>
    Distance = 6,
    /// <summary>
    /// ��������� �������
    /// </summary>
    Area = 7,
    /// <summary>
    /// �������������� �������� ��������
    /// </summary>
    PointsEditing = 8,
    /// <summary>
    /// ��������� �����
    /// </summary>
    NewLines = 9,
    /// <summary>
    /// ��������� ��������
    /// </summary>
    NewAreas = 10,
    /// <summary>
    /// �������������� ����� ��� ����� � ��������
    /// </summary>
    ApexesEditing = 11
  }
}
