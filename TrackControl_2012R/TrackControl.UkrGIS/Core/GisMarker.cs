using System.Drawing;
using TrackControl.General;

namespace TrackControl.UkrGIS.Core
{
  /// <summary>
  /// ������� ����� ��� ��������, ������������ � ��������������
  /// ���������� UkrGIS
  /// </summary>
  public class GisMarker
  {
    // ����� ������� � �������
    public static readonly Font LABEL_FONT = new Font("Tahoma", 7f, FontStyle.Bold);

    /// <summary>
    /// �������������� ���������� �������.
    /// </summary>
    private  PointLatLng _position;
    public PointLatLng Position
    {
      get { return _position; }
    }
    /// <summary>
    /// ID �������
    /// </summary>
    private int _id;
    public int Id
    {
      get { return _id; }
    }
    /// <summary>
    /// ���������� ������� �� ��������� �������� UkrGIS
    /// </summary>
    public Point LocalPosition;

    #region --   .ctor   --   
    /// <summary>
    /// �������������� ����� ����� ������ TrackControl.UrkGIS.Core.GisMarker
    /// </summary>
    /// <param name="id">ID �������</param>
    /// <param name="position">�������������� ���������� �������</param>
    public GisMarker(int id, PointLatLng position)
    {
      _id = id;
      _position = position;
    }
    #endregion

    /// <summary>
    /// ������ GIS-������ � ��������� ����������� ���������.
    /// ���������������� ���������
    /// </summary>
    /// <param name="g">����������� ��������</param>
    internal virtual void OnRender(Graphics g)
    {
      return;
    }

    /// <summary>
    /// ������ ������� � �������
    /// </summary>
    /// <param name="g">����������� ��������</param>
    public static void DrawLabel(Graphics g, string label, Brush wrapBrush, Brush stringBrush, int x, int y)
    {
      g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y - 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x, y - 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y - 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x + 1, y + 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x, y + 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, wrapBrush, x - 1, y + 1, StringFormat.GenericTypographic);
      g.DrawString(label, LABEL_FONT, stringBrush, x, y, StringFormat.GenericTypographic);
    }
  }
}
