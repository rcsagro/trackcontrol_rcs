using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using GeoData;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.UkrGIS.SearchEngine;

namespace TrackControl.UkrGIS.Core
{
    /// <summary>
    /// ������ ��� ������ � GIS-����������� EPRASYS
    /// </summary>
    public class ComUkrGIS : Singleton<ComUkrGIS>
    {
        #region --   External Functions   --

        /// <summary>
        /// ���������� "�������" ��� �������� COM-�������� �����.
        /// </summary>
        /// <param name="Factory"></param>
        [DllImport("GeoData_COM", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern void GetFactory(ref IFactory Factory);

        #endregion

        private IFactory _factory;
        private Control _parent;

        internal IMapControl Map;
        internal IBase GDB;
        private IGdbTable ZonesGDB;
        private IMapLayer ZonesLayer;
        internal IGisStyle ZonesStyle;

        /// <summary>
        /// ����������� ��� ������� TrackControl.UkrGIS.Core.ComUkrGIS
        /// </summary>
        public ComUkrGIS()
        {
            #region --   �������� �� �������������� ����������   --

            if (Instance != null)
            {
                throw new Exception(
                    "�� ��������� ������� ��������� Singlton-������, � ���������� ��� ��������������. �������� \"new ComUkrGIS\" �� \"ComUkrGIS.Instance\".");
            }

            #endregion

            try
            {
                GetFactory(ref _factory);
                Map = _factory.New_Map();
                GDB = _factory.New_Base();
                ZonesGDB = _factory.New_Table(null,
                    "CREATE TABLE TcCheckZones ( ID INT, GIS GIS, Title STR, Style INT, Changed BOOL )");
                ZonesStyle = _factory.New_StyleByText(Properties.Resources.Style);
                Map.RulerMode = 1;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString(), "Error UkrGis", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ������� ���������� �����
        /// </summary>
        public MapTool Tool
        {
            set
            {
                if(Map == null)
                    return;

                switch (value)
                {
                    case MapTool.Move:
                        Map.ToolName = "Move";
                        break;
                    case MapTool.Info:
                        Map.ToolName = "Info";
                        break;
                    case MapTool.ZoomIn:
                        Map.ToolName = "ZoomIn";
                        break;
                    case MapTool.ZoomOut:
                        Map.ToolName = "ZoomOut";
                        break;
                    case MapTool.Distance:
                        Map.ToolName = "Distance";
                        break;
                    case MapTool.Area:
                        Map.ToolName = "Area";
                        break;
                    case MapTool.PointsEditing:
                        Map.ToolName = "PointsEditing";
                        break;
                    case MapTool.NewLines:
                        Map.ToolName = "NewLines";
                        break;
                    case MapTool.NewAreas:
                        Map.ToolName = "NewAreas";
                        break;
                    case MapTool.ApexesEditing:
                        Map.ToolName = "ApexesEditing";
                        break;
                    case MapTool.Infos:
                    default:
                        Map.ToolName = "Info*";
                        break;
                }
            }
        }

        /// <summary>
        /// �������������� ����� � ������ �����, ������������ � ��������
        /// </summary>
        public PointLatLng CurrentPosition
        {
            get
            {
                if (Map == null)
                    return new PointLatLng(50, 30);

                TRealPoint p = Map.View.get_Center();
                return new PointLatLng(p.Y, p.X);
            }
            set
            {
                TRealPoint p = new TRealPoint();
                p.X = value.Lng;
                p.Y = value.Lat;

                if (Map != null)
                    Map.View.set_Center(ref p);
            }
        }

        /// <summary>
        /// ��������� ����� � ��������� ���������� (��������).
        /// </summary>
        /// <param name="control">�������, � ������� ����������� ������ �����</param>
        public void Adjust(Control control)
        {
            if (null != Map)
            {
                Map.Unadvise(_parent);
                Map.Parent = 0;
                if (null != control && control.IsHandleCreated)
                {
                    _parent = control;
                    Map.Parent = _parent.Handle.ToInt32();
                    Map.Advise(_parent);

                    TScreenRect rect;
                    rect.left = 0;
                    rect.top = 0;
                    rect.right = _parent.Width;
                    rect.bottom = _parent.Height;

                    Map.set_Bounds(ref rect);
                }
            }
        }

        /// <summary>
        /// ��������� ����� �� ���������� ���������.
        /// </summary>
        /// <param name="path">���� � ����� �����</param>
        /// <param name="dbKeys">����-����� ��� ����</param>
        /// <returns>True - ��� ������� �������� �����</returns>
        public bool Open(string path, IEnumerable<UInt32> dbKeys)
        {
            Release();
            TBaseOpenParams param;
            param.Swapping = 0;
            param.ReadOnly = 1;
            IEnumerator<UInt32> iterator = dbKeys.GetEnumerator();

            bool next = iterator.MoveNext();

            do
            {
                param.DB_key = next ? iterator.Current : 0;
                try
                {
                    GDB.Open(path, ref param);
                    Map.Project.Open(GDB, "", 0);
                    ZonesLayer = Map.Project.NewLayer(ZonesGDB);
                    ZonesLayer.LayerName = "CzMapLayer";
                    ZonesLayer.LabelFormat = "Title";
                    ZonesLayer.NormalStyle = _factory.New_StyleByText(Properties.Resources.Style);
                    Map.View.MaxScale = Globals.GIS_MAX;
                    Map.View.MinScale = Globals.GIS_MIN / 5;
                    next = false; // ����� �� �����
                }
                catch (Exception)
                {
                    next = iterator.MoveNext();
                }
            } while (next);

            if (GDB.Opened)
                GeoLocator.Instance.Adjust(Path.GetFileName(path), Map, GDB);

            return GDB.Opened;
        }

        /// <summary>
        /// ������������ �������� �����.
        /// </summary>
        public void Release()
        {
            if(GDB == null)
                return;
          
            if (GDB.Opened)
            {
                Map.Project.Close();
                GDB.Close();
            }
        }

        public void Resize()
        {
            if (null != _parent && _parent.IsHandleCreated)
            {
                TScreenRect rect;
                rect.left = 0;
                rect.top = 0;
                rect.right = _parent.Width;
                rect.bottom = _parent.Height;

                Map.set_Bounds(ref rect);
            }
        }

        /// <summary>
        /// ���������������� ����������� �����.
        /// </summary>
        public void Repaint()
        {
            if (_parent != null)
            {
                if (_parent.IsHandleCreated && (Map != null))
                {
                    if (_parent.InvokeRequired)
                    {
                        MethodInvoker m = delegate
                        {
                            Map.NoEraseBackground();
                            _parent.Invalidate(true);
                        };
                        _parent.Invoke(m);
                    }
                    else
                    {
                        Map.NoEraseBackground();
                        _parent.Invalidate(true);
                    }
                }
            }
        }

        public void Refresh()
        {
            if(Map == null)
                return;
            
            Map.Refresh();
        }

        /// <summary>
        /// ������������ �������������� ��������� �������������� ����� � 
        /// ���������� ����� �� ��������� ��������.
        /// </summary>
        /// <param name="latlng">����� � �������������� �����������</param>
        /// <returns>����� � �������� ����������� ��������</returns>
        public Point FromLatLngToLocal(PointLatLng latlng)
        {
            TRealPoint real = new TRealPoint();
            real.X = latlng.Lng;
            real.Y = latlng.Lat;
            TScreenPoint p = Map.View.MapToClient(ref real);
            return new Point(p.X, p.Y);
        }

        /// <summary>
        /// ������������� ����� ��� ��������� ����������� ��� �� ��� ����������
        /// �������������
        /// </summary>
        /// <param name="s">��������� ������������� �����</param>
        internal void SetZoneStyle(string s)
        {
            if (null != ZonesLayer)
            {
                ZonesLayer.NormalStyle = _factory.New_StyleByText(s);
            }
        }

        /// <summary>
        /// ��������� ���� � ���� ����������� ���
        /// </summary>
        /// <param name="title">�������� ����</param>
        /// <param name="points">��������� �����-������ ��������� ����</param>
        /// <param name="styleId">ID ����� ��� ��������� ����</param>
        public void AddZone(string title, IEnumerable<PointLatLng> points, int styleId)
        {
            if (null != ZonesLayer)
            {
                List<PointLatLng> pList = new List<PointLatLng>(points);
                double[] prms = new double[2*pList.Count + 2]; // ��������� ����� ������ ��������� � ������
                int index;
                for (int i = 0; i < pList.Count; i++)
                {
                    index = i*2;
                    prms[index] = pList[i].Lng;
                    prms[index + 1] = pList[i].Lat;
                }
                prms[prms.Length - 2] = pList[0].Lng;
                prms[prms.Length - 1] = pList[0].Lat;

                IGisGeometry geometry = _factory.New_Geometry(TGisGeometryKind.gisAreaGeometry);
                geometry.NewChain(1, prms);

                ZonesGDB.AddRecord();
                index = ZonesGDB.RecCount;
                ZonesLayer.set_Geometry(index, geometry);
                int fieldId_Title = ZonesGDB.FindField("Title");
                int fieldId_Style = ZonesGDB.FindField("Style");
                ZonesGDB.EditRecord(index);
                ZonesGDB.set_AsString(fieldId_Title, index, title);
                ZonesGDB.set_AsInteger(fieldId_Style, index, styleId);
                ZonesGDB.Post();
            }
        }

        public void ClearZones()
        {
            if(ZonesGDB == null)
                return;

            while (ZonesGDB.RecCount > 0)
            {
                ZonesGDB.DeleteRecord(ZonesGDB.RecCount);
            }
        }
    }
}
