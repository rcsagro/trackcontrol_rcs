using System;
using NMock2;
using NUnit.Framework;
using TrackControl.General;

namespace TrackControl.Zones.Test
{
  [TestFixture]
  public class ZonesManagerTest
  {
    Mockery _mocks;
    IZonesProvider _zonesProvider;
    IZoneGroupsProvider _groupsProvider;

    [SetUp]
    public void SetUp()
    {
      _mocks = new Mockery();
      _zonesProvider = _mocks.NewMock<IZonesProvider>();
      _groupsProvider = _mocks.NewMock<IZoneGroupsProvider>();
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_zonesProvider_isNull()
    {
      new ZonesManager(null, _groupsProvider);
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_groupsProvider_isNull()
    {
      new ZonesManager(_zonesProvider, null);
    }

    [Test]
    public void RootProperty_Should_notBe_NULL()
    {
      IZonesManager manager = new ZonesManager(_zonesProvider, _groupsProvider);
      Assert.IsNotNull(manager.Root);
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void AddGroup_Should_Throw_anException_when_group_isNull()
    {
      IZonesManager manager = new ZonesManager(_zonesProvider, _groupsProvider);
      manager.AddGroup(null);
    }

    [Test]
    public void AddingOneGroupTest()
    {
      IZonesManager manager = new ZonesManager(_zonesProvider, _groupsProvider);
      manager.AddGroup(new ZonesGroup("Group", ""));
      Assert.AreEqual(1, manager.Root.AllGroups.Count);
    }
  }
}
