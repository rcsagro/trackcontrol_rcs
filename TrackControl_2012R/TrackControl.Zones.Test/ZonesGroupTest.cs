using System;
using NUnit.Framework;
using TrackControl.General;

namespace TrackControl.Zones.Test
{
  [TestFixture]
  public class ZonesGroupTest
  {
    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_name_isNull()
    {
      ZonesGroup group = new ZonesGroup(null, "");
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_description_isNull()
    {
      ZonesGroup group = new ZonesGroup("", null);
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_style_isNull()
    {
      ZonesGroup group = new ZonesGroup("", "", null);
    }

    [Test]
    public void ZonesGroup_Should_Be_Correct_when_AddingZoneToGroup()
    {
      ZonesGroup empty = new ZonesGroup("Empty", "");
      ZonesGroup group = new ZonesGroup("Group", "");

      Zone zone = new Zone("Zone", "", empty, new ZonesStyle(), DateTime.Now, "");
      group.AddZone(zone);

      Assert.AreEqual(group, zone.Group);
      Assert.AreEqual(1, group.AllItems.Count);
      Assert.AreEqual(0, empty.AllItems.Count);
    }
  }
}
