using System;
using NUnit.Framework;
using TrackControl.General;

namespace TrackControl.Zones.Test
{
  [TestFixture]
  public class ZoneTest
  {
    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void Ctor_Should_Throw_anException_when_name_isNull()
    {
        new Zone(null, "", new ZonesGroup("Group", ""), new ZonesStyle(), DateTime.Now, "");
    }

    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void Ctor_Should_Throw_anException_when_name_isEmptyString()
    {
        new Zone("", "", new ZonesGroup("Group", ""), new ZonesStyle(), DateTime.Now,"");
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_description_isNull()
    {
        new Zone("Zone", null, new ZonesGroup("Group", ""), new ZonesStyle(), DateTime.Now, "");
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_group_isNull()
    {
        new Zone("Zone", "", null, new ZonesStyle(), DateTime.Now, "");
    }

    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Ctor_Should_Throw_anException_when_style_isNull()
    {
        new Zone("Zone", "", new ZonesGroup("Group", ""), null, DateTime.Now, "");
    }

    [Test]
    public void ZonesGroup_Should_Be_Correct_when_AssiningGroup_toZone()
    {
      ZonesGroup empty = new ZonesGroup("Empty", "");
      ZonesGroup group = new ZonesGroup("Group", "");

      Zone zone = new Zone("Zone", "", empty, new ZonesStyle(), DateTime.Now, "");
      zone.Group = group;

      Assert.AreEqual(group, zone.Group);
      Assert.AreEqual(1, group.AllItems.Count);
      Assert.AreEqual(0, empty.AllItems.Count);
    }

    [Test]
    [ExpectedException]
    public void Zone_Should_Throw_anException_when_AssiningNullGroup()
    {
      ZonesGroup group = new ZonesGroup("Group", "");
      Zone zone = new Zone("Zone", "", group, new ZonesStyle(), DateTime.Now, "");
      zone.Group = null;
    }

    [Test]
    public void BoundsPropertyTest_afterAssigning_newPoints()
    {
      ZonesGroup group = new ZonesGroup("Group", "");
      Zone zone = new Zone("Zone", "", group, new ZonesStyle(), DateTime.Now, "");
      zone.Points = new PointLatLng[]{
        new PointLatLng(50.0, 29.0),
        new PointLatLng(50.0, 31.0),
        new PointLatLng(51.0, 30.0)
      };

      RectLatLng expected = new RectLatLng(51.0, 29.0, 2.0, 1.0);
      Assert.AreEqual(expected, zone.Bounds);
    }

    [Test]
    public void IsGeometryChangedTest_afterAssining_newPoints()
    {
      ZonesGroup group = new ZonesGroup("Group", "");
      Zone zone = new Zone("Zone", "", group, new ZonesStyle(), DateTime.Now, "");
      zone.Points = new PointLatLng[]{
        new PointLatLng(50.0, 29.0),
        new PointLatLng(50.0, 31.0),
        new PointLatLng(51.0, 30.0)
      };
      Assert.IsTrue(zone.IsGeometryChanged);
    }
  }
}
