using BaseReports.Procedure;
using DevExpress.XtraSpellChecker.Parser;
using EventTracker;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using ReportsOnPassengerTraffic;
using System;
using System.Net;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Patameters;
using TrackControl.General.Services;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.GMap.UI;
using TrackControl.Hasp;
using TrackControl.MySqlDal;
using TrackControl.Notification;
using TrackControl.Online;
using TrackControl.Reports;
using TrackControl.Setting;
using TrackControl.Tuning;
using TrackControl.UkrGIS;
using TrackControl.Vehicles;
using TrackControl.Vehicles.Tuning;
using TrackControl.Zones;
using TrackControl.Zones.Tuning;

namespace TrackControl
{
    public class AppModel : Singleton<AppModel>
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        public event MethodInvoker SynhroCheckedVehiclesFromOnlineToReport;
        public event MethodInvoker SynhroCheckedVehiclesFromReportToOnline;

        public AppModel()
        {
            #region --   �������� �� �������������� ����������   --

            if (null != Instance)
            {
                throw new Exception(
                    "�� ��������� ������� ��������� Singlton-������, � ���������� ��� ��������������. �������� \"new AppModel\" �� \"AppModel.Instance\".");
            }

            #endregion

            
        }

        //public DbCommon DbCommon
        public DriverDb DbCommon
        {
            get { return _dbCommon; }
            set
            {
                _dbCommon = value;
                DataSetManager.ConnectionString = _dbCommon.CS;
            }
        }

        public VehiclesModel VehiclesModel
        {
            get
            {
                if (null == _vModel)
                {
                    VehiclesGroup root = VehiclesGroup.CreateRoot();
                    root.Style = GroupStyle.RootStyle;
                    _vModel = new VehiclesModel(root);
                    TrackControl.Temp.VehiclesModelInitializer.InitFromDataSet(_vModel, DataSet);
                    TrackControl.Temp.VehiclesModelInitializer.initPcbVersion(_vModel);
                }
                return _vModel;
            }
        }

        private VehiclesModel _vModel;

        private IZonesManager _zonesManager;

        public IZonesManager ZonesManager
        {
            get
            {
                if (null == _zonesManager)
                {
                    _zonesManager = new ZonesManager(
                        new ZonesProvider(DbCommon),
                        new ZoneGroupsProvider(DbCommon));
                    _zonesManager.StartLoading();
                }
                return _zonesManager;
            }
        }

        public ReportPassZonesModel ReportPassZonesModel
        {
            get
            {
                if (null == _reportPassZonesModel)
                    _reportPassZonesModel = new ReportPassZonesModel(ZonesManager, new ReportPassZonesProvider(_dbCommon));

                return _reportPassZonesModel;
            }
        }

        private ReportPassZonesModel _reportPassZonesModel;

        /// <summary>
        /// ������� ������� ���������� 
        /// </summary>
        public atlantaDataSet DataSet
        {
            get
            {
                if (null == _dataset)
                {
                    _dataset = new atlantaDataSet();
                    DataSetManager.Fill(_dataset);
                }

                return _dataset;
            }
        }

        private atlantaDataSet _dataset;

        public DataviewTableAdapter DataViewAdapter
        {
            get
            {
                if (null == _dataviewAdapter)
                {
                    _dataviewAdapter = new DataviewTableAdapter(DbCommon.CS);
                }
                return _dataviewAdapter;
            }
        }

        private DataviewTableAdapter _dataviewAdapter;

        /// <summary>
        /// ��������� ��� ����������� � ��������.
        /// ���������� ������������ ���� ���������
        /// </summary>
        public Notifier Notifier
        {
            get
            {
                if (null == _notifier)
                {
                    _notifier = new Notifier(DataSet, DbCommon.CS, HaspWrap.MapCodes, ZonesManager);
                }

                return _notifier;
            }
        }

        private Notifier _notifier;

        private NoticeMonitoringOnLine _monitoringOnLine;

        public NoticeMonitoringOnLine MonitoringOnLine
        {
            get
            {
                if (null == _monitoringOnLine)
                {
                    _monitoringOnLine = new NoticeMonitoringOnLine();
                }
                return _monitoringOnLine;
            }
        }

        private NoticeMonitoringPeriod _monitoringPeriod;

        public NoticeMonitoringPeriod MonitoringPeriod
        {
            get
            {
                if (null == _monitoringPeriod)
                {
                    _monitoringPeriod = new NoticeMonitoringPeriod();
                }
                return _monitoringPeriod;
            }
        }

        public DictionariesForm Dictionaries
        {
            get
            {
                if (null == _dictionaries)
                {
                    _dictionaries = new DictionariesForm();
                }
                return _dictionaries;
            }
        }

        private DictionariesForm _dictionaries;

        #region --   ������ ������ ����������   --

        /// <summary>
        /// ����� �������
        /// </summary>
        public ReportsMode ReportsMode
        {
            get
            {
                if (null == _reportsMode)
                {
                    _reportsMode = new ReportsMode(VehiclesModel, ZonesManager);
                    _reportsMode.SynhroCheckedVehicles += onSynhroVehiclesFromReportToOnline;

                }
                return _reportsMode;
            }
            set { _reportsMode = value; }
        }

        private ReportsMode _reportsMode;

        /// <summary>
        /// ����� ������ ��������
        /// </summary>
        public OnlineMode OnlineMode
        {
            get
            {
                if (null == _onlineMode)
                {
                    _onlineMode = new OnlineMode(
                        VehiclesModel,
                        ZonesManager,
                        GoogleMapControl,
                        GisMapControl,
                        new OnlineDataProvider(DbCommon), new SettingsProvider(DbCommon));
                    _onlineMode.SynhroCheckedVehicles += onSynhroVehiclesFromOnlineToReport;

                    //TrackControl.GMap.Markers.LShared.InitListMarkers();
                }
                return _onlineMode;
            }
        }

        private OnlineMode _onlineMode;

        public TuningMode TuningMode
        {
            get
            {
                if (null == _tuningMode)
                {
                    _tuningMode = new TuningMode();
                    AdminForm.SetVehicleMode( VehiclesModel );
                    _tuningMode.AddTuning(new OldMode(VehiclesModel));
                    _tuningMode.AddTuning(new DriversTuningMode(VehiclesModel, new DriverProvider(DbCommon)));
                    _tuningMode.AddTuning(new VehiclesTuningMode(VehiclesModel, new VehicleProvider(DbCommon),
                        new VehiclesGroupProvider(DbCommon)));
                    _tuningMode.AddTuning(new ZonesTuningMode(ZonesManager, GoogleMapControl));
                    _tuningMode.AddTuning(new NoticeTuning());
                }
                return _tuningMode;
            }
        }

        private TuningMode _tuningMode;

        public void initReportsObjects()
        {
            _reportsMode.initObjects();
        }

        #endregion

        #region --   ���������� ����������   --

        /// <summary>
        /// ������� ����� Eprasys UkrGIS
        /// </summary>
        public GisMapControl GisMapControl
        {
            get
            {
                if (null == _gisMapControl)
                {
                    _gisMapControl = new GisMapControl();
                    _gisMapControl.Dock = DockStyle.Fill;
                }
                return _gisMapControl;
            }
        }

        private GisMapControl _gisMapControl;

        /// <summary>
        /// ������� ����� Google
        /// </summary>
        public GoogleMapControl GoogleMapControl
        {
            get
            {
                if (null == _googleMapControl)
                {
                    _googleMapControl = new GoogleMapControl();
                    _googleMapControl.Dock = DockStyle.Fill;
                    ProxySettings ps = ProxyService.FetchSettings();

                    IWebProxy iWebProxy = ProxyService.FetchSettings().Proxy;
                    if (null != iWebProxy)
                    {
                        if (ps.IsUserAuthenticationNeeded)
                        {
                            NetworkCredential credential = new NetworkCredential(ps.Login, ps.Password);
                            iWebProxy.Credentials = credential;
                        }
                        else
                            iWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                        GMaps.Instance.Proxy = iWebProxy;
                    }

                    ProxyService.ProxyChanged += delegate(WebProxy proxy)
                    {
                        if (null != proxy)
                        {
                            ps = ProxyService.FetchSettings();
                            if (ps.IsUserAuthenticationNeeded)
                            {
                                NetworkCredential credential = new NetworkCredential(ps.Login, ps.Password);
                                proxy.Credentials = credential;
                            }
                            else
                                proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                            GMaps.Instance.Proxy = proxy;
                        }
                    };

                    TotalParamsProvider totpar = new TotalParamsProvider();
                    GMapOverlay.IsRcsNominatimUse = totpar.IsRcsNominatimUse; // �������� � ����������
                    GMapOverlay.IsOpenStreetMapUse = totpar.IsOpenStreetMapUse; // ������� � ����������
                    BuildGraphs.IsGraphFuelShow = totpar.IsGraphFuelShow; // �������� � ����������
                    GMapOverlay.IsOpenStreetMapUse = totpar.IsOpenStreetMapUse; // �������� � ����������
                    Kilometrage.MinTimeMove = Convert.ToDouble(totpar.MinTimeMove); // �������� � ����������
                }
                return _googleMapControl;
            }
        }

        private GoogleMapControl _googleMapControl;

        #endregion

        private void onSynhroVehiclesFromOnlineToReport()
        {
            if (SynhroCheckedVehiclesFromOnlineToReport != null)
                SynhroCheckedVehiclesFromOnlineToReport();
        }

        private void onSynhroVehiclesFromReportToOnline()
        {
            if (SynhroCheckedVehiclesFromReportToOnline != null)
                SynhroCheckedVehiclesFromReportToOnline();
        }
    }
}
